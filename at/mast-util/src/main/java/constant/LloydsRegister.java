package constant;

import helper.TestDataHelper;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static constant.LloydsRegister.RandomiseType.DIGITS;
import static constant.LloydsRegister.RandomiseType.LETTERS;

/**
 * Constant file to define all the neccesary information needed in the test scripts
 * <p>
 * Created by JHuxtable on 11/08/2015.
 */
public class LloydsRegister
{

    // DateFormat
    public static final SimpleDateFormat BACKEND_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat FRONTEND_TIME_FORMAT = new SimpleDateFormat("dd MMM yyyy");
    public static final DateTimeFormatter FRONTEND_TIME_FORMAT_DTS = DateTimeFormatter.ofPattern("dd MMM yyyy");

    // Default Employee
    public static final long DEFAULT_EMPLOYEE = 3;

    //File Path to Upload
    public static final File fileToUpload = new File("Attachment/SamplePack.docx");
    public static final String gridFileToUpload = "C:\\Grid\\SamplePack.docx";
    public static final String fileName = "SamplePack.docx";

    // REGEX Format
    public static final String SHOWING_REGEX = "([0-9]{0,3}) of";
    public static final String TOTAL_REGEX = "of ([0-9]{0,5})";
    public static final String NUMBER_REGEX = "[0-9]*$";

    // asset item relationshiptype
    public static final long ASSET_ITEM_RELATIONSHIP_TYPE_ID_IS_RELATED_TO = 9;

    // Family Product Statutory
    public static final String PRODUCT_FAMILY_STATUTORY = "3";

    // Local Date Object
    public static final LocalDate localDate = LocalDate.now();

    // Active Job Statuses
    public static final List<String> ACTIVE_JOB_STATUSES()
    {
        final List<String> activeJobStatuses = new ArrayList<>();
        activeJobStatuses.add("SDO Assigned");
        activeJobStatuses.add("Resource Assigned");
        activeJobStatuses.add("Under Survey");
        activeJobStatuses.add("Under Reporting");
        activeJobStatuses.add("Awaiting TR Assignment");
        activeJobStatuses.add("Under TR");
        activeJobStatuses.add("Awaiting Endorser Assignment");
        activeJobStatuses.add("Under Endorsement");

        return activeJobStatuses;
    }

    public static final class JobData
    {
        public static final String sdoCode = "Liverpool";
        public static final String caseType = "123";
        public static final String location = "Auto";
        public static final String description = "Auto KL Job Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        public static final String strEtaDate = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(1))));
        public static final String strEtdDate = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(2))));
        public static final String zeroVisit = "No";
        public static final String jobCategory = "Survey";
    }

    public static final class CocData
    {
        public static final String title = "Auto";
        public static final String description = "Auto KL Coc Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        public static final String expectedImposedDateErrorMessage = "The imposed date cannot be in the past.";
        public static final String expectedImposedInvalidDateMessage = "Date is not valid. Must be in the format 'DD MMM YYYY'";
        public static final String expectedTitleHelperText = "50 character limit";
        public static final String expectedDescriptionHelperText = "2000 character limit";
    }

    public static final class BatchActionData
    {
        public static final String title = "Auto";
        public static final String description = "Auto KL BatchActions Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        public static final String surveyorGuidance = "Auto KL Surveyor guidance" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
    }

    public static final class JobTeamData
    {
        public static final String leadSurveyor = "Allan Hyde";
    }

    public static final class DefectData
    {
        public static final String title = "Auto";
        public static final String description = "Auto KL Ai Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
    }

    public static final class AiData
    {
        public static final String title = "Auto";
        public static final String description = "Auto KL Ai Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        public static final String surveyorGuidance = "Auto KL Surveyor Guidance" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        public static final String expectedImposedDateErrorMessage = "The imposed date cannot be in the past.";
        public static final String expectedImposedInvalidDateMessage = "Date is not valid. Must be in the format 'DD MMM YYYY'";
        public static final String expectedTitleHelperText = "50 character limit";
        public static final String expectedDescriptionSurveyorGuidanceHelperText = "2000 character limit";
    }

    public static final class AssetData
    {
        public static final String builder = "Auto Builder";
        public static final Integer yardNumber = 123;
        public static final String assetName = "Auto KL";
        public static final String dateOfBuild = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now())));
        public static final String assetCategory = "Vessel";
        public static final String classSection = "Naval";
        public static final String statCode1 = "Cargo Carrying";
        public static final String statCode2 = "Tankers";
        public static final String statCode3 = "Liquefied Gas";
        public static final String statCode4 = "LNG Tanker";
        public static final String statCode5 = "LNG Tanker";
    }

    public static final class CaseData
    {
        public static final String contractReferenceData = "Auto Builder KL";
        public static final String losingSocietyData = "Bureau";
        public static final String caseAcceptanceDateData = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(5))));
        public static final String estimatedBuildDateData = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(8))));
        public static final int grossTonnageData = 2;
        public static final String portOfRegistryData = "Chi";
        public static final String proposedFlagData = "Ang";
        public static final String currentFlagData = "Alg";
        public static final String caseOfficeData = "Gij";
        public static final String caseOwnerData = "Ra";
        public static final String caseAdminData = "James";
    }

    // Compare dates
    public static final Comparator<java.util.Date> compareDate()
    {
        final Comparator<java.util.Date> byMostRecent = (o1, o2) ->
        {
            if (o1.before(o2))
                return 1;
            else if (o1.equals(o2))
                return 0;
            else
                return -1;
        };
        return byMostRecent;
    }

    public static String randomiseConstant(final Object value, final RandomiseType randomiseType)
    {
        if (randomiseType == DIGITS)
        {
            return value.toString() + RandomStringUtils.randomNumeric(5);
        }
        else if (randomiseType == LETTERS)
        {
            return value.toString() + " " + RandomStringUtils.randomAlphabetic(5);
        }
        else
        {
            return value.toString();
        }
    }

    public static String randomiseConstant(final Object value)
    {
        return randomiseConstant(value, DIGITS);
    }

    public enum RandomiseType
    {
        DIGITS,
        LETTERS
    }

    //Expected Life Cycle Status
    public final static String[] expectedLifecycleStatusList = {
            "Under construction",
            "Build suspended",
            "In service",
            "Laid up",
            "Under repair",
            "In casualty",
            "Converting",
            "To be broken up",
            "Decommissioned",
            "To be handed over",
            "Cancelled"};
}
