package constant;

public enum Request
{
    GET, PUT, POST, DELETE
}
