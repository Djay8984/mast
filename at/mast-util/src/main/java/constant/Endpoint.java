package constant;

public enum Endpoint
{

    ASSET("/asset/%s"),
    ASSET_ACTIONABLE_ITEM("/asset/%s/actionable-item"),
    ASSET_ACTIONABLE_ITEM_QUERY("/asset/%s/actionable-item/query?page=%s&size=%s"),
    ASSET_ATTACHMENT("/asset/%s/attachment/%s"),
    ASSET_ATTACHMENT_ATTACHMENTID("/asset/%s/attachment/%s"),
    ASSET_ATTACHMENT_PAGE("/asset/%s/attachment/?page=%s&size=%s"),
    ASSET_CASE("/asset/%s/case"),
    ASSET_COC("/asset/%s/coc/%s"),
    ASSET_COC_PAGE("/asset/%s/coc/?page=%s&size=%s"),
    ASSET_COC_STATUSID("/asset/%s/coc?statusId=%s"),
    ASSET_COC_QUERY("/asset/%s/coc/query"),
    ASSET_DEFECT("/asset/%s/defect/?page=%s&size=%s"),
    ASSET_DEFECT_QUERY("/asset/%s/defect/query?page=%s&size=%s"),
    ASSET_DEFECT_STATUSID("/asset/%s/defect?statusId=%s"),
    ASSET_ITEM("/asset/%s/item/%s"),
    ASSET_ITEM_QUERY("/asset/%s/item/query"),
    ASSET_ITEM_ATTRIBUTE("/asset/%s/item/%s/attribute/%s"),
    ASSET_ITEM_RELATIONSHIP("/asset/%s/item/%s/relationship/%s"),
    ASSET_NOTE("/asset/%s/asset-note/%s"),
    ASSET_NOTE_PAGE("/asset/%s/asset-note/?page=%s&size=%s"),
    ASSET_NOTE_STATUSID("/asset/%s/asset-note?statusId=%s"),
    ASSET_NOTE_QUERY("/asset/%s/asset-note/query"),
    ASSET_QUERY("/asset/query/"),
    ASSET_ROOT_ITEM("/asset/%s/root-item"),
    ASSET_ROOT_DRAFT_ITEM("/asset/%s/root-draft-item"),
    ASSET_PAGE("/asset/?page=%s&size=%s"),
    ASSET_MODEL("/asset-model/%s?version=%s"),
    ASSET_CATEGORY("/asset-category/%s"),
    ASSET_TYPE("/asset-type/%s"),
    ASSET_SERVICE("/asset/%s/service/%s"),
    ATTACHMENT_LOCATION("/attachment-location"),
    ATTRIBUTE_TYPE("/attribute-type/%s"),
    BUILDER("/builder/%s"),
    BUILDER_PAGE("/builder/?page=%s&size=%s"),
    CASE("/case/%s"),
    CASE_PAGE("/case/?page=%s&size=%s"),
    CASE_NOTE("/note/%s"),
    CASE_ATTACHMENT("/case/%s/attachment/%s"),
    CASE_QUERY_PAGE("/case/query/"),
    CUSTOMER("/customer/%s"),
    CUSTOMER_PAGE("/customer/?page=%s&size=%s"),
    DEFECT("/defect/%s"),
    DEFECT_POST("/defect/"),
    DEFECT_REPAIR_PAGE("/defect/%s/repair/?page=%s&size=%s"),
    DEFECT_REPAIR("/defect/%s/repair"),
    DEFECT_REPAIR_REPAIR_ID("/defect/%s/repair/%s"),
    DRAFT_ASSET_ITEM("/asset/%s/draft-item/%s?useOriginalId=true"),
    EMPLOYEE_EMPLOYEEID("/employee/%s"),
    EMPLOYEE("/employee/?search=%s"),
    FLAG_STATE("/flag-state/%s"),
    FLAG_STATE_PAGE("/flag-state/?page=%s&size=%s"),
    IACS_SOCIETY("/iacs-society/%s"),
    IHS_NUMBER("/ihs/%s"),
    HARMONISATION_DATE("/asset/%s/harmonisation-date"),
    JOBS_FOR_ASSET("/asset/%s/job"),
    JOB("/job/%s"),
    JOB_PAGE("/job/?page=%s&size=%s"),
    JOB_QUERY("/job/query?order=ASC"),
    JOB_WIP_ACTIONABLE_ITEM("/job/%s/wip-actionable-item/%s"),
    JOB_WIP_CERT("/job/%s/wip-certificate/%s"),
    JOB_WIP_CERT_PAGE("/job/%s/wip-certificate/?page=%s&size=%s"),
    MILESTONE("/case/%s/milestone"),
    OFFICE("/office/%s"),
    OFFICE_PAGE("/office/?page=%s&size=%s"),
    OFFICE_ROLE("/office-role/%s"),
    OFFICE_WITH_ROLE("/office/?size=20&roleName=%s"),
    PORT_OF_REGISTRY("/port-of-registry/%s"),
    PRE_EIC_INSPECTION_STATUS("/pre-eic-inspection-status/%s"),
    PRODUCT("/asset/%s/product"),
    PRODUCT_AVAILABLE("/asset/%s/available-product"),
    PRODUCT_MODEL("/product-model"),
    RISK_ASSESSMENT("/risk-assessment-status/%s"),
    REFERENCE_DATA_MAP("/reference-data/%s/%s"),
    REPORT("/job/%s/report/%s"),
    RULE_SET("/rule-set"),
    SERVICE_ASSET("/asset/%s/service"),
    SERVICE_CATALOGUE("/service-catalogue/%s"),
    SERVICE_JOB("/job/%s/service"),
    SCHEDULE_RULE_TYPE("/service-catalogue/%s/scheduling-rule-type"),
    SERVICE_AVAILABLE("/asset/%s/product/%s/available-service"),
    SURVEY("/job/%s/survey/%s"),
    SURVEYOR("/surveyor/%s"),
    SURVEY_STATUS("/survey-status/%s"),
    SURVEYOR_PAGE("/surveyor?page=%s&size=%s"),
    SURVEYOR_PAGE_WITH_OFFICE("/surveyor?page=%s&size=%s&officeId=%s"),
    SURVEYOR_PAGE_WITH_SEARCH("/surveyor?page=%s&size=%s&search=%s"),
    WIP_ASSET_NOTE("/job/%s/wip-asset-note/%s"),
    WIP_ASSET_NOTE_PAGE("/job/%s/wip-asset-note/?page=%s&size=%s"),
    WIP_COC("/job/%s/wip-coc/%s"),
    WIP_COC_DEFECT("/job/%s/wip-coc?defectId=%s"),
    WIP_COC_QUERY("/job/%s/wip-coc/query");

    private String endpoint;

    Endpoint(String endpoint)
    {
        this.endpoint = endpoint;
    }

    public String getUrl(String[] values)
    {
        return String.format(endpoint, values);
    }

    public String getUrl(String value)
    {
        return String.format(endpoint, value);
    }

    public String getUrl()
    {
        return endpoint;
    }
}
