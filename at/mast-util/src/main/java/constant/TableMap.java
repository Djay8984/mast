package constant;

import java.util.HashMap;

public enum TableMap
{

    ACTIONABLE_ITEM(new HashMap<String, Object>()
    {{
        put("getDescription", "narrative");
        put("getImposedDate", "imposed_date");
        put("getDueDate", "due_date");
        put("getRequireApproval", "require_approval");
        put("getSurveyorGuidance", "surveyor_guidance");
        put("getEditableBySurveyor", "editable_by_surveyor");
        put("getCategory", new HashMap<String, String>()
        {{
            put("getId", "category_id");
        }});
        put("getConfidentialityType", new HashMap<String, String>()
        {{
            put("getId", "confidentiality_type_id");
        }});
        put("getJob", new HashMap<String, String>()
        {{
            put("getId", "job_id");
        }});
        put("getStatus", new HashMap<String, String>()
        {{
            put("getId", "status_id");
        }});
        put("getDefect", new HashMap<String, String>()
        {{
            put("getId", "defect_id");
        }});
        put("getAssetItem", new HashMap<String, String>()
        {{
            put("getId", "asset_item_id");
        }});
        put("getEmployee", new HashMap<String, String>()
        {{
            put("getId", "employee_id");
        }});
        put("getTemplate", new HashMap<String, String>()
        {{
            put("getId", "template_id");
        }});
        put("getParent", new HashMap<String, String>()
        {{
            put("getId", "parent_id");
        }});
    }}),
    ASSET(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getBuildDate", "build_date");
        put("getKeelLayingDate", "keel_laying_date");
        put("getGrossTonnage", "gross_tonnage");
        put("getClassNotation", "class_notation");
        put("getYardNumber", "primary_yard_number");
        put("getHarmonisationDate", "harmonisation_date");
        put("getIhsAsset", new HashMap<String, String>()
        {{
            put("getId", "imo_number");
        }});
    }}),
    ASSET_DEFECT(new HashMap<String, Object>()
    {{
        put("getFirstFrameNumber", "first_frame_number");
        put("getLastFrameNumber", "last_frame_number");
        put("getPromptThoroughRepair", "prompt_thorough_flag");
        put("getIncidentDate", "incident_date");
        put("getIncidentDescription", "description");
        put("getTitle", "title");
    }}),
    ASSET_DRAFT_ITEM(new HashMap<String, Object>()
    {{
        put("getId", "id");
        put("getName", "name");
        put("getReviewed", "reviewed");
    }}),
    ASSET_ITEM(new HashMap<String, Object>()
    {{
        put("getId", "id");
        put("getName", "name");
        put("getReviewed", "reviewed");
    }}),
    ASSET_ITEM_ATTRIBUTE(new HashMap<String, Object>()
    {{
        put("getId", "id");
        put("getValue", "value");
    }}),
    ASSET_ITEMS_QUERY(new HashMap<String, Object>()
    {{
        put("getItems", new HashMap<String, String>()
        {{
            put("getId", "id");
        }});
        put("getRelated", new HashMap<String, Object>()
        {{
            put("getId", "id");
            put("getToItem", new HashMap<String, String>()
            {{
                put("getId", "to_item_id");
            }});
            put("getType", new HashMap<String, String>()
            {{
                put("getId", "relationship_type_id");
            }});
        }});
        put("getAttributes", new HashMap<String, Object>()
        {{
            put("getId", "id");
            put("getValue", "value");
            put("getAttributeType", new HashMap<String, String>()
            {{
                put("getId", "attribute_type_id");
            }});
        }});
    }}),
    ASSET_ITEM_QUERY(new HashMap<String, Object>()
    {{
        put("getItemId", "ai.id");
        put("getItemTypeId", "item_type_id");
        put("getAttributeId", "aa.id");
        put("getItemName", "name");
        put("getAttributeValue", "value");
    }}),
    ASSET_ITEM_RELATIONSHIP(new HashMap<String, Object>()
    {{
        put("getToItem", new HashMap<String, String>()
        {{
            put("getId", "to_item_id");
        }});
        put("getType", new HashMap<String, String>()
        {{
            put("getId", "relationship_type_id");
        }});
    }}),
    ASSET_NOTE(new HashMap<String, Object>()
    {{
        put("getTemplate", new HashMap<String, String>()
        {{
            put("getId", "template_id");
        }});
        put("getTitle", "title");
        put("getImposedDate", "imposed_date");
        put("getDueDate", "due_date");
        put("getDescription", "narrative");
        put("getStatus", new HashMap<String, String>()
        {{
            put("getId", "status_id");
        }});
        put("getAssetItem", new HashMap<String, String>()
        {{
            put("getId", "asset_item_id");
        }});
        put("getJob", new HashMap<String, String>()
        {{
            put("getId", "job_id");
        }});
        put("getAsset", new HashMap<String, String>()
        {{
            put("getId", "asset_id");
        }});
        put("getInheritedFlag", "inherited_flag");
        put("getActionTaken", "action_taken");
        put("getEmployee", new HashMap<String, String>()
        {{
            put("getId", "employee_id");
        }});
        put("getConfidentialityType", new HashMap<String, String>()
        {{
            put("getId", "confidentiality_type_id");
        }});
        put("getCategory", new HashMap<String, String>()
        {{
            put("getId", "category_id");
        }});
        put("getSurveyorGuidance", "surveyor_guidance");
    }}),
    CODICIL_QUERY(new HashMap<String, Object>()
    {{
        put("getSearchString", "title");
        put("getCategoryList", "category_id");
        put("getStatusList", "status_id");
        put("getConfidentialityList", "confidentiality_type_id");
        put("getDueDateMin", "due_date");
        put("getDueDateMax", "due_date");
        put("getItemTypeList", "asset_item_id");
    }}),
    ASSET_QUERY(new HashMap<String, Object>()
    {{
        put("getSearch", "name");
        put("getLifecycleStatusId", "asset_lifecycle_status_id");
        put("getImoNumber", "imo_number");
        put("getName", "name");
        put("getAssetTypeId", "asset_type_id");
        put("getYardNumber", "primary_yard_number");
        put("getBuildDateMin", "build_date");
        put("getBuildDateMax", "build_date");
        put("getClassStatusId", "class_status_id");
        put("getFlagStateId", "flag_state_id");
        put("getGrossTonnageMin", "gross_tonnage");
        put("getGrossTonnageMax", "gross_tonnage");
        put("getPartyId", "party_id");
        put("getPartyFunctionTypeId", "party_role_id");
        put("getPartyRoleId", "party_role_id");
        put("getBuilder", "builder");
        put("getIdList", "id");
        put("getCategoryId", "asset_category_id");
    }}),
    ASSET_CODICIL(new HashMap<String, Object>()
    {{
        put("getTemplate", new HashMap<String, String>()
        {{
            put("getId", "template_id");
        }});
        put("getTitle", "title");
        put("getImposedDate", "imposed_date");
        put("getDueDate", "due_date");
        put("getDescription", "narrative");
        put("getStatus", new HashMap<String, String>()
        {{
            put("getId", "status_id");
        }});
        put("getAssetItem", new HashMap<String, String>()
        {{
            put("getId", "asset_item_id");
        }});
        put("getJob", new HashMap<String, String>()
        {{
            put("getId", "job_id");
        }});
        put("getAsset", new HashMap<String, String>()
        {{
            put("getId", "asset_id");
        }});
        put("getDefect", new HashMap<String, String>()
        {{
            put("getId", "defect_id");
        }});
        put("getInheritedFlag", "inherited_flag");
        put("getActionTaken", "action_taken");
        put("getEmployee", new HashMap<String, String>()
        {{
            put("getId", "employee_id");
        }});
        put("getEditableBySurveyor", "editable_by_surveyor");
        put("getConfidentialityType", new HashMap<String, String>()
        {{
            put("getId", "confidentiality_type_id");
        }});
        put("getCategory", new HashMap<String, String>()
        {{
            put("getId", "category_id");
        }});
        put("getSurveyorGuidance", "surveyor_guidance");
        put("getRequireApproval", "require_approval");
    }}),
    ATTACHMENT(new HashMap<String, Object>()
    {{
        put("getTitle", "title");
        put("getNote", "note");
        put("getAttachmentUrl", "attachment_url");
        put("getAssetLocationViewpoint", "asset_location_viewpoint");
        put("getAuthor", new HashMap<String, String>()
        {{
            put("getId", "author_id");
        }});
    }}),
    ATTRIBUTE_TYPE(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getDescription", "description");
        put("getDescriptionOrder", "description_order");
        put("getNamingOrder", "naming_order");
        put("getNamingDecoration", "naming_decoration");
        put("getDisplayOrder", "display_order");
    }}),
    CASE(new HashMap<String, Object>()
    {{
        put("getAsset", new HashMap<String, String>()
        {{
            put("getId", "asset_id");
        }});
        put("getCaseType", new HashMap<String, String>()
        {{
            put("getId", "case_type_id");
        }});
        put("getCaseStatus", new HashMap<String, String>()
        {{
            put("getId", "case_status_id");
        }});
        put("getPreEicInspectionStatus", new HashMap<String, String>()
        {{
            put("getId", "pre_eic_inspection_status_id");
        }});
        put("getRiskAssessmentStatus", new HashMap<String, String>()
        {{
            put("getId", "risk_assessment_status_id");
        }});
        put("getTocAcceptanceDate", "toc_acceptance_date");
        put("getContractReferenceNumber", "contract_reference");
        put("getCaseAcceptanceDate", "case_acceptance_date");
    }}),
    CASE_NOTE(new HashMap<String, Object>()
    {{
        put("getCreationDate", "creation_date");
        put("getNote", "note");
    }}),
    CASE_PAGE(new HashMap<String, Object>()
    {{
        put("getAsset", new HashMap<String, String>()
        {{
            put("getId", "asset_id");
        }});
        put("getCaseType", new HashMap<String, String>()
        {{
            put("getId", "case_type_id");
        }});
        put("getCaseStatus", new HashMap<String, String>()
        {{
            put("getId", "case_status_id");
        }});
        put("getPreEicInspectionStatus", new HashMap<String, String>()
        {{
            put("getId", "pre_eic_inspection_status_id");
        }});
        put("getRiskAssessmentStatus", new HashMap<String, String>()
        {{
            put("getId", "risk_assessment_status_id");
        }});
        put("getTocAcceptanceDate", "toc_acceptance_date");
        put("getContractReferenceNumber", "contract_reference");
        put("getCaseAcceptanceDate", "case_acceptance_date");
    }}),
    CASE_QUERY(new HashMap<String, Object>()
    {{
        put("getCaseStatusId", "case_status_id");
        put("getEmployeeId", "employee_id");
        put("getAssetId", "asset_id");
    }}),
    CASE_QUERY_RESPONSE(new HashMap<String, Object>()
    {{
        put("getAsset", new HashMap<String, String>()
        {{
            put("getId", "asset_id");
        }});
        put("getCaseType", new HashMap<String, String>()
        {{
            put("getId", "case_type_id");
        }});
        put("getCaseStatus", new HashMap<String, String>()
        {{
            put("getId", "case_status_id");
        }});
        put("getPreEicInspectionStatus", new HashMap<String, String>()
        {{
            put("getId", "pre_eic_inspection_status_id");
        }});
        put("getRiskAssessmentStatus", new HashMap<String, String>()
        {{
            put("getId", "risk_assessment_status_id");
        }});
        put("getTocAcceptanceDate", "toc_acceptance_date");
        put("getContractReferenceNumber", "contract_reference");
        put("getCaseAcceptanceDate", "case_acceptance_date");
    }}),
    CUSTOMER(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getPhoneNumber", "phone_number");
        put("getFaxNumber", "fax_number");
        put("getCdhCustomerId", "cdh_customer_id");
        put("getJdeReferenceNumber", "jde_reference_number");
    }}),
    DEFECT(new HashMap<String, Object>()
    {{
        put("getFirstFrameNumber", "first_frame_number");
        put("getLastFrameNumber", "last_frame_number");
        put("getPromptThoroughRepair", "prompt_thorough_flag");
        put("getIncidentDate", "incident_date");
        put("getIncidentDescription", "description");
        put("getTitle", "title");
    }}),
    DEFECT_REPAIR(new HashMap<String, Object>()
    {{
        put("getDescription", "description");
        put("getPromptThoroughRepair", "prompt_thorough_flag");
        put("getNarrative", "narrative");
        put("getRepairTypeDescription", "repair_type_description");
        put("getConfirmed", "confirmed");
    }}),
    DEFECT_QUERY(new HashMap<String, Object>()
    {{
        put("getSearchString", "title");
        put("getCategoryList", "defect_category_id");
        put("getStatusList", "defect_status_id");
        put("getConfidentialityList", "confidentiality_type_id");
        put("getDueDateMin", "incident_date");
        put("getDueDateMax", "incident_date");
        //put("getItemTypeList", "asset_item_id");
    }}),
    EMPLOYEE(new HashMap<String, Object>()
    {{
        put("getOneWorldNumber", "oneworld_number");
        put("getFirstName", "first_name");
        put("getLastName", "last_name");
        put("getEmailAddress", "email_address");

    }}),
    HARMONISATION_DATE(new HashMap<String, Object>()
    {{
        put("getDate", "harmonisation_date");
    }}),
    IHS_NUMBER(new HashMap<String, Object>()
    {{
        put("getYardNumber", "YARDNO");

    }}),
    JOB(new HashMap<String, Object>()
    {{
        put("getNote", "notes");
        put("getRequestByTelephoneIndicator", "request_by_telephone_flag");
        put("getStatusReason", "status_reason");
        put("getWorkOrderNumber", "work_order_number");
        put("getWrittenServiceRequestReceivedDate", "written_service_request_received_date");
        put("getWrittenServiceResponseSentDate", "written_service_response_sent_date");
    }}),
    JOB_WIP_CERTIFICATE(new HashMap<String, Object>()
    {{
        put("getCertificateNumber", "certificate_number");
        put("getExpiryDate", "expiry_date");
        put("getExtendedDate", "extended_date");
        put("getIssueDate", "issue_date");
    }}),
    JOB_QUERY(new HashMap<String, Object>()
    {{
        put("getJobStatusId", "status_id");
        put("getEmployeeId", "lr_employee_id");
        put("getOfficeId", "lr_office_id");
        put("getAssetId", "asset_id");
        put("getSearch", "j.id");
    }}),
    MILESTONE(new HashMap<String, Object>()
    {{
        put("getCaseMilestoneList", new HashMap<String, String>()
        {{
            put("getCompletionDate", "completion_date");
            put("getDueDate", "due_date");
            put("getInScope", "in_scope_flag");
        }});
    }}),
    NAME(new HashMap<String, Object>()
    {{
        put("getName", "name");
    }}),
    PRODUCT(new HashMap<String, Object>()
    {{
        put("getName", "product_name");
        put("getProductCatalogueId", "product_id");
        put("getDescription", "product_description");
        put("getDisplayOrder", "display_order");
    }}),
    REFERENCE_DATA(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getDescription", "description");
    }}),
    REPORT(new HashMap<String, Object>()
    {{
        put("getReportVersion", "report_version");
        put("getClassRecommendation", "recommendation");
        put("getIssueDate", "issue_date");
    }}),
    SERVICE(new HashMap<String, Object>()
    {{
        put("getServiceCatalogueId", "service_catalogue_id");
        put("getCyclePeriodicity", "cycle_periodicity");
        put("getAssignedDate", "assigned_date");
        put("getDueDate", "due_date");
        put("getLowerRangeDate", "lower_range_date");
        put("getUpperRangeDate", "upper_range_date");
    }}),
    SURVEY(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getDateOfCrediting", "date_of_crediting");
        put("getScheduleDatesUpdated", "schedule_dates_updated");
    }}),
    SURVEY_FULL(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getDateOfCrediting", "date_of_crediting");
        put("getScheduleDatesUpdated", "schedule_dates_updated");
    }}),
    SURVEY_REFERENCE(new HashMap<String, Object>()
    {{
        put("getName", "name");
    }}),
    SOCIETY_RULESET(new HashMap<String, Object>()
    {{
        put("getName", "name");
        put("getCategory", "category");
        put("getIsLrRuleset", "is_lr_ruleset");
    }}),
    WIP_COC(new HashMap<String, Object>()
    {{
        put("getImposedDate", "imposed_date");
        put("getDueDate", "due_date");
        put("getDescription", "narrative");
        put("getActionTaken", "action_taken");
        put("getInheritedFlag", "inherited_flag");
    }});

    private HashMap<String, Object> tableMap;

    TableMap(HashMap<String, Object> tableMap)
    {
        this.tableMap = tableMap;
    }

    public HashMap<String, Object> getTableMap()
    {
        return tableMap;
    }

}
