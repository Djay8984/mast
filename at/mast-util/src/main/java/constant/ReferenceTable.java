package constant;

public enum ReferenceTable
{

    ASSET_CATEGORY("mast_ref_asset_category"),
    ATTACHMENT_LOCATION("mast_ref_attachmentlocation"),
    IACS_SOCIETY("mast_ref_classsociety"),
    OFFICE_ROLE("MAST_REF_OFFICEROLE"),
    PORT_OF_REGISTRY("MAST_REF_PORT"),
    PRE_EIC_INSPECTION_STATUS("mast_ref_preeicinspectionstatus"),
    RISK_ASSESSMENT("mast_ref_riskassessmentstatus"),
    SCHEDULE_RULE_TYPE("MAST_REF_SERVICE_CATALOGUE_SCHEDULING_RULE_TYPE"),
    SURVEY("mast_db.mast_ref_surveystatus");

    private String tableName;

    ReferenceTable(String tableName)
    {
        this.tableName = tableName;
    }

    public String getTableName()
    {
        return tableName;
    }
}
