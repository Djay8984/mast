package typifiedelement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class RadioButton extends TypifiedElement implements Locatable
{

    // LABEL_XPATH is needed because it is the only interactable DOM
    // the input tag itself holds other properties
    private final String LABEL_XPATH = "./following-sibling::label";

    /**
     * Specifies a radio button of a radio button group that will be used to find all other buttons of this group.
     *
     * @param wrappedElement {@code WebElement} representing radio button.
     */
    public RadioButton(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    /**
     * Returns the radio button
     *
     * @return {@code WebElements} representing a radio button.
     */
    private WebElement getButton()
    {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("input"))
            xpath = "./following::input[@type = 'radio'] | ./preceding::input[@type = " +
                    "'radio']";
        else
            xpath = "self::input";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    /**
     * Select a radio button if it's not already selected.
     */
    public void select()
    {
        if (!this.isSelected())
        {
            getButton().findElement(By.xpath(LABEL_XPATH)).click();
        }
    }

    /**
     * Deselect a radio button if it's already selected
     */
    public void deselect()
    {
        if (this.isSelected())
        {
            getButton().findElement(By.xpath(LABEL_XPATH)).click();
        }
    }

    /**
     * Check that it is selected
     */
    @Override
    public boolean isSelected()
    {
        String b = getButton().getAttribute("checked");
        return b != null && b.equals("true");
    }

    @Override
    public Coordinates getCoordinates()
    {
        return null;
    }
}



