package typifiedelement;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class HoverOver extends TypifiedElement
{
    private WebElement hoverOverIcon;

    public HoverOver(WebElement hoverOverIcon)
    {
        super(hoverOverIcon);
        this.hoverOverIcon = hoverOverIcon;
    }

    public <T extends HtmlElement> T openHoverOverElement(T hoverOverShows)
    {
        Actions actions = new Actions(BaseTest.getDriver());
        actions.moveToElement(hoverOverIcon).click();
        actions.build().perform();
        BaseTest.getWait().until(ExpectedConditions.visibilityOf(hoverOverShows));
        return hoverOverShows;
    }

    public void closeHoverOverElement(){
        Actions actions = new Actions(BaseTest.getDriver());
        actions.moveByOffset(100, 100);
        actions.build().perform();
    }
}
