package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class TextArea extends TypifiedElement implements Locatable
{
    public TextArea(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    public Dimension setSizeByOffset(int x, int y)
    {

        //Need to get margin, instead of hardcoding -2 pixels?
        Actions actions = new Actions(BaseTest.getDriver());
        actions.moveToElement(this.getWrappedElement(),
                this.getWrappedElement().getSize().getWidth() - 2,
                this.getWrappedElement().getSize().getHeight() - 2)
                .clickAndHold()
                .moveByOffset(x, y)
                .release()
                .perform();
        return this.getWrappedElement().getSize();
    }

    @Override
    public Coordinates getCoordinates()
    {
        return null;
    }
}
