package typifiedelement;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;

public class LookAheadSelect extends TypifiedElement
{

    private final String OPTIONS_CSS = "li[data-ng-repeat='match in matches']";

    public LookAheadSelect(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getSelect()
    {
        return this.getWrappedElement();
    }

    /**
     * Returns all options belonging to this select tag.
     *
     * @return A list of {@code WebElements} representing options.
     */
    public List<WebElement> getOptions()
    {
        return getSelect().findElements(By.cssSelector(OPTIONS_CSS));
    }

    /**
     * Select the option at the given index. This is done by examining the "index" attribute of an
     * element, and not merely by counting.
     *
     * @param index The option at this index will be selected
     */
    public void selectByIndex(int index)
    {
        if (index < this.getOptions().size())
        {
            getOptions().get(index).click();
        }
        else
            throw new NoSuchElementException("index out of bounds, no options were selected");
    }

    /**
     * Select all options that display text matching the argument. That is, when given "Bar" this
     * would select an option like:
     * <p>
     * &lt;option value="foo"&gt;Bar&lt;/option&gt;
     *
     * @param text The visible text to match against
     */
    public void selectByVisibleText(String text)
    {
        WebElement e = getOptions().stream()
                .filter(o -> o.getText().equals(text))
                .findFirst()
                .orElseThrow(java.util.NoSuchElementException::new);
        e.click();
    }

    /**
     * Returns the option that is currently moused over
     *
     * @return WebElement
     */
    public WebElement getCurrentActiveOption()
    {
        WebElement e = getOptions().stream()
                .filter(o -> o.findElement(By.cssSelector(OPTIONS_CSS))
                        .getAttribute("class")
                        .contains("active"))
                .findFirst()
                .orElseThrow(java.util.NoSuchElementException::new);
        return e;
    }

     /**
     * Select the option based on the given text. If the given text is not present(since it is displayed based on the entries in DB), the element in first index will be selected
     *
     * @param text The option with this text will be selected
     */
    public void selectText(String text)
    {
        WebElement e = getOptions().stream()
                    .filter(o -> o.getText().equals(text))
                    .findFirst()
                .orElse(getOptions().get(0));
        e.click();
    }
}
