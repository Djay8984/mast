package model.cases;

import com.baesystems.ai.lr.dto.cases.CasePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class CasePage extends RestHelper<CasePageResourceDto>
{

    private int page;
    private int size;

    public CasePage(int page, int size)
    {
        this.endpoint = Endpoint.CASE_PAGE.getUrl(new String[]{
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CASE_CASE " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.CASE_PAGE;
    }
}
