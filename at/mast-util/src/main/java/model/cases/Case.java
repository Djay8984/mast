package model.cases;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Case extends RestHelper<CaseWithAssetDetailsDto>
{

    public Case(int id)
    {
        this.endpoint = Endpoint.CASE.getUrl(Integer.toString(id));
        get();
    }

    public Case(CaseDto caseDto)
    {
        this.endpoint = Endpoint.CASE.getUrl("");
        post(caseDto);
    }

    public Case(CaseDto caseDto, int id)
    {
        this.endpoint = Endpoint.CASE.getUrl(Integer.toString(id));
        put(caseDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CASE_CASE WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.CASE;
    }

}
