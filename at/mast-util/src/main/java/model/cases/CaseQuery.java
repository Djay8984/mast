package model.cases;

import com.baesystems.ai.lr.dto.cases.CasePageResourceDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class CaseQuery extends RestHelper<CasePageResourceDto>
{

    private CaseQueryDto queryDto;

    public CaseQuery(CaseQueryDto caseQueryDto)
    {
        this.endpoint = Endpoint.CASE_QUERY_PAGE.getUrl();
        this.queryDto = caseQueryDto;
        post(caseQueryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        if (queryDto.getEmployeeId() == null)
        {
            return DatabaseHelper.executeQuery("SELECT DISTINCT * FROM MAST_CASE_CASE " +
                    "WHERE " + EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.CASE_QUERY));
        }
        else
        {
            return DatabaseHelper.executeQuery("SELECT DISTINCT c.* FROM MAST_CASE_CASE c " +
                    "JOIN MAST_CASE_CASERESOURCE r ON c.id = r.case_id " +
                    "WHERE " + EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.CASE_QUERY)
                    + " ORDER BY id ASC;");
        }
    }

    protected TableMap getTableMap()
    {
        return TableMap.CASE_QUERY_RESPONSE;
    }
}
