package model.report;

import com.baesystems.ai.lr.dto.reports.ReportDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Report extends RestHelper<ReportDto>
{

    public Report(int jobId)
    {
        this.endpoint = Endpoint.REPORT.getUrl(new String[]{
                Integer.toString(jobId),
                "",
                });
        getArray();
    }

    public Report(int jobId, int reportId)
    {
        this.endpoint = Endpoint.REPORT.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(reportId),
                });
        get();
    }

    public Report(ReportDto report, int jobId)
    {
        this.endpoint = Endpoint.REPORT.getUrl(new String[]{
                Integer.toString(jobId),
                ""
        });
        post(report);
    }

    public Report(ReportDto report, int jobId, int reportId)
    {
        this.endpoint = Endpoint.REPORT.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(reportId)
        });
        put(report);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_REPORT WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.REPORT;
    }
}
