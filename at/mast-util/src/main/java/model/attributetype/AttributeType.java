package model.attributetype;

import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

@Deprecated
public class AttributeType extends RestHelper<AttributeTypeDto>
{

    private int attributeTypeId;

    public AttributeType(int attributeTypeId)
    {
        this.endpoint = Endpoint.ATTRIBUTE_TYPE.getUrl(new String[]{
                Integer.toString(attributeTypeId)
        });
        this.attributeTypeId = attributeTypeId;
        get();
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(String.format("select * from mast_ref_assetattributetype where id = %s", attributeTypeId)
        );
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.ATTRIBUTE_TYPE;
    }
}
