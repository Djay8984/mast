package model.service;

import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import constant.TableMap;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.SERVICE_CATALOGUE;

public class ServiceCatalogue extends RestHelper<ServiceCatalogueDto>
{

    public ServiceCatalogue(int id)
    {
        this.endpoint = SERVICE_CATALOGUE.getUrl(Integer.toString(id));
        get();
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return null;
    }

    @Override
    protected TableMap getTableMap()
    {
        return null;
    }
}
