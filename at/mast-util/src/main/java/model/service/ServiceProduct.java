package model.service;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import constant.Endpoint;
import constant.Request;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;
import org.testng.Assert;

import java.sql.ResultSet;

public class ServiceProduct extends RestHelper<ScheduledServiceListDto>
{

    private int assetId;

    public ServiceProduct(int assetId, int serviceId, Request request)
    {
        this.endpoint = Endpoint.ASSET_SERVICE.getUrl(
                new String[]{
                        Integer.toString(assetId),
                        Integer.toString(serviceId)
                }
        );
        this.assetId = assetId;
        switch (request)
        {
            case GET:
                get();
                break;
            case DELETE:
                delete();
                break;
            default:
                Assert.fail("You can only use GET/DELETE methods in this constructor.");
        }
    }

    public ServiceProduct(int assetId)
    {
        this.endpoint = Endpoint.ASSET_SERVICE.getUrl(
                new String[]{
                        Integer.toString(assetId),
                        ""
                }
        );
        this.assetId = assetId;
        get();
    }

    public ServiceProduct(int assetId, ScheduledServiceListDto serviceDto)
    {
        this.endpoint = Endpoint.ASSET_SERVICE.getUrl(
                new String[]{
                        Integer.toString(assetId),
                        ""
                }
        );
        this.assetId = assetId;
        post(serviceDto);
    }

    public ServiceProduct(int assetId, int serviceId, ScheduledServiceListDto serviceDto, Request request)
    {
        this.endpoint = Endpoint.ASSET_SERVICE.getUrl(
                new String[]{
                        Integer.toString(assetId),
                        Integer.toString(serviceId)
                }
        );
        this.assetId = assetId;
        switch (request)
        {
            case PUT:
                put(serviceDto);
                break;
            case POST:
                post(serviceDto);
                break;
            default:
                Assert.fail("Only PUT/POST allowed in this constructor!");
        }
    }

    public ServiceProduct(AssetDto assetDto, int id)
    {
        this.endpoint = Endpoint.ASSET.getUrl(Integer.toString(id));
        this.assetId = assetId;
        put(assetDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM mast_db.MAST_ASSET_SCHEDULEDSERVICE WHERE id = '" + assetId + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.SERVICE;
    }
}
