package model.service;

import com.baesystems.ai.lr.dto.services.SurveyDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class ServiceJob extends RestHelper<SurveyDto>
{

    public ServiceJob(int jobId)
    {
        this.endpoint = Endpoint.SERVICE_JOB.getUrl(Integer.toString(jobId));
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT * FROM MAST_JOB_JOBSERVICEINSTANCE WHERE id = " + dto.getId()
        );
    }

    protected TableMap getTableMap()
    {
        return TableMap.SURVEY;
    }
}
