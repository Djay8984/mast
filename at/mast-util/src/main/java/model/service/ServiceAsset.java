package model.service;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class ServiceAsset extends RestHelper<ScheduledServiceDto>
{

    public ServiceAsset(int assetId)
    {
        this.endpoint = Endpoint.SERVICE_ASSET.getUrl(Integer.toString(assetId));
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM mast_db.MAST_ASSET_SCHEDULEDSERVICE WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.SERVICE;
    }
}
