package model.harmonisationdate;

import com.baesystems.ai.lr.dto.DateDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;
import java.util.Date;

public class HarmonisationDate extends RestHelper<DateDto>
{

    private final int assetId;

    public HarmonisationDate(int assetId)
    {
        this.endpoint = Endpoint.HARMONISATION_DATE.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        get();
    }

    public HarmonisationDate(int assetId, Date date)
    {
        this.endpoint = Endpoint.HARMONISATION_DATE.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        DateDto dateDto = new DateDto();
        dateDto.setDate(date);
        putExpectArray(dateDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT harmonisation_date FROM mast_db.MAST_ASSET_ASSET where id = " + assetId + ";"
        );
    }

    protected TableMap getTableMap()
    {
        return TableMap.HARMONISATION_DATE;
    }

}
