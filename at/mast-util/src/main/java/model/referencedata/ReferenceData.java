package model.referencedata;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import constant.Endpoint;
import constant.ReferenceTable;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class ReferenceData extends RestHelper<ReferenceDataDto>
{

    private final ReferenceTable referenceTable;
    private TableMap tableMap;

    public ReferenceData(Endpoint endpoint, ReferenceTable referenceTable, TableMap tableMap)
    {
        setEndpoint(endpoint.getUrl(""));
        this.referenceTable = referenceTable;
        this.tableMap = tableMap;
        getArray();
    }

    public ReferenceData(Endpoint endpoint, ReferenceTable referenceTable, int id)
    {
        setEndpoint(endpoint.getUrl(Integer.toString(id)));
        this.referenceTable = referenceTable;
        get();
    }

    public ReferenceData(Endpoint endpoint, ReferenceTable referenceTable, TableMap tableMap, int id)
    {
        setEndpoint(endpoint.getUrl(Integer.toString(id)));
        this.referenceTable = referenceTable;
        this.tableMap = tableMap;
        get();
    }

    public ReferenceData(Endpoint endpoint, ReferenceTable referenceTable)
    {
        setEndpoint(endpoint.getUrl(""));
        this.referenceTable = referenceTable;
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT * FROM " + referenceTable.getTableName() + " WHERE id = " + getDto().getId() + ";"
        );
    }

    protected TableMap getTableMap()
    {
        if (this.tableMap != null)
            return this.tableMap;
        else
            return TableMap.REFERENCE_DATA;
    }

}
