package model.referencedata;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataMapDto;
import com.google.gson.Gson;
import constant.Endpoint;
import constant.TableMap;
import helper.RestHelper;

import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ReferenceDataMap extends RestHelper<ReferenceDataMapDto>
{

    private final RefData refData;

    public ReferenceDataMap(Subset subset, RefData refData)
    {
        this.endpoint = Endpoint.REFERENCE_DATA_MAP.getUrl(new String[]{
                subset.toString().toLowerCase(),
                refData.toString().toLowerCase().replaceAll("_", "-")
        });
        this.refData = refData;
        get();
    }

    public ArrayList<ReferenceDataDto> getReferenceDataArrayList()
    {
        ArrayList<ReferenceDataDto> referenceDataDtos = new ArrayList<>();
        Gson gson = new Gson();
        for (LinkedHashMap<String, Object> linkedHashMap : getReferenceData())
        {
            String hashMapJson = gson.toJson(linkedHashMap);
            referenceDataDtos.add(gson.fromJson(hashMapJson, ReferenceDataDto.class));
        }
        return referenceDataDtos;
    }

    public <T> ArrayList<T> getReferenceDataAsClass(Type type)
    {
        ArrayList<T> referenceDataDtos = new ArrayList<>();
        Gson gson = new Gson();
        for (LinkedHashMap<String, Object> linkedHashMap : getReferenceData())
        {
            String hashMapJson = gson.toJson(linkedHashMap);
            referenceDataDtos.add(gson.fromJson(hashMapJson, type));
        }
        return referenceDataDtos;
    }

    @SuppressWarnings(value = "unchecked")
    private ArrayList<LinkedHashMap<String, Object>> getReferenceData()
    {
        return (ArrayList<LinkedHashMap<String, Object>>) getDto().getReferenceData().get(this.refData.getHashmapKey());
    }

    protected ResultSet getResultSet(String method)
    {
        return null;
    }

    protected TableMap getTableMap()
    {
        return null;
    }

    public enum Subset
    {
        ASSET, ATTACHMENT, CASE, DEFECT, FLAG, JOB, PARTY, REPAIR, SERVICE
    }

    public enum RefData
    {
        ASSET_TYPES("assetTypes"),
        ITEM_TYPES("itemTypes"),
        LIFECYCLE_STATUSES("lifecycleStatuses"),
        CLASS_STATUSES("classStatuses"),
        CASE_STATUSES("caseStatuses"),
        CASE_TYPES("caseTypes"),
        CODICIL_TEMPLATES("codicilTemplates"),
        FLAGS("flags"),
        JOB_STATUSES("jobStatuses"),
        OFFICE_ROLES("officeRoles"),
        PARTY_ROLES("partyRoles"),
        PARTY_FUNCTIONS("partyFunctions"),
        ATTRIBUTE_TYPES("attributeTypes");

        private final String hashmapKey;

        RefData(String hashmapKey)
        {
            this.hashmapKey = hashmapKey;
        }

        public String getHashmapKey()
        {
            return this.hashmapKey;
        }
    }
}
