package model.referencedata;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import constant.Endpoint;
import constant.ReferenceTable;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class ReferenceDataArray extends RestHelper<ReferenceDataDto>
{

    private final ReferenceTable referenceTable;

    public ReferenceDataArray(Endpoint endpoint, ReferenceTable referenceTable)
    {
        setEndpoint(endpoint.getUrl());
        this.referenceTable = referenceTable;
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT * FROM " + referenceTable.getTableName() + " WHERE id = " + getDto().getId() + ";"
        );
    }

    protected TableMap getTableMap()
    {
        return TableMap.REFERENCE_DATA;
    }

}
