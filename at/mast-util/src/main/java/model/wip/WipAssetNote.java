package model.wip;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class WipAssetNote extends RestHelper<AssetNoteDto>
{

    public WipAssetNote(int assetId, int assetNoteId)
    {
        this.endpoint = Endpoint.WIP_ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetNoteId)
        });
        get();
    }

    public WipAssetNote(AssetNoteDto assetNote, int assetId)
    {
        this.endpoint = Endpoint.WIP_ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                "",
                });
        post(assetNote);
    }

    public WipAssetNote(AssetNoteDto assetNote, int assetId, int assetNoteId)
    {
        this.endpoint = Endpoint.WIP_ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetNoteId)
        });
        put(assetNote);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE id = '" + this.dto.getId()
                + "' AND codicil_type_id = 3;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_NOTE;
    }
}
