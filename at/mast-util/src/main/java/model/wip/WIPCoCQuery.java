package model.wip;

import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.RestHelper;

import java.sql.ResultSet;

public class WIPCoCQuery extends RestHelper<CoCPageResourceDto>
{
    public WIPCoCQuery(final String jobId, final CodicilDefectQueryDto queryDto)
    {
        this.endpoint = Endpoint.WIP_COC_QUERY.getUrl(jobId);
        post(queryDto);
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.WIP_COC;
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        // Not required
        return null;
    }
}
