package model.wip;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class WipCoc extends RestHelper<CoCDto>
{

    public WipCoc(int jobId, int wipCocId)
    {
        this.endpoint = Endpoint.WIP_COC.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(wipCocId)
        });
        get();
    }

    public WipCoc(CoCDto wipCoc, int jobId)
    {
        this.endpoint = Endpoint.WIP_COC.getUrl(new String[]{
                Integer.toString(jobId),
                ""
        });
        post(wipCoc);
    }

    public WipCoc(CoCDto wipCoc, int jobId, int wipCocId)
    {
        this.endpoint = Endpoint.WIP_COC.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(wipCocId)
        });
        put(wipCoc);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE id = '" + this.dto.getId()
                + "' AND codicil_type_id = 1;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.WIP_COC;
    }
}
