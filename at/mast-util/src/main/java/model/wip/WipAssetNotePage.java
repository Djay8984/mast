package model.wip;

import com.baesystems.ai.lr.dto.codicils.AssetNotePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class WipAssetNotePage extends RestHelper<AssetNotePageResourceDto>
{

    private int page;
    private int size;
    private int jobId;

    public WipAssetNotePage(int jobId, int page, int size)
    {
        this.endpoint = Endpoint.WIP_ASSET_NOTE_PAGE.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        this.jobId = jobId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE job_id =  " + jobId
                + " AND codicil_type_id = 3 "
                + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_NOTE;
    }
}
