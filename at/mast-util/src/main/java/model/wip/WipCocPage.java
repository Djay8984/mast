package model.wip;

import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class WipCocPage extends RestHelper<CoCPageResourceDto>
{

    private int jobId;
    private int defectId;

    public WipCocPage(int jobId, int defectId)
    {
        this.endpoint = Endpoint.WIP_COC_DEFECT.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(defectId)
        });
        this.jobId = jobId;
        this.defectId = defectId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT JWC.* FROM MAST_JOB_CODICILWIP JWC " +
                "WHERE JWC.JOB_ID = " + jobId + " AND JWC.DEFECT_ID = " + defectId + " AND codicil_type_id = 1;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.WIP_COC;
    }
}
