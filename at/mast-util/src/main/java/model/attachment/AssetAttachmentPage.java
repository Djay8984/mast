package model.attachment;

import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetAttachmentPage extends RestHelper<SupplementaryInformationPageResourceDto>
{

    private int page;
    private int size;
    private int assetId;

    public AssetAttachmentPage(int assetId, int page, int size)
    {
        this.endpoint = Endpoint.ASSET_ATTACHMENT_PAGE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        this.assetId = assetId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT DISTINCT * FROM MAST_XXX_SUPPLEMENTARYINFORMATION si left join" +
                " MAST_XXX_SUPPLEMENTARYINFORMATIONLINK sil on si.id = sil.id where sil.asset_id =  " +
                assetId + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ATTACHMENT;
    }
}
