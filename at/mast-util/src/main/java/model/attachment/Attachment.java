package model.attachment;

import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import constant.Endpoint;
import constant.Request;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Attachment extends RestHelper<SupplementaryInformationDto>
{

    private String id;
    private String type;

    public Attachment(Endpoint endpoint, String[] values, Request request)
    {
        this.endpoint = endpoint.getUrl(values);
        this.id = values[0];
        String endpointString = this.endpoint;
        endpointString = endpointString.substring(endpointString.indexOf('/') + 1);
        this.type = endpointString.substring(0, endpointString.indexOf('/'));
        if (request.equals(Request.DELETE))
        {
            delete();
        }
        else
        {
            get();
        }
    }

    public Attachment(Endpoint endpoint, SupplementaryInformationDto attachmentDto, String[] values)
    {
        this.endpoint = endpoint.getUrl(values);
        this.id = values[0];
        String endpointString = this.endpoint;
        endpointString = endpointString.substring(endpointString.indexOf('/') + 1);
        this.type = endpointString.substring(0, endpointString.indexOf('/'));
        post(attachmentDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_XXX_SUPPLEMENTARYINFORMATION si right join" +
                " MAST_XXX_SUPPLEMENTARYINFORMATIONLINK sil on si.id = sil.id where sil." + type + "_id =  " +
                id + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ATTACHMENT;
    }

}
