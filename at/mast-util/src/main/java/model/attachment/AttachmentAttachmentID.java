package model.attachment;

import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AttachmentAttachmentID extends RestHelper<SupplementaryInformationDto>
{

    private String id;
    private String type;
    private String attachmentId;

    public AttachmentAttachmentID(Endpoint endpoint, SupplementaryInformationDto attachmentDto, String[] values)
    {
        this.endpoint = endpoint.getUrl(values);
        this.id = values[0];
        this.attachmentId = values[1];
        String endpointString = this.endpoint;
        endpointString = endpointString.substring(endpointString.indexOf('/') + 1);
        this.type = endpointString.substring(0, endpointString.indexOf('/'));
        put(attachmentDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * " +
                "FROM mast_xxx_supplementaryinformation si " +
                "RIGHT JOIN mast_xxx_supplementaryinformationlink sil ON si.id = sil.id " +
                "WHERE sil." + type + "_id" + "= " + id + " AND si.id = " + attachmentId + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ATTACHMENT;
    }

}
