package model.imo;

import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class ImoNumber extends RestHelper<IhsAssetDto>
{

    private int imoNumber;

    public ImoNumber(int imoNumber)
    {
        this.endpoint = Endpoint.IHS_NUMBER.getUrl(new String[]{
                Integer.toString(imoNumber),
                });
        this.imoNumber = imoNumber;
        get();
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("select * from ihs_db.absd_ship_search where LRNO = '" + imoNumber + "';");
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.IHS_NUMBER;
    }
}
