package model.surveyor;

import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.employees.SurveyorPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.truth.Truth.assertThat;

public class SurveyorOfficePage extends RestHelper<SurveyorPageResourceDto>
{

    private int page;
    private int size;
    private int officeId;

    public SurveyorOfficePage(int officeId, int page, int size)
    {
        this.endpoint = Endpoint.SURVEYOR_PAGE_WITH_OFFICE.getUrl(
                new String[]{
                        Integer.toString(page),
                        Integer.toString(size),
                        Integer.toString(officeId)
                }
        );
        this.page = page;
        this.size = size;
        this.officeId = officeId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT * FROM mast_lrp_employee_office AS eo" +
                        " INNER JOIN mast_lrp_employee_role AS ear ON eo.lr_employee_id = ear.lr_employee_id" +
                        " INNER JOIN mast_lrp_employee AS e ON e.id = ear.lr_employee_id" +
                        " WHERE lr_office_id = '" + officeId + "' AND ear.employee_role_id = 2 " +
                        " GROUP BY ear.lr_employee_id " + sqlLimitForPage(page, size) + ";"
        );
    }

    protected TableMap getTableMap()
    {
        return null;
    }

    @Override
    protected void verifyPage()
    {
        ResultSet resultSet = getResultSet(null);
        try
        {
            int i = 0;
            while (resultSet.next())
            {
                SurveyorDto surveyorDto = this.dto.getContent().get(i);
                String fullName = resultSet.getString("first_name") + " " + resultSet.getString("last_name");
                assertThat(surveyorDto.getName()).isEqualTo(fullName);
                i++;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
