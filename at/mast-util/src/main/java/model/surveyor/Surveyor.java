package model.surveyor;

import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import constant.Endpoint;
import constant.TableMap;
import helper.RestHelper;

import java.sql.ResultSet;

public class Surveyor extends RestHelper<SurveyorDto>
{

    public Surveyor(int id)
    {
        this.endpoint = Endpoint.SURVEYOR.getUrl(
                Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return null;
    }

    protected TableMap getTableMap()
    {
        return null;
    }
}

