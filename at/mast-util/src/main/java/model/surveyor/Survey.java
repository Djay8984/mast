package model.surveyor;

import com.baesystems.ai.lr.dto.services.SurveyDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Survey extends RestHelper<SurveyDto>
{

    public Survey(SurveyDto surveyDto, int jobId, int surveyId)
    {
        this.endpoint = Endpoint.SURVEY.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(surveyId)
        });
        put(surveyDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_JobServiceInstance WHERE id = '"
                + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.SURVEY_FULL;
    }
}
