package model.surveyor;

import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.employees.SurveyorPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.truth.Truth.assertThat;

public class SurveyorSearchPage extends RestHelper<SurveyorPageResourceDto>
{

    private int page;
    private int size;
    private String searchTerm;

    public SurveyorSearchPage(String searchTerm, int page, int size)
    {
        this.endpoint = Endpoint.SURVEYOR_PAGE_WITH_SEARCH.getUrl(
                new String[]{
                        Integer.toString(page),
                        Integer.toString(size),
                        searchTerm
                }
        );
        this.page = page;
        this.size = size;
        this.searchTerm = searchTerm;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT DISTINCT e.id, first_name, last_name FROM mast_lrp_employee AS e " +
                        "INNER JOIN mast_lrp_employee_role AS er ON e.id = er.lr_employee_id " +
                        "WHERE er.employee_role_id = 2 AND " +
                        "first_name LIKE '" + searchTerm + "' OR last_name LIKE '" + searchTerm + "' " +
                        sqlLimitForPage(page, size) + ";"
        );
    }

    protected TableMap getTableMap()
    {
        return null;
    }

    @Override
    protected void verifyPage()
    {
        ResultSet resultSet = getResultSet(null);
        try
        {
            int i = 0;
            while (resultSet.next())
            {
                SurveyorDto surveyorDto = this.dto.getContent().get(i);
                String fullName = resultSet.getString("first_name") + " " + resultSet.getString("last_name");
                assertThat(surveyorDto.getName()).isEqualTo(fullName);
                i++;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
