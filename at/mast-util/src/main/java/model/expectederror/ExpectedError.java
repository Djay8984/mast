package model.expectederror;

import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import constant.Endpoint;
import constant.Request;
import constant.TableMap;
import helper.RestHelper;
import org.testng.Assert;

import java.sql.ResultSet;

import static com.google.common.truth.Truth.assertThat;

public class ExpectedError extends RestHelper<ErrorMessageDto>
{

    private String errorMessage = null;

    public ExpectedError(Endpoint endpoint, String value, int statusCode)
    {
        this.endpoint = endpoint.getUrl(value);
        this.statusCode = statusCode;
        get();
    }

    public ExpectedError(Endpoint endpoint, String[] values, int statusCode)
    {
        this.endpoint = endpoint.getUrl(values);
        this.statusCode = statusCode;
        get();
    }

    public ExpectedError(Endpoint endpoint, String value, int statusCode, String errorMessage, Request request)
    {
        this.endpoint = endpoint.getUrl(value);
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        switch (request)
        {
            case GET:
                get();
                break;
            case DELETE:
                delete();
                break;
            default:
                Assert.fail("Only GET/DELETE is accepted in this constructor!");
        }
    }

    public ExpectedError(Endpoint endpoint, String[] values, int statusCode, String errorMessage, Request request)
    {
        this.endpoint = endpoint.getUrl(values);
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        switch (request)
        {
            case GET:
                get();
                break;
            case DELETE:
                delete();
                break;
            default:
                Assert.fail("Only GET/DELETE is accepted in this constructor!");
        }
    }

    public ExpectedError(Endpoint endpoint, Object dto, String value, Request request, int statusCode, String errorMessage)
    {
        this.endpoint = endpoint.getUrl(value);
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        if (request.equals(Request.PUT))
        {
            put(dto);
        }
        else if (request.equals(Request.POST))
        {
            post(dto);
        }
        else
        {
            Assert.fail("Only PUT/POST is accepted in this constructor!");
        }
    }

    public ExpectedError(Endpoint endpoint, Object dto, String[] values, Request request, int statusCode, String errorMessage)
    {
        this.endpoint = endpoint.getUrl(values);
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        if (request.equals(Request.PUT))
        {
            put(dto);
        }
        else if (request.equals(Request.POST))
        {
            post(dto);
        }
        else
        {
            Assert.fail("Only PUT/POST is accepted in this constructor!");
        }
    }

    protected ResultSet getResultSet(String method)
    {
        return null;
    }

    protected TableMap getTableMap()
    {
        return null;
    }

    @Override
    public void verify()
    {
        if (this.errorMessage == null)
            return;
        assertThat(getDto().getMessage()).isEqualTo(this.errorMessage);
    }

}
