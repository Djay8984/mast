/*
package util.model.office;

import com.baesystems.ai.lr.dto.employees.OfficeDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class OfficePage extends RestHelper<OfficeDto> {

    private int page;
    private int size;
    private Endpoint endpointEnum;
    private String officeRole;

    public OfficePage(int page, int size) {
        this.endpointEnum = Endpoint.OFFICE_PAGE;
        this.endpoint = Endpoint.OFFICE_PAGE.getUrl(new String[] {
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        get();
    }

    public OfficePage(String officeRole) {
        this.endpointEnum = Endpoint.OFFICE_WITH_ROLE;
        this.officeRole = officeRole;
        this.endpoint = this.endpointEnum.getUrl(this.officeRole);
        get();
    }

    public OfficePage(String parameter, String value) {
        this.endpointEnum = Endpoint.OFFICE;
        this.endpoint = endpointEnum.getUrl("?" + parameter + "=" + value + "*");
        get();
    }

    protected ResultSet getResultSet(String method) {
        if (endpointEnum.equals(Endpoint.OFFICE_WITH_ROLE)) {
            return DatabaseHelper
                    .executeQuery("SELECT * FROM MAST_LRP_OFFICE as o " +
                            "INNER JOIN MAST_REF_OFFICEROLE as ofr ON o.office_role_id = ofr.id " +
                            "WHERE ofr.name = '" + this.officeRole + "';");
        }
        else {
            return DatabaseHelper.executeQuery("SELECT * FROM MAST_LRP_OFFICE " + sqlLimitForPage(page, size) + ";");
        }
    }

    protected TableMap getTableMap() {
        if (endpointEnum.equals(Endpoint.OFFICE)) return null;
        return TableMap.NAME;
    }
}
*/
