package model.office;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Office extends RestHelper<OfficeDto>
{

    public Office(int id)
    {
        this.endpoint = Endpoint.OFFICE.getUrl(Integer.toString(id));
        get();
    }

    public Office(AssetDto assetDto)
    {
        this.endpoint = Endpoint.OFFICE.getUrl("");
        post(assetDto);
    }

    public Office(AssetDto assetDto, int id)
    {
        this.endpoint = Endpoint.OFFICE.getUrl(Integer.toString(id));
        put(assetDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_REF_OFFICE WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.NAME;
    }
}
