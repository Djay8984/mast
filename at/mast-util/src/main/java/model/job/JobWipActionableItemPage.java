package model.job;

import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.JOB_WIP_ACTIONABLE_ITEM;

public class JobWipActionableItemPage extends RestHelper<ActionableItemPageResourceDto>
{

    private int jobId;

    public JobWipActionableItemPage(int jobId)
    {
        this.endpoint = JOB_WIP_ACTIONABLE_ITEM.getUrl(new String[]{
                Integer.toString(jobId),
                ""
        });
        this.jobId = jobId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_job_codicilwip " +
                "WHERE codicil_type_id = 2 AND job_id = " + jobId);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ACTIONABLE_ITEM;
    }
}
