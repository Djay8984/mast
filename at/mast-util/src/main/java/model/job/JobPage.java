package model.job;

import com.baesystems.ai.lr.dto.jobs.JobPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class JobPage extends RestHelper<JobPageResourceDto>
{

    private int assetId;
    private int page;
    private int size;

    public JobPage(Endpoint endpoint, int id)
    {
        this.endpoint = endpoint.getUrl(Integer.toString(id));
        this.assetId = id;
        get();
    }

    public JobPage(Endpoint endpoint, int page, int size)
    {
        this.endpoint = endpoint.getUrl(new String[]{Integer.toString(page), Integer.toString(size)});
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        if (assetId != 0)
            return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_job_job WHERE asset_id = " + assetId + ";");
        else
        {
            return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_job_job " + sqlLimitForPage(page, size) + ";");
        }
    }

    protected TableMap getTableMap()
    {
        return TableMap.JOB;
    }

}
