package model.job;

import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class JobWipCertificate extends RestHelper<CertificateDto>
{

    public JobWipCertificate(int jobId, int jobWipCertId)
    {
        this.endpoint = Endpoint.JOB_WIP_CERT.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(jobWipCertId)
        });
        get();
    }

    public JobWipCertificate(CertificateDto certificateDto, int jobId)
    {
        this.endpoint = Endpoint.JOB_WIP_CERT.getUrl(new String[]{
                Integer.toString(jobId),
                ""
        });
        post(certificateDto);
    }

    public JobWipCertificate(CertificateDto certificateDto, int jobId, int jobWipCertId)
    {
        this.endpoint = Endpoint.JOB_WIP_CERT.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(jobWipCertId)
        });
        put(certificateDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CERTIFICATEWIP WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.JOB_WIP_CERTIFICATE;
    }
}
