package model.job;

import com.baesystems.ai.lr.dto.jobs.JobPageResourceDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;
import model.referencedata.ReferenceDataMap;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static constant.LloydsRegister.ACTIVE_JOB_STATUSES;
import static model.referencedata.ReferenceDataMap.RefData.JOB_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.JOB;

public class JobQueryPage extends RestHelper<JobPageResourceDto>
{

    private JobQueryDto queryDto;

    public JobQueryPage(JobQueryDto queryDto)
    {
        this.endpoint = Endpoint.JOB_QUERY.getUrl();
        this.queryDto = queryDto;
        post(queryDto);
    }

    public JobQueryPage(List<Long> employeeId, List<Long> assetId)
    {

        JobQueryDto queryDto = new JobQueryDto();
        if (!employeeId.equals(Collections.EMPTY_LIST))
        {
            queryDto.setEmployeeId(employeeId);
        }
        if (!assetId.equals(Collections.EMPTY_LIST))
        {
            queryDto.setAssetId(assetId);
        }
        queryDto.setJobStatusId(getJobIds(ACTIVE_JOB_STATUSES()));

        this.endpoint = Endpoint.JOB_QUERY.getUrl();
        this.queryDto = queryDto;

        post(queryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT DISTINCT j.id, j.notes, j.request_by_telephone_flag, " +
                        "j.status_reason, j.work_order_number, " +
                        "j.written_service_request_received_date, j.written_service_response_sent_date " +
                        "FROM mast_db.mast_job_job AS j " +
                        "LEFT JOIN mast_db.mast_job_joboffice AS jo ON j.id = jo.job_id " +
                        "LEFT JOIN mast_db.mast_job_jobresource AS jr ON j.id = jr.job_id " +
                        "WHERE " + EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.JOB_QUERY) + " ORDER BY j.id ASC;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.JOB;
    }

    public List<Long> getJobIds(List<String> activeJobStatuses)
    {
        return activeJobStatuses.stream()
                .map(x -> getJobIdByStatus(x))
                .collect(Collectors.toList());
    }

    public Long getJobIdByStatus(String status)
    {
        ArrayList<ReferenceDataDto> jobStatuses =
                new ReferenceDataMap(JOB, JOB_STATUSES)
                        .getReferenceDataArrayList();
        return jobStatuses.stream()
                .filter(dto -> dto.getName().equals(status))
                .findFirst()
                .map(ReferenceDataDto::getId)
                .orElse(null);
    }

}


