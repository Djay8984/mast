package model.job;

import com.baesystems.ai.lr.dto.jobs.CertificatePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class JobWipCertificatePage extends RestHelper<CertificatePageResourceDto>
{

    private int page;
    private int size;
    private int jobId;

    public JobWipCertificatePage(int jobId, int page, int size)
    {
        this.endpoint = Endpoint.JOB_WIP_CERT_PAGE.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        this.jobId = jobId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CERTIFICATEWIP WHERE job_id =  " + jobId + " "
                + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.JOB_WIP_CERTIFICATE;
    }
}
