package model.job;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.JOB_WIP_ACTIONABLE_ITEM;

public class JobWipActionableItem extends RestHelper<ActionableItemDto>
{

    public JobWipActionableItem(int jobId, int actionableItemId)
    {
        this.endpoint = JOB_WIP_ACTIONABLE_ITEM.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(actionableItemId)
        });
        get();
    }

    public JobWipActionableItem(int jobId, int actionableItemId, ActionableItemDto actionableItemDto)
    {
        this.endpoint = JOB_WIP_ACTIONABLE_ITEM.getUrl(new String[]{
                Integer.toString(jobId),
                Integer.toString(actionableItemId)
        });
        put(actionableItemDto);
    }

    public JobWipActionableItem(ActionableItemDto actionableItemDto, int jobId)
    {
        this.endpoint = JOB_WIP_ACTIONABLE_ITEM.getUrl(new String[]{
                Integer.toString(jobId),
                ""
        });
        post(actionableItemDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(String.format("SELECT * FROM mast_db.mast_job_codicilwip " +
                "WHERE codicil_type_id = 2 AND id = %s", this.dto.getId()));
    }

    protected TableMap getTableMap()
    {
        return TableMap.ACTIONABLE_ITEM;
    }
}
