package model.job;

import com.baesystems.ai.lr.dto.jobs.JobDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Job extends RestHelper<JobDto>
{

    public Job(int id)
    {
        this.endpoint = Endpoint.JOB.getUrl(Integer.toString(id));
        get();
    }

    public Job(JobDto jobDto)
    {
        this.endpoint = Endpoint.JOB.getUrl("");
        post(jobDto);
    }

    public Job(JobDto jobDto, int id)
    {
        this.endpoint = Endpoint.JOB.getUrl(Integer.toString(id));
        put(jobDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_JOB WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.JOB;
    }

}
