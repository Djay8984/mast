package model.ruleset;

import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class SocietyRuleset extends RestHelper<SocietyRulesetDto>
{

    public SocietyRuleset()
    {
        this.endpoint = Endpoint.RULE_SET.getUrl();
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_REF_RULESET2 WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.SOCIETY_RULESET;
    }
}
