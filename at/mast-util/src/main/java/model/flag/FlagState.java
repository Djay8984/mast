package model.flag;

import com.baesystems.ai.lr.dto.references.FlagStateDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class FlagState extends RestHelper<FlagStateDto>
{

    public FlagState(int id)
    {
        this.endpoint = Endpoint.FLAG_STATE.getUrl(Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_REF_FLAGADMINISTRATION WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.NAME;
    }
}
