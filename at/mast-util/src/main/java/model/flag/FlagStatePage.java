package model.flag;

import com.baesystems.ai.lr.dto.references.FlagStatePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class FlagStatePage extends RestHelper<FlagStatePageResourceDto>
{

    private int page;
    private int size;

    public FlagStatePage(int page, int size)
    {
        this.endpoint = Endpoint.FLAG_STATE_PAGE.getUrl(new String[]{
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_REF_FLAGADMINISTRATION " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.NAME;
    }
}
