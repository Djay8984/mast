package model.asset;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.ASSET_ACTIONABLE_ITEM;

public class AssetActionableItem extends RestHelper<ActionableItemDto>
{

    public AssetActionableItem(ActionableItemDto itemDto, int assetId)
    {
        this.endpoint = ASSET_ACTIONABLE_ITEM.getUrl(Integer.toString(assetId));
        post(itemDto);
    }

    public AssetActionableItem(ActionableItemDto itemDto, int assetId, int actionableItemId)
    {
        this.endpoint = Endpoint.ASSET_ACTIONABLE_ITEM.getUrl(new String[]{
                Integer.toString(assetId)
        });
        if (actionableItemId > 0)
        {
            this.endpoint = this.endpoint + "/" + actionableItemId;
        }
        put(itemDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(String.format("SELECT * FROM mast_db.mast_asset_codicil " +
                "WHERE codicil_type_id = 2 AND id = %s", this.dto.getId()));
    }

    protected TableMap getTableMap()
    {
        return TableMap.ACTIONABLE_ITEM;
    }
}
