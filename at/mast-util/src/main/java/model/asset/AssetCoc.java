package model.asset;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCoc extends RestHelper<CoCDto>
{

    public AssetCoc(int assetId, int assetCocId)
    {
        this.endpoint = Endpoint.ASSET_COC.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetCocId)
        });
        get();
    }

    public AssetCoc(CoCDto assetCoc, int assetId)
    {
        this.endpoint = Endpoint.ASSET_COC.getUrl(new String[]{
                Integer.toString(assetId),
                ""
        });
        post(assetCoc);
    }

    public AssetCoc(CoCDto assetCoc, int assetId, int assetCocId)
    {
        this.endpoint = Endpoint.ASSET_COC.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetCocId)
        });
        put(assetCoc);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_CODICIL WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_CODICIL;
    }
}
