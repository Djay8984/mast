package model.asset;

import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCoCQuery extends RestHelper<CoCPageResourceDto>
{

    private final int assetId;
    private CodicilDefectQueryDto queryDto;

    public AssetCoCQuery(int assetId, CodicilDefectQueryDto queryDto)
    {
        this.endpoint = Endpoint.ASSET_COC_QUERY.getUrl(Integer.toString(assetId));
        this.queryDto = queryDto;
        this.assetId = assetId;
        post(queryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        String sql = "SELECT * FROM mast_asset_codicil WHERE " +
                EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.CODICIL_QUERY) + " " +
                "AND asset_id = " + assetId + " AND codicil_type_id = 1 AND deleted = 0 ORDER BY id";
        return DatabaseHelper.executeQuery(sql);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_CODICIL;
    }
}
