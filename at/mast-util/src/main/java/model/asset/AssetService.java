package model.asset;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import constant.TableMap;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.ASSET_SERVICE;

public class AssetService extends RestHelper<ScheduledServiceDto>
{

    public AssetService(int assetId, int serviceId, ScheduledServiceDto scheduledServiceDto)
    {
        this.endpoint = ASSET_SERVICE.getUrl(new String[]{Integer.toString(assetId), Integer.toString(serviceId)});
        put(scheduledServiceDto);
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return null;
    }

    @Override
    protected TableMap getTableMap()
    {
        return null;
    }
}
