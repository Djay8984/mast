package model.asset;

import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;
import helper.TestDataHelper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.truth.Truth.assertThat;

public class AssetModel extends RestHelper<AssetModelDto>
{

    private final Integer assetId;

    public AssetModel(Integer id)
    {
        this.assetId = id;
        setEndpoint(Endpoint.ASSET_MODEL.getUrl(new String[] {id.toString(), TestDataHelper.getLatestAssetVersion(assetId).toString()}));
        get();
    }

    public AssetModel(Integer id, Integer assetVersion) {
        this.assetId = id;
        setEndpoint(Endpoint.ASSET_MODEL.getUrl(new String[] {id.toString(), assetVersion.toString()}));
    }


    private static void item(ItemDto item) throws SQLException
    {
        ResultSet itemRs = DatabaseHelper
                .executeQuery("SELECT * FROM MAST_ASSET_ASSETITEM WHERE id = " + item.getId() + ";");
        assertThat(DatabaseHelper.getRowCount(itemRs)).isEqualTo(1);
        itemRs.first();

        assertThat(item.getName()).isEqualTo(itemRs.getString("name"));

        ResultSet nestedAttributesRs = DatabaseHelper
                .executeQuery("SELECT * FROM mast_asset_assetitemattribute WHERE item_id = " + item.getId() + ";");
        assertThat(DatabaseHelper.getRowCount(nestedAttributesRs))
                .isEqualTo(item.getAttributes().size());

        for (ItemDto nestedItem : item.getItems())
        {
            item(nestedItem);
        }
        for (AttributeDto attribute : item.getAttributes())
        {
            ResultSet attributeRs = DatabaseHelper
                    .executeQuery("SELECT * FROM mast_asset_assetitemattribute WHERE id = " + attribute.getId() + ";");
            attributeRs.first();
            assertThat(attribute.getValue()).isEqualTo(attributeRs.getString("value"));
        }
    }

    public ResultSet getResultSet(String method)
    {
        return null;
    }

    public TableMap getTableMap()
    {
        return null;
    }

    @Override
    public void verify()
    {
        for (ItemDto item : dto.getItems())
        {
            try
            {
                item(item);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

}
