package model.asset;

import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetDefect extends RestHelper<DefectPageResourceDto>
{

    private int id;
    private int page;
    private int size;
    private String sort = "";
    private String order = "";
    private String sqlQuery;

    public AssetDefect(int id, int page, int size)
    {
        this.endpoint = Endpoint.ASSET_DEFECT.getUrl(new String[]{
                Integer.toString(id),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.id = id;
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method)
    {

        if (sort.equals("") && order.equals(""))
        {
            sqlQuery = "SELECT * FROM MAST_DEFECT_DEFECT WHERE ASSET_ID = " + id + " "
                    + sqlLimitForPage(page, size) + ";";
        }
        else
        {
            sqlQuery = "SELECT * FROM MAST_DEFECT_DEFECT WHERE ASSET_ID = " + id + " "
                    + " ORDER BY " + sort
                    + " " + order + " "
                    + sqlLimitForPage(page, size) + ";";
        }
        return DatabaseHelper.executeQuery(sqlQuery);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_DEFECT;
    }
}
