package model.asset;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemPageResourceDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.TableMap.ASSET_ITEM_QUERY;
import static helper.EndpointQueryHelper.buildSqlForQueryDto;

public class AssetItemQuery extends RestHelper<LazyItemPageResourceDto>
{

    private ItemQueryDto queryDto;
    private int assetId;

    public AssetItemQuery(ItemQueryDto queryDto, int assetId)
    {
        this.endpoint = Endpoint.ASSET_ITEM_QUERY.getUrl(Integer.toString(assetId));
        this.queryDto = queryDto;
        this.assetId = assetId;
        post(queryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        if (method == null)
        {
            if (queryDto.getAttributeId() != null)
            {
                return DatabaseHelper.executeQuery("SELECT ai.id FROM mast_db.mast_asset_assetitemattribute AS aa " +
                        "INNER JOIN mast_asset_assetitem AS ai ON aa.item_id = ai.id WHERE " +
                        buildSqlForQueryDto(queryDto, ASSET_ITEM_QUERY) + " AND asset_id = " + assetId + " GROUP BY item_id");
            }
            else
            {
                return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_asset_assetitem AS ai" +
                        " WHERE " + buildSqlForQueryDto(queryDto, ASSET_ITEM_QUERY) + " AND asset_id = " + assetId);
            }
        }
        LazyItemDto lazyItemDto = (LazyItemDto) pageArrayDto;
        switch (method)
        {
            case "getItems":
                return DatabaseHelper.executeQuery("SELECT id FROM mast_db.mast_asset_assetitem WHERE id IN " +
                        "(SELECT to_item_id FROM mast_asset_assetitemrelationship WHERE from_item_id = "
                        + lazyItemDto.getId() + ") AND asset_id = " + assetId + ";");
            case "getRelated":
                return DatabaseHelper.executeQuery(
                        "SELECT * FROM mast_db.mast_asset_assetitemrelationship " +
                                "WHERE from_item_id = " + lazyItemDto.getId() + ";"
                );
            case "getAttributes":
                return DatabaseHelper.executeQuery(
                        "SELECT * FROM mast_db.mast_asset_assetitemattribute WHERE item_id = " + lazyItemDto.getId() + ";"
                );
            default:
                return null;
        }
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEMS_QUERY;
    }
}
