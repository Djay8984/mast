package model.asset;

import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetItemRelationship extends RestHelper<ItemRelationshipDto>
{

    public AssetItemRelationship(ItemRelationshipDto itemRelationship, int assetId, int itemId)
    {
        this.endpoint = Endpoint.ASSET_ITEM_RELATIONSHIP.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemId),
                ""
        });
        post(itemRelationship);
    }

    public AssetItemRelationship(int assetId, int itemId, String relationshipId)
    {

        this.endpoint = Endpoint.ASSET_ITEM_RELATIONSHIP.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemId),
                relationshipId
        });

        delete();
    }

    protected ResultSet getResultSet(String method)
    {

        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSETITEMRELATIONSHIP WHERE ID = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEM_RELATIONSHIP;
    }

}
