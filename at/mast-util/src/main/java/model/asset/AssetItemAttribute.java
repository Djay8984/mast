package model.asset;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetItemAttribute extends RestHelper<AttributeDto>
{

    private int attributeTypeId;
    private String value;
    private int assetId;
    private int itemId;
    private int attributeId;

    public AssetItemAttribute(AttributeDto attributeDto, int assetId, int itemId, int attributeId)
    {
        this.endpoint = Endpoint.ASSET_ITEM_ATTRIBUTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemId),
                Integer.toString(attributeId)
        });
        this.assetId = assetId;
        this.itemId = itemId;
        this.attributeId = attributeId;
        this.attributeTypeId = attributeDto.getAttributeType().getId().intValue();
        this.value = attributeDto.getValue();
        put(attributeDto);
    }

    public AssetItemAttribute(AttributeDto attributeDto, int assetId)
    {
        this.itemId = attributeDto.getItem().getId().intValue();
        this.attributeTypeId = attributeDto.getAttributeType().getId().intValue();
        this.value = attributeDto.getValue();
        this.endpoint = Endpoint.ASSET_ITEM_ATTRIBUTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemId),
                ""
        });
        post(attributeDto);
    }

    public AssetItemAttribute(AttributeDto attributeDto, int assetId, int itemIdIn)
    {
        this.itemId = itemIdIn;
        this.attributeTypeId = attributeDto.getAttributeType().getId().intValue();
        this.value = attributeDto.getValue();
        this.endpoint = Endpoint.ASSET_ITEM_ATTRIBUTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemIdIn),
                Integer.toString(attributeDto.getId().intValue())
        });
        delete();
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("select * from mast_asset_assetitemattribute where item_id = '" + this.itemId
                + "' and attribute_type_id = '" + this.attributeTypeId + "' and value = '"
                + this.value + "';");
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEM_ATTRIBUTE;
    }
}
