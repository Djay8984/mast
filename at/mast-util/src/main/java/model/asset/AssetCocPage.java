package model.asset;

import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCocPage extends RestHelper<CoCPageResourceDto>
{

    private int page;
    private int size;
    private int assetId;

    public AssetCocPage(int assetId, int page, int size)
    {
        this.endpoint = Endpoint.ASSET_COC_PAGE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        this.assetId = assetId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_CODICIL WHERE asset_id =  " + assetId +
                " AND codicil_type_id = 1 "
                + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_CODICIL;
    }
}
