package model.asset;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetItem extends RestHelper<LazyItemDto>
{

    private int assetId;
    private int id;

    public AssetItem(int assetId, int id)
    {
        this.endpoint = Endpoint.ASSET_ITEM.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(id)
        });
        this.assetId = assetId;
        this.id = id;
        get();
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("select * from mast_asset_assetitem where asset_id = '" + assetId + "' and id = '"
                + id + "';");
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEM;
    }
}
