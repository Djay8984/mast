package model.asset;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCase extends RestHelper<CaseDto>
{

    public AssetCase(int assetId)
    {
        this.endpoint = Endpoint.ASSET_CASE.getUrl(Integer.toString(assetId));
        getArray();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CASE_CASE WHERE id =  " + this.dto.getId() + "; ");
    }

    protected TableMap getTableMap()
    {
        return TableMap.CASE_PAGE;
    }
}
