package model.asset;

import com.baesystems.ai.lr.dto.assets.AssetLightPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetPage extends RestHelper<AssetLightPageResourceDto>
{

    private int page = 0;
    private int size = 0;
    private boolean check = true;

    public AssetPage(int page, int size)
    {
        this.endpoint = Endpoint.ASSET_PAGE.getUrl(new String[]{
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        this.check = true;
        get();
    }

    public AssetPage(int page, int size, String order, String sort)
    {
        this.endpoint = Endpoint.ASSET_PAGE.getUrl(new String[]{
                Integer.toString(page),
                Integer.toString(size)
        }) + "&order=" + order + "&sort=" + sort;
        this.page = page;
        this.size = size;
        this.check = false;
        get();
    }

    // The enpoint takes 2 %s, Why you no work?!
    public AssetPage(String parameters)
    {
        this.endpoint = Endpoint.ASSET_PAGE.getUrl(parameters);
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        if (page == 0 && size == 0)
        {
            return null;
        }
        if (check)
            return TableMap.ASSET;
        else
            return null;

    }
}
