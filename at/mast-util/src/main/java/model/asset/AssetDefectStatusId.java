package model.asset;

import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetDefectStatusId extends RestHelper<DefectPageResourceDto>
{

    private int assetId;
    private int statusId;

    public AssetDefectStatusId(int assetId, int statusId)
    {
        this.endpoint = Endpoint.ASSET_DEFECT_STATUSID.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(statusId),
                });
        this.assetId = assetId;
        this.statusId = statusId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_DEFECT_DEFECT WHERE ASSET_ID = '"
                + assetId + "' AND DEFECT_STATUS_ID = '"
                + statusId + "' ; ");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_DEFECT;
    }
}
