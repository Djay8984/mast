package model.asset;

import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCategory extends RestHelper<AssetCategoryDto>
{

    public AssetCategory(int id)
    {
        this.endpoint = Endpoint.ASSET_CATEGORY.getUrl(Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_REF_ASSET_CATEGORY WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.REFERENCE_DATA;
    }
}
