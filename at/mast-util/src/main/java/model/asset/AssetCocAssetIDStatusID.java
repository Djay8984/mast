package model.asset;

import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetCocAssetIDStatusID extends RestHelper<CoCPageResourceDto>
{

    private int assetId;
    private int statusId;

    public AssetCocAssetIDStatusID(int assetId, int statusId)
    {
        this.endpoint = Endpoint.ASSET_COC_STATUSID.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(statusId)
        });
        this.assetId = assetId;
        this.statusId = statusId;

        get();

    }

    protected ResultSet getResultSet(String method)
    {

        return DatabaseHelper.executeQuery("select * from mast_asset_codicil where mast_asset_codicil.asset_id = '"
                + assetId
                + "' and mast_asset_codicil.status_id = '"
                + statusId
                + "' AND codicil_type_id = 1;");

    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_CODICIL;
    }

}
