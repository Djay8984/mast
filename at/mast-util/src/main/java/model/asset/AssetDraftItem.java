package model.asset;

import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetDraftItem extends RestHelper<LazyItemDto>
{

    private int assetId;

    public AssetDraftItem(int assetId, int id)
    {
        this.endpoint = Endpoint.DRAFT_ASSET_ITEM.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(id)
        });
        this.assetId = assetId;
        get();
    }

    public AssetDraftItem(int assetId, int id, DraftItemListDto draftItemList)
    {
        this.endpoint = Endpoint.DRAFT_ASSET_ITEM.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(id)
        });
        this.assetId = assetId;
        put(draftItemList);
    }

    public AssetDraftItem(int assetId, DraftItemListDto draftItemList)
    {
        this.endpoint = Endpoint.DRAFT_ASSET_ITEM.getUrl(new String[]{
                Integer.toString(assetId),
                ""
        });
        this.assetId = assetId;
        post(draftItemList);
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper
                .executeQuery("SELECT * FROM mast_asset_draftassetitem WHERE asset_id = " + assetId + " AND id = " + this.dto.getId() + ";");
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.ASSET_DRAFT_ITEM;
    }
}
