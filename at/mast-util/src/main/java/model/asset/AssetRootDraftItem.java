package model.asset;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetRootDraftItem extends RestHelper<LazyItemDto>
{

    private int assetId;

    public AssetRootDraftItem(int assetId)
    {
        this.endpoint = Endpoint.ASSET_ROOT_DRAFT_ITEM.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT adi.* FROM mast_db.mast_asset_draftassetitem adi " +
                "where asset_id = " + assetId);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_DRAFT_ITEM;
    }
}
