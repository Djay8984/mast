package model.asset;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetRootItem extends RestHelper<LazyItemDto>
{

    private int assetId;

    public AssetRootItem(int assetId)
    {
        this.endpoint = Endpoint.ASSET_ROOT_ITEM.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT ai.* FROM mast_db.mast_asset_assetitem ai " +
                "JOIN mast_db.mast_asset_asset a on ai.asset_id = a.id " +
                "where asset_id = " + assetId + " and ai.id = a.asset_item_id");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEM;
    }
}
