package model.asset;

import com.baesystems.ai.lr.dto.assets.AssetLightPageResourceDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.regex.Pattern;

public class AssetQuery extends RestHelper<AssetLightPageResourceDto>
{

    private AssetQueryDto queryDto;

    public AssetQuery(AssetQueryDto queryDto)
    {
        this.endpoint = Endpoint.ASSET_QUERY.getUrl();
        this.queryDto = queryDto;
        post(queryDto);
    }

    public static AssetQueryDto getDefaultDto()
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        /**
         * I am not sure why Category Id is Set to 6???
         * Hence commented for now
         * assetQueryDto.setCategoryId(Collections.singletonList(6L));
         */
        assetQueryDto.setLifecycleStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 10L));
        return assetQueryDto;
    }

    protected ResultSet getResultSet(String method)
    {
        if (queryDto.getSearch() != null)
        {
            String newSearch = queryDto.getSearch().replaceAll(Pattern.quote("*"), "");
            queryDto.setSearch(newSearch);
        }

        if (queryDto.getPartyFunctionTypeId() == null && queryDto.getPartyId() == null && queryDto.getPartyRoleId() == null)
        {
            String sql = "SELECT * from mast_asset_asset WHERE " + EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.ASSET_QUERY)
                    + ";";
            return DatabaseHelper.executeQuery(sql);
        }
        else
        {
            String sql = "SELECT distinct a.*, ap.party_id, ap.party_role_id " +
                    "FROM mast_asset_asset a " +
                    "LEFT JOIN mast_asset_asset_party ap on a.id = ap.asset_id " +
                    "WHERE " + EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.ASSET_QUERY) +
                    "GROUP BY a.id;";
            return DatabaseHelper.executeQuery(sql);
        }
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET;
    }
}
