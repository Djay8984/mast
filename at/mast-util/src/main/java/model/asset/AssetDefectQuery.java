package model.asset;

import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetDefectQuery extends RestHelper<DefectPageResourceDto>
{

    private int id;
    private int page;
    private int size;
    private String sort = "";
    private String order = "";
    private String sqlQuery;
    private CodicilDefectQueryDto queryDto;

    public AssetDefectQuery(int id, int page, int size, String sort, String order)
    {
        this.endpoint = Endpoint.ASSET_DEFECT_QUERY.getUrl(new String[]{
                Integer.toString(id),
                Integer.toString(page),
                Integer.toString(size)
        });

        if (!(sort.equals("") && order.equals("")))
        {
            this.endpoint = this.endpoint + "&order=" + order + "&sort=" + sort;
        }
        CodicilDefectQueryDto queryDto = new CodicilDefectQueryDto();
        this.id = id;
        this.page = page;
        this.size = size;
        this.sort = sort;
        this.order = order;
        this.queryDto = queryDto;
        post(queryDto);
    }

    public AssetDefectQuery(int assetId, int page, int size, CodicilDefectQueryDto queryDto)
    {
        this.endpoint = Endpoint.ASSET_DEFECT_QUERY.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.id = assetId;
        this.page = page;
        this.size = size;
        this.queryDto = queryDto;
        post(queryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        if (sort.equals("") && order.equals(""))
        {
            sqlQuery = "SELECT * FROM MAST_DEFECT_DEFECT WHERE " +
                    EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.DEFECT_QUERY) +
                    " AND ASSET_ID = " + id +
                    " AND deleted = 0 "
                    + sqlLimitForPage(page, size) + ";";
        }
        else
        {
            sqlQuery = "SELECT * FROM MAST_DEFECT_DEFECT WHERE ASSET_ID = " + id + " "
                    + " ORDER BY " + sort
                    + " " + order + " "
                    + sqlLimitForPage(page, size) + ";";
        }
        return DatabaseHelper.executeQuery(sqlQuery);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_DEFECT;
    }
}
