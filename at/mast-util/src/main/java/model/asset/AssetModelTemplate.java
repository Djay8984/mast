package model.asset;

import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.dto.validation.AttributeRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemTemplateDto;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;
import org.testng.Assert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static com.google.common.truth.Truth.assertThat;

public class AssetModelTemplate extends RestHelper<AssetModelTemplateDto>
{

    public AssetModelTemplate()
    {
        this.endpoint = "/asset-model-template";
        get();
    }

    public ResultSet getResultSet(String method)
    {
        return null;
    }

    public TableMap getTableMap()
    {
        return null;
    }

    @Override
    public void verify()
    {
        for (Map.Entry<Long, ItemTemplateDto> entry : dto.getAssetModelTemplate().entrySet())
        {
            ItemTemplateDto itemTemplateDto = entry.getValue();
            long id = entry.getKey();

            ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM mast_ref_assetattributetype WHERE" +
                    " item_type_id = " + id);
            try
            {
                while (rs.next())
                {
                    boolean found = false;
                    for (AttributeRuleDto attributeRuleDto : itemTemplateDto.getValidAttributes())
                    {
                        if (attributeRuleDto.getId() != null && attributeRuleDto.getId() == rs.getInt("id"))
                        {
                            found = true;
                            assertThat(rs.getLong("attribute_type_id")).isEqualTo(attributeRuleDto.getAttributeTypeId());
                            assertThat(rs.getBoolean("transferable")).isEqualTo(attributeRuleDto.getTransferable());
                        }
                    }
                    if (!found)
                    {
                        Assert.fail("Failed to find attribute type relationship id '" + rs.getInt("id") + "' for the item" +
                                "type with the id of '" + rs.getInt("item_type_id") + "'.");
                    }
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

}
