package model.asset;

import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.EndpointQueryHelper;
import helper.RestHelper;

import java.sql.ResultSet;

import static constant.Endpoint.ASSET_ACTIONABLE_ITEM_QUERY;

public class AssetActionableItemQuery extends RestHelper<ActionableItemPageResourceDto>
{

    private int id;
    private int page;
    private int size;
    private String sort = "";
    private String order = "";
    private String sqlQuery = "";
    private CodicilDefectQueryDto queryDto;

    public AssetActionableItemQuery(int id, int page, int size, String sort, String order)
    {
        this.endpoint = ASSET_ACTIONABLE_ITEM_QUERY.getUrl(new String[]{
                Integer.toString(id),
                Integer.toString(page),
                Integer.toString(size)
        });

        if (!(sort.equals("") && order.equals("")))
        {
            this.endpoint = this.endpoint + "&order=" + order + "&sort=" + sort;
        }
        CodicilDefectQueryDto queryDto = new CodicilDefectQueryDto();
        this.id = id;
        this.page = page;
        this.size = size;
        this.sort = sort;
        this.order = order;
        this.queryDto = queryDto;
        post(queryDto);
    }

    public AssetActionableItemQuery(int assetId, int page, int size, CodicilDefectQueryDto queryDto)
    {
        this.endpoint = ASSET_ACTIONABLE_ITEM_QUERY.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.id = assetId;
        this.page = page;
        this.size = size;
        this.queryDto = queryDto;
        post(queryDto);
    }

    protected ResultSet getResultSet(String method)
    {
        if (sort.equals("") && order.equals(""))
        {
            sqlQuery = String.format("SELECT * FROM mast_db.mast_asset_codicil WHERE " +
                    EndpointQueryHelper.buildSqlForQueryDto(queryDto, TableMap.CODICIL_QUERY) +
                    " AND codicil_type_id = 2 AND asset_id = %s", this.id) + sqlLimitForPage(page, size) + ";";
        }
        else
        {
            sqlQuery = String.format("SELECT * FROM mast_db.mast_asset_codicil WHERE codicil_type_id = 2 AND asset_id = %s ORDER BY %s %s", this.id,
                    sort, order) + sqlLimitForPage(page, size) + ";";
        }
        return DatabaseHelper.executeQuery(sqlQuery);
    }

    protected TableMap getTableMap()
    {
        return TableMap.ACTIONABLE_ITEM;
    }
}
