package model.asset;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Asset extends RestHelper<AssetLightDto>
{

    public Asset(int id)
    {
        this.endpoint = Endpoint.ASSET.getUrl(Integer.toString(id));
        get();
    }

    public Asset(AssetLightDto assetDto)
    {
        this.endpoint = Endpoint.ASSET.getUrl("");
        post(assetDto);
    }

    public Asset(AssetLightDto assetDto, int id)
    {
        this.endpoint = Endpoint.ASSET.getUrl(Integer.toString(id));
        put(assetDto);
    }

    @Override
    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET WHERE id = '" + this.dto.getId() + "';");
    }

    @Override
    protected TableMap getTableMap()
    {
        return TableMap.ASSET;
    }
}
