package model.asset;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetNote extends RestHelper<AssetNoteDto>
{

    public AssetNote(int assetId, int assetNoteId)
    {
        this.endpoint = Endpoint.ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetNoteId)
        });
        get();
    }

    public AssetNote(AssetNoteDto assetNote, int assetId)
    {
        this.endpoint = Endpoint.ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                ""
        });
        post(assetNote);
    }

    public AssetNote(AssetNoteDto assetNote, int assetId, int assetNoteId)
    {
        this.endpoint = Endpoint.ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(assetNoteId)
        });
        put(assetNote);
    }

    public void deleteAssetNote(int assetId, String assetNoteId)
    {
        this.endpoint = Endpoint.ASSET_NOTE.getUrl(new String[]{
                Integer.toString(assetId),
                assetNoteId

        });

        delete();

    }

    public ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_CODICIL WHERE id = '" + this.dto.getId() + "'" +
                "AND codicil_type_id = 3;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_NOTE;
    }
}

