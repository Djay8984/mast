package model.asset;

import com.baesystems.ai.lr.dto.assets.AttributePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class AssetItemAttributePage extends RestHelper<AttributePageResourceDto>
{
    private int assetId;
    private int itemId;

    public AssetItemAttributePage(int assetId, int itemId)
    {
        this.endpoint = Endpoint.ASSET_ITEM_ATTRIBUTE.getUrl(new String[]{
                Integer.toString(assetId),
                Integer.toString(itemId),
                ""
        });
        this.assetId = assetId;
        this.itemId = itemId;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT aa.id, aa.value, aa.attribute_type_id " +
                "FROM mast_asset_asset a " +
                "JOIN mast_asset_assetitem ai ON a.id = ai.asset_id " +
                "JOIN mast_asset_assetitemattribute aa ON ai.id = aa.item_id " +
                "WHERE a.id = " + assetId + " AND ai.id = " + itemId + " AND aa.deleted = 0;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.ASSET_ITEM_ATTRIBUTE;
    }
}
