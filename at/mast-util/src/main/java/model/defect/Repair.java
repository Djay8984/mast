package model.defect;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Repair extends RestHelper<RepairDto>
{

    public Repair(int id)
    {
        this.endpoint = Endpoint.DEFECT_REPAIR.getUrl(Integer.toString(id));
        get();
    }

    public Repair(RepairDto defectRepairDto, int id)
    {
        this.endpoint = Endpoint.DEFECT_REPAIR.getUrl(Integer.toString(id));
        post(defectRepairDto);
    }

    public Repair(int id, int repairId)
    {
        String[] args = {Integer.toString(id), Integer.toString(repairId)};
        this.endpoint = Endpoint.DEFECT_REPAIR_REPAIR_ID.getUrl(args);
        get();
    }

    public Repair(RepairDto defectRepairDto, int id, int repairId)
    {
        String[] args = {Integer.toString(id), Integer.toString(repairId)};
        this.endpoint = Endpoint.DEFECT_REPAIR_REPAIR_ID.getUrl(args);
        put(defectRepairDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_DEFECT_REPAIR WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.DEFECT_REPAIR;
    }
}
