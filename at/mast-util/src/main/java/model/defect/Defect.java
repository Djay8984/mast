package model.defect;

import com.baesystems.ai.lr.dto.defects.DefectDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Defect extends RestHelper<DefectDto>
{

    public Defect(int id)
    {
        this.endpoint = Endpoint.DEFECT.getUrl(Integer.toString(id));
        get();
    }

    public Defect(DefectDto defectDto, int id)
    {
        String[] args = {Integer.toString(id)};
        this.endpoint = Endpoint.DEFECT.getUrl(args);
        put(defectDto);
    }

    public Defect(DefectDto defectDto)
    {
        this.endpoint = Endpoint.DEFECT.getUrl();
        post(defectDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_DEFECT_DEFECT WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.DEFECT;
    }
}
