package model.defect;

import com.baesystems.ai.lr.dto.defects.RepairPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class DefectRepairPage extends RestHelper<RepairPageResourceDto>
{

    private int id;
    private int page;
    private int size;

    public DefectRepairPage(int id, int page, int size)
    {
        this.endpoint = Endpoint.DEFECT_REPAIR_PAGE.getUrl(new String[]{
                Integer.toString(id),
                Integer.toString(page),
                Integer.toString(size)
        });
        this.id = id;
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_DEFECT_REPAIR where defect_id = " + id + " " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap()
    {
        return TableMap.DEFECT_REPAIR;
    }
}
