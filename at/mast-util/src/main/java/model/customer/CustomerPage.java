/*
package util.model.customer;

import constant.Endpoint;
import com.baesystems.ai.lr.dto.customers.PartyLightPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class CustomerPage extends RestHelper<PartyLightPageResourceDto> {

    private int page;
    private int size;

    public CustomerPage(int page, int size) {
        this.endpoint = Endpoint.CUSTOMER_PAGE.getUrl(new String[] {
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method) {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CDH_PARTY " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap() {
        return TableMap.NAME;
    }
}
*/