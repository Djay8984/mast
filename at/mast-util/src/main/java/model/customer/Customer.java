package model.customer;

import com.baesystems.ai.lr.dto.customers.OrganisationDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Customer extends RestHelper<OrganisationDto>
{

    public Customer(int id)
    {
        this.endpoint = Endpoint.CUSTOMER.getUrl(Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CDH_PARTY WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.CUSTOMER;
    }
}
