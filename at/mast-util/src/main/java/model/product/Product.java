package model.product;

import com.baesystems.ai.lr.dto.services.ProductDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Product extends RestHelper<ProductDto>
{

    private final int assetId;

    public Product(int assetId)
    {
        this.endpoint = Endpoint.PRODUCT.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        getArray();
    }

    public Product(int assetId, String productCatalogueListDto)
    {
        this.endpoint = Endpoint.PRODUCT.getUrl(Integer.toString(assetId));
        this.assetId = assetId;
        putExpectArray(productCatalogueListDto);
    }

    public static void deleteAllProductsLinkedToAsset(int assetId)
    {
        DatabaseHelper.insertQuery("DELETE FROM MAST_PRODUCT WHERE asset_id = " + assetId + " AND deleted = 0;");
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery(
                "SELECT DISTINCT product.id, catalogue.id AS product_id, catalogue.name AS product_name, catalogue.description as product_description, catalogue.product_group_id, catalogue.display_order, "
                        +
                        "pgroup.name AS product_group_name, pgroup.description AS product_group_description, pgroup.display_order AS pg_display_order, "
                        +
                        "family.name AS product_type_name, family.description AS product_type_description " +
                        "FROM MAST_REF_RULESETCATEGORY_PRODUCTCATALOGUE ruleset " +
                        "JOIN MAST_ASSET_ASSET asset ON ruleset.ruleset_id = asset.product_ruleset_id " +
                        "JOIN MAST_REF_PRODUCTCATALOGUE catalogue ON ruleset.product_catalogue_id = catalogue.id " +
                        "JOIN MAST_REF_PRODUCTGROUP pgroup ON catalogue.product_group_id = pgroup.id " +
                        "JOIN MAST_REF_PRODUCTFAMILY family ON pgroup.product_family_id = family.id " +
                        "JOIN MAST_ASSET_ASSETPRODUCT product ON catalogue.id = product.product_catalogue_id " +
                        "WHERE product.id = " + this.getDto().getId() + " and product.deleted = 0 ORDER BY product.id ASC;");
    }

    protected TableMap getTableMap()
    {
        return TableMap.PRODUCT;
    }
}
