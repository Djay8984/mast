package model.milestone;

import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class MilestonePage extends RestHelper<CaseMilestoneListDto>
{

    private int id;

    public MilestonePage(int id)
    {
        this.endpoint = Endpoint.MILESTONE.getUrl(Integer.toString(id));
        this.id = id;
        get();
    }

    public MilestonePage(int id, CaseMilestoneListDto caseMilestoneListDto)
    {
        this.endpoint = Endpoint.MILESTONE.getUrl(Integer.toString(id));
        this.id = id;
        put(caseMilestoneListDto);
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CASE_CASEMILESTONE WHERE case_id = '" + id + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.MILESTONE;
    }
}
