/*
package util.model.builder;

import com.baesystems.ai.lr.dto.assets.BuilderPageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class BuilderPage extends RestHelper<BuilderPageResourceDto> {

    private int page;
    private int size;

    public BuilderPage(int page, int size) {
        this.endpoint = Endpoint.BUILDER_PAGE.getUrl(new String[] {
                Integer.toString(page),
                Integer.toString(size)
        });
        this.page = page;
        this.size = size;
        get();
    }

    protected ResultSet getResultSet(String method) {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CDH_PARTY " + sqlLimitForPage(page, size) + ";");
    }

    protected TableMap getTableMap() {
        return TableMap.NAME;
    }
}
*/
