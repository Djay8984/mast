package model.builder;

import com.baesystems.ai.lr.dto.assets.BuilderDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Builder extends RestHelper<BuilderDto>
{

    public Builder(int id)
    {
        this.endpoint = Endpoint.BUILDER.getUrl(Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM MAST_CDH_PARTY WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.NAME;
    }

}
