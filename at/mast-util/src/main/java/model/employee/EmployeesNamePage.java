package model.employee;

import com.baesystems.ai.lr.dto.employees.EmployeePageResourceDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class EmployeesNamePage extends RestHelper<EmployeePageResourceDto>
{

    private String searchKey;

    public EmployeesNamePage(String searchKey)
    {
        this.endpoint = Endpoint.EMPLOYEE.getUrl(searchKey);
        this.searchKey = searchKey;
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_lrp_employee WHERE first_name like '" +
                searchKey + "' OR last_name like '" + searchKey + "';"
        );
    }

    protected TableMap getTableMap()
    {
        return TableMap.EMPLOYEE;
    }

}
