package model.employee;

import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import constant.Endpoint;
import constant.TableMap;
import helper.DatabaseHelper;
import helper.RestHelper;

import java.sql.ResultSet;

public class Employee extends RestHelper<LrEmployeeDto>
{

    public Employee(int id)
    {
        this.endpoint = Endpoint.EMPLOYEE_EMPLOYEEID.getUrl(Integer.toString(id));
        get();
    }

    protected ResultSet getResultSet(String method)
    {
        return DatabaseHelper.executeQuery("SELECT * FROM mast_db.mast_lrp_employee WHERE id = '" + this.dto.getId() + "';");
    }

    protected TableMap getTableMap()
    {
        return TableMap.EMPLOYEE;
    }
}
