package helper;

import constant.TableMap;
import junit.framework.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EndpointQueryHelper
{

    public static String buildSqlForQueryDto(Object queryDto, TableMap tableMap)
    {
        String whereSql = "";
        String[] uncheckedMethods = {
                "getClass", "hashCode", "toString", "getItemAndType", "getInternalId"
        };
        for (Method dtoMethod : queryDto.getClass().getMethods())
        {
            try
            {
                Object value = dtoMethod.invoke(queryDto);
                if (value != null
                        && !Arrays.toString(uncheckedMethods).contains(dtoMethod.getName())
                        && dtoMethod.getName() != null
                        )
                {
                    if (!whereSql.equals(""))
                    {
                        whereSql += " AND ";
                    }
                    String dbColumn = (String) tableMap.getTableMap().get(dtoMethod.getName());
                    if (value instanceof List<?>)
                    {
                        List<?> listValue = (List<?>) value;
                        String listString = listValue.toString().replace('[', ' ');
                        listString = listString.replace(']', ' ');
                        whereSql += dbColumn + " IN (" + listString + ")";
                    }
                    else if (value instanceof String)
                    {
                        String stringValue = (String) value;
                        whereSql += dbColumn + " LIKE '%" + stringValue + "%'";
                    }
                    else if (value instanceof Long)
                    {
                        Long longValue = (Long) value;
                        whereSql += dbColumn + " = '" + longValue + "'";
                    }
                    else if (value instanceof Double)
                    {
                        whereSql += dbColumn + minMax(dtoMethod) + value + "'";
                    }
                    else if (value instanceof Date)
                    {
                        Date dateValue = (Date) value;

                        //Subtract/Add 12 hours from the date so it's at midnight, not noon
                        int addSubtract = 0;
                        if (dtoMethod.getName().contains("Max"))
                        {
                            addSubtract = 12;
                        }
                        else if (dtoMethod.getName().contains("Min"))
                        {
                            addSubtract = -12;
                        }
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(dateValue);
                        calendar.add(Calendar.HOUR, addSubtract);

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        whereSql += dbColumn + minMax(dtoMethod) + sdf.format(calendar.getTime()) + "'";
                    }
                    else
                    {
                        Assert.fail("Uncaught object type in the query dto! Get method was: " + dtoMethod.getName());
                    }
                }
            }
            catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException e)
            {
                //We don't need anything here
            }
        }
        return whereSql;
    }

    private static String minMax(Method dtoMethod)
    {
        String operator;
        if (dtoMethod.getName().contains("Max"))
        {
            operator = " <= '";
        }
        else if (dtoMethod.getName().contains("Min"))
        {
            operator = " >= '";
        }
        else
        {
            operator = " = '";
        }
        return operator;
    }
}
