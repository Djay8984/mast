package helper;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import model.asset.AssetItem;
import model.asset.AssetModel;
import model.asset.AssetQuery;
import model.cases.CaseQuery;
import model.job.JobQueryPage;
import model.referencedata.ReferenceDataMap;
import model.wip.WIPCoCQuery;
import org.testng.Assert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.frameworkium.core.ui.tests.BaseTest.getDriver;
import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.*;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

/**
 * This class is to help with the creation and insertion of test data needed for automated frontend.
 * <p>
 * Important: - Existing test data should always be used if it allows the test to be ran and pass unlimited times. - If
 * a test can only be ran once with existing test data, you should create test data to allow the test to run unlimited
 * times.
 */
public class TestDataHelper
{

    /**
     * Inserts an IHS asset into MAST_IHS_ASSET, this is for creating a unique IMO number you can use on case setup
     *
     * @param yardNumber     - The yard number for the asset
     * @param ihsAssetTypeId - The asset type
     * @return - The IMO number for the IHS asset
     */
    public static long insertIhsAsset(Integer yardNumber, int ihsAssetTypeId)
    {
        try
        {
            ResultSet resultSet = DatabaseHelper.executeQuery(
                    "SELECT LRNO FROM ABSD_SHIP_SEARCH ORDER BY LRNO DESC LIMIT 1",
                    DatabaseHelper.Database.IHS);
            resultSet.first();
            long newIhsAssetId = resultSet.getLong("LRNO");
            newIhsAssetId++;
            DatabaseHelper.insertQuery(
                    "INSERT INTO ABSD_SHIP_SEARCH (LRNO,YARDNO,STAT5CODE) " +
                            "VALUES('" + newIhsAssetId + "', '" + yardNumber + "', '" + ihsAssetTypeId + "')",
                    DatabaseHelper.Database.IHS);
            ResultSet rs = DatabaseHelper.executeQuery(
                    "SELECT LRNO FROM ABSD_SHIP_SEARCH ORDER BY LRNO DESC LIMIT 1;",
                    DatabaseHelper.Database.IHS);
            if (rs.next())
            {
                return rs.getLong("LRNO");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Inset an asset with a generated yard number with builder ID of 1
     *
     * @return - Asset Data model with IMO/Builder/Yard Number set
     */
    public static AssetDto insertAsset()
    {
        return insertAsset(TestDataHelper.getUniqueYardNumber(), 1);
    }

    /**
     * Method to insert a new asset into the MAST_DB
     *
     * @param builderId  - Builder ID for asset
     * @param yardNumber - Yard Number for asset
     * @return - Asset Data model with IMO/Builder/Yard Number set
     */
    public static AssetDto insertAsset(Integer yardNumber, int builderId)
    {
        long ihsAssetId = insertIhsAsset(yardNumber, 1);
        String buildDate = "2002-10-14";
        String keelLayingDate = "2011-10-29";

        String yardNumberString = "";
        if (yardNumber != null)
        {
            yardNumberString = Integer.toString(yardNumber);
        }

        DatabaseHelper.insertQuery("INSERT INTO `mast_db`.`MAST_ASSET_ASSET`" +
                " (`asset_type_id`, `imo_number`, `class_status_id`, `build_date`, `keel_laying_date`, " +
                " `gross_tonnage`, `builder_id`, `iacs_society_id`, `product_ruleset_id`, " +
                " `proposed_flag_state_id`, `proposed_registered_port_id`, `name`, `class_notation`, `is_lead`, `asset_category_id`," +
                " `yard_number`, `harmonisation_date`) " +
                "VALUES " +
                "('2', '" + ihsAssetId + "', '1', '" + buildDate + "', '" + keelLayingDate + "', '67410', " +
                "'" + builderId + "', '1', '1', '417', '417', 'Yorktown', 'Yorktown', '0', '5', " +
                "'" + yardNumberString + "', '2015-10-01');");

        LinkResource builder = new LinkResource();
        builder.setId((long) builderId);

        LinkResource ihsAssetDto = new LinkResource();
        ihsAssetDto.setId(ihsAssetId);

        AssetDto assetDto = new AssetDto();
        assetDto.setBuilder("John Smith");
        if (yardNumber != null)
        {
            assetDto.setYardNumber(Integer.toString(yardNumber));
        }
        assetDto.setIhsAsset(ihsAssetDto);
        try
        {
            assetDto.setBuildDate(BACKEND_TIME_FORMAT.parse(buildDate));
            assetDto.setKeelLayingDate(BACKEND_TIME_FORMAT.parse(keelLayingDate));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return assetDto;
    }

    /**
     * Retrieves the asset with the highest ID in the mast_asset table
     *
     * @return - Asset with highest ID
     */
    public static ResultSet retrieveLatestAsset()
    {
        try
        {
            ResultSet resultSet = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET ORDER BY id DESC LIMIT 1");
            resultSet.first();
            return resultSet;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method which generates a random number to serve as the unique yard number
     *
     * @return - Unique yard number
     */
    public static int getUniqueYardNumber()
    {
        Random random = new Random();
        return random.nextInt(((999999 - 1) + 1) + 1);
    }

    /**
     * Method which inserts a new uncommitted case so that we can delete it
     *
     * @return - the row ID of the inserted case
     */
    public static String insertUncommittedCase()
    {
        DatabaseHelper.insertQuery("INSERT INTO mast_db.MAST_CASE_CASE (asset_id,\n" +
                "case_type_id,\n" +
                "case_status_id, \n" +
                "pre_eic_inspection_status_id,\n" +
                "risk_assessment_status_id,\n" +
                "business_process_id, \n" +
                "toc_acceptance_date, \n" +
                "flag_state_id,\n" +
                "proposed_iacs_society_id, \n" +
                "contract_reference_number, \n" +
                "maintainence_regime,\n" +
                "created_on)\n" +
                "values (200,1,1,1,1,10,'1998-10-08', 301, 7, '49582-05', '0938295', '1987-08-18 14:33');");

        ResultSet rs = DatabaseHelper.executeQuery("SELECT last_insert_id() AS id;");
        String getRowId = "0";
        try
        {
            rs.first();
            getRowId = Integer.toString(rs.getInt("id"));
            return getRowId;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return getRowId;
    }

    /**
     * Method which tries to find the deleted case
     *
     * @return - the number of cases it found
     */
    public static int findDeletedCase(String caseId) throws SQLException
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT COUNT(*) FROM mast_db.MAST_CASE_CASE WHERE id = " + caseId + ";");
        rs.first();
        return rs.getInt("count(*)");
    }

    /**
     * Method which gets the case number from the current URL * @return - the caseNumber
     */
    public static String returnCaseNumberPresentInUrl()
    {
        String currentURL = getDriver().getCurrentUrl();
        Pattern pattern = Pattern.compile("case\\/(\\d+)\\/case-setup");
        Matcher matcher = pattern.matcher(currentURL);
        if (matcher.find())
        {
            return matcher.group(1);
        }
        else
        {
            return "";
        }
    }

    /**
     * Method to captilise the first letter of a string, and lowercase everything else
     *
     * @param value - Value to format
     * @return - Formatted value
     */
    public static String captiliseFormat(String value)
    {
        return value.substring(0, 1).toUpperCase() + value.substring(1).toLowerCase();
    }

    public static int insertService(String productId, String assetId)
    {
        DatabaseHelper.insertQuery("insert into mast_db.mast_asset_scheduledservice (service_catalogue_id, \n" +
                "asset_id, \n" +
                "cycle_periodicity, \n" +
                "assigned_date, \n" +
                "due_date, \n" +
                "lower_range_date, \n" +
                "upper_range_date,\n" +
                "deleted,\n" +
                "provisional_date_flag, \n" +
                "service_status_id)\n" +
                "values\n" +
                "(81, " + assetId + ", 36, \"2015-04-12\", \"2015-11-30\", \"2015-10-30\", " +
                "\"2015-11-30\", 0, 0, 1);");

        ResultSet rs = DatabaseHelper.executeQuery("SELECT last_insert_id() as id;");
        int getRowId = 0;
        try
        {
            rs.first();
            getRowId = rs.getInt("id");
            return getRowId;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return getRowId;
    }

    public static Date randomDate()
    {
        long beginTime = Timestamp.valueOf("1980-01-01 00:00:00").getTime();
        long endTime = Timestamp.valueOf("2015-12-31 00:58:00").getTime();
        long diff = endTime - beginTime + 1;
        long date = beginTime + (long) (Math.random() * diff);

        return new Date(date);
    }

    /**
     * Method which inserts a new codicil so that we can delete it
     *
     * @return - the row ID of the inserted case
     */
    public static String insertCodicil()
    {
        DatabaseHelper.insertQuery("Insert into mast_db.mast_codicil(name, \n" +
                "template_name, \n" +
                "impose_date, \n" +
                "due_date, \n" +
                "affected_items, \n" +
                "description, \n" +
                "codicil_type_id,\n" +
                "codicil_status_id,\n" +
                "asset_id,\n" +
                "inherited_from_losing_society)\n" +
                "values('Automated Codicil','Automated Template','2014-03-20','2014-04-05','','An automated codicil',1,2,1,0);");

        ResultSet rs = DatabaseHelper.executeQuery("SELECT last_insert_id() AS id;");
        String getRowId = "0";
        try
        {
            rs.first();
            getRowId = Integer.toString(rs.getInt("id"));
            return getRowId;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return getRowId;
    }

    public static String getTodayDate()
    {
        Calendar currentDate = Calendar.getInstance();
        return FRONTEND_TIME_FORMAT.format(currentDate.getTime());
    }

    public static String getYesterdayDate()
    {
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, -1);
        return FRONTEND_TIME_FORMAT.format(currentDate.getTime());
    }

    public static Date getTomorrowDate()
    {
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, 1);
        return currentDate.getTime();
    }

    public static int getLastInsertedRow(String tableName) throws SQLException
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT id FROM " + tableName + " ORDER BY id DESC LIMIT 1");
        rs.first();
        return rs.getInt("id");
    }

    public static CustomerHasFunctionDto getPartyDetailsFromAsset(int assetId) throws SQLException
    {
        CustomerHasFunctionDto partyDto = new CustomerHasFunctionDto();
        LinkResource resource = new LinkResource();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT party_id, party_role_id FROM mast_asset_asset_party WHERE asset_id = " + assetId + ";");
        rs.first();
        while (rs.next())
        {
            partyDto.setId((long) rs.getInt("party_id"));
            resource.setId((long) rs.getInt("party_role_id"));
            partyDto.setCustomerFunction(resource);
        }
        return partyDto;
    }

    public static ArrayList<Long> getEmployeeId(int caseId) throws SQLException
    {
        ArrayList<Long> ids = new ArrayList<>();
        ResultSet caseResource = DatabaseHelper.executeQuery(
                "SELECT DISTINCT id from mast_db.mast_case_caseresource where case_id = " + caseId + ";");
        while (caseResource.next())
        {
            Long id = caseResource.getLong("id");
            ids.add(id);
        }
        return ids;
    }

    public static ItemDto getItemDetailsFromAsset(int assetId) throws SQLException
    {
        ItemDto itemDto = new ItemDto();
        LinkResource resource = new LinkResource();
        List<AttributeDto> attributeDtos = new ArrayList<>();

        ResultSet rs = DatabaseHelper.executeQuery("SELECT distinct ai.id as item_id, ai.item_type_id, aa.id as attribute_id, ai.name, aa.value " +
                "FROM mast_asset_asset a " +
                "JOIN mast_asset_assetitem ai ON a.id = ai.asset_id " +
                "JOIN mast_asset_assetitemattribute aa ON ai.id = aa.item_id " +
                "WHERE a.id = " + assetId);
        while (rs.next())
        {
            if (rs.isFirst())
            {
                itemDto.setId((long) rs.getInt("item_id"));
                itemDto.setName(rs.getString("name"));
                resource.setId((long) rs.getInt("item_type_id"));
                itemDto.setItemType(resource);
            }
            AttributeDto attributeDto = new AttributeDto();
            attributeDto.setId((long) rs.getInt("attribute_id"));
            attributeDto.setValue(rs.getString("value"));
            attributeDtos.add(attributeDto);
            itemDto.setAttributes(attributeDtos);
        }
        return itemDto;
    }

    public static Map<String, Integer> getAssetItemAttributeIds() throws SQLException
    {
        String sqlString = "select i.asset_id, i.id as item_id, a.id as attribute_id from mast_asset_assetitem i, "
                + "mast_asset_assetitemattribute a, mast_ref_assetattributetype t where i.id = a.item_id " +
                "and i.asset_id is not null and a.value is not null and t.id = a.attribute_type_id " +
                "and t.name = 'ALPHANUMERIC ID'";
        ResultSet results = DatabaseHelper.executeQuery(sqlString);

        Map<String, Integer> ids = new HashMap<>();

        results.last();
        int count = results.getRow();
        Random random = new Random();
        results.absolute(random.nextInt(count) + 1);
        ids.put("asset_id", results.getInt("asset_id"));
        ids.put("item_id", results.getInt("item_id"));
        ids.put("attribute_id", results.getInt("attribute_id"));
        return ids;
    }

    public static Map<String, Integer> getRandomAssetItem() throws SQLException
    {
        String sqlString = "select * from mast_asset_assetItem where asset_id is not null";
        ResultSet results = DatabaseHelper.executeQuery(sqlString);

        results.last();
        int max = results.getRow() - 1;
        Random random = new Random();
        results.absolute(random.nextInt(max) + 1);
        Map<String, Integer> ids = new HashMap<>();
        ids.put("assetId", results.getInt("asset_id"));
        ids.put("itemId", results.getInt("id"));
        return ids;
    }

    public static int getAttributeTypeId() throws SQLException
    {
        String sqlString = "select * from mast_ref_assetattributetype where name = 'ALPHANUMERIC ID'";
        ResultSet results = DatabaseHelper.executeQuery(sqlString);

        results.last();
        int max = results.getRow();
        Random random = new Random();
        results.absolute(random.nextInt(max));
        return results.getInt("id");
    }

    public static HashMap<String, Object> getReferenceDataById(String referenceDataTableName, int id, List<String> tableColumnNames)
            throws SQLException
    {
        HashMap<String, Object> map = new HashMap<>();
        String sqlQuery = String.format("select * from %s where id = %s", referenceDataTableName, id);
        ResultSet rs = DatabaseHelper.executeQuery(sqlQuery);
        rs.next();
        map.put("id", id);
        for (String tableColumnName : tableColumnNames)
        {
            map.put(tableColumnName, rs.getObject(tableColumnName));
        }
        return map;
    }

    public static int getRandomNumber(int min, int max)
    {
        Random rn = new Random();
        int range = ((max - min) + min);
        return rn.nextInt(range) + 1;
    }

    public static String getEmployeeWithJobsOrCases(boolean hasJobsCases)
    {
        String query = "SELECT DISTINCT e.first_name, e.last_name FROM mast_lrp_employee e " +
                "LEFT JOIN mast_case_caseresource cr ON e.id = cr.employee_id " +
                "LEFT JOIN mast_job_jobresource jr ON e.id = jr.lr_employee_id ";

        if (hasJobsCases)
        {
            query += "WHERE cr.id is not null or jr.id is not null limit 1;";
        }
        else
        {
            query += "WHERE cr.id is null and jr.id is null limit 1";
        }

        ResultSet rs = DatabaseHelper.executeQuery(query);
        try
        {
            rs.first();
            return rs.getString("first_name") + " " + rs.getString("last_name");
        }
        catch (SQLException e)
        {
            Assert.fail("The test was unable to get an employee");
            return null;
        }
    }

    public static String getFlagCodeFromSearch()
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT name FROM mast_ref_flagadministration " +
                "WHERE id =  " + getRandomNumber(100, 200) + ";");
        try
        {
            rs.first();
            return rs.getString("name");
        }
        catch (SQLException e)
        {
            Assert.fail("Unable to find the flag name");
            return null;
        }
    }

    public static int getBuilderId(String name)
    {
        String[] names = name.split(" ");
        ResultSet rs = DatabaseHelper.executeQuery("SELECT id FROM mast_lrp_employee " +
                "WHERE first_name LIKE '%" + names[0] + "%'" +
                "AND last_name LIKE '%" + names[1] + "%';");
        try
        {
            rs.first();
            return rs.getInt("id");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    public static LocalDate toLocalDate(Date date)
    {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date toDate(LocalDate localDate)
    {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static CaseQuery getCasesForDefaultEmployee()
    {
        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Collections.singletonList(DEFAULT_EMPLOYEE));
        caseQueryDto.setCaseStatusId(Arrays.asList(1L, 3L));
        return new CaseQuery(caseQueryDto);
    }

    public static List<String> getItemNamesByAssetId(int assetId)
    {
        AssetModel assetModel = new AssetModel(assetId);
        return assetModel.getDto().getItems().get(0).getItems().stream()
                .map(c -> new AssetItem(assetId, c.getId().intValue()))
                .map(d -> d.getDto().getName())
                .collect(Collectors.toList());
    }

    public static String getAttributeTypeNameById(int Id)
    {
        ArrayList<ReferenceDataDto> attributeTypeName = new ReferenceDataMap(ASSET, ATTRIBUTE_TYPES)
                .getReferenceDataArrayList();
        return attributeTypeName
                .stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(Id)))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    public static int getAssetIdByImoNumber(String imoNumber)
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto
                .setImoNumber(Collections.singletonList(Long.parseLong(imoNumber)));
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        return assetQuery.getDto().getContent().get(0).getId()
                .intValue();
    }

    public static int getAssetIdByName(String name)
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto
                .setSearch(name);
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        return assetQuery.getDto().getContent().get(0).getId()
                .intValue();
    }

    public static AssetQuery getAssetsForDefaultEmployee()
    {
        CaseQuery caseQuery = getCasesForDefaultEmployee();
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        List<Long> assetIds = new ArrayList<>();
        caseQuery.getDto().getContent().forEach(c -> assetIds.add(c.getAsset().getId()));
        assetQueryDto.setIdList(assetIds);
        return new AssetQuery(assetQueryDto);
    }

    public static List<CoCDto> getWipCoCsForJob(final Long jobId, final String wipCocName)
    {
        CodicilDefectQueryDto queryDto = new CodicilDefectQueryDto();
        queryDto.setSearchString(wipCocName);

        WIPCoCQuery wipCoCQuery = new WIPCoCQuery(jobId.toString(), queryDto);
        return wipCoCQuery.getDto().getContent();
    }

    public static List<JobDto> getJobsByAssetId(final Long assetId)
    {
        final JobQueryDto jobQueryDto = new JobQueryDto();
        jobQueryDto.setAssetId(Arrays.asList(assetId));
        final JobQueryPage jobQueryPage = new JobQueryPage(jobQueryDto);
        return jobQueryPage.getDto().getContent();
    }

    public static String getClassStatusById(int id)
    {
        ArrayList<ReferenceDataDto> classStatus = new ReferenceDataMap(ASSET, CLASS_STATUSES)
                .getReferenceDataArrayList();
        return classStatus.stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    public static List<List<String>> getExpectedAttributes(int assetId, String itemName) throws SQLException
    {
        List<List<String>> table = new ArrayList<>();
        String sqlString = "SELECT name, min_occurance, max_occurance, naming_order " +
                "FROM mast_ref_assetattributetype where item_type_id in " +
                "(select item_type_id from mast_asset_assetitem " +
                "where asset_id = " + assetId + " and name = '" + itemName + "') and min_occurance != 1 " +
                "order by display_order, name;";
        ResultSet resultset = DatabaseHelper.executeQuery(sqlString);
        while (resultset.next())
        {
            List<String> row = new ArrayList<>();
            row.add(resultset.getString("name"));
            row.add(resultset.getString("min_occurance"));
            row.add(resultset.getString("max_occurance"));
            row.add(resultset.getString("naming_order"));
            table.add(row);
        }

        return table;
    }

    public static AssetTypeDto getAssetTypeBasedOnParent(Long parentId)
    {
        ReferenceDataMap assetTypes = new ReferenceDataMap(ASSET, ASSET_TYPES);
        for (Object assetTypeDto : assetTypes.getReferenceDataAsClass(AssetTypeDto.class))
        {
            // do not change to .equals under any circumstances!
            if (parentId == ((AssetTypeDto) assetTypeDto).getParentId())
            {
                return (AssetTypeDto) assetTypeDto;
            }
        }
        return null;
    }

    public static List<String> getItemsFromReferenceData(int assetId, String rootItem)
    {
        List<String> itemName = new ArrayList<>();
        String sqlString = "SELECT atr.id, aitf.name from_name, aitt.name to_name, atr.min_occurance, atr.max_occurance " +
                "FROM mast_ref_assetitemtyperelationship atr " +
                "join mast_ref_assetitemtype aitf on atr.from_item_type_id = aitf.id " +
                "join mast_ref_assetitemtype aitt on atr.to_item_type_id = aitt.id " +
                "WHERE from_item_type_id = (SELECT item_type_id FROM mast_asset_versionedassetitem where asset_id = " + assetId + " AND name = '"
                + rootItem + "') " +
                "AND atr.asset_category_id = (SELECT asset_category_id from mast_asset_asset where id = " + assetId + ") " +
                "order by aitt.name;";

        ResultSet resultset = DatabaseHelper.executeQuery(sqlString);
        try
        {
            while (resultset.next())
            {
                itemName.add(resultset.getString("to_name"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return itemName;
    }

    public static List<ReferenceDataDto> getSdoFromReferenceData()
    {
        ResultSet resultSet = DatabaseHelper.executeQuery(
                "SELECT * FROM mast_db.mast_ref_office WHERE office_role_id = 1");
        List<ReferenceDataDto> sdoOffices = new ArrayList<>();
        try
        {
            while (resultSet.next())
            {
                ReferenceDataDto referenceDataDto = new ReferenceDataDto();
                referenceDataDto.setId(resultSet.getLong("id"));
                referenceDataDto.setName(resultSet.getString("name"));
                sdoOffices.add(referenceDataDto);
            }
        }
        catch (SQLException e)
        {
            Assert.fail(e.toString());
        }
        return sdoOffices;
    }

    public static List<HashMap<String, Object>> getNonRestrictedByFlagProductByProductFamilyId(String productFamilyId) throws SQLException
    {

        List<HashMap<String, Object>> products = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();

        String sqlQuery = String.format(
                "SELECT distinct pc.id, pc.name, pc.product_group_id FROM mast_db.mast_ref_productcatalogue as pc " +
                        "INNER JOIN mast_db.mast_ref_productgroup as pg " +
                        "ON pc.product_group_id = pg.id " +
                        "INNER JOIN mast_db.mast_ref_productfamily as pf " +
                        "ON   pg.product_family_id = pf.id " +
                        "WHERE pf.id = %s ;",
                productFamilyId);

        ResultSet rs = DatabaseHelper.executeQuery(sqlQuery);

        while (rs.next())
        {
            map = new HashMap<>();
            map.put("id", rs.getObject("id"));
            map.put("name", rs.getObject("name"));
            products.add(map);
        }

        return products;
    }

    public static List<HashMap<String, Object>> getRestrictedByFlagProductByProductFamilyId(String productFamilyId, String flagAdministrationId)
            throws SQLException
    {

        List<HashMap<String, Object>> products = new ArrayList<>();
        HashMap<String, Object> map = new HashMap<>();

        String sqlQuery = String.format(
                "SELECT distinct pc.id, pc.name, pc.product_group_id FROM mast_db.mast_ref_productcatalogue as pc " +
                        "INNER JOIN mast_db.mast_ref_productgroup as pg " +
                        "ON pc.product_group_id = pg.id " +
                        "INNER JOIN mast_db.mast_ref_productfamily as pf " +
                        "ON   pg.product_family_id = pf.id " +
                        "JOIN mast_db.mast_ref_servicecatalogue as sc " +
                        "on sc.product_catalogue_id = pc.id " +
                        "LEFT JOIN mast_db.mast_ref_servicecatalogue_flagadministration as scf " +
                        "on sc.id = scf.service_catalogue_id " +
                        "WHERE pf.id = %s AND scf.flag_administration_id = %s ;",
                productFamilyId, flagAdministrationId);

        ResultSet rs = DatabaseHelper.executeQuery(sqlQuery);
        while (rs.next())
        {
            map = new HashMap<>();
            map.put("id", rs.getObject("id"));
            map.put("name", rs.getObject("name"));
            products.add(map);
        }
        return products;
    }

    public static List<String> getServiceRegimeNameByProductCatalogueId(String productCatalogueId) throws SQLException
    {

        List<String> serviceRegimeNames = new ArrayList<>();
        serviceRegimeNames.add("");

        String sqlQuery = String.format(
                "SELECT sr.name AS service_regime_name, COUNT(sc.id) AS total_services FROM mast_db.mast_ref_servicecatalogue AS sc " +
                        "INNER JOIN mast_db.mast_ref_productcatalogue AS pc " +
                        "ON sc.product_catalogue_id = pc.id " +
                        "INNER JOIN mast_db.mast_ref_schedulingregime AS sr " +
                        "ON sc.scheduling_regime_id = sr.id " +
                        "WHERE product_catalogue_id = %s " +
                        "GROUP BY sr.name ;",
                productCatalogueId);

        ResultSet rs = DatabaseHelper.executeQuery(sqlQuery);
        while (rs.next())
        {
            serviceRegimeNames.add(rs.getObject("service_regime_name").toString());
        }

        return serviceRegimeNames;
    }

    public static Integer getLatestAssetVersion(int assetId)
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT asset_version_id FROM mast_asset_versionedasset "
                + "WHERE id = " + assetId + " ORDER BY asset_version_id DESC LIMIT 1");
        try
        {
            rs.first();
            return rs.getInt("asset_version_id");
        }
        catch (SQLException e)
        {
            assert_().fail(e.getMessage());
            return 0;
        }
    }

    public static String truncateString(int maxLength, String string)
    {
        String returnValue = string.trim();
        if (string.length() > maxLength)
            returnValue = string.substring(0, maxLength) + "…";
        return returnValue;
    }

    public static ArrayList<String> getLifecycleStatusesNames()
    {
        ArrayList<ReferenceDataDto> lifecycleStatusDtos =
                new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES).getReferenceDataArrayList();
        ArrayList<String> status = new ArrayList<>();
        for (ReferenceDataDto lifecycleStatusDto : lifecycleStatusDtos)
        {
            status.add(lifecycleStatusDto.getName());
        }
        return status;
    }
}
