package helper;

import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.frameworkium.core.common.properties.Property;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;
import constant.TableMap;
import org.apache.http.HttpStatus;
import org.testng.Assert;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.google.common.truth.Truth.assert_;
import static com.jayway.restassured.config.DecoderConfig.decoderConfig;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import static com.jayway.restassured.config.RestAssuredConfig.newConfig;
import static constant.LloydsRegister.BACKEND_TIME_FORMAT;

/**
 * Class to help calling the endpoints for the MAST BE. This will support all operations, Rest Assured
 * shouldn'putExpectArray be used inside your frontend. Extra functionality, if needed, should be inserted in here.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class RestHelper<T>
{

    public final static String BE_PREFIX = "/mast/api/v2";

    /**
     * Set the port for backend assured based on environment
     */
    static
    {
        try
        {
            if (Property.GRID_URL.isSpecified())
            {
                RestAssured.baseURI = "http://" + new URL(Property.GRID_URL.getValue()).getHost() + ":8080";
            }
            else
            {
                RestAssured.baseURI = "http://localhost:8080";
            }
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        RestAssured.config = newConfig()
                .decoderConfig(decoderConfig().defaultContentCharset("UTF-8"))
                .encoderConfig(encoderConfig().defaultContentCharset("UTF-8"))
                .objectMapperConfig(
                        ObjectMapperConfig.objectMapperConfig().jackson2ObjectMapperFactory(new Jackson2ObjectMapperFactory()
                        {
                            @SuppressWarnings("rawtypes")
                            @Override
                            public com.fasterxml.jackson.databind.ObjectMapper create(Class cls, String charset)
                            {
                                com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
                                objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
                                return objectMapper;
                            }
                        }));
    }

    protected String endpoint;
    protected int statusCode = HttpStatus.SC_OK;

    protected T dto;
    protected Object pageArrayDto;
    private String authGroup = "Admin";
    private String authUser = "Allan Hyde";
    private T[] dtoArray;
    private Class<T> typeParameter;
    private Class<T> typeParameterArray;

    @SuppressWarnings("unchecked")
    public RestHelper()
    {
        typeParameter = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
        typeParameterArray = (Class<T>) Array.newInstance(typeParameter, 0).getClass();
    }

    /**
     * Generates a group of security headers for authentication and authorisation.
     *
     * @param checkSum
     * @return List of headers
     */
    private static Headers createHeaders(final String checkSum, String group, String user)
    {
        final Header groupsHeader = new Header("groups", group);
        final Header userHeader = new Header("user", user);
        final Header signHeader = new Header("idsclientsign", checkSum);
        final Header versionHeader = new Header("idsclientversion", SignUtils.getClientVersion());
        return new Headers(groupsHeader, userHeader, signHeader, versionHeader);
    }

    /**
     * This method performs a GET request on a desired endpoint. It will serialize the JSON response back to a data
     * model which is provided by the developers. For example, for a case, use the class CaseDto.
     * <p>
     * This is for endpoints which provide more than one of the objects! For example, multiple cases, asset etc.
     *
     * @return - The serialized object from the response
     */
    @SuppressWarnings("unchecked")
    protected void get()
    {
        final String checkSum = SignUtils.generateChecksum("GET", getUrl(), null);
        this.setDto(
                RestAssured
                        .expect()
                        .statusCode(statusCode)
                        .given()
                        .headers(createHeaders(checkSum, authGroup, authUser))
                        .get(getUrl())
                        .as(typeParameter));
    }

    @SuppressWarnings("unchecked")
    protected void getArray()
    {
        final String checkSum = SignUtils.generateChecksum("GET", getUrl(), null);
        this.setDtoArray(
                (T[]) RestAssured
                        .expect()
                        .statusCode(statusCode)
                        .given()
                        .headers(createHeaders(checkSum, authGroup, authUser))
                        .get(getUrl())
                        .as(typeParameterArray));
    }

    /**
     * Method for using post with the MAST BE endpoints. Pass in an data model object to deserialize.
     *
     * @param body - Object to deserialize and to use as the JSON body
     * @return - RestAssured Response object
     */
    @SuppressWarnings("unchecked")
    protected void post(Object body)
    {
        final String checkSum = SignUtils.generateChecksum("POST", getUrl(), serializeObjectToJSON(body));
        setResponse(
                RestAssured
                        .given()
                        .headers(createHeaders(checkSum, authGroup, authUser))
                        .contentType(ContentType.JSON)
                        .body(body)
                        .post(getUrl()),
                false);
    }

    /**
     * Method for using put with the MAST BE endpoints. Pass in an data model object to deserialize.
     *
     * @param body - Object to deserialize and to use as the JSON body
     * @return - RestAssured Response object
     */
    @SuppressWarnings("unchecked")
    protected void put(Object body)
    {
        final String checkSum = SignUtils.generateChecksum("PUT", getUrl(), serializeObjectToJSON(body));
        setResponse(
                RestAssured
                        .given()
                        .headers(createHeaders(checkSum, authGroup, authUser))
                        .contentType(ContentType.JSON)
                        .body(body)
                        .put(getUrl()),
                false);
    }

    /**
     * Method for using put with the MAST BE endpoints. Pass in an data model object to deserialize. On return, you must
     * be expecting an array response, or this won'putExpectArray work.
     *
     * @param body - Object to deserialize and to use as the JSON body
     * @return - RestAssured Response object
     */
    @SuppressWarnings("unchecked")
    protected void putExpectArray(Object body)
    {
        final String checkSum = SignUtils.generateChecksum("PUT", getUrl(), serializeObjectToJSON(body));
        setResponse(
                RestAssured
                        .given()
                        .headers(createHeaders(checkSum, authGroup, authUser))
                        .contentType(ContentType.JSON)
                        .body(body)
                        .put(getUrl()),
                true);
        verifyArray();
    }

    /**
     * This method is for testing a delete endpoint, for example deleting a case. Pass in a url and a status code to
     * assert.
     */
    @SuppressWarnings("unchecked")
    protected void delete()
    {

        final String checkSum = SignUtils.generateChecksum("DELETE", getUrl(), null);
        RestAssured
                .expect()
                .statusCode(statusCode)
                .given()
                .headers(createHeaders(checkSum, authGroup, authUser))
                .delete(getUrl());
    }

    public T getDto()
    {
        return this.dto;
    }

    private void setDto(T dto)
    {
        this.dto = dto;
    }

    public T[] getDtoArray()
    {
        return this.dtoArray;
    }

    private void setDtoArray(T[] dto)
    {
        this.dtoArray = dto;
    }

    @SuppressWarnings(value = "unchecked")
    private void setResponse(Response response, boolean isArray)
    {
        if (response.getStatusCode() == this.statusCode)
        {
            if (isArray)
                this.setDtoArray((T[]) response.as(typeParameterArray));
            else
                this.setDto(response.as(typeParameter));
            return;
        }
        switch (response.getStatusCode())
        {
            case HttpStatus.SC_BAD_REQUEST:
            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
            case HttpStatus.SC_CONFLICT:
            case HttpStatus.SC_NOT_FOUND:
                ErrorMessageDto errorMessageDto = response.as(ErrorMessageDto.class);
                Assert.fail("Expected status code " + statusCode + ", actual was " + response.getStatusCode() + ".\n" +
                        "MAST BE Message: " + errorMessageDto.getMessage() + "\n" +
                        "MAST BE Detailed Message: " + errorMessageDto.getDetailedMessage());
                break;
            default:
                Assert.fail("Expected status code " + statusCode + ", actual was " + response.getStatusCode() + ".");
                break;
        }
    }

    private String getUrl()
    {
        return BE_PREFIX + endpoint;
    }

    private String serializeObjectToJSON(Object object)
    {
        if (!object.getClass().equals(String.class))
        {
            String bodyAsString = null;
            final ObjectMapper mapper = new ObjectMapper();
            try
            {
                bodyAsString = mapper.writeValueAsString(object);
            }
            catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
            return bodyAsString;
        }
        else
        {
            return (String) object;
        }
    }

    @SuppressWarnings("unchecked")
    protected void verify()
    {
        if (typeParameter.getName().contains("Page"))
        {
            verifyPage();
        }
        else
        {
            verifySingle();
        }
    }

    private void verifyArray()
    {
        for (T t : dtoArray)
        {
            setDto(t);
            verifySingle();
        }
    }

    protected void verifyPage()
    {
        if (getTableMap() == null)
            return;
        HashMap<String, Object> tableMap = getTableMap().getTableMap();
        ResultSet resultSet = getResultSet(null);
        try
        {
            Method method = getDto().getClass().getMethod("getContent");
            ArrayList contentList = (ArrayList) method.invoke(getDto());
            Method sizeMethod = contentList.getClass().getMethod("size");
            int size = (int) sizeMethod.invoke(contentList);
            int i = 0;
            assert_()
                    .withFailureMessage("Results returned from the BE, doesn't match whats in the DB")
                    .that(size)
                    .isEqualTo(DatabaseHelper.getRowCount(getResultSet(null)));
            while (resultSet.next())
            {
                Object content = contentList.get(i);
                pageArrayDto = content;
                verifyResponse(tableMap, resultSet, content);
                i++;
            }
        }
        catch (Exception e)
        {
            Assert.fail("Error during verifying:\n" + e.toString());
        }
    }

    private void verifySingle()
    {
        if (getTableMap() == null)
            return;
        HashMap<String, Object> tableMap = getTableMap().getTableMap();
        ResultSet resultSet = getResultSet(null);
        try
        {
            resultSet.first();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        verifyResponse(tableMap, resultSet, getDto());
    }

    @SuppressWarnings(value = "unchecked")
    private void verifyResponse(HashMap<String, Object> tableMap, ResultSet resultSet, Object values)
    {
        if (values == null)
            return;
        for (Map.Entry<String, Object> entry : tableMap.entrySet())
        {
            try
            {
                Method method = values.getClass().getMethod(entry.getKey());
                Object value = method.invoke(values);
                if (entry.getValue() instanceof String)
                {
                    String tableColumn = (String) entry.getValue();
                    Object dbValue = resultSet.getObject(tableColumn);
                    if (value instanceof Date && dbValue != null)
                    {
                        dbValue = BACKEND_TIME_FORMAT.format(dbValue);
                        value = BACKEND_TIME_FORMAT.format(value);
                    }
                    else if (value instanceof Boolean && dbValue == null)
                    {
                        dbValue = false;
                    }
                    else if (tableColumn.equals("flag_state_id"))
                    {
                        dbValue = resultSet.getString(tableColumn);
                    }
                    assert_()
                            .withFailureMessage("Value (" + entry.getKey() + ") in the backend" +
                                    " didn't match the database (" + tableColumn + ")")
                            .that(value)
                            .isEqualTo(dbValue);
                }
                else if (entry.getValue() instanceof HashMap)
                {
                    if (value instanceof List<?>)
                    {
                        ResultSet nestedResultSet = getResultSet(entry.getKey());
                        if (DatabaseHelper.getRowCount(nestedResultSet) != 0)
                        {
                            nestedResultSet.first();
                            ((List) value).forEach(v ->
                            {
                                verifyResponse(
                                        (HashMap<String, Object>) entry.getValue(),
                                        nestedResultSet,
                                        v);
                                try
                                {
                                    nestedResultSet.next();
                                }
                                catch (SQLException e)
                                {
                                    Assert.fail("Test fail due to SQL error: " + e.toString());
                                }
                            });
                        }
                    }
                    else
                    {
                        verifyResponse((HashMap<String, Object>) entry.getValue(), resultSet, value);
                    }
                }
            }
            catch (Exception e)
            {
                Assert.fail("Error during verifying:\n" + e.toString());
            }
        }
    }

    protected void setEndpoint(String endpoint)
    {
        this.endpoint = endpoint;
    }

    protected String sqlLimitForPage(int page, int size)
    {
        int start = 0;
        if (page != 0)
        {
            start = size * page;
        }
        return " LIMIT " + start + ", " + size;
    }

    protected abstract ResultSet getResultSet(String method);

    protected abstract TableMap getTableMap();

}
