package helper;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PageFactory extends com.frameworkium.core.ui.pages.PageFactory
{

    public static <T extends BasePage<T>> T newInstance(Class<T> clazz)
    {
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        new WebDriverWait(BaseTest.getDriver(), 20).until(
                ExpectedConditions.invisibilityOfElementLocated(By.tagName("spinner"))
        );
        T newPO = com.frameworkium.core.ui.pages.PageFactory.newInstance(clazz, 20);
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        return newPO;
    }

}
