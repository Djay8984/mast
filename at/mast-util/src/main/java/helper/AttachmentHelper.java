package helper;

import static com.frameworkium.core.common.properties.Property.GRID_URL;
import static constant.LloydsRegister.gridFileToUpload;
import static constant.LloydsRegister.fileToUpload;

public class AttachmentHelper
{

    public static String getAttachment()
    {
        return (GRID_URL.isSpecified()) ? gridFileToUpload : fileToUpload.getAbsolutePath();
    }

}
