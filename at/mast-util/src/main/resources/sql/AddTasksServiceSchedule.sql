/*SQL SCRIPT FOR ADDING TASKS TO A JOB SO THEY MAY BE CREDITED USING THE CREDITING SYSTEM

PLEASE ENSURE TO UPDATE THE JOB ID AND ASSET ID AS APPROPRIATE

BEFORE RUNNING THIS SCRIPT YOU SHOULD CREATE A JOB AND SET THE LEAD SURVEYOR AND CONFIRM
THE JOB SCOPE.  USE THE JOB ID FROM THE JOB HEADER TO UPDATE THIS SCRIPT THEN SELECT THE
LIGHTNING BOLT FROM THE MYSQL WORKBENCH BAR ABOVE

AFTER RUNNING THE SCRIPT REFRESH MAST AND THE TASKS SHOULD BE ADDED TO THE CREDITING SECTION*/

SET @my_asset_id = %s; /*ENTER ASSET ID HERE*/;
SET @my_asset_item_id = (SELECT id FROM `mast_db`.`mast_asset_assetitem` WHERE asset_id=@my_asset_id LIMIT 1);
SET @service_instance_id = (SELECT id FROM `mast_db`.`mast_asset_scheduledservice` WHERE asset_id=@my_asset_id LIMIT 1);

INSERT INTO `mast_db`.`mast_asset_workitem`
(reference_code, name, description, long_description, service_code, asset_item_id, work_item_type_id, work_item_action_id,  attribute_mandatory, due_date, notes, codicil_id, attachment_required, item_order, resolution_date, resolution_status_id, class_society_id, assigned_to_id, assigned_date, resolved_by_id, scheduled_service_id, conditional_parent_id, credit_status_id, task_number)
VALUES
('D101', null, null, null, null, @my_asset_item_id, 1, 1, 0, null, null, null, 0, null, null, null, null, 3, null, null, @service_instance_id,  null, 4, '320-AMN575-14');