package helper;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class PageFactory extends com.frameworkium.core.ui.pages.PageFactory
{

    public static <T extends BasePage<T>> T newInstance(Class<T> clazz)
    {
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        new WebDriverWait(BaseTest.getDriver(), 10).until(
                ExpectedConditions.invisibilityOfElementLocated(By.className("spinner")));
        T newPO = com.frameworkium.core.ui.pages.PageFactory.newInstance(clazz);
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        return newPO;
    }

    public static <T extends BasePage<T>> T newInstance(Class<T> clazz, String url, long timeoutInSeconds)
    {
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        new WebDriverWait(BaseTest.getDriver(), 10).until(
                ExpectedConditions.invisibilityOfElementLocated(By.className("spinner")));
        T newPO = com.frameworkium.core.ui.pages.PageFactory.newInstance(clazz, url, timeoutInSeconds);
        BaseTest.getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        return newPO;
    }
}
