package helper;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.WebElement;

public class AppHelper
{
    public static void scrollIntoView(WebElement element) {
        BaseTest.getDriver().executeScript("arguments[0].scrollIntoView(true);", element);
    }
}
