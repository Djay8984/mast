package constant;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ClassDirect
{

    // DateFormat
    public static final SimpleDateFormat BACKEND_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat FRONTEND_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final DateTimeFormatter FRONTEND_TIME_FORMAT_DTS = DateTimeFormatter.ofPattern("dd MMM yyyy");

    // Local Date Object
    public static final LocalDate localDate = LocalDate.now();
}
