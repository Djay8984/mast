package typifiedelement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class ClearableTextBox extends TypifiedElement
{

    private final String CLEAR_BUTTON_XPATH = "./following-sibling::div[@role='button'] | " +
            "./following-sibling::div[contains(@class,'clear-btn')]";

    public ClearableTextBox(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTextBox()
    {
        String tag = this.getWrappedElement().getTagName();

        String xpath;
        if (!tag.equals("input"))
            xpath = "./descendant::input | ./preceding-sibling::input";
        else
            xpath = "self::input";
        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public void clickClear()
    {
        getTextBox().findElement(By.xpath(CLEAR_BUTTON_XPATH)).click();
    }

    @Override
    public void sendKeys(CharSequence... var)
    {
        getTextBox().sendKeys(var);
    }

    @Override
    public String getText()
    {
        return getTextBox().getAttribute("value");
    }
}
