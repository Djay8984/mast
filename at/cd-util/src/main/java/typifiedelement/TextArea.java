package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.*;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class TextArea extends TypifiedElement
{
    private WebDriver driver = BaseTest.getDriver();
    private JavascriptExecutor js = (JavascriptExecutor) driver;

    public TextArea(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTextArea()
    {
        String tag = this.getWrappedElement().getTagName();
        String xpath;

        if (!tag.equals("textarea"))
            xpath = "./textarea";

        else
            xpath = "self::textarea";

        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    public Dimension setSizeByOffset(int x, int y)
    {
        Dimension d = getTextArea().getSize();
        int h = d.height;
        int w = d.width;

        if (x != 0)
        {
            js.executeScript("arguments[0].setAttribute(\"style\",\"width: " + (w + x) + "px\")",
                    getTextArea());
        }
        if (y != 0)
        {
            js.executeScript("arguments[0].setAttribute(\"style\",\"height: " + (y + h) + "px\")",
                    getTextArea());
        }
        return getTextArea().getSize();
    }

    @Override
    public String getText()
    {
        return getTextArea().getAttribute("value");
    }
}
