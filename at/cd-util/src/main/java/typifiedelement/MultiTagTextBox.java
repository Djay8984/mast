package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MultiTagTextBox extends TypifiedElement
{

    private final String INPUT_CSS = "input";
    private final String DROP_DOWN_CSS = "ul";
    private final String OPTIONS_CSS = "[ng-repeat='option in $select.items']";
    private final String TAG_CSS = "[ng-repeat='$item in $select.selected track by $index']";
    private final String TAG_CLOSE_CSS = "[class='close ui-select-match-close']";
    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 10);

    /**
     * Specifies a multi tag textbox
     *
     * @param wrappedElement {@code WebElement} representing multi tag textbox.
     */
    public MultiTagTextBox(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    /**
     * Returns the multi tag textbox at its root node
     *
     * @return {@code WebElements} representing a multi tag textbox.
     */
    private WebElement getRootNode()
    {

        String tag = this.getWrappedElement().getTagName();
        String className = this.getWrappedElement().getAttribute("class");

        String xpath;

        if (tag.equals("div") && className.equals("container"))
            xpath = "./parent::input-tags";
        else if (tag.equals("div") && className.contains("ui-select-container"))
            xpath = "./parent::div/parent::input-tags";
        else if (tag.equals("div"))
            xpath = "./ancestor::input-tags";
        else if (tag.equals("input") && className.contains("ui-select-search"))
            xpath = "./parent::div/parent::div[contains(@class,'ui-select-container')" +
                    "]/parent::div[@class='container']/parent::input-tags";
        else
            xpath = "self::input-tags";

        return this.getWrappedElement().findElement(By.xpath(xpath));
    }

    /**
     * Returns the multi tag textbox dropdown
     *
     * @return {@code WebElements} representing a multi tag textbox's dropdown.
     */
    private WebElement getDropDown()
    {
        WebElement e = getRootNode().findElement(By.cssSelector(DROP_DOWN_CSS));
        wait.until(ExpectedConditions.attributeToBe(e, "aria-hidden",
                "false"));
        return e;
    }

    /**
     * Indicates the display status of the dropdown
     *
     * @return {@code boolean}
     */
    public boolean isDropdownVisible()
    {
        return getDropDown().isDisplayed();
    }

    /**
     * Returns all options that belongs to this multi tag textbox
     *
     * @return A list of {@code WebElements} representing the options
     */
    public List<WebElement> getOptions()
    {
        // this is a workaround for the worse element staleness offender !!!!
        List<WebElement> options = getDropDown().findElements(By.cssSelector(OPTIONS_CSS));
        try
        {
            // parallelstream waits will allow individual waits for all options all at once
            // It's hacky, but clever hacky...and also I have no other choice
            options.parallelStream().forEach(s -> wait.withTimeout(200, TimeUnit.MILLISECONDS).until
                    (ExpectedConditions.stalenessOf(s)));
        }
        catch (StaleElementReferenceException | TimeoutException e)
        {
            options = getDropDown().findElements(By.cssSelector(OPTIONS_CSS));
        }
        return options;
    }

    /**
     * Select the first option that displays the text that matches the given text argument
     *
     * @param text
     */
    public void selectOptionByVisibleText(String text)
    {
        WebElement e = getOptions().stream()
                .filter(o -> o.getText().equals(text))
                .findFirst()
                .orElseThrow(java.util.NoSuchElementException::new);
        e.click();
    }

    /**
     * Select the option at the given index
     *
     * @param index
     */
    public void selectOptionByIndex(int index)
    {
        if (index < this.getOptions().size())
        {
            getOptions().get(index).click();
        }
        else
            throw new NoSuchElementException("index out of bounds, no options were selected");
    }

    /**
     * Returns a list of tags that are associated with the multi tag textbox
     *
     * @return
     */
    public List<WebElement> getTags()
    {
        return getRootNode().findElements(By.cssSelector(TAG_CSS));
    }

    /**
     * Deselect the first tag that displays the text that matches the given text argument
     *
     * @param text
     */
    public void deselectTagByVisibleText(String text, Operation operation)
    {
        if (operation.equals(Operation.FIRST))
        {
            getTags().stream()
                    .filter(o -> o.getText()
                            .replaceFirst("^\\s×\\s", "")
                            .equals(text))
                    .findFirst()
                    .get()
                    .findElement(By.cssSelector(TAG_CLOSE_CSS))
                    .click();
        }
        else if (operation.equals(Operation.ALL))
        {
            getTags().stream()
                    .filter(o -> o.getText().replaceFirst("^\\s×\\s", "").equals(text))
                    .forEach(t -> t.findElement(By.cssSelector(TAG_CLOSE_CSS))
                            .click());
        }
    }

    public void deselectTagByVisibleText(String text)
    {
        deselectTagByVisibleText(text, Operation.FIRST);
    }

    /**
     * Deselect the first tag at the given index
     *
     * @param index
     */
    public void deselectTagByIndex(int index)
    {
        if (index < this.getTags().size())
        {
            getTags().get(index).findElement(By.cssSelector(TAG_CLOSE_CSS)).click();
        }
        else
            throw new NoSuchElementException("index out of bounds, no options were selected");
    }

    @Override
    public void sendKeys(CharSequence... charSequences)
    {
        getRootNode().findElement(By.cssSelector(INPUT_CSS)).sendKeys(charSequences);
    }

    @Override
    public void clear()
    {
        getRootNode().findElement(By.cssSelector(INPUT_CSS)).clear();
    }

    @Override
    public String getText()
    {
        return getRootNode().findElement(By.cssSelector(INPUT_CSS)).getAttribute("value");
    }

    /**
     * Returns the error message associated with the multi tag textbox
     *
     * @return error message as a {@code String}
     */
    private String getErrorMessage()
    {
        return getRootNode()
                .findElement(By.cssSelector("[data-ng-messages='vm.form[vm.name].$error']"))
                .getText();
    }

    public enum Operation
    {
        FIRST, ALL
    }
}


