package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

public class Dropdown extends TypifiedElement
{

    private final String OPTION_CSSSELECTOR = "[class*='md-select-menu-container'][aria-hidden='false'] md-option";
    private WebDriver driver = BaseTest.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public Dropdown(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getSelect()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::md-input-container"));
    }

    public List<WebElement> getOptions()
    {
        getSelect().findElement(By.cssSelector("md-select")).click();
        return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
                By.cssSelector(OPTION_CSSSELECTOR)));
    }

    public void closeOptionList()
    {
        List<WebElement> options = driver.findElements(By.cssSelector(OPTION_CSSSELECTOR));
        options.get(0).sendKeys(Keys.ESCAPE);
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public void selectByText(String text)
    {
        List<WebElement> options = getOptions();
        options.stream()
                .filter(e -> Objects.equals(e.getText(), text)).findFirst()
                .orElseThrow(NoSuchElementException::new)
                .click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public void selectByIndex(int index)
    {
        List<WebElement> options = getOptions();
        options.get(index).click();
        wait.until(ExpectedConditions.invisibilityOfAllElements(options));
    }

    public WebElement getSelectedOption()
    {
        return getSelect().findElement(By.cssSelector("md-select-value"));
    }
}
