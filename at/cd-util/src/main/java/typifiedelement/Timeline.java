package typifiedelement;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Timeline extends TypifiedElement
{
    private static final String STARTDATE_CSSSELECTOR = "div[class='noUi-handle'][data-handle='0'] div";
    private static final String ENDDATE_CSSSELECTOR = "div[class='noUi-handle'][data-handle='1'] div";
    private static final String TIMEAXIS_CSSSELECTOR = "div.noUi-pips";
    private static final String TICKMARKS_CSSSELECTOR = "div.noUi-pips div[style^='left: ']:not([style^='left: -'])";
    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy");
    private WebDriver driver = BaseTest.getDriver();

    public Timeline(WebElement wrappedElement)
    {
        super(wrappedElement);
    }

    private WebElement getTimeline()
    {
        return this.getWrappedElement().findElement(By.xpath("./ancestor-or-self::timeline"));
    }

    public List<WebElement> getSelectedDates()
    {
        WebElement startDateThumb = getTimeline().findElement(By.cssSelector(STARTDATE_CSSSELECTOR));
        WebElement endDateThumb = getTimeline().findElement(By.cssSelector(ENDDATE_CSSSELECTOR));
        List<WebElement> selectedDated = new ArrayList<>();
        selectedDated.add(startDateThumb);
        selectedDated.add(endDateThumb);
        return selectedDated;
    }

    public void selectStartDate(String startDateString)
    {
        WebElement endDateThumb = getTimeline().findElement(By.cssSelector(ENDDATE_CSSSELECTOR));
        selectDateRange(startDateString, endDateThumb.getText());
    }

    public void selectEndDate(String endDateString)
    {
        WebElement startDateThumb = getTimeline().findElement(By.cssSelector(STARTDATE_CSSSELECTOR));
        selectDateRange(startDateThumb.getText(), endDateString);
    }

    public void selectDateRange(String startDateString, String endDateString)
    {
        WebElement startDateThumb = getTimeline().findElement(By.cssSelector(STARTDATE_CSSSELECTOR));
        WebElement endDateThumb = getTimeline().findElement(By.cssSelector(ENDDATE_CSSSELECTOR));
        LocalDate startDateToSelect = convertStringToDate(startDateString);
        LocalDate endDateToSelect = convertStringToDate(endDateString);
        LocalDate selectedEndDate = convertStringToDate(endDateThumb.getText());
        if (!startDateToSelect.plusMonths(1).isAfter(endDateToSelect))
        {
            if (startDateToSelect.isBefore(selectedEndDate))
            {
                selectDate(startDateThumb, startDateString);
                selectDate(endDateThumb, endDateString);
            }
            else
            {
                selectDate(endDateThumb, endDateString);
                selectDate(startDateThumb, startDateString);
            }
        }
        else
        {
            throw new IllegalArgumentException(String.format("Start date '%s' is either same or after the end date '%s'.",
                    startDateString, endDateString));
        }
    }

    private LocalDate convertStringToDate(String dateString)
    {
        return LocalDate.parse(String.format("01 %s", dateString), DATETIME_FORMATTER);
    }

    private void selectDate(WebElement thumb, String dateString)
    {
        LocalDate selectedDate = convertStringToDate(thumb.getText());
        LocalDate dateToSelect = convertStringToDate(dateString);
        int offset = calculateThumbSlideOffset();
        if (!selectedDate.isEqual(dateToSelect))
        {
            offset = (selectedDate.isBefore(dateToSelect)) ? offset : -offset;
            do
            {
                if (!isSaveToSlide(thumb, offset))
                {
                    throw new IllegalArgumentException(String.format("Date '%s' is outside the available scale.", dateString));
                }
                simulateMouseMove(thumb, offset, 0);
            }
            while (!convertStringToDate(thumb.getText()).isEqual(dateToSelect));
        }
    }

    private boolean isSaveToSlide(WebElement thumb, int offset)
    {
        WebElement dateRange = thumb.findElement(By.xpath("./ancestor::div[@class='noUi-origin']"));
        String style = dateRange.getAttribute("style");
        return !((Objects.equals(style, "left: 0%;") && offset < 0) || (Objects.equals(style, "left: 100%;") && offset > 0));
    }

    private int calculateThumbSlideOffset()
    {
        WebElement timeAxis = getTimeline().findElement(By.cssSelector(TIMEAXIS_CSSSELECTOR));
        List<WebElement> tickMarks = timeAxis.findElements(By.cssSelector(TICKMARKS_CSSSELECTOR));
        return timeAxis.getSize().getWidth() / (tickMarks.size() - 1);
    }

    /*
    * Workaround to slide the thumbs because Actions builder didn't work.
    * Source code originated from http://stackoverflow.com/questions/19384710/javascript-workaround-for-drag-and-drop-in-selenium-webdriver
    */
    private void simulateMouseMove(WebElement element, int xOffset, int yOffset)
    {
        ((JavascriptExecutor) driver).executeScript(
                "function simulate(f,c,d,e){var b,a=null;for(b in eventMatchers)if(eventMatchers[b].test(c)){a=b;break}if(!a)return!1;document.createEvent?" +
                        "(b=document.createEvent(a),a==\"HTMLEvents\"?b.initEvent(c,!0,!0):b.initMouseEvent(c,!0,!0,document.defaultView,0,d,e,d,e,!1,!1,!1,!1,0,null),f.dispatchEvent(b)):" +
                        "(a=document.createEventObject(),a.detail=0,a.screenX=d,a.screenY=e,a.clientX=d,a.clientY=e,a.ctrlKey=!1,a.altKey=!1,a.shiftKey=!1,a.metaKey=!1,a.button=1,f.fireEvent(\"on\"+c,a));return!0} " +
                        "var eventMatchers={HTMLEvents:/^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/," +
                        "MouseEvents:/^(?:click|dblclick|mouse(?:down|up|over|move|out))$/};" +
                        "simulate(arguments[0],\"mousedown\",0,0); " +
                        "simulate(arguments[0],\"mousemove\",arguments[1],arguments[2]); " +
                        "simulate(arguments[0],\"mouseup\",arguments[1],arguments[2]);", element, Integer.toString(xOffset),
                Integer.toString(yOffset));
    }
}
