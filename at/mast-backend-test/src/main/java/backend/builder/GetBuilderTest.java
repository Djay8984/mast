package backend.builder;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.builder.Builder;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetBuilderTest
{

    @DataProvider(name = "Builders")
    public Object[][] builders()
    {
        return new Object[][]{
                {1}, {101}, {200}
        };
    }

    @Issue("LRD-551")
    @Test(
            description = "Uses RestAssured to test the builder endpoint can retrieve a single builder",
            dataProvider = "Builders",
            enabled = false
    )
    public void getBuilderTest(int id)
    {
        new Builder(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-551")
    public void negativeGetBuilderTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.BUILDER, id, statusCode, message, Request.GET);
    }

}
