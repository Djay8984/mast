package backend.builder;

import helper.DataProviders;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetBuildersTest
{

    @Test(
            description = "Uses RestAssured to test the get builders endpoint",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-225")
    public void getBuildersTest(int page, int size)
    {
    }

}
