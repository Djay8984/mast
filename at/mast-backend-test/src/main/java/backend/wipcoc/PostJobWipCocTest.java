package backend.wipcoc;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipCoc;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostJobWipCocTest
{

    @DataProvider(name = "Jobs")
    public Object[][] assets()
    {
        return new Object[][]{
                {3}, {5}, {7}
        };
    }

    @Test(description = "BE: POST job/{id}/wip-coc, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1010")
    public void postJobWipCocTest(int jobId)
    {
        CoCDto wipCoc = new CoCDto();
        LinkResource defect = new LinkResource();
        defect.setId((long) 1);
        LinkResource asset = new LinkResource();
        asset.setId((long) 12);
        LinkResource status = new LinkResource();
        status.setId((long) 1);
        LinkResource confidentiality = new LinkResource();
        confidentiality.setId((long) 1);
        LinkResource job = new LinkResource();
        job.setId((long) jobId);

        wipCoc.setAsset(asset);
        wipCoc.setDefect(defect);
        wipCoc.setImposedDate(Date.valueOf("2016-02-16"));
        wipCoc.setDueDate(Date.valueOf("2016-02-16"));
        wipCoc.setDescription("This is an automated wip coc description");
        wipCoc.setInheritedFlag(false);
        wipCoc.setStatus(status);
        wipCoc.setRequireApproval(true);
        wipCoc.setTitle("Automated Title");
        wipCoc.setConfidentialityType(confidentiality);
        wipCoc.setJob(job);
        wipCoc.setJobScopeConfirmed(true);
        wipCoc.setActionTaken("An automated action was taken");

        new WipCoc(wipCoc, jobId);
    }

    @Test(description = "BE: POST job/{id}/wip-coc, Story 6.11",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1010")
    public void negativePostJobWipCocTest(String jobId, int statusCode, String message)
    {
        CoCDto wipCoc = new WipCoc(1, 1).getDto();
        wipCoc.setId(null);

        new ExpectedError(Endpoint.WIP_COC,
                wipCoc,
                new String[]{jobId, ""},
                Request.POST,
                statusCode,
                message);
    }
}
