package backend.wipcoc;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipCocPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobWipCocsTest
{

    @DataProvider(name = "JobsPages")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3, 1},
                {4, 2},
                {5, 2}
        };
    }

    @Test(description = "BE: GET job/{id}/wip-coc?defectId={id}, Story 6.11",
            dataProvider = "JobsPages")
    @Issue("LRT-1009")
    public void getWipCocsTest(int jobId, int defectId)
    {
        new WipCocPage(jobId, defectId);
    }

    @Test(description = "BE: GET job/{id}/wip-coc?defectId={id}, Story 6.11",
            dataProvider = "NegativeTwoParameterPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1009")
    public void negativeGetJobWipAssetCocsTest(String jobId, String defectId, int statusCode, String message)
    {
        new ExpectedError(Endpoint.WIP_COC_DEFECT, new String[]{jobId, defectId}, statusCode, message, Request.GET);
    }
}
