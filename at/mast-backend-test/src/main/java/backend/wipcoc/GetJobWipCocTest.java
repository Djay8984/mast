package backend.wipcoc;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipCoc;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobWipCocTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3, 3}, {4, 5}, {7, 9}
        };
    }

    @Test(description = "BE: GET job/{id}/wipcoc/{id}, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1011")
    //todo - do not remove this test, wipcoc has changed and not entirely removed - fix later
    public void getJobWipCocTest(int jobId, int wipCocId)
    {
        new WipCoc(jobId, wipCocId);
    }

    @Test(description = "BE: GET job/{id}/wipcoc/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1011")
    public void negativeGetJobWipCocTest(String jobId, String wipCocId, int statusCode, String message)
    {
        if (wipCocId.equals("0") || wipCocId.equals("10000000000"))
        {
            statusCode = 404;
            message = "Entity not found for identifier: " + wipCocId + ".";
        }
        new ExpectedError(Endpoint.WIP_COC, new String[]{jobId, wipCocId}, statusCode, message, Request.GET);
    }
}
