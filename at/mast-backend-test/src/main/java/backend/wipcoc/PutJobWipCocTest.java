package backend.wipcoc;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipCoc;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutJobWipCocTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3, 7}, {5, 6}, {6, 4}
        };
    }

    @Test(description = "BE: PUT job/{id}/wip-coc/{id}, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1012")

    public void putJobWipCocTest(int jobId, int wipCocId)
    {
        WipCoc wipCoc = new WipCoc(jobId, wipCocId);

        wipCoc.getDto().setActionTaken("An action was taken");
        wipCoc.getDto().setDueDate(Date.valueOf("2016-02-16"));
        wipCoc.getDto().setAffectedItems("this was the affected item");

        LrEmployeeDto employee = new LrEmployeeDto();
        employee.setId(1L);
        wipCoc.getDto().setEmployee(employee);

        new WipCoc(wipCoc.getDto(), jobId, wipCocId);
    }

    @Test(description = "BE: PUT job/{id}/wip-coc/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1012")
    public void negativePutJobWipCocTest(String jobId, String wipCocId, int statusCode, String errorMessage)
    {
        CoCDto wipCoc = new WipCoc(1, 1).getDto();
        wipCoc.setId(Long.valueOf(wipCocId));

        new ExpectedError(Endpoint.WIP_COC,
                wipCoc,
                new String[]{jobId, wipCocId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
