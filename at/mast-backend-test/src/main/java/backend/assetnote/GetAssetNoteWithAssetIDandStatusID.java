package backend.assetnote;

import model.asset.AssetNoteAssetIDStatusID;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetNoteWithAssetIDandStatusID
{

    @DataProvider(name = "AssetsWithAssetIDAndStatusID")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1},
                {1, 2},
                {1, 3}
        };
    }

    @Test(description = "BE: GET /asset/assetID/asset-note?statusId= ",
            dataProvider = "AssetsWithAssetIDAndStatusID")
    @Issue("LRT-1301")
    public void getAssetNoteWithAssetIDAndStatusID(int assetId, int statusId)
    {
        new AssetNoteAssetIDStatusID(assetId, statusId);
    }

}
