package backend.assetnote;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetNote;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostAssetNoteTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1}, {409}, {6}
        };
    }

    @Test(description = "BE: /asset/{id}/asset-note, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1034")
    public void postAssetNoteTest(int assetId)
    {
        AssetNoteDto assetNoteDto = new AssetNoteDto();
        LinkResource asset = new LinkResource();
        asset.setId((long) assetId);

        ReferenceDataDto status = new ReferenceDataDto();
        status.setId((long) 1);

        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        assetNoteDto.setAsset(asset);
        assetNoteDto.setImposedDate(Date.valueOf("2019-02-16"));
        assetNoteDto.setDueDate(Date.valueOf("2016-02-16"));
        assetNoteDto.setDescription("This is an automated asset note description");
        assetNoteDto.setActionTaken("An automated action was taken");
        assetNoteDto.setInheritedFlag(false);
        assetNoteDto.setStatus(linkResource);
        assetNoteDto.setTitle("Automated Title");
        assetNoteDto.setCategory(linkResource);
        assetNoteDto.setConfidentialityType(linkResource);

        new AssetNote(assetNoteDto, assetId);
    }

    @Test(description = "BE: /asset/{id}/asset-note, Story 6.11",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1034")
    public void negativePostAssetNoteTest(String assetId, int statusCode, String message)
    {
        AssetNoteDto assetNoteDto = new AssetNote(1, 17).getDto();
        assetNoteDto.setId(null);

        new ExpectedError(Endpoint.ASSET_NOTE,
                assetNoteDto,
                new String[]{assetId, ""},
                Request.POST,
                statusCode,
                message);
    }
}
