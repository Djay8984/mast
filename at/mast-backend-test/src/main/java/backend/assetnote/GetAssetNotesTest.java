package backend.assetnote;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetNotePage;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetNotesTest
{

    @DataProvider(name = "AssetsPages")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 0, 10},
                {419, 0, 10},
                {401, 0, 10}
        };
    }

    @Test(description = "BE: /asset/{id}/asset-note/?page={id}&size={id}, Story 6.11",
            dataProvider = "AssetsPages")
    @Issue("LRT-1036")
    public void getAssetNotesTest(int assetId, int page, int size)
    {
        new AssetNotePage(assetId, page, size);
    }

    @Test(description = "BE: /asset/{id}/asset-note/?page={id}&size={id}, Story 6.11",
            dataProvider = "NegativeForPagesWithId",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1036")
    public void negativeGetAssetNotesTest(String assetId, String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_NOTE_PAGE, new String[]{assetId, page, size}, statusCode, message, Request.GET);
    }
}

