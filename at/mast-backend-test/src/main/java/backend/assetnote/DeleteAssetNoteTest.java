package backend.assetnote;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import constant.LloydsRegister;
import helper.TestDataHelper;
import model.asset.AssetNote;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import static com.google.common.truth.Truth.assert_;

public class DeleteAssetNoteTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1}, {501}, {510}
        };
    }

    @Test(description = "BE: delete /asset/x/assetnote/y, Story 9.50",
            dataProvider = "Assets")
    @Issue("LRT-1941")
    public void deleteAssetNoteTest(int assetId)
    {

        AssetNoteDto assetNoteDto = new AssetNoteDto();
        LinkResource asset = new LinkResource();
        asset.setId((long) assetId);
        ReferenceDataDto status = new ReferenceDataDto();
        status.setId((long) 1);
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        assetNoteDto.setAsset(asset);

        java.util.Date utilTodayDate = new java.util.Date();
        try
        {
            utilTodayDate = LloydsRegister.FRONTEND_TIME_FORMAT.parse(TestDataHelper.getTodayDate());
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        java.sql.Date sqlTodayDate = new java.sql.Date(utilTodayDate.getTime());

        assetNoteDto.setImposedDate(sqlTodayDate);
        assetNoteDto.setDueDate(sqlTodayDate);

        assetNoteDto.setDescription("This is an automated asset note description");
        assetNoteDto.setActionTaken("An automated action was taken");
        assetNoteDto.setInheritedFlag(false);
        assetNoteDto.setStatus(linkResource);
        assetNoteDto.setCategory(linkResource);
        assetNoteDto.setConfidentialityType(linkResource);
        assetNoteDto.setTitle("Automation Test Coffee smell");

        AssetNote assetNote = new AssetNote(assetNoteDto, assetId);

        assetNote.deleteAssetNote(assetId, Long.toString(assetNote.getDto().getId()));

        try
        {
            ResultSet rs = assetNote.getResultSet(null);
            rs.next();

            assert_()
                    .withFailureMessage("Assetnote ID :" + Long.toString(assetNote.getDto().getId()) + "has not been updated as deleted")
                    .that(rs.getObject("deleted").toString()).isEqualTo("true");

        }
        catch (SQLException e)
        {
            Assert.fail("Test fail due to SQL error: " + e.toString());
        }

    }

}
