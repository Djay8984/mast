package backend.assetnote;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetNote;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetNoteTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 17}, {417, 18}, {419, 19}
        };
    }

    @Test(description = "BE: /asset/{id}/asset-note/{id}, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1018")
    public void getAssetNoteTest(int assetId, int assetNoteId)
    {
        new AssetNote(assetId, assetNoteId);
    }

    @Test(description = "BE: /asset/{id}/asset-note/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1018")
    public void negativeGetAssetNoteTest(String assetId, String assetNoteId, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_NOTE, new String[]{assetId, assetNoteId}, statusCode, message, Request.GET);
    }
}

