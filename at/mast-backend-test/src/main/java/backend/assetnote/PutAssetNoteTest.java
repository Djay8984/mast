package backend.assetnote;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetNote;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutAssetNoteTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 17}, {417, 18}, {419, 19}
        };
    }

    @Test(description = "BE: /asset/{id}/asset-note/{id}, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1024")
    public void putAssetNoteTest(int assetId, int assetNoteId)
    {
        AssetNoteDto assetNote = new AssetNote(assetId, assetNoteId).getDto();
        LinkResource asset = new LinkResource();
        asset.setId(Long.valueOf(assetId));

        assetNote.setAsset(asset);
        assetNote.setActionTaken("An action was taken");
        assetNote.setDueDate(Date.valueOf("2016-02-16"));
        assetNote.setAffectedItems("this was the affected item");

        new AssetNote(assetNote, assetId, assetNoteId);
    }

    @Test(description = "BE: /asset/{id}/asset-note/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1024")
    public void negativePutAssetNoteTest(String assetId, String assetNoteId, int statusCode, String errorMessage)
    {
        AssetNoteDto assetNoteDto = new AssetNote(1, 17).getDto();
        assetNoteDto.setId(Long.valueOf(assetNoteId));

        new ExpectedError(Endpoint.ASSET_NOTE,
                assetNoteDto,
                new String[]{assetId, assetNoteId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
