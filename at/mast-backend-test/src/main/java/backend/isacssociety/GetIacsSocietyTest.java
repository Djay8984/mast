package backend.isacssociety;

import constant.Endpoint;
import constant.ReferenceTable;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.referencedata.ReferenceData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetIacsSocietyTest
{

    @DataProvider(name = "Iacs")
    public static Object[][] iacs()
    {
        return new Object[][]{
                {1},
                {4},
                {10},
                {15},
                {22},
                {28},
                };
    }

    @Issue("LRD-493")
    @Test(
            description = "MAST BE: GET /iacs-society/{id}",
            dataProvider = "Iacs",
            enabled = false
    )
    public void getIacsSocietyTest(int id)
    {
        new ReferenceData(Endpoint.IACS_SOCIETY, ReferenceTable.IACS_SOCIETY, id);
    }

    @Issue("LRD-493")
    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    public void negativeGetIacsSocietyTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.IACS_SOCIETY, id, statusCode, message, Request.GET);
    }
}
