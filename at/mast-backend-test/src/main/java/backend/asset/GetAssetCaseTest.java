package backend.asset;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCase;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetCaseTest
{

    @DataProvider(name = "AssetCase")
    public Object[][] assets()
    {
        return new Object[][]{
                {1},
                {4},
                {501}
        };
    }

    @Test(description = "BE: GET asset/x/case",
            dataProvider = "AssetCase"
    )
    @Issue("LRT-1778")
    public void getAssetCaseTest(int assetId)
    {
        new AssetCase(assetId);
    }

    @Test(description = "BE: GET asset/x/case",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1778")
    public void negativeGetAssetCaseTest(String asset_ID, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_CASE, asset_ID, statusCode, message, Request.GET);
    }
}
