package backend.asset;

import model.asset.AssetModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetModelTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1}, {2}, {428}
        };
    }

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "Assets"
    )
    @Issue("LRD-225")
    public void getAssetModelTest(int id)
    {
        new AssetModel(id);
    }

}
