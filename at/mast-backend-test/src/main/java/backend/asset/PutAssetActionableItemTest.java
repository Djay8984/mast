package backend.asset;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import model.asset.AssetActionableItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Date;

public class PutAssetActionableItemTest
{

    @DataProvider(name = "ActionableItem")
    private Object[][] assets()
    {
        return new Object[][]{
                {501, 30}, {501, 31}, {501, 35}
        };
    }

    @Issue("LRT-1529")
    @Test(description = "BE: PUT asset/x/actionableItem/y",
            dataProvider = "ActionableItem")
    public void putAssetActionableItemTest(int assetId, int actionableItemId)
    {

        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        ActionableItemDto actionableItemDto = new ActionableItemDto();
        actionableItemDto.setTitle("Automated Actionable Item");
        actionableItemDto.setDescription("Automated Actionable Item Description");
        actionableItemDto.setRequireApproval(true);
        actionableItemDto.setImposedDate(new Date());
        actionableItemDto.setDueDate(new Date());
        actionableItemDto.setCategory(linkResource);
        actionableItemDto.setConfidentialityType(linkResource);
        actionableItemDto.setJob(linkResource);
        actionableItemDto.setDefect(linkResource);
        actionableItemDto.setEmployee(linkResource);
        actionableItemDto.setId((long) actionableItemId);
        actionableItemDto.setStatus(linkResource);

        new AssetActionableItem(actionableItemDto, assetId, actionableItemId);
    }

}
