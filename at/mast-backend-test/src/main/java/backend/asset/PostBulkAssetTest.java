package backend.asset;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import helper.DatabaseHelper;
import helper.TestDataHelper;
import model.asset.Asset;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class PostBulkAssetTest
{

    private static final String PATH_ROOT = "C:\\asset\\";
    private static final String DONE = "done\\";
    private static final String FAILURE = "failure\\";
    private static final String SUCCESS = "success\\";
    private static final String RESOURCES = "src\\main\\resources\\";
    private static final String NEW_LINE = System.getProperty("line.separator");

    private List<String> diffs;

    @Test(description = "BE: /asset/query, Story 7.38", enabled = false)
    @Issue("LRT-1030")
    public void postAssetQueryTest() throws IOException
    {
        int assetCount = countAssets();

        String fileName = "BulkAssetTest" + UUID.randomUUID() + ".tar.gz";

        Path source = Paths.get("src\\main\\resources\\BulkAssetTest.tar.gz");
        File targetFile = new File(PATH_ROOT + fileName);
        if (!targetFile.createNewFile())
        {
            throw new IOException("unable to create target file: " + targetFile.getName());
        }
        FileOutputStream target = new FileOutputStream(targetFile);
        Files.copy(source, target);
        target.close();

        File doneFile = new File(PATH_ROOT + DONE + fileName);

        Long time = new Date().getTime();
        while (true)
        {
            boolean successFileExists = false;
            boolean failureFileExists = false;
            String[] successFiles = new File(PATH_ROOT + DONE + SUCCESS).list();
            String[] failureFiles = new File(PATH_ROOT + DONE + FAILURE).list();
            for (String name : successFiles)
            {
                if (name.contains(fileName))
                {
                    successFileExists = true;
                }
            }
            for (String name : failureFiles)
            {
                if (name.contains(fileName))
                {
                    failureFileExists = true;
                }
            }
            if (doneFile.exists() && successFileExists && failureFileExists)
            {
                break;
            }
            Long newTime = new Date().getTime();
            if (newTime - time > 100000)
            {
                break;
            }
        }

        int expectedAssetCount = assetCount + 10;
        int newAssetCount = countAssets();
        assertEquals("Assets not loaded", expectedAssetCount, newAssetCount);
        assertFalse("File still exists in c:/asset.", targetFile.exists());
        assertTrue("File not in done folder.", doneFile.exists());
        diffs = new ArrayList<>();
        boolean verifySuccess = verifyFile(fileName, SUCCESS);
        assertTrue("Success file not as expected." + convertToString(diffs), verifySuccess);
        diffs = new ArrayList<>();
        boolean verifyFailure = verifyFile(fileName, FAILURE);
        assertTrue("Failure file not as expected." + convertToString(diffs), verifyFailure);
    }

    /**
     * Create a test data file with failure records included.
     */
    @BeforeMethod(alwaysRun = true)
    public void createTestDataFile() throws IOException, SQLException
    {
        File output = new File("src\\main\\resources\\output.json");
        File outputWithHeader = new File("src\\main\\resources\\outputHead.json");
        List<AssetLightDto> assetDtos = new ArrayList<>();
        String stringHeader = "{\"assets\":";
        String stringFooter = "}";
        LinkResource nonExistentIMO = new LinkResource();
        nonExistentIMO.setId(1111111L);
        LinkResource duplicateIMO = new LinkResource();
        duplicateIMO.setId(1234566L);
        LinkResource validIMO = new LinkResource();
        validIMO.setId(7654321L);

        for (int i = 12; i < 27; i++)
        {
            AssetLightDto assetDto = new Asset(i).getDto();
            assetDto.setId(null);
            // Include 10 valid assets in the test file.
            assetDto.setIhsAsset(null);
            if (i == 22)
            {
                // Include an asset with an IMO that doesn't exist in IHS - non existent IMO
                assetDto.setIhsAsset(nonExistentIMO);
                assetDto.setBuilder(null);
                assetDto.setYardNumber(null);
            }
            else if (i == 23)
            {
                // Include two assets with the same IMO - Duplicate IMO
                assetDto.setIhsAsset(duplicateIMO);
                assetDto.setBuilder(null);
                assetDto.setYardNumber(null);
                assetDtos.add(assetDto);
                AssetLightDto assetDto2 = new Asset(i).getDto();
                assetDto2.setIhsAsset(duplicateIMO);
                assetDto2.setBuilder(null);
                assetDto2.setYardNumber(null);
                assetDtos.add(assetDto2);
                continue;
            }
            else if (i == 24)
            {
                // Include two assets with the same builder and yard number - Duplicate Builder and Yard
                assetDtos.add(assetDto);
                assetDtos.add(assetDto);
                continue;
            }
            else if (i == 25)
            {
                // Include an asset with no IMO or builder - Invalid unique identifiers
                assetDto.setBuilder(null);
            }
            else if (i == 26)
            {
                // Include an asset with both IMO and Builder - Invalid unique identifiers
                assetDto.setIhsAsset(validIMO);
            }
            int builderId = TestDataHelper.getBuilderId(assetDto.getBuilder());
            if (builderId != 0)
            {
                int maxYardNo = getMaxYardNoForBuilder(builderId);
                maxYardNo++;
                assetDto.setYardNumber("" + maxYardNo);
            }
            assetDtos.add(assetDto);
        }
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();

        FileOutputStream fileOutputStream = new FileOutputStream(output);
        FileInputStream is = new FileInputStream(output);
        FileOutputStream fos = new FileOutputStream(outputWithHeader);

        writer.writeValue(fileOutputStream, assetDtos);
        fileOutputStream.close();

        fos.write(stringHeader.getBytes());
        byte[] bytes = new byte[1024];
        int read = is.read(bytes);
        while (read > 0)
        {
            fos.write(bytes, 0, read);
            read = is.read(bytes);
        }
        fos.write(stringFooter.getBytes());
        output.deleteOnExit();

    }

    /**
     * verify file identified by supplied filename against appropriate success or failure file.
     *
     * @param filename name of actual file
     * @param fileType success or failure
     * @return true if actual file matches appropriate success or failure file
     * @throws IOException on exception
     */
    private boolean verifyFile(final String filename, final String fileType) throws IOException
    {
        boolean verified = false;
        String directoryName = PATH_ROOT + DONE + fileType;
        File directory = new File(directoryName);
        String expectedFileName;
        switch (fileType)
        {
            case SUCCESS:
                expectedFileName = "expected_succeeded.txt";
                break;
            case FAILURE:
                expectedFileName = "expected_failed.txt";
                break;
            default:
                throw new IllegalArgumentException("Filetype must be success or failure");
        }
        File expected = new File(RESOURCES + expectedFileName);
        String[] files = directory.list();
        for (String name : files)
        {
            if (name.contains(filename))
            {
                File actual = new File(directoryName + name);
                verified = performComparison(actual, expected);
            }
        }
        return verified;
    }

    /**
     * compares expected file with actual file.
     *
     * @param actual   actual file output
     * @param expected expected sample file
     * @return true if actual matches expected
     * @throws IOException on exception
     */
    private boolean performComparison(final File actual, final File expected) throws IOException
    {
        boolean result = true;

        LineNumberReader expectedReader = new LineNumberReader(new FileReader(expected));
        LineNumberReader actualReader = new LineNumberReader(new FileReader(actual));
        String expectedLine = expectedReader.readLine();
        String actualLine = actualReader.readLine();
        while (expectedLine != null && actualLine != null)
        {
            expectedLine = expectedLine.substring(expectedLine.indexOf("\t"));
            actualLine = actualLine.substring(actualLine.indexOf("\t"));
            if (!expectedLine.equals(actualLine))
            {
                diffs.add(expected.getName() + " differs from " + actual.getName() + " at line "
                        + actualReader.getLineNumber() + ": " + NEW_LINE + "expected: " + expectedLine
                        + NEW_LINE + " actual: " + actualLine);
                result = false;
            }
            expectedLine = expectedReader.readLine();
            actualLine = actualReader.readLine();
        }

        return result;
    }

    /**
     * convert Arraylist of differences to an output string.
     *
     * @param diffs arraylist of differences between expected and actual file
     * @return String representation of the differences between the two files
     */
    private String convertToString(final List<String> diffs)
    {
        StringBuilder output = new StringBuilder("");
        for (String line : diffs)
        {
            output.append(line);
            output.append(NEW_LINE);
        }
        return output.toString();
    }

    public int countAssets()
    {
        return DatabaseHelper.getRowCount(DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET;"));
    }

    public int getMaxYardNoForBuilder(int builderId) throws SQLException
    {

        int yardNo;
        ResultSet resultSet = DatabaseHelper.executeQuery("select * from MAST_ASSET_ASSET where builder_id = "
                + builderId + " and primary_yard_number = (select max(primary_yard_number) from MAST_ASSET_ASSET where builder_id = "
                + builderId + ");");
        try
        {
            resultSet.absolute(1);
            yardNo = resultSet.getInt("primary_yard_number");
        }
        catch (SQLException sqle)
        {
            throw new SQLException("Exception getting max yard number for builder id " + builderId, sqle);
        }
        return yardNo;
    }
}
