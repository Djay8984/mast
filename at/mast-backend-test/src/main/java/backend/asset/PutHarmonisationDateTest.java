package backend.asset;

import com.baesystems.ai.lr.dto.DateDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.expectederror.ExpectedError;
import model.harmonisationdate.HarmonisationDate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

import static com.google.common.truth.Truth.assertThat;
import static constant.LloydsRegister.BACKEND_TIME_FORMAT;

public class PutHarmonisationDateTest
{

    @DataProvider(name = "Asset")
    public Object[][] asset()
    {
        return new Object[][]{
                {3, Date.valueOf("2016-03-04")},
                {17, Date.valueOf("2016-06-30")},
                {100, Date.valueOf("2016-09-08")}
        };
    }

    @Issue("LRT-483")
    @Test(description = "MAST BE: PUT /asset/{id}/harmonisation-date",
            dataProvider = "Asset",
            enabled = false
    )
    public void putHarmonisationDateTest(int assetId, Date date)
    {
        new HarmonisationDate(assetId, date);
        assertThat(BACKEND_TIME_FORMAT.format(new HarmonisationDate(assetId).getDto().getDate()))
                .isEqualTo(BACKEND_TIME_FORMAT.format(date));
    }

    @Issue("LRT-483")
    @Test(description = "MAST BE: Negative PUT /asset/{id}/harmonisation-date",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    public void negativePutHarmonisationDateTest(String assetId, int statusCode, String errorMessage)
    {
        DateDto dateDto = new DateDto();
        dateDto.setDate(TestDataHelper.randomDate());
        new ExpectedError(Endpoint.HARMONISATION_DATE, dateDto, assetId, Request.PUT, statusCode, errorMessage);
    }
}
