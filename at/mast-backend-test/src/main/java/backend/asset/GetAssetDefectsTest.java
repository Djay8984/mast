package backend.asset;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetDefect;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetDefectsTest
{

    @DataProvider(name = "DefectPages")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 0, 10},
                {154, 0, 10},
                {385, 0, 10}
        };
    }

    @Test(description = "BE: GET/asset/{id}/defect/?page={id}&size={id}, Story 5.21",
            dataProvider = "DefectPages"
    )
    @Issue("LRT-1025")
    public void getAssetDefectsTest(int assetId, int page, int size)
    {
        new AssetDefect(assetId, page, size);
    }

    @Test(description = " Negative BE: GET/asset/{id}/defect/?page={id}&size={id}, Story 5.21",
            dataProvider = "NegativeForPagesWithId",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1025")
    public void negativeGetAssetDefectsTest(String assetId, String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_DEFECT, new String[]{assetId, page, size}, statusCode, message, Request.GET);
    }
}
