package backend.asset;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetItem;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetItemTest
{

    @DataProvider(name = "AssetItem")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1},
                {4, 3},
                };
    }

    @Test(description = "BE: GET asset/x/item/y",
            dataProvider = "AssetItem"
    )
    @Issue("LRT-1306")
    public void getAssetItemTest(int asset_ID, int ID)
    {
        new AssetItem(asset_ID, ID);
    }

    @Test(description = "BE: GET asset/x/item/y",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1306")
    public void negativeGetAssetItemTest(String asset_ID, String ID, int statusCode, String message)
    {
        if (ID.equals("50000"))
            message = "Item with identifier 50000 not found for asset 1";
        new ExpectedError(Endpoint.ASSET_ITEM, new String[]{asset_ID, ID}, statusCode, message, Request.GET);
    }
}
