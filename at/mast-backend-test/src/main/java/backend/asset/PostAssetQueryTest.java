package backend.asset;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import helper.TestDataHelper;
import model.asset.Asset;
import model.asset.AssetQuery;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PostAssetQueryTest
{

    @DataProvider(name = "Asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {1},
                {3},
                {419},
                {401}
        };
    }

    @Test(description = "BE: /asset/query", dataProvider = "Asset")
    @Issue("LRT-1255")
    public void postAssetQueryAllTest(int assetId) throws SQLException
    {
        AssetLightDto assetDto = new Asset(assetId).getDto();
        CustomerHasFunctionDto partyDto = TestDataHelper.getPartyDetailsFromAsset(assetId);
        AssetQueryDto queryDto = new AssetQueryDto();

        List<Long> lifeCycle = new ArrayList();
        lifeCycle.add(assetDto.getAssetLifecycleStatus().getId());

        List<Long> imo = new ArrayList();
        imo.add(assetDto.getIhsAsset().getId());

        List<Long> assetTypeId = new ArrayList();
        assetTypeId.add(assetDto.getAssetType().getId());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(assetDto.getBuildDate());

        List<Long> classStatus = new ArrayList();
        classStatus.add(assetDto.getClassStatus().getId());

        List<Long> flagState = new ArrayList();
        flagState.add(assetDto.getFlagState().getId());

        queryDto.setLifecycleStatusId(lifeCycle);
        queryDto.setImoNumber(imo);
        queryDto.setSearch(assetDto.getName());
        queryDto.setAssetTypeId(assetTypeId);
        queryDto.setYardNumber(assetDto.getYardNumber());
        calendar.add(Calendar.DATE, -5);
        queryDto.setBuildDateMin(calendar.getTime());
        calendar.add(Calendar.DATE, 10);
        queryDto.setBuildDateMax(calendar.getTime());
        queryDto.setClassStatusId(classStatus);
        queryDto.setFlagStateId(flagState);
        queryDto.setGrossTonnageMin(assetDto.getGrossTonnage() - 5);
        queryDto.setGrossTonnageMax(assetDto.getGrossTonnage() + 5);
        queryDto.setBuilder(assetDto.getBuilder());

        if (partyDto.getId() != null)
        {
            List<Long> party = new ArrayList<>();
            party.add(partyDto.getId());
            queryDto.setPartyId(party);
        }
        new AssetQuery(queryDto);
    }

    @Test(description = "BE: /asset/query", dataProvider = "Asset")
    @Issue("LRT-1255")
    public void postAssetQueryOneTest(int assetId)
    {
        AssetLightDto assetDto = new Asset(assetId).getDto();
        AssetQueryDto queryDto = new AssetQueryDto();

        List<Long> lifecycleStatus = new ArrayList<>();
        lifecycleStatus.add(assetDto.getAssetLifecycleStatus().getId());
        queryDto.setLifecycleStatusId(lifecycleStatus);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> imo = new ArrayList();
        imo.add(assetDto.getIhsAsset().getId());
        queryDto.setImoNumber(imo);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> classStatus = new ArrayList();
        classStatus.add(assetDto.getClassStatus().getId());
        queryDto.setClassStatusId(classStatus);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> flagState = new ArrayList();
        flagState.add(assetDto.getFlagState().getId());
        queryDto.setFlagStateId(flagState);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        queryDto.setBuilder(assetDto.getBuilder());
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> id = new ArrayList();
        id.add(assetDto.getId());
        queryDto.setIdList(id);
        new AssetQuery(queryDto);
    }

    @DataProvider(name = "Search")
    public Object[][] search()
    {
        return new Object[][]{
                {"*carn*"},
                {"*costa*"},
                {"*PRINCESS*"}
        };
    }

    @Test(description = "BE: /asset/query", dataProvider = "Search")
    @Issue("LRT-1255")
    public void postAssetQuerySearchTest(String search)
    {
        AssetQueryDto queryDto = new AssetQueryDto();
        queryDto.setSearch(search);
        new AssetQuery(queryDto);
    }

    @DataProvider(name = "MultipleAssets")
    public Object[][] multipleAssets()
    {
        return new Object[][]{
                {1, 327, 274},
                {419, 5, 224},
                {401, 9, 76}
        };
    }

    @Test(description = "BE: /asset/query", dataProvider = "MultipleAssets")
    @Issue("LRT-1255")
    public void postAssetQueryMultipleItemsTest(int id1, int id2, int id3)
    {
        AssetQueryDto queryDto = new AssetQueryDto();
        AssetLightDto assetDto1 = new Asset(id1).getDto();
        AssetLightDto assetDto2 = new Asset(id2).getDto();
        AssetLightDto assetDto3 = new Asset(id3).getDto();

        List<Long> lifecycleStatus = new ArrayList<>();
        lifecycleStatus.add(assetDto1.getAssetLifecycleStatus().getId());
        lifecycleStatus.add(assetDto2.getAssetLifecycleStatus().getId());
        lifecycleStatus.add(assetDto3.getAssetLifecycleStatus().getId());
        queryDto.setLifecycleStatusId(lifecycleStatus);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> imo = new ArrayList();
        imo.add(assetDto1.getIhsAsset().getId());
        imo.add(assetDto2.getIhsAsset().getId());
        imo.add(assetDto3.getIhsAsset().getId());
        queryDto.setImoNumber(imo);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> classStatus = new ArrayList();
        classStatus.add(assetDto1.getClassStatus().getId());
        classStatus.add(assetDto2.getClassStatus().getId());
        classStatus.add(assetDto3.getClassStatus().getId());
        queryDto.setClassStatusId(classStatus);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        List<Long> flagState = new ArrayList();
        flagState.add(assetDto1.getFlagState().getId());
        flagState.add(assetDto2.getFlagState().getId());
        flagState.add(assetDto3.getFlagState().getId());
        queryDto.setFlagStateId(flagState);
        new AssetQuery(queryDto);

        queryDto = new AssetQueryDto();
        queryDto.setBuilder(assetDto1.getBuilder());
        new AssetQuery(queryDto);
    }
}
