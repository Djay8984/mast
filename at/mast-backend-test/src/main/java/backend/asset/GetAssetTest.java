package backend.asset;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.Asset;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "Assets"
    )
    @Issue("LRD-227")
    public void getAssetTest(int id)
    {
        new Asset(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void negativeGetAssetTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET, id, statusCode, message, Request.GET);
    }

}
