package backend.asset;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetDefectQuery;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;

public class PostAssetDefectQueryTest
{

    @DataProvider(name = "Asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, "id", "asc"},
                {154, "id", "desc"},
                {385, "id", "asc"}
        };
    }

    @Test(description = "Story 9.38 BE: POST asset/x/defect/query ?",
            dataProvider = "Asset")
    @Issue("LRT-1940")
    public void postAssetDefectQueryTest(int assetId, String order, String sort) throws SQLException
    {
        new AssetDefectQuery(assetId, 0, 10, order, sort);
    }

    @Test(description = "Story 9.38 Negative BE: POST asset/x/defect/query ?, ",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1940")
    public void negativePostAssetDefectQueryTest(String assetId, int statusCode, String message)
    {
        CodicilDefectQueryDto queryDto = new CodicilDefectQueryDto();
        new ExpectedError(Endpoint.ASSET_DEFECT_QUERY, queryDto, new String[]{assetId, "0", "10"}, Request.POST, statusCode, message);
    }
}
