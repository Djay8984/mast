package backend.asset;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetActionableItemQuery;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import static constant.Endpoint.ASSET_ACTIONABLE_ITEM_QUERY;

public class PostAssetActionableItemQueryTest
{

    @DataProvider(name = "Asset")
    private Object[][] assets()
    {
        return new Object[][]{
                {501, "id", "asc"},
                {501, "id", "desc"}
        };
    }

    @Issue("LRT-1938")
    @Test(
            dataProvider = "Asset",
            description = "Story 9.38 MAST BE: POST /asset/x/actionable-item/query? "
    )

    public void postAssetActionableItemTest(int assetId, String order, String sort)
    {
        new AssetActionableItemQuery(assetId, 0, 10, order, sort);
    }

    @Test(description = "Story 9.38 Negative MAST BE: POST /asset/x/actionable-item/query?  ",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1938")
    public void negativePostAssetActionableItemTest(String assetId, int statusCode, String message)
    {
        CodicilDefectQueryDto queryDto = new CodicilDefectQueryDto();
        new ExpectedError(ASSET_ACTIONABLE_ITEM_QUERY, queryDto, new String[]{assetId, "0", "10"}, Request.POST, statusCode, message);
    }

}
