package backend.asset;

import model.asset.AssetDefectStatusId;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetDefectStatusId
{

    @DataProvider(name = "DefectStatusId")
    public Object[][] defectStatusId()
    {
        return new Object[][]{
                {1, 1},
                {276, 2},
                {383, 3}
        };
    }

    @Test(description = "BE: GET /asset/x/defect?status=(open), Story 8.3",
            dataProvider = "DefectStatusId"
    )
    @Issue("LRT-1157")
    public void getAssetDefectStatusId(int assetId, int statusId)
    {
        new AssetDefectStatusId(assetId, statusId);
    }

}
