package backend.asset;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import constant.Endpoint;
import constant.Request;
import helper.TestDataHelper;
import model.asset.Asset;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostAssetTest
{

    @Issue("LRT-1305")
    @Test(description = "Test to verify the ability to create an asset.")
    public void postAssetTest() throws SQLException
    {
        AssetDto assetDto = createValidAssetDto();

        new Asset(assetDto);
    }

    @Test(description = "Test to verify error if no name is supplied for the new asset.")
    public void negativePostAssetNoNameTest()
    {
        AssetDto assetDto = createValidAssetDto();
        assetDto.setName(null);
        int statusCode = 400;
        String message = "Validation failed with errors: name may not be null.";

        new ExpectedError(Endpoint.ASSET, assetDto, "", Request.POST, statusCode, message);
    }

    @Test(description = "Test to verify error if no asset category is supplied for the new asset.")
    public void negativePostAssetNoAssetCategoryTest()
    {
        AssetDto assetDto = createValidAssetDto();
        assetDto.setAssetCategory(null);
        int statusCode = 400;
        String message = "Validation failed with errors: assetCategory may not be null.";

        new ExpectedError(Endpoint.ASSET, assetDto, "", Request.POST, statusCode, message);
    }

    @Test(description = "Test to verify error if no asset type is supplied for the new asset.")
    public void negativePostAssetNoAssetTypeTest()
    {
        AssetDto assetDto = createValidAssetDto();
        assetDto.setAssetType(null);
        int statusCode = 400;
        String message = "Validation failed with errors: assetType may not be null.";

        new ExpectedError(Endpoint.ASSET, assetDto, "", Request.POST, statusCode, message);
    }

    @Test(description = "Test to verify error if no IMO number or builder details are supplied for the new asset.")
    public void negativePostAssetNoImoOrBuilderTest()
    {
        AssetDto assetDto = createValidAssetDto();
        assetDto.setIhsAsset(null);
        assetDto.setBuilder(null);
        int statusCode = 409;
        String message = "Validation failed with errors: An Asset must have either an IMO Number or both a Builder and Yard Number.";

        new ExpectedError(Endpoint.ASSET, assetDto, "", Request.POST, statusCode, message);
    }

    @Test(description = "Test to verify error if both IMO number and builder details are supplied for the new asset.")
    public void negativePostAssetBothImoAndBuilderTest()
    {
        AssetDto assetDto = createValidAssetDto();
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        assetDto.setIhsAsset(linkResource);
        int statusCode = 409;
        String message = "Validation failed with errors: An Asset must have either an IMO Number or both a Builder and Yard Number.";

        new ExpectedError(Endpoint.ASSET, assetDto, "", Request.POST, statusCode, message);
    }

    private AssetDto createValidAssetDto()
    {
        AssetDto assetDto = new AssetDto();
        AssetModelDto assetModelDto = new AssetModelDto();
        ItemDto itemDto = new ItemDto();
        List<ItemDto> items = new ArrayList<>();
        itemDto.setId(1L);
        items.add(itemDto);
        assetModelDto.setItems(items);
        assetDto.setName("Trudys Ship");
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        assetDto.setAssetCategory(linkResource);
        assetDto.setBuildDate(new Date());
        assetDto.setAssetType(linkResource);
        assetDto.setBuilder("Sarah Pallett");
        assetDto.setYardNumber("" + TestDataHelper.getUniqueYardNumber());
        assetDto.setClassStatus(linkResource);
        assetDto.setClassNotation("BAE Systems Test Asset");
        assetDto.setKeelLayingDate(new Date());
        assetDto.setGrossTonnage(752D);
        assetDto.setRuleSet(linkResource);
        assetDto.setProductRuleSet(linkResource);
        assetDto.setPreviousRuleSet(linkResource);
        LinkResource proposedFlagState = new LinkResource();
        proposedFlagState.setId(255L);
        assetDto.setFlagState(proposedFlagState);
        assetDto.setFlagState(linkResource);
        assetDto.setHarmonisationDate(new Date());
        assetDto.setAssetLifecycleStatus(linkResource);
        return assetDto;
    }
}
