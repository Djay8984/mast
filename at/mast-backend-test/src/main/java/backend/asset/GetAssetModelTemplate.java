package backend.asset;

import model.asset.AssetModelTemplate;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetModelTemplate
{

    @Test(description = "Backend test for endpoint: /asset-model-template",
            enabled = false)
    @Issue("LRD-361")
    public void assetModelTemplateBackendTest()
    {
        new AssetModelTemplate();
    }
}
