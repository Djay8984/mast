package backend.asset;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetActionableItem;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Date;

public class PostAssetActionableItem
{

    @DataProvider(name = "Asset")
    private Object[][] assets()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRT-1527")
    @Test(
            dataProvider = "Asset",
            description = "MAST BE: POST /asset/x/actionable-item"
    )
    public void postAssetActionableItemTest(int assetId)
    {
        new AssetActionableItem(getActionableItem(), assetId);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            description = "MAST BE Negative: POST /asset/x/actionable-item"
    )
    @Issue("LRT-1527")
    public void negativeGetJobWipActionableItemTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_ACTIONABLE_ITEM, getActionableItem(), id, Request.POST, statusCode, message);
    }

    private ActionableItemDto getActionableItem()
    {
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        ActionableItemDto actionableItemDto = new ActionableItemDto();
        actionableItemDto.setTitle("Automated Actionable Item");
        actionableItemDto.setRequireApproval(true);
        actionableItemDto.setConfidentialityType(linkResource);
        actionableItemDto.setDescription("Automated Actionable Item Description");
        actionableItemDto.setCategory(linkResource);
        actionableItemDto.setDefect(linkResource);
        actionableItemDto.setDueDate(new Date());
        actionableItemDto.setEditableBySurveyor(true);
        actionableItemDto.setEmployee(linkResource);
        actionableItemDto.setImposedDate(new Date());
        actionableItemDto.setJob(linkResource);
        actionableItemDto.setStatus(linkResource);

        return actionableItemDto;
    }

}
