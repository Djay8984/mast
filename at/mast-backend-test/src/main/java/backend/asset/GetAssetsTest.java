package backend.asset;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetPage;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetsTest
{

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-225")
    public void getAssetsTest(int page, int size)
    {
        new AssetPage(page, size);
    }

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-225")
    public void negativeGetAssetsTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
