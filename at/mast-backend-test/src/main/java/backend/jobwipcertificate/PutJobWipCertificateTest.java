package backend.jobwipcertificate;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobWipCertificate;
import model.job.JobWipCertificatePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutJobWipCertificateTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {4, 5}, {5, 3}, {6, 2}
        };
    }

    @Test(description = "BE: PUT /job/{jobId}/wip-certificate/{certificateId}, Story 6.1",
            dataProvider = "Jobs")
    @Issue("LRT-1039")
    public void putJobWipCertificateTest(int jobId, int certificateId)
    {
        CertificateDto certificateDto = new JobWipCertificatePage(jobId, 0, 1).getDto().getContent().get(0);

        certificateDto.setExtendedDate(Date.valueOf("2019-09-16"));
        certificateDto.setCertificateNumber("56");
        certificateDto.setExpiryDate(Date.valueOf("2019-09-17"));

        new JobWipCertificate(certificateDto, jobId, certificateId);
    }

    @Test(description = "BE Negative: PUT /job/{jobId}/wip-certificate/{certificateId}, Story 6.1",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1039")
    public void negativePutJobWipCertificateTest(String jobId, String certificateId, int statusCode, String errorMessage)
    {
        CertificateDto certificateDto = new JobWipCertificatePage(5, 0, 1).getDto().getContent().get(0);
        certificateDto.setId(Long.valueOf(certificateId));
        // if job Id is set to zero in the body then it throws a validation error on the body
        if (!jobId.equals("0"))
        {
            LinkResource job = new LinkResource();
            job.setId(Long.valueOf(jobId));
            certificateDto.setJob(job);
        }

        new ExpectedError(Endpoint.JOB_WIP_CERT,
                certificateDto,
                new String[]{jobId, certificateId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
