package backend.jobwipcertificate;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobWipCertificatePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobWipCertificatesTest
{

    @DataProvider(name = "JobPages")
    public Object[][] jobPages()
    {
        return new Object[][]{
                {3, 0, 10},
                {4, 0, 10},
                {5, 0, 10}
        };
    }

    @Test(description = "BE: GET /job/{jobId}/wip-certificate/?page={id}&size={id}, Story 6.1",
            dataProvider = "JobPages")
    @Issue("LRT-1037")
    public void getJobWipCertificatesTest(int jobId, int page, int size)
    {
        new JobWipCertificatePage(jobId, page, size);
    }

    @Test(description = "BE Negative: GET /job/{jobId}/wip-certificate/?page={id}&size={id}, Story 6.1",
            dataProvider = "NegativeForPagesWithId",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1037")
    public void negativeGetJobWipCertificatesTest(String jobId, String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.JOB_WIP_CERT_PAGE, new String[]{jobId, page, size}, statusCode, message, Request.GET);
    }
}
