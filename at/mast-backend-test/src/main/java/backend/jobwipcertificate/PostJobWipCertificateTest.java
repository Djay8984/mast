package backend.jobwipcertificate;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.references.*;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobWipCertificate;
import model.job.JobWipCertificatePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostJobWipCertificateTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3}, {5}, {7}
        };
    }

    @Test(description = "BE:POST /job/{jobId}/wip-certificate, Story 6.1",
            dataProvider = "Jobs")
    @Issue("LRT-1038")
    public void postJobWipCertificateTest(int jobId)
    {
        CertificateDto certificateDto = new CertificateDto();
        ReferenceDataDto certificateStatus = new ReferenceDataDto();
        certificateStatus.setId((long) 1);

        CertificateActionDto actionDto = new CertificateActionDto();
        actionDto.setId((long) 2);

        CertificateTypeDto typeDto = new CertificateTypeDto();
        typeDto.setId((long) 1);

        OfficeDto officeDto = new OfficeDto();
        officeDto.setId((long) 8);

        LrEmployeeDto employeeDto = new LrEmployeeDto();
        employeeDto.setId((long) 6);

        LinkResource job = new LinkResource();
        job.setId((long) jobId);

        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        certificateDto.setCertificateNumber("1234");
        certificateDto.setCertificateStatus(linkResource);
        certificateDto.setCertificateType(linkResource);
        certificateDto.setCertificateAction(linkResource);
        certificateDto.setExpiryDate(Date.valueOf("2016-02-16"));
        certificateDto.setExtendedDate(Date.valueOf("2016-02-16"));
        certificateDto.setOffice(linkResource);
        certificateDto.setEmployee(linkResource);
        certificateDto.setJob(job);

        new JobWipCertificate(certificateDto, jobId);
    }

    @Test(description = "BE Negative: POST /job/{jobId}/wip-certificate, Story 6.1",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1038")
    public void negativePostJobWipCertificateTest(String jobId, int statusCode, String message)
    {
        CertificateDto certificateDto = new JobWipCertificatePage(4, 0, 1).getDto().getContent().get(0);
        certificateDto.setId(null);

        new ExpectedError(Endpoint.JOB_WIP_CERT,
                certificateDto,
                new String[]{jobId, ""},
                Request.POST,
                statusCode,
                message);
    }
}
