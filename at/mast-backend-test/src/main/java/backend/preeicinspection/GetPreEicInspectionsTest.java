package backend.preeicinspection;

import constant.Endpoint;
import constant.ReferenceTable;
import model.referencedata.ReferenceData;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetPreEicInspectionsTest
{

    @Issue("LRD-476")
    @Test(description = "MAST BE: GET /pre-eic-inspection-status",
            enabled = false)
    public void getPreEicInspectionStatus()
    {
        new ReferenceData(Endpoint.PRE_EIC_INSPECTION_STATUS, ReferenceTable.PRE_EIC_INSPECTION_STATUS);
    }
}
