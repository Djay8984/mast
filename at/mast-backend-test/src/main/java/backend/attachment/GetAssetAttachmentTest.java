package backend.attachment;

import model.attachment.AssetAttachmentPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetAttachmentTest
{

    @DataProvider(name = "AssetAttachmentPages")
    public Object[][] attachments()
    {
        return new Object[][]{
                {1, 0, 10},
                {4, 0, 10}
        };
    }

    @Test(description = "BE: GET/asset/{id}/attachment/?page={id}&size={id}, Story 9.1",
            dataProvider = "AssetAttachmentPages"
    )
    @Issue("LRT-1823")
    public void getAssetAttachmentsTest(int assetId, int page, int size)
    {
        new AssetAttachmentPage(assetId, page, size);
    }

}
