package backend.attachment;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import constant.Endpoint;
import constant.Request;
import helper.DatabaseHelper;
import helper.TestDataHelper;
import model.attachment.Attachment;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static com.google.common.truth.Truth.assert_;

public class AssetAttachmentTest
{

    @Issue("LRT-1834")
    @Test(description = "Test to verify the ability to create an asset attachment.")
    public void postAssetAttachmentTest() throws SQLException
    {
        SupplementaryInformationDto supplementaryInformationDto = createValidSupplementaryInformationDto();
        String[] values = new String[]{"428", ""};
        new Attachment(Endpoint.ASSET_ATTACHMENT, supplementaryInformationDto, values);
    }

    @Test(description = "Test to verify error if the title for the new asset attachment is too long.")
    public void negativePostAssetAttachmentTitleTooLongTest()
    {
        SupplementaryInformationDto supplementaryInformationDto = createValidSupplementaryInformationDto();
        supplementaryInformationDto.setTitle("This is a very long title which should fail validation.");
        int statusCode = 400;
        String message = "Validation failed with errors: title invalid length.";

        new ExpectedError(Endpoint.ASSET_ATTACHMENT, supplementaryInformationDto, new String[]{"1", ""}, Request.POST, statusCode, message);

    }

    @Test(description = "Test to verify error if the Confidentiality Type Id is missing.")
    public void negativePostAssetAttachmentConfidentialityTypeIdMissingTest()
    {
        SupplementaryInformationDto supplementaryInformationDto = createValidSupplementaryInformationDto();
        supplementaryInformationDto.getConfidentialityType().setId(null);
        int statusCode = 400;
        String message = "Validation failed with errors: confidentialityType.id may not be null.";

        new ExpectedError(Endpoint.ASSET_ATTACHMENT, supplementaryInformationDto, new String[]{"1", ""}, Request.POST, statusCode, message);

    }

    @Test(description = "Test to verify error if AttachmentTypeId is missing.")
    public void negativePostAssetAttachmentAttachmentTypeIdMissingTest()
    {
        SupplementaryInformationDto supplementaryInformationDto = createValidSupplementaryInformationDto();
        supplementaryInformationDto.getAttachmentType().setId(null);
        int statusCode = 400;
        String message = "Validation failed with errors: attachmentType.id may not be null.";

        new ExpectedError(Endpoint.ASSET_ATTACHMENT, supplementaryInformationDto, new String[]{"1", ""}, Request.POST, statusCode, message);
    }

    @Test(description = "Test to verify error if Attachment Location Id is missing.")
    public void negativePostAssetAttachmentLocationIdMissingTest()
    {
        SupplementaryInformationDto supplementaryInformationDto = createValidSupplementaryInformationDto();
        supplementaryInformationDto.getLocation().setId(null);
        int statusCode = 400;
        String message = "Validation failed with errors: location.id may not be null.";

        new ExpectedError(Endpoint.ASSET_ATTACHMENT, supplementaryInformationDto, new String[]{"1", ""}, Request.POST, statusCode, message);
    }

    @Issue("LRT-1838")
    @Test(description = "Story 9.34 BE: delete /asset/x/attachment/y",
            dependsOnMethods = "postAssetAttachmentTest")
    public void deleteAssetAttachmentTest() throws SQLException
    {
        int id = TestDataHelper.getLastInsertedRow("MAST_XXX_SUPPLEMENTARYINFORMATIONLINK");
        String[] values = new String[]{"428", String.valueOf(id)};
        new Attachment(Endpoint.ASSET_ATTACHMENT, values, Request.DELETE);
        ResultSet rs = DatabaseHelper.executeQuery(String.format("SELECT * FROM MAST_XXX_SUPPLEMENTARYINFORMATION si right join" +
                " MAST_XXX_SUPPLEMENTARYINFORMATIONLINK sil on si.id = sil.id where sil.asset_id =  %s AND si.id = %s ;", values));
        rs.next();

        assert_()
                .withFailureMessage("Attachment ID :[" + rs.getObject("si.id").toString() + "] has not been updated as deleted")
                .that(rs.getObject("si.deleted").toString()).isEqualTo("true");

    }

    private SupplementaryInformationDto createValidSupplementaryInformationDto()
    {
        SupplementaryInformationDto supplementaryInformationDto = new SupplementaryInformationDto();
        supplementaryInformationDto.setTitle("SupplementaryDto");
        supplementaryInformationDto.setNote("This is a supplementaryDto note.");
        supplementaryInformationDto.setAttachmentUrl("www.attachment.url");
        supplementaryInformationDto.setAssetLocationViewpoint("assetLocationViewpoint");
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        supplementaryInformationDto.setAttachmentType(linkResource);
        supplementaryInformationDto.setConfidentialityType(linkResource);
        supplementaryInformationDto.setLocation(linkResource);
        LrEmployeeDto author = new LrEmployeeDto();
        author.setId(1L);
        author.setFirstName("John");
        author.setLastName("Smith");
        supplementaryInformationDto.setAuthor(author);
        supplementaryInformationDto.setCreationDate(new Date());
        return supplementaryInformationDto;
    }

}
