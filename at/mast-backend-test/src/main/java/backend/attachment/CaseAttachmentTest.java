package backend.attachment;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.attachment.Attachment;
import model.expectederror.ExpectedError;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;

import java.sql.SQLException;

public class CaseAttachmentTest
{

    private static final int caseId = 2;
    private static Attachment postedAttachment;

    @Test(description = "BE: POST /case/{id}/attachment")
    @Issue("LRT-759")
    public void postAttachmentTest() throws SQLException
    {
        SupplementaryInformationDto attachmentDto = new SupplementaryInformationDto();
        attachmentDto.setTitle("Title");
        attachmentDto.setNote("Note");
        attachmentDto.setAttachmentUrl("Url");
        attachmentDto.setAssetLocationViewpoint("LocationViewpoint");

        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) 1);

        attachmentDto.setAttachmentType(linkResource);
        attachmentDto.setConfidentialityType(linkResource);
        attachmentDto.setLocation(linkResource);

        postedAttachment = new Attachment(Endpoint.CASE_ATTACHMENT, attachmentDto, new String[]{
                Integer.toString(caseId),
                ""
        });
    }

    @Test(
            description = "BE: GET /case/{id}/attachment",
            dependsOnMethods = {"postAttachmentTest"}
    )
    @Issue("LRT-760")
    public void getAttachmentTest() throws SQLException
    {
        new Attachment(Endpoint.CASE_ATTACHMENT, new String[]{
                Integer.toString(caseId),
                Long.toString(postedAttachment.getDto().getId())
        }
                , Request.GET);
    }

    @Test(
            description = "BE: DELETE /case/{id}/attachment/{id}",
            dependsOnMethods = {"postAttachmentTest", "getAttachmentTest"}
    )
    @Issue("LRT-762")
    public void deleteAttachmentTest() throws SQLException
    {
        new Attachment(Endpoint.CASE_ATTACHMENT, new String[]{
                Integer.toString(caseId),
                Long.toString(postedAttachment.getDto().getId())
        }, Request.DELETE);

        new ExpectedError(Endpoint.CASE_ATTACHMENT, new String[]{
                Integer.toString(caseId),
                Long.toString(postedAttachment.getDto().getId())
        }, HttpStatus.SC_NOT_FOUND);
    }

    @Test(
            description = "BE: Negative scenarios for all attachment endpoints",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issues({
            @Issue("LRT-759"),
            @Issue("LRT-760"),
            @Issue("LRT-762")
    })
    public void negativeAttachmentScenarioTest(String caseId, int statusCode, String errorMessage)
    {
        new ExpectedError(Endpoint.CASE_ATTACHMENT, new String[]{caseId, ""}, statusCode, errorMessage, Request.GET);
    }

}
