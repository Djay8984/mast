package backend.attachment;

import constant.Endpoint;
import constant.ReferenceTable;
import model.referencedata.ReferenceDataArray;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAttachmentLocationTest
{

    @Test(description = "BE Test: GET /attachment-location",
            enabled = false)
    @Issue("LRT-758")
    public void getAttachmentLocationTest()
    {
        new ReferenceDataArray(Endpoint.ATTACHMENT_LOCATION, ReferenceTable.ATTACHMENT_LOCATION);
    }
}
