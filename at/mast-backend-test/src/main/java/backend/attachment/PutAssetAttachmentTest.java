package backend.attachment;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.attachment.AssetAttachmentPage;
import model.attachment.AttachmentAttachmentID;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Date;

import static com.google.common.truth.Truth.assert_;

public class PutAssetAttachmentTest
{

    final int number = 0;
    final int assetId = 1;

    @Issue("LRT-1835")
    @Test(description = "BE put /asset/x/attachment/y")
    public void putAssetAttachmentTest()
    {
        //get a pre-existing attachment - the "BEFORE" information
        AssetAttachmentPage attachment = new AssetAttachmentPage(assetId, 0, 1);
        Long attachmentId = attachment.getDto().getContent()
                .get(number)
                .getId();
        String attachmentTitle = attachment.getDto().getContent()
                .get(number)
                .getTitle();
        String attachmentNote = attachment.getDto().getContent()
                .get(number)
                .getNote();
        String attachmentUrl = attachment.getDto().getContent()
                .get(number)
                .getAttachmentUrl();
        String attachmentLocationViewPoint = attachment.getDto()
                .getContent()
                .get(number)
                .getAssetLocationViewpoint();
        LinkResource attachmentType = attachment.getDto()
                .getContent()
                .get(number)
                .getAttachmentType();
        Long attachmentTypeId = attachmentType.getId();
        LinkResource confidentialityType = attachment.getDto()
                .getContent()
                .get(number)
                .getConfidentialityType();
        Long confidentialityTypeId = confidentialityType.getId();

        Date creationDate = attachment.getDto()
                .getContent()
                .get(number)
                .getCreationDate();

        // Create "AFTER"  values by mutating the "before" information by adding
        // today's date
        String date = TestDataHelper.getTodayDate();
        SupplementaryInformationDto supplementaryInformationDto = new
                SupplementaryInformationDto();
        attachmentTitle = attachmentTitle + date;
        attachmentNote = attachmentNote + date;
        attachmentUrl = attachmentUrl + date;
        attachmentLocationViewPoint = attachmentLocationViewPoint + date;

        // Create an valid "AFTER" supplementary information dto with the new
        // mutated values
        SupplementaryInformationDto afterSupplementaryInformationDto = new
                SupplementaryInformationDto();
        afterSupplementaryInformationDto.setId(attachmentId);
        afterSupplementaryInformationDto.setTitle(attachmentTitle);
        afterSupplementaryInformationDto.setNote(attachmentNote);
        afterSupplementaryInformationDto.setAttachmentUrl(attachmentUrl);
        afterSupplementaryInformationDto.setAssetLocationViewpoint(attachmentLocationViewPoint);
        LinkResource afterAttachmentType = new LinkResource();
        afterAttachmentType.setId(attachmentTypeId);
        afterSupplementaryInformationDto.setAttachmentType(afterAttachmentType);
        LinkResource afterConfidentialityType = new LinkResource();
        afterConfidentialityType.setId(confidentialityTypeId);
        afterSupplementaryInformationDto.setConfidentialityType(afterConfidentialityType);
        LrEmployeeDto afterAuthor = new LrEmployeeDto();
        afterAuthor.setId(1L);
        supplementaryInformationDto.setAuthor(afterAuthor);
        afterSupplementaryInformationDto.setCreationDate(creationDate);

        String[] values = new String[2];
        values[0] = String.valueOf(assetId);
        values[1] = String.valueOf(attachmentId);
        new AttachmentAttachmentID(Endpoint.ASSET_ATTACHMENT_ATTACHMENTID,
                afterSupplementaryInformationDto, values);

        //verify that the details are updated
        AssetAttachmentPage afterAttachment = new AssetAttachmentPage(assetId, 0, 1);
        assert_().withFailureMessage("The updated Attachment Title is " + attachmentTitle)
                .that(attachmentTitle)
                .isEqualTo(afterAttachment.getDto()
                        .getContent()
                        .get(number)
                        .getTitle());
        assert_().withFailureMessage("The updated Note is " + attachmentNote)
                .that(attachmentNote)
                .isEqualTo(afterAttachment.getDto()
                        .getContent()
                        .get(number)
                        .getNote());
        assert_().withFailureMessage("The updated Attachment URL is  " + attachmentUrl)
                .that(attachmentUrl)
                .isEqualTo(afterAttachment.getDto()
                        .getContent()
                        .get(number)
                        .getAttachmentUrl());
        assert_().withFailureMessage("The updated Asset Location Viewpoint is " + attachmentLocationViewPoint)
                .that(attachmentLocationViewPoint)
                .isEqualTo(afterAttachment.getDto()
                        .getContent()
                        .get(number)
                        .getAssetLocationViewpoint());
    }

    @Issue("LRT-1835")
    @Test(description = "Verify that the request id does not match resource id")
    public void negativePutAssetAttachmentTest()
    {
        //get a pre-existing attachment - the "BEFORE" information
        AssetAttachmentPage attachment = new AssetAttachmentPage(assetId, 0, 1);
        SupplementaryInformationDto supplementaryInformationDto = attachment
                .getDto().getContent().get(number);
        int statusCode = 400;
        String[] values = new String[2];
        values[0] = String.valueOf(assetId);
        values[1] = String.valueOf(supplementaryInformationDto.getId());

        supplementaryInformationDto.setId(null);

        String message = "The  ID in the request body null, does not match " +
                "the resource id " + values[1];

        new ExpectedError(Endpoint.ASSET_ATTACHMENT_ATTACHMENTID,
                supplementaryInformationDto,
                values,
                Request.PUT,
                statusCode,
                message);
    }

    @Issue("LRT-1835")
    @Test(description = ("Negative test for two parameters"),
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    public void negativePutAssetAttachmentTest(String assetId, String attachmentId, int statusCode, String errorMessage)
    {

        AssetAttachmentPage attachment = new AssetAttachmentPage(Integer.parseInt(assetId), 0, 1);
        SupplementaryInformationDto supplementaryInformationDto = attachment
                .getDto().getContent().get(number);
        supplementaryInformationDto.setId(Long.parseLong(attachmentId));

        new ExpectedError(Endpoint.ASSET_ATTACHMENT_ATTACHMENTID,
                supplementaryInformationDto,
                new String[]{String.valueOf(assetId), String.valueOf(attachmentId)},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}

