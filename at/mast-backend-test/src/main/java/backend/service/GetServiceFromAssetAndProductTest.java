package backend.service;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.service.ServiceProduct;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetServiceFromAssetAndProductTest
{

    @DataProvider(name = "AssetProducts")
    public static Object[][] assetProducts()
    {
        return new Object[][]{
                {1}, {2}
        };
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE: GET /asset/{id}/product/{id}/service",
            dataProvider = "AssetProducts",
            enabled = false
    )
    public void getServiceFromAssetAndProductTest(int assetId)
    {
        new ServiceProduct(assetId);
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE Negative: GET /asset/{id}/product/{id}/service",
            dataProviderClass = DataProviders.class,
            dataProvider = "NegativeTwoParameterGeneric",
            enabled = false
    )
    public void negativeGetServiceFromAssetAndProductTest(
            String assetId,
            String productId,
            int statusCode,
            String errorMessage
    )
    {
        if (assetId.equals("50000"))
            errorMessage = "Entity combination not found.";
        new ExpectedError(Endpoint.ASSET_SERVICE, new String[]{assetId, productId, ""}, statusCode, errorMessage, Request.GET);
    }

}
