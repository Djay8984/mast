package backend.service;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.service.ServiceJob;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetServiceFromJobTest
{

    @DataProvider(name = "Job")
    public static Object[][] job()
    {
        return new Object[][]{
                {1}, {4}, {6}
        };
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE: GET /job/{id}/service",
            dataProvider = "Job",
            enabled = false
    )
    public void getServiceFromJobTest(int jobId)
    {
        new ServiceJob(jobId);
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE Negative: GET /job/{id}/service",
            dataProviderClass = DataProviders.class,
            dataProvider = "NegativeOneParameter",
            enabled = false
    )
    public void negativeGetServiceFromJobTest(
            String assetId,
            int statusCode,
            String errorMessage
    )
    {
        new ExpectedError(Endpoint.SERVICE_JOB, assetId, statusCode, errorMessage, Request.GET);
    }

}
