package backend.service;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.harmonisationdate.HarmonisationDate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetHarmonisationDateTest
{

    @DataProvider(name = "Asset")
    public static Object[][] asset()
    {
        return new Object[][]{
                {3}, {17}, {100}
        };
    }

    @Issue("LRT-482")
    @Test(
            description = "MAST BE: GET /asset/{id}/harmonisation-date",
            dataProvider = "Asset"
    )
    public void getHarmonisationDateTest(int assetId)
    {
        new HarmonisationDate(assetId);
    }

    @Issue("LRT-482")
    @Test(
            description = "MAST BE Negative: GET /asset/{id}/harmonisation-date",
            dataProviderClass = DataProviders.class,
            dataProvider = "NegativeOneParameter"
    )
    public void negativeGetHarmonisationDateTest(String assetId, int statusCode, String errorMessage)
    {
        new ExpectedError(Endpoint.HARMONISATION_DATE, assetId, statusCode, errorMessage, Request.GET);
    }

}
