package backend.service;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import helper.TestDataHelper;
import model.asset.AssetService;
import model.service.ServiceProduct;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Calendar;
import java.util.Date;

import static com.google.common.truth.Truth.assertThat;

public class PutServiceTest
{

    @DataProvider(name = "Services")
    public static Object[][] services()
    {
        return new Object[][]{
                {501},
                {501}
        };
    }

    @Issue("LRD-227")
    @Test(
            description = "MAST BE: PUT /asset/{id}/product/{id}/service",
            dataProvider = "Services",
            enabled = false
    )
    public void putServiceTest(int assetId)
    {
        ServiceProduct serviceProduct = new ServiceProduct(assetId);

        for (ScheduledServiceDto serviceDto : serviceProduct.getDto().getScheduledServices())
        {
            String uniqueNumber = Integer.toString(TestDataHelper.getUniqueYardNumber());

            serviceDto.setLastPartheldJob(uniqueNumber);

            Calendar c = Calendar.getInstance();
            c.setTime(serviceDto.getDueDate());
            c.add(Calendar.DATE, 1);
            Date updatedCertificateDate = c.getTime();
            serviceDto.setDueDate(updatedCertificateDate);

            int editedServiceDtoId = serviceDto.getId().intValue();

            new AssetService(assetId, editedServiceDtoId, serviceDto);

            boolean found = false;
            for (ScheduledServiceDto newServiceDto : new ServiceProduct(assetId).getDto().getScheduledServices())
            {
                if (newServiceDto.getId() == editedServiceDtoId)
                {
                    found = true;
                    assertThat(updatedCertificateDate).isEqualTo(serviceDto.getDueDate());
                    assertThat(uniqueNumber).isEqualTo(serviceDto.getLastPartheldJob());
                    break;
                }
            }
            Assert.assertTrue(found, "After editing the service with the ID of " + serviceDto.getId() + "" +
                    " applied to an asset, it wasn't found after getting the services for that asset again.");
        }
    }
}
