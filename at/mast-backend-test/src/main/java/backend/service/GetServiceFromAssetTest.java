package backend.service;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.service.ServiceAsset;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetServiceFromAssetTest
{

    @DataProvider(name = "AssetProducts")
    public static Object[][] assetProducts()
    {
        return new Object[][]{
                {1}, {419}, {201}
        };
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE: GET /asset/{id}/service",
            dataProvider = "AssetProducts"
    )
    public void getServiceFromAssetTest(int assetId)
    {
        new ServiceAsset(assetId);
    }

    @Issue("LRT-484")
    @Test(
            description = "MAST BE Negative: GET /asset/{id}/service",
            dataProviderClass = DataProviders.class,
            dataProvider = "NegativeOneParameter"
    )
    public void negativeGetServiceFromAssetTest(
            String assetId,
            int statusCode,
            String errorMessage
    )
    {
        new ExpectedError(Endpoint.SERVICE_ASSET, assetId, statusCode, errorMessage, Request.GET);
    }

}
