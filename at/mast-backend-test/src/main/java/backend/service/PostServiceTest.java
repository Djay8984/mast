package backend.service;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.DatabaseHelper;
import model.expectederror.ExpectedError;
import model.service.ServiceProduct;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;

public class PostServiceTest
{

    public static ScheduledServiceListDto scheduledServiceListDto = new ScheduledServiceListDto();
    public static ScheduledServiceDto serviceDto = new ScheduledServiceDto();

    @DataProvider(name = "Service")
    public static Object[][] service()
    {
        return new Object[][]{
                {1}
        };
    }

    @Issue("LRT-654")
    @Test(
            description = "MAST BE: POST /asset/{id}/service",
            dataProvider = "Service",
            enabled = false
    )
    public void postServiceTest(int assetId)
    {
        createServiceDto(assetId);
        scheduledServiceListDto.setScheduledServices(Collections.singletonList(serviceDto));

        new ServiceProduct(assetId, scheduledServiceListDto);
    }

    @Test(
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class,
            description = "MAST BE Negative: POST /asset/{id}/product/{id}/service/{id}",
            enabled = false
    )
    public void negativePostServiceTest(
            String assetId,
            String productId,
            int statusCode,
            String errorMessage
    )
    {
        if (assetId.equals("50000"))
            errorMessage = "Entity combination not found.";
        createServiceDto(1);
        serviceDto.setServiceCatalogueId((long) 1);
        new ExpectedError(
                Endpoint.ASSET_SERVICE,
                serviceDto,
                new String[]{assetId, productId, ""},
                Request.POST,
                statusCode,
                errorMessage
        );
    }

    private void createServiceDto(int assetId)
    {
        LinkResource asset = new LinkResource();
        asset.setId((long) assetId);
        serviceDto.setAsset(asset);
        serviceDto.setAssignedDate(new Date());
        serviceDto.setContinuous(true);
        serviceDto.setCyclePeriodicity(36);
        serviceDto.setDueDate(new Date());
        serviceDto.setLowerRangeDate(new Date());
        LinkResource schedulingRuleType = new LinkResource();
        schedulingRuleType.setId(1L);
        serviceDto.setUpperRangeDate(new Date());
        serviceDto.setProvisionalDates(false);
        serviceDto.setServiceCatalogueId(1L);
    }

    private Long getServiceCatalogueFromProduct(int productId)
    {
        ResultSet product = DatabaseHelper.executeQuery(
                "SELECT * FROM mast_asset_assetproduct WHERE id = " + productId + ";"
        );
        try
        {
            product.first();
            int productCatalogueId = product.getInt("product_catalogue_id");
            ResultSet serviceCatalogue = DatabaseHelper.executeQuery(
                    "SELECT * FROM mast_ref_servicecatalogue WHERE product_catalogue_id = " + productCatalogueId + ";"
            );
            serviceCatalogue.first();
            return serviceCatalogue.getLong("id");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
