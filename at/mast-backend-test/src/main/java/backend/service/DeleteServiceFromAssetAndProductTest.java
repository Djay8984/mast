package backend.service;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.expectederror.ExpectedError;
import model.service.ServiceProduct;
import org.testng.annotations.Test;

public class DeleteServiceFromAssetAndProductTest
{

    @Test(
            description = "MAST BE: DELETE /asset/{id}/product/{id}/service/{id}",
            enabled = false
    )
    public void deleteServiceFromAssetAndProductTest()
    {
        int serviceId = TestDataHelper.insertService("1", "1");
        new ServiceProduct(1, serviceId, Request.DELETE);
    }

    @Test(
            dataProvider = "NegativeThreeParameter",
            dataProviderClass = DataProviders.class,
            description = "MAST BE Negative: DELETE /asset/{id}/product/{id}/service/{id}",
            enabled = false
    )
    public void negativeDeleteServiceFromAssetAndProductTest(
            String assetId,
            String productId,
            String serviceId,
            int statusCode,
            String errorMessage
    )
    {
        new ExpectedError(
                Endpoint.ASSET_SERVICE,
                new String[]{assetId, productId, serviceId},
                statusCode,
                errorMessage,
                Request.DELETE
        );
    }
}
