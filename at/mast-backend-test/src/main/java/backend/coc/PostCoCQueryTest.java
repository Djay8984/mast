package backend.coc;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCoCQuery;
import model.expectederror.ExpectedError;
import org.joda.time.LocalDate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;

import static backend.coc.PostCoCQueryTest.CoCSearchType.*;

public class PostCoCQueryTest
{

    @DataProvider(name = "Asset")
    public Object[][] assets()
    {
        return new Object[][][]{
                {{0L}, {CATEGORY_LIST}, {1}},
                {{0L, 1L}, {CATEGORY_LIST}, {501}},
                {{0L, 1L, 2L}, {CATEGORY_LIST}, {1}},
                {{0L, 3L}, {CATEGORY_LIST}, {1}},
                {{0L, 1L}, {CONFIDENTIALITY_LIST}, {1}},
                {{0L, 1L, 3L}, {CONFIDENTIALITY_LIST}, {501}},
                {{new LocalDate().withYear(2016).withDayOfMonth(25).withMonthOfYear(12)}, {DUE_DATE_MAX}, {1}},
                {{new LocalDate().withYear(1992).withDayOfMonth(1).withMonthOfYear(5)}, {DUE_DATE_MAX}, {501}},
                {{new LocalDate().withYear(1992).withDayOfMonth(8).withMonthOfYear(10)}, {DUE_DATE_MIN}, {1}},
                {{new LocalDate().withYear(2020).withDayOfMonth(6).withMonthOfYear(6)}, {DUE_DATE_MIN}, {1}},
                {{"Desc%"}, {SEARCH_STRING}, {1}},
                {{"Not Present in DB"}, {SEARCH_STRING}, {1}},
                };
    }

    @Test(description = "BE: POST /asset/{id}/coc/query",
            dataProvider = "Asset")
    @Issue("LRT-1936")
    public void postAssetCoCQueryTest(Object[] searchParameters, Object[] searchTypes, Object[] assetId)
    {
        CodicilDefectQueryDto codicilDefectQueryDto = new CodicilDefectQueryDto();
        Arrays.stream(searchTypes).forEachOrdered(
                sT ->
                {
                    CoCSearchType searchType = (CoCSearchType) sT;
                    switch (searchType)
                    {
                        case CATEGORY_LIST:
                            codicilDefectQueryDto.setCategoryList(
                                    Arrays.asList(Arrays.copyOf(searchParameters, searchParameters.length, Long[].class))
                            );
                            break;
                        case CONFIDENTIALITY_LIST:
                            codicilDefectQueryDto.setConfidentialityList(
                                    Arrays.asList(Arrays.copyOf(searchParameters, searchParameters.length, Long[].class))
                            );
                            break;
                        case DUE_DATE_MAX:
                            codicilDefectQueryDto.setDueDateMax(((LocalDate) searchParameters[0]).toDate());
                            break;
                        case DUE_DATE_MIN:
                            codicilDefectQueryDto.setDueDateMin(((LocalDate) searchParameters[0]).toDate());
                            break;
                        case SEARCH_STRING:
                            codicilDefectQueryDto.setSearchString((String) searchParameters[0]);
                            break;
                    }
                }
        );
        new AssetCoCQuery((int) assetId[0], codicilDefectQueryDto);
    }

    @Test(description = "Negative BE: /asset/{id}/coc/query",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class)
    @Issue("LRT-1936")
    public void negativePostAssetCoCQueryTest(String assetId, int statusCode, String message)
    {
        CodicilDefectQueryDto codicilDefectQueryDto = new CodicilDefectQueryDto();
        new ExpectedError(Endpoint.ASSET_COC_QUERY,
                codicilDefectQueryDto,
                new String[]{assetId},
                Request.POST,
                statusCode,
                message);
    }

    enum CoCSearchType
    {
        CATEGORY_LIST, CONFIDENTIALITY_LIST, DUE_DATE_MAX, DUE_DATE_MIN, SEARCH_STRING, ITEM_TYPE
    }

}
