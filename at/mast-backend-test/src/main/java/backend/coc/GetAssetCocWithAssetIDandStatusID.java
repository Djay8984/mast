package backend.coc;

import model.asset.AssetCocAssetIDStatusID;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetCocWithAssetIDandStatusID
{

    @DataProvider(name = "AssetsWithAssetIDAndStatusID")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1},
                {1, 2},
                {1, 3}
        };
    }

    @Test(description = "BE: GET /asset/assetID/coc/?statusID= ",
            dataProvider = "AssetsWithAssetIDAndStatusID")
    @Issue("LRT-1300")
    public void getAssetCocWithAssetIDandStatusID(int assetId, int statusId)
    {
        new AssetCocAssetIDStatusID(assetId, statusId);
    }

}
