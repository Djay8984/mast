package backend.coc;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCoc;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutAssetCocTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {306, 4}, {411, 9}, {147, 6}
        };
    }

    @Test(description = "BE: PUT /asset/{id}/coc/{id}, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1017")
    public void putAssetCocTest(int assetId, int assetCocId)
    {
        AssetCoc assetCoc = new AssetCoc(assetId, assetCocId);

        assetCoc.getDto().setTitle("Automated title");
        assetCoc.getDto().setActionTaken("An action was taken");
        assetCoc.getDto().setDueDate(Date.valueOf("2019-02-16"));
        assetCoc.getDto().setAffectedItems("this was the affected item");

        new AssetCoc(assetCoc.getDto(), assetId, assetCocId);
    }

    @Test(description = "BE: PUT /asset/{id}/coc/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1017")
    public void negativePutAssetCocTest(String assetId, String assetCocId, int statusCode, String errorMessage)
    {
        CoCDto assetCocDto = new AssetCoc(1, 1).getDto();
        assetCocDto.setId(Long.valueOf(assetCocId));
        assetCocDto.setTitle("Title");

        new ExpectedError(Endpoint.ASSET_COC,
                assetCocDto,
                new String[]{assetId, assetCocId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}

