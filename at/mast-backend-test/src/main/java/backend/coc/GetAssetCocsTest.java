package backend.coc;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCocPage;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetCocsTest
{

    @DataProvider(name = "AssetsPages")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 0, 10},
                {147, 0, 10},
                {239, 0, 10}
        };
    }

    @Test(description = "BE: GET /asset/{id}/coc/?page&size, Story 6.11",
            dataProvider = "AssetsPages")
    @Issue("LRT-1014")
    public void getAssetCocsTest(int assetId, int page, int size)
    {
        new AssetCocPage(assetId, page, size);
    }

    @Test(
            description = "BE: GET /asset/{id}/coc/?page&size, Story 6.11",
            dataProvider = "NegativeForPagesWithId",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1014")
    public void negativeGetAssetCocsTest(String assetId, String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_COC_PAGE, new String[]{assetId, page, size}, statusCode, message, Request.GET);
    }
}
