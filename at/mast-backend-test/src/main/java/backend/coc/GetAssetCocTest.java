package backend.coc;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCoc;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetCocTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1}, {1, 7}, {411, 9}, {306, 10}
        };
    }

    @Test(description = "BE: GET /asset/{id}/coc/{id}, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1016")
    public void getAssetCocTest(int assetId, int assetCocId)
    {
        new AssetCoc(assetId, assetCocId);
    }

    @Test(
            description = "BE: GET /asset/{id}/coc/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameterGeneric",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1016")
    public void negativeGetAssetCocTest(String assetId, String assetNoteId, int statusCode, String message)
    {
        new ExpectedError(Endpoint.ASSET_COC, new String[]{assetId, assetNoteId}, statusCode, message, Request.GET);
    }
}
