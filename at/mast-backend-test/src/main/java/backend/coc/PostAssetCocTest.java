package backend.coc;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetCoc;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostAssetCocTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {389}, {239}, {365}
        };
    }

    @Test(description = "BE: POST /asset/{id}/coc, Story 6.11",
            dataProvider = "Assets")
    @Issue("LRT-1015")
    public void postAssetCocTest(int assetId)
    {
        CoCDto assetCocDto = new CoCDto();
        LinkResource asset = new LinkResource();
        asset.setId((long) assetId);
        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) 1);

        assetCocDto.setAsset(asset);
        assetCocDto.setImposedDate(Date.valueOf("2018-02-16"));
        assetCocDto.setDueDate(Date.valueOf("2018-02-17"));
        assetCocDto.setDescription("This is an automated description");
        assetCocDto.setInheritedFlag(false);
        assetCocDto.setStatus(linkResource);
        assetCocDto.setRequireApproval(false);
        assetCocDto.setConfidentialityType(linkResource);
        assetCocDto.setTitle("Automated Coc title");

        new AssetCoc(assetCocDto, assetId);
    }

    @Test(description = "BE: POST /asset/{id}/coc, Story 6.11",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1015")
    public void negativePostAssetCocTest(String assetId, int statusCode, String message)
    {
        CoCDto assetCocDto = new AssetCoc(1, 1).getDto();
        assetCocDto.setId(null);

        new ExpectedError(Endpoint.ASSET_COC,
                assetCocDto,
                new String[]{assetId, ""},
                Request.POST,
                statusCode,
                message);
    }
}
