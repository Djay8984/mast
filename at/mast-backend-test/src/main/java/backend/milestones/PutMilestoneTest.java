package backend.milestones;

import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import model.milestone.MilestonePage;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PutMilestoneTest
{

    @Test(
            description = "MAST BE: PUT /case/{id}/milestone"
    )
    public void putMilestoneTest()
    {
        CaseMilestoneListDto milestoneList = new MilestonePage(173).getDto();
        List<CaseMilestoneDto> list = new ArrayList<>();

        CaseMilestoneDto milestone = milestoneList.getCaseMilestoneList().get(0);
        milestone.setCompletionDate(Date.from(Instant.now()));
        list.add(milestone);

        milestoneList.setCaseMilestoneList(list);
        new MilestonePage(173, milestoneList);
    }
}
