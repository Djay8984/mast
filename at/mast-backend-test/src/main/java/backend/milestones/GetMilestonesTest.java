package backend.milestones;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.milestone.MilestonePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetMilestonesTest
{

    @DataProvider(name = "Case")
    public Object[][] cases()
    {
        return new Object[][]{
                {1}, {14}, {7}
        };
    }

    @Test(
            description = "MAST BE: GET /case/{id}/milestone",
            dataProvider = "Case"
    )
    @Issue("LRT-747")
    public void getMilestoneTest(int id)
    {
        new MilestonePage(id);
    }

    @Test(
            description = "MAST BE Negative: GET /case/{id}/milestone",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-747")
    public void negativeGetMilestoneTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.MILESTONE, id, statusCode, message, Request.GET);
    }

}
