package backend.milestones;

//TODO - Refactor once R2 FE includes this functionality
/*
import com.baesystems.ai.lr.cases.dto.CaseMilestoneDto;
import com.frameworkium.core.ui.tests.BaseTest;
import lreg.pages.casesetup.CaseSetupPage;
import lreg.pages.dashboard.CreateCaseDashboardPage;
import lreg.pages.dashboard.DashboardPage;
import lreg.util.RestHelper;
import lreg.util.TestDataHelper;
import lreg.util.Verify;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class CheckGeneratedMilestonesTest extends BaseTest {

    @BeforeClass(alwaysRun = true)
    public void insertCaseThroughFrontEnd() throws SQLException {
        //add committed case - needs to be done through front end!
        CreateCaseDashboardPage createCaseDashboardPage = DashboardPage.open().then().clickCreateCase();
        CaseSetupPage caseSetupPage = createCaseDashboardPage.startNewCaseWithNewImoNumber();
        caseSetupPage.fillCaseSetupPageWithCorrectValues();

        //check nothing is there beforehand
        int caseId = TestDataHelper.getLastInsertedRow("mast_case_case") + 1;
        RestHelper.get("/case/" + caseId + "/milestone", HttpStatus.SC_NOT_FOUND);

        caseSetupPage.commitCase();
    }

    @Test(description = "Testing that the generated milestones for a committed case are correct")
    public void checkGeneratedMilestonesTest() throws SQLException {
        int caseId = TestDataHelper.getLastInsertedRow("mast_case_case");
        CaseMilestoneDto[] milestones = (CaseMilestoneDto[]) RestHelper.get(new CaseMilestoneDto[]{}, "/case/" + caseId + "/milestone");
        for (CaseMilestoneDto milestone: milestones) {
            Verify.milestones(Integer.toString(caseId), milestone);
        }
    }
}
*/