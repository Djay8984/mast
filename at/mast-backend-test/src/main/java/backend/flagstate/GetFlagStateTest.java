package backend.flagstate;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.flag.FlagState;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetFlagStateTest
{

    @DataProvider(name = "FlagState")
    public Object[][] flagState()
    {
        return new Object[][]{
                {5}, {101}, {200}
        };
    }

    @Test(
            description = "MAST BE: GET /flag-state/{flagStateId}",
            dataProvider = "FlagState"
    )
    @Issue("LRD-557")
    public void getFlagStateTest(int id)
    {
        new FlagState(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-557")
    public void negativeGetFlagStateTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.FLAG_STATE, id, statusCode, message, Request.GET);
    }

}
