package backend.flagstate;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.flag.FlagStatePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetFlagStatesTest
{

    @Test(
            description = "MAST BE: GET /flag-state/?page&size",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-552")
    public void getFlagStateTest(int page, int size)
    {
        new FlagStatePage(page, size);
    }

    @Test(
            description = "MAST BE Negative: GET /flag-state/?page&size",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-552")
    public void negativeGetFlagStateTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.FLAG_STATE_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
