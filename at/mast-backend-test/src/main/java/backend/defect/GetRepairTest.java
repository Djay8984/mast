package backend.defect;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.defect.Repair;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetRepairTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1, 1}, {2, 7}
        };
    }

    @Test(
            description = "MAST BE: GET /defect/{id}/repair/{repair-id}   Story 5.13",
            dataProvider = "Defect"
    )
    @Issue("LRD-535")
    public void getDefectRepairTest(int id, int repairId)
    {
        new Repair(id, repairId);
    }

    @Test(
            dataProvider = "NegativeTwoParameterDefects",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-535")
    public void negativeGetDefectRepairTest(String id, String repairId, int statusCode, String message)
    {
        if (id.equals("50000") && repairId.equals("1"))
            message = "Repair with id 1 is not linked to the Defect with id 50000";
        else if (repairId.equals("50000"))
            message = "Repair with id 50000 is not linked to the Defect with id 1";
        new ExpectedError(Endpoint.DEFECT_REPAIR_REPAIR_ID, new String[]{id, repairId}, statusCode, message, Request.GET);
    }
}
