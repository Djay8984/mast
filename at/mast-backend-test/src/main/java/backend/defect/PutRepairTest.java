package backend.defect;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.defect.Repair;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class PutRepairTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1, 1}, {2, 7}
        };
    }

    @Issue("LRT-732")
    @Test(
            description = "MAST BE: PUT /defect/{id}/repair/{repairId}   Story 5.13",
            dataProvider = "Defect"
    )
    //fixme - returns 404
    public void putDefectRepairTest(int id, int repairId)
    {
        Repair repair = new Repair(id, repairId);

        String uniqueString = Integer.toString(TestDataHelper.getUniqueYardNumber());
        repair.getDto().setDescription(uniqueString);

        repair = new Repair(repair.getDto(), id, repairId);
        assertThat(repair.getDto().getDescription()).isEqualTo(uniqueString);
    }

    @Test(description = "MAST BE: PUT /defect/{id}/repair/{repair-Id}   Story 5.13",
            dataProvider = "NegativeTwoParameterDefects",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-535")
    public void negativePutDefectRepairTest(String id, String repairId, int statusCode, String errorMessage)
    {
        if (id.equals("50000") && repairId.equals("1"))
            errorMessage = "Repair with id 1 is not linked to the Defect with id 50000";
        else if (repairId.equals("50000"))
            errorMessage = "Repair with id 50000 is not linked to the Defect with id 1";
        RepairDto defectRepairDto = new Repair(1, 1).getDto();
        defectRepairDto.setId(Long.valueOf(repairId));

        new ExpectedError(Endpoint.DEFECT_REPAIR_REPAIR_ID,
                defectRepairDto,
                new String[]{id, repairId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
