package backend.defect;

import model.defect.DefectRepairPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetRepairsTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1, 0, 10}, {2, 0, 10}
        };
    }

    @Test(
            description = "MAST BE: GET /defect/{id}/repair   Story 5.13",
            dataProvider = "Defect"
    )
    @Issue("LRD-535")
    public void getDefectRepairsTest(int id, int page, int size)
    {
        new DefectRepairPage(id, page, size);
    }
}
