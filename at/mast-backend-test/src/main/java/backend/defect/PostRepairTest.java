package backend.defect;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.defects.DefectItemLinkDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.defect.Repair;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostRepairTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1}, {2}
        };
    }

    @Issue("LRT-535")
    @Test(
            description = "MAST BE: POST /defect/{id}/repair/",
            dataProvider = "Defect"
    )
    public void postDefectRepairTest(int id) throws SQLException
    {
        RepairDto defectRepairDto = new RepairDto();

        defectRepairDto.setPromptThoroughRepair(false);
        defectRepairDto.setDescription("Repair 1");
        defectRepairDto.setRepairTypeDescription("Repair Type 1");
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        defectRepairDto.setDefect(linkResource);
        List<LinkResource> linkResourceList = new ArrayList<>();
        linkResourceList.add(linkResource);
        defectRepairDto.setRepairTypes(linkResourceList);
        linkResource = new LinkResource();
        linkResource.setId(3L);
        defectRepairDto.setRepairAction(linkResource);
        defectRepairDto.setMaterialsUsed(linkResourceList);

        List<RepairItemDto> repairs = new ArrayList<>();
        RepairItemDto repairItemDto = new RepairItemDto();
        DefectItemLinkDto defectItemLinkDto = new DefectItemLinkDto();
        linkResource = new LinkResource();
        linkResource.setId(1L);
        defectItemLinkDto.setItem(linkResource);
        defectItemLinkDto.setId(1L);
        repairItemDto.setItem(defectItemLinkDto);
        repairs.add(repairItemDto);
        defectRepairDto.setRepairs(repairs);
        defectRepairDto.setConfirmed(true);

        new Repair(defectRepairDto, id);
    }

    @Test(description = "MAST BE: POST /defect/{id}/repair/",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-535")
    public void negativePostDefectRepairTest(String id, int statusCode, String message)
    {
        RepairDto defectRepairDto = new Repair(1, 1).getDto();
        defectRepairDto.setId(null);

        new ExpectedError(Endpoint.DEFECT_REPAIR,
                defectRepairDto,
                new String[]{id, ""},
                Request.POST,
                statusCode,
                message);
    }
}
