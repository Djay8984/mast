package backend.defect;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.defect.Defect;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GetDefectTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1}, {2}
        };
    }

    @Test(
            description = "MAST BE: GET /defect/{id}",
            dataProvider = "Defect"
    )
    public void getDefectTest(int id)
    {
        new Defect(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    public void negativeGetDefectTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.DEFECT, id, statusCode, message, Request.GET);
    }
}
