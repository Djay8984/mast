package backend.defect;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import constant.Endpoint;
import constant.Request;
import model.defect.Defect;
import model.expectederror.ExpectedError;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PostDefectTest
{

    @Test(
            description = "MAST BE: POST /defect/"
    )
    public void postDefectTest() throws SQLException
    {
        DefectDto defectDto = new DefectDto();

        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        List<LinkResource> linkResourceList = new ArrayList<>();
        linkResourceList.add(linkResource);

        defectDto.setInternalId("A Defect Code");
        defectDto.setFirstFrameNumber(1);
        defectDto.setLastFrameNumber(2);
        defectDto.setPromptThoroughRepair(false);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 2);
        defectDto.setIncidentDate(cal.getTime());
        defectDto.setIncidentDescription("A description of the incident");
        defectDto.setTitle("A Title");
        defectDto.setAsset(linkResource);
        JobDefectDto jobDefectDto = new JobDefectDto();
        jobDefectDto.setJob(linkResource);
        List<JobDefectDto> jobDefectDtos = new ArrayList<>();
        defectDto.setJobs(jobDefectDtos);
        DefectCategoryDto defectCategoryDto = new DefectCategoryDto();
        defectCategoryDto.setCategoryLetter("A");
        defectCategoryDto.setParent(linkResource);
        defectDto.setDefectCategory(defectCategoryDto);
        defectDto.setDefectStatus(linkResource);
        DefectDefectValueDto defectDefectValueDto = new DefectDefectValueDto();
        defectDefectValueDto.setOtherDetails("Other Details");
        DefectValueDto defectValueDto = new DefectValueDto();
        defectDefectValueDto.setDefectValue(defectValueDto);
        List<DefectDefectValueDto> values = new ArrayList<>();
        values.add(defectDefectValueDto);
        defectDto.setValues(values);
        DefectItemDto defectItemDto = new DefectItemDto();
        ItemLightDto itemLightDto = new ItemLightDto();
        defectItemDto.setItem(itemLightDto);
        List<DefectItemDto> items = new ArrayList<>();
        items.add(defectItemDto);
        defectDto.setItems(items);
    }

    @Test(description = "MAST BE: POST /defect/"
    )
    public void negativePostDefectTest()
    {
        DefectDto defectDto = new Defect(1).getDto();
        defectDto.setAsset(null);
        defectDto.setId(null);

        new ExpectedError(Endpoint.DEFECT_POST,
                defectDto,
                new String[]{},
                Request.POST,
                HttpStatus.SC_BAD_REQUEST,
                "Validation failed with errors: asset may not be null.");
    }
}
