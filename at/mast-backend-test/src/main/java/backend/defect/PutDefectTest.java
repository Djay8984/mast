package backend.defect;

import com.baesystems.ai.lr.dto.defects.DefectDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.defect.Defect;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.google.common.truth.Truth.assertThat;

public class PutDefectTest
{

    @DataProvider(name = "Defect")
    public Object[][] defect()
    {
        return new Object[][]{
                {1}, {2}
        };
    }

    @Test(
            description = "MAST BE: PUT /defect/{id}/",
            dataProvider = "Defect"
    )
    public void putDefectTest(int id)
    {
        Defect defect = new Defect(id);

        String uniqueString = Integer.toString(TestDataHelper.getUniqueYardNumber());
        defect.getDto().setInternalId(uniqueString);

        defect = new Defect(defect.getDto(), id);
        assertThat(defect.getDto().getInternalId()).isEqualTo(uniqueString);
    }

    @Test(description = "MAST BE: PUT /defect/{id}",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    public void negativePutDefectTest(String id, int statusCode, String errorMessage)
    {
        DefectDto defectDto = new Defect(1).getDto();
        defectDto.setId(Long.valueOf(id));
        defectDto.setCourseOfActionCount(1);

        new ExpectedError(Endpoint.DEFECT,
                defectDto,
                new String[]{id},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
