/*
Important: Removed as part of modularisation, removing local jars

package backend.cdh;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import helper.TestDataHelper;

import java.net.MalformedURLException;
import java.sql.SQLException;

public class CdhIntegrationTest {
    //this needs adding to tomcat so it can talk to this endpoint
    private static final String url = "http://localhost:8080/mast-integration/api/v2/createUpdateOrgPtService";
    private final String username = "SOAP_USER";
    private final String password = "PASSWORD";

    @Issue("LRT-375")
    @Test(description = "That a user can create a customer through a CDH message")
    public void createCustomer() throws MalformedURLException, SQLException, InterruptedException {
        CreateUpdateOrgPt newCustomerMessage = (CreateUpdateOrgPt) setupFactory().create();
        int previousRow = TestDataHelper.getLastInsertedRow("mast_lrp_employee");
        int nextRow = previousRow+1;

        OrgDetailsType orgDetails = createOrgDetails(String.valueOf(nextRow), String.valueOf(nextRow), "I", "Created automated customers");
        CdhDetails.Organization organisation = new CdhDetails.Organization();
        organisation.setOrgDetails(orgDetails);
        CdhDetails details = new CdhDetails();

        details.setOrganization(organisation);
        newCustomerMessage.createUpdateOrgOp(details);

        int newRow;
        do {
            Thread.sleep(5000);
            newRow = TestDataHelper.getLastInsertedRow("mast_lrp_party");
        }
        while (previousRow == newRow);

        //Verify.party(orgDetails.getPartyId(), orgDetails.getCustomerName(), 2, orgDetails.getCustomerId());
    }

    @Issue("LRT-393")
    @Test(description = "That a user can update a customer through a CDH message")
    public void updateCustomer() throws SQLException, InterruptedException {
        CreateUpdateOrgPt newCustomerMessage = (CreateUpdateOrgPt) setupFactory().create();

        OrgDetailsType orgDetails = createOrgDetails("2", "2", "I", "Updated automated customers");
        CdhDetails.Organization organisation = new CdhDetails.Organization();
        organisation.setOrgDetails(orgDetails);

        ContactDetailsType contactDetailsType = new ContactDetailsType();
        contactDetailsType.setContactId("9999");

        CdhDetails.Organization.ContactDetails contactDetails = new CdhDetails.Organization.ContactDetails();
        contactDetails.getContact().add(contactDetailsType);

        CdhDetails.Organization.AddressDetails address = new CdhDetails.Organization.AddressDetails();
        address.setAddress(createAddress("Description", "1 Wit-ma-wot-ma-gate", "Line", "Another Line", "Yet another Line", "York","GB", "North Yorkshire", "YO1 2UG"));
        organisation.setAddressDetails(address);
        organisation.setContactDetails(contactDetails);

        CdhDetails details = new CdhDetails();
        details.setOrganization(organisation);
        newCustomerMessage.createUpdateOrgOp(details);

        Thread.sleep(5000);
        //Verify.party(orgDetails.getPartyId(), orgDetails.getCustomerName(), 2, orgDetails.getCustomerId());
    }

    @Issue("LRT-375")
    @Test(description = "That a user can create a customer with all the information with a CDH message")
    public void createCustomerComplete() throws SQLException, InterruptedException {
        CreateUpdateOrgPt newCustomerMessage = (CreateUpdateOrgPt) setupFactory().create();
        int previousRow = TestDataHelper.getLastInsertedRow("mast_cdh_party");
        int nextRow = previousRow +1;

        AccountDetailsType account = createAccount();
        CdhDetails.Accounts accounts = new CdhDetails.Accounts();
        accounts.getAccount().add(account);

        CdhDetails.Organization organisation = new CdhDetails.Organization();
        OrgDetailsType orgDetails = createOrgDetails(String.valueOf(nextRow),String.valueOf(nextRow), "A", "Automated customerS");
        organisation.setOrgDetails(orgDetails);

        EmailType emailType = new EmailType();
        emailType.setEmail("a.customer@customerwork.com");
        ContactDetailsType.Emails emails = new ContactDetailsType.Emails();
        emails.getEmail().add(emailType);
        ContactDetailsType contactDetailsType = new ContactDetailsType();
        contactDetailsType.setContactId("9999");
        contactDetailsType.setEmails(emails);

        CdhDetails.Organization.ContactDetails contactDetails = new CdhDetails.Organization.ContactDetails();
        contactDetails.getContact().add(contactDetailsType);

        CdhDetails.Organization.AddressDetails address = new CdhDetails.Organization.AddressDetails();
        address.setAddress(createAddress("Description", "10 Long Tongue Scrog Lane", "Houses Hill", "Another Line", "Yet another Line", "Huddersfield","GB", "West Yorkshire", "HD5 3RF"));
        organisation.setAddressDetails(address);
        organisation.setContactDetails(contactDetails);

        CdhDetails details = new CdhDetails();
        details.setOrganization(organisation);
        details.setAccounts(accounts);
        newCustomerMessage.createUpdateOrgOp(details);

        int newRow;
        do {
            Thread.sleep(5000);
            newRow = TestDataHelper.getLastInsertedRow("mast_cdh_party");
        }
        while (previousRow == newRow);
        //Verify.party(orgDetails.getPartyId(),orgDetails.getCustomerName(), 1, orgDetails.getCustomerId());
        //Verify.partyAddress(address);
    }

    @Issue("LRT-393")
    @Test(description = "That a user can update a customer changing all the saved information with a CDH message")
    public void updateCustomerComplete() throws SQLException, InterruptedException {
        CreateUpdateOrgPt newCustomerMessage = (CreateUpdateOrgPt) setupFactory().create();

        AccountDetailsType account = createAccount();

        CdhDetails.Accounts accounts = new CdhDetails.Accounts();
        accounts.getAccount().add(account);

        CdhDetails.Organization organisation = new CdhDetails.Organization();
        OrgDetailsType orgDetails = createOrgDetails("6", "6", "A", "Newly Updated Customer");
        organisation.setOrgDetails(orgDetails);

        EmailType emailType = new EmailType();
        emailType.setEmail("a.customer@customerwork.com");

        ContactDetailsType.Emails emails = new ContactDetailsType.Emails();
        emails.getEmail().add(emailType);

        ContactDetailsType contactDetailsType = new ContactDetailsType();
        contactDetailsType.setContactId("9999");
        contactDetailsType.setEmails(emails);

        CdhDetails.Organization.ContactDetails contactDetails = new CdhDetails.Organization.ContactDetails();
        contactDetails.getContact().add(contactDetailsType);

        CdhDetails.Organization.AddressDetails address = new CdhDetails.Organization.AddressDetails();
        address.setAddress(createAddress("Description", "1 Wit-ma-wot-ma-gate", "Line", "Another Line", "Yet another Line", "York","GB", "North Yorkshire", "YO1 2UG"));
        organisation.setAddressDetails(address);
        organisation.setContactDetails(contactDetails);

        CdhDetails details = new CdhDetails();
        details.setOrganization(organisation);
        details.setAccounts(accounts);
        newCustomerMessage.createUpdateOrgOp(details);

        Thread.sleep(5000);
        //Verify.party(orgDetails.getPartyId(), orgDetails.getCustomerName(), 1, orgDetails.getCustomerId());
        //Verify.partyAddress(address, orgDetails.getPartyId());
    }

    public JaxWsProxyFactoryBean setupFactory(){
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(CreateUpdateOrgPt.class);
        factory.setUsername(username);
        factory.setPassword(password);
        factory.setAddress(url);
        return factory;
    }

    private OrgDetailsType createOrgDetails(String partyId, String customerId, String status, String customerName)
    {
        OrgDetailsType orgDetails = new OrgDetailsType();
        orgDetails.setPartyId(partyId);
        orgDetails.setCustomerId(customerId);
        orgDetails.setStatus(status);
        orgDetails.setCustomerName(customerName);
        return orgDetails;
    }

    private AccountDetailsType createAccount()
    {
        AccountDetailsType account = new AccountDetailsType();
        account.setAccountNumber("1234");
        account.setJdeReferenceNumber("1234");
        account.setClientFacingOffice("N");
        account.setPartyId("1");
        account.setStream("M");

        AddressDetailsType address = createAddress("Description", "191a Sandon Road", "Beaconside", "Another Line", "Yet another Line", "Stafford","GB", "Staffordshire", "ST163NH");
        AccountDetailsType.AddressDetails addressDetailsTypeAccounts = new AccountDetailsType.AddressDetails();
        addressDetailsTypeAccounts.setAddress(address);
        account.setAddressDetails(addressDetailsTypeAccounts);
        return account;
    }

    private AddressDetailsType createAddress(String description, String line1, String line2, String line3,
                                             String line4, String city, String country, String county, String postcode)
    {
        AddressDetailsType address = new AddressDetailsType();
        address.setAddressDescription(description);
        address.setAddressLine1(line1);
        address.setAddressLine2(line2);
        address.setAddressLine3(line3);
        address.setAddressLine4(line4);
        address.setCity(city);
        address.setCountry(country);
        address.setCounty(county);
        address.setPostalCode(postcode);
        return address;
    }
}
*/