package backend.job;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import model.job.Job;
import org.joda.time.DateTime;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostJobTest
{

    @Issue("LRT-731")
    @Test(
            description = "MAST BE: POST /job/"
    )
    public void postJobTest() throws SQLException
    {
        JobDto jobDto = new JobDto();

        jobDto.setCreatedOn(DateTime.now().toDate());

        List<EmployeeLinkDto> employeeLinkDtos = new ArrayList<>();
        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) 2);

        EmployeeLinkDto employeeLinkDto = new EmployeeLinkDto();
        employeeLinkDto.setId((long) 1);
        employeeLinkDto.setEmployeeRole(linkResource);

        NamedLinkResource employee = new NamedLinkResource();
        employee.setId(1L);

        employeeLinkDto.setLrEmployee(employee);
        employeeLinkDtos.add(employeeLinkDto);

        List<OfficeLinkDto> officeDtos = new ArrayList<>();
        OfficeLinkDto officeLinkDto = new OfficeLinkDto();

        linkResource = new LinkResource();
        linkResource.setId((long) 2);

        //SDO role  in mast_ref_officerole = 1
        LinkResource officeRole = new LinkResource();
        officeRole.setId((long) 1);

        OfficeDto officeDto = new OfficeDto();
        officeDto.setId(8L);

        officeLinkDto.setId((long) 8);
        officeLinkDto.setOfficeRole(officeRole);
        officeLinkDto.setOffice(linkResource);
        officeDtos.add(officeLinkDto);

        LinkResource aCase = new LinkResource();
        aCase.setId(1L);

        jobDto.setAsset(new LinkVersionedResource(linkResource.getId(), null));
        jobDto.setEmployees(employeeLinkDtos);
        jobDto.setJobStatus(linkResource);
        jobDto.setOffices(officeDtos);
        jobDto.setLocation("Automated location");
        jobDto.setWrittenServiceResponseSentDate(Date.valueOf("2018-07-22"));
        jobDto.setWrittenServiceRequestReceivedDate(Date.valueOf("2018-07-22"));
        jobDto.setDescription("Automated description");
        jobDto.setEtaDate(Date.valueOf("2018-05-22"));
        jobDto.setEtdDate(Date.valueOf("2018-06-22"));
        jobDto.setaCase(aCase);
        jobDto.setRequestedAttendanceDate(Date.valueOf("2018-04-14"));
        jobDto.setScopeConfirmed(true);

        new Job(jobDto);
    }
}
