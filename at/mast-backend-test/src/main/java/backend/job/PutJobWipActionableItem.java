package backend.job;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.expectederror.ExpectedError;
import model.job.JobWipActionableItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class PutJobWipActionableItem
{

    @DataProvider(name = "Job")
    public Object[][] job()
    {
        return new Object[][]{
                {1, 19}, {2, 21}
        };
    }

    @Test(
            description = "MAST BE: PUT /job/{id}/wip-actionable-item",
            dataProvider = "Job"
    )
    @Issue("LRT-1946")
    //fixme - returning 404
    public void putJobWipActionableItemTest(int id, int actionableItemId)
    {
        ActionableItemDto jobWipActionableItem = new JobWipActionableItem(id, actionableItemId).getDto();

        jobWipActionableItem.setDescription(Integer.toString(TestDataHelper.getRandomNumber(0, 99999999)));
        jobWipActionableItem.setRequireApproval(true);
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        jobWipActionableItem.setCategory(linkResource);
        jobWipActionableItem.setTitle(Integer.toString(TestDataHelper.getRandomNumber(0, 99999999)));
        jobWipActionableItem.setConfidentialityType(linkResource);
        jobWipActionableItem.setStatus(linkResource);
        new JobWipActionableItem(id, jobWipActionableItem.getId().intValue(), jobWipActionableItem);
    }

    @Test(
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1946")
    public void negativePutJobWipActionableItemTest(String jobId, String actionableItemId, int statusCode, String message)
    {
        if (actionableItemId.equals("50000"))
            message = "WIP Actionable item with id 50000 does not exist for Job 1.";
        ActionableItemDto jobWipActionableItem = new JobWipActionableItem(2, 21).getDto();
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        jobWipActionableItem.getJob().setId(Long.parseLong(jobId));
        jobWipActionableItem.setCategory(linkResource);
        jobWipActionableItem.setTitle("Automation");
        jobWipActionableItem.setConfidentialityType(linkResource);
        jobWipActionableItem.setRequireApproval(false);
        new ExpectedError(
                Endpoint.JOB_WIP_ACTIONABLE_ITEM,
                jobWipActionableItem,
                new String[]{jobId, actionableItemId},
                Request.PUT,
                statusCode,
                message
        );
    }

}
