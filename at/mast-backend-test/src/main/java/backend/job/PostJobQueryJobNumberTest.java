package backend.job;

import com.baesystems.ai.lr.dto.query.JobQueryDto;
import model.job.JobQueryPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class PostJobQueryJobNumberTest
{

    @DataProvider(name = "JobQuery")
    public Object[][] jobQueryOne()
    {
        return new Object[][]{
                {161234569L},
                {161234589L},
                {161234631L}
        };
    }

    @Issue("LRT-1943")
    @Test(description = "BE: POST /job/query API to include job number parameter",
            dataProvider = "JobQuery"

    )
    public void postJobOneQueryTest(Long jobNumber)
    {
        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setSearch(jobNumber);
        new JobQueryPage(queryDto);
    }
}
