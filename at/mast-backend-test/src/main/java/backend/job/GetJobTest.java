package backend.job;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.Job;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobTest
{

    @DataProvider(name = "Job")
    public Object[][] job()
    {
        return new Object[][]{
                {1}, {4}, {6}
        };
    }

    @Test(
            description = "MAST BE: GET /job/{id}",
            dataProvider = "Job"
    )
    @Issue("LRD-557")
    public void getJobTest(int id)
    {
        new Job(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-557")
    public void negativeGetJobTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.JOB, id, statusCode, message, Request.GET);
    }

}
