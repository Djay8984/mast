package backend.job;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobWipActionableItemPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobWipActionableItem
{

    @DataProvider(name = "Job")
    public Object[][] job()
    {
        return new Object[][]{
                {1}, {2}
        };
    }

    @Test(
            description = "MAST BE: GET /job/{id}/wip-actionable-item",
            dataProvider = "Job"
    )
    @Issue("LRT-1944")
    public void getJobWipActionableItemTest(int id)
    {
        new JobWipActionableItemPage(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1944")
    public void negativeGetJobWipActionableItemTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.JOB_WIP_ACTIONABLE_ITEM, new String[]{id, ""}, statusCode, message, Request.GET);
    }

}
