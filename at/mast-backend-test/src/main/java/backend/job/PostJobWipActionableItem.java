package backend.job;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobWipActionableItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Date;

public class PostJobWipActionableItem
{
    @DataProvider(name = "Job")
    public Object[][] job()
    {
        return new Object[][]{
                {1}, {13}, {59879809}
        };
    }

    @Test(description = "BE: New API POST /job/x/wip-actionable-item",
            dataProvider = "Job")
    @Issue("LRT-1945")
    public void postJobWipActionableItem(int id)
    {
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        ItemLightDto itemLight = new ItemLightDto();
        itemLight.setId(1L);

        ActionableItemDto actionableItemDto = new ActionableItemDto();
        actionableItemDto.setTitle("Automated Actionable Item");
        actionableItemDto.setDescription("Automated Actionable Item Description");
        actionableItemDto.setImposedDate(new Date());
        actionableItemDto.setDueDate(new Date());
        actionableItemDto.setRequireApproval(true);
        actionableItemDto.setEditableBySurveyor(true);
        actionableItemDto.setCategory(linkResource);
        actionableItemDto.setConfidentialityType(linkResource);
        actionableItemDto.setStatus(linkResource);
        actionableItemDto.setDefect(linkResource);
        actionableItemDto.setAssetItem(itemLight);
        actionableItemDto.setEmployee(linkResource);
        linkResource = new LinkResource();
        linkResource.setId((long) id);
        actionableItemDto.setJob(linkResource);
        new JobWipActionableItem(actionableItemDto, id);
    }

    @Test(dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            description = "BE Negative: New API POST /job/x/wip-actionable-item")
    @Issue("LRT-1945")
    public void negativePostJobWipActionableItemTest(String id, int statusCode, String message)
    {
        ActionableItemDto jobWipActionableItem = new JobWipActionableItem(1, 19).getDto();
        jobWipActionableItem.setId(null);
        jobWipActionableItem.getJob().setId(Long.parseLong(id));
        new ExpectedError(Endpoint.JOB_WIP_ACTIONABLE_ITEM,
                jobWipActionableItem,
                new String[]{id, ""},
                Request.POST,
                statusCode,
                message);
    }
}
