package backend.job;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobsTest
{

    @Test(
            description = "MAST BE: GET /job/?page&size",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void getJobsTest(int page, int size)
    {
        new JobPage(Endpoint.JOB_PAGE, page, size);
    }

    @Test(
            description = "MAST BE Negative: GET /job/?page&size",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void negativeGetJobsTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.JOB_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
