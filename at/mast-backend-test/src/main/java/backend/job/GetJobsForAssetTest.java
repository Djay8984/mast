package backend.job;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.job.JobPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetJobsForAssetTest
{

    @DataProvider(name = "Asset")
    public static Object[][] asset()
    {
        return new Object[][]{
                {1}, {101}
        };
    }

    @Test(
            description = "MAST BE: GET /asset/{id}/job",
            dataProvider = "Asset"
    )
    @Issue("LRD-227")
    public void getJobsForAssetTest(int id)
    {
        new JobPage(Endpoint.JOBS_FOR_ASSET, id);
    }

    @Test(
            description = "MAST BE Negative: GET /asset/{id}/job",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void negativeGetJobsForAssetTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.JOBS_FOR_ASSET, id, statusCode, message, Request.GET);
    }

}
