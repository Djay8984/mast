package backend.job;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.expectederror.ExpectedError;
import model.job.Job;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class PutJobTest
{

    @DataProvider(name = "Job")
    public Object[][] job()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRT-732")
    @Test(
            description = "MAST BE: PUT /job/{id}",
            dataProvider = "Job"
    )
    public void putJobTest(int id)
    {
        Job job = new Job(id);

        String uniqueString = Integer.toString(TestDataHelper.getUniqueYardNumber());
        job.getDto().setNote(uniqueString);

        List<OfficeLinkDto> officeDtos = new ArrayList<>();
        OfficeLinkDto officeLinkDto = new OfficeLinkDto();

        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) 1);

        OfficeDto officeDto = new OfficeDto();
        officeDto.setId(8L);

        officeLinkDto.setId((long) 8);
        officeLinkDto.setOfficeRole(linkResource);
        officeLinkDto.setOffice(linkResource);
        officeDtos.add(officeLinkDto);

        job.getDto().setOffices(officeDtos);

        job = new Job(job.getDto(), id);
        assertThat(job.getDto().getNote()).isEqualTo(uniqueString);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-557")
    public void negativeJobTest(String id, int statusCode, String errorMessage)
    {
        JobDto job = new Job(1).getDto();
        job.setId(Long.valueOf(id));
        new ExpectedError(Endpoint.JOB, job, id, Request.PUT, statusCode, errorMessage);
    }
}
