package backend.job;

import com.baesystems.ai.lr.dto.query.JobQueryDto;
import model.job.JobQueryPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.ArrayList;
import java.util.List;

public class PostJobQueryTest
{

    @DataProvider(name = "JobQueryOne")
    public Object[][] jobQueryOne()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRT-1265")
    @Test(description = "BE: POST /job/query",
            dataProvider = "JobQueryOne"
    )
    public void postJobOneQueryTest(int id)
    {
        List<Long> list = new ArrayList();
        list.add((long) id);

        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setJobStatusId(list);

        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setEmployeeId(list);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setOfficeId(list);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setAssetId(list);
        new JobQueryPage(queryDto);
    }

    @DataProvider(name = "JobQueryTwo")
    public Object[][] jobQueryTwo()
    {
        return new Object[][]{
                {1, 2}, {2, 3}, {3, 4}
        };
    }

    @Issue("LRT-1265")
    @Test(description = "BE: POST /job/query",
            dataProvider = "JobQueryTwo"
    )
    public void postJobTwoQueryTest(int id1, int id2)
    {
        List<Long> list1 = new ArrayList();
        list1.add((long) id1);

        List<Long> list2 = new ArrayList();
        list2.add((long) id2);

        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setJobStatusId(list1);
        queryDto.setEmployeeId(list2);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setEmployeeId(list1);
        queryDto.setOfficeId(list2);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setOfficeId(list1);
        queryDto.setAssetId(list2);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setAssetId(list1);
        queryDto.setJobStatusId(list2);
        new JobQueryPage(queryDto);
    }

    @DataProvider(name = "JobQueryThree")
    public Object[][] jobQueryThree()
    {
        return new Object[][]{
                {1, 2, 3}, {1, 3, 3}, {1, 4, 6}
        };
    }

    @Issue("LRT-1265")
    @Test(description = "BE: POST /job/query",
            dataProvider = "JobQueryThree"
    )
    public void postJobThreeQueryTest(int id1, int id2, int id3)
    {
        List<Long> list1 = new ArrayList();
        list1.add((long) id1);

        List<Long> list2 = new ArrayList();
        list2.add((long) id2);

        List<Long> list3 = new ArrayList();
        list3.add((long) id3);

        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setJobStatusId(list1);
        queryDto.setEmployeeId(list2);
        queryDto.setOfficeId(list3);
        new JobQueryPage(queryDto);

        queryDto = new JobQueryDto();
        queryDto.setEmployeeId(list1);
        queryDto.setOfficeId(list2);
        queryDto.setAssetId(list3);
        new JobQueryPage(queryDto);
    }

    @DataProvider(name = "JobQueryFour")
    public Object[][] jobQueryFour()
    {
        return new Object[][]{
                {1, 1, 1, 1}, {5, 3, 1, 501}, {1, 4, 1, 6}
        };
    }

    @Issue("LRT-1265")
    @Test(description = "BE: POST /job/query",
            dataProvider = "JobQueryFour")
    public void postJobFourQueryTest(int id1, int id2, int id3, int id4)
    {
        List<Long> list1 = new ArrayList();
        list1.add((long) id1);

        List<Long> list2 = new ArrayList();
        list2.add((long) id2);

        List<Long> list3 = new ArrayList();
        list3.add((long) id3);

        List<Long> list4 = new ArrayList();
        list4.add((long) id4);

        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setJobStatusId(list1);
        queryDto.setEmployeeId(list2);
        queryDto.setOfficeId(list3);
        queryDto.setAssetId(list4);
        new JobQueryPage(queryDto);
    }

    @DataProvider(name = "JobQueryMultipleFour")
    public Object[][] jobQueryMultipleFour()
    {
        return new Object[][]{
                {1, 2, 3, 4}, {5, 3, 1, 501}, {5, 4, 1, 6}
        };
    }

    @Issue("LRT-1265")
    @Test(description = "BE: POST /job/query",
            dataProvider = "JobQueryMultipleFour")
    public void postJobMultipleFourQueryTest(int id1, int id2, int id3, int id4)
    {
        List<Long> list1 = new ArrayList();
        list1.add((long) id1);
        list1.add((long) id2);
        list1.add((long) id3);
        list1.add((long) id4);

        List<Long> list2 = new ArrayList();
        list2.add((long) id1);
        list2.add((long) id2);
        list2.add((long) id3);
        list2.add((long) id4);

        List<Long> list3 = new ArrayList();
        list3.add((long) id1);
        list3.add((long) id2);
        list3.add((long) id3);
        list3.add((long) id4);

        List<Long> list4 = new ArrayList();
        list4.add((long) id1);
        list4.add((long) id2);
        list4.add((long) id3);
        list4.add((long) id4);

        JobQueryDto queryDto = new JobQueryDto();
        queryDto.setJobStatusId(list1);
        queryDto.setEmployeeId(list2);
        queryDto.setOfficeId(list3);
        queryDto.setAssetId(list4);
        new JobQueryPage(queryDto);
    }
}
