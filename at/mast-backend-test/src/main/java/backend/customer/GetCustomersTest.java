package backend.customer;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetCustomersTest
{

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-225")
    public void getCustomersTest(int page, int size)
    {
    }

    @Test(
            description = "Uses RestAssured to test the get asset endpoint",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-225")
    public void negativeGetCustomersTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.CUSTOMER_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
