package backend.customer;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.customer.Customer;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetCustomerTest
{

    @DataProvider(name = "Customer")
    public Object[][] customer()
    {
        return new Object[][]{
                {1}, {4}, {7}
        };
    }

    @Test(
            description = "MAST BE: GET /customer/{customerId}",
            dataProvider = "Customer",
            enabled = false
    )
    @Issue("LRD-227")
    public void getCustomerTest(int id)
    {
        new Customer(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-227")
    public void negativeGetCustomerTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.CUSTOMER, id, statusCode, message, Request.GET);
    }

}
