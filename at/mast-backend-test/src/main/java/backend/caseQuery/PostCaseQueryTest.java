package backend.caseQuery;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import helper.TestDataHelper;
import model.cases.Case;
import model.cases.CaseQuery;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostCaseQueryTest
{

    @DataProvider(name = "Case")
    public Object[][] cases()
    {
        return new Object[][]{
                {1}, {20}, {99}
        };
    }

    @Test(description = "MAST BE: GET /case/query with everything filled out",
            dataProvider = "Case")
    @Issue("LRT-1254")
    public void postCaseQueryAllTest(int id) throws SQLException
    {
        List<Long> assets = new ArrayList<>();
        List<Long> status = new ArrayList<>();
        CaseDto caseDto = new Case(id).getDto();

        assets.add(caseDto.getAsset().getId());
        status.add(caseDto.getCaseStatus().getId());
        List<Long> employees = TestDataHelper.getEmployeeId(1);

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setAssetId(assets);
        caseQueryDto.setCaseStatusId(status);
        caseQueryDto.setEmployeeId(employees);
        new CaseQuery(caseQueryDto);
    }

    @Test(description = "MAST BE:POST /case/query with only one list",
            dataProvider = "Case")
    @Issue("LRT-1254")
    public void postCaseQueryOneTest(int caseId)
    {
        CaseDto caseDto = new Case(caseId).getDto();

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        List<Long> asset = new ArrayList<>();
        asset.add(caseDto.getAsset().getId());
        caseQueryDto.setAssetId(asset);
        new CaseQuery(caseQueryDto);

        caseQueryDto = new CaseQueryDto();
        List<Long> status = new ArrayList<>();
        status.add(caseDto.getCaseStatus().getId());
        caseQueryDto.setCaseStatusId(status);
        new CaseQuery(caseQueryDto);
    }

    @DataProvider(name = "MultipleCases")
    public Object[][] multipleCases()
    {
        return new Object[][]{
                {1, 20, 99},
                {101, 74, 35},
                {106, 5, 27}
        };
    }

    @Test(description = "MAST BE:POST /case/query with multiple items",
            dataProvider = "MultipleCases")
    @Issue("LRT-1254")
    public void postCaseQueryMultiplesTest(int id1, int id2, int id3) throws SQLException
    {
        CaseDto caseDto1 = new Case(id1).getDto();
        CaseDto caseDto2 = new Case(id2).getDto();
        CaseDto caseDto3 = new Case(id3).getDto();

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        List<Long> assets = new ArrayList<>();
        assets.add(caseDto1.getAsset().getId());
        assets.add(caseDto2.getAsset().getId());
        assets.add(caseDto3.getAsset().getId());
        caseQueryDto.setAssetId(assets);
        new CaseQuery(caseQueryDto);

        caseQueryDto = new CaseQueryDto();
        List<Long> status = new ArrayList<>();
        status.add(caseDto1.getCaseStatus().getId());
        status.add(caseDto2.getCaseStatus().getId());
        status.add(caseDto3.getCaseStatus().getId());
        caseQueryDto.setCaseStatusId(status);
        new CaseQuery(caseQueryDto);

        caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(TestDataHelper.getEmployeeId(id1));
        new CaseQuery(caseQueryDto);
    }
}
