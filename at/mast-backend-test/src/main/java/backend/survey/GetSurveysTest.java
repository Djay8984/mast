package backend.survey;

import constant.Endpoint;
import constant.ReferenceTable;
import constant.TableMap;
import model.referencedata.ReferenceData;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetSurveysTest
{

    @Issue("LRD-992")
    @Test(
            description = "MAST BE: GET /survey-status/",
            enabled = false
    )
    public void getSurveysTest()
    {
        new ReferenceData(Endpoint.SURVEY_STATUS, ReferenceTable.SURVEY, TableMap.SURVEY_REFERENCE);
    }
}
