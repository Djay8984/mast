package backend.survey;

import constant.Endpoint;
import constant.ReferenceTable;
import constant.Request;
import constant.TableMap;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.referencedata.ReferenceData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetSurveyTest
{

    @DataProvider(name = "Survey")
    public static Object[][] survey()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRD-993")
    @Test(
            description = "MAST BE: GET /survey-status/{id}",
            dataProvider = "Survey",
            enabled = false
    )
    public void getSurveyTest(int id)
    {
        new ReferenceData(Endpoint.SURVEY_STATUS, ReferenceTable.SURVEY, TableMap.SURVEY_REFERENCE, id);
    }

    @Issue("LRD-993")
    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    public void negativeGetSurveyTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.SURVEY_STATUS, id, statusCode, message, Request.GET);
    }
}
