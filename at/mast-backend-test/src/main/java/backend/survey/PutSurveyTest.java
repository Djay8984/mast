package backend.survey;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.surveyor.Survey;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutSurveyTest
{

    @DataProvider(name = "Job")
    public static Object[][] job()
    {
        return new Object[][]{
                {1L, 1L}, {4L, 4L}, {59879797L, 7L}
        };
    }

    @Test(description = "BE:PUT /job/*jobId*/survey/*surveyId*, Story 4.9",
            dataProvider = "Job")
    @Issue("LRT-994")
    public void putSurveyTest(Long jobId, Long surveyId)
    {
        new Survey(createSurvey(jobId, surveyId), jobId.intValue(), surveyId.intValue());
    }

    @Test(description = "Negative BE:PUT /job/*jobId*/survey/*surveyId*, Story 4.9",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-994")
    public void negativePutSurveyTest(String jobId, String surveyId, int statusCode, String message)
    {

        new ExpectedError(Endpoint.SURVEY,
                createSurvey(Long.parseLong(jobId), Long.parseLong(surveyId)),
                new String[]{jobId, surveyId},
                Request.PUT,
                statusCode,
                message);
    }

    public SurveyDto createSurvey(Long jobId, Long surveyId)
    {
        SurveyDto survey = new SurveyDto();

        LinkResource job = new LinkResource();
        job.setId(jobId);

        ReferenceDataDto referenceDataDto = new ReferenceDataDto();
        referenceDataDto.setId(1L);

        ServiceCatalogueDto serviceCatalogueDto = new ServiceCatalogueDto();
        ProductCatalogueDto productCatalogueDto = new ProductCatalogueDto();
        productCatalogueDto.setId(1L);
        serviceCatalogueDto.setProductCatalogue(productCatalogueDto);
        serviceCatalogueDto.setId(1L);

        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);

        ScheduledServiceDto serviceDto = new ScheduledServiceDto();
        serviceDto.setId(1L);
        serviceDto.setCyclePeriodicity(10);
        serviceDto.setServiceCatalogueId(1L);
        serviceDto.setProvisionalDates(false);

        LrEmployeeDto employeeDto = new LrEmployeeDto();
        employeeDto.setId(1L);
        employeeDto.setFirstName("Sarah");
        employeeDto.setLastName("Pallett");

        survey.setId(surveyId);
        survey.setName("Automated survey");
        survey.setJob(job);
        survey.setServiceCatalogue(linkResource);
        survey.setSurveyStatus(linkResource);
        survey.setDateOfCrediting(Date.valueOf("2017-06-04"));
        survey.setCreditedBy(linkResource);
        survey.setScheduleDatesUpdated(false);
        survey.setApproved(true);

        return survey;
    }
}
