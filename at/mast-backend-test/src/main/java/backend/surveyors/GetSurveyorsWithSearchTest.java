package backend.surveyors;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.DatabaseHelper;
import model.expectederror.ExpectedError;
import model.surveyor.SurveyorSearchPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetSurveyorsWithSearchTest
{

    @DataProvider(name = "Surveyors")
    public static Object[][] surveyors() throws SQLException
    {
        return new Object[][]{
                {getSurveyorName(1, false)},
                {getSurveyorName(3, false)},
                {getSurveyorName(4, true)},
                };
    }

    private static String getSurveyorName(int id, boolean useLastName) throws SQLException
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT first_name, last_name FROM MAST_LRP_EMPLOYEE WHERE id = " + id);
        rs.first();
        String columnName = (useLastName) ? "last_name" : "first_name";
        return "%" + rs.getString(columnName) + "%";
    }

    @Test(
            description = "MAST BE: GET /surveyor/?search",
            dataProvider = "Surveyors"
    )
    @Issue("LRT-727")
    public void getSurveyorsWithSearchTest(String searchTerm)
    {
        new SurveyorSearchPage(searchTerm, 0, 50);
    }

    @Test(
            description = "MAST BE Negative: GET /surveyor/?search",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-727")
    public void negativeGetSurveyorsWithSearchTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.SURVEYOR_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
