package backend.surveyors;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.surveyor.SurveyorPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetSurveyorsTest
{

    @Test(
            description = "MAST BE: GET /surveyor/?page&size",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-556")
    public void getSurveyorsTest(int page, int size)
    {
        new SurveyorPage(page, size);
    }

    @Test(
            description = "MAST BE Negative: GET /surveyor/?page&size",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-747")
    public void negativeGetSurveyorsTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.SURVEYOR_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
