package backend.surveyors;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.surveyor.SurveyorOfficePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetSurveyorsWithOfficeTest
{

    @DataProvider(name = "Surveyors")
    public static Object[][] surveyors()
    {
        return new Object[][]{
                {1}, {2}, {3}, {4}, {5}, {6}
        };
    }

    @Test(
            description = "MAST BE: GET /surveyor/?officeId",
            dataProvider = "Surveyors"
    )
    @Issue("LRT-727")
    public void getSurveyorsWithOfficeTest(int officeId)
    {
        new SurveyorOfficePage(officeId, 0, 50);
    }

    @Test(
            description = "MAST BE Negative: GET /surveyor/?officeId",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-727")
    public void negativeGetSurveyorsTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.SURVEYOR_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
