package backend.riskassessment;

import constant.Endpoint;
import constant.ReferenceTable;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.referencedata.ReferenceData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetRiskAssessmentTest
{

    @DataProvider(name = "RiskAssessment")
    public static Object[][] riskAssessment()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRD-475")
    @Test(
            description = "MAST BE: GET /risk-assessment-status/{id}",
            dataProvider = "RiskAssessment",
            enabled = false
    )
    public void getRiskAssessmentTest(int id)
    {
        new ReferenceData(Endpoint.RISK_ASSESSMENT, ReferenceTable.RISK_ASSESSMENT, id);
    }

    @Issue("LRD-475")
    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    public void negativeGetRiskAssessmentTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.RISK_ASSESSMENT, id, statusCode, message, Request.GET);
    }
}
