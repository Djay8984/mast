package backend.assetitem;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetItemRelationship;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssetItemRelationshipTest
{

    private static List<AssetItemRelationship> assetItemRelationship = new ArrayList<>();
    private static int counter = 0;

    @DataProvider(name = "AssetsItems")
    public Object[][] assetsItems()
    {
        return new Object[][]{
                {501, 598, 682},
                {428, 59, 142},
                {502, 1137, 1220}
        };
    }

    @Test(description = "LRT-1311 BE: POST asset/x/item/y/relationship {'toItem': {'id':a}, 'relationshipType':{'id':b}} Story LRT-1190 and LRT-1191",
            dataProvider = "AssetsItems")
    @Issue("LRT-1311")
    public void postAssetItemRelationshipTest(int assetId, int frItemId, int toItemId)
    {

        ItemRelationshipDto itemRelationshipDto = new ItemRelationshipDto();
        ItemLightDto toItem = new ItemLightDto();
        toItem.setId((long) toItemId);

        LinkResource relationshipTypeLinkResource = new LinkResource();
        relationshipTypeLinkResource.setId((long) 9);

        itemRelationshipDto.setToItem(toItem);
        itemRelationshipDto.setType(relationshipTypeLinkResource);

        assetItemRelationship.add(new AssetItemRelationship(itemRelationshipDto, assetId, frItemId));
    }

    @Test(description = "LRT-1311 BE: POST asset/x/item/y/relationship {'toItem': {'id':a}, 'relationshipType':{'id':b}} Story LRT-1190 and LRT-1191",
            dataProvider = "NegativeTwoParameterAssets",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1311")
    public void negativePostAssetItemRelationshipTest(String assetId, String itemId, int statusCode, String message)
    {
        if (itemId.equals("50000"))
            message = "Item with identifier 50000 not found for asset 1";

        ItemLightDto item = new ItemLightDto();
        item.setId(1L);
        ItemRelationshipDto itemRelationshipDto = new ItemRelationshipDto();
        itemRelationshipDto.setId(null);
        itemRelationshipDto.setToItem(item);

        new ExpectedError(Endpoint.ASSET_ITEM_RELATIONSHIP,
                itemRelationshipDto,
                new String[]{assetId, itemId, ""},
                Request.POST,
                statusCode,
                message);
    }

    @Test(
            description = "LRT-1312  BE: DELETE asset/x/item/y/relationship/z",
            dependsOnMethods = {"postAssetItemRelationshipTest"},
            dataProvider = "AssetsItems"
    )
    @Issue("1312")
    public void deleteAssetItemRelationshipTest(int assetId, int frItemId, int toItemId) throws SQLException
    {
        new AssetItemRelationship(assetId, frItemId, Long.toString(assetItemRelationship.get(counter).getDto().getId()));
        counter++;
    }

    @Test(
            description = "LRT-1312  BE: DELETE asset/x/item/y/relationship/z",
            dataProvider = "NegativeThreeParameter",
            dataProviderClass = DataProviders.class
    )

    @Issue("LRT-1312")
    public void negativeDeleteAssetItemRelationshipTest(String assetId, String frItemId, String itemRelationshipId, int statusCode, String message)
    {
        if (frItemId.equals("50000"))
            message = "Item with identifier 50000 not found for asset 1";
        new ExpectedError(Endpoint.ASSET_ITEM_RELATIONSHIP, new String[]{assetId, frItemId, itemRelationshipId}, statusCode, message, Request.DELETE);
    }

}
