package backend.assetitem;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.asset.AssetItemAttribute;
import model.asset.AssetItemAttributePage;
import model.expectederror.ExpectedError;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PutAssetItemAttributeTest
{

    @Test(description = "BE: PUT /asset/{id}/item/{id}/attribute/{id}")
    @Issue("LRT-1309")
    public void putAssetItemAttributeTest() throws SQLException
    {
        Map<String, Integer> ids = TestDataHelper.getAssetItemAttributeIds();
        int assetId = ids.get("asset_id");
        int itemId = ids.get("item_id");
        int attributeId = ids.get("attribute_id");
        List<AttributeDto> attributes = new AssetItemAttributePage(assetId, itemId).getDto().getContent();
        AttributeDto attribute = null;

        for (AttributeDto atr : attributes)
        {
            if (atr.getId() == attributeId)
            {
                attribute = atr;
                break;
            }
        }

        if (attribute == null)
        {
            Assert.fail("Attribute with id " + attributeId + " not found.");
        }

        attribute.setValue(UUID.randomUUID().toString());

        new AssetItemAttribute(attribute, assetId, itemId, attributeId);
    }

    @Test(description = "BE: PUT /asset/{id}/item/{id}/attribute/{id}",
            dataProvider = "NegativeAttributeThreeParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1024")
    public void negativePutAssetItemAttributeTest(String assetId, String itemId, String attributeId, int statusCode, String errorMessage)
    {
        AttributeDto attributeDto = new AttributeDto();
        LinkResource item = new LinkResource();
        item.setId(Long.parseLong(itemId));
        attributeDto.setItem(item);
        attributeDto.setId(Long.parseLong(attributeId));
        LinkResource attributeType = new LinkResource();
        attributeType.setId(1L);
        attributeDto.setAttributeType(attributeType);
        attributeDto.setValue("An attribute value");

        new ExpectedError(Endpoint.ASSET_ITEM_ATTRIBUTE,
                attributeDto,
                new String[]{assetId, itemId, attributeId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
