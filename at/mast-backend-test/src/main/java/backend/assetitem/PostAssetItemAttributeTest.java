package backend.assetitem;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.asset.AssetItemAttribute;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;

public class PostAssetItemAttributeTest
{

    @DataProvider(name = "AssetItemAttribute")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1}, {4, 3}, {428, 8}
        };
    }

    @Test(description = "BE: POST asset/x/item/y/attribute",
            dataProvider = "AssetItemAttribute")
    @Issue("LRT-1308")
    public void postAssetItemAttributeTest(int assetId, int itemID) throws SQLException
    {
        AttributeDto attributeDto = new AttributeDto();
        LinkResource attributeType = new LinkResource();
        int attributeId = TestDataHelper.getAttributeTypeId();
        attributeType.setId((long) attributeId);
        attributeDto.setValue("Test");
        attributeDto.setAttributeType(attributeType);
        LinkResource item = new LinkResource();
        item.setId((long) itemID);
        attributeDto.setItem(item);

        new AssetItemAttribute(attributeDto, assetId);

    }

    @Test(description = "BE: POST asset/x/item/y/attribute",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1308")
    public void negativePostAssetItemAttributeTest(String assetId, String itemID, int statusCode, String message)
            throws SQLException
    {
        if (assetId.equals("50000"))
            message = "Item with identifier 1 not found for asset 50000";
        if (itemID.equals("50000"))
            message = "Item with identifier 50000 not found for asset 1";
        AttributeDto attributeDto = new AttributeDto();
        LinkResource id = new LinkResource();
        int attributeId = TestDataHelper.getAttributeTypeId();
        id.setId((long) attributeId);
        attributeDto.setValue("Test");
        attributeDto.setAttributeType(id);
        attributeDto.setId(null);

        new ExpectedError(Endpoint.ASSET_ITEM_ATTRIBUTE,
                attributeDto,
                new String[]{assetId, itemID, ""},
                Request.POST,
                statusCode,
                message);
    }
}
