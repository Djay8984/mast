package backend.assetitem;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.asset.AssetItemAttributePage;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetAssetItemAttributesTest
{

    @DataProvider(name = "AssetItemAttributes")
    public Object[][] assetItems()
    {
        return new Object[][]{
                {5, 6000},
                {428, 8},
                {502, 1086},
                };
    }

    @Test(description = "BE: GET/asset/{id}/item/{id}/attribute",
            dataProvider = "AssetItemAttributes"
    )
    @Issue("LRT-1307")
    public void getAssetItemAttributesTest(int assetId, int itemId)
    {
        new AssetItemAttributePage(assetId, itemId);
    }

    @Test(description = "Negative BE: GET/asset/{id}/item/{id}/attribute",
            dataProvider = "NegativeTwoParameterAssets",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1307")
    public void negativeGetAssetItemAttributesTest(String assetId, String itemId, int statusCode, String message)
    {
        if (itemId.equals("50000"))
            message = "Item with identifier 50000 not found for asset 1";
        new ExpectedError(Endpoint.ASSET_ITEM_ATTRIBUTE, new String[]{assetId, itemId, ""}, statusCode, message, Request.GET);
    }
}
