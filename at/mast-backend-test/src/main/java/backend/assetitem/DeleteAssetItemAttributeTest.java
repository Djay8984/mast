package backend.assetitem;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import helper.TestDataHelper;
import model.asset.AssetItemAttribute;
import model.asset.AssetItemAttributePage;
import org.junit.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DeleteAssetItemAttributeTest
{

    @Test(description = "BE: DELETE asset/{assetId}/item/{itemId}/attribute/{attributeId}")
    @Issue("LRT-759")
    public void deleteAssetItemAttributeTest() throws SQLException
    {
        boolean attributeCreated = false;
        int assetId;
        int itemId;
        int attributeTypeId;
        String value;
        AttributeDto newAttribute = null;
        do
        {
            Map<String, Integer> ids = TestDataHelper.getRandomAssetItem();
            assetId = ids.get("assetId");
            itemId = ids.get("itemId");
            attributeTypeId = TestDataHelper.getAttributeTypeId();
            LinkResource itemResource = new LinkResource();
            itemResource.setId((long) itemId);
            LinkResource attributeType = new LinkResource();
            attributeType.setId((long) attributeTypeId);
            value = UUID.randomUUID().toString();
            AttributeDto attributeDto = new AttributeDto();
            attributeDto.setItem(itemResource);
            attributeDto.setAttributeType(attributeType);
            attributeDto.setValue(value);

            try
            {
                new AssetItemAttribute(attributeDto, assetId);

                newAttribute = getAttributeDto(assetId, itemId, value);

                if (newAttribute == null)
                {
                    Assert.fail("Failed to add new Attribute.");
                }
                else
                {
                    attributeCreated = true;
                }
            }
            catch (AssertionError ae)
            {
                if (!ae.getMessage().contains("maximum instance rules."))
                {
                    throw ae;
                }
            }
        }
        while (!attributeCreated);

        new AssetItemAttribute(newAttribute, assetId, itemId);

        if (getAttributeDto(assetId, itemId, value) != null)
        {
            Assert.fail("Attribute " + newAttribute.getId().intValue() + " has not been deleted.");
        }
    }

    private AttributeDto getAttributeDto(int assetId, int itemId, String value)
    {
        List<AttributeDto> attributes = new AssetItemAttributePage(assetId, itemId).getDto().getContent();
        AttributeDto newAttribute = null;

        for (AttributeDto atr : attributes)
        {
            if (atr.getValue() != null && atr.getValue().equals(value))
            {
                newAttribute = atr;
                break;
            }
        }
        return newAttribute;
    }

}
