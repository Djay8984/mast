package backend.assetitem;

import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import helper.TestDataHelper;
import model.asset.AssetItemQuery;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PostAssetItemQueryTest
{

    @DataProvider(name = "Asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {501},
                {5},
                {428},
                {503}
        };
    }

    @Test(description = "BE: /asset/{id}/item/query",
            dataProvider = "Asset")
    @Issue("LRT-1302")
    public void postAssetItemQueryAllTest(int assetId) throws SQLException
    {
        ItemDto itemDto = TestDataHelper.getItemDetailsFromAsset(assetId);
        ItemQueryDto queryDto = new ItemQueryDto();

        List<Long> itemId = new ArrayList<>();
        itemId.add(itemDto.getId());
        queryDto.setItemId(itemId);

        List<Long> itemTypeId = new ArrayList<>();
        itemTypeId.add(itemDto.getItemType().getId());
        queryDto.setItemTypeId(itemTypeId);

        queryDto.setItemName(itemDto.getName());

        List<Long> attributeId = new ArrayList<>();
        List<String> attributeName = new ArrayList<>();
        for (int i = 0; i < itemDto.getAttributes().size(); i++)
        {
            attributeId.add(itemDto.getAttributes().get(i).getId());
            attributeName.add(itemDto.getAttributes().get(i).getValue());
        }

        Random rn = new Random();
        int randomAttribute = rn.nextInt(attributeName.size());
        queryDto.setAttributeId(Collections.singletonList(itemDto.getAttributes().get(randomAttribute).getId()));
        queryDto.setAttributeValue(itemDto.getAttributes().get(randomAttribute).getValue());

        new AssetItemQuery(queryDto, assetId);
    }

    @Test(description = "BE: /asset/{id}/item/query",
            dataProvider = "Asset")
    @Issue("LRT-1302")
    public void postAssetItemQueryOneTest(int assetId) throws SQLException
    {
        ItemDto itemDto = TestDataHelper.getItemDetailsFromAsset(assetId);
        ItemQueryDto queryDto = new ItemQueryDto();

        List<Long> itemId = new ArrayList<>();
        itemId.add(itemDto.getId());
        queryDto.setItemId(itemId);
        new AssetItemQuery(queryDto, assetId);

        queryDto = new ItemQueryDto();
        List<Long> itemTypeId = new ArrayList<>();
        itemTypeId.add(itemDto.getItemType().getId());
        queryDto.setItemTypeId(itemTypeId);
        new AssetItemQuery(queryDto, assetId);

        queryDto = new ItemQueryDto();
        queryDto.setItemName(itemDto.getName());
        new AssetItemQuery(queryDto, assetId);

        queryDto = new ItemQueryDto();
        List<Long> attributeId = new ArrayList<>();
        for (int i = 0; i < itemDto.getAttributes().size(); i++)
        {
            attributeId.add(itemDto.getAttributes().get(i).getId());
        }
        queryDto.setAttributeId(attributeId);
        new AssetItemQuery(queryDto, assetId);
    }

    @Test(description = "Negative BE: /asset/{id}/item/query",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class)
    @Issue("LRT-1302")
    public void negativePostAssetItemQueryTest(String assetId, int statusCode, String message)
    {
        ItemQueryDto queryDto = new ItemQueryDto();
        queryDto.setItemName("test");

        new ExpectedError(Endpoint.ASSET_ITEM_QUERY,
                queryDto,
                new String[]{assetId},
                Request.POST,
                statusCode,
                message);
    }
}
