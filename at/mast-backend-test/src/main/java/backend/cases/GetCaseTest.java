package backend.cases;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.cases.Case;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetCaseTest
{

    @DataProvider(name = "Cases")
    public Object[][] cases()
    {
        return new Object[][]{
                {1}, {4}, {7}
        };
    }

    @Test(
            description = "Uses RestAssured to test the case endpoint can retrieve a single case",
            dataProvider = "Cases"
    )
    @Issue("LRD-230")
    public void getCaseTest(int id)
    {
        new Case(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    public void negativeGetCaseTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.CASE, id, statusCode, message, Request.GET);
    }
}
