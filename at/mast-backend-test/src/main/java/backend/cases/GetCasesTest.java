package backend.cases;

import helper.DataProviders;
import model.cases.CasePage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetCasesTest
{

    @Test(
            description = "Check that the pagination for selecting cases is working",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-232")
    public void getCasesTest(int page, int size)
    {
        new CasePage(page, size);
    }

}
