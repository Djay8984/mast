package backend.cases;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import model.cases.Case;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.Random;

public class PostCaseTest
{

    @Issue("LRD-559")
    @Test(description = "Test to verify the ability of creating a case.")
    public void postCaseTest() throws SQLException
    {
        Random random = new Random();
        String uniqueRef = "TESTREF" + random.nextInt((10000 - 1)) + 1;

        CaseDto caseDto = new CaseDto();

        LinkResource caseType = new LinkResource();
        caseType.setId((long) 2);
        caseDto.setCaseType(caseType);

        LinkResource caseStatus = new LinkResource();
        caseStatus.setId((long) 1);
        caseDto.setCaseStatus(caseStatus);

        LinkResource businessProcess = new LinkResource();
        businessProcess.setId((long) 10);

        LinkResource asset = new LinkResource();
        asset.setId(8L);

        caseDto.setAsset(asset);

        caseDto.setContractReferenceNumber(uniqueRef);

        new Case(caseDto);
    }
}
