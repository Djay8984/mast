package backend.cases;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import model.cases.Case;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import static com.google.common.truth.Truth.assertThat;

public class PutCaseTest
{

    @Issue("LRD-559")
    @Test(description = "Test to verify the ability of creating a case.")
    public void putCaseTest() throws SQLException
    {
        CaseDto aCase = new Case(10).getDto();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(aCase.getTocAcceptanceDate());
        calendar.add(Calendar.DATE, 1);
        Date newDate = calendar.getTime();
        aCase.setTocAcceptanceDate(newDate);
        aCase.setCaseAcceptanceDate(java.sql.Date.valueOf("2016-02-16"));

        new Case(aCase, 10);

        aCase = new Case(10).getDto();
        assertThat(aCase.getTocAcceptanceDate()).isEqualTo(newDate);
    }
}
