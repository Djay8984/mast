package backend.product;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.product.Product;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetProductTest
{

    @DataProvider(name = "Asset")
    public Object[][] asset()
    {
        return new Object[][]{
                {1}, {12}
        };
    }

    @Issue("LRD-1068")
    @Test(
            description = "MAST BE: GET /asset/{id}/product",
            dataProvider = "Asset"
    )
    public void getProductTest(int assetId)
    {
        new Product(assetId);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void negativeGetAvailableProductTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.PRODUCT, id, statusCode, message, Request.GET);
    }

}
