package backend.product;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import model.product.Product;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class PutProductTest
{

    @DataProvider(name = "Asset")
    public Object[][] asset()
    {
        return new Object[][]{
                {1}, {12}
        };
    }

    @Issue("LRD-1068")
    @Test(
            description = "MAST BE: PUT /asset/{id}/product",
            dataProvider = "Asset",
            enabled = false
    )
    public void putProductTest(int assetId)
    {
        Product product = new Product(assetId);

        ProductCatalogueDto[] productCatalogueDto = new ProductCatalogueDto[product.getDtoArray().length];

        int i = 0;
        for (ProductDto productDto : product.getDtoArray())
        {
            ProductCatalogueDto productCatalogue = new ProductCatalogueDto();
            productCatalogue.setId(productDto.getProductCatalogueId());
            productCatalogue.setName(productDto.getName());
            productCatalogue.setDescription(productDto.getDescription());
            productCatalogue.setDisplayOrder(productDto.getDisplayOrder());
            LinkResource productGroup = new LinkResource();
            productGroup.setId(1L);
            productCatalogue.setProductGroup(productGroup);
            LinkResource productType = new LinkResource();
            productType.setId(1L);
            productCatalogue.setProductType(productType);
            productCatalogueDto[i] = productCatalogue;
            i++;
        }
        //TODO - once a dto has been added map to said dto and put it in
    }
}
