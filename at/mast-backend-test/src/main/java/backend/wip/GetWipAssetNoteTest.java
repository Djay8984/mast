package backend.wip;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipAssetNote;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetWipAssetNoteTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {1, 11}, {59879798, 17}, {3, 12}
        };
    }

    @Test(description = "BE: GET /job/{id}/wip-asset-note/{id}, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1032")
    // FIXME: 29/6/2016 - returns 404
    public void getWipAssetNoteTest(int jobId, int wipAssetNoteId)
    {
        new WipAssetNote(jobId, wipAssetNoteId);
    }

    @Test(description = "BE: GET /job/{id}/wip-asset-note/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1032")
    // FIXME: 29/6/2016 - returns 404
    public void negativeGetWipAssetNoteTest(String jobId, String assetNoteId, int statusCode, String message)
    {
        new ExpectedError(Endpoint.WIP_ASSET_NOTE, new String[]{jobId, assetNoteId}, statusCode, message, Request.GET);
    }
}
