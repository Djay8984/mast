package backend.wip;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipAssetNote;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PutWipAssetNoteTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {59879798, 17}, {3, 12}
        };
    }

    @Test(description = "BE: PUT /job/{id}/wip-asset-note/{id}, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1024")
    //fixme - returns 404
    public void putWipAssetNoteTest(int jobId, int assetNoteId)
    {
        WipAssetNote assetNote = new WipAssetNote(jobId, assetNoteId);

        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) jobId);

        assetNote.getDto().setActionTaken("An action was taken");
        assetNote.getDto().setDueDate(Date.valueOf("2016-02-16"));
        assetNote.getDto().setAffectedItems("this was the affected item");
        assetNote.getDto().setJob(linkResource);

        new WipAssetNote(assetNote.getDto(), jobId, assetNoteId);
    }

    @Test(description = "BE: PUT /job/{id}/wip-asset-note/{id}, Story 6.11",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-1024")
    //fixme - returns 404
    public void negativePutWipAssetNoteTest(String jobId, String assetNoteId, int statusCode, String errorMessage)
    {
        if (assetNoteId.equals("50000"))
            errorMessage = "Entity combination not found.";
        AssetNoteDto assetNoteDto = new WipAssetNote(1, 11).getDto();
        assetNoteDto.setId(Long.valueOf(assetNoteId));
        LinkResource linkResource = new LinkResource();
        linkResource.setId(1L);
        assetNoteDto.setJob(linkResource);

        new ExpectedError(Endpoint.WIP_ASSET_NOTE,
                assetNoteDto,
                new String[]{jobId, assetNoteId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
