package backend.wip;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipAssetNotePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetWipAssetNotesTest
{

    @DataProvider(name = "JobsPages")
    public Object[][] jobs()
    {
        return new Object[][]{
                {1, 0, 10},
                {59879801, 0, 10},
                {3, 0, 10}
        };
    }

    @Test(description = "BE: GET /job/{id}/wip-asset-note/?page&size, Story 6.11",
            dataProvider = "JobsPages")
    @Issue("LRT-1029")
    public void getWipAssetNotesTest(int jobId, int page, int size)
    {
        new WipAssetNotePage(jobId, page, size);
    }

    @Test(description = "BE: GET /job/{id}/wip-asset-note/?page&size , Story 6.11",
            dataProvider = "NegativeForPagesWithId",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1029")
    public void negativeGetWipAssetNotesTest(String jobId, String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.WIP_ASSET_NOTE_PAGE, new String[]{jobId, page, size}, statusCode, message, Request.GET);
    }
}
