package backend.wip;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.wip.WipAssetNote;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostWipAssetNoteTest
{

    @DataProvider(name = "Jobs")
    public Object[][] assets()
    {
        return new Object[][]{
                {1, 1}, {59879800, 1}, {6, 1}
        };
    }

    @Test(description = "BE: POST /job/{id}/wip-asset-note, Story 6.11",
            dataProvider = "Jobs")
    @Issue("LRT-1030")
    //fixme - returns 404
    public void postWipAssetNoteTest(int jobId, int assetId)
    {

        AssetNoteDto assetNoteDto = new AssetNoteDto();
        LinkResource job = new LinkResource();
        job.setId((long) jobId);
        LinkResource asset = new LinkResource();
        asset.setId((long) assetId);
        LinkResource link = new LinkResource();
        link.setId((long) 1);

        assetNoteDto.setAsset(asset);
        assetNoteDto.setJob(job);
        assetNoteDto.setImposedDate(Date.valueOf("2016-02-16"));
        assetNoteDto.setDueDate(Date.valueOf("2016-02-16"));
        assetNoteDto.setDescription("This is an automated wip asset note description");
        assetNoteDto.setActionTaken("An automated action was taken");
        assetNoteDto.setInheritedFlag(false);
        assetNoteDto.setStatus(link);
        assetNoteDto.setTitle("Automated asset note title");
        assetNoteDto.setCategory(link);
        assetNoteDto.setConfidentialityType(link);

        new WipAssetNote(assetNoteDto, jobId);
    }

    @Test(description = "BE: POST /job/{id}/wip-asset-note, Story 6.11",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1030")
    public void negativePostWipAssetNoteTest(String jobId, int statusCode, String errorMessage)
    {
        AssetNoteDto assetNoteDto = new WipAssetNote(1, 11).getDto();
        LinkResource job = new LinkResource();
        job.setId(Long.valueOf(jobId));
        assetNoteDto.setId(null);
        assetNoteDto.setJob(job);

        new ExpectedError(Endpoint.WIP_ASSET_NOTE,
                assetNoteDto,
                new String[]{jobId, ""},
                Request.POST,
                statusCode,
                errorMessage);
    }
}
