package backend.portofregistry;

import constant.Endpoint;
import constant.ReferenceTable;
import constant.Request;
import constant.TableMap;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.referencedata.ReferenceData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetPortOfRegistryTest
{

    @DataProvider(name = "PortOfRegistry")
    public static Object[][] portOfRegistry()
    {
        return new Object[][]{
                {1},
                {4},
                {10},
                {15},
                {22},
                {28},
                };
    }

    @Issue("LRD-493")
    @Test(
            description = "MAST BE: GET /port-of-registry/{id}",
            dataProvider = "PortOfRegistry"
    )
    public void getPortOfRegistryTest(int id)
    {
        new ReferenceData(Endpoint.PORT_OF_REGISTRY, ReferenceTable.PORT_OF_REGISTRY, TableMap.NAME, id);
    }

    @Issue("LRD-493")
    @Test(
            description = "MAST BE Negative: GET /port-of-registry/{id}",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    public void negativeGetPortOfRegistryTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.PORT_OF_REGISTRY, id, statusCode, message, Request.GET);
    }
}
