package backend.authorisation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.frameworkium.core.common.properties.Property;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;
import helper.DatabaseHelper;
import helper.SignUtils;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.truth.Truth.assertThat;
import static helper.RestHelper.BE_PREFIX;

public class AuthorisationSetCorrectlyTest
{

    private static Logger logger = LogManager.getLogger(AuthorisationSetCorrectlyTest.class);

    @DataProvider(name = "Requests")
    public Object[][] request()
    {
        return new Object[][]{
                {"Save a Condition of Class", "/asset/1/coc", "post"},
                {"Create Job", "/job", "post"},
                {"Update a Job", "/job/1/", "put"},
                {"Create Case", "/case/", "post"},
                {"Delete a Case", "/case/9999999999", "delete"},
                {"Update a Case", "/case/1", "put"},
                {"Fetch all Cases", "/case?order=desc&page=0&size=15&sort=id", "get"},
                {"Fetch all Conditions of Class for an Asset", "/asset/1/coc", "get"}
        };
    }

    /*
    @BeforeClass(alwaysRun = true)
    public void turnOnAuthorisation() throws InterruptedException {
        logger.info("Turning on authorisation for the 'authorisationSetCorrectlyTest' test...");
        DatabaseHelper.insertQuery("UPDATE `security_db`.`mast_security_status` SET `enabled`='1' WHERE `id`='1';");
        logger.info("Sleeping for 1m after turning on authorisation due to the BE minute refresh.");
        Thread.sleep(60000);
    }
    */

    //PLEASE NOTE - security needs to be turned on for this test to work
    @Issue("LRT-473")
    @Test(
            description = "Check that when the security is turned on, if the user does an unauthorised" +
                    "request it comes back with a status of unauthorised",
            dataProvider = "Requests",
            singleThreaded = true,
            enabled = false
    )
    public void authorisationSetCorrectlyTest(String function, String request, String requestType) throws SQLException
    {
        for (Group groupType : Group.values())
        {
            Response response;
            Boolean permittedFunction = permittedFunction(groupType.name, function, requestType);

            RestHelper restHelper = new RestHelper();

            if (permittedFunction)
            {
                switch (requestType)
                {
                    case "get":
                        restHelper.get(request, HttpStatus.SC_OK, groupType.name);
                        break;
                    case "put":
                        response = restHelper.put(request, "{}", groupType.name);
                        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
                        break;
                    case "post":
                        response = restHelper.post(request, "{}", groupType.name);
                        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
                        break;
                    case "delete":
                        if (function.equals("Delete Case"))
                        {
                            restHelper.delete(request, HttpStatus.SC_OK, groupType.name);
                        }
                        else
                        {
                            restHelper.delete(request, HttpStatus.SC_NOT_FOUND, groupType.name);
                        }
                        break;
                }
            }
            else
            {
                switch (requestType)
                {
                    case "get":
                        response = restHelper.get(request, HttpStatus.SC_MOVED_TEMPORARILY, groupType.name);
                        assertThat(response.body().prettyPrint()).contains("\"message\": null");
                        break;
                    case "put":
                        response = restHelper.put(request, "{}", groupType.name);
                        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_MOVED_TEMPORARILY);
                        break;
                    case "post":
                        response = restHelper.post(request, "{}", groupType.name);
                        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_MOVED_TEMPORARILY);
                        break;
                    case "delete":
                        restHelper.delete(request, HttpStatus.SC_MOVED_TEMPORARILY, groupType.name);
                        break;
                }
            }
        }
    }

    /*
    @AfterClass(alwaysRun = true)
    public void turnOffAuthorisation() {
        logger.info("Turned off authorisation after test.");
        DatabaseHelper.insertQuery("UPDATE `security_db`.`mast_security_status` SET `enabled`='0' WHERE `id`='1';");
    }
    */

    public Boolean permittedFunction(String groupName, String function, String requestType)
    {
        try
        {
            ResultSet rs = DatabaseHelper.executeQuery("select distinct " +
                    "role.id, " +
                    "groupdo.name, " +
                    "role.name, " +
                    "api.method, " +
                    "api.uri " +
                    "from security_db.RBAC_ROLE role " +
                    "inner join security_db.RBAC_GROUP_ROLE grouproles on role.id=grouproles.role_id " +
                    "inner join security_db.RBAC_GROUP groupdo on grouproles.group_id=groupdo.id " +
                    "inner join security_db.RBAC_API api on api.id=groupdo.id " +
                    "where " +
                    "groupdo.name = '" + groupName + "' " +
                    "and api.method = '" + requestType.toUpperCase() + "' " +
                    "and role.name = '" + function + "' " +
                    "and role.enabled=1 and grouproles.enabled=1");
            return rs.next();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public enum Group
    {
        SDO("SDO"),
        CFO("CFO"),
        MMSDelegatedCFO("MMS Delegated CFO"),
        SurveyorAuditorInspector("Surveyor/Auditor/Inspector"),
        LeadSurveyorAuditorInspector("Lead Surveyor/Auditor/Inspector"),
        AuthorisingSurveyorAuditorInspector("Authorising Surveyor/Auditor/Inspector"),
        EIC("EIC"),
        ClassGroup("Class Group"),
        AreaTechnicalOffice("Area Technical Office"),
        MmsMmso("MMS/MMSO"),
        MDS("MDS"),
        Dummy("Dummy"); //fake group

        private final String name;

        Group(String name)
        {
            this.name = name;
        }
    }

    /**
     * Class to help calling the endpoints for the MAST BE. This will support all operations, Rest Assured shouldn'putExpectArray
     * be used inside your frontend. Extra functionality, if needed, should be inserted in here.
     */
    private class RestHelper
    {

        /**
         * Set the port for backend assured based on environment
         */
        public RestHelper()
        {
            if (Property.GRID_URL.isSpecified())
            {
                try
                {
                    RestAssured.baseURI = "http://" + new URL(Property.GRID_URL.getValue()).getHost() + ":8080";
                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }
            }
        }

        /**
         * This method performs a GET request on a desired endpoint. It will serialize the JSON response back to
         * a data model which is provided by the developers. For example, for a case, use the class CaseDto.
         *
         * @param object - The dto object for the endpoint your calling. Eg: /note = CaseNoteDto
         * @param url - The url of the endpoint, no http/port is needed. Eg: /case
         * @return - The serialized object from the response
         */

        /**
         * Generates a group of security headers for authentication and authorisation.
         *
         * @param checkSum
         * @return List of headers
         */
        private Headers createHeaders(final String checkSum, String group)
        {
            // Groups
            final Header groupsHeader = new Header("groups", group);

            // Client Sign
            final Header signHeader = new Header("idsclientsign", checkSum);

            // Client version
            final Header versionHeader = new Header("idsclientversion", SignUtils.getClientVersion());

            return new Headers(groupsHeader, signHeader, versionHeader);
        }

        @SuppressWarnings("unchecked")
        public Object get(Object object, String url)
        {
            final String checkSum = SignUtils.generateChecksum("GET", BE_PREFIX + url, null);
            Object response = RestAssured
                    .expect()
                    .statusCode(HttpStatus.SC_OK)
                    .given()
                    .headers(createHeaders(checkSum, "Admin"))
                    .get(BE_PREFIX + url)
                    .as(object.getClass());
            return response;
        }

        /**
         * This method is more-so for negative scenarios as it doesn'putExpectArray provide a serialized object to return.
         * Pass in a url and a status code to assert.
         *
         * @param url        - Url of the endpoint to call
         * @param statusCode - Status code to assert, for example 404/400
         * @return - RestAssured Response object
         */
        @SuppressWarnings("unchecked")
        public Response get(String url, int statusCode)
        {
            final String checkSum = SignUtils.generateChecksum("GET", BE_PREFIX + url, null);
            Response response = RestAssured
                    .expect()
                    .statusCode(statusCode)
                    .given()
                    .headers(createHeaders(checkSum, "Admin"))
                    .get(BE_PREFIX + url);
            return response;
        }

        /**
         * This method is more-so for negative scenarios with a particular group user as it doesn'putExpectArray provide a serialized object to return.
         * Pass in a url and a status code to assert.
         *
         * @param url        - Url of the endpoint to call
         * @param statusCode - Status code to assert, for example 404/400
         * @param group      - name of the group you will be using to make the request
         * @return - RestAssured Response object
         */
        public Response get(String url, int statusCode, String group)
        {
            final String checkSum = SignUtils.generateChecksum("GET", BE_PREFIX + url, null);

            Response response = RestAssured
                    .expect()
                    .statusCode(statusCode)
                    .given()
                    .headers(createHeaders(checkSum, group))
                    .get(BE_PREFIX + url);
            return response;
        }

        /**
         * Method for using post with the MAST BE endpoints. Pass in an data model object to deserialize.
         *
         * @param url   - Url of the endpoint
         * @param body  - Object to deserialize and to use as the JSON body
         * @param group - name of the group you will be using to make the request
         * @return - RestAssured Response object
         */
        @SuppressWarnings("unchecked")
        public Response post(String url, Object body, String group)
        {
            body = serializeObjectToJSON(body);
            final String checkSum = SignUtils.generateChecksum("POST", BE_PREFIX + url, (String) body);
            Response response = RestAssured
                    .given()
                    .headers(createHeaders(checkSum, group))
                    .contentType(ContentType.JSON)
                    .body(body)
                    .post(BE_PREFIX + url);
            return response;
        }

        /**
         * Method for using put with the MAST BE endpoints. Pass in an data model object to deserialize.
         *
         * @param url   - Url of the endpoint
         * @param body  - Object to deserialize and to use as the JSON body
         * @param group - name of the group you will be using to make the request
         * @return - RestAssured Response object
         */
        @SuppressWarnings("unchecked")
        public Response put(String url, Object body, String group)
        {
            body = serializeObjectToJSON(body);
            final String checkSum = SignUtils.generateChecksum("PUT", BE_PREFIX + url, (String) body);
            Response response = RestAssured
                    .given()
                    .headers(createHeaders(checkSum, group))
                    .contentType(ContentType.JSON)
                    .body(body)
                    .put(BE_PREFIX + url);
            return response;
        }

        /**
         * This method is for testing a delete endpoint, for example deleting a case.
         * Pass in a url and a status code to assert.
         *
         * @param url        - Url of the endpoint to call
         * @param statusCode - Status code to assert, for example 404/400
         * @return - RestAssured Response object
         */
        public Response delete(String url, int statusCode, String group)
        {
            final String checkSum = SignUtils.generateChecksum("DELETE", BE_PREFIX + url, null);
            Response response = RestAssured
                    .expect()
                    .statusCode(statusCode)
                    .given()
                    .headers(createHeaders(checkSum, group))
                    .delete(BE_PREFIX + url);
            return response;
        }

        private String serializeObjectToJSON(Object object)
        {
            if (!object.getClass().equals(String.class))
            {
                String bodyAsString = null;
                final ObjectMapper mapper = new ObjectMapper();
                try
                {
                    bodyAsString = mapper.writeValueAsString(object);
                }
                catch (JsonProcessingException e)
                {
                    e.printStackTrace();
                }
                return bodyAsString;
            }
            else
            {
                return (String) object;
            }
        }
    }
}



