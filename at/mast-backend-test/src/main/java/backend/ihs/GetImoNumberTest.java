package backend.ihs;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.imo.ImoNumber;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetImoNumberTest
{

    @DataProvider(name = "ImoNumber")
    public Object[][] assets()
    {
        return new Object[][]{
                {7654315},
                {7654311},
                {7654322},

                };
    }

    @Test(description = "BE: GET ihs/{imoNumber}",
            dataProvider = "ImoNumber"
    )
    @Issue("LRT-1304")
    public void getImoNumberTest(int imoNumber)
    {
        new ImoNumber(imoNumber);
    }

    @Test(description = "BE: GET ihs/{imoNumber}",
            dataProvider = "NegativeOneParameterImo",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1304")
    public void negativeGetImoNumberTest(String imoNumber, int statusCode, String message)
    {
        new ExpectedError(Endpoint.IHS_NUMBER, new String[]{imoNumber}, statusCode, message, Request.GET);
    }
}
