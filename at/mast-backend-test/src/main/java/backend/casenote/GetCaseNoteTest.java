package backend.casenote;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetCaseNoteTest
{

    @DataProvider(name = "CaseNotes")
    public Object[][] caseNotes()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Test(
            description = "Uses RestAssured to test the case notes endpoint",
            dataProvider = "CaseNotes",
            enabled = false
    )
    @Issue("LRD-222")
    public void getCaseNoteTest(int id)
    {
    }

    @Test(
            description = "Uses RestAssured to test the case notes endpoint",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-222")
    public void negativeGetCaseNoteTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.CASE_NOTE, id, statusCode, message, Request.GET);
    }

}
