package backend.offices;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.office.Office;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetOfficeTest
{

    @DataProvider(name = "Offices")
    public Object[][] offices()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Test(
            description = "MAST BE: GET /office/{id}",
            dataProvider = "Offices"
    )
    @Issue("LRD-227")
    public void getOfficeTest(int id)
    {
        new Office(id);
    }

    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRD-227")
    public void negativeGetAssetTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.OFFICE, id, statusCode, message, Request.GET);
    }

}
