package backend.offices;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.SQLException;

public class GetOfficesWithOfficeRoleTest
{

    @DataProvider(name = "OfficeRoles")
    public static Object[][] officeRoles()
    {
        return new Object[][]{
                {"CFO"}, {"SDO"}, {"DSO"}, {"TSO"}, {"TSDO"}, {"TSSAO"}, {"TEST"}
        };
    }

    @Issue("LRT-729")
    @Test(
            description = "MAST BE: GET /office/?size=20&roleName",
            dataProvider = "OfficeRoles",
            enabled = false
    )
    public void getOfficesWithOfficeRoleTest(String officeRole) throws SQLException
    {
    }
}
