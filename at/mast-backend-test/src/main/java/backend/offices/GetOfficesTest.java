package backend.offices;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetOfficesTest
{

    @Test(
            description = "MAST BE: GET /office/?page&size",
            dataProvider = "PositivePages",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-225")
    public void getOfficesTest(int page, int size)
    {
    }

    @Test(
            description = "MAST BE Negative: GET /office/?page&size",
            dataProvider = "NegativeForPages",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-225")
    public void negativeGetOfficesTest(String page, String size, int statusCode, String message)
    {
        new ExpectedError(Endpoint.OFFICE_PAGE, new String[]{page, size}, statusCode, message, Request.GET);
    }

}
