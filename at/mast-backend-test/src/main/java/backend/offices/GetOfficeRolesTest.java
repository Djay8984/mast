package backend.offices;

import constant.Endpoint;
import constant.ReferenceTable;
import model.referencedata.ReferenceData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetOfficeRolesTest
{

    @DataProvider(name = "OfficeRole")
    public Object[][] officeRole()
    {
        return new Object[][]{
                {1}, {2}, {4}
        };
    }

    @Issue("LRD-553")
    @Test(
            description = "MAST BE: GET /office-role/%s",
            dataProvider = "OfficeRole",
            enabled = false
    )
    public void getOfficeRoleTest(int id)
    {
        new ReferenceData(Endpoint.OFFICE_ROLE, ReferenceTable.OFFICE_ROLE, id);
    }

    @Issue("LRD-553")
    @Test(description = "MAST BE: GET /office-role",
            enabled = false)
    public void getOfficeRolesTest()
    {
        new ReferenceData(Endpoint.OFFICE_ROLE, ReferenceTable.OFFICE_ROLE);
    }
}
