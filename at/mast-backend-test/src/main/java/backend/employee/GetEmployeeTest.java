package backend.employee;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.employee.Employee;
import model.expectederror.ExpectedError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetEmployeeTest
{

    @DataProvider(name = "Employee")
    public static Object[][] employee()
    {
        return new Object[][]{
                {1}, {2}, {3}
        };
    }

    @Issue("LRD-1260")
    @Test(
            description = "MAST BE: GET /employee/{employeeid}",
            dataProvider = "Employee"
    )
    public void getEmployeeTest(int id)
    {
        new Employee(id);
    }

    @Issue("LRD-1260")
    @Test(
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    public void negativeGetEmployeeTest(String id, int statusCode, String message)
    {
        new ExpectedError(Endpoint.EMPLOYEE_EMPLOYEEID, id, statusCode, message, Request.GET);
    }

}
