package backend.employee;

import model.employee.EmployeesNamePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetEmployeesWithNameTest
{

    @DataProvider(name = "Employees")
    public static Object[][] employees()
    {
        return new Object[][]{
                {"%Jose%"},
                {"%Sam%"},
                {"%^%"},
                {"%Tom%"},
                {"%Smith%"},
                {"%Mend%"},
                {"%ae%"}
        };
    }

    @Test(
            description = "MAST BE: GET /employee/?search",
            dataProvider = "Employees"
    )
    @Issue("LRT-1235")
    public void getEmployeesWithName(String searchKey)
    {
        new EmployeesNamePage(searchKey);
    }

}
