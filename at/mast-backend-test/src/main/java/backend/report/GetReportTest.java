package backend.report;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.report.Report;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetReportTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {5, 2},
                {3, 5},
                {7, 6}
        };
    }

    @Test(description = "GET /job/*jobId*/report/*reportId*, Story 5.22",
            dataProvider = "Jobs")
    @Issue("LRT-1028")
    public void getReportTest(int jobId, int reportId)
    {
        new Report(jobId, reportId);
    }

    @Test(description = "BE Negative: GET /job/*jobId*/report/*reportId*, Story 5.22",
            dataProvider = "NegativeTwoParameterJobs",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1028")
    public void negativeGetReportTest(String jobId, String reportId, int statusCode, String message)
    {
        if (jobId.equals("50000"))
            message = "Report with id 1 is not linked to the Job with id 50000";
        if (reportId.equals("50000"))
            message = "Report with id 50000 is not linked to the Job with id 1";
        new ExpectedError(Endpoint.REPORT, new String[]{jobId, reportId}, statusCode, message, Request.GET);
    }
}
