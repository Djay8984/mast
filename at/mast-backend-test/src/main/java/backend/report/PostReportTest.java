package backend.report;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.report.Report;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;

public class PostReportTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3},
                {5}
        };
    }

    @Test(description = "POST /job/*jobId*/report, Story 5.22",
            dataProvider = "Jobs")
    @Issue("LRT-1026")
    public void postReportTest(int jobId)
    {
        ReportDto reportDto = new ReportDto();

        LinkResource job = new LinkResource();
        job.setId((long) jobId);

        LinkResource reportType = new LinkResource();
        reportType.setId((long) 1);

        ReferenceDataDto type = new ReferenceDataDto();
        type.setId((long) 1);

        reportDto.setReportVersion((long) 1);
        reportDto.setJob(job);
        reportDto.setReportType(reportType);
        reportDto.setClassRecommendation("This is an automated class recommendation");
        reportDto.setIssueDate(Date.valueOf("2015-02-16"));

        new Report(reportDto, jobId);
    }

    @Test(description = "BE Negative: POST /job/*jobId*/report, Story 5.22",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1026")
    public void negativePostReportTest(String jobId, int statusCode, String message)
    {
        ReportDto reportDto = new Report(1).getDto();
        reportDto.setId(null);

        new ExpectedError(Endpoint.REPORT,
                reportDto,
                new String[]{jobId, ""},
                Request.POST,
                statusCode,
                message);
    }
}
