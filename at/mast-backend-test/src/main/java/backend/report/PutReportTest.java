package backend.report;

import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.report.Report;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class PutReportTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {7, 6},
                {4, 4},
                {6, 7}
        };
    }

    @Test(
            description = " BE: PUT /job/*jobId*/report/*reportId*, Story 5.22",
            dataProvider = "Jobs",
            enabled = false
    )
    @Issue("LRT-1027")
    public void putReportTest(int jobId, int reportId)
    {
        ReportDto reportDto = new Report(jobId, reportId).getDto();

        reportDto.setReportVersion(null);
        reportDto.setClassRecommendation("This is an updated class recommendation");

        new Report(reportDto, jobId, reportId);
    }

    @Test(description = "BE Negative: PUT /job/*jobId*/report/*reportId*, Story 5.22",
            dataProvider = "NegativeTwoParameter",
            dataProviderClass = DataProviders.class,
            enabled = false
    )
    @Issue("LRD-1039")
    public void negativePutReportTest(String jobId, String reportId, int statusCode, String errorMessage)
    {
        if (reportId.equals("50000"))
            errorMessage = "Report with id 50000 is not linked to the Job with id 1";

        ReportLightDto reportDto = new Report(1, 1).getDto();
        reportDto.setId(Long.valueOf(reportId));
        reportDto.setReportVersion(null);

        new ExpectedError(Endpoint.REPORT,
                reportDto,
                new String[]{jobId, reportId},
                Request.PUT,
                statusCode,
                errorMessage);
    }
}
