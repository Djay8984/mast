package backend.report;

import constant.Endpoint;
import constant.Request;
import helper.DataProviders;
import model.expectederror.ExpectedError;
import model.report.Report;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

public class GetReportsTest
{

    @DataProvider(name = "Jobs")
    public Object[][] jobs()
    {
        return new Object[][]{
                {3},
                {4},
                {6}
        };
    }

    @Test(description = "GET /job/*jobId*/report, Story 5.22",
            dataProvider = "Jobs")
    @Issue("LRT-1028")
    public void getReportsTest(int jobId)
    {
        new Report(jobId);
    }

    @Test(description = "BE Negative: GET /job/*jobId*/report, Story 5.22",
            dataProvider = "NegativeOneParameter",
            dataProviderClass = DataProviders.class
    )
    @Issue("LRT-1028")
    public void negativeGetReportsTest(String jobId, int statusCode, String message)
    {
        new ExpectedError(Endpoint.REPORT, new String[]{jobId, ""}, statusCode, message, Request.GET);
    }
}
