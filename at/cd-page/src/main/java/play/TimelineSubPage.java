package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.Timeline;

import java.util.List;
import java.util.stream.Collectors;

public class TimelineSubPage extends BasePage<TimelineSubPage>
{
    @FindBy(css = "timeline[data-start-date='vm.startDate']")
    @Visible
    private Timeline timelineA;

    @FindBy(css = "div.timeline-container")
    @Visible
    private Timeline timelineB;

    @Step("")
    public List<String> getSelectedDatesA()
    {
        return timelineA.getSelectedDates().stream().map(WebElement::getText).collect(Collectors.toList());
    }

    @Step("")
    public List<String> getSelectedDatesB()
    {
        return timelineB.getSelectedDates().stream().map(WebElement::getText).collect(Collectors.toList());
    }

    @Step("")
    public TimelineSubPage selectStartDateA(String startDateString)
    {
        timelineA.selectStartDate(startDateString);
        return this;
    }

    @Step("")
    public TimelineSubPage selectStartDateB(String startDateString)
    {
        timelineB.selectStartDate(startDateString);
        return this;
    }

    @Step("")
    public TimelineSubPage selectEndDateA(String endDateString)
    {
        timelineA.selectEndDate(endDateString);
        return this;
    }

    @Step("")
    public TimelineSubPage selectEndDateB(String endDateString)
    {
        timelineB.selectEndDate(endDateString);
        return this;
    }

    @Step("")
    public TimelineSubPage selectDateRangeA(String startDateString, String endDateString)
    {
        timelineA.selectDateRange(startDateString, endDateString);
        return this;
    }

    @Step("")
    public TimelineSubPage selectDateRangeB(String startDateString, String endDateString)
    {
        timelineB.selectDateRange(startDateString, endDateString);
        return this;
    }
}
