package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.Datepicker;

import java.time.LocalDate;

public class DatePickerTimePickerSubPage extends BasePage<DatePickerTimePickerSubPage> {

    @FindBy(css = "md-datepicker[data-ng-model='vm.startDate']")
    @Visible
    private Datepicker startDateDatepicker1;

    // Same datepicker, but recognized as the child INPUT tag
    @Visible
    @FindBy(css = "md-datepicker[data-ng-model='vm.startDate'] input.md-datepicker-input")
    private Datepicker startDateDatepicker2;

    @Step("")
    public DatePickerTimePickerSubPage setStartDateByCalendarPane1(String startDate) {
        startDateDatepicker1.selectByCalendarPane(startDate);
        return this;
    }

    @Step("")
    public DatePickerTimePickerSubPage setStartDateByCalendarPane2(String startDate) {
        startDateDatepicker2.selectByCalendarPane(startDate);
        return this;
    }

    @Step("")
    public DatePickerTimePickerSubPage setStartDateByCalendarPane1(LocalDate startDate) {
        startDateDatepicker1.selectByCalendarPane(startDate);
        return this;
    }

    @Step("")
    public DatePickerTimePickerSubPage setStartDateByCalendarPane2(LocalDate startDate) {
        startDateDatepicker2.selectByCalendarPane(startDate);
        return this;
    }

    @Step("")
    public DatePickerTimePickerSubPage setStartDate1(String startDate) {
        startDateDatepicker1.clear();
        startDateDatepicker1.sendKeys(startDate);
        return this;
    }

    @Step("")
    public DatePickerTimePickerSubPage setStartDate2(String startDate) {
        startDateDatepicker1.clear();
        startDateDatepicker2.sendKeys(startDate);
        return this;
    }

    @Step("")
    public String getStartDate1() {
        return startDateDatepicker1.getSelectedDate();
    }

    @Step("")
    public String getStartDate2() {
        return startDateDatepicker2.getSelectedDate();
    }
}
