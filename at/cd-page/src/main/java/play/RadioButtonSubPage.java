package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;

public class RadioButtonSubPage extends BasePage<RadioButtonSubPage>
{
    @Name("")
    @Visible
    //use the parent selector
    @FindBy(css = "md-radio-button[value='Apple']")
    private RadioButton appleRadioButton;

    @Name("")
    @Visible
    // use the child selector - this is to test if the xpath redirect works
    @FindBy(css = "md-radio-button[value='Banana'] div.md-label")
    private RadioButton bananaRadioButton;

    @Step("")
    public RadioButtonSubPage selectAppleRadioButton()
    {
        appleRadioButton.select();
        return this;
    }

    @Step("")
    public RadioButtonSubPage deselectAppleRadioButton()
    {
        appleRadioButton.deselect();
        return this;
    }

    @Step("")
    public RadioButtonSubPage selectBananaRadioButton()
    {
        bananaRadioButton.select();
        return this;
    }

    @Step("")
    public RadioButtonSubPage deselectBananaRadioButton()
    {
        bananaRadioButton.deselect();
        return this;
    }

    @Step("")
    public boolean isAppleRadioButtonSelected()
    {
        return appleRadioButton.isSelected();
    }

    @Step("")
    public boolean isBananaRadioButtonSelected()
    {
        return bananaRadioButton.isSelected();
    }

    @Step("")
    public String getAppleRadioButtonText()
    {
        return appleRadioButton.getText();
    }

    @Step("")
    public String getBananaRadioButtonText()
    {
        return bananaRadioButton.getText();
    }
}
