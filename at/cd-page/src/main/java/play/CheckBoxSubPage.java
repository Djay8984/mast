package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.CheckBox;

public class CheckBoxSubPage extends BasePage<CheckBoxSubPage>
{

    @FindBy(css = "md-checkbox[aria-label='Apple']")
    @Visible
    //use parent selector
    private CheckBox appleCheckBox;

    @FindBy(css = "md-checkbox[aria-label='Banana'] span")
    @Visible
    //use child selector - this is to test whether the xpath redirection works
    private CheckBox bananaCheckBox;

    @Step("")
    public CheckBoxSubPage selectAppleCheckBox()
    {
        appleCheckBox.select();
        return this;
    }

    @Step("")
    public CheckBoxSubPage deselectAppleCheckBox()
    {
        appleCheckBox.deselect();
        return this;
    }

    @Step("")
    public CheckBoxSubPage selectBananaCheckBox()
    {
        bananaCheckBox.select();
        return this;
    }

    @Step("")
    public CheckBoxSubPage deselectBananaCheckBox()
    {
        bananaCheckBox.deselect();
        return this;
    }

    @Step("")
    public boolean isAppleCheckBoxChecked()
    {
        return appleCheckBox.isSelected();
    }

    @Step("")
    public boolean isBananaCheckBoxChecked()
    {
        return bananaCheckBox.isSelected();
    }

    @Step("")
    public String getAppleRadioCheckBoxText()
    {
        return appleCheckBox.getText();
    }

    @Step("")
    public String getBananaRadioCheckBoxText()
    {
        return bananaCheckBox.getText();
    }

}
