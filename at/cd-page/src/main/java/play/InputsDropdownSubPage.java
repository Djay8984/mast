package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.Dropdown;
import typifiedelement.NumericTextBox;

import java.util.List;
import java.util.stream.Collectors;

public class InputsDropdownSubPage extends BasePage<InputsDropdownSubPage>
{

    @FindBy(xpath = "//h1[contains(text(), 'Inputs & Dropdown')]/ancestor::md-card//label[contains(text(), 'Hourly Rate (USD)')]/ancestor::md-input-container")
    @Visible
    private NumericTextBox hourlyRateTextBox1;

    // Same text box, but recognized as the child INPUT tag
    @Visible
    @FindBy(css = "input[type='number']")
    private NumericTextBox hourlyRateTextBox2;

    @FindBy(xpath = "//h1[contains(text(), 'Inputs & Dropdown')]/ancestor::md-card//md-select[@data-ng-model='vm.projectType']/ancestor::md-input-container")
    @Visible
    private Dropdown projectType1;

    // Same dropdown, but recognized as the child MD-SELECT tag
    @FindBy(css = "md-select[data-ng-model='vm.projectType']")
    @Visible
    private Dropdown projectType2;

    @Step("")
    public InputsDropdownSubPage clearHourlyRate1()
    {
        hourlyRateTextBox1.clear();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage clearHourlyRate2()
    {
        hourlyRateTextBox2.clear();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage clickHourlyRateCountUpButton1()
    {
        hourlyRateTextBox1.clickCountUp();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage clickHourlyRateCountUpButton2()
    {
        hourlyRateTextBox2.clickCountUp();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage clickHourlyRateCountDownButton1()
    {
        hourlyRateTextBox1.clickCountDown();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage clickHourlyRateCountDownButton2()
    {
        hourlyRateTextBox2.clickCountDown();
        return this;
    }

    @Step("")
    public InputsDropdownSubPage selectProjectType1(String text)
    {
        projectType1.selectByText(text);
        return this;
    }

    @Step("")
    public InputsDropdownSubPage selectProjectType2(String text)
    {
        projectType2.selectByText(text);
        return this;
    }

    @Step("")
    public InputsDropdownSubPage selectProjectType1(int index)
    {
        projectType1.selectByIndex(index);
        return this;
    }

    @Step("")
    public InputsDropdownSubPage selectProjectType2(int index)
    {
        projectType2.selectByIndex(index);
        return this;
    }

    @Step("")
    public String getHourlyRate1()
    {
        return hourlyRateTextBox1.getText();
    }

    @Step("")
    public InputsDropdownSubPage setHourlyRate1(String hourlyRate)
    {
        hourlyRateTextBox1.sendKeys(hourlyRate);
        return this;
    }

    @Step("")
    public String getHourlyRate2()
    {
        return hourlyRateTextBox2.getText();
    }

    @Step("")
    public InputsDropdownSubPage setHourlyRate2(String hourlyRate)
    {
        hourlyRateTextBox2.sendKeys(hourlyRate);
        return this;
    }

    @Step("")
    public String getHourlyRateLabelText1()
    {
        return hourlyRateTextBox1.getLabelText();
    }

    @Step("")
    public String getHourlyRateLabelText2()
    {
        return hourlyRateTextBox2.getLabelText();
    }

    @Step("")
    public List<String> getProjectTypeOptionsText1()
    {
        List<String> options = projectType1.getOptions()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
        projectType1.closeOptionList();
        return options;
    }

    @Step("")
    public List<String> getProjectTypeOptionsText2()
    {
        List<String> options = projectType2.getOptions()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
        projectType2.closeOptionList();
        return options;
    }

    @Step("")
    public String getProjectTypeSelectedOption1()
    {
        return projectType1.getSelectedOption().getText();
    }

    @Step("")
    public String getProjectTypeSelectedOption2()
    {
        return projectType2.getSelectedOption().getText();
    }
}
