package play;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.MultiTagTextBox;

import java.util.List;
import java.util.stream.Collectors;

import static typifiedelement.MultiTagTextBox.Operation;

public class MultiTagTextBoxSubPage extends BasePage<MultiTagTextBoxSubPage>
{

    //use parent css
    @FindBy(css = "[data-name='tag1']")
    private MultiTagTextBox multiTagTextBox1;

    //use parent css with offset
    @FindBy(css = "[data-name='tag1'] div[class='container']")
    private MultiTagTextBox multiTagTextBox1ByCssOffset1;

    //use parent css with offset
    @FindBy(css = "[data-name='tag1'] div[class*='ui-select-container']")
    private MultiTagTextBox multiTagTextBox1ByCssOffset2;

    //use parent css with offset
    @FindBy(css = "[data-name='tag1'] div[class*='ui-select-container'] > div")
    private MultiTagTextBox multiTagTextBox1ByCssOffset3;

    //use parent css with offset
    @FindBy(css = "[data-name='tag1'] input[class*='ui-select-search']")
    private MultiTagTextBox multiTagTextBox1ByCssOffset4;

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1(String input)
    {
        multiTagTextBox1.clear();
        multiTagTextBox1.sendKeys(input);
        return this;
    }

    @Step("")
    public boolean isMultiTagTextBox1Displayed()
    {
        return multiTagTextBox1.isDisplayed();
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByOffset1(String input)
    {
        multiTagTextBox1ByCssOffset1.clear();
        multiTagTextBox1ByCssOffset1.sendKeys(input);
        return this;
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByOffset2(String input)
    {
        multiTagTextBox1ByCssOffset2.clear();
        multiTagTextBox1ByCssOffset2.sendKeys(input);
        return this;
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByOffset3(String input)
    {
        multiTagTextBox1ByCssOffset3.clear();
        multiTagTextBox1ByCssOffset3.sendKeys(input);
        return this;
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByOffset4(String input)
    {
        multiTagTextBox1ByCssOffset4.clear();
        multiTagTextBox1ByCssOffset4.sendKeys(input);
        return this;
    }

    @Step("")
    public List<String> getMultiTagTextBox1OptionsText()
    {
        return multiTagTextBox1.getOptions()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByVisibleText(String input)
    {
        multiTagTextBox1.selectOptionByVisibleText(input);
        return this;
    }

    @Step("")
    public MultiTagTextBoxSubPage setMultiTagTextBox1ByIndex(int index)
    {
        multiTagTextBox1.selectOptionByIndex(index);
        return this;
    }

    @Step("")
    public List<WebElement> getMultiTagTextBox1Tags()
    {
        return multiTagTextBox1.getTags();
    }

    @Step("")
    public MultiTagTextBoxSubPage deselectMultiTagTextBox1TagsByText(String input)
    {
        multiTagTextBox1.deselectTagByVisibleText(input, Operation.ALL);
        return this;
    }

    @Step("")
    public MultiTagTextBoxSubPage deselectMultiTagTextBox1TagsByIndex(int index)
    {
        multiTagTextBox1.deselectTagByIndex(index);
        return this;
    }
}
