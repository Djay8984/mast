package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.ClearableTextBox;

public class ClearableTextBoxSubPage extends BasePage<ClearableTextBoxSubPage>
{

    //use parent selector here
    @Visible
    @FindBy(css = "input-clear[ic-model='vm.inputWithClearBtnValue']")
    private ClearableTextBox oneDirectiveTextBox1;

    // same textbox but use child selector here - this is to test that the xpath redirector works
    @Visible
    @FindBy(css = "input-clear[ic-model='vm.inputWithClearBtnValue'] input")
    private ClearableTextBox oneDirectiveTextBox2;

    @Visible
    @FindBy(css = "input[add-clear-btn]")
    private ClearableTextBox separateDirectiveTextBox;

    @Step("")
    public ClearableTextBoxSubPage setOneDirectiveTextBox1(String input)
    {
        oneDirectiveTextBox1.sendKeys(input);
        return this;
    }

    @Step("")
    public ClearableTextBoxSubPage setOneDirectiveTextBox2(String input)
    {
        oneDirectiveTextBox2.sendKeys(input);
        return this;
    }

    @Step("")
    public ClearableTextBoxSubPage clickClearOneDirectiveTextBox1()
    {
        oneDirectiveTextBox1.clickClear();
        return this;
    }

    @Step("")
    public ClearableTextBoxSubPage clickClearOneDirectiveTextBox2()
    {
        oneDirectiveTextBox2.clickClear();
        return this;
    }

    @Step("")
    public String getClearOneDirectiveTextBox1()
    {
        return oneDirectiveTextBox1.getText();
    }

    @Step("")
    public String getClearOneDirectiveTextBox2()
    {
        return oneDirectiveTextBox2.getText();
    }

    @Step("")
    public ClearableTextBoxSubPage setseparateDirectiveTextBox(String input)
    {
        separateDirectiveTextBox.sendKeys(input);
        return this;
    }

    @Step("")
    public ClearableTextBoxSubPage clickClearSeparateDirectiveTextBox()
    {
        separateDirectiveTextBox.clickClear();
        return this;
    }

    @Step("")
    public String getSeparateDirectiveTextBox()
    {
        return separateDirectiveTextBox.getText();
    }
}
