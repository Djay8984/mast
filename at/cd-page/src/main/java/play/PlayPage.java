package play;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;

public class PlayPage extends BasePage<PlayPage>
{
    public static PlayPage open()
    {
        return PageFactory.newInstance(PlayPage.class, "http://localhost:8080/play", 20);
    }

    @Step("Get the multi Tag textbox section")
    public MultiTagTextBoxSubPage getMultiTagTextBoxPage()
    {
        return PageFactory.newInstance(MultiTagTextBoxSubPage.class);
    }

    @Step("Get the radio button section")
    public RadioButtonSubPage getRadioButtonSubPage()
    {
        return PageFactory.newInstance(RadioButtonSubPage.class);
    }

    @Step("Get the checkbox section")
    public CheckBoxSubPage getCheckBoxSubPage()
    {
        return PageFactory.newInstance(CheckBoxSubPage.class);
    }

    @Step("Get the checkbox section")
    public ClearableTextBoxSubPage getClearableTextBoxSubPage()
    {
        return PageFactory.newInstance(ClearableTextBoxSubPage.class);
    }

    @Step("Get the inputs & dropdown section")
    public InputsDropdownSubPage getInputsDropdownSubPage()
    {
        return PageFactory.newInstance(InputsDropdownSubPage.class);
    }

    @Step("Get the date picker & time picker section")
    public DatePickerTimePickerSubPage getDatePickerTimePickerSubPage() {
        return PageFactory.newInstance(DatePickerTimePickerSubPage.class);
    }

    @Step("Get the text area section")
    public TextAreaSubPage getTextAreaSubPage()
    {
        return PageFactory.newInstance(TextAreaSubPage.class);
    }

    @Step("Get the timeline section")
    public TimelineSubPage getTimelineSubPage()
    {
        return PageFactory.newInstance(TimelineSubPage.class);
    }
}
