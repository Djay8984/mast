package play;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import typifiedelement.TextArea;

public class TextAreaSubPage extends BasePage<TextAreaSubPage>
{
    @FindBy(css = "textarea")
    @Visible
    private TextArea textArea;

    @Step("Set input")
    public TextAreaSubPage setTextArea(String input)
    {
        textArea.sendKeys(input);
        return this;
    }

    @Step("Set text area by offset")
    public TextAreaSubPage setTextAreaSizeByOffset(int xOffset, int yOffset)
    {
        textArea.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Text Area - Get size")
    public Dimension getTextAreaDimension()
    {
        return textArea.getSize();
    }

    @Step("Get text from text area")
    public String getTextAreaText()
    {
        return textArea.getText();
    }
}
