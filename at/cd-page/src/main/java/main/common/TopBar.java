package main.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class TopBar extends BasePage<TopBar>
{

    @Visible
    @Name("Class Direct Logo")
    @FindBy(css = ".md-icon-button[aria-label='Asset Dashboard']")
    private WebElement classDirectLogo;

    @Visible
    @Name("My Account Button")
    @FindBy(css = "[data-ui-sref='user']")
    private WebElement myAccountButton;

}
