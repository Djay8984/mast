package main.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class FilterBarPage extends BasePage<FilterBarPage>
{
    @Visible
    @Name("Filter Asset Button")
    @FindBy(css = "[data-ng-click='vm.onClick()']")
    private WebElement filterAssetButton;

    @Visible
    @Name("Select Category DropDown Button")
    @FindBy(css = "[name='category'] .md-select-value")
    private WebElement selectCategoryDropDown;

    @Visible
    @Name("Search Text Box")
    @FindBy(css = "[data-ng-model='vm.searchTerm']")
    private WebElement searchTextBox;

    @Visible
    @Name("Sort By DropDown")
    @FindBy(css = "[data-ng-model='vm.selectedSortType']")
    private WebElement sortByDropDown;

    @Step("Click Filter Asset Side Bar")
    public FilterAssetsPage clickFilterAssertButton(){
        filterAssetButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(FilterAssetsPage.class);
    }
}
