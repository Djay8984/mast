package main.filter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.Datepicker;
import typifiedelement.RadioButton;

public class FilterAssetsPage extends BasePage<FilterAssetsPage>
{

    @Name("Filter Asset Side Bar")
    @FindBy(css = "[data-ng-click='vm.onClick()']")
    private WebElement filterAssetSideBar;

    @Visible
    @Name("All Vessels RadioButton")
    @FindBy(css = "md-radio-button[value='all']")
    private RadioButton allVesselsRadioButton;

    @Visible
    @Name("Pagination Count Text")
    @FindBy(css = "[data-total-count='vm.totalCount'] [data-translate='cd-result-summary']")
    private WebElement paginationCountText;

    @Visible
    @Name("My Favourites Button")
    @FindBy(css = "md-radio-button[value='favourites']")
    private WebElement myFavouriteRadioButton;

    @Visible
    @Name("From Date Of Build Button")
    @FindBy(css = "input[aria-label='From']")
    private Datepicker fromDateOfBuildButton;

    @Visible
    @Name("To Date Of Build Button")
    @FindBy(css = "input[aria-label='To']")
    private Datepicker toDateOfBuildButton;

    @Step("Set From Date Of Build")
    public FilterAssetsPage setFromDateOfBuild(String date)
    {
        fromDateOfBuildButton.clear();
        fromDateOfBuildButton.sendKeys(date);
        fromDateOfBuildButton.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set To date Of Build")
    public FilterAssetsPage setToDateOfBuild(String date)
    {
        toDateOfBuildButton.clear();
        toDateOfBuildButton.sendKeys(date);
        toDateOfBuildButton.sendKeys(Keys.TAB);
        return this;
    }

    //TODO: This method is used to act as search button here and is
    //to be removed once the original implementation is done
    @Step("Click Filter Asset Side Bar")
    public <T extends BasePage<T>> T clickFilterAssertButton(Class<T> pageObjectClass)
    {
        filterAssetSideBar.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(pageObjectClass);
    }
}
