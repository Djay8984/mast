package main.landing;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import main.common.TopBar;
import main.landing.vessellist.VesselListPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class LandingPage extends BasePage<LandingPage>
{

    public static TopBar getTopBar()
    {
        return PageFactory.newInstance(TopBar.class);
    }

    @Visible
    @Name("Vessel List Tab")
    @FindBy(css = "[md-tab-scroll*='mdTabsCtrl'] md-tab-item:nth-child(1)")
    private WebElement vesselListTab;

    @Visible
    @Name("Service Schedule Tab")
    @FindBy(css = "[md-tab-scroll*='mdTabsCtrl'] md-tab-item:nth-child(2)")
    private WebElement serviceScheduleTab;

    @Visible
    @Name("Administration Tab")
    @FindBy(css = "[md-tab-scroll*='mdTabsCtrl'] md-tab-item:nth-child(3)")
    private WebElement administrationTab;

    @Step("Click vessel list tab")
    public VesselListPage clickVesselListTab()
    {
        vesselListTab.click();
        return PageFactory.newInstance(VesselListPage.class);
    }

    /**
     * This method is used to get VesselListPage after LandingPage loads with VesselListPage as
     * the default page displayed
     * <p>
     * {@link #clickVesselListTab()} is used when {@link VesselListPage} is a result of the tab
     * click
     *
     * @return {@code VesselListPage}
     */
    @Step("Get vessel list page")
    public VesselListPage getVesselListPage()
    {
        return PageFactory.newInstance(VesselListPage.class);
    }

    public static LandingPage open()
    {
        return PageFactory.newInstance(LandingPage.class, "http://localhost:8080/fleet/vessel-list", 20);
    }
}

