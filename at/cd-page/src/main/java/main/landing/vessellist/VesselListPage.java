package main.landing.vessellist;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import main.element.AssetCard;
import main.filter.FilterBarPage;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class VesselListPage extends BasePage<VesselListPage>
{

    @Name("Asset Cards")
    private List<AssetCard> assetCards;

    @Step("Get Asset Cards")
    public List<AssetCard> getAssetCards(){
        return assetCards;
    }

    public FilterBarPage getFilterPage(){
        return PageFactory.newInstance(FilterBarPage.class);
    }
}
