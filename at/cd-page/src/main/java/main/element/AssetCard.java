package main.element;

import com.frameworkium.core.ui.annotations.Visible;
import constant.ClassDirect;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.text.ParseException;
import java.util.Date;

@Name("Asset Cards")
@FindBy(css = "[data-asset='asset']")
public class AssetCard extends HtmlElement
{

    @Visible
    @Name("Asset Emblem")
    @FindBy(css = "[data-type='vm.asset.type']")
    private WebElement assetEmblem;

    @Visible
    @Name("Asset Name")
    @FindBy(css = ".card-name.ng-binding")
    private WebElement assetName;

    @Visible
    @Name("Imo Number")
    @FindBy(css = ".info-detail[data-ng-show*='imo']")
    private WebElement imoNumber;

    @Visible
    @Name("Date of build")
    @FindBy(css = ".info-detail[data-ng-show*='buildDate']")
    private WebElement getDateOfBuild;

    @Step("Get Date Of Build")
    public Date getDateOfBuild()
    {
        Date date;
        String dateOfBuild = getDateOfBuild.getText();
        try
        {
            date = ClassDirect.FRONTEND_TIME_FORMAT.parse(dateOfBuild);
        }
        catch (ParseException e)
        {
            date = null;
        }
        return date;
    }
}
