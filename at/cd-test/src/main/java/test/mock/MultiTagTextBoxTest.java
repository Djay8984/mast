package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import play.MultiTagTextBoxSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class MultiTagTextBoxTest extends BaseTest
{
    @Test(description = "mock test for multi tag textbox")
    @Issue("MOCK")
    public void test()
    {
        //---------Check that css redirect works and getOption works
        MultiTagTextBoxSubPage mttbp = PlayPage.open()
                .getMultiTagTextBoxPage();
        List<String> options = mttbp.setMultiTagTextBox1("a")
                .getMultiTagTextBox1OptionsText();
        assertThat(options).containsExactly("England", "France", "Malaysia", "England", "France",
                "Malaysia", "England", "France", "Malaysia");

        options = mttbp.setMultiTagTextBox1ByOffset1("f")
                .getMultiTagTextBox1OptionsText();
        assertThat(options).containsExactly("France", "France", "France");

        options = mttbp.setMultiTagTextBox1ByOffset2("c")
                .getMultiTagTextBox1OptionsText();
        assertThat(options).containsExactly("Greece", "France", "Greece", "France", "Greece",
                "France");

        options = mttbp.setMultiTagTextBox1ByOffset3("d")
                .getMultiTagTextBox1OptionsText();
        assertThat(options).containsExactly("England", "England", "England");

        options = mttbp.setMultiTagTextBox1ByOffset4("s")
                .getMultiTagTextBox1OptionsText();
        assertThat(options).containsExactly("Malaysia", "Malaysia", "Malaysia");

        //--------Check that getTag works
        List<WebElement> tags = mttbp.setMultiTagTextBox1("e")
                .setMultiTagTextBox1ByVisibleText("England")
                .getMultiTagTextBox1Tags();
        assertThat(tags.get(0).getText().replaceFirst("^\\s×\\s", "")).isEqualTo("England");

        tags = mttbp.deselectMultiTagTextBox1TagsByText("England")
                .getMultiTagTextBox1Tags();
        assertThat(tags.size()).isEqualTo(0);

        tags = mttbp.setMultiTagTextBox1("e")
                .setMultiTagTextBox1ByVisibleText("France")
                .setMultiTagTextBox1("m")
                .setMultiTagTextBox1ByIndex(0)
                .getMultiTagTextBox1Tags();
        assertThat(tags.get(0).getText().replaceFirst("^\\s×\\s", "")).isEqualTo("France");
        assertThat(tags.get(1).getText().replaceFirst("^\\s×\\s", "")).isEqualTo("Malaysia");

        tags = mttbp.deselectMultiTagTextBox1TagsByIndex(0)
                .getMultiTagTextBox1Tags();
        assertThat(tags.size()).isEqualTo(1);
        assertThat(tags.get(0).getText().replaceFirst("^\\s×\\s", "")).isEqualTo("Malaysia");
    }
}
