package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.InputsDropdownSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class NumericTextBoxTest extends BaseTest
{

    @Test(description = "This is to test the typified NumericTextBox")
    @Issue("MOCK")
    public void test()
    {

        // Navigate to Play page
        InputsDropdownSubPage inputsDropdownSubPage = PlayPage.open()
                .getInputsDropdownSubPage();

        // Verify clearing the text box #1
        inputsDropdownSubPage.clearHourlyRate1();
        assertThat(inputsDropdownSubPage.getHourlyRate1().isEmpty()).isTrue();

        // Verify entering the text box #1
        inputsDropdownSubPage.setHourlyRate1("900");
        assertThat(inputsDropdownSubPage.getHourlyRate1()).isEqualTo("900");

        // Verify clicking on count up arrow for the text box #1
        inputsDropdownSubPage.clickHourlyRateCountUpButton1();
        assertThat(inputsDropdownSubPage.getHourlyRate1()).isEqualTo("901");

        // Verify clicking on count down arrow for the text box #1
        inputsDropdownSubPage.clickHourlyRateCountDownButton1();
        assertThat(inputsDropdownSubPage.getHourlyRate1()).isEqualTo("900");

        // Verify retrieving a label text for the text box #1
        assertThat(inputsDropdownSubPage.getHourlyRateLabelText1())
                .isEqualTo("Hourly Rate (USD)");

        // Verify clearing the text box #2
        inputsDropdownSubPage.clearHourlyRate2();
        assertThat(inputsDropdownSubPage.getHourlyRate2().isEmpty()).isTrue();

        // Verify entering the text box #2
        inputsDropdownSubPage.setHourlyRate2("800");
        assertThat(inputsDropdownSubPage.getHourlyRate2()).isEqualTo("800");

        // Verify clicking on count up arrow for the text box #2
        inputsDropdownSubPage.clickHourlyRateCountUpButton2();
        assertThat(inputsDropdownSubPage.getHourlyRate2()).isEqualTo("801");

        // Verify clicking on count down arrow for the text box #2
        inputsDropdownSubPage.clickHourlyRateCountDownButton2();
        assertThat(inputsDropdownSubPage.getHourlyRate2()).isEqualTo("800");

        // Verify retrieving a label text for the text box #2
        assertThat(inputsDropdownSubPage.getHourlyRateLabelText2())
                .isEqualTo("Hourly Rate (USD)");
    }
}
