package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.ClearableTextBoxSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class ClearableTextBoxTest extends BaseTest
{
    @Test(description = "This is to test the typified clearable TextBox")
    @Issue("MOCK")
    public void test()
    {

        final String testString1 = "Yabba";
        final String testString2 = "Dabba";

        ClearableTextBoxSubPage ctbsp = PlayPage.open()
                .getClearableTextBoxSubPage();
        String s = ctbsp.clickClearOneDirectiveTextBox1()
                .setOneDirectiveTextBox1(testString1)
                .getClearOneDirectiveTextBox1();
        assertThat(s).isEqualTo(testString1);

        s = ctbsp.clickClearOneDirectiveTextBox1()
                .getClearOneDirectiveTextBox1();
        assertThat(s).isEqualTo("");

        s = ctbsp.clickClearOneDirectiveTextBox2()
                .setOneDirectiveTextBox2(testString2)
                .getClearOneDirectiveTextBox2();
        assertThat(s).isEqualTo(testString2);

        s = ctbsp.clickClearOneDirectiveTextBox2()
                .getClearOneDirectiveTextBox2();
        assertThat(s).isEqualTo("");

        s = ctbsp.clickClearSeparateDirectiveTextBox()
                .setseparateDirectiveTextBox(testString1)
                .getSeparateDirectiveTextBox();
        assertThat(s).isEqualTo(testString1);

        s = ctbsp.clickClearSeparateDirectiveTextBox()
                .getSeparateDirectiveTextBox();
        assertThat(s).isEqualTo("");
    }
}
