package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.PlayPage;
import play.TimelineSubPage;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assert_;

public class TimelineTest extends BaseTest
{
    @Test(description = "This is to test the typified Timeline")
    @Issue("MOCK")
    public void test()
    {
        // Navigate to Play page
        TimelineSubPage timelineSubPage = PlayPage.open().getTimelineSubPage();

        // Verify selecting a date range
        timelineSubPage.selectDateRangeA("Jan 2015", "Jan 2016");
        assertThat(timelineSubPage.getSelectedDatesA()).isEqualTo(Arrays.asList("Jan 2015", "Jan 2016"));

        // Verify selecting a date range after the previously selected range
        timelineSubPage.selectDateRangeB("Jan 2017", "Jan 2018");
        assertThat(timelineSubPage.getSelectedDatesB()).isEqualTo(Arrays.asList("Jan 2017", "Jan 2018"));

        // Verify selecting a start date on the left boundary
        timelineSubPage.selectStartDateA("Apr 2014");
        assertThat(timelineSubPage.getSelectedDatesA().get(0)).isEqualTo("Apr 2014");

        // Verify selecting an end date on the right boundary
        timelineSubPage.selectEndDateB("Apr 2021");
        assertThat(timelineSubPage.getSelectedDatesB().get(1)).isEqualTo("Apr 2021");

        // Negative test - Verify selecting a start date out of scale
        try
        {
            timelineSubPage.selectStartDateA("Mar 2014");
            assert_().fail("");
        }
        catch (IllegalArgumentException e)
        {
            assertThat(e).hasMessage("Date 'Mar 2014' is outside the available scale.");
        }

        // Negative test - Verify selecting an end date out of scale
        try
        {
            timelineSubPage.selectEndDateA("May 2021");
            assert_().fail("");
        }
        catch (IllegalArgumentException e)
        {
            assertThat(e).hasMessage("Date 'May 2021' is outside the available scale.");
        }

        // Negative test - Verify selecting a start date after an end date
        try
        {
            timelineSubPage.selectEndDateA("Jan 2016");
            timelineSubPage.selectStartDateA("Feb 2016");
            assert_().fail("");
        }
        catch (IllegalArgumentException e)
        {
            assertThat(e).hasMessage("Start date 'Feb 2016' is either same or after the end date 'Jan 2016'.");
        }

        // Negative test - Verify selecting an end date before a start date
        try
        {
            timelineSubPage.selectStartDateA("Jan 2015");
            timelineSubPage.selectEndDateA("Dec 2014");
            assert_().fail("");
        }
        catch (IllegalArgumentException e)
        {
            assertThat(e).hasMessage("Start date 'Jan 2015' is either same or after the end date 'Dec 2014'.");
        }
    }
}
