package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.DatePickerTimePickerSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assertThat;

public class DatepickerTest extends BaseTest {

    private static final SimpleDateFormat FRONTEND_TIME_FORMAT = new SimpleDateFormat("dd MMM yyyy");

    @Test(description = "This is to test the typified Datepicker")
    @Issue("MOCK")
    public void test() {

        String startDate1 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now())));
        String startDate2 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(15))));
        String startDate3 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(30))));
        String startDate4 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(60))));
        LocalDate startDate5 = LocalDate.now().plusDays(730);
        LocalDate startDate6 = LocalDate.now().plusDays(365);

        DatePickerTimePickerSubPage datePickerTimePickerSubPage = PlayPage.open()
                .getDatePickerTimePickerSubPage();

        System.out.println(String.format("Ver1 - Verified date: %s", startDate1));
        datePickerTimePickerSubPage.setStartDateByCalendarPane1(startDate1);
        assertThat(datePickerTimePickerSubPage.getStartDate1()).isEqualTo(startDate1);

        System.out.println(String.format("Ver2 - Verified date: %s", startDate2));
        datePickerTimePickerSubPage.setStartDateByCalendarPane2(startDate2);
        assertThat(datePickerTimePickerSubPage.getStartDate2()).isEqualTo(startDate2);

        System.out.println(String.format("Ver3 - Verified date: %s", startDate3));
        datePickerTimePickerSubPage.setStartDate1(startDate3);
        assertThat(datePickerTimePickerSubPage.getStartDate1()).isEqualTo(startDate3);

        System.out.println(String.format("Ver4 - Verified date: %s", startDate4));
        datePickerTimePickerSubPage.setStartDate2(startDate4);
        assertThat(datePickerTimePickerSubPage.getStartDate1()).isEqualTo(startDate4);

        String expectedDate5 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(startDate5)));
        System.out.println(String.format("Ver5 - Verified date: %s", expectedDate5));
        datePickerTimePickerSubPage.setStartDateByCalendarPane1(startDate5);
        assertThat(datePickerTimePickerSubPage.getStartDate1()).isEqualTo(expectedDate5);

        String expectedDate6 = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(startDate6)));
        System.out.println(String.format("Ver6 - Verified date: %s", expectedDate6));
        datePickerTimePickerSubPage.setStartDateByCalendarPane2(startDate6);
        assertThat(datePickerTimePickerSubPage.getStartDate2()).isEqualTo(expectedDate6);
    }
}