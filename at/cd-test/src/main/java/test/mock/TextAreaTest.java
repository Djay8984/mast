package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.Dimension;
import org.testng.annotations.Test;
import play.PlayPage;
import play.TextAreaSubPage;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class TextAreaTest extends BaseTest
{
    @Test(description = "This is to test the typified text area")
    @Issue("MOCK")
    public void test()
    {
        TextAreaSubPage tasp = PlayPage.open()
                .getTextAreaSubPage();
        Dimension d = tasp.getTextAreaDimension();
        tasp.setTextArea("Banana")
                .setTextAreaSizeByOffset(0, 100);
        assertThat(tasp.getTextAreaDimension().height).isEqualTo(d.height + 100);

        assertThat(tasp.getTextAreaText()).isEqualTo("Banana");
    }
}
