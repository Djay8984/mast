package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.CheckBoxSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class CheckBoxTest extends BaseTest
{

    @Test(description = "This is to test the typified CheckBox")
    @Issue("MOCK")
    public void test()
    {
        CheckBoxSubPage cbsp = PlayPage.open()
                .getCheckBoxSubPage();
        cbsp.selectAppleCheckBox();
        assertThat(cbsp.isAppleCheckBoxChecked()).isTrue();
        assertThat(cbsp.isBananaCheckBoxChecked()).isFalse();

        cbsp.selectBananaCheckBox();
        assertThat(cbsp.isAppleCheckBoxChecked()).isTrue();
        assertThat(cbsp.isBananaCheckBoxChecked()).isTrue();

        cbsp.deselectAppleCheckBox();
        assertThat(cbsp.isAppleCheckBoxChecked()).isFalse();
        assertThat(cbsp.isBananaCheckBoxChecked()).isTrue();

        cbsp.deselectBananaCheckBox();
        assertThat(cbsp.isAppleCheckBoxChecked()).isFalse();
        assertThat(cbsp.isBananaCheckBoxChecked()).isFalse();

        assertThat(cbsp.getAppleRadioCheckBoxText()).isEqualTo("Apple");
        assertThat(cbsp.getBananaRadioCheckBoxText()).isEqualTo("Banana");
    }
}
