package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.InputsDropdownSubPage;
import play.PlayPage;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class DropdownTest extends BaseTest
{

    @Test(description = "This is to test the typified Dropdown")
    @Issue("MOCK")
    public void test()
    {

        // Navigate to Play page
        InputsDropdownSubPage inputsDropdownSubPage = PlayPage.open()
                .getInputsDropdownSubPage();

        // Verify available project type options for the dropdown #1
        List<String> options1 = inputsDropdownSubPage.getProjectTypeOptionsText1();
        assertThat(options1).isEqualTo(Arrays.asList("Application", "Website"));

        // Verify selecting an option by text for the dropdown #1
        inputsDropdownSubPage.selectProjectType1("Application");
        assertThat(inputsDropdownSubPage.getProjectTypeSelectedOption1()).isEqualTo("Application");

        // Verify selecting an option by index for the dropdown #1
        inputsDropdownSubPage.selectProjectType1(1);
        assertThat(inputsDropdownSubPage.getProjectTypeSelectedOption1()).isEqualTo("Website");

        // Verify available project type options for the dropdown #2
        List<String> options2 = inputsDropdownSubPage.getProjectTypeOptionsText2();
        assertThat(options2).isEqualTo(Arrays.asList("Application", "Website"));

        // Verify selecting an option by text for the dropdown #2
        inputsDropdownSubPage.selectProjectType2("Application");
        assertThat(inputsDropdownSubPage.getProjectTypeSelectedOption2()).isEqualTo("Application");

        // Verify selecting an option by index for the dropdown #2
        inputsDropdownSubPage.selectProjectType2(1);
        assertThat(inputsDropdownSubPage.getProjectTypeSelectedOption2()).isEqualTo("Website");
    }
}
