package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import play.PlayPage;
import play.RadioButtonSubPage;
import ru.yandex.qatools.allure.annotations.Issue;

import static com.google.common.truth.Truth.assertThat;

public class RadioButtonTest extends BaseTest
{

    @Test(description = "This is to test the typified RadioButton")
    @Issue("MOCK")
    public void test()
    {
        RadioButtonSubPage rbsp = PlayPage.open()
                .getRadioButtonSubPage();
        rbsp.selectAppleRadioButton();
        assertThat(rbsp.isAppleRadioButtonSelected()).isTrue();
        assertThat(rbsp.isBananaRadioButtonSelected()).isFalse();

        rbsp.selectBananaRadioButton();
        assertThat(rbsp.isAppleRadioButtonSelected()).isFalse();
        assertThat(rbsp.isBananaRadioButtonSelected()).isTrue();

        assertThat(rbsp.getAppleRadioButtonText()).isEqualTo("Apple");
        assertThat(rbsp.getBananaRadioButtonText()).isEqualTo("Banana");
    }
}
