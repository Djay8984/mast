<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
<suite name="Lloyds Register Automation">
    <test name="All Tests" preserve-order="true" verbose="2">
        <packages>
            <package name="test.*">
                <exclude name="test.mock.*"/>
            </package>
        </packages>
    </test>
</suite>