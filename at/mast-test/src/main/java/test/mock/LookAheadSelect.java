package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.FlagSearchSubPage;

public class LookAheadSelect extends BaseTest
{
    @Test(description = "Typified look ahead select dropdown")
    @Issue("MOCK")
    public void test()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        FlagSearchSubPage flagSearchSubPage = searchFilterSubPage.clickFlagFilterButton();

        flagSearchSubPage.setFlagCode("A")
                .selectFlagFromDropdown("Albania");
    }
}
