package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class AddToJobScopeTables extends BaseTest
{

    @Test(description = "Add to job scope tables with children and grandchildren elements")
    @Issue("MOCK")
    public void test()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput("504")
                .clickAssetSearchApplyButton();

        List<AllJobElement> allJobElements = workHubPage.clickMyWorkTab()
                .getAssetCards()
                .get(0)
                .clickViewAssetButton()
                .clickJobsTab()
                .getAllJobsElements();
        BaseServiceTable c = allJobElements.get(0)
                .clickNavigateToJobArrowButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab()
                .getClassificationTable();
        c.getRows().get(0).getChildren().get(0).clickPlusOrMinusIcon();

        assertThat(c.getRows()
                .get(0)
                .getText())
                .isEqualTo("Classification");
        assertThat(c.getRows().size())
                .isEqualTo(1);
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getText())
                .isEqualTo("Hull");
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .get(10)
                .getText())
                .isEqualTo("Transfer of Class");
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .size())
                .isEqualTo(11);
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getServiceRows()
                .get(0)
                .getServiceCode())
                .isEqualTo("ECIL");
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getServiceRows()
                .size())
                .isEqualTo(27);
        assertThat(c.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getServiceRows()
                .get(26)
                .getServiceName())
                .isEqualTo("Underwater Examination");
    }
}
