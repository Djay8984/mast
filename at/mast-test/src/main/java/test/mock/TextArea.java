package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.Dimension;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import testhub.TestHubPage;
import workhub.WorkHubPage;

public class TextArea extends BaseTest
{

    @Test(description = "mock test for resizing text area")
    @Issue("MOCK")
    public void test()
    {

        TestHubPage testHubPage = WorkHubPage.open().clickTestHubTab();
        Dimension d1 = testHubPage.getTextAreaDimension();
        testHubPage.setTextAreaSizeByOffset(0, 100);
        Dimension d2 = testHubPage.getTextAreaDimension();

        System.out.println("Original: " + d1 + "\n"
                + "New: " + d2 + "\n");

        testHubPage.move();

        System.out.println("END");
    }
}
