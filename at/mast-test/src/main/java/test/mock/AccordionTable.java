package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.jobs.details.crediting.base.BaseServiceTable;
import viewasset.sub.jobs.details.crediting.element.ServiceElement;
import workhub.WorkHubPage;

public class AccordionTable extends BaseTest
{

    @Test(description = "Test accordion table with parent-child-grandchildren relationship")
    @Issue("MOCK")
    public void test()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput("504")
                .clickAssetSearchApplyButton();
        BaseServiceTable c = workHubPage.clickMyWorkTab().getAssetCards().get(0)
                .clickViewAssetButton()
                .clickJobsTab().getAllJobsElements().get(0)
                .clickNavigateToJobArrowButton()
                .clickGoToCreditingButton()
                .getClassificationTable();
        ServiceElement e = c.getRows()
                .get(2)
                .clickPlusButton()
                .getChildren()
                .get(0)
                .clickPlusButton()
                .getChildren()
                .get(0)
                .clickPlusButton()
                .getChildren()
                .get(0)
                .clickPlusButton()
                .clickPlusButton()
                .getChildren()
                .get(0);
        System.out.println(e.getText());
    }
}
