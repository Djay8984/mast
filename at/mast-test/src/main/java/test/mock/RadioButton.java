package test.mock;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import testhub.TestHubPage;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assertThat;

/**
 * Created by mlee1 on 27/8/2016.
 */
public class RadioButton extends BaseTest
{
    @Test(description = "Mock test for typified Radio Button")
    @Issue("MOCK")
    public void test()
    {
        TestHubPage testHubPage = WorkHubPage.open().clickTestHubTab();
        assertThat(testHubPage.isYesRadioButtonSelected()).isFalse();
        testHubPage.selectYesRadioButton();
        assertThat(testHubPage.isYesRadioButtonSelected()).isTrue();
        testHubPage.selectNoRadioButton();
        assertThat(testHubPage.isYesRadioButtonSelected()).isFalse();
    }
}
