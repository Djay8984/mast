package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import org.apache.commons.collections.comparators.NullComparator;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

public class AssetOrderingTest extends BaseTest
{
    private Comparator<AssetData> ascendingIMOThenAssetDataComparator = (assetOne, assetTwo) ->
    {
        final NullComparator nullComparator = new NullComparator();

        final int result = nullComparator.compare(assetOne.getImo(), assetTwo.getImo());

        if (result != 0)
        {
            return result;
        }

        return nullComparator.compare(assetOne.getAssetId(), assetTwo.getAssetId());

    };
    private Comparator<AssetData> ascendingAssetThenIMODataComparator = (assetOne, assetTwo) ->
    {
        final NullComparator nullComparator = new NullComparator();

        final int result = nullComparator.compare(assetOne.getAssetId(), assetTwo.getAssetId());

        if (result != 0)
        {
            return result;
        }

        return nullComparator.compare(assetOne.getImo(), assetTwo.getImo());

    };

    @Test(description = "Order Assets By IMO and Asset ID Ascending Then Descending")
    public void orderAssetsByIMOAndAssetIdTestAscendingThenDescending() throws ClientProtocolException, IOException
    {
        final WorkHubPage workHubPage = WorkHubPage.open();

        // Get all assets that may be displayed, and select the asset sort for IMO and Asset Id ascending

        final AllAssetsSubPage allAssetsSubPage = workHubPage
                .clickAllAssetsTab()
                .setAssetSortOrder("IMO Number/Asset Identifier Ascending");

        // Test the assets are sorted as expected

        testTheIMOAndAssetIdOrder(allAssetsSubPage, ascendingIMOThenAssetDataComparator);

        // Now select the descending sort

        allAssetsSubPage
                .setAssetSortOrder("IMO Number/Asset Identifier Descending");

        // Test the assets are sorted as expected

        testTheIMOAndAssetIdOrder(allAssetsSubPage, ascendingAssetThenIMODataComparator.reversed());
    }

    private void testTheIMOAndAssetIdOrder(final AllAssetsSubPage allAssetsSubPage, final Comparator<AssetData> localComparator)
    {
        // Map them to simpler objects to avoid multiple page reads and page staleness.

        List<AssetData> assetDataFromThePage = allAssetsSubPage
                .getAssetCards()
                .stream()
                .map(asset -> new AssetData(
                        asset.isIMODisplayed() ? Long.decode(asset.getImoNumber()) : null,
                        asset.isAssetIdDisplayed() ? Long.decode(asset.getAssetId()) : null))

                .collect(Collectors.toList());

        // Locally apply a sort to a new list of simpler objects that we expect from the page.

        List<AssetData> expectedSortedData = assetDataFromThePage.stream()
                .sorted(localComparator)
                .collect(Collectors.toList());

        // Assert that the sorted list from the page and the locally sorted list are the same.

        assertThat(assetDataFromThePage)
                .containsExactlyElementsIn(expectedSortedData).inOrder();
    }

    private class AssetData
    {
        private final Long imo;
        private final Long assetId;

        public AssetData(final Long imo, final Long assetId)
        {
            this.imo = imo;
            this.assetId = assetId;
        }

        public Long getImo()
        {
            return imo;
        }

        public Long getAssetId()
        {
            return assetId;
        }

    }

}
