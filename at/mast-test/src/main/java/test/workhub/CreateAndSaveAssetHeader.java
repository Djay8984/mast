package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.DatabaseHelper;
import helper.TestDataHelper;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.cases.CasesPage;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.sub.IMORegisteredAssetPage;
import workhub.sub.sub.NonIMORegisteredAssetPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.*;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class CreateAndSaveAssetHeader extends BaseTest
{

    private final String[] expectedVesselAssetType = {
            "Cargo Carrying",
            "Tankers",
            "Liquefied Gas",
            "LNG Tanker",
            "LNG Tanker"};

    private final String[] expectedAcvAssetType = {
            "Non Ship Structures",
            "Non Ship Structures",
            "Non Ship Structures",
            "Air Cushion Vehicle (Hovercraft)",
            "Air Cushion Vehicle Crew Boat"};

    private final String[] expectedBuoyAssetType = {
            "Non Ship Structures",
            "Non Ship Structures",
            "Non Ship Structures",
            "Buoy",
            "Mooring Buoy"};

    private final String[] expectedFloatingDockAssetType = {
            "Non Ship Structures",
            "Non Ship Structures",
            "Non Ship Structures",
            "Floating Dock",
            "Floating Dock"};

    private final String[] expectedLinkSpanAssetType = {
            "Non Ship Structures",
            "Non Ship Structures",
            "Non Ship Structures",
            "Linkspan/Jetty",
            "Linkspan/Jetty"};

    private final String[] expectedShipLiftAssetType = {
            "Non Ship Structures",
            "Non Ship Structures",
            "Non Ship Structures",
            "Floating Dock",
            "Mechanical Lift Dock"};

    @DataProvider(name = "AssetCategoryTypes")
    public Object[][] assets()
    {
        return new Object[][]{
                {"ACV", expectedAcvAssetType},
                {"Buoy", expectedBuoyAssetType},
                {"Floating Dock", expectedFloatingDockAssetType},
                {"Linkspan", expectedLinkSpanAssetType},
                {"Ship Lift", expectedShipLiftAssetType},
                {"Vessel", expectedVesselAssetType}
        };
    }

    @Test(description = "US8.14. AC1-3 - Create and save IMO registered asset header",
            dataProvider = "AssetCategoryTypes")
    @Issue("LRT-1599")
    public void createAndSaveImoRegisteredAssetHeaderTest(String assetCategory, String[] assetTypes) throws SQLException
    {
        String assetName = "AutoKL " + RandomStringUtils.randomNumeric(10);
        String buildDate = FRONTEND_TIME_FORMAT.format(TestDataHelper.randomDate());

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        IMORegisteredAssetPage iMORegisteredAssetPage
                = addNewAssetPage.clickImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Non-IMO Registered asset button is expected to be selected")
                .that(addNewAssetPage.isIMORegisteredAssetButtonIsSelected())
                .isTrue();

        //AC: 1
        //Step: 3
        assert_().withFailureMessage("Create Asset Button is expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 4
        //Step: 4

        List<String> imoNumber = getImoNumber();
        List<String> lrNumber = getLrNumber();
        String uniqueImoNumber = lrNumber.stream()
                .filter(e -> !imoNumber.contains(e)).findFirst().get();

        iMORegisteredAssetPage
                .setIMONumber(uniqueImoNumber)
                .then().setAssetName(assetName)
                .then().clickAssetCategoryButton()
                .then().selectAssetCategoryByName(assetCategory)
                .then().clickAssetTypeButton()
                .then().setStatcodeOneDropdown(assetTypes[0])
                .then().setStatcodeTwoDropdown(assetTypes[1])
                .then().setStatcodeThreeDropdown(assetTypes[2])
                .then().setStatcodeFourDropdown(assetTypes[3])
                .then().setStatcodeFiveDropdown(assetTypes[4])
                .then().clickSelectAssetTypeSelectButton()
                .then().setDateOfBuild(buildDate);

        assert_().withFailureMessage("Create Asset Button is expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isTrue();

        //AC: 2
        //Step: 5
        CasesPage casesPage = addNewAssetPage.clickCreateAssetButton();
        assert_().withFailureMessage("Asset common page header is expected to be displayed")
                .that(casesPage.isAssetSubHeaderDisplayed()).isTrue();

        casesPage.clickAssetHeaderLogo();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(assetName).then().clickAssetSearchApplyButton();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(assetName);

        //AC: 3
        //Step: 6
        assert_().withFailureMessage("Asset Name is expected to be " + assetName)
                .that(allAssetsAssetElement.getAssetName().equals(assetName))
                .isTrue();
        assert_().withFailureMessage("Asset Type is expected to be " + assetTypes[4])
                .that(allAssetsAssetElement.getAssetType()
                        .equals(assetTypes[4]))
                .isTrue();
        assert_().withFailureMessage("Build date is expected to be " + buildDate)
                .that(allAssetsAssetElement.getBuildDate().equals(buildDate))
                .isTrue();

        String beImoNumber = getImoNumberByName(assetName);
        assert_().withFailureMessage("IMO Number is expected to be " + beImoNumber)
                .that(allAssetsAssetElement.getImoNumber().equals(beImoNumber))
                .isTrue();

        //AC: 4 and 5
        //Step: 7
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        assert_().withFailureMessage("Top Level Item" + assetCategory + "is expected to display")
                .that(assetModelSubPage.getBreadcrumbText().equals(assetCategory))
                .isTrue();
    }

    @Test(description = "US8.14. AC1-3 - Create and save non-IMO registered asset header")
    @Issue("LRT-1600")
    public void createAndSaveNonImoRegisteredAssetHeaderTest() throws SQLException
    {
        String builderName = RandomStringUtils.randomAlphabetic(10);
        String yardNumber = String.valueOf(RandomStringUtils.randomNumeric(9));
        String assetName = "AutoKL " + RandomStringUtils.randomNumeric(10);

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage
                = addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Non-IMO Registered asset button is expected to be selected")
                .that(addNewAssetPage.isNonIMORegisteredAssetButtonSelected())
                .isTrue();

        //AC: 1
        //Step: 3
        assert_().withFailureMessage("Create Asset Button is expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 4
        //Step: 4
        nonIMORegisteredAssetPage.setBuilderName(builder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild);

        assert_().withFailureMessage("Create Asset Button is expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isTrue();

        //AC: 2
        //Step: 5
        CasesPage casesPage = addNewAssetPage.clickCreateAssetButton();
        assert_().withFailureMessage("Asset commonpage header is expected to be displayed")
                .that(casesPage.isAssetSubHeaderDisplayed()).isTrue();

        casesPage.clickAssetHeaderLogo();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(assetName).then().clickAssetSearchApplyButton();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(assetName);

        //AC: 3
        //Step: 6
        assert_().withFailureMessage("Asset Name is expected to be " + assetName)
                .that(allAssetsAssetElement.getAssetName()).isEqualTo(assetName);
        assert_().withFailureMessage("Asset Type is expected to be " + statCode4)
                .that(allAssetsAssetElement.getAssetType()).isEqualTo(statCode4);
        assert_().withFailureMessage("Build date is expected to be " + dateOfBuild)
                .that(allAssetsAssetElement.getBuildDate()).isEqualTo(dateOfBuild);

        String beImoNumber = getImoNumberByName(assetName);
        assert_().withFailureMessage("IMO Number is expected to be " + beImoNumber)
                .that(allAssetsAssetElement.getAssetId()).isEqualTo(beImoNumber);

        //AC: 4 and 5
        //Step: 7
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        assert_().withFailureMessage("Top Level Item" + assetCategory + "is expected to display")
                .that(assetModelSubPage.getBreadcrumbText()).isEqualTo(assetCategory);
    }

    //Get assetId from DB based on assetName
    //When IMO number = null, assetID is show as IMO number in FE
    private String getImoNumberByName(String name) throws SQLException
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_VERSIONEDASSET WHERE name = '" + name + "';");
        String getId = "0";
        try
        {
            rs.first();
            getId = Integer.toString(rs.getInt("id"));
            return getId;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return getId;
    }

    private List<String> getImoNumber() throws SQLException
    {
        ArrayList<String> imoNumbers = new ArrayList<>();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_VERSIONEDASSET ;");
        while (rs.next())
        {
            String ids = rs.getString("imo_number");
            imoNumbers.add(ids);
        }
        return imoNumbers;
    }

    private List<String> getLrNumber() throws SQLException
    {
        ArrayList<String> lrNumbers = new ArrayList<>();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM ihs_db.absd_ship_search ;");
        while (rs.next())
        {
            String ids = rs.getString("LRNO");
            lrNumbers.add(ids);
        }
        return lrNumbers;
    }
}

