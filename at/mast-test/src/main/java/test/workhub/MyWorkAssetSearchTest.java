package test.workhub;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.asset.AssetQuery;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Arrays;

import static com.google.common.truth.Truth.assertThat;

public class MyWorkAssetSearchTest extends BaseTest
{

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {1}, {3}, {20}
        };
    }

    @Test(
            description = "Story 7.34: As a user in online mode, I want to carry out a search on dashboard so " +
                    "that I can view the relevant results",
            dataProvider = "asset"
    )
    @Issue("LRT-884")

    public void myWorkAssetSearchTest(int assetId)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        Asset searchableAsset = new Asset(assetId);

        //AC 1 & 5
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        //AC 7
        workHubPage.closeSearchFilterAssetsButton();
        workHubPage.clickSearchFilterAssetsButton();

        //AC 2 - Search By IMO
        Long imoNumber = searchableAsset.getDto().getIhsAsset().getId();
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setImoNumber(Arrays.asList(imoNumber));

        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        searchFilterSubPage.setAssetSearchInput(imoNumber.toString());
        searchFilterSubPage.clickAssetSearchApplyButton();
        assertThat(allAssetsSubPage.getAssetCardCount()).isEqualTo(assetQuery.getDto().getContent().size());
        assertThat(allAssetsSubPage.getAssetCardByIndex(0).getImoNumber()).isEqualTo(imoNumber.toString());

        //AC 2 - Search By Asset ID
        Long searchAssetId = searchableAsset.getDto().getId();
        assetQueryDto = new AssetQueryDto();
        assetQueryDto.setIdList(Arrays.asList(searchAssetId));

        assetQuery = new AssetQuery(assetQueryDto);
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(searchAssetId.toString());
        searchFilterSubPage.clickAssetSearchApplyButton();
        assertThat(allAssetsSubPage.getAssetCardCount()).isEqualTo(assetQuery.getDto().getContent().size());
        assertThat(allAssetsSubPage.getAssetCardByIndex(0).getAssetName()).isEqualTo(searchableAsset.getDto().getName());

        //AC 2 - Search By Asset Name
        String assetName = searchableAsset.getDto().getName();
        assetQueryDto = new AssetQueryDto();
        assetQueryDto.setSearch(assetName);

        assetQuery = new AssetQuery(assetQueryDto);
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(assetName);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assertThat(allAssetsSubPage.getAssetCardCount()).isEqualTo(assetQuery.getDto().getContent().size());
        assertThat(allAssetsSubPage.getAssetCardByIndex(0).getAssetName()).isEqualTo(searchableAsset.getDto().getName());

        //AC 6
        String invalidSearch = imoNumber + " " + assetId + " " + assetName;
        assetQueryDto = new AssetQueryDto();
        assetQueryDto.setSearch(invalidSearch);

        assetQuery = new AssetQuery(assetQueryDto);
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(invalidSearch);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assertThat(allAssetsSubPage.getAssetCardCount()).isEqualTo(assetQuery.getDto().getContent().size());

        //AC 4
        String wildCard = "*";
        assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 10L));
        assetQueryDto.setSearch(wildCard);

        assetQuery = new AssetQuery(assetQueryDto);
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(wildCard);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assertThat(workHubPage.getPaginationCountPage().getTotalAmount()).isEqualTo(assetQuery.getDto().getContent().size());
    }

}
