package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.sub.sub.IMORegisteredAssetPage;
import workhub.sub.sub.NonIMORegisteredAssetPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.*;
import static workhub.sub.sub.NonIMORegisteredAssetPage.Type.ERROR;

public class CreateNonImoRegisteredAssetTest extends BaseTest
{

    final private int assetId = 501;
    final private String specialChar = "@#$%&*()!_+";
    final private String assetNameMoreThanFiftyChar = "This is to test asset name cannot be more than 50 characters";
    final private String invalidBuildDate = "29/02/2017";
    final private String invalidBuildDateFormat = "02/29/2017";
    final private String invalidBuilderName = "8596543685122";
    final private String assetCategoryName = "Vessel";

    @Test(description = "US8.13. AC1-7 - Create non-IMO registered asset")
    @Issue("LRT-1588")
    public void createNonImoRegisteredAssetTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage
                = addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Non-IMO Registered asset button is expected to be selected")
                .that(addNewAssetPage.isNonIMORegisteredAssetButtonSelected())
                .isTrue();
        //AC: 2
        //Step: 3
        assert_().withFailureMessage("IMO number field is not Expected to be displayed")
                .that(nonIMORegisteredAssetPage.isImoNumberFieldDisplayed())
                .isFalse();

        //AC: 1
        //Step: 4
        assert_().withFailureMessage("Builder text field is expected to be blank")
                .that(nonIMORegisteredAssetPage.getBuilderText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Yard number text field is expected to be blank")
                .that(nonIMORegisteredAssetPage.getYardNumberText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Name text filed is expected to be blank")
                .that(nonIMORegisteredAssetPage.getAssetNameText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Date of Build filed is expected to be blank")
                .that(nonIMORegisteredAssetPage.getDateOfBuildText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Category Selected Option type is not expected to be displayed")
                .that(nonIMORegisteredAssetPage.isSelectedAssetCategoryDisplayed())
                .isFalse();

        assert_().withFailureMessage("Asset Selected Option type is not expected to be displayed")
                .that(nonIMORegisteredAssetPage.isSelectedAssetTypeDisplayed())
                .isFalse();

        //AC: 1
        //Step: 5
        nonIMORegisteredAssetPage.setBuilderName(builder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild);

        assert_().withFailureMessage("Create Asset Button is expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isTrue();

        //AC: 3
        //Step: 6
        nonIMORegisteredAssetPage.setAssetName(specialChar);
        assert_().withFailureMessage("Asset Name Field is not expected to have special characters")
                .that(nonIMORegisteredAssetPage
                        .isErrorIconDisplayedInAssetNameField(ERROR))
                .isTrue();
        assert_().withFailureMessage("Create Asset Button is not expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        nonIMORegisteredAssetPage.setAssetName(assetNameMoreThanFiftyChar);
        assert_().withFailureMessage("Asset Name Field is not expected to have more than 50 characters")
                .that(nonIMORegisteredAssetPage
                        .isErrorIconDisplayedInAssetNameField(ERROR))
                .isTrue();
        assert_().withFailureMessage("Create Asset Button is not expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 3
        //Step: 7
        nonIMORegisteredAssetPage
                .setAssetName(assetName)
                .then().setDateOfBuild(invalidBuildDate);

        assert_().withFailureMessage("Date of build Field is expected to be Valid")
                .that(nonIMORegisteredAssetPage
                        .isErrorIconDisplayedInDateOfBuildField(ERROR)
                        || nonIMORegisteredAssetPage.getValidationMessage()
                        .equals("Please select a valid date"))
                .isTrue();

        nonIMORegisteredAssetPage.setDateOfBuild(invalidBuildDateFormat);

        assert_().withFailureMessage("Date of build Field is expected to be in Valid format")
                .that(nonIMORegisteredAssetPage
                        .isErrorIconDisplayedInDateOfBuildField(ERROR)
                        || nonIMORegisteredAssetPage.getValidationMessage()
                        .equals("Please select a valid date"))
                .isTrue();

        assert_().withFailureMessage("Create Asset Button is not expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 4
        //Step: 8
        nonIMORegisteredAssetPage
                .setDateOfBuild(dateOfBuild)
                .then().setBuilderName(invalidBuilderName);
        assert_().withFailureMessage("Builder build Field is not expected to a number")
                .that(nonIMORegisteredAssetPage
                        .isErrorIconDisplayedInBuilderField(ERROR))
                .isTrue();
        assert_().withFailureMessage("Create Asset Button is not expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 5
        //Step: 9
        AttachmentsAndNotesPage attachmentsAndNotesPage
                = nonIMORegisteredAssetPage
                .setDateOfBuild(dateOfBuild)
                .then().clickAddAttachmentsButton();
        attachmentsAndNotesPage.clickCloseButton();
        attachmentsAndNotesPage.getAddNewAssetPage();

        //AC: 6
        //Step: 10
        IMORegisteredAssetPage iMORegisteredAssetPage
                = addNewAssetPage.clickImoRegisteredAssetRadioButton();

        assert_().withFailureMessage("IMO Number text field is expected to be blank")
                .that(iMORegisteredAssetPage.getImoNumberText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Name field is expected to be blank")
                .that(iMORegisteredAssetPage.getAssetNameText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Date of Build filed is expected to be blank")
                .that(iMORegisteredAssetPage.getDateOfBuildText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Category Selected Option type is not expected to be displayed")
                .that(iMORegisteredAssetPage.isSelectedAssetCategoryDisplayed())
                .isFalse();

        assert_().withFailureMessage("Asset Selected Option type is not expected to be displayed")
                .that(iMORegisteredAssetPage.isSelectedAssetTypeDisplayed())
                .isFalse();
        assert_().withFailureMessage("Create Asset Button is not expected to be clickable")
                .that(addNewAssetPage.isCreateAssetButtonClickable())
                .isFalse();

        //AC: 7
        //Step: 11
        addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Builder text field is expected to be blank")
                .that(nonIMORegisteredAssetPage.getBuilderText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Yard number text field is expected to be blank")
                .that(nonIMORegisteredAssetPage.getYardNumberText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Name text filed is expected to be blank")
                .that(nonIMORegisteredAssetPage.getAssetNameText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Date of Build filed is expected to be blank")
                .that(nonIMORegisteredAssetPage.getDateOfBuildText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Category Selected Option type is not expected to be displayed")
                .that(nonIMORegisteredAssetPage.isSelectedAssetCategoryDisplayed())
                .isFalse();

        assert_().withFailureMessage("Asset Selected Option type is not expected to be displayed")
                .that(nonIMORegisteredAssetPage.isSelectedAssetTypeDisplayed())
                .isFalse();
    }

    @Test(description = "US 9.34 - 9.5 AC:1-4 - Delete Note on Create Asset - Non IMO Registered Asset")
    @Issue("LRT-1964")
    public void deleteNoteOnNonImoRegisteredAssetTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage
                = addNewAssetPage.clickNonImoRegisteredAssetRadioButton();

        //Step: 3
        AttachmentsAndNotesPage attachmentsAndNotesPage = nonIMORegisteredAssetPage
                .setBuilderName(builder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setDateOfBuild(dateOfBuild).clickAddAttachmentsButton();

        //AC: 1
        //Step: 4
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() == 0)
        {

            String randomNote = RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().clickAllButton()
                    .then().clickAddNoteButton();

            assert_().withFailureMessage("Added note is expected to be present")
                    .that(attachmentsAndNotesPage
                            .getNoteAndAttachmentCards()
                            .stream()
                            .filter(e -> e.getNoteText().equals(randomNote))
                            .findFirst()
                            .isPresent())
                    .isTrue();
        }

        String noteText = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText();
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        assert_().withFailureMessage("Edit Note Icon is expected to be displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is expected to be displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 5
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        //AC: 4
        //Step: 6
        confirmationModalPage.clickCancelButton();

        assert_().withFailureMessage("Added note is expected to be present")
                .that(attachmentsAndNotesPage
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getNoteText().equals(noteText))
                        .findFirst()
                        .isPresent())
                .isTrue();

        //AC: 3
        //Step: 7
        if (!noteDetailsSubPage.isDeleteIconDisplayed())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        }
        noteDetailsSubPage.then().clickDeleteIcon()
                .then().clickConfirmationButton(AttachmentsAndNotesPage.class);
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() != 0)
        {
            noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
            assert_().withFailureMessage("The deleted note is expected to be present")
                    .that(noteDetailsSubPage.getNoteText())
                    .contains(noteText);
        }
        else
        {
            assert_().withFailureMessage("The deleted note is expected to be present")
                    .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().size())
                    .isEqualTo(0);
        }

        //AC: 3
        //Step: 8
        noteSubPage.refreshPage();
        addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        nonIMORegisteredAssetPage.clickAddAttachmentsButton();
        attachmentsAndNotesPage.clickAddNoteButton();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() != 0)
        {
            noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
            assert_().withFailureMessage("The deleted note is expected to be present")
                    .that(noteDetailsSubPage.getNoteText())
                    .contains(noteText);
        }
        else
        {
            assert_().withFailureMessage("The deleted note is expected to be present")
                    .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().size())
                    .isEqualTo(0);
        }
    }

}
