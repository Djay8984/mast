package test.workhub;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import model.asset.Asset;
import model.asset.AssetQuery;
import model.cases.Case;
import model.cases.CaseQuery;
import model.office.Office;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetCase;
import workhub.element.MyWorkAssetElement;
import workhub.sub.MyWorkSubPage;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.*;
import static workhub.sub.AssetSortOrderDropDownSubPage.AssetOrderBy;

public class ViewCasesAssignedToUserTest extends BaseTest
{

    private final String[] expectedCaseStatusList = {
            "Uncommitted",
            "Populating",
            "Job Phase",
            "Validate & Update",
            "On Hold"};

    private AssetQuery assetQuery;
    private CaseQuery employeeCases;
    private int defaultShownAssetsOnFE;

    @BeforeMethod(alwaysRun = true)
    public void setDefaultShownAssetsOnFE()
    {
        //Find all cases assigned to the DEFAULT_EMPLOYEE
        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Collections.singletonList(DEFAULT_EMPLOYEE));
        caseQueryDto.setCaseStatusId(Arrays.asList(1L, 2L, 3L));
        employeeCases = new CaseQuery(caseQueryDto);
        //Collect all cases that are assigned to DEFAULT EMPLOYEE by asset Id
        List<Long> assetIdList = employeeCases.getDto().getContent().stream()
                .map(caseDto -> caseDto.getAsset().getId())
                .collect(Collectors.toList());
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setIdList(assetIdList);
        //Find all assets with asset Id from DEFAULT EMPLOYEE that has cases
        assetQuery = new AssetQuery(assetQueryDto);
        defaultShownAssetsOnFE = assetQuery.getDto().getContent().size();

    }

    @Test(description = "Story 7.29 AC1-10 - As a user in online mode, I " +
            "want " +
            "to " +
            "be " +
            "able to see cases assigned to me on my Work Hub dashboard so " +
            "that I can identify the case I want to manage"
    )
    @Issue("LRT-871")
    public void displayAndOrdering()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        //AC1 - checks that user is able to see cases assigned to them

        //Sort the asset by alphabetical order descending "Asset name A-Z"
        List<AssetLightDto> sortedAssetsByAscendingNameOrderFromBE =
                assetQuery.getDto().getContent().stream()
                        .sorted((d1, d2) -> d1.getName().compareTo(d2.getName()))
                        .collect(Collectors.toList());

        List<MyWorkAssetCase> allCases = myWorkSubPage.getCaseItems();
        List<Long> caseIdsAssociatedToUser = employeeCases.getDto().getContent()
                .stream()
                .map(CaseDto::getId)
                .collect(Collectors.toList());
        List<Long> listOfCaseIdsFEShownToUser = allCases.stream()
                .map(MyWorkAssetCase::getID)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Assets with cases assigned to user are " +
                "shown on My Work hub dashboard")
                .that(caseIdsAssociatedToUser)
                .containsAllIn(listOfCaseIdsFEShownToUser);
        //Sort the cases by "created on date" from BE
        Comparator<CaseDto> byAssetName = Comparator.comparing(e->{
            Asset caseAsset1 = new Asset(e.getAsset().getId().intValue());
            return caseAsset1.getDto().getName();});
        Comparator<CaseDto> byNewestDate = byAssetName.thenComparing(Collections.reverseOrder(
                (Comparator.comparing(CaseDto::getCreatedOn))));

        List<CaseDto>
                listOfSortedCasesByCreatedDateAssignedToSingleAssetPerUser =
                employeeCases.getDto().getContent().stream()
                        .sorted(byNewestDate)
                        .collect(Collectors.toList());

        // AC2 AC4 -  check that info per case are correctly displayed for all
        // cases are sorted by date and timestamp
        for (int i = 0; i < allCases.size(); i++)
        {
            // AC2 - Verify that all cases displays information correctly
            // AC4 - Verify that cases are sorted per creation date

            assert_().withFailureMessage("Checking sort by " +
                    "matching attributes. For case#" + i +
                    " : case status matches")
                    .that(allCases.get(i)
                            .getCaseStatus())
                    .isEqualTo(getCaseStatusById(
                            (listOfSortedCasesByCreatedDateAssignedToSingleAssetPerUser.get(i).getCaseStatus().getId())));
            assert_().withFailureMessage("Checking sort by " +
                    "matching attributes. For case#" + i +
                    " : case type matches")
                    .that(allCases.get(i)
                            .getCaseType())
                    .isEqualTo(getCaseTypeById(
                            (listOfSortedCasesByCreatedDateAssignedToSingleAssetPerUser.get(i).getCaseType().getId())));
            assert_().withFailureMessage("Checking sort by " +
                    "matching attributes. For case#" + i +
                    " : CFO matches")
                    .that(allCases.get(i).getCfo())
                    .isEqualTo(getOfficeNameByCaseIdAndOfficeRole(
                            listOfSortedCasesByCreatedDateAssignedToSingleAssetPerUser.get(i).getId(), "CFO"));
            assert_().withFailureMessage("Checking sort by " +
                    "matching attributes. For case#" + i +
                    " : SDO matches")
                    .that(allCases.get(i).getSdo())
                    .isEqualTo(getOfficeNameByCaseIdAndOfficeRole(
                            listOfSortedCasesByCreatedDateAssignedToSingleAssetPerUser.get(i).getId(), "SDO"));
        }

        //AC3 - check that BE has expectedCaseStatusList
        ArrayList<ReferenceDataDto> caseStatuses =
                new ReferenceDataMap(CASE, CASE_STATUSES)
                        .getReferenceDataArrayList();
        List<String> listOfCaseStatusNamesInDB = caseStatuses.stream()
                .map(ReferenceDataDto::getName)
                .collect(Collectors.toList());
        assert_().withFailureMessage(("Case status should have these values: " +
                Arrays.toString(expectedCaseStatusList)))
                .that(listOfCaseStatusNamesInDB)
                .containsAllIn(Arrays.asList(expectedCaseStatusList));
        //AC3 - check that FE has expectedClassStatusList

        allCases.stream()
                .forEach(cs ->
                        assert_().withFailureMessage("Case status should" +
                                " be one of these" + Arrays.toString
                                (expectedCaseStatusList))
                                .that(Arrays.asList(expectedCaseStatusList))
                                .contains(cs.getCaseStatus())
                );

        //AC5 - Check "asset name A-Z is selected by default
        assert_().withFailureMessage("Default sort order is \"Asset name A-Z\"")
                .that(myWorkSubPage.getAssetSortOrderDropDownSubPage().getSelectedOptionsFromAssetSortOrderDropdown())
                .isEqualTo("Asset Name A>Z");
        List<String> listOfAssetNamesFromFEShownToUser = allCases.stream()
                .map(e-> e.hoverOverInformationIcon().getAssetName())
                .collect(Collectors.toList());
        //AC5 - Check that the order of the asset is sorted by A-Z
        for (int i = 0; i < listOfAssetNamesFromFEShownToUser.size(); i++)
        {
            assert_().withFailureMessage(("Front end sorts  " +
                    "By A-Z, matches BE sorts by A-Z"))
                    .that(listOfAssetNamesFromFEShownToUser.get(i))
                    .isEqualTo(sortedAssetsByAscendingNameOrderFromBE
                            .get(i).getName());
        }
        //AC5 - Check that the order of the asset is sorted by Z-A
        myWorkSubPage = myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.NAME_DESC.getString());

        List<MyWorkAssetElement> sortedAssetsByDescendingNameOrderFromFE =
                myWorkSubPage.getAssetCards();
        Comparator<AssetLightDto> byNameDescOrder = Collections.reverseOrder
                (Comparator.comparing(AssetLightDto::getName));
        List<AssetLightDto> sortedAssetsByDescendingNameOrderFromBE =
                assetQuery.getDto().getContent()
                        .stream()
                        .sorted(byNameDescOrder)
                        .collect(Collectors.toList());
        for (int i = 0; i < listOfAssetNamesFromFEShownToUser.size(); i++)
        {
            assert_().withFailureMessage(("Front end sorts  " +
                    "By Z-A, matches BE sorts by Z-A"))
                    .that(sortedAssetsByDescendingNameOrderFromFE.get(i).getAssetName())
                    .isEqualTo(sortedAssetsByDescendingNameOrderFromBE
                            .get(i).getName());
            // returns null
        }
        //AC5 - Check that the order of asset is sorted by date build ascending
        myWorkSubPage = myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.BUILD_DATE_ASC.getString());

        List<MyWorkAssetElement> sortedAssetsByAscendingDateBuildOrderFromFE =
                myWorkSubPage.getAssetCards();

        List<AssetLightDto> sortedAssetByAscendingDateBuildOrderFromBE =
                assetQuery.getDto().getContent()
                        .stream()
                        .filter(dto->dto.getBuildDate()!=null)
                        .sorted((a1, a2) -> a1.getBuildDate().compareTo(a2.getBuildDate()))
                        .collect(Collectors.toList());
        for (int i = 0; i < listOfAssetNamesFromFEShownToUser.size(); i++)
        {
            assert_().withFailureMessage(("Front end sorts  " +
                    "build date ascending, matches BE sorts by build date " +
                    "ascending"))
                    .that(sortedAssetsByAscendingDateBuildOrderFromFE.get(i)
                            .getAssetName())
                    .isEqualTo(sortedAssetByAscendingDateBuildOrderFromBE
                            .get(i).getName());
        }
        //AC5 - Check that the order of asset is sorted by date build descending
        myWorkSubPage = myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.BUILD_DATE_DESC.getString());

        List<MyWorkAssetElement> sortedAssetsByDescendingDateBuildOrderFromFE =
                myWorkSubPage.getAssetCards();
        Comparator<AssetLightDto> byBuildDateDescOrder = Collections
                .reverseOrder
                        (Comparator.comparing(AssetLightDto::getBuildDate));
        List<AssetLightDto> sortedAssetByDescendingDateBuildOrderFromBE =
                assetQuery.getDto().getContent()
                        .stream()
                        .filter(dto->dto.getBuildDate()!=null)
                        .sorted(byBuildDateDescOrder)
                        .collect(Collectors.toList());
        for (int i = 0; i < listOfAssetNamesFromFEShownToUser.size(); i++)
        {
            assert_().withFailureMessage(("Front end sorts  " +
                    "by build date descending, matches BE sorts by build date" +
                    " descending"))
                    .that(sortedAssetsByDescendingDateBuildOrderFromFE.get(i).getAssetName())
                    .isEqualTo(sortedAssetByDescendingDateBuildOrderFromBE
                            .get(i).getName());
        }
        //AC5 - Check that the order of asset is sorted by name ascending
        myWorkSubPage = myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.NAME_ASC.getString());

        List<MyWorkAssetElement> sortedAssetsByAscendingNameOrderFromFE =
                myWorkSubPage.getAssetCards();
        for (int i = 0; i < listOfAssetNamesFromFEShownToUser.size(); i++)
        {
            assert_().withFailureMessage(("Front end sorts  " +
                    "By A-Z, matches BE sorts by A-Z"))
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getAssetName())
                    .isEqualTo(sortedAssetsByAscendingNameOrderFromBE
                            .get(i).getName());
        }

        //AC6 - Check pagination count matches
        assert_().withFailureMessage("Total amount of assets is" + sortedAssetsByAscendingNameOrderFromBE.size())
                .that(myWorkSubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(sortedAssetsByAscendingNameOrderFromBE.size());
        assert_().withFailureMessage("Total amount show is" + defaultShownAssetsOnFE)
                .that(myWorkSubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(defaultShownAssetsOnFE);

        //AC7 - Visual confirmation of emblem - not automatable

        //AC7 - Check that each pane displays correct details for all cards
        for (int i = 0; i < sortedAssetsByAscendingNameOrderFromFE.size(); i++)
        {
            assert_().withFailureMessage("Asset name matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getAssetName())
                    .isEqualTo(sortedAssetsByAscendingNameOrderFromBE.get(i)
                            .getName());
            assert_().withFailureMessage("Imo number matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getImoNumber())
                    .isEqualTo(sortedAssetsByAscendingNameOrderFromBE.get(i)
                            .getIhsAsset().getId().toString());
            assert_().withFailureMessage("Asset type matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getAssetType())
                    .isEqualTo(getAssetTypeById(
                            sortedAssetsByAscendingNameOrderFromBE.get(i)
                                    .getAssetType().getId()));
            assert_().withFailureMessage("Build date matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getBuildDate())
                    .isEqualTo(LloydsRegister.FRONTEND_TIME_FORMAT.format
                            (sortedAssetsByAscendingNameOrderFromBE
                                    .get(i).getBuildDate()));
            assert_().withFailureMessage("Gross tonnage matches")
                    .that(Integer.parseInt(sortedAssetsByAscendingNameOrderFromFE.get(i).getGrossTonnage()))
                    .isEqualTo(sortedAssetsByAscendingNameOrderFromBE
                            .get(i).getGrossTonnage().intValue());
            assert_().withFailureMessage("Flag status matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getFlag())
                    .isEqualTo(getFlagById(sortedAssetsByAscendingNameOrderFromBE
                            .get(i).getFlagState().getId()));
            assert_().withFailureMessage("Class status matches")
                    .that(sortedAssetsByAscendingNameOrderFromFE.get(i).getClassStatus())
                    .isEqualTo(getClassStatusById(sortedAssetsByAscendingNameOrderFromBE
                            .get(i).getClassStatus().getId()));
        }


        //AC8 - Check that the "view all cases" button is displayed per asset
        for (MyWorkAssetCase assetCase : allCases)
        {
            assert_().withFailureMessage("All asset button is displayed")
                    .that(assetCase.hoverOverInformationIcon().isViewAllCasesOrJobsButtonDisplayed())
                    .isTrue();
        }

        //AC9 - Check that number of active cases matches BE and FE
        for (int i = 0; i < sortedAssetsByAscendingNameOrderFromFE.size(); i++)
        {
            List<MyWorkAssetCase> casesFromFECard = sortedAssetsByAscendingNameOrderFromFE
                    .get(i).getMyWorkAssetCases();
            //Get cases associated with this asset from the sorted BE
            Long id = sortedAssetsByAscendingNameOrderFromBE.get(i)
                    .getId();
            CaseQueryDto caseQueryDto = new CaseQueryDto();
            caseQueryDto.setEmployeeId(Collections.singletonList(DEFAULT_EMPLOYEE));
            caseQueryDto.setAssetId(Collections.singletonList(id));
            CaseQuery caseQueryByAsset = new CaseQuery(caseQueryDto);
            assert_().withFailureMessage("Number of active cases per")
                    .that(casesFromFECard.size())
                    .isEqualTo(caseQueryByAsset.getDto().getContent().size());
        }

        //AC10
        if (sortedAssetsByAscendingNameOrderFromBE.size() > defaultShownAssetsOnFE)
        {
            assert_().withFailureMessage("Load more button is displayed")
                    .that(myWorkSubPage.isLoadMoreButtonDisplayed())
                    .isTrue();
        }

        //AC10 - Click on load more assets option and verify that workhub
        // dash board is displayed
        // No assertion needed, checks navigation to viewAssetPage @Visible
        ViewAssetPage viewAssetPage = sortedAssetsByAscendingNameOrderFromFE.get(1).clickViewAssetButton();
    }

    @Test(description = "7.29 AC 11-16 - Navigation options & default asset " +
            "and case panes display")
    @Issue("LRT-872")
    public void navigationOptionsAndDefaultAssetAndCasePanesDisplay()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();
        List<MyWorkAssetElement> allCards = myWorkSubPage.getAssetCards();

        //AC16 - User see cases assigned to them
        //This is a repeat of LRT-871 AC1 AC2 AC4
        assert_().withFailureMessage("Total amount show is" + defaultShownAssetsOnFE)
                .that(myWorkSubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(defaultShownAssetsOnFE);

        //AC11 - Check that View Asset button is displayed
        for (MyWorkAssetElement card : allCards)
        {
            assert_().withFailureMessage("View asset button is displayed for " +
                    "each asset")
                    .that(card.isViewAssetButtonDisplayed())
                    .isTrue();
        }

        //AC12 - Check that case arrow button is displayed
        for (MyWorkAssetElement card : allCards)
        {
            List<MyWorkAssetCase> cases = card
                    .getMyWorkAssetCases();
            cases.stream()
                    .forEach(cs ->
                            assert_().withFailureMessage("Case navigation " +
                                    "button is displayed for all cases")
                                    .that(cs.isNavigateToCaseArrowButtonDisplayed())
                                    .isTrue()
                    );
        }

        //AC13 - Check that Batch actions link is displayed
        assert_().withFailureMessage("Batch Actions Button is displayed")
                .that(workHubPage.isBatchActionsButtonDisplayed())
                .isTrue();

        //AC14 - Check that add new asset button is displayed
        assert_().withFailureMessage("Add new asset button is displayed")
                .that(workHubPage.isAddNewAssetButtonDisplayed())
                .isTrue();

        //AC15 - Check that the system displays number of case count against
        // the specific asset
        //This is a repeat of LRT-871 AC9
    }

    private String getOfficeRoleNameById(Long id)
    {
        ArrayList<ReferenceDataDto> officeRoles =
                new ReferenceDataMap(CASE, OFFICE_ROLES)
                        .getReferenceDataArrayList();
        return officeRoles.stream()
                .filter(ofr -> ofr.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getOfficeNameByCaseIdAndOfficeRole(Long caseId, String
            officeRoleName)
    {
        Case singleCase = new Case(caseId.intValue());
        return
                singleCase.getDto().getOffices().stream()
                        .filter(officeLinkDto ->
                                getOfficeRoleNameById(officeLinkDto.getOfficeRole()
                                        .getId()).equals(officeRoleName))
                        .map(officeLinkDto -> officeLinkDto.getOffice().getId())
                        .map(id -> new Office(id.intValue()))
                        .map(office -> office.getDto().getName())
                        .findFirst()
                        .orElse("-");
    }

    private String getAssetTypeById(Long id)
    {
        ArrayList<ReferenceDataDto> assetTypes =
                new ReferenceDataMap(ASSET, ASSET_TYPES)
                        .getReferenceDataArrayList();
        return assetTypes.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getFlagById(Long id)
    {
        ArrayList<ReferenceDataDto> flags =
                new ReferenceDataMap(FLAG, FLAGS)
                        .getReferenceDataArrayList();
        return flags.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getClassStatusById(Long id)
    {
        ArrayList<ReferenceDataDto> classStatuses =
                new ReferenceDataMap(ASSET, CLASS_STATUSES)
                        .getReferenceDataArrayList();
        return classStatuses.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);

    }

    private String getCaseStatusById(Long id)
    {
        ArrayList<ReferenceDataDto> caseStatuses =
                new ReferenceDataMap(CASE, CASE_STATUSES)
                        .getReferenceDataArrayList();
        return caseStatuses.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getCaseTypeById(Long id)
    {
        ArrayList<ReferenceDataDto> caseTypes =
                new ReferenceDataMap(CASE, CASE_TYPES)
                        .getReferenceDataArrayList();
        return caseTypes.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }
}
