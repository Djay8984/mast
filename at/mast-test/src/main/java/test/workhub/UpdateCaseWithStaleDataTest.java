package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.cases.viewcase.ViewCasePage;
import workhub.WorkHubPage;

import java.io.IOException;

public class UpdateCaseWithStaleDataTest extends BaseTest
{
    @Test(description = "Attempt To Update A Case With Stale Data")
    public void updateCaseWithStaleDataTest() throws ClientProtocolException, IOException
    {
        // Create a brand new asset and case
        final WorkHubPage workHubPage = WorkHubPage.open();

        final ViewCasePage viewCasePage = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getCaseByIndex(0)
                .clickCaseDetails();

        // TODO Blocked by LRD-4616 as a regression issue prevents any cases from being updated.

        // update the case

        // make a restful call to update the case differently

        // press the save button

        // assert the staleness dialog

        // assert the result of the reload.
    }

}
