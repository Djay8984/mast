package test.workhub;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetQuery;
import model.cases.CaseQuery;
import model.flag.FlagState;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetCase;
import workhub.element.MyWorkAssetElement;
import workhub.element.MyWorkAssetJob;
import workhub.sub.MyWorkSubPage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;
import static model.referencedata.ReferenceDataMap.RefData.ASSET_TYPES;
import static model.referencedata.ReferenceDataMap.RefData.CLASS_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class AssetHeaderTileInformationTest extends BaseTest
{

    final int maxChar = 20;

    @Test(description = "Story 7.31 - As user in online mode, I want to be " +
            "able to view appropriate asset header tile information so that I" +
            " can identify the asset I am looking for by looking important " +
            "minimum information")
    @Issue("LRT-885")
    public void assetHeaderTileInformationTest()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Arrays.asList(DEFAULT_EMPLOYEE));
        caseQueryDto.setCaseStatusId(Arrays.asList(1L, 2L, 3L));
        CaseQuery employeeCases = new CaseQuery(caseQueryDto);

        List<CaseDto> caseDtos = employeeCases.getDto().getContent();
        //Collect all cases that are assigned to DEFAULT EMPLOYEE by asset Id
        List<Long> idList = caseDtos.stream()
                .map(caseDto ->
                        caseDto.getAsset().getId()
                )
                .collect(Collectors.toList());

        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setIdList(idList);

        //Find all assets with asset Id from DEFAULT EMPLOYEE that has cases
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        //Sort and collect all matching asset queries
        List<AssetLightDto> sortedAssetListWithCase = assetQuery.getDto()
                .getContent()
                .stream()
                .sorted((d1, d2) ->
                        d1.getName().compareTo(d2.getName())
                )
                .collect(Collectors.toList());

        List<MyWorkAssetJob> jobItems = myWorkSubPage.getJobItems();
        for(MyWorkAssetJob jobItem : jobItems)
        {
            //AC1 - All set to visible - no need for assertions
            MyWorkAssetElement card = jobItem.mouseoverAssetInformation();
            List<MyWorkAssetElement> cards = myWorkSubPage.getAssetCards();
            // but assert once on a sample just for due diligence
            // Compare asset name
            assert_().withFailureMessage("Asset name is not displayed correctly")
                    .that(card.getAssetName().trim())
                    .isEqualTo(sortedAssetListWithCase.get(0).getName().trim());

            //Compare asset type
            //This is disgusting
            AssetTypeDto assetTypeDto =
                    (AssetTypeDto) new ReferenceDataMap(ASSET, ASSET_TYPES).getReferenceDataAsClass(AssetTypeDto.class)
                            .stream()
                            .filter(
                                    assetType -> ((AssetTypeDto) assetType).getId()
                                            .equals(sortedAssetListWithCase.get(0).getAssetType().getId())
                            ).findFirst().orElse(null);
            assert_().withFailureMessage("Asset type is not displayed correctly")
                    .that(card.getAssetType())
                    .isEqualTo(assetTypeDto.getName());
            //Compare build date
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY");
            assert_().withFailureMessage("Build Date is not displayed correctly")
                    .that(card.getBuildDate())
                    .isEqualTo(sdf.format(sortedAssetListWithCase.get(0)
                            .getBuildDate()));
            //Compare imo number
            assert_().withFailureMessage("IMO Number is not displayed correctly")
                    .that(card.getImoNumber())
                    .isEqualTo(sortedAssetListWithCase.get(0).getIhsAsset().getId
                            ().toString());
            //Compare flag text
            FlagState flag = new FlagState(sortedAssetListWithCase.get(0)
                    .getFlagState().getId().intValue());
            assert_().withFailureMessage("Flag is not displayed correctly")
                    .that(card.getFlag().trim())
                    .isEqualTo(flag.getDto().getName().trim());
            //Compare Gross tonnage
            assert_().withFailureMessage("Gross tonnage is not displayed correctly")
                    .that(Integer.parseInt(card.getGrossTonnage()))
                    .isEqualTo(sortedAssetListWithCase.get(0).getGrossTonnage().intValue());

            //Compare class status
            Long classId = sortedAssetListWithCase.get(0).getClassStatus().getId();
            ArrayList<ReferenceDataDto> classStatuses =
                    new ReferenceDataMap(ASSET, CLASS_STATUSES)
                            .getReferenceDataArrayList();
            String classStatus = classStatuses.stream()
                    .filter(dto -> dto.getId().equals(classId))
                    .findFirst()
                    .map(dto -> dto.getName())
                    .orElse(null);
            assert_().withFailureMessage("Class status is not displayed correctly")
                    .that(card.getClassStatus())
                    .isEqualTo(classStatus);
            jobItem.closeMouseoverPane();
        }
    }
}

