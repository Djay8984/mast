package test.workhub;

import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.job.Job;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import viewasset.ViewAssetPage;
import viewasset.assetheaderadditionalinformation.edit.assetdetails.EditAssetDetailsPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.EditJobPage;
import workhub.WorkHubPage;

import java.io.IOException;
import java.util.List;

public class UpdateJobWithStaleDataTest extends BaseTest
{

    private static final String JOB_STALENESS_DIALOG_TEXT = "There is a conflict with this job and your changes have not been\n" +
            "saved. You will be sent back to the view mode of this job so you can read\n" +
            "the last saved version";

    @Test(description = "Attempt To Update A Job With Stale Data")
    public void updateJobWithStaleDataTest() throws ClientProtocolException, IOException
    {
        // Create a brand new asset, case, and job
        final WorkHubPage workHubPage = WorkHubPage.open();

        final ViewAssetPage viewAssetPage = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset();

        EditAssetDetailsPage editAssetDetailsPage = viewAssetPage
                .clickViewMoreButton()
                .getAssetDetailsPage()
                .clickEditButton()
                .getEditAssetDetailsPage();

        final String newAssetName = editAssetDetailsPage.getNameText();

        editAssetDetailsPage
                .clickAssetHeaderTab()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase();

        final EditJobPage editJobPage = viewAssetPage
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickEditJobButton();

        // ...then start editing it in the UI

        editJobPage.enterDescription("New Description");

        // Update the job via the REST api with different data
        final int assetId = TestDataHelper.getAssetIdByName(newAssetName);

        List<JobDto> jobsByAssetId = TestDataHelper.getJobsByAssetId(Long.valueOf(assetId));
        final JobDto jobDto = jobsByAssetId.get(0);

        final String originalDescription = jobDto.getDescription();

        jobDto.setDescription(originalDescription + "X");
        new Job(jobDto, jobDto.getId().intValue());

        // TODO Also blocked by LRD-4616 where the date picker fails to allow an entity to be saved.

        // // Now try to save the stale data in the ui.
        // editJobPage.clickSaveButtonExpectingStaleness();
        //
        // final StalenessPage stalenessDialog = PageFactory.newInstance(StalenessPage.class);
        // assertThat(stalenessDialog.getHeader()).isEqualTo("Unable to save changes due to conflict");
        // assertThat(stalenessDialog.getBody()).isEqualTo(JOB_STALENESS_DIALOG_TEXT);
        //
        // // Ensure the reload updates the page with the data from the rest call.
        // final EditJobPage reloadedPage = stalenessDialog.clickReloadButton(EditJobPage.class);
        //
        // assertThat(reloadedPage.getDescription()).isEqualTo(originalDescription + "X");
    }

}
