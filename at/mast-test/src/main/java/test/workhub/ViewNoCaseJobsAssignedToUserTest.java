package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import model.employee.Employee;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.MyWorkSubPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;
import static helper.TestDataHelper.getEmployeeWithJobsOrCases;

public class ViewNoCaseJobsAssignedToUserTest extends BaseTest
{

    private WorkHubPage workHubPage;

    @Test(description = "Story 7.30 - As a user in online mode, I want to" +
            " be able to see if no cases or jobs are assigned to me so " +
            "that I know I do not have any", enabled = false)
    @Issue("LRT-896")
    public void viewCaseJobsAssignedToUserTest()
    {

        //Get user with no jobs or case assigned
        String employeeName = getEmployeeWithJobsOrCases(false);

        workHubPage = WorkHubPage.open();
        ConfigurationPage configPage = workHubPage.getHeader()
                .clickConfigPage();
        configPage.setMastUserFullName(employeeName).then()
                .setMastGroupsInput("EIC");
        configPage.clickSaveButton();

        //AC1
        workHubPage = workHubPage.getHeader().clickMastLogo();
        assert_().withFailureMessage("All assets tab is active")
                .that(workHubPage.isAllAssetTabSelected())
                .isTrue();

        //AC2 AC3
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();
        assert_().withFailureMessage("0 cards should be displayed")
                .that(myWorkSubPage.getAssetCardCount())
                .isEqualTo(0);
        assert_().withFailureMessage("Notification message with 'You are " +
                "not currently assigned to any cases.' is displayed")
                .that(workHubPage.getNotificationMessageText())
                .isEqualTo("You are not currently assigned to any cases.");

        //AC4
        assert_().withFailureMessage("All assets tab is displayed")
                .that(workHubPage.isAllAssetTabDisplayed())
                .isTrue();

        //AC5
        assert_().withFailureMessage("Search  filter assets button is " +
                "displayed")
                .that(workHubPage.isSearchFilterAssetsButtonDisplayed())
                .isTrue();

        //AC6
        assert_().withFailureMessage("Batch action button is displayed")
                .that(workHubPage.isBatchActionsButtonDisplayed())
                .isTrue();

        //AC7
        assert_().withFailureMessage("Add new asset button is displayed")
                .that(workHubPage.isAddNewAssetButtonDisplayed())
                .isTrue();

        //Log into system as valid user that has no jobs assigned
        configPage = workHubPage.getHeader().clickConfigPage();
        configPage.setMastGroupsInput("ADMIN").then()
                .clickSaveButton();

        //AC6 AC7---Repeat step2 to step7 but with different condition----
        //AC1
        workHubPage = workHubPage.getHeader().clickMastLogo();
        assert_().withFailureMessage("All assets tab is active")
                .that(workHubPage.isAllAssetTabSelected())
                .isTrue();

        //AC2 AC3
        myWorkSubPage = workHubPage.clickMyWorkTab();
        assert_().withFailureMessage("0 cards should be displayed")
                .that(myWorkSubPage.getAssetCardCount())
                .isEqualTo(0);
        assert_().withFailureMessage("Notification message with 'You are " +
                "not currently assigned to any cases.' is displayed")
                .that(workHubPage.getNotificationMessageText())
                .isEqualTo("You are not currently assigned to any cases.");

        //AC4
        assert_().withFailureMessage("All assets tab is displayed")
                .that(workHubPage.isAllAssetTabDisplayed())
                .isTrue();

        //AC5
        assert_().withFailureMessage("Search  filter assets button is " +
                "displayed")
                .that(workHubPage.isSearchFilterAssetsButtonDisplayed())
                .isTrue();

        //AC6
        assert_().withFailureMessage("Batch action button is NOT displayed")
                .that(workHubPage.isBatchActionsButtonDisplayed())
                .isFalse();

        //AC7
        assert_().withFailureMessage("Add new asset button is NOT " +
                "displayed")
                .that(workHubPage.isAddNewAssetButtonDisplayed())
                .isFalse();
    }

    @AfterTest(alwaysRun = true)
    public void returnUserToDefault()
    {
        ConfigurationPage configurationPage = workHubPage.getHeader()
                .clickConfigPage();
        configurationPage.setMastUserFullName(new Employee((int) DEFAULT_EMPLOYEE).getDto().getName()).then()
                .setMastGroupsInput("Admin,EIC");
        configurationPage.clickSaveButton();
    }

}
