package test.workhub;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import viewasset.assetheaderadditionalinformation.edit.assetdetails.EditAssetDetailsPage;
import viewasset.sub.modal.StalenessPage;
import workhub.WorkHubPage;

import java.io.IOException;

import static com.google.common.truth.Truth.assertThat;

public class UpdateAssetWithStaleDataTest extends BaseTest
{
    private static final String ASSET_STALENESS_DIALOG_TEXT = "There is a conflict with this asset and your changes have not been\n" +
            "saved. You will be sent back to the view mode of this asset so you can read\n" +
            "the last saved version";

    @Test(description = "Attempt To Update An Asset With Stale Data")
    public void updateAssetWithStaleDataTest() throws ClientProtocolException, IOException
    {
        // Create a brand new asset, open it for editing, then change one of the fields.
        final WorkHubPage workHubPage = WorkHubPage.open();

        final EditAssetDetailsPage editAssetDetailsPage = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickViewMoreButton()
                .getAssetDetailsPage()
                .clickEditButton()
                .getEditAssetDetailsPage();

        final String originalAssetName = editAssetDetailsPage.getNameText();

        final EditAssetDetailsPage detailsPageReadyForSave = editAssetDetailsPage.setNameText("New Name");

        // Using a separate rest call, update one of the fields for this asset before clicking save
        final int assetId = TestDataHelper.getAssetIdByName(originalAssetName);
        final AssetLightDto assetFromRestService = new Asset(assetId).getDto();
        assetFromRestService.setBuilder(assetFromRestService.getBuilder() + "X");
        new Asset(assetFromRestService, assetId);

        // Click the save button and confirm it fails to update with an expected Conflict dialog
        final StalenessPage stalenessDialog = detailsPageReadyForSave.clickSaveButton(StalenessPage.class);
        assertThat(stalenessDialog.getHeader()).isEqualTo("Unable to save changes due to conflict");
        assertThat(stalenessDialog.getBody()).isEqualTo(ASSET_STALENESS_DIALOG_TEXT);

        // Ensure the reload updates the page with the data from the rest call.
        final EditAssetDetailsPage reloadedPage = stalenessDialog.clickReloadButton(EditAssetDetailsPage.class);

        assertThat(reloadedPage.getNameText()).isEqualTo(originalAssetName);
        assertThat(reloadedPage.getBuilderText()).isEqualTo(assetFromRestService.getBuilder());

    }

}
