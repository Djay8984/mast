package test.workhub;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.beust.jcommander.internal.Lists;
import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import constant.LloydsRegister;
import model.asset.AssetQuery;
import model.employee.Employee;
import model.job.Job;
import model.job.JobQueryPage;
import model.office.Office;
import model.referencedata.ReferenceDataMap;
import org.joda.time.DateTime;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetElement;
import workhub.element.MyWorkAssetJob;
import workhub.sub.MyWorkSubPage;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.*;
import static workhub.sub.AssetSortOrderDropDownSubPage.AssetOrderBy;

public class ViewJobAssignedToUserTest extends BaseTest
{

    private Employee employee;
    private String employeeName;

    @Test(enabled = false, description = "Story 7.33 - As a user in online mode, " +
            "I want to view the Jobs assigned to me " +
            "so that I can take appropriate action")
    @Issue("LRT-899")
    public void assignedJobsDisplayAndOrdering()
    {
        employee = new Employee((int) DEFAULT_EMPLOYEE);
        employeeName = employee.getDto().getFirstName() + " " + employee.getDto().getLastName();

        WorkHubPage workHubPage = WorkHubPage.open();

        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        //AC4 Check if Order by drop down is set to asset name A > Z by default
        assert_().withFailureMessage("Asset is not order by Name in ascending.")
                .that(myWorkSubPage.getAssetSortOrderDropDownSubPage().getSelectedOptionsFromAssetSortOrderDropdown())
                .contains("name-asc");

        //Get all active jobs for DEFAULT_EMPLOYEE
        //AC2
        JobQueryPage jobQueryPage = new JobQueryPage(Collections.singletonList(DEFAULT_EMPLOYEE), Collections.EMPTY_LIST);

        List<JobDto> allJobs = jobQueryPage.getDto().getContent();
        Set<Long> allAssetIDSet = allJobs.stream()
                .map(job ->
                        job.getAsset().getId()
                )
                .collect(Collectors.toSet());

        //Get all assets by assetID from all active jobs assigned to user
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setIdList(Lists.newArrayList(allAssetIDSet));
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);

        //AC4 Order by asset name A > Z ASC
        List<AssetLightDto> allAssetLightDtoOrderByNameList = assetQuery.getDto().getContent()
                .stream()
                .sorted((dto1, dto2) -> dto1.getName().compareTo(dto2.getName()))
                .collect(Collectors.toList());

        List<String> allDtoAssetNameOrderByNameList =
                allAssetLightDtoOrderByNameList.stream()
                        .map(c -> c.getName().toLowerCase())
                        .collect(Collectors.toList());

        List<Long> allDtoAssetIDOrderByNameList =
                allAssetLightDtoOrderByNameList.stream()
                        .map(c -> c.getId())
                        .collect(Collectors.toList());

        List<MyWorkAssetElement> allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();
        List<String> allDisplayMyWorkAssetNameList =
                allDisplayMyWorkAssetElement.stream()
                        .map(c -> c.getAssetName().toLowerCase())
                        .collect(Collectors.toList());

        assert_().withFailureMessage("Assets that assigned to user with active jobs are not displayed in Asset Name ascending order ")
                .that(allDisplayMyWorkAssetNameList)
                .containsExactlyElementsIn(allDtoAssetNameOrderByNameList.subList(0, allDisplayMyWorkAssetNameList.size()));

        Long selectedAssetId = allAssetLightDtoOrderByNameList.get(0).getId();
        String selectedAssetName = allAssetLightDtoOrderByNameList.get(0).getName();

        //AC4 Order by asset name Z > A DESC
        myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.NAME_DESC.getString());

        allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();

        allDisplayMyWorkAssetNameList = allDisplayMyWorkAssetElement.stream()
                .map(c -> c.getAssetName().toLowerCase())
                .collect(Collectors.toList());
        //Reverser order for allDtoAssetNameOrderByNameList
        Collections.reverse(allDtoAssetNameOrderByNameList);
        assert_().withFailureMessage("Asset that assigned to user with active job are not displayed in Asset Name descending order")
                .that(allDisplayMyWorkAssetNameList)
                .containsExactlyElementsIn(allDtoAssetNameOrderByNameList.subList(0, allDisplayMyWorkAssetNameList.size()));

        //AC4 Order by BuildDate ASC
        List<AssetLightDto> allAssetLightDtoOrderByBuildDateList = assetQuery.getDto().getContent()
                .stream()
                .sorted((dto1, dto2) -> dto1.getBuildDate().compareTo(dto2.getBuildDate()))
                .collect(Collectors.toList());

        List<String> allDtoAssetNameOrderByBuildDateList =
                allAssetLightDtoOrderByBuildDateList.stream()
                        .map(c -> c.getName().toLowerCase())
                        .collect(Collectors.toList());

        myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.BUILD_DATE_ASC.getString());

        allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();
        allDisplayMyWorkAssetNameList = allDisplayMyWorkAssetElement.stream()
                .map(c -> c.getAssetName().toLowerCase())
                .collect(Collectors.toList());

        assert_().withFailureMessage("Assets that assigned to user with jobs are not displayed in BuildDate ascending order")
                .that(allDisplayMyWorkAssetNameList)
                .containsExactlyElementsIn(allDtoAssetNameOrderByBuildDateList.subList(0, allDisplayMyWorkAssetNameList.size()));

        //AC4 Order by BuildDate DESC
        myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.BUILD_DATE_DESC.getString());
        allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();
        allDisplayMyWorkAssetNameList = allDisplayMyWorkAssetElement.stream()
                .map(c -> c.getAssetName().toLowerCase())
                .collect(Collectors.toList());
        //Reverse order for allDtoAssetNameOrderByBuildDateList
        Collections.reverse(allDtoAssetNameOrderByBuildDateList);
        //todo sort by two key build date desc and asset name asc
        assert_().withFailureMessage("Assets that assigned to user with jobs are not displayed in BuildDate descending order")
                .that(allDisplayMyWorkAssetNameList)
                .containsExactlyElementsIn(allDtoAssetNameOrderByBuildDateList.subList(0, allDisplayMyWorkAssetNameList.size()));

        //Order by name ASC
        myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.NAME_ASC.getString());

        allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();

        //AC5
        //Iterate every Asset Card
        for (int i = 0; i < allDisplayMyWorkAssetElement.size() - 1; i++)
        {

            //LRT-900 AC8 Verify asset detail
            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] is different from database ")
                    .that(allDisplayMyWorkAssetElement.get(i).getAssetName().toLowerCase())
                    .isEqualTo(allAssetLightDtoOrderByNameList.get(i).getName().toLowerCase());

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect IMO number ")
                    .that(allDisplayMyWorkAssetElement.get(i).getImoNumber())
                    .isEqualTo(allAssetLightDtoOrderByNameList.get(i).getIhsAsset().getId().toString());

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect asset type")
                    .that(allDisplayMyWorkAssetElement.get(i).getAssetType().toLowerCase())
                    .isEqualTo(getAssetTypeById(allAssetLightDtoOrderByNameList.get(i).getAssetType().getId()).toLowerCase());

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect date of build")
                    .that(allDisplayMyWorkAssetElement.get(i).getBuildDate())
                    .isEqualTo(LloydsRegister.FRONTEND_TIME_FORMAT.format(allAssetLightDtoOrderByNameList.get(i).getBuildDate()));

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect GRT")
                    .that(Integer.parseInt(allDisplayMyWorkAssetElement.get(i).getGrossTonnage()))
                    .isEqualTo(allAssetLightDtoOrderByNameList.get(i).getGrossTonnage().intValue());

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect flag")
                    .that(allDisplayMyWorkAssetElement.get(i).getFlag().toLowerCase())
                    .isEqualTo(getFlagById(allAssetLightDtoOrderByNameList.get(i).getFlagState().getId()).toLowerCase());

            assert_().withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] incorrect class status")
                    .that(allDisplayMyWorkAssetElement.get(i).getClassStatus().toLowerCase())
                    .isEqualTo(getClassStatusById(allAssetLightDtoOrderByNameList.get(i).getClassStatus().getId()).toLowerCase());

            //AC8 and AC12 check View Asset button is displayed
            assert_()
                    .withFailureMessage("Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] View Asset Button is not displayed.")
                    .that(allDisplayMyWorkAssetElement.get(i).isViewAssetButtonDisplayed()).isTrue();

            assert_().withFailureMessage(
                    "Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] View All Jobs Button is not displayed.")
                    .that(allDisplayMyWorkAssetElement.get(i).isViewAllCasesOrJobsButtonDisplayed()).isTrue();

            //AC8 Preview Push Down button is inactive according step 10 LRT-900

            //all display jobs per asset
            List<MyWorkAssetJob> allDisplayJobPerAsset = allDisplayMyWorkAssetElement.get(i).getMyWorkAssetJobs();
            JobQueryPage jobQueryPagePerAsset = new JobQueryPage(Collections.EMPTY_LIST,
                    Collections.singletonList(allDtoAssetIDOrderByNameList.get(i)));

            //Get all jobdto order by created date asc
            List<JobDto> allJobDtoPerAsset = jobQueryPagePerAsset.getDto().getContent()
                    .stream()
                    .sorted((dto1, dto2) -> dto1.getCreatedOn().compareTo(dto2.getCreatedOn()))
                    .collect(Collectors.toList());
            //order by created date desc, display the most recent job on top
            Collections.reverse(allJobDtoPerAsset);

            //AC11 No of count of jobs per asset
            assert_().withFailureMessage(
                    "Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] does not has number of jobs displayed as database")
                    .that(allDisplayMyWorkAssetElement.get(i).getNumberOfJob()).isEqualTo(allJobDtoPerAsset.size());

            //AC3 detail of each job
            //Iterate every job per asset
            for (int j = 0; j < allJobDtoPerAsset.size(); j++)
            {

                assert_().withFailureMessage(
                        "Asset name [" + allDisplayMyWorkAssetElement.get(i).getAssetName() + "] job number not display in correct order  ")
                        .that(allDisplayJobPerAsset.get(j).getJobNumnber()).isEqualTo(allJobDtoPerAsset.get(j).getId().toString());

                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "] incorrect job status ")
                        .that(allDisplayJobPerAsset.get(j).getJobStatus().toLowerCase())
                        .isEqualTo(getJobStatusById(allJobDtoPerAsset.get(j).getJobStatus().getId()).toLowerCase());  //todo how to grab job status

                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "] incorrect office role")
                        .that(allDisplayJobPerAsset.get(j).getJobOfficeRole().toLowerCase())
                        .contains(getOfficeRoleNameById(allJobDtoPerAsset.get(j).getOffices().get(0).getOfficeRole().getId()).toLowerCase());

                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "] incorrect office name")
                        .that(allDisplayJobPerAsset.get(j).getJobOffice().toLowerCase()).contains(
                        new Office(allJobDtoPerAsset.get(j).getOffices().get(0).getOffice().getId().intValue()).getDto().getName().toLowerCase());

                //todo Service Type retrieve value , GET /asset/{assetId}/service block by bug LRD-2161

                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "] incorrect ETA date")
                        .that(allDisplayJobPerAsset.get(j).getJobETA())
                        .isEqualTo(LloydsRegister.FRONTEND_TIME_FORMAT.format(allJobDtoPerAsset.get(j).getEtaDate()));

                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "] incorrect ETD date")
                        .that(allDisplayJobPerAsset.get(j).getJobETD())
                        .isEqualTo(LloydsRegister.FRONTEND_TIME_FORMAT.format(allJobDtoPerAsset.get(j).getEtaDate()));

                //AC13
                assert_().withFailureMessage("Job Number [" + allDisplayJobPerAsset.get(j).getJobNumnber() + "]")
                        .that(allDisplayJobPerAsset.get(j).isNavigateToJobArrowButtonDisplayed()).isTrue();

            }

            List<String> jobNumberCreatedNow = new ArrayList<>();

            try
            {
                //Create 2 jobs with today creation date
                jobNumberCreatedNow.add(createJobWithTodayDate(selectedAssetId));
                jobNumberCreatedNow.add(createJobWithTodayDate(selectedAssetId));
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

            myWorkSubPage.getAssetSortOrderDropDownSubPage().setAssetSortOrder(AssetOrderBy.NAME_ASC.getString());
            //all display jobs for selectedAsset
            List<String> allDisplayJobNumberForSelectedAsset
                    = myWorkSubPage.getAssetCardByAssetName(selectedAssetName).getMyWorkAssetJobs().stream().map(MyWorkAssetJob::getJobNumnber)
                    .collect(Collectors.toList());
            //AC6 2 newly created Jobs in the same day appear on the Asset Card
            assert_().withFailureMessage("2 jobs which have just been created on the same day are expected to be displayed")
                    .that(allDisplayJobNumberForSelectedAsset)
                    .contains(jobNumberCreatedNow);

            //Get all jobdto order by created date asc
            allJobDtoPerAsset = new JobQueryPage(Collections.EMPTY_LIST, Collections.singletonList(selectedAssetId)).getDto().getContent()
                    .stream()
                    .sorted((dto1, dto2) -> dto1.getCreatedOn().compareTo(dto2.getCreatedOn()))
                    .collect(Collectors.toList());
            //order by created date desc, display the most recent job on top
            Collections.reverse(allJobDtoPerAsset);
            //AC6 2 Jobs created on the same day order by then alphabetically A>Z based on the Job number
            assert_().withFailureMessage("2 jobs for asset [" + selectedAssetName
                    + "]  created on the same day are expected to be displayed according to Job number alphabetically A>Z ")
                    .that(allJobDtoPerAsset.stream().sorted((dto1, dto2) -> dto1.getId().compareTo(dto2.getId())).map(x -> x.getId().toString())
                            .collect(Collectors.toList()))
                    .containsExactlyElementsIn(allDisplayJobNumberForSelectedAsset);

        }
    }

    @Test(description = "Story 7.33 - As a user in online mode, " +
            "I want to view the Jobs assigned to me " +
            "so that I can take appropriate action")
    @Issue("LRT-900")
    public void navigationOptionsAndDefaultAssetJobPanesDisplay()
    {
        employee = new Employee((int) DEFAULT_EMPLOYEE);
        employeeName = employee.getDto().getFirstName() + " " + employee.getDto().getLastName();

        WorkHubPage workHubPage = WorkHubPage.open();

        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");

        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        //AC14 user is able to see jobs that are assigned to them -repeated in LRT-899 AC2
        //AC14 default max 3 assets card in one page
        int defaultMaxNoAssetDisplayPerPage = 3;
        int defaultMaxNoJobDisplay = 3;
        List<MyWorkAssetElement> allDisplayMyWorkAssetElement = myWorkSubPage.getAssetCards();
        List<MyWorkAssetJob> allDisplayedMyWorkAssetJob = myWorkSubPage.getJobItems();

        assert_().withFailureMessage("Default maximum number of Jobs display ")
                .that(allDisplayedMyWorkAssetJob.size()).isAtMost(defaultMaxNoAssetDisplayPerPage);

        //AC7
        assert_().withFailureMessage("Number of Jobs displayed is different from pagination")
                .that(allDisplayedMyWorkAssetJob.size())
                .isEqualTo(myWorkSubPage.getPaginationCountPage().getShownAmount());

        //AC8 and AC12  Click button View Asset
        ViewAssetPage viewAssetPage = allDisplayedMyWorkAssetJob.get(0)
                .mouseoverAssetInformation().clickViewAssetButton();
        assert_().withFailureMessage("Asset Model Page is displayed ")
                .that(viewAssetPage.isAssetModelTabSelected()).isTrue();
        workHubPage = viewAssetPage.getHeader().clickMastLogo();

        //AC8 Click button no of jobs per asset
        allDisplayedMyWorkAssetJob.get(0).mouseoverAssetInformation().clickJobCountButton();
        assert_().withFailureMessage("Job Page is displayed ")
                .that(viewAssetPage.isJobsTabSelected()).isTrue();

        workHubPage = viewAssetPage.getHeader().clickMastLogo();

        //AC10 Click button View All Jobs
        allDisplayedMyWorkAssetJob.get(0).mouseoverAssetInformation().clickJobCountButton();
        assert_().withFailureMessage("Asset Model Page is displayed ")
                .that(viewAssetPage.isJobsTabSelected()).isTrue();

        workHubPage = viewAssetPage.getHeader().clickMastLogo();

        //AC13 Click job navigation button
        assert_().withFailureMessage("Job Page is expected to displayed after clicking on arrow")
                .that(allDisplayedMyWorkAssetJob.get(0).clickNavigateToJobArrowButton().isJobPageDisplayed())
                .isTrue();

    }

    private String getJobStatusById(Long id)
    {
        ArrayList<ReferenceDataDto> jobStatuses =
                new ReferenceDataMap(JOB, JOB_STATUSES)
                        .getReferenceDataArrayList();
        return jobStatuses.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getAssetTypeById(Long id)
    {
        ArrayList<ReferenceDataDto> assetTypes =
                new ReferenceDataMap(ASSET, ASSET_TYPES)
                        .getReferenceDataArrayList();
        return assetTypes.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getFlagById(Long id)
    {
        ArrayList<ReferenceDataDto> flags =
                new ReferenceDataMap(FLAG, FLAGS)
                        .getReferenceDataArrayList();
        return flags.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getClassStatusById(Long id)
    {
        ArrayList<ReferenceDataDto> classStatuses =
                new ReferenceDataMap(ASSET, CLASS_STATUSES)
                        .getReferenceDataArrayList();
        return classStatuses.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    private String getOfficeRoleNameById(Long id)
    {
        ArrayList<ReferenceDataDto> officeRoles =
                new ReferenceDataMap(CASE, OFFICE_ROLES)
                        .getReferenceDataArrayList();
        return officeRoles.stream()
                .filter(ofr -> ofr.getId().equals(id))
                .findFirst()
                .map(ReferenceDataDto::getName)
                .orElse(null);
    }

    public String createJobWithTodayDate(Long assetId) throws SQLException
    {
        JobDto jobDto = new JobDto();
        jobDto.setCreatedOn(DateTime.now().toDate());

        List<EmployeeLinkDto> employeeLinkDtos = new ArrayList<>();
        LinkResource linkResource = new LinkResource();
        linkResource.setId((long) 2);

        EmployeeLinkDto employeeLinkDto = new EmployeeLinkDto();
        employeeLinkDto.setId((long) 1);
        employeeLinkDto.setEmployeeRole(linkResource);

        NamedLinkResource employee = new NamedLinkResource();
        employee.setId(DEFAULT_EMPLOYEE);

        employeeLinkDto.setLrEmployee(employee);
        employeeLinkDtos.add(employeeLinkDto);

        List<OfficeLinkDto> officeDtos = new ArrayList<>();
        OfficeLinkDto officeLinkDto = new OfficeLinkDto();

        linkResource = new LinkResource();
        linkResource.setId((long) 2);

        LinkResource assetID = new LinkResource();
        assetID.setId(assetId); //BAE Systems AI Test Asset 1

        //SDO role  in mast_ref_officerole = 1
        LinkResource officeRole = new LinkResource();
        officeRole.setId((long) 1);

        OfficeDto officeDto = new OfficeDto();
        officeDto.setId(8L);

        officeLinkDto.setId((long) 8);
        officeLinkDto.setOfficeRole(officeRole);
        officeLinkDto.setOffice(linkResource);
        officeDtos.add(officeLinkDto);

        LinkResource aCase = new LinkResource();
        aCase.setId(1L);

        jobDto.setAsset(new LinkVersionedResource(assetId, null));
        jobDto.setEmployees(employeeLinkDtos);
        jobDto.setJobStatus(linkResource);
        jobDto.setOffices(officeDtos);
        jobDto.setLocation("Automated location");
        jobDto.setWrittenServiceResponseSentDate(Date.valueOf("2018-07-22"));
        jobDto.setWrittenServiceRequestReceivedDate(Date.valueOf("2018-07-22"));
        jobDto.setDescription("Automated description");
        jobDto.setEtaDate(Date.valueOf("2018-05-22"));
        jobDto.setEtdDate(Date.valueOf("2018-06-22"));
        jobDto.setaCase(aCase);
        jobDto.setRequestedAttendanceDate(Date.valueOf("2018-04-14"));
        jobDto.setScopeConfirmed(true);

        return new Job(jobDto).getDto().getId().toString();
    }
}
