package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.asset.AssetQuery;
import model.cases.CaseQuery;
import org.joda.time.DateTime;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.MyWorkSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Arrays;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;

public class ViewSearchResultsOnDashboardTest extends BaseTest
{

    @Test(description = "Story 7.36 - As a user in online mode, I want to " +
            "view the search results on My Work dashboard and All Assets " +
            "dashboard so that I can take the required action with the search" +
            " results"
    )
    @Issue("LRT-959")
    public void viewSearchResultsOnDashboardTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Arrays.asList(DEFAULT_EMPLOYEE));
        CaseQuery employeeCases = new CaseQuery(caseQueryDto);
        Asset searchableAsset = new Asset(employeeCases.getDto().getContent().get(0).getAsset().getId().intValue());

        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(Arrays.asList(searchableAsset.getDto().getAssetLifecycleStatus().getId()));
        assetQueryDto.setBuildDateMin(new DateTime(searchableAsset.getDto().getBuildDate()).minusDays(1).toDate());
        assetQueryDto.setBuildDateMax(new DateTime(searchableAsset.getDto().getBuildDate()).plusDays(1).toDate());
        assetQueryDto.setBuilder(searchableAsset.getDto().getBuilder());
        assetQueryDto.setClassStatusId(Arrays.asList(searchableAsset.getDto().getClassStatus().getId()));
        assetQueryDto.setFlagStateId(Arrays.asList(searchableAsset.getDto().getFlagState().getId()));
        AssetQuery assetQueryWithBuilder = new AssetQuery(assetQueryDto);
        assetQueryDto.setBuilder(null);
        AssetQuery assetQueryWithoutBuilder = new AssetQuery(assetQueryDto);

        //AC 1
        searchFilterSubPage.applySearchFiltersBasedOnQuery(assetQueryDto);

        //AC 3
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_()
                .withFailureMessage("Amount of results shown didn't match results from BE")
                .that(allAssetsSubPage.getAssetCardCount())
                .isEqualTo(assetQueryWithoutBuilder.getDto().getContent().size());

        //AC 4 & AC 6
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();
        searchFilterSubPage.isLifecycleStatusFilterButtonSelected();
        searchFilterSubPage.isClassStatusFilterButtonSelected();
        searchFilterSubPage.isBuildDateFilterButtonSelected();

        //AC 7
        assert_()
                .withFailureMessage("Can't find the asset which was searched for after clicking my work")
                .that(myWorkSubPage.getAssetCardByAssetName(searchableAsset.getDto().getName()))
                .isNotNull();

        //AC 8
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput("ASSET DOESNT EXIST");
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_()
                .withFailureMessage("Search should of returned blank, but contained results")
                .that(allAssetsSubPage.getAssetCardCount())
                .isEqualTo(0);

        //AC 9
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(searchableAsset.getDto().getName());
        assert_()
                .withFailureMessage("Search results shouldn't of changed without clicking apply")
                .that(allAssetsSubPage.getAssetCardCount())
                .isEqualTo(0);

        //AC 10
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_()
                .withFailureMessage("Can't find the asset which was searched for after changing the search criteria")
                .that(allAssetsSubPage.getAssetCardByAssetName(searchableAsset.getDto().getName()))
                .isNotNull();
    }
}
