package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.AssetTypeFilterSubPage;
import workhub.sub.searchfilter.LifecycleStatusFilterSubPage;

import java.util.ArrayList;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.ASSET_TYPES;
import static model.referencedata.ReferenceDataMap.RefData.LIFECYCLE_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class ResetLifecycleStatusTest extends BaseTest
{

    private ArrayList<ReferenceDataDto> lifecycleStatusDtos;

    @Test(description = "US7.37 AC:1,2,3,4 - Online user is able to reset the " +
            "search results for the Lifecycle Status tab.")
    @Issue("LRT-880")
    public void resetLifeCycleStatusTabTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage = searchFilterSubPage.clickLifecycleStatusFilterButton();

        assert_().withFailureMessage(getLifecycleStatuses().get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(0)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(1)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(2)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(3)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(4)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(5)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(6)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(7)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(8)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(9)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(10)))
                .isFalse();

        //AC: 1
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(0));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(1));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(2));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(3));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(4));

        assert_().withFailureMessage(getLifecycleStatuses().get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(0)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(1)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(2)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(3)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(4)))
                .isFalse();

        //AC: 2
        lifecycleStatusFilterSubPage.clickSelectAllButton();

        assert_().withFailureMessage("All Check boxes are checked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //AC: 2
        lifecycleStatusFilterSubPage.clickClearButton();

        assert_().withFailureMessage("All Check boxes are unchecked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //AC: 2
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(6));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(7));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(8));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(9));
        lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(getLifecycleStatuses().get(10));

        assert_().withFailureMessage(getLifecycleStatuses().get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(6)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(7)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(8) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(8)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(9) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(9)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(10) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(10)))
                .isTrue();

        //AC: 2
        lifecycleStatusFilterSubPage.clickClearButton();
        assert_().withFailureMessage("All Check boxes are unchecked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //AC: 3
        lifecycleStatusFilterSubPage.clickResetButton();
        assert_().withFailureMessage("User has assets with cases assigned")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isGreaterThan(0);

        //AC: 4

        assert_().withFailureMessage(getLifecycleStatuses().get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(0)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(1)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(2)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(3)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(4)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(5)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(6)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(7)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(8)))
                .isFalse();

        assert_().withFailureMessage(getLifecycleStatuses().get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(9)))
                .isTrue();

        assert_().withFailureMessage(getLifecycleStatuses().get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(getLifecycleStatuses().get(10)))
                .isFalse();
    }

    @Test(description = "US7.37 AC:1,2,3,4 - Online user is able to reset the" +
            " search results for the Asset type tab.")
    @Issue("LRT-870")
    public void resetAssetTypeTabTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        AssetTypeFilterSubPage assetTypeFilterSubPage = searchFilterSubPage.clickAssetTypeFilterButton();
        assert_().withFailureMessage("Statcode 1 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");

        AssetTypeDto statcodeOneAssetType = getAssetTypeBasedOnParent(null);
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());

        assert_().withFailureMessage("Statcode 2 is visible after selecting a value in statcode 1")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isTrue();

        //AC 1 & 2
        searchFilterSubPage.clickAssetSearchResetButton();
        assert_().withFailureMessage("Reset Button Label should be displayed")
                .that(searchFilterSubPage.getAssetSearchResetButtonText())
                .isEqualTo("Reset");

        assert_().withFailureMessage("Reset Button Color is white")
                .that(searchFilterSubPage.getAssetSearchResetButtonColor())
                .isEqualTo("rgba(231, 235, 238, 1)");

        assert_().withFailureMessage("Statcode 2 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");

        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        assert_().withFailureMessage("Statcode 2 is visible after selecting a value in statcode 1")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isTrue();

        AssetTypeDto statcodeTwoAssetType = getAssetTypeBasedOnParent(statcodeOneAssetType.getId());
        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        assert_().withFailureMessage("Statcode 3 is visible after selecting a value in statcode 2")
                .that(assetTypeFilterSubPage.isStatcodeThreeDropdownVisible())
                .isTrue();

        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("Statcode 1 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");

        assert_().withFailureMessage("Statcode 2 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isFalse();

        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        assert_().withFailureMessage("Statcode 2 is visible after selecting a value in statcode 1")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isTrue();

        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        assert_().withFailureMessage("Statcode 3 is visible after selecting a value in statcode 2")
                .that(assetTypeFilterSubPage.isStatcodeThreeDropdownVisible())
                .isTrue();

        AssetTypeDto statcodeThreeAssetType = getAssetTypeBasedOnParent(statcodeTwoAssetType.getId());
        assetTypeFilterSubPage.setStatcodeThreeDropdown(statcodeThreeAssetType.getName());
        assert_().withFailureMessage("Statcode 4 is visible after selecting a value in statcode 3")
                .that(assetTypeFilterSubPage.isStatcodeFourDropdownVisible())
                .isTrue();
        //AC: 1 and 2
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("Statcode 1 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");
        assert_().withFailureMessage("Statcode 2 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isFalse();
        assert_().withFailureMessage("Statcode 3 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeThreeDropdownVisible())
                .isFalse();
        //step13
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        assert_().withFailureMessage("Statcode 2 is visible after selecting a value in statcode 1")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isTrue();

        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        assert_().withFailureMessage("Statcode 3 is visible after selecting a value in statcode 2")
                .that(assetTypeFilterSubPage.isStatcodeThreeDropdownVisible())
                .isTrue();

        assetTypeFilterSubPage.setStatcodeThreeDropdown(statcodeThreeAssetType.getName());
        assert_().withFailureMessage("Statcode 4 is visible after selecting a value in statcode 3")
                .that(assetTypeFilterSubPage.isStatcodeFourDropdownVisible())
                .isTrue();

        //AC: 1 and 2
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("Statcode 1 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");
        assert_().withFailureMessage("Statcode 2 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isFalse();
        assert_().withFailureMessage("Statcode 3 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeThreeDropdownVisible())
                .isFalse();
        assert_().withFailureMessage("Statcode 4 is not visible after reset")
                .that(assetTypeFilterSubPage.isStatcodeFourDropdownVisible())
                .isFalse();

        //AC:3
        searchFilterSubPage.clickAssetSearchResetButton();
        assert_().withFailureMessage("User has assets with cases assigned")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isGreaterThan(0);

        //AC:4
        assert_().withFailureMessage("Statcode 1 default drop-down is ALL")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");
    }

    private ArrayList<String> getLifecycleStatuses()
    {
        if (lifecycleStatusDtos == null)
        {
            lifecycleStatusDtos = new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES).getReferenceDataArrayList();
        }
        ArrayList<String> status = new ArrayList<>();
        for (int i = 0; i < lifecycleStatusDtos.size(); i++)
        {
            status.add(lifecycleStatusDtos.get(i).getName());
        }
        return status;
    }

    private AssetTypeDto getAssetTypeBasedOnParent(Long parentId)
    {
        ReferenceDataMap assetTypes = new ReferenceDataMap(ASSET, ASSET_TYPES);
        for (Object assetTypeDto : assetTypes.getReferenceDataAsClass(AssetTypeDto.class))
        {
            // do not change to .equals under any circumstances!
            if (parentId == ((AssetTypeDto) assetTypeDto).getParentId())
            {
                return (AssetTypeDto) assetTypeDto;
            }
        }
        return null;
    }
}
