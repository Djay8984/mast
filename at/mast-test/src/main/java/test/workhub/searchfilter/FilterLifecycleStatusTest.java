package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.LifecycleStatusFilterSubPage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.LIFECYCLE_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class FilterLifecycleStatusTest extends BaseTest
{

    private final String[] expectedLifecycleStatusList = {
            "Under construction",
            "Build suspended",
            "In service",
            "Laid up",
            "Under repair",
            "In casualty",
            "Converting",
            "To be broken up",
            "Decommisioned",
            "To be handed over",
            "Cancelled"
    };

    @Test(description = "US7.21 AC:1-5 User is able to filter by Lifecycle Status")
    @Issue("LRT-967")
    public void filterLifeCycleStatusTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickMyWorkTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        List<String> getLifecycleStatusesText = getLifecycleStatuses()
                .stream()
                .collect(Collectors.toList());

        //Step 3
        //AC: 1 and AC: 2
        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage = searchFilterSubPage
                .clickLifecycleStatusFilterButton();

        //check that FE has expected LifeCycleStatusList
        assert_()
                .withFailureMessage(("Life Cycle status should have these values: " +
                        Arrays.asList(expectedLifecycleStatusList)))
                .that(getLifecycleStatusesText)
                .isEqualTo(Arrays.asList(expectedLifecycleStatusList));

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(0)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(1)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(2)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(3)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(4)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(5)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(6)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(7)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(8)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(9)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(10)))
                .isFalse();

        //Step: 4
        //AC: 1 and AC: 2
        lifecycleStatusFilterSubPage.clickResetButton();
        workHubPage.clickAllAssetsTab();

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(0)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(1)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(2)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(3)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(4)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(5)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(6)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(7)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(8)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(9)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(10)))
                .isFalse();

        //Step: 5
        //AC: 3
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(0));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(1));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(2));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(3));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(4));

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(0)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(1)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(2)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(3)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(4)))
                .isFalse();

        //Step: 6
        //AC: 4
        lifecycleStatusFilterSubPage.clickSelectAllButton();
        assert_().withFailureMessage("All Check boxes are checked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //Step: 7
        //AC: 5
        lifecycleStatusFilterSubPage.clickClearButton();
        assert_().withFailureMessage("All Check boxes are checked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //Step: 8
        // /AC: 3
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(6));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(7));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(8));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(9));
        lifecycleStatusFilterSubPage.
                deSelectLifecycleStatusesCheckBoxes(getLifecycleStatusesText.get(10));

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(6)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(7)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(8) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(8)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(9) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(9)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(10) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(10)))
                .isTrue();

        //Step: 9
        //AC: 1
        lifecycleStatusFilterSubPage.clickResetButton();

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(8)))
                .isFalse();

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(9)))
                .isTrue();

        //Step: 10
        //AC: 1 and AC: 2
        lifecycleStatusFilterSubPage.clickResetButton();

        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(0)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(1)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(2)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(3)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(4)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(5)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(6)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(7)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(8)))
                .isFalse();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(9)))
                .isTrue();
        assert_()
                .withFailureMessage(getLifecycleStatusesText.get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(getLifecycleStatusesText.get(10)))
                .isFalse();
    }

    private ArrayList<String> getLifecycleStatuses()
    {
        ArrayList<ReferenceDataDto> lifecycleStatusDtos =
                new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES)
                        .getReferenceDataArrayList();
        ArrayList<String> status = new ArrayList<>();
        for (int i = 0; i < lifecycleStatusDtos.size(); i++)
        {
            status.add(lifecycleStatusDtos.get(i).getName());
        }
        return status;
    }
}
