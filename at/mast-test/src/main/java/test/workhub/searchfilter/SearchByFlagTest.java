package test.workhub.searchfilter;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.FlagSearchSubPage;
import workhub.sub.searchfilter.GrossTonnageSearchSubPage;

import static com.google.common.truth.Truth.assert_;

public class SearchByFlagTest extends BaseTest
{

    @Test(
            description = "Automation - Story 7.25 - As a user I want to be able to filter by flag name "
                    + " so that I can find the assets associated to a specific flag"
    )
    @Issue("LRT-988")
    public void searchByFlagTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        FlagSearchSubPage flagSearchSubPage = searchFilterSubPage.clickFlagFilterButton();

        assert_().withFailureMessage("The error icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.ERROR))
                .isFalse();
        assert_().withFailureMessage("The clear icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.CLEAR))
                .isFalse();

        String flag = TestDataHelper.getFlagCodeFromSearch();
        String shortFlag = flag.substring(0, 3);

        //AC1, AC4
        flagSearchSubPage.setFlagCode(shortFlag);
        assert_().withFailureMessage("Unable to find the error icon")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.ERROR))
                .isTrue();
        assert_().withFailureMessage("The clear icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.CLEAR))
                .isFalse();

        //AC5
        flagSearchSubPage.selectFlagFromDropdown(flag);
        assert_().withFailureMessage("The error icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.ERROR))
                .isFalse();
        assert_().withFailureMessage("Unable to find the clear icon")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.CLEAR))
                .isTrue();

        //AC3
        flagSearchSubPage.clickClearFlagText();
        assert_().withFailureMessage("The error icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.ERROR))
                .isFalse();
        assert_().withFailureMessage("The clear icon should not be seen")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.CLEAR))
                .isFalse();

        flag = TestDataHelper.getFlagCodeFromSearch().substring(0, 3);

        //AC2, AC6
        flagSearchSubPage.setFlagCode(flag);
        GrossTonnageSearchSubPage grossTonnage = searchFilterSubPage.clickGrossTonnageFilterButton();
        assert_().withFailureMessage("The gross tonnage search should not be seen, you should be on the flag search")
                .that(grossTonnage.isGrossTonnageAreaDisplayed())
                .isFalse();
    }
}
