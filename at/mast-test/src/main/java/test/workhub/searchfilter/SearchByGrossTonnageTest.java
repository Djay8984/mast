package test.workhub.searchfilter;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.GrossTonnageSearchSubPage;

import static com.google.common.truth.Truth.assert_;

public class SearchByGrossTonnageTest extends BaseTest
{

    @DataProvider(name = "GrossTonnage")
    public Object[][] grossTonnage()
    {
        return new Object[][]{
                {"20", "1000", false, true},
                {"1000000", "8888888", false, true},//AC1
                {"10000000", "88888888", false, true},//AC1
                {"4056", "75", true, false}, //AC5, AC9
                {"dfgoitj", "whoops", false, true},//AC1
                {"!£$%", "^&*()", false, true},//AC1
                {"5923", "9564", false, true},//AC1
                {"test1234", "bork8765", false, true},//AC1
                {"", "9999", false, true}, //AC2, AC3
                {"1000", "", false, false}, //AC2, AC4
                {"9999", "9999", false, true}//AC8
        };
    }

    @Test(
            description = "Story 7.26 - As a user I want to be able to filter by gross tonnage so " +
                    "that I can find the relevant assets",
            dataProvider = "GrossTonnage"
    )
    @Issue("LRT-791")
    public void searchByGrossTonnageTest(String minTonnage, String maxTonnage, boolean minErrorShown, boolean maxErrorShown)
    {
        final int MAX_CHARACTERS = 7;

        WorkHubPage workHubPage = WorkHubPage.open();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        GrossTonnageSearchSubPage tonnageSearchSubPage = searchFilterSubPage.clickGrossTonnageFilterButton();
        tonnageSearchSubPage.setMinGrossTonnage(minTonnage);
        tonnageSearchSubPage.setMaxGrossTonnage(maxTonnage);
        if (minErrorShown)
        {
            assert_().withFailureMessage("The error message is not displayed")
                    .that(tonnageSearchSubPage.isGrossTonnageMinErrorDisplayed())
                    .isTrue();
        }
        if (maxErrorShown)
        {
            tonnageSearchSubPage.setMinGrossTonnage("9999999");
            assert_().withFailureMessage("the error message is not displayed")
                    .that(tonnageSearchSubPage.isGrossTonnageMaxErrorDisplayed())
                    .isTrue();
            tonnageSearchSubPage.setMinGrossTonnage(minTonnage);
        }

        assert_().withFailureMessage("The maximum allowed characters should be 7")
                .that(tonnageSearchSubPage.getMinGrossTonnage().length())
                .isAtMost(MAX_CHARACTERS);
        assert_().withFailureMessage("The maximum allowed characters should be 7")
                .that(tonnageSearchSubPage.getMaxGrossTonnage().length())
                .isAtMost(MAX_CHARACTERS);

        minTonnage = minTonnage.replaceAll("[^0-9]", "");
        if (minTonnage.length() > MAX_CHARACTERS)
        {
            minTonnage = minTonnage.substring(0, MAX_CHARACTERS);
        }
        maxTonnage = maxTonnage.replaceAll("[^0-9]", "");
        if (maxTonnage.length() > MAX_CHARACTERS)
        {
            maxTonnage = maxTonnage.substring(0, MAX_CHARACTERS);
        }

        assert_().withFailureMessage("The expected and actual minimum gross tonnage do not match")
                .that(tonnageSearchSubPage.getMinGrossTonnage())
                .isEqualTo(minTonnage);
        assert_().withFailureMessage("The expected and actual maximum gross tonnage do not match")
                .that(tonnageSearchSubPage.getMaxGrossTonnage())
                .isEqualTo(maxTonnage);
    }
}
