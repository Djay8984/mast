package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetPage;
import model.asset.AssetQuery;
import model.cases.CaseQuery;
import model.flag.FlagState;
import model.referencedata.ReferenceDataMap;
import org.apache.commons.lang.WordUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.element.MyWorkAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.MyWorkSubPage;
import workhub.sub.PaginationCountSubPage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class AssetViewInformationTest extends BaseTest
{

    @Test(description = "7.32 AC 1-6 - All Asset view dashboard order & display")
    @Issue("LRT-911")
    public void allAssetViewDisplayTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();
        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Arrays.asList(DEFAULT_EMPLOYEE));
        caseQueryDto.setCaseStatusId(Arrays.asList(1L, 2L, 3L));
        CaseQuery assetCases = new CaseQuery(caseQueryDto);

        List<CaseDto> caseDtos = assetCases.getDto().getContent();
        List<Long> idList = caseDtos.stream()
                .map(caseDto ->
                        caseDto.getAsset().getId()
                )
                .collect(Collectors.toList());

        AssetQueryDto assetCaseQueryDto = new AssetQueryDto();
        assetCaseQueryDto.setIdList(idList);
        AssetQuery assetCaseQuery = new AssetQuery(assetCaseQueryDto);
        List<AssetLightDto> sortedAssetListWithCase = assetCaseQuery.getDto()
                .getContent()
                .stream()
                .sorted((d1, d2) ->
                        d1.getName().compareTo(d2.getName())
                )
                .collect(Collectors.toList());
        List<MyWorkAssetElement> cards = myWorkSubPage.getAssetCards();
        for (int i = 0; i < cards.size(); i++)
        {
            assert_().withFailureMessage("Asset name is displayed and matches")
                    .that(cards.get(i).getAssetName().trim())
                    .isEqualTo(sortedAssetListWithCase.get(i).getName().trim());
        }

        //Step: 3
        // AC: 1
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(getLifecycleStatusesID());
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);

        assetQuery.getDto().getContent();

        assert_().withFailureMessage("User can see all assets which do not " +
                "have a lifecycle status of 'decommissioned' or 'cancelled'")
                .that(allAssetsSubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        //Step: 4
        //AC: 2
        AssetPage asset = new AssetPage(0, 12, "asc", "name");
        List<AssetLightDto> assetNameAscDtos = asset.getDto().getContent()
                .stream().filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());
        ArrayList<String> ascNameAsc = new ArrayList<>();
        for (int i = 0; i < assetNameAscDtos.size(); i++)
        {
            ascNameAsc.add(WordUtils.capitalize(assetNameAscDtos.get(i)
                    .getName()));
        }

        for (int i = 0; i < ascNameAsc.size(); i++)
        {
            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(ascNameAsc.get(i));

            FlagState flag = new FlagState(assetNameAscDtos.get(i)
                    .getFlagState().getId().intValue());

            Date date = assetNameAscDtos.get(i).getBuildDate();
            SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM yyyy");

            AssetTypeDto assetTypeDto =
                    (AssetTypeDto) new ReferenceDataMap(ASSET, ASSET_TYPES).getReferenceDataAsClass(AssetTypeDto.class)
                            .stream()
                            .filter(
                                    assetType -> ((AssetTypeDto) assetType).getId()
                                            .equals(sortedAssetListWithCase.get(0).getAssetType().getId())
                            ).findFirst().orElse(null);

            assert_().withFailureMessage("Asset Name is displayed")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(ascNameAsc.get(i));

            assert_().withFailureMessage("Asset Type is displayed")
                    .that(allAssetsAssetElement.getAssetType())
                    .isEqualTo(assetTypeDto.getName());

            assert_().withFailureMessage("Build data is displayed")
                    .that(allAssetsAssetElement.getBuildDate())
                    .isEqualTo(formatDate.format(date).toString());

            assert_().withFailureMessage("IMO number is displayed")
                    .that(allAssetsAssetElement.getImoNumber())
                    .isEqualTo(assetNameAscDtos
                            .get(i).getIhsAsset().getId().toString());

            assert_().withFailureMessage("Gross Tonnage is displayed")
                    .that(allAssetsAssetElement.getGrossTonnage())
                    .isEqualTo(Integer.toString(assetNameAscDtos.get(i)
                            .getGrossTonnage().intValue()));

            assert_().withFailureMessage("Flag is displayed")
                    .that(allAssetsAssetElement.getFlag())
                    .isEqualTo(flag.getDto().getName());

            assert_().withFailureMessage("Class status is displayed")
                    .that(allAssetsAssetElement.getClassStatus())
                    .isEqualTo(getClassStatusById(assetNameAscDtos.get(i)
                            .getClassStatus().getId().intValue()));
        }

        //Step: 5
        //AC: 3
        assert_().withFailureMessage("Assets displayed against the available " +
                "number").that(allAssetsSubPage.getPaginationCountPage()
                .getPaginationCountText())
                .isEqualTo("Showing " + allAssetsSubPage.getAssetCardCount() +
                        " of " + assetQuery.getDto()
                        .getContent().size() + " assets");

        //Step: 6
        //Sort By name A-Z
        //AC: 4

        assert_().withFailureMessage("Asset name A-Z is Selected").that(allAssetsSubPage
                .getSelectedOptionsFromAssetSortOrderDropdown())
                .isEqualTo("Asset name A-Z");

        allAssetsSubPage.setAssetSortOrder("Asset name A-Z");

        for (int i = 0; i < ascNameAsc.size(); i++)
        {
            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(ascNameAsc.get(i));

            assert_().withFailureMessage("Asset Name is displayed in Ascending Order")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(ascNameAsc.get(i));
        }

        //Step: 7
        //Verify Sort DropDown Values
        //AC: 4

        assert_().withFailureMessage("Asset name A-Z is present")
                .that(allAssetsSubPage
                        .isSortDropDownTextDisplayed("Asset name A-Z")).isTrue();

        assert_().withFailureMessage("Asset name A-Z is present")
                .that(allAssetsSubPage
                        .isSortDropDownTextDisplayed("Asset name Z-A")).isTrue();

        assert_().withFailureMessage("Asset name A-Z is present")
                .that(allAssetsSubPage
                        .isSortDropDownTextDisplayed("Build date ASC")).isTrue();

        assert_().withFailureMessage("Asset name A-Z is present")
                .that(allAssetsSubPage
                        .isSortDropDownTextDisplayed("Build date DESC")).isTrue();

        //Step: 8
        //Sort By name Z-A
        //AC: 4
        allAssetsSubPage.setAssetSortOrder("Asset name Z-A");
        asset = new AssetPage(0, 12, "desc", "name");

        List<AssetLightDto> assetNameDscDtos = asset.getDto().getContent()
                .stream().filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());
        ArrayList<String> ascNameDsc = new ArrayList<>();
        for (int i = 0; i < assetNameDscDtos.size(); i++)
        {
            ascNameDsc.add(WordUtils.capitalize(assetNameDscDtos.get(i)
                    .getName()));
        }
        for (int i = 0; i < ascNameDsc.size(); i++)
        {
            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(ascNameDsc.get(i));
            assert_().withFailureMessage("Asset Name is displayed in Ascending Order")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(ascNameDsc.get(i));
        }

        //Step: 9
        //Sort by Build Date ASC
        //AC: 4
        allAssetsSubPage.setAssetSortOrder("Build date ASC");
        asset = new AssetPage(0, 12, "asc", "BuildDate");
        List<AssetLightDto> assetBuildAscDtos = asset.getDto().getContent()
                .stream().filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());
        ArrayList<String> assetBuildAsc = new ArrayList<>();
        for (int i = 0; i < assetBuildAscDtos.size(); i++)
        {
            assetBuildAsc.add(WordUtils.capitalize(assetBuildAscDtos.get(i)
                    .getName()));
        }
        for (int i = 0; i < assetBuildAsc.size(); i++)
        {
            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(assetBuildAsc.get(i));
            assert_().withFailureMessage("Build Date is displayed in Ascending Order")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(assetBuildAsc.get(i));
        }

        //Step: 10
        //Sort By Build Date DESC
        //AC: 4
        allAssetsSubPage.setAssetSortOrder("Build date DESC");
        asset = new AssetPage(0, 12, "desc", "BuildDate");
        List<AssetLightDto> assetBuildDscDtos = asset.getDto().getContent()
                .stream().filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());
        ArrayList<String> assetBuildDsc = new ArrayList<>();
        for (int i = 0; i < assetBuildDscDtos.size(); i++)
        {
            assetBuildDsc.add(WordUtils.capitalize(assetBuildDscDtos.get(i)
                    .getName()));
        }

        for (int i = 0; i < assetBuildDsc.size(); i++)
        {
            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(assetBuildDsc.get(i));
            assert_().withFailureMessage("Build Date is displayed in Ascending Order")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(assetBuildDsc.get(i));
        }

        //Step: 11
        //Sort Asset name A-Z
        //AC: 4
        allAssetsSubPage.setAssetSortOrder("Asset name A-Z");
        assert_().withFailureMessage("View Asset Button is displayed").
                that(allAssetsSubPage.isViewAssetButtonDisplayed())
                .doesNotContain(false);

        //Step: 13 and 14
        //Sort Build date ASC to verify for both active Cases and No Cases
        //AC: 6

        allAssetsSubPage.setAssetSortOrder("Build date ASC");
        CaseQueryDto caseQuerydto = new CaseQueryDto();
        caseQuerydto.setAssetId(Arrays.asList(assetBuildAscDtos.get(0).getId()));
        CaseQuery cases = new CaseQuery(caseQuerydto);
        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                .getAssetCardByAssetName(assetBuildAsc.get(0));
        if (cases.getDto().getContent().size() > 0)

            assert_().withFailureMessage("Active cases are display")
                    .that(allAssetsAssetElement.getCaseCountText())
                    .isEqualTo(cases.getDto()
                            .getContent().size() + " case");
        else
        {
            assert_().withFailureMessage("No Active cases to display")
                    .that(allAssetsAssetElement.getCaseCountText())
                    .isEqualTo("No active cases");
        }

        caseQueryDto.setAssetId(Arrays.asList(assetBuildAscDtos.get(1).getId()));
        cases = new CaseQuery(caseQueryDto);

        allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(assetBuildAsc.get(1));
        if (cases.getDto().getContent().size() > 0)
            assert_().withFailureMessage("Active cases are display")
                    .that(allAssetsAssetElement
                            .getCaseCountText()).isEqualTo(cases.getDto()
                    .getContent().size() + " case");
        else
        {
            assert_().withFailureMessage("No Active cases to display")
                    .that(allAssetsAssetElement
                            .getCaseCountText()).isEqualTo("No active cases");
        }
    }

    @Test(description = "7.32 AC 7-11 - Asset header titles and push downs")
    @Issue("LRT-912")
    public void assetHeaderPushDownTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setEmployeeId(Arrays.asList(DEFAULT_EMPLOYEE));
        caseQueryDto.setCaseStatusId(Arrays.asList(1L, 2L, 3L));
        CaseQuery employeeCases = new CaseQuery(caseQueryDto);

        List<CaseDto> caseDtos = employeeCases.getDto().getContent();
        List<Long> idList = caseDtos.stream()
                .map(caseDto ->
                        caseDto.getAsset().getId()
                )
                .collect(Collectors.toList());

        AssetQueryDto assetCaseQueryDto = new AssetQueryDto();
        assetCaseQueryDto.setIdList(idList);

        AssetQuery assetCaseQuery = new AssetQuery(assetCaseQueryDto);
        List<AssetLightDto> sortedAssetListWithCase = assetCaseQuery.getDto()
                .getContent()
                .stream()
                .sorted((d1, d2) ->
                        d1.getName().compareTo(d2.getName())
                )
                .collect(Collectors.toList());
        List<MyWorkAssetElement> cards = myWorkSubPage.getAssetCards();

        assert_().withFailureMessage("Asset name is displayed and matches")
                .that(cards.get(0).getAssetName().trim())
                .isEqualTo(sortedAssetListWithCase.get(0).getName().trim());

        //Step: 3
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(getLifecycleStatusesID());
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);

        assert_().withFailureMessage("User can see all assets which do not " +
                "have a lifecycle status of 'decommissioned' or 'cancelled'")
                .that(allAssetsSubPage.getPaginationCountPage()
                        .getTotalAmount()).isEqualTo(assetQuery.getDto()
                .getContent().size());

        //Step: 4,5,6 and 7
        //AC: 7, 8, 9 and 10
        allAssetsSubPage.setAssetSortOrder("Build date ASC");
        AssetPage asset = new AssetPage(0, 12, "asc", "BuildDate");
        List<AssetLightDto> assetBuildAscDtos = asset.getDto().
                getContent().stream().filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());
        ArrayList<String> assetBuildAsc = new ArrayList<>();
        for (int i = 0; i < assetBuildAscDtos.size(); i++)
        {
            assetBuildAsc.add(assetBuildAscDtos.get(i).getName());
        }

        for (int i = 0; i < assetBuildAsc.size(); i++)
        {

            AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                    .getAssetCardByAssetName(assetBuildAsc.get(i));

            if (!allAssetsAssetElement.getCaseCountText().contains("No active cases"))
            {
                MyWorkAssetElement myWorkAssetElement = allAssetsAssetElement
                        .clickOpenAssetTitle();
                assert_().withFailureMessage("Push down panel displayed")
                        .that(myWorkAssetElement.isPushDownPanelDisplayed())
                        .isTrue();

                myWorkAssetElement.clickCloseAssetTitle();
                assert_().withFailureMessage("Push down panel not displayed")
                        .that(allAssetsSubPage.isPushDownPanelDisplayed())
                        .isFalse();
            }
            else
            {
                assert_().withFailureMessage("Asset Icon not clickable")
                        .that(allAssetsAssetElement.isOpenAssetIconClickable())
                        .isTrue();
            }
        }
        //Step: 8 and 9
        //AC: 11
        if (assetQuery.getDto().getContent().size() > 12)
        {
            assert_().withFailureMessage("Load More Button is displayed ")
                    .that(allAssetsSubPage.isLoadMoreButtonDisplayed()).isTrue();
            allAssetsSubPage.clickLoadMoreButton();

            PaginationCountSubPage paginationCountSubPage = allAssetsSubPage
                    .getPaginationCountPage();
            assert_().withFailureMessage("Pagination count is displayed correctly ")
                    .that(paginationCountSubPage.getShownAmount())
                    .isGreaterThan(12);
        }
        else
        {
            assert_().withFailureMessage("Load More Button is not displayed ")
                    .that(allAssetsSubPage.isLoadMoreButtonDisplayed())
                    .isFalse();
        }
    }

    private String getClassStatusById(int id)
    {
        ArrayList<ReferenceDataDto> classStatus =
                new ReferenceDataMap(ASSET, CLASS_STATUSES)
                        .getReferenceDataArrayList();
        String status = classStatus.stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(dto -> dto.getName())
                .orElse(null);
        return status;
    }

    private ArrayList<Long> getLifecycleStatusesID()
    {
        ArrayList<ReferenceDataDto> lifecycleStatusDtos =
                new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES).getReferenceDataArrayList();
        ArrayList<Long> status = new ArrayList<>();
        for (int i = 0; i < lifecycleStatusDtos.size(); i++)
        {
            status.add(lifecycleStatusDtos.get(i).getId());
        }
        status.remove((long) 9);
        status.remove((long) 11);
        return status;
    }

}
