package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.ClassStatusFilterSubPage;

import java.util.ArrayList;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.CLASS_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class ClassStatusFilterTest extends BaseTest
{

    @Test(description = "Story 7.22 - As a user I want to be able to filter by" +
            " Class status so that I can restrict search results based on the " +
            "class status of an asset")
    @Issue("LRT-970")
    public void filterByClassStatusTest()
    {

        WorkHubPage workHubPage = WorkHubPage.open();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        //AC: 1 and 2
        ClassStatusFilterSubPage classStatusFilterSubPage = searchFilterSubPage.clickClassStatusFilterButton();

        assert_().withFailureMessage("All Check boxes are checked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //AC: 3
        classStatusFilterSubPage.deSelectClassStatusesCheckBoxes(getClassStatuses().get(0));
        classStatusFilterSubPage.deSelectClassStatusesCheckBoxes(getClassStatuses().get(1));
        classStatusFilterSubPage.deSelectClassStatusesCheckBoxes(getClassStatuses().get(2));
        classStatusFilterSubPage.deSelectClassStatusesCheckBoxes(getClassStatuses().get(3));
        classStatusFilterSubPage.deSelectClassStatusesCheckBoxes(getClassStatuses().get(4));

        assert_().withFailureMessage(getClassStatuses().get(0) + " Class Status is checked")
                .that(classStatusFilterSubPage.isClassStatusSelected(getClassStatuses().get(0)))
                .isFalse();

        assert_().withFailureMessage(getClassStatuses().get(1) + " Class Status is checked")
                .that(classStatusFilterSubPage.isClassStatusSelected(getClassStatuses().get(1)))
                .isFalse();

        assert_().withFailureMessage(getClassStatuses().get(2) + " Class Status is checked")
                .that(classStatusFilterSubPage.isClassStatusSelected(getClassStatuses().get(2)))
                .isFalse();

        assert_().withFailureMessage(getClassStatuses().get(3) + " Class Status is checked")
                .that(classStatusFilterSubPage.isClassStatusSelected(getClassStatuses().get(3)))
                .isFalse();

        assert_().withFailureMessage(getClassStatuses().get(4) + " Class Status is checked")
                .that(classStatusFilterSubPage.isClassStatusSelected(getClassStatuses().get(4)))
                .isFalse();

        //AC: 4
        classStatusFilterSubPage.clickClearButton();
        assert_().withFailureMessage("All Check boxes are unchecked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //AC: 4
        classStatusFilterSubPage.clickSelectAllButton();
        assert_().withFailureMessage("All Check boxes are checked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

    }

    private ArrayList<String> getClassStatuses()
    {
        ArrayList<ReferenceDataDto> classStatusDtos =
                new ReferenceDataMap(ASSET, CLASS_STATUSES).getReferenceDataArrayList();
        ArrayList<String> status = new ArrayList<>();
        for (int i = 0; i < classStatusDtos.size(); i++)
        {
            status.add(classStatusDtos.get(i).getName());
        }
        return status;
    }
}
