package test.workhub.searchfilter;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import junit.framework.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.BuildDateFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class BuildDateFilterTest extends BaseTest
{

    @DataProvider(name = "dates")
    public static Object[][] dates()
    {
        return new Object[][]{
                {"01 Feb 2016", "22 Mar 2016"},
                {
                        TestDataHelper.getYesterdayDate(),
                        TestDataHelper.getTodayDate()
                },
                {"29 Feb 2016", "22 Mar 2017"}
        };
    }

    @Test(description = "Story 7.24 - As a user I want to be able to filter by build date"
            + "so that I can find the assets which were built in specific year",
            dataProvider = "dates")
    @Issue("LRT-976")
    public void inputBuildDateTest(String frBuildDate, String toBuildDate)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        BuildDateFilterSubPage buildDateFilterSubPage = searchFilterSubPage.clickBuildDateFilterButton();

        //To Date field cannot be less than From Date field
        buildDateFilterSubPage.setFrBuildDate(toBuildDate);
        buildDateFilterSubPage.setToBuildDate(frBuildDate);
        String toDateBeforeFrDateErrorMessage = "'To' date cannot be before 'From' date";
        if (buildDateFilterSubPage.isToBuildDateBeforeFrBuildDate())
        {
            Assert.assertTrue("Should display To Build Date cannot be before From Build date error message",
                    buildDateFilterSubPage.getToBuildDateInvalidDateMessage().contains(toDateBeforeFrDateErrorMessage));
        }

        // Input From Build Date only
        buildDateFilterSubPage.setFrBuildDate(frBuildDate);
        buildDateFilterSubPage.setToBuildDate("");
        assert_().withFailureMessage("Should be able to search with From Build Date only")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable()).isTrue();

        // Input To Build Date only
        buildDateFilterSubPage.setFrBuildDate("");
        buildDateFilterSubPage.setToBuildDate(toBuildDate);
        assert_().withFailureMessage("Should be able to search with To Build Date only")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable()).isTrue();
    }

    @DataProvider(name = "invalidDates")
    public Object[][] invalidDates()
    {
        return new Object[][]{
                {"AAA", "Please select a valid date."},
                {"E", "Please select a valid date."},
                {"99999", "'From' date cannot be after 'To' date"},
                {"199o", "Please select a valid date."},
                {"!", "Please select a valid date."}
        };
    }

    @Test(description = "Story 7.24 - As a user I want to be able to filter by build date"
            + "so that I can find the assets which were built in specific year",
            dataProvider = "invalidDates"
    )
    @Issue("LRT-976")
    public void inputInvalidBuildDateTest(String invalidDate, String invalidMessage)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        BuildDateFilterSubPage buildDateFilterSubPage = searchFilterSubPage.clickBuildDateFilterButton();

        //Invalid Date
        buildDateFilterSubPage.setFrBuildDate(invalidDate);
        buildDateFilterSubPage.getFrBuildDateInvalidDateMessage();
        Assert.assertTrue("Warning symbol is not display when From Build Date validation failed ", buildDateFilterSubPage.isFrBuildDateInvalid());
        Assert.assertEquals(invalidMessage, buildDateFilterSubPage.getFrBuildDateInvalidDateMessage());

        buildDateFilterSubPage.setToBuildDate(invalidDate);
        Assert.assertTrue("Warning symbol is not display when To Build Date validation failed ", buildDateFilterSubPage.isToBuildDateInvalid());
        Assert.assertEquals(invalidMessage, buildDateFilterSubPage.getToBuildDateInvalidDateMessage());
    }
}







