package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.customer.Customer;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.CustomerFilterSubPage;

import java.util.ArrayList;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.PARTY_FUNCTIONS;
import static model.referencedata.ReferenceDataMap.RefData.PARTY_ROLES;
import static model.referencedata.ReferenceDataMap.Subset.PARTY;

public class CustomerDetailsFilterTest extends BaseTest
{

    @DataProvider(name = "Id")
    public static Object[][] id()
    {
        return new Object[][]{
                {1},
                {2},
                {4}
        };
    }

    @Test(description = "Story 7.27 - As a user I want to be able to filter " +
            "by customer details so that I can find the asset",
            dataProvider = "Id")
    @Issue("LRT-1013")
    public void customerDetailsFilterTest(int id)
    {
        WorkHubPage workHubPage = WorkHubPage.open();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        CustomerFilterSubPage customerFilterSubPage = searchFilterSubPage
                .clickCustomerFilterButton();
        assert_().withFailureMessage("Customer name input is displayed")
                .that(customerFilterSubPage
                        .isCustomerNameInputDisplayed()).isTrue();
        assert_().withFailureMessage("Customer function dropdown is displayed ")
                .that(customerFilterSubPage
                        .isCustomerFunctionDropdownDisplayed()).isTrue();
        assert_().withFailureMessage("Customer role dropdown is displayed").that
                (customerFilterSubPage
                        .isCustomerRoleDropdownDisplayed()).isTrue();

        String customerName = getCustomerNameById(id);
        String customerRole = getCustomerRoleById(id);
        String customerFunction = getCustomerFunctionById(id);

        // AC1, AC2, AC3 - enter partial name
        customerFilterSubPage.setCustomerName(customerName.substring(0, 1));
        assert_().withFailureMessage("Customer name dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerNameDropdownDisplayed()).isTrue();

        //AC2 , AC5, AC6 - enter full name but lowercase
        customerFilterSubPage.setCustomerName(customerName.toLowerCase());
        assert_().withFailureMessage("Customer name validation message is " +
                "displayed ")
                .that(customerFilterSubPage
                        .getCustomerValidationMessage()).isEqualTo("Please enter a valid customer");

        //AC4
        customerFilterSubPage.setCustomerName(customerName.substring(0, 5));
        assert_().withFailureMessage("Customer name dropdown is displayed")
                .that(customerFilterSubPage.isCustomerNameDropdownDisplayed())
                .isTrue();
        customerFilterSubPage.selectFromCustomerNameDropdownByFirstMatch(customerName);
        assert_().withFailureMessage("Customer name validation is not " +
                "displayed")
                .that(customerFilterSubPage.getCustomerValidationMessage())
                .isEqualTo(null);

        //AC7
        customerFilterSubPage.clickCustomerFunctionDropdown();
        assert_().withFailureMessage("Customer function dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerFunctionDropdownDisplayed()).isTrue();

        //AC8
        customerFilterSubPage.clickCustomerRoleDropdown();
        assert_().withFailureMessage("Customer role dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerRoleDropdownDisplayed()).isTrue();

        //AC9
        //invalids
        customerFilterSubPage.setCustomerName(customerName.substring(0, 1));
        assert_().withFailureMessage("Search is blocked and Search Filter " +
                "SubPage is still displayed")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();
        //valids
        customerFilterSubPage.setCustomerName(customerName);
        assert_().withFailureMessage("Search button should be enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();
        searchFilterSubPage.clickAssetSearchApplyButton();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickCustomerFilterButton().then()
                .setCustomerName("").then()
                .setCustomerRoleDropdown(customerRole);

        assert_().withFailureMessage("Search button should be enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();
        searchFilterSubPage.clickAssetSearchApplyButton();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickCustomerFilterButton().then()
                .setCustomerRoleDropdown("Select a Customer role").then()
                .setCustomerFunctionDropdown(customerFunction);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_().withFailureMessage("Search button should be enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();

    }

    private String getCustomerNameById(int id)
    {
        Customer customer = new Customer(id);
        return customer.getDto().getName();
    }

    private String getCustomerRoleById(int id)
    {
        ArrayList<ReferenceDataDto> partyRolesDtos =
                new ReferenceDataMap(PARTY, PARTY_ROLES)
                        .getReferenceDataArrayList();
        String customerRole = partyRolesDtos.stream()
                //getId() returns Long - annoying!
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(dto -> dto.getName())
                .orElse(null);
        return customerRole;
    }

    private String getCustomerFunctionById(int id)
    {
        ArrayList<ReferenceDataDto> partyFunctionDtos =
                new ReferenceDataMap(PARTY, PARTY_FUNCTIONS)
                        .getReferenceDataArrayList();
        String customerFunction = partyFunctionDtos.stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(dto -> dto.getName())
                .orElse(null);
        return customerFunction;
    }
}
