package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.referencedata.ReferenceDataMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.AssetTypeFilterSubPage;

import static com.google.common.truth.Truth.assertThat;
import static model.referencedata.ReferenceDataMap.RefData.ASSET_TYPES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class AssetTypeFilterTest extends BaseTest
{

    @Test(description = "Story 7.20 - As a user, I want to be able to filter by Asset Type so that I can limit the" +
            " search results to only those asset types that I am looking for")
    @Issue("LRT-1127")
    public void resetAssetTypeFilterTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        AssetTypeFilterSubPage assetTypeFilterSubPage = searchFilterSubPage.clickAssetTypeFilterButton();

        assertThat(assetTypeFilterSubPage.getStatcodeOneDropdownValue()).isEqualTo("All");

        AssetTypeDto statcodeOneAssetType = getAssetTypeBasedOnParent(null);
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());

        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeTwoDropdownVisible(),
                "Statcode 2 dropdown wasn't visible after selecting a value in statcode 1."
        );

        //AC 1 & 2
        searchFilterSubPage.clickAssetSearchResetButton();
        assertThat(assetTypeFilterSubPage.getStatcodeOneDropdownValue()).isEqualTo("All");

        //AC 2
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeTwoDropdownVisible(),
                "Statcode 2 dropdown wasn't visible after selecting a value in statcode 1."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeTwoDropdownValue()).isEqualTo("All");

        //AC 2 & AC 6
        AssetTypeDto statcodeTwoAssetType = getAssetTypeBasedOnParent(statcodeOneAssetType.getId());
        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 dropdown wasn't visible after selecting a value in statcode 2."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeThreeDropdownValue()).isEqualTo("All");

        //AC 3 & AC 6
        AssetTypeDto statcodeThreeAssetType = getAssetTypeBasedOnParent(statcodeTwoAssetType.getId());
        assetTypeFilterSubPage.setStatcodeThreeDropdown(statcodeThreeAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeFourDropdownVisible(),
                "Statcode 4 dropdown wasn't visible after selecting a value in statcode 3."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeFourDropdownValue()).isEqualTo("All");

        //AC 1 & 2
        searchFilterSubPage.clickAssetSearchResetButton();

        assertThat(assetTypeFilterSubPage.getStatcodeOneDropdownValue()).isEqualTo("All");

        //AC 2
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeTwoDropdownVisible(),
                "Statcode 2 dropdown wasn't visible after selecting a value in statcode 1."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeTwoDropdownValue()).isEqualTo("All");

        //AC 2 & AC 6
        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 dropdown wasn't visible after selecting a value in statcode 2."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeThreeDropdownValue()).isEqualTo("All");

        //AC 3 & AC 6
        assetTypeFilterSubPage.setStatcodeThreeDropdown(statcodeThreeAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeFourDropdownVisible(),
                "Statcode 4 dropdown wasn't visible after selecting a value in statcode 3."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeFourDropdownValue()).isEqualTo("All");

        //AC 5 & AC 6
        AssetTypeDto statcodeFourAssetType = getAssetTypeBasedOnParent(statcodeThreeAssetType.getId());
        assetTypeFilterSubPage.setStatcodeFourDropdown(statcodeFourAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeFiveDropdownVisible(),
                "Statcode 5 dropdown wasn't visible after selecting a value in statcode 4."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeFiveDropdownValue()).isEqualTo("All");

        //AC 1 & AC 2
        searchFilterSubPage.clickAssetSearchResetButton();

        assertThat(assetTypeFilterSubPage.getStatcodeOneDropdownValue()).isEqualTo("All");
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeTwoDropdownVisible(),
                "Statcode 2 is visible after resetting the form."
        );
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 is visible after resetting the form."
        );
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeFourDropdownVisible(),
                "Statcode 4 is visible after resetting the form."
        );
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeFiveDropdownVisible(),
                "Statcode 5 is visible after resetting the form."
        );
    }

    @Test(
            description = "Story 7.20 - As a user, I want to be able to filter by Asset Type so that I can limit the" +
                    " search results to only those asset types that I am looking for"
    )
    @Issue("LRT-834")
    public void statcodeToAllTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        AssetTypeFilterSubPage assetTypeFilterSubPage = searchFilterSubPage.clickAssetTypeFilterButton();

        AssetTypeDto statcodeOneAssetType = getAssetTypeBasedOnParent(null);

        //AC 2 & AC 6
        assetTypeFilterSubPage.setStatcodeOneDropdown(statcodeOneAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeTwoDropdownVisible(),
                "Statcode 2 dropdown wasn't visible after selecting a value in statcode 1."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeTwoDropdownValue()).isEqualTo("All");

        AssetTypeDto statcodeTwoAssetType = getAssetTypeBasedOnParent(statcodeOneAssetType.getId());
        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 dropdown wasn't visible after selecting a value in statcode 2."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeThreeDropdownValue()).isEqualTo("All");

        AssetTypeDto statcodeThreeAssetType = getAssetTypeBasedOnParent(statcodeTwoAssetType.getId());
        assetTypeFilterSubPage.setStatcodeThreeDropdown(statcodeThreeAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeFourDropdownVisible(),
                "Statcode 4 dropdown wasn't visible after selecting a value in statcode 3."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeFourDropdownValue()).isEqualTo("All");

        //AC 7
        assetTypeFilterSubPage.setStatcodeTwoDropdown("All");
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 dropdown was visible after returning to default value in statcode 2."
        );
        Assert.assertFalse(
                assetTypeFilterSubPage.isStatcodeFourDropdownVisible(),
                "Statcode 4 dropdown was visible after returning to default value in statcode 3."
        );
        assetTypeFilterSubPage.setStatcodeTwoDropdown(statcodeTwoAssetType.getName());
        Assert.assertTrue(
                assetTypeFilterSubPage.isStatcodeThreeDropdownVisible(),
                "Statcode 3 dropdown wasn't visible after selecting a value in statcode 2."
        );
        assertThat(assetTypeFilterSubPage.getStatcodeThreeDropdownValue()).isEqualTo("All");
    }

    private AssetTypeDto getAssetTypeBasedOnParent(Long parentId)
    {
        ReferenceDataMap assetTypes = new ReferenceDataMap(ASSET, ASSET_TYPES);
        for (Object assetTypeDto : assetTypes.getReferenceDataAsClass(AssetTypeDto.class))
        {
            // do not change to .equals under any circumstances!
            if (parentId == ((AssetTypeDto) assetTypeDto).getParentId())
            {
                return (AssetTypeDto) assetTypeDto;
            }
        }
        return null;
    }

}
