package test.workhub.searchfilter;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.AssetQuery;
import model.builder.Builder;
import model.customer.Customer;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.*;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;
import static model.referencedata.ReferenceDataMap.Subset.PARTY;

public class FilterAssetsMyWorkAllAssetsDashboardTest extends BaseTest
{

    private final String[] expectedLifecycleStatusList = {
            "Under construction",
            "Build suspended",
            "In service",
            "Laid up",
            "Under repair",
            "In casualty",
            "Converting",
            "To be broken up",
            "Decommisioned",
            "To be handed over",
            "Cancelled"};

    private final String[] expectedClassStatusList = {
            "Class Contemplated",
            "In Class (Pending Class Exec Approval)",
            "In Class",
            "In Class (Laid Up)",
            "Class Suspended",
            "Cancelled",
            "Class Withdrawn(Disclassed)"};

    @Test(description = "Story 7.35 - As a user I want to select values for " +
            "filter so that I can view relevant Assets on My Work and All " +
            "Assets dashboard")
    @Issue("LRT-916")
    public void filterByLifeCycleStatusTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage
                = searchFilterSubPage.clickLifecycleStatusFilterButton();

        //check that FE has expected LifeCycleStatusList
        ArrayList<String> lifecycleStatusesText = getLifecycleStatuses();
        assert_()
                .withFailureMessage(("Life Cycle status should have these values: " +
                        Arrays.asList(expectedLifecycleStatusList)))
                .that(lifecycleStatusesText)
                .isEqualTo(Arrays.asList(expectedLifecycleStatusList));

        //Step: 3
        //AC: 1 & 2
        assert_().withFailureMessage(lifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(0)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(1)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(2)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(3)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(4)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(5) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(5)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage.isLifecycleStatusSelected(lifecycleStatusesText.get(6)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(7)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(8) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(8)))
                .isFalse();

        assert_().withFailureMessage(lifecycleStatusesText.get(9) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(9)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(10) + " lifecycleStatus is unchecked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(10)))
                .isFalse();

        //Step: 4
        //AC: 3
        lifecycleStatusFilterSubPage
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(0))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(1))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(2))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(3))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(4));

        assert_().withFailureMessage(lifecycleStatusesText.get(0) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(0)))
                .isFalse();

        assert_().withFailureMessage(lifecycleStatusesText.get(1) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(1)))
                .isFalse();

        assert_().withFailureMessage(lifecycleStatusesText.get(2) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(2)))
                .isFalse();

        assert_().withFailureMessage(lifecycleStatusesText.get(3) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(3)))
                .isFalse();

        assert_().withFailureMessage(lifecycleStatusesText.get(4) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(4)))
                .isFalse();

        //Step: 5
        //AC: 4
        lifecycleStatusFilterSubPage.clickSelectAllButton();

        assert_().withFailureMessage("All Check boxes are checked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //Step: 6
        //AC: 5
        lifecycleStatusFilterSubPage.clickClearButton();

        assert_().withFailureMessage("All Check boxes are checked").
                that(lifecycleStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //Step: 3
        //AC: 3
        lifecycleStatusFilterSubPage
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(6))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(7))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(8))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(9))
                .then()
                .deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(10));

        assert_().withFailureMessage(lifecycleStatusesText.get(6) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(6)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(7) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(7)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(8) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(8)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(9) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(9)))
                .isTrue();

        assert_().withFailureMessage(lifecycleStatusesText.get(10) + " lifecycleStatus is checked")
                .that(lifecycleStatusFilterSubPage
                        .isLifecycleStatusSelected(lifecycleStatusesText.get(10)))
                .isTrue();
    }

    @Test(description = "US7.35 AC:Story 7.22 - User is able to filter Class" +
            " Status for search results restrictions")
    @Issue("LRT-921")
    public void filterByClassStatusTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        //Step: 3
        //AC: 1 and 2
        ClassStatusFilterSubPage classStatusFilterSubPage = searchFilterSubPage
                .clickClassStatusFilterButton();

        //check that FE has expected ClassStatusList
        ArrayList<String> classStatusesText = getClassStatuses();
        assert_()
                .withFailureMessage(("Life Cycle status should have these values: " +
                        Arrays.asList(expectedClassStatusList)))
                .that(classStatusesText)
                .isEqualTo(Arrays.asList(expectedClassStatusList));

        assert_().withFailureMessage("All Check boxes are checked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //Step: 4
        //AC: 3
        classStatusFilterSubPage
                .deSelectClassStatusesCheckBoxes(classStatusesText.get(0))
                .then()
                .deSelectClassStatusesCheckBoxes(classStatusesText.get(1))
                .then()
                .deSelectClassStatusesCheckBoxes(classStatusesText.get(2))
                .then()
                .deSelectClassStatusesCheckBoxes(classStatusesText.get(3))
                .then()
                .deSelectClassStatusesCheckBoxes(classStatusesText.get(4));

        assert_().withFailureMessage(classStatusesText.get(0) + " Class Status is checked")
                .that(classStatusFilterSubPage
                        .isClassStatusSelected(classStatusesText.get(0)))
                .isFalse();

        assert_().withFailureMessage(classStatusesText.get(1) + " Class Status is checked")
                .that(classStatusFilterSubPage
                        .isClassStatusSelected(classStatusesText.get(1)))
                .isFalse();

        assert_().withFailureMessage(classStatusesText.get(2) + " Class Status is checked")
                .that(classStatusFilterSubPage
                        .isClassStatusSelected(classStatusesText.get(2)))
                .isFalse();

        assert_().withFailureMessage(classStatusesText.get(3) + " Class Status is checked")
                .that(classStatusFilterSubPage
                        .isClassStatusSelected(classStatusesText.get(3)))
                .isFalse();

        assert_().withFailureMessage(classStatusesText.get(4) + " Class Status is checked")
                .that(classStatusFilterSubPage
                        .isClassStatusSelected(classStatusesText.get(4)))
                .isFalse();

        //Step: 5
        //AC: 4
        classStatusFilterSubPage.clickClearButton();
        assert_().withFailureMessage("All Check boxes are unchecked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(true);

        //AC: 4
        classStatusFilterSubPage.clickSelectAllButton();
        assert_().withFailureMessage("All Check boxes are checked").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

    }

    @Test(description = "US7.35 AC:Story 7.24 - User is able to filter by" +
            " build date to find assets from a specific year.")
    @Issue("LRT-923")
    public void filterByBuildDateTest() throws ParseException
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        //Step: 2
        BuildDateFilterSubPage buildDateFilterSubPage = searchFilterSubPage
                .clickBuildDateFilterButton();

        //Step: 3 and Step: 4
        //AC: 1 and AC: 3
        buildDateFilterSubPage.setFrBuildDate("01 Feb 2016")
                .then().setToBuildDate(TestDataHelper.getTodayDate());

        String toDateBeforeFrDateErrorMessage = "'To' date cannot be before 'From' date";
        if (buildDateFilterSubPage.isToBuildDateBeforeFrBuildDate())
        {
            assert_().withFailureMessage("Should display To Build Date cannot " +
                    "be before From Build date error message")
                    .that(buildDateFilterSubPage.getToBuildDateInvalidDateMessage())
                    .contains(toDateBeforeFrDateErrorMessage);
        }

        //Step: 5
        // AC: 2
        buildDateFilterSubPage.setFrBuildDate("")
                .then().setToBuildDate(TestDataHelper.getTodayDate());

        assert_().withFailureMessage("Should be able to search with To" +
                " Build Date only")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();

        searchFilterSubPage.clickAssetSearchApplyButton();

        // AC: 6
        workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickBuildDateFilterButton();

        buildDateFilterSubPage.setFrBuildDate(TestDataHelper.getYesterdayDate())
                .then().setToBuildDate("");

        assert_().withFailureMessage("Should be able to search with From " +
                "Build Date only")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();

        searchFilterSubPage.clickAssetSearchApplyButton();

        //Step:7, 8 and 9
        //AC: 4 and AC: 5
        workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickBuildDateFilterButton();

        buildDateFilterSubPage.setFrBuildDate("99999!AAEo")
                .then().getFrBuildDateInvalidDateMessage();

        assert_().withFailureMessage("Warning symbol is not display" +
                " when From Build Date validation failed ")
                .that(buildDateFilterSubPage.isFrBuildDateInvalid())
                .isTrue();
        assert_().withFailureMessage("Error Message is Displayed")
                .that(buildDateFilterSubPage.getFrBuildDateInvalidDateMessage())
                .isEqualTo("Please select a valid date.");
        assert_().withFailureMessage("Search Button is not Cickable")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 10
        //AC: 6
        buildDateFilterSubPage.setFrBuildDate(TestDataHelper.getYesterdayDate())
                .then().setToBuildDate("");

        searchFilterSubPage.clickAssetSearchApplyButton();

        LocalDate fromData = TestDataHelper.toLocalDate(FRONTEND_TIME_FORMAT.parse(TestDataHelper.getYesterdayDate()));

        assert_().withFailureMessage("Build Date FE equals Build Date BE")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(getAssetsByFromBuildDate(fromData).getDto().getContent().size());

        //Step: 11
        //AC: 7
        workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickBuildDateFilterButton();

        buildDateFilterSubPage.setFrBuildDate("")
                .then().setToBuildDate(TestDataHelper.getTodayDate());

        searchFilterSubPage.clickAssetSearchApplyButton();

        LocalDate toData = TestDataHelper.toLocalDate(FRONTEND_TIME_FORMAT.parse(TestDataHelper.getTodayDate()));

        assert_().withFailureMessage("Build Date FE equals Build Date BE")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo(getAssetsByToBuildDate(toData).getDto().getContent().size());

    }

    @Test(description = "US7.35 AC:Story 7.25 - User is able to filter by flag name.")
    @Issue("LRT-935")
    public void searchByFlagTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        ////Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        FlagSearchSubPage flagSearchSubPage = searchFilterSubPage.clickFlagFilterButton();

        //Step: 4
        //AC: 1 and AC: 4
        String flag = TestDataHelper.getFlagCodeFromSearch();
        String flagName = flag.substring(0, 3);
        flagSearchSubPage.setFlagCode(flagName).then().selectFlagFromDropdown(flag);
        searchFilterSubPage.clickAssetSearchApplyButton();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        List<AllAssetsAssetElement> cards = allAssetsSubPage.getAssetCards();
        List<String> flagNameFE = cards.stream()
                .map(e -> e.getFlag())
                .collect((Collectors.toList()));
        assert_().withFailureMessage("Flag Name is not present or is incorrect")
                .that(flagNameFE.get(0))
                .isEqualTo(flag);

        //Step: 5
        //AC:2 and AC: 6
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickFlagFilterButton();
        flag = TestDataHelper.getFlagCodeFromSearch();
        flagSearchSubPage.setFlagCode(flag);
        assert_().withFailureMessage("Search Button is not Cickable")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 6
        //AC: 2
        flag = TestDataHelper.getFlagCodeFromSearch();
        flagName = flag.substring(0, 3);
        flagSearchSubPage.setFlagCode(flagName).then().selectFlagFromDropdown(flag);
        searchFilterSubPage.clickAssetSearchApplyButton();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        cards = allAssetsSubPage.getAssetCards();
        flagNameFE = cards.stream().map(e -> e.getFlag()).collect((Collectors.toList()));
        assert_().withFailureMessage("Flag Name is present").that(flagNameFE.get(0)).isEqualTo(flag);

        //AC: 5
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickFlagFilterButton();
        flagSearchSubPage.setFlagCode("Test Negative Behaviour");
        assert_().withFailureMessage("Error Icon is displayed")
                .that(flagSearchSubPage.isIconDisplayed(FlagSearchSubPage.type.ERROR))
                .isTrue();
        assert_().withFailureMessage("Search Button is not Cickable")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 7
        //AC:3
        flagSearchSubPage.clearFlagText();
        assert_().withFailureMessage("Flag Text Box is Clear")
                .that(flagSearchSubPage.getFlagCodeText())
                .isEqualTo("");

        //Step: 8
        //AC:3
        flag = TestDataHelper.getFlagCodeFromSearch();
        flagName = flag.substring(0, 3);
        flagSearchSubPage.setFlagCode(flagName).then().selectFlagFromDropdown(flag);
        searchFilterSubPage.clickAssetSearchApplyButton();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        cards = allAssetsSubPage.getAssetCards();
        flagNameFE = cards.stream().map(e -> e.getFlag()).collect((Collectors.toList()));
        assert_().withFailureMessage("Flag Name is present")
                .that(flagNameFE.get(0))
                .isEqualTo(flag);

    }

    @Test(description = "US7.35 AC:Story 7.26 - User is able to filter " +
            "by build gross tonnage to find assets.")
    @Issue("LRT-936")
    public void filterByGrossTonnageTest() throws ParseException
    {
        final String MAX_CHARACTERS = "7";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        //Step: 4
        //AC: 1, AC: 2, AC: 4 AC: 6 and AC: 7
        List<Integer> grtBackend = getGrossTonnage();
        GrossTonnageSearchSubPage tonnageSearchSubPage = searchFilterSubPage
                .clickGrossTonnageFilterButton();

        tonnageSearchSubPage.setMinGrossTonnage(String.valueOf(grtBackend.get(0)));

        assert_().withFailureMessage("The error message is not displayed")
                .that(tonnageSearchSubPage.isGrossTonnageMaxErrorDisplayed())
                .isFalse();

        searchFilterSubPage.clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        List<AllAssetsAssetElement> cards = allAssetsSubPage.getAssetCards();

        List<Integer> grossTonnage = cards.
                stream()
                .map(e -> Integer.parseInt(e.getGrossTonnage()))
                .collect((Collectors.toList()));

        for (Integer aGrossTonnage2 : grossTonnage)
        {
            assert_().withFailureMessage("Assets Displayed have GRT greater than" +
                    " " + grtBackend.get(0))
                    .that(aGrossTonnage2 >= grtBackend.get(0))
                    .isTrue();
        }
        assert_().withFailureMessage("GRT FE equals GRT BE")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo((getGrossTonnage()
                        .stream()
                        .filter(e -> e >= grtBackend.get(0))
                        .collect((Collectors.toList())))
                        .size());

        //Step: 5
        //AC: 1, AC: 2, AC: 6 and AC: 7
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickGrossTonnageFilterButton();
        tonnageSearchSubPage.setMinGrossTonnage("10000000");

        assert_().withFailureMessage("Data Limit is 7 Digits")
                .that(tonnageSearchSubPage.getDataLengthOfMinGrossTonnage())
                .isEqualTo(MAX_CHARACTERS);

        //Step: 6
        //AC: 1, AC: 2, AC: 4 AC: 6 and AC: 7
        tonnageSearchSubPage.clearMinGrossTonnageTextBox()
                .then().setMaxGrossTonnage(String.valueOf(grtBackend.get(3)));

        assert_().withFailureMessage("The error message is not displayed")
                .that(tonnageSearchSubPage.isGrossTonnageMinErrorDisplayed())
                .isFalse();

        searchFilterSubPage.clickAssetSearchApplyButton();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        cards = allAssetsSubPage.getAssetCards();

        grossTonnage = cards.
                stream()
                .map(e -> Integer.parseInt(e.getGrossTonnage()))
                .collect((Collectors.toList()));

        for (Integer aGrossTonnage1 : grossTonnage)
        {
            assert_().withFailureMessage("Assets Displayed have GRT greater than" +
                    " " + grtBackend.get(3))
                    .that(aGrossTonnage1 <= grtBackend.get(3))
                    .isTrue();
        }
        assert_().withFailureMessage("GRT FE equals GRT BE")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo((getGrossTonnage()
                        .stream()
                        .filter(e -> e <= grtBackend.get(3))
                        .collect((Collectors.toList())))
                        .size());

        //Step: 7
        //AC: 1, AC: 2, AC: 6 and AC: 7
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickGrossTonnageFilterButton();
        tonnageSearchSubPage.setMaxGrossTonnage("10000000");

        assert_().withFailureMessage("Data Limit is 7 Digits")
                .that(tonnageSearchSubPage.getDataLengthOfMaxGrossTonnage())
                .isEqualTo(MAX_CHARACTERS);

        //Step: 8
        //AC: 1 and AC: 5
        tonnageSearchSubPage.clearMaxGrossTonnageTextBox()
                .then().setMinGrossTonnage("1000000")
                .then().setMaxGrossTonnage("7777777");

        assert_().withFailureMessage("Search Button is Cickable")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isTrue();

        //Step: 9
        //AC: 1, AC: 5 and AC: 9
        tonnageSearchSubPage.clearMaxGrossTonnageTextBox()
                .then().setMinGrossTonnage("7777777")
                .then().setMaxGrossTonnage("1000000");

        assert_().withFailureMessage("the error message is not displayed")
                .that(tonnageSearchSubPage.isGrossTonnageMinErrorDisplayed())
                .isTrue();
        assert_().withFailureMessage("Search Button is Cickable")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 10
        //AC: 8
        tonnageSearchSubPage.clearMaxGrossTonnageTextBox()
                .then().setMinGrossTonnage(String.valueOf(grtBackend.get(10)))
                .then().setMaxGrossTonnage(String.valueOf(grtBackend.get(10)));
        searchFilterSubPage.clickAssetSearchApplyButton();

        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        cards = allAssetsSubPage.getAssetCards();

        grossTonnage = cards.
                stream()
                .map(e -> Integer.parseInt(e.getGrossTonnage()))
                .collect((Collectors.toList()));

        for (Integer aGrossTonnage : grossTonnage)
        {
            assert_().withFailureMessage("Assets Displayed")
                    .that(aGrossTonnage)
                    .isEqualTo(grtBackend.get(10));
        }
        assert_().withFailureMessage("GRT FE equals GRT BE")
                .that(workHubPage.getPaginationCountPage().getTotalAmount())
                .isEqualTo((getGrossTonnage()
                        .stream()
                        .filter(e -> e.equals(grtBackend.get(10)))
                        .collect((Collectors.toList())))
                        .size());
    }

    @Test(description = "US7.35 AC:Story 7.27- User is able to filter by " +
            "customer details and find assets.")
    @Issue("LRT-937")
    public void customerDetailsFilterTest()
    {
        int id = 39;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        //Step: 2
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        //Step: 3
        CustomerFilterSubPage customerFilterSubPage = searchFilterSubPage
                .clickCustomerFilterButton();
        assert_().withFailureMessage("Customer name input is displayed")
                .that(customerFilterSubPage
                        .isCustomerNameInputDisplayed()).isTrue();
        assert_().withFailureMessage("Customer function dropdown is displayed ")
                .that(customerFilterSubPage
                        .isCustomerFunctionDropdownDisplayed()).isTrue();
        assert_().withFailureMessage("Customer role dropdown is displayed").that
                (customerFilterSubPage
                        .isCustomerRoleDropdownDisplayed()).isTrue();

        String customerName = getCustomerNameById(id);
        String customerRole = getCustomerRoleById(id);
        String customerFunction = getCustomerFunctionById(id);

        //Step: 4
        // AC1, AC2, AC3 - enter partial name
        customerFilterSubPage.setCustomerName(customerName.substring(0, 1));
        assert_().withFailureMessage("Customer name dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerNameDropdownDisplayed()).isTrue();

        //Step: 5
        //AC2 , AC5, AC6 - enter full name but lowercase
        customerFilterSubPage.setCustomerName(customerName.toLowerCase());
        assert_().withFailureMessage("Customer name validation message is " +
                "displayed ")
                .that(customerFilterSubPage
                        .getCustomerValidationMessage())
                .isEqualTo("Please enter a valid customer");

        customerFilterSubPage.setCustomerName(customerName.substring(0, 5));
        assert_().withFailureMessage("Customer name dropdown is displayed")
                .that(customerFilterSubPage.isCustomerNameDropdownDisplayed())
                .isTrue();
        customerFilterSubPage.selectFromCustomerNameDropdownByFirstMatch(customerName);
        assert_().withFailureMessage("Customer name validation is not " +
                "displayed")
                .that(customerFilterSubPage.getCustomerValidationMessage())
                .isEqualTo(null);

        //Step: 6
        //AC7
        customerFilterSubPage.clickCustomerFunctionDropdown();
        assert_().withFailureMessage("Customer function dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerFunctionDropdownDisplayed()).isTrue();

        //Step: 7
        //AC8
        customerFilterSubPage.clickCustomerRoleDropdown();
        assert_().withFailureMessage("Customer role dropdown is displayed")
                .that(customerFilterSubPage
                        .isCustomerRoleDropdownDisplayed()).isTrue();

        //Step: 8
        //AC9
        //Invalid Data
        customerFilterSubPage.setCustomerName(customerName.substring(0, 1));
        assert_().withFailureMessage("Search is blocked and Search Filter " +
                "SubPage is still displayed")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Valid Data
        customerFilterSubPage.setCustomerName(customerName);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_().withFailureMessage("Search is enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isSearchFilterSubPageContainerDisplayed())
                .isFalse();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickCustomerFilterButton().then()
                .setCustomerName("").then()
                .setCustomerRoleDropdown(customerRole);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_().withFailureMessage("Search is enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isSearchFilterSubPageContainerDisplayed())
                .isFalse();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickCustomerFilterButton().then()
                .setCustomerRoleDropdown("Select a Customer role").then()
                .setCustomerFunctionDropdown(customerFunction);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assert_().withFailureMessage("Search is enabled and Search Filter " +
                "SubPage closes after search")
                .that(searchFilterSubPage.isSearchFilterSubPageContainerDisplayed())
                .isFalse();
    }

    @Test(description = "US7.35 AC:Story 7.23 - User is able to search by" +
            " a builder and yard number to find built assets"
    )
    @Issue("LRT-922")
    public void buildAndYardNumberFilterTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        //Step: 2
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        BuildLocationFilterSubPage buildLocationFilterSubPage = searchFilterSubPage
                .clickBuildLocationFilterButton();

        //Step: 3
        //AC: 1
        assert_().withFailureMessage("Customer name input is not displayed")
                .that(buildLocationFilterSubPage
                        .isPrimaryBuilderInputDisplayed()).isTrue();
        assert_().withFailureMessage("Customer function dropdown is not displayed ")
                .that(buildLocationFilterSubPage
                        .isYardNumberInputDisplayed()).isTrue();

        //Step: 4
        //AC: 2 and AC: 5
        Builder builder = new Builder(1);
        String primaryBuilderName = builder.getDto().getName();
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 1));
        assert_().withFailureMessage("Customer name dropdown is not displayed")
                .that(buildLocationFilterSubPage
                        .isPrimaryBuilderDropdownDisplayed()).isTrue();

        //Step: 5
        //AC: 4
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 5));
        assert_().withFailureMessage("Customer name dropdown is not displayed")
                .that(buildLocationFilterSubPage.isPrimaryBuilderDropdownDisplayed())
                .isTrue();
        buildLocationFilterSubPage.selectFromPrimaryBuilderDropdownByFirstMatch(primaryBuilderName);
        assert_().withFailureMessage("Primary Builder validation is not " +
                "displayed")
                .that(buildLocationFilterSubPage.getPrimaryBuilderValidationMessage())
                .isEqualTo(null);

        //Step: 6
        //AC: 4
        //Invalid Data
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 1));
        assert_().withFailureMessage("Search is blocked and Search Filter " +
                "SubPage is still displayed")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 7
        //AC: 3
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 5))
                .then().selectFromPrimaryBuilderDropdownByFirstMatch(primaryBuilderName)
                .then().setYardNumberSearchInput("***");

        workHubPage.clickAllAssetsTab();

        //Step: 8
        //AC: 6
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 5))
                .then().selectFromPrimaryBuilderDropdownByFirstMatch(primaryBuilderName)
                .then().clearPrimaryBuilderSearchInput();
        assert_().withFailureMessage("Primary Builder Search is clear")
                .that(buildLocationFilterSubPage.getPrimaryBuilderText())
                .isEqualTo("");

        //Step: 9
        //AC: 7
        buildLocationFilterSubPage.setYardNumberSearchInput("1")
                .then().clearYardNumberSearchInput();
        assert_().withFailureMessage("Yard Number Search is clear")
                .that(buildLocationFilterSubPage.getYardNumberSarchText())
                .isEqualTo("");

        //Step: 10
        //AC: 8 and AC: 9
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput(primaryBuilderName.substring(0, 5))
                .then().selectFromPrimaryBuilderDropdownByFirstMatch(primaryBuilderName)
                .then().setYardNumberSearchInput("This is to test Error message");
        assert_().withFailureMessage("Yard Number Error Message Is shown")
                .that(buildLocationFilterSubPage.getYardNumberValidationMessage())
                .isEqualTo("The yard number cannot be more than 10 characters");
        assert_().withFailureMessage("Search is blocked and cannot be clicked ")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

        //Step: 11
        //AC: 8 and AC: 9
        buildLocationFilterSubPage.setPrimaryBuilderSearchInput("This is to test Error message")
                .then().setYardNumberSearchInput("1");
        assert_().withFailureMessage("Primary Builder Error Message Is shown")
                .that(buildLocationFilterSubPage.getPrimaryBuilderValidationMessage())
                .isEqualTo("Please enter a valid builder");
        assert_().withFailureMessage("Search is blocked and cannot be clicked ")
                .that(searchFilterSubPage.isAssetSearchApplyButtonClickable())
                .isFalse();

    }

    private ArrayList<String> getLifecycleStatuses()
    {
        ArrayList<ReferenceDataDto> lifecycleStatusDtos =
                new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES).getReferenceDataArrayList();
        ArrayList<String> status = new ArrayList<>();
        for (ReferenceDataDto lifecycleStatusDto : lifecycleStatusDtos)
        {
            status.add(lifecycleStatusDto.getName());
        }
        return status;
    }

    private ArrayList<String> getClassStatuses()
    {
        ArrayList<ReferenceDataDto> classStatusDtos =
                new ReferenceDataMap(ASSET, CLASS_STATUSES).getReferenceDataArrayList();
        ArrayList<String> status = new ArrayList<>();
        for (ReferenceDataDto classStatusDto : classStatusDtos)
        {
            status.add(classStatusDto.getName());
        }
        return status;
    }

    private AssetQuery getAssetsByFromBuildDate(LocalDate fromDate)
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 10L));
        assetQueryDto.setBuildDateMin(Date.from(fromDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        return new AssetQuery(assetQueryDto);
    }

    private AssetQuery getAssetsByToBuildDate(LocalDate toDate)
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 10L));
        assetQueryDto.setBuildDateMax(Date.from(toDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        return new AssetQuery(assetQueryDto);
    }

    private List<Integer> getGrossTonnage()
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setClassStatusId(Arrays.asList(1L, 2L, 3L));
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        List<Double> grossTonnage = assetQuery.getDto()
                .getContent()
                .stream().filter(dto -> dto.getGrossTonnage() != null)
                .map(e -> e.getGrossTonnage()).collect(Collectors.toList());
        List<Integer> grossTonnageInt = new ArrayList<>();
        for (Double aGrossTonnage : grossTonnage)
        {
            grossTonnageInt.add(aGrossTonnage.intValue());
        }
        return grossTonnageInt;
    }

    private String getCustomerNameById(int id)
    {
        Customer customer = new Customer(id);
        return customer.getDto().getName();
    }

    private String getCustomerRoleById(int id)
    {
        ArrayList<ReferenceDataDto> partyRolesDtos =
                new ReferenceDataMap(PARTY, PARTY_ROLES)
                        .getReferenceDataArrayList();
        return partyRolesDtos.stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(dto -> dto.getName())
                .orElse(null);
    }

    private String getCustomerFunctionById(int id)
    {
        ArrayList<ReferenceDataDto> partyFunctionDtos =
                new ReferenceDataMap(PARTY, PARTY_FUNCTIONS)
                        .getReferenceDataArrayList();
        return partyFunctionDtos.stream()
                .filter(dto -> dto.getId().equals(Long.valueOf(id)))
                .findFirst()
                .map(dto -> dto.getName())
                .orElse(null);
    }

}
