package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import helper.TestDataHelper;
import model.employee.Employee;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;

import static com.google.common.truth.Truth.assertThat;
import static constant.LloydsRegister.DEFAULT_EMPLOYEE;

public class ViewMastHeaderTest extends BaseTest
{

    private WorkHubPage workHubPage;

    @DataProvider(name = "Name")
    public static Object[][] name()
    {
        return new Object[][]{
                {TestDataHelper.getEmployeeWithJobsOrCases(false), "All assets"},
                {TestDataHelper.getEmployeeWithJobsOrCases(true), "My work"}
        };
    }

    @Test(description = "Automation - Story 7.28 - As a logged in user who is online I want to be able " +
            "to see the header of the system so that I can interact with it",
            dataProvider = "Name")
    @Issue("LRT-876")
    public void viewMastHeaderTest(String employee, String activeTab)
    {
        workHubPage = WorkHubPage.open();
        ConfigurationPage configPage = workHubPage.getHeader().clickConfigPage();
        configPage.setMastUserFullName(employee);
        configPage.clickSaveButton();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        workHubPage.clickMyWorkTab();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        workHubPage.clickMyWorkTab();
        workHubPage.getHeader().clickMastText();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        AllAssetsSubPage assetsPage = workHubPage.clickAllAssetsTab();
        assetsPage.getAssetCardByIndex(0).clickViewAssetButton();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        workHubPage.clickAllAssetsTab();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        workHubPage.clickAddNewAsset();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);

        workHubPage.clickBatchActions();
        workHubPage.getHeader().clickMastLogo();
        assertThat(workHubPage.getActiveTab()).isEqualTo(activeTab);
    }

    @AfterMethod(alwaysRun = true)
    public void setUserToDefault()
    {
        Employee employee = new Employee((int) DEFAULT_EMPLOYEE);
        ConfigurationPage configPage = workHubPage.getHeader().clickConfigPage();
        configPage.setMastUserFullName(employee.getDto().getFirstName() + " " + employee.getDto().getLastName());
        configPage.clickSaveButton();
    }

}
