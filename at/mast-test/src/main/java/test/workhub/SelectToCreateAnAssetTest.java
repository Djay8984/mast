package test.workhub;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.sub.sub.IMORegisteredAssetPage;

import static com.google.common.truth.Truth.assert_;

public class SelectToCreateAnAssetTest extends BaseTest
{

    @Test(description = "US8.11 - AC: User is able to select to create an asset")
    @Issue("LRT-1319")
    public void selectToCreateAnAssetTest()
    {

        //Step: 1 and 2
        WorkHubPage workHubPage = WorkHubPage.open();

        //AC: 1
        //Step: 3
        assert_().withFailureMessage("Add new asset button expected to be clickable")
                .that(workHubPage.isAddNewAssetButtonIsClickable())
                .isTrue();
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        //AC: 2
        assert_().withFailureMessage("IMO registered asset button expected to be selected")
                .that(addNewAssetPage.isIMORegisteredAssetButtonIsSelected())
                .isTrue();

        //Step: 4
        addNewAssetPage.clickNavigateBackButton();

        //AC: 1
        //Step: 5
        workHubPage.clickAllAssetsTab();
        assert_().withFailureMessage("Add new asset button expected to be clickable")
                .that(workHubPage.isAddNewAssetButtonIsClickable())
                .isTrue();
        workHubPage.clickAddNewAsset();
        //AC: 2
        assert_().withFailureMessage("IMO registered asset button expected to be selected")
                .that(addNewAssetPage.isIMORegisteredAssetButtonIsSelected())
                .isTrue();

        //AC: 3
        //Step: 6
        IMORegisteredAssetPage iMORegisteredAssetPage = addNewAssetPage.clickImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("IMO number text box is expected to be blank")
                .that(iMORegisteredAssetPage.getImoNumberText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Asset Name text box is expected to be blank")
                .that(iMORegisteredAssetPage.getAssetNameText().isEmpty())
                .isTrue();

        //AC: 4
        //Step: 7
        addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Non IMO registered asset button expected to be selected")
                .that(addNewAssetPage.isNonIMORegisteredAssetButtonSelected())
                .isTrue();

        //AC: 4
        //Step: 8
        addNewAssetPage.clickImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("IMO registered asset button expected to be selected")
                .that(addNewAssetPage.isIMORegisteredAssetButtonIsSelected())
                .isTrue();
    }
}
