package test.workhub;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.wip.WipCoc;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import viewasset.ViewAssetPage;
import viewasset.assetheaderadditionalinformation.edit.assetdetails.EditAssetDetailsPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage.JobRelatedTable;
import viewasset.sub.jobs.details.conditionofclass.modal.AddCocConfirmationPage;
import workhub.WorkHubPage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UpdateWIPCoCWithStaleDataTest extends BaseTest
{

    private static final String COC_STALENESS_DIALOG_TEXT = "There is a conflict with this condition of class and your changes have not been\n" +
            "saved. You will be sent back to the view mode of this condition of class so you can read\n" +
            "the last saved version";

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

    @Test(description = "Attempt To Update A WIP CoC With Stale Data")
    public void updateWIPCoCWithStaleDataTest() throws ClientProtocolException, IOException
    {
        // Create a brand new asset, case, job, and WIP CoC
        final WorkHubPage workHubPage = WorkHubPage.open();

        final ViewAssetPage viewAssetPage = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset();

        EditAssetDetailsPage editAssetDetailsPage = viewAssetPage
                .clickViewMoreButton()
                .getAssetDetailsPage()
                .clickEditButton()
                .getEditAssetDetailsPage();

        final String newAssetName = editAssetDetailsPage.getNameText();

        editAssetDetailsPage
                .clickAssetHeaderTab()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase();

        final JobRelatedTable jobRelatedTable = viewAssetPage
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickGoToCocsButton()
                .clickAddNewButton()
                .setTitle("title")
                .setDescription("description")
                .setImposedDate(dateFormat.format(DateUtils.addDays(new Date(), 1)))
                .setDueDate(dateFormat.format(DateUtils.addDays(new Date(), 1)))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddCocConfirmationPage.class)
                .clickOkButton()
                .getJobRelatedTable();

        ViewConditionOfClassPage viewConditionOfClassPage = jobRelatedTable.getRows()
                .stream().filter(e -> e.getCocTitle().contains("title"))
                .findFirst()
                .get()
                .clickFurtherDetailsArrow();

        EditConditionOfClassPage editedCocPage = viewConditionOfClassPage
                .clickEditButton()
                .setDescription("New Description");

        // Update the job via the REST api with different data
        final int assetId = TestDataHelper.getAssetIdByName(newAssetName);

        final List<JobDto> jobsByAssetId = TestDataHelper.getJobsByAssetId(Long.valueOf(assetId));
        final JobDto jobDto = jobsByAssetId.get(0);

        final CoCDto cocDtoToUpdate = TestDataHelper.getWipCoCsForJob(jobDto.getId(), "title").get(0);

        cocDtoToUpdate.setDescription("Updated Description");

        new WipCoc(cocDtoToUpdate, jobDto.getId().intValue(), cocDtoToUpdate.getId().intValue());

        // TODO Also blocked by LRD-4616 where the date picker fails to allow an entity to be saved.

        // // Now try to save the stale data in the ui.
        // editedCocPage.clickSaveButtonExpectingStaleness();
        //
        // final StalenessPage stalenessDialog = PageFactory.newInstance(StalenessPage.class);
        // assertThat(stalenessDialog.getHeader()).isEqualTo("Unable to save changes due to conflict");
        // assertThat(stalenessDialog.getBody()).isEqualTo(COC_STALENESS_DIALOG_TEXT);
        //
        // // Ensure the reload updates the page with the data from the rest call.
        // final EditConditionOfClassPage reloadedPage =
        // stalenessDialog.clickReloadButton(EditConditionOfClassPage.class);
        //
        // assertThat(reloadedPage.getDescriptionText()).isEqualTo("Updated Description");
    }

}
