package test.viewasset;

import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import com.google.common.collect.Ordering;
import model.asset.AssetItem;
import model.asset.AssetModel;
import model.asset.AssetQuery;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetElement;
import workhub.sub.MyWorkSubPage;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class NavigateThroughAssetStructureInListViewTest extends BaseTest
{
    private final int defaultItemListSize = 50;
    private final int defaultItemNameLength = 40;

    @Test(
            description = " Story - 8.17 - As a user I want to navigate through the asset structure in the list view, so that I can view the related items"
    )
    @Issue("LRT-1220")
    public void navigateThroughAssetStructureInListView()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();

        //AC8 - Choose the first card and click View asset button -
        MyWorkAssetElement firstCard = myWorkSubPage.getAssetCardByIndex(0);
        String imoNumber = firstCard.getImoNumber();
        ViewAssetPage viewAssetPage = firstCard.clickViewAssetButton();
        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage(); //defaults here
        ListViewSubPage listViewSubPage = assetModelSubPage
                .getListViewSubPage(); //defaults here
        //Get the list of items from FE
        List<ListViewItemElement> assetItems = listViewSubPage.getAssetItems();
        List<String> assetItemsOnFE = assetItems.stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());
        //Get the list of items from BE
        int assetId = getAssetIdByImoNumber(imoNumber);
        List<String> assetItemsOnBE = getItemNamesByAssetId(assetId);
        assert_().withFailureMessage("The asset item list view shows the " +
                "correct items ")
                .that(assetItemsOnFE)
                .isEqualTo(assetItemsOnBE);

        //AC1 AC2 AC8 - Select an entry which has related items and open
        //Just do one level deep - doing exhaustively takes too long
        AssetModel assetModel = new AssetModel(assetId);
        List<ItemDto> childItems = assetModel.getDto().getItems().get(0)
                .getItems();
        //AC1 AC8 - Check if all the child now matches
        for (int i = 0; i < assetItems.size(); i++)
        {
            //Click on a item
            //if clickable check that item has children!
            if (assetItems.get(i).isItemNameClickable())
            {
                listViewSubPage = assetItems.get(i).clickItemName();
                //Gather all the child items on FE that are clickable - ie with
                // child
                List<String> childItemNamesOnFE = listViewSubPage.getAssetItems()
                        .stream()
                        .map(ListViewItemElement::getItemName)
                        .collect(Collectors.toList());
                //Gather all the child items on BE
                List<String> childItemNamesOnBE = childItems.get(i).getItems()
                        .stream()
                        .map(itemDto -> new AssetItem(assetId, itemDto.getId()
                                .intValue()))
                        .map(assetItem -> assetItem.getDto().getName())
                        .collect(Collectors.toList());
                //Compare!
                assert_().withFailureMessage("All child item names matches. FE " +
                        "contains " + childItemNamesOnFE + ". BE contains " + childItemNamesOnBE)
                        .that(childItemNamesOnFE.containsAll(childItemNamesOnBE))
                        .isTrue();
                //return to parent - to loop again
                listViewSubPage.clickBreadcrumbLinkByIndex(0);
            }
            else
            {//AC2 - else not clickable - check that there is no children
                List<String> childItemNotClickable = childItems.get(i)
                        .getItems()
                        .stream()
                        .map(itemDto -> new AssetItem(assetId, itemDto.getId()
                                .intValue()))
                        .map(assetItem -> assetItem.getDto().getName())
                        .collect(Collectors.toList());
                assert_().withFailureMessage("Item " + assetItems.get(i)
                        .getItemName() + "has no children")
                        .that(childItemNotClickable)
                        .isEmpty();
            }
        }

        //AC5 - Check that breadcrumbs is visible and correctly displayed
        //AC6 - Check that user can navigate backwards using breadcrumbs
        //AC7 - Check that user can navigate to first level using breadcrumbs
        //AC11 - Check that option to navigate to item specific page is
        // available for each item
        //AC10 - Check that review complete and status indicator is displayed
        // for each item
        //AC3 - partial test; not in Sprint 8 - Check that item is sorted by
        // child item
        //AC9 - Check that a maximum of 50 items are loaded and Load More
        // Button is displayed
        //AC4 - Check that item names are at most 40 char long and truncated
        crawlThroughItemsRecursive(listViewSubPage, listViewSubPage
                .getBreadcrumbTextByIndex
                        (0));
    }

    /**
     * This is a recursion method.
     * It finds all clickable item and click on them and goes deeper into the
     * breadcrumb tree.
     * It remembers what has been clicked before via currentItemNameFromBreadCrumb
     * and check against the latest updated breadcrumb
     * It checks AC5-AC11 all in one go
     *
     * @param listViewSubPage               - the current page to start with
     * @param currentItemNameFromBreadCrumb - the current breadcrumb text.
     *                                      This will be checked as part of
     *                                      the test when an item is clicked
     *                                      and breadcrumb updated.
     * @return ListViewSubPage
     */
    private ListViewSubPage crawlThroughItemsRecursive(ListViewSubPage listViewSubPage, String
            currentItemNameFromBreadCrumb)
    {
        //On a fresh new page, gather all list of items
        List<ListViewItemElement> assetItems = listViewSubPage.getAssetItems();
        List<String> assetItemNames = assetItems.stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());
        //AC3 - Check that items are ordered
        assert_().withFailureMessage("Items " + assetItemNames + "is " +
                "naturally sorted")
                .that(Ordering.natural().isOrdered(assetItemNames))
                .isTrue();
        //AC4 - check that asset item names are at most 40 char and truncated
        assetItemNames.stream().forEach(s ->
                assert_().withFailureMessage(s + "is " +
                        defaultItemNameLength + " char long")
                        .that(s.length())
                        .isAtMost(defaultItemNameLength)
        );
        //AC9 - Check that max items displayed is 50 and load more button is
        // displayed
        assert_().withFailureMessage("Asset item list is at most " + defaultItemListSize)
                .that(assetItems.size())
                .isAtMost(defaultItemListSize);
        if (assetItems.size() > defaultItemListSize)
        {
            assert_().withFailureMessage("Load more button is displayed")
                    .that(listViewSubPage.isLoadMoreButtonDisplayed())
                    .isTrue();
        }
        //******
        //check all items on the page
        for (int i = 0; i < assetItems.size(); i++)
        {
            //For every item...
            ListViewItemElement assetItem = assetItems.get(i);
            //AC11 - check that further details arrow is displayed
            assert_().withFailureMessage("For " + assetItem.getItemName() +
                    " arrow button for further details is displayed")
                    .that(assetItem.isFurtherDetailsArrowDisplayed())
                    .isTrue();
            //AC10 - check that review complete section and status indicator
            // is displayed
            assert_().withFailureMessage("For " + assetItem.getItemName() +
                    " is reviewed complete displayed")
                    .that(assetItem.isReviewCompleteDisplayed())
                    .isTrue();
            assert_().withFailureMessage("For " + assetItem.getItemName() +
                    " status indicator is displayed")
                    .that(assetItem.isStatusIndicatorDisplayed())
                    .isTrue();
            //If the item is clickable i.e. have children
            if (assetItem.isItemNameClickable())
            {
                String currAssetItemName = assetItem.getItemName();
                //Click on it and do RECURSION MIND BLOW!!! - go deeper!
                listViewSubPage = crawlThroughItemsRecursive(assetItem.clickItemName(), currAssetItemName);
                //AC5 - Check that latest breadcrumb is equal to item we
                // clicked previously
                assert_().withFailureMessage("For " + currentItemNameFromBreadCrumb +
                        " matches the breadcrumb" + listViewSubPage.getBreadcrumbTextByIndex
                        (listViewSubPage.getBreadCrumbTextSize() - 1))
                        .that(currentItemNameFromBreadCrumb)
                        .isEqualTo(listViewSubPage.getBreadcrumbTextByIndex
                                (listViewSubPage.getBreadCrumbTextSize() - 1));
            }
        }
        //AC6 AC7 navigate backwards one depth by breadcrumb
        if (listViewSubPage.getBreadCrumbTextSize() > 1)
        {
            return listViewSubPage.clickBreadcrumbLinkByIndex
                    (listViewSubPage
                            .getBreadCrumbTextSize() - 2);
        }
        else
        {
            //No more children, return null and break recursion
            return null;
        }
    }

    private int getAssetIdByImoNumber(String imoNumber)
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setImoNumber(Collections.singletonList(Long.parseLong(imoNumber)));
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        return assetQuery.getDto().getContent().get(0).getId()
                .intValue();
    }

    private List<String> getItemNamesByAssetId(int assetId)
    {
        AssetModel assetModel = new AssetModel(assetId);
        return
                assetModel.getDto().getItems().get(0).getItems().stream()
                        .map(c -> new AssetItem(assetId, c.getId().intValue()))
                        .map(d -> d.getDto().getName())
                        .collect(Collectors.toList());
    }
}

