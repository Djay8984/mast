package test.viewasset;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetItemAttributePage;
import model.asset.AssetModel;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.SearchWithinAssetModelPage;
import viewasset.sub.assetmodel.element.HierarchyViewItemElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static helper.TestDataHelper.getAttributeTypeNameById;
import static helper.TestDataHelper.getItemNamesByAssetId;

public class SearchWithinAssetModelForItemsTest extends BaseTest
{

    final int assetId = 501;
    final String startingString = "a";
    final String endingString = "p";

    @Test(description = "US8.20 AC:1-6 User is able to search within the asset model.")
    @Issue("LRT-1213")
    public void searchingWithinAssetModelTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement firstCard = allAssetsSubPage.getAssetCardByIndex(0);
        ViewAssetPage viewAssetPage = firstCard.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        //AC: 1
        //Step: 3
        ListViewSubPage listViewSubPage = assetModelSubPage
                .getListViewSubPage();

        List<ListViewItemElement> assetItemsListView =
                listViewSubPage.getAssetItems();
        List<String> assetItemsOnFeListView = assetItemsListView.stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());

        //Get the list of items from BE
        List<String> assetItemsOnBE = getItemNamesByAssetId(assetId);

        assert_().withFailureMessage("The asset item list view is expected to show the " +
                "correct items ")
                .that(assetItemsOnFeListView)
                .isEqualTo(assetItemsOnBE);

        //AC: 1
        //STep: 4
        HierarchyViewSubPage hierarchyViewSubPage =
                assetModelSubPage.clickHierarchyViewButton();
        List<HierarchyViewItemElement> assetItemsHierarchyView =
                hierarchyViewSubPage.getAssetItems();
        List<String> assetItemsOnFeHierarchyView = assetItemsHierarchyView.stream()
                .map(HierarchyViewItemElement::getItemName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("The asset item Hierarchy View is  expected to show the " +
                "correct items ")
                .that(assetItemsOnFeHierarchyView)
                .isEqualTo(assetItemsOnBE);

        //AC: 2
        //Step: 5
        SearchWithinAssetModelPage searchWithinAssetModelPage =
                assetModelSubPage.clickSearchWithinAssetModelButton();

        assert_().withFailureMessage("Item text box is expected to be displayed")
                .that(searchWithinAssetModelPage.isItemTextBoxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Attribute text box is expected to be displayed")
                .that(searchWithinAssetModelPage.isAttributeTextBoxDisplayed())
                .isTrue();
        assert_().withFailureMessage("Attribute value text box is expected to be displayed")
                .that(searchWithinAssetModelPage.isAttributeValueTextBoxDisplayed())
                .isTrue();

        //AC: 3
        //Step: 6
        assert_().withFailureMessage("Results are expected not to be displayed")
                .that(searchWithinAssetModelPage.isSearchResultsDisplayed())
                .isFalse();

        //AC: 4
        //Step: 7
        for (String anAssetItemsOnBE : assetItemsOnBE)
        {
            searchWithinAssetModelPage.
                    setItemSearchInput(anAssetItemsOnBE).then()
                    .clickSearchButton();

            assert_().withFailureMessage("Item " + anAssetItemsOnBE + " is expected to be shown")
                    .that(searchWithinAssetModelPage.getSearchResultText())
                    .isEqualTo(Collections.singletonList(anAssetItemsOnBE));

        }

        //Get Attributes from BE
        AssetModel assetModel = new AssetModel(assetId);
        AssetItemAttributePage assetItemAttributePage =
                new AssetItemAttributePage(assetId, assetModel.getDto()
                        .getItems().get(0).getId().intValue());
        List<Long> attributeTypeID = new ArrayList<>();
        for (int i = 0; i < assetItemAttributePage.getDto().getContent().size(); i++)
        {
            attributeTypeID.add(assetItemAttributePage.getDto()
                    .getContent().get(i).getAttributeType().getId());
        }
        //Get only distinct attribute_type_id
        Set<Long> distinctAttributeId = new HashSet<>(attributeTypeID);
        List<Long> list = new ArrayList<>(distinctAttributeId);

        ArrayList<String> attributeTypeNames = list.stream()
                .map(aList -> getAttributeTypeNameById(aList.intValue()))
                .collect(Collectors.toCollection(ArrayList::new));

        //AC: 4
        //Step: 8
        searchWithinAssetModelPage.setItemSearchInput(assetItemsOnBE.get(0))
                .then().setAttributeSearchInput(attributeTypeNames.get(0))
                .then().clickSearchButton();

        assert_().withFailureMessage("User is expected to use more than one fields to search")
                .that(searchWithinAssetModelPage.getSearchResultText())
                .isEqualTo(Collections.singletonList(assetItemsOnBE.get(0)));

        //Step: 9
        //Ignored: AC: 5 states wildcard (* symbol) to be used before or after character string.

        //AC: 5
        //Step: 10
        searchWithinAssetModelPage.setItemSearchInput("*" + endingString)
                .then().clickSearchButton();
        List<String> resultEndingWith = searchWithinAssetModelPage.getSearchResultText();
        for (String aResultEndingWith : resultEndingWith)
            assert_().withFailureMessage("All the results expected to end with" + endingString)
                    .that(aResultEndingWith.trim().toLowerCase()
                            .endsWith(endingString))
                    .isTrue();

        //AC: 5
        //Step: 11
        searchWithinAssetModelPage.setItemSearchInput(startingString + "*")
                .then().clickSearchButton();
        List<String> resultStartingWith = searchWithinAssetModelPage.getSearchResultText();
        for (String aResultStartingWith : resultStartingWith)
            assert_().withFailureMessage("All the results expected to be start with" + startingString)
                    .that(aResultStartingWith.trim().toLowerCase()
                            .startsWith(startingString))
                    .isTrue();

        //AC: 6
        //Step: 12
        searchWithinAssetModelPage.clickSearchWithinAssetModelButton();
        assert_().withFailureMessage("User is on Asset Model Page and" +
                " is expected to click on Build Asset Model")
                .that(assetModelSubPage.isCheckOutForEditingButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("User is on Asset Model Page and" +
                " is expected to be click on Hierarchy View Button ")
                .that(assetModelSubPage.isHierarchyViewButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("User is on Asset Model Page and" +
                " is expected to be click on List View Button Button ")
                .that(assetModelSubPage.isListViewButtonEnabled())
                .isTrue();

    }

}
