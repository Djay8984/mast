package test.viewasset.serviceschedule;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.DatabaseHelper;
import helper.TestDataHelper;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.serviceschedule.ClassificationSubPage;
import viewasset.sub.serviceschedule.ServiceSchedulePage;
import viewasset.sub.serviceschedule.StatutorySubPage;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.sub.IMORegisteredAssetPage;
import workhub.sub.sub.NonIMORegisteredAssetPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class NavigateToServiceScheduleTest extends BaseTest
{

    private final String[] assetType = {
            "Cargo Carrying",
            "Tankers",
            "Liquefied Gas",
            "LNG Tanker",
            "LNG Tanker"};
    List<String> expectedViewByDropdown = Arrays.asList("Product", "Service Type");
    String assetCategory = "Vessel";
    String buildDate = FRONTEND_TIME_FORMAT.format(TestDataHelper.randomDate());
    String expectedNoticeMessage = "There are no products or services in" +
            " the service schedule for this product family";

    @Test(description = "US 12.1 - IMO Registered Asset- After creating an" +
            " Asset the user is able to navigate to the Service Schedule")
    @Issue("LRT-2325")
    public void imoRegisteredNavigateToServiceScheduleTest() throws SQLException
    {

        String imoAssetName = "IMO Registered Asset KL Auto " + TestDataHelper.getUniqueYardNumber();
        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //This is to get an IMO number which are not associated to any asset
        List<String> imoNumber = getImoNumber();
        List<String> lrNumber = getLrNumber();
        String uniqueImoNumber = lrNumber.stream()
                .filter(e -> !imoNumber.contains(e)).findFirst().get();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        IMORegisteredAssetPage iMORegisteredAssetPage
                = addNewAssetPage.clickImoRegisteredAssetRadioButton();
        iMORegisteredAssetPage
                .setIMONumber(uniqueImoNumber)
                .then().setAssetName(imoAssetName)
                .then().clickAssetCategoryButton()
                .then().selectAssetCategoryByName(assetCategory)
                .then().clickAssetTypeButton()
                .then().setStatcodeOneDropdown(assetType[0])
                .then().setStatcodeTwoDropdown(assetType[1])
                .then().setStatcodeThreeDropdown(assetType[2])
                .then().setStatcodeFourDropdown(assetType[3])
                .then().setStatcodeFiveDropdown(assetType[4])
                .then().clickSelectAssetTypeSelectButton()
                .then().setDateOfBuild(buildDate);

        CasesPage casesPage = addNewAssetPage.clickCreateAssetButton();
        casesPage.clickAssetHeaderLogo();

        //Step: 3
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(imoAssetName).then().clickAssetSearchApplyButton();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(imoAssetName);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //AC: 1
        //Step: 4
        ServiceSchedulePage serviceSchedulePage = viewAssetPage.clickServiceScheduleTab();

        //AC: 2
        //Step: 5
        assert_().withFailureMessage("Classification tab is expected to be selected")
                .that(serviceSchedulePage.isClassificationTabSelected())
                .isTrue();

        //AC: 3
        //Step: 6
        List<String> viewByDropdown = serviceSchedulePage.getViewByDropDownOptions();
        assert_().withFailureMessage("View by drop down options are expected to be displayed")
                .that(serviceSchedulePage.getViewByDropDownOptions()
                        .equals(expectedViewByDropdown))
                .isTrue();

        //AC: 4
        //Step: 7
        ClassificationSubPage classificationSubPage = serviceSchedulePage.clickClassificationTab();
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(classificationSubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 8
        assert_().withFailureMessage("Add products CTA is expected to be displayed")
                .that(classificationSubPage.isAddProductsButtonDisplayed())
                .isTrue();

        //AC: 4
        //Step: 9
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.SERVICE_TYPE);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(classificationSubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 10
        assert_().withFailureMessage("Add products CTA is not expected to be displayed")
                .that(classificationSubPage.isAddProductsButtonDisplayed())
                .isFalse();

        //AC: 4
        //Step: 11
        StatutorySubPage statutorySubPage = serviceSchedulePage.clickStatutoryTab();
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.PRODUCT);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(statutorySubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 12
        assert_().withFailureMessage("Add products CTA is expected to be displayed")
                .that(statutorySubPage.isAddProductsButtonDisplayed())
                .isTrue();

        //AC: 4
        //Step: 13
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.SERVICE_TYPE);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(statutorySubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 14
        assert_().withFailureMessage("Add products CTA is not expected to be displayed")
                .that(statutorySubPage.isAddProductsButtonDisplayed())
                .isFalse();

        //AC: 6
        //Step: 15
        Date date = getHarmonisationDateByName(imoAssetName);
        String harmonizationDate = FRONTEND_TIME_FORMAT.format(date);
        assert_().withFailureMessage("Harmonisation date format is expected to be displayed")
                .that(serviceSchedulePage.getHarmonisationDate())
                .isEqualTo(harmonizationDate);

        //AC: 6 and AC: 7
        //Step: 16 and 17
        serviceSchedulePage.clickEditRuleSetIcon();
    }

    @Test(description = "US 12.1 - Non IMO Registered Asset -  After creating " +
            "an Asset the user is able to navigate to the Service Schedule")
    @Issue("LRT-2326")
    public void nonImoRegisteredNavigateToServiceScheduleTest() throws SQLException
    {

        String builderName = RandomStringUtils.randomAlphabetic(10);
        String yardNumber = String.valueOf(RandomStringUtils.randomNumeric(9));
        String nonImoAssetName = "AutoKL " + RandomStringUtils.randomNumeric(10);
        String buildDate = FRONTEND_TIME_FORMAT.format(TestDataHelper.randomDate());
        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage
                = addNewAssetPage.clickNonImoRegisteredAssetRadioButton();

        nonIMORegisteredAssetPage
                .setBuilderName(builderName)
                .then().setYardNumber(yardNumber)
                .then().setAssetName(nonImoAssetName)
                .then().clickAssetCategoryButton()
                .then().selectAssetCategoryByName(assetCategory)
                .then().clickAssetTypeButton()
                .then().setStatcodeOneDropdown(assetType[0])
                .then().setStatcodeTwoDropdown(assetType[1])
                .then().setStatcodeThreeDropdown(assetType[2])
                .then().setStatcodeFourDropdown(assetType[3])
                .then().setStatcodeFiveDropdown(assetType[4])
                .then().clickSelectAssetTypeSelectButton()
                .then().setDateOfBuild(buildDate);

        CasesPage casesPage = addNewAssetPage.clickCreateAssetButton();
        casesPage.clickAssetHeaderLogo();

        //Step: 3
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(nonImoAssetName).then().clickAssetSearchApplyButton();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(nonImoAssetName);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //AC: 1
        //Step: 4
        ServiceSchedulePage serviceSchedulePage = viewAssetPage.clickServiceScheduleTab();

        //AC: 2
        //Step: 5
        assert_().withFailureMessage("Classification tab is expected to be selected")
                .that(serviceSchedulePage.isClassificationTabSelected())
                .isTrue();

        //AC: 3
        //Step: 6
        List<String> viewByDropdown = serviceSchedulePage.getViewByDropDownOptions();
        assert_().withFailureMessage("View by drop down options are expected to be displayed")
                .that(serviceSchedulePage.getViewByDropDownOptions()
                        .equals(expectedViewByDropdown))
                .isTrue();

        //AC: 4
        //Step: 7
        ClassificationSubPage classificationSubPage = serviceSchedulePage.clickClassificationTab();
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(classificationSubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 8
        assert_().withFailureMessage("Add products CTA is expected to be displayed")
                .that(classificationSubPage.isAddProductsButtonDisplayed())
                .isTrue();

        //AC: 4
        //Step: 9
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.SERVICE_TYPE);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(classificationSubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 10
        assert_().withFailureMessage("Add products CTA is not expected to be displayed")
                .that(classificationSubPage.isAddProductsButtonDisplayed())
                .isFalse();

        //AC: 4
        //Step: 11
        StatutorySubPage statutorySubPage = serviceSchedulePage.clickStatutoryTab();
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.PRODUCT);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(statutorySubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 12
        assert_().withFailureMessage("Add products CTA is expected to be displayed")
                .that(statutorySubPage.isAddProductsButtonDisplayed())
                .isTrue();

        //AC: 4
        //Step: 13
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.SERVICE_TYPE);
        assert_().withFailureMessage("Notice Message is expected to be present")
                .that(statutorySubPage.getNoticeMessage()
                        .equals(expectedNoticeMessage))
                .isTrue();

        //AC: 5
        //Step: 14
        assert_().withFailureMessage("Add products CTA is not expected to be displayed")
                .that(statutorySubPage.isAddProductsButtonDisplayed())
                .isFalse();

        //AC: 6
        //Step: 15
        Date date = getHarmonisationDateByName(nonImoAssetName);
        String harmonizationDate = FRONTEND_TIME_FORMAT.format(date);
        assert_().withFailureMessage("Harmonisation date format is expected to be displayed")
                .that(serviceSchedulePage.getHarmonisationDate()
                        .equals(harmonizationDate))
                .isTrue();

        //AC: 6 and AC: 7
        //Step: 16 and 17
        serviceSchedulePage.clickEditRuleSetIcon();
    }

    private Date getHarmonisationDateByName(String name) throws SQLException
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET WHERE name = '" + name + "';");
        try
        {
            rs.first();
            Date getDate = (rs.getDate("harmonisation_date"));
            return getDate;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private List<String> getImoNumber() throws SQLException
    {
        ArrayList<String> imoNumbers = new ArrayList<>();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_ASSET ;");
        while (rs.next())
        {
            String ids = rs.getString("imo_number");
            imoNumbers.add(ids);
        }
        return imoNumbers;
    }

    private List<String> getLrNumber() throws SQLException
    {
        ArrayList<String> lrNumbers = new ArrayList<>();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM ihs_db.absd_ship_search ;");
        while (rs.next())
        {
            String ids = rs.getString("LRNO");
            lrNumbers.add(ids);
        }
        return lrNumbers;
    }

}
