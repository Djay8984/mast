package test.viewasset.serviceschedule;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import model.product.Product;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.serviceschedule.ServiceSchedulePage;
import viewasset.sub.serviceschedule.StatutorySubPage;
import viewasset.sub.serviceschedule.addproducts.AddProductPage;
import viewasset.sub.serviceschedule.element.ProductElement;
import viewasset.sub.serviceschedule.element.ProductHeaderElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.PRODUCT_FAMILY_STATUTORY;

public class AddStatutoryProductTest extends BaseTest
{

    private final int assetId = 546;
    private String flagStateId = "400";

    @Test(description = "Automation Test Story 12.4 As a user I want to select to add statutory products so that I can add the relevant services")
    @Issue("LRT-2372")
    public void AddStatutoryProductTest() throws InterruptedException, SQLException
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        ServiceSchedulePage serviceSchedulePage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton().clickServiceScheduleTab();

        serviceSchedulePage.clickStatutoryTab();
        serviceSchedulePage.selectViewByDropDownOption(ServiceSchedulePage.ViewBy.PRODUCT);

        AddProductPage addProductPage = serviceSchedulePage.clickAddProductsButton();

        //AC1 , AC4
        assert_().withFailureMessage("Select Product Statutory is expected to be displayed.")
                .that(addProductPage.getSelectedProductFamilyName())
                .isEqualTo("Statutory");

        //AC2
        //@Visible in PO in AddProductPage

        //AC4
        serviceSchedulePage = addProductPage.clickBackButton();
        assert_().withFailureMessage("Expected to allow user to navigate back to Service Schedule Page by clicking back button")
                .that(serviceSchedulePage.isStatutoryTabSelected())
                .isTrue();

        final AddProductPage addProductPageStatic = serviceSchedulePage.clickAddProductsButton();

        //AC5
        assert_().withFailureMessage("Restricted by flag is expected to be selected by default.")
                .that(addProductPageStatic.isRestrictetByFlagSelected());

        List<HashMap<String, Object>> allNotRestrictedByFlagProducts = new ArrayList<>();
        List<HashMap<String, Object>> allRestrictedByFlagProducts = new ArrayList<>();

        List<HashMap<String, Object>> notRestrictedByFlagProducts = new ArrayList<>();
        List<HashMap<String, Object>> restrictedByFlagProducts = new ArrayList<>();

        allNotRestrictedByFlagProducts = TestDataHelper.getNonRestrictedByFlagProductByProductFamilyId(PRODUCT_FAMILY_STATUTORY);
        allRestrictedByFlagProducts = TestDataHelper.getRestrictedByFlagProductByProductFamilyId(PRODUCT_FAMILY_STATUTORY, flagStateId);

        //Product which has been added to service schedule
        List<String> productIds = Arrays.asList(new Product(assetId).getDtoArray()).stream().map(BaseDto::getId).map(String::valueOf)
                .collect(Collectors.toList());

        //filter out product which has been added to service schedule
        notRestrictedByFlagProducts = allNotRestrictedByFlagProducts.stream()
                .filter(x -> !(productIds.contains(x.get("id").toString())))
                .collect(Collectors.toList());

        restrictedByFlagProducts = allRestrictedByFlagProducts.stream()
                .filter(x -> !(productIds.contains(x.get("id").toString())))
                .collect(Collectors.toList());

        //AC2 verify list of display for all restricted by flag product
        assert_()
                .withFailureMessage("All product which is restricted by flag and has not been added to service schedule is expected to be displayed.")
                .that(restrictedByFlagProducts.stream().map(x -> x.get("name")).map(Object::toString).collect(Collectors.toList()))
                .containsAllIn(addProductPageStatic.getProductElements().stream().map(ProductElement::getProductName).collect(Collectors.toList()));

        //AC6 iterate each product service regime dropdown, default is blank
        for (HashMap<String, Object> product : restrictedByFlagProducts)
        {

            assert_().withFailureMessage(
                    "Restricted by flag [" + product.get("name").toString() + "]" + "By default regime service drop down is expected to be blank")
                    .that(addProductPageStatic.getProductElementByProductName(product.get("name").toString()).getSelectedServiceRegime().trim())
                    .isEqualTo("?");

            assert_().withFailureMessage("Options in regime service is expected to be same with backend")
                    .that(TestDataHelper.getServiceRegimeNameByProductCatalogueId(product.get("id").toString()))
                    .containsAllIn(
                            addProductPageStatic.getProductElementByProductName(product.get("name").toString()).getServiceRegimeDropDownOptions());
        }

        addProductPageStatic.selectNotRestrictedByFlag();

        // AC7 , AC8 verify list of not restricted by flag product
        assert_().withFailureMessage(
                "All product which is not restricted by flag and has not been added to service schedule is expected to be displayed.")
                .that(notRestrictedByFlagProducts.stream().map(x -> x.get("name")).map(Object::toString).collect(Collectors.toList()))
                .containsAllIn(addProductPageStatic.getProductElements().stream().map(ProductElement::getProductName).collect(Collectors.toList()));

        //AC6 iterate each product service regime dropdown, default is blank
        for (HashMap<String, Object> product : notRestrictedByFlagProducts)
        {

            assert_().withFailureMessage(
                    "Not restricted by flag [" + product.get("name").toString() + "]" + "By default regime service drop down is expected to be blank")
                    .that(addProductPageStatic.getProductElementByProductName(product.get("name").toString()).getSelectedServiceRegime().trim())
                    .isEqualTo("?");

            assert_().withFailureMessage("Options in regime service is expected to be same with backend")
                    .that(TestDataHelper.getServiceRegimeNameByProductCatalogueId(product.get("id").toString()))
                    .containsAllIn(
                            addProductPageStatic.getProductElementByProductName(product.get("name").toString()).getServiceRegimeDropDownOptions());
        }

        List<String> selectedSomeRestrictedByFlagProduct = new ArrayList<>();
        List<String> selectedSomeNotRestrictedByFlagProduct = new ArrayList<>();

        int numberOfSelectedProduct = 3;

        if (numberOfSelectedProduct <= addProductPageStatic.getProductElements().size())
        {
            for (int i = 0; i < numberOfSelectedProduct; i++)
            {
                addProductPageStatic.getProductElements().get(i).checkProductCheckbox();
            }
            //AC9
            assert_().withFailureMessage("System is expected to allow multiple select checkboxes.")
                    .that(addProductPageStatic.getProductElements().stream().filter(ProductElement::isProductCheckboxChecked)
                            .collect(Collectors.toList()).size())
                    .isEqualTo(numberOfSelectedProduct);

            for (int i = 0; i < numberOfSelectedProduct; i++)
            {
                addProductPageStatic.getProductElements().get(i).uncheckProductCheckbox();
            }
            //AC9
            assert_().withFailureMessage("System is expected to allow user to unselect checkboxes.")
                    .that(addProductPageStatic.getProductElements().stream().filter(ProductElement::isProductCheckboxChecked)
                            .collect(Collectors.toList()).size())
                    .isEqualTo(0);

            for (int i = 0; i < numberOfSelectedProduct; i++)
            {
                addProductPageStatic.getProductElements().get(i).checkProductCheckbox();
                selectedSomeNotRestrictedByFlagProduct.add(addProductPageStatic.getProductElements().get(i).getProductName());
            }

            addProductPageStatic.selectRestrictedByFlag();

            for (int i = 0; i < numberOfSelectedProduct; i++)
            {
                addProductPageStatic.getProductElements().get(i).checkProductCheckbox();
                selectedSomeRestrictedByFlagProduct.add(addProductPageStatic.getProductElements().get(i).getProductName());
            }

            addProductPageStatic.selectNotRestrictedByFlag();
            //AC10
            selectedSomeNotRestrictedByFlagProduct.forEach(x ->
            {
                assert_().withFailureMessage("Checked not restricted by flag products [" + x + "] is expected to be persist after switching flag")
                        .that(addProductPageStatic.getProductElementByProductName(x).isProductCheckboxChecked())
                        .isTrue();
            });

            addProductPageStatic.selectRestrictedByFlag();
            //AC10
            selectedSomeRestrictedByFlagProduct.forEach(x ->
            {
                assert_().withFailureMessage("Checked restricted by flag products [" + x + "] is expected to be persist after switching flag")
                        .that(addProductPageStatic.getProductElementByProductName(x).isProductCheckboxChecked())
                        .isTrue();
            });
        }

        //AC4 able navigate to previous page
        //AC12 //Step 19
        serviceSchedulePage = addProductPageStatic.clickButtonCancel();

        assert_().withFailureMessage("Expected to allow user to navigate back to Service Schedule Page")
                .that(serviceSchedulePage.isStatutoryTabSelected())
                .isTrue();

        StatutorySubPage statutorySubPage = serviceSchedulePage.clickStatutoryTab();

        assert_().withFailureMessage("Selected products is expected not to be added after clicking button Cancel")
                .that(statutorySubPage.getProductElements().stream().map(ProductHeaderElement::getProductName).collect(Collectors.toList()))
                .doesNotContain(selectedSomeRestrictedByFlagProduct);

        addProductPage = serviceSchedulePage.clickAddProductsButton();

        //AC13
        assert_().withFailureMessage("Button Add is expected to be disabled when non of the products is checked")
                .that(addProductPage.getProductElements().stream().noneMatch(ProductElement::isProductCheckboxChecked) && !addProductPage
                        .isButtonAddEnabled())
                .isTrue();

        //Select one product and set service regime
        List<String> addedProductName = new ArrayList<>();
        addProductPage.getProductElements().get(0).checkProductCheckbox();
        addProductPage.getProductElements().get(0).setServiceRegimeDropDownWithLastOption();
        addedProductName.add(addProductPage.getProductElements().get(0).getProductName());

        //AC13 step 21
        assert_().withFailureMessage("Button Add is expected to be enabled when at least one product is selected")
                .that(addProductPage.isButtonAddEnabled())
                .isTrue();

        serviceSchedulePage = addProductPage.clickButtonAdd();
        //AC 14
        assert_().withFailureMessage("Added products are expected to be displayed in Service Schedule Page")
                .that(serviceSchedulePage.clickStatutoryTab().getProductElements().stream().map(ProductHeaderElement::getProductName)
                        .collect(Collectors.toList()))
                .containsAllIn(addedProductName);

        //AC3 Step 8 , AC11 step 18
        addProductPage = serviceSchedulePage.clickAddProductsButton();
        assert_().withFailureMessage("Product which has been added to service schedule should not be displayed in Add Product Page")
                .that(addProductPage.getProductElements().stream().map(ProductElement::getProductName).collect(Collectors.toList()))
                .containsNoneIn(addedProductName);

    }
}
