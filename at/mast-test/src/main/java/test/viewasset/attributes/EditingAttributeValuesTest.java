package test.viewasset.attributes;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetItemAttributePage;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class EditingAttributeValuesTest extends BaseTest
{

    @Test(description = "8.25 AC 1-7 - Editing attribute values")
    @Issue("LRT-1253")
    public void editingAttributeValuesTest()
    {

        int assetId = 501;
        int itemId = 689;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        AssetModelSubPage assetModelPage = viewAssetPage.getAssetModelSubPage();
        ListViewSubPage listViewSubPage = assetModelPage.getListViewSubPage();

        //Step: 3
        ItemsDetailsPage itemsDetailsPage
                = listViewSubPage.getItemByIndex(2).clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = itemsDetailsPage.clickAttributesTab();
        if (attributesSubPage.getAllDisplayedAttributeElement().size() == 0)
        {
            assert_().withFailureMessage("No attributes have been added message is expected to be displayed")
                    .that(attributesSubPage.getAttributeNoticeText()
                            .equals("No attributes have been added to this item."))
                    .isTrue();
        }

        //Step: 4
        attributesSubPage.clickNavigateBackButton(AssetModelSubPage.class);

        //Step: 5, 6 and 7
        assetModelPage.getListViewSubPage();
        ListViewItemElement listViewItemElement = listViewSubPage.getItemByIndex(1);
        listViewSubPage = listViewItemElement.clickItemName();

        String itemName = new AssetItem(assetId, itemId).getDto().getName();
        listViewItemElement = listViewSubPage.getItemByIndex(1);

        //AC 1 & 7
        itemsDetailsPage = listViewItemElement.clickFurtherDetailsArrow();
        attributesSubPage = itemsDetailsPage.clickAttributesTab();
        List<String> names
                = attributesSubPage.getAllDisplayedAttributeElementNames();
        List<AttributeDto> attributeDtos
                = new AssetItemAttributePage(assetId, itemId).getDto().getContent();
        List<AttributeTypeDto> attributeTypeDtosOrderByDisplayOrder
                = getAttributeTypeDtoById(attributeDtos)
                .stream()
                .sorted((x1, x2) -> x1.getDisplayOrder().compareTo(x2.getDisplayOrder()))
                .collect(Collectors.toList());

        assert_().withFailureMessage("All attributes that relate to item [" + itemName + "] are present")
                .that(names)
                .containsExactlyElementsIn(attributeTypeDtosOrderByDisplayOrder.stream()
                        .map(ReferenceDataDto::getName).collect(Collectors.toList()));

        //AC 7
        assert_().withFailureMessage("Save button is expected to be Disabled")
                .that(attributesSubPage.isSaveChangesButtonEnabled())
                .isFalse();
        assert_().withFailureMessage("Discard Changes button is expected to be Disabled")
                .that(attributesSubPage.isDiscardChangesButtonEnabled())
                .isFalse();

        List<String> newAttributeValues = new ArrayList<>();

        //AC 2 & 3
        for (AttributeDto attributeDto : attributeDtos)
        {
            AttributeTypeDto attributeTypeDto = attributeTypeDtosOrderByDisplayOrder.stream()
                    .filter(aT -> aT.getId().equals(attributeDto.getAttributeType().getId())).findFirst().orElse(null);

            //Switch based on the attribute value type and check if it's correct
            String attributeValueType = attributeTypeDto.getValueType().getName();
            String attributeName = attributeTypeDto.getName();
            switch (attributeValueType)
            {
                case "Date":
                    assert_().withFailureMessage("Attribute value was expected to be a date picker")
                            .that(attributesSubPage.getAttributeTagName(attributeName))
                            .isEqualTo("date");

                    attributesSubPage.setAttributeValue(attributeName, "123/abc/£$");
                    assert_().withFailureMessage("Error Message is expected to be displayed")
                            .that(attributesSubPage
                                    .isErrorIconDisplayed(attributeName, AttributesSubPage.type.ERROR)
                                    || attributesSubPage.getValidationErrorMessage(attributeName)
                                    .equals("Please select a valid date"))
                            .isTrue();

                    newAttributeValues.add(FRONTEND_TIME_FORMAT.format(new Date()));
                    attributesSubPage.setAttributeValue(
                            attributeName,
                            newAttributeValues.get(newAttributeValues.size() - 1)
                    );
                    break;

                case "Float":
                    assert_().withFailureMessage("Attribute value was expected to be a text field")
                            .that(attributesSubPage.getAttributeTagName(attributeName))
                            .isEqualTo("input");

                    attributesSubPage.setAttributeValue(attributeName, "TEST");
                    assert_().withFailureMessage("Error Message is expected to be displayed")
                            .that(attributesSubPage
                                    .isErrorIconDisplayed(attributeName, AttributesSubPage.type.ERROR)
                                    || attributesSubPage.getValidationErrorMessage(attributeName)
                                    .equals("Please enter a valid number"))
                            .isTrue();

                    newAttributeValues.add(Integer.toString(TestDataHelper.getUniqueYardNumber()));
                    attributesSubPage.setAttributeValue(
                            attributeName,
                            newAttributeValues.get(newAttributeValues.size() - 1)
                    );
                    break;

                case "Lookup":
                case "Boolean":
                    assert_().withFailureMessage("Attribute value was expected to be a dropdown")
                            .that(attributesSubPage.getAttributeTagName(attributeName))
                            .isEqualTo("select");

                    //AC 6
                    final List<String> attributeValues = attributesSubPage.getAttributeDropDownValues(attributeName);
                    attributeTypeDto.getAllowedValues().forEach(v ->
                            assert_().withFailureMessage("Value '" + v.getName() + "' wasn't found in the dropdown")
                                    .that(v.getName())
                                    .isIn(attributeValues));
                    newAttributeValues.add(attributeValues.get(
                            TestDataHelper.getRandomNumber(0, attributeValues.size() - 1)));
                    attributesSubPage.selectAttributeDropDownByVisibleText(
                            attributeName,
                            newAttributeValues.get(newAttributeValues.size() - 1)
                    );
                    break;

                case "String":
                    assert_().withFailureMessage("Attribute value was expected to be a text field")
                            .that(attributesSubPage.getAttributeTagName(attributeName))
                            .isEqualTo("input");
                    newAttributeValues.add(Integer.toString(TestDataHelper.getUniqueYardNumber()));
                    attributesSubPage.setAttributeValue(
                            attributeName,
                            newAttributeValues.get(newAttributeValues.size() - 1)
                    );
                    break;
            }
        }

        //AC 7
        assert_().withFailureMessage("Discard button is expected to be enabled")
                .that(attributesSubPage.isDiscardChangesButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("Save button is expected to be enabled")
                .that(attributesSubPage.isSaveChangesButtonEnabled())
                .isTrue();

        //AC 2
        attributesSubPage.clickSaveChangesButton().clickOkButton();

        attributesSubPage.getAllDisplayedAttributeElement().forEach(
                attributeElement -> assert_().withFailureMessage("Attribute value wasn't updated upon saving")
                        .that(attributeElement.getAttributeValue()).isIn(newAttributeValues));
    }

    private List<AttributeTypeDto> getAttributeTypeDtoById(List<AttributeDto> attributeDto)
    {
        ReferenceDataMap referenceDataMap =
                new ReferenceDataMap(ReferenceDataMap.Subset.ASSET, ReferenceDataMap.RefData.ATTRIBUTE_TYPES);
        List<AttributeTypeDto> attributeTypeDtos = referenceDataMap.getReferenceDataAsClass(AttributeTypeDto.class);

        return attributeDto
                .stream()
                .map(
                        c -> attributeTypeDtos.stream()
                                .filter(a -> a.getId().equals(c.getAttributeType().getId()))
                                .findFirst()
                                .orElse(new AttributeTypeDto())
                )
                .collect(Collectors.toList());
    }
}
