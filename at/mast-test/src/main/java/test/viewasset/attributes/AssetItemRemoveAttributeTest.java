package test.viewasset.attributes;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.attributes.element.AttributeElement;
import viewasset.item.attributes.sub.AddAttributePage;
import viewasset.item.attributes.sub.element.AddAttributeElement;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AssetItemRemoveAttributeTest extends BaseTest
{

    int assetId = 501;

    @Test(description = "Story -  8.26 - As a user on item specific page, I want to remove attributes added against an item" +
            "so that I can ensure the asset model is correct.")
    @Issue("LRT-1272")
    public void assetItemRemoveAttributeTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        AssetModelSubPage assetModelSubPage
                = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton().clickAssetModelTab();

        //TODO: AC: 4 and AC 5
        /**
         * These commented steps are required to work on AC: 4 and AC: 5
         * HierarchyViewSubPage hierarchyViewSubPage = assetModelSubPage
         .clickHierarchyViewButton();

         List<HierarchyViewItemElement> hierarchyViewItemElement = hierarchyViewSubPage.getAssetItems();
         String itemId = hierarchyViewItemElement.get(0).findElement(By.cssSelector("div")).getAttribute("id").replaceAll("\\D+","");

         AssetItemAttributePage assetItemAttributePage = new AssetItemAttributePage(501,Integer.valueOf(itemId));
         Long d = assetItemAttributePage.getDto().getContent().get(0).getAttributeType().getId();*
         */

        ListViewSubPage listViewSubPage
                = assetModelSubPage.clickListViewButton();
        ListViewItemElement listViewItemElement
                = listViewSubPage.getItemByIndex(2);

        ItemsDetailsPage itemsDetailsPage
                = listViewItemElement.clickFurtherDetailsArrow();

        //Step: 3
        AttributesSubPage attributesSubPage
                = itemsDetailsPage.clickAttributesTab();
        AttributesSubPage.AttributeTable attributeTable
                = attributesSubPage.getAttributeTable();
        List<AttributeElement> attributeElement
                = attributeTable.getRows();

        //Step: 4
        List<AttributeElement> deleteItem = attributeElement.stream()
                .filter(e -> (!e.isMandatoryTextDisplayInLastColumnContainer()))
                .collect(Collectors.toList());
        for (AttributeElement deleteItemAttribute : deleteItem)
        {
            assert_().withFailureMessage("For Non mandatory attributes Delete Icon is expected to be displayed")
                    .that(deleteItemAttribute.isDeleteIconDisplay())
                    .isTrue();
        }

        //Step: 5
        List<AttributeElement> mandatory = attributeElement.stream()
                .filter(e -> e.isMandatoryTextDisplayInLastColumnContainer())
                .collect(Collectors.toList());
        for (AttributeElement mandatoryAttribute : mandatory)
        {
            assert_().withFailureMessage("For mandatory attributes 'Mandatory' text is displayed")
                    .that(mandatoryAttribute.getLastColumnContainerText().equals("Mandatory"))
                    .isTrue();
        }

        //AC: 1
        //Step: 6
        String deletedItem = deleteItem.get(0).getAttributeNameText();
        deleteItem.get(0).clickDeleteAttributeIcon();
        assert_().withFailureMessage("Deleted Attribute is no longer displayed")
                .that(attributeElement.stream()
                        .filter(e -> e.getAttributeNameText().equals(deletedItem))
                        .findFirst().isPresent())
                .isFalse();

        //AC: 2
        //Step: 7
        for (AttributeElement mandatoryAttribute : mandatory)
        {
            assert_().withFailureMessage("For mandatory attributes Delete Icon is not displayed")
                    .that(mandatoryAttribute.isDeleteIconDisplay())
                    .isFalse();
        }

        //AC: 3
        //Step: 8
        AddAttributePage addAttributePage = attributesSubPage.clickAddAttributesButton();
        List<AddAttributeElement> addAttributeElements = addAttributePage.getAttributes();
        assert_().withFailureMessage("Deleted Attribute is os expected to be present in Add attribute section")
                .that(addAttributeElements.stream()
                        .filter(e -> e.getAttributeName().equals(deletedItem))
                        .findFirst().isPresent())
                .isTrue();

        //Step: 13
        addAttributeElements.stream()
                .filter(e -> e.getAttributeName().equals(deletedItem))
                .findFirst().get()
                .clickCheckbox();
        addAttributePage.clickAddAttributeButton();
        deleteItem = attributeElement.stream()
                .filter(e -> (!e.isMandatoryTextDisplayInLastColumnContainer()))
                .collect(Collectors.toList());
        deleteItem.get(0).clickDeleteAttributeIcon();
        attributesSubPage.clickAddAttributesButton();
        addAttributeElements = addAttributePage.getAttributes();
        assert_().withFailureMessage("Deleted Attribute is os expected to be present in Add attribute section")
                .that(addAttributeElements.stream()
                        .filter(e -> e.getAttributeName().equals(deletedItem))
                        .findFirst().isPresent())
                .isTrue();

        //Step: 14
        addAttributePage.clickCloseButton();
        assert_().withFailureMessage("Discard changes button is expected to be Enabled")
                .that(attributesSubPage.isDiscardChangesButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("Save Changes button is expected to be Enabled")
                .that(attributesSubPage.isSaveChangesButtonEnabled())
                .isTrue();
    }
}
