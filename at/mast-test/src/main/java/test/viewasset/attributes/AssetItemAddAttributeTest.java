package test.viewasset.attributes;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.attributes.sub.AddAttributePage;
import viewasset.item.attributes.sub.element.AddAttributeElement;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AssetItemAddAttributeTest extends BaseTest
{

    @Test(description = "Story - 8.24 - User is on an item specific page and is able to " +
            "add attributes to the item.")
    @Issue("LRT-1230")
    public void assetItemAddAttributeTest() throws SQLException
    {
        Asset searchableAsset = new Asset(501);
        String name = searchableAsset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name);
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage
                .getAssetCardByIndex(0)
                .clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage.getAssetModelSubPage();
        ListViewSubPage listViewSubPage = assetModelSubPage.getListViewSubPage();
        ListViewItemElement element = listViewSubPage.getItemByIndex(1);
        String itemName = element.getItemName();
        ItemsDetailsPage viewAssetAttributePage = element.clickFurtherDetailsArrow();

        //Workaround as all items have all attributes set to the item
        AttributesSubPage attributesSubPage = viewAssetAttributePage.getAttributesTab();

        //Save changes only if attributes have been deleted.
        final boolean[] deletedAttributes = {false};
        attributesSubPage.getAllDisplayedAttributeElement()
                .forEach(a ->
                {
                    if (!a.isMandatoryTextDisplayInLastColumnContainer())
                    {
                        a.clickDeleteAttributeIcon();
                        deletedAttributes[0] = true;
                    }
                });
        if (deletedAttributes[0])
            attributesSubPage.clickSaveChangesButton().clickOkButton();

        //AC1, AC2, AC3, AC4
        int i = 0;
        AddAttributePage addAttributePage = viewAssetAttributePage.clickAttributesTab().clickAddAttributesButton();

        List<List<String>> expectedAttributes = TestDataHelper.getExpectedAttributes(501, itemName);

        assert_().withFailureMessage("The expected size of attributes does not match the actual size of attributes.")
                .that(addAttributePage.getAttributes().size()).isEqualTo(expectedAttributes.size());

        for (AddAttributeElement attribute : addAttributePage.getAttributes())
        {
            assert_().withFailureMessage("The expected attribute name doesn't match the actual attribute name")
                    .that(expectedAttributes.get(i).get(0)).isEqualTo(attribute.getAttributeName());

            boolean multiple = Integer.parseInt(expectedAttributes.get(i).get(2)) > 1;

            assert_().withFailureMessage("The expected allow multiple does not match the expected allow multiple")
                    .that(multiple).isEqualTo(attribute.isMultiple());

            assert_().withFailureMessage("The expected naming order does not match the expected naming order")
                    .that(expectedAttributes.get(i).get(3)).isEqualTo(attribute.getNamingOrder());
            i++;
        }

        //AC5
        assert_().withFailureMessage("The attribute header doesn't match what was expected")
                .that(addAttributePage.attributeHeaderText()).isEqualTo("Attribute name");

        assert_().withFailureMessage("The multiple header doesn't match what was expected")
                .that(addAttributePage.multipleHeaderText()).isEqualTo("Multiple");

        assert_().withFailureMessage("The naming order header doesn't match what was expected")
                .that(addAttributePage.namingHeaderText()).isEqualTo("Naming order");

        assert_().withFailureMessage("The instances header does not match what was expected")
                .that(addAttributePage.instancesHeaderText()).isEqualTo("No. of instances");

        //AC8
        String addedAttribute = null;
        for (AddAttributeElement attribute : addAttributePage.getAttributes())
        {
            if (attribute.isMultiple())
            {
                addedAttribute = attribute.getAttributeName();
                validateMultipleAttributeRow(addAttributePage, addedAttribute);
                break;
            }
        }
        addAttributePage.clickCloseButton();
        addAttributePage = viewAssetAttributePage.clickAttributesTab().clickAddAttributesButton();

        //AC11
        if (addedAttribute != null)
        {
            assert_().withFailureMessage("The multiple attribute should still be seen in the list")
                    .that(addAttributePage.getAttributeByName(addedAttribute)).isNotNull();

            assert_().withFailureMessage("The multiple attribute should not be checked")
                    .that(addAttributePage.getAttributeByName(addedAttribute).isInstancesEnabled()).isFalse();

            assert_().withFailureMessage("The multiple attribute should have defaulted back to 0")
                    .that(addAttributePage.getAttributeByName(addedAttribute).getInstancesValue()).isEqualTo("0");
        }

        //AC13
        assert_().withFailureMessage("The add attribute button should not be enabled")
                .that(addAttributePage.isAddAttributeButtonDisabled()).isTrue();

        //AC12 & AC13
        List<AddAttributeElement> attributeElements = addAttributePage.getAttributes();
        attributeElements.forEach(AddAttributeElement::clickCheckbox);

        attributeElements.forEach(a -> assert_().withFailureMessage(String.format(
                "The attribute '%s' should be ticked",
                a.getAttributeName())).that(a.isCheckboxChecked()).isTrue());

        assert_().withFailureMessage("The add attribute button should be enabled")
                .that(addAttributePage.isAddAttributeButtonDisabled()).isFalse();
        //AC14
        assert_().withFailureMessage("The amount of attributes selected is not what was expected")
                .that(addAttributePage.countAttributesSelected())
                .isEqualTo(attributeElements.size() + " attributes selected");

        //AC15
        addAttributePage.clickCloseButton();
        addAttributePage = viewAssetAttributePage.clickAttributesTab().clickAddAttributesButton();

        AddAttributePage finalAddAttributePage = addAttributePage;
        attributeElements.forEach(a -> assert_().withFailureMessage("The attributes should still be seen in the list")
                .that(
                        finalAddAttributePage.getAttributes().stream().filter(
                                fA -> fA.getAttributeName().equals(a.getAttributeName())).findFirst().orElse(null)
                )
                .isNotNull());

        //AC16
        List<String> addedAttributeNames = addAttributePage.getAttributes().stream()
                .map(AddAttributeElement::getAttributeName).collect(Collectors.toList());
        addAttributePage.getAttributes().forEach(AddAttributeElement::clickCheckbox);
        AttributesSubPage attributesPage = addAttributePage.clickAddAttributeButton();

        List<String> displayedAttributeNames = attributesPage.getAllDisplayedAttributeElementNames();

        assert_().withFailureMessage("The attributes should have been added to the item")
                .that(displayedAttributeNames).containsAllIn(addedAttributeNames);
    }

    private void validateMultipleAttributeRow(AddAttributePage addAttributePage, String attributeName)
    {
        AddAttributeElement attribute = addAttributePage.getAttributeByName(attributeName);

        //AC10
        assert_().withFailureMessage("The instances textbox should be enabled")
                .that(attribute.isInstancesEnabled()).isTrue();

        attribute.clickPlusButton();
        assert_().withFailureMessage("You should be unable to add multiple instances until the attribute is ticked")
                .that(attribute.getInstancesValue()).isEqualTo("0");

        //AC9
        attribute.clickCheckbox();
        assert_().withFailureMessage("The default value for multiple instances is 1")
                .that(attribute.getInstancesValue()).isEqualTo("1");

        attribute.clickPlusButton();
        attribute.clickPlusButton();
        attribute.clickPlusButton();
        assert_().withFailureMessage("The user is unable to add multiple instances")
                .that(attribute.getInstancesValue()).isEqualTo("4");

        attribute.enterInstances("5");
        assert_().withFailureMessage("The user is unable to enter multiple instances")
                .that(attribute.getInstancesValue()).isEqualTo("5");

        attribute.enterInstances("test");
        assert_().withFailureMessage("The user is able to enter characters to the textbox")
                .that(attribute.getInstancesValue()).isEqualTo("");

        attribute.enterInstances("!£$%^&*()");
        assert_().withFailureMessage("The user is able to enter special character to the textbox")
                .that(attribute.getInstancesValue()).isEqualTo("");

        attribute.enterInstances("1");
    }
}



