package test.viewasset;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.surveyor.Surveyor;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.assetheaderadditionalinformation.AttachmentsAndNotesPage;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SelectCaseTypePage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.EditJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.defects.DefectsPage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.element.MyWorkAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.MyWorkSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class ViewAssetHeaderTest extends BaseTest
{
    @Test(description = "US11.10 - As a user I want to view the asset header so that when I move to a different page of the application, I am always aware which asset the information is associated with")
    @Issue("LRT-2320, LRT-2321, LRT-2323")

    public void ViewAssetHeaderTest()
    {

        int assetId = 501;
        int assetIdWithLesserAttributes = 2;
        final String productCatalogueName = "Classification";
        final String jobScopeServiceType = "Machinery";

        WorkHubPage workHubPage = WorkHubPage.open();
        // (AC : 1)
        HeaderPage headerPage = workHubPage.getHeader();

        assert_().withFailureMessage("The Asset Header is expected to be not shown")
                .that(headerPage.isAssetHeaderTabDisplayed()).isFalse();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        searchFilterSubPage.clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetId(assetId);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        // (AC : 2) (AC : 1) (AC : 4)
        AssetLightDto assetDto = new Asset(assetId).getDto();
        assert_().withFailureMessage("The Asset Header is expected to be expanded")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();

        if (!assetDto.getGrossTonnage().toString().isEmpty())
        {
            assert_().withFailureMessage("The Asset Header is expected to display Gross Tonnage")
                    .that(viewAssetPage.isGrossTonnagePresent())
                    .isTrue();
        }
        if (!assetDto.getLeadImo().toString().isEmpty())
        {
            assert_().withFailureMessage("The Asset Header displays Imo Number")
                    .that(viewAssetPage.isImoNumberPresent())
                    .isTrue();
        }
        if (!assetDto.getAssetType().toString().isEmpty())
        {
            assert_().withFailureMessage("The Asset Header displays Asset Type")
                    .that(viewAssetPage.isAssetTypePresent())
                    .isTrue();
        }
        if (!assetDto.getBuildDate().toString().isEmpty())
        {
            assert_().withFailureMessage("The Asset Header displays Date Of Build")
                    .that(viewAssetPage.isDateOfBuildPresent())
                    .isTrue();
        }
        if (!assetDto.getFlagState().toString().isEmpty())
        {
            assert_().withFailureMessage("The Asset Header displays Flag")
                    .that(viewAssetPage.isFlagPresent())
                    .isTrue();
        }

        // (AC : 6)

        AssetModelSubPage assetModelSubPage = viewAssetPage.getAssetModelSubPage();
        ListViewSubPage listView = assetModelSubPage.getListViewSubPage();
        listView = listView.getItemByIndex(1).clickItemName();
        assert_().withFailureMessage("The Asset Header is expected to be expanded in Item page of Asset")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        // (AC:5a & AC: 6)
        ItemsDetailsPage itemsDetailsPage = listView.getItemByIndex(1).clickFurtherDetailsArrow();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in items details page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        viewAssetPage.clickExpandButtonInHeader();
        assert_().withFailureMessage("The Asset Header is expected to expand on clicking expand button")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class);

        // (AC : 5k)
        CasesPage casesPage = viewAssetPage.clickCasesTab();
        assert_().withFailureMessage("The Asset Header is expected to be expanded in Cases Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        SelectCaseTypePage addCasePage = casesPage.clickAddCaseButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in items details page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        addCasePage.clickNavigateBackButton();

        viewAssetPage.clickServiceScheduleTab();
        assert_().withFailureMessage("The Asset Header is expected to be expanded in Service schedule commonpage page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        assert_().withFailureMessage("The Asset Header is expected to be expanded in Job commonpage page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        // (AC :5) , (AC : 6)
        AllJobElement allJobElement = jobsSubPage.getAllJobsByIndex(1);
        assert_().withFailureMessage("The Asset Header is expected to be expanded in Job element Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        ViewJobDetailsPage viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();

        // (AC:5i)
        EditJobPage editJob = viewJobDetailsPage.getHeader().clickEditButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Job Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        viewAssetPage.clickExpandButtonInHeader();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Job Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        editJob.clickNavigateBackButton();

        // (AC: 5d),(Ac : 6)
        ConditionsOfClassPage conditionsOfClassPage = viewJobDetailsPage.clickGoToCocsButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Condition Of Class Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        conditionsOfClassPage.clickNavigateBackButton();

        ActionableItemsPage jobActionableItemsPage = viewJobDetailsPage.clickGoToActionableItemsButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in ActionableItems Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        jobActionableItemsPage.clickNavigateBackButton();

        DefectsPage jobDefectsPage = viewJobDetailsPage.clickGoToDefectsButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Defect Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        jobDefectsPage.clickNavigateBackButton();

        AssetNotesPage assetNotesPage = viewJobDetailsPage.clickGoToAssetNotesButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Asset Notes Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();

        // (AC : 5e)
        viewAssetPage.clickExpandButtonInHeader();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Asset Notess Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        assetNotesPage.clickNavigateBackButton();

        // (AC : 5h),(AC : 5l)
        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Add/Edit Job team Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName);
        addOrEditJobTeamPage.clickNavigateBackButton();

        // (AC : 5g) (AC : 5c)
        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Job Scope Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        addToJobScopePage.clickNavigateBackButton();

        CreditingPage creditingPage = viewJobDetailsPage.clickGoToCreditingButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Job Crediting Page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        creditingPage.clickNavigateBackButton(ViewJobDetailsPage.class);

        viewJobDetailsPage.getHeader().clickNavigateBackButton();
        // (AC : 5f)
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAssetPage.clickCodicilsAndDefectsTab();
        assert_().withFailureMessage("The Asset Header is expected to be expanded in Codicil and Defect page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();
        // (AC : 5j)
        AddConditionOfClassPage addConditionOfClassPage = codicilsAndDefectsPage.clickCocAddNewButton();
        assert_().withFailureMessage("The Asset Header is expected to be collapsed in Add CoC details page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        addConditionOfClassPage.clickNavigateBackButton();

        assetModelSubPage = viewAssetPage.clickAssetModelTab();

        // (AC : 5b)
        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Asset model commonpage page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isFalse();
        viewAssetPage.clickExpandButtonInHeader();
        assert_().withFailureMessage("The Asset Header is expected to be Collapsed in Asset model commonpage page")
                .that(viewAssetPage.isAssetHeaderExpanded())
                .isTrue();

        headerPage = workHubPage.getHeader();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        // (AC : 1)
        assert_().withFailureMessage("The Asset Header is expected to be not shown")
                .that(headerPage.isAssetHeaderTabDisplayed())
                .isFalse();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetIdWithLesserAttributes));
        searchFilterSubPage.clickAssetSearchApplyButton();

        viewAssetPage = allAssetsSubPage.getAssetCardByAssetId(assetIdWithLesserAttributes)
                .clickViewAssetButton();

        // (AC : 3)
        AttachmentsAndNotesPage attachmentsAndNotesPage = viewAssetPage.clickNotesAndAttachmentsIcon();

        // (AC : 7)
        attachmentsAndNotesPage.clickCloseButton();
        viewAssetPage.clickViewMoreButton();
        assert_().withFailureMessage("The Asset Header is not expected to have Expand/Collapse button")
                .that(viewAssetPage.isExpandOrCollapseButtonPresent())
                .isFalse();
    }
}
