package test.viewasset;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetItem;
import model.asset.AssetItemQuery;
import model.asset.AssetRootItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AssetModelListChangeViewsTest extends BaseTest
{

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {428}, {501}, {502}
        };
    }

    @Test(
            description = "Story - 8.19 - As a user I want to navigate between the list view & hierarchy view"
                    + " so that I am able to see the same item in different view without having to look"
                    + " for the specific item again in a different view",
            dataProvider = "asset"
    )
    @Issue("LRT-1231")
    public void assetModelListChangeViews(int assetId)
    {
        int rootItem = new AssetRootItem(assetId).getDto().getId().intValue();
        LazyItemDto items = new AssetItem(assetId, rootItem).getDto();
        List<Long> assetItems = items.getItems()
                .stream()
                .map(LinkVersionedResource::getId)
                .collect(Collectors.toList());

        ItemQueryDto query = new ItemQueryDto();
        query.setItemId(assetItems);
        List<LazyItemDto> assetModel = new AssetItemQuery(query, assetId).getDto().getContent();

        items = new AssetItem(assetId, assetModel.get(1).getId().intValue()).getDto();
        assetItems.clear();
        assetItems = items.getItems()
                .stream()
                .map(LinkVersionedResource::getId)
                .collect(Collectors.toList());

        query = new ItemQueryDto();
        query.setItemId(assetItems);
        List<LazyItemDto> childAssetModel = new AssetItemQuery(query, assetId).getDto().getContent();

        LazyItemDto childItem = childAssetModel
                .stream()
                .filter(child -> child.getItems().size() == 0)
                .findFirst()
                .get();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage
                .getAssetCardByIndex(0)
                .clickViewAssetButton();

        AssetModelSubPage assetModelPage = viewAssetPage.getAssetModelSubPage();

        //AC1
        assert_().withFailureMessage("The list view should be the default view")
                .that(assetModelPage.isListViewButtonSelected())
                .isTrue();

        //AC2
        String assetName = assetModel.get(4).getName();
        ListViewSubPage listView = assetModelPage.getListViewSubPage();
        listView.getItemByIndex(5).clickItemName();
        HierarchyViewSubPage hierarchyView = assetModelPage.clickHierarchyViewButton();

        assert_().withFailureMessage("The item selected in list view should be selected in hierarchy view")
                .that(hierarchyView.getSelectedAssetItemName())
                .isEqualTo(assetName);

        listView = assetModelPage.clickListViewButton();
        listView.clickBreadcrumbLinkByIndex(0);
        hierarchyView = assetModelPage.clickHierarchyViewButton();

        assetName = assetModel.get(1).getName();
        String childName = childItem.getName();
        hierarchyView.getItemBasedOnItemName(assetName).clickCollapseIcon();

        assetModelPage.clickListViewButton();
        assert_().withFailureMessage("The item selected in hierarchy view should be selected in list view")
                .that(listView.getSelectedAssetItem())
                .isEqualTo(assetName);

        //AC4
        assert_().withFailureMessage("An item with no children should not be clickable")
                .that(listView
                        .getItemByItemName(childName)
                        .isItemClickable())
                .isFalse();

        hierarchyView = assetModelPage.clickHierarchyViewButton();

        assert_().withFailureMessage("An item with no children should not be expandable")
                .that(hierarchyView
                        .getItemBasedOnItemName(childName)
                        .isItemExpandable())
                .isFalse();

        //AC3
        assert_().withFailureMessage("The asset item name is not displayed")
                .that(hierarchyView.getItemBasedOnItemName(childName).isDisplayed())
                .isTrue();

        assert_().withFailureMessage("The parent asset item is not expanded")
                .that(hierarchyView.getItemBasedOnItemName(assetName).isItemExpanded())
                .isTrue();

        //AC6
        listView = assetModelPage.clickListViewButton();

        assert_().withFailureMessage("The parent item cannot be seen in the breadcrumb")
                .that(listView.getBreadcrumbTextByIndex(1))
                .isEqualTo(assetName);

        assert_().withFailureMessage("The number of items under the parent is not what was expected")
                .that(listView.getAssetItems().size())
                .isEqualTo(childAssetModel.size());

        //AC8
        hierarchyView = assetModelPage.clickHierarchyViewButton();
        hierarchyView.getItemBasedOnItemName(childName).clickCollapseIcon();

        assert_().withFailureMessage("The user was unable to click and highlight child item")
                .that(hierarchyView.getSelectedAssetItemName())
                .isEqualTo(childName);

        //AC9
        listView = assetModelPage.clickListViewButton();

        assert_().withFailureMessage("The no associated text was not shown")
                .that(listView.isNoAssociatedItemsMessageShown())
                .isTrue();

        assert_().withFailureMessage("The no associated items message does not match the acceptance criteria")
                .that(listView.noAssociatedItemsMessageText())
                .isEqualTo("There are no associated items for the selected item.");

        listView.clickBreadcrumbLinkByIndex(1);
        assert_().withFailureMessage("The user should have navigated back to the parent")
                .that(listView.isNoAssociatedItemsMessageShown())
                .isFalse();

        //AC10
        viewAssetPage.clickCasesTab();
        viewAssetPage.clickAssetModelTab();

        assert_().withFailureMessage("The list view should be the default view")
                .that(assetModelPage.isListViewButtonSelected())
                .isTrue();

        assetModelPage.clickHierarchyViewButton();

        viewAssetPage.clickCasesTab();
        viewAssetPage.clickAssetModelTab();

        assert_().withFailureMessage("The list view should be the default view")
                .that(assetModelPage.isListViewButtonSelected())
                .isTrue();
    }
}
