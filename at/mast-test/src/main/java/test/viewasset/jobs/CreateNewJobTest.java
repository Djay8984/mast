package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static constant.LloydsRegister.JobData;

public class CreateNewJobTest extends BaseTest
{

    @Test(description = "US -10.7 Create new job")
    @Issue("LRT-1826")
    public void createNewJobTest()
    {

        final int assetId = 501;
        final String defaultCaseType = "Please Select";
        final String maxCharacters = "50";
        final String jobStatus = "SDO Assigned";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //AC: 1
        //Step: 3
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        assert_().withFailureMessage("Add New Job Button is expected to be clickable")
                .that(jobsSubPage.isAddNewJobButtonClickable()).isTrue();

        //AC: 2
        //Step: 4
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();

        //AC: 3 and 12
        //Step: 5
        assert_().withFailureMessage("Save Button is not expected to be be clickable")
                .that(addNewJobPage.isSaveButtonClickable())
                .isFalse();

        //AC: 3 and AC: 10
        //Step: 6
        assert_().withFailureMessage("By default No Cases are expected to selected ")
                .that(addNewJobPage.getDefaultSelectedCaseType()
                        .equals(defaultCaseType))
                .isTrue();

        addNewJobPage.selectJobCategorySurvey()
                .then().setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(LloydsRegister.JobData.location)
                .then().setJobDescription(LloydsRegister.JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate);
        assert_().withFailureMessage("Save Button is expected to be be clickable")
                .that(addNewJobPage.isSaveButtonClickable())
                .isTrue();

        addNewJobPage.clearSdoTextField().setSdoCodeWithoutEnter(" ");
        assert_().withFailureMessage("SDO error icon is expected to be displayed")
                .that(addNewJobPage.isSdoErrorIconDisplayed(AddNewJobPage
                        .type.ERROR))
                .isTrue();

        addNewJobPage.setSdoCode(JobData.sdoCode).then().selectCaseTypeByVisibleText(defaultCaseType);

        addNewJobPage.selectCaseTypeByVisibleText(JobData.caseType).then().clearJobLocationTextField();
        assert_().withFailureMessage("Job location error icon is expected to be displayed")
                .that(addNewJobPage.getLocationRequiredValidationMessageText()
                        .equals("Location is a required field"))
                .isTrue();

        addNewJobPage.setJobLocation(JobData.location).then().clearJobDescriptionTextField();

        addNewJobPage.setJobDescription(JobData.description).then().clearEtaTextField();
        assert_().withFailureMessage("ETA error icon is expected to be displayed")
                .that(addNewJobPage.isEtaErrorIconDisplayed(AddNewJobPage
                        .type.ERROR) || addNewJobPage.getEtaValidationMessageText()
                        .equals("ETA is a required field"))
                .isTrue();

        addNewJobPage.setEtaDate(JobData.strEtaDate)
                .then().clearEtdTextField();
        assert_().withFailureMessage("ETD error icon is expected to be displayed")
                .that(addNewJobPage.isEtdErrorIconDisplayed(AddNewJobPage
                        .type.ERROR) || addNewJobPage.getEtdValidationMessageText()
                        .equals("ETD is a required field"))
                .isTrue();

        addNewJobPage.setEtdDate(JobData.strEtdDate)
                .then().clearRequestedAttendanceDateTextField();
        assert_().withFailureMessage("Requested attendance date error icon is expected to be displayed")
                .that(addNewJobPage.isRequestedAttendanceDateErrorIconDisplayed(AddNewJobPage
                        .type.ERROR) || addNewJobPage.getEtaValidationMessageText()
                        .equals("Requested attendance date is a required field"))
                .isTrue();

        addNewJobPage.setRequestedAttendanceDate(JobData.strEtdDate);
        assert_().withFailureMessage("Save Button is expected to be be clickable")
                .that(addNewJobPage.isSaveButtonClickable())
                .isTrue();

        //AC: 4
        //Step: 7
        assert_().withFailureMessage("SDO filed is expected to be Type ahead function")
                .that(addNewJobPage.clearSdoTextField().isSdoFieldTypeAhead(JobData.sdoCode.substring(0, 5)))
                .isTrue();
        addNewJobPage.setSdoCode(JobData.sdoCode);

        //AC: 4
        //Step: 8
        addNewJobPage.setJobLocation(JobData.location);

        //AC: 5
        //Step: 9
        assert_().withFailureMessage("Cases are expected to be selected from drop down")
                .that(addNewJobPage.getCaseDropDownSize()).isGreaterThan(0);
        addNewJobPage.selectCaseTypeByVisibleText(JobData.caseType);

        //AC: 6
        //Step: 10
        assert_().withFailureMessage("Maximum job location character size is expected to be " + maxCharacters)
                .that(addNewJobPage.getJobLocationCharacterSize()
                        .equals(maxCharacters))
                .isTrue();
        assert_().withFailureMessage("Maximum job description character size is expected to be " + maxCharacters)
                .that(addNewJobPage.getJobDescriptionCharacterSize()
                        .equals(maxCharacters))
                .isTrue();

        //AC: 7 and AC: 8
        //Step: 11
        addNewJobPage.setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate);
        assert_().withFailureMessage("Save Button is expected to be be clickable")
                .that(addNewJobPage.isSaveButtonClickable())
                .isTrue();

        //AC: 7, AC: 8, AC: 9 and AC: 10
        //Step: 12 and 13
        LocalDate lesserEtaDate = LloydsRegister.localDate.minusDays(1);
        Date lesserThanEtaDate = Date.valueOf(lesserEtaDate);
        addNewJobPage.setRequestedAttendanceDate(
                String.valueOf(FRONTEND_TIME_FORMAT.format(lesserThanEtaDate)));
        assert_().withFailureMessage("Requested attendance date error icon is expected to be displayed")
                .that(addNewJobPage.isRequestedAttendanceDateErrorIconDisplayed(AddNewJobPage
                        .type.ERROR) || addNewJobPage.getValidationMessageText()
                        .equals("Requested Attendance Date' must be between the 'ETA' and 'ETD' Dates"))
                .isTrue();

        LocalDate greaterEtdDate = LloydsRegister.localDate.plusDays(2);
        Date greaterThanEtdDate = Date.valueOf(greaterEtdDate);
        addNewJobPage.setRequestedAttendanceDate(
                String.valueOf(FRONTEND_TIME_FORMAT.format(greaterThanEtdDate)));
        assert_().withFailureMessage("Requested attendance date error icon is expected to be displayed")
                .that(addNewJobPage.isRequestedAttendanceDateErrorIconDisplayed(AddNewJobPage
                        .type.ERROR) || addNewJobPage.getValidationMessageText()
                        .equals("Requested Attendance Date' must be between " +
                                "the 'ETA' and 'ETD' Dates"))
                .isTrue();

        //AC: 11
        //Step: 14 and 15
        addNewJobPage.clearSdoTextField()
                .then().setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate)
                .then().clickCancelButton();
        jobsSubPage.clickAddNewJobButton();
        addNewJobPage.clickNavigateBackButton();

        //AC: 12 and AC:13
        //Step: 16
        jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.addNewJob();

        String jobId = viewJobDetailsPage.getHeader().getJobId();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        AllJobElement allJobElement = jobsSubPage.getAllJobsByJobId(jobId);
        assert_().withFailureMessage("Job Status is expected to be Assigned")
                .that(allJobElement.getJobStatus().equals(jobStatus))
                .isTrue();
    }
}
