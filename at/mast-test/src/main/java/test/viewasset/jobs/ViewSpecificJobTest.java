package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.HeaderPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class ViewSpecificJobTest extends BaseTest
{

    @Test(description = "US 10.8 View a specific job")
    @Issue("LRT-1877")
    public void viewSpecificJobTest()
    {

        final int assetId = 501;
        final int jobIdLength = 9;
        final String jobStatus = "SDO Assigned";
        final String expectedCancelJobInformationText = "Cancel the job if the service request needs to be withdrawn without any T&E recorded";
        final String expectedAbortJobInformationText = "Abort the job if the service request needs to be withdrawn with T&E recorded";
        final String expectedJobScopeHeaderText = "Job scope and service schedule";
        final String expectedJobTeamHeaderText = "Job team";
        final String expectedJobUpdatesReportingHeaderText = "Job updates & reporting";
        final String expectedTechnicalReviewHeaderText = "Technical review";
        final String expectedEndorsementHeaderText = "Endorsement";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        //AC: 2
        //Step: 3
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.addNewJob();

        //AC: 3
        //Step: 4
        HeaderPage headerPage = viewJobDetailsPage.getHeader();
        String jobId = headerPage.getJobId();
        assert_().withFailureMessage("Job ID length is expected to be 9 ")
                .that(jobId.length()).isEqualTo(jobIdLength);

        jobsSubPage = headerPage.clickNavigateBackButton();
        AllJobElement allJobElement = jobsSubPage.getAllJobsByJobId(jobId);
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();
        viewJobDetailsPage.getHeader();
        //AC: 4
        //Step: 5
        assert_().withFailureMessage("Job ID is expected to be displayed ")
                .that(headerPage.getJobId().equals(jobId))
                .isTrue();
        assert_().withFailureMessage("Location is expected to be displayed ")
                .that(headerPage.getLocation().equals(JobData.location))
                .isTrue();
        assert_().withFailureMessage("Start date is expected to be displayed ")
                .that(headerPage.getStartDate().equals(JobData.strEtdDate))
                .isTrue();
        assert_().withFailureMessage("Job status is expected to be displayed ")
                .that(headerPage.getStatus().equals(jobStatus))
                .isTrue();

        //AC: 5, AC: 6, AC: 7 and AC: 8
        //Step: 6, 7, 8 and 9
        assert_().withFailureMessage("Cancel job information text is expected to be displayed ")
                .that(headerPage.getCancelJobInformationText()
                        .equals(expectedCancelJobInformationText))
                .isTrue();
        assert_().withFailureMessage("Abort job information text is expected to be displayed ")
                .that(headerPage.getAbortJobInformationText()
                        .equals(expectedAbortJobInformationText))
                .isTrue();

        //AC: 9
        //Step: 10
        assert_().withFailureMessage("Service type field is expected to be blank ")
                .that(headerPage.getServiceTypeText().isEmpty())
                .isTrue();

        //AC: 10 and AC: 11
        //Step: 11 and 12
        assert_().withFailureMessage("Job Scope section is expected to be displayed ")
                .that(viewJobDetailsPage.getJobScopeHeaderText()
                        .equals(expectedJobScopeHeaderText))
                .isTrue();

        assert_().withFailureMessage("Job Team section is expected to be displayed ")
                .that(viewJobDetailsPage.getJobTeamHeaderText()
                        .equals(expectedJobTeamHeaderText))
                .isTrue();

        assert_().withFailureMessage("Job updates & reporting section is expected to be displayed ")
                .that(viewJobDetailsPage.getJobUpdatesReportingHeaderText()
                        .equals(expectedJobUpdatesReportingHeaderText))
                .isTrue();

        assert_().withFailureMessage("Technical review section is expected to be displayed ")
                .that(viewJobDetailsPage.getTechnicalReviewHeaderText()
                        .equals(expectedTechnicalReviewHeaderText))
                .isTrue();

        assert_().withFailureMessage("Endorsement review section is expected to be displayed ")
                .that(viewJobDetailsPage.getEndorsementHeaderText()
                        .equals(expectedEndorsementHeaderText))
                .isTrue();

        //AC: 12
        //Step: 12
        viewJobDetailsPage.getHeader();
        headerPage.clickAttachmentButton();
    }
}
