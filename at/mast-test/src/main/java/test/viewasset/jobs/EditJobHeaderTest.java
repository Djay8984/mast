package test.viewasset.jobs;

import static com.google.common.truth.Truth.assert_;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.frameworkium.core.ui.tests.BaseTest;

import model.asset.Asset;
import model.asset.AssetCase;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.EditJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

public class EditJobHeaderTest extends BaseTest
{

    @DataProvider(name = "JobStatus")
    public Object[][] jobStatus()
    {
        return new Object[][]{
                              {"Aborted", false},
                              {"Awaiting Endorser Assignment", false},
                              {"Awaiting TR Assignment", false},
                              {"Cancelled", false},
                              {"Closed", false},
                              {"Resource Assigned", true},
                              {"SDO Assigned", true},
                              {"Under Endorsement", false},
                              {"Under Reporting", true},
                              {"Under Survey", true},
                              {"Under TR", false}
        };
    }

    @Test(description = "US - 10.9 Edit job header", dataProvider = "JobStatus")
    @Issue("LRT-1899")
    public void editJobHeaderTest(final String status, final boolean isEditable)
    {
        final int assetId = 501;
        final Asset asset = new Asset(assetId);

        final WorkHubPage workHubPage = WorkHubPage.open();
        final AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        final SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(asset.getDto().getName());
        searchFilterSubPage.clickAssetSearchApplyButton();

        final ViewAssetPage viewAssetPage = allAssetsSubPage
                .getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton();

        JobsSubPage jobs = viewAssetPage.clickJobsTab();
        while (jobs.isLoadMoreButtonPresent())
        {
            jobs.clickLoadMoreButton();
        }
        ViewJobDetailsPage jobDetails = jobs
                .getMyJobsByJobStatus(status)
                .clickNavigateToJobArrowButton();

        final String jobId = jobDetails.getHeader().getJobId();

        // AC2
        if (isEditable)
        {
            assert_().withFailureMessage("The user should be able to click on the edit button")
                    .that(jobDetails.getHeader().isEditButtonEnabled()).isTrue();

            EditJobPage editJob = jobDetails.getHeader().clickEditButton();

            // AC3
            if (status.equals("Under Survey"))
            {
                assert_().withFailureMessage("The SDO text field should be disabled")
                        .that(editJob.isSdoTextFieldEnabled()).isFalse();
            }

            // AC1
            final String location = "Automated location";
            editJob.enterLocation(location);
            assert_().withFailureMessage("The user should be able to edit the service location")
                    .that(editJob.getLocation()).isEqualTo(location);

            final AssetCase assetCase = new AssetCase(assetId);
            final CaseDto[] cases = assetCase.getDtoArray();
            final String aCase = cases[1].getId().toString();

            editJob.selectCase(aCase);
            assert_().withFailureMessage("The user should be able to edit the case")
                    .that(editJob.getCase()).contains(aCase);

            final String eta = "12 Oct 2016";
            editJob.enterEtaDate(eta);
            assert_().withFailureMessage("The user should be able to edit the ETA date")
                    .that(editJob.getEtaDate()).isEqualTo(eta);

            final String etd = "12 Nov 2016";
            editJob.enterEtdDate(etd);
            assert_().withFailureMessage("The user should be able to edit the ETD date")
                    .that(editJob.getEtdDate()).isEqualTo(etd);

            final String requestedAttendanceDate = "24 Oct 2016";
            editJob.enterRequestedAttendanceDate(requestedAttendanceDate);
            assert_().withFailureMessage("The user should be able to edit the requested attendance date")
                    .that(editJob.getRequestedAttendanceDate()).isEqualTo(requestedAttendanceDate);

            // AC4
            final String description = "This is a description for a job";
            editJob.enterDescription(description);
            assert_().withFailureMessage("The user should be able to edit the job description")
                    .that(editJob.getDescription()).isEqualTo(description);

            // AC5
            editJob.enterDescription("");
            assert_().withFailureMessage("The user should not be able to save the job details")
                    .that(editJob.isSaveButtonEnabled()).isFalse();

            // AC6
            if (!status.equals("Under Survey"))
            {
                editJob.enterDescription(description);
                editJob.enterSDO("Hull");
                assert_().withFailureMessage("The user should be able to save the job details")
                        .that(editJob.isSaveButtonEnabled()).isTrue();
                jobDetails = editJob.clickSaveButton();

                AddOrEditJobTeamPage jobTeam = jobDetails.clickAddOrGoToJobTeamButton();
                jobTeam.setLeadSurveyorTextBox("Choi Leng Lead Surveyor");
                jobDetails = jobTeam.clickSaveButton();

                editJob = jobDetails.getHeader().clickEditButton();
                editJob.enterSDO("Southampton");
                jobDetails = editJob.clickSaveButton();

                jobTeam = jobDetails.clickAddOrGoToJobTeamButton();

                assert_().withFailureMessage("The lead surveyor should have been cleared when the SDO changed")
                        .that(jobTeam.getLeadSurveyor()).isEqualTo("");

                jobDetails = jobTeam.clickCancelButton();

                // AC8
                assert_().withFailureMessage("When the lead surveyor is blank, then the job status should revert to SDO assigned")
                        .that(jobDetails.getHeader().getStatus()).isEqualTo("SDO Assigned");

                // AC7
                assert_().withFailureMessage("The job team button should say Add job team")
                        .that(jobDetails.getJobTeamButtonText()).isEqualTo("Add job team");
            }
        }
        else
        {
            assert_().withFailureMessage("The user should not be able to click on the edit button")
                    .that(jobDetails.getHeader().isEditButtonEnabled()).isFalse();
        }
    }
}
