package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import model.surveyor.Surveyor;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class AmendJobTeamTest extends BaseTest
{

    @Test(description = "US12.24 - Amend the Job team so that I can make the necessary updates")
    @Issue("LRT-2461")

    public void amendJobTeamTest()
    {

        int assetId = 501;
        String noMatchesFoundErrorMessage = "No matches found";
        String duplicateErrorMessage = "Duplicate names are not permitted";
        String requiredErrorMessage = "Lead surveyor is required";
        String status = "Resource Assigned";

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByAssetId(assetId).clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();

        //AC 1,2
        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        assert_().withFailureMessage("The Lead Surveyor field is expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isLeadSurveyorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The SDO Coordinator field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isSdoCoordinatorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The Authorising Surveyor field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isAuthorisingSurveyorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The Fleet services Specialist field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isFleetServicesSpecialistTextBoxEnabled())
                .isTrue();

        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        Surveyor leadSurveyor = new Surveyor(2);
        String leadSurveyorName = leadSurveyor.getDto().getName();
        Surveyor authSurveyor = new Surveyor(3);
        String authSurveyorName = authSurveyor.getDto().getName();
        Surveyor fssSpecialist = new Surveyor(4);
        String fssSpecialistName = fssSpecialist.getDto().getName();

        addOrEditJobTeamPage.clearLeadSurveyorTextbox()
                .then().setLeadSurveyorTextBox(surveyorName)
                .then().clearLeadSurveyorTextbox()
                .then().setLeadSurveyorTextBoxWithoutEnter(surveyorName + "123abcJamesIsaAlphabet");

        assert_().withFailureMessage("Error message " + noMatchesFoundErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getInLineErrorInLeadSurveyorTextBox())
                .contains(noMatchesFoundErrorMessage);

        addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().setSdoCoordinatorTextBox(leadSurveyorName)
                .then().setLeadSurveyorTextBox(leadSurveyorName);
        //AC 3
        assert_().withFailureMessage("Error message " + duplicateErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getDuplicateErrorMessageInLeadSurveyorTextBox())
                .contains(duplicateErrorMessage);
        //AC 4,5
        addOrEditJobTeamPage.clearLeadSurveyorTextbox();
        assert_().withFailureMessage("Error message " + requiredErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getErrorMessageInLeadSurveyorTextBox())
                .contains(requiredErrorMessage);

        addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName.substring(0, 3))
                .then().setAuthorisingSurveyorTextBox(authSurveyorName)
                .then().setFleetServicesSpecialistTextBox(fssSpecialistName);

        addOrEditJobTeamPage.clickSaveButton();

        //AC 5
        viewJobDetailsPage.getHeader().clickNavigateBackButton();
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        AllJobElement allJobElementBasedOnStatus = jobsSubPage.getMyJobsByJobStatus(status);
        allJobElementBasedOnStatus.clickNavigateToJobArrowButton();
        viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        addOrEditJobTeamPage.setLeadSurveyorTextBox(leadSurveyorName)
                .then().setSdoCoordinatorTextBox(surveyorName)
                .then().setAuthorisingSurveyorTextBox(authSurveyorName)
                .then().setFleetServicesSpecialistTextBox(fssSpecialistName)
                .then().setLeadSurveyorTextBox("");

        assert_().withFailureMessage("Error message " + requiredErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getErrorMessageInLeadSurveyorTextBox())
                .contains(requiredErrorMessage);

        addOrEditJobTeamPage.clickCancelButton();
    }
}
