package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.conditionofclass.AddConditionOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.element.DataElement;
import viewasset.sub.jobs.details.conditionofclass.modal.AddCocConfirmationPage;
import viewasset.sub.jobs.details.conditionofclass.searchfilter.ConditionOfClassPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class SearchViewCocDefectFromJobTest extends BaseTest
{
    final int assetId = 501;

    @Test(description = "Story 11.33 - As a user I want to carry out a search " +
            "and view search results on the codicil and/or defect specific " +
            "page of the Job so that I can find the instance I am looking for")
    @Issue("LRT-2522")

    public void searchViewCocDefectFromJobTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate)
                .then().clickSaveButton();

        ConditionsOfClassPage conditionsOfClassPage
                = viewJobDetailsPage.clickGoToCocsButton();
        ConditionsOfClassPage.JobRelatedTable jobRelatedTable
                = conditionsOfClassPage.getJobRelatedTable();

        //Add job related coc if not present
        if (jobRelatedTable.getRows().size() == 0)
        {
            AddConditionOfClassPage addConditionOfClassPage
                    = conditionsOfClassPage.clickAddNewButton();
            AddCocConfirmationPage addCocConfirmationPage = addConditionOfClassPage.setTitle(CocData.title)
                    .setDescription(CocData.description)
                    .selectInheritedCheckBox()
                    .setImposedDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                    .setDueDate(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS))
                    .clickLrAndCustomerButton()
                    .clickSaveButton(AddCocConfirmationPage.class);
            addCocConfirmationPage.clickOkButton();
        }

        jobRelatedTable = conditionsOfClassPage.getJobRelatedTable();
        List<String> jobRelatedItemDefaultNames
                = jobRelatedTable.getRows()
                .stream().map(DataElement::getCocTitle)
                .collect(Collectors.toList());

        ConditionsOfClassPage.OthersTable othersTable
                = conditionsOfClassPage.getOthersTable();
        List<String> otherItemDefaultNames
                = othersTable.getRows().stream()
                .map(DataElement::getCocTitle)
                .collect(Collectors.toList());

        //AC: 1 and AC: 2
        //Step: 2 and 3
        ConditionOfClassPage searchConditionOfClassPage
                = conditionsOfClassPage.clickSearchFilterConditionsOfClassButton();
        String expectedRGBA = searchConditionOfClassPage.getViewAllBackgroundColour();

        assert_().withFailureMessage("View All Button is expected to selected")
                .that(searchConditionOfClassPage.getViewAllBackgroundColour().equals(expectedRGBA))
                .isTrue();
        assert_().withFailureMessage("View Job Related Only is not expected to be selected")
                .that(searchConditionOfClassPage.getViewJobRelatedOnlyBackgroundColour()
                        .equals(expectedRGBA))
                .isFalse();
        assert_().withFailureMessage("View Others Only is not expected to be selected")
                .that(searchConditionOfClassPage.getViewOtherOnlyBackgroundColour()
                        .equals(expectedRGBA))
                .isFalse();

        //AC:3
        //Step: 4 and 5
        searchConditionOfClassPage.clickViewJobRelatedOnlyButton();
        searchConditionOfClassPage.clickSearchButton();
        conditionsOfClassPage.getJobRelatedTable();
        List<String> jobRelatedItemSearchResultNames
                = jobRelatedTable.getRows()
                .stream()
                .map(DataElement::getCocTitle)
                .collect(Collectors.toList());
        assert_().withFailureMessage("Searched results are expected to be displayed ")
                .that(jobRelatedItemSearchResultNames)
                .isEqualTo(jobRelatedItemDefaultNames);

        assert_().withFailureMessage("Job Related Section Header is expected to be Displayed  ")
                .that(conditionsOfClassPage.isJobRelatedSectionHeaderDisplayed())
                .isTrue();
        conditionsOfClassPage.clickSearchFilterConditionsOfClassButton();
        assert_().withFailureMessage("View Job Related Only is expected to be selected")
                .that(searchConditionOfClassPage.getViewJobRelatedOnlyBackgroundColour())
                .isEqualTo(expectedRGBA);

        searchConditionOfClassPage.clickViewOtherOnlyButton();
        searchConditionOfClassPage.clickSearchButton();
        List<String> othersItemSearchResultNames = othersTable.getRows()
                .stream().map(DataElement::getCocTitle)
                .collect(Collectors.toList());
        assert_().withFailureMessage("Searched results are expected to be displayed ")
                .that(othersItemSearchResultNames)
                .isEqualTo(otherItemDefaultNames);

        assert_().withFailureMessage("Others Section Header is expected to be Displayed ")
                .that(conditionsOfClassPage.isOthersSectionHeaderDisplayed())
                .isTrue();

        conditionsOfClassPage.clickSearchFilterConditionsOfClassButton();
        assert_().withFailureMessage("View Others Only is expected to be selected")
                .that(searchConditionOfClassPage.getViewOtherOnlyBackgroundColour())
                .isEqualTo(expectedRGBA);

        //Step: 6
        searchConditionOfClassPage.clickCategoryFilterButton();
        searchConditionOfClassPage.clickDueDateFilterButton();
        searchConditionOfClassPage.clickStatusFilterButton();
        searchConditionOfClassPage.clickSdoFilterButton();
        searchConditionOfClassPage.clickConfidentialityFilterButton();

        //AC: 4
        //Step: 7
        String title = CocData.title;
        searchConditionOfClassPage.setSearchText(title).clickSearchButton();
        conditionsOfClassPage.getJobRelatedTable();
        assert_().withFailureMessage("search Coc is expected to be displayed")
                .that(jobRelatedTable.getRows().stream()
                        .filter(e -> e.getCocTitle().equals(title))
                        .findFirst().isPresent())
                .isTrue();
    }
}
