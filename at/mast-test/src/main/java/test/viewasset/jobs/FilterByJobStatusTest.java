package test.viewasset.jobs;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import model.asset.Asset;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.conditionofclass.sub.JobStatusFilterSubPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.ACTIVE_JOB_STATUSES;
import static model.referencedata.ReferenceDataMap.RefData.JOB_STATUSES;
import static model.referencedata.ReferenceDataMap.Subset.JOB;

public class FilterByJobStatusTest extends BaseTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {502}
        };
    }

    @Test(
            description = "Story 10.5 As a user I want to filter by Job status" +
                    " so that I can find all the jobs that are in a specific status",
            dataProvider = "Assets"
    )
    @Issue("LRT-1731")
    public void filterByJobStatusTest(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        //find asset with all job statuses
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName()).clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        JobStatusFilterSubPage jobStatusFilterSubPage = jobsSubPage.clickSearchFilterJobsButton().then().clickJobStatusFilterButton();

        //AC1 default status
        ACTIVE_JOB_STATUSES().forEach(
                jobStatusName -> assert_().withFailureMessage("Job status [" + jobStatusName + "] is checked")
                        .that(jobStatusFilterSubPage.isJobStatusChecked(jobStatusName)).isTrue()
        );

        //AC2  select job in Cancelled , Aborted , Closed
        jobStatusFilterSubPage.clickClear();
        jobStatusFilterSubPage.selectJobStatus("Cancelled");
        assert_().withFailureMessage("Job status [Cancelled] is checked")
                .that(jobStatusFilterSubPage.isJobStatusChecked("Cancelled")).isTrue();

        jobStatusFilterSubPage.clickClear();
        jobStatusFilterSubPage.selectJobStatus("Aborted");
        assert_().withFailureMessage("Job status [Aborted] is checked")
                .that(jobStatusFilterSubPage.isJobStatusChecked("Aborted")).isTrue();

        jobStatusFilterSubPage.clickClear();
        jobStatusFilterSubPage.selectJobStatus("Closed");
        assert_().withFailureMessage("Job status [Closed] is checked")
                .that(jobStatusFilterSubPage.isJobStatusChecked("Closed")).isTrue();

        //AC3 Select all options in one go
        jobStatusFilterSubPage.clickSelectAll();
        assert_().withFailureMessage("All job statuses are selected")
                .that(jobStatusFilterSubPage.isAllJobStatusChecked()).isTrue();

        //AC4  Select options individualy
        jobStatusFilterSubPage.verifyIfAllDisplayJobStatusCheckedInvidually();

        //AC5  Validate the ordering of the job statuses is as per ref data
        assert_().withFailureMessage("Jobs statuses are displayed as per reference data")
                .that(allRefDataJobStatus()).containsExactlyElementsIn(jobStatusFilterSubPage.allDisplayJobStatusName());

    }

    private List<String> allRefDataJobStatus()
    {
        return new ReferenceDataMap(JOB, JOB_STATUSES)
                .getReferenceDataArrayList()
                .stream()
                .map(ReferenceDataDto::getName)
                .collect(Collectors.toList());

    }

}
