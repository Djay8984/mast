package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import model.surveyor.Surveyor;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.DueAndOverduePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceSchedulePage;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.details.scope.addtojobscope.element.CocAinAnElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.RowElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ServiceElement;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class SelectServicesInJobScopePageTest extends BaseTest
{

    final String sdoCode = "Chittagong";
    final String jobLocation = "Auto KL" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
    final String jobDescription = "Auto KL Test Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
    LocalDate currentDate = LocalDate.now();
    Date etaDate = Date.valueOf(currentDate);
    final String strEtaDate = String.valueOf(FRONTEND_TIME_FORMAT.format(etaDate));
    LocalDate date = currentDate.plusDays(1);
    Date etdDate = Date.valueOf(date);
    final String strEtdDate = String.valueOf(FRONTEND_TIME_FORMAT.format(etdDate));

    @Test(description = "US 10.12 - As a user on the job scope page, I want to select due or overdue services and/or services from service schedule "
            +
            "so that I can focus on those services that more urgent or from the service schedule.LRT- 1908")
    @Issue("LRT-2507")
    public void SelectServicesInJobScopePage()
    {
        int assetID = 501;
        final String caseType = "123";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy");

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID))
                .then().clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID)
                        .getDto()
                        .getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();

        ViewJobDetailsPage viewJobDetailsPage =
                addNewJobPage.setSdoCode(sdoCode)
                        .then().selectCaseTypeByVisibleText(caseType)
                        .then().setJobLocation(jobLocation)
                        .then().setJobDescription(jobDescription)
                        .then().setEtaDate(strEtaDate)
                        .then().setEtdDate(strEtdDate)
                        .then().setRequestedAttendanceDate(strEtdDate)
                        .then().clickSaveButton();

        String jobId = viewJobDetailsPage.getHeader().getJobId();

        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();

        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }

        viewJobDetailsPage = jobsSubPage.getAllJobsByJobId("059879879")
                .clickNavigateToJobArrowButton();

        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage
                .clickAddOrGoToJobTeamButton();

        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        viewJobDetailsPage = addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().clickSaveButton();

        //AC 1
        AddToJobScopePage addToJobScopePage = viewJobDetailsPage
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        addToJobScopePage.clickDueAndOverdueTab();

        DueAndOverduePage dueAndOverduePage = addToJobScopePage.getDueAndOverdueSubPage();

        //AC1

        DueAndOverduePage.ClassificationTable cdodt = dueAndOverduePage.getClassificationTable();
        DueAndOverduePage.StatutoryTable sdodt = dueAndOverduePage.getStatutoryTable();
        DueAndOverduePage.ConditionsOfClassTable cocdodt = dueAndOverduePage.getConditionOfClassTable();
        DueAndOverduePage.ActionableItemsTable aidodt = dueAndOverduePage.getActionableItemsTable();
        DueAndOverduePage.AssetNotesTable andodt = dueAndOverduePage.getAssetNotesTable();

        //AC 3

        cdodt.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        List<ServiceElement> serviceElements = new ArrayList<ServiceElement>();
        List<RowElement> rowElements = new ArrayList<RowElement>();

        cdodt.getRows()
                .stream()
                .forEach(e ->
                        rowElements.addAll(e.getChildren()));

        sdodt.getRows()
                .stream()
                .forEach(e ->
                        rowElements.addAll(e.getChildren()));

        rowElements.stream().forEach(e -> serviceElements.addAll(e.getServiceRows()));

        List<CocAinAnElement> cocdodes = cocdodt.getRows();
        List<CocAinAnElement> aidodes = aidodt.getRows();
        List<CocAinAnElement> andodes = andodt.getRows();

        List<CocAinAnElement> allCoCAiAnElements = new ArrayList<CocAinAnElement>();
        if (cocdodt.getRows() != null)
        {
            allCoCAiAnElements.addAll(cocdodt.getRows());
        }
        if (aidodt.getRows() != null)
        {
            allCoCAiAnElements.addAll(aidodt.getRows());
        }
        if (andodt.getRows() != null)
        {
            allCoCAiAnElements.addAll(andodt.getRows());
        }

        if (allCoCAiAnElements.size() != 0)
        {
            assert_().withFailureMessage("Warning message is expected to display in all entries of Due and Overdue page")
                    .that(allCoCAiAnElements.stream()
                            .filter(e -> e.getWarningMessage().isEmpty()).count())
                    .isEqualTo(0);
            //AC 2
            //AC 4
            for (int i = 0; i < allCoCAiAnElements.size(); i++)
            {

                LocalDate localLowerRangeDate = LocalDate.parse(serviceElements
                        .get(i)
                        .getLowerRangeDate()
                        .toString(), formatter);
                LocalDate localUpperRangeDate = LocalDate.parse(serviceElements
                        .get(i)
                        .getUpperRangeDate()
                        .toString(), formatter);

                if (serviceElements.get(i).getWarningMessage().contains("Over"))
                {
                    assert_().withFailureMessage("OverDue  - lower range date and upper range date is expected to be displayed correctly")
                            .that(currentDate.isAfter(localUpperRangeDate))
                            .isTrue();
                }
                else
                {
                    assert_().withFailureMessage("Due  - lower range date and upper range date is expected to be displayed correctly")
                            .that((currentDate.isBefore(localUpperRangeDate) || currentDate.equals(localUpperRangeDate))
                                    && (currentDate.isAfter(localLowerRangeDate)))
                            .isTrue();
                }

                assert_().withFailureMessage("Service Name is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getServiceName())
                        .isNotNull();
                assert_().withFailureMessage("Service Code is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getServiceCode())
                        .isNotNull();
                assert_().withFailureMessage("Warning message is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getWarningMessage())
                        .isNotNull();
                assert_().withFailureMessage("Due date is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getDueDate())
                        .isNotNull();
                assert_().withFailureMessage("LowerRange Date message is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getLowerRangeDate())
                        .isNotNull();
                assert_().withFailureMessage("UpperRange Date message is expected to be displayed")
                        .that(allCoCAiAnElements.get(i).getUpperRangeDate())
                        .isNotNull();
            }
        }

        assert_().withFailureMessage("Warning message is expected to display in all entries of Due and Overdue page")
                .that(serviceElements.stream()
                        .filter(e -> e
                                .getWarningMessage().isEmpty()).count()).isEqualTo(0);

        //AC 2
        //AC 4
        for (int i = 0; i < serviceElements.size(); i++)
        {

            LocalDate localLowerRangeDate = LocalDate.parse(serviceElements
                    .get(i)
                    .getLowerRangeDate()
                    .toString(), formatter);
            LocalDate localUpperRangeDate = LocalDate.parse(serviceElements
                    .get(i)
                    .getUpperRangeDate()
                    .toString(), formatter);

            if (serviceElements.get(i).getWarningMessage().contains("Over"))
            {
                assert_().withFailureMessage("OverDue  - lower range date and upper range date is expected to be displayed correctly")
                        .that(currentDate.isAfter(localUpperRangeDate))
                        .isTrue();
            }
            else
            {
                assert_().withFailureMessage("Due  - lower range date and upper range date is expected to be displayed correctly")
                        .that((currentDate.isBefore(localUpperRangeDate) || currentDate.equals(localUpperRangeDate))
                                && (currentDate.isAfter(localLowerRangeDate)))
                        .isTrue();
            }
            assert_().withFailureMessage("Service Name is expected to be displayed")
                    .that(serviceElements.get(i).getServiceName())
                    .isNotNull();
            assert_().withFailureMessage("Service Code is expected to be displayed")
                    .that(serviceElements.get(i).getServiceCode())
                    .isNotNull();
            assert_().withFailureMessage("Warning message is expected to be displayed")
                    .that(serviceElements.get(i).getWarningMessage())
                    .isNotNull();
            assert_().withFailureMessage("Due date is expected to be displayed")
                    .that(serviceElements.get(i).getDueDate())
                    .isNotNull();
            assert_().withFailureMessage("LowerRange Date message is expected to be displayed")
                    .that(serviceElements.get(i).getLowerRangeDate())
                    .isNotNull();
            assert_().withFailureMessage("UpperRange Date message is expected to be displayed")
                    .that(serviceElements.get(i).getUpperRangeDate())
                    .isNotNull();
        }

        //AC 5
        serviceElements.get(0).selectCheckBox();
        if (allCoCAiAnElements.size() != 0)
        {
            cocdodes.get(0).selectCheckBox();
            aidodes.get(0).selectCheckBox();
            andodes.get(0).selectCheckBox();
        }

        dueAndOverduePage.getHeader().clickSelectAllLink();

        //AC 6
        ServiceSchedulePage serviceSchedulePage = addToJobScopePage.clickServiceScheduleTab();

        ServiceSchedulePage.ClassificationTable csspt = serviceSchedulePage.getClassificationTable();
        ServiceSchedulePage.StatutoryTable ssspt = serviceSchedulePage.getStatutoryTable();
        ServiceSchedulePage.ConditionsOfClassTable cocsspt = serviceSchedulePage.getConditionOfClassTable();
        ServiceSchedulePage.ActionableItemsTable aisspt = serviceSchedulePage.getActionableItemsTable();
        ServiceSchedulePage.AssetNotesTable ansspt = serviceSchedulePage.getAssetNotesTable();

        List<ServiceElement> serviceSSPElements = new ArrayList<ServiceElement>();
        List<RowElement> rowSSPElements = new ArrayList<RowElement>();

        csspt.getRows()
                .stream()
                .forEach(e ->
                        rowSSPElements.addAll(e.getChildren()));

        ssspt.getRows().stream().forEach(e -> rowSSPElements.addAll(e.getChildren()));

        rowSSPElements.stream().forEach(e -> serviceSSPElements.addAll(e.getServiceRows()));

        List<CocAinAnElement> allCoCAiAnSSPElements = new ArrayList<CocAinAnElement>();
        if (cocsspt.getRows() != null)
        {
            allCoCAiAnSSPElements.addAll(cocsspt.getRows());
        }
        if (aisspt.getRows() != null)
        {
            allCoCAiAnSSPElements.addAll(aisspt.getRows());
        }
        if (ansspt.getRows() != null)
        {
            allCoCAiAnSSPElements.addAll(ansspt.getRows());
        }

        if (allCoCAiAnSSPElements.size() != 0)
        {
            assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                    allCoCAiAnSSPElements
                            .stream()
                            .filter(e -> !(e.getCheckbox().isSelected()))
                            .count())
                    .isNotEqualTo(allCoCAiAnSSPElements.size());
        }

        assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                serviceSSPElements
                        .stream()
                        .filter(e -> !(e.getCheckbox().isSelected()))
                        .count())
                .isNotEqualTo(serviceSSPElements.size());

        //AC 6
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

        BaseServiceTable capt = serviceAllPage.getClassificationTable();
        BaseServiceTable sapt = serviceAllPage.getStatutoryTable();
        ServiceAllPage.ConditionsOfClassTable cocapt = serviceAllPage.getConditionsOfClassTable();
        ServiceAllPage.ActionableItemsTable aiapt = serviceAllPage.getActionableItemsTable();
        ServiceAllPage.AssetNotesTable anapt = serviceAllPage.getAssetNotesTable();

        List<ServiceElement> serviceAPElements = new ArrayList<ServiceElement>();
        List<RowElement> rowAPElements = new ArrayList<RowElement>();

        capt.getRows()
                .stream()
                .forEach(e ->
                        rowAPElements.addAll(e.getChildren()));

        sapt.getRows().stream().forEach(e ->
                rowAPElements
                        .addAll(e.getChildren())
        );

        rowAPElements.stream().forEach(e ->
        {
            e.clickPlusOrMinusIcon();
            serviceAPElements.addAll(e.getServiceRows());
        });

        List<CocAinAnElement> allCoCAiAnAPElements = new ArrayList<CocAinAnElement>();

        if (cocapt.getRows() != null)
        {
            allCoCAiAnAPElements.addAll(cocapt.getRows());
        }
        if (aiapt.getRows() != null)
        {
            allCoCAiAnAPElements.addAll(aiapt.getRows());
        }
        if (anapt.getRows() != null)
        {
            allCoCAiAnAPElements.addAll(anapt.getRows());
        }

        if (allCoCAiAnAPElements.size() != 0)
        {
            assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                    allCoCAiAnAPElements
                            .stream()
                            .filter(e -> !(e.getCheckbox().isSelected()))
                            .count())
                    .isNotEqualTo(allCoCAiAnAPElements.size());
        }

        assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                serviceAPElements
                        .stream()
                        .filter(e -> !(e.getCheckbox().isSelected()))
                        .count())
                .isNotEqualTo(serviceAPElements.size());

        //AC 14
        dueAndOverduePage = addToJobScopePage.clickDueAndOverdueTab();

        if (allCoCAiAnElements.size() != 0)
        {
            assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                    allCoCAiAnElements
                            .stream()
                            .filter(e -> !(e.getCheckbox().isSelected()))
                            .count())
                    .isEqualTo(allCoCAiAnAPElements.size());
        }

        assert_().withFailureMessage("The services are not expected be selected in Service schedule Page").that(
                serviceElements
                        .stream()
                        .filter(e -> !(e.getCheckbox().isSelected()))
                        .count())
                .isEqualTo(serviceAPElements.size());

        //AC 7
        serviceElements.get(0).deSelectCheckBox();
        dueAndOverduePage.getHeader().clickClearAllLink();

        //AC 8
        serviceElements.get(0).selectCheckBox();
        if (allCoCAiAnElements.size() != 0)
        {
            cocdodes.get(0).selectCheckBox();
            aidodes.get(0).selectCheckBox();
            andodes.get(0).selectCheckBox();
        }

        assert_().withFailureMessage("The number of services selected is expected to display the correct value")
                .that(dueAndOverduePage.getNumberOfServicesSelected())
                .isEqualTo(1);

        dueAndOverduePage.getHeader().clickSelectAllLink();
        assert_().withFailureMessage("The number of services selected is expected to display the correct value").that(
                dueAndOverduePage.getNumberOfServicesSelected())
                .isNotNull();

        dueAndOverduePage.getHeader().clickClearAllLink();

        serviceSchedulePage = addToJobScopePage.clickServiceScheduleTab();
        serviceSSPElements.get(0).selectCheckBox();

        //AC 12, 9
        ConfirmModalWindow confirmModalWindow = dueAndOverduePage.getFooter()
                .clickAddButton(ConfirmModalWindow.class);

        JobScopePage jobScopePage = confirmModalWindow.clickOkButton(JobScopePage.class);

        jobScopePage.clickSelectAllLink();
        confirmModalWindow = jobScopePage.clickRemoveSelectedLink();
        confirmModalWindow.clickOkButton(JobScopePage.class);
    }

    @Test()
    public void SelectServicesInJobScopePageWithNoServices()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        final String caseType = "6";
        String assetID = "427";

        String noServiceInServiceSchedule = "No services in the service schedule for this asset.";
        String noServiceInDueAndOverdue = "No due or overdue services for this asset.";

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID))
                .then().clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByIndex(0)
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();

        ViewJobDetailsPage viewJobDetailsPage =
                addNewJobPage.setSdoCode(sdoCode)
                        .then().selectCaseTypeByVisibleText(caseType)
                        .then().setJobLocation(jobLocation)
                        .then().setJobDescription(jobDescription)
                        .then().setEtaDate(strEtaDate)
                        .then().setEtdDate(strEtdDate)
                        .then().setRequestedAttendanceDate(strEtdDate)
                        .then().clickSaveButton();

        String jobId = viewJobDetailsPage.getHeader().getJobId();

        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();

        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }

        viewJobDetailsPage = jobsSubPage.getAllJobsByJobId(jobId)
                .clickNavigateToJobArrowButton();

        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage
                .clickAddOrGoToJobTeamButton();

        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        viewJobDetailsPage = addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().clickSaveButton();

        //AC 10
        AddToJobScopePage addToJobScopePage = viewJobDetailsPage
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class);

        DueAndOverduePage dueAndOverDuePage = addToJobScopePage.clickDueAndOverdueTab();
        assert_().withFailureMessage("The system will display a message if no services are available for a Asset").that(
                dueAndOverDuePage.getNoServicesText())
                .contains(noServiceInDueAndOverdue);

        //AC 11,13
        ServiceSchedulePage serviceSchedulePage = addToJobScopePage.clickServiceScheduleTab();

        assert_().withFailureMessage("The system will display a message if no services are available for a Asset").that(
                serviceSchedulePage.getNoServicesText())
                .contains(noServiceInServiceSchedule);

    }
}
