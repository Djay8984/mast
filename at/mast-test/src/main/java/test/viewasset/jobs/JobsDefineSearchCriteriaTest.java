package test.viewasset.jobs;

import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.job.JobQueryPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.conditionofclass.sub.JobStatusFilterSubPage;
import viewasset.sub.jobs.element.AllJobElement;
import viewasset.sub.jobs.searchfilter.SearchFilterJobsPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.PaginationCountSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.SQLException;
import java.util.Collections;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class JobsDefineSearchCriteriaTest extends BaseTest
{

    @Test(description = "US - 10.2 Jobs - Define search criteria")
    @Issue("LRT-1709")
    public void jobsDefineSearchCriteriaTest() throws SQLException
    {

        final int assetId = 501;
        final int jobStatusId = 9;
        final String invalidJobNumber = "123456789";
        final String errorMessage = "No jobs found for your search criteria\n" +
                "Please search again";
        final String jobStatus = "Cancelled";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AllJobElement allJobElement = jobsSubPage.getAllJobsByIndex(0);
        String jobNumber = allJobElement.getJobNumber();

        //AC: 1
        //Step: 3
        assert_().withFailureMessage("Search filter jobs button is expected to be clickable")
                .that(jobsSubPage.isSearchFilterJobsButtonClickable()).isTrue();
        SearchFilterJobsPage searchFilterJobsPage = jobsSubPage.clickSearchFilterJobsButton();

        //AC: 2 and AC: 3
        //Step: 4 and 5
        //@Visible Annotation covers these ACs

        //AC: 4 and AC: 5
        //Step: 6
        searchFilterJobsPage.setSearchInput(jobNumber).then().clickSearchButton();
        JobQueryDto jobQueryDto = new JobQueryDto();
        jobQueryDto.setAssetId(Collections.singletonList((long) assetId));
        JobQueryPage jobQueryPage = new JobQueryPage(jobQueryDto);
        int jobsCount = jobQueryPage.getDto().getContent()
                .stream().filter(e -> e.getId().
                        equals(Long.valueOf(jobNumber)))
                .collect(Collectors.toList())
                .size();

        allJobElement = jobsSubPage.getAllJobsByIndex(0);
        assert_().withFailureMessage("Expected job is to shown")
                .that(jobNumber.equals(allJobElement.getJobNumber()))
                .isTrue();

        //AC: 6
        //Step: 7
        PaginationCountSubPage paginationCountSubPage = jobsSubPage.getPaginationPage();
        assert_().withFailureMessage("number of results displayed on the screen is " +
                "expected to be equal to results returned from the search")
                .that(String.valueOf(jobsSubPage.getAllJobsCount())
                        .equals(String.valueOf(paginationCountSubPage.getShownAmount()))
                        && String.valueOf(paginationCountSubPage.getTotalAmount())
                        .equals(String.valueOf(jobsCount)))
                .isTrue();

        //AC: 9
        //Step: 8
        jobsSubPage.clickSearchFilterJobsButton();
        searchFilterJobsPage.clickResetButton();
        assert_().withFailureMessage("number of results displayed on the screen " +
                "is expected to be equal to results returned from the search")
                .that(String.valueOf(jobsSubPage.getAllJobsCount())
                        .equals(String.valueOf(paginationCountSubPage.getShownAmount()))
                        && String.valueOf(paginationCountSubPage.getTotalAmount())
                        .equals(String.valueOf(jobQueryPage.getDto().getContent().size())))
                .isTrue();

        //AC: 7
        //Step: 9
        jobsSubPage.clickSearchFilterJobsButton();
        searchFilterJobsPage.setSearchInput(invalidJobNumber)
                .then().clickSearchButton();
        assert_().withFailureMessage("Valid error message is expected to be displayed")
                .that(jobsSubPage.getMessage().equals(errorMessage))
                .isTrue();

        //AC: 8
        //Step: 10
        jobsSubPage.clickSearchFilterJobsButton();
        searchFilterJobsPage.clickResetButton();
        jobsSubPage.clickSearchFilterJobsButton();
        JobStatusFilterSubPage jobStatusFilterSubPage
                = searchFilterJobsPage.clickJobStatusFilterButton();
        jobStatusFilterSubPage.clickClear().then().selectJobStatus(jobStatus);
        searchFilterJobsPage.clickSearchButton();

        jobQueryDto.setAssetId(Collections.singletonList((long) assetId));
        jobQueryDto.setJobStatusId(Collections.singletonList((long) jobStatusId));
        jobQueryPage = new JobQueryPage(jobQueryDto);

        assert_().withFailureMessage("number of results displayed on the screen " +
                "is expected to be equal to results returned from the search")
                .that(String.valueOf(jobsSubPage.getAllJobsCount())
                        .equals(String.valueOf(paginationCountSubPage.getShownAmount()))
                        && String.valueOf(paginationCountSubPage.getTotalAmount())
                        .equals(String.valueOf(jobQueryPage.getDto().getContent().size())))
                .isTrue();
    }
}
