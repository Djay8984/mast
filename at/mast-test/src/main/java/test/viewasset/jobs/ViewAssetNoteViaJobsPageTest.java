package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;
import viewasset.sub.jobs.details.assetnotes.DetailsPage;
import viewasset.sub.jobs.details.assetnotes.SearchFilterAssetNotesPage;
import viewasset.sub.jobs.details.assetnotes.elements.AssetNoteElement;
import viewasset.sub.jobs.details.conditionofclass.sub.CategoryFilterSubPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class ViewAssetNoteViaJobsPageTest extends BaseTest
{

    @Test(description = "US 10.24 AC6 - 9.48 - AC:2,3,5,6 -User can view asset note  - via Jobs page")
    @Issue("LRT-2151")

    public void viewAssetNoteViaJobsPageTest()
    {

        int assetID = 501;
        String uniqueIdentifier = "AN";
        String assetNotesByStatus[] = {"Open", "Inactive", "Deleted", "Cancelled"};

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        //Step: 3
        AllJobElement allJobElement = jobsSubPage.getAllJobsByIndex(0);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        //Step: 4
        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        //Step: 5
        SearchFilterAssetNotesPage searchFilterAssetNotesPage
                = assetNotesPage.clickSearchFilterAssetNotesButton();
        CategoryFilterSubPage categoryFilterSubPage
                = searchFilterAssetNotesPage.clickCategoryFilterButton();
        categoryFilterSubPage.selectAssetNoteCheckBox();
        searchFilterAssetNotesPage.clickSearchButton();

        //AC: 2 and AC: 6
        //Step: 6
        AssetNotesPage.OtherTable jobTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> jobAssetNote = jobTable.getRows();
        DetailsPage assetNoteDetailsPage = jobAssetNote.stream().filter(e -> e.getItemStatus().contains(assetNotesByStatus[0])).findFirst().get()
                .clickFurtherDetailsArrow();
        assert_().withFailureMessage("Edit button is expected to be displayed ")
                .that(assetNoteDetailsPage.isEditButtonPresent())
                .isTrue();
        assert_().withFailureMessage("Cancel button is expected to be displayed ")
                .that(assetNoteDetailsPage.isCancelButtonPresent())
                .isTrue();
        assert_().withFailureMessage("Close button is expected to be displayed ")
                .that(assetNoteDetailsPage.isCloseButtonPresent())
                .isTrue();
        assert_().withFailureMessage("Unique identifier AN is expected to be displayed ")
                .that(assetNoteDetailsPage.getAssetNoteNameText()
                        .contains(uniqueIdentifier))
                .isTrue();

        assetNoteDetailsPage.clickNavigateBackButton();

        //AC: 3 and AC: 6
        //Step: 7
        jobTable = assetNotesPage.getOtherTable();
        jobAssetNote = jobTable.getRows();
        assetNoteDetailsPage = jobAssetNote.stream().filter(e -> e.getItemStatus().contains(assetNotesByStatus[1])).findFirst().get()
                .clickFurtherDetailsArrow();
        assert_().withFailureMessage("Edit button is expected to be displayed ")
                .that(assetNoteDetailsPage.isEditButtonPresent())
                .isTrue();
        assert_().withFailureMessage("Cancel button is expected to be displayed ")
                .that(assetNoteDetailsPage.isCancelButtonPresent())
                .isTrue();
        assert_().withFailureMessage("Close button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isCloseButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Unique identifier AN is expected to be displayed ")
                .that(assetNoteDetailsPage.getAssetNoteNameText()
                        .contains(uniqueIdentifier))
                .isTrue();

        assetNoteDetailsPage.clickNavigateBackButton();

        //AC: 5 and AC: 6
        //Step: 8
        jobTable = assetNotesPage.getOtherTable();
        jobAssetNote = jobTable.getRows();
        assetNoteDetailsPage = jobAssetNote.stream().filter(e -> e.getItemStatus().contains(assetNotesByStatus[2])).findFirst().get()
                .clickFurtherDetailsArrow();
        assert_().withFailureMessage("Edit button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isEditButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Cancel button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isCancelButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Close button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isCloseButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Unique identifier AN is expected to be displayed ")
                .that(assetNoteDetailsPage.getAssetNoteNameText()
                        .contains(uniqueIdentifier))
                .isTrue();

        assetNoteDetailsPage.clickNavigateBackButton();

        //AC: 5 and AC: 6
        //Step: 9 and Step: 10
        jobTable = assetNotesPage.getOtherTable();
        jobAssetNote = jobTable.getRows();
        assetNoteDetailsPage = jobAssetNote.stream().filter(e -> e.getItemStatus().contains(assetNotesByStatus[3])).findFirst().get()
                .clickFurtherDetailsArrow();
        assert_().withFailureMessage("Edit button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isEditButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Cancel button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isCancelButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Close button is not expected to be displayed ")
                .that(assetNoteDetailsPage.isCloseButtonPresent())
                .isFalse();
        assert_().withFailureMessage("Unique identifier AN is expected to be displayed ")
                .that(assetNoteDetailsPage.getAssetNoteNameText()
                        .contains(uniqueIdentifier))
                .isTrue();
    }
}
