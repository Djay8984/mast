package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobData;
import constant.LloydsRegister.JobTeamData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class ConfirmJobScopeTest extends BaseTest
{

    @Test(description = "Story 10.18 - As a user I want to select to confirm the job scope so that I can start planning other job details LRT-1781")
    @Issue("LRT-2490")
    public void ConfirmJobScopeTest()
    {

        final int assetId = 501;
        final String jobScopeToBeDefinedText = "Job scope to be defined";
        final String jobScopeConfirmedText = "Job scope confirmed";

        WorkHubPage workHubPage = WorkHubPage.open();

        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        searchFilterSubPage.setAssetSearchInput(asset.getDto().getName())
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(asset.getDto().getName());

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.setSdoCode(JobData.sdoCode)
                .selectCaseTypeByVisibleText(JobData.caseType)
                .setJobLocation(JobData.location)
                .setJobDescription(JobData.description)
                .setEtaDate(JobData.strEtaDate)
                .setEtdDate(JobData.strEtdDate)
                .setRequestedAttendanceDate(JobData.strEtdDate)
                .clickSaveButton();

        String jobId = viewJobDetailsPage.getHeader().getJobId();
        AddOrEditJobTeamPage addOrEditJobTeamPage
                = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        addOrEditJobTeamPage.setLeadSurveyorTextBox(JobTeamData.leadSurveyor)
                .then().selectTypeAheadSuggestionDropBoxByIndex(0)
                .then().clickSaveButton();

        assert_().withFailureMessage("Job scope confirmation message is expected to be present ")
                .that(viewJobDetailsPage.getJobScopeConfirmationText()
                        .equals(jobScopeToBeDefinedText))
                .isTrue();

        viewJobDetailsPage.getHeader().clickNavigateBackButton();

        jobsSubPage.getAllJobsByJobId(jobId).clickNavigateToJobArrowButton();

        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();
        BaseServiceTable classificationTable = serviceAllPage.getClassificationTable();
        BaseServiceTable statutoryTable = serviceAllPage.getStatutoryTable();
        ServiceAllPage.ConditionsOfClassTable conditionOfClassTable = serviceAllPage
                .getConditionsOfClassTable();
        ServiceAllPage.ActionableItemsTable actionableItemsTable = serviceAllPage.getActionableItemsTable();
        ServiceAllPage.AssetNotesTable assetNotesTable = serviceAllPage.getAssetNotesTable();

        classificationTable.getRows().get(0).getChildren().get(0).getServiceRows().get(0)
                .selectCheckBox();
        conditionOfClassTable.getRows().get(0).selectCheckBox();

        ConfirmModalWindow confirmMiscWindow = serviceAllPage.getFooter().clickAddButton(ConfirmModalWindow
                .class);

        JobScopePage jobScopePage = confirmMiscWindow.clickOkButton(JobScopePage.class);
        //AC 4
        assert_().withFailureMessage("Job scope is expected to be in Unconfirmed status")
                .that(jobScopePage.getJobScopeStatus())
                .contains("Unconfirmed");
        //AC 1
        viewJobDetailsPage = jobScopePage.clickConfirmJobScopeButton()
                .then().clickConfirmScopeButton();

        jobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(JobScopePage.class);
        //AC 2 & AC 3
        assert_().withFailureMessage("Job scope is expected to be in Unconfirmed status")
                .that(jobScopePage.getJobScopeStatus())
                .contains("Confirmed");
        assert_().withFailureMessage("Confirmed Job SCope is expected to show the date of last confirmation")
                .that(jobScopePage.isConfirmedDatePresent())
                .isTrue();
        assert_().withFailureMessage("Confirmed Job SCope is expected to show the user who last confirmed")
                .that(jobScopePage.isConfirmedByPresent())
                .isTrue();

        ConfirmModalWindow confirmRemoveSelectedWindow = jobScopePage.clickSelectAllLink()
                .then().clickRemoveSelectedLink();
        jobScopePage = confirmRemoveSelectedWindow.clickOkButton(JobScopePage.class);

        //AC 5
        addToJobScopePage = jobScopePage.clickAddServicesCocAIsButton();
        addToJobScopePage.clickAllTab();
        conditionOfClassTable.getRows().get(0).selectCheckBox();
        actionableItemsTable.getRows().get(0).selectCheckBox();
        assetNotesTable.getRows().get(0).selectCheckBox();
        serviceAllPage.getFooter().clickAddButton(JobScopePage.class);
        //AC 6
        viewJobDetailsPage = jobScopePage.clickConfirmJobScopeButton().then().clickConfirmScopeButton();

        assert_().withFailureMessage("Job scope is expected to be in Unconfirmed status")
                .that(viewJobDetailsPage.getJobScopeText())
                .contains(jobScopeConfirmedText);
        //AC 7,8
        ConditionsOfClassPage conditionsOfClassPage = viewJobDetailsPage.clickGoToCocsButton();
        ConditionsOfClassPage.JobRelatedTable jobRelatedTable
                = conditionsOfClassPage.getJobRelatedTable();
        assert_().withFailureMessage("Elements added in Job scope are expected to be displayed in Job related section of CoC")
                .that(jobRelatedTable.getRows())
                .isNotEmpty();

        viewJobDetailsPage = conditionsOfClassPage.clickNavigateBackButton();
        ActionableItemsPage actionableItemsPage = viewJobDetailsPage.clickGoToActionableItemsButton();
        ActionableItemsPage.JobRelatedTable jobRelatedTableAN = actionableItemsPage.getJobRelatedTable();
        assert_().withFailureMessage("Elements added in Job scope are expected to be displayed in Job related section of Actionable Items")
                .that(jobRelatedTableAN.getRows())
                .isNotEmpty();

        viewJobDetailsPage = actionableItemsPage.clickNavigateBackButton();
        AssetNotesPage assetNotesPage = viewJobDetailsPage.clickGoToAssetNotesButton();
        AssetNotesPage.JobRelatedTable jobRelatedTableAsN = assetNotesPage.getJobRelatedTable();
        assert_().withFailureMessage("Elements added in Job scope are expected to be displayed in Job related section of Asset notes")
                .that(jobRelatedTableAsN.getRows())
                .isNotEmpty();

        //AC 9 - todo clarification

    }
}
