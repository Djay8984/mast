package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobTeamData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.JobScopeServiceTaskElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.CocAiAnServiceElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.JobScopeServiceTypeElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub.JobScopeHierarchyNodeElements;
import viewasset.sub.jobs.details.scope.element.ServiceItemElements;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.JobData;

public class JobCreditingSectionTest extends BaseTest
{

    @Test(description = "Story 11.13 - As a user on the job page I want to " +
            "see the crediting section so that I can start reporting the services")
    @Issue("LRT-2409")
    public void jobCreditingSectionTest()
    {

        final int assetId = 501;
        final String expectedJobScopeConfirmationText = "Job scope to be defined";
        final String jobScopeServiceType = "Machinery";
        final String productCatalogueName = "Classification";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 3
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        //AC: 1 and AC: 7
        //Step: 4
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate)
                .then().clickSaveButton();

        String jobId = viewJobDetailsPage.getHeader().getJobId();

        AddOrEditJobTeamPage addOrEditJobTeamPage
                = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        addOrEditJobTeamPage.setLeadSurveyorTextBox(JobTeamData.leadSurveyor)
                .then().clickSaveButton();

        assert_().withFailureMessage("Job scope confirmation message is expected to be present ")
                .that(viewJobDetailsPage.getJobScopeConfirmationText()
                        .equals(expectedJobScopeConfirmationText))
                .isTrue();

        //Step: 5
        viewJobDetailsPage.getHeader().clickNavigateBackButton();

        //Step: 6 and 7
        //AC: 2 and 3
        jobsSubPage.getAllJobsByJobId(jobId).clickNavigateToJobArrowButton();

        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

        JobScopeServiceTaskElements JobScopeServiceTaskElement
                = serviceAllPage.getJobScopeServiceTaskByName(productCatalogueName);
        if (JobScopeServiceTaskElement.isProductCatalogueExpanded())
        {
            JobScopeServiceTaskElement.clickOnExpandIcon();
        }

        JobScopeServiceTypeElements jobScopeServiceTypeElement
                = JobScopeServiceTaskElement.getJobScopeServiceTypeByName(jobScopeServiceType);
        if (jobScopeServiceTypeElement.isJobScopeServiceTypeExpanded())
        {
            jobScopeServiceTypeElement.clickOnExpandIcon();
        }

        List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements
                = jobScopeServiceTypeElement.getAllJobScopeHierarchyNodeElements();
        String serviceCode = jobScopeHierarchyNodeElements.get(0).getServiceCodeText();
        jobScopeHierarchyNodeElements.get(0).selectCheckBox();
        JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

        viewJobDetailsPage = jobScopePage.clickConfirmJobScopeButton()
                .then().clickConfirmScopeButton();

        assert_().withFailureMessage("Percentage Complete Icon is expected to be present")
                .that(viewJobDetailsPage.isPercentageCompleteIconPresent())
                .isTrue();

        assert_().withFailureMessage("Service Code is expected to be present")
                .that(viewJobDetailsPage.getServiceCodeText()
                        .equals(serviceCode)).isTrue();

        //AC: 5 an 8
        //Step: 11 and 12
        assert_().withFailureMessage("Go to crediting button is expected to be clickable")
                .that(viewJobDetailsPage.isGoToCreditingButtonEnabled())
                .isTrue();

        assert_().withFailureMessage("Add To P17 Button is expected to be displayed")
                .that(viewJobDetailsPage.isAddToP17ButtonDisplayed())
                .isTrue();

        //AC: 5
        //Step: 13 and 14
        viewJobDetailsPage.clickGoToCreditingButton().then().clickNavigateBackButton
                (ViewJobDetailsPage.class);

        //AC: 6
        //Step: 15
        jobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(JobScopePage.class);
        jobScopePage.clickSelectAllLink()
                .then().clickRemoveSelectedLink()
                .then().clickOkButton(JobScopePage.class);

        jobScopePage.clickAddServicesCocAIsButton();

        addToJobScopePage.clickAllTab();
        CocAiAnServiceElements cocAiAnServiceElements
                = serviceAllPage.getCocAiAnServiceElementsByName("Conditions of Class");
        if (cocAiAnServiceElements.isServiceItemExpanded())
        {
            cocAiAnServiceElements.clickExpandIcon();
        }
        List<ServiceItemElements> serviceItemElements = cocAiAnServiceElements.getServiceItemElements();
        serviceItemElements.get(0).selectCheckBox();
        serviceAllPage.getFooter().clickAddButton(JobScopePage.class);
        jobScopePage.clickConfirmJobScopeButton().then().clickConfirmScopeButton();

        assert_().withFailureMessage("Service code is not expected to be present")
                .that(viewJobDetailsPage.isServiceCodePresent())
                .isFalse();
    }
}
