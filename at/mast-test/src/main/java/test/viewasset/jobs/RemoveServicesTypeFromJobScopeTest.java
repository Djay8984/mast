package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobData;
import constant.LloydsRegister.JobTeamData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class RemoveServicesTypeFromJobScopeTest extends BaseTest
{

    @Test(description = "US - 10.17 Remove CoCs, ANs and/or AIs from job scope")
    @Issue("LRT-2039")
    public void removeServicesTypeFromJobScopeTest()
    {

        final int assetId = 501;
        final int index = 0;
        final String expectedServiceRemovedConfirmationText = "There are no services, Conditions" +
                " of Class, Asset Notes or Actionable Items currently in scope.";
        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.addNewJob();

        AddOrEditJobTeamPage addJobTeamSubPage
                = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        addJobTeamSubPage.setLeadSurveyorTextBox(JobTeamData.leadSurveyor)
                .then().clickSaveButton();

        //AC: 6 and AC: 7
        //Step: 8 and 9
        AddToJobScopePage addToJobScopePage
                = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

        serviceAllPage.clickCocExpandIcon()
                .then().selectCocCheckBoxByIndex(index);
        String cocTitle = serviceAllPage.getCocNodeTitleByIndex(index);
        serviceAllPage.clickActionableItemExpandIcon()
                .then().selectActionableItemsCheckBoxByIndex(index);
        String actionableItemsTitle
                = serviceAllPage.getActionableItemsNodeTitleByIndex(index);

        serviceAllPage.clickAssetNotesExpandIcon()
                .then().selectAssetNotesCheckBoxByIndex(index);
        String assetItemsName = serviceAllPage.getAssetNotesNodeTitleByIndex(index);

        JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

        assert_().withFailureMessage("Added services " + cocTitle + " is expected to be present ")
                .that(jobScopePage.getCocNodeTitleByIndex(index).equals(cocTitle))
                .isTrue();

        assert_().withFailureMessage("Added services " + actionableItemsTitle + " " +
                "is expected to be present ")
                .that(jobScopePage.getActionableItemsNodeTitleByIndex(index)
                        .equals(actionableItemsTitle))
                .isTrue();

        assert_().withFailureMessage("Added services " + assetItemsName + "" +
                " is expected to be present ")
                .that(jobScopePage.getAssetNotesNodeTitleByIndex(index)
                        .equals(assetItemsName))
                .isTrue();

        //AC: 1 and AC: 2
        //Step: 4 and 5
        jobScopePage.selectCocCheckBoxByIndex(index).then()
                .selectActionableItemsCheckBoxByIndex(index)
                .then().selectAssetNotesCheckBoxByIndex(index);

        ConfirmModalWindow ServiceCancellationModelWindow
                = jobScopePage.clickRemoveSelectedLink();

        //AC: 4
        //Step: 5
        ServiceCancellationModelWindow.clickCancelButton(JobScopePage.class);
        assert_().withFailureMessage("Added services " + cocTitle + " " +
                "is expected to be present")
                .that(jobScopePage.getCocNodeTitleByIndex(index)
                        .equals(cocTitle))
                .isTrue();
        assert_().withFailureMessage("Added services " + actionableItemsTitle + " " +
                "is expected to be present ")
                .that(jobScopePage.getActionableItemsNodeTitleByIndex(index)
                        .equals(actionableItemsTitle))
                .isTrue();
        assert_().withFailureMessage("Added services " + assetItemsName + "" +
                " is expected to be present ")
                .that(jobScopePage.getAssetNotesNodeTitleByIndex(index)
                        .equals(assetItemsName))
                .isTrue();

        //AC: 3 and 5
        //Step: 6 and 7
        jobScopePage.clickRemoveSelectedLink()
                .then().clickOkButton(JobScopePage.class);

        assert_().withFailureMessage("Condition of Class Items are not " +
                "expected to be present")
                .that(jobScopePage.isCocItemsPresent())
                .isFalse();

        assert_().withFailureMessage("Actionable Items are not expected to be present")
                .that(jobScopePage.isActionableItemsPresent())
                .isFalse();

        assert_().withFailureMessage("Asset Notes Items are not expected to be present")
                .that(jobScopePage.isAssetNotesPresent())
                .isFalse();

        assert_().withFailureMessage("Service Removed ConfirmationModalPage Text is expected to be present")
                .that(jobScopePage.getServiceRemovedConfirmationText()
                        .equals(expectedServiceRemovedConfirmationText))
                .isTrue();
    }
}
