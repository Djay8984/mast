package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.surveyor.Surveyor;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.crediting.EditSurveyPage;
import viewasset.sub.jobs.details.crediting.base.BaseServiceTable;
import viewasset.sub.jobs.details.crediting.element.ServiceElement;
import viewasset.sub.jobs.details.crediting.modal.LeaveConfirmationPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.JobScopeServiceTaskElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.JobScopeServiceTypeElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub.JobScopeHierarchyNodeElements;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.ConfirmScopeModalWindow;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class EditMetaDataOfCreditedTaskTest extends BaseTest
{

    @DataProvider(name = "data")
    public Object[][] jobStatus()
    {
        return new Object[][]{
                {"Under Reporting"},
                {"Under Survey"},
                {"Resource Assigned"}};
    }

    @Test(description = "US 11.19 - As a user I want to edit the meta-data of the credited tasks/ services so that I can ensure that the information is up-to-date\t\n", dataProvider = "data")
    @Issue("LRT-2649")
    public void EditMetaDataOfCreditedTask(String jobStatus)
    {

        int assetID = 501;
        final String jobScopeServiceType = "Machinery";
        final String productCatalogueName = "Classification";

        WorkHubPage workHubPage = WorkHubPage.open();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID))
                .then().clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID)
                        .getDto()
                        .getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }

        AllJobElement allJobElement = jobsSubPage.getAllJobsElements()
                .stream()
                .filter(c -> c.getJobStatus().equals(jobStatus))
                .findFirst()
                .get();

        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();
        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        viewJobDetailsPage = addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().selectTypeAheadSuggestionDropBoxByIndex(0)
                .then().clickSaveButton();

        if (viewJobDetailsPage.getTextOfAddOrGoToJobScopeButton().contains("Add"))
        {
            AddToJobScopePage addToJobScopePage = viewJobDetailsPage
                    .clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
            ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

            JobScopeServiceTaskElements JobScopeServiceTaskElement
                    = serviceAllPage.getJobScopeServiceTaskByName(productCatalogueName);
            if (JobScopeServiceTaskElement.isProductCatalogueExpanded())
            {
                JobScopeServiceTaskElement.clickOnExpandIcon();
            }

            JobScopeServiceTypeElements jobScopeServiceTypeElement
                    = JobScopeServiceTaskElement.getJobScopeServiceTypeByName(jobScopeServiceType);
            if (jobScopeServiceTypeElement.isJobScopeServiceTypeExpanded())
            {
                jobScopeServiceTypeElement.clickOnExpandIcon();
            }

            List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements
                    = jobScopeServiceTypeElement.getAllJobScopeHierarchyNodeElements();
            String serviceTypeText = jobScopeHierarchyNodeElements.get(0).getServiceTypeText();
            jobScopeHierarchyNodeElements.get(0).selectCheckBox();
            JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

            viewJobDetailsPage = jobScopePage.clickNavigateBackButton();
        }

        if (viewJobDetailsPage.getJobScopeConfirmationText().contains("to be confirmed"))
        {
            JobScopePage jobScopePage = viewJobDetailsPage
                    .clickAddOrGoToJobScopeButton(JobScopePage.class);

            ConfirmScopeModalWindow confirmScopeModalWindow = jobScopePage.clickConfirmJobScopeButton();
            viewJobDetailsPage = confirmScopeModalWindow.clickConfirmScopeButton();
        }

        CreditingPage creditingPage = viewJobDetailsPage.clickGoToCreditingButton();

        BaseServiceTable classificationTable = creditingPage.getClassificationTable();
        List<ServiceElement> classificationElements = creditingPage.getClassificationTable()
                .getRows();

        EditSurveyPage editSurveyPage = classificationElements.get(0).clickEditSurveyIcon();

        //AC 1, 2
        assert_().withFailureMessage("Narrative box is expected to be displayed and editable")
                .that(editSurveyPage.isNarrativeTextBoxEnabled())
                .isTrue();

        //AC 4 - to check credited on and credited by field - we are not scripting this, because this will always pass. Since such element is not present even if it is not in editable.

        //AC 5,7
        editSurveyPage.setNarrativeTextBox("Absdfherjtiertnfv1576ahgk54575"); //A-Z a-z 0-9

        assert_().withFailureMessage("Save Button is expected to be  displayed and enabled")
                .that(editSurveyPage.isSaveButtonEnabled())
                .isTrue();

        String textInNarrativeTextBox = editSurveyPage.getNarrativeTextBox();
        creditingPage = editSurveyPage.clickSaveButton();

        classificationTable = creditingPage.getClassificationTable();
        classificationElements = creditingPage.getClassificationTable()
                .getRows();

        editSurveyPage = classificationElements.get(0).clickEditSurveyIcon();
        //AC 8,6
        assert_().withFailureMessage("The values in Narrative text box is expected to be saved")
                .that(editSurveyPage.getNarrativeTextBox())
                .contains(textInNarrativeTextBox);

        editSurveyPage.setNarrativeTextBox("asfdASDAF1453465");
        editSurveyPage.selectRequiresFollowupCheckBox();
        creditingPage = editSurveyPage.clickSaveButton();

        LeaveConfirmationPage leaveConfirmationPage = creditingPage
                .clickNavigateBackButton(LeaveConfirmationPage.class);
        leaveConfirmationPage.clickSaveChangesButton();

        jobsSubPage = viewJobDetailsPage.getHeader()
                .clickNavigateBackButton();
    }
}




