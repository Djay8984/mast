package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.attachment.AttachmentSubPage;
import viewasset.sub.commonpage.attachment.modal.ConfirmationAttachment;
import viewasset.sub.commonpage.note.EditNoteSubPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;
import viewasset.sub.jobs.details.assetnotes.DetailsPage;
import viewasset.sub.jobs.details.assetnotes.elements.AssetNoteElement;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static helper.TestDataHelper.getAssetIdByImoNumber;

public class ViewEditAssetNotesFromCurrentJobTest extends BaseTest
{

    String seenByButtonText = "All";
    int assetID = 501;

    @Test(description = "US10.24 AC8 – 9.2 AC 1-4 - View additional details of Notes and Attachments when creating Asset Note - Via Jobs page  ")
    @Issue("LRT-2145 , LRT-2146")
    public void AddAssetNote()
    {

        String helpText = "Type your note here";
        WorkHubPage workHubPage = WorkHubPage.open();
        HeaderPage headerPage = workHubPage.getHeader();
        String currentUser = headerPage.getCurrentUser();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        AssetNotesPage.OtherTable otherTable = assetNotesPage.getOtherTable();
        AssetNoteElement assetNotesElementInOtherHeader = otherTable.getRows().get(1).clickAssetNote();
        DetailsPage assetNoteDetailsPage = assetNotesElementInOtherHeader.clickFurtherDetailsArrow();

        AttachmentsAndNotesPage attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();

        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        attachmentsAndNotesPage = noteSubPage.clickCancelButton();
        //AC 1,2
        //AC 5
        noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        assert_().withFailureMessage("Add Note button in the form is expected to be disabled")
                .that(noteSubPage.isAddNoteButtonEnabled())
                .isFalse();
        //AC 4
        assert_().withFailureMessage("The Add Note button is expected to be display a help text")
                .that(noteSubPage.getHelperText()).contains(helpText);
        //AC 6
        assert_().withFailureMessage("The current date is displayed")
                .that(noteSubPage.getNoteDate().toString()).isSameAs(LocalDate.now());//.format(FRONTEND_TIME_FORMAT));
        assert_().withFailureMessage("The is current user is expected to displayed")
                .that(noteSubPage.getCurrentUser()).contains(currentUser);

        //AC 7
        assert_().withFailureMessage("Add Note button in the form is expected to be disabled")
                .that(noteSubPage.isCancelButtonEnabled())
                .isTrue();

        // AC 3,10
        noteSubPage.setNoteTextBox(
                "This is an automated test. <> 1000. This is an automated test. <> 1000. This is an automated test. <> 1000. This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.This is an automated test. <> 1000.");
        assert_().withFailureMessage("Character limit for description is expected to be 1000characters")
                .that(noteSubPage.getNoteText().length()).isAtMost(1000);
        // AC 8
        attachmentsAndNotesPage = noteSubPage.clickCancelButton();
        noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        //AC 9
        noteSubPage.setNoteTextBox("This is an automated test");

        if (seenByButtonText.equalsIgnoreCase("all"))
        {
            noteSubPage.clickAllButton();
        }
        //AC 7
        assert_().withFailureMessage("Add Note button in the form is expected to be disabled")
                .that(noteSubPage.isAddNoteButtonEnabled())
                .isTrue();
        noteSubPage.clickAddNoteButton();

        attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).click();
        assert_().withFailureMessage("The note that is added is displayed in AttachmentsAndNotesPage")
                .that(attachmentsAndNotesPage.getNoteDescription()).contains("This is an automated test");
    }

    @Test(description = "US10.24 AC8 – 9.2 AC 1-4 - View additional details of Notes and Attachments when creating Asset Note - Via Jobs page  ")
    @Issue("LRT-2147")
    public void EditAssetNote()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        HeaderPage headerPage = workHubPage.getHeader();
        String currentUser = headerPage.getCurrentUser();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        AssetNotesPage.OtherTable jobTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> jobAssetNote = jobTable.getRows();
        AssetNoteElement assetNoteElement = jobAssetNote.get(1).clickAssetNote();
        DetailsPage assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        AttachmentsAndNotesPage attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();

        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        //AC 9
        attachmentsAndNotesPage = noteSubPage.clickCancelButton();

        while (!(attachmentsAndNotesPage.isLoadMoreButtonPresent()))
        {
            noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
            noteSubPage.setNoteTextBox("This is an automated test");
            if (seenByButtonText.equalsIgnoreCase("all"))
            {
                noteSubPage.clickAllButton();
            }
            attachmentsAndNotesPage = noteSubPage.clickAddNoteButton();
        }

        //AC 1
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);;
        assert_().withFailureMessage("The Edit description is not expected to be editable")
                .that(noteDetailsSubPage.isNoteDescriptionFieldEditable())
                .isFalse();

        //AC 7
        assert_().withFailureMessage("The Visible for: is expected to be non editable")
                .that(noteDetailsSubPage.isVisibleForButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("The  Visible for: is expected to be the confidentiality level set be the user")
                .that(noteDetailsSubPage.getVisibleForButton()).contains("All");

        //AC 1,2
        EditNoteSubPage editNoteSubPage = noteDetailsSubPage.clickEditIcon();
        assert_().withFailureMessage("The Edit description is expected to be editable")
                .that(editNoteSubPage.isNoteTextBoxEnabled())
                .isTrue();
        //AC 4,6

        assert_().withFailureMessage("The Edit Note is expected to have text")
                .that(editNoteSubPage.getNoteTextBox()).isNotEmpty();

        //AC 10
        editNoteSubPage.setNoteTextBox("text to copy/paste");
        editNoteSubPage.setNoteTextBox(Keys.chord(Keys.CONTROL, "a"));
        editNoteSubPage.setNoteTextBox(Keys.chord(Keys.CONTROL, "c"));
        String textCopied = editNoteSubPage.getNoteTextBox();
        editNoteSubPage.setNoteTextBox(Keys.chord(Keys.CONTROL, "v"));
        assert_().withFailureMessage("The hot keys can copy, paste,etc")
                .that(editNoteSubPage.getNoteTextBox()).contains(textCopied);

        // AC 8
        assert_().withFailureMessage("The Creator Name is expected to be displayed")
                .that(editNoteSubPage.isCreatorNameDisplayed())
                .isTrue();
        assert_().withFailureMessage("The date is expected to be displayed")
                .that(editNoteSubPage.isCreationDateDisplayed())
                .isTrue();

        //AC 9
        noteDetailsSubPage = editNoteSubPage.clickSaveButton();

        //AC 3
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        confirmationModalPage.clickCancelButton();

        // AC 12
        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        editNoteSubPage = noteDetailsSubPage.clickEditIcon();
        String textNote = editNoteSubPage.getNoteTextBox();
        noteDetailsSubPage = editNoteSubPage.clickCancelButton();

        //AC 10 , 11, 12
        assetNoteDetailsPage = attachmentsAndNotesPage.clickCloseButton();
        assetNotesPage = assetNoteDetailsPage.clickNavigateBackButton();
        viewJobDetailsPage = assetNotesPage.clickNavigateBackButton();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();
        headerPage = jobsSubPage.getHeader();

        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        jobsSubPage = viewAssetPage.clickJobsTab();

        allJobElement = jobsSubPage.getJobByIndex(1);
        viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        AssetNotesPage.OtherTable otherTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> otherAssetNotes = jobTable.getRows();
        assetNoteElement = otherAssetNotes.get(1).clickAssetNote();
        assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();

        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        assert_().withFailureMessage("The text is expected to be retained")
                .that(noteDetailsSubPage.getNoteText())
                .contains(textNote);
    }

    @Test(description = "US10.24 AC8 – 9.2 AC 1-4 - View additional details of Notes and Attachments when creating Asset Note - Via Jobs page  ")
    @Issue("LRT-2148")
    public void DeleteAssetNote()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        HeaderPage headerPage = workHubPage.getHeader();
        String currentUser = headerPage.getCurrentUser();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        AssetNotesPage.OtherTable jobTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> jobAssetNote = jobTable.getRows();
        AssetNoteElement assetNoteElement = jobAssetNote.get(1).clickAssetNote();
        DetailsPage assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        AttachmentsAndNotesPage attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();

        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();

        attachmentsAndNotesPage = noteSubPage.clickCancelButton();

        while (!(attachmentsAndNotesPage.isLoadMoreButtonPresent()))
        {
            noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
            noteSubPage.setNoteTextBox("This is an automated test");
            if (seenByButtonText.equalsIgnoreCase("all"))
            {
                noteSubPage.clickAllButton();
            }
            attachmentsAndNotesPage = noteSubPage.clickAddNoteButton();
        }
        //AC 1
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        String textNote = noteDetailsSubPage.getNoteText();
        //AC 2
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();

        //AC 4
        attachmentsAndNotesPage = confirmationModalPage.clickCancelButton();

        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        assert_().withFailureMessage("The note is expected to be displayed")
                .that(noteDetailsSubPage.getNoteText())
                .contains(textNote);

        assetNoteDetailsPage = attachmentsAndNotesPage.clickCloseButton();
        assetNotesPage = assetNoteDetailsPage.clickNavigateBackButton();
        viewJobDetailsPage = assetNotesPage.clickNavigateBackButton();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();
        headerPage = jobsSubPage.getHeader();

        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        jobsSubPage = viewAssetPage.clickJobsTab();

        allJobElement = jobsSubPage.getJobByIndex(1);
        viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        jobTable = assetNotesPage.getOtherTable();
        jobAssetNote = jobTable.getRows();
        assetNoteElement = jobAssetNote.get(1).clickAssetNote();
        assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();

        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        assert_().withFailureMessage("The same note is expected to be present")
                .that(noteDetailsSubPage.getNoteText())
                .isEqualTo(textNote);
        //AC 3
        confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        confirmationModalPage.clickCancelButton();
        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        attachmentsAndNotesPage = noteDetailsSubPage.clickDeleteIcon()
                .then().clickConfirmationButton(AttachmentsAndNotesPage.class);
        noteSubPage.refreshPage();
        attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();
        noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        assert_().withFailureMessage("The note is expected to be deleted")
                .that(noteDetailsSubPage.getNoteText()).isNotEqualTo(textNote);
    }

    @Test(description = "10.24 - As a user I want to select to view AN�s from the job page, so that I can check if any other AN�s needs to be edited or raised as part of current job")
    @Issue("LRT-2143")
    public void viewAssetNoteViaJobsPageTest()
    {

        String imoNumber = "1000021";
        String noItemsFoundErrorMessage = "No items found";
        int assetID = getAssetIdByImoNumber(imoNumber);
        WorkHubPage workHubPage = WorkHubPage.open();
        String jobIdWithAN = "059879847";
        String jobIdWithOutJobRelatedAN = "059879831";

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getAllJobsByJobId(jobIdWithAN);//059879842");
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();
        //AC 1
        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        AssetNotesPage.JobRelatedTable jobTable = assetNotesPage.getJobRelatedTable();
        List<AssetNoteElement> jobAssetNotes = jobTable.getRows();
        AssetNotesPage.OtherTable otherTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> otherAssetNotes = otherTable.getRows();

        //AC 3
        assert_().withFailureMessage("Job related section is expected to be " +
                "displayed along with all Asset Notes")
                .that(jobAssetNotes.size())
                .isNotEqualTo(null);
        //Ac 4
        assert_().withFailureMessage("Others section is expected to be " +
                "displayed along with all Asset Notes")
                .that(otherAssetNotes.size())
                .isNotEqualTo(null);

        viewJobDetailsPage = assetNotesPage.clickNavigateBackButton();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();
        // AC 5
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        allJobElement = jobsSubPage.getAllJobsByJobId(jobIdWithOutJobRelatedAN);
        viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();
        assetNotesPage = viewJobDetailsPage.clickGoToAssetNotesButton();

        otherTable = assetNotesPage.getOtherTable();
        otherAssetNotes = otherTable.getRows();

        assert_().withFailureMessage("Others section is expected to be " +
                "displayed along with all Asset Notes")
                .that(otherAssetNotes.size())
                .isNotEqualTo(null);

        assert_().withFailureMessage("Job Related section is expected to display " + noItemsFoundErrorMessage)
                .that(assetNotesPage.getErrorMessage()).contains(noItemsFoundErrorMessage);

        viewJobDetailsPage = assetNotesPage.clickNavigateBackButton();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();
        // AC 7

        HeaderPage headerPage = jobsSubPage.getHeader();
        workHubPage = headerPage.clickMastLogo();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(2));
        searchFilterSubPage.clickAssetSearchApplyButton();

        allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetId(2);

        viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        jobsSubPage = viewAssetPage.clickJobsTab();
        allJobElement = jobsSubPage.getJobByIndex(0);
        viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();
        assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        assert_().withFailureMessage("Job Related section is expected to display " + noItemsFoundErrorMessage)
                .that(assetNotesPage.getErrorMessage()).contains(noItemsFoundErrorMessage);
        assert_().withFailureMessage("Others section is expected to display" + noItemsFoundErrorMessage)
                .that(assetNotesPage.getErrorMessage()).contains(noItemsFoundErrorMessage);

    }

    @Test(description = "US10.24 AC8 – 9.1 AC 1-12 - View Notes and Attachments when creating Asset Note - via Jobs page ")
    @Issue("LRT-2144")
    public void ViewAssetNote()
    {

        String jobId = "059879838";
        WorkHubPage workHubPage = WorkHubPage.open();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID).getDto().getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AssetNotesPage assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        //AC 1 is descoped from Automation, which deals with icon
        //AC 2
        AssetNotesPage.OtherTable otherTable = assetNotesPage.getOtherTable();
        List<AssetNoteElement> otherAssetNote = otherTable.getRows();
        AssetNoteElement assetNoteElement = otherAssetNote.get(1).clickAssetNote();
        DetailsPage assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        AttachmentsAndNotesPage attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();
        //AC 3
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        attachmentsAndNotesPage = noteSubPage.clickCancelButton();

        while (!(attachmentsAndNotesPage.isLoadMoreButtonPresent()))
        {
            noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
            noteSubPage.setNoteTextBox("This is an automated test");
            if (seenByButtonText.equalsIgnoreCase("all"))
            {
                noteSubPage.clickAllButton();
            }
            attachmentsAndNotesPage = noteSubPage.clickAddNoteButton();
        }
        Integer numberOfNotesDisplayed = attachmentsAndNotesPage.getNoteAndAttachmentCards().size();
        assert_().withFailureMessage("system is expected to display 12 attachments and notes")
                .that(numberOfNotesDisplayed).isAtMost(12);
        attachmentsAndNotesPage.clickLoadMoreButton();
        assert_().withFailureMessage("system is expected to display more attachments and notes")
                .that(numberOfNotesDisplayed).isAtLeast(12);
        while (attachmentsAndNotesPage.isLoadMoreButtonPresent())
        {
            NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
            ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
            attachmentsAndNotesPage = confirmationModalPage.clickConfirmationButton(AttachmentsAndNotesPage.class);

        }

        // AC 9

        for (int i = 0; i < attachmentsAndNotesPage.getNoteAndAttachmentCards().size(); i++)
        {
            assert_().withFailureMessage("system is expected to display date in each note")
                    .that((attachmentsAndNotesPage.getNoteAndAttachmentCards().get(i).isDateDisplayed())).isTrue();
            assert_().withFailureMessage("Expected to display Name of the Creator in each note")
                    .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().get(i).getCreatorName()).isNotEmpty();
            //.that(attachmentsAndNotesPage.getNoteAndAttachmentCards().stream().filter(e -> e.getCreatorName().isEmpty())).isNull();
            assert_().withFailureMessage("system is expected to display Note description in each note")
                    .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().get(i).getNoteText()).isNotEmpty();
        }
        //AC 10 - descoped
        //AC 11
        assert_().withFailureMessage("The Note page is expected to have close button")
                .that(attachmentsAndNotesPage.isCloseButtonDisplayed()).isTrue();

        //AC 12
        assetNoteDetailsPage = attachmentsAndNotesPage.clickCloseButton();
        assetNotesPage = assetNoteDetailsPage.clickNavigateBackButton();
        viewJobDetailsPage = assetNotesPage.clickNavigateBackButton();
        jobsSubPage = viewJobDetailsPage.getHeader().clickNavigateBackButton();

        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        allJobElement = jobsSubPage.getAllJobsByJobId(jobId);
        viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();
        assetNotesPage
                = viewJobDetailsPage.clickGoToAssetNotesButton();

        otherTable = assetNotesPage.getOtherTable();
        otherAssetNote = otherTable.getRows();
        assetNoteElement = otherAssetNote.get(1).clickAssetNote();
        assetNoteDetailsPage = assetNoteElement.clickFurtherDetailsArrow();

        attachmentsAndNotesPage = assetNoteDetailsPage.clickNotesAndAttachmentIcon();
        assert_().withFailureMessage("The Note page is expected to have close button")
                .that(attachmentsAndNotesPage.isCloseButtonDisplayed()).isTrue();

        //AC 4
        AttachmentSubPage attachmentPage = attachmentsAndNotesPage.clickAddAttachmentButton();
        ConfirmationAttachment confirmationAttachment = attachmentPage.clickCancelButton();
        confirmationAttachment.clickCancelButton();
    }

}
