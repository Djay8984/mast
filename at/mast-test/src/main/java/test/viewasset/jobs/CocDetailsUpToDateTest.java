package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.DatabaseHelper;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.element.JobRelatedElements;
import viewasset.sub.jobs.element.MyJobsElements;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static constant.LloydsRegister.NUMBER_REGEX;

public class CocDetailsUpToDateTest extends BaseTest
{

    @Test(description = "US 10.35 - View existing CoC(s) records")
    @Issue("LRT-2111")
    public void viewExistingCocRecordsTest() throws ParseException
    {

        final int assetId = 501;
        String jobNumber = "161234628";
        String closeJobNumber = "161234631";
        String status = "Open";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        //Step: 3
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        MyJobsElements myJobsElements = jobsSubPage.getMyJobsByJobNumber(jobNumber);

        //Step: 4
        ViewJobDetailsPage viewJobDetailsPage
                = myJobsElements.clickFurtherDetailsArrow();

        //AC: 1, AC: 2, AC: 3 and AC: 4
        //Step: 5 and 6
        ConditionsOfClassPage conditionsOfClassPage
                = viewJobDetailsPage.clickGoToCocsButton();

        assert_().withFailureMessage("Job related section is expected to be " +
                "displayed along with all CoC")
                .that(conditionsOfClassPage.getJobRelatedCocCount())
                .isNotEqualTo(null);

        assert_().withFailureMessage("Others section is expected to be " +
                "displayed along with all CoC")
                .that(conditionsOfClassPage.getOthersCocCount())
                .isNotEqualTo(null);

        //AC: 5
        //Step: 8
        conditionsOfClassPage.clickNavigateBackButton();

        //AC: 1
        //Step: 9
        viewJobDetailsPage.clickGoToCocsButton();
        JobRelatedElements jobRelatedElements
                = conditionsOfClassPage.getFirstJobRelatedCoc(status);

        String cocID = jobRelatedElements.getCocID();

        //AC: 6 and AC: 7
        //Step: 10 and 11

        ViewConditionOfClassPage viewConditionOfClassPage
                = jobRelatedElements.clickFurtherDetailsArrow();
        assert_().withFailureMessage("Description is expected to be displayed")
                .that(viewConditionOfClassPage.getCocDescriptionText().replaceAll(NUMBER_REGEX, "").toLowerCase()
                        .equals(getDescription(cocID).toLowerCase()))
                .isTrue();

        assert_().withFailureMessage("Impose date is expected to be displayed")
                .that(viewConditionOfClassPage.getCocImposeDate()
                        .equals(String.valueOf(FRONTEND_TIME_FORMAT
                                .format(getImposedDate(cocID.trim())))))
                .isTrue();

        assert_().withFailureMessage("Inherited Type is expected to be displayed")
                .that(viewConditionOfClassPage.getCocInheritedType()
                        .equals(getInheritedStatus(cocID)))
                .isTrue();

        assert_().withFailureMessage("Visible for field is expected to be displayed")
                .that(viewConditionOfClassPage.getCocVisibleForText()
                        .equals("LR Internal"))
                .isTrue();

        assert_().withFailureMessage("Attachments is expected to be displayed")
                .that(viewConditionOfClassPage.isAttachmentDisplayed())
                .isTrue();

        //Step: 12
        viewConditionOfClassPage.clickNavigateBackButton(ConditionsOfClassPage.class);
        conditionsOfClassPage.clickNavigateBackButton();
        viewJobDetailsPage.getHeader().clickNavigateBackButton();

        //AC: 6
        //Step: 13
        myJobsElements = jobsSubPage.getMyJobsByJobNumber(closeJobNumber);
        viewJobDetailsPage = myJobsElements.clickFurtherDetailsArrow();
        conditionsOfClassPage = viewJobDetailsPage.clickGoToCocsButton();

        //AC: 7
        //Step: 14
        jobRelatedElements = conditionsOfClassPage.getFirstJobRelatedCoc(status);
        cocID = jobRelatedElements.getCocID();
        viewConditionOfClassPage = jobRelatedElements.clickFurtherDetailsArrow();
        assert_().withFailureMessage("Description is expected to be displayed")
                .that(viewConditionOfClassPage.getCocDescriptionText().replaceAll(NUMBER_REGEX, "").toLowerCase()
                        .equals(getDescription(cocID).toLowerCase()))
                .isTrue();

        assert_().withFailureMessage("Impose date is expected to be displayed")
                .that(viewConditionOfClassPage.getCocImposeDate()
                        .equals(String.valueOf(FRONTEND_TIME_FORMAT
                                .format(getImposedDate(cocID.trim())))))
                .isTrue();

        assert_().withFailureMessage("Inherited Type is expected to be displayed")
                .that(viewConditionOfClassPage.getCocInheritedType()
                        .equals(getInheritedStatus(cocID)))
                .isTrue();

        assert_().withFailureMessage("Visible for field is expected to be displayed")
                .that(viewConditionOfClassPage.getCocVisibleForText().equals("LR Internal"))
                .isTrue();

        assert_().withFailureMessage("Attachments is expected to be displayed")
                .that(viewConditionOfClassPage.isAttachmentDisplayed())
                .isTrue();
    }

    protected String getDescription(String id)
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE id = '" + id + "';");
        String result;
        try
        {
            rs.first();
            result = rs.getString("narrative");
            return result;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    protected Date getImposedDate(String id)
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE id = '" + id + "';");
        Date result;
        try
        {
            rs.first();
            result = rs.getDate("imposed_date");
            return result;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    //Inherited Status in BE is boolean. In FE it is Yes or No
    protected String getInheritedStatus(String id)
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_JOB_CODICILWIP WHERE id = '" + id + "';");
        int result;
        try
        {
            rs.first();
            result = rs.getInt("inherited_flag");
            if (result != 0)
                return "Yes";
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return "No";
    }
}
