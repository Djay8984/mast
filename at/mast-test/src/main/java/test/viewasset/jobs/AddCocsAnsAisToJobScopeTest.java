package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.DueAndOverduePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceSchedulePage;
import viewasset.sub.jobs.details.scope.addtojobscope.actionableItems.ViewActionableItemsPage;
import viewasset.sub.jobs.details.scope.addtojobscope.assetnotes.ViewAssetNotePage;
import viewasset.sub.jobs.details.scope.addtojobscope.conditionofclass.ViewConditionOfClassPage;
import viewasset.sub.jobs.details.scope.addtojobscope.conditionofclass.element.LinkedDefectElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.CocAinAnElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;

public class AddCocsAnsAisToJobScopeTest extends BaseTest
{
    @Test(description = "US - 10.15 Add CoCs, ANs and/or AIs to job scope")
    @Issue("LRT-1913")
    public void addCocsAnsAisToJobScopeTest() throws ParseException
    {

        int assetId = 501;
        final String expectedServiceStatus = "Open";
        String jodId = "000000001";
        final String[] expectedServices = {
                "Conditions of Class",
                "Actionable Items",
                "Asset Notes",
                };

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step:1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step:2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobSubPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobSubPage.setSdoCode(JobData.sdoCode)
                .selectCaseTypeByVisibleText(JobData.caseType)
                .setJobLocation(JobData.location)
                .setJobDescription(JobData.description)
                .setEtaDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS))
                .setEtdDate(LocalDate.now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setRequestedAttendanceDate(LocalDate.now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .clickSaveButton().clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor).clickSaveButton();

        //AC: 1
        //Step: 3
        AddToJobScopePage addToJobScopePage
                = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);

        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();
        List<CocAinAnElement> cocAinAnElements = serviceAllPage.getConditionsOfClassTable()
                .getRows().get(0).clickPlusOrMinusIcon().getChildren();
        if (cocAinAnElements.size() > 0)
        {
            for (CocAinAnElement cocAinAnElement : cocAinAnElements)
                assert_().withFailureMessage("Service Status is expected to be 'Open'")
                        .that(cocAinAnElement.getNodeStatus()
                                .equals(expectedServiceStatus))
                        .isTrue();
        }
        cocAinAnElements = serviceAllPage.getActionableItemsTable()
                .getRows().get(0).clickPlusOrMinusIcon()
                .getChildren();
        if (cocAinAnElements.size() > 0)
        {
            for (CocAinAnElement cocAinAnElement : cocAinAnElements)
                assert_().withFailureMessage("Service Status is expected to be 'Open'")
                        .that(cocAinAnElement.getNodeStatus()
                                .equals(expectedServiceStatus))
                        .isTrue();
        }
        cocAinAnElements = serviceAllPage.getAssetNotesTable()
                .getRows().get(0).clickPlusOrMinusIcon()
                .getChildren();
        if (cocAinAnElements.size() > 0)
        {
            for (CocAinAnElement cocAinAnElement : cocAinAnElements)
                assert_().withFailureMessage("Service Status is expected to be 'Open'")
                        .that(cocAinAnElement.getNodeStatus()
                                .equals(expectedServiceStatus))
                        .isTrue();
        }

        //AC: 2
        //Step: 4
        assert_().withFailureMessage("Service item " + expectedServices[0] +
                "is expected to be present")
                .that(serviceAllPage.getConditionsOfClassTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[0]);
        assert_().withFailureMessage("Service item " + expectedServices[1] +
                "is expected to be present")
                .that(serviceAllPage.getActionableItemsTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[1]);
        assert_().withFailureMessage("Service item " + expectedServices[2] +
                "is expected to be present")
                .that(serviceAllPage.getAssetNotesTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[2]);

        ServiceSchedulePage serviceSchedulePage
                = addToJobScopePage.clickServiceScheduleTab();
        assert_().withFailureMessage("Service item " + expectedServices[0] +
                "is expected to be present")
                .that(serviceSchedulePage.getConditionOfClassTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[0]);
        assert_().withFailureMessage("Service item " + expectedServices[1] +
                "is expected to be present")
                .that(serviceSchedulePage.getActionableItemsTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[1]);
        assert_().withFailureMessage("Service item " + expectedServices[2] +
                "is expected to be present")
                .that(serviceSchedulePage.getAssetNotesTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[2]);

        DueAndOverduePage dueAndOverduePage = addToJobScopePage.clickDueAndOverdueTab();
        assert_().withFailureMessage("Service item " + expectedServices[0] +
                "is expected to be present")
                .that(dueAndOverduePage.getConditionOfClassTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[0]);
        assert_().withFailureMessage("Service item " + expectedServices[1] +
                "is expected to be present")
                .that(dueAndOverduePage.getActionableItemsTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[1]);
        assert_().withFailureMessage("Service item " + expectedServices[2] +
                "is expected to be present")
                .that(dueAndOverduePage.getAssetNotesTable()
                        .getRows().get(0).getJobScopeServiceType().replaceAll("(\\d+)", "")
                        .replace("()", "").trim()).isEqualTo(expectedServices[2]);
        //AC: 4
        //Step: 6
        addToJobScopePage.clickAllTab();
        List<Date> dueDates = serviceAllPage.getConditionsOfClassTable()
                .getRows().get(0).getChildren()
                .stream()
                .map(CocAinAnElement::getDueDate)
                .collect(Collectors.toList());
        List<Date> sortedDueDates = dueDates.stream()
                .sorted(LloydsRegister.compareDate().reversed())
                .collect(Collectors.toList());
        assert_().withFailureMessage("All CoCs due date are expected to be " +
                "presented in chronological order")
                .that(dueDates).isEqualTo(sortedDueDates);

        dueDates = serviceAllPage.getActionableItemsTable()
                .getRows().get(0).getChildren()
                .stream()
                .map(CocAinAnElement::getDueDate)
                .collect(Collectors.toList());
        sortedDueDates = dueDates.stream()
                .sorted(LloydsRegister.compareDate().reversed())
                .collect(Collectors.toList());
        assert_().withFailureMessage("All Actionable Items due date are expected to be " +
                "presented in chronological order")
                .that(dueDates).isEqualTo(sortedDueDates);

        List<Date> imposedDates = serviceAllPage.getAssetNotesTable()
                .getRows().get(0).getChildren()
                .stream()
                .map(CocAinAnElement::getImposedDate)
                .collect(Collectors.toList());
        List<Date> sortedImposedDates = imposedDates.stream()
                .sorted(LloydsRegister.compareDate().reversed())
                .collect(Collectors.toList());
        assert_().withFailureMessage("All Asset Notes imposed date are expected \" +\n" +
                "to be presented in chronological order ")
                .that(imposedDates).isEqualTo(sortedImposedDates);

        //AC: 6
        //Step: 8
        serviceAllPage.getConditionsOfClassTable()
                .getRows().get(0).getChildren().get(0)
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class)
                .getHeader()
                .clickNavigateBackButton()
                .clickAllTab()
                .getActionableItemsTable()
                .getRows().get(0).clickPlusOrMinusIcon().getChildren().get(0)
                .clickFurtherDetailsArrow(ViewActionableItemsPage.class)
                .getHeader()
                .clickNavigateBackButton()
                .clickAllTab()
                .getAssetNotesTable()
                .getRows().get(0).clickPlusOrMinusIcon().getChildren().get(0)
                .clickFurtherDetailsArrow(ViewAssetNotePage.class)
                .getHeader()
                .clickNavigateBackButton();
        //AC: 7
        //Step: 9
        addToJobScopePage.getHeader().clickMastLogo();
        workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        asset = new Asset(1);
        name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        allAssetsSubPage = workHubPage.clickAllAssetsTab();

        allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        allAssetsAssetElement.clickViewAssetButton();
        jobsSubPage = viewAssetPage.clickJobsTab();

        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        viewJobDetailsPage = jobsSubPage.getAllJobsElements().stream()
                .filter(e -> e.getJobNumber().equals(jodId))
                .findFirst().get()
                .clickNavigateToJobArrowButton();

        JobScopePage jobScopePage
                = viewJobDetailsPage.clickAddOrGoToJobScopeButton(JobScopePage.class);
        addToJobScopePage = jobScopePage.clickAddServicesCocAIsButton();
        serviceAllPage = addToJobScopePage.clickAllTab();

        ViewConditionOfClassPage viewConditionOfClassPage = serviceAllPage
                .getConditionsOfClassTable()
                .getRows().get(0).clickPlusOrMinusIcon()
                .getChildren()
                .stream()
                .filter(CocAinAnElement::isDefectNodePresent)
                .collect(Collectors.toList()).get(0)
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);
        List<LinkedDefectElements> linkedDefectElement
                = viewConditionOfClassPage.getLinkedDefect();
        linkedDefectElement.get(0).clickFurtherDetailsArrow();
    }
}
