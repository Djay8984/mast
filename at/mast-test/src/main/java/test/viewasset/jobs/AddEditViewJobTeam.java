package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import model.surveyor.Surveyor;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class AddEditViewJobTeam extends BaseTest
{
    @Test(description = "US - 10.10 Add, edit and view job team ")
    @Issue("LRT-1904")

    public void addEditViewJobTeam()
    {
        int assetId = 501;
        String noMatchesFoundErrorMessage = "No matches found";
        String duplicateErrorMessage = "Duplicate names are not permitted";
        String requiredErrorMessage = "Lead surveyor is required";
        String status = "Under Technical Review";

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByAssetId(assetId).clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }
        AllJobElement allJobElement = jobsSubPage.getJobByIndex(1);
        ViewJobDetailsPage viewJobDetailsPage = allJobElement.clickNavigateToJobArrowButton();

        //AC 1
        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        Surveyor leadSurveyor = new Surveyor(2);
        String leadSurveyorName = leadSurveyor.getDto().getName();
        Surveyor authSurveyor = new Surveyor(3);
        String authSurveyorName = authSurveyor.getDto().getName();
        Surveyor fssSpecialist = new Surveyor(4);
        String fssSpecialistName = fssSpecialist.getDto().getName();

        //AC 2
        addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().setSdoCoordinatorTextBox(leadSurveyorName)
                .then().setLeadSurveyorTextBox(leadSurveyorName);
        //AC 3
        assert_().withFailureMessage("Error message " + duplicateErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getDuplicateErrorMessageInLeadSurveyorTextBox())
                .contains(duplicateErrorMessage);

        addOrEditJobTeamPage.clearLeadSurveyorTextbox()
                .then().setLeadSurveyorTextBox(surveyorName)
                .then().clearLeadSurveyorTextbox()
                .then().setLeadSurveyorTextBoxWithoutEnter(surveyorName + "123abcJamesIsaAlphabet");

        assert_().withFailureMessage("Error message " + noMatchesFoundErrorMessage + " is expected")
                .that(addOrEditJobTeamPage.getInLineErrorInLeadSurveyorTextBox())
                .contains(noMatchesFoundErrorMessage);
        //AC 6
        addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName.substring(0, 3))
                .then().setAuthorisingSurveyorTextBox(authSurveyorName)
                .then().setFleetServicesSpecialistTextBox(fssSpecialistName)
                .then().setSdoCoordinatorTextBox(leadSurveyorName);

        //AC 5,9,10
        addOrEditJobTeamPage.clickSaveButton();

        //AC 7,11
        viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        assert_().withFailureMessage("The Add Job team in View mode is expected to display the values")
                .that((addOrEditJobTeamPage.getSdoCoordinator().contains(leadSurveyorName))
                        && (addOrEditJobTeamPage.getLeadSurveyor().contains(surveyorName.substring(0, 3)))
                        && (addOrEditJobTeamPage.getAuthorisingSurveyor().contains(authSurveyorName))
                        && (addOrEditJobTeamPage.getFleetServiceSpecialist().contains(fssSpecialistName)))
                .isTrue();

        //AC 12
        addOrEditJobTeamPage.setSdoCoordinatorTextBox("")
                .then().setAuthorisingSurveyorTextBox("")
                .then().setFleetServicesSpecialistTextBox("")
                .then().setLeadSurveyorTextBox("");

        addOrEditJobTeamPage.clickCancelButton();
        viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        //AC 8
        assert_().withFailureMessage("The Fleet services Specialist field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isSdoCoordinatorTextBoxOptional())
                .isTrue();
        addOrEditJobTeamPage.setSdoCoordinatorTextBox("")
                .then().clickSaveButton();

    }
}
