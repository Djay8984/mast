package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import model.job.Job;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.PaginationCountSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class ViewJobsAssociatedToAssetTest extends BaseTest
{

    private final int maxDisplayed = 8;
    private final String noActiveJobMessage = "There are currently no jobs for this asset.";

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {501},
                {3}
        };
    }

    @DataProvider(name = "AssetsWithZeroJobs")
    public Object[][] assetsWithZeroJobs()
    {
        return new Object[][]{
                {4},
                };
    }

    @Test(
            description = "Story 10.1 As a user I want to view the job(s) associated to the asset" +
                    " so that I can take the appropriate action",
            dataProvider = "Assets"
    )
    @Issue("LRT-1685")
    public void viewJobsAssociatedToAssetTest(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        //AC2 AC1
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement card = allAssetsSubPage.getAssetCardByIndex(0);

        ViewAssetPage viewAssetPage = card.clickJobButton();
        //checks elements via @Visible - verify from Job Count Button
        viewAssetPage.getJobsSubPage();

        //go back to start
        workHubPage = viewAssetPage.getHeader().clickMastLogo();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();
        card = allAssetsSubPage.getAssetCardByIndex(0);
        viewAssetPage = card.clickJobButton();
        //checks elements via @Visible - verify from view all jobs button
        viewAssetPage.getJobsSubPage();

        //go back to start
        workHubPage = viewAssetPage.getHeader().clickMastLogo();
        allAssetsSubPage = workHubPage.clickAllAssetsTab();

        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        AllAssetsAssetElement assetCard = allAssetsSubPage.getAssetCardByIndex(0);
        viewAssetPage = assetCard.clickJobButton();
        //checks elements via @Visible - verify from allAsset Job button
        viewAssetPage.getJobsSubPage();

        //AC3
        viewAssetPage.clickAssetModelTab();
        viewAssetPage.clickJobsTab();
        viewAssetPage.clickServiceScheduleTab();
        viewAssetPage.clickJobsTab();
        viewAssetPage.clickCasesTab();
        viewAssetPage.clickJobsTab();
        viewAssetPage.clickCodicilsAndDefectsTab();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        PaginationCountSubPage paginationCountSubPage = jobsSubPage.getPaginationPage();

        //AC7
        assert_().withFailureMessage("Default number of jobs displayed are at maximum"
                + maxDisplayed)
                .that(jobsSubPage.getAllJobsCount() <= maxDisplayed)
                .isTrue();

        //AC4 - AC does not specify what the default filter is - ignore contents, verify not empty
        while (jobsSubPage.isLoadMoreButtonPresent())
        {
            jobsSubPage.clickLoadMoreButton();
        }

        assert_().withFailureMessage("System is expected to show jobs which meet the default "
                + "filter values")
                .that(jobsSubPage.getAllJobsCount() == paginationCountSubPage.getShownAmount())
                .isTrue();

        List<AllJobElement> jobs = jobsSubPage.getAllJobsElements();

        //AC5
        for (AllJobElement job : jobs)
        {
            assert_().withFailureMessage("Job Number is expected to be displayed")
                    .that(job.isJobNumberDisplayed())
                    .isTrue();
            assert_().withFailureMessage("Job status is expected to be displayed")
                    .that(job.isJobStatusDisplayed())
                    .isTrue();
            assert_().withFailureMessage("Job SDO is expected to be displayed")
                    .that(job.isJobOfficeDisplayed())
                    .isTrue();
            assert_().withFailureMessage("Job ETA is expected to be displayed")
                    .that(job.isJobETADisplayed())
                    .isTrue();
            assert_().withFailureMessage("Job ETD is expected to be displayed")
                    .that(job.isJobETDDisplayed())
                    .isTrue();
        }

        //AC6
        List<Date> jobCreationDates = jobs.stream()
                .map(j -> new Job(Integer.parseInt(j.getJobNumber())))
                .map(j -> j.getDto().getCreatedOn())
                .collect(Collectors.toList());
        Comparator<Date> byMostRecent = (o1, o2) ->
        {
            if (o1.before(o2))
                return 1;
            else if (o1.equals(o2))
                return 0;
            else
                return -1;
        };

        List<Date> sortedCreationDates = jobCreationDates.stream()
                .sorted(byMostRecent)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Jobs are sorted by most recent\n"
                + "expected: " + sortedCreationDates + "\n"
                + "actual: " + jobCreationDates + "\n"
        )
                .that(jobCreationDates.equals(sortedCreationDates))
                .isTrue();
    }

    @Test(
            description = "Story 10.1 As a user I want to view the job(s) associated to the asset" +
                    " so that I can take the appropriate action",
            dataProvider = "AssetsWithZeroJobs"
    )
    @Issue("LRT-1685")
    public void viewJobsAssociatedToAssetTestWithZeroJobs(int assetId)
    {

        //AC8 - no active jobs
        WorkHubPage workHubPage = WorkHubPage.open();
        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement card = allAssetsSubPage.getAssetCardByIndex(0);
        ViewAssetPage viewAssetPage = card.clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.getJobsSubPage();

        assert_().withFailureMessage("Failure message to be displayed: "
                + "expected:  \"" + noActiveJobMessage + "\"\n"
                + "actual: " + jobsSubPage.getMessage())
                .that(jobsSubPage.getMessage().contains(noActiveJobMessage))
                .isTrue();
    }
}
