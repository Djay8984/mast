package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.element.MyJobsElements;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class DeleteExistingJobNoteTest extends BaseTest
{

    final int assetId = 501;

    @Test(description = "US9.5 AC:1-4 - Delete an existing note")
    @Issue("LRT-1649")
    public void deleteJobAssetNoteTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        MyJobsElements myJobsElements = jobsSubPage.getJobsByIndex(0);
        ViewJobDetailsPage myJobsDetailPage = myJobsElements.clickFurtherDetailsArrow();

        //AC: 1
        //Step: 3
        AttachmentsAndNotesPage attachmentsAndNotesPage = myJobsDetailPage.getHeader()
                .clickAttachmentButton();

        // This is part of Pre-requisite.
        //If Note is not present then add a Note

        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() == 0)
        {
            String randomNote = RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().clickAllButton()
                    .then().clickAddNoteButton();

            assert_().withFailureMessage("Added Note is Present")
                    .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(randomNote))
                    .isTrue();
        }

        //AC: 1
        //Step: 4
        String noteText = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText();
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        assert_().withFailureMessage("Edit Note Icon is Displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is Displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 5
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        //AC: 4
        //Step: 6
        confirmationModalPage.clickCancelButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 4
        //Step: 7
        //Cannot close and Reopen the MAST App. So The page to be refreshed
        noteSubPage.refreshPage();
        myJobsDetailPage.getHeader().clickAttachmentButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 3
        //Step: 8
        if (!noteDetailsSubPage.isDeleteIconDisplayed())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        }
        noteDetailsSubPage.clickDeleteIcon().then()
                .clickConfirmationButton(AttachmentsAndNotesPage.class);
        assert_().withFailureMessage("Note is not present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //AC: 3
        //Step: 9
        //Cannot close and Reopen the MAST App. So The page to be refreshed
        noteSubPage.refreshPage();
        myJobsDetailPage.getHeader().clickAttachmentButton();
        assert_().withFailureMessage("Note is not present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();
    }

}
