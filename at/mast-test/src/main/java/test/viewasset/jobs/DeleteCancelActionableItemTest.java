package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.actionableitems.AddActionableItemPage;
import viewasset.sub.jobs.details.actionableitems.ViewActionableItemPage;
import viewasset.sub.jobs.details.actionableitems.element.DataElement;
import viewasset.sub.jobs.details.actionableitems.modal.ConfirmAddedActionableItemWindow;
import viewasset.sub.jobs.details.actionableitems.searchfilter.SearchFilterPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static constant.LloydsRegister.localDate;

public class DeleteCancelActionableItemTest extends BaseTest
{

    final int assetId = 501;
    final String aiWithApprovalTitle = "ApprovalTest";
    final String aiWithoutApprovalTitle = "NoApprovalTest";
    final String aiDescription = "Test Description";
    final String surveyorGuidance = "Test Surveyor Guidance ";
    List<String> aiStatus = Arrays.asList("Deleted", "Change Recommended");

    @Test(description = "10.23 - As a user I want to delete, cancel or mark an " +
            "AI as recommended for closure so that I can close the instance" +
            " that is no longer required")
    @Issue("LRT-2324")
    public void deleteCancelActionableItemTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 3
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        //Step: 4
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.setSdoCode(JobData.sdoCode)
                .selectCaseTypeByVisibleText(JobData.caseType)
                .setJobLocation(JobData.location)
                .setJobDescription(JobData.description)
                .setEtaDate(JobData.strEtaDate)
                .setEtdDate(JobData.strEtdDate)
                .setRequestedAttendanceDate(JobData.strEtdDate)
                .clickSaveButton();

        //Step: 5
        ActionableItemsPage actionableItemsPage = viewJobDetailsPage.clickGoToActionableItemsButton();

        AddActionableItemPage addActionableItemPage = null;

        for (int i = 1; i < 3; i++)
        {
            addActionableItemPage = actionableItemsPage.clickAddNewButton();
            addActionableItems(addActionableItemPage, aiWithApprovalTitle + i, true);
        }
        for (int i = 1; i < 3; i++)
        {
            addActionableItemPage = actionableItemsPage.clickAddNewButton();
            addActionableItems(addActionableItemPage, aiWithoutApprovalTitle + i, false);
        }

        //This is to reset the filter
        //Items of all status will be displayed
        SearchFilterPage searchFilterPage
                = actionableItemsPage.clickSearchFilterActionableItemButton();
        searchFilterPage.clickStatusFilterButton().clickSelectAllButton();
        searchFilterPage.clickSearchButton();
        List<DataElement> dataElements = actionableItemsPage.getJobRelatedTable().getRows();

        //Step: 6
        ViewActionableItemPage viewActionableItemPage = dataElements.stream()
                .filter(e -> e.getTitleText()
                        .contains(aiWithoutApprovalTitle + 1))
                .findFirst()
                .get().clickFurtherDetailsArrowButton();
        assert_().withFailureMessage("Edit Button is expected to be displayed")
                .that(viewActionableItemPage.isEditButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Cancel button is expected to be displayed")
                .that(viewActionableItemPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Close button is expected to be displayed")
                .that(viewActionableItemPage.isCloseButtonDisplayed())
                .isTrue();

        //Step: 7, 8 and 9
        viewActionableItemPage = viewActionableItemPage.clickCloseButton().clickGoBackButton();
        viewActionableItemPage.clickCloseButton().clickDeleteButton();
        dataElements = actionableItemsPage.getJobRelatedTable().getRows();
        assert_().withFailureMessage("When an Item is Closed, the status is expected to be 'Deleted'")
                .that(dataElements.stream()
                        .filter(e -> e.getTitleText().contains(aiWithoutApprovalTitle + 1))
                        .findFirst().get().getItemStatus())
                .isEqualTo(aiStatus.get(0));

        //Step: 10, 11 and 12
        dataElements.stream().filter(e -> e.getTitleText().contains(aiWithoutApprovalTitle + 2))
                .findFirst().get().clickFurtherDetailsArrowButton();
        viewActionableItemPage.clickCancelButton().clickGoBackButton();
        viewActionableItemPage.clickCancelButton().clickCancelButton();
        dataElements = actionableItemsPage.getJobRelatedTable().getRows();
        assert_().withFailureMessage("When an Item is Cancelled, the AI instance is not expected to be displayed")
                .that(dataElements.stream()
                        .filter(e -> e.getTitleText().contains(aiWithoutApprovalTitle + 2))
                        .findFirst().isPresent()).isFalse();

        //Step: 14
        dataElements.stream().filter(e -> e.getTitleText().contains(aiWithApprovalTitle + 1))
                .findFirst().get().clickFurtherDetailsArrowButton();

        assert_().withFailureMessage("Recommend Closure Button is expected to be displayed")
                .that(viewActionableItemPage.isRecommendClosureButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Recommend Amendment Button is expected to be displayed")
                .that(viewActionableItemPage.isRecommendAmendmentButtonDisplayed())
                .isTrue();

        //Step: 15 and 16
        viewActionableItemPage.clickRecommendClosureButton().clickGoBackButton();

        //Step: 17
        viewActionableItemPage.clickRecommendClosureButton().clickRequestClosureButton();
        dataElements = actionableItemsPage.getJobRelatedTable().getRows();
        assert_().withFailureMessage("When an Item is moved Recommend closure, then the status is expected to be 'Change Recommended'")
                .that(dataElements.stream()
                        .filter(e -> e.getTitleText().contains(aiWithApprovalTitle + 1))
                        .findFirst().get()
                        .getItemStatus())
                .isEqualTo(aiStatus.get(1));

        //Step: 19
        dataElements = actionableItemsPage.getJobRelatedTable().getRows();
        dataElements.stream().filter(e -> e.getTitleText().contains(aiWithApprovalTitle + 2))
                .findFirst().get().clickFurtherDetailsArrowButton();

        //Step: 20 and 21
        viewActionableItemPage.clickRecommendAmendmentButton().clickGoBackButton();

        //Step:22
        viewActionableItemPage.clickRecommendAmendmentButton().clickRequestChangeButton();
        dataElements = actionableItemsPage.getJobRelatedTable().getRows();
        assert_().withFailureMessage("When an Item is moved  Recommend Amendment, then the status is expected to be 'Change Recommended'")
                .that(dataElements.stream()
                        .filter(e -> e.getTitleText().contains(aiWithApprovalTitle + 2))
                        .findFirst().get()
                        .getItemStatus())
                .isEqualTo(aiStatus.get(1));

    }

    private ActionableItemsPage addActionableItems(AddActionableItemPage addActionableItemPage,
            String title, boolean approval)
    {
        addActionableItemPage.setTitle(title)
                .clickClassCategoryButton()
                .setDescription(aiDescription)
                .setSurveyorGuidance(surveyorGuidance)
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(localDate.plusDays(1)))))
                .setDueDate(String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(localDate.plusDays(2)))))
                .clickVisibleForAllButton();
        if (approval)
            addActionableItemPage.selectRequiredApprovalCheckBox();
        ConfirmAddedActionableItemWindow confirmAddedActionableItemWindow = addActionableItemPage.clickSaveButton();
        return confirmAddedActionableItemWindow.clickOkButton(ActionableItemsPage.class);
    }
}
