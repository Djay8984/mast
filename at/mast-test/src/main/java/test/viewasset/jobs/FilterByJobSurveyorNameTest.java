package test.viewasset.jobs;

import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.employee.EmployeesNamePage;
import model.job.JobQueryPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.searchfilter.SearchFilterJobsPage;
import viewasset.sub.jobs.searchfilter.sub.SurveyorNameFilterSubPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Arrays;
import java.util.Collections;

import static com.google.common.truth.Truth.assert_;

public class FilterByJobSurveyorNameTest extends BaseTest
{

    @DataProvider(name = "Assets")
    public Object[][] assets()
    {
        return new Object[][]{
                {501}
        };
    }

    @Test(
            description = "Story 10.4 As a user I want to filter by surveyor name " +
                    " so that I can find job(s) that are assigned to specific surveyor/ lead surveyor",
            dataProvider = "Assets"
    )
    @Issue("LRT-1728")
    public void FilterByJobSurveyorName(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        //find asset with all job statuses
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        searchFilterSubPage.clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName()).clickJobButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        SearchFilterJobsPage searchFilterJobsPage = jobsSubPage.clickSearchFilterJobsButton();
        SurveyorNameFilterSubPage surveyorNameFilterSubPage = searchFilterJobsPage.clickSurveyorNameFilterButton();

        // AC1  Type-ahead prompt
        assert_().withFailureMessage("Place holder text in the SDO field is meant to state 'Start typing in Surveyor name'")
                .that(surveyorNameFilterSubPage.getPlaceHolderText())
                .isEqualTo("Start typing in Surveyor name");

        //de-scope to check scrollable with > 8 items

        // AC1  Display matching options to the user after 1 character has been entered
        surveyorNameFilterSubPage.setSurveyorsnameInputField("M");
        assert_().withFailureMessage("Typeahead result is showed after entering 1 character in Survery Name input field")
                .that(surveyorNameFilterSubPage.getSurveyorNameSearchResultCount()).isNotEqualTo(0);

        assert_().withFailureMessage("Number of Typeahead result showed is the same with BE with 1 character searching")
                .that(surveyorNameFilterSubPage.getSurveyorNameSearchResultCount())
                .isEqualTo(new EmployeesNamePage("%M%").getDto().getContent().size());

        // AC1  Refine options with entering additional character
        surveyorNameFilterSubPage.setSurveyorsnameInputField("Mi");
        assert_().withFailureMessage("Number of Typeahead result showed is the same with BE with 2 character searching")
                .that(surveyorNameFilterSubPage.getSurveyorNameSearchResultCount())
                .isEqualTo(new EmployeesNamePage("%Mi%").getDto().getContent().size());

        surveyorNameFilterSubPage.setSurveyorsnameInputField("Min");
        assert_().withFailureMessage("Number of Typeahead result showed is the same with BE with 3 character searching")
                .that(surveyorNameFilterSubPage.getSurveyorNameSearchResultCount())
                .isEqualTo(new EmployeesNamePage("%Min%").getDto().getContent().size());

        if (surveyorNameFilterSubPage.getSurveyorNameSearchResultCount() > 0)
        {
            String firstSearchResult = surveyorNameFilterSubPage.getTypeaheadResults().get(0).getText();
            surveyorNameFilterSubPage.clickSurveyorNameResultByName(firstSearchResult);
            // AC1 Typeahead annotation 4 Select option
            assert_().withFailureMessage("[" + firstSearchResult + "] is selected from search result")
                    .that(surveyorNameFilterSubPage.getSurveyorsnameInputField())
                    .isEqualTo(firstSearchResult);
            assert_().withFailureMessage("Surveyors name input field with typeaheaed clearable is displayed")
                    .that(surveyorNameFilterSubPage.IsSurveyorsNameInputFieldWithTypeaheaedClearableIsDisplayed()).isTrue();
            //AC1 Type ahead Annotation 5
            assert_().withFailureMessage("Surveyors name input field with typeaheaed clearable is not editable")
                    .that(surveyorNameFilterSubPage.IsSurveyorsNameInputFieldWithTypeaheaedClearableIsEnabled()).isFalse();
            //AC1 Type ahead Annotation 5
            surveyorNameFilterSubPage.clickClear();
            assert_().withFailureMessage("Surveyors name input field is resets to initial blank state")
                    .that(surveyorNameFilterSubPage.getSurveyorsnameInputField()).isEqualTo("");

        }

        //AC1 no result
        surveyorNameFilterSubPage.setSurveyorsnameInputField("Something Not Exit");
        assert_().withFailureMessage("Message [No matches] is shown in search result").that(surveyorNameFilterSubPage.isResultsEmpty()).isTrue();

        //AC1 Typeahead Annotation 6 No matches found
        assert_().withFailureMessage("Invalid Surveyors Name in input field").that(surveyorNameFilterSubPage.getValidationErrors())
                .isEqualTo("No match found");

        assert_().withFailureMessage("Invalid Surveyors Name in input field").that(surveyorNameFilterSubPage.getValidationErrors())
                .isEqualTo("No match found");
        assert_().withFailureMessage("Other button is block when there is error in Surveyor name input field.")
                .that(surveyorNameFilterSubPage.isOtherSearchBlocked()).isTrue();

        //AC2  jobs assigned to the defined surveyor
        LrEmployeeDto lrEmployeeDto = new EmployeesNamePage("%Jose%").getDto().getContent().get(0);

        JobQueryDto jobQueryDto = new JobQueryDto();
        jobQueryDto.setEmployeeId(Collections.singletonList(lrEmployeeDto.getId()));
        jobQueryDto.setAssetId(Collections.singletonList((long) assetId));
        jobQueryDto.setJobStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L));
        JobQueryPage jobQueryPage = new JobQueryPage(jobQueryDto);

        searchFilterJobsPage.clickResetButton().then().clickSearchFilterJobsButton();
        surveyorNameFilterSubPage = searchFilterJobsPage.clickSurveyorNameFilterButton();
        surveyorNameFilterSubPage.setSurveyorsnameInputField(lrEmployeeDto.getName());
        searchFilterJobsPage.clickSearchButton();
        assert_().withFailureMessage("Results shown match what was returned in the BE when searched with surveyor name.")
                .that(jobsSubPage.getAllJobsCount())
                .isEqualTo(jobQueryPage.getDto().getContent().size());

        //AC3 todo Surveyors are defined as "Lead surveyors" not yet implement

    }

}
