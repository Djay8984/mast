package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import configurationpage.ConfigurationPage;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.DueAndOverduePage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.element.MyWorkAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.MyWorkSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class SelectToAddServicesToJobScopeForFirstTimeTest extends BaseTest
{

    @Test(description = "10.11 AC 1-4 - Select to add services to the job scope for the first time")
    @Issue("LRT-2209")
    public void selectToAddServicesToJobScopeForFirstTimeTest()
    {
        final int assetId = 501;
        final String defaultJobScopeText = "Job scope to be defined";

        WorkHubPage workHubPage = WorkHubPage.open();
        ConfigurationPage.setMastGroup(workHubPage.getHeader().clickConfigPage(), "Admin");
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetId(assetId);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();


        //AC1
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.addNewJob();
        assert_().withFailureMessage("Job scope is empty and is expected to display: "
                + defaultJobScopeText)
                .that(viewJobDetailsPage.getJobScopeText())
                .isEqualTo(defaultJobScopeText);

        //AC2 AC3 AC4 - @Visible verifies addToJobScopePage
        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        DueAndOverduePage dueAndOverduePage = addToJobScopePage.getDueAndOverdueSubPage();

        //AC4
        addToJobScopePage.clickAllTab();
        assert_().withFailureMessage("All Tab is expected to be selected")
                .that(addToJobScopePage.isAllTabSelected())
                .isTrue();
        addToJobScopePage.clickServiceScheduleTab();
        assert_().withFailureMessage("Service Schedule Tab is expected to be selected")
                .that(addToJobScopePage.isServiceScheduleTabSelected())
                .isTrue();
        addToJobScopePage.clickDueAndOverdueTab();
        assert_().withFailureMessage("Due and Overdue Tab is expected to be selected")
                .that(addToJobScopePage.isDueAndOverdueTabSelected())
                .isTrue();
    }
}
