package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import constant.LloydsRegister.JobData;
import model.asset.Asset;
import org.openqa.selenium.Dimension;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.codicilsanddefects.modalwindow.EditConfirmationWindow;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.conditionofclass.AddConditionOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class CreateViewAmendInheritedOrNonDefectCocTest extends BaseTest
{

    final int assetId = 501;

    @Test(description = "US 10.33 - As a user I want to create, view and/or amend " +
            "an inherited or non-defect CoC as part of the job, so that I can" +
            " create a CoC without a defect against the Asset")
    @Issue("LRT-2243")
    public void viewEditInheritedOrNonDefectCocTest()
    {

        //Step:1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step:2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step:3
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobSubPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobSubPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(now().format(FRONTEND_TIME_FORMAT_DTS))
                .then().setEtdDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .then().setRequestedAttendanceDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .then().clickSaveButton();

        ConditionsOfClassPage conditionsOfClassPage = viewJobDetailsPage.clickGoToCocsButton();

        //AC: 2
        //Step: 4 and 5
        AddConditionOfClassPage addConditionOfClassPage = conditionsOfClassPage.clickAddNewButton();
        addConditionOfClassPage.setTitle(CocData.title);
        assert_().withFailureMessage("Description, Imposed date, Due date and " +
                "Visible for fields are expected to to be mandatory ")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isFalse();

        addConditionOfClassPage.setDescription(CocData.description);
        assert_().withFailureMessage("Imposed date, Due date and Visible for fields are expected to to be mandatory ")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isFalse();

        addConditionOfClassPage.setImposedDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Due date and Visible for fields are expected to to be mandatory ")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isFalse();
        addConditionOfClassPage.setDueDate(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Visible for field is expected to to be mandatory")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isFalse();
        addConditionOfClassPage.clickLrAndCustomerButton();
        assert_().withFailureMessage("Title, Description, Imposed date, Due date and " +
                "Visible for fields are expected to to be mandatory ")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 2
        //Step: 13
        assert_().withFailureMessage("Save Button is expected to be clickable")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 2
        //Step:6
        addConditionOfClassPage.setImposedDate(now().minusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Error Icon/Message is Expected to be displayed")
                .that(addConditionOfClassPage
                        .isErrorIconDisplayedInImposedDateTextBox(AddConditionOfClassPage.type.ERROR)
                        || addConditionOfClassPage.getImposedDateErrorMessageText().
                        equals(CocData.expectedImposedDateErrorMessage))
                .isTrue();
        assert_().withFailureMessage("Due Date Text Box is expected to be disabled")
                .that(addConditionOfClassPage.isDueDateFieldEnabled())
                .isFalse();

        //AC: 2
        //Step: 7
        addConditionOfClassPage.setImposedDate(String.valueOf(now().minusDays(2)));
        assert_().withFailureMessage("Error Icon/Message is Expected to be displayed")
                .that(addConditionOfClassPage
                        .isErrorIconDisplayedInImposedDateTextBox(AddConditionOfClassPage.type.ERROR)
                        || addConditionOfClassPage.getImposedDateErrorMessageText().
                        equals(CocData.expectedImposedInvalidDateMessage))
                .isTrue();

        //AC: 2
        //Step: 8
        addConditionOfClassPage.setImposedDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Error Icon/Message is not Expected to be displayed")
                .that(addConditionOfClassPage
                        .isErrorIconDisplayedInImposedDateTextBox(AddConditionOfClassPage.type.ERROR))
                .isFalse();
        assert_().withFailureMessage("User is expected to enter due dates as it " +
                "is not before today's date and is in the correct format ")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 2
        //Step: 9
        assert_().withFailureMessage("Title Field Helper Text is expected to be displayed")
                .that(addConditionOfClassPage.getTitleFieldHelperText()
                        .equals(CocData.expectedTitleHelperText))
                .isTrue();

        assert_().withFailureMessage("Description Field Helper Text is expected to be displayed")
                .that(addConditionOfClassPage.getTitleDescriptionFieldHelperText()
                        .equals(CocData.expectedDescriptionHelperText))
                .isTrue();

        //AC: 2
        //Step: 11
        assert_().withFailureMessage("LR Internal Button is expected to be Disabled")
                .that(addConditionOfClassPage.isLrInternalButtonEnabled())
                .isFalse();
        assert_().withFailureMessage("LR and Customer Button is expected to be Enabled")
                .that(addConditionOfClassPage.isLrAndCustomerButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("All button is expected to be Enabled")
                .that(addConditionOfClassPage.isAllButtonEnabled())
                .isTrue();

        //Step: 12
        conditionsOfClassPage
                = addConditionOfClassPage.clickCancelButton();
        addConditionOfClassPage = conditionsOfClassPage.clickAddNewButton();
        assert_().withFailureMessage("Title Text Box is expected to be empty")
                .that(addConditionOfClassPage.getTitleText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Description Text Box is expected to be empty")
                .that(addConditionOfClassPage.getDescriptionText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Imposed date Text Box is expected to be empty")
                .that(addConditionOfClassPage.getImposedDateText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Due Date Text Box is expected to be empty")
                .that(addConditionOfClassPage.getDueDateText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Save button is expected to be disabled")
                .that(addConditionOfClassPage.isSaveButtonEnabled())
                .isFalse();

        //Step: 14, 15 and 16
        conditionsOfClassPage = addConditionOfClassPage.setDescription(CocData.description)
                .setTitle(CocData.title)
                .setDescription(CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(ConditionsOfClassPage.class);

        //AC: 3
        //Step: 17
        //TODO: Related to LRD-3370. Once Fixed will work on this

        //AC: 5
        //Step: 19
        ConditionsOfClassPage.JobRelatedTable jobRelatedTable
                = conditionsOfClassPage.getJobRelatedTable();
        assert_().withFailureMessage("Added COc is expected to be present in Job related section")
                .that(jobRelatedTable.getRows()
                        .stream().filter(e -> e.getCocTitle().contains(CocData.title))
                        .findFirst()
                        .isPresent())
                .isTrue();

        //AC: 4
        //Step: 18
        ViewConditionOfClassPage viewConditionOfClassPage
                = jobRelatedTable.getRows()
                .stream().filter(e -> e.getCocTitle().contains(CocData.title))
                .findFirst()
                .get()
                .clickFurtherDetailsArrow();
        assert_().withFailureMessage("Added Title is expected to be present")
                .that(viewConditionOfClassPage.getTitleText()
                        .equals(CocData.title.toLowerCase()))
                .isTrue();
        assert_().withFailureMessage("Added description is expected to be present")
                .that(viewConditionOfClassPage.getCocDescriptionText()
                        .equals(CocData.description.toLowerCase()))
                .isTrue();
        assert_().withFailureMessage("Added Imposed date is expected to be present")
                .that(viewConditionOfClassPage.getCocImposeDate()
                        .equals(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();
        assert_().withFailureMessage("Added Due date is expected to be present")
                .that(viewConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //Step: 20
        assert_().withFailureMessage("Edit button is expected to be Enabled")
                .that(viewConditionOfClassPage.isEditButtonEnabled())
                .isTrue();

        //Step: 21 and 22
        EditConditionOfClassPage editConditionOfClassPage
                = viewConditionOfClassPage.clickEditButton();

        assert_().withFailureMessage("Description Field is expected to be editable")
                .that(editConditionOfClassPage.isDescriptionFieldEditable())
                .isTrue();
        assert_().withFailureMessage("Due Date Field is expected to be editable")
                .that(editConditionOfClassPage.isDueDateFieldEditable())
                .isTrue();

        String descriptionEdited = CocData.description + " Edit";
        editConditionOfClassPage.setDescription(descriptionEdited)
                .setDueDate(now().plusDays(4)
                        .format(FRONTEND_TIME_FORMAT_DTS));

        //Step: 23
        viewConditionOfClassPage = editConditionOfClassPage.clickSaveButton();
        assert_().withFailureMessage("Edited description is expected to be present")
                .that(viewConditionOfClassPage.getCocDescriptionText()
                        .equals(descriptionEdited.toLowerCase()))
                .isTrue();
        assert_().withFailureMessage("Edited Due date is expected to be present")
                .that(viewConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(4).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //Step: 10
        viewConditionOfClassPage.clickEditButton();
        Dimension beforeResize = addConditionOfClassPage.getTextAreaDimension();
        addConditionOfClassPage.setTextAreaSizeByOffset(0, 300);
        Dimension afterResize = addConditionOfClassPage.getTextAreaDimension();
        assert_().withFailureMessage("Description Text box is expected to be resized")
                .that(beforeResize.equals(afterResize)).isFalse();

        addConditionOfClassPage.clickSaveButton(ViewConditionOfClassPage.class);
        //Step: 24
        viewConditionOfClassPage.clickNavigateBackButton(ConditionsOfClassPage.class);
        jobRelatedTable = conditionsOfClassPage.getJobRelatedTable();
        assert_().withFailureMessage("Edited Non Inherited Coc is expected to be present" +
                " in Job related section ").that(jobRelatedTable.getRows()
                .stream().filter(e -> e.getCocTitle().contains(CocData.title))
                .findFirst()
                .isPresent())
                .isTrue();

        //Step: 25
        assert_().withFailureMessage("").that(jobRelatedTable.getRows()
                .stream().filter(e -> e.getCocId().matches(".*\\d+.*"))
                .findFirst()
                .isPresent())
                .isTrue();
    }

    @Test(description = "US 10.33 - As a user I want to create, view and/or amend " +
            "an inherited or non-defect CoC as part of the job, so that I can " +
            "create a CoC without a defect against the Asset")
    @Issue("LRT-2244")
    public void editInheritedOrNonDefectCocOutsideEditModeTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        AddNewJobPage addNewJobSubPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobSubPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(now().format(FRONTEND_TIME_FORMAT_DTS))
                .then().setEtdDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .then().setRequestedAttendanceDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .then().clickSaveButton();

        ConditionsOfClassPage conditionsOfClassPage
                = viewJobDetailsPage.clickGoToCocsButton();
        AddConditionOfClassPage addConditionOfClassPage
                = conditionsOfClassPage.clickAddNewButton();
        addConditionOfClassPage.setTitle(CocData.title)
                .setDescription(CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(ConditionsOfClassPage.class);

        //AC: 1
        //Step: 3
        ConditionsOfClassPage.JobRelatedTable jobRelatedTable
                = conditionsOfClassPage.getJobRelatedTable();
        assert_().withFailureMessage("Added Coc is expected to be present in Job related section")
                .that(jobRelatedTable.getRows()
                        .stream().filter(e -> e.getCocTitle().contains(CocData.title))
                        .findFirst()
                        .isPresent())
                .isTrue();

        ViewConditionOfClassPage viewConditionOfClassPage
                = jobRelatedTable.getRows()
                .stream().filter(e -> e.getCocTitle().contains(CocData.title))
                .findFirst()
                .get()
                .clickFurtherDetailsArrow();

        //AC: 10
        //STep: 4
        assert_().withFailureMessage("Edit button is expected to be enabled")
                .that(viewConditionOfClassPage.isEditButtonEnabled())
                .isTrue();

        //AC: 10
        //Step: 5
        EditConditionOfClassPage editConditionOfClassPage
                = viewConditionOfClassPage.clickEditButton();

        editConditionOfClassPage.clickCancelButton();
        viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Changes made in description should not be saved")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(CocData.description))
                .isTrue();
        assert_().withFailureMessage("Changes made for Due date should not be saved")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //AC: 10
        //Step: 6
        String descriptionEdited = CocData.description + " Edit";
        editConditionOfClassPage.setDescription(descriptionEdited)
                .setDueDate(now().plusDays(4)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        EditConfirmationWindow editConfirmationWindow
                = editConditionOfClassPage.clickNavigateBackButton(EditConfirmationWindow.class);
        viewConditionOfClassPage
                = editConfirmationWindow.clickContinueWithoutSaveChangesButton();
        viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Changes made in description should not be saved")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(CocData.description))
                .isTrue();
        assert_().withFailureMessage("Changes made for Due date should not be saved")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //AC: 10
        //Step: 7
        editConditionOfClassPage.setDescription(descriptionEdited)
                .setDueDate(now().plusDays(4)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        editConfirmationWindow
                = editConditionOfClassPage.clickNavigateBackButton(EditConfirmationWindow.class);
        editConfirmationWindow.clickSaveChangesButton();
        viewConditionOfClassPage.clickEditButton();

        assert_().withFailureMessage("Changes made in description should be saved")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(descriptionEdited))
                .isTrue();
        assert_().withFailureMessage("Changes made for Due date should be saved")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(4).format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();
    }
}
