package test.viewasset.jobs;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import model.asset.Asset;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.note.EditNoteSubPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.element.NoteAndAttachmentElement;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.actionableitems.AddActionableItemPage;
import viewasset.sub.jobs.details.actionableitems.AddActionableItemPage.Type;
import viewasset.sub.jobs.details.actionableitems.EditActionableItemPage;
import viewasset.sub.jobs.details.actionableitems.ViewActionableItemPage;
import viewasset.sub.jobs.details.actionableitems.element.DataElement;
import viewasset.sub.jobs.details.actionableitems.element.ItemElement;
import viewasset.sub.jobs.details.defects.AddDefectPage;
import viewasset.sub.jobs.details.defects.DefectsPage;
import viewasset.sub.jobs.details.defects.ViewDefectPage;
import workhub.WorkHubPage;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.*;
import static constant.LloydsRegister.JobData.*;
import static java.time.LocalDate.now;
import static viewasset.sub.commonpage.note.EditNoteSubPage.Type.ERROR;

// todo temporarily commented Tests that use LazyItemDto.getRelated() as structure has changed
// and merge is required to unblock stories, these should be refactored to the new structure asap
// see: TomB
public class CreateEditAiTest extends BaseTest
{

    final int assetId = 501;
    final int maximumCharLimit = 1000;
    final String noteHelperText = "Type your note here";
    final String currentUser = "Allan Hyde";
    String noteText = RandomStringUtils.randomAlphabetic(10);
    String specialChar = "%%#$%^&*(@!#$%%%%%%";
    private Asset asset;

    @BeforeMethod(alwaysRun = true)
    private void getData()
    {
        asset = new Asset(assetId);
    }

    @Test(description = "US10.22 AC 1 - 3 As part of Current Job create a new and/or edit AI")
    @Issue("LRT-2424")
    public void createNewAiAndNewDefectTest()
    {
        //Step: 1, 2, 3, 4, 5, 6 and 7
        //AC: 1, AC: 2 and AC: 3 are covered by @Visible Annotation
        navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .clickAddNewButton()
                .getHeaderPage()
                .clickNavigateBackButton(ActionableItemsPage.class)
                .clickNavigateBackButton()
                .clickGoToDefectsButton()
                .clickAddNewButton();
    }

    @Test(description = "US10.22 AC 7 - 12 As part of Current Job create a new and/or edit AI")
    @Issue("LRT-2428")
    public void addAiFromDefectTest()
    {
        //AC: 7
        //Step: 1, 2, 3, 4,5 and 6
        AddDefectPage addDefectPage = navigateToJobAndCreateNewJob()
                .clickGoToDefectsButton()
                .clickAddNewButton();
        addDefectPage.setTitle(DefectData.title)
                .clickElectroTechnicalButton();

        ActionableItemsPage actionableItemsPage = addDefectPage
                .setDescription(DefectData.description)
                .clickSaveButton()
                .clickOkButton()
                .clickNavigateBackButton()
                .getJobRelatedTable().getRows()
                .stream().filter(e -> e.getTitleText().contains(DefectData.title))
                .findFirst().get().clickFurtherDetailsArrowButton()
                .clickAddActionableItemButton()
                .setTitle(AiData.title)
                .clickClassCategoryButton()
                .setDescription(AiData.description)
                .setSurveyorGuidance(AiData.surveyorGuidance)
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(localDate.plusDays(1)))))
                .setDueDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(localDate.plusDays(2)))))
                .clickVisibleForAllButton()
                .clickSaveButton()
                .clickOkButton(ViewDefectPage.class)
                .clickNavigateBackButton()
                .clickNavigateBackButton()
                .clickGoToActionableItemsButton();

        //AC: AC: 7 and 8
        //Step: 7
        assert_().withFailureMessage("Added AI is expected to be displayed ")
                .that(actionableItemsPage.getJobRelatedTable()
                        .getRows().stream()
                        .filter(e -> e.getTitleText().contains(AiData.title))
                        .findFirst().isPresent())
                .isTrue();

        //TODO: need clarification for step 8
        //Step: 8

        //AC: 10
        //Step: 9
        ViewActionableItemPage viewActionableItemPage
                = actionableItemsPage.getJobRelatedTable()
                .getRows()
                .stream()
                .filter(e -> e.getTitleText().contains(AiData.title))
                .findFirst()
                .get()
                .clickFurtherDetailsArrowButton();
        assert_().withFailureMessage("Edit button is expected to be enabled ")
                .that(viewActionableItemPage.isEditButtonEnabled())
                .isTrue();
        EditActionableItemPage editActionableItemPage
                = viewActionableItemPage.clickEditButton();

        //AC: 11
        //Step: 10
        assert_().withFailureMessage("Visible for all button is expected to disabled ")
                .that(editActionableItemPage.isVisibleForAllButtonEnabled())
                .isFalse();
        assert_().withFailureMessage("Visible for LR and Customer button is expected to disabled ")
                .that(editActionableItemPage.isVisibleForLrAndCustomerButtonEnabled())
                .isFalse();
        assert_().withFailureMessage("Visible for LR Internal button is expected to disabled ")
                .that(editActionableItemPage.isVisibleForLrInternalButtonEnabled())
                .isFalse();
        //AC: 12
        //Step: 11
        String editDescription = AiData.description + "Edit";
        editActionableItemPage
                .setDescription(editDescription)
                .clickSaveButton();
        assert_().withFailureMessage("Edited description is expected to be displayed ")
                .that(viewActionableItemPage.getDescription())
                .isEqualTo(editDescription);
    }

    @Test(description = "US10.22 AC4 - 8.4 - AC 1-13 - From the Job Specific " +
            "Ai Page - Add a new Actionable Item without a template")
    @Issue("LRT-2333")
    public void addAiWithoutTemplateTest()
    {
        //Step: 1 and 2
        AddActionableItemPage addActionableItemPage
                = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .clickAddNewButton();

        //AC: 1 and AC: 2
        //Step: 3
        assert_().withFailureMessage("Title is expected to be blank")
                .that(addActionableItemPage.getTitle())
                .isEmpty();
        assert_().withFailureMessage("Description is expected to be blank")
                .that(addActionableItemPage.getDescription())
                .isEmpty();
        assert_().withFailureMessage("Surveyor Guidance is expected to be blank")
                .that(addActionableItemPage.getSurveyorGuidance())
                .isEmpty();

        //AC: 3
        //Step: 4
        assert_().withFailureMessage("Required Approval CheckBox is not expected to be selected")
                .that(addActionableItemPage.isRequiredApprovalCheckBoxSelected())
                .isFalse();

        //AC: 8
        //Step: 5
        assert_().withFailureMessage("Title Helper Text is expected to be displayed")
                .that(addActionableItemPage.getTitleFieldHelperText())
                .isEqualTo(AiData.expectedTitleHelperText);
        assert_().withFailureMessage("Description Helper Text is expected to be displayed")
                .that(addActionableItemPage.getDescriptionFieldHelperText())
                .isEqualTo(AiData.expectedDescriptionSurveyorGuidanceHelperText);
        assert_().withFailureMessage("Surveyor Guidance Helper Text is expected to be displayed")
                .that(addActionableItemPage.getSurveyorGuidanceFieldHelperText())
                .isEqualTo(AiData.expectedDescriptionSurveyorGuidanceHelperText);

        //AC: 9
        //Step: 6
        Dimension beforeResize = addActionableItemPage.getDescriptionFieldDimension();
        addActionableItemPage.setDescriptionTextBoxSizeByOffset(0, 100);
        Dimension afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Description Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        beforeResize = addActionableItemPage.getSurveyorGuidanceFieldDimension();
        addActionableItemPage.setSurveyorGuidanceTextBoxSizeByOffset(0, 150);
        afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Surveyor Guidance Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        //AC: 3, AC: 4 and AC: 10\
        //Step: 7 and 9
        addActionableItemPage.setTitle(AiData.title);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickClassCategoryButton();
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDescription(AiData.description);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(1)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDueDate(String.valueOf(FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickVisibleForAllButton();
        assert_().withFailureMessage("Mandatory fields are filled and save button is expected to be enabled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 7
        //Step: 8
        addActionableItemPage.setTitle("");
        assert_().withFailureMessage("Title Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isTitleTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();
        addActionableItemPage.setDescription("");
        assert_().withFailureMessage("Description Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isDescriptionTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();
        addActionableItemPage.setImposedDate("");
        assert_().withFailureMessage("Imposed Date Text Box Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();

        //AC: 5 and 6
        //Step: 10 and 11
        addActionableItemPage.setTitle(AiData.title)
                .setDescription(AiData.description)
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(1)))))
                .setDueDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Imposed Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isFalse();
        assert_().withFailureMessage("Due Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isDueDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isFalse();

        //AC: 12 and 13
        //Step: 14
        ActionableItemsPage actionableItemsPage = addActionableItemPage
                .clickStartFromTemplateButton()
                .clickXButton()
                .clickSaveButton()
                .clickOkButton(ActionableItemsPage.class);

        //AC: 4 and 11
        //Step: 12 and 13
        List<DataElement> dataElements = actionableItemsPage
                .getJobRelatedTable()
                .getRows();
        assert_().withFailureMessage("Newly created AI with status Open is expected to be displayed")
                .that(dataElements
                        .stream()
                        .filter(e -> e.getTitleText().contains(AiData.title))
                        .findFirst()
                        .get()
                        .getItemStatus()
                        .equals("Open"))
                .isTrue();
    }

    //    @Test(description = "US 10.22 - 8.5 - AC 1-14 - From the Job Specific AI " +
    //            "page - Add a New Actionable Item without a template - associate " +
    //            "to an Asset or Item")
    //    @Issue("LRT-2334")
    //    public void addAiWithAssetItemTest() {
    //        //Step: 1 and 2
    //        AddActionableItemPage addActionableItemPage
    //                = navigateToJobAndCreateNewJob()
    //                .clickGoToActionableItemsButton()
    //                .clickAddNewButton();
    //        SelectAssetItemPage selectAssetItemPage = addActionableItemPage.clickSelectItemButton();
    //        List<ItemElement> items = selectAssetItemPage.getAssetItemTable().getItems();
    //
    //        //AC: 3
    //        //Step: 8
    //        List<ItemElement> children = items.get(0).getChildren();
    //        children = scrollAndExpandAllChildElements(children, 0);
    //        assert_().withFailureMessage("At lowest level there will not be the option" +
    //                " available to expand")
    //                .that(children.stream()
    //                        .filter(e -> (e.isPlusIconDisplayed()))
    //                        .collect(Collectors.toList()).size())
    //                .isEqualTo(0);
    //
    //        //AC: 1 and AC: 2
    //        //Step: 5 and 6
    //        LazyItemDto rootItem = new AssetRootItem(assetId).getDto();
    //        List<Long> assetItems = rootItem.getItems()
    //                .stream()
    //                .map(LinkResource::getId)
    //                .collect(Collectors.toList());
    //
    //        ItemQueryDto query = new ItemQueryDto();
    //        query.setItemId(assetItems);
    //        List<LazyItemDto> assetModel = new AssetItemQuery(query, assetId)
    //                .getDto()
    //                .getContent();
    //
    //        List<String> beItems = assetModel
    //                .stream()
    //                .map(ItemBaseDto::getName)
    //                .collect(Collectors.toList());
    //        assert_().withFailureMessage("First Level of children are expected to be displayed")
    //                .that(beItems).isEqualTo(children.stream()
    //                .map(ItemElement::getItemName)
    //                .collect(Collectors.toList()));
    //
    //
    //        //AC: 6
    //        //Step: 11
    //        for (ItemElement parentItems : items) {
    //            assert_().withFailureMessage("By default no item is expected to be selected ")
    //                    .that(parentItems.isRadioButtonSelected()).isFalse();
    //        }
    //        for (ItemElement childItems : children) {
    //            assert_().withFailureMessage("By default no item is expected to be selected ")
    //                    .that(childItems.isRadioButtonSelected()).isFalse();
    //        }
    //
    //        //AC: 9
    //        //Step: 15
    //        assert_().withFailureMessage("Assign selected button is not expected to be Enabled")
    //                .that(selectAssetItemPage.isAssignSelectedButtonEnabled()).isFalse();
    //
    //        //AC: 1
    //        //Step: 4 and 16
    //        children.get(0).clickRadioButton();
    //        assert_().withFailureMessage("Radio Button is expected to be clicked")
    //                .that(children.get(0).isRadioButtonSelected()).isTrue();
    //
    //        assert_().withFailureMessage("Assign selected button is not expected to be Enabled")
    //                .that(selectAssetItemPage.isAssignSelectedButtonEnabled()).isTrue();
    //
    //        //AC: 5
    //        //Step: 9
    //        children.get(1).clickRadioButton();
    //        assert_().withFailureMessage("Multiple items cannot be selected")
    //                .that(children.get(0).isRadioButtonSelected()).isFalse();
    //        assert_().withFailureMessage("Multiple items cannot be selected")
    //                .that(children.get(1).isRadioButtonSelected()).isTrue();
    //
    //        String firstSelectedItem = children.get(1).getItemName();
    //
    //        //AC: 4
    //        //Step: 10
    //        for (ItemElement parentItems : items) {
    //            assert_().withFailureMessage("Item names is expected to be a maximum of 40 characters ")
    //                    .that(parentItems.getItemName().length() <= 40)
    //                    .isTrue();
    //        }
    //        for (ItemElement childItems : children) {
    //            assert_().withFailureMessage("Item names is expected to be a maximum of 40 characters ")
    //                    .that(childItems.getItemName().length() <= 40)
    //                    .isTrue();
    //        }
    //
    //        //todo: Step: 7,12,13,14
    //
    //        //AC: 9 and AC: 10
    //        //Step: 16 and 17
    //        assert_().withFailureMessage("Added Item is expected to be displayed")
    //                .that(selectAssetItemPage.clickAssignSelectedButton()
    //                        .getSelectedItemText())
    //                .isEqualTo(firstSelectedItem);
    //        assert_().withFailureMessage("Remove Icon is expected to be displayed")
    //                .that(addActionableItemPage.isRemoveItemIconDisplayed())
    //                .isTrue();
    //
    //        //AC:11
    //        //Step: 18
    //        assert_().withFailureMessage("Change Selection is expected to be displayed")
    //                .that(addActionableItemPage.isChangeSelectionButtonDisplayed()).isTrue();
    //
    //        //AC: 12 and AC: 13
    //        //Step: 19 and 20
    //        children = addActionableItemPage.clickChangeSelectionButton()
    //                .getAssetItemTable().getItems().get(0).getChildren();
    //
    //        assert_().withFailureMessage("Item Selected by user is expected to be selected ")
    //                .that(children.get(1).isRadioButtonSelected())
    //                .isTrue();
    //
    //        children.get(2).clickRadioButton();
    //        String secondSelectedItem = children.get(2).getItemName();
    //        assert_().withFailureMessage("Selected different Item is expected to be displayed")
    //                .that(selectAssetItemPage.clickAssignSelectedButton()
    //                        .getSelectedItemText())
    //                .isEqualTo(secondSelectedItem);
    //
    //        //AC: 13
    //        //Step: 21
    //        addActionableItemPage.clickRemoveItemIcon();
    //        assert_().withFailureMessage("Item Selected is not expected to be displayed ")
    //                .that(addActionableItemPage.isSelectedItemDisplayed())
    //                .isFalse();
    //
    //        //AC: 14
    //        //Step: 22
    //        addActionableItemPage.setTitle(AiData.title)
    //                .setDescription(AiData.description)
    //                .clickSelectItemButton()
    //                .clickCancelButton();
    //        assert_().withFailureMessage("previously entered information is not expected changed")
    //                .that(addActionableItemPage.getTitle())
    //                .isEqualTo(AiData.title);
    //        assert_().withFailureMessage("previously entered information is not expected changed")
    //                .that(addActionableItemPage.getDescription())
    //                .isEqualTo(AiData.description);
    //    }

    @Test(description = "US10.22 AC5,6 - 8.4 - AC 1-13 - From the Open Defect " +
            "Page - Add a new Actionable Item without a template")
    @Issue("LRT-2335")
    public void addAiFromOpenDefectPageTest()
    {
        //AC: 1 and 2
        //Step: 1, 2, 3, 4 and 5
        AddDefectPage addDefectPage = navigateToJobAndCreateNewJob()
                .clickGoToDefectsButton()
                .clickAddNewButton();
        addDefectPage.setTitle(DefectData.title)
                .clickElectroTechnicalButton();

        AddActionableItemPage addActionableItemPage = addDefectPage
                .setDescription(DefectData.description)
                .clickSaveButton()
                .clickOkButton()
                .clickNavigateBackButton()
                .getJobRelatedTable().getRows()
                .stream().filter(e -> e.getTitleText().contains(DefectData.title))
                .findFirst().get().clickFurtherDetailsArrowButton()
                .clickAddActionableItemButton();

        //AC: 3
        //Step: 6
        assert_().withFailureMessage("Required Approval CheckBox is not expected to be selected")
                .that(addActionableItemPage.isRequiredApprovalCheckBoxSelected())
                .isFalse();

        //AC: 8
        //Step: 7
        assert_().withFailureMessage("Title Helper Text is expected to be displayed")
                .that(addActionableItemPage.getTitleFieldHelperText())
                .isEqualTo(AiData.expectedTitleHelperText);
        assert_().withFailureMessage("Description Helper Text is expected to be displayed")
                .that(addActionableItemPage.getDescriptionFieldHelperText())
                .isEqualTo(AiData.expectedDescriptionSurveyorGuidanceHelperText);
        assert_().withFailureMessage("Surveyor Guidance Helper Text is expected to be displayed")
                .that(addActionableItemPage.getSurveyorGuidanceFieldHelperText())
                .isEqualTo(AiData.expectedDescriptionSurveyorGuidanceHelperText);

        //AC: 9
        //Step: 8
        Dimension beforeResize = addActionableItemPage.getDescriptionFieldDimension();
        addActionableItemPage.setDescriptionTextBoxSizeByOffset(0, 100);
        Dimension afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Description Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        beforeResize = addActionableItemPage.getSurveyorGuidanceFieldDimension();
        addActionableItemPage.setSurveyorGuidanceTextBoxSizeByOffset(0, 150);
        afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Surveyor Guidance Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        //AC: 3 ,AC: 4 and AC: 10
        //Step: 9 and 11
        addActionableItemPage.setTitle(AiData.title);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickClassCategoryButton();
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDescription(AiData.description);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(1)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDueDate(String.valueOf(FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickVisibleForAllButton();
        assert_().withFailureMessage("Mandatory fields are filled and save button is expected to be enabled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 7
        //Step: 10
        addActionableItemPage.setTitle("");
        assert_().withFailureMessage("Title Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isTitleTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();
        addActionableItemPage.setDescription("");
        assert_().withFailureMessage("Description Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isDescriptionTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();
        addActionableItemPage.setImposedDate("");
        assert_().withFailureMessage("Imposed Date Text Box Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isTrue();

        //AC: 5 and AC: 6
        //Step: 12 and 13
        addActionableItemPage.setTitle(AiData.title)
                .setDescription(AiData.description)
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(1)))))
                .setDueDate(String.valueOf(FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Imposed Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isFalse();
        assert_().withFailureMessage("Due Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isDueDateTextBoxErrorIconDisplayed(Type.ERROR))
                .isFalse();

        //AC: 13 and AC: 14
        //Step: 18
        addActionableItemPage.clickStartFromTemplateButton().clickXButton();

        //AC: 11 and 12
        //Step: 14
        // ActionableItemsPage actionableItemsPage
        DefectsPage defectsPage = addActionableItemPage.clickVisibleForAllButton()
                .clickSaveButton()
                .clickOkButton(ViewDefectPage.class)
                .clickNavigateBackButton();
        assert_().withFailureMessage("Defect status is expected to be 'Open' ")
                .that(defectsPage
                        .getJobRelatedTable()
                        .getRows()
                        .stream()
                        .filter(e -> e.getTitleText().contains(DefectData.title))
                        .findFirst()
                        .get()
                        .getItemStatus())
                .isEqualTo("Open");
        ActionableItemsPage actionableItemsPage = defectsPage.clickNavigateBackButton()
                .clickGoToActionableItemsButton();

        //AC: 4
        //Step: 17
        assert_().withFailureMessage("Added AI is expected to be displayed ")
                .that(actionableItemsPage.getJobRelatedTable()
                        .getRows().stream()
                        .filter(e -> e.getTitleText().contains(AiData.title))
                        .findFirst().isPresent())
                .isTrue();
    }

    @Test(description = "US10.22 - 9.1 - As part of Current job  - View the " +
            "notes & attachment(s) added on an Actionable Item")
    @Issue("LRT-2336")
    public void viewNoteAndAttachmentsOnAiTest()
    {
        //AC: 1
        //Step: 1,2,3 and 4
        AttachmentsAndNotesPage attachmentsAndNotesPage = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .getHeader()
                .clickAttachmentButton();

        //AC: 12
        //Step: 15
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() == 0)
        {
            assert_().withFailureMessage("No attachments or notes found message is expected to be displayed")
                    .that(attachmentsAndNotesPage.getAttachmentAndNotesMessage())
                    .isEqualTo("No attachments or notes found.");
        }

        //AC: 2, AC: 3  and AC: 4
        //Step: 5, 6 and 7
        attachmentsAndNotesPage.clickAddAttachmentButton();
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();

        //AC: 5 and AC: 8
        //Step: 8, 11 and 12
        addNotes(attachmentsAndNotesPage, noteSubPage);

        //AC: 6
        //Step: 9
        assert_().withFailureMessage("By Default the number of note displayed is 12")
                .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().size())
                .isEqualTo(12);
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() >= 12)
        {
            assert_().withFailureMessage("Load More button is expected to be displayed ")
                    .that(attachmentsAndNotesPage.isLoadMoreButtonPresent())
                    .isTrue();
        }

        //AC: 7
        //Step: 10
        List<java.util.Date> noteDate = attachmentsAndNotesPage
                .getNoteAndAttachmentCards()
                .stream()
                .map(NoteAndAttachmentElement::getDate)
                .collect(Collectors.toList());
        List<java.util.Date> sortedDueDate = noteDate.stream()
                .sorted(LloydsRegister.compareDate().reversed())
                .collect(Collectors.toList());
        assert_().withFailureMessage("All note date are expected to be presented in chronological order ")
                .that(noteDate)
                .isEqualTo(sortedDueDate);

        //AC: 11
        //Step: 14
        assert_().withFailureMessage("Delete Icon is expected to be displayed")
                .that(attachmentsAndNotesPage.getNoteAndAttachmentCards()
                        .get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class).isDeleteIconDisplayed())
                .isTrue();
    }

    @Test(description = "US10.22 - 9.2 - As part of Current job " +
            "- View more details about the note or attachment on an Actionable Item ")
    @Issue("LRT-2337")
    public void viewMoreDetailsOfNoteAndAttachmentsOnAiTest()
    {

        //Step: 1,2,3 and 4
        AttachmentsAndNotesPage attachmentsAndNotesPage = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .getHeader()
                .clickAttachmentButton();

        //AC: 1 and AC: 2
        //Step: 5 and 6
        attachmentsAndNotesPage.clickAddAttachmentButton();
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        addNotes(attachmentsAndNotesPage, noteSubPage);

        //AC: 4
        //Step: 8
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.getNoteAndAttachmentCards()
                .get(0).clickNoteOrAttachmentElement(NoteDetailsSubPage.class);
        assert_().withFailureMessage("Edit Icon is expected to be displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();

        assert_().withFailureMessage("Delete Icon is expected to be displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        assert_().withFailureMessage("Can only view more details of a single entity at a time")
                .that(attachmentsAndNotesPage
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(NoteAndAttachmentElement::isNoteSelected)
                        .collect(Collectors.toList())
                        .size())
                .isEqualTo(1);
    }

    @Test(description = "US10.22 - US 9.3 - As part of Current job - Add a note on and Actionable Item ")
    @Issue("LRT-2338")
    public void addingNoteOnAiTest()
    {
        //Step: 1,2 and 3
        AttachmentsAndNotesPage attachmentsAndNotesPage = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .getHeader()
                .clickAttachmentButton();

        //AC: 1 and AC: 2
        //Step: 4 and 5
        attachmentsAndNotesPage.clickAddAttachmentButton();
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton()
                .setNoteTextBox(noteText);
        assert_().withFailureMessage("Confidentiality level is expected to be Mandatory ")
                .that(noteSubPage.isAddNoteButtonEnabled())
                .isFalse();
        noteSubPage.setNoteTextBox("").clickAllButton();
        assert_().withFailureMessage("Description field is expected to be Mandatory ")
                .that(noteSubPage.isAddNoteButtonEnabled())
                .isFalse();

        //AC: 3 and AC: 10
        //Step: 6
        assert_().withFailureMessage("Character limit for description is expected to be 1000 ")
                .that(noteSubPage.getNoteTextBoxCharacterLimit())
                .isEqualTo(maximumCharLimit);

        //AC: 4
        //Step: 7
        assert_().withFailureMessage("Note Text Box Helper Text is expected to be displayed")
                .that(noteSubPage.getNoteTextBoxHelperText())
                .isEqualTo(noteHelperText);

        //AC: 5
        //Step: 8
        assert_().withFailureMessage("LR Internal is expected to be enabled")
                .that(noteSubPage.isLrInternalButtonEnabled())
                .isTrue();
        assert_().withFailureMessage("LR and Customer is expected to be enabled")
                .that(noteSubPage.isLrAndCustomerButtonEnabled()).isTrue();
        assert_().withFailureMessage("All button is expected to be enabled")
                .that(noteSubPage.isAllButtonEnabled())
                .isTrue();

        //AC: 6
        //Step: 9
        assert_().withFailureMessage("Current date is expected to be displayed ")
                .that(LloydsRegister.FRONTEND_TIME_FORMAT.format(noteSubPage.getNoteDate()))
                .isEqualTo(LloydsRegister.localDate.format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Creator name is expected to be displayed ")
                .that(noteSubPage.getCurrentUser())
                .isEqualTo(currentUser);

        //AC: 7
        //Step: 10
        //Covered by @Visible Annotation

        //AC: 8
        //AC: 11
        noteSubPage.setNoteTextBox(noteText).clickCancelButton();
        assert_().withFailureMessage("System is not expected to save Note")
                .that(attachmentsAndNotesPage
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getNoteText().equals(noteSubPage))
                        .findFirst()
                        .isPresent())
                .isFalse();
        assert_().withFailureMessage("Added Note is not expected to be saved")
                .that(attachmentsAndNotesPage.clickAddNoteButton().getNoteText())
                .isEmpty();

        //AC: 9
        //Step: 12
        assert_().withFailureMessage("Added note is expected to be present")
                .that(noteSubPage.setNoteTextBox(noteText)
                        .clickAllButton()
                        .clickAddNoteButton()
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getNoteText().equals(noteText))
                        .findFirst()
                        .isPresent())
                .isTrue();
    }

    @Test(description = "US10.22 - US 9.4 - As part of current Job - Edit an existing note on an Actionable Item ")
    @Issue("LRT-2339")
    public void editNoteOnAiTest()
    {

        //AC: 1
        //Step: 1, 2, 3, 4, 5 and 6
        NoteDetailsSubPage noteDetailsSubPage = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .getHeader()
                .clickAttachmentButton().clickAddNoteButton().setNoteTextBox(noteText)
                .clickAllButton().clickAddNoteButton().getNoteAndAttachmentCards()
                .stream().filter(e -> e.getNoteText().equals(noteText)).findFirst().get()
                .clickNoteOrAttachmentElement(NoteDetailsSubPage.class);

        //AC: 2
        //Step: 7
        EditNoteSubPage editNoteSubPage = noteDetailsSubPage.clickEditIcon();
        assert_().withFailureMessage("Note Text box is edit mode is expected to be Editable")
                .that(editNoteSubPage.isNoteTextBoxEnabled())
                .isTrue();

        //AC: 3
        //Step: 8
        assert_().withFailureMessage("Delete Icon is expected to be disabled ")
                .that(noteDetailsSubPage.isDeleteIconEnabled())
                .isTrue();

        //AC: 4
        //Step: 9
        assert_().withFailureMessage("Edit Note is text is expected to be displayed")
                .that(editNoteSubPage.getNoteTextBox())
                .isEqualTo(noteText);
        String editNote = noteText + "Edit";

        editNoteSubPage.setNoteTextBox(editNote)
                .clickSaveButton()
                .clickEditIcon();
        assert_().withFailureMessage("Edited Note is expected to be present")
                .that(editNoteSubPage.getNoteTextBox())
                .isEqualTo(editNote);

        //AC: 5
        //Step: 10
        editNoteSubPage.cutValueFromNoteTextBox();
        assert_().withFailureMessage("Hot Key Ctrl+x is expected to be allowed ")
                .that(editNoteSubPage.getNoteTextBox())
                .isEmpty();
        editNoteSubPage.pasteValueToNoteTextBox();
        assert_().withFailureMessage("Hot Key Ctrl+v is expected to be allowed ")
                .that(editNoteSubPage.getNoteTextBox())
                .isEqualTo(editNote);
        editNoteSubPage.copyValueFromNoteTextBox().pasteValueToNoteTextBox();
        assert_().withFailureMessage("Hot Key Ctrl+c is expected to be allowed")
                .that(editNoteSubPage.getNoteTextBox())
                .isEqualTo(editNote + editNote);

        //AC: 6
        //Step: 11
        editNoteSubPage.setNoteTextBox(specialChar)
                .cutValueFromNoteTextBox()
                .pasteValueToNoteTextBox();
        assert_().withFailureMessage("Special Characters are not allowed")
                .that(editNoteSubPage.isErrorIconDisplayed(ERROR))
                .isTrue();

        //AC: 7
        //Step: 12
        assert_().withFailureMessage("confidentiality selected by the user previously is expected to be displayed")
                .that(editNoteSubPage.getVisibleForText())
                .isEqualTo("All");

        //AC: 8
        //Step: 13
        assert_().withFailureMessage("Current date is expected to be displayed ")
                .that(LloydsRegister.FRONTEND_TIME_FORMAT.format(editNoteSubPage.getCreationDate()))
                .isEqualTo(LloydsRegister.localDate.format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Creator name is expected to be displayed")
                .that(editNoteSubPage.getCreatorName())
                .isEqualTo(currentUser);

        //AC: 9
        //Step: 14
        //Covered by @Visible Annotation

        //AC: 12
        //Step: 15
        editNoteSubPage.clickCancelButton();
        assert_().withFailureMessage("System should not save the changes made ")
                .that(noteDetailsSubPage.getNoteText().equals(noteText))
                .isFalse();

        //AC: 10 and AC: 11
        //Step: 16 and 17
        noteDetailsSubPage.clickEditIcon().setNoteTextBox(noteText).clickSaveButton();
        assert_().withFailureMessage("System should save the changes made ")
                .that(noteDetailsSubPage.getNoteText().equals(noteText))
                .isTrue();
    }

    @Test(description = "US10.22 - 9.5 AC:1-4 - On a Current Job - Delete an existing note on and Actionable Item ")
    @Issue("LRT-2340")
    public void deleteNoteOnAiTest()
    {
        //AC: 1
        //Step: 1, 2, 3 and 4
        NoteDetailsSubPage noteDetailsSubPage = navigateToJobAndCreateNewJob()
                .clickGoToActionableItemsButton()
                .getHeader()
                .clickAttachmentButton()
                .clickAddNoteButton()
                .setNoteTextBox(noteText)
                .clickAllButton()
                .clickAddNoteButton()
                .getNoteAndAttachmentCards()
                .stream()
                .filter(e -> e.getNoteText().equals(noteText))
                .findFirst()
                .get()
                .clickNoteOrAttachmentElement(NoteDetailsSubPage.class);

        //AC: 2 and 4
        //Step: 5,6 and 7
        assert_().withFailureMessage("Added note is expected to be present")
                .that(noteDetailsSubPage
                        .clickDeleteIcon()
                        .clickCancelButton()
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getNoteText().equals(noteText))
                        .findFirst()
                        .isPresent())
                .isTrue();

        //AC: 3
        //Step: 8 and 9
        assert_().withFailureMessage("Deleted note is not expected to be present")
                .that(noteDetailsSubPage
                        .clickDeleteIcon()
                        .clickConfirmationButton(AttachmentsAndNotesPage.class)
                        .getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getNoteText().equals(noteText))
                        .findFirst()
                        .isPresent())
                .isFalse();
    }

    private ViewJobDetailsPage navigateToJobAndCreateNewJob()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput(String.valueOf(assetId))
                .clickAssetSearchApplyButton();
        return workHubPage.clickAllAssetsTab()
                .getAssetCardByAssetName(asset.getDto().getName())
                .clickViewAssetButton()
                .clickJobsTab()
                .clickAddNewJobButton()
                .setSdoCode(sdoCode)
                .selectCaseTypeByVisibleText(caseType)
                .setJobLocation(location)
                .setJobDescription(description)
                .setEtaDate(now().format(FRONTEND_TIME_FORMAT_DTS))
                .setEtdDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setRequestedAttendanceDate(now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .clickSaveButton();
    }

    private List<ItemElement> scrollAndExpandAllChildElements(List<ItemElement> itemElements, int j)
    {
        for (int i = j; i < itemElements.size(); i++)
        {
            if (itemElements.get(i).isPlusIconDisplayed())
            {
                itemElements.get(i).clickPlusIcon();
                List<ItemElement> itemElements1 = itemElements.get(i).getChildren();
                scrollAndExpandAllChildElements(itemElements1, 0);
            }
        }
        return itemElements;
    }

    private AttachmentsAndNotesPage addNotes(AttachmentsAndNotesPage attachmentsAndNotesPage, NoteSubPage noteSubPage)
    {
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() < 12)
        {
            int count = attachmentsAndNotesPage.getNoteAndAttachmentCards().size();
            for (int i = 0; i < 14 - count; i++)
            {
                String randomNote = RandomStringUtils.randomAlphabetic(10);
                noteSubPage.setNoteTextBox(randomNote)
                        .clickAllButton()
                        .clickAddNoteButton();
                //First 12 note will be displayed in UI
                if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() < 13)
                {
                    assert_().withFailureMessage("Added Note is expected to be present")
                            .that(attachmentsAndNotesPage
                                    .getNoteAndAttachmentCards()
                                    .stream()
                                    .filter(e -> e.getNoteText().equals(randomNote))
                                    .findFirst()
                                    .isPresent())
                            .isTrue();
                    assert_().withFailureMessage("Creator is expected to be present")
                            .that(attachmentsAndNotesPage
                                    .getNoteAndAttachmentCards()
                                    .stream()
                                    .filter(e -> e.getCreatorName().equals(currentUser))
                                    .findFirst()
                                    .isPresent())
                            .isTrue();

                    assert_().withFailureMessage("Creator is expected to be present")
                            .that(attachmentsAndNotesPage
                                    .getNoteAndAttachmentCards()
                                    .stream()
                                    .filter(NoteAndAttachmentElement::isDateDisplayed)
                                    .findFirst()
                                    .isPresent())
                            .isTrue();
                }
                attachmentsAndNotesPage.clickAddNoteButton();
            }
        }
        return attachmentsAndNotesPage;
    }
}

