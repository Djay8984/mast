package test.viewasset;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetItem;
import model.asset.AssetItemAttributePage;
import model.asset.AssetModel;
import org.apache.commons.lang.WordUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.SearchWithinAssetModelPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetElement;
import workhub.sub.MyWorkSubPage;

import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static helper.TestDataHelper.*;

public class SearchingWithinAssetModelTest extends BaseTest
{

    private final String errorMessage = "No items found for your search criteria. Please search again.";

    @Test(description = "US8.21 AC:1-11 - User is able to view the search" +
            " results when searching within the asset model.")
    @Issue("LRT-1219")
    public void searchingWithinAssetModelTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        MyWorkSubPage myWorkSubPage = workHubPage.clickMyWorkTab();
        MyWorkAssetElement firstCard = myWorkSubPage.getAssetCardByIndex(0);
        String imoNumber = firstCard.getImoNumber();
        ViewAssetPage viewAssetPage = firstCard.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        ListViewSubPage listViewSubPage = assetModelSubPage
                .getListViewSubPage();

        List<ListViewItemElement> assetItems = listViewSubPage.getAssetItems();
        List<String> assetItemsOnFE = assetItems.stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());

        //Get the list of items from BE
        int assetId = getAssetIdByImoNumber(imoNumber);
        List<String> assetItemsOnBE = getItemNamesByAssetId(assetId);

        assert_().withFailureMessage("The asset item list view shows the " +
                "correct items ")
                .that(assetItemsOnFE)
                .isEqualTo(assetItemsOnBE);

        //Step: 3
        SearchWithinAssetModelPage searchWithinAssetModelPage =
                assetModelSubPage.clickSearchWithinAssetModelButton();

        //Step: 4
        //AC: 1, 3 and 4
        int i = 0;
        for (String anAssetItemsOnBE : assetItemsOnBE)
        {
            searchWithinAssetModelPage = searchWithinAssetModelPage.
                    setItemSearchInput(anAssetItemsOnBE).then()
                    .clickSearchButton();

            assert_().withFailureMessage("Item " + anAssetItemsOnBE + " is shown")
                    .that(searchWithinAssetModelPage.getSearchResultText())
                    .isEqualTo(Arrays.asList(anAssetItemsOnBE));

            assert_().withFailureMessage("Navigation option is Displayed")
                    .that(searchWithinAssetModelPage.
                            isNavigationOptionDisplayed(anAssetItemsOnBE))
                    .isTrue();

            assert_().withFailureMessage("Item " + anAssetItemsOnBE + " " +
                    "is shown after search is carried out")
                    .that(searchWithinAssetModelPage.getItemFieldText())
                    .isEqualTo(anAssetItemsOnBE);

            searchWithinAssetModelPage.clearItemSearchInput();

            assert_().withFailureMessage("Item " + anAssetItemsOnBE + " " +
                    "is shown after search is clear")
                    .that(searchWithinAssetModelPage.getSearchResultText())
                    .isEqualTo(Arrays.asList(anAssetItemsOnBE));
            i++;
            if (i == 5)
            {
                break;
            }
        }

        //Get Attribute Type ID from BE
        AssetModel assetModel = new AssetModel(assetId);
        AssetItemAttributePage assetItemAttributePage =
                new AssetItemAttributePage(assetId, assetModel.getDto()
                        .getItems().get(0).getId().intValue());

        List<Long> attributeTypeID = new ArrayList<>();
        for (i = 0; i < assetItemAttributePage.getDto().getContent().size(); i++)
        {
            attributeTypeID.add(assetItemAttributePage.getDto()
                    .getContent().get(i).getAttributeType().getId());
        }

        //Get only distinct attribute_type_id
        Set<Long> distinctAttributeId = new HashSet<>(attributeTypeID);
        List<Long> list = new ArrayList<>(distinctAttributeId);

        ArrayList<String> attributeTypeNames = new ArrayList<>();
        for (Long aList : list)
        {
            attributeTypeNames
                    .add(getAttributeTypeNameById(aList.intValue()));
        }

        AssetItem assetItem = new AssetItem(assetId
                , assetModel.getDto().getItems().get(0).getId().intValue());
        String itemName = assetItem.getDto().getName();

        //Step: 5
        //AC: 1, 3 and 4
        for (String attributeTypeName1 : attributeTypeNames)
        {
            searchWithinAssetModelPage = searchWithinAssetModelPage.
                    setAttributeSearchInput(attributeTypeName1).then()
                    .clickSearchButton();

            assert_().withFailureMessage("Attribute Type Name " +
                    attributeTypeName1 + " is shown")
                    .that(searchWithinAssetModelPage.getSearchResultText()
                            .stream()
                            .filter(e -> e.equals(attributeTypeName1))
                            .findFirst()
                            .isPresent())
                    .isTrue();

            assert_().withFailureMessage("Navigation option is Displayed")
                    .that(searchWithinAssetModelPage
                            .isNavigationOptionDisplayed(attributeTypeName1))
                    .isTrue();

            searchWithinAssetModelPage.clearItemSearchInput();
            assert_().withFailureMessage("Attribute Type Name " +
                    attributeTypeName1 + " is shown after search is clear")
                    .that(searchWithinAssetModelPage.getAttributeTypeNameText())
                    .isEqualTo(attributeTypeName1);
        }

        //Get Attribute Value from BE
        List<AttributeDto> attributeDto = assetItemAttributePage.getDto()
                .getContent()
                .stream()
                .filter(dto -> dto.getValue() != null)
                .collect(Collectors.toList());
        List<String> attributeValue = attributeDto.stream()
                .map(AttributeDto::getValue)
                .collect(Collectors.toList());

        //Step 6 and 7
        //AC: 1, 3 4 and 8
        for (String anAttributeValue : attributeValue)
        {
            searchWithinAssetModelPage
                    .setAttributeValueSearchInput(anAttributeValue)
                    .then().clickSearchButton();
            assert_().withFailureMessage("Attribute Value "
                    + anAttributeValue + " is shown")
                    .that(searchWithinAssetModelPage.getSearchResultText()
                            .stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst()
                            .isPresent())
                    .isTrue();

            assert_().withFailureMessage("Navigation option is Displayed")
                    .that(searchWithinAssetModelPage
                            .isNavigationOptionDisplayed(itemName))
                    .isTrue();
            searchWithinAssetModelPage.clearAttributeValueSearchInput();
            assert_().withFailureMessage("Attribute Value "
                    + anAttributeValue + " is shown after search is clear")
                    .that(searchWithinAssetModelPage.getSearchResultText()
                            .stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst()
                            .isPresent())
                    .isTrue();
        }

        //Step 8 and 9 and 10
        //AC: 2, 3, 4, 5 and 6
        //TODO:Assertion needs to be modified once fix is provided for bug LDR-2166
        for (String attributeTypeName : attributeTypeNames)
        {

            searchWithinAssetModelPage.setItemSearchInput(itemName)
                    .then().setAttributeSearchInput(attributeTypeName)
                    .then().clickSearchButton();
            assert_().withFailureMessage("Search criteria returns matching attributes")
                    .that(searchWithinAssetModelPage.getSearchResultText()
                            .stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst().isPresent())
                    .isTrue();

            assert_().withFailureMessage("Navigation option is Displayed")
                    .that(searchWithinAssetModelPage
                            .isNavigationOptionDisplayed(itemName))
                    .isTrue();

            searchWithinAssetModelPage.clearItemSearchInput()
                    .then().clearAttributeSearchInput();
            assert_().withFailureMessage("Search results appear after clearing the search items")
                    .that(searchWithinAssetModelPage.getSearchResultText()
                            .stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst()
                            .isPresent())
                    .isTrue();
        }

        //Step: 11
        //AC: 7
        searchWithinAssetModelPage
                .setItemSearchInput("This is to test error Message")
                .then().clickSearchButton();
        assert_().withFailureMessage("Error Message is displayed")
                .that(searchWithinAssetModelPage.getErrorMessageText())
                .isEqualTo(errorMessage);

        //Step: 12
        //AC: 10
        searchWithinAssetModelPage.setItemSearchInput("*")
                .then().setAttributeSearchInput("*")
                .then().setAttributeValueSearchInput("*");

        assert_().withFailureMessage("Search Button is not clickable")
                .that(searchWithinAssetModelPage.isSearchButtonClickable())
                .isTrue();

        //Step: 13
        //AC: 9
        for (i = 0; i < attributeValue.size(); i++)
        {

            searchWithinAssetModelPage
                    .setAttributeValueSearchInput
                            (WordUtils.capitalize(attributeValue.get(i)))
                    .then().clickSearchButton();
            assert_().withFailureMessage("\"Item " + assetItemsOnBE.get(i) + " is shown")
                    .that(searchWithinAssetModelPage
                            .getSearchResultText().stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst().isPresent())
                    .isTrue();
            searchWithinAssetModelPage.clearAttributeValueSearchInput();
            assert_().withFailureMessage("\"Item " + assetItemsOnBE.get(i) + "" +
                    " is shown after search is clear")
                    .that(searchWithinAssetModelPage
                            .getSearchResultText().stream()
                            .filter(e -> e.equals(itemName))
                            .findFirst().isPresent())
                    .isTrue();
        }
        //Step: 14
        //AC: 11
        //TODO:Search criteria need to changed once fix is provided for bug LDR-2166

        searchWithinAssetModelPage.
                setAttributeSearchInput(attributeTypeNames.get(0))
                .then().clickSearchButton();

        assert_().withFailureMessage("Search Results Displayed")
                .that(searchWithinAssetModelPage.getResultItemCount())
                .isEqualTo(searchWithinAssetModelPage.getShownAmount());

        if (searchWithinAssetModelPage.getTotalAmount() > 20)
        {
            searchWithinAssetModelPage.clickLoadMoreButton();
            assert_().withFailureMessage("Search Results Displayed")
                    .that(searchWithinAssetModelPage.getResultItemCount())
                    .isEqualTo(searchWithinAssetModelPage.getShownAmount());
        }

    }

}
