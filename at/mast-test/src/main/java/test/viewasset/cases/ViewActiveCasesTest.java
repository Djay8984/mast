package test.viewasset.cases;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.cases.CaseQuery;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.CasesPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class ViewActiveCasesTest extends BaseTest
{

    @Test(description = "Automation Test: Story 11.1 - As a user I want to view the active cases against an asset" +
            " so that I can take the appropriate actions against them")
    @Issue("LRT-2117")
    public void viewActiveCasesTest()
    {

        WorkHubPage workHubPage = WorkHubPage.open();

        //AC1,2
        AssetLightDto assetLightDto = TestDataHelper.getAssetsForDefaultEmployee().getDto().getContent().get(0);
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetLightDto.getName())
                .clickAssetSearchApplyButton();
        CasesPage viewCase = workHubPage.clickAllAssetsTab()
                .getAssetCardByAssetName(assetLightDto.getName())
                .clickCasesButton();

        assert_().withFailureMessage("The user should have been taken to the view case page")
                .that(viewCase.isAddCaseButtonPresent()).isTrue();

        workHubPage = workHubPage.getHeader().clickMastLogo();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetLightDto.getName())
                .clickAssetSearchApplyButton();
        viewCase = workHubPage.clickAllAssetsTab()
                .getAssetCardByAssetName(assetLightDto.getName())
                .clickCasesButton();

        assert_().withFailureMessage("The user should have been taken to the view case page")
                .that(viewCase.isAddCaseButtonPresent()).isTrue();

        //AC3
        List<Long> assetQuery = new ArrayList<>();
        assetQuery.add(assetLightDto.getId());
        CaseQueryDto caseQueryDto = new CaseQueryDto();
        caseQueryDto.setAssetId(assetQuery);
        List<CaseDto> cases = new CaseQuery(caseQueryDto).getDto().getContent();

        if (cases.size() > 20)
        {
            assert_().withFailureMessage("You should see 20 cases shown")
                    .that(viewCase.amountOfCases()).isEqualTo("20");
        }

        for (int i = 0; i < cases.size(); i++)
        {
            assert_().withFailureMessage("The expected and actual case ID do not match")
                    .that("C-000000" + cases.get(i).getId()).isEqualTo(viewCase.getCaseByIndex(i).getCaseId());
        }

        //AC5
        assert_().withFailureMessage("The user should be able to add a new case")
                .that(viewCase.isAddCaseButtonPresent()).isTrue();

        //AC4
        workHubPage = workHubPage.getHeader().clickMastLogo();
        AllAssetsAssetElement adonia = workHubPage.clickAllAssetsTab().getAssetCardByAssetName("Adonia");

        assert_().withFailureMessage("The asset Adonia should not have any active cases to it")
                .that(adonia.getCaseCountText()).isEqualTo("0\nActive cases");

        viewCase = adonia.clickCasesButton();
        assert_().withFailureMessage("No no active cases message is not shown")
                .that(viewCase.isAddCaseButtonPresent()).isTrue();

        //AC5
        assert_().withFailureMessage("The user should be able to add a new case")
                .that(viewCase.isAddCaseButtonPresent()).isTrue();
    }
}
