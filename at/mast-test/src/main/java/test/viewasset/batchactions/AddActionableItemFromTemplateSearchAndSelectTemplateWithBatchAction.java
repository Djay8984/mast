package test.viewasset.batchactions;

import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.BatchActionData;
import helper.TestDataHelper;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.batchaction.BatchActionsPage;
import workhub.batchaction.define.DefineSubPage;
import workhub.batchaction.define.template.ItemTemplatePage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static constant.LloydsRegister.localDate;
import static model.referencedata.ReferenceDataMap.RefData.CODICIL_TEMPLATES;
import static model.referencedata.ReferenceDataMap.RefData.ITEM_TYPES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class AddActionableItemFromTemplateSearchAndSelectTemplateWithBatchAction extends BaseTest
{

    @Test(description = "Story 7.7 - As a user who has selected to add Actionable Item from a " +
            "template, I want to be able to search and select template so that I can continue " +
            "with the batch action process. "
    )
    @Issue("LRT-982")
    public void addActionableItemFromTemplateSearchAndSelectTemplateWithBatchAction()
    {

        final String searchMessage = "No templates found for your search criteria. Please" +
                " search again";

        WorkHubPage workHubPage = WorkHubPage.open();
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        //AC1
        DefineSubPage defineSubPage = batchActionsPage.getDefineSubPage()
                .clickActionableItemRadioButton();
        ItemTemplatePage itemTemplatePage = defineSubPage.setTitleTextInput(BatchActionData.title)
                .setDescriptionTextInput(BatchActionData.description)
                .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(localDate.plusDays(2).format(FRONTEND_TIME_FORMAT_DTS))
                .clickStatutoryButton()
                .clickLrInternalButton()
                .clickStartFromATemplateButton();

        assert_().withFailureMessage("Title text is expected to be empty")
                .that(itemTemplatePage.getTitleText())
                .isEqualTo("");
        assert_().withFailureMessage("Item type is expected to be empty")
                .that(itemTemplatePage.getItemTypeText())
                .isEqualTo("");

        //AC10
        defineSubPage = itemTemplatePage.clickCloseButton();
        assert_().withFailureMessage("Title text is expected to be: "
                + BatchActionData.title)
                .that(defineSubPage.getTitleTextInput())
                .isEqualTo(BatchActionData.title);
        assert_().withFailureMessage("Description text is expected to be: "
                + BatchActionData.description)
                .that(defineSubPage.getDescriptionTextInput())
                .isEqualTo(BatchActionData.description);
        assert_().withFailureMessage("Statutory button is expected to be selected")
                .that(defineSubPage.isStatutoryButtonSelected())
                .isTrue();
        assert_().withFailureMessage("LR Internal Button is expected to be selected")
                .that(defineSubPage.isLrInternalButtonSelected())
                .isTrue();
        assert_().withFailureMessage("Imposed date is expected to be "
                + localDate.now().toString())
                .that(defineSubPage.getImposedDate())
                .isEqualTo(localDate.now().plusDays(1).format
                        (FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Due date is expected to be "
                + localDate.now().plusDays(1).toString())
                .that(defineSubPage.getDueDate())
                .isEqualTo(localDate.now().plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));

        //AC7
        itemTemplatePage = defineSubPage.clickStartFromATemplateButton();
        assert_().withFailureMessage("Live button is expected to be selected by default ")
                .that(itemTemplatePage.isLiveButtonSelected())
                .isTrue();
        itemTemplatePage.clickAllButton();
        assert_().withFailureMessage("All button is expected to be selected")
                .that(itemTemplatePage.isAllButtonSelected())
                .isTrue();
        itemTemplatePage.clickLiveButton();
        assert_().withFailureMessage("Live button is expected to be selected")
                .that(itemTemplatePage.isLiveButtonSelected())
                .isTrue();

        //AC9 AC7
        ItemTemplatePage.TemplateSearchResultTable table = itemTemplatePage
                .clickFindTemplateButton();
        List<ItemTemplatePage.DataRow> rows = table.getDataRows();
        //check that only live status are available
        Map<String, String> byName = rows.stream()
                .collect(Collectors.toMap(ItemTemplatePage.DataRow::getName, ItemTemplatePage
                        .DataRow::getStatus));
        for (Map.Entry<String, String> entry : byName.entrySet())
        {
            assert_().withFailureMessage(entry.getKey()
                    + " status is expected to be Live")
                    .that(entry.getValue())
                    .doesNotContain("All");
        }
        //
        itemTemplatePage.clickAllButton()
                .clickFindTemplateButton();
        table = itemTemplatePage
                .clickFindTemplateButton();
        rows = table.getDataRows();
        //check that live and all status are available
        byName = rows.stream()
                .collect(Collectors.toMap(ItemTemplatePage.DataRow::getName, ItemTemplatePage
                        .DataRow::getStatus));
        for (Map.Entry<String, String> entry : byName.entrySet())
        {
            assert_().withFailureMessage(entry.getKey()
                    + " status is expected to be Live or All")
                    .that(entry.getValue())
                    .isAnyOf("Live", "All");
        }

        //AC2 - valid title
        ArrayList<CodicilTemplateDto> codicilTemplates =
                new ReferenceDataMap(ASSET, CODICIL_TEMPLATES).getReferenceDataAsClass
                        (CodicilTemplateDto.class);
        //todo remove filter getId()!=3 - if LRD - 3439 is not a bug
        final String cocTitle = codicilTemplates.stream()
                .filter(ct -> ct.getCodicilType().getId() != 3)
                .findFirst()
                .get()
                .getTitle();
        final String cocTitleTwo = codicilTemplates.stream()
                .filter(ct -> ct.getCodicilType().getId() != 3)
                .filter(ct -> !ct.getTitle().equals(cocTitle))
                .findFirst()
                .get()
                .getTitle();
        table = itemTemplatePage.setTitleText(cocTitle)
                .clickFindTemplateButton();
        assert_().withFailureMessage("Search results is expected to be "
                + cocTitle)
                .that(table.getDataRows().get(0).getName())
                .isEqualTo(cocTitle);

        //AC2 - invalid title
        table = itemTemplatePage.setTitleText(cocTitle + TestDataHelper.getRandomNumber(0, 999_999))
                .clickFindTemplateButton();
        assert_().withFailureMessage(searchMessage
                + "  is expected to be displayed")
                .that(itemTemplatePage.getSearchMessage())
                .isEqualTo(searchMessage);

        //AC6 - wildcard before
        table = itemTemplatePage.setTitleText("*" + cocTitleTwo.substring(5, cocTitleTwo.length()))
                .clickFindTemplateButton();
        assert_().withFailureMessage("Search results is expected to be "
                + cocTitleTwo)
                .that(table.getDataRows().get(0).getName())
                .isEqualTo(cocTitleTwo);

        //AC6 - wildcard after
        //get next item, so that table is refreshed
        table = itemTemplatePage.setTitleText(cocTitle.substring(0, 5) + "*")
                .clickFindTemplateButton();
        assert_().withFailureMessage("Search results is expected to be "
                + cocTitle)
                .that(table.getDataRows().get(0).getName())
                .isEqualTo(cocTitle);

        //AC6 - wildcard before and after
        //get the first, so that table is refreshed
        table = itemTemplatePage.setTitleText("*" + cocTitleTwo.substring(1, 10) + "*")
                .clickFindTemplateButton();
        assert_().withFailureMessage("Search results is expected to be "
                + cocTitleTwo)
                .that(table.getDataRows().get(0).getName())
                .isEqualTo(cocTitleTwo);

        //AC3,AC4,AC5
        //for cocidilTitle get the typeId
        CodicilTemplateDto codicilTemplateDto = codicilTemplates.stream()
                .filter(ct -> ct.getCodicilType().getId() != 3)
                .findFirst()
                .orElse(null);

        //Reference data doesn't have any item types for codicil templates currently.
        if (codicilTemplateDto.getItemType() == null)
            return;

        Long id = codicilTemplateDto.getItemType().getId();

        ArrayList<ItemTypeDto> itemTypeDtos =
                new ReferenceDataMap(ASSET, ITEM_TYPES).getReferenceDataAsClass
                        (ItemTypeDto.class);
        String itemType = itemTypeDtos.stream()
                .filter(i -> i.getId().equals(id))
                .findFirst()
                .get()
                .getName();
        itemTemplatePage.setTitleText("")
                .setItemTypeText("asdfasdfasdfasdfasdfsdafs");
        assert_().withFailureMessage("Item type dropdown with lookahead is expected to be " +
                "displayed")
                .that(itemTemplatePage.getItemTypeDropDownTextByIndex(0))
                .isEqualTo("No matches found");
        itemTemplatePage.setTitleText("")
                .setItemTypeText(itemType.substring(0, 5));
        assert_().withFailureMessage("Item type dropdown with lookahead is expected to be " +
                "displayed")
                .that(itemTemplatePage.getItemTypeDropDownOptions().size())
                .isNotSameAs(0);

        //AC8
        table = itemTemplatePage.selectItemTypeDropdownByIndex(0)
                .setTitleText(cocTitle)
                .setItemTypeText(itemType)
                .clickFindTemplateButton();
        assert_().withFailureMessage("Search results is expected to display "
                + cocTitle)
                .that(table.getDataRows().get(0).getName())
                .isEqualTo(cocTitle);

    }
}
