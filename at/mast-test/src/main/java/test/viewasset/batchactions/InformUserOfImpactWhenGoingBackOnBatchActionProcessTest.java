package test.viewasset.batchactions;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.BatchActionData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import workhub.WorkHubPage;
import workhub.batchaction.BatchActionsPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.DefineSubPage;
import workhub.batchaction.element.Asset;
import workhub.batchaction.modal.ConfirmationPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static constant.LloydsRegister.localDate;

public class InformUserOfImpactWhenGoingBackOnBatchActionProcessTest extends BaseTest
{

    private final String warningMessage = "Going back to the previous step means that you will " +
            "lose all\n" +
            "your currently selected assets in Step 2: Apply.";

    @DataProvider(name = "itemType")
    public Object[][] itemType()
    {
        return new Object[][]{
                {"Asset Note"},
                {"Actionable Item"}
        };
    }

    @Test(description = "Story 7.16 - As a user who is on the second step of the batch action " +
            "process and has selected to go back to the first step of the process I want to be told" +
            " of the impact so that I can make an informed choice\n" +
            "AC:1-4 - User on the second step of the batch action process can select to go back to the first step.",
            dataProvider = "itemType"
    )
    @Issues({@Issue("LRT-982"),
             @Issue("LRT-1094")
    })
    public void informUserOfImpactWhenGoingBackOnBatchActionProcessTest(String toTest)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        if (toTest.equals("Asset Note"))
        {
            batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        }
        else
        {
            batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                    .setDueDate(localDate.plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        }
        batchActionsPage.getDefineSubPage()
                .setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .clickLrInternalButton()
                .clickNextButton()
                //AC1 AC2
                .getAssetSection()
                .getCards().get(0)
                .clickAddAssetButton();

        ConfirmationPage confirmationPage = batchActionsPage.clickDefineTab(ConfirmationPage.class);

        assert_().withFailureMessage("Warning message is expected to appear")
                .that(confirmationPage.getText())
                .isEqualTo(warningMessage);

        DefineSubPage defineSubPage = confirmationPage.clickContinueButton();

        //AC3 AC4
        // Previous information was preserved in DefineSubPage
        assert_().withFailureMessage("Title input is expected to be preserved")
                .that(defineSubPage.getTitleTextInput())
                .isEqualTo(BatchActionData.title);
        assert_().withFailureMessage("Description input is expected to be preserved")
                .that(defineSubPage.getDescriptionTextInput())
                .isEqualTo(BatchActionData.description);
        assert_().withFailureMessage("Statutory button is expected to be selected")
                .that(defineSubPage.isStatutoryButtonSelected())
                .isTrue();
        assert_().withFailureMessage("Imposed date is expected to be preserved")
                .that(defineSubPage.getImposedDate())
                .isEqualTo(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("LR Internal Button is expected to be selected")
                .that(defineSubPage.isLrInternalButtonSelected())
                .isTrue();
        if (toTest.equals("Actionable Item"))
            assert_().withFailureMessage("Due date is expected to be preserved")
                    .that(defineSubPage.getDueDate())
                    .isEqualTo(localDate.plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));

        //AC3 - all previous data will reset to default in ApplySubPage
        ApplySubPage applySubPage = batchActionsPage.clickApplyTab();
        Asset asset = applySubPage.getAssetSection()
                .getCards()
                .get(0);
        assert_().withFailureMessage("The previously selected button is expected to be unselected")
                .that(asset.getAddButtonText())
                .isEqualTo("Add");
    }

    @Test(description = "Story 7.16 - As a user who is on the second step of the batch action " +
            "process and has selected to go back to the first step of the process I want to be told" +
            " of the impact so that I can make an informed choice\n" +
            "AC:5 - Cancelling the warning message on batch action. ",
            dataProvider = "itemType"
    )
    @Issues({@Issue("LRT-810"),
             @Issue("LRT-1095")
    })
    public void cancellingTheWarningMessageOnBatchActionTest(String toTest)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        if (toTest.equals("Asset Note"))
        {
            batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        }
        else
        {
            batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                    .setDueDate(localDate.plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        }

        ApplySubPage applySubPage = batchActionsPage.getDefineSubPage()
                .setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .clickLrInternalButton()
                .clickNextButton();
        Asset asset = applySubPage.getAssetSection()
                .getCards().get(0)
                .clickAddAssetButton();
        //AC5 - all information will be retained
        batchActionsPage.clickDefineTab(ConfirmationPage.class)
                .clickCancelButton();
        assert_().withFailureMessage("The previously selected button is expected to be selected")
                .that(asset.getAddButtonText())
                .isEqualTo("Added to selection");
    }

    @Test(description = "Story 7.16 - As a user who is on the second step of the batch action " +
            "process and has selected to go back to the first step of the process I want to be told" +
            " of the impact so that I can make an informed choice\n" +
            "US7.16 AC:6 - Unable to go back to Define step after Apply step has been completed. ",
            dataProvider = "itemType"
    )
    @Issues({@Issue("LRT-810"),
             @Issue("LRT-1096")
    })
    public void unableToGoBackToDefineStepAfterApplyStepHasBeenCompleted(String toTest)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        if (toTest.equals("Asset Note"))
        {
            batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        }
        else
        {
            batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton()
                    .setImposedDate(localDate.plusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                    .setDueDate(localDate.plusDays(2).format(FRONTEND_TIME_FORMAT_DTS));
        }

        ApplySubPage applySubPage = batchActionsPage.getDefineSubPage()
                .setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .clickLrInternalButton()
                .clickNextButton();
        Asset asset = applySubPage.getAssetSection()
                .getCards().get(0)
                .clickAddAssetButton();
        //AC6
        workHubPage = applySubPage.clickCompleteButton().clickOKButton();
        //WTF - the page waits before going back to workhubpage
        //no asserts needed, rely on workHubPage @Visible
    }
}
