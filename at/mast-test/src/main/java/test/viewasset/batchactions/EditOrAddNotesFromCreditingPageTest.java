package test.viewasset.batchactions;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.JobData;
import constant.LloydsRegister.JobTeamData;
import model.asset.Asset;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.crediting.element.ServiceElement;
import viewasset.sub.jobs.details.crediting.modal.ConfirmEditChangesPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.JobScopeServiceTaskElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.JobScopeServiceTypeElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub.JobScopeHierarchyNodeElements;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class EditOrAddNotesFromCreditingPageTest extends BaseTest
{

    final int assetId = 501;
    final String jobScopeServiceType = "Machinery";
    final String productCatalogueName = "Classification";

    @Test(description = "Story 11.22 - As a user on the crediting page I want to" +
            " select to save the progress and/or add notes so that I can" +
            " ensure that all the changes I have made so far are saved")
    @Issue("LRT-2665")
    public void editOrSaveChangesInCreditingPageTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 3 and 4
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AddNewJobPage addNewJobSubPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobSubPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate)
                .then().clickSaveButton();

        AddOrEditJobTeamPage AddOrEditJobTeamPage
                = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        AddOrEditJobTeamPage.setLeadSurveyorTextBox(JobTeamData.leadSurveyor)
                .then().clickSaveButton();

        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

        JobScopeServiceTaskElements JobScopeServiceTaskElement
                = serviceAllPage.getJobScopeServiceTaskByName(productCatalogueName);
        if (JobScopeServiceTaskElement.isProductCatalogueExpanded())
        {
            JobScopeServiceTaskElement.clickOnExpandIcon();
        }

        JobScopeServiceTypeElements jobScopeServiceTypeElement
                = JobScopeServiceTaskElement.getJobScopeServiceTypeByName(jobScopeServiceType);
        if (jobScopeServiceTypeElement.isJobScopeServiceTypeExpanded())
        {
            jobScopeServiceTypeElement.clickOnExpandIcon();
        }

        List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements
                = jobScopeServiceTypeElement.getAllJobScopeHierarchyNodeElements();
        String serviceTypeText = jobScopeHierarchyNodeElements.get(0).getServiceTypeText();
        jobScopeHierarchyNodeElements.get(0).selectCheckBox();
        JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

        viewJobDetailsPage = jobScopePage.clickConfirmJobScopeButton()
                .then().clickConfirmScopeButton();

        //AC: 1 and AC 2
        //Step: 5
        CreditingPage creditingPage
                = viewJobDetailsPage.clickGoToCreditingButton();
        List<ServiceElement> serviceElements
                = creditingPage.getClassificationTable().getRows();
        ServiceElement creditingServiceElement
                = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();

        creditingServiceElement.clickPartHeldRadioButton();

        // /AC: 4
        //Step: 6 and 7
        ConfirmEditChangesPage confirmEditChangesPage
                = creditingPage.clickNavigateBackButton(ConfirmEditChangesPage.class);
        assert_().withFailureMessage("Leave Page Modal window is expected to be present")
                .that(confirmEditChangesPage.isLeavePageModelWindowPresent())
                .isTrue();

        //AC: 7
        //Step: 8
        confirmEditChangesPage.clickCancelButton();
        serviceElements = creditingPage.getClassificationTable().getRows();
        creditingServiceElement = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();

        //TODO- Need to hook on to specific css once implemented
        assert_().withFailureMessage("Changes made is expected to remain unchanged")
                .that(creditingServiceElement.isPartHeldRadioButtonSelected())
                .isTrue();

        //AC: 4 and AC 6
        //Step: 9 and 10
        creditingServiceElement.clickCompleteRadioButton();
        creditingPage.clickNavigateBackButton(ConfirmEditChangesPage.class);
        confirmEditChangesPage.clickDiscardChangesButton();
        viewJobDetailsPage.clickGoToCreditingButton();
        serviceElements = creditingPage.getClassificationTable().getRows();
        creditingServiceElement = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();
        //TODO- Need to hook on to specific css once implemented
        assert_().withFailureMessage("Changes made is not expected to be saved")
                .that(creditingServiceElement.isCompleteRadioButtonSelected())
                .isFalse();

        //AC: 5
        //Step: 11, 12 and 13
        serviceElements = creditingPage.getClassificationTable().getRows();
        creditingServiceElement = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();
        creditingServiceElement.clickCompleteRadioButton();
        creditingPage.clickNavigateBackButton(ConfirmEditChangesPage.class);
        confirmEditChangesPage.clickSaveChangesButton();
        viewJobDetailsPage.clickGoToCreditingButton();
        serviceElements = creditingPage.getClassificationTable().getRows();
        creditingServiceElement = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();
        //TODO- Need to hook on to specific css once implemented
        assert_().withFailureMessage("Changes made is expected to be saved")
                .that(creditingServiceElement.isCompleteRadioButtonSelected())
                .isTrue();

        //AC: 3
        //Step: 14
        serviceElements = creditingPage.getClassificationTable().getRows();
        creditingServiceElement = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();
        creditingServiceElement.clickNotStartRadioButton();
        creditingPage.clickSaveButton();
        //TODO- Need to hook on to specific css once implemented
        assert_().withFailureMessage("Changes made is expected to be saved")
                .that(creditingServiceElement.isNotStartedRadioButtonSelected())
                .isTrue();
    }

    @Test(description = "Story 11.22 - (NOTES AND ATTACHMENTS) - As a user on " +
            "the crediting page I want to select to save the progress and/" +
            "or add notes so that I can ensure that all the changes I have " +
            "made so far are saved")
    @Issue("LRT-2667")
    public void editOrSaveNotesAndAttachmentsTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 3 and 4
        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.setSdoCode(JobData.sdoCode)
                .then().selectCaseTypeByVisibleText(JobData.caseType)
                .then().setJobLocation(JobData.location)
                .then().setJobDescription(JobData.description)
                .then().setEtaDate(JobData.strEtaDate)
                .then().setEtdDate(JobData.strEtdDate)
                .then().setRequestedAttendanceDate(JobData.strEtdDate)
                .then().clickSaveButton();

        AddOrEditJobTeamPage addOrEditJobTeamPage
                = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        addOrEditJobTeamPage.setLeadSurveyorTextBox(JobTeamData.leadSurveyor)
                .then().clickSaveButton();

        AddToJobScopePage addToJobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
        ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

        JobScopeServiceTaskElements JobScopeServiceTaskElement
                = serviceAllPage.getJobScopeServiceTaskByName(productCatalogueName);
        if (JobScopeServiceTaskElement.isProductCatalogueExpanded())
        {
            JobScopeServiceTaskElement.clickOnExpandIcon();
        }

        JobScopeServiceTypeElements jobScopeServiceTypeElement
                = JobScopeServiceTaskElement.getJobScopeServiceTypeByName(jobScopeServiceType);
        if (jobScopeServiceTypeElement.isJobScopeServiceTypeExpanded())
        {
            jobScopeServiceTypeElement.clickOnExpandIcon();
        }

        List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements
                = jobScopeServiceTypeElement.getAllJobScopeHierarchyNodeElements();
        String serviceTypeText = jobScopeHierarchyNodeElements.get(0).getServiceTypeText();
        jobScopeHierarchyNodeElements.get(0).selectCheckBox();
        JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

        viewJobDetailsPage = jobScopePage.clickConfirmJobScopeButton()
                .then().clickConfirmScopeButton();

        //AC: 8
        //Step: 5 and 6
        CreditingPage creditingPage
                = viewJobDetailsPage.clickGoToCreditingButton();

        List<ServiceElement> serviceElements
                = creditingPage.getClassificationTable().getRows();
        ServiceElement creditingServiceElement
                = serviceElements.stream()
                .filter(e -> e.getServiceType().equals(serviceTypeText))
                .findFirst().get();

        AttachmentsAndNotesPage attachmentsAndNotesPage
                = creditingServiceElement.clickAttachmentIcon();

        //Step: 7 and 9
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() == 0)
        {
            String randomNote = "Auto Test KL" + RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().clickAllButton()
                    .then().clickAddNoteButton();
            assert_().withFailureMessage("Added Note is expected to Present")
                    .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(randomNote))
                    .isTrue();
        }

        String noteText = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText();
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        assert_().withFailureMessage("Edit Note Icon is expected Displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is expected Displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //Step: 10
        noteText = "Auto Test Edit Note KL";
        noteDetailsSubPage.clickEditIcon().then().setNoteTextBox(noteText)
                .then().clickSaveButton();
        assert_().withFailureMessage("Edited Note is expected to be present")
                .that(attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText().equals(noteText)).isTrue();

        //Step: 11
        if (!noteDetailsSubPage.isDeleteIconDisplayed())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        }
        noteDetailsSubPage.clickDeleteIcon().then()
                .clickConfirmationButton(AttachmentsAndNotesPage.class);
        assert_().withFailureMessage("Note is expected to be deleted")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();
    }

}
