package test.viewasset.batchactions;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import model.asset.Asset;
import model.surveyor.Surveyor;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.conditionofclass.AddConditionOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.crediting.element.InfoElement;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.JobScopeServiceTaskElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.JobScopeServiceTypeElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub.JobScopeHierarchyNodeElements;
import viewasset.sub.jobs.details.scope.element.JobScopeElements;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class ViewCocInJobCreditingSectionTest extends BaseTest
{
    @Test(description = "US 11.21: A user can view the job generating service section so that they can see the other services")
    @Issue("LRT-2472")
    public void ViewJobCreditingSectionFromCoc()
    {
        int assetID = 501;
        final String jobScopeServiceType = "Machinery";
        final String productCatalogueName = "Classification";
        String jobId = "010000022";

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetID))
                .then().clickAssetSearchApplyButton();

        ViewAssetPage viewAssetPage = allAssetsSubPage.
                getAssetCardByAssetName(new Asset(assetID)
                        .getDto()
                        .getName())
                .clickJobButton();

        JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();

        AllJobElement allJobElement = jobsSubPage.getAllJobsByJobId(jobId);
        ViewJobDetailsPage viewJobDetailsPage
                = allJobElement.clickNavigateToJobArrowButton();

        AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();
        Surveyor surveyor = new Surveyor(1);
        String surveyorName = surveyor.getDto().getName();
        viewJobDetailsPage = addOrEditJobTeamPage.setLeadSurveyorTextBox(surveyorName)
                .then().selectTypeAheadSuggestionDropBoxByIndex(0)
                .then().clickSaveButton();

        if (viewJobDetailsPage.getTextOfAddOrGoToJobScopeButton().contains("Add"))
        {
            AddToJobScopePage addToJobScopePage = viewJobDetailsPage
                    .clickAddOrGoToJobScopeButton(AddToJobScopePage.class);
            ServiceAllPage serviceAllPage = addToJobScopePage.clickAllTab();

            JobScopeServiceTaskElements JobScopeServiceTaskElement
                    = serviceAllPage.getJobScopeServiceTaskByName(productCatalogueName);
            if (JobScopeServiceTaskElement.isProductCatalogueExpanded())
            {
                JobScopeServiceTaskElement.clickOnExpandIcon();
            }

            JobScopeServiceTypeElements jobScopeServiceTypeElement
                    = JobScopeServiceTaskElement.getJobScopeServiceTypeByName(jobScopeServiceType);
            if (jobScopeServiceTypeElement.isJobScopeServiceTypeExpanded())
            {
                jobScopeServiceTypeElement.clickOnExpandIcon();
            }

            List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements
                    = jobScopeServiceTypeElement.getAllJobScopeHierarchyNodeElements();
            String serviceTypeText = jobScopeHierarchyNodeElements.get(0).getServiceTypeText();
            jobScopeHierarchyNodeElements.get(0).selectCheckBox();
            JobScopePage jobScopePage = serviceAllPage.getFooter().clickAddButton(JobScopePage.class);

            viewJobDetailsPage = jobScopePage.clickNavigateBackButton();
        }
        //AC 1
        ConditionsOfClassPage conditionsOfClassPage
                = viewJobDetailsPage.clickGoToCocsButton();

        AddConditionOfClassPage addConditionOfClassPage = conditionsOfClassPage.clickAddNewButton();

        conditionsOfClassPage = addConditionOfClassPage.setTitle(CocData.title)
                .setDescription(CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(ConditionsOfClassPage.class);

        viewJobDetailsPage = conditionsOfClassPage.clickNavigateBackButton();
        JobScopePage jobScopePage = viewJobDetailsPage.clickAddOrGoToJobScopeButton(JobScopePage.class);
        JobScopePage.JobGeneratedTable jobGeneratedTable = jobScopePage.getJobGeneratedTable();
        List<JobScopeElements> jobGeneratedElements = jobGeneratedTable.getRows();

        assert_().withFailureMessage("The newly created Coc is expected to appear in job generated section of Job Scope Page")
                .that(jobGeneratedElements.get(0).getName()).contains(CocData.title);

        viewJobDetailsPage = jobScopePage.clickNavigateBackButton();
        CreditingPage creditingPage = viewJobDetailsPage.clickGoToCreditingButton();
        List<InfoElement> jobGeneratedServicesElements = creditingPage.getCocsTable().getRows();

        //AC 2
        assert_().withFailureMessage("The newly created Coc is expected to appear in job generated section of Crediting Page")
                .that(jobGeneratedServicesElements.get(0).getName()).contains(CocData.title);

        //AC 3a, 3b, 3c, 3d
        for (InfoElement jobGeneratedServicesElement : jobGeneratedServicesElements)
        {
            assert_().withFailureMessage("The job generated listing of Crediting Page is expected to have Service code")
                    .that(jobGeneratedServicesElement.getId())
                    .isNotEmpty();
            assert_().withFailureMessage("The job generated listing of Crediting Page is expected to have Service Name")
                    .that(jobGeneratedServicesElement.getName())
                    .isNotEmpty();
            assert_().withFailureMessage("The job generated listing of Crediting Page is expected to have CreditedBy fields")
                    .that(jobGeneratedServicesElement.getCreditedBy())
                    .isNotEmpty();
            assert_().withFailureMessage("The job generated listing of Crediting Page is expected to have Credited On field")
                    .that(jobGeneratedServicesElement.getCreditedOnDate())
                    .isNotEmpty();
        }

        //AC 4
        for (InfoElement jobGeneratedServicesElement : jobGeneratedServicesElements)
        {
            assert_().withFailureMessage("The job generated listing of Crediting Page is not editable")
                    .that(jobGeneratedServicesElement.isCreditedByFieldEditable())
                    .isFalse();
            assert_().withFailureMessage("The job generated listing of Crediting Page is not editable")
                    .that(jobGeneratedServicesElement.isCreditedOnFieldEditable())
                    .isFalse();
        }
    }
}
