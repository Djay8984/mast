package test.viewasset.batchactions;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.BatchActionData;
import helper.PageFactory;
import helper.TestDataHelper;
import model.asset.AssetQuery;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.batchaction.BatchActionsPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.DefineSubPage;
import workhub.batchaction.element.Asset;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.*;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class ResetSearchResultsForBatchActionsTest extends BaseTest
{

    final String searchTextInput = "Test AutoKL";
    final String classStatus = "Class Suspended";
    final String primaryBuilder = "Fincantieri";
    final int yardNumber = 1;
    final int minGrt = 100000;
    final int maxGrt = 1000000;
    final String customerName = "Ocean Shipbuilders Limited";
    final String customerRole = "Fleet manager";
    private final String[] expectedLifecycleStatusList = {
            "Under construction",
            "Build suspended",
            "In service",
            "Laid up",
            "Under repair",
            "In casualty",
            "Converting",
            "To be broken up",
            "Decommissioned",
            "To be handed over",
            "Cancelled"
    };
    private final String[] statCode = {
            "Cargo Carrying",
            "Tankers",
            "Liquefied Gas",
            "LNG Tanker"
    };
    private AssetQuery assetQuery;

    @DataProvider(name = "BatchActionType")
    public Object[][] batchAction()
    {
        return new Object[][]{
                {"Asset note"},
                {"Actionable item"}
        };
    }

    @BeforeMethod(alwaysRun = true)
    private void getData()
    {
        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setLifecycleStatusId(Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 10L));
        assetQuery = new AssetQuery(assetQueryDto);
    }

    @Test(description = "US7.19 AC:1- 4 - Search bar for Batch Action can be reset",
            dataProvider = "BatchActionType")
    @Issue("LRT-1080")
    public void batchActionSearchBarResetTest(String batchAction)
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        DefineSubPage defineSubPage;
        //Step: 3
        if (batchAction.equals("Asset note"))
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton();
        }
        else
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton();
        }
        defineSubPage.setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .setSurveyorTextInput(BatchActionData.surveyorGuidance)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        if (batchAction.equals("Actionable item"))
        {
            defineSubPage.setDueDate(now().plusDays(2)
                    .format(FRONTEND_TIME_FORMAT_DTS));
        }

        ApplySubPage applySubPage = defineSubPage.clickLrInternalButton()
                .clickNextButton();
        //Step: 4
        SearchFilterSubPage searchFilterSubPage
                = applySubPage.clickSearchFilterAssetsButton();

        //AC: 1 and 2
        //Step: 5, 6 , 7 and 8
        searchFilterSubPage.setAssetSearchInput(searchTextInput)
                .clickAssetSearchResetButton();
        applySubPage.clickSearchFilterAssetsButton();
        assert_().withFailureMessage("Search Text Field is expected to be empty ")
                .that(searchFilterSubPage.getAssetSearchInputText().isEmpty())
                .isTrue();

        //AC: 3 and AC 4
        //Step: 9
        searchFilterSubPage.clickAssetSearchResetButton();
        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());
    }

    @Test(description = "US7.19 AC:1-4 - Filters for Batch Action can be reset",
            dataProvider = "BatchActionType")
    @Issue("LRT-1092")
    public void batchActionFilterResetTest(String batchAction)
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        DefineSubPage defineSubPage;
        //Step: 3
        if (batchAction.equals("Asset note"))
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton();
        }
        else
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton();
        }
        defineSubPage.setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .setSurveyorTextInput(BatchActionData.surveyorGuidance)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        if (batchAction.equals("Actionable item"))
        {
            defineSubPage.setDueDate(now().plusDays(2)
                    .format(FRONTEND_TIME_FORMAT_DTS));
        }

        ApplySubPage applySubPage = defineSubPage.clickLrInternalButton()
                .clickNextButton();

        //Step: 4
        //Asset Type
        SearchFilterSubPage searchFilterSubPage
                = applySubPage.clickSearchFilterAssetsButton();
        AssetTypeFilterSubPage assetTypeFilterSubPage
                = searchFilterSubPage.clickAssetTypeFilterButton();

        assetTypeFilterSubPage.setStatcodeOneDropdown(statCode[0])
                .setStatcodeTwoDropdown(statCode[1])
                .setStatcodeThreeDropdown(statCode[2])
                .setStatcodeFourDropdown(statCode[3])
                .setStatcodeFiveDropdown(statCode[3]);

        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto().getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetTypeFilterButton();
        assert_().withFailureMessage("Statcode 1 value is expected to be  equal to 'ALL' ")
                .that(assetTypeFilterSubPage.getStatcodeOneDropdownValue())
                .isEqualTo("All");
        assert_().withFailureMessage("Statcode 1 is not expected to be Displayed ")
                .that(assetTypeFilterSubPage.isStatcodeTwoDropdownVisible())
                .isFalse();

        //Lifecycle status
        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage
                = searchFilterSubPage.clickLifecycleStatusFilterButton();

        lifecycleStatusFilterSubPage
                .clickClearButton()
                .selectLifeCycleStatus(expectedLifecycleStatusList[0]);
        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickLifecycleStatusFilterButton();
        for (int i = 0; i < expectedLifecycleStatusList.length; i++)
        {
            if (i == 8 || i == 10)
            {
                assert_()
                        .withFailureMessage(expectedLifecycleStatusList[i] +
                                "lifecycleStatus is expected to be checked")
                        .that(lifecycleStatusFilterSubPage
                                .isLifecycleStatusSelected(expectedLifecycleStatusList[i]))
                        .isFalse();
            }
            if (i != 8 && i != 10)
            {
                assert_()
                        .withFailureMessage(expectedLifecycleStatusList[i] +
                                "lifecycleStatus is expected to be checked")
                        .that(lifecycleStatusFilterSubPage
                                .isLifecycleStatusSelected(expectedLifecycleStatusList[i]))
                        .isTrue();
            }
        }

        //Class status
       ClassStatusFilterSubPage classStatusFilterSubPage
                = searchFilterSubPage.clickClassStatusFilterButton();
        classStatusFilterSubPage.clickClearButton().selectClassStatus(classStatus);
        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickClassStatusFilterButton();
        assert_().withFailureMessage("All Check boxes expected to set to default state ").
                that(classStatusFilterSubPage.getAllCheckboxCheckedStatus())
                .doesNotContain(false);

        //Build Location
        BuildLocationFilterSubPage buildLocationFilterSubPage
                = searchFilterSubPage.clickBuildLocationFilterButton();
        buildLocationFilterSubPage
                .setPrimaryBuilderSearchInput(primaryBuilder)
                .setYardNumberSearchInput(String.valueOf(yardNumber));

        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickBuildLocationFilterButton();

        assert_().withFailureMessage("Primary builder filed is expected to be Empty ")
                .that(buildLocationFilterSubPage.getPrimaryBuilderText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Yard number is expected to be Empty ")
                .that(buildLocationFilterSubPage.getYardNumberSarchText().isEmpty())
                .isTrue();

        //Build Date
        BuildDateFilterSubPage buildDateFilterSubPage
                = searchFilterSubPage.clickBuildDateFilterButton();
        buildDateFilterSubPage
                .setFrBuildDate(TestDataHelper.getYesterdayDate())
                .setToBuildDate(TestDataHelper.getTodayDate());

        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickBuildDateFilterButton();
        assert_().withFailureMessage("From Date is expected to be Empty ")
                .that(buildDateFilterSubPage.getFrBuildDate().isEmpty())
                .isTrue();
        assert_().withFailureMessage("To Date is expected to be Empty ")
                .that(buildDateFilterSubPage.getToBuildDate().isEmpty())
                .isTrue();

        //Flag
        FlagSearchSubPage flagSearchSubPage = searchFilterSubPage.clickFlagFilterButton();
        flagSearchSubPage.setFlagCode(TestDataHelper.getFlagCodeFromSearch());
        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickFlagFilterButton();

        assert_().withFailureMessage("Flag Code Test box is expected to be empty")
                .that(flagSearchSubPage.getFlagCodeText().isEmpty()).isTrue();

        //Gross Tonnage
        GrossTonnageSearchSubPage grossTonnageSearchSubPage
                = searchFilterSubPage.clickGrossTonnageFilterButton();
        grossTonnageSearchSubPage
                .setMinGrossTonnage(String.valueOf(minGrt))
                .setMaxGrossTonnage(String.valueOf(maxGrt));
        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickGrossTonnageFilterButton();
        assert_().withFailureMessage("Min Gross Tonnage is expected to be Empty")
                .that(grossTonnageSearchSubPage.getMinGrossTonnageText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("Max Gross Tonnage is expected to be Empty")
                .that(grossTonnageSearchSubPage.getMaxGrossTonnageText().isEmpty())
                .isTrue();

        //Customer
        CustomerFilterSubPage customerFilterSubPage
                = searchFilterSubPage.clickCustomerFilterButton();
        customerFilterSubPage.setCustomerName(customerName)
                .setCustomerRoleDropdown(customerRole);
        searchFilterSubPage.clickAssetSearchApplyButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();

        assert_().withFailureMessage("All Assets are expected to be displayed ")
                .that(applySubPage.getAllAssetCountFromAddAllButton())
                .isEqualTo(assetQuery.getDto()
                        .getContent().size());

        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickCustomerFilterButton();
        assert_().withFailureMessage("Customer name text box is expected to be empty")
                .that(customerFilterSubPage.getCustomerNameText().isEmpty())
                .isTrue();

        assert_().withFailureMessage("Customer role Dropdown is expected to set to Default")
                .that(customerFilterSubPage.getCustomerRoleDefaultDropdownValue()
                        .equals("Select a Customer role"))
                .isTrue();
        assert_().withFailureMessage("Customer function Dropdown is expected to set to Default")
                .that(customerFilterSubPage.getCustomerFunctionDefaultDropdownValue()
                        .equals("Select a Customer function"))
                .isTrue();
    }

    @Test(description = "US7.19 AC:5 - If the user resets after selecting " +
            "assets, assets remain selected",
            dataProvider = "BatchActionType")
    @Issue("LRT-1093")
    public void resetSelectingAssetsTest(String batchAction)
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        BatchActionsPage batchActionsPage = workHubPage.clickBatchActions();

        DefineSubPage defineSubPage;
        //Step: 3
        if (batchAction.equals("Asset note"))
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickAssetNoteRadioButton();
        }
        else
        {
            defineSubPage = batchActionsPage.getDefineSubPage()
                    .clickActionableItemRadioButton();
        }
        defineSubPage.setTitleTextInput(BatchActionData.title)
                .clickStatutoryButton()
                .setDescriptionTextInput(BatchActionData.description)
                .setSurveyorTextInput(BatchActionData.surveyorGuidance)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        if (batchAction.equals("Actionable item"))
        {
            defineSubPage.setDueDate(now().plusDays(2)
                    .format(FRONTEND_TIME_FORMAT_DTS));
        }

        ApplySubPage applySubPage = defineSubPage.clickLrInternalButton()
                .clickNextButton();

        //Step: 4
        //Asset Type
        SearchFilterSubPage searchFilterSubPage
                = applySubPage.clickSearchFilterAssetsButton();
        AssetTypeFilterSubPage assetTypeFilterSubPage
                = searchFilterSubPage.clickAssetTypeFilterButton();
        assetTypeFilterSubPage.setStatcodeOneDropdown(statCode[0]);
        searchFilterSubPage.clickAssetSearchApplyButton();
        List<Asset> assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Lifecycle status
        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage
                = searchFilterSubPage.clickLifecycleStatusFilterButton();

        lifecycleStatusFilterSubPage
                .clickClearButton()
                .selectLifeCycleStatus(expectedLifecycleStatusList[0]);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Class Status
        ClassStatusFilterSubPage classStatusFilterSubPage
                = searchFilterSubPage.clickClassStatusFilterButton();
        classStatusFilterSubPage.clickClearButton().selectClassStatus(classStatus);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Build Locations
        BuildLocationFilterSubPage buildLocationFilterSubPage
                = searchFilterSubPage.clickBuildLocationFilterButton();
        buildLocationFilterSubPage
                .setPrimaryBuilderSearchInput(primaryBuilder)
                .setYardNumberSearchInput(String.valueOf(yardNumber));
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Build Date
        BuildDateFilterSubPage buildDateFilterSubPage
                = searchFilterSubPage.clickBuildDateFilterButton();
        buildDateFilterSubPage
                .setFrBuildDate(now().minusDays(100)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setToBuildDate(TestDataHelper.getTodayDate());
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Flag
        FlagSearchSubPage flagSearchSubPage = searchFilterSubPage.clickFlagFilterButton();
        flagSearchSubPage.setFlagCode(TestDataHelper.getFlagCodeFromSearch());
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Gross Tonnage
        GrossTonnageSearchSubPage grossTonnageSearchSubPage
                = searchFilterSubPage.clickGrossTonnageFilterButton();
        grossTonnageSearchSubPage
                .setMinGrossTonnage(String.valueOf(minGrt))
                .setMaxGrossTonnage(String.valueOf(maxGrt));
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
        applySubPage.clickSearchFilterAssetsButton();

        //Customer
        CustomerFilterSubPage customerFilterSubPage
                = searchFilterSubPage.clickCustomerFilterButton();
        customerFilterSubPage.setCustomerName(customerName)
                .setCustomerRoleDropdown(customerRole);
        searchFilterSubPage.clickAssetSearchApplyButton();
        assetElements = applySubPage.getAssetSection().getCards();
        searchAndVerifyForAssetSelected(assetElements.get(0), applySubPage, searchFilterSubPage);
    }

    private ApplySubPage searchAndVerifyForAssetSelected(Asset assetElement,
            ApplySubPage applySubPage,
            SearchFilterSubPage searchFilterSubPage)
    {
        String imoNumber = assetElement.getAssetImoNumber();
        assetElement.clickAddAssetButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();
        applySubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(imoNumber)
                .clickAssetSearchApplyButton();
        List<Asset> allAssetElements = applySubPage.getAssetSection().getCards();
        assetElement = allAssetElements.stream()
                .filter(c -> c.getAssetImoNumber().equals(imoNumber))
                .findFirst().orElse(null);
        assert_().withFailureMessage("Asset is expected to be selected")
                .that(assetElement.isAddedToSelectionButtonDisplayed()).isTrue();
        applySubPage.clickRemoveAllButton()
                .clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();
        return PageFactory.newInstance(ApplySubPage.class);
    }
}
