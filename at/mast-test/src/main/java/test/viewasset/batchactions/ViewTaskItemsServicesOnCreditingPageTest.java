package test.viewasset.batchactions;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Issues;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.element.AllJobElement;
import workhub.WorkHubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class ViewTaskItemsServicesOnCreditingPageTest extends BaseTest
{
    @Test(description = "Story 11.14 - As a user who has confirmed the job scope," +
            " I want to navigate to the crediting page so that I can view all the relevant tasks," +
            " items and services that I need to report on",
            enabled = false
    )
    @Issues({@Issue("LRT-1981"),
             @Issue("LRT-2516")
    })
    public void navigateToCreditingPage()
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput("501")
                .clickAssetSearchApplyButton();

        JobsSubPage jobsSubPage = workHubPage.clickMyWorkTab()
                .getAssetCards()
                .get(0)
                .clickViewAssetButton()
                .clickJobsTab();
        List<AllJobElement> allJobElements = jobsSubPage.getAllJobsElements();

        for (int i = 0; i < allJobElements.size(); i++)
        {

            ViewJobDetailsPage viewJobDetailsPage = allJobElements.get(i)
                    .clickNavigateToJobArrowButton();
            //AC6
            if (viewJobDetailsPage.getJobScopeText().equals("Job scope to be confirmed"))
                assert_().withFailureMessage("Go to crediting is expected to be disabled")
                        .that(viewJobDetailsPage.isGoToCreditingButtonEnabled())
                        .isFalse();
            //AC1
            if (viewJobDetailsPage.getJobScopeText().equals("Job scope confirmed"))
            {
                assert_().withFailureMessage("Go to crediting is expected to be enabled")
                        .that(viewJobDetailsPage.isGoToCreditingButtonEnabled())
                        .isTrue();
                //AC16
                viewJobDetailsPage = viewJobDetailsPage.clickGoToCreditingButton()
                        .clickNavigateBackButton
                                (ViewJobDetailsPage.class);
                //no assertion needed - @Visible for saveButton
            }
            allJobElements = viewJobDetailsPage.getHeader().clickNavigateBackButton()
                    .getAllJobsElements();
        }
    }

    @Test(description = "Story 11.14 - AC 5, AC 16" +
            " As a user who has confirmed the job scope," +
            " I want to navigate to the crediting page so that I can view all the relevant tasks," +
            " items and services that I need to report on",
            enabled = false)
    @Issues({@Issue("LRT-1981"),
             @Issue("LRT-2516")
    })
    public void navigateToCreditingPage_AC5_AC16()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput("501")
                .clickAssetSearchApplyButton();

        JobsSubPage jobsSubPage = workHubPage.clickMyWorkTab()
                .getAssetCards()
                .get(0)
                .clickViewAssetButton()
                .clickJobsTab();
        List<AllJobElement> allJobElements = jobsSubPage.getAllJobsElements();

        //AC5
        CreditingPage creditingPage = allJobElements.get(0).clickNavigateToJobArrowButton()
                .clickGoToCreditingButton();
        //no assertion - @Visible for Header
        creditingPage.getHeader();

        //AC16
        //no assertion needed - @Visible for saveButton

        //AC10 - descope?
        //AC2
        //AC3
        //AC4
        //AC7 - accordion
        //AC8 -
        //AC9
        //AC11
        //AC13
        //AC14
        //AC15
        //AC18
    }

    @Test(description = "Story 11.14 - AC 14, AC15, AC18" +
            " As a user who has confirmed the job scope," +
            " I want to navigate to the crediting page so that I can view all the relevant tasks," +
            " items and services that I need to report on",
            enabled = false)
    @Issues({@Issue("LRT-1981"),
             @Issue("LRT-2516")
    })
    public void navigateToCreditingPage_AC14_AC15_AC18()
    {

        //Need to add the credits

        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput("504")
                .clickAssetSearchApplyButton();

        JobsSubPage jobsSubPage = workHubPage.clickMyWorkTab()
                .getAssetCards()
                .get(0)
                .clickViewAssetButton()
                .clickJobsTab();
        List<AllJobElement> allJobElements = jobsSubPage.getAllJobsElements();
        allJobElements.get(0).clickNavigateToJobArrowButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab()
                .getClassificationTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getServiceRows();

        //add the elements before the test

    }
}
