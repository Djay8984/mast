package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.ActionableItemPage;
import viewasset.sub.codicilsanddefects.assetnotes.AssetNotePage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.codicilsanddefects.defects.DefectsPage;
import viewasset.sub.codicilsanddefects.element.ActionableItemsElement;
import viewasset.sub.codicilsanddefects.element.AssetNotesElement;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import viewasset.sub.codicilsanddefects.element.DefectsElement;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.NUMBER_REGEX;

public class SearchByTitleDescriptionGuidanceTest extends BaseTest
{

    @Test(description = "US9.38 AC:1+2 - User can search by title, description " +
            "and/or surveyor guidance using a specific character string")
    @Issue("LRT-1673")
    public void searchByTitleDescriptionGuidanceTest()
    {
        final int assetId = 501;
        final String startingString = "a";
        final String endingString = "n";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 3
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //AC: 1 and AC: 2
        //Step: 4
        codicilsAndDefectsPage = searchFilterCodicilsAndDefectsPage
                .setSearchText("*" + endingString)
                .then().clickSearchButton();

        List<String> cocCardTitle = codicilsAndDefectsPage.getCocTitle();
        if (cocCardTitle != null)
        {
            for (String title : cocCardTitle)
            {
                codicilsAndDefectsPage.clickCocLoadMoreButton();
                ConditionsOfClassElement conditionsOfClassElement =
                        codicilsAndDefectsPage.getCocByTitle(title.toLowerCase());
                ViewConditionOfClassPage viewConditionOfClassPage =
                        conditionsOfClassElement.clickFurtherDetailsArrow();
                String description = viewConditionOfClassPage.getCocDescriptionText().replaceAll(NUMBER_REGEX, "");
                String cocTitle = viewConditionOfClassPage.getTitleText();
                assert_().withFailureMessage("Conditions of Class Name/Description ends with " + endingString)
                        .that(cocTitle.endsWith(endingString) || description.endsWith(endingString))
                        .isTrue();
                viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);
            }
        }

        List<String> actionableItemsTitle = codicilsAndDefectsPage.getActionableItemsTitle();
        if (actionableItemsTitle != null)
        {
            for (String title : actionableItemsTitle)
            {
                codicilsAndDefectsPage.clickActionableItemLoadMoreButton();
                ActionableItemsElement actionableItemsElement =
                        codicilsAndDefectsPage.getActionableItemByTitle(title);
                ActionableItemPage actionableItemPage =
                        actionableItemsElement.clickFurtherDetailsArrow();
                String description = actionableItemPage.getActionableItemDescriptionText();
                String actionableItemTitle = actionableItemPage.getActionableItemTitleText();
                assert_().withFailureMessage("Actionable Item Name/Description ends with " + endingString)
                        .that(actionableItemTitle.endsWith(endingString) || description.endsWith(endingString))
                        .isTrue();
                actionableItemPage.clickNavigateBackButton();
            }
        }

        List<String> assetNotesTitle = codicilsAndDefectsPage.getAssetNotesTitle();
        if (assetNotesTitle != null)
        {
            for (String title : assetNotesTitle)
            {
                codicilsAndDefectsPage.clickAssetNotesLoadMoreButton();
                AssetNotesElement assetNotesElement =
                        codicilsAndDefectsPage.getAssetNotesByTitle(title.toLowerCase());
                AssetNotePage assetNotePage =
                        assetNotesElement.clickFurtherDetailsArrow();

                String description = assetNotePage.getAssetNoteDescriptionText();
                String assetNoteTitle = assetNotePage.getAssetNoteItemTitleText();
                assert_().withFailureMessage("Asset Note Name/Description ends with " + endingString)
                        .that(assetNoteTitle.endsWith(endingString) || description.endsWith(endingString))
                        .isTrue();
                assetNotePage.clickNavigateBackButton();
            }
        }

        List<String> defectsTitle = codicilsAndDefectsPage.getDefectsTitle();
        if (defectsTitle != null)
        {
            for (String title : defectsTitle)
            {
                codicilsAndDefectsPage.clickDefectsLoadMoreButton();
                DefectsElement defectsElement =
                        codicilsAndDefectsPage.getDefectsByTitle(title);
                DefectsPage defectsPage =
                        defectsElement.clickFurtherDetailsArrow();
                String description = defectsPage.getDefectsDescriptionText();
                String defectItemName = defectsPage.getDefectItemNameText();
                assert_().withFailureMessage("Defects Name/Description ends with " + endingString)
                        .that(defectItemName.endsWith(endingString) || description.endsWith(endingString))
                        .isTrue();
                defectsPage.clickNavigateBackButton();
            }
        }
        //AC: 1 and AC: 2
        //Step: 5
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.setSearchText(startingString + "*")
                .then().clickSearchButton();

        cocCardTitle = codicilsAndDefectsPage.getCocTitle();
        if (cocCardTitle != null)
        {
            for (String title : cocCardTitle)
            {
                codicilsAndDefectsPage.clickCocLoadMoreButton();
                ConditionsOfClassElement conditionsOfClassElement =
                        codicilsAndDefectsPage.getCocByTitle(title);
                ViewConditionOfClassPage viewConditionOfClassPage =
                        conditionsOfClassElement.clickFurtherDetailsArrow();
                String description = viewConditionOfClassPage.getCocDescriptionText().replaceAll(NUMBER_REGEX, "");
                String cocTitle = viewConditionOfClassPage.getTitleText();
                assert_().withFailureMessage("Conditions of Class Name/Description starts with " + startingString)
                        .that(cocTitle.startsWith(startingString) || description.startsWith(startingString))
                        .isTrue();
                viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);
            }
        }

        actionableItemsTitle = codicilsAndDefectsPage
                .getActionableItemsTitle();
        if (actionableItemsTitle != null)
        {
            for (String title : actionableItemsTitle)
            {
                codicilsAndDefectsPage.clickActionableItemLoadMoreButton();
                ActionableItemsElement actionableItemsElement =
                        codicilsAndDefectsPage.getActionableItemByTitle(title);
                ActionableItemPage actionableItemPage =
                        actionableItemsElement.clickFurtherDetailsArrow();
                String description = actionableItemPage.getActionableItemDescriptionText();
                String actionableItemTitle = actionableItemPage.getActionableItemTitleText();
                assert_().withFailureMessage("Actionable Item Name/Description starts with " + startingString)
                        .that(actionableItemTitle.startsWith(startingString) || description.startsWith(startingString))
                        .isTrue();
                actionableItemPage.clickNavigateBackButton();
            }
        }

        assetNotesTitle = codicilsAndDefectsPage.getAssetNotesTitle();
        if (assetNotesTitle != null)
        {
            for (String title : assetNotesTitle)
            {
                codicilsAndDefectsPage.clickAssetNotesLoadMoreButton();
                AssetNotesElement assetNotesElement =
                        codicilsAndDefectsPage.getAssetNotesByTitle(title);
                AssetNotePage assetNotePage =
                        assetNotesElement.clickFurtherDetailsArrow();
                String description = assetNotePage.getAssetNoteDescriptionText();
                String assetNoteTitle = assetNotePage.getAssetNoteItemTitleText();
                assert_().withFailureMessage("Asset Note Name/Description starts with " + startingString)
                        .that(assetNoteTitle.startsWith(startingString) || description.startsWith(startingString))
                        .isTrue();
                assetNotePage.clickNavigateBackButton();
            }

            defectsTitle = codicilsAndDefectsPage.getDefectsTitle();
            if (defectsTitle != null)
            {
                for (String title : defectsTitle)
                {
                    codicilsAndDefectsPage.clickDefectsLoadMoreButton();
                    DefectsElement defectsElement =
                            codicilsAndDefectsPage.getDefectsByTitle(title);
                    DefectsPage defectsPage =
                            defectsElement.clickFurtherDetailsArrow();
                    String description = defectsPage.getDefectsDescriptionText();
                    String defectItemName = defectsPage.getDefectItemNameText();
                    assert_().withFailureMessage("Defects Name/Description starts with " + startingString)
                            .that(defectItemName.startsWith(startingString) || description.startsWith(startingString))
                            .isTrue();
                }
            }
        }
    }
}
