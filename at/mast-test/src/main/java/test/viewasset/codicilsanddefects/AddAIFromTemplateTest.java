package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.template.AddActionableItemTemplatePage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class AddAIFromTemplateTest extends BaseTest
{

    @Test(description = "US 8.6 - 7.7 AC:1-12 - Add Actionable Item from template – Search and select template")
    @Issue("LRT-1542")
    public void AddAIFromTemplateSearchAndSelect()
    {
        int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        String noTemplateFoundString = "No templates found for your search criteria. Please search again";
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();
        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();

        addActionableItemPage.setTitle(LloydsRegister.CocData.title)
                .clickClassButton()
                .setDescription(LloydsRegister.CocData.description)
                .setSurveyorTextInput("Surveyor guidance " + LloydsRegister.CocData.description)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton();

        //AC 1

        AddActionableItemTemplatePage addActionableItemTemplatePage = addActionableItemPage
                .clickStartFromATemplateButton();

        assert_().withFailureMessage("No search results expected to be displayed")
                .that(addActionableItemTemplatePage.getSearchMessage())
                .isEmpty();

        //AC 10
        addActionableItemPage = addActionableItemTemplatePage.clickCloseButton();

        addActionableItemTemplatePage = addActionableItemPage.clickStartFromATemplateButton();

        //AC 7
        assert_().withFailureMessage("The status is expected to be live by default")
                .that(addActionableItemTemplatePage.isLiveButtonSelected())
                .isTrue();

        //AC 2
        addActionableItemTemplatePage.setTitleText("title")
                .then()
                .clickFindTemplateButton();

        assert_().withFailureMessage("Empty search results are expected to be displayed with appropriate message")
                .that(addActionableItemTemplatePage.getSearchMessage())
                .isEqualTo(noTemplateFoundString);

        String searchTemplateString = "PSC HIGH RISK";
        addActionableItemTemplatePage.setTitleText(searchTemplateString);
        AddActionableItemTemplatePage.TemplateSearchResultTable table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        assert_().withFailureMessage("Search results are expected to be displayed based on the search criteria")
                .that(table.getRows().get(0).getName()).isEqualTo(searchTemplateString);
        //Ac 6
        addActionableItemTemplatePage.setTitleText("p*");
        table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        assert_().withFailureMessage("Search results are expected to be displayed with appropriate results")
                .that(table.getRows()
                        .stream()
                        .map(e ->
                                (e.getName())
                                        .contains("p*"))
                        .count())
                .isEqualTo(table
                        .getRows()
                        .size());

        addActionableItemTemplatePage.setTitleText("*t");
        table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        assert_().withFailureMessage("Search results are expected to be displayed with appropriate results")
                .that(table.getRows()
                        .stream()
                        .map(e ->
                                (e.getName())
                                        .contains("*t"))
                        .count())
                .isEqualTo(table
                        .getRows()
                        .size());

        addActionableItemTemplatePage.setTitleText("*a*");
        table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        List<AddActionableItemTemplatePage.DataRow> rows = table.getRows();
        assert_().withFailureMessage("Search results are expected to be displayed with appropriate results")
                .that(rows
                        .stream()
                        .map(e ->
                                (e.getName())
                                        .contains("*a*"))
                        .count())
                .isEqualTo(rows
                        .size());

        addActionableItemPage = addActionableItemTemplatePage.clickCloseButton();
        addActionableItemTemplatePage = addActionableItemPage.clickStartFromATemplateButton();

        //AC 9
        table = addActionableItemTemplatePage
                .clickFindTemplateButton();
        rows = table.getRows();

        Map<String, String> byName = rows.stream()
                .collect(Collectors.toMap(AddActionableItemTemplatePage.DataRow::getName, AddActionableItemTemplatePage
                        .DataRow::getStatus));
        for (Map.Entry<String, String> e : byName.entrySet())
        {
            assert_().withFailureMessage(e.getKey()
                    + " status is expected to be Live")
                    .that(e.getValue())
                    .contains("Live");

            //todo AC 3,4,5,8 - related to itemType - defect.
        }
    }

    @Test(description = "US8.6 - 7.8 AC:1-18 - Add Actionable Item from template – View template Search results")
    @Issue("LRT-1544")
    public void AddAIFromTemplateView()
    {
        int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();
        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();

        addActionableItemPage.setTitle(LloydsRegister.CocData.title)
                .clickClassButton()
                .setDescription(LloydsRegister.CocData.description)
                .setSurveyorTextInput("Surveyor guidance " + LloydsRegister.CocData.description)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton();

        AddActionableItemTemplatePage addActionableItemTemplatePage = addActionableItemPage
                .clickStartFromATemplateButton();

        assert_().withFailureMessage("No search results expected to be displayed")
                .that(addActionableItemTemplatePage.getSearchMessage())
                .isEmpty();
        //AC 1
        addActionableItemTemplatePage.clickAllButton()
                .then()
                .setTitleText("*");

        AddActionableItemTemplatePage.TemplateSearchResultTable table = addActionableItemTemplatePage.clickFindTemplateButton();
        List<AddActionableItemTemplatePage.DataRow> rows = table.getRows();

        //AC 4
        for (int i = 0; i < rows.size(); i++)
        {
            assert_().withFailureMessage("Search results expected to be displayed in the correct format")
                    .that(rows.get(i).getName().isEmpty() && rows.get(i).getDescriptionText().isEmpty() && rows.get(i).getStatus().isEmpty() && rows
                            .get(i).getAppliedToAssets().isEmpty())
                    .isFalse();
        }

        //AC 13
        List<AddActionableItemTemplatePage.DataRow> allRows = new ArrayList<>();
        rows.stream().filter(e ->
                !e.getStatus().contains("Live"))
                .forEach(e ->
                        assert_().withFailureMessage(" The templates which are not of live status are not expected to have a radio button")
                                .that(e.isRadioButtonDisplayed())
                                .isFalse());

        rows.get(1).clickNameRadioButton();
        rows.get(2).clickNameRadioButton();

        //AC 15

        List<AddActionableItemTemplatePage.DataRow> liveRows = table
                .getRows()
                .stream()
                .filter(e ->
                        e.getStatus().contains("Live")).collect(Collectors.toList());


        /* todo : Radio typified should be used.
        assert_().withFailureMessage("Only One template can be selected").that(
                liveRows.stream()
                        .filter(e ->
                                e.isNameRadioButtonSelected()).count())
                                .isEqualTo(1);*/

        //AC 1,4
        addActionableItemTemplatePage.clickLiveButton()
                .then()
                .setTitleText("*");

        //AC 7
        table = addActionableItemTemplatePage.clickFindTemplateButton();
        rows = table.getRows();

        assert_().withFailureMessage("Search criteria is expected to be the same")
                .that(addActionableItemTemplatePage.getTitleText()).isEqualTo("*");
        assert_().withFailureMessage("Search criteria is expected to be the same")
                .that(addActionableItemTemplatePage.isLiveButtonSelected()).isTrue();

        //AC 6
        rows.stream()
                .forEach(e ->
                        assert_().withFailureMessage("Name is expected to display in full")
                                .that(e.getName()
                                        .contains("..."))
                                .isFalse());

        //AC 10
        List<String> liveRowDescription = new ArrayList<String>();
        rows.stream()
                .forEach(e ->
                        liveRowDescription
                                .add(e.getDescriptionText()));

        for (int i = 1; i < rows.size(); i++)
        {

            assert_().withFailureMessage("Description is expected to be displayed in alphabetical order")
                    .that(rows.get(i).getDescriptionText().trim())
                    .isEqualTo(liveRowDescription.get(i).trim());

            //AC 5
            assert_().withFailureMessage("Description is expected to have a maximum of 150 characters")
                    .that(rows.get(i).getDescriptionText().length()).isAtMost(151);

            //AC 2
            assert_().withFailureMessage("The applied to assets should be an integer of size of 7 digits")
                    .that(Integer.parseInt(rows.get(i).getAppliedToAssets())).isLessThan(9999999);
        }

        //AC 14
        addActionableItemTemplatePage.setTitleText("");
        assert_().withFailureMessage("The search results are expected to be displayed")
                .that(table.getRows().size()).isNotEqualTo(0);
        addActionableItemPage = addActionableItemTemplatePage.clickCloseButton();

        //AC 16
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.isClassButtonSelected()).isTrue();
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.getDescriptionText()).contains(LloydsRegister.CocData.description);
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.isLrAndCustomerButtonEnabled()).isTrue();
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.getImposedDateText()).isNotNull();
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.getDueDateText()).isNotNull();

        //AC 12,14
        addActionableItemTemplatePage = addActionableItemPage
                .clickStartFromATemplateButton();

        assert_().withFailureMessage("No search results expected to be displayed")
                .that(addActionableItemTemplatePage.getSearchMessage())
                .isEmpty();

        //AC 11
        //AC 8, AC 3
        String searchTemplateString = "PSC HIGH RISK";
        addActionableItemTemplatePage.setTitleText(searchTemplateString);
        table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        assert_().withFailureMessage("Search results are expected to be displayed based on the search criteria")
                .that(table.getRows().get(0).getName()).isEqualTo(searchTemplateString);
    }
}
