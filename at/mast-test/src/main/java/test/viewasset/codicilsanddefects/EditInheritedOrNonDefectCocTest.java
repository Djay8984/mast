package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.modal.AddConfirmationPage;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import viewasset.sub.codicilsanddefects.modalwindow.EditConfirmationWindow;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class EditInheritedOrNonDefectCocTest extends BaseTest
{

    @Test(description = "Story 9.53 edit an inherited or non-defect CoC")
    @Issue("LRT-1583")
    public void editAnInheritedNonDefectCocTest()
    {

        final int assetId = 501;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        AddConditionOfClassPage addConditionOfClassPage
                = codicilsAndDefectsPage.clickCocAddNewButton();

        codicilsAndDefectsPage = addConditionOfClassPage.setTitle(CocData.title)
                .setDescription(CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddConfirmationPage.class)
                .clickOkButton();

        //AC: 1
        //Step: 5
        while (codicilsAndDefectsPage.isCocLoadMoreButtonDisplayed())
        {
            codicilsAndDefectsPage.clickCocLoadMoreButton();
        }
        List<ConditionsOfClassElement> conditionsOfClassElement
                = codicilsAndDefectsPage.getCocItems();

        ViewConditionOfClassPage viewConditionOfClassPage
                = conditionsOfClassElement.stream()
                .filter(e -> e.getCocTitle().toLowerCase().contains(CocData.title.toLowerCase()))
                .findFirst()
                .get().clickFurtherDetailsArrow();

        assert_().withFailureMessage("Edit button is expected to be clickable")
                .that(viewConditionOfClassPage.isEditButtonEnabled())
                .isTrue();

        //AC: 2
        //Step: 6
        EditConditionOfClassPage editConditionOfClassPage
                = viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Description Field is expected to be editable")
                .that(editConditionOfClassPage.isDescriptionFieldEditable())
                .isTrue();
        assert_().withFailureMessage("Due Date Field is expected to be editable")
                .that(editConditionOfClassPage.isDueDateFieldEditable())
                .isTrue();

        //AC: 3
        //Step: 7
        assert_().withFailureMessage("Close Edit Button is not expected to be displayed")
                .that(editConditionOfClassPage.isCloseEditButtonDisplayed())
                .isFalse();

        //AC: 4 and AC: 9
        //Step: 8
        editConditionOfClassPage.clickCancelButton();

        //AC: 6 AC: 7
        //Step: 9
        viewConditionOfClassPage.clickEditButton();
        String descriptionEdited = CocData.description + " Edit";
        editConditionOfClassPage.setDescription(descriptionEdited)
                .setDueDate(now().plusDays(4)
                        .format(FRONTEND_TIME_FORMAT_DTS));

        EditConfirmationWindow editConfirmationWindow
                = editConditionOfClassPage.clickNavigateBackButton(EditConfirmationWindow.class);

        editConfirmationWindow.clickContinueWithoutSaveChangesButton();
        viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Changes made in description field are not expected to be saved ")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(CocData.description))
                .isTrue();
        assert_().withFailureMessage("Changes made in Due date filed are not expected to be saved ")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(2)
                                .format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //AC: 8
        //Step: 10
        editConditionOfClassPage.setDescription(descriptionEdited)
                .setDueDate(now().plusDays(4)
                        .format(FRONTEND_TIME_FORMAT_DTS));

        editConditionOfClassPage.clickNavigateBackButton(EditConfirmationWindow.class);
        editConfirmationWindow.clickSaveChangesButton();
        viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Changes made in description field are expected to be saved ")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(descriptionEdited))
                .isTrue();
        assert_().withFailureMessage("Changes made in Due date filed are expected to be saved ")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(4)
                                .format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();

        //AC: 5
        //Step: 11
        String newDescription = CocData.description + " New";
        editConditionOfClassPage.setDescription(newDescription)
                .setDueDate(now().plusDays(5)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickSaveButton();

        viewConditionOfClassPage.clickEditButton();
        assert_().withFailureMessage("Changes made in description field are expected to be saved ")
                .that(editConditionOfClassPage.getDescriptionText()
                        .equals(newDescription))
                .isTrue();
        assert_().withFailureMessage("Changes made in Due date filed are expected to be saved ")
                .that(editConditionOfClassPage.getDueDateText()
                        .equals(now().plusDays(5)
                                .format(FRONTEND_TIME_FORMAT_DTS)))
                .isTrue();
    }
}
