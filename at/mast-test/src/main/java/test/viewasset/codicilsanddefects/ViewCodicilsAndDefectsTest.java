package test.viewasset.codicilsanddefects;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.element.ActionableItemsElement;
import viewasset.sub.codicilsanddefects.element.AssetNotesElement;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class ViewCodicilsAndDefectsTest extends BaseTest
{

    private AssetCoCQuery coc;
    private AssetActionableItemQuery actionableItem;
    private AssetNoteQuery assetNote;
    private AssetDefectQuery assetDefect;

    @BeforeMethod(alwaysRun = true)
    private void getData()
    {
        int assetId = 501;
        List<Long> confidentiality = Arrays.asList(1L, 2L, 3L);
        List<Long> cocCategory = Arrays.asList(7L,8L,9L,10L,11L,12L);
        List<Long> defectCategory = Arrays.asList(1L, 3L, 4L, 5L, 6L);
        List<Long> actionableItemCategory = Arrays.asList(4L,5L,6L, 11L, 12L, 13L, 14L, 15L);
        List<Long> assetNoteCategory = Arrays.asList(1L, 2L,3L,4L,5L,6L,7L,8L,9L, 10L);
        List<Long> status = Arrays.asList(2L, 3L, 4L, 9L, 10L, 11L, 14L);
        List<Long> defectStatus = Arrays.asList(1L);

        CodicilDefectQueryDto codicilDefectQuery = new CodicilDefectQueryDto();
        codicilDefectQuery.setConfidentialityList(confidentiality);
        codicilDefectQuery.setCategoryList(cocCategory);
        codicilDefectQuery.setStatusList(status);
        coc = new AssetCoCQuery(assetId, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(actionableItemCategory);
        actionableItem = new AssetActionableItemQuery(assetId, 0, 100, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(assetNoteCategory);
        assetNote = new AssetNoteQuery(assetId, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(defectCategory);
        codicilDefectQuery.setStatusList(defectStatus);
        assetDefect = new AssetDefectQuery(assetId, 0, 100, codicilDefectQuery);
    }

    @Test(description = "Automation - Story - 8.1 - As a user I want to view the codicils & defects tab" +
            " so that I can view a record of the codicils that have been raised against a specific asset")
    @Issue("LRT-1201")
    public void viewCodicilsAndDefectsTest()
    {
        int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name).clickViewAssetButton();

        //AC1
        CodicilsAndDefectsPage codicilsAndDefects = viewAsset.clickCodicilsAndDefectsTab();

        //AC2
        assert_().withFailureMessage("The conditions of class label should be present")
                .that(codicilsAndDefects.cocLabel()).contains("Conditions of Class");

        assert_().withFailureMessage("The expected amount of CoC does not match the actual")
                .that(codicilsAndDefects.cocNumber()).isEqualTo("(" + coc.getDto().getContent().size() + ")");

        //AC3
        CodicilsAndDefectsPage.CoCTable cocTable = codicilsAndDefects.getCocTable();
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }
        for (CoCDto coCDto : coc.getDto().getContent())
        {
            String title = returnTitle(coCDto.getTitle());

            ConditionsOfClassElement cocByTitle = codicilsAndDefects.getCocByTitle(title);

            assert_().withFailureMessage("The expected conditions of class with title " + title + " cannot be found")
                    .that(cocByTitle).isNotNull();

            assert_().withFailureMessage("The expected and actual id do not match")
                    .that(cocByTitle.getCoCId()).isEqualTo("000" + coCDto.getId());

            assert_().withFailureMessage("The expected and actual impose date do not match")
                    .that(cocByTitle.getImposeDate()).isEqualTo(FRONTEND_TIME_FORMAT.format(coCDto.getImposedDate()));

            assert_().withFailureMessage("The expected and actual due date do not match")
                    .that(cocByTitle.getDueDate()).isEqualTo(FRONTEND_TIME_FORMAT.format(coCDto.getDueDate()));

            //AC5
            assert_().withFailureMessage("The only state allowed to show is open")
                    .that(cocByTitle.getCocStatus()).isEqualTo("Open");
        }

        //AC2
        assert_().withFailureMessage("The actionable items label should be seen")
                .that(codicilsAndDefects.actionableItemLabel()).isEqualTo("Actionable Items");

        assert_().withFailureMessage("The expected amount of actionable items does not match actual")
                .that(codicilsAndDefects.actionableItemCount()).isEqualTo("(" + actionableItem.getDto().getContent().size() + ")");

        codicilsAndDefects = codicilsAndDefects.clickActionableItemLoadMoreButton();

        //AC3
        CodicilsAndDefectsPage.ActionableItemsTable actionableItemsTable = codicilsAndDefects.getActionableItemsTable();
        while (actionableItemsTable.isLoadMoreButtonDisplayed())
        {
            actionableItemsTable.clickLoadMoreButton();
        }
        for (ActionableItemDto actionableItemDto : actionableItem.getDto().getContent())
        {
            String title = returnTitle(actionableItemDto.getTitle());

            ActionableItemsElement actionableItemByTitle = codicilsAndDefects.getActionableItemByTitle(title);

            assert_().withFailureMessage("The expected actionable item cannot be found")
                    .that(actionableItemByTitle.getName()).isNotNull();

            assert_().withFailureMessage("The expected and actual id does not match")
                    .that(actionableItemByTitle.getActionableItemId()).isEqualTo("000" + actionableItemDto.getId());

            assert_().withFailureMessage("The expected and actual impose date do not match")
                    .that(actionableItemByTitle.getImposeDate()).isEqualTo(FRONTEND_TIME_FORMAT.format(actionableItemDto.getImposedDate()));

            assert_().withFailureMessage("The expected and actual due date does not match")
                    .that(actionableItemByTitle.getDueDate()).isEqualTo(FRONTEND_TIME_FORMAT.format(actionableItemDto.getDueDate()));

            //AC4
            assert_().withFailureMessage("The actionable item status should either be open, inactive or change recommended")
                    .that(actionableItemByTitle.getActionableItemStatus()).isAnyOf("Open", "Inactive", "Change Recommended");
        }

        //AC2
        assert_().withFailureMessage("The asset note label should be seen")
                .that(codicilsAndDefects.assetNoteLabel()).isEqualTo("Asset Notes");

        assert_().withFailureMessage("The expected amount of asset notes does not match actual")
                .that(codicilsAndDefects.assetNotesCount()).isEqualTo("(" + assetNote.getDto().getContent().size() + ")");


        codicilsAndDefects.clickAssetNotesLoadMoreButton();
        codicilsAndDefects = codicilsAndDefects.clickAssetNotesLoadMoreButton();

        //AC3
        for (AssetNoteDto assetNoteDto : assetNote.getDto().getContent())
        {
            String title = returnTitle(assetNoteDto.getTitle());

            AssetNotesElement assetNotesByTitle = codicilsAndDefects.getAssetNotesByTitle(title);

            assert_().withFailureMessage("The expected asset note cannot be found")
                    .that(assetNotesByTitle).isNotNull();

            assert_().withFailureMessage("The expected and actual id does not match")
                    .that(assetNotesByTitle.getAssetNoteId()).isEqualTo("000" + assetNoteDto.getId());

            assert_().withFailureMessage("The expected and actual impose date do not match")
                    .that(assetNotesByTitle.getAssetNoteImposeDate()).isEqualTo(FRONTEND_TIME_FORMAT.format(assetNoteDto.getImposedDate()));

            //AC4
            assert_().withFailureMessage("The asset note status should either be open, inactive or change recommended")
                    .that(assetNotesByTitle.getAssetNoteStatus()).isAnyOf("Open", "Inactive", "Change Recommended");
        }
        //AC6
        assert_().withFailureMessage("The number of CoCs, actionable items and asset notes do not match the total tally")
                .that(returnCount(codicilsAndDefects.assetNotesCount())
                        + returnCount(codicilsAndDefects.cocNumber())
                        + returnCount(codicilsAndDefects.actionableItemCount()))
                .isEqualTo(returnCount(codicilsAndDefects.getTotalCodicilsCount()));

        //AC8
        assert_().withFailureMessage("The Codicil Of Class add new button is not displayed")
                .that(codicilsAndDefects.isCocAddNewButtonDisplayed()).isTrue();

        assert_().withFailureMessage("The Actionable item add new button is not displayed")
                .that(codicilsAndDefects.isActionableItemAddNewButtonDisplayed()).isTrue();

        assert_().withFailureMessage("The Asset Note add new button is not displayed")
                .that(codicilsAndDefects.isAssetNoteAddNewButtonDisplayed()).isTrue();

        //AC9
        assert_().withFailureMessage("The defect title should be seen with only open defects shown")
                .that(codicilsAndDefects.getDefectTitle()).isEqualTo("Defects");

        assert_().withFailureMessage("The expected amount of defects does not match actual")
                .that(codicilsAndDefects.getTotalDefectsCount()).contains("(" + assetDefect.getDto().getContent().size() + ")");

        //AC7
        assetId = 12;
        asset = new Asset(assetId);
        name = asset.getDto().getName();
        workHubPage.getHeader().clickMastLogo();
        workHubPage.clickAllAssetsTab();
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        viewAsset = allAssets.getAssetCardByAssetName(name).clickViewAssetButton();
        codicilsAndDefects = viewAsset.clickCodicilsAndDefectsTab();

        assert_().withFailureMessage("The conditions of class label should be present")
                .that(codicilsAndDefects.cocLabel()).contains("Conditions of Class");

        assert_().withFailureMessage("The no items found label should be seen for conditions of class")
                .that(codicilsAndDefects.isCocNoItemsFound()).isTrue();

        assert_().withFailureMessage("The no items found label should be seen for actionable items")
                .that(codicilsAndDefects.isActionableItemsNoItemsFound()).isTrue();

        assert_().withFailureMessage("The no items found label should be seen for asset notes")
                .that(codicilsAndDefects.isAssetNotesNoItemsFound()).isTrue();

        assert_().withFailureMessage("The no items found label should be seen for defects")
                .that(codicilsAndDefects.isDefectNoItemsFound()).isTrue();
    }

    private int returnCount(String count)
    {
        count = count.replace("(", "");
        count = count.replace(")", "");
        return Integer.parseInt(count);
    }

    private String returnTitle(String title)
    {
        if (title.length() > 18)
        {
            return title.substring(0, 18) + "…";
        }
        return title;
    }
}
