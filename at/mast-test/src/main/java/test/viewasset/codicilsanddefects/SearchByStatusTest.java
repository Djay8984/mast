package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.element.ActionableItemsElement;
import viewasset.sub.codicilsanddefects.element.AssetNotesElement;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import viewasset.sub.codicilsanddefects.element.DefectsElement;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.sub.StatusSubPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class SearchByStatusTest extends BaseTest
{

    final int assetId = 501;
    final String[] statusCheckBoxes = {
            "Open",
            "Inactive",
            "Recommended change",
            "Deleted"};
    String expectedStatus = "Change Recommended";

    @Test(description = "US9.41 - Codicil search by status")
    @Issue("LRT-1557")
    public void searchByStatusTest()
    {

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 5
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //Step: 6
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();

        //AC: 1
        //Step: 7
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();

        //AC: 3
        //Step: 10
        statusSubPage.clickClearAllButton();
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isFalse();

        //AC: 2 and 4
        //Step: 8 and 9
        statusSubPage.clickSelectAllButton();
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isTrue();

        //Step 11 and 12
        searchFilterCodicilsAndDefectsPage.clickSearchButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isTrue();
    }

    @Test(description = "US9.41 - Test to search for Codicils and Defects " +
            "using the 'Status' filter of 'Open'")
    @Issue("LRT-1927")
    public void searchByStatusFilterOpenTest()
    {

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 5
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //Step: 6
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();

        //AC: 1
        //Step: 7
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();

        //AC: 2
        //Step: 8 and 9
        statusSubPage.clickClearAllButton().then().selectCheckBoxByName(statusCheckBoxes[0]);
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isFalse();

        //Step: 10
        searchFilterCodicilsAndDefectsPage.clickSearchButton();
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<ConditionsOfClassElement> conditionsOfClassElement =
                    codicilsAndDefectsPage.clickCocLoadMoreButton()
                            .then().getCocItems();
            List<String> cocStatus = conditionsOfClassElement
                    .stream()
                    .map(e -> e.getCocStatusText())
                    .collect(Collectors.toList());
            for (String cocStatusCheckBox : cocStatus)
            {
                assert_().withFailureMessage("All Coc status is expected to be " + statusCheckBoxes[0])
                        .that(cocStatusCheckBox.equals(statusCheckBoxes[0])).isTrue();
            }
        }

        //Step: 11
        if (codicilsAndDefectsPage.getActionableItems().size() > 0)
        {
            List<ActionableItemsElement> actionableItemsElement =
                    codicilsAndDefectsPage.clickActionableItemLoadMoreButton()
                            .then().getActionableItems();
            List<String> actionableItemStatus = actionableItemsElement
                    .stream()
                    .map(e -> e.getActionableItemStatus())
                    .collect(Collectors.toList());
            for (String actionableItem : actionableItemStatus)
            {
                assert_().withFailureMessage("All actionable item status is expected to be " + statusCheckBoxes[0])
                        .that(actionableItem.equals(statusCheckBoxes[0])).isTrue();
            }
        }

        //Step: 12
        if (codicilsAndDefectsPage.getAssetNote().size() > 0)
        {
            List<AssetNotesElement> assetNotesElement = codicilsAndDefectsPage.clickAssetNotesLoadMoreButton()
                    .then().getAssetNote();
            List<String> assetNotesStatus = assetNotesElement.
                    stream()
                    .map(e -> e.getAssetNoteStatus())
                    .collect(Collectors.toList());
            for (String assetNotes : assetNotesStatus)
            {
                assert_().withFailureMessage("All asset notes status is expected to be " + statusCheckBoxes[0])
                        .that(assetNotes.equals(statusCheckBoxes[0])).isTrue();
            }
        }

        //Step: 13
        if (codicilsAndDefectsPage.getDefects().size() > 0)
        {
            List<DefectsElement> defectsElement = codicilsAndDefectsPage.clickDefectsLoadMoreButton()
                    .then().getDefects();
            List<String> defectsStatus = defectsElement
                    .stream()
                    .map(e -> e.getDefectsStatusText())
                    .collect(Collectors.toList());
            for (String defects : defectsStatus)
            {
                assert_().withFailureMessage("All defects status is expected to be " + statusCheckBoxes[0])
                        .that(defects.equals(statusCheckBoxes[0])).isTrue();
            }
        }
    }

    @Test(description = "US9.41 - Test to search for Codicils and Defects " +
            "using the 'Status' filter of: 'Recommended change'")
    @Issue("LRT-1928")
    public void searchByStatusFilterRecommendedTest()
    {

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 5
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //Step: 6
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();

        //AC: 1
        //Step: 7
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();

        //AC: 2
        //Step: 8 and 9
        statusSubPage.clickClearAllButton().then().selectCheckBoxByName(statusCheckBoxes[2]);
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isFalse();

        //Step: 10
        searchFilterCodicilsAndDefectsPage.clickSearchButton();
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<ConditionsOfClassElement> conditionsOfClassElement =
                    codicilsAndDefectsPage.clickCocLoadMoreButton()
                            .then().getCocItems();
            List<String> cocStatus = conditionsOfClassElement
                    .stream()
                    .map(e -> e.getCocStatusText())
                    .collect(Collectors.toList());
            for (String cocStatusCheckBox : cocStatus)
            {
                assert_().withFailureMessage("All Coc status is expected to be " + statusCheckBoxes[2])
                        .that(cocStatusCheckBox.equals(expectedStatus)).isTrue();
            }
        }

        //Step: 11
        if (codicilsAndDefectsPage.getActionableItems().size() > 0)
        {
            List<ActionableItemsElement> actionableItemsElement =
                    codicilsAndDefectsPage.clickActionableItemLoadMoreButton()
                            .then().getActionableItems();
            List<String> actionableItemStatus = actionableItemsElement
                    .stream()
                    .map(e -> e.getActionableItemStatus())
                    .collect(Collectors.toList());
            for (String actionableItem : actionableItemStatus)
            {
                assert_().withFailureMessage("All actionable item status is expected to be " + statusCheckBoxes[2])
                        .that(actionableItem.equals(expectedStatus)).isTrue();
            }
        }

        //Step: 12
        if (codicilsAndDefectsPage.getAssetNote().size() > 0)
        {
            List<AssetNotesElement> assetNotesElement = codicilsAndDefectsPage.clickAssetNotesLoadMoreButton()
                    .then().getAssetNote();
            List<String> assetNotesStatus = assetNotesElement.
                    stream()
                    .map(e -> e.getAssetNoteStatus())
                    .collect(Collectors.toList());
            for (String assetNotes : assetNotesStatus)
            {
                assert_().withFailureMessage("All asset notes status is expected to be " + statusCheckBoxes[2])
                        .that(assetNotes.equals(expectedStatus)).isTrue();
            }
        }

    }

    @Test(description = "US9.41 - Test to search for Codicils and Defects using" +
            " the 'Status' filter of 'Deleted'")
    @Issue("LRT-1929")
    public void searchByStatusFilterDeletedTest()
    {

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 5
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //Step: 6
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();

        //AC: 1
        //Step: 7
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();

        //AC: 2
        //Step: 8 and 9
        statusSubPage.clickClearAllButton().then().selectCheckBoxByName(statusCheckBoxes[3]);
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isTrue();

        //Step: 10
        searchFilterCodicilsAndDefectsPage.clickSearchButton();
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<ConditionsOfClassElement> conditionsOfClassElement =
                    codicilsAndDefectsPage.clickCocLoadMoreButton()
                            .then().getCocItems();
            List<String> cocStatus = conditionsOfClassElement
                    .stream()
                    .map(e -> e.getCocStatusText())
                    .collect(Collectors.toList());
            for (String cocStatusCheckBox : cocStatus)
            {
                assert_().withFailureMessage("All CocStatus is expected to be " + statusCheckBoxes[3])
                        .that(cocStatusCheckBox.equals(statusCheckBoxes[3])).isTrue();
            }
        }

        //Step: 11
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<ActionableItemsElement> actionableItemsElement =
                    codicilsAndDefectsPage.clickActionableItemLoadMoreButton()
                            .then().getActionableItems();
            List<String> actionableItemStatus = actionableItemsElement
                    .stream()
                    .map(e -> e.getActionableItemStatus())
                    .collect(Collectors.toList());
            for (String actionableItem : actionableItemStatus)
            {
                assert_().withFailureMessage("All actionable item status is expected to be " + statusCheckBoxes[3])
                        .that(actionableItem.equals(statusCheckBoxes[3])).isTrue();
            }
        }

        //Step: 12
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<AssetNotesElement> assetNotesElement = codicilsAndDefectsPage.clickAssetNotesLoadMoreButton()
                    .then().getAssetNote();
            List<String> assetNotesStatus = assetNotesElement.
                    stream()
                    .map(e -> e.getAssetNoteStatus())
                    .collect(Collectors.toList());
            for (String assetNotes : assetNotesStatus)
            {
                assert_().withFailureMessage("All asset notes status is expected to be " + statusCheckBoxes[3])
                        .that(assetNotes.equals(statusCheckBoxes[3])).isTrue();
            }
        }

    }

    @Test(description = "US9.41 - Test to search for Codicils and Defects using " +
            "the 'Status' filter with a combination of statuses")
    @Issue("LRT-1930")
    public void searchByCombinationStatusFilterTest()
    {

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        //Step: 3
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 5
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //Step: 6
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();

        //AC: 1
        //Step: 7
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();

        //AC: 2
        //Step: 8 and 9
        statusSubPage.clickClearAllButton().then().selectCheckBoxByName(statusCheckBoxes[0])
                .then().selectCheckBoxByName(statusCheckBoxes[1]);
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isFalse();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isFalse();

        //Step: 10
        searchFilterCodicilsAndDefectsPage.clickSearchButton();
        if (codicilsAndDefectsPage.getCocItems().size() > 0)
        {
            List<ConditionsOfClassElement> conditionsOfClassElement =
                    codicilsAndDefectsPage.clickCocLoadMoreButton()
                            .then().getCocItems();
            List<String> cocStatus = conditionsOfClassElement
                    .stream()
                    .map(e -> e.getCocStatusText())
                    .collect(Collectors.toList());
            for (String cocStatusCheckBox : cocStatus)
            {
                assert_().withFailureMessage("All Coc status is expected to be "
                        + statusCheckBoxes[0] + " or" + statusCheckBoxes[1])
                        .that(cocStatusCheckBox.equals(statusCheckBoxes[0])
                                || cocStatusCheckBox.equals(statusCheckBoxes[1]))
                        .isTrue();
            }
        }

        //Step: 11
        if (codicilsAndDefectsPage.getActionableItems().size() > 0)
        {
            List<ActionableItemsElement> actionableItemsElement =
                    codicilsAndDefectsPage.clickActionableItemLoadMoreButton()
                            .then().getActionableItems();
            List<String> actionableItemStatus = actionableItemsElement
                    .stream()
                    .map(e -> e.getActionableItemStatus())
                    .collect(Collectors.toList());
            for (String actionableItem : actionableItemStatus)
            {
                assert_().withFailureMessage("All actionable item status is expected " +
                        "to be " + statusCheckBoxes[0] + " or" + statusCheckBoxes[1])
                        .that(actionableItem.equals(statusCheckBoxes[0])
                                || actionableItem.equals(statusCheckBoxes[1]))
                        .isTrue();
            }
        }

        //Step: 12
        if (codicilsAndDefectsPage.getAssetNote().size() > 0)
        {
            List<AssetNotesElement> assetNotesElement =
                    codicilsAndDefectsPage.clickAssetNotesLoadMoreButton()
                            .then().getAssetNote();
            List<String> assetNotesStatus = assetNotesElement.
                    stream()
                    .map(e -> e.getAssetNoteStatus())
                    .collect(Collectors.toList());
            for (String assetNotes : assetNotesStatus)
            {
                assert_().withFailureMessage("All asset notes status is expected " +
                        "to be " + statusCheckBoxes[0] + " or" + statusCheckBoxes[1])
                        .that(assetNotes.equals(statusCheckBoxes[0])
                                || assetNotes.equals(statusCheckBoxes[1]))
                        .isTrue();
            }
        }

        //Step: 13
        if (codicilsAndDefectsPage.getDefects().size() > 0)
        {
            List<DefectsElement> defectsElement = codicilsAndDefectsPage.clickDefectsLoadMoreButton()
                    .then().getDefects();
            List<String> defectsStatus = defectsElement
                    .stream()
                    .map(e -> e.getDefectsStatusText())
                    .collect(Collectors.toList());
            for (String defects : defectsStatus)
            {
                assert_().withFailureMessage("All defects status is expected " +
                        "to be " + statusCheckBoxes[0] + " or" + statusCheckBoxes[1])
                        .that(defects.equals(statusCheckBoxes[0])
                                || defects.equals(statusCheckBoxes[1]))
                        .isTrue();
            }
        }
    }
}

