package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.sub.*;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class ResetCodicilsAndDefectsSearchFiltersTest extends BaseTest
{

    @Test(description = "US9.37 - AC:1-3 - User is able to reset the codicils and defects search and filters")
    @Issue("LRT-1663")

    public void resetCodicilsAndDefectsSearchFiltersTest() throws ParseException
    {

        LocalDate localDate = LocalDate.now();
        final int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();
        final String[] expectedTypeAndCategoryFilterCheckBoxes = {
                "Asset Note",
                "Actionable Item",
                "Condition of Class",
                "Defect"};

        final String[] statusCheckBoxes = {
                "Open",
                "Inactive",
                "Recommended change",
                "Deleted"};

        final String[] confidentialityCheckBoxes = {
                "LR internal",
                "LR and customer",
                "All"};

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //AC: 1, AC: 2 and AC: 3
        //Step: 3
        searchFilterCodicilsAndDefectsPage.setSearchText("This is test search")
                .then().clickResetButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        assert_().withFailureMessage("Search Field is expected to be blank")
                .that(searchFilterCodicilsAndDefectsPage.getSearchText()
                        .isEmpty())
                .isTrue();

        //Reset Type and character Filters
        TypesAndCategorySubPage typesAndCategorySubPage
                = searchFilterCodicilsAndDefectsPage.clickTypeAndCategoryFilterButton();
        typesAndCategorySubPage.clickClearAllButton();
        searchFilterCodicilsAndDefectsPage.clickResetButton();

        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickTypeAndCategoryFilterButton();

        for (String expectedTypeAndCategoryFilterCheckBox : expectedTypeAndCategoryFilterCheckBoxes)
            assert_().withFailureMessage("All checkboxes aren't set to the default statuses")
                    .that(typesAndCategorySubPage
                            .isTypeAndCategoryFilterCheckBoxesSelected
                                    (expectedTypeAndCategoryFilterCheckBox))
                    .isTrue();

        //Reset Due date Filter
        DueDateSubPage dueDateSubPage
                = searchFilterCodicilsAndDefectsPage.clickDueDateFilterButton();

        Date fromDate = Date.valueOf(localDate);
        LocalDate date = localDate.plusDays(1);
        Date toDate = Date.valueOf(date);
        dueDateSubPage.setFromDueDate(String.valueOf(FRONTEND_TIME_FORMAT.format(fromDate)))
                .then().setToDueDate(String.valueOf(FRONTEND_TIME_FORMAT.format(toDate)));
        searchFilterCodicilsAndDefectsPage.clickResetButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickDueDateFilterButton();
        assert_().withFailureMessage("From Due date is expected to be empty")
                .that(dueDateSubPage.getFromDueDateText().isEmpty())
                .isTrue();
        assert_().withFailureMessage("To Due date is expected to be empty")
                .that(dueDateSubPage.getToDueDateText().isEmpty())
                .isTrue();

        //Reset Status Filter
        StatusSubPage statusSubPage
                = searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();
        statusSubPage.clickClearAllButton();
        searchFilterCodicilsAndDefectsPage.clickResetButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickStatusFilterButton();
        assert_().withFailureMessage(statusCheckBoxes[0] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[1] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[2] + " is expected to be checked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[2]))
                .isTrue();
        assert_().withFailureMessage(statusCheckBoxes[3] + " is expected to be unchecked ")
                .that(statusSubPage.isStatusCheckBoxSelected(statusCheckBoxes[3]))
                .isFalse();

        //Reset Item type filter
        ItemTypeSubPage itemTypeSubPage
                = searchFilterCodicilsAndDefectsPage.clickItemTypeFilterButton();
        itemTypeSubPage.setItemTypeSearch("This is test Item");
        searchFilterCodicilsAndDefectsPage.clickResetButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickItemTypeFilterButton();
        assert_().withFailureMessage("Item Type Search field is expected to be blank")
                .that(itemTypeSubPage.getItemTypeText().isEmpty())
                .isTrue();

        //Reset Confidentiality filter
        ConfidentialitySubPage confidentialitySubPage
                = searchFilterCodicilsAndDefectsPage.clickConfidentialityFilterButton();
        confidentialitySubPage.clickClearAllButton();
        searchFilterCodicilsAndDefectsPage.clickResetButton();
        codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();
        searchFilterCodicilsAndDefectsPage.clickConfidentialityFilterButton();

        assert_().withFailureMessage(confidentialityCheckBoxes[0] + " is expected to be checked ")
                .that(confidentialitySubPage.isConfidentialityCheckBoxSelected(confidentialityCheckBoxes[0]))
                .isTrue();
        assert_().withFailureMessage(confidentialityCheckBoxes[1] + " is expected to be checked ")
                .that(confidentialitySubPage.isConfidentialityCheckBoxSelected(confidentialityCheckBoxes[1]))
                .isTrue();
        assert_().withFailureMessage(confidentialityCheckBoxes[2] + " is expected to be checked ")
                .that(confidentialitySubPage.isConfidentialityCheckBoxSelected(confidentialityCheckBoxes[2]))
                .isTrue();

    }

}
