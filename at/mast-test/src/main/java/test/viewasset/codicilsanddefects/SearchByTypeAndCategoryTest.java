package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.sub.TypesAndCategorySubPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import static com.google.common.truth.Truth.assert_;

public class SearchByTypeAndCategoryTest extends BaseTest
{

    private final String[] expectedTypeAndCategoryFilterCheckBoxes = {
            "Asset Note",
            "Actionable Item",
            "Condition of Class",
            "Defect"};

    private final String[] expectedAssetNoteAndActionableCheckBoxes = {
            "Statutory",
            "Class",
            "External",
            };

    private final String[] expectedCocCheckBoxes = {
            "Hull",
            "Machinery",
            "Refridgeration",
            "Lifting Appliances",
            "Asset"};

    private final String[] expectedDefectCheckBoxes = {
            "Electro-Technical",
            "Hull",
            "Lifting Appliance",
            "Machinery",
            "Refrigeration"};

    @Test(description = "US9.39 - Codicil search by type and category")
    @Issue("LRT-1553")
    public void searchByTypeAndCategoryTest()
    {

        final int assetId = 501;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        //Step: 3
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Step: 4
        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        //AC: 1
        //Step: 5
        TypesAndCategorySubPage typesAndCategorySubPage =
                searchFilterCodicilsAndDefectsPage.clickTypeAndCategoryFilterButton();

        //AC: 2
        //Step: 6
        for (String expectedTypeAndCategoryFilterCheckBox : expectedTypeAndCategoryFilterCheckBoxes)
            assert_().withFailureMessage("All checkboxes aren't selected by default")
                    .that(typesAndCategorySubPage
                            .isTypeAndCategoryFilterCheckBoxesSelected
                                    (expectedTypeAndCategoryFilterCheckBox))
                    .isTrue();

        //AC: 3
        //Step: 7
        for (String expectedAssetNoteAndActionableCheckBox : expectedAssetNoteAndActionableCheckBoxes)
            assert_().withFailureMessage("All Assets Note commonpage checkboxes aren't selected by default")
                    .that(typesAndCategorySubPage
                            .isAssetNoteSubCheckBoxesSelected
                                    (expectedAssetNoteAndActionableCheckBox))
                    .isTrue();

        for (String expectedAssetNoteAndActionableCheckBox : expectedAssetNoteAndActionableCheckBoxes)
            assert_().withFailureMessage("All Actionable Item commonpage checkboxes aren't selected by default")
                    .that(typesAndCategorySubPage
                            .isActionableSubCheckBoxesSelected
                                    (expectedAssetNoteAndActionableCheckBox))
                    .isTrue();

        for (String expectedCocCheckBox : expectedCocCheckBoxes)
            assert_().withFailureMessage("All Coc commonpage checkboxes aren't selected by default")
                    .that(typesAndCategorySubPage
                            .isCocSubCheckBoxesSelected
                                    (expectedCocCheckBox))
                    .isTrue();

        for (String expectedDefectCheckBox : expectedDefectCheckBoxes)
            assert_().withFailureMessage("All defect checkboxes aren't selected by default")
                    .that(typesAndCategorySubPage
                            .isDefectSubCheckBoxesSelected
                                    (expectedDefectCheckBox))
                    .isTrue();

        //AC: 4
        //Step: 8
        typesAndCategorySubPage.clickClearAllButton();
        for (String expectedTypeAndCategoryFilterCheckBox : expectedTypeAndCategoryFilterCheckBoxes)
            assert_().withFailureMessage("All checkboxes aren't un-selected")
                    .that(typesAndCategorySubPage
                            .isTypeAndCategoryFilterCheckBoxesSelected
                                    (expectedTypeAndCategoryFilterCheckBox))
                    .isFalse();

        typesAndCategorySubPage.clickSelectAllButton();
        for (String expectedTypeAndCategoryFilterCheckBox : expectedTypeAndCategoryFilterCheckBoxes)
            assert_().withFailureMessage("All checkboxes aren't by default selected")
                    .that(typesAndCategorySubPage
                            .isTypeAndCategoryFilterCheckBoxesSelected
                                    (expectedTypeAndCategoryFilterCheckBox))
                    .isTrue();
    }
}
