package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.element.NoteAndAttachmentElement;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;

import static com.google.common.truth.Truth.assert_;

public class DeleteExistingNoteTest extends BaseTest
{

    final int assetId = 501;
    final String cocId = "00021";

    @Test(description = "US 9.19 - 9.5 AC1-4 - Delete an " +
            "existing 'Asset note' on CoC edit")
    @Issue("LRT-2071")
    public void deleteAssetNoteCocEditModeTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        MyWorkAssetElement myWorkAssetElement = allAssetsSubPage.getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(name))
                .findFirst()
                .get()
                .clickOpenAssetTitle();

        ViewAssetPage viewAssetPage = myWorkAssetElement.clickViewAssetButton();

        //Step: 3
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAssetPage
                .clickCodicilsAndDefectsTab();

        //Step: 4
        ConditionsOfClassElement conditionsOfClassElement =
                codicilsAndDefectsPage.getCocItems().get(1);
        ViewConditionOfClassPage viewConditionOfClassPage =
                conditionsOfClassElement.clickFurtherDetailsArrow();

        //Step: 5
        EditConditionOfClassPage editConditionOfClassPage = viewConditionOfClassPage
                .clickEditButton();

        //Step: 6
        AttachmentsAndNotesPage attachmentsAndNotesPage =
                editConditionOfClassPage.clickAttachmentButton();
        /*
        // This is part of Pre-requisite.
        //If Note is not present then add a Note
         */
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() == 0)
        {
            String randomNote = RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().clickAllButton()
                    .then().clickAddNoteButton();

            assert_().withFailureMessage("Added Note is Present")
                    .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(randomNote))
                    .isTrue();
        }

        //AC: 1
        //Step: 7
        String noteText = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText();
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        assert_().withFailureMessage("Edit Note Icon is Displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is Displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 8
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        //AC: 4
        //Step: 9
        confirmationModalPage.clickCancelButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 4
        //Step: 10
        //Cannot close and Reopen the MAST App. So This page to be refreshed
        noteSubPage.refreshPage();
        editConditionOfClassPage.clickAttachmentButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 3
        //Step: 11
        if (!noteDetailsSubPage.isDeleteIconDisplayed())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        }
        noteDetailsSubPage.clickDeleteIcon().then()
                .clickConfirmationButton(AttachmentsAndNotesPage.class);
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //AC: 3
        //Step: 12
        //Cannot close and Reopen the MAST App. So This page to be refreshed
        noteSubPage.refreshPage();
        editConditionOfClassPage.clickAttachmentButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();
    }

    @Test(description = "US 9.19 - 9.5 AC1-4 - Delete an existing " +
            "'Asset note' on CoC creation")
    @Issue("LRT-2072")
    public void deleteAssetNoteCocCreationModeTest()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        MyWorkAssetElement myWorkAssetElement = allAssetsSubPage.getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(name))
                .findFirst()
                .get()
                .clickOpenAssetTitle();

        ViewAssetPage viewAssetPage = myWorkAssetElement.clickViewAssetButton();

        //Step: 3
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAssetPage
                .clickCodicilsAndDefectsTab();

        //Step: 4
        AddConditionOfClassPage addConditionOfClassPage =
                codicilsAndDefectsPage.clickCocAddNewButton();

        //AC: 1
        //Step: 5
        AttachmentsAndNotesPage attachmentsAndNotesPage =
                addConditionOfClassPage.clickAttachmentButton();

        //Step: 6 and 7
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        ArrayList<String> noteName = new ArrayList<>();
        if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() < 13)
        {
            int count = attachmentsAndNotesPage.getNoteAndAttachmentCards().size();
            for (int i = 0; i < 12 - count; i++)
            {
                String randomNote = RandomStringUtils.randomAlphabetic(10);
                noteName.add(randomNote);
                noteSubPage.setNoteTextBox(randomNote)
                        .then().clickAllButton()
                        .then().clickAddNoteButton();
                //First 12 note will be displayed in UI
                if (attachmentsAndNotesPage.getNoteAndAttachmentCards().size() < 13)
                {
                    assert_().withFailureMessage("Added Note is Present")
                            .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(randomNote))
                            .isTrue();
                }
                noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
            }
        }

        //AC: 1
        //Step: 8
        String noteText = attachmentsAndNotesPage.getNoteAndAttachmentCards().get(0).getNoteText();
        NoteDetailsSubPage noteDetailsSubPage = attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        assert_().withFailureMessage("Edit Note Icon is Displayed")
                .that(noteDetailsSubPage.isEditIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is Displayed")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 9
        ConfirmationModalPage confirmationModalPage = noteDetailsSubPage.clickDeleteIcon();
        //AC: 4
        //Step: 10
        confirmationModalPage.clickCancelButton();
        assert_().withFailureMessage("Note is present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC:4
        //Step: 11
        //Cannot close and Reopen the MAST App. So This page to be refreshed
        assert_().withFailureMessage("Note isn't in expanded view after clicking after")
                .that(noteDetailsSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 3
        //Step: 12
        if (!noteDetailsSubPage.isDeleteIconDisplayed())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteText);
        }
        noteDetailsSubPage.clickDeleteIcon().then()
                .clickConfirmationButton(AttachmentsAndNotesPage.class);
        assert_().withFailureMessage("Note is present after deletion")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //AC: 3
        //Step: 13
        //Cannot close and Reopen the MAST App. So This page to be refreshed
        noteSubPage.refreshPage();
        addConditionOfClassPage.clickAttachmentButton();
        assert_().withFailureMessage("Note is not present")
                .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //CleanUp Code to clean all added Note
        for (NoteAndAttachmentElement noteElement : attachmentsAndNotesPage.getNoteAndAttachmentCards())
        {
            attachmentsAndNotesPage.clickOnNoteByNoteText(noteElement.getNoteText());
            noteDetailsSubPage.clickDeleteIcon().then()
                    .clickConfirmationButton(AttachmentsAndNotesPage.class);
            assert_().withFailureMessage("Note is not present")
                    .that(attachmentsAndNotesPage.isNoteByNoteTextDisplayed(noteElement.getNoteText()))
                    .isFalse();
        }

    }

}
