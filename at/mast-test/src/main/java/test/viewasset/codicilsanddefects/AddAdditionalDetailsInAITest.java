package test.viewasset.codicilsanddefects;

import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.asset.AssetModel;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.BreakLinkToTemplateModalPage;
import viewasset.sub.codicilsanddefects.actionableitems.select.SelectAssetItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.template.AddActionableItemTemplatePage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;
import static model.referencedata.ReferenceDataMap.RefData.CODICIL_TEMPLATES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class AddAdditionalDetailsInAITest extends BaseTest
{

    @Test(description = "US8.7. AC1 - Story 7.9 - As a user who has selected and applied a template for Actionable Item, " +
            "I want to add additional required details so that I can progress to the next step of the process" +
            "US8.7. AC2-9 - Actionable Item template - Add additional details")
    @Issue("LRT-1550 LRT-1551")
    public void viewAdditionalDetailsAITest()
    {
        int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        String noTemplateFoundString = "No templates found for your search criteria. Please search again";
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();
        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();

        addActionableItemPage.clickClassButton();
        //AC 1
        AddActionableItemTemplatePage addActionableItemTemplatePage = addActionableItemPage
                .clickStartFromATemplateButton();

        AddActionableItemTemplatePage.TemplateSearchResultTable table = addActionableItemTemplatePage
                .clickFindTemplateButton();

        table.getRows().get(0).clickNameRadioButton();
        addActionableItemPage = addActionableItemTemplatePage.clickSelectTemplateButton();

        ArrayList<CodicilTemplateDto> codicilTemplates =
                new ReferenceDataMap(ASSET, CODICIL_TEMPLATES).getReferenceDataAsClass
                        (CodicilTemplateDto.class);

        assert_().withFailureMessage("The template Name is expected to be displayed")
                .that(addActionableItemPage.getTemplateName())
                .isEqualTo(codicilTemplates
                        .get(0)
                        .getTitle());
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.getDescriptionText())
                .isEqualTo(codicilTemplates.get(0).getDescription());
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that((addActionableItemPage.isLrAndCustomerButtonEnabled())
                        || (addActionableItemPage.isLrInternalButtonEnabled())
                        || (addActionableItemPage.isAllButtonEnabled()))
                .isFalse();

        String confidentiality = codicilTemplates.get(0).getConfidentialityType().toString().replaceAll("[^0-9]", "");
        if (Integer.parseInt(confidentiality) == 3)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isAllButtonSelected());
        }
        else if (Integer.parseInt(confidentiality) == 2)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isLrInternalButtonSelected());
        }
        else if (Integer.parseInt(confidentiality) == 1)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isLrAndCustomerButtonSelected());
        }

        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage
                        .isTitleEnabled())
                .isFalse();
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage
                        .isDescriptionEnabled())
                .isFalse();

        //AC 4
        SelectAssetItemPage selectAssetItemPage = addActionableItemPage.clickSelectItemOrChangeSelectionButton();

        AssetModel assetModel = new AssetModel(assetId);
        List<ItemDto> assetItemsBE = assetModel
                .getDto()
                .getItems()
                .get(0)
                .getItems();

        SelectAssetItemPage.ItemTable itemTable
                = selectAssetItemPage.getItemTable();
        //AC 5
        assert_().withFailureMessage("The asset model is displayed for the current asset")
                .that(itemTable.getRows()
                        .get(0)
                        .getChildren()
                        .size())
                .isEqualTo(assetItemsBE.size());

        //AC 3,6
        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickSelectRadioBox();

        //AC 7
        addActionableItemPage = selectAssetItemPage.clickCancelButton();

        //AC 2 , AC 4
        addActionableItemPage.setImposedDate(now().minusDays(1)
                .format(FRONTEND_TIME_FORMAT_DTS));

        String errorMessageImposedDate = "The imposed date cannot be in the past";
        String errorMessageDueDate = "The due date cannot be before the imposed date";
        String errorMessageInvalidDateFormat = "Date is not valid. Must be in the format 'DD MMM YYYY'";
        String errorMessageInvalidDate = "This date does not exist";
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageImposedDate);

        addActionableItemPage.setImposedDate("29 Feb 2017");
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageInvalidDate);

        addActionableItemPage.setImposedDate("32 Aug 2017");
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageInvalidDateFormat);

        addActionableItemPage.setImposedDate(now().plusDays(1)
                .format(FRONTEND_TIME_FORMAT_DTS));

        //AC 3, AC 4
        addActionableItemPage.setDueDate(now().minusDays(1)
                .format(FRONTEND_TIME_FORMAT_DTS));

        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageDueDate);

        addActionableItemPage.setDueDate("29 Feb 2017");
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageInvalidDate);

        addActionableItemPage.setDueDate("32 Aug 2017");
        assert_().withFailureMessage("The entry in addActionableItemPage cannot be modified")
                .that(addActionableItemPage.getErrorMessage())
                .contains(errorMessageInvalidDateFormat);

        addActionableItemPage.setDueDate(now().plusDays(2)
                .format(FRONTEND_TIME_FORMAT_DTS));

        //AC 5
        assert_().withFailureMessage("The Break Link to Template button is expected to be displayed")
                .that(addActionableItemPage
                        .isBreakLinkToTemplateButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("The Change Template button is expected to be displayed")
                .that(addActionableItemPage
                        .isChangeTemplateButtonDisplayed())
                .isTrue();

        //AC 11
        addActionableItemTemplatePage = addActionableItemPage.clickChangeTemplate();
        //AC 12
        assert_().withFailureMessage("No search results expected to be displayed")
                .that(addActionableItemTemplatePage
                        .getSearchMessage())
                .isEmpty();

        //AC 6
        addActionableItemPage = addActionableItemTemplatePage.clickCloseButton();
        BreakLinkToTemplateModalPage breakLinkToTemplateModalPage = addActionableItemPage.clickBreakLinkToTemplate();

        //AC 9
        addActionableItemPage = breakLinkToTemplateModalPage.clickCancelButton();

        assert_().withFailureMessage("The template Name is expected to be displayed")
                .that(addActionableItemPage.getTemplateName())
                .isEqualTo(codicilTemplates
                        .get(0)
                        .getTitle());
        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that(addActionableItemPage.getDescriptionText())
                .isEqualTo(codicilTemplates
                        .get(0)
                        .getDescription());

        assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                .that((addActionableItemPage.isLrAndCustomerButtonEnabled())
                        || (addActionableItemPage.isLrInternalButtonEnabled())
                        || (addActionableItemPage.isAllButtonEnabled()))
                .isFalse();
        if (Integer.parseInt(confidentiality) == 3)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isAllButtonSelected());
        }
        else if (Integer.parseInt(confidentiality) == 2)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isLrInternalButtonSelected());
        }
        else if (Integer.parseInt(confidentiality) == 1)
        {
            assert_().withFailureMessage("The entry in addActionableItemPage is expected to remain the same")
                    .that(addActionableItemPage.isLrAndCustomerButtonSelected());
        }

        assert_().withFailureMessage("The Break Link to Template button is expected to be displayed")
                .that(addActionableItemPage
                        .isBreakLinkToTemplateButtonDisplayed())
                .isTrue();

        assert_().withFailureMessage("The Change Template button is expected to be displayed")
                .that(addActionableItemPage
                        .isChangeTemplateButtonDisplayed())
                .isTrue();

        //AC 10
        assert_().withFailureMessage("The Actionable Item can be saved only when all data are valid")
                .that(addActionableItemPage
                        .isSaveButtonEnabled())
                .isFalse();

        //AC 8, AC 7
        breakLinkToTemplateModalPage = addActionableItemPage.clickBreakLinkToTemplate();
        addActionableItemPage = breakLinkToTemplateModalPage.clickBreakLinkButton();

        assert_().withFailureMessage("The template Name is expected to be displayed")
                .that(addActionableItemPage.getTemplateName())
                .isEmpty();
        assert_().withFailureMessage("The Break Link to Template button is not expected to be displayed")
                .that(addActionableItemPage.isBreakLinkToTemplateButtonDisplayed())
                .isFalse();
        assert_().withFailureMessage("The Change Template button is not expected to be displayed")
                .that(addActionableItemPage.isChangeTemplateButtonDisplayed())
                .isFalse();
        assert_().withFailureMessage("The Title is expected to be editable")
                .that(addActionableItemPage.isTitleEnabled())
                .isTrue();
        assert_().withFailureMessage("The description is expected to be editable")
                .that(addActionableItemPage.isDescriptionEnabled())
                .isTrue();
        assert_().withFailureMessage("The Surveyor guidance is expected to be  editable")
                .that(addActionableItemPage.isSurveyorGuidanceTextEnabled())
                .isTrue();

        //AC 10
        assert_().withFailureMessage("The Actionable Item can be saved only when all data are valid")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isTrue();
    }
}
