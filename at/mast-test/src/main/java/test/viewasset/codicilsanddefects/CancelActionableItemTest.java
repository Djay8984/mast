package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.ViewActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.AddConfirmationWindow;
import viewasset.sub.codicilsanddefects.actionableitems.modal.ConfirmCancelPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.ConfirmDeletePage;
import viewasset.sub.codicilsanddefects.element.DataElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class CancelActionableItemTest extends BaseTest
{

    @Test(description = "US8.10. AC1-8 - Codicil and Defects Tab - As EIC, CFO-3, MMS, MMSO, Class Group - Able to close or cancel an active or inactive Actionable Item LRT-1172")
    @Issue("LRT-1568")
    public void CancelActionableItem()
    {
        int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();

        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();


        AddConfirmationWindow addConfirmationWindow = addActionableItemPage.setTitle(CocData.title)
                .clickClassButton()
                .setDescription(CocData.description)
                .setSurveyorTextInput("Surveyor guidance " + CocData.description)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddConfirmationWindow.class);

        codicilsAndDefectsPage = addConfirmationWindow.clickOkButton();

        //AC 1 AC 5
        CodicilsAndDefectsPage.ActionableItemsTable actionableItemsTable = codicilsAndDefectsPage.getActionableItemsTable();
        List<DataElement> actionableItemsElements = actionableItemsTable.getRows();

        while (actionableItemsTable.isLoadMoreButtonDisplayed())
        {
            actionableItemsTable.clickLoadMoreButton();
        }

        ViewActionableItemPage viewActionableItemPage = actionableItemsElements.stream().filter(e -> e.getStatus().contains("Open"))
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewActionableItemPage.class);

        String actionableItemSelected = viewActionableItemPage.getTitleText();

        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display cancel button ")
                .that(viewActionableItemPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display edit button ")
                .that(viewActionableItemPage.isEditButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display close button ")
                .that(viewActionableItemPage.isCloseButtonDisplayed())
                .isTrue();

        ConfirmDeletePage confirmDeletePage = viewActionableItemPage
                .clickCloseButton(); //Click Delete button - In UI - it shows the name as close but does delete action
        //AC 2
        viewActionableItemPage = confirmDeletePage.clickGoBackButton();

        confirmDeletePage = viewActionableItemPage
                .clickCloseButton(); //Click Delete button - In UI - it shows the name as close but does delete action
        //AC 3
        codicilsAndDefectsPage = confirmDeletePage.clickDeleteButton();

        while (actionableItemsTable.isLoadMoreButtonDisplayed())
        {
            actionableItemsTable.clickLoadMoreButton();
        }
        actionableItemsTable = codicilsAndDefectsPage.getActionableItemsTable();
        actionableItemsElements = actionableItemsTable.getRows();
        // AC 3
        assert_().withFailureMessage("The AI is expected to be deleted")
                .that(actionableItemsElements.stream().filter(e -> e.getItemName().contains(actionableItemSelected)).count())
                .isEqualTo(0);

        //AC 5
        viewActionableItemPage = actionableItemsElements.stream().filter(e -> e.getStatus().contains("Inactive"))
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewActionableItemPage.class);

        String inactiveActionableItem = viewActionableItemPage.getTitleText();

        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display cancel button ")
                .that(viewActionableItemPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display edit button ")
                .that(viewActionableItemPage.isEditButtonDisplayed())
                .isTrue();

        //AC 6
        ConfirmCancelPage confirmCancelPage = viewActionableItemPage.clickCancelButton();

        //AC 8
        viewActionableItemPage = confirmCancelPage.clickGoBackButton();

        //AC 7
        confirmCancelPage = viewActionableItemPage.clickCancelButton();
        codicilsAndDefectsPage = confirmCancelPage.clickCancelButton();

        assert_().withFailureMessage("The AI is expected to be deleted")
                .that(actionableItemsElements.stream().filter(e -> e.getItemName().contains(actionableItemSelected)).count())
                .isEqualTo(0);
    }
}
