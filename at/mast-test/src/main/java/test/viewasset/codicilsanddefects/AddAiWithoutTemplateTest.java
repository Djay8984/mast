package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import model.asset.Asset;
import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.AddConfirmationWindow;
import workhub.WorkHubPage;

import java.sql.Date;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage.Type.ERROR;

public class AddAiWithoutTemplateTest extends BaseTest
{
    final int assetId = 501;
    final String expectedHelperText = "2000 character limit";
    private Asset asset;

    @BeforeMethod(alwaysRun = true)
    private void getData()
    {
        asset = new Asset(assetId);
    }

    @Test(description = "8.4 - AC 1-14 - Codicil and Defect tab - Add new Actionable Item without a template")
    @Issue("LRT- 1314")
    public void addAiWithoutTemplate()
    {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput(String.valueOf(assetId))
                .clickAssetSearchApplyButton();

        //AC: 1 and 2
        //Step: 2 and 3
        AddActionableItemPage addActionableItemPage = workHubPage.clickAllAssetsTab()
                .getAssetCardByAssetName(asset.getDto().getName())
                .clickViewAssetButton()
                .clickCodicilsAndDefectsTab()
                .clickActionableItemAddNewButton();

        //AC: 3
        //Step: 4
        assert_().withFailureMessage("Required approval for change is expected to be unchecked")
                .that(addActionableItemPage.isRequiredApprovalForChangeSelected())
                .isFalse();

        //AC: 8
        //Step: 5
        assert_().withFailureMessage("Description helper text is expected to be present")
                .that(addActionableItemPage.getDescriptionFieldHelperText())
                .isEqualTo(expectedHelperText);
        assert_().withFailureMessage("Surveyor guidance helper text is expected to be present")
                .that(addActionableItemPage.getSurveyorGuidanceFieldHelperText())
                .isEqualTo(expectedHelperText);

        //AC: 9
        //Step: 6
        Dimension beforeResize = addActionableItemPage.getDescriptionFieldDimension();
        addActionableItemPage.setDescriptionTextBoxSizeByOffset(0, 100);
        Dimension afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Description Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        beforeResize = addActionableItemPage.getSurveyorGuidanceFieldDimension();
        addActionableItemPage.setSurveyorGuidanceTextBoxSizeByOffset(0, 150);
        afterResize = addActionableItemPage.getDescriptionFieldDimension();
        assert_().withFailureMessage("Surveyor Guidance Text box is expected to be resized")
                .that(beforeResize.equals(afterResize))
                .isFalse();

        //AC: 3 AC: 4 and AC: 10
        //Step: 7 and 9
        addActionableItemPage.setTitle(LloydsRegister.AiData.title);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickClassCategoryButton();
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDescription(LloydsRegister.AiData.description);
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setImposedDate(String.valueOf(LloydsRegister.FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(1)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.setDueDate(String.valueOf(LloydsRegister.FRONTEND_TIME_FORMAT
                .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Mandatory fields are expected to be filled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isFalse();
        addActionableItemPage.clickAllButton();
        assert_().withFailureMessage("Mandatory fields are filled and save button is expected to be enabled")
                .that(addActionableItemPage.isSaveButtonEnabled())
                .isTrue();

        //AC: 7
        //Step: 8
        addActionableItemPage.setTitle("");
        assert_().withFailureMessage("Title Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isTitleTextBoxErrorIconDisplayed(ERROR))
                .isTrue();
        addActionableItemPage.setDescription("");
        assert_().withFailureMessage("Description Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isDescriptionTextBoxErrorIconDisplayed(ERROR))
                .isTrue();
        addActionableItemPage.setImposedDate("");
        assert_().withFailureMessage("Imposed Date Text Box Error Icon is expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(ERROR))
                .isTrue();

        //AC: 5 and AC: 6
        //Step: 10 and 11
        addActionableItemPage.setTitle(LloydsRegister.AiData.title)
                .setDescription(LloydsRegister.AiData.description)
                .setImposedDate(String.valueOf(LloydsRegister.FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(1)))))
                .setDueDate(String.valueOf(LloydsRegister.FRONTEND_TIME_FORMAT
                        .format(Date.valueOf(LocalDate.now().plusDays(2)))));
        assert_().withFailureMessage("Imposed Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isImposedDateTextBoxErrorIconDisplayed(ERROR))
                .isFalse();
        assert_().withFailureMessage("Due Date Error Icon is not expected to be displayed")
                .that(addActionableItemPage
                        .isDueDateTextBoxErrorIconDisplayed(ERROR))
                .isFalse();

        //AC: 12 and 13
        //STep: 14
        addActionableItemPage.clickStartFromTemplateButton().clickXButton();

        //AC: 11 and AC: 4
        //Step:12 and 13
        CodicilsAndDefectsPage.ActionableItemsTable actionableItemsTable = addActionableItemPage
                .clickSaveButton(AddConfirmationWindow.class)
                .clickOkButton()
                .getActionableItemsTable();

        while (actionableItemsTable.isLoadMoreButtonDisplayed())
        {
            actionableItemsTable.clickLoadMoreButton();
        }
        assert_().withFailureMessage("Added AI is expected to be displayed and status is 'Open' ")
                .that(actionableItemsTable
                        .getRows()
                        .stream()
                        .filter(e -> e.getItemName().contains(LloydsRegister.AiData.title))
                        .findFirst()
                        .get()
                        .getStatus()
                        .equals("Open"))
                .isTrue();
    }
}

