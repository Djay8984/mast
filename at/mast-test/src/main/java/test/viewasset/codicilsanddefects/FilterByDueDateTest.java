package test.viewasset.codicilsanddefects;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.sub.DueDateSubPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;

public class FilterByDueDateTest extends BaseTest
{

    private AssetCoCQuery coc;
    private AssetActionableItemQuery actionableItem;
    private AssetNoteQuery assetNote;
    private AssetDefectQuery assetDefect;

    @BeforeMethod(alwaysRun = true)
    private void getData()
    {
        int assetId = 501;
        List<Long> confidentiality = Arrays.asList(1L, 2L, 3L);
        List<Long> cocCategory = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L, 13L, 14L, 15L);
        List<Long> defectCategory = Arrays.asList(1L, 3L, 4L, 5L, 6L);
        List<Long> actionableItemCategory = Arrays.asList(9L, 10L, 11L);
        List<Long> assetNoteCategory = Arrays.asList(9L, 10L, 11L);
        List<Long> status = Arrays.asList(2L, 3L, 4L, 9L, 10L, 11L, 14L);
        List<Long> defectStatus = Arrays.asList(1L);

        CodicilDefectQueryDto codicilDefectQuery = new CodicilDefectQueryDto();
        codicilDefectQuery.setConfidentialityList(confidentiality);
        codicilDefectQuery.setCategoryList(cocCategory);
        codicilDefectQuery.setStatusList(status);
        codicilDefectQuery.setDueDateMin(Date.valueOf(LocalDate.now()));
        coc = new AssetCoCQuery(assetId, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(actionableItemCategory);
        actionableItem = new AssetActionableItemQuery(assetId, 0, 100, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(assetNoteCategory);
        assetNote = new AssetNoteQuery(assetId, codicilDefectQuery);

        codicilDefectQuery.setCategoryList(defectCategory);
        codicilDefectQuery.setStatusList(defectStatus);
        assetDefect = new AssetDefectQuery(assetId, 0, 100, codicilDefectQuery);
    }

    @Test(description = "US9.40 - codicil search by due date")
    @Issue("LRT-1675")
    public void filterByDueDateTest() throws SQLException
    {

        final int assetId = 501;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();
        String inValidFromDate = "31 Jun 2016";
        String inValidToDate = "29 Feb 2021";
        String nonExistingDateMessage = "Please select an existing date.";
        String inValidDateMessage = "Please select a valid date.";
        String inValidToDateMessage = "'To' date cannot be before 'From' date";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();

        //Step: 2
        workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId))
                .then().clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        CodicilsAndDefectsPage codicilsAndDefectsPage =
                viewAssetPage.clickCodicilsAndDefectsTab();

        //Step: 3
        SearchFilterCodicilsAndDefectsPage searchFilterCodicilsAndDefectsPage =
                codicilsAndDefectsPage.clickSearchFilterCodicilsAndDefectsButton();

        DueDateSubPage dueDateSubPage = searchFilterCodicilsAndDefectsPage.clickDueDateFilterButton();

        //AC: 1 and AC: 2
        //Step: 4, 5, 6 and 7
        dueDateSubPage.setFromDueDate(LocalDate.now().minusDays(1).format(FRONTEND_TIME_FORMAT_DTS))
                .setToDueDate(LocalDate.now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("User is expected to set From Due Date ")
                .that(dueDateSubPage.getFromDueDateText()
                        .equals(String.valueOf(LocalDate.now().minusDays(1).format(FRONTEND_TIME_FORMAT_DTS))))
                .isTrue();

        assert_().withFailureMessage("User is expected to set To Due Date ")
                .that(dueDateSubPage.getToDueDateText()
                        .equals(String.valueOf(LocalDate.now().plusDays(1)
                                .format(FRONTEND_TIME_FORMAT_DTS))))
                .isTrue();

        //Step: 13
        assert_().withFailureMessage("Search button is expected to be clickable")
                .that(searchFilterCodicilsAndDefectsPage.isSearchButtonEnabled()).isTrue();

        //AC: 1 and AC: 3
        //Step: 8
        dueDateSubPage.clearFromDueDate().clearToDueDate()
                .setFromDueDate(inValidFromDate);

        assert_().withFailureMessage("Error Icon is expected to be displayed in From Text Box ")
                .that(dueDateSubPage.isErrorIconDisplayedInFromDateTextBox(DueDateSubPage.type.ERROR))
                .isTrue();
        assert_().withFailureMessage("Error Message is expected to be displayed ")
                .that(dueDateSubPage.getNonExistingDateMessageText().equals(nonExistingDateMessage))
                .isTrue();

        dueDateSubPage.clearFromDueDate().setToDueDate(inValidToDate);
        assert_().withFailureMessage("Error Message is expected to be displayed ")
                .that(dueDateSubPage.isErrorIconDisplayedInToDateTextBox(DueDateSubPage.type.ERROR))
                .isTrue();
        assert_().withFailureMessage("Error Message is expected to be displayed ")
                .that(dueDateSubPage.getNonExistingDateMessageText().equals(nonExistingDateMessage))
                .isTrue();

        //AC: 3
        //Step: 9
        inValidFromDate = "XXXXXXXXX";
        inValidToDate = "YYYYYYYYY";
        dueDateSubPage.clearFromDueDate().clearToDueDate()
                .setFromDueDate(inValidFromDate);
        assert_().withFailureMessage("Error Message is expected to be displayed ")
                .that(dueDateSubPage.getInvalidDateMessageText().equals(inValidDateMessage))
                .isTrue();
        dueDateSubPage.clearFromDueDate().setToDueDate(inValidToDate);
        assert_().withFailureMessage("Error Message is expected to be displayed ")
                .that(dueDateSubPage.getInvalidDateMessageText().equals(inValidDateMessage))
                .isTrue();

        //AC: 2
        //Step: 10, 11
        dueDateSubPage.clearFromDueDate().clearToDueDate()
                .setFromDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS))
                .setToDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS));

        assert_().withFailureMessage("To Date and From Date can be Same date and" +
                " search button is expected to be enabled")
                .that(searchFilterCodicilsAndDefectsPage.isSearchButtonEnabled())
                .isTrue();

        //AC: 2
        //Step: 12
        dueDateSubPage.setFromDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS))
                .setToDueDate(LocalDate.now().minusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("To Date cannot be prior to From Date")
                .that(dueDateSubPage.getInvalidToDateMessageText()
                        .equals(inValidToDateMessage))
                .isTrue();

        //AC: 3 and 4
        //Step: 14
        dueDateSubPage.setFromDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS))
                .setToDueDate(inValidToDate);
        assert_().withFailureMessage("User Cannot perform search when one of To " +
                "or From date field value is invalid ")
                .that(searchFilterCodicilsAndDefectsPage.isSearchButtonEnabled())
                .isFalse();

        //AC: 3 and 4
        //Step: 15
        dueDateSubPage.setFromDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS))
                .setToDueDate(LocalDate.now().plusDays(1).format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("User Can perform search when From and To are valid dates ")
                .that(searchFilterCodicilsAndDefectsPage.isSearchButtonEnabled())
                .isTrue();

        //AC: 5
        //Step: 16
        dueDateSubPage.clearToDueDate().setFromDueDate(LocalDate.now().format(FRONTEND_TIME_FORMAT_DTS));
        codicilsAndDefectsPage = searchFilterCodicilsAndDefectsPage.clickSearchButton();

        //COC
        int cocItemCount = coc.getDto().getContent().size();
        CodicilsAndDefectsPage.CoCTable coCTable = codicilsAndDefectsPage.getCocTable();
        while (coCTable.isLoadMoreButtonDisplayed())
        {
            coCTable.clickLoadMoreButton();
        }
        if (cocItemCount > 0)
        {
            assert_().withFailureMessage("Results returned are expected to match the search criteria ")
                    .that(coCTable.getRows().size())
                    .isEqualTo(cocItemCount);
        }

        //AI
        int actionableItemCount = actionableItem.getDto().getContent().size();
        CodicilsAndDefectsPage.ActionableItemsTable actionableItemsTable
                = codicilsAndDefectsPage.getActionableItemsTable();
        while (actionableItemsTable.isLoadMoreButtonDisplayed())
        {
            actionableItemsTable.clickLoadMoreButton();
        }
        if (actionableItemCount > 0)
        {
            assert_().withFailureMessage("Results returned are expected to match the search criteria ")
                    .that(actionableItemsTable.getRows().size())
                    .isEqualTo(actionableItemCount);
        }

        //AN
        int assetNoteCount = assetNote.getDto().getContent().size();
        CodicilsAndDefectsPage.AssetNotesTable assetNotesTable
                = codicilsAndDefectsPage.getAssetNoteTable();

        while (assetNotesTable.isLoadMoreButtonDisplayed())
        {
            assetNotesTable.clickLoadMoreButton();
        }
        if (assetNoteCount > 0)
        {
            assert_().withFailureMessage("Results returned are expected to match the search criteria ")
                    .that(assetNotesTable.getRows().size())
                    .isEqualTo(assetNoteCount);
        }

        //Defect
        int assetDefectCount = assetDefect.getDto().getContent().size();
        CodicilsAndDefectsPage.DefectsTable defectsTable
                = codicilsAndDefectsPage.getDefectsTable();
        while (defectsTable.isLoadMoreButtonDisplayed())
        {
            defectsTable.clickLoadMoreButton();
        }
        if (assetDefectCount > 0)
        {
            assert_().withFailureMessage("Results returned are expected to match the search criteria ")
                    .that(defectsTable.getRows().size())
                    .isEqualTo(assetDefectCount);
        }
    }
}
