package test.viewasset.codicilsanddefects;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister.CocData;
import model.asset.Asset;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.element.BaseElement;
import viewasset.sub.codicilsanddefects.conditionsofclass.modal.AddConfirmationPage;
import viewasset.sub.codicilsanddefects.element.DataElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static java.time.LocalDate.now;

public class ViewCocTest extends BaseTest
{

    @Test(description = "Automated Test Story 9.52 view the CoC - LRT-1454")
    @Issue("LRT-1579")
    public void viewCoCTest()
    {

        int assetId = 1;
        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        //AC1
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();

        CodicilsAndDefectsPage.CoCTable cocTable = codicilsAndDefectsPage.getCocTable();
        List<DataElement> cocElements = cocTable.getRows();
        ViewConditionOfClassPage viewConditionOfClassPage = cocElements.get(0)
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);
        codicilsAndDefectsPage = viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);

        assert_().withFailureMessage("The conditions of class label should be present")
                .that(codicilsAndDefectsPage.cocLabel())
                .contains("Conditions of Class");

        AddConditionOfClassPage addConditionOfClassPage
                = codicilsAndDefectsPage.clickCocAddNewButton();

        codicilsAndDefectsPage = addConditionOfClassPage.setTitle(CocData.title)
                .setDescription(CocData.description)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddConfirmationPage.class)
                .clickOkButton();

        cocTable = codicilsAndDefectsPage.getCocTable();
        cocElements = cocTable.getRows();
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }

        viewConditionOfClassPage = cocElements.stream().filter(e -> e.getItemName()
                .contains(CocData.title))
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);
        //AC 2
        assert_().withFailureMessage("Inherited Coc Item Details is expected to display cancel button ")
                .that(viewConditionOfClassPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Inherited Coc Item Details is expected to display edit button ")
                .that(viewConditionOfClassPage.isEditButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Inherited Coc Item Details is expected to display close button ")
                .that(viewConditionOfClassPage.isCloseButtonDisplayed())
                .isTrue();

        codicilsAndDefectsPage = viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);

        //AC 2
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }

        viewConditionOfClassPage = cocElements.stream().filter(e -> e.isDefectPresent())
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);

        assert_().withFailureMessage("Defect related Coc Item Details is expected to display cancel button ")
                .that(viewConditionOfClassPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Defect related Coc Item Details is expected to display edit button ")
                .that(viewConditionOfClassPage.isEditButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Defect related Coc Item Details is expected to display close button ")
                .that(viewConditionOfClassPage.isCloseButtonDisplayed())
                .isTrue();

        //AC 5
        assert_().withFailureMessage("Defect related Coc Item Details is expected to display linked Defect Details")
                .that(viewConditionOfClassPage.isLinkedDefectDetailsDisplayed())
                .isTrue();

        codicilsAndDefectsPage = viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);

        //AC 6
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }
        viewConditionOfClassPage = cocElements.stream().filter(e -> e.isRepairPresent())
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);

        assert_().withFailureMessage("Non Defect Coc Item with Repair is expected to display repair Details in the details page")
                .that(viewConditionOfClassPage.isRepairDetailsDisplayed())
                .isTrue();

        //AC 7
        ViewConditionOfClassPage.RepairTable repairTable = viewConditionOfClassPage.getRepairTable();
        List<BaseElement> repairElement = repairTable.getRows();
        for (BaseElement aRepairElement : repairElement)
        {
            assert_().withFailureMessage("The repair element is expected to display Value,Id,Date and ActionTaken")
                    .that((aRepairElement.isActionNameDisplayed())
                            && (aRepairElement.isDateAddedDisplayed())
                            && (aRepairElement.isIdDisplayed())
                            && (aRepairElement.isTitleDisplayed()))
                    .isTrue();

            if (viewConditionOfClassPage.getCocInheritedType().contains("Yes"))
            {
                assert_().withFailureMessage("The repair element is expected to have the Id with prefix Coc-I")
                        .that(aRepairElement.getId().contains("CoC-I-"))
                        .isTrue();
            }
            else //todo clarification - raised
            {
                assert_().withFailureMessage("The repair element is expected to have the Id with prefix Coc-N")
                        .that(aRepairElement.getId().contains("CoC-N-"))
                        .isTrue();
            }
        }

        codicilsAndDefectsPage = viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);

        // AC 3
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }
        viewConditionOfClassPage = cocElements.stream().filter(e -> (!e.isDefectPresent()))
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);

        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display cancel button ")
                .that(viewConditionOfClassPage.isCancelButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display edit button ")
                .that(viewConditionOfClassPage.isEditButtonDisplayed())
                .isTrue();
        assert_().withFailureMessage("Non Defect Coc Item Details is expected to display close button ")
                .that(viewConditionOfClassPage.isCloseButtonDisplayed())
                .isTrue();

        codicilsAndDefectsPage = viewConditionOfClassPage.clickNavigateBackButton(CodicilsAndDefectsPage.class);
        //Ac 4
        while (cocTable.isLoadMoreButtonDisplayed())
        {
            cocTable.clickLoadMoreButton();
        }
        viewConditionOfClassPage = cocElements.stream().filter(e -> e.getStatus().contains("cancel"))
                .findFirst().get()
                .clickFurtherDetailsArrow(ViewConditionOfClassPage.class);

        assert_().withFailureMessage("Coc Item Details in Cancelled Status is expected to display cancel button ")
                .that(viewConditionOfClassPage.isCancelButtonDisplayed())
                .isFalse();
        assert_().withFailureMessage("Coc Item Details in Cancelled Status is expected to display edit button ")
                .that(viewConditionOfClassPage.isEditButtonDisplayed())
                .isFalse();
        assert_().withFailureMessage("Coc Item Details in Cancelled Status is expected to display close button ")
                .that(viewConditionOfClassPage.isCloseButtonDisplayed())
                .isFalse();
    }

}
