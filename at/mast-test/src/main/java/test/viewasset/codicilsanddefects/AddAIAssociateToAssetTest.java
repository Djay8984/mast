package test.viewasset.codicilsanddefects;

import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.Asset;
import model.asset.AssetModel;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.select.SelectAssetItemPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class AddAIAssociateToAssetTest extends BaseTest
{

    @Test(description = "US 10.22 - 8.5 - AC 1-14 - From the Job Specific AI page -" +
            " Add a New Actionable Item without a template - associate to an Asset or Item")
    @Issue("LRT-2334")
    public void AddAIAssociateToAssetTest()
    {

        String changeSelectionText = "Change selection";
        int assetId = 501;
        final String title = "Auto KL" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        String description = "Auto KL Test Description" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssets = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(name).then().clickAssetSearchApplyButton();
        ViewAssetPage viewAsset = allAssets.getAssetCardByAssetName(name)
                .clickViewAssetButton();

        //AC1
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAsset.clickCodicilsAndDefectsTab();
        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();

        SelectAssetItemPage selectAssetItemPage = addActionableItemPage.clickSelectItemOrChangeSelectionButton();

        AssetModel assetModel = new AssetModel(assetId);
        List<ItemDto> assetItemsBE = assetModel.getDto().getItems().get(0)
                .getItems();

        SelectAssetItemPage.ItemTable itemTable
                = selectAssetItemPage.getItemTable();
        //AC 2
        assert_().withFailureMessage("The asset model is displayed for the current asset")
                .that(itemTable.getRows()
                        .get(0)
                        .getChildren()
                        .size())
                .isEqualTo(assetItemsBE.size());

        assert_().withFailureMessage("The items are displayed in hierarchical view and can be expanded to view the child")
                .that(itemTable.getRows()
                        .get(0)
                        .getChildren()
                        .get(0)
                        .clickPlusOrMinusIcon()).isNotNull();

        //AC 3
        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(4).clickPlusOrMinusIcon();

        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(4).getChildren().get(0).clickPlusOrMinusIcon();

        //AC 4
        for (int i = 0; i < itemTable.getRows().get(0).getChildren().size(); i++)
        {
            if (itemTable.getRows()
                    .get(0)
                    .getChildren()
                    .get(i)
                    .getItemName()
                    .length() >= 40)
            {
                assert_().withFailureMessage("The characters above the length of 40 are expected to be replaced by ...")
                        .that(itemTable.getRows()
                                .get(0)
                                .getChildren()
                                .get(i)
                                .getItemName()
                                .substring(40, 42))
                        .isEqualTo("...");
            }
        }

        //AC 6
        int noOfSelectedItem = 0;
        for (int i = 0; i < itemTable.getRows().get(0).getChildren().size(); i++)
        {
            if (itemTable.getRows().get(0).getChildren().get(i).isSelectRadioBoxSelected())
            {
                noOfSelectedItem++;
            }
        }

        assert_().withFailureMessage("No item is expected to be selected by default")
                .that(noOfSelectedItem).isEqualTo(0);
        //AC 9
        assert_().withFailureMessage("Assign selected button is expected to be disabled")
                .that(selectAssetItemPage.isAssignSelectedButtonEnabled()).isFalse();

        //AC 5
        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickSelectRadioBox();
        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(1)
                .clickSelectRadioBox();

        for (int i = 0; i < itemTable.getRows().get(0).getChildren().size(); i++)
        {
            if (itemTable.getRows().get(0).getChildren().get(i).isSelectRadioBoxSelected())
            {
                noOfSelectedItem++;
            }
        }

        assert_().withFailureMessage("Only one item can be selected")
                .that(noOfSelectedItem)
                .isEqualTo(1);

        //AC 10
        assert_().withFailureMessage("Assign selected button is expected to be disabled")
                .that(selectAssetItemPage.isAssignSelectedButtonEnabled()
                ).isTrue();

        addActionableItemPage = selectAssetItemPage.clickAssignSelectedButton();

        String itemSelected = addActionableItemPage.getSelectedItemText();
        assert_().withFailureMessage("The selected item is expected to be displayed")
                .that(itemSelected)
                .isNotNull();

        //AC 11
        assert_().withFailureMessage("The Change selection option is expected to be displayed")
                .that(addActionableItemPage.getSelectItemOrChangeSelectionButtonText())
                .contains(changeSelectionText);

        //AC 12
        selectAssetItemPage = addActionableItemPage.clickSelectItemOrChangeSelectionButton();
        itemTable.getRows()
                .get(0)
                .getChildren()
                .get(2)
                .clickSelectRadioBox();

        //AC 13
        addActionableItemPage = selectAssetItemPage.clickCancelButton();

        //AC 14
        assert_().withFailureMessage("The selected item is expected to be displayed and not changed")
                .that(addActionableItemPage.getSelectedItemText())
                .isEqualTo(itemSelected);

        //AC 7,8 - No search option present in the page - raised clarification

    }
}
