package test.viewasset;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.DatabaseHelper;
import model.asset.Asset;
import model.asset.AssetQuery;
import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.Step;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static junit.framework.Assert.assertEquals;

public class AssetModelListViewTest extends BaseTest
{

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {428}, {501}, {502}
        };
    }

    @Test(
            description = "Story - 8.16 - Viewing an asset model in the list view.",
            dataProvider = "asset"
    )
    @Issue("LRT-1215")
    public void assetModelListViewTest(int assetId) throws SQLException
    {
        // Test step 1
        WorkHubPage workHubPage = WorkHubPage.open();

        // Test step 2
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        Asset searchableAsset = new Asset(assetId);

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        Long imoNumber = searchableAsset.getDto().getIhsAsset().getId();
        String name = searchableAsset.getDto().getName();

        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setImoNumber(Arrays.asList(imoNumber));

        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        searchFilterSubPage.setAssetSearchInput(name);
        searchFilterSubPage.clickAssetSearchApplyButton(WorkHubPage.class);
        assertThat(allAssetsSubPage.getAssetCardCount())
                .isEqualTo(assetQuery.getDto()
                        .getContent().stream()
                        .filter(e -> e.getId().equals(Long.valueOf(assetId)))
                        .collect(Collectors.toList())
                        .size());
        assertThat(allAssetsSubPage.getAssetCardByIndex(0).getImoNumber()).isEqualTo(imoNumber.toString());

        // Test Step 7 & 9 Verify page loads correctly with required elements visible
        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByIndex(0)
                .clickViewAssetButton();
        ListViewSubPage listViewSubPage = viewAssetPage.getAssetModelSubPage().getListViewSubPage();

        // Verify step 3, Hierarchical View button, step 5, Search within asset model button
        AssetModelSubPage assetModelSubPage = viewAssetPage.getAssetModelSubPage();

        String breadcrumb = listViewSubPage.getBreadcrumbTextByIndex(0);
        String expectedBreadcrumb = getExpectedBreadcrumb(assetId);

        // Test Step 4 verify displayed breadcrumbs
        assertEquals("Breadcrumb does not match expected breadcrumb.", expectedBreadcrumb, breadcrumb);

        // AC4 system default view is list view
        listViewSubPage.isListViewPageDisplayed();

        // Test Step 6, AC1
        verifyAssetItems(assetId, listViewSubPage);

        // Test Step 8, Verify the review complete column is correctly populated
        verifyReviewCompleteValues(assetId, listViewSubPage);

        // Test Step 11, verify child page displayed on clicking item name
        ListViewItemElement element = listViewSubPage.getItemByIndex(1);
        ListViewSubPage childPage = element.clickItemName();
    }

    @Step("Verify Review complete figures for each item")
    public void verifyReviewCompleteValues(int assetId, ListViewSubPage listViewSubPage)
    {
        for (ListViewItemElement element : listViewSubPage.getAssetItems())
        {
            String itemName = element.getItemName();
            ResultSet results = getChildItems(itemName, assetId);
            int countChildItems = 0;
            int countCompleteItems = 0;
            try
            {
                while (results.next())
                {
                    countChildItems++;
                    if (results.getInt("reviewed") != 0)
                    {
                        countCompleteItems++;
                    }
                }
            }
            catch (SQLException sqle)
            {
                Assert.fail("SQL Exception thrown whilst attempting to verify Review Complete values." + sqle.toString());
            }
            String expectedReviewCompleteValues = countCompleteItems + "/" + countChildItems;
            String actualReviewCompleteValues = element.getReviewCompleteValues();
            String actualReviewInProgressValues = element.getReviewInProgressValues();

            assertEquals("Expected Review Complete values do not match actual values.",
                    expectedReviewCompleteValues, actualReviewInProgressValues + actualReviewCompleteValues);
        }
    }

    @Step("Verify item names listed in list view")
    public void verifyAssetItems(int assetId, ListViewSubPage listViewSubPage) throws SQLException
    {
        HashSet<String> itemNames = new HashSet<>();
        for (ListViewItemElement element : listViewSubPage.getAssetItems())
        {
            itemNames.add(element.getItemName());
        }
        HashSet<String> expectedItemNames = getExpectedItemNames(assetId);
        for (String name : expectedItemNames)
        {
            if (itemNames.add(name))
            {
                Assert.fail("Expected item name " + name + " not found on page.");
            }
        }
    }

    private ResultSet getChildItems(String itemName, int assetId)
    {
        String sqlString = "select air.to_item_id as itemId, ai.reviewed as reviewed from " +
                "mast_asset_assetitemrelationship air, mast_asset_versionedassetitem ai " +
                "where air.from_item_id = ai.id and ai.name = '" + itemName + "' and ai.asset_id = " +
                assetId + ";";
        return DatabaseHelper.executeQuery(sqlString);
    }

    private String getExpectedBreadcrumb(int assetId) throws SQLException
    {
        String sqlString = "select ai.name as name from mast_asset_versionedasset a, mast_asset_versionedassetitem ai" +
                " where a.asset_item_id = ai.id and a.id = " + assetId + ";";
        ResultSet results = DatabaseHelper.executeQuery(sqlString);
        results.first();
        String breadcrumb = results.getString("name");
        return breadcrumb;
    }

    private HashSet<String> getExpectedItemNames(int assetId) throws SQLException
    {
        String sqlString = "SELECT name FROM mast_asset_versionedassetitem where id in" +
                "(SELECT air.to_item_id FROM mast_asset_versionedasset a, mast_asset_assetitemrelationship air" +
                " where air.from_item_id = a.asset_item_id and a.id = " + assetId + ");";
        ResultSet resultset = DatabaseHelper.executeQuery(sqlString);
        HashSet<String> results = new HashSet<>();
        while (resultset.next())
        {
            results.add(resultset.getString("name"));
        }
        return results;
    }
}
