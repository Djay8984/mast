package test.viewasset;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import org.testng.annotations.BeforeMethod;

public class DeleteNotesAttachmentCreateAssetTest extends BaseTest
{

    private Asset asset;

    @BeforeMethod(alwaysRun = true)
    public void getAsset()
    {
        asset = new Asset(501);
    }

/*    @Test(description = "US 9.34 - 9.5 AC:1-4 - Delete Note on Create Asset - IMO Registered Asset")
    @Issue("LRT-1961")
    public void deleteNotesOnCreateAssetIMORegisteredAssetTest() {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        IMORegisteredAssetPage iMORegisteredAssetPage =
                addNewAssetPage.clickImoRegisteredAssetRadioButton();

        String imoNumber = asset.getDto().getIhsAsset().getId().toString();

        //Step: 3
        iMORegisteredAssetPage.setIMONumber(imoNumber).then().clickLookUpIhsDataButton();

        assert_().withFailureMessage("Error Icon is Note Displayed")
                .that(iMORegisteredAssetPage.isErrorIconDisplayed(IMORegisteredAssetPage.type.ERROR))
                .isFalse();

        assert_().withFailureMessage("Add Attachments Button is Clickable")
                .that(iMORegisteredAssetPage.isAddAttachmentsButtonClickable())
                .isTrue();

        AttachmentsAndNotesPage attachmentsAndNotesPage =
                iMORegisteredAssetPage.clickAddAttachmentsButton();

        *//*
        // This is part of Pre-requisite.
        //If Note is not present then add a Note
         *//*
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (noteSubPage.getDisplayedNoteCount() == 0) {
            String randomNote = RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().selectSeenByValue("All")
                    .then().clickAddNoteButton();

            assert_().withFailureMessage("Added Note is Present")
                    .that(noteSubPage.isNoteByNoteTextDisplayed(randomNote))
                    .isTrue();
        }

        //AC: 1
        //Step: 4
        String noteText = noteSubPage.getNoteTextByIndex(0);
        noteSubPage.clickOnNoteByNoteText(noteText);

        assert_().withFailureMessage("Expand Layout Is Displayed")
                .that(noteSubPage.isNoteExpandLayoutDisplayed())
                .isTrue();
        assert_().withFailureMessage("Edit Note Icon is Displayed")
                .that(noteSubPage.isEditNoteIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is Displayed")
                .that(noteSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 5
        noteSubPage.clickDeleteButton();
        assert_().withFailureMessage("Delete ConfirmationModalPage overlay is Displayed")
                .that(noteSubPage.isDeleteConfirmationOverlayDisplayed())
                .isTrue();

        //AC: 4
        //Step: 6
        noteSubPage.clickCancelNoteConfirmationButton();
        assert_().withFailureMessage("Note is present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 3
        //Step: 7
        if (!noteSubPage.isDeleteIconDisplayed()) {
            noteSubPage.clickOnNoteByNoteText(noteText);
        }
        noteSubPage.clickDeleteButton().then()
                .clickDeleteButton();
        assert_().withFailureMessage("Note is not present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //AC: 3
        //Step: 8
        //Cannot close and Reopen the MAST App. So The page to be refreshed
        noteSubPage.refreshPage();
        iMORegisteredAssetPage.clickAddAttachmentsButton();
        assert_().withFailureMessage("Note is not present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();
    }*/

   /* @Test(description = "US 9.34 - 9.5 AC:1-4 - Delete Note on Create Asset - Non IMO Registered Asset")
    @Issue("LRT-1964")
    public void deleteNotesOnCreateAssetNonIMORegisteredAssetTest() {

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        workHubPage.clickAllAssetsTab();

        //Step: 2
        AddNewAssetPage addNewAssetPage = workHubPage.clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage =
                addNewAssetPage.clickNonImoRegisteredAssetRadioButton();

        //Step: 3
        nonIMORegisteredAssetPage.setBuilderName(asset.getDto().getBuilder())
                .then().setYardNumber(asset.getDto().getYardNumber())
                .then().setAssetName(asset.getDto().getName())
                .then().clickAssetCategoryButton()
                .then().selectAssetCategoryByName("ANY")
                .then().setDateOfBuild(FRONTEND_TIME_FORMAT.format(asset.getDto().getBuildDate()));

        assert_().withFailureMessage("Add Attachments Button is Clickable")
                .that(nonIMORegisteredAssetPage.isAddAttachmentsButtonClickable())
                .isTrue();

        AttachmentsAndNotesPage attachmentsAndNotesPage =
                nonIMORegisteredAssetPage.clickAddAttachmentsButton();

        *//*
        // This is part of Pre-requisite.
        //If Note is not present then add a Note
         *//*
        NoteSubPage noteSubPage = attachmentsAndNotesPage.clickAddNoteButton();
        if (noteSubPage.getDisplayedNoteCount() == 0) {
            String randomNote = RandomStringUtils.randomAlphabetic(10);
            noteSubPage.setNoteTextBox(randomNote)
                    .then().selectSeenByValue("All")
                    .then().clickAddNoteButton();

            assert_().withFailureMessage("Added Note is Present")
                    .that(noteSubPage.isNoteByNoteTextDisplayed(randomNote))
                    .isTrue();
        }

        //AC: 1
        //Step: 4
        String noteText = noteSubPage.getNoteTextByIndex(0);
        noteSubPage.clickOnNoteByNoteText(noteText);

        assert_().withFailureMessage("Expand Layout Is Displayed")
                .that(noteSubPage.isNoteExpandLayoutDisplayed())
                .isTrue();
        assert_().withFailureMessage("Edit Note Icon is Displayed")
                .that(noteSubPage.isEditNoteIconDisplayed())
                .isTrue();
        assert_().withFailureMessage("Delete Note Icon is Displayed")
                .that(noteSubPage.isDeleteIconDisplayed())
                .isTrue();

        //AC: 2
        //Step: 5
        noteSubPage.clickDeleteButton();
        assert_().withFailureMessage("Delete ConfirmationModalPage overlay is Displayed")
                .that(noteSubPage.isDeleteConfirmationOverlayDisplayed())
                .isTrue();

        //AC: 4
        //Step: 6
        noteSubPage.clickCancelNoteConfirmationButton();
        assert_().withFailureMessage("Note is present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isTrue();

        //AC: 3
        //Step: 7
        if (!noteSubPage.isDeleteIconDisplayed()) {
            noteSubPage.clickOnNoteByNoteText(noteText);
        }
        noteSubPage.clickDeleteButton().then()
                .clickDeleteButton();
        assert_().withFailureMessage("Note is not present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();

        //AC: 3
        //Step: 8
        //Cannot close and Reopen the MAST App. So The page to be refreshed
        noteSubPage.refreshPage();
        nonIMORegisteredAssetPage.clickAddAttachmentsButton();
        assert_().withFailureMessage("Note is not present")
                .that(noteSubPage.isNoteByNoteTextDisplayed(noteText))
                .isFalse();
    }*/

}
