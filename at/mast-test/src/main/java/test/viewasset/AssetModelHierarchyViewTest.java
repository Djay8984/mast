package test.viewasset;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.AssetItemQuery;
import model.asset.AssetRootItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AssetModelHierarchyViewTest extends BaseTest
{

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {501}, {502}, {503}
        };
    }

    @Test(
            description = "Automation - Story - 8.18 - As a user I want to view the asset model " +
                    "in a hierarchy view so that I can ensure that the model has been set up" +
                    "with the correct parent-child relationships",
            dataProvider = "asset"
    )
    @Issue("LRT-1229")
    public void assetModelHierarchyViewTest(int assetId)
    {
        LazyItemDto rootItem = new AssetRootItem(assetId).getDto();
        List<Long> assetItems = rootItem.getItems()
                .stream()
                .map(LinkVersionedResource::getId)
                .collect(Collectors.toList());

        ItemQueryDto query = new ItemQueryDto();
        query.setItemId(assetItems);
        List<LazyItemDto> assetModel = new AssetItemQuery(query, assetId).getDto().getContent();

        List<Long> childAssetItems = assetModel.get(1).getItems()
                .stream()
                .map(LinkVersionedResource::getId)
                .collect(Collectors.toList());

        query = new ItemQueryDto();
        query.setItemId(childAssetItems);
        List<LazyItemDto> childAssetModel = new AssetItemQuery(query, assetId).getDto().getContent();

        LazyItemDto childItem = childAssetModel
                .stream()
                .filter(child -> child.getItems().size() == 0)
                .findFirst()
                .get();

        String childName = childItem.getName();

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(String.valueOf(assetId)).clickAssetSearchApplyButton();

        //AC 1
        HierarchyViewSubPage hierarchyView = allAssetsSubPage
                .getAssetCardByIndex(0)
                .clickViewAssetButton()
                .getAssetModelSubPage()
                .clickHierarchyViewButton();

        assert_().withFailureMessage("The hierarchy view cannot be seen")
                .that(hierarchyView.isHierarchyViewPageDisplayed()).isTrue();

        //AC2
        assert_().withFailureMessage("Incorrect root item is shown")
                .that(hierarchyView.getRootItemText())
                .isEqualTo(rootItem.getName());

        int i = 0;
        for (LazyItemDto item : assetModel)
        {
            assert_().withFailureMessage("Item name in the frontend does not match backend")
                    .that(item.getName())
                    .isEqualTo(hierarchyView.getAssetItems().get(i).getItemName());
            i++;
        }

        //AC4, AC5
        String assetName = assetModel.get(1).getName();
        hierarchyView.getItemBasedOnItemName(assetName).clickCollapseIcon();

        assert_().withFailureMessage("The parent item should of expanded")
                .that(hierarchyView.getItemBasedOnItemName(assetName).isItemExpanded())
                .isTrue();

        hierarchyView.getItemBasedOnItemName(childName).clickCollapseIcon();
        assert_().withFailureMessage("This should be a child item and not be expandable")
                .that(hierarchyView.getItemBasedOnItemName(childName).isItemExpanded())
                .isFalse();

        i = hierarchyView.getItemNamePosition(childName);
        for (LazyItemDto item : childAssetModel)
        {
            assert_().withFailureMessage("item name in the frontend does not match backend")
                    .that(item.getName())
                    .isEqualTo(hierarchyView.getAssetItems().get(i).getItemName());
            i++;
        }

        //AC6
        assert_().withFailureMessage("The child item should not be expandable")
                .that(hierarchyView.getItemBasedOnItemName(childName).isItemExpandable())
                .isFalse();

        //AC7
        assert_().withFailureMessage("When hovering over the item attribute link, it does not say 'open'")
                .that(hierarchyView.getItemBasedOnItemName(childName).hoverOverItemAttributeLink())
                .isTrue();

        hierarchyView.getItemBasedOnItemName(childName).clickItemAttributesLink();
    }
}
