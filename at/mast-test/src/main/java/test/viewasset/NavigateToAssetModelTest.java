package test.viewasset;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import workhub.WorkHubPage;
import workhub.element.MyWorkAssetElement;
import workhub.sub.AllAssetsSubPage;

import static com.google.common.truth.Truth.assert_;

public class NavigateToAssetModelTest extends BaseTest
{

    @Test(
            description = "Story - 8.15 - As a user I want to navigate to the" +
                    " asset model of a specific asset so that I can take appropriate action"

    )
    @Issue("LRT-1209")
    public void navigateToAssetModelTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        MyWorkAssetElement myWorkAssetElement = workHubPage.clickMyWorkTab().then()
                .getAssetCardByIndex(0);

        //AC1. AC3
        ViewAssetPage viewAssetPage = myWorkAssetElement
                .clickViewAssetButton();
        assert_().withFailureMessage("List view button is selected")
                .that(viewAssetPage.getAssetModelSubPage().isListViewButtonSelected())
                .isTrue();
        assert_().withFailureMessage("List View Page is active by default")
                .that(viewAssetPage.getAssetModelSubPage().getListViewSubPage()
                        .isListViewPageDisplayed())
                .isTrue();

        //AC1, AC3
        AllAssetsSubPage allAssetsSubPage = workHubPage.getHeader()
                .clickMastLogo().then()
                .clickAllAssetsTab();
        viewAssetPage = allAssetsSubPage
                .getAssetCardByIndex(0)
                .clickViewAssetButton();
        assert_().withFailureMessage("List view button is selected")
                .that(viewAssetPage.getAssetModelSubPage().isListViewButtonSelected())
                .isTrue();
        assert_().withFailureMessage("List View Page is active by default")
                .that(viewAssetPage.getAssetModelSubPage().getListViewSubPage()
                        .isListViewPageDisplayed())
                .isTrue();

        //AC2
        viewAssetPage.clickAssetModelTab();
        assert_().withFailureMessage("Asset Model Tab is selected")
                .that(viewAssetPage.isAssetModelTabSelected())
                .isTrue();
        viewAssetPage.clickServiceScheduleTab();
        assert_().withFailureMessage("Service Schedule Tab is selected")
                .that(viewAssetPage.isServiceScheduleTab())
                .isTrue();
        viewAssetPage.clickCasesTab();
        assert_().withFailureMessage("Cases Tab is selected")
                .that(viewAssetPage.isCasesTabSelected())
                .isTrue();
        viewAssetPage.clickJobsTab();
        assert_().withFailureMessage("Jobs Tab is selected")
                .that(viewAssetPage.isJobsTabSelected())
                .isTrue();
        viewAssetPage.clickCodicilsAndDefectsTab();
        assert_().withFailureMessage("Codicils and Defects Tab is selected")
                .that(viewAssetPage.isCodicilsAndDefectsTab())
                .isTrue();
        viewAssetPage.clickAssetModelTab();
        assert_().withFailureMessage("Asset Model Tab is selected")
                .that(viewAssetPage.isAssetModelTabSelected())
                .isTrue();
    }
}

