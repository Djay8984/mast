package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import header.HeaderPage;
import helper.DatabaseHelper;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetItemQuery;
import model.asset.AssetRootItem;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.relationship.AddRelatedItemPage;
import viewasset.item.relationship.RelationshipsSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class CreateItemRelationshipTest extends BaseTest
{

    private HeaderPage headerPage;

    @DataProvider(name = "item")
    public Object[][] item()
    {
        return new Object[][]{
                {501, 688}
        };
    }

    @Test(description = "Story 8.29 - As a user I want to create new lateral relationship "
            + "so that I can define the related items",
            dataProvider = "item")
    @Issue("LRT-1326")
    public void CreateItemRelationshipTest(int assetId, int itemId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        BuildAssetModelPage buildAssetModelPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton().then().clickAssetModelTab().clickCheckOutForEditingButton();

        ItemsDetailsPage itemsDetailsPage = buildAssetModelPage.getBuildAssetModelTable().getRootItem().clickFurtherDetailsArrow();
        RelationshipsSubPage relationshipsSubPage = itemsDetailsPage.clickRelationshipsTab();

        List<String> RelatedItemNamesBeforeClickingCreateNewRelationshipButton = relationshipsSubPage.getAllDisplayedRelatedItemElementNames();

        //AC1
        assert_().withFailureMessage("Create New Relationship button is expected to be enabled.")
                .that(relationshipsSubPage.isCreateNewRelationshipButtonEnabled())
                .isTrue();

        AddRelatedItemPage addRelatedItemPage = relationshipsSubPage.clickCreateNewRelationshipButton();

        //AC14 15 click close without choosing anything
        itemsDetailsPage = addRelatedItemPage.clickCloseButton();
        assert_().withFailureMessage("No related item is expected to be added without selecting any radio button of item in Add Related Item Page")
                .that(RelatedItemNamesBeforeClickingCreateNewRelationshipButton)
                .containsExactlyElementsIn(itemsDetailsPage.clickRelationshipsTab().getAllDisplayedRelatedItemElementNames());

        addRelatedItemPage = relationshipsSubPage.clickCreateNewRelationshipButton();

        LazyItemDto rootItemDto = new AssetRootItem(assetId).getDto();
        List<Long> assetItemIds = rootItemDto.getItems()
                .stream()
                .map(BaseDto::getId)
                .collect(Collectors.toList());

        List<String> assetItemNames = rootItemDto.getItems()
                .stream()
                .map(x -> getItemNameById(assetId, x.getId().intValue()))
                .collect(Collectors.toList());

        ItemQueryDto query = new ItemQueryDto();
        query.setItemId(assetItemIds);
        List<LazyItemDto> assetModel = new AssetItemQuery(query, assetId).getDto().getContent();

        assert_().withFailureMessage("Asset item name for asset id [" + assetId + "]is expected to be the same from backend.")
                .that(addRelatedItemPage.getRootItemName())
                .isEqualTo(rootItemDto.getName());

        if (rootItemDto.getItems().size() > 0)
        {
            assert_().withFailureMessage("Asset item is expected to be expanded by default")
                    .that(addRelatedItemPage.isRootItemExpandable())
                    .isFalse();
        }

        //AC3 first level children is expected to be displayed
        assert_().withFailureMessage("First level child is expected to be display by default.")
                .that(addRelatedItemPage.getAllRelatedItemOptionElementNames())
                .containsExactlyElementsIn(assetItemNames);

        final AddRelatedItemPage addRelatedItemPage1 = addRelatedItemPage;

        for (int i = 0; i < 2; i++)
        {
            if (assetModel.get(i).getItems().size() > 0)
            {
                //AC4 check first level child item name
                assert_().withFailureMessage("Item which has child item(s) isn't expandable.")
                        .that(addRelatedItemPage1.getItemBasedOnItemName(getItemNameById(assetId, assetModel.get(i).getId().intValue()))
                                .isItemExpandable())
                        .isTrue();

                addRelatedItemPage1.getItemBasedOnItemName(getItemNameById(assetId, assetModel.get(i).getId().intValue())).clickItemNode();

                //iterate all child items
                assetModel.get(i).getItems().forEach(
                        itemLR -> {
                            //AC7 no item is selected by default
                            assert_().withFailureMessage(
                                    "Item [" + getItemNameById(assetId, itemLR.getId().intValue()) + "] is not selected by default.")
                                    .that(addRelatedItemPage1.getItemBasedOnItemName(getItemNameById(assetId, itemLR.getId().intValue()))
                                            .isItemRadioButtonSelected())
                                    .isFalse();
                        }
                );

                //iterate related item  verify if the item related is disabled
                if (assetModel.get(i).getId() == itemId)
                {
                    assetModel.get(i).getRelated().forEach(relatedItemLR -> {

                                AssetItem assetItem = new AssetItem(assetId, relatedItemLR.getId().intValue());

                                //AC8 AC9
                                assert_().withFailureMessage("Related item [" + assetItem.getDto().getName() + "] is expected to be disabled.")
                                        .that(addRelatedItemPage1.getItemBasedOnItemName(assetItem.getDto().getName()).isItemRadioButtonEnabled())
                                        .isFalse();
                                //AC10 relationship ID 9 - is associated with
                                if (assetItem.getDto().getItemType().getId().equals(LloydsRegister.ASSET_ITEM_RELATIONSHIP_TYPE_ID_IS_RELATED_TO))
                                {
                                    assert_().withFailureMessage(
                                            "Item [" + assetItem.getDto().getName() + "] which is associated with is expected to be disabled.")
                                            .that(addRelatedItemPage1.getItemBasedOnItemName(assetItem.getDto().getName()).isItemRadioButtonEnabled())
                                            .isFalse();
                                }
                            }
                    );
                }
            }
            else
            {
                //AC5
                assert_().withFailureMessage("Item which does not has child item will not be able to expand and plus sign not displayed.")
                        .that(addRelatedItemPage1.getItemBasedOnItemName(getItemNameById(assetId, assetModel.get(i).getId().intValue()))
                                .isItemExpandable())
                        .isFalse();
            }
        }

        List<String> allItemNames = addRelatedItemPage1.getAllRelatedItemOptionElementNames();
        String selectAnItem = allItemNames.stream().filter(a -> addRelatedItemPage1.getItemBasedOnItemName(a).isItemRadioButtonEnabled()).findFirst()
                .get();
        addRelatedItemPage1.getItemBasedOnItemName(selectAnItem).selectItemRadioButton();

        relationshipsSubPage = addRelatedItemPage1.clickCreateRelationshipButton();

        //AC13 navigated back to the relationship tab newly created item now added as a related item with the current item
        assert_().withFailureMessage("Newly created related item is expected to display")
                .that(relationshipsSubPage.getAllDisplayedRelatedItemElementNames().contains(selectAnItem))
                .isTrue();
        query.setItemId(Collections.singletonList((long) itemId));

        //AC12  By default any relationship created between the items from the relationship tab will be ‘is associated with’ relationship
        assert_().withFailureMessage("Newly created related item is mark as [is associated with] relationship")
                .that(getRelationShipTypeId(assetId, itemId))
                .isEqualTo(LloydsRegister.ASSET_ITEM_RELATIONSHIP_TYPE_ID_IS_RELATED_TO);
    }

    private String getItemNameById(int assetId, int itemId)
    {
        return new AssetItem(assetId, itemId).getDto().getName();
    }

    private Integer getRelationShipTypeId(int assetId, int itemId)
    {
        ResultSet rs = DatabaseHelper.executeQuery("SELECT relationship_type_id FROM mast_asset_assetitemrelationship WHERE asset_id = " + assetId
                + " AND from_item_id = " + itemId);
        try
        {
            rs.first();
            Integer getTypeId = (rs.getInt("relationship_type_id"));
            return getTypeId;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @AfterTest(alwaysRun = true)
    public void discard()
    {
        headerPage.discardCheckoutForAllAssets();
    }

}
