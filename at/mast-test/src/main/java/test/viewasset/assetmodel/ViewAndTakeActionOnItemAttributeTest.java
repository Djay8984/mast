package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetItemAttributePage;
import model.referencedata.ReferenceDataMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.sub.AssetModelSubPage;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.ATTRIBUTE_TYPES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class ViewAndTakeActionOnItemAttributeTest extends BaseTest
{
    private HeaderPage headerPage;

    @DataProvider(name = "itemWithAttribute")
    public Object[][] itemWithAttribute()
    {
        return new Object[][]{
                {501, 688}
        };
    }

    @Test(description = "Story 8.23 User is able to view attributes of an item and the actions taken against the attributes and its values.",
            dataProvider = "itemWithAttribute")
    @Issue("LRT-1225")
    public void ViewAndTakeActionOnItemWithAttribute(int assetId, int itemId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton()
                .then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton()
                .clickAssetModelTab();

        String itemName = new AssetItem(assetId, itemId).getDto().getName();
        ItemsDetailsPage itemsDetailsPage = assetModelSubPage
                .clickCheckOutForEditingButton()
                .getItemByItemName(itemName)
                .clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = itemsDetailsPage.clickAttributesTab();

        List<String> allDisplayedAttributeNames = attributesSubPage.getAllDisplayedAttributeElementNames();
        List<AttributeDto> attributeDtos = new AssetItemAttributePage(assetId, itemId).getDto().getContent();
        List<AttributeTypeDto> attributeTypeDtosOrderByDisplayOrder = getAttributeTypeDtoById(attributeDtos)
                .stream()
                .sorted((x1, x2) -> x1.getDisplayOrder().compareTo(x2.getDisplayOrder()))
                .collect(Collectors.toList());

        // AC2
        assert_().withFailureMessage("Number of all displayed attributes and backend are tally for item [" + itemName + "]")
                .that(allDisplayedAttributeNames.size())
                .isEqualTo(attributeDtos.size());

        assert_().withFailureMessage("Number of attribute in label Showing count of attributes is correct.")
                .that(attributesSubPage.getNumberOfAttribute())
                .isEqualTo(attributeDtos.size());

        // AC1 , AC5, AC6
        assert_().withFailureMessage("All attributes that relate to item [" + itemName + "] are present")
                .that(allDisplayedAttributeNames)
                .containsExactlyElementsIn(attributeTypeDtosOrderByDisplayOrder.stream().map(ReferenceDataDto::getName).collect(Collectors.toList()));

        // AC3, AC4
        assert_().withFailureMessage("Column Header Attribute name is displayed")
                .that(attributesSubPage.getAttributeNameColumnHeaderText())
                .isEqualTo("Attribute name");

        assert_().withFailureMessage("Column Header Value is displayed")
                .that(attributesSubPage.getValueColumnHeaderText())
                .isEqualTo("Value");

        assert_().withFailureMessage("Column Header Decorator is displayed")
                .that(attributesSubPage.getDecoratorColumnHeaderText())
                .isEqualTo("Decorator");

        if (attributesSubPage.getAllDisplayedAttributeElement().size() > 0 &&
                !attributesSubPage.getAllDisplayedAttributeElement().get(0).isMandatoryTextDisplayInLastColumnContainer())
        {
            assert_().withFailureMessage("Remove Attribute Link is displayed")
                    .that(attributesSubPage.getAllDisplayedAttributeElement().get(0).isDeleteIconDisplay())
                    .isTrue();
        }

        //AC7  Able to see an option that allows the user to add attributes.
        assert_().withFailureMessage("Add Attribute button is displayed")
                .that(attributesSubPage.isAddAttributesButtonDisplayed())
                .isTrue();

        //AC8 skip for sticky footer location verification
    }

    @DataProvider(name = "itemWithoutAttribute")
    public Object[][] itemWithoutAttribute()
    {
        return new Object[][]{
                {428, 606}
        };
    }

    @Test(description = "Story 8.23 User is able to view attributes of an item and the actions taken against the attributes and its values.",
            dataProvider = "itemWithoutAttribute")
    @Issue("LRT-1225")
    public void ViewItemWithoutAttribute(int assetId, int itemId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton().clickAssetModelTab();

        String itemName = new AssetItem(assetId, itemId).getDto().getName();
        ItemsDetailsPage itemsDetailsPage = assetModelSubPage
                .clickCheckOutForEditingButton()
                .getItemByItemName(itemName)
                .clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = itemsDetailsPage.clickAttributesTab();

        //AC9
        assert_().withFailureMessage("Message [No attributes have been added to this item.] is displayed")
                .that(attributesSubPage.getAttributeCountLabelText())
                .isEqualTo("No attributes have been added to this item.");
        //AC9
        assert_().withFailureMessage("Add Attribute button is displayed")
                .that(attributesSubPage.isAddAttributesButtonDisplayed())
                .isTrue();
    }

    private List<AttributeTypeDto> getAttributeTypeDtoById(List<AttributeDto> attributeDto)
    {
        final ArrayList<Object> attributeTypes =
                new ReferenceDataMap(ASSET, ATTRIBUTE_TYPES).getReferenceDataAsClass(AttributeTypeDto.class);
        return attributeDto
                .stream()
                .map(
                        c -> (AttributeTypeDto) attributeTypes.stream()
                                .filter(a -> ((AttributeTypeDto) a).getId().equals(c.getAttributeType().getId()))
                                .findFirst()
                                .orElse(null)
                )
                .collect(Collectors.toList());
    }
    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets() {
        headerPage.discardCheckoutForAllAssets();
    }
}
