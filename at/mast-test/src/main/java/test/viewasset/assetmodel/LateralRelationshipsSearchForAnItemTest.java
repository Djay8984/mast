package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.DatabaseHelper;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetRootItem;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.relationship.AddRelatedItemPage;
import viewasset.item.relationship.RelationshipsSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.element.PreviewItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class LateralRelationshipsSearchForAnItemTest extends BaseTest
{
    private HeaderPage headerPage;
    private AddRelatedItemPage addRelatedItemPage;
    private Boolean isAddRelatedItemPageOpen = false;

    @Test(description = "US 8.30 AC 1-14 - Lateral relationships - search for an item")
    @Issue("LRT-1626")
    public void lateralRelationshipsSearchForAnItemTest() throws SQLException
    {

        final int assetId = 501;
        final String startingString = "A";
        final String endingString = "D";
        final String invalidItemName = "This is an invalid Item Name";
        final String validItemName = "Display Clear Button";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        BuildAssetModelPage buildAssetModelPage = allAssetsAssetElement.clickViewAssetButton()
                .clickAssetModelTab().clickCheckOutForEditingButton();

        //Step: 3
        ItemsDetailsPage itemsDetailsPage
                = buildAssetModelPage.getRootItem().clickFurtherDetailsArrow();

        //Step: 4
        RelationshipsSubPage relationshipsSubPage
                = itemsDetailsPage.clickRelationshipsTab();

        //Step: 5
        addRelatedItemPage
                = relationshipsSubPage.clickCreateNewRelationshipButton();
        isAddRelatedItemPageOpen = true;

        //AC: 1
        //Step: 6
        addRelatedItemPage.setSearchItemText(validItemName);
        assert_().withFailureMessage("Clear Button is expected to be displayed")
                .that(addRelatedItemPage.isClearButtonDisplayed())
                .isTrue();

        PreviewItemElement rootItemElement = addRelatedItemPage.getRootItem();
        List<PreviewItemElement> previewItemElements
                = rootItemElement.getNestedRelatedItems();
        List<String> assetItemsOnBE = getItemNamesByAssetId(assetId);
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("Asset Items in FE should Match with BE")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 1, AC: 2, AC: 4, AC: 5 and AC: 8
        //Step:7, 8, 9, 10 and 13
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            addRelatedItemPage.setSearchItemText(assetItemsOnBE.get(i))
                    .then().clickSearchButtonExpectResults();
            List<String> resultSet = addRelatedItemPage.getResults();
            for (int j = 0; j < resultSet.size(); j++)
            {
                assert_().withFailureMessage("User is expected to  carry out the" +
                        " search and the corresponding entries are returned in the results")
                        .that(resultSet.get(j).equals(assetItemsOnBE.get(i)))
                        .isTrue();

                addRelatedItemPage.clearSearchItemText();
                assert_().withFailureMessage("search results is expected to be " +
                        "displayed even after search field is cleared")
                        .that(resultSet.get(j).equals(assetItemsOnBE.get(i)))
                        .isTrue();

                String feName = resultSet.get(j);
                List<String> beNames = getResultSet(String.valueOf(assetId))
                        .stream()
                        .filter(e -> e.equals(feName))
                        .collect(Collectors.toList());

                assert_().withFailureMessage("Pagination count is expected to be displayed")
                        .that(addRelatedItemPage.getPaginationCountText()
                                .equals("Showing " + resultSet.size() + " of "
                                        + beNames.size()))
                        .isTrue();
            }
        }

        //AC: 3, AC: 4, AC: 4 and AC: 6
        //Step: 11
        addRelatedItemPage = addRelatedItemPage.setSearchItemText(startingString + "*")
                .then().clickSearchButtonExpectResults();
        List<String> startingNames = getResultSet(String.valueOf(assetId))
                .stream().filter(e -> e.startsWith(startingString))
                .collect(Collectors.toList());
        List<String> resultSet = addRelatedItemPage.getResults();
        for (String aResultSet : resultSet)
        {
            assert_().withFailureMessage("All the Item are expected to start with " + startingString)
                    .that(aResultSet.startsWith(startingString))
                    .isTrue();
        }
        assert_().withFailureMessage("Pagination count is expected to be displayed")
                .that(addRelatedItemPage.getPaginationCountText())
                .isEqualTo("Showing " + resultSet.size() + " of "
                        + startingNames.size());

        //AC: 3, AC: 4, AC: 4 and AC: 6
        //Step: 12
        addRelatedItemPage = addRelatedItemPage.setSearchItemText("*" + endingString)
                .then().clickSearchButtonExpectResults();
        List<String> endingNames = getResultSet(String.valueOf(assetId))
                .stream().filter(e -> e.endsWith(endingString))
                .collect(Collectors.toList());
        resultSet = addRelatedItemPage.getResults();

        for (String aResultSet : resultSet)
        {
            assert_().withFailureMessage("All the Item are expected to end with " + endingString)
                    .that(aResultSet.endsWith(endingString))
                    .isTrue();
        }
        assert_().withFailureMessage("Pagination count is expected to be displayed")
                .that(addRelatedItemPage.getPaginationCountText()
                        .equals("Showing " + resultSet.size() + " of "
                                + endingNames.size()))
                .isTrue();

        //AC: 7
        //Step: 14
        addRelatedItemPage.setSearchItemText(invalidItemName)
                .then().clickSearchButtonExpectNoResults();
        assert_().withFailureMessage("Results are not expected to display for" +
                "invalid Item Name").that(addRelatedItemPage.getResults().isEmpty())
                .isTrue();

        //AC: 9
        //Step: 15
        addRelatedItemPage.clearSearchItemText();
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("Asset Items in FE should Match with BE")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 9
        //Step: 16
        addRelatedItemPage.setSearchItemText("*");
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User is expected to see the default hierarchy view")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 10
        //Step: 17
        addRelatedItemPage.clearSearchItemText();
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User is expected to see the default hierarchy view")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 11, AC: 12, AC: 13 and AC: 14
        //Step: 18, 19, 20 and 21
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 1; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User will not be able to select the " +
                    "parent or child item for the item which is currently active")
                    .that(previewItemElements.get(0).isItemRadioButtonEnabled())
                    .isFalse();
            previewItemElements.get(i).clickItemRadioButton();
            assert_().withFailureMessage("User can only select one item from the search results")
                    .that(previewItemElements.get(i).isItemRadioButtonSelected())
                    .isTrue();
            for (int j = 0; j < previewItemElements.size(); j++)
            {
                if (j != i)
                {
                    assert_().withFailureMessage("User can only select one item from the search results")
                            .that(previewItemElements.get(j).isItemRadioButtonSelected())
                            .isFalse();
                }
            }

            assert_().withFailureMessage("User item can be selected and is able to click Create relationship button")
                    .that(addRelatedItemPage.isCreateRelationshipButtonEnabled())
                    .isTrue();

        }

        //Step: 22
        addRelatedItemPage.clickCloseButton();
        isAddRelatedItemPageOpen = false;
        itemsDetailsPage.clickNavigateBackButton(AttributesSubPage.class);
        itemsDetailsPage.clickNavigateBackButton(BuildAssetModelPage.class);

        itemsDetailsPage = buildAssetModelPage.getRootItem().clickFurtherDetailsArrow();
        itemsDetailsPage.clickRelationshipsTab();
        relationshipsSubPage.clickCreateNewRelationshipButton();
        isAddRelatedItemPageOpen = true;

        addRelatedItemPage.setSearchItemText(validItemName);
        assert_().withFailureMessage("Clear Button is expected to be displayed")
                .that(addRelatedItemPage.isClearButtonDisplayed())
                .isTrue();

        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("Asset Items in FE should Match with BE")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 1, AC: 2, AC: 4, AC: 5 and AC: 8
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            addRelatedItemPage.setSearchItemText(assetItemsOnBE.get(i))
                    .then().clickSearchButtonExpectResults();
            resultSet = addRelatedItemPage.getResults();
            for (int j = 0; j < resultSet.size(); j++)
            {
                assert_().withFailureMessage("User is expected to  carry out the" +
                        " search and the corresponding entries are returned in the results")
                        .that(resultSet.get(j).equals(assetItemsOnBE.get(i)))
                        .isTrue();

                addRelatedItemPage.clearSearchItemText();
                assert_().withFailureMessage("search results is expected to be " +
                        "displayed even after search field is cleared")
                        .that(resultSet.get(j).equals(assetItemsOnBE.get(i)))
                        .isTrue();

                String feName = resultSet.get(j);
                List<String> beNames = getResultSet(String.valueOf(assetId))
                        .stream()
                        .filter(e -> e.equals(feName))
                        .collect(Collectors.toList());

                assert_().withFailureMessage("Pagination count is expected to be displayed")
                        .that(addRelatedItemPage.getPaginationCountText()
                                .equals("Showing " + resultSet.size() + " of "
                                        + beNames.size()))
                        .isTrue();
            }
        }

        //AC: 3, AC: 4, AC: 4 and AC: 6
        addRelatedItemPage.setSearchItemText(startingString + "*")
                .then().clickSearchButtonExpectResults();
        resultSet = addRelatedItemPage.getResults();
        for (String aResultSet : resultSet)
        {
            assert_().withFailureMessage("All the Item are expected to start with " + startingString)
                    .that(aResultSet.startsWith(startingString))
                    .isTrue();
        }
        assert_().withFailureMessage("Pagination count is expected to be displayed")
                .that(addRelatedItemPage.getPaginationCountText()
                        .equals("Showing " + resultSet.size() + " of "
                                + startingNames.size()))
                .isTrue();

        //AC: 3, AC: 4, AC: 4 and AC: 6
        addRelatedItemPage.setSearchItemText("*" + endingString)
                .then().clickSearchButtonExpectResults();
        resultSet = addRelatedItemPage.getResults();

        for (String aResultSet : resultSet)
        {
            assert_().withFailureMessage("All the Item are expected to end with")
                    .that(aResultSet.endsWith(endingString))
                    .isTrue();
        }
        assert_().withFailureMessage("Pagination count is expected to be displayed")
                .that(addRelatedItemPage.getPaginationCountText()
                        .equals("Showing " + resultSet.size() + " of "
                                + endingNames.size()))
                .isTrue();

        //AC: 7
        addRelatedItemPage.setSearchItemText(invalidItemName)
                .then().clickSearchButtonExpectNoResults();
        assert_().withFailureMessage("Results are not expected to display for" +
                "invalid Item Name").that(addRelatedItemPage.getResults().isEmpty())
                .isTrue();

        //AC: 9
        addRelatedItemPage.clearSearchItemText();
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("Asset Items in FE should Match with BE")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 9
        addRelatedItemPage.setSearchItemText("*");
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User is expected to see the default hierarchy view")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 10
        addRelatedItemPage.clearSearchItemText();
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 0; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User is expected to see the default hierarchy view")
                    .that(previewItemElements.get(i).getItemName()
                            .equals(assetItemsOnBE.get(i)))
                    .isTrue();
        }

        //AC: 11, AC: 12, AC: 13 and AC: 14
        rootItemElement = addRelatedItemPage.getRootItem();
        previewItemElements = rootItemElement.getNestedRelatedItems();
        for (int i = 1; i < previewItemElements.size(); i++)
        {
            assert_().withFailureMessage("User will not be able to select the " +
                    "parent or child item for the item which is currently active")
                    .that(previewItemElements.get(0).isItemRadioButtonEnabled())
                    .isFalse();
            previewItemElements.get(i).clickItemRadioButton();
            assert_().withFailureMessage("User can only select one item from the search results")
                    .that(previewItemElements.get(i).isItemRadioButtonSelected())
                    .isTrue();
            for (int j = 0; j < previewItemElements.size(); j++)
            {
                if (j != i)
                {
                    assert_().withFailureMessage("User can only select one item from the search results")
                            .that(previewItemElements.get(j).isItemRadioButtonSelected())
                            .isFalse();
                }
            }

            assert_().withFailureMessage("User item can be selected and is able to click Create relationship button")
                    .that(addRelatedItemPage.isCreateRelationshipButtonEnabled())
                    .isTrue();
        }
    }

    private List<String> getItemNamesByAssetId(int assetId)
    {
        AssetRootItem assetRootItem = new AssetRootItem(assetId);
        return
                assetRootItem.getDto().getItems().stream()
                        .map(c -> new AssetItem(assetId, c.getId().intValue()))
                        .map(d -> d.getDto().getName())
                        .collect(Collectors.toList());

    }

    private List<String> getResultSet(String assert_ID) throws SQLException
    {
        ArrayList<String> names = new ArrayList<>();
        ResultSet itemName = DatabaseHelper.executeQuery("select * from mast_asset_versionedassetitem where asset_id = '" + assert_ID + "';");
        while (itemName.next())
        {
            String beName = itemName.getString("name");
            names.add(beName);
        }
        return names;
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        if (isAddRelatedItemPageOpen)
        {
            addRelatedItemPage.clickCloseButton();
        }
        headerPage.discardCheckoutForAllAssets();
    }
}
