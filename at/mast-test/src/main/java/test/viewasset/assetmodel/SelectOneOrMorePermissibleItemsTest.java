package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetRootItem;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.PreviewAssetModelPage;
import viewasset.sub.assetmodel.element.PreviewItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class SelectOneOrMorePermissibleItemsTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(description = "US 9.58 User is able to select one or more permissible items")
    @Issue("LRT-1478")
    public void selectOneOrMorePermissibleItemsTest()
    {

        final int assetId = 503;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset assetOne = new Asset(assetId);
        String assetOneName = assetOne.getDto().getName();
        int rootItem = new AssetRootItem(assetId).getDto().getId().intValue();
        LazyItemDto items = new AssetItem(assetId, rootItem).getDto();

        searchFilterSubPage.setAssetSearchInput(assetOneName)
                .then().clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(assetOneName);

        //Step: 3
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();
        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        //Step: 4
        BuildAssetModelPage buildAssetModelPage
                = assetModelSubPage.clickCheckOutForEditingButton();
        //Step: 5
        buildAssetModelPage.getBuildAssetModelTable().getRootItem().clickItemName();
        CopyFromAssetModelPage copyFromAssetModelPage
                = buildAssetModelPage.clickCopyFromAssetModelButton();

        Asset assetTwo = new Asset(428);
        String assetName = assetTwo.getDto().getName();

        searchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton()
                .then().setAssetSearchInput(assetName);
        searchFilterSubPage.clickAssetSearchApplyButton(PreviewAssetModelPage.class);

        allAssetsSubPage = copyFromAssetModelPage.getAllAssetsSubPage();
        allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(assetName);
        PreviewAssetModelPage previewAssetModelPage
                = allAssetsAssetElement.clickPreviewButton();
        PreviewItemElement rootItemElement = previewAssetModelPage.getRootItem();
        rootItemElement.clickExpandIcon();

        //AC: 1 and AC: 2
        //Step: 6, 7, 8, 9, 10, 11 and 12
        List<PreviewItemElement> previewItemElements
                = rootItemElement.getNestedItems();

        for (int i = 0; i < 5; i++)
        {
            PreviewItemElement previewItemElement = previewItemElements.get(i);
            if (previewItemElement.isItemCheckboxPresent())
                previewItemElement.clickExpandIcon();
            List<PreviewItemElement> nestedPreviewItemElements
                    = previewItemElement.getChildNestedItems();
            if (nestedPreviewItemElements != null)
            {
                assert_().withFailureMessage("Item checkboxes are meant to be present to allow for copying")
                        .that(previewItemElement.isItemCheckboxPresent()).isTrue();
                previewItemElement.clickItemCheckbox();

                //AC: 3, AC: 4 and AC: 5
                for (PreviewItemElement nestedPreviewItemElement : nestedPreviewItemElements)
                {
                    assert_().withFailureMessage("All child Items are expected to be" +
                            " selected when parent item is selected")
                            .that(nestedPreviewItemElement.isItemCheckboxSelected())
                            .isTrue();
                }

                //AC: 3
                assert_().withFailureMessage("Items are expected to be selected")
                        .that(previewItemElement.isItemCheckboxSelected())
                        .isTrue();
                previewItemElement.clickItemCheckbox();

                //AC: 7
                nestedPreviewItemElements.get(0).clickItemCheckbox();

                //AC: 8
                assert_().withFailureMessage("When One of the Child Item is " +
                        "Selected, Parent Item Should be selected")
                        .that(previewItemElement.isItemCheckboxSelected())
                        .isTrue();

                assert_().withFailureMessage("Breadcrumb is expected to be displayed")
                        .that(previewAssetModelPage.getBreadCrumbText()
                                .equals("Copy 1 items to " + items.getName()))
                        .isTrue();
                assert_().withFailureMessage("Copy select button is expected to be displayed")
                        .that(previewAssetModelPage.isCopySelectedButtonDisplayed())
                        .isTrue();
                assert_().withFailureMessage("Cancel button is expected to be displayed")
                        .that(previewAssetModelPage.isCancelButtonDisplayed())
                        .isTrue();

                //AC: 6
                previewItemElement.clickItemCheckbox();
                for (PreviewItemElement nestedPreviewItemElement : nestedPreviewItemElements)
                {
                    assert_().withFailureMessage("All child Items are expected to be" +
                            " unselected when parent item is unselected")
                            .that(nestedPreviewItemElement.isItemCheckboxSelected())
                            .isFalse();
                }
                previewItemElement.clickCollapseIcon();
            }
        }

        //AC: 9
        //Step 13
        previewItemElements.get(1).clickItemCheckbox();
        copyFromAssetModelPage.clickSearchFilterAssetsButton()
                .then().setAssetSearchInput(assetOneName);
        searchFilterSubPage.clickAssetSearchApplyButton(ApplySubPage.class);
        allAssetsSubPage = copyFromAssetModelPage.getAllAssetsSubPage();
        allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(assetOneName);
        allAssetsAssetElement.clickPreviewButton();

        searchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton()
                .then().setAssetSearchInput(assetName);
        searchFilterSubPage.clickAssetSearchApplyButton(ApplySubPage.class);

        allAssetsSubPage = copyFromAssetModelPage.getAllAssetsSubPage();
        allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(assetName);
        previewAssetModelPage
                = allAssetsAssetElement.clickPreviewButton();
        rootItemElement = previewAssetModelPage.getRootItem();
        rootItemElement.clickExpandIcon();

        previewItemElements = rootItemElement.getNestedItems();
        assert_().withFailureMessage("System is expected to lose the selections " +
                "made of previous asset")
                .that(previewItemElements.get(1).isItemCheckboxSelected())
                .isFalse();
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
