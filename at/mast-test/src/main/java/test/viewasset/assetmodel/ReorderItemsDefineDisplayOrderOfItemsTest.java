package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class ReorderItemsDefineDisplayOrderOfItemsTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(description = "US9.64 to Reorder items/define display order of items within Build Asset Model ")
    @Issue("LRT-1511")

    public void reorderItemsDefineDisplayOrderOfItems()
    {

        final int assetId = 428;
        String assetName = "OLUJA";

        WorkHubPage workHubPage = WorkHubPage
                .open();
        headerPage = workHubPage.getHeader();

        AllAssetsSubPage allAssetsSubPage = workHubPage
                .clickAllAssetsTab();

        workHubPage
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetId(assetId)
                .clickViewAssetButton().then()
                .clickAssetModelTab();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        BuildAssetModelPage.BuildAssetModelTable buildAssetModelTable = buildAssetModelPage.getBuildAssetModelTable();
        List<DataElement> bames = buildAssetModelTable.getRows();

        //AC1
        DataElement buildAssetModelElement = buildAssetModelPage.getItemByIndex(1);
        buildAssetModelElement.clickItemName();
        assert_().withFailureMessage("The move down icon is expected to be displayed")
                .that(buildAssetModelElement.isMoveDownIconPresent())
                .isTrue();
        assert_().withFailureMessage("The move up icon is expected to be displayed")
                .that(buildAssetModelElement.isMoveUpIconPresent())
                .isTrue();
        buildAssetModelElement.clickItemName();

        //AC3 - move last item down - disallowed
        bames.get(0).clickItemName();
        assert_().withFailureMessage("First DataElement is expected to disallow move up option ")
                .that(bames.get(0).isMoveUpIconPresent())
                .isFalse();
        assert_().withFailureMessage("First DataElement is expected to allow move down option ")
                .that(bames.get(0).isMoveDownIconPresent())
                .isTrue();

        //AC4 - move first item up - disallowed
        DataElement bame = bames.get(bames.size() - 1);
        bame.clickItemName();
        assert_().withFailureMessage("Last DataElement is expected to allow move up option")
                .that(bame.isMoveUpIconPresent())
                .isTrue();
        assert_().withFailureMessage("Last DataElement is expected to disallow move down option")
                .that(bame.isMoveDownIconPresent())
                .isFalse();
        bame.clickItemName();

        //AC2 - can only move within same parent
        List<DataElement> childBames = bames.get(0)
                .clickExpandIcon()
                .getChildren();
        bame = childBames.get(0);
        bame.clickItemName();
        for (int i = 0; i < childBames.size() - 1; i++)
        {
            //move to last row
            bame.clickMoveDownIcon();
        }
        assert_().withFailureMessage("Items are expected to be disallowed from moving out from " +
                "its parent item")
                .that(bame.isMoveDownIconPresent())
                .isFalse();
        for (int i = 0; i < childBames.size() - 1; i++)
        {
            //move to first row
            bame.clickMoveUpIcon();
        }
        assert_().withFailureMessage("Items are expected to be disallowed from moving out from " +
                "its parent item")
                .that(bame.isMoveUpIconPresent())
                .isFalse();
        bames.get(0)
                .clickCollapseIcon();

        //AC6 - select parent with only 1 child - no movement allowed
        bame = bames.stream().filter(DataElement::isItemExpandable)
                .findFirst().get();
        childBames = bame.clickExpandIcon().getChildren();
        childBames.get(childBames.size() - 1);
        assert_().withFailureMessage(childBames.get(0).getItemName()
                + "is expected to be disallowed from moving down")
                .that(childBames.get(childBames.size() - 1).isMoveDownIconPresent())
                .isFalse();
        childBames.get(0).clickItemName();
        assert_().withFailureMessage(childBames.get(0).getItemName()
                + "is expected to be disallowed from moving up")
                .that(childBames.get(0).isMoveUpIconPresent())
                .isFalse();

        //AC7 - reorder - finish check that order is maintained
        //random reorder
        bame = bames.get(2);
        bame.clickItemName();
        for (int i = 0; i < 3; i++)
        {
            bame.clickMoveDownIcon();
        }
        bame = bames.get(7);
        bame.clickItemName();
        for (int i = 0; i < 4; i++)
        {
            bame.clickMoveUpIcon();
        }

        List<String> itemNamesModified = bames.stream()
                .map(DataElement::getItemName)
                .collect(Collectors.toList());
        ListViewSubPage listViewSubPage = buildAssetModelPage.getHeader().getCheckinAssetElement(assetName)
                .clickCheckinButton().clickCheckinButton();
        buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();

        bames = buildAssetModelPage.getBuildAssetModelTable().getRows();
        List<String> itemNamesModifiedRecall = bames.stream()
                .map(DataElement::getItemName)
                .collect(Collectors.toList());

        assert_().withFailureMessage(itemNamesModifiedRecall.toString()
                + "is expected to be ordered exactly to "
                + itemNamesModified)
                .that(itemNamesModified)
                .isEqualTo(itemNamesModifiedRecall);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}




