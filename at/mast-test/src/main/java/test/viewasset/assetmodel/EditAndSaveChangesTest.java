package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import header.HeaderPage;
import model.asset.Asset;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.PreviewAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.modal.CopyAcrossAttributeValuesWindow;
import viewasset.sub.assetmodel.element.BuildAssetModelElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import viewasset.sub.assetmodel.element.PreviewItemElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class EditAndSaveChangesTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(description = "Story 9.65  Within Build Asset Mode save and finish making changes")
    @Issue("LRT-1507")
    public void editAndSaveChangesTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        ViewAssetPage viewAssetPage = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset();

        String assetName = LloydsRegister.AssetData.assetName;

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .clickAssetModelTab();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage
                .clickCheckOutForEditingButton();

        //AC: 1
        //Covered in @Visible

        //AC: 2
        AddFromReferenceDataPage addFromReferenceDataPage
                = buildAssetModelPage
                .clickAddFromReferenceDataButton();

        List<ReferenceItemElement> referenceItems
                = addFromReferenceDataPage
                .getReferenceItems();

        List<ReferenceItemElement> selectableReferenceData
                = referenceItems.stream().filter(e -> e.isCheckBoxEnabled())
                .collect(Collectors.toList());

        String referenceItem = selectableReferenceData
                .stream()
                .findFirst()
                .get()
                .getReferenceItemName();

        selectableReferenceData
                .stream()
                .findFirst()
                .get()
                .clickCheckbox();

        addFromReferenceDataPage
                .clickAddSelected();

        ListViewSubPage listViewSubPage = buildAssetModelPage
                .clickBuildAssetModelButton();

        List<ListViewItemElement> listViewItemElement = listViewSubPage
                .getAssetItems();

        assert_().withFailureMessage("Added Item from reference data is expected " +
                " to be displayed in List view ")
                .that(listViewItemElement.stream()
                        .filter(e -> e.getItemName().equals(referenceItem))
                        .findFirst().isPresent())
                .isTrue();

        assetModelSubPage
                .clickCheckOutForEditingButton();

        List<BuildAssetModelElement> buildAssetModelElement = buildAssetModelPage.getItems();
        assert_().withFailureMessage("Added Item  Item from reference data is expected " +
                "to be displayed in hierarchical view ")
                .that(buildAssetModelElement.stream()
                        .filter(o -> o.getItemName().equals(referenceItem))
                        .findFirst().isPresent())
                .isTrue();

        //AC: 2
        CopyFromAssetModelPage copyFromAssetModelPage
                = buildAssetModelPage
                .clickCopyFromAssetModelButton();

        Asset assetTwo = new Asset(428);
        assetName = assetTwo
                .getDto()
                .getName();

        SearchFilterSubPage searchFilterSubPage
                = copyFromAssetModelPage.clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName);
        searchFilterSubPage.clickAssetSearchApplyButton();

        AllAssetsSubPage allAssetsSubPage = copyFromAssetModelPage.getAllAssetsSubPage();
        AllAssetsAssetElement allAssetsAssetElement
                = allAssetsSubPage.getAssetCardByAssetName(assetName);
        PreviewAssetModelPage previewAssetModelSubPage
                = allAssetsAssetElement.clickPreviewButton();
        PreviewItemElement rootItemElement = previewAssetModelSubPage.getRootItem();
        rootItemElement.clickExpandIcon();

        List<PreviewItemElement> previewItemElements
                = rootItemElement.getNestedItems();
        String assetItem = previewItemElements.stream()
                .filter(e -> (!e.getItemName().equals(referenceItem)))
                .findFirst()
                .get()
                .getItemName();
        previewItemElements.stream()
                .filter(e -> (!e.getItemName().equals(referenceItem)))
                .findFirst()
                .get()
                .clickItemCheckbox();

        CopyAcrossAttributeValuesWindow copyAcrossAttributeValuesWindow
                = previewAssetModelSubPage
                .clickCopySelectButton();

        buildAssetModelPage = copyAcrossAttributeValuesWindow
                .clickYesButton();

        buildAssetModelPage
                .clickBuildAssetModelButton();

        listViewItemElement = listViewSubPage
                .getAssetItems();

        assert_().withFailureMessage("Copied Item from other asset model is expected " +
                "to be displayed in List view ")
                .that(listViewItemElement.stream()
                        .filter(e -> e.getItemName().equals(assetItem))
                        .findFirst().isPresent())
                .isTrue();
        assetModelSubPage.clickCheckOutForEditingButton();
        buildAssetModelElement = buildAssetModelPage.getItems();
        assert_().withFailureMessage("Copied Item from other asset model is expected" +
                " to be displayed in hierarchical view ")
                .that(buildAssetModelElement.stream()
                        .filter(o -> o.getItemName().equals(assetItem))
                        .findFirst().isPresent())
                .isTrue();

        buildAssetModelPage
                .clickBuildAssetModelButton();

        //AC: 2
        viewAssetPage.clickCodicilsAndDefectsTab();
        viewAssetPage.clickJobsTab();
        viewAssetPage.clickAssetModelTab();
        assetModelSubPage.getListViewSubPage();
        listViewItemElement = listViewSubPage.getAssetItems();

        assert_().withFailureMessage("Added Item from reference data is expected " +
                "to be displayed in List view ")
                .that(listViewItemElement.stream()
                        .filter(e -> e.getItemName().equals(referenceItem))
                        .findFirst().isPresent())
                .isTrue();

        assert_().withFailureMessage("Copied Item from other asset model is expected " +
                "to be displayed in List view ")
                .that(listViewItemElement.stream()
                        .filter(e -> e.getItemName().equals(assetItem))
                        .findFirst().isPresent())
                .isTrue();

        assetModelSubPage.clickCheckOutForEditingButton();
        buildAssetModelElement = buildAssetModelPage.getItems();
        assert_().withFailureMessage("Added Item from reference data is expected " +
                "to be displayed in hierarchical view ")
                .that(buildAssetModelElement.stream()
                        .filter(e -> e.getItemName().equals(referenceItem))
                        .findFirst().isPresent())
                .isTrue();

        assert_().withFailureMessage("Copied Item from other asset model is expected " +
                "to be displayed in List view  ")
                .that(buildAssetModelElement.stream()
                        .filter(e -> e.getItemName().equals(assetItem))
                        .findFirst().isPresent())
                .isTrue();
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
