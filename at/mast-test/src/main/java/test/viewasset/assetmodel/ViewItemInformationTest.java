package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.asset.AssetItem;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.SearchWithinAssetModelPage;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static helper.TestDataHelper.getItemNamesByAssetId;

public class ViewItemInformationTest extends BaseTest
{
    private HeaderPage headerPage;

    @DataProvider(name = "item")
    public Object[][] item()
    {
        return new Object[][]{
                {501, 688}
        };
    }

    @Test(description = "Story - 8.22 - As a user on the item specific page, I want to be able to view the item" +
            " information, so that I can ensure that the item is up to date",
            dataProvider = "item")
    @Issue("LRT-1221")
    public void viewItemInformation(int assetId, int itemId)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        String assetName = new Asset(assetId).getDto().getName();

        AssetModelSubPage assetModelSubPage = allAssetsSubPage
                .getAssetCardByAssetName(assetName)
                .clickViewAssetButton().clickAssetModelTab();

        String assetItemName = assetModelSubPage.getBreadcrumbText();

        String itemName = new AssetItem(assetId, itemId).getDto().getName();
        //AC 1
        ItemsDetailsPage itemsDetailsPage = assetModelSubPage.clickCheckOutForEditingButton().getBuildAssetModelTable().getRows().get(0)
                .clickFurtherDetailsArrow();

        //AC 1
        assert_().withFailureMessage("Specific item detail page for [" + itemName + "] is expected to display.")
                .that(itemsDetailsPage.getItemNameText()).isEqualTo(itemName);

        //AC 5
        assert_().withFailureMessage("Attributes tab is expected to be selected by default.")
                .that(itemsDetailsPage.getSelectedActiveTabName())
                .isEqualTo("Attributes");

        //AC 2
        if (itemName.equalsIgnoreCase(assetItemName))
        {
            assert_().withFailureMessage("The option to remove an item is expected to be displayed")
                    .that(itemsDetailsPage.isRemoveItemButtonDisplayed())
                    .isFalse();
        }
        else
        {
            assert_().withFailureMessage("The option to remove an item is expected to be displayed")
                    .that(itemsDetailsPage.isRemoveItemButtonDisplayed())
                    .isTrue();
        }

        //AC 3
        assert_().withFailureMessage("The attachment and notes icon is expected to be displayed")
                .that(itemsDetailsPage.getHeaderPage().getAssetDetailsPage().isAttachmentAndNotesIconDisplayed())
                .isTrue();

        //AC 4
        assert_().withFailureMessage("The Mark As Complete button is expected to be displayed")
                .that(itemsDetailsPage.isMarkAsCompleteButtonDisplayed())
                .isTrue();

        //AC 6
        assert_().withFailureMessage("The Tasks Tab is expected to be displayed")
                .that(itemsDetailsPage.isTasksTabDisplayed())
                .isTrue();
        assert_().withFailureMessage("The Relationship is expected to be displayed")
                .that(itemsDetailsPage.isRelationshipTabDisplayed())
                .isTrue();

        //AC 7
        assert_().withFailureMessage("The Navigate back button is expected to be displayed")
                .that(itemsDetailsPage.isNavigateBackButtonDisplayed())
                .isTrue();

        //AC 9
        ListViewSubPage listViewSubPage = itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class)
                .getHeaderPage().getCheckinAssetElement(assetName).clickCheckinButton().clickCheckinButton();

        List<String> assetItemsOnBE = getItemNamesByAssetId(assetId);

        SearchWithinAssetModelPage searchWithinAssetModelPage =
                assetModelSubPage.clickSearchWithinAssetModelButton();

        searchWithinAssetModelPage = searchWithinAssetModelPage.
                setItemSearchInput(assetItemsOnBE.get(1))
                .then()
                .clickSearchButton();

        assert_().withFailureMessage("Navigation option is Displayed")
                .that(searchWithinAssetModelPage.
                        isNavigationOptionDisplayed(assetItemsOnBE.get(1)))
                .isTrue();

        assert_().withFailureMessage("[" + itemName + "] is expected to be in search result.")
                .that(searchWithinAssetModelPage.getSearchResultText()
                        .contains(assetItemsOnBE.get(1)))
                .isTrue();
        //AC 8
        itemsDetailsPage = searchWithinAssetModelPage.clickArrowToItemDetailPageBaseOnItemName(assetItemsOnBE.get(1));
        itemsDetailsPage.clickNavigateBackButton(SearchWithinAssetModelPage.class);

        assert_().withFailureMessage("[" + itemName + "] is expected to be in search result.")
                .that(searchWithinAssetModelPage.getSearchResultText()
                        .contains(assetItemsOnBE.get(1)))
                .isTrue();

        //AC 9
        HierarchyViewSubPage hierarchyViewSubPage = assetModelSubPage.clickHierarchyViewButton();
        itemsDetailsPage = hierarchyViewSubPage.getItemBasedOnItemName(itemName)
                .clickItemAttributesLink();
        itemsDetailsPage.clickNavigateBackButton(HierarchyViewSubPage.class);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
