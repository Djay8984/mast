package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.asset.AssetQuery;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class ViewSearchesResultsWithoutSisterAssetsInCopyAssetModelTest extends BaseTest
{
    private HeaderPage headerPage;

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {501}
        };
    }

    @Test(description = "Story 9.69 AC:1-3,5-9 For Asset Search in Asset Model, view results for searches without Sister Assets ",
            dataProvider = "asset")
    @Issue("LRT-1502")
    public void viewSearchesResultsWithoutSisterAssetsInCopyAssetModelTest(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        SearchFilterSubPage myWorkHubSearchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();

        myWorkHubSearchFilterSubPage.setAssetSearchInput(String.valueOf(assetId));
        myWorkHubSearchFilterSubPage.clickAssetSearchApplyButton(ApplySubPage.class);

        CopyFromAssetModelPage copyFromAssetModelPage = allAssetsSubPage.getAssetCardByIndex(0).clickViewAssetButton()
                .clickAssetModelTab().clickCheckOutForEditingButton().getBuildAssetModelTable()
                .getRootItem().clickItemName().clickCopyFromAssetModelButton();

        int defaultNoOfAsset = copyFromAssetModelPage.getAllAssetsSubPage().getAssetCardCount();

        SearchFilterSubPage copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        //AC1
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput("");
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Search successfully completed, without completing search field or filters.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCardCount()).isGreaterThan(0);

        //AC9
        copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchResetButton();
        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Default number of assets are displayed ")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCardCount()).isEqualTo(defaultNoOfAsset);

        //AC6
        copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput("Invalid input");
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Message [Please try searching again.] is expected to display with invalid search input.")
                .that(copyFromAssetModelPage.getNoResultsFoundContainerText()).contains("Please try searching again.");

        //AC2 search input and 1 filter option
        String searchInput = "502";
        AssetQueryDto assetQueryDto = AssetQuery.getDefaultDto();
        assetQueryDto.setSearch(searchInput);
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);

        copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        copyAssetModelPageSearchFilterSubPage.clickClassStatusFilterButton()
                .then().clickClearButton()
                .then().selectClassStatus("Class Contemplated");
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);

        assert_().withFailureMessage("Number of search results with search input and filter option is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().size()).isEqualTo(assetQuery.getDto().getContent().size());
        assert_().withFailureMessage("First asset from search results with search input and one filter option is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName())
                .isEqualTo(assetQuery.getDto().getContent().get(0).getName());

        //AC7 AC3 search input and 2 filter option
        String minGrossTonnage = "992";
        assetQueryDto.setGrossTonnageMin(Double.valueOf(minGrossTonnage));
        assetQuery = new AssetQuery(assetQueryDto);

        copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickGrossTonnageFilterButton()
                .then().setMinGrossTonnage(minGrossTonnage);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with search input and 2 filter option is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().size()).isEqualTo(assetQuery.getDto().getContent().size());

        assert_().withFailureMessage("First asset from search results with search input and 2 filter option is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName())
                .isEqualTo(assetQuery.getDto().getContent().get(0).getName());

        //AC7 AC3
        searchInput = "502";
        assetQueryDto = AssetQuery.getDefaultDto();
        assetQueryDto.setSearch(searchInput);
        assetQuery = new AssetQuery(assetQueryDto);

        copyAssetModelPageSearchFilterSubPage = copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchResetButton();
        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with search input AssetId only is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto().getContent().size());

        assert_().withFailureMessage("First asset from search results with search input AssetId only is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName())
                .isEqualTo(assetQuery.getDto().getContent().get(0).getName());

        //AC7 AC3 In search field IMO Number only
        searchInput = new Asset(502).getDto().getIhsAsset().getId().toString();
        AssetQuery.getDefaultDto();
        assetQueryDto.setSearch(searchInput);
        assetQuery = new AssetQuery(assetQueryDto);

        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with search input IMO number only is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto().getContent().size());

        assert_().withFailureMessage("First asset from search results with search input IMO number only is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName())
                .isEqualTo(assetQuery.getDto().getContent().get(0).getName());

        //AC3 AC7 In search field Asset Name only
        searchInput = new Asset(503).getDto().getName();
        AssetQuery.getDefaultDto();
        assetQueryDto.setSearch(searchInput);
        assetQuery = new AssetQuery(assetQueryDto);

        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with search input AssetName only is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto().getContent().size());

        assert_().withFailureMessage("First asset from search results with search input AssetName only is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName())
                .isEqualTo(assetQuery.getDto().getContent().get(0).getName());

        //AC5 AC7
        searchInput = "*";
        AssetQuery.getDefaultDto();
        assetQueryDto.setSearch(searchInput);
        assetQuery = new AssetQuery(assetQueryDto);

        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with wildcard search input only is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto().getContent().size());
        assert_().withFailureMessage("First asset from search results with wildcard search input only is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName().toLowerCase())
                .isEqualTo(
                        assetQuery.getDto().getContent()
                                .stream().sorted((dto1, dto2) -> dto1.getName().compareTo(dto2.getName()))
                                .collect(Collectors.toList())
                                .stream().findFirst().get().getName().toLowerCase()
                );

        //AC3 AC7 Search field empty , one filter options
        AssetQuery.getDefaultDto();
        assetQueryDto.setClassStatusId(Collections.singletonList(1L));
        assetQuery = new AssetQuery(assetQueryDto);

        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchResetButton();
        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickClassStatusFilterButton()
                .then().clickClearButton()
                .then().selectClassStatus("Class Contemplated");

        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with blank search input and 1 filter option is expected to be same with BE.")
                .that(copyFromAssetModelPage
                        .getAllAssetsSubPage()
                        .getPaginationCountPage()
                        .getTotalAmount())
                .isEqualTo(assetQuery.getDto()
                        .getContent()
                        .size());

        assert_().withFailureMessage("First asset from search result with blank search input and 1 filter option is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName().toLowerCase())
                .isEqualTo(
                        assetQuery.getDto().getContent()
                                .stream().sorted((dto1, dto2) -> dto1.getName().compareTo(dto2.getName()))
                                .collect(Collectors.toList())
                                .stream().findFirst().get().getName().toLowerCase()
                );

        //AC3 AC7 Search field empty , multiple filter options
        AssetQuery.getDefaultDto();
        assetQueryDto.setGrossTonnageMin(Double.valueOf(minGrossTonnage));
        assetQuery = new AssetQuery(assetQueryDto);

        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchResetButton();
        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.clickClassStatusFilterButton()
                .then().clickClearButton()
                .then().selectClassStatus("Class Contemplated");
        copyAssetModelPageSearchFilterSubPage.clickGrossTonnageFilterButton()
                .then().setMinGrossTonnage(minGrossTonnage);
        copyAssetModelPageSearchFilterSubPage.clickAssetSearchApplyButton(CopyFromAssetModelPage.class);
        assert_().withFailureMessage("Number of search results with blank search input and 2 filter option is expected to be same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(assetQuery.getDto().getContent().size());

        assert_().withFailureMessage("First asset from search result with blank search input and 2 filter option is expected to be the same with BE.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().get(0).getAssetName().toLowerCase())
                .isEqualTo(
                        assetQuery.getDto().getContent()
                                .stream().sorted((dto1, dto2) -> dto1.getName().compareTo(dto2.getName()))
                                .collect(Collectors.toList())
                                .stream().findFirst().get().getName().toLowerCase()
                );
        //AC8
        int totalNumOfPreviousSearchResult = copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount();
        List<String> previousSearchResult = copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards()
                .stream().map(x -> x.getName()).collect(Collectors.toList());

        searchInput = "Something Different";
        copyFromAssetModelPage.clickSearchFilterAssetsButton();
        copyAssetModelPageSearchFilterSubPage.setAssetSearchInput(searchInput);
        assert_().withFailureMessage("Total number of search results is expected to be same with previous search result before click Search button.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getPaginationCountPage().getTotalAmount())
                .isEqualTo(totalNumOfPreviousSearchResult);
        assert_().withFailureMessage("Search result is expected to be the same with previous search result before click Search button.")
                .that(copyFromAssetModelPage.getAllAssetsSubPage().getAssetCards().stream().map(x -> x.getName()).collect(Collectors.toList()))
                .containsExactlyElementsIn(previousSearchResult);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
