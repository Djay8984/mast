package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.TestDataHelper;
import model.asset.Asset;
import model.asset.AssetItem;
import model.asset.AssetModel;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class OptionsToBuildAnAssetModelTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(
            description = "US9.55 - User is able to see the appropriate" +
                    " options to build an asset model"
    )
    @Issue("LRT-1477")
    public void optionsToBuildAnAssetModelTest()
    {

        final int assetId = 501;

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        //Step: 2
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        ListViewSubPage listViewSubPage = assetModelSubPage
                .getListViewSubPage();

        //Get the list of items from FE
        List<ListViewItemElement> assetItems = listViewSubPage.getAssetItems();

        AssetModel assetModel = new AssetModel(assetId);
        List<ItemDto> childItems = assetModel.getDto().getItems().get(0)
                .getItems();

        //AC: 5
        //Step: 3 and 4

        //Click on a item
        //if clickable check that item has children!
        for (int i = 0; i < 3; i++)
            if (assetItems.get(i).isItemNameClickable())
            {
                listViewSubPage = assetItems.get(i).clickItemName();
                //Gather all the child items on FE that are clickable - ie with
                // child
                List<String> childItemNamesOnFE = listViewSubPage.getAssetItems()
                        .stream()
                        .map(ListViewItemElement::getItemName)
                        .collect(Collectors.toList());
                //Gather all the child items on BE
                List<String> childItemNamesOnBE = childItems.get(i).getItems()
                        .stream()
                        .map(itemDto -> new AssetItem(assetId, itemDto.getId()
                                .intValue()))
                        .map(assetItem -> TestDataHelper.truncateString(41, assetItem.getDto().getName()))
                        .collect(Collectors.toList());
                //Compare!
                assert_().withFailureMessage("All child item names matches. FE " +
                        "contains " + childItemNamesOnFE + ". BE contains " + childItemNamesOnBE)
                        .that(childItemNamesOnFE.containsAll(childItemNamesOnBE))
                        .isTrue();

                //return to parent - to loop again
                listViewSubPage.clickBreadcrumbLinkByIndex(0);
            }
            else
            {//AC2 - else not clickable - check that there is no children
                List<String> childItemNotClickable = childItems.get(i)
                        .getItems()
                        .stream()
                        .map(itemDto -> new AssetItem(assetId, itemDto.getId()
                                .intValue()))
                        .map(assetItem -> assetItem.getDto().getName())
                        .collect(Collectors.toList());
                assert_().withFailureMessage("Item " + assetItems.get(i)
                        .getItemName() + " has no children")
                        .that(childItemNotClickable)
                        .isEmpty();
            }

        crawlThroughItemsRecursive(listViewSubPage, listViewSubPage.getBreadcrumbTextByIndex(0));

        //AC: 1 and 2
        //Step: 5 and 6
        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        BuildAssetModelPage.BuildAssetModelTable buildAssetModelTable = buildAssetModelPage.getBuildAssetModelTable();

        //AC: 3 and 4 are covered as part of @Visible Annotation
        //Step: 7 and 8
        List<DataElement> items = buildAssetModelTable.getRows();
        List<String> buildItemNames = items.stream()
                .map(DataElement::getItemName)
                .collect(Collectors.toList());

        //AC: 6 and 7
        //Step: 9
        int i = 1;
        while (i < buildItemNames.size())
        {
            DataElement buildAssetModelElement = items.get(i);
            buildAssetModelElement.clickItemName();

            //Move-Up icon will not be present for first item
            if (i > 1)
            {
                assert_().withFailureMessage("Move Up Icon is expected to be to be displayed")
                        .that(buildAssetModelElement.isMoveUpIconPresent())
                        .isTrue();
            }

            assert_().withFailureMessage("Delete Icon is expected to be to be displayed")
                    .that(buildAssetModelElement.isDeleteIconDisplayed())
                    .isTrue();
            assert_().withFailureMessage("Duplicate Icon is expected to be to be displayed")
                    .that(buildAssetModelElement.isDuplicateIconDisplayed())
                    .isTrue();

            //Move-Down icon will not be present for last item
            if (i < buildItemNames.size() - 1)
            {
                assert_().withFailureMessage("Move Down Icon is expected to be to be displayed")
                        .that(buildAssetModelElement.isMoveDownIconPresent())
                        .isTrue();
            }
            i++;

            buildAssetModelElement.clickItemName();
        }

        DataElement buildAssetModelElement = items.get(0);
        buildAssetModelElement.clickItemName();

        //AC: 8
        //Step: 10
        listViewSubPage = buildAssetModelPage.getHeader().getCheckinAssetElement(name)
                .clickCheckinButton().clickCheckinButton();

        //AC: 9
        //Step: 11
        //BUG LRD-5685
        assert_().withFailureMessage("Item selected in the build mode is expected to be select in list view mode ")
                .that(listViewSubPage.getBreadcrumbTextByIndex(listViewSubPage.getBreadCrumbTextSize() - 1))
                .isEqualTo(buildItemNames.get(0));

    }

    private ListViewSubPage crawlThroughItemsRecursive(ListViewSubPage listViewSubPage, String
            currentItemNameFromBreadCrumb)
    {
        //On a fresh new page, gather all list of items
        List<ListViewItemElement> assetItems = listViewSubPage.getAssetItems();
        //Check all items on the page
        for (ListViewItemElement assetItem : assetItems)
        {
            //If the item is clickable i.e. have children
            if (assetItem.isItemNameClickable())
            {
                String currAssetItemName = assetItem.getItemName();
                //Click on it and do RECURSION MIND BLOW
                listViewSubPage = crawlThroughItemsRecursive(assetItem.clickItemName(), currAssetItemName);
                // clicked previously
                assert_().withFailureMessage("For " + currentItemNameFromBreadCrumb +
                        " matches the breadcrumb" + listViewSubPage.getBreadcrumbTextByIndex
                        (listViewSubPage.getBreadCrumbTextSize() - 1))
                        .that(currentItemNameFromBreadCrumb)
                        .isEqualTo(listViewSubPage.getBreadcrumbTextByIndex
                                (listViewSubPage.getBreadCrumbTextSize() - 1));
                break;
            }
        }
        if (listViewSubPage.getBreadCrumbTextSize() > 1)
        {
            return listViewSubPage.clickBreadcrumbLinkByIndex
                    (listViewSubPage
                            .getBreadCrumbTextSize() - 2);
        }
        else
        {
            return null;
        }
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }

}
