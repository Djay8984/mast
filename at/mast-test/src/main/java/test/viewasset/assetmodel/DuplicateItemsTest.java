package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.attributes.element.AttributeElement;
import viewasset.item.attributes.modal.EditConfirmationPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.BuildAssetModelElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class DuplicateItemsTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(description = "US 9.62 - Duplicate Items")
    @Issue("LRT-1516")
    public void duplicateItemsTest()
    {

        final int assetId = 501;
        String secondItemName = "ENGINE GROUP";
        String attributeValue = RandomStringUtils.randomNumeric(6);
        String attributeType = "ML NUMBER";

        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage.clickAssetModelTab();
        ListViewSubPage listViewSubPage = assetModelSubPage.getListViewSubPage();
        ListViewItemElement listViewItemElement
                = listViewSubPage.getItemByIndex(1);
        String beforeReviewValue = listViewItemElement.getReviewInProgressValues();

        //AC: 1
        //Step: 3
        BuildAssetModelPage buildAssetModelPage
                = assetModelSubPage.clickCheckOutForEditingButton();
        DataElement buildAssetModelElement
                = buildAssetModelPage.getItemByIndex(1);
        buildAssetModelElement.clickExpandIcon();
        List<DataElement> subItems = buildAssetModelElement.getChildren();
        List<DataElement> itemsToDuplicate = subItems.stream()
                .filter(e -> e.isDuplicateIconDisplayed())
                .collect(Collectors.toList());

        //AC:1
        //Step: 4
        assert_().withFailureMessage("Duplicate icon is expected to be displayed")
                .that(itemsToDuplicate.get(0).isDuplicateIconDisplayed())
                .isTrue();

        //AC: 2 and AC: 3
        //Step: 5 and 6
        String itemName = itemsToDuplicate.get(0).getItemName();
        int testItemCountBefore = subItems.stream()
                .filter(e -> e.getItemName().equals(itemName))
                .collect(Collectors.toList())
                .size();
        itemsToDuplicate.get(0).clickDuplicateItemIcon();
        subItems = buildAssetModelElement.getChildren();
        int testItemCountAfter = subItems.stream()
                .filter(e -> e.getItemName().equals(itemName))
                .collect(Collectors.toList())
                .size();
        assert_().withFailureMessage("Duplicated instance of an Item is expected to be displayed")
                .that(testItemCountAfter)
                .isGreaterThan(testItemCountBefore);

        //AC: 4 AC:5a ana AC: 7
        //Step: 7
        buildAssetModelElement = buildAssetModelPage.getItemByItemName(secondItemName);
        buildAssetModelElement.clickDuplicateItemIcon();
        List<BuildAssetModelElement> items = buildAssetModelPage.getItems();
        List<BuildAssetModelElement> originalItems = items.stream()
                .filter(e -> e.getItemName().equals(secondItemName))
                .collect(Collectors.toList());
        buildAssetModelElement.clickExpandIcon();
        List<String> originalItemsNames = buildAssetModelElement
                .getChildren()
                .stream()
                .map(e -> e.getItemName())
                .collect(Collectors.toList());
        originalItems.stream()
                .filter(e -> e.getItemName().equals(secondItemName))
                .collect(Collectors.toList())
                .get((originalItems.size()) - 1)
                .clickExpandCollapseIcon();
        List<String> duplicateItemsNames = buildAssetModelElement
                .getChildren().stream()
                .map(e -> e.getItemName())
                .collect(Collectors.toList());
        assert_().withFailureMessage("Duplicated parent Item along with " +
                "child Items are expected to be displayed")
                .that(originalItemsNames.equals(duplicateItemsNames))
                .isTrue();

        //Step: 11
        buildAssetModelPage.clickFinishButton();
        assetModelSubPage.clickListViewButton();

        //AC: 8 and 9
        //Step: 12 and 13
        listViewItemElement = listViewSubPage.getItemByIndex(1);
        assert_().withFailureMessage("Status indicator is expected to be displayed ")
                .that(listViewItemElement.isStatusIndicatorDisplayed())
                .isTrue();
        String afterReviewValue = listViewItemElement.getReviewInProgressValues();

        assert_().withFailureMessage("Review completed status us expected to be increased")
                .that(Integer.valueOf(afterReviewValue))
                .isGreaterThan(Integer.valueOf(beforeReviewValue));
        listViewSubPage = listViewItemElement.clickItemName();
        List<ListViewItemElement> listViewSubItemElement = listViewSubPage.getAssetItems();
        ListViewItemElement childItem = listViewSubItemElement.stream()
                .filter(e -> e.getItemName().equals(itemName))
                .findFirst().get();
        assert_().withFailureMessage("Status indicator is expected to be displayed ")
                .that(childItem.isStatusIndicatorDisplayed())
                .isTrue();
        String childItemReviewValue = childItem.getReviewInProgressValues();
        assert_().withFailureMessage("Review completed for child is expected to be '0'")
                .that(Integer.valueOf(childItemReviewValue).equals(0))
                .isTrue();
        listViewSubPage.clickBreadcrumbLinkByIndex(0);

        //AC: 5b and 5c
        //Step: 8
        List<ListViewItemElement> listViewItemElements = listViewSubPage.getAssetItems();
        ItemsDetailsPage ltemDetailsPage = listViewItemElements.get(0).clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = ltemDetailsPage.clickAttributesTab();
        List<AttributeElement> attributeElement = attributesSubPage.getAllDisplayedAttributeElement();
        attributeElement.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeType))
                .findFirst()
                .get()
                .clearAttributeValue()
                .setAttributeValue(attributeValue);
        EditConfirmationPage editConfirmationPage
                = attributesSubPage.clickSaveChangesButton()
                .clickOkButton()
                .clickNavigateBackButton(EditConfirmationPage.class);
        assetModelSubPage = editConfirmationPage.clickSaveChangesButton();
        assetModelSubPage.getListViewSubPage();
        listViewSubPage.clickBreadcrumbLinkByIndex(0);
        listViewItemElements = listViewSubPage.getAssetItems();
        String c = listViewItemElements.get(0).getItemName();
        buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();

        buildAssetModelElement = buildAssetModelPage.getItemByIndex(1);
        buildAssetModelElement.clickDuplicateItemIcon();
        assetModelSubPage = buildAssetModelPage.clickFinishButton();

        listViewSubPage = assetModelSubPage.getListViewSubPage();
        listViewSubPage.clickBreadcrumbLinkByIndex(0);
        listViewItemElements = listViewSubPage.getAssetItems();
        List<ListViewItemElement> listViewDuplicatedItemElements
                = listViewItemElements.stream()
                .filter(e -> e.getItemName().equals(c))
                .collect(Collectors.toList());
        listViewDuplicatedItemElements
                .get(listViewDuplicatedItemElements.size() - 1)
                .clickFurtherDetailsArrow();
        attributesSubPage = ltemDetailsPage.clickAttributesTab();
        attributeElement = attributesSubPage.getAllDisplayedAttributeElement();

        assert_().withFailureMessage("Duplicated Items Attributed is not expected to be copied")
                .that(attributeElement.stream()
                        .filter(e -> e.getAttributeNameText().equals(attributeType))
                        .findFirst().get()
                        .getAttributeValue().isEmpty())
                .isTrue();
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
