package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import model.asset.Asset;
import model.asset.AssetQuery;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.PreviewAssetModelPage;
import viewasset.sub.assetmodel.element.PreviewItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.Collections;
import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class UserCanImportFromAnotherAssetModelTest extends BaseTest
{

    private HeaderPage headerPage;

    @Test(
            description = "As a user on the 'Import from another asset model page', I want to be able to preview" +
                    " an asset model so that I can check the model before selecting to import it"
    )
    @Issue("LRT-1605")
    public void userCanImportFromAnotherAssetModelTest()
    {
        Asset selectedAsset = new Asset(501);

        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setCategoryId(Collections.singletonList(selectedAsset.getDto().getAssetCategory().getId()));
        AssetLightDto assetToCopy = new AssetQuery(assetQueryDto).getDto().getContent().get(2);

        //Step 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        SearchFilterSubPage searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(selectedAsset.getDto().getName());
        searchFilterSubPage.clickAssetSearchApplyButton(ApplySubPage.class);

        ViewAssetPage viewAssetPage = allAssetsSubPage.getAssetCardByAssetName(selectedAsset.getDto().getName())
                .clickViewAssetButton();
        AssetModelSubPage assetModelSubPage = viewAssetPage.clickAssetModelTab();
        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();

        //Step 2
        CopyFromAssetModelPage copyFromAssetModelButtonSubPage =
                buildAssetModelPage.clickCopyFromAssetModelButton();
        searchFilterSubPage = copyFromAssetModelButtonSubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.setAssetSearchInput(assetToCopy.getName());
        searchFilterSubPage.clickAssetSearchApplyButton();

        allAssetsSubPage = copyFromAssetModelButtonSubPage.getAllAssetsSubPage();
        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage.getAssetCardByAssetName(
                assetToCopy.getName()
        );

        //Step 3
        //AC 1 - Click Preview Button
        PreviewAssetModelPage previewAssetModelPage = allAssetsAssetElement.clickPreviewButton();
        PreviewItemElement rootItemElement = previewAssetModelPage.getRootItem();
        rootItemElement.clickExpandIcon();

        //AC 7 - Not able to copy once selected
        assert_().withFailureMessage("Copy button visible with no items selected")
                .that(previewAssetModelPage.isCopySelectedButtonDisplayed())
                .isFalse();

        //AC2 & AC 3 & AC 4
        List<PreviewItemElement> previewItemElements = rootItemElement.getNestedItems();
        PreviewItemElement itemToCopy = findItemToCopy(previewItemElements);
        itemToCopy.clickItemCheckbox();

        //AC 5
        assert_().withFailureMessage("Top-level item was allowed to be copied")
                .that(rootItemElement.isItemCheckboxPresent())
                .isFalse();

        //AC 7 - Able to copy once selected
        assert_().withFailureMessage("Items weren't allowed to be copied after being selected")
                .that(previewAssetModelPage.isCopySelectedButtonDisplayed())
                .isTrue();

        //AC 6 - De-scoped
    }

    private PreviewItemElement findItemToCopy(List<PreviewItemElement> previewItemElements)
    {
        for (PreviewItemElement previewItemElement : previewItemElements)
        {
            if (previewItemElement.isItemCheckboxPresent())
                return previewItemElement;
            previewItemElement.clickExpandIcon();
            List<PreviewItemElement> nestedPreviewItemElements = previewItemElement.getNestedItems();
            if (nestedPreviewItemElements != null)
            {
                PreviewItemElement itemToCopy = findItemToCopy(nestedPreviewItemElements);
                if (itemToCopy != null)
                    return itemToCopy;
            }
        }
        Assert.fail("No item to copy is found underneath this asset!");
        return null;
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
