package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.TestDataHelper;
import model.asset.AssetDraftItem;
import model.asset.AssetItem;
import model.asset.AssetRootItem;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ModalDiscardChangesSubPage;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.sub.AllAssetsSubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

// todo temporarily commented Tests that use LazyItemDto.getRelated() as structure has changed
// and merge is required to unblock stories, these should be refactored to the new structure asap
// see: TomB
public class RemoveItemTest extends BaseTest
{
    private HeaderPage headerPage;

    String cannotDeleteHeaderMessage = "Item cannot be deleted";
    String cannotDeleteBodyMessage = "cannot be deleted";
    String deleteHeaderMessage = "Delete item";
    String deleteBodyMessage = "Are you sure you want to delete %s?\nDependent items or tasks will also be deleted";

    String associatedServiceParentItemName = "SHELL AND APPENDAGES GROUP";
    String associatedServiceItemName = "SHELL";
    String associatedOpenCocDefParentItemName = "ANCHORING, MOORING AND TETHERING GROUP";
    String associatedOpenCocDefItemName = "SOLID FIXED PITCH PROPELLER";

    String notAssociatedParentItem1 = "";
    int notAssociatedParentItem1Id = 0;
    String notAssociatedChildItem1 = "";
    String notAssociatedParentItem2 = "";
    int notAssociatedParentItem2Id = 0;
    String notAssociatedChildItem2 = "";
    String padding = "";

    @DataProvider(name = "asset")
    public Object[][] assets()
    {
        return new Object[][]{
                {501}
        };
    }

    @Test(description = "US 9.63 remove an item", dataProvider = "asset")
    @Issue("LRT-2152")
    public void removeItemAsPartOfAssetModelInAssetModelPage(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage
                .open();

        AllAssetsSubPage allAssetsSubPage = workHubPage
                .clickAllAssetsTab();

        workHubPage
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetId(assetId)
                .clickViewAssetButton().then()
                .clickAssetModelTab();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();

        // prepare data for removing in step 13 onwards
        AddFromReferenceDataPage addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedParentItem1 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedParentItem1).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();

        addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedParentItem2 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedParentItem2).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();

        buildAssetModelPage.getItemByItemName(notAssociatedParentItem1).clickItemName();
        addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedChildItem1 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedChildItem1).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();

        buildAssetModelPage.getItemByItemName(notAssociatedParentItem2).clickItemName();
        addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedChildItem2 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedChildItem2).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();

        HierarchyViewSubPage hierarchyViewSubPage = buildAssetModelPage.clickFinishButton().then().clickHierarchyViewButton();

        assert_().withFailureMessage(notAssociatedParentItem1 + " : newly added asset item is expected to be displayed")
                .that(hierarchyViewSubPage.getAssetItems().stream().anyMatch(x -> x.getItemName().equals(notAssociatedParentItem1)))
                .isTrue();

        hierarchyViewSubPage.getItemBasedOnItemName(notAssociatedParentItem1).clickCollapseIcon();
        assert_().withFailureMessage(notAssociatedChildItem1 + " : newly added asset item is expected to be displayed")
                .that(hierarchyViewSubPage.getItemBasedOnItemName(notAssociatedParentItem1).getAssetItems().stream()
                        .anyMatch(x -> x.getItemName().equals(notAssociatedChildItem1)))
                .isTrue();

        String reviewCompleteValues = assetModelSubPage.clickListViewButton().getItemByItemName(notAssociatedParentItem2).getReviewCompleteValues();
        cannotDeleteBodyMessage = "%s cannot be deleted because it is\n%s associated with services defects or codicils";

        buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        buildAssetModelPage.getItemByItemName(notAssociatedParentItem1).clickItemName();

        notAssociatedParentItem1Id = new AssetRootItem(assetId).getDto().getRelated().stream()
                .filter(x -> new AssetItem(assetId, x.getId().intValue()).getDto().getName().equals(notAssociatedParentItem1))
                .findFirst()
                .get().getId().intValue();

        notAssociatedParentItem2Id = new AssetRootItem(assetId).getDto().getRelated().stream()
                .filter(x -> new AssetItem(assetId, x.getId().intValue()).getDto().getName().equals(notAssociatedParentItem2))
                .findFirst()
                .get().getId().intValue();

        List<String> draftRelatedItemNames = new AssetDraftItem(assetId, notAssociatedParentItem1Id).getDto().getRelated().stream()
                .map(a -> new AssetItem(assetId, a.getId().intValue()).getDto().getName())
                .collect(Collectors.toList());

        assert_().withFailureMessage("Hierarchy will expand to display all items which are connected under the selected " + notAssociatedParentItem1)
                .that(buildAssetModelPage.getItemByItemName(notAssociatedParentItem1).getChildren().stream()
                        .map(DataElement::getItemName)
                        .collect(Collectors.toList()))
                .containsAllIn(draftRelatedItemNames);

        buildAssetModelPage.getItemByItemName(draftRelatedItemNames.get(0)).clickItemName();

        assert_().withFailureMessage("Selected item is expected to be highlighted")
                .that(buildAssetModelPage.getItemByItemName(draftRelatedItemNames.get(0)).isItemSelected())
                .isTrue();

        //AC1 , AC2
        assert_().withFailureMessage("Delete icon is expected to be displayed for item [" + draftRelatedItemNames.get(0) + "]")
                .that(buildAssetModelPage.getItemByItemName(draftRelatedItemNames.get(0)).isDeleteIconDisplayed())
                .isTrue();

        //extra 11 space need delete message from DataElement
        padding = "           ";

        //AC3 , AC4  step 10
/*
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(associatedOpenCocDefParentItemName).clickDeleteItemIcon();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(cannotDeleteHeaderMessage);
        assert_().withFailureMessage("[" + String.format(cannotDeleteBodyMessage, associatedOpenCocDefParentItemName, padding) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(cannotDeleteBodyMessage, associatedOpenCocDefParentItemName, padding));
        buildAssetModelPage.getModalDiscardChangesSubPage().clickOkButton();

        //AC3 , AC4  step 8
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(associatedOpenCocDefParentItemName).clickItemName()
                .getItemByItemName(associatedOpenCocDefItemName).clickDeleteItemIcon();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(cannotDeleteHeaderMessage);
        assert_().withFailureMessage("[" + String.format(cannotDeleteBodyMessage, associatedOpenCocDefItemName, padding) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(cannotDeleteBodyMessage, associatedOpenCocDefItemName, padding));
        buildAssetModelPage.getModalDiscardChangesSubPage().clickOkButton();

        //AC3 , AC4  step 9
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(associatedServiceParentItemName).clickDeleteItemIcon();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(cannotDeleteHeaderMessage);
        assert_().withFailureMessage("[" + String.format(cannotDeleteBodyMessage, associatedServiceParentItemName, padding) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(cannotDeleteBodyMessage, associatedServiceParentItemName, padding));
        buildAssetModelPage.getModalDiscardChangesSubPage().clickOkButton();

        //AC3 , AC4  step 7
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(associatedServiceParentItemName).clickItemName()
                .getItemByItemName(associatedServiceItemName).clickDeleteItemIcon();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(cannotDeleteHeaderMessage);
        assert_().withFailureMessage("[" + String.format(cannotDeleteBodyMessage, associatedServiceItemName, padding) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(cannotDeleteBodyMessage, associatedServiceItemName, padding));
        buildAssetModelPage.getModalDiscardChangesSubPage().clickOkButton();

        //AC5 step 11
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(notAssociatedParentItem2).clickItemName()
                .getItemByItemName(notAssociatedChildItem2).clickDeleteItemIcon();
        assert_().withFailureMessage(deleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(deleteHeaderMessage);
        assert_().withFailureMessage("Message [" + String.format(deleteBodyMessage, notAssociatedChildItem2) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(deleteBodyMessage, notAssociatedChildItem2));

        //AC8 step 12
        buildAssetModelPage.getModalDiscardChangesSubPage().clickCancelButton();
        assert_().withFailureMessage("System is expected to remain in the same page before clicking cancel button")
                .that(buildAssetModelPage.isBuildAssetModelButtonSelected())
                .isTrue();

        //AC6 step 14
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(notAssociatedChildItem2).clickDeleteItemIcon();
        buildAssetModelPage.getModalDiscardChangesSubPage().clickDeleteItemButton();
        assert_().withFailureMessage("Removed item [" + notAssociatedChildItem2 + "] is expected not found in database")
                .that(new AssetDraftItem(assetId, notAssociatedParentItem2Id).getDto().getRelated().stream().anyMatch(x -> new AssetItem(assetId, x.getId().intValue()).getDto().getName().equals(notAssociatedChildItem1)))
                .isFalse();

        //step 15
        buildAssetModelPage = buildAssetModelPage.getItemByItemName(notAssociatedParentItem1).clickDeleteItemIcon();

        //AC5
        assert_().withFailureMessage(deleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getHeader())
                .contains(deleteHeaderMessage);
        assert_().withFailureMessage("Message [" + String.format(deleteBodyMessage, notAssociatedParentItem1) + "] is expected to be displayed")
                .that(buildAssetModelPage.getModalDiscardChangesSubPage().getBody())
                .contains(String.format(deleteBodyMessage, notAssociatedParentItem1));

        //AC6 step 16
        buildAssetModelPage.getModalDiscardChangesSubPage().clickDeleteItemButton();

        assert_().withFailureMessage(notAssociatedParentItem1 + " is deleted and it is not expected to be found in database")
                .that(new AssetRootDraftItem(assetId).getDto().getRelated().stream().anyMatch(t -> new AssetItem(assetId, t.getId().intValue()).getDto().getName().equals(notAssociatedParentItem1)))
                .isFalse();
        assert_().withFailureMessage("Parent of " + notAssociatedChildItem1 + " is deleted and it is expected not found in database")
                .that(new AssetRootDraftItem(assetId).getDto().getRelated().stream().anyMatch(t -> new AssetItem(assetId, t.getId().intValue()).getDto().getName().equals(notAssociatedChildItem1)))
                .isFalse();
*/

        //step step 17
        assetModelSubPage = buildAssetModelPage.clickFinishButton();

        //AC9 step 18
        assert_().withFailureMessage("System is expected to update the review complete")
                .that(assetModelSubPage.clickListViewButton().getItemByItemName(notAssociatedParentItem2).getReviewCompleteValues())
                .isNotEqualTo(reviewCompleteValues);

        //AC10 step 19 todo problem to create lateral relationship

    }

    @Test(description = "US 9.63 remove an item", dataProvider = "asset")
    @Issue("LRT-2156")
    public void removeItemAsPartOfAssetModelInItemSpecificPage(int assetId)
    {

        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetId(assetId)
                .clickViewAssetButton().then()
                .clickAssetModelTab();

        // create data for removing in step 18
        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        AddFromReferenceDataPage addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedParentItem1 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedParentItem1).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();
        buildAssetModelPage.getItemByItemName(notAssociatedParentItem1).clickItemName();
        addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        notAssociatedChildItem1 = addFromReferenceDataPage.getReferenceItems()
                .stream().filter(ReferenceItemElement::isCheckBoxEnabled).findFirst().get().getReferenceItemName();
        addFromReferenceDataPage.getItemByItemName(notAssociatedChildItem1).clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();
        assetModelSubPage = buildAssetModelPage.clickFinishButton();

        notAssociatedParentItem1Id = new AssetRootItem(assetId).getDto().getRelated().stream()
                .filter(x -> new AssetItem(assetId, x.getId().intValue()).getDto().getName().equals(notAssociatedParentItem1))
                .findFirst()
                .get().getId().intValue();

        List<String> relatedItemNames = new AssetItem(assetId, notAssociatedParentItem1Id).getDto().getRelated()
                .stream()
                .map(a -> new AssetItem(assetId, a.getId().intValue()).getDto().getName())
                .map(b -> trimNameWithLength(b, 40))
                .collect(Collectors.toList());

        HierarchyViewSubPage hierarchyViewSubPage = assetModelSubPage.clickHierarchyViewButton();
        hierarchyViewSubPage.getItemBasedOnItemName(notAssociatedParentItem1).clickCollapseIcon();

        assert_().withFailureMessage("Hierarchy will expand to display all items which are connected under the selected " + notAssociatedParentItem1)
                .that(hierarchyViewSubPage.getItemBasedOnItemName((notAssociatedParentItem1)).getAssetItems()
                        .stream().map(assetItem -> TestDataHelper.truncateString(40, assetItem.getItemName()))
                        .collect(Collectors.toList()))
                .containsAllIn(relatedItemNames);

        cannotDeleteBodyMessage = "%s cannot be deleted because it is\n%s associated with services, defects or codicils";
        padding = "";

        //AC4 step 12
        ItemsDetailsPage itemsDetailsPage = hierarchyViewSubPage.getItemBasedOnItemName(associatedServiceParentItemName).clickItemAttributesLink();

        assert_().withFailureMessage("User is expected to navigate to the 'Item specific' page for " + associatedServiceParentItemName)
                .that(itemsDetailsPage.getItemPageTitleText())
                .contains("Item: " + associatedServiceParentItemName);
        ModalDiscardChangesSubPage modalDiscardChangesSubPage = itemsDetailsPage.clickRemoveItemButton();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(modalDiscardChangesSubPage.getHeader())
                .isEqualTo(cannotDeleteHeaderMessage);
        assert_().withFailureMessage(String.format(cannotDeleteBodyMessage, associatedServiceParentItemName, padding))
                .that(modalDiscardChangesSubPage.getBody())
                .isEqualTo(String.format(cannotDeleteBodyMessage, associatedServiceParentItemName, padding));
        modalDiscardChangesSubPage.clickOkButton();
        assetModelSubPage = itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class);
        //AC3 step 6
        itemsDetailsPage = assetModelSubPage.clickHierarchyViewButton().then()
                .getItemBasedOnItemName(associatedServiceParentItemName).clickCollapseIcon().then()
                .getItemBasedOnItemName(associatedServiceItemName).clickItemAttributesLink();
        assert_().withFailureMessage("User is expected to navigate to the 'Item specific' page for " + associatedServiceItemName)
                .that(itemsDetailsPage.getItemPageTitleText())
                .contains("Item: " + associatedServiceItemName);
        modalDiscardChangesSubPage = itemsDetailsPage.clickRemoveItemButton();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(modalDiscardChangesSubPage.getHeader())
                .isEqualTo(cannotDeleteHeaderMessage);
        assert_().withFailureMessage(String.format(cannotDeleteBodyMessage, associatedServiceItemName, padding))
                .that(modalDiscardChangesSubPage.getBody())
                .isEqualTo(String.format(cannotDeleteBodyMessage, associatedServiceItemName, padding));
        modalDiscardChangesSubPage.clickOkButton();
        assetModelSubPage = itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class);

        //AC3 step 15
        itemsDetailsPage = assetModelSubPage.clickHierarchyViewButton().then()
                .getItemBasedOnItemName(associatedOpenCocDefParentItemName).clickItemAttributesLink();
        assert_().withFailureMessage("User is expected to navigate to the 'Item specific' page for " + associatedOpenCocDefParentItemName)
                .that(itemsDetailsPage.getItemPageTitleText())
                .contains("Item: " + associatedOpenCocDefParentItemName);
        modalDiscardChangesSubPage = itemsDetailsPage.clickRemoveItemButton();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(modalDiscardChangesSubPage.getHeader())
                .isEqualTo(cannotDeleteHeaderMessage);
        assert_().withFailureMessage(String.format(cannotDeleteBodyMessage, associatedOpenCocDefParentItemName, padding))
                .that(modalDiscardChangesSubPage.getBody())
                .isEqualTo(String.format(cannotDeleteBodyMessage, associatedOpenCocDefParentItemName, padding));
        modalDiscardChangesSubPage.clickOkButton();
        assetModelSubPage = itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class);

        //AC3 step 9
        itemsDetailsPage = assetModelSubPage.clickHierarchyViewButton().then()
                .getItemBasedOnItemName(associatedOpenCocDefParentItemName).clickCollapseIcon().then()
                .getItemBasedOnItemName(associatedOpenCocDefItemName).clickItemAttributesLink();
        assert_().withFailureMessage("User is expected to navigate to the 'Item specific' page for " + associatedOpenCocDefItemName)
                .that(itemsDetailsPage.getItemPageTitleText())
                .contains("Item: " + associatedOpenCocDefItemName);
        modalDiscardChangesSubPage = itemsDetailsPage.clickRemoveItemButton();
        assert_().withFailureMessage(cannotDeleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(modalDiscardChangesSubPage.getHeader())
                .isEqualTo(cannotDeleteHeaderMessage);
        assert_().withFailureMessage(String.format(cannotDeleteBodyMessage, associatedOpenCocDefItemName, padding))
                .that(modalDiscardChangesSubPage.getBody())
                .isEqualTo(String.format(cannotDeleteBodyMessage, associatedOpenCocDefItemName, padding));
        modalDiscardChangesSubPage.clickOkButton();
        assetModelSubPage = itemsDetailsPage.clickNavigateBackButton(AssetModelSubPage.class);

        //AC5 step 18
        hierarchyViewSubPage = assetModelSubPage.clickHierarchyViewButton();
        itemsDetailsPage = hierarchyViewSubPage.getItemBasedOnItemName(notAssociatedParentItem1).clickCollapseIcon().then()
                .getItemBasedOnItemName(notAssociatedChildItem1).clickItemAttributesLink();
        assert_().withFailureMessage("User is expected to navigate to the 'Item specific' page for " + associatedOpenCocDefItemName)
                .that(itemsDetailsPage.getItemPageTitleText())
                .contains("Item: " + notAssociatedChildItem1);
        //AC4 step 19
        modalDiscardChangesSubPage = itemsDetailsPage.clickRemoveItemButton();
        assert_().withFailureMessage(deleteHeaderMessage + " is expected to be displayed in delete item message window")
                .that(modalDiscardChangesSubPage.getHeader())
                .isEqualTo(deleteHeaderMessage);

        assert_().withFailureMessage("[" + String.format(deleteBodyMessage, notAssociatedChildItem1) + "] is expected to be displayed")
                .that(modalDiscardChangesSubPage.getBody())
                .contains(String.format(deleteBodyMessage, notAssociatedChildItem1));

        //AC7 step 20
        modalDiscardChangesSubPage.clickDeleteItemButton();
        assert_().withFailureMessage("Parent item [" + notAssociatedParentItem1 + "] of deleted item is expected to be selected")
                .that(hierarchyViewSubPage.getItemBasedOnItemName(notAssociatedParentItem1).isItemSelected())
                .isTrue();

    }

    private String trimNameWithLength(String originalName, int maxLength)
    {
        if (originalName.length() > maxLength)
            return originalName.substring(0, maxLength + 1) + "…";
        else
            return originalName;

    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
