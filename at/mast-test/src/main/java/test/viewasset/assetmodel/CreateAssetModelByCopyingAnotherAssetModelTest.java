package test.viewasset.assetmodel;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.TestDataHelper;
import model.asset.Asset;
import model.asset.AssetQuery;
import model.referencedata.ReferenceDataMap;
import org.apache.commons.lang.WordUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static model.referencedata.ReferenceDataMap.RefData.ASSET_TYPES;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class CreateAssetModelByCopyingAnotherAssetModelTest extends BaseTest
{

    private HeaderPage headerPage;

    @Test(description = "US9.56 AC1-12 - Creating an asset model by copying another asset model")
    @Issue("LRT-1567")
    public void createAssetModelByCopyingAnotherAssetModelTest()
    {
        final int assetId = 501;
        //Step: 1
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        //Step: 2
        workHubPage.clickAllAssetsTab();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage.setAssetSearchInput(name)
                .then().clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement =
                allAssetsSubPage.getAssetCardByAssetName(name);

        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        AssetModelSubPage assetModelSubPage = viewAssetPage
                .getAssetModelSubPage();

        BuildAssetModelPage buildAssetModelPage =
                assetModelSubPage.clickCheckOutForEditingButton();

        //AC: 1
        //Step: 3
        CopyFromAssetModelPage copyFromAssetModelButtonSubPage =
                buildAssetModelPage.clickCopyFromAssetModelButton();

        AssetQueryDto assetQueryDto = new AssetQueryDto();
        assetQueryDto.setCategoryId(Collections.singletonList(asset.getDto().getAssetCategory().getId()));
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        List<AssetLightDto> assetNameDtos = assetQuery.getDto()
                .getContent().stream().sorted((d1, d2) ->
                        d1.getName().compareTo(d2.getName()))
                .filter(dto -> dto.getName() != null)
                .collect(Collectors.toList());

        ArrayList<String> assetName = assetNameDtos.stream()
                .map(assetNameDto -> WordUtils.capitalize(assetNameDto
                        .getName())).collect(Collectors.toCollection(ArrayList::new));

        //AC: 2
        //Step: 3
        for (int i = 0; i < copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed() - 1; i++)
        {
            allAssetsAssetElement = copyFromAssetModelButtonSubPage
                    .getAssetCardByAssetName(assetName.get(i));
            assert_().withFailureMessage("Expected that no assets should be selected")
                    .that(allAssetsAssetElement.isAssetSelected())
                    .isFalse();
        }

        //AC: 3
        //Step: 5
        if (assetQuery.getDto().getContent().size() > 12)
        {
            assert_().withFailureMessage("Default expected asset count should be displayed")
                    .that(copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed())
                    .isEqualTo(12);
        }
        else
        {
            assert_().withFailureMessage("Default expected asset count should be displayed")
                    .that(copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed())
                    .isEqualTo(assetQuery.getDto().getContent().size());
        }

        //AC: 4 and 5
        //Step: 6 and 7
        //No assertion needed, set to @Visible

        //AC: 6 and AC: 7
        //Step: 8, 9 and 10
        copyFromAssetModelButtonSubPage.clickNavigateBackButton();
        List<DataElement> items = new ArrayList<>();

        for (int i = 0; i < 3; i++)
        {
            items = buildAssetModelPage.getTableItems();
            List<String> buildItemNames = items.stream()
                    .map(DataElement::getItemName)
                    .collect(Collectors.toList());

            DataElement buildAssetModelElement = items.get(i);
            buildAssetModelElement.clickItemName();
            buildAssetModelPage.clickCopyFromAssetModelButton();

            assert_().withFailureMessage("Expected breadcrumbs name should be displayed")
                    .that(copyFromAssetModelButtonSubPage
                            .getBreadcrumbTextByIndex(copyFromAssetModelButtonSubPage.getBreadCrumbTextSize() - 1))
                    .isEqualTo(buildItemNames.get(i));

            assert_().withFailureMessage("Breadcrumbs should not clickable")
                    .that(copyFromAssetModelButtonSubPage
                            .isBreadcrumbClickable(copyFromAssetModelButtonSubPage.getBreadCrumbTextSize() - 1))
                    .isFalse();

            copyFromAssetModelButtonSubPage.clickNavigateBackButton();
        }
        buildAssetModelPage.clickCopyFromAssetModelButton();

        //AC: 8
        //Step: 11
        for (int i = 0; i < copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed() - 1; i++)
        {

            allAssetsAssetElement = copyFromAssetModelButtonSubPage
                    .getAssetCardByAssetName(assetName.get(i));

            int finalI = i;
            AssetTypeDto assetTypeDto =
                    (AssetTypeDto) new ReferenceDataMap(ASSET, ASSET_TYPES).getReferenceDataAsClass(AssetTypeDto.class)
                            .stream()
                            .filter(
                                    assetType -> ((AssetTypeDto) assetType).getId()
                                            .equals(assetNameDtos.get(finalI).getAssetType().getId())
                            ).findFirst().orElse(null);

            assert_().withFailureMessage("Expected asset name should be displayed")
                    .that(allAssetsAssetElement.getAssetName())
                    .isEqualTo(assetName.get(i));

            assert_().withFailureMessage("Expected asset type should be displayed")
                    .that(allAssetsAssetElement.getAssetType())
                    .isEqualTo(assetTypeDto.getName());

            if (assetNameDtos.get(i).getIhsAsset() != null)
            {
                assert_().withFailureMessage("Expected IMO number should be displayed")
                        .that(allAssetsAssetElement.getImoNumber())
                        .isEqualTo(assetNameDtos
                                .get(i).getIhsAsset().getId().toString());
            }

            assert_().withFailureMessage("Expected gross tonnage should be displayed")
                    .that(Long.parseLong(allAssetsAssetElement.getGrossTonnage()))
                    .isEqualTo(
                            (assetNameDtos.get(i).getGrossTonnage() != null)
                                    ? assetNameDtos.get(i).getGrossTonnage().longValue() : 0L
                    );

            LinkResource classStatusId = assetNameDtos.get(i).getClassStatus();
            assert_().withFailureMessage("Expected class status should be displayed")
                    .that(allAssetsAssetElement.getClassStatus())
                    .isEqualTo(
                            (classStatusId != null)
                                    ? TestDataHelper.getClassStatusById(classStatusId.getId().intValue()) : null
                    );
        }

        //AC: 9
        //Step: 12
        assert_().withFailureMessage("Assets displayed against the available " +
                "number").that(copyFromAssetModelButtonSubPage.getPaginationCountPage()
                .getPaginationCountText())
                .isNotEmpty();

        //AC: 10
        //Step: 13
        assert_().withFailureMessage("Asset name A-Z is Expected to be present ")
                .that(copyFromAssetModelButtonSubPage
                        .isSortDropDownTextDisplayed("Asset name A-Z")).isTrue();

        assert_().withFailureMessage("Asset name Z-A is Expected to be present")
                .that(copyFromAssetModelButtonSubPage
                        .isSortDropDownTextDisplayed("Asset name Z-A")).isTrue();

        assert_().withFailureMessage("Build date ASC is Expected to be present")
                .that(copyFromAssetModelButtonSubPage
                        .isSortDropDownTextDisplayed("Build date Ascending")).isTrue();

        assert_().withFailureMessage("Build date DESC is Expected to be present")
                .that(copyFromAssetModelButtonSubPage
                        .isSortDropDownTextDisplayed("Build date Descending")).isTrue();

        //AC: 11
        //Step: 14
        copyFromAssetModelButtonSubPage.clickNavigateBackButton();
        buildAssetModelPage.clickCopyFromAssetModelButton();

        for (int i = 0; i < copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed() - 1; i++)
        {
            allAssetsAssetElement = copyFromAssetModelButtonSubPage.getAssetCardByAssetName(assetName.get(i));
            assert_().withFailureMessage("Preview Button is expected to be displayed")
                    .that(allAssetsAssetElement
                            .isPreviewButtonDisplayed()).isTrue();

            assert_().withFailureMessage("Copy Button is expected to be displayed")
                    .that(allAssetsAssetElement
                            .isCopyButtonDisplayed()).isTrue();
        }

        //AC: 12
        //Step: 15
        copyFromAssetModelButtonSubPage.clickNavigateBackButton();
        DataElement buildAssetModelElement = items.get(0);
        buildAssetModelElement.clickItemName();
        buildAssetModelPage.clickCopyFromAssetModelButton();
        for (int i = 0; i < copyFromAssetModelButtonSubPage.getNumberOfCardsDisplayed() - 1; i++)
        {

            allAssetsAssetElement = copyFromAssetModelButtonSubPage.getAssetCardByAssetName(assetName.get(i));
            assert_().withFailureMessage("Preview Button is expected to be displayed")
                    .that(allAssetsAssetElement
                            .isPreviewButtonDisplayed()).isTrue();
        }
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
