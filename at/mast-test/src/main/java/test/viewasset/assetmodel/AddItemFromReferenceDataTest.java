package test.viewasset.assetmodel;

import com.fasterxml.jackson.databind.deser.Deserializers;
import com.frameworkium.core.ui.tests.BaseTest;
import com.mysql.fabric.xmlrpc.base.Data;
import header.HeaderPage;
import helper.TestDataHelper;
import model.asset.Asset;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.BuildAssetModelElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import viewasset.sub.codicilsanddefects.actionableitems.base.BaseTable;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AddItemFromReferenceDataTest extends BaseTest
{

    private HeaderPage headerPage;

    @Test(description = "US9.61 - Add item(s) from reference data")
    @Issue("LRT-1530")
    public void addItemFormReferenceData() throws SQLException
    {

        final int assetId = 501;
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();
        SearchFilterSubPage searchFilterSubPage = workHubPage
                .clickSearchFilterAssetsButton();

        Asset asset = new Asset(assetId);
        String name = asset.getDto().getName();

        searchFilterSubPage
                .setAssetSearchInput(name)
                .clickAssetSearchApplyButton(ApplySubPage.class);

        AllAssetsSubPage allAssetsSubPage = workHubPage
                .clickAllAssetsTab();
        AllAssetsAssetElement allAssetsAssetElement = allAssetsSubPage
                .getAssetCardByAssetName(name);

        AssetModelSubPage assetModelSubPage = allAssetsAssetElement
                .clickViewAssetButton()
                .clickAssetModelTab();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage
                .clickCheckOutForEditingButton();

        String rootItemName = "VESSEL";
        buildAssetModelPage
                .getRootItem()
                .clickItemName();
        AddFromReferenceDataPage referenceData = buildAssetModelPage
                .clickAddFromReferenceDataButton();

        List<String> itemName = TestDataHelper.getItemsFromReferenceData(assetId, rootItemName);

        //AC2
        assert_().withFailureMessage("The breadcrumb text is not what was expected")
                .that(referenceData.getBreadcrumbTextByIndex(0))
                .isEqualTo(rootItemName);

        //AC1
        List<String> referenceItemElementNames = referenceData
                .getReferenceItems()
                .stream()
                .map(ReferenceItemElement::getReferenceItemName)
                .collect(Collectors.toList());
        int i = 1;
        for (String item : itemName)
        {
            assert_().withFailureMessage("The expected reference item is not shown")
                    .that(item).isIn(referenceItemElementNames);
            i++;
        }

        referenceData.getItemByItemName("ANCHORING, MOORING AND TETHERING GROUP")
                .clickCheckbox();

        BuildAssetModelPage buildAssetModel = referenceData
                .clickAddSelected();

        DataElement buildAssetModelElement = buildAssetModel
                .getItemByItemName(rootItemName);

        buildAssetModelElement
                .clickItemName();

        referenceData = buildAssetModel
                .clickAddFromReferenceDataButton();

        assert_().withFailureMessage("The breadcrumb text is not what was expected")
                .that(referenceData.getBreadcrumbTextByIndex(1)).isEqualTo(rootItemName);

        itemName = TestDataHelper.getItemsFromReferenceData(assetId, rootItemName);

        i = 1;
        for (String item : itemName)
        {
            assert_().withFailureMessage("The expected reference item is not shown")
                    .that(item).isEqualTo(referenceData.getItemByIndex(i).getReferenceItemName());
            i++;
        }

        //AC4
        referenceData.getItemByItemName("DECK GROUP")
                .clickCheckbox();

        referenceData.getItemByItemName("ANCHORING, MOORING AND TETHERING GROUP")
                .clickCheckbox();

        //AC5
        assert_().withFailureMessage("The add selected button is not displayed")
                .that(referenceData.isAddSelectedDisplayed())
                .isTrue();

        assert_().withFailureMessage("The cancel button is not displayed")
                .that(referenceData.isCancelButtonDisplayed())
                .isTrue();

        //AC6
        assert_().withFailureMessage("The expected number of item types does not match what was found")
                .that(referenceData.getNumItemTypes())
                .isEqualTo("2");

        //AC7, AC9
        buildAssetModel = referenceData
                .clickBackButton();

        assert_().withFailureMessage("The selected items should not be seen in this list as the user cancelled the action")
                .that(buildAssetModel.getItemByItemName("DECK GROUP"))
                .isNull();
        assert_().withFailureMessage("The selected items should not be seen in this list as the user cancelled the action")
                .that(buildAssetModel.getItemByItemName("ANCHORING, MOORING AND TETHERING GROUP"))
                .isNull();

        //AC8, AC9
        buildAssetModel.getItemByItemName(rootItemName)
                .clickItemName();

        referenceData = buildAssetModel
                .clickAddFromReferenceDataButton();

        referenceData.getItemByItemName("DECK GROUP")
                .clickCheckbox();

        referenceData.getItemByItemName("ANCHORING, MOORING AND TETHERING GROUP")
                .clickCheckbox();

        buildAssetModel = referenceData
                .clickAddSelected();

        //AC10, AC11
        buildAssetModel.getItemByItemName(rootItemName)
                .clickItemName();

        assert_().withFailureMessage("The selected items should be seen in this list")
                .that(buildAssetModel.getItemByItemName("DECK GROUP").isDisplayed())
                .isTrue();

        assert_().withFailureMessage("The selected items should be seen in this list")
                .that(buildAssetModel.getItemByItemName("ANCHORING, MOORING AND TETHERING GROUP").isDisplayed())
                .isTrue();

        //AC12

        referenceData = buildAssetModel
                .clickAddFromReferenceDataButton();

        referenceData.getItemByItemName("TANK GROUP")
                .clickCheckbox();

        buildAssetModel = referenceData
                .clickCancelButton();

        buildAssetModel.getItemByItemName(rootItemName)
                .clickExpandIcon();

        assert_().withFailureMessage("The selected items should not be seen in this list as the user cancelled the action")
                .that(buildAssetModel.getItemByItemName("TANK GROUP"))
                .isNull();
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
