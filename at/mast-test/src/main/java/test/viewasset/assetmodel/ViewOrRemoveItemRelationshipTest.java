package test.viewasset.assetmodel;

import com.frameworkium.core.ui.tests.BaseTest;
import model.asset.Asset;
import model.asset.AssetItem;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.relationship.RelationshipsSubPage;
import viewasset.sub.AssetModelSubPage;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;

import static com.google.common.truth.Truth.assert_;

// todo temporarily commented Tests that use are broken by structural changes
// and merge is required to unblock stories, these should be refactored to the new structure asap
// see: TomB
public class ViewOrRemoveItemRelationshipTest extends BaseTest
{

    @DataProvider(name = "item")
    public Object[][] item()
    {
        return new Object[][]{
                {428, 31}
        };
    }

    //    @Test(description = "Story 8.28 - As a user I want to view and/ or remove relationships for a specific item"
    //            + "so that I can ensure that one or more items are correctly related.",
    //            dataProvider = "item")
    //    @Issue("LRT-1293")
    //    public void viewOrRemoveItemRelationshipTest(int assetId, int itemId) {
    //        WorkHubPage workHubPage = WorkHubPage.open();
    //        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
    //        workHubPage.clickSearchFilterAssetsButton().then()
    //                .setAssetSearchInput(String.valueOf(assetId)).then()
    //                .clickAssetSearchApplyButton();
    //
    //        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
    //                .clickViewAssetButton().clickAssetModelTab() ;
    //
    //        String itemName = new AssetItem(assetId, itemId).getDto().getName();
    //        ItemsDetailsPage itemsDetailsPage = assetModelSubPage
    //                .clickSearchWithinAssetModelButton()
    //                .setItemSearchInput(itemName)
    //                .clickSearchButton()
    //                .clickArrowToItemDetailPageBaseOnItemName(itemName);
    //
    //        assert_().withFailureMessage("Specific item detail page for [" + itemName + "] is expected to display."  )
    //                .that(itemsDetailsPage.getItemNameText()).isEqualTo(itemName) ;
    //        assert_().withFailureMessage("Attributes tab is selected by default.")
    //                .that(itemsDetailsPage.getSelectedActiveTabName()).isEqualTo("Attributes");
    //
    //        AttributesSubPage attributesSubPage = itemsDetailsPage.getAttributesTab() ;
    //
    //        List<String> allDisplayedAttributeNames = attributesSubPage.getAllDisplayedAttributeElementNames()   ;
    //        List<AttributeDto> attributeDtos = new AssetItemAttributePage(assetId, itemId).getDto().getContent();
    //        assert_().withFailureMessage("Number of all displayed attributes and backend are tally for item [" + itemName + "]")
    //                .that(allDisplayedAttributeNames.size())
    //                .isEqualTo(attributeDtos.size());
    //
    //        RelationshipsSubPage relationshipsSubPage =  itemsDetailsPage.clickRelationshipsTab();
    //
    //        List<String> itemRelationshipDtoRelatedItemNames = new AssetItem(assetId, itemId).getDto().getRelated()
    //                .stream()
    //                .filter(itemRelationshipDto -> itemRelationshipDto.getType().getId() == 9)
    //                .map(ItemRelationshipDto::getToItem)
    //                .map(y -> getItemNameById(assetId, y.getId().intValue()))
    //                .sorted( (dto1, dto2) -> dto1.compareTo(dto2))
    //                .collect(Collectors.toList()) ;
    //
    //        //AC1 AC2
    //        assert_().withFailureMessage("Number of displayed related items aren't equal to BE.")
    //                .that(relationshipsSubPage.getAllDisplayedRelatedItemElement().size())
    //                .isEqualTo(itemRelationshipDtoRelatedItemNames.size());
    //
    //        //AC3
    //        assert_().withFailureMessage("Number of related items display in show count label is expected to be the same with BE.")
    //                .that(relationshipsSubPage.getNumberOfRelatedItem())
    //                .isEqualTo(itemRelationshipDtoRelatedItemNames.size());
    //
    //        //AC4 AC5 AC6
    //        assert_().withFailureMessage("All added related items are displayed by default according to item name.")
    //                .that(relationshipsSubPage.getAllDisplayedRelatedItemElementNames())
    //                .containsExactlyElementsIn(itemRelationshipDtoRelatedItemNames);
    //
    //        //AC5
    //        relationshipsSubPage.getAllDisplayedRelatedItemElement()
    //                .forEach(
    //                        oneElement -> {
    //                            assert_().withFailureMessage("Item [" + oneElement.getRelatedItemNameText() + "] is expected to be displayed")
    //                                    .that(oneElement.isRelatedItemNameTextDisplayed())
    //                                    .isTrue();
    //
    //                            assert_().withFailureMessage("Delete link for item [" + oneElement.getRelatedItemNameText() + "] is expected to be displayed")
    //                                    .that(oneElement.isRelatedItemDeleteLinkDisplayed())
    //                                    .isTrue();
    //                        }
    //                );
    //
    //        //AC7 AC9 AC10
    //        String selectedRelatedItemName = relationshipsSubPage.getAllDisplayedRelatedItemElement().get(0).getRelatedItemNameText();
    //        relationshipsSubPage = relationshipsSubPage.getAllDisplayedRelatedItemElement().get(0).clickDeleteItemRelationshipLink();
    //        assert_().withFailureMessage("[" +  selectedRelatedItemName + "] Removed item is not expected to be displayed")
    //                .that(relationshipsSubPage.getAllDisplayedRelatedItemElementNames().stream().anyMatch(x -> x.equals(selectedRelatedItemName)))
    //                .isFalse();
    //
    //    }

    @DataProvider(name = "itemWithoutRelatedItem")
    public Object[][] itemWithoutRelatedItem()
    {
        return new Object[][]{
                {501, 688}
        };
    }

    @Test(description = "Story 8.28 - As a user I want to view and/ or remove relationships for a specific item"
            + "so that I can ensure that one or more items are correctly related.",
            dataProvider = "itemWithoutRelatedItem")
    @Issue("LRT-1294")
    public void ViewItemWithoutRelatedItemTest(int assetId, int itemId)
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        workHubPage.clickSearchFilterAssetsButton().then()
                .setAssetSearchInput(String.valueOf(assetId)).then()
                .clickAssetSearchApplyButton();

        AssetModelSubPage assetModelSubPage = allAssetsSubPage.getAssetCardByAssetName(new Asset(assetId).getDto().getName())
                .clickViewAssetButton().clickAssetModelTab();

        String itemName = new AssetItem(assetId, itemId).getDto().getName();
        ItemsDetailsPage itemsDetailsPage = assetModelSubPage.clickListViewButton().getItemByItemName(itemName).clickFurtherDetailsArrow();
        RelationshipsSubPage relationshipsSubPage = itemsDetailsPage.clickRelationshipsTab();

        //AC8
        assert_().withFailureMessage("Message [No relationships have been added to this item] is expected to be displayed")
                .that(relationshipsSubPage.getRelatedItemsCountLabelText())
                .isEqualTo("No relationships have been added to this item");
    }

    private String getItemNameById(int assetId, int itemId)
    {
        return new AssetItem(assetId, itemId).getDto().getName();
    }
}
