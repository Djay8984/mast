package test;

import com.frameworkium.core.ui.tests.BaseTest;
import main.element.AssetCard;
import main.landing.LandingPage;
import main.landing.vessellist.VesselListPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.ClassDirect.FRONTEND_TIME_FORMAT_DTS;
import static constant.ClassDirect.localDate;
import static helper.TestDateHelper.toDate;

public class FleetDashBoardDefaultViewTest extends BaseTest
{
    final String fromDate = FRONTEND_TIME_FORMAT_DTS.format(localDate.minusDays(600));
    final String toDate = FRONTEND_TIME_FORMAT_DTS.format(localDate.minusDays(3));

    @Test(description = "Default view_All_no assets marked as Favourite and filter assets by date of build")
    @Issue("LRCDT-592")

    public void fleetDashBoardDefaultViewTest()
    {
        //Open the app set "From Date of Build" under Filter Assets side Bar and perform search
        VesselListPage vesselListPage = LandingPage.open()
                .getVesselListPage()
                .getFilterPage()
                .clickFilterAssertButton()
                .setFromDateOfBuild(fromDate)
                .clickFilterAssertButton(VesselListPage.class);

        List<Date> buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The FROM date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified From date of build ")
                .that(e.equals(toDate(localDate.minusDays(900)))
                        || e.after(toDate(localDate.minusDays(900))))
                .isTrue());

        //Set "To Date of Build" under Filter Assets side Bar and perform search
        vesselListPage.getFilterPage()
                .clickFilterAssertButton()
                .setFromDateOfBuild("")
                .setToDateOfBuild(toDate)
                .clickFilterAssertButton(VesselListPage.class);

        buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The TO date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified To date of build ")
                .that(e.equals(toDate(localDate.minusDays(3)))
                        || e.before(toDate(localDate.minusDays(3))))
                .isTrue());

        //Set "From Date of Build" and "To Date of Build" under Filter Assets side Bar and perform search
        vesselListPage.getFilterPage()
                .clickFilterAssertButton()
                .setFromDateOfBuild(fromDate)
                .setToDateOfBuild(toDate)
                .clickFilterAssertButton(VesselListPage.class);

        buildDates = getFilteredBuildDates(vesselListPage);
        assert_().withFailureMessage("The FROM and TO date search criteria should return results")
                .that(buildDates.size() > 0)
                .isTrue();
        buildDates.forEach(e -> assert_().withFailureMessage("Displayed Assets Build Date are expected to be with specified date of build ")
                .that(e.equals(toDate(localDate.minusDays(900)))
                        || e.after(toDate(localDate.minusDays(900)))
                        || e.equals(toDate(localDate.minusDays(3)))
                        || e.before(toDate(localDate.minusDays(3))))
                .isTrue());
    }

    private List<Date> getFilteredBuildDates(VesselListPage vesselListPage)
    {
        return vesselListPage.getAssetCards()
                .stream()
                .filter(e -> e.getDateOfBuild() != null)
                .map(AssetCard::getDateOfBuild)
                .collect(Collectors.toList());
    }
}
