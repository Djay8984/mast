package release3test;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import model.asset.AssetQuery;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.PaginationCountSubPage;
import workhub.sub.SearchFilterSubPage;
import workhub.sub.searchfilter.LifecycleStatusFilterSubPage;

import java.util.ArrayList;
import java.util.Arrays;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.expectedLifecycleStatusList;

public class ResetSearchTest extends BaseTest
{
    @Test(description = "US DB.09: Reset Search")
    @Issue("LRD-5784")

    public void resetSearchTest()
    {
        //Get All assets from BE
        AssetQueryDto assetQueryDto = AssetQuery.getDefaultDto();
        AssetQuery assetQuery = new AssetQuery(assetQueryDto);
        int totalCountDefault = assetQuery.getDto().getContent().size();

        //Check that FE has expected LifeCycleStatusList
        ArrayList<String> lifecycleStatusesText = TestDataHelper.getLifecycleStatusesNames();
        assert_()
                .withFailureMessage(("Life Cycle status  are expected to have these values: " +
                        Arrays.asList(expectedLifecycleStatusList)))
                .that(lifecycleStatusesText)
                .isEqualTo(Arrays.asList(expectedLifecycleStatusList));

        //Open the App and click on All Assets Tab
        WorkHubPage workHubPage = WorkHubPage.open();
        AllAssetsSubPage allAssetsSubPage = workHubPage.clickAllAssetsTab();
        int count = allAssetsSubPage.getAssetCards().size();
        PaginationCountSubPage paginationCountSubPage
                = allAssetsSubPage.getPaginationCountPage();

        //Check for assets displayed in FE matched to that of BE
        assert_().withFailureMessage("Shown count is expected to be " + count)
                .that(paginationCountSubPage.getShownAmount())
                .isEqualTo(count);
        assert_().withFailureMessage("Total Count is expected to be " + totalCountDefault)
                .that(paginationCountSubPage.getTotalAmount())
                .isEqualTo(totalCountDefault);

        //Apply Filter
        SearchFilterSubPage searchFilterSubPage
                = workHubPage.clickSearchFilterAssetsButton();
        assert_().withFailureMessage("Filter icon is not expected to be displayed")
                .that(searchFilterSubPage.isFilterIconDisplayed())
                .isFalse();

        LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage
                = searchFilterSubPage.clickLifecycleStatusFilterButton();
        for (int i = 0; i < 2; i++)
        {
            lifecycleStatusFilterSubPage.deSelectLifecycleStatusesCheckBoxes(lifecycleStatusesText.get(i));
        }
        assert_().withFailureMessage("Filter icon is expected to be displayed")
                .that(searchFilterSubPage.isFilterIconDisplayed())
                .isTrue();

        //Get Asset From BE for filtered LifecycleStatus
        assetQueryDto.setLifecycleStatusId(Arrays.asList(3L, 4L, 5L, 6L, 7L, 8L, 10L));
        assetQuery = new AssetQuery(assetQueryDto);
        int totalCountFilter = assetQuery.getDto().getContent().size();
        allAssetsSubPage = searchFilterSubPage.clickAssetSearchApplyButton(AllAssetsSubPage.class);
        count = allAssetsSubPage.getAssetCards().size();
        paginationCountSubPage = allAssetsSubPage.getPaginationCountPage();

        //Check for assets displayed in FE matched to that of BE
        assert_().withFailureMessage("Shown count is expected to be " + count)
                .that(paginationCountSubPage.getShownAmount())
                .isEqualTo(count);
        assert_().withFailureMessage("Total Count is expected to be " + totalCountFilter)
                .that(paginationCountSubPage.getTotalAmount())
                .isEqualTo(totalCountFilter);

        //Reset the Filter
        workHubPage.clickSearchFilterAssetsButton();
        searchFilterSubPage.clickAssetSearchResetButton();
        count = allAssetsSubPage.getAssetCards().size();
        paginationCountSubPage = allAssetsSubPage.getPaginationCountPage();

        //Check for default assets are displayed
        assert_().withFailureMessage("Shown count is expected to be " + count)
                .that(paginationCountSubPage.getShownAmount())
                .isEqualTo(count);
        assert_().withFailureMessage("Total Count is expected to be " + totalCountDefault)
                .that(paginationCountSubPage.getTotalAmount())
                .isEqualTo(totalCountDefault);
        searchFilterSubPage = workHubPage.clickSearchFilterAssetsButton();
        assert_().withFailureMessage("Filter icon is not expected to be displayed")
                .that(searchFilterSubPage.isFilterIconDisplayed())
                .isFalse();
    }
}
