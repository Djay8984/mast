package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.modal.AddConfirmationPage;
import viewasset.sub.codicilsanddefects.element.DataElement;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.CocData.description;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static constant.LloydsRegister.randomiseConstant;
import static java.time.LocalDate.now;

public class CreateCocAiTest extends BaseTest
{

    private final static String status = "Open";
    private final String title = randomiseConstant(LloydsRegister.CocData.title);

    @Test(description = "R2 User Journey 19: Create and record Defects, Codicils and Conditions of Class")
    @Issue("LRD-3925")
    public void createCocAiTest()
    {

        CodicilsAndDefectsPage codicilsAndDefectsPage
                = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCodicilsAndDefectsTab()
                .clickCocAddNewButton()
                .setTitle(title)
                .setDescription(description)
                .selectInheritedCheckBox()
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddConfirmationPage.class)
                .clickOkButton();

        DataElement dataElement
                = codicilsAndDefectsPage
                .getCocTable().getRows()
                .stream()
                .filter(e -> e.getItemName().contains(title))
                .findFirst()
                .get();
        assert_().withFailureMessage("Added Conditions of Class Title is expected to be displayed")
                .that(dataElement.getItemName().contains(title))
                .isTrue();

        //TODO: Uncomment once LRD-2878 is resolved
        /*
        assert_().withFailureMessage("Added Conditions of Class Imposed Date is expected to be displayed")
                .that(dataElement.getImposeDate())
                .isEqualTo(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        assert_().withFailureMessage("Added Conditions of Class Due Date is expected to be displayed")
                .that(dataElement.getDueDate())
                .isEqualTo(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        */

        assert_().withFailureMessage("Added Conditions of Class Status is expected to be displayed")
                .that(dataElement.getStatus())
                .isEqualTo(status);

        codicilsAndDefectsPage.clickActionableItemAddNewButton()
                .setTitle(title)
                .clickClassButton()
                .setDescription(description)
                .setSurveyorTextInput("Surveyor guidance " + description)
                .setImposedDate(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS))
                .clickAllButton()
                .clickSaveButton(AddConfirmationPage.class)
                .clickOkButton();

        dataElement = codicilsAndDefectsPage
                .getActionableItemsTable()
                .getRows()
                .stream()
                .filter(e -> e.getItemName()
                        .contains(title))
                .findFirst()
                .get();

        assert_().withFailureMessage("Added Actionable Item Title is expected to be displayed")
                .that(dataElement.getItemName().contains(title))
                .isTrue();

        //TODO: Uncomment once LRD-2878 is resolved
        /*
        assert_().withFailureMessage("Added Actionable Item Imposed Date is expected to be displayed")
                .that(dataElement.getImposeDate())
                .isEqualTo(now().plusDays(1)
                        .format(FRONTEND_TIME_FORMAT_DTS));

        assert_().withFailureMessage("Added Actionable Item Due Date is expected to be displayed")
                .that(dataElement.getDueDate())
                .isEqualTo(now().plusDays(2)
                        .format(FRONTEND_TIME_FORMAT_DTS));
        */

        assert_().withFailureMessage("Added Actionable Item Status is expected to be displayed")
                .that(dataElement.getStatus())
                .isEqualTo(status);
    }
}
