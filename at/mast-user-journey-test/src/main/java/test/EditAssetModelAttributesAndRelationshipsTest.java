package test;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.relationship.AddRelatedItemPage;
import viewasset.item.relationship.RelationshipsSubPage;
import viewasset.item.relationship.element.RelatedItemElement;
import viewasset.item.relationship.element.RelatedItemOptionElement;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class EditAssetModelAttributesAndRelationshipsTest extends BaseTest
{

    private HeaderPage headerPage;

    final String editAttributeValue = "Test KL" + RandomStringUtils.randomAlphabetic(10);

    @Test(description = "R2 User Journey 7: Edit an Asset Model by modifying items and attributes and relationships")
    @Issue("LRD-3895")
    public void editAssetModelAttributesAndRelationshipsTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        AssetModelSubPage assetModelSubPage
                = workHubPage
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickAssetModelTab();
        BuildAssetModelPage buildAssetModelPage
                = assetModelSubPage.clickCheckOutForEditingButton();
        AddFromReferenceDataPage addFromReferenceDataPage
                = buildAssetModelPage.clickAddFromReferenceDataButton();
        addFromReferenceDataPage
                .getReferenceItems()
                .forEach(ReferenceItemElement::clickCheckbox);

        addFromReferenceDataPage
                .clickAddSelected()
                .getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickItemName();
        buildAssetModelPage.clickAddFromReferenceDataButton();

        addFromReferenceDataPage
                .getReferenceItems()
                .forEach(ReferenceItemElement::clickCheckbox);
        addFromReferenceDataPage
                .clickAddSelected()
                .clickFinishButton();

        ItemsDetailsPage itemsDetailsPage
                = assetModelSubPage
                .getListViewSubPage()
                .getAssetItems()
                .get(0)
                .clickItemName()
                .getAssetItems()
                .get(0)
                .clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = itemsDetailsPage.clickAttributesTab();
        attributesSubPage.getAllDisplayedAttributeElement()
                .get(0)
                .clearAttributeValue()
                .setAttributeValue(editAttributeValue);

        attributesSubPage.clickSaveChangesButton().clickOkButton();

        AddRelatedItemPage addRelatedItemPage
                = itemsDetailsPage
                .clickRelationshipsTab()
                .clickCreateNewRelationshipButton();
        List<RelatedItemOptionElement> relatedItemOptionElement
                = addRelatedItemPage
                .getAllRelatedItemOptionElements();

        List<RelatedItemOptionElement> children = relatedItemOptionElement
                .get(0)
                .clickItemNode()
                .getChildItems();
        String relationshipItem = children.get(1)
                .getItemName();
        children
                .get(1)
                .selectItemRadioButton();

        RelationshipsSubPage relationshipsSubPage
                = addRelatedItemPage
                .clickCreateRelationshipButton();

        List<RelatedItemElement> relatedItemElements
                = relationshipsSubPage
                .getAllDisplayedRelatedItemElement();

        assert_().withFailureMessage("Added Relationship is expected to be displayed ")
                .that(relatedItemElements
                        .stream()
                        .filter(e -> e.getRelatedItemNameText().equals(relationshipItem))
                        .findFirst()
                        .get().getRelatedItemNameText())
                .isEqualTo(relationshipItem);

        relatedItemElements
                .stream()
                .filter(e -> e.getRelatedItemNameText().equals(relationshipItem))
                .findFirst()
                .get()
                .clickDeleteItemRelationshipLink();

        assert_().withFailureMessage("Edited Attribute Value is expected to be displayed")
                .that(itemsDetailsPage.clickAttributesTab()
                        .getAllDisplayedAttributeElement()
                        .get(0)
                        .getAttributeValue())
                .isEqualTo(editAttributeValue);

        relatedItemElements = itemsDetailsPage
                .clickRelationshipsTab()
                .getAllDisplayedRelatedItemElement();
        assert_().withFailureMessage("Deleted Relationship is not expected to be displayed ")
                .that(relatedItemElements
                        .stream()
                        .filter(e -> e.getRelatedItemNameText().equals(relationshipItem))
                        .count())
                .isEqualTo(0);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
