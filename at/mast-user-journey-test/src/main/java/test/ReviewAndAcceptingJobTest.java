package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.DARPage;
import viewasset.sub.jobs.details.crediting.DSRPage;
import viewasset.sub.jobs.details.crediting.FARPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;

public class ReviewAndAcceptingJobTest extends BaseTest
{
    final String expectedStatus = "Approved";
    String leadSurveyor = "James Rolf SDO";

    @Test(description = "R2 User Journey 17: Review and Accepting a Job")
    @Issue("LRD-3923")
    public void reviewAndAcceptingJobTest()
    {

        ServiceAllPage serviceAllPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        ViewJobDetailsPage viewJobDetailsPage =
                serviceAllPage
                        .getFooter()
                        .clickAddButton(ConfirmModalWindow.class)
                        .clickOkButton(JobScopePage.class)
                        .clickConfirmJobScopeButton()
                        .clickConfirmScopeButton()
                        .clickGoToCreditingButton()
                        //TODO: Bug LRD-4784
                        .clickGenerateDarButton()
                        .setDate(LloydsRegister.localDate.format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                        .clickGenerateDarButton()
                        .clickSubmitDarButton()
                        .clickOkButton(DARPage.class)
                        .getHeaderPage()
                        .clickNavigateBackButton()
                        .clickGoToCreditingButton()
                        .clickGenerateFarButton()
                        .setDate(LloydsRegister.localDate.plusDays(1).format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                        .clickGenerateFarButton()
                        .clickSubmitFarButton()
                        .clickOkButton(FARPage.class)
                        .getHeaderPage()
                        .clickNavigateBackButton()
                        .clickGenerateDsrButton()
                        .clickSubmitDsrButton()
                        .clickSubmitDsrButton()
                        .clickOkButton(DSRPage.class)
                        .getHeaderPage()
                        .clickNavigateBackButton()
                        .clickAddOrGoToJobTeamButton()
                        .setLeadSurveyorTextBox(leadSurveyor)
                        .setAuthorisingSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                        .clickSaveButton()
                        .clickApproveTechnicalReviewButton()
                        .clickApproveDSRButton();

        assert_().withFailureMessage("FSR Status is expected to be " + expectedStatus)
                .that(viewJobDetailsPage.getFSRStatus())
                .isEqualTo(expectedStatus);
        assert_().withFailureMessage("Technical review status is expected to be " + expectedStatus)
                .that(viewJobDetailsPage.getTechnicalReviewStatus())
                .isEqualTo(expectedStatus);
        assert_().withFailureMessage("DSR Status is expected to be " + expectedStatus)
                .that(viewJobDetailsPage.getDSRStatus())
                .isEqualTo(expectedStatus);

    }
}
