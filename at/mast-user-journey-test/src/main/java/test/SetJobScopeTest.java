package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ServiceElement;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;

public class SetJobScopeTest extends BaseTest
{

    @Test(description = "R2 User Journey 12: Set and Save the Scope of the Job")
    @Issue("LRD-3901")

    public void setSaveJobScopeTest()
    {
        ServiceAllPage serviceAllPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        ServiceElement serviceElement = serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0);
        String expectedService = serviceElement.getServiceName();
        serviceElement.selectCheckBox();

        CreditingPage creditingPage =
                serviceAllPage
                        .getFooter()
                        .clickAddButton(ConfirmModalWindow.class)
                        .clickOkButton(JobScopePage.class)
                        .clickConfirmJobScopeButton()
                        .clickConfirmScopeButton()
                        .clickGoToCreditingButton();

        assert_().withFailureMessage("Added Service Type is expected to be displayed")
                .that(creditingPage.getStatutoryTable()
                        .getRows()
                        .get(0)
                        .getServiceType())
                .isEqualTo(expectedService);
    }
}
