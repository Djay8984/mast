package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.sub.IMORegisteredAssetPage;
import workhub.sub.sub.util.ImoAsset;

import java.sql.SQLException;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.*;
import static constant.LloydsRegister.randomiseConstant;

public class CreateImoRegisteredAssetTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 4: Create an IMO registered Asset")
    @Issue("LRD-3892")
    public void createImoRegisteredAssetTest() throws SQLException
    {

        AddNewAssetPage addNewAssetPage = WorkHubPage
                .open()
                .clickAddNewAsset();

        IMORegisteredAssetPage iMORegisteredAssetPage
                = addNewAssetPage.clickImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("IMO Registered Asset Radio Button is Selected")
                .that(iMORegisteredAssetPage.isIMORegisteredAssetButtonIsSelected())
                .isTrue();
        addNewAssetPage.clickImoRegisteredAssetRadioButton()
                .setIMONumber(new ImoAsset().getUniqueImoNumber())
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild);

        assert_().withFailureMessage("All the fields are expected to be filled")
                .that(addNewAssetPage.isCreateAssetButtonEnabled())
                .isTrue();
        AllAssetsAssetElement assetCard = addNewAssetPage.clickCreateAssetButton()
                .getViewAssetPage()
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName()
                        .equals(assetName))
                .findFirst()
                .get();
        assert_().withFailureMessage("Created Asset is expected to be Displayed")
                .that(assetCard.getAssetName())
                .isEqualTo(assetName);
    }
}
