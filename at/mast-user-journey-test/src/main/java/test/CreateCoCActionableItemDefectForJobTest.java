package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.element.DataElement;
import viewasset.sub.jobs.details.defects.DefectsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static constant.LloydsRegister.randomiseConstant;

public class CreateCoCActionableItemDefectForJobTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 19a:Create and record Defects, Codicils and Conditions of Class")
    @Issue("LRD-4175")
    public void createCoCActionableItemDefectForJobTest()
    {

        // Create a new asset
        AllAssetsAssetElement assetCard = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(assetName))
                .findFirst()
                .get();

        // Add a new case to the asset
        ViewAssetPage viewAssetPage = assetCard.clickViewAssetButton()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectFirstEntryRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createFirstEntryCase()
                .getViewAssetPage();

        // Add a new job to the asset
        ViewJobDetailsPage viewJobDetailsPage = viewAssetPage.clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob();

        ServiceAllPage serviceAllPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        serviceAllPage.getFooter()
                .clickAddButton(ConfirmModalWindow.class)
                .clickOkButton(JobScopePage.class)
                .clickConfirmJobScopeButton()
                .clickConfirmScopeButton();

        // Initialize test data
        String conditionOfClassName = LloydsRegister.CocData.title;
        String actionableItemName = LloydsRegister.AiData.title;
        String defectName = LloydsRegister.DefectData.title;
        LocalDate tomorrow = LloydsRegister.localDate.plusDays(1);
        LocalDate dayAfterTomorrow = LloydsRegister.localDate.plusDays(2);

        // Add a new condition of class to the job
        ConditionsOfClassPage conditionsOfClassPage = viewJobDetailsPage.clickGoToCocsButton()
                .clickAddNewButton()
                .setTitle(conditionOfClassName)
                .setDescription(LloydsRegister.CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(tomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(dayAfterTomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(ConfirmationModalPage.class)
                .clickConfirmationButton(ConditionsOfClassPage.class);

        // Verify that the condition of class has been added
        ConditionsOfClassPage.JobRelatedTable jobRelatedTable = conditionsOfClassPage.getJobRelatedTable();
        String displayedConditionOfClassName = jobRelatedTable.getRows()
                .stream().map(DataElement::getCocTitle)
                .collect(Collectors.toList()).get(0);

        assert_().withFailureMessage("Condition of class is expected to be displayed in Job related section of CoC")
                .that(displayedConditionOfClassName)
                .isEqualTo(conditionOfClassName);

        // Navigate to View Job Details page
        viewJobDetailsPage = conditionsOfClassPage.clickNavigateBackButton();

        // Add a new actionable item to the job
        ActionableItemsPage actionableItemsPage = viewJobDetailsPage.clickGoToActionableItemsButton()
                .clickAddNewButton()
                .setTitle(actionableItemName)
                .clickClassCategoryButton()
                .setDescription(LloydsRegister.AiData.description)
                .setSurveyorGuidance(LloydsRegister.AiData.surveyorGuidance)
                .setImposedDate(String.valueOf(tomorrow.format(FRONTEND_TIME_FORMAT_DTS)))
                .setDueDate(String.valueOf(dayAfterTomorrow.format(FRONTEND_TIME_FORMAT_DTS)))
                .clickVisibleForAllButton()
                .clickSaveButton()
                .clickOkButton(ActionableItemsPage.class);

        // Verify that the actionable item has been added
        ActionableItemsPage.JobRelatedTable jobRelatedAiTable = actionableItemsPage.getJobRelatedTable();
        String displayedActionableItemName = jobRelatedAiTable.getRows()
                .stream().map(viewasset.sub.jobs.details.actionableitems.element.DataElement::getTitleText)
                .collect(Collectors.toList()).get(0).replaceAll("\\d+$", "").trim();

        assert_().withFailureMessage("Actionable item is expected to be displayed in Job related section of Actionable Items")
                .that(displayedActionableItemName)
                .isEqualTo(actionableItemName);

        // Navigate to View Job Details page
        viewJobDetailsPage = actionableItemsPage.clickNavigateBackButton();

        // Add a new defect to the job
        DefectsPage defectsPage = viewJobDetailsPage.clickGoToDefectsButton()
                .clickAddNewButton()
                .setTitle(defectName)
                .clickDefectCategory()
                .clickElectroTechnicalButton()
                .getAddDefectPage()
                .clickSaveButton()
                .clickOkButton()
                .clickNavigateBackButton();

        // Verify that the defect has been added
        DefectsPage.JobRelatedTable jobRelatedDefectsTable = defectsPage.getJobRelatedTable();
        String displayedDefectName = jobRelatedDefectsTable.getRows()
                .stream().map(viewasset.sub.jobs.details.defects.element.DataElement::getTitleText)
                .collect(Collectors.toList()).get(0).replaceAll("\\d+$", "").trim();

        assert_().withFailureMessage("Defect is expected to be displayed in Job related section of Defects")
                .that(displayedDefectName)
                .isEqualTo(displayedDefectName);
    }

}
