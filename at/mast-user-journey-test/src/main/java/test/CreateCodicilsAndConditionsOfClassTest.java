package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.AddConfirmationWindow;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.modal.AddConfirmationPage;
import viewasset.sub.codicilsanddefects.element.ActionableItemsElement;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;
import static constant.LloydsRegister.randomiseConstant;

public class CreateCodicilsAndConditionsOfClassTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);
    private final String cocTitle = randomiseConstant(LloydsRegister.CocData.title);

    @Test(description = "R2 User Journey 19: Create and record Defects, Codicils and Conditions of Class")
    @Issue("LRD-4139")
    public void createCodicilsAndConditionsOfClassTest()
    {

        // Create a new asset
        AllAssetsAssetElement assetCard = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(assetName))
                .findFirst()
                .get();

        // Navigate to View Asset page
        ViewAssetPage viewAssetPage = assetCard.clickViewAssetButton();

        // Navigate to Codicils and Defects page
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAssetPage.clickCodicilsAndDefectsTab();

        // Initialize test data
        LocalDate tomorrow = LloydsRegister.localDate.plusDays(1);
        LocalDate dayAfterTomorrow = LloydsRegister.localDate.plusDays(2);
        String actionableItemCategory = "Class";
        DateTimeFormatter frontendTimeFormat = DateTimeFormatter.ofPattern("dd MMM yyyy");

        // Add Condition of Class
        AddConditionOfClassPage addConditionOfClassPage = codicilsAndDefectsPage.clickCocAddNewButton();
        codicilsAndDefectsPage = addConditionOfClassPage.setTitle(cocTitle)
                .setDescription(LloydsRegister.CocData.description)
                .selectInheritedCheckBox()
                .setImposedDate(tomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(dayAfterTomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrAndCustomerButton()
                .clickSaveButton(AddConfirmationPage.class)
                .clickOkButton();

        // Verify that Condition of Class has been added
        List<ConditionsOfClassElement> conditionsOfClassElements = codicilsAndDefectsPage.getCocItems();

        assert_().withFailureMessage("One CoC is expected to be Displayed")
                .that(conditionsOfClassElements.size())
                .isEqualTo(1);

        assert_().withFailureMessage("CoC Name is expected to be Displayed")
                .that(conditionsOfClassElements.get(0).getCocTitle().trim())
                .isEqualTo(cocTitle);

        //TODO: Uncomment once LRD-2878 is resolved
        /*
        assert_().withFailureMessage("CoC Imposed Date is expected to be Displayed")
                 .that(conditionsOfClassElements.get(0).getImposeDate().trim())
                 .isEqualTo(tomorrow.format(frontendTimeFormat));

        assert_().withFailureMessage("CoC Due Date is expected to be Displayed")
                 .that(conditionsOfClassElements.get(0).getDueDate().trim())
                 .isEqualTo(dayAfterTomorrow.format(frontendTimeFormat));
        */

        // Add Actionable Item
        AddActionableItemPage addActionableItemPage = codicilsAndDefectsPage.clickActionableItemAddNewButton();
        codicilsAndDefectsPage = addActionableItemPage.setTitle(cocTitle)
                .clickClassButton()
                .setDescription(LloydsRegister.CocData.description)
                .setSurveyorTextInput("Surveyor guidance " + LloydsRegister.CocData.description)
                .setImposedDate(tomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(dayAfterTomorrow.format(FRONTEND_TIME_FORMAT_DTS))
                .clickLrInternalButton()
                .clickSaveButton(AddConfirmationWindow.class)
                .clickOkButton();

        // Verify that Actionable Item has been added
        List<ActionableItemsElement> actionableItemsElements = codicilsAndDefectsPage.getActionableItems();

        assert_().withFailureMessage("One Actionable Item is expected to be Displayed")
                .that(actionableItemsElements.size())
                .isEqualTo(1);

        assert_().withFailureMessage("Actionable Item Name is expected to be Displayed")
                .that(actionableItemsElements.get(0).getActionableItemsTitle().trim())
                .isEqualTo(cocTitle);

        assert_().withFailureMessage("Actionable Item Category is expected to be Displayed")
                .that(actionableItemsElements.get(0).getActionableItemCategory().trim())
                .isEqualTo(actionableItemCategory);

        //TODO: Uncomment once LRD-2878 is resolved
        /*
        assert_().withFailureMessage("Actionable Item Imposed Date is expected to be Displayed")
                 .that(actionableItemsElements.get(0).getImposeDate().trim())
                 .isEqualTo(tomorrow.format(frontendTimeFormat));

        assert_().withFailureMessage("Actionable Item Due Date is expected to be Displayed")
                 .that(actionableItemsElements.get(0).getDueDate().trim())
                 .isEqualTo(dayAfterTomorrow.format(frontendTimeFormat));
        */
    }
}
