package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.DARPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;

public class GenerateAndSubmitDAR extends BaseTest
{
    final String expectedReportType = "DAR";

    @Test(description = "R2 User Journey 27:Generate, View and Submit the DAR")
    @Issue("LRD-4151")

    public void generateAndSubmitDARTest()
    {
        ServiceAllPage serviceAllPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        ViewJobDetailsPage viewJobDetailsPage =
                serviceAllPage
                        .getFooter()
                        .clickAddButton(ConfirmModalWindow.class)
                        .clickOkButton(JobScopePage.class)
                        .clickConfirmJobScopeButton()
                        .clickConfirmScopeButton()
                        .clickGoToCreditingButton()
                        .clickGenerateDarButton()
                        .setDate(LloydsRegister.localDate.format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                        //TODO: Bug LRD-4784
                        .clickGenerateDarButton()
                        .clickSubmitDarButton()
                        .clickOkButton(DARPage.class)
                        .getHeaderPage()
                        .clickNavigateBackButton();

        assert_().withFailureMessage("Generated DAR is expected to be Displayed")
                .that(viewJobDetailsPage.getJobInProgressCardsTitle()
                        .stream()
                        .filter(e -> e.contains(expectedReportType))
                        .findFirst()
                        .get()
                        .contains(expectedReportType))
                .isTrue();
    }
}
