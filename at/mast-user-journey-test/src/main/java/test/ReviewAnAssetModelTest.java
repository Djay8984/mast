package test;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.attributes.element.AttributeElement;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class ReviewAnAssetModelTest extends BaseTest
{
    private static void checkMarkedAsComplete(ListViewItemElement listViewItemElement)
    {
        assert_().withFailureMessage("The system has not marked all child elements as completed")
                .that(listViewItemElement.isStatusComplete())
                .isEqualTo(true);
    }

    @Test(description = "R2 User Journey 8: Review an Asset Model")
    @Issue("LRD-3897")
    public void reviewAnAssetModelTest()
    {
        AssetModelSubPage assetModelSubPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickAssetModelTab();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage.clickCheckOutForEditingButton();
        addReferenceDataItem(buildAssetModelPage);

        buildAssetModelPage.getItemByIndex(0).getChildren().get(0).clickItemName();
        addReferenceDataItem(buildAssetModelPage);

        buildAssetModelPage.getItemByIndex(0).getChildren().get(0).clickItemName()
                .getItemByIndex(0).getChildren().get(0)
                .getChildren().get(0).clickItemName();

        AddFromReferenceDataPage addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        addFromReferenceDataPage.getReferenceItems()
                .stream()
                .forEach(ReferenceItemElement::clickCheckbox);
        addFromReferenceDataPage.clickAddSelected();
        buildAssetModelPage.clickFinishButton();

        ItemsDetailsPage itemsDetailsPage = assetModelSubPage.clickListViewButton()
                .then().getItemByIndex(1).clickItemName()
                .then().getItemByIndex(1).clickFurtherDetailsArrow();
        AttributesSubPage attributesSubPage = itemsDetailsPage.clickAttributesTab();
        List<AttributeElement> attributeElements = attributesSubPage.getAllDisplayedAttributeElement();
        for (AttributeElement attributeElement : attributeElements)
        {
            String attributeName = attributeElement.getAttributeNameText();
            String attributeValueType = attributeElement.getAttributeValueType();
            switch (attributeValueType)
            {
                case "input":
                    attributesSubPage.setAttributeValue(attributeName, "Test");
                    break;
                case "select":
                    String dropDownValue = attributeElement.getAttributeDropdownValues()
                            .get(TestDataHelper.getRandomNumber(0, attributeElement.getAttributeDropdownValues().size() - 1));
                    attributesSubPage.selectAttributeDropDownByVisibleText(attributeName, dropDownValue);
                default:
                    break;
            }
        }

        attributesSubPage.clickSaveChangesButton().then().clickOkButton();

        assert_().withFailureMessage("Mark as complete button is not enabled")
                .that(itemsDetailsPage.isMarkAsCompleteButtonEnabled())
                .isTrue();

        itemsDetailsPage.clickMarkAsComplete();
        itemsDetailsPage.clickNavigateBackButton();

        ListViewItemElement getFirstItem = assetModelSubPage.clickListViewButton()
                .getItemByIndex(1).clickItemName().getItemByIndex(1);
        checkMarkedAsComplete(getFirstItem);

        getFirstItem.clickItemName().getAssetItems().stream()
                .forEach(ReviewAnAssetModelTest::checkMarkedAsComplete);
    }

    private void addReferenceDataItem(BuildAssetModelPage buildAssetModelPage)
    {
        AddFromReferenceDataPage addFromReferenceDataPage = buildAssetModelPage.clickAddFromReferenceDataButton();
        assert_().withFailureMessage("There are no reference items to add")
                .that(addFromReferenceDataPage.getReferenceItems().size())
                .isGreaterThan(0);
        addFromReferenceDataPage.getItemByIndex(TestDataHelper
                .getRandomNumber(1, addFromReferenceDataPage.getReferenceItems().size()))
                .clickCheckbox();
        addFromReferenceDataPage.clickAddSelected();
    }
}
