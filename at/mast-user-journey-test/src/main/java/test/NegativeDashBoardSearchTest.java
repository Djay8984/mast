package test;

import com.frameworkium.core.ui.tests.BaseTest;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class NegativeDashBoardSearchTest extends BaseTest
{
    private final String[] invalidData = {
            "In_valid$$$$ Asset",
            "9999999",
            RandomStringUtils.randomAlphabetic(10)};

    @Test(description = "R2 User Journey 2: Negative Dashboard and Search")
    @Issue("LRD-3890")
    public void negativeDashBoardSearchTest()
    {
        WorkHubPage workHubPage = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .getHeader()
                .clickMastLogo();
        for (String anInvalidData : invalidData)
        {
            workHubPage.clickSearchFilterAssetsButton()
                    .setAssetSearchInput(anInvalidData)
                    .clickAssetSearchApplyButton(WorkHubPage.class);
            assert_().withFailureMessage("No results are displayed for invalid data")
                    .that(workHubPage
                            .clickAllAssetsTab()
                            .getAssetCards()
                            .stream()
                            .map(AllAssetsAssetElement::getAssetName)
                            .collect(Collectors.toList())
                            .size())
                    .isEqualTo(0);
        }
    }
}






