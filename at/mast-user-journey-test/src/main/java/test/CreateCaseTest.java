package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SelectCaseTypePage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.cases.addcase.modal.CannotCreateCasePage;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.randomiseConstant;

public class CreateCaseTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 9: Create a Case for an Asset (Create a Case for an Asset (AIC, AIMC-in, First entry, TOC-in, TOMC-in, Non-classed)")
    @Issue("LRD-3898")
    public void createCaseTest()
    {
        ViewAssetPage viewAssetPage = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName);

        CasesPage casesPage = viewAssetPage.clickCasesTab();
        SelectCaseTypePage selectCaseTypePage = casesPage.clickAddCaseButton();

        SpecifyDetailsPage specifyDetailsPage = selectCaseTypePage
                .selectAimcInShipRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        assert_().withFailureMessage("The asset Name is expected to be displayed")
                .that(specifyDetailsPage.getAssetName())
                .isEqualTo(assetName);
        assert_().withFailureMessage("The asset Type is expected to be displayed")
                .that(specifyDetailsPage.getAssetType())
                .isEqualTo(LloydsRegister.AssetData.statCode5);

        casesPage = specifyDetailsPage.createAimcInShipCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("AIMC"))
                .isNotNull();

        selectCaseTypePage = casesPage.clickAddCaseButton();
        String cannotCreateCase = "Cannot create case";
        CannotCreateCasePage cannotCreateCasePage = selectCaseTypePage
                .selectAimcInShipRadioButton()
                .then().clickNextButton(CannotCreateCasePage.class);

        assert_().withFailureMessage("Expected to display Cannot create Case when a case type is already present")
                .that(cannotCreateCasePage.getHeaderText())
                .contains(cannotCreateCase);

        selectCaseTypePage = cannotCreateCasePage.clickOkButton();

        specifyDetailsPage = selectCaseTypePage
                .selectAicRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);

        assert_().withFailureMessage("The asset Name is expected to be displayed")
                .that(specifyDetailsPage.getAssetName())
                .isEqualTo(assetName);
        assert_().withFailureMessage("The asset Type is expected to be displayed")
                .that(specifyDetailsPage.getAssetType())
                .isEqualTo(LloydsRegister.AssetData.statCode5);

        casesPage = specifyDetailsPage.createAicCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("AIC"))
                .isNotNull();

        selectCaseTypePage = casesPage.clickAddCaseButton();

        specifyDetailsPage = selectCaseTypePage
                .selectFirstEntryRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        casesPage = specifyDetailsPage.createFirstEntryCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("First entry"))
                .isNotNull();

        selectCaseTypePage = casesPage.clickAddCaseButton();
        specifyDetailsPage = selectCaseTypePage
                .selectTocInRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        casesPage = specifyDetailsPage.createTocInCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("TOC-In"))
                .isNotNull();

        selectCaseTypePage = casesPage.clickAddCaseButton();
        specifyDetailsPage = selectCaseTypePage
                .selectTomcInShipRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        casesPage = specifyDetailsPage.createTomcInShipCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("TOMC-In"))
                .isNotNull();

        selectCaseTypePage = casesPage.clickAddCaseButton();
        specifyDetailsPage = selectCaseTypePage
                .selectNonClassedRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        casesPage = specifyDetailsPage.createNonClassedCase();

        assert_().withFailureMessage("The Case of the respective type is expected to be created")
                .that(casesPage.getCaseByCaseType("Non-classed"))
                .isNotNull();

    }
}
