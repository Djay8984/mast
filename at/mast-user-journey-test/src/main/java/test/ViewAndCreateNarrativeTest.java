package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;

public class ViewAndCreateNarrativeTest extends BaseTest
{

    final static String test = "This is a test Message";

    @Test(description = "R2 User Journey 24: View and create Narrative by adding free text to services and tasks")
    @Issue("LRD-4145")
    public void viewAndCreateNarrativeTest()
    {
        ServiceAllPage serviceAllPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(LloydsRegister.JobTeamData.leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        CreditingPage creditingPage = serviceAllPage
                .getFooter()
                .clickAddButton(ConfirmModalWindow.class)
                .clickOkButton(JobScopePage.class)
                .clickConfirmJobScopeButton()
                .clickConfirmScopeButton()
                .clickGoToCreditingButton()
                .getStatutoryTable()
                .getRows()
                .get(0)
                .clickEditSurveyIcon()
                .setNarrativeTextBox(test)
                .clickSaveButton();

        assert_().withFailureMessage("Added Narrative Text is expected to be Saved")
                .that(creditingPage
                        .getStatutoryTable()
                        .getRows()
                        .get(0)
                        .clickEditSurveyIcon()
                        .getNarrativeTextBox())
                .isEqualTo(test);

    }
}
