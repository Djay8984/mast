package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.attachment.AttachmentDetailsPage;
import viewasset.sub.commonpage.note.element.NoteAndAttachmentElement;
import workhub.WorkHubPage;

import java.util.List;

import static helper.AttachmentHelper.getAttachment;
import static com.google.common.truth.Truth.assert_;

public class ViewingAndDeletingAnAttachmentTest extends BaseTest
{
    @Test(description = "R2 User Journey 16: Viewing/Attaching and Deleting an attachment online")
    @Issue("LRD-3921")
    public void viewingAndDeletingAnAttachmentTest()
    {
        AttachmentsAndNotesPage attachmentsAndNotesPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .getHeader()
                .clickAttachmentButton()
                .clickAddAttachmentButton()
                .browseAndAttachFile(getAttachment())
                .clickAllButton()
                .clickUploadButton();

        List<NoteAndAttachmentElement> noteAndAttachmentElements = attachmentsAndNotesPage
                .getNoteAndAttachmentCards();
        assert_().withFailureMessage("Added Attachment is expected to be displayed")
                .that(noteAndAttachmentElements.stream()
                        .filter(e -> e.getAttachmentName().equals(LloydsRegister.fileName))
                        .findFirst()
                        .isPresent())
                .isTrue();

        noteAndAttachmentElements.stream()
                .filter(e -> e.getAttachmentName().equals(LloydsRegister.fileName))
                .findFirst().get().clickNoteOrAttachmentElement(AttachmentDetailsPage.class)
                .clickDeleteIcon()
                .clickConfirmationButton(AttachmentsAndNotesPage.class);

        assert_().withFailureMessage("Added Attachment is expected to be displayed")
                .that(attachmentsAndNotesPage.getNoteAndAttachmentCards()
                        .stream()
                        .filter(e -> e.getAttachmentName().equals(LloydsRegister.fileName))
                        .findFirst()
                        .isPresent())
                .isFalse();

    }
}
