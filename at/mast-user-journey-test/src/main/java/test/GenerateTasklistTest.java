package test;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.DatabaseHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.assetheaderadditionalinformation.assetdetails.AssetDetailsPage;
import viewasset.sub.serviceschedule.addservicetoproducts.SpecifyDetailsPage;
import viewasset.sub.serviceschedule.classification.SelectProductsPage;
import viewasset.sub.serviceschedule.classification.element.SelectProductElement;
import viewasset.sub.serviceschedule.element.DataElement;
import viewasset.sub.serviceschedule.service.ViewServicePage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class GenerateTasklistTest extends BaseTest
{
    final String serviceType = "AS Annual";
    final String taskNumber = "320-AMN575-14";

    @Test(description = "R2 User Journey 22:Generate Tasklist")
    @Issue("LRD-4143")

    public void generateTaskListTest()
    {

        //Create an Asset
        AssetDetailsPage assetDetailsPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickViewMoreButton()
                .getAssetDetailsPage();

        //Get Asset Name
        String assetName = assetDetailsPage.getAssetName();
        AllAssetsAssetElement allAssetsAssetElement = assetDetailsPage.getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(assetName))
                .findFirst()
                .get();

        //Get Asset ID
        String assetId = allAssetsAssetElement.getAssetId();

        //Go to ServiceSchedule and Add Products
        SelectProductsPage selectProductsPage
                = allAssetsAssetElement.clickViewAssetButton()
                .clickServiceScheduleTab()
                .clickEditRuleSetIcon()
                .selectRuleSetByIndex(1)
                .clickOkButton()
                .clickClassificationTab()
                .clickAddProductsButton();
        SelectProductElement selectProductElement = selectProductsPage
                .getSelectProductElements()
                .get(0);

        String productName = selectProductElement.getProductName();

        selectProductElement.clickProductCheckBox();
        List<DataElement> dataElements = selectProductsPage.clickAddButton()
                .getClassificationTable()
                .getRows();

        assert_().withFailureMessage("Added Product is expected to be displayed")
                .that(dataElements
                        .stream()
                        .filter(e -> e.getProductName().equals(productName))
                        .findFirst()
                        .isPresent())
                .isTrue();

        //Add Services to Added Products
        SpecifyDetailsPage specifyDetailsPage = dataElements
                .stream()
                .filter(e -> e.getProductName().equals(productName))
                .findFirst()
                .get()
                .clickAddButton()
                .selectCheckBoxByName(serviceType)
                .clickNextButton();

        dataElements = specifyDetailsPage
                .clickCompleteButton()
                .getClassificationTable()
                .getRows()
                .get(0)
                .getServiceData();

        assert_().withFailureMessage("Added Service is expected to be displayed")
                .that(dataElements
                        .stream()
                        .filter(e -> e.getServiceTitle().equals(serviceType))
                        .findFirst()
                        .isPresent())
                .isTrue();

        //SQL interaction
        DatabaseHelper.runSqlFile("sql/AddTasksServiceSchedule.sql", assetId);

        ViewServicePage viewServicePage = dataElements
                .stream()
                .filter(e -> e.getServiceTitle().equals(serviceType))
                .findFirst()
                .get()
                .clickFurtherDetailsArrow();

        assert_().withFailureMessage("Added Task is expected to be displayed")
                .that(viewServicePage.getTaskListTable()
                        .getRows()
                        .get(0)
                        .getTaskNumber())
                .isEqualTo(taskNumber);
    }
}
