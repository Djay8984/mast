package test;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.EndorsementPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.DARPage;
import viewasset.sub.jobs.details.crediting.DSRPage;
import viewasset.sub.jobs.details.crediting.FARPage;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.JobData.strEtaDate;
import static constant.LloydsRegister.JobData.strEtdDate;
import static constant.LloydsRegister.JobTeamData.leadSurveyor;

public class EndorseSurveyTest extends BaseTest
{

    String otherLeadSurveyor = "James Rolf SDO";
    String otherAuthorisingSurveyor = "Jose Sevilla";
    String jobStatus = "Closed";

    @Test(description = "User Journey 33: Survey is endorsed")
    @Issue("LRD-4164")

    public void endorseSurvey()
    {

        ServiceAllPage serviceAllPage = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAimcInShipRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAimcInShipCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();

        ViewJobDetailsPage viewJobDetailsPage = serviceAllPage
                .getFooter()
                .clickAddButton(ConfirmModalWindow.class)
                .clickOkButton(JobScopePage.class)
                .clickConfirmJobScopeButton()
                .clickConfirmScopeButton()
                .clickGoToCreditingButton()
                .clickGenerateDarButton()
                .setDate(strEtaDate)
                //TODO: Bug LRD-4784
                .clickGenerateDarButton()
                .clickSubmitDarButton()
                .clickOkButton(DARPage.class)
                .getHeaderPage()
                .clickNavigateBackButton()
                .clickGoToCreditingButton()
                .clickGenerateFarButton()
                .setDate(strEtdDate)
                .clickGenerateFarButton()
                .clickSubmitFarButton()
                .clickOkButton(FARPage.class)
                .getHeaderPage()
                .clickNavigateBackButton()
                .clickGenerateDsrButton()
                .clickSubmitDsrButton()
                .clickSubmitDsrButton()
                .clickOkButton(DSRPage.class)
                .getHeaderPage()
                .clickNavigateBackButton()
                .clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(otherLeadSurveyor)
                .setAuthorisingSurveyorTextBox(leadSurveyor)
                .clickSaveButton()
                .clickApproveTechnicalReviewButton()
                //TODO: Bug LRD-4678
                .clickApproveDSRButton()
                .clickAddOrGoToJobTeamButton()
                .setAuthorisingSurveyorTextBox(otherAuthorisingSurveyor)
                .setFleetServicesSpecialistTextBox(leadSurveyor)
                .clickSaveButton();

        EndorsementPage endorsementPage = viewJobDetailsPage
                .clickViewActionsButton(EndorsementPage.class)
                .selectEicCaseCheckBox()
                .clickCompleteButton();

        assert_().withFailureMessage("Tick Should Be Displayed When Completed")
                .that(endorsementPage.isCompletedTickPresent())
                .isTrue();

        assert_().withFailureMessage("Completed By Should Display Name: " + leadSurveyor)
                .that(endorsementPage.getCompletedBy())
                .isEqualTo(leadSurveyor);

        assert_().withFailureMessage("Tick Box Should Now Display A Re-Open Button")
                .that(endorsementPage.isReopenButtonPresent())
                .isTrue();

        endorsementPage
                .clickBackButton(ViewJobDetailsPage.class);
        viewJobDetailsPage
                .clickCloseJobButton()
                .clickConfirmCloseJobButton(ViewJobDetailsPage.class);

        assert_().withFailureMessage("Tick Should Be Displayed Now The Job Is Closed")
                .that(viewJobDetailsPage.getClosedJobTick())
                .isTrue();

        assert_().withFailureMessage("Job Status Should Read Closed")
                .that(viewJobDetailsPage.getJobStatus())
                .isEqualTo(jobStatus);
    }
}
