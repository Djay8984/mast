package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.statCode5;
import static constant.LloydsRegister.randomiseConstant;

public class DashBoardSearchTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 1: Dashboard and Search")
    @Issue("LRD-3889")
    public void dashBoardSearchTest()
    {
        AllAssetsAssetElement assetCard = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName()
                        .equals(assetName))
                .findFirst()
                .get();

        assert_().withFailureMessage("Asset Name is expected to be Displayed")
                .that(assetCard.getAssetName())
                .isEqualTo(assetName);

        assert_().withFailureMessage("Asset type is expected to be Displayed")
                .that(assetCard.getAssetType())
                .isEqualTo(statCode5);

        //TODO: Uncomment once LRD-2878 is resolved
        /*
        assert_().withFailureMessage("Date of build is expected to be Displayed")
                .that(assetCard.getBuildDate())
                .isEqualTo(dateOfBuild);
         */
    }
}
