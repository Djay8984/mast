package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import header.HeaderPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.CopyFromAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.randomiseConstant;

public class CreateAssetModelByCopyingExistingAssetModelTest extends BaseTest
{
    private HeaderPage headerPage;

    private final String firstAssetName = randomiseConstant(LloydsRegister.AssetData.assetName);
    private final String secondAssetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 5: Create an Asset Model by copying an existing Asset Model")
    @Issue("LRT-3893")

    public void createAssetModelByCopyingExistingAssetModelTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        AddFromReferenceDataPage addFromReferenceDataPage = workHubPage
                .clickAddNewAsset()
                .createImoRegisteredAsset(firstAssetName)
                .clickAssetModelTab()
                .clickCheckOutForEditingButton()
                .clickAddFromReferenceDataButton();

        List<ReferenceItemElement> referenceItemElements = addFromReferenceDataPage
                .getReferenceItems();

        referenceItemElements.forEach(ReferenceItemElement::clickCheckbox);

        List<ReferenceItemElement> referenceItemElement = addFromReferenceDataPage
                .clickAddSelected()
                .getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickItemName()
                .clickAddFromReferenceDataButton()
                .getReferenceItems();
        List<String> actualItems = referenceItemElement.stream().map(ReferenceItemElement::getReferenceItemName)
                .collect(Collectors.toList());

        referenceItemElement.forEach(ReferenceItemElement::clickCheckbox);
        AssetModelSubPage assetModelSubPage = addFromReferenceDataPage
                .clickAddSelected()
                .clickFinishButton();

        BuildAssetModelPage buildAssetModelPage = assetModelSubPage
                .getHeaderPage()
                .clickMastLogo()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(secondAssetName)
                .clickAssetModelTab()
                .clickCheckOutForEditingButton();
        buildAssetModelPage.clickCopyFromAssetModelButton()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(firstAssetName)
                .clickAssetSearchApplyButton(CopyFromAssetModelPage.class)
                .getAllAssetsSubPage()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(firstAssetName))
                .findFirst()
                .get()
                .clickCopyButton()
                .clickYesButton();

        List<DataElement> childItems = buildAssetModelPage.getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickExpandIcon()
                .getChildren();
        List<String> expectedItemsToBeCopied = childItems.stream()
                .map(DataElement::getItemName).collect(Collectors.toList());
        assert_().withFailureMessage("Copied Item is expected to be displayed in Build Asset Model")
                .that(actualItems)
                .isEqualTo(expectedItemsToBeCopied);

        List<String> expectedItemsInAssetModel = buildAssetModelPage
                .clickFinishButton()
                .clickListViewButton()
                .getAssetItems()
                .get(0)
                .clickItemName()
                .getAssetItems()
                .stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());
        assert_().withFailureMessage("Copied Item is expected to be displayed in View Asset Model")
                .that(actualItems)
                .isEqualTo(expectedItemsInAssetModel);

        List<String> expectedItemsInBuildModel = assetModelSubPage.clickCheckOutForEditingButton()
                .getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .getChildren()
                .stream()
                .map(DataElement::getItemName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Copied Item is expected to be displayed in Build Asset Model")
                .that(actualItems)
                .isEqualTo(expectedItemsInBuildModel);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
