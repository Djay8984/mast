package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import workhub.AddNewAssetPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.sub.NonIMORegisteredAssetPage;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.*;
import static constant.LloydsRegister.RandomiseType.LETTERS;
import static constant.LloydsRegister.randomiseConstant;

public class CreateNonImoRegisteredAssetTest extends BaseTest
{

    private final String builder = randomiseConstant(LloydsRegister.AssetData.builder, LETTERS);
    private final String yardNumber = randomiseConstant(LloydsRegister.AssetData.yardNumber);
    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 3: Create an non-IMO registered Asset")
    @Issue("LRD-3891")
    public void createNonImoRegisteredAssetTest()
    {

        AddNewAssetPage addNewAssetPage = WorkHubPage
                .open()
                .clickAddNewAsset();
        NonIMORegisteredAssetPage nonIMORegisteredAssetPage = addNewAssetPage.clickNonImoRegisteredAssetRadioButton();
        assert_().withFailureMessage("Non-IMO registered asset Radio Button is expected to be selected")
                .that(nonIMORegisteredAssetPage.isNonIMORegisteredAssetButtonIsSelected())
                .isTrue();
        nonIMORegisteredAssetPage.setBuilderName(builder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .setClassSection(classSection)
                .clickSelectAssetTypeSelectButton()
                .setDateOfBuild(dateOfBuild);

        assert_().withFailureMessage("All the fields are expected to be filled")
                .that(addNewAssetPage.isCreateAssetButtonEnabled())
                .isTrue();
        AllAssetsAssetElement assetCard = addNewAssetPage.clickCreateAssetButton()
                .getViewAssetPage()
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName()
                        .equals(assetName))
                .findFirst()
                .get();
        assert_().withFailureMessage("Created Asset is expected to be Displayed")
                .that(assetCard.getAssetName())
                .isEqualTo(assetName);
    }
}
