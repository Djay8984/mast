package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import helper.TestDataHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.serviceschedule.ClassificationSubPage;
import viewasset.sub.serviceschedule.addservicetoproducts.SpecifyDetailsPage;
import viewasset.sub.serviceschedule.classification.SelectProductsPage;
import viewasset.sub.serviceschedule.classification.element.SelectProductElement;
import viewasset.sub.serviceschedule.element.DataElement;
import viewasset.sub.serviceschedule.service.ViewServicePage;
import workhub.WorkHubPage;

import java.util.List;

import static com.google.common.truth.Truth.assert_;

public class CreateUpdateDeleteProductsServicesTest extends BaseTest
{
    final String expectedLastCreditedOn = Integer.toString(TestDataHelper.getRandomNumber(0, 999999));
    final String serviceType = "AS Annual";
    final String expectedServiceStatus = "Not started | Provisional";

    @Test(description = "R2 User Journey 12: Create, Update and Delete products and services")
    @Issue("LRD-3902")

    public void createUpdateDeleteProductsServicesTest()
    {

        SelectProductsPage selectProductsPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickServiceScheduleTab()
                .clickEditRuleSetIcon()
                .selectRuleSetByIndex(1)
                .clickOkButton()
                .clickClassificationTab()
                .clickAddProductsButton();
        SelectProductElement selectProductElement = selectProductsPage
                .getSelectProductElements()
                .get(0);

        String productName = selectProductElement.getProductName();

        selectProductElement.clickProductCheckBox();
        List<DataElement> dataElements = selectProductsPage.clickAddButton()
                .getClassificationTable()
                .getRows();

        assert_().withFailureMessage("Added Product is expected to be displayed")
                .that(dataElements
                        .stream()
                        .filter(e -> e.getProductName().equals(productName))
                        .findFirst()
                        .isPresent())
                .isTrue();

        SpecifyDetailsPage specifyDetailsPage = dataElements
                .stream()
                .filter(e -> e.getProductName().equals(productName))
                .findFirst()
                .get()
                .clickAddButton()
                .selectCheckBoxByName(serviceType)
                .clickNextButton();
        String expectedCyclePeriodicity = specifyDetailsPage.getCyclePeriodicity();
        dataElements = specifyDetailsPage
                .clickCompleteButton()
                .getClassificationTable()
                .getRows()
                .get(0)
                .getServiceData();

        assert_().withFailureMessage("Added Service is expected to be displayed")
                .that(dataElements
                        .stream()
                        .filter(e -> e.getServiceTitle().equals(serviceType))
                        .findFirst()
                        .isPresent())
                .isTrue();

        ViewServicePage viewServicePage = dataElements
                .stream()
                .filter(e -> e.getServiceTitle().equals(serviceType))
                .findFirst()
                .get()
                .clickFurtherDetailsArrow();

        assert_().withFailureMessage("Service Added is expected to be present under" + productName)
                .that(viewServicePage.getProductName().equals(productName))
                .isTrue();

        assert_().withFailureMessage("Added Cycle Periodicity is expected to be displayed")
                .that(viewServicePage.getPeriodicity().equals(expectedCyclePeriodicity + " months"))
                .isTrue();

        viewServicePage
                .clickEditButton()
                .setAssignedDate(LloydsRegister.localDate.plusDays(1).format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                .setDueDate(LloydsRegister.localDate.plusDays(5).format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                .setLowerRangeDate(LloydsRegister.localDate.plusDays(2).format(LloydsRegister.FRONTEND_TIME_FORMAT_DTS))
                .setLastCreditedOnText(expectedLastCreditedOn)
                .clickProvisionalButton()
                .clickSaveButton();

        assert_().withFailureMessage("Modified Last credited is expected to be Displayed")
                .that(viewServicePage.getLastCreditedOnText())
                .isEqualTo(expectedLastCreditedOn);

        assert_().withFailureMessage("Modified Service Status is expected to be displayed ")
                .that(viewServicePage.getServiceStatus())
                .isEqualTo(expectedServiceStatus);

        //TODO: change to verify the date rather verifying date field to be displayed
        assert_().withFailureMessage("Assigned date is expected to be displayed ")
                .that(viewServicePage.isAssignedDateDisplayed())
                .isTrue();

        assert_().withFailureMessage("Due Date is expected to be displayed")
                .that(viewServicePage.isDueDateDisplayed())
                .isTrue();

        assert_().withFailureMessage("Lower Range Date is expected to be displayed")
                .that(viewServicePage.isLowerRangeDateDisplayed())
                .isTrue();

        ClassificationSubPage classificationSubPage =
                viewServicePage.clickNavigateBackButton();
        dataElements = classificationSubPage.getClassificationTable()
                .getRows();
        dataElements
                .stream()
                .filter(e -> e.getProductName().equals(productName))
                .findFirst()
                .get()
                .getServiceData()
                .stream()
                .filter(e -> e.getServiceTitle().equals(serviceType))
                .findFirst()
                .get()
                .selectCheckBox();

        classificationSubPage.clickDeleteIcon()
                .clickRemoveServiceButton();

        assert_().withFailureMessage("Deleted Service is not expected to be displayed")
                .that(classificationSubPage.getClassificationTable()
                        .getRows()
                        .stream()
                        .filter(e -> e.getProductName().equals(productName))
                        .findFirst()
                        .get()
                        .getServiceData()
                        .stream()
                        .filter(e -> e.getServiceTitle().equals(serviceType))
                        .findFirst()
                        .isPresent())
                .isFalse();
    }
}
