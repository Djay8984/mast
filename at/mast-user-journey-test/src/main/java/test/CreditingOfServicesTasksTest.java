package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import helper.DatabaseHelper;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.element.ServiceElement;
import viewasset.sub.jobs.details.scope.JobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.ServiceAllPage;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.JobTeamData.leadSurveyor;
import static constant.LloydsRegister.randomiseConstant;

public class CreditingOfServicesTasksTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);
    private final String expectedTaskNumber = "320-AMN575-14";

    @Test(description = "Crediting of Services and Tasks")
    @Issue("LRD-4146")

    public void creditingOfServicesTasksTest()
    {
        //Create an Asset
        AllAssetsAssetElement allAssetsAssetElement = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(assetName))
                .findFirst()
                .get();

        String assetId = allAssetsAssetElement.getAssetId();
        ViewAssetPage viewAssetPage = allAssetsAssetElement.clickViewAssetButton();

        //Add items for an Asset
        AddFromReferenceDataPage
                addFromReferenceDataPage = viewAssetPage
                .clickAssetModelTab()
                .clickCheckOutForEditingButton()
                .clickAddFromReferenceDataButton();
        List<ReferenceItemElement> referenceItemElements =
                addFromReferenceDataPage.getReferenceItems();
        referenceItemElements.forEach(ReferenceItemElement::clickCheckbox);
        referenceItemElements = addFromReferenceDataPage
                .clickAddSelected()
                .getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .get(0)
                .clickItemName()
                .clickAddFromReferenceDataButton()
                .getReferenceItems();
        referenceItemElements.forEach(ReferenceItemElement::clickCheckbox);
        viewAssetPage = addFromReferenceDataPage
                .clickAddSelected()
                .clickFinishButton().getViewAssetPage();

        //Create a Case and Job
        ViewJobDetailsPage viewJobDetailsPage = viewAssetPage.clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob();
        String jobId = viewJobDetailsPage.getHeader().getJobId();

        //Add Surveyor
        ServiceAllPage serviceAllPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton()
                .setLeadSurveyorTextBox(leadSurveyor)
                .clickSaveButton()
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class)
                .clickAllTab();

        //Add Services
        serviceAllPage.getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusOrMinusIcon()
                .getChildren()
                .get(0)
                .clickPlusOrMinusIcon()
                .getServiceRows()
                .get(0)
                .selectCheckBox();
        serviceAllPage
                .getFooter()
                .clickAddButton(ConfirmModalWindow.class)
                .clickOkButton(JobScopePage.class)
                .clickConfirmJobScopeButton()
                .clickConfirmScopeButton();

        //SQL interaction
        DatabaseHelper.runSqlFile("sql/AddWorkItems.sql", assetId, jobId);
        List<ServiceElement> serviceElements = viewJobDetailsPage
                .clickGoToCreditingButton()
                .getStatutoryTable()
                .getRows()
                .get(0)
                .clickPlusButton()
                .getChildren()
                .get(0)
                .clickPlusButton()
                .getChildren();

        assert_().withFailureMessage("Added task '%s' is expected to be displayed", expectedTaskNumber)
                .that(serviceElements.get(0).getTaskNumber())
                .isEqualTo(expectedTaskNumber);
    }
}
