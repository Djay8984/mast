package test;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import workhub.WorkHubPage;

import static helper.AttachmentHelper.getAttachment;
import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.fileName;

public class CertificateManagementTest extends BaseTest
{

    @Test(description = "R2 User Journey 26:Certificate Management")
    @Issue("LRD-4147")
    public void certificateManagementTest()
    {
        AttachmentsAndNotesPage attachmentsAndNotesPage = WorkHubPage.open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase()
                .getViewAssetPage()
                .clickJobsTab()
                .clickAddNewJobButton()
                .addNewJob()
                .getHeader()
                .clickAttachmentButton()
                .clickAddAttachmentButton()
                .browseAndAttachFile(getAttachment())
                .clickAllButton()
                .clickUploadButton();

        assert_().withFailureMessage("Added Attachment is expected to be displayed").that(attachmentsAndNotesPage
                .getNoteAndAttachmentCards()
                .stream()
                .filter(e -> e.getAttachmentName().equals(fileName))
                .findFirst()
                .isPresent())
                .isTrue();
    }

}
