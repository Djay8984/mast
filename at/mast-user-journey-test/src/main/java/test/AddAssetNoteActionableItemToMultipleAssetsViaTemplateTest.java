package test;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import workhub.WorkHubPage;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.DefineSubPage;
import workhub.batchaction.element.SelectedAsset;
import workhub.batchaction.modal.SuccessfulPage;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.SearchFilterSubPage;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.AssetData.*;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class AddAssetNoteActionableItemToMultipleAssetsViaTemplateTest extends BaseTest
{

    @Test(description = "R2 User Journey 18: Adding an Asset Note/Actionable Item to multiple Assets via a Template")
    @Issue("LRD-3924")
    public void addAssetNoteActionableItemToMultipleAssetsViaTemplateTest()
    {

        // Create first asset
        String firstAssetBuilder = "Auto Builder " + RandomStringUtils.randomAlphabetic(5);
        String firstAssetName = "Auto KL" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        WorkHubPage workHubPage = WorkHubPage.open()
                .clickAddNewAsset()
                .clickNonImoRegisteredAssetRadioButton()
                .setBuilderName(firstAssetBuilder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(firstAssetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild)
                .clickCreateAsset()
                .getHeader()
                .clickMastLogo();

        // Create second asset
        String secondAssetBuilder = "Auto Builder " + RandomStringUtils.randomAlphabetic(5);
        String secondAssetName = "Auto KL" + Integer.toString(TestDataHelper.getRandomNumber(0, 99999999));
        workHubPage = workHubPage.clickAddNewAsset()
                .clickNonImoRegisteredAssetRadioButton()
                .setBuilderName(secondAssetBuilder)
                .setYardNumber(String.valueOf(yardNumber))
                .setAssetName(secondAssetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild)
                .clickCreateAsset()
                .getHeader()
                .clickMastLogo();

        // Navigate to Batch actions > Asset note page
        DefineSubPage defineSubPage = workHubPage.clickBatchActions()
                .clickDefineTab(DefineSubPage.class)
                .clickAssetNoteRadioButton();

        // Enter Asset note details, use a template
        ApplySubPage applySubPage = defineSubPage.clickStartFromATemplateButton()
                .clickFindTemplateButton()
                .getDataRows().get(0)
                .clickNameRadioButton()
                .getItemTemplatePage()
                .clickSelectTemplateButton()
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(1)))))
                .clickNextButton();

        // Find the first asset
        SearchFilterSubPage searchFilterSubPage = applySubPage.clickSearchFilterAssetsButton();
        applySubPage = searchFilterSubPage.setAssetSearchInput(firstAssetName)
                .clickAssetSearchApplyButton(ApplySubPage.class);

        // Select the first asset
        applySubPage.getAssetSection()
                .getCards()
                .get(0)
                .clickAddAssetButton();

        // Find the second asset
        searchFilterSubPage = applySubPage.clickSearchFilterAssetsButton();
        applySubPage = searchFilterSubPage.setAssetSearchInput(secondAssetName)
                .clickAssetSearchApplyButton(ApplySubPage.class);

        // Select the second asset
        applySubPage.getAssetSection()
                .getCards()
                .get(0)
                .clickAddAssetButton();

        // Get a list of the selected assets
        applySubPage.clickViewSelectedAssetButton();
        ApplySubPage.SelectedAssetSection selectedAssetSection = applySubPage.getSelectedSection();
        List<SelectedAsset> selectedAssets = selectedAssetSection.getCards();

        // Verify that the first asset is displayed
        assert_().withFailureMessage("Verify Asset note: Asset '%s' is expected to be displayed on View Selected Assets section.", firstAssetName)
                .that(selectedAssets.get(0).getAssetName())
                .isEqualTo(firstAssetName);

        // Verify that the second asset is displayed
        assert_().withFailureMessage("Verify Asset note: Asset note - Asset '%s' is expected to be displayed on View Selected Assets section.",
                secondAssetName)
                .that(selectedAssets.get(1).getAssetName())
                .isEqualTo(secondAssetName);

        // Click on Complete button
        SuccessfulPage successfulPage = applySubPage.clickCompleteButton();

        // Verify that the first asset is displayed on Batch Action Successful page
        assert_().withFailureMessage("Verify Asset note: Asset '%s' is expected to be displayed on Batch Action Successful page.", firstAssetName)
                .that(successfulPage.getSelectedAssets().get(0))
                .contains(firstAssetName);

        // Verify that the second asset is displayed on Batch Action Successful page
        assert_().withFailureMessage("Verify Asset note: Asset '%s' is expected to be displayed on Batch Action Successful page.", secondAssetName)
                .that(successfulPage.getSelectedAssets().get(1))
                .contains(secondAssetName);

        // Click on OK button
        workHubPage = successfulPage.clickOKButton();

        // Navigate to Batch actions > Actionable item page
        defineSubPage = workHubPage.clickBatchActions()
                .clickDefineTab(DefineSubPage.class)
                .clickActionableItemRadioButton();

        // Enter Actionable item details, use a template
        applySubPage = defineSubPage.clickStartFromATemplateButton()
                .clickFindTemplateButton()
                .getDataRows().get(0)
                .clickNameRadioButton()
                .getItemTemplatePage()
                .clickSelectTemplateButton()
                .setImposedDate(String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(1)))))
                .clickNextButton();

        // Find the first asset
        searchFilterSubPage = applySubPage.clickSearchFilterAssetsButton();
        applySubPage = searchFilterSubPage.setAssetSearchInput(firstAssetName)
                .clickAssetSearchApplyButton(ApplySubPage.class);

        // Select the first asset
        applySubPage.getAssetSection()
                .getCards()
                .get(0)
                .clickAddAssetButton();

        // Find the second asset
        searchFilterSubPage = applySubPage.clickSearchFilterAssetsButton();
        applySubPage = searchFilterSubPage.setAssetSearchInput(secondAssetName)
                .clickAssetSearchApplyButton(ApplySubPage.class);

        // Select the second asset
        applySubPage.getAssetSection()
                .getCards()
                .get(0)
                .clickAddAssetButton();

        // Get a list of the selected assets
        applySubPage.clickViewSelectedAssetButton();
        selectedAssetSection = applySubPage.getSelectedSection();
        selectedAssets = selectedAssetSection.getCards();

        // Verify that the first asset is displayed
        assert_()
                .withFailureMessage("Verify Actionable item: Asset '%s' is expected to be displayed on View Selected Assets section.", firstAssetName)
                .that(selectedAssets.get(0).getAssetName())
                .isEqualTo(firstAssetName);

        // Verify that the second asset is displayed
        assert_().withFailureMessage("Verify Actionable item: Asset '%s' is expected to be displayed on View Selected Assets section.",
                secondAssetName)
                .that(selectedAssets.get(1).getAssetName())
                .isEqualTo(secondAssetName);

        // Click on Complete button
        successfulPage = applySubPage.clickCompleteButton();

        // Verify that the first asset is displayed on Batch Action Successful page
        assert_()
                .withFailureMessage("Verify Actionable item: Asset '%s' is expected to be displayed on Batch Action Successful page.", firstAssetName)
                .that(successfulPage.getSelectedAssets().get(0))
                .contains(firstAssetName);

        // Verify that the second asset is displayed on Batch Action Successful page
        assert_().withFailureMessage("Verify Actionable item: Asset '%s' is expected to be displayed on Batch Action Successful page.",
                secondAssetName)
                .that(successfulPage.getSelectedAssets().get(1))
                .contains(secondAssetName);

        // Click on OK button
        workHubPage = successfulPage.clickOKButton();

        // Find the first asset
        AllAssetsAssetElement assetCard = workHubPage.getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(firstAssetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(firstAssetName))
                .findFirst()
                .get();

        // Navigate to View Asset page
        ViewAssetPage viewAssetPage = assetCard.clickViewAssetButton();

        // Navigate to Codicils and defects page
        CodicilsAndDefectsPage codicilsAndDefectsPage = viewAssetPage.clickCodicilsAndDefectsTab();

        // Verify that the Asset Note has been added
        assert_().withFailureMessage("Asset Note for asset '%s' is expected to be displayed on Codicils and defects page.", firstAssetName)
                .that(codicilsAndDefectsPage.getAssetNote().size())
                .isEqualTo(1);

        // Verify that the Actionable item has been added
        assert_().withFailureMessage("Actionable item for asset '%s' is expected to be displayed on Codicils and defects page.", firstAssetName)
                .that(codicilsAndDefectsPage.getActionableItems().size())
                .isEqualTo(1);

        // Find the second asset
        assetCard = codicilsAndDefectsPage.getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(secondAssetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(secondAssetName))
                .findFirst()
                .get();

        // Navigate to View Asset page
        viewAssetPage = assetCard.clickViewAssetButton();

        // Navigate to Codicils and defects page
        codicilsAndDefectsPage = viewAssetPage.clickCodicilsAndDefectsTab();

        // Verify that the Asset Note has been added
        assert_().withFailureMessage("Asset Note for asset '%s' is expected to be displayed on Codicils and defects page.", secondAssetName)
                .that(codicilsAndDefectsPage.getAssetNote().size())
                .isEqualTo(1);

        // Verify that the Actionable item has been added
        assert_().withFailureMessage("Actionable item for asset '%s' is expected to be displayed on Codicils and defects page.", secondAssetName)
                .that(codicilsAndDefectsPage.getActionableItems().size())
                .isEqualTo(1);
    }
}
