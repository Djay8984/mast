package test;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.cases.viewcase.casedetails.CaseDetailsPage;
import viewasset.sub.cases.viewcase.casedetails.EditCasePage;
import workhub.WorkHubPage;

import java.sql.SQLException;

import static com.google.common.truth.Truth.assert_;

public class EditCaseForAnAssetTest extends BaseTest
{
    /**
     * Edit Case data
     */
    final String contractReference = "Edit Case Test";
    final String assetName = "AssetName Edited";
    final String grossTonnage = "425";
    final String portOfRegistry = "Abenra";
    final String currentFlag = "Angola";
    final String proposedFlag = "Argentina";
    final String caseCfo = "Newcastle upon Tyne";
    final String jobSdo = "Qingdao";
    final String leadSurveyor = "Min Yi Chan";
    final String tso = "Liverpool";

    @Test(description = "R2 User Journey 10: Editing of a Case for an Asset")
    @Issue("LRD-3899")

    public void editCaseForAnAssetTest() throws SQLException
    {
        CasesPage casesPage = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createImoRegisteredAsset()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectAicRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createAicCase();

        EditCasePage editCasePage = casesPage.getCaseByCaseType("AIC")
                .clickCaseDetails()
                .clickCaseDetailsTab()
                .clickEditButton();

        String expectedClassStatus = editCasePage.getClassStatusDropdownText(2);
        String expectedLifeCycleStatus = editCasePage.getLifecycleStatusDropdownTest(2);
        CaseDetailsPage caseDetailsPage = editCasePage.setContractReference(contractReference)
                .setAssetName(assetName)
                .setGrossTonnage(grossTonnage)
                .selectLifecycleStatusDropdown(2)
                .selectClassStatusDropdown(2)
                .setPortOfRegistry(portOfRegistry)
                .setCurrentFlag(currentFlag)
                .setProposedFlag(proposedFlag)
                .clickSingleButton()
                .setCaseCfo(caseCfo)
                .setJobSdo(jobSdo)
                .setLeadSurveyor(leadSurveyor)
                .setTso(tso)
                .clickSaveButton();

        assert_().withFailureMessage("Edited contract reference is expected to be present ")
                .that(caseDetailsPage.getContractReference())
                .isEqualTo(contractReference);

        assert_().withFailureMessage("Edited assetName is expected to be present ")
                .that(caseDetailsPage.getAssetName())
                .isEqualTo(assetName);

        assert_().withFailureMessage("Edited grossTonnage is expected to be present ")
                .that(caseDetailsPage.getGrossTonnage())
                .isEqualTo(grossTonnage + " m³");

        assert_().withFailureMessage("Edited LifeCycleStatus is expected to be present ")
                .that(caseDetailsPage.getLifecycleStatus())
                .isEqualTo(expectedLifeCycleStatus);

        assert_().withFailureMessage("Edited ClassStatus is expected to be present ")
                .that(caseDetailsPage.getClassStatus())
                .isEqualTo(expectedClassStatus);

        assert_().withFailureMessage("Edited portOfRegistry is expected to be present ")
                .that(caseDetailsPage.getPortOfRegistry())
                .isEqualTo(portOfRegistry);

        assert_().withFailureMessage("Edited currentFlag is expected to be present ")
                .that(caseDetailsPage.getCurrentFlag())
                .isEqualTo(currentFlag);

        assert_().withFailureMessage("Edited proposedFlag is expected to be present ")
                .that(caseDetailsPage.getProposedFlag())
                .isEqualTo(proposedFlag);

        assert_().withFailureMessage("Edited caseCfo is expected to be present ")
                .that(caseDetailsPage.getCfo().toLowerCase())
                .isEqualTo(caseCfo.toLowerCase());

        assert_().withFailureMessage("Edited jobSdo is expected to be present ")
                .that(caseDetailsPage.getJobSdo())
                .isEqualTo(jobSdo);

        assert_().withFailureMessage("Edited Tso is expected to be present ")
                .that(caseDetailsPage.getTso())
                .isEqualTo(tso);

        assert_().withFailureMessage("Edited leadSurveyor is expected to be present ")
                .that(caseDetailsPage.getLeadSurveyor())
                .isEqualTo(leadSurveyor);
    }
}
