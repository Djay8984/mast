package test;

import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.buildassetmodel.AddFromReferenceDataPage;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.ListViewItemElement;
import viewasset.sub.assetmodel.element.ReferenceItemElement;
import workhub.WorkHubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class CreateAssetModelUsingReferenceDataTest extends BaseTest
{
    private HeaderPage headerPage;

    @Test(description = "R2 User Journey 6: Create an Asset Model by using reference Data")
    @Issue("LRD-3894")
    public void createAssetModelUsingReferenceDataTest()
    {
        WorkHubPage workHubPage = WorkHubPage.open();
        headerPage = workHubPage.getHeader();

        AddFromReferenceDataPage addFromReferenceDataPage = workHubPage
                .clickAddNewAsset()
                .createImoRegisteredAsset()
                .clickAssetModelTab()
                .clickCheckOutForEditingButton()
                .clickAddFromReferenceDataButton();

        List<ReferenceItemElement> referenceItemElements = addFromReferenceDataPage
                .getReferenceItems();
        List<String> expectedReferenceItems = referenceItemElements
                .stream()
                .map(ReferenceItemElement::getReferenceItemName)
                .collect(Collectors.toList());

        referenceItemElements.forEach(ReferenceItemElement::clickCheckbox);
        AssetModelSubPage assetModelSubPage = addFromReferenceDataPage
                .clickAddSelected()
                .clickFinishButton();
        List<ListViewItemElement> listViewItemElements
                = assetModelSubPage.clickListViewButton()
                .getAssetItems();

        List<String> actualListViewItemElements = listViewItemElements
                .stream()
                .map(ListViewItemElement::getItemName)
                .collect(Collectors.toList());

        assert_().withFailureMessage("Added items from reference data is expected to be displayed")
                .that(expectedReferenceItems)
                .isEqualTo(actualListViewItemElements);

        List<String> actualBuildAssetModelItems = assetModelSubPage
                .clickCheckOutForEditingButton()
                .getBuildAssetModelTable()
                .getRows()
                .get(0)
                .getChildren()
                .stream()
                .map(DataElement::getItemName)
                .collect(Collectors.toList());
        assert_().withFailureMessage("Added items from reference data is expected to be displayed")
                .that(expectedReferenceItems)
                .isEqualTo(actualBuildAssetModelItems);
    }

    @AfterTest(alwaysRun = true)
    public void discardCheckoutForAllAssets()
    {
        headerPage.discardCheckoutForAllAssets();
    }
}
