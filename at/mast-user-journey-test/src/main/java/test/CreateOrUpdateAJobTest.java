package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SelectCaseTypePage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.details.EditJobPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import java.sql.Date;
import java.time.LocalDate;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static constant.LloydsRegister.JobData.*;
import static constant.LloydsRegister.randomiseConstant;

public class CreateOrUpdateAJobTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "User Journey 11: Create/Update a Job")
    @Issue("LRD-3900")
    public void CreateOrUpdateAJob()
    {

        // Create a new Asset
        AllAssetsAssetElement assetCard = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName()
                        .equals(assetName))
                .findFirst()
                .get();

        ViewAssetPage viewAssetPage = assetCard.clickViewAssetButton();

        // Create new Case
        CasesPage casesPage = viewAssetPage.clickCasesTab();
        final SelectCaseTypePage selectCaseTypePage = casesPage.clickAddCaseButton();

        final SpecifyDetailsPage specifyDetailsPage = selectCaseTypePage
                .selectAimcInShipRadioButton()
                .then()
                .clickNextButton(SpecifyDetailsPage.class);
        casesPage = specifyDetailsPage.createAimcInShipCase();

        assetCard = casesPage.getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName()
                        .equals(assetName))
                .findFirst()
                .get();

        viewAssetPage = assetCard.clickViewAssetButton();

        // Create a new job for the newly created asset
        final JobsSubPage jobsSubPage = viewAssetPage.clickJobsTab();
        final AddNewJobPage addNewJobPage = jobsSubPage.clickAddNewJobButton();
        ViewJobDetailsPage viewJobDetailsPage = addNewJobPage.selectCaseTypeByIndex(1)
                .then().selectJobCategorySurvey()
                .then().setJobLocation(location)
                .then().setJobDescription(description)
                .then().setEtaDate(strEtaDate)
                .then().setEtdDate(strEtdDate)
                .then().setRequestedAttendanceDate(strEtdDate)
                .then().setSdoCode(sdoCode)
                .then().clickSaveButton();

        // Check Add Team Page
        final AddOrEditJobTeamPage addOrEditJobTeamPage = viewJobDetailsPage.clickAddOrGoToJobTeamButton();

        assert_().withFailureMessage("The Lead Surveyor field is expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isLeadSurveyorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The SDO Coordinator field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isSdoCoordinatorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The Authorising Surveyor field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isAuthorisingSurveyorTextBoxEnabled())
                .isTrue();
        assert_().withFailureMessage("The Fleet services Specialist field is  expected to be displayed and editable")
                .that(addOrEditJobTeamPage.isFleetServicesSpecialistTextBoxEnabled())
                .isTrue();

        viewJobDetailsPage = addOrEditJobTeamPage.clickCancelButton();
        final AddToJobScopePage addToJobScopePage = viewJobDetailsPage
                .clickAddOrGoToJobScopeButton(AddToJobScopePage.class);

        viewJobDetailsPage = addToJobScopePage.clickNavigateBackButton();
        final EditJobPage editJobPage = viewJobDetailsPage.getHeader().clickEditButton();

        // Edit job with new data and check if the changes have taken its effect in the job
        final String locationEdit = LloydsRegister.JobData.location + "Mundra";
        final String strEtaDateEdit = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().minusDays(1))));
        final String strEtdDateEdit = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now().plusDays(10))));
        final String strRequestedDateEdit = String.valueOf(FRONTEND_TIME_FORMAT.format(Date.valueOf(LocalDate.now())));

        editJobPage.enterEtaDate(strEtaDateEdit)
                .enterEtdDate(strEtdDateEdit)
                .enterRequestedAttendanceDate(strRequestedDateEdit)
                .enterLocation(locationEdit);

        viewJobDetailsPage = editJobPage.clickSaveButton();

        assert_().withFailureMessage("The Job details are expected to be updated")
                .that(viewJobDetailsPage.getHeader().getLocation()).isEqualTo(locationEdit);
        // TODO: Uncomment once LRD-2878 is resolved
        /*
         * assert_().withFailureMessage("The Job details are expected to be updated")
         * .that(viewJobDetailsPage.getHeader().getEtaDate()).isEqualTo(strEtaDateEdit); assert_().withFailureMessage(
         * "The Job details are expected to be updated")
         * .that(viewJobDetailsPage.getHeader().getEtdDate()).isEqualTo(strEtdDateEdit);
         */
    }
}
