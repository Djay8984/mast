package test;

import com.frameworkium.core.ui.tests.BaseTest;
import constant.LloydsRegister;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.cases.addcase.element.ViewCaseElement;
import viewasset.sub.cases.viewcase.ViewCasePage;
import workhub.WorkHubPage;
import workhub.element.AllAssetsAssetElement;

import static com.google.common.truth.Truth.assert_;
import static constant.LloydsRegister.randomiseConstant;

public class CancelCaseTest extends BaseTest
{

    private final String assetName = randomiseConstant(LloydsRegister.AssetData.assetName);

    @Test(description = "R2 User Journey 35:Cancelling a case")
    @Issue("LRD-4174")
    public void cancelCaseTest()
    {

        // Create a new asset
        AllAssetsAssetElement assetCard = WorkHubPage
                .open()
                .clickAddNewAsset()
                .createNonImoRegisteredAsset(assetName)
                .getHeader()
                .clickMastLogo()
                .clickSearchFilterAssetsButton()
                .setAssetSearchInput(assetName)
                .clickAssetSearchApplyButton(WorkHubPage.class)
                .clickAllAssetsTab()
                .getAssetCards()
                .stream()
                .filter(e -> e.getAssetName().equals(assetName))
                .findFirst()
                .get();

        // Add a new case to the asset
        CasesPage casesPage = assetCard.clickViewAssetButton()
                .clickCasesTab()
                .clickAddCaseButton()
                .selectFirstEntryRadioButton()
                .clickNextButton(SpecifyDetailsPage.class)
                .createFirstEntryCase();

        // View the added case
        ViewCaseElement viewCaseElement = casesPage.getCaseByIndex(0);
        ViewCasePage viewCasePage = viewCaseElement.clickCaseDetails();

        // Init test data
        String reasonForCancelling = "test";
        String caseStatus = "Cancelled";

        // Cancel the added case
        viewCasePage = viewCasePage.clickCancelCaseButton()
                .setReasonForCancelling(reasonForCancelling)
                .clickCancelCaseButton();

        // Verify that the case has been cancelled on View Case page
        assert_().withFailureMessage("Reason for cancelling is expected to be displayed on View Case page")
                .that(viewCasePage.getReasonForCancelling())
                .contains(reasonForCancelling);

        assert_().withFailureMessage("Case status is expected to be displayed on View Case page")
                .that(viewCasePage.getCaseStatus())
                .isEqualTo(caseStatus);

        // Navigate to Cases page
        casesPage = viewCasePage.clickNavigateBackButton();

        // Verify that the case has been cancelled on Cases page
        assert_().withFailureMessage("Case status is expected to be displayed on Cases page")
                .that(casesPage.getCaseByIndex(0).getCaseStatus())
                .isEqualTo(caseStatus);
    }
}

