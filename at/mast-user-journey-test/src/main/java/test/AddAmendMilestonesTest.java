package test;

import com.frameworkium.core.ui.tests.BaseTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;
import viewasset.sub.cases.viewcase.ViewCasePage;
import viewasset.sub.cases.viewcase.milestones.ManageMilestonesPage;
import viewasset.sub.cases.viewcase.milestones.MilestonesSubPage;
import workhub.WorkHubPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class AddAmendMilestonesTest extends BaseTest
{

    @Test(description = "R2 User Journey 15: Add/Amend Milestones")
    @Issue("LRD-3920")
    public void addAmendMilestonesTest()
    {

        CasesPage casesPage = WorkHubPage.open()
                .then().clickAddNewAsset()
                .then().createNonImoRegisteredAsset()
                .then().clickCasesTab()
                .then().clickAddCaseButton()
                .then().selectFirstEntryRadioButton()
                .then().clickNextButton(SpecifyDetailsPage.class)
                .then().createFirstEntryCase();

        ViewCasePage viewCasePage = casesPage.getCaseByIndex(0)
                .clickCaseDetails();

        MilestonesSubPage milestonesSubPage = viewCasePage.clickMilestonesTab()
                .then().checkMilestoneCheckboxByIndex(0)
                .then().clickSaveChangesToMilestones()
                .then().clickConfirmCompleteMilestoneButton();

        assert_().withFailureMessage("Milestone should be marked as complete")
                .that(milestonesSubPage.getMilestoneByIndex(0).isMilestoneCompleted())
                .isEqualTo(true);

        ManageMilestonesPage manageMilestonesPage = milestonesSubPage.clickManageMilestonesButton();

        List<String> itemDescriptionsChangingScope = manageMilestonesPage.checkAvailableCheckboxes()
                .then().getScopeChangeableDescriptions();

        List<String> itemDescriptionsMarkedAsOutOfScope = manageMilestonesPage.clickMilestoneOutOfScope()
                .then().clickSaveAndConfirm()
                .then().clickManageMilestonesButton()
                .then().clickOutOfScopeTab()
                .then().getScopeChangeableDescriptions();

        checkThatItemsHaveChangedScopeCorrectly(itemDescriptionsChangingScope, itemDescriptionsMarkedAsOutOfScope);

        itemDescriptionsChangingScope.clear();

        itemDescriptionsChangingScope = manageMilestonesPage.checkAvailableCheckboxes()
                .then().getScopeChangeableDescriptions();

        List<String> itemsInScope = manageMilestonesPage.clickMilestoneInScope()
                .then().clickSaveAndConfirm()
                .then().clickManageMilestonesButton()
                .then().clickInScopeTab()
                .then().getScopeChangeableDescriptions();

        checkThatItemsHaveChangedScopeCorrectly(itemDescriptionsChangingScope, itemsInScope);
    }

    private void checkThatItemsHaveChangedScopeCorrectly(List<String> itemsMarkedToChangeScope, List<String> itemsInDestinationScopeTab)
    {
        List<String> itemsThatHaveChangedScope = itemsMarkedToChangeScope
                .stream()
                .filter(itemsInDestinationScopeTab::contains)
                .collect(Collectors.toList());

        assert_().withFailureMessage("All items that were marked as out of scope should now appear in the out of scope tab")
                .that(itemsMarkedToChangeScope).isEqualTo(itemsThatHaveChangedScope);
    }
}
