package util;

import com.frameworkium.core.ui.js.JavascriptWait;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AppHelper
{

    public static void waitForModal()
    {
        WebDriverWait wait = new WebDriverWait(BaseTest.getDriver(), 10);
        wait.pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".modal-overlay")));
        wait.pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".modal-overlay")));
    }

    public static void waitForOnScreenAnimation()
    {
        //Tried every dynamic implementation of trying to get this stable, but zero ways worked... I want to cry.
        try
        {
            Thread.sleep(200);
        }
        catch (InterruptedException e)
        {
            //Ignore
        }
        WebDriverWait wait = new WebDriverWait(BaseTest.getDriver(), 10);
        wait.pollingEvery(2000, TimeUnit.MILLISECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ng-animate")));
        new JavascriptWait(BaseTest.getDriver(), wait).waitForJavascriptFramework();
    }

    public static void scrollToBottom()
    {
        BaseTest.getDriver().executeScript("document.querySelector('.main-container').scrollTop = 10000");
    }

}
