package util;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ClickClearHelper
{

    public static void clickClearText(WebElement webElement)
    {
        Double width = webElement.getSize().getWidth() * 0.95;
        Double y = webElement.getSize().getHeight() * 0.9;
        Actions builder = new Actions(BaseTest.getDriver());
        builder.moveToElement(webElement, width.intValue(), y.intValue()).click().build().perform();
    }

}
