package util;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This page object is specifically designed to help with the calendar popup which MAST uses.
 * <p>
 * Pass in the ul.dropdown-menu which is for the calendar DataElement on the page
 */
public class CalendarHelper extends BasePage<CalendarHelper>
{

    private WebElement calendarElement;
    private WebElement previousMonthButton;
    private WebElement nextMonthButton;
    private WebElement monthYearTitle;

    private SimpleDateFormat month = new SimpleDateFormat("MMM");
    private SimpleDateFormat year = new SimpleDateFormat("yyyy");
    private SimpleDateFormat day = new SimpleDateFormat("dd");

    /**
     * Constructor for CalendarHelper, requires the ul DataElement for the calendar dropdown
     *
     * @param calendarElement - ul DataElement of the calendar
     */
    public CalendarHelper(final WebElement calendarElement)
    {
        this.calendarElement = calendarElement;
        this.nextMonthButton = calendarElement.findElement(By.cssSelector("thead tr:nth-child(1) th:nth-child(3)"));
        this.previousMonthButton = calendarElement.findElement(By.cssSelector("thead tr:nth-child(1) th:nth-child(1)"));
        this.monthYearTitle = calendarElement.findElement(
                By.cssSelector("thead tr:nth-child(1) th:nth-child(2)"));
    }

    /**
     * Browses to the year and the month which is needed
     *
     * @param year  - The year to select
     * @param month - The month to select
     */
    public void selectYearAndMonth(long year, Month month)
    {
        if (getCalendarYear() < year)
        {
            do
            {
                nextMonthButton.click();
            }
            while (getCalendarYear() != year);
        }
        else if (getCalendarYear() > year)
        {
            do
            {
                previousMonthButton.click();
            }
            while (getCalendarYear() != year);
        }
        if (getCalendarMonth().getMonthNumber() < month.getMonthNumber())
        {
            do
            {
                nextMonthButton.click();
            }
            while (getCalendarMonth() != month);
        }
        else if (getCalendarMonth().getMonthNumber() > month.getMonthNumber())
        {
            do
            {
                previousMonthButton.click();
            }
            while (getCalendarMonth() != month);
        }
    }

    /**
     * Selects a day to use on the calendar. This can be used without selecting a month/year, so only a date
     * can be selected upon the calendar opening
     *
     * @param day - Day required
     */
    public void selectDay(int day)
    {
        List<WebElement> calendarDays = calendarElement.findElements(By.cssSelector("tbody td"));
        for (WebElement calendarDay : calendarDays)
        {
            if (calendarDay.getText().equals(Integer.toString(day))
                    && !calendarDay.getAttribute("class").contains("past"))
            {
                calendarDay.click();
                break;
            }
        }
    }

    /**
     * Parses a java date to select the date on the calendar
     *
     * @param date - Java date to select on the calendar
     */
    public void selectDate(Date date)
    {
        String requiredMonth = this.month.format(date);
        String requiredYear = this.year.format(date);
        if (!getCalendarMonth().getShortName().equals(requiredMonth)
                || Long.parseLong(requiredYear) != getCalendarYear())
        {
            for (Month month : Month.values())
            {
                if (month.getShortName().equals(requiredMonth))
                {
                    this.selectYearAndMonth(Long.parseLong(requiredYear), month);
                    break;
                }
            }
        }
        selectDay(Integer.parseInt(this.day.format(date)));
    }

    private String[] getMonthAndYear()
    {
        return monthYearTitle.getText().split("-");
    }

    private long getCalendarYear()
    {
        String[] monthAndYear = getMonthAndYear();
        return Long.parseLong(monthAndYear[0]);
    }

    private Month getCalendarMonth()
    {
        String[] monthAndYear = getMonthAndYear();
        for (Month month : Month.values())
        {
            if (month.getShortName().equals(monthAndYear[1]))
            {
                return month;
            }
        }
        return null;
    }

    /**
     * Month enum to use when selecting by months. Contains the number and shorthand name in
     * the format the calendar uses
     */
    public enum Month
    {
        JANUARY(1, "Jan"),
        FEBRUARY(2, "Feb"),
        MARCH(3, "Mar"),
        APRIL(4, "Apr"),
        MAY(5, "May"),
        JUNE(6, "Jun"),
        JULY(7, "Jul"),
        AUGUST(8, "Aug"),
        SEPTEMBER(9, "Sep"),
        OCTOBER(10, "Oct"),
        NOVEMBER(11, "Nov"),
        DECEMBER(12, "Dec");

        private int monthNumber;
        private String shortName;

        Month(int monthNumber, String shortName)
        {
            this.monthNumber = monthNumber;
            this.shortName = shortName;
        }

        public int getMonthNumber()
        {
            return this.monthNumber;
        }

        public String getShortName()
        {
            return this.shortName;
        }
    }

}
