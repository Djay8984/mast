package viewasset;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.assetheaderadditionalinformation.AssetHeaderAdditionalInformationPage;
import viewasset.assetheaderadditionalinformation.AttachmentsAndNotesPage;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.serviceschedule.ServiceSchedulePage;

import java.util.List;

public class ViewAssetPage extends BasePage<ViewAssetPage>
{

    @Name("Asset model tab")
    @FindBy(css = "#asset-tab-container div.tab-item:nth-child(1)")
    @Visible
    private WebElement assetModelTab;

    @Name("Service schedule Tab")
    @FindBy(css = "#asset-tab-container div.tab-item:nth-child(2)")
    @Visible
    private WebElement serviceScheduleTab;

    @Name("Cases Tab")
    @FindBy(css = "#asset-tab-container div.tab-item:nth-child(3)")
    @Visible
    private WebElement casesTab;

    @Name("Jobs Tab")
    @FindBy(css = "#asset-tab-container div.tab-item:nth-child(4)")
    @Visible
    private WebElement jobsTab;

    @Name("Codicils and defects")
    @FindBy(css = "#asset-tab-container div.tab-item:nth-child(5)")
    @Visible
    private WebElement codicilsAndDefectsTab;

    @Name("Build asset model")
    @FindBy(className = "primary-button")
    private WebElement buildAssetModel;

    @Visible
    @Name("Asset Emblem")
    @FindBy(css = ".asset-class")
    private WebElement assetEmblem;

    @Name("Asset Name")
    @FindBy(id = "asset-subheader-asset-name")
    private WebElement assetName;

    @Name("IMO Number")
    @FindBy(css = "[data-ng-if='vm.asset.ihsAsset.id'] strong")
    private List<WebElement> imoNumber;

    @Name("Asset Type")
    @FindBy(css = "[data-ng-if='vm.asset.assetType'] strong")
    private List<WebElement> assetType;

    @Name("Date of build")
    @FindBy(css = "[data-ng-if='vm.asset.buildDate'] strong")
    private List<WebElement> dateOfBuild;

    @Name("Gross Tonnage")
    @FindBy(css = "[data-ng-if='vm.asset.grossTonnage'] strong")
    private List<WebElement> grossTonnage;

    @Name("Flag")
    @FindBy(css = "[data-ng-if='vm.asset.flagState.name'] strong")
    private List<WebElement> flag;

    @Name("Class Status")
    @FindBy(css = "[data-ng-if='vm.asset.classStatus'] strong")
    private List<WebElement> classStaus;

    @Name("View More Button")
    @FindBy(css = "button[class='transparent-dark-button']")
    private WebElement viewMoreButton;

    @Name("Notes And Attachment Icon")
    @FindBy(css = ".attachment-directive-icon-dark")
    private WebElement notesAndAttachmentIcon;

    @Name("Expand Image in Header Tab ")
    @FindBy(css = "[data-ng-click='vm.subheaderOpen = true']")
    private WebElement expandButtonInHeader;

    @Name("Collapse Image in Header Tab ")
    @FindBy(css = "[data-ng-click='vm.subheaderOpen = false']")
    private List<WebElement> collapseButtonInHeader;

    @Name("Expand/Collapse Image in Header Tab ")
    @FindBy(id = "logo-asset-header-toggle-subheader")
    private List<WebElement> expandOrCollapseButtonInHeader;

    @Step("Check if grossTonnage is Displayed")
    public boolean isGrossTonnagePresent()
    {
        return grossTonnage.size() > 0;
    }

    @Step("Check if imoNumber is Displayed")
    public boolean isImoNumberPresent()
    {
        return imoNumber.size() > 0;
    }

    @Step("Check if assetType is Displayed")
    public boolean isAssetTypePresent()
    {
        return assetType.size() > 0;
    }

    @Step("Check if dateOfBuild is Displayed")
    public boolean isDateOfBuildPresent()
    {
        return dateOfBuild.size() > 0;
    }

    @Step("Check if flag is Displayed")
    public boolean isFlagPresent()
    {
        return flag.size() > 0;
    }

    @Step("Check if Class Status is Displayed")
    public boolean isClassStatusDisplayed()
    {
        return classStaus.size() > 0;
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Click Notes and Attachments Icon")
    public AttachmentsAndNotesPage clickNotesAndAttachmentsIcon()
    {
        notesAndAttachmentIcon.click();
        new WebDriverWait(BaseTest.getDriver(), 25).until(ExpectedConditions.visibilityOfElementLocated
                (By.cssSelector(".modalwindow.is-active")));
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Click Notes and Attachments Icon")
    public AssetHeaderAdditionalInformationPage clickViewMoreButton()
    {
        viewMoreButton.click();
        return PageFactory.newInstance(AssetHeaderAdditionalInformationPage.class);
    }

    @Step("Click Expand Button in Asset Header")
    public ViewAssetPage clickExpandButtonInHeader()
    {
        expandButtonInHeader.click();
        return this;
    }

    @Step("Check if Expand or Collapse Button is present in Asset Header")
    public boolean isExpandOrCollapseButtonPresent()
    {
        return expandOrCollapseButtonInHeader.size() > 0;
    }

    @Step("Check if the Asset Header is Expanded")
    public boolean isAssetHeaderExpanded()
    {
        return collapseButtonInHeader.size() > 0;
    }

    @Step("Clicking the Asset model tab")
    public AssetModelSubPage clickAssetModelTab()
    {
        assetModelTab.click();
        return PageFactory.newInstance(AssetModelSubPage.class);
    }

    @Step("Clicking the Service schedule tab")
    public ServiceSchedulePage clickServiceScheduleTab()
    {
        serviceScheduleTab.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Clicking the Cases tab")
    public CasesPage clickCasesTab()
    {
        casesTab.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Clicking the Jobs Tab")
    public JobsSubPage clickJobsTab()
    {
        jobsTab.click();
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Clicking the Codicils and defects")
    public CodicilsAndDefectsPage clickCodicilsAndDefectsTab()
    {
        codicilsAndDefectsTab.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    /**
     * Use this get to return Asset Model Subpage after
     * MyWorkAssetElement.clickViewAssetButton()
     * AllAssetsAssetElement.clickViewAssetButton()
     * <p>
     * This is because methods above returns ViewAssetPage which defaults
     * to viewasset.assetmodel.AssetModelSubPage and viewasset.assetmodel.assetmodel
     * .ListViewSubPage.
     *
     * @return AssetModelSubPage
     */
    @Step("Get Asset Model Subpage")
    public AssetModelSubPage getAssetModelSubPage()
    {
        return PageFactory.newInstance(AssetModelSubPage.class);
    }

    /**
     * Use this getter to return Jobs Subpage after
     * MyWorkAssetElement.clickJobCountButton()
     * <p>
     * This is because method(s) above returns ViewAssetPage which defaults
     * to viewasset.JobsSubPage
     *
     * @return
     */
    @Step("Get Jobs Subpage")
    public JobsSubPage getJobsSubPage()
    {
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Check that Asset Model Tab is selected")
    public boolean isAssetModelTabSelected()
    {
        return assetModelTab.getAttribute("class").contains("active");
    }

    @Step("Check that Service Schedule Tab is selected")
    public boolean isServiceScheduleTab()
    {
        return serviceScheduleTab.getAttribute("class").contains("active");
    }

    @Step("Check that Service Schedule Tab is selected")
    public boolean isCasesTabSelected()
    {
        return casesTab.getAttribute("class").contains("active");
    }

    @Step("Check that Jobs Tab is selected")
    public boolean isJobsTabSelected()
    {
        return jobsTab.getAttribute("class").contains("active");
    }

    @Step("Check that Codicils and Defects Tab is selected")
    public boolean isCodicilsAndDefectsTab()
    {
        return codicilsAndDefectsTab.getAttribute("class").contains("active");
    }
}
