package viewasset.sub.serviceschedule.addproducts;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ServiceSchedulePage;
import viewasset.sub.serviceschedule.element.ProductElement;

import java.util.List;

public class AddProductPage extends BasePage<AddProductPage>
{

    @Name("Back button")
    @FindBy(css = "#add-product-header-back svg")
    @Visible
    private WebElement backButton;

    @Name("Selected Product Family Name")
    @FindBy(css = "h2 .ng-binding")
    @Visible
    private WebElement selectedProductFamilyName;

    @Name("Restricted by flag")
    @FindBy(css = ".segmented li:nth-child(2)")
    @Visible
    private WebElement restrictedByFlag;

    @Name("Not restricted by flag")
    @FindBy(css = ".segmented li:nth-child(1)")
    @Visible
    private WebElement notRestrictedByFlag;

    @Name("Button Cancel")
    @FindBy(id = "footer-cancel")
    @Visible
    private WebElement buttonCancel;

    @Name("Button Add")
    @FindBy(id = "footer-confirm")
    @Visible
    private WebElement buttonAdd;

    private List<ProductElement> ProductElements;

    @Step("Clicking back button to navigate back")
    public ServiceSchedulePage clickBackButton()
    {
        backButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-tab-service-schedule")));
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Retrieve Selected Product Name")
    public String getSelectedProductFamilyName()
    {
        return selectedProductFamilyName.getText();
    }

    @Step("Verify if Not restricted by flag selected")
    public boolean isNotRestrictetByFlagSelected()
    {
        return notRestrictedByFlag.findElement(By.tagName("input")).getAttribute("class").contains("ng-valid-parse");
    }

    @Step("Select Not restricted by flag")
    public void selectNotRestrictedByFlag()
    {
        if (!isNotRestrictetByFlagSelected())
            notRestrictedByFlag.click();
    }

    @Step("Verify if Restricted by flag selected")
    public boolean isRestrictetByFlagSelected()
    {
        return restrictedByFlag.findElement(By.tagName("input")).getAttribute("class").contains("ng-valid-parse");
    }

    @Step("Select Restricted by flag")
    public void selectRestrictedByFlag()
    {
        if (!isNotRestrictetByFlagSelected())
            restrictedByFlag.click();
    }

    @Step("Retrieve all product elements")
    public List<ProductElement> getProductElements()
    {
        return ProductElements;
    }

    @Step("Retrieve all product elements")
    public ProductElement getProductElementByProductName(String name)
    {
        return getProductElements().stream()
                .filter(x -> x.getProductName().equals(name))
                .findFirst()
                .get();
    }

    @Step("Clicking Cancel button")
    public ServiceSchedulePage clickButtonCancel()
    {
        buttonCancel.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Clicking Add button")
    public ServiceSchedulePage clickButtonAdd()
    {
        buttonAdd.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-tab-service-schedule")));
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Is Add button enabled")
    public boolean isButtonAddEnabled()
    {
        return buttonAdd.isEnabled();
    }

}
