package viewasset.sub.serviceschedule.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ClassificationSubPage;

public class RemoveServicesConfirmationPage extends BasePage<RemoveServicesConfirmationPage>
{

    @Visible
    @Name("Remove Services Button")
    @FindBy(css = "[data-ng-click='vm.confirm()'")
    private WebElement removeServiceButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$dismiss()']")
    private WebElement cancelButton;

    @Step("Click Remove Service Button")
    public ClassificationSubPage clickRemoveServiceButton()
    {
        removeServiceButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(
                By.cssSelector(".modal is-active")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(
                By.cssSelector("[data-ng-click='vm.deleteSelectedServices()'] svg")));
        return PageFactory.newInstance(ClassificationSubPage.class);
    }

    @Step("Click Cancel Button")
    public ClassificationSubPage clickCancelButton()
    {
        cancelButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(
                By.cssSelector(".modal is-active")));
        return PageFactory.newInstance(ClassificationSubPage.class);
    }
}
