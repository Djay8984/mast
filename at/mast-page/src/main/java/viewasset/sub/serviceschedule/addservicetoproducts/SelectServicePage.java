package viewasset.sub.serviceschedule.addservicetoproducts;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class SelectServicePage extends BasePage<SelectServicePage>
{

    @Visible
    @Name("Next Button")
    @FindBy(id = "footer-confirm")
    private WebElement nextButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "footer-cancel")
    private WebElement cancelButton;

    @Name("Services Check Box")
    @FindBy(css = "[data-ng-if='vm.showOption(opt)']")
    private List<WebElement> servicesCheckBoxes;

    @Step("Select check box by name")
    public SelectServicePage selectCheckBoxByName(String name)
    {
        servicesCheckBoxes.stream().filter(e -> e.findElement(By.tagName("label")).getText().trim().equals(name))
                .findFirst()
                .get().findElement(By.tagName("button")).click();
        return this;
    }

    @Step("Click next button")
    public SpecifyDetailsPage clickNextButton()
    {
        nextButton.click();
        return PageFactory.newInstance(SpecifyDetailsPage.class);
    }
}
