package viewasset.sub.serviceschedule.element;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;
import java.util.stream.Collectors;

@Name("Product element")
@FindBy(css = "form [data-ng-repeat='option in vm.visibleOptions']")
public class ProductElement extends HtmlElement
{

    @Name("Product checkbox")
    @FindBy(css = "button.checkbox-dir")
    private WebElement productCheckBox;

    @Name("Product name")
    @FindBy(tagName = "label")
    private WebElement productName;

    @Name("Service Regime dropdown")
    @FindBy(css = "select[data-ng-model]")
    private WebElement serviceRegimeDropDown;

    @Step("Is product checkbox checked")
    public boolean isProductCheckboxChecked()
    {
        return productCheckBox.getAttribute("class").contains("checked");
    }

    @Step("Check product checkbox")
    public void checkProductCheckbox()
    {
        if (!isProductCheckboxChecked())
            productCheckBox.click();
    }

    @Step("Uncheck product checkbox")
    public void uncheckProductCheckbox()
    {
        if (isProductCheckboxChecked())
            productCheckBox.click();
    }

    @Step("Retrieve product name")
    public String getProductName()
    {
        WebDriver driver = BaseTest.getDriver();

        //scroll to the item that are ON screen
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        productName);

        return productName.getText();
    }

    @Step("Get All Service Regime dropdown options")
    public List<String> getServiceRegimeDropDownOptions()
    {
        Select select = new Select(serviceRegimeDropDown);
        return select.getOptions()
                .stream().map(WebElement::getText)
                .sorted((a, b) -> a.compareTo(b))
                .collect(Collectors.toList());
    }

    @Step("Get selected Service Regime dropdown")
    public String getSelectedServiceRegime()
    {
        return serviceRegimeDropDown.getAttribute("value");
    }

    @Step("Set Service Regime dropdown")
    public void setServiceRegimeDropDown(String option)
    {
        new Select(serviceRegimeDropDown).selectByVisibleText(option);
    }

    @Step("Set Service Regime dropdown with last option")
    public void setServiceRegimeDropDownWithLastOption()
    {
        new Select(serviceRegimeDropDown).selectByVisibleText(getServiceRegimeDropDownOptions().get(getServiceRegimeDropDownOptions().size() - 1));
    }
}




