package viewasset.sub.serviceschedule;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.base.BaseTable;
import viewasset.sub.serviceschedule.classification.SelectProductsPage;
import viewasset.sub.serviceschedule.modal.RemoveServicesConfirmationPage;

public class ClassificationSubPage extends BasePage<ClassificationSubPage>
{

    @Visible
    @Name("View By DropDown")
    @FindBy(id = "list-view-by")
    private WebElement viewByDropDown;

    @Name("Add Products Button")
    @FindBy(css = "[data-ng-click='vm.addProduct()']")
    private WebElement addProductsButton;

    @Name("Notice Message")
    @FindBy(css = "[data-ng-if='vm.isEmptySchedule()']")
    private WebElement noticeMessage;

    @Name("Delete Icon")
    @FindBy(css = "[data-ng-click='vm.deleteSelectedServices()'] svg")
    private WebElement deleteIcon;

    @Name("Classification Table")
    private ClassificationTable classificationTable;

    @Step("Is Add Products Button displayed")
    public boolean isAddProductsButtonDisplayed()
    {
        return addProductsButton.isDisplayed();
    }

    @Step("Get Notice Message")
    public String getNoticeMessage()
    {
        return noticeMessage.getText().trim();
    }

    @Step("Click Add Product Button")
    public SelectProductsPage clickAddProductsButton()
    {
        addProductsButton.click();
        return PageFactory.newInstance(SelectProductsPage.class);
    }

    @Step("Get Classification Table")
    public ClassificationTable getClassificationTable()
    {
        return classificationTable;
    }

    @Step("CLick Delete Icon")
    public RemoveServicesConfirmationPage clickDeleteIcon()
    {
        deleteIcon.click();
        return PageFactory.newInstance(RemoveServicesConfirmationPage.class);
    }

    @FindBy(css = "[data-ng-switch-when='product']")
    public class ClassificationTable extends BaseTable
    {
    }
}
