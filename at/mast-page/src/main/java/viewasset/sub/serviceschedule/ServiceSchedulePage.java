package viewasset.sub.serviceschedule;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.addproducts.AddProductPage;
import viewasset.sub.serviceschedule.modal.EditRuleSetPage;

import java.util.List;
import java.util.stream.Collectors;

public class ServiceSchedulePage extends BasePage<ServiceSchedulePage>
{

    @Visible
    @Name("classification Tab")
    @FindBy(css = "div[data-ng-repeat='option in vm.tabOptions']:nth-child(1)")
    private WebElement classificationTab;

    @Visible
    @Name("Statutory Tab")
    @FindBy(css = "div[data-ng-repeat='option in vm.tabOptions']:nth-child(2)")
    private WebElement statutoryTab;

    @Visible
    @Name("View By Drop Down")
    @FindBy(id = "list-view-by")
    private WebElement viewByDropDown;

    @Name("Harmonisation date text")
    @FindBy(id = "list-harmonisation-date")
    private WebElement harmonisationDateText;

    @Name("Harmonisation date Edit Icon")
    @FindBy(css = "[data-ng-click='vm.editHarmonisationDate()']")
    private WebElement harmonisationDateEditIcon;

    @Name("Rule Set Edit Icon")
    @FindBy(css = "[data-ng-click='vm.editRuleSet()']")
    private WebElement ruleSetEditIcon;

    @Name("Add Product")
    @FindBy(css = "[data-ng-click='vm.addProduct()']")
    private WebElement addProduct;

    @Step("Click classification tab")
    public ClassificationSubPage clickClassificationTab()
    {
        classificationTab.click();
        return PageFactory.newInstance(ClassificationSubPage.class);
    }

    @Step("Click statutory tab")
    public StatutorySubPage clickStatutoryTab()
    {
        statutoryTab.click();
        return PageFactory.newInstance(StatutorySubPage.class);
    }

    @Step("Is Classification tab selected")
    public boolean isClassificationTabSelected()
    {
        return classificationTab.getAttribute("class")
                .contains("white-button-grey-border arrowed");
    }

    @Step("Is Statutory tab selected")
    public boolean isStatutoryTabSelected()
    {
        return statutoryTab.getAttribute("class")
                .contains("white-button-grey-border arrowed");
    }

    @Step("Get selected view by dropdown")
    public String getSelectedViewByDropDown()
    {
        return viewByDropDown.getAttribute("value");
    }

    @Step("Get All view by dropdown options")
    public List<String> getViewByDropDownOptions()
    {
        Select select = new Select(viewByDropDown);
        return select.getOptions()
                .stream().map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("Select view by dropDown Option")
    public void selectViewByDropDownOption(ViewBy option)
    {
        new Select(viewByDropDown).selectByVisibleText(option.viewBy);
    }

    @Step("Get Harmonisation Date Text")
    public String getHarmonisationDate()
    {
        return harmonisationDateText.getText();
    }

    @Step("Click on edit rule set icon")
    public EditRuleSetPage clickEditRuleSetIcon()
    {
        ruleSetEditIcon.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(EditRuleSetPage.class);
    }

    @Step("Clicking Add products button")
    public AddProductPage clickAddProductsButton()
    {
        addProduct.click();
        return PageFactory.newInstance(AddProductPage.class);
    }

    public enum ViewBy
    {

        SERVICE_TYPE("Service Type"),
        PRODUCT("Product");

        private String viewBy;

        ViewBy(String viewBy)
        {
            this.viewBy = viewBy;
        }

        public String toString()
        {
            return this.viewBy;
        }
    }
}
