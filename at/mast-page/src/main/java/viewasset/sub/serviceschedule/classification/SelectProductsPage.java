package viewasset.sub.serviceschedule.classification;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ClassificationSubPage;
import viewasset.sub.serviceschedule.classification.element.SelectProductElement;

import java.util.List;

public class SelectProductsPage extends BasePage<SelectProductsPage>
{

    @Visible
    @Name("Add Button")
    @FindBy(id = "footer-confirm")
    private WebElement addButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "footer-cancel")
    private WebElement cancelButton;

    @Name("Select Product Element")
    private List<SelectProductElement> selectProductElement;

    @Step("Click Add Button")
    public ClassificationSubPage clickAddButton()
    {
        addButton.click();
        return PageFactory.newInstance(ClassificationSubPage.class);
    }

    @Step("Get Select Product Element")
    public List<SelectProductElement> getSelectProductElements()
    {
        return selectProductElement;
    }

}
