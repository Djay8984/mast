package viewasset.sub.serviceschedule.service.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Data Elements")
@FindBy(css = "[data-ng-repeat='task in vm.tasks']")
public class DataElement extends HtmlElement
{

    @Name("Task Number")
    @FindBy(css = "[data-ng-bind='task.taskNumber']")
    private WebElement taskNumber;

    @Step("Get Task Number")
    public String getTaskNumber()
    {
        return taskNumber.getText().trim();
    }

}
