package viewasset.sub.serviceschedule.service;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ClassificationSubPage;
import viewasset.sub.serviceschedule.service.base.BaseTable;

import java.util.List;

public class ViewServicePage extends BasePage<ViewServicePage>
{

    @Visible
    @Name("Edit Button")
    @FindBy(css = "[data-ng-click='vm.editService()']")
    private WebElement editButton;

    @Visible
    @Name("Attachment Icon")
    @FindBy(css = "[modal-title='vm.item.name']")
    private WebElement attachmentIcon;

    @Visible
    @Name("Back Button")
    @FindBy(css = ".back-button")
    private WebElement backButton;

    @Name("Product Name")
    @FindBy(css = "[data-ng-bind^='vm.product.name']")
    private WebElement productName;

    @Name("Periodicity Text")
    @FindBy(css = "[data-ng-bind*='cyclePeriodicity']")
    private WebElement periodicityText;

    @Name("Assigned Date")
    @FindBy(css = "[data-ng-bind*='effectiveAssignedDate']")
    private List<WebElement> assignedDate;

    @Name("Due Date")
    @FindBy(css = "[data-ng-bind*='effectiveDueDate']")
    private List<WebElement> dueDate;

    @Name("Lower Date Range")
    @FindBy(css = "[data-ng-bind*='effectiveLowerRangeDate']")
    private List<WebElement> lowerDateRange;

    @Name("Upper Date Range")
    @FindBy(css = "[data-ng-bind*='effectiveUpperRangeDate']")
    private List<WebElement> upperDateRange;

    @Name("Last credited on")
    @FindBy(css = "[data-ng-bind*='lastCreditedOn']")
    private WebElement lastCreditedOnText;

    @Name("Date on completion")
    @FindBy(css = "[data-ng-bind*='dateOnCompletion']")
    private List<WebElement> dateOnCompletion;

    @Name("Service Status")
    @FindBy(css = "[data-ng-bind='vm.displayStatuses()']")
    private WebElement serviceStatus;
    @Name("Task List Table")
    private TaskListTable taskListTable;

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }

    @Step("Get Periodicity")
    public String getPeriodicity()
    {
        return periodicityText.getText().trim();
    }

    @Step("Get Last credited on Text")
    public String getLastCreditedOnText()
    {
        return lastCreditedOnText.getText().trim();
    }

    @Step("Is Assigned Date Displayed")
    public boolean isAssignedDateDisplayed()
    {
        return assignedDate.size() > 0;
    }

    @Step("Is Due Date Displayed")
    public boolean isDueDateDisplayed()
    {
        return dueDate.size() > 0;
    }

    @Step("Is Lower Range Date Displayed")
    public boolean isLowerRangeDateDisplayed()
    {
        return lowerDateRange.size() > 0;
    }

    @Step("Is Upper Range Date Displayed")
    public boolean isUpperRangeDateDisplayed()
    {
        return upperDateRange.size() > 0;
    }

    @Step("Is Date On Completion Date Displayed")
    public boolean isDateOnCompletionDisplayed()
    {
        return dateOnCompletion.size() > 0;
    }

    @Step("Click Edit Button")
    public EditServicePage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(EditServicePage.class);
    }

    @Step("Get Service Status")
    public String getServiceStatus()
    {
        return serviceStatus.getText().trim();
    }

    @Step("Click navigate back button")
    public ClassificationSubPage clickNavigateBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(ClassificationSubPage.class);
    }

    @Step("Get Task List Table")
    public TaskListTable getTaskListTable()
    {
        return taskListTable;
    }

    @FindBy(css = "[data-pageable='vm.tasks']")
    public class TaskListTable extends BaseTable
    {
    }

}
