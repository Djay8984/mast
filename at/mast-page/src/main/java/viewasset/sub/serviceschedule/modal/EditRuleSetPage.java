package viewasset.sub.serviceschedule.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ServiceSchedulePage;

import java.util.List;

public class EditRuleSetPage extends BasePage<EditRuleSetPage>
{
    @Visible
    @Name("Ok Button")
    @FindBy(css = "[data-ng-click='vm.confirm()']")
    private WebElement okButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$dismiss()']")
    private WebElement cancelButton;

    @Visible
    @Name("Edit Rule Set Section")
    @FindBy(id = "modal-edit-rule-set")
    private WebElement editRuleSetSection;

    @Name("Rule set Checkbox")
    @FindBy(css = ".checkbox-container")
    private List<WebElement> ruleSetCheckbox;

    @Step("Select Rule Set By Index")
    public EditRuleSetPage selectRuleSetByIndex(int index)
    {
        ruleSetCheckbox.get(0).findElement(By.tagName("button")).click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Rule Set By Name")
    public EditRuleSetPage selectRuleSetByName(String name)
    {
        ruleSetCheckbox.stream()
                .filter(e -> e.findElement(
                        By.cssSelector("label[data-ng-bind='option.name']"))
                        .getText().trim().equals(name))
                .findFirst()
                .get()
                .findElement(By.tagName("button"))
                .click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Ok Button")
    public ServiceSchedulePage clickOkButton()
    {
        okButton.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Click Cancel Button")
    public ServiceSchedulePage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }
}
