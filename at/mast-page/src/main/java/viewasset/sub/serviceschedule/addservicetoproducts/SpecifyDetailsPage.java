package viewasset.sub.serviceschedule.addservicetoproducts;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.ClassificationSubPage;

public class SpecifyDetailsPage extends BasePage<SpecifyDetailsPage>
{

    @Visible
    @Name("Assigned date Text Box")
    @FindBy(css = "[data-ng-model='service.assignedDateManual']")
    private WebElement assignedDateTextBox;

    @Visible
    @Name("Due date Text Box")
    @FindBy(css = "[data-ng-model='service.dueDateManual']")
    private WebElement dueDateTextBox;

    @Visible
    @Name("Lower Date Range Text Box")
    @FindBy(css = "[data-ng-model='service.lowerRangeDateManual']")
    private WebElement lowerDateRangeTextBox;

    @Visible
    @Name("Upper Date Range Text Box")
    @FindBy(css = "[data-ng-model='service.upperRangeDateManual']")
    private WebElement upperDateRangeTextBox;

    @Visible
    @Name("Cycle periodicity Text Box")
    @FindBy(css = "[data-ng-model='service.cyclePeriodicity']")
    private WebElement cyclePeriodicityTextBox;

    @Visible
    @Name("Last credited on Text Box")
    @FindBy(css = "[data-ng-model='service.lastCreditedJob']")
    private WebElement lastCreditedOnTextBox;

    @Visible
    @Name("Not started Button")
    @FindBy(css = "[data-ng-repeat='status in vm.serviceStatuses']:nth-child(1)")
    private WebElement notStartedButton;

    @Visible
    @Name("Part Held Button")
    @FindBy(css = "[data-ng-repeat='status in vm.serviceStatuses']:nth-child(2)")
    private WebElement partHeldButton;

    @Visible
    @Name("Held Button")
    @FindBy(css = "[data-ng-repeat='status in vm.serviceStatuses']:nth-child(3)")
    private WebElement heldButton;

    @Visible
    @Name("Part held on Text Box")
    @FindBy(css = "[data-ng-model='service.partheldJob']")
    private WebElement partHeldOnTextBox;

    @Visible
    @Name("Completion date Text Box")
    @FindBy(css = "[data-ng-model='service.completionDate']")
    private WebElement completionDateTextBox;

    @Visible
    @Name("None Button")
    @FindBy(css = "[data-ng-repeat*='vm.provisionalStatuses']:nth-child(1)")
    private WebElement noneButton;

    @Visible
    @Name("Provisional Button")
    @FindBy(css = "[data-ng-repeat*='vm.provisionalStatuses']:nth-child(2)")
    private WebElement provisionButton;

    @Visible
    @Name("Complete Button")
    @FindBy(id = "footer-confirm")
    private WebElement completeButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "footer-cancel")
    private WebElement cancelButton;

    @Step("Click Complete Button")
    public ClassificationSubPage clickCompleteButton()
    {
        completeButton.click();
        return PageFactory.newInstance(ClassificationSubPage.class);
    }

    @Step("Get cycle Periodicity")
    public String getCyclePeriodicity()
    {
        return cyclePeriodicityTextBox.getAttribute("value").trim();
    }

    @Step("Set cycle Periodicity ")
    public SpecifyDetailsPage setCyclePeriodicity(String periodicity)
    {
        cyclePeriodicityTextBox.clear();
        cyclePeriodicityTextBox.sendKeys(String.valueOf(periodicity));
        return this;
    }
}
