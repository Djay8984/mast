package viewasset.sub.serviceschedule.classification.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Select Product Element")
@FindBy(css = "[data-ng-repeat='option in vm.visibleOptions']")
public class SelectProductElement extends HtmlElement
{

    @Name("Product Check Box")
    @FindBy(css = "[data-ng-model='option.selected']")
    private WebElement productCheckBox;

    @Name("Product Name")
    @FindBy(css = "[data-ng-bind='option.name']")
    private WebElement productName;

    @Step("Click Product Check Box")
    public SelectProductElement clickProductCheckBox()
    {
        productCheckBox.click();
        return this;
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }
}
