package viewasset.sub.serviceschedule.service;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class EditServicePage extends BasePage<EditServicePage>
{

    @Visible
    @Name("Assigned date Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.assignedDate']")
    private WebElement assignedDateTextBox;

    @Visible
    @Name("Due date Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.dueDate']")
    private WebElement dueDateTextBox;

    @Visible
    @Name("Lower Date Range Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.lowerRangeDate']")
    private WebElement lowerDateRangeTextBox;

    @Visible
    @Name("Upper Date Range Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.upperRangeDate']")
    private WebElement upperDateRangeTextBox;

    @Visible
    @Name("Cycle periodicity Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.cyclePeriodicity']")
    private WebElement cyclePeriodicityTextBox;

    @Visible
    @Name("Last credited on Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.lastCreditedJob']")
    private WebElement lastCreditedOnTextBox;

    @Visible
    @Name("Not started Button")
    @FindBy(css = "[data-ng-repeat='serviceStatus in vm.serviceStatuses']:nth-child(1)")
    private WebElement notStartedButton;

    @Visible
    @Name("Part Held Button")
    @FindBy(css = "[data-ng-repeat='serviceStatus in vm.serviceStatuses']:nth-child(2)")
    private WebElement partHeldButton;

    @Visible
    @Name("Held Button")
    @FindBy(css = "[data-ng-repeat='serviceStatus in vm.serviceStatuses']:nth-child(3)")
    private WebElement heldButton;

    @Visible
    @Name("None Button")
    @FindBy(css = "[data-ng-repeat*='vm.provisionalStatuses']:nth-child(1)")
    private WebElement noneButton;

    @Visible
    @Name("Provisional Button")
    @FindBy(css = "[data-ng-repeat*='vm.provisionalStatuses']:nth-child(2)")
    private WebElement provisionButton;

    @Name("Date on completion Text Box")
    @FindBy(css = "[data-ng-model='vm.editConfig.completionDate']")
    private WebElement dateOnCompletionTextBox;

    @Visible
    @Name("Save Button")
    @FindBy(id = "footer-confirm")
    private WebElement saveButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "footer-cancel")
    private WebElement cancelButton;

    @Step("Set Assigned Date")
    public EditServicePage setAssignedDate(String date)
    {
        assignedDateTextBox.clear();
        assignedDateTextBox.sendKeys(date);
        assignedDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Due Date")
    public EditServicePage setDueDate(String date)
    {
        dueDateTextBox.clear();
        dueDateTextBox.sendKeys(date);
        dueDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Lower Date Range")
    public EditServicePage setLowerRangeDate(String date)
    {
        lowerDateRangeTextBox.clear();
        lowerDateRangeTextBox.sendKeys(date);
        lowerDateRangeTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Upper Date Range")
    public EditServicePage setUpperRangeDate(String date)
    {
        upperDateRangeTextBox.clear();
        upperDateRangeTextBox.sendKeys(date);
        upperDateRangeTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Last credited on Text")
    public EditServicePage setLastCreditedOnText(String lastCreditedOn)
    {
        lastCreditedOnTextBox.clear();
        lastCreditedOnTextBox.sendKeys(lastCreditedOn);
        return this;
    }

    @Step("Set Date on completion")
    public EditServicePage setDateOnCompletion(String date)
    {
        dateOnCompletionTextBox.clear();
        dateOnCompletionTextBox.sendKeys(date);
        dateOnCompletionTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Click Save Button")
    public ViewServicePage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ViewServicePage.class);
    }

    @Step("Click Held status: Not Started Button")
    public EditServicePage clickNotStartedButton()
    {
        notStartedButton.click();
        return this;
    }

    @Step("Click Provisional Button")
    public EditServicePage clickProvisionalButton()
    {
        provisionButton.click();
        return this;
    }
}
