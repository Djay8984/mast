package viewasset.sub.serviceschedule.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Product Heaader element")
@FindBy(css = ".product-header")
public class ProductHeaderElement extends HtmlElement
{

    @Name("Product Name")
    @FindBy(css = "[data-ng-bind='product.name']")
    private WebElement productName;

    @Step("Retrieve product name")
    public String getProductName()
    {
        return productName.getText();
    }

}
