package viewasset.sub.serviceschedule.service.base;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.serviceschedule.service.element.DataElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Data Element")
    private List<DataElement> dataElements;

    @Step("Get Data Element")
    public List<DataElement> getRows()
    {
        return dataElements;
    }
}
