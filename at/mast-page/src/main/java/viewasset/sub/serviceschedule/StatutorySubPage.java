package viewasset.sub.serviceschedule;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.serviceschedule.element.ProductHeaderElement;

import java.util.List;

public class StatutorySubPage extends BasePage<StatutorySubPage>
{

    @Name("Add Products Button")
    @FindBy(css = "[data-ng-click='vm.addProduct()']")
    private WebElement addProductsButton;

    @Name("Notice Message")
    @FindBy(css = "[data-ng-if='vm.isEmptySchedule()']")
    private WebElement noticeMessage;

    private List<ProductHeaderElement> ProductElements;

    @Step("Is Add Products Button displayed")
    public boolean isAddProductsButtonDisplayed()
    {
        return addProductsButton.isDisplayed();
    }

    @Step("Get Notice Message")
    public String getNoticeMessage()
    {
        return noticeMessage.getText().trim();
    }

    @Step("Retrieve all Product Elements")
    public List<ProductHeaderElement> getProductElements()
    {
        return ProductElements;
    }

}
