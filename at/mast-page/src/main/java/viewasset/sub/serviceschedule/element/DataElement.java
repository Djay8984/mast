package viewasset.sub.serviceschedule.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.serviceschedule.addservicetoproducts.SelectServicePage;
import viewasset.sub.serviceschedule.service.ViewServicePage;

import java.util.List;

@Name("Data Element")
@FindBy(xpath = "./div/div[@class='grid-block align-justify product-header']")
public class DataElement extends HtmlElement
{

    @Name("Service Data")
    @FindBy(xpath = "./following-sibling::div")
    private List<DataElement> serviceData;

    @Visible
    @Name("Product Name")
    @FindBy(css = "[data-ng-bind='product.name']")
    private WebElement productName;

    @Visible
    @Name("Bin Icon")
    @FindBy(css = "[data-ng-click='vm.deleteProduct(product)']")
    private WebElement binIcon;

    @Visible
    @Name("Plus Icon")
    @FindBy(css = "[data-ng-click='vm.addService(product)']")
    private WebElement plusIcon;

    @Name("Service Title")
    @FindBy(css = ".title")
    private WebElement serviceTitle;

    @Name("Further More Details Arrow")
    @FindBy(css = "[data-ng-click='vm.viewService()']")
    private WebElement furtherMoreDetailsArrow;

    @Name("Service Check Box")
    @FindBy(css = "[data-ng-model='serviceModel.selected']")
    private WebElement serviceCheckBox;

    @Step("Click Add Button on product")
    public SelectServicePage clickAddButton()
    {
        plusIcon.click();
        return PageFactory.newInstance(SelectServicePage.class);
    }

    @Step("Get Product Name")
    public String getProductName()
    {
        return productName.getText().trim();
    }

    @Step("Get Service Date")
    public List<DataElement> getServiceData()
    {
        return serviceData;
    }

    @Step("Get Service title")
    public String getServiceTitle()
    {
        return serviceTitle.getText().trim().replaceAll(" ", "").replace("-", " ");
    }

    @Step("Click Further Details Arrow")
    public ViewServicePage clickFurtherDetailsArrow()
    {
        furtherMoreDetailsArrow.click();
        return PageFactory.newInstance(ViewServicePage.class);
    }

    @Step("Select Check Box")
    public DataElement selectCheckBox()
    {
        serviceCheckBox.click();
        return this;
    }
}
