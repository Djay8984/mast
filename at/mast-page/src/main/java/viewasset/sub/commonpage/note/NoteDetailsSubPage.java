package viewasset.sub.commonpage.note;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;

public class NoteDetailsSubPage extends BasePage<NoteDetailsSubPage>
{

    @Name("Delete Note Icon")
    private final static String DELETE_ICON_ID = "note-delete";
    @Name("Edit Note Icon")
    @FindBy(id = "note-edit")
    private WebElement editIcon;
    @FindBy(id = DELETE_ICON_ID)
    private WebElement deleteIcon;

    @Name("Visible for  Button")
    @FindBy(css = "#visible-for ul li")
    private WebElement visibleForButton;

    @Name("Note creation date Details")
    @FindBy(css = "#note-creation-date")
    private WebElement creationDate;

    @Name("Edit note description TextBox")
    @FindBy(id = "note-description-preview")
    private WebElement noteTextField;

    @Step("Is Edit Note Icon Displayed")
    public boolean isEditIconDisplayed()
    {
        return editIcon.isDisplayed();
    }

    @Step("Is Delete Note Icon Displayed")
    public boolean isDeleteIconDisplayed()
    {
        return driver.findElements(By.id(DELETE_ICON_ID)).size() > 0;
    }

    @Step("Click on Delete Button")
    public ConfirmationModalPage clickDeleteIcon()
    {
        deleteIcon.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmationModalPage.class);
    }

    @Step("Click Note Edit Icon")
    public EditNoteSubPage clickEditIcon()
    {
        editIcon.click();
        return PageFactory.newInstance(EditNoteSubPage.class);
    }

    @Step("Is Visible For Button Enabled")
    public boolean isVisibleForButtonEnabled()
    {
        return visibleForButton.isEnabled();
    }

    @Step("Get Confidentiality from Visible For Button")
    public String getVisibleForButton()
    {
        return visibleForButton.getText();
    }

    @Step("Get Text from Note")
    public String getNoteText()
    {
        return noteTextField.getText();
    }

    @Step("Check if the editNoteDescription field is editable")
    public boolean isNoteDescriptionFieldEditable()
    {
        return noteTextField.getTagName().contains("input");
    }

    @Step("Is Delete Note Icon Enabled")
    public boolean isDeleteIconEnabled()
    {
        System.out.println(deleteIcon.getAttribute("class"));
        return deleteIcon.getAttribute("class").contains("disabled");
    }
}
