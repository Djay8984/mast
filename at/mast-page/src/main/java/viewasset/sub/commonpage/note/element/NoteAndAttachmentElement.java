package viewasset.sub.commonpage.note.element;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import constant.LloydsRegister;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Name("Note Cards")
@FindBy(css = "[data-ng-click='vm.toggleDetails(attachment)']")
public class NoteAndAttachmentElement extends HtmlElement
{

    @Name("Note Name")
    @FindBy(css = "[data-ng-if='!vm.attachment.isAttachment()']")
    private WebElement noteName;

    @Name("Date in noteCard")
    @FindBy(css = "[data-ng-bind='vm.attachment.updatedDate || vm.attachment.creationDate | moment']")
    private List<WebElement> date;

    @Name("Name in noteCard")
    @FindBy(css = "[data-ng-bind='vm.attachment.updatedBy || vm.attachment.creator']")
    private WebElement creatorName;

    @Name("Description in noteCard")
    @FindBy(css = "[data-ng-if='!vm.attachment.isAttachment()']")
    private WebElement description;

    @Name("Note Selected")
    @FindBy(css = "[attachment='attachment']>div")
    private WebElement noteSelected;

    @Name("Attachment Name")
    @FindBy(css = "[data-ng-bind='vm.attachment.title']")
    private WebElement attachmentName;

    @Step("Get Note Date")
    public Date getDate()
    {
        String noteDate = date.get(0).getText().trim();
        Date date = null;
        try
        {
            date = LloydsRegister.FRONTEND_TIME_FORMAT.parse(noteDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Step("Get Note Text")
    public String getNoteText()
    {
        return noteName.getText().trim();
    }

    @Step("Get Name of Creator")
    public String getCreatorName()
    {
        return creatorName.getText().trim();
    }

    @Step("Get Note Description")
    public String getDescription()
    {
        return description.getText().trim();
    }

    public <T extends BasePage<T>> T clickNoteOrAttachmentElement(Class<T> pageObjectClass)
    {
        this.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Is Date Displayed")
    public boolean isDateDisplayed()
    {
        return date.size() > 0;
    }

    @Step("Is Note Selected")
    public boolean isNoteSelected()
    {
        return noteSelected.getAttribute("class").contains("arrowed blue expanded");
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    @Step("Get Attachment Name")
    public String getAttachmentName()
    {
        return attachmentName.getText().trim();
    }
}
