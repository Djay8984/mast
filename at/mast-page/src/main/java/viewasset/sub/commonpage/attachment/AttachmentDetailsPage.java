package viewasset.sub.commonpage.attachment;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.note.modal.ConfirmationModalPage;

public class AttachmentDetailsPage extends BasePage<AttachmentDetailsPage>
{

    @Name("Delete Note Icon")
    @FindBy(id = "note-delete")
    private WebElement deleteIcon;

    @Name("Download Icon")
    @FindBy(css = "[data-ng-if='vm.showDownloadButton']")
    private WebElement downloadIcon;

    @Visible
    @Name("Visible for  Button")
    @FindBy(css = "#visible-for ul li")
    private WebElement visibleForButton;

    @Visible
    @Name("Note creation date Details")
    @FindBy(css = "#note-creation-date")
    private WebElement creationDate;

    @Step("Click on Delete Button")
    public ConfirmationModalPage clickDeleteIcon()
    {
        deleteIcon.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmationModalPage.class);
    }
}
