package viewasset.sub.commonpage.note;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import constant.LloydsRegister;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.text.ParseException;
import java.util.Date;

public class EditNoteSubPage extends BasePage<EditNoteSubPage>
{

    @Visible
    @Name("Edit note description TextBox")
    @FindBy(id = "note-description-edit")
    private WebElement noteTextBox;

    @Visible
    @Name("Visible for  Button")
    @FindBy(css = "#visible-for ul li")
    private WebElement visibleForButton;

    @Visible
    @Name("Note creation date Details")
    @FindBy(css = "#note-creation-date")
    private WebElement creationDate;

    @Visible
    @Name("Note creatorName Details")
    @FindBy(css = "#note-creator")
    private WebElement creatorName;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "#note-edit-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Edit Note Save Button")
    @FindBy(id = "note-edit-save")
    private WebElement saveButton;

    @Step("Check if Edit Note Text field is editable")
    public boolean isNoteTextBoxEnabled()
    {
        return noteTextBox.isEnabled();
    }

    @Step("Check if Note creator name is visible")
    public boolean isCreatorNameDisplayed()
    {
        return creatorName.isDisplayed();
    }

    @Step("Get Note Creation Date")
    public boolean isCreationDateDisplayed()
    {
        return creationDate.isDisplayed();
    }

    @Step("Click Cancel Button")
    public NoteDetailsSubPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(NoteDetailsSubPage.class);
    }

    @Step("click Edit Note Save Button")
    public NoteDetailsSubPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(NoteDetailsSubPage.class);
    }

    @Step("Get text Of Note")
    public String getNoteTextBox()
    {
        return noteTextBox.getAttribute("value");
    }

    @Step("Set Edit Note TextBox")
    public EditNoteSubPage setNoteTextBox(String description)
    {
        noteTextBox.clear();
        noteTextBox.sendKeys(description);
        return this;
    }

    @Step("Check if the editNoteDescription field is editable")
    public boolean isNoteDescriptionFieldEditable()
    {
        return noteTextBox.getTagName().contains("input");
    }

    @Step("Copy value from Note Text Box")
    public EditNoteSubPage copyValueFromNoteTextBox()
    {
        noteTextBox.sendKeys(Keys.chord(Keys.CONTROL, "c"));
        return this;
    }

    @Step("Paste Value to NoteText Box ")
    public EditNoteSubPage pasteValueToNoteTextBox()
    {
        noteTextBox.sendKeys(Keys.chord(Keys.CONTROL, "v"));
        return this;
    }

    @Step("Cut value form Note Text Box")
    public EditNoteSubPage cutValueFromNoteTextBox()
    {
        noteTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        noteTextBox.sendKeys(Keys.chord(Keys.CONTROL, "x"));
        return this;
    }

    @Step("Get Visible for Text")
    public String getVisibleForText()
    {
        return visibleForButton.findElement(By.tagName("label")).getText();
    }

    @Step("see if the error icon is displayed")
    public boolean isErrorIconDisplayed(Type type)
    {
        return noteTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Get Note Date")
    public Date getCreationDate()
    {
        String noteDate = creationDate.getText().trim();
        Date date = null;
        try
        {
            date = LloydsRegister.FRONTEND_TIME_FORMAT.parse(noteDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Step("Get Creator Name")
    public String getCreatorName()
    {
        return creatorName.getText().trim();
    }

    public enum Type
    {
        ERROR("images/red_warning.png");

        private final String image;

        Type(String image)
        {
            this.image = image;
        }
    }

}
