package viewasset.sub.commonpage.attachment;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.commonpage.attachment.modal.ConfirmationAttachment;

import java.io.File;

public class AttachmentSubPage extends BasePage<AttachmentSubPage>
{

    @Name("Cancel Attachment confirmation Button")
    @FindBy(css = "[data-ng-click='vm.cancelAttachment()']")
    private WebElement cancelButton;

    @Name("Choose Button")
    @FindBy(css = "input[type='file']")
    private WebElement chooseButton;

    @Name("LR internal button")
    @FindBy(css = "[data-ng-repeat*='vm.confidentialityTypes']:nth-child(1)")
    private WebElement lrInternalButton;

    @Name("LR and customer button")
    @FindBy(css = "[data-ng-repeat*='vm.confidentialityTypes']:nth-child(2)")
    private WebElement lrAndCustomerButton;

    @Name("All button")
    @FindBy(css = "[data-ng-repeat*='vm.confidentialityTypes']:nth-child(3)")
    private WebElement allButton;

    @Visible()
    @Name("Upload Button")
    @FindBy(css = "[data-ng-click='vm.saveAttachments()']")
    private WebElement uploadButton;

    @Step("click Cancel Button")
    public ConfirmationAttachment clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ConfirmationAttachment.class);
    }

    @Step("Upload \"{0}\",  file by choosing file and then clicking Choose")
    public AttachmentSubPage browseAndAttachFile(String path) {
        chooseButton.sendKeys(path);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click All Button")
    public AttachmentSubPage clickAllButton() {
        allButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Upload Button")
    public AttachmentsAndNotesPage clickUploadButton() {
        uploadButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

}
