package viewasset.sub.commonpage.note.modal;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;

import java.util.List;

public class ConfirmationModalPage extends BasePage<ConfirmationModalPage>
{

    @Name("Buttons")
    @FindBy(css = "modal-footer button")
    private List<WebElement> buttons;

    @Step("Click Cancel Button in overlay")
    public AttachmentsAndNotesPage clickCancelButton()
    {
        buttons.get(0).click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("click Delete Note confirmation Button")
    public <T extends BasePage<T>> T clickConfirmationButton(Class<T> pageObjectClass)
    {
        if (buttons.size() > 1)
            buttons.get(1).click();
        else
            buttons.get(0).click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
