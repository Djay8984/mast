package viewasset.sub.commonpage.note;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;

import java.text.ParseException;
import java.util.Date;

import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;

public class NoteSubPage extends BasePage<NoteSubPage>
{

    @Visible
    @Name("Note Text box")
    @FindBy(css = "textarea[name='addNoteDescription']")
    private WebElement noteTextBox;

    @Visible
    @Name("LR Internal Button")
    @FindBy(css = "#seenBy ul li label[for='confidentiality-type-note-0']")
    private WebElement lrInternalButton;

    @Visible
    @Name("LR and Customer Button")
    @FindBy(css = "#seenBy ul li label[for='confidentiality-type-note-1']")
    private WebElement lrAndCustomerButton;

    @Visible
    @Name("All Button")
    @FindBy(css = "#seenBy ul li label[for='confidentiality-type-note-2']")
    private WebElement allButton;

    @Visible
    @Name("Add Button")
    @FindBy(css = "button[data-ng-click='vm.saveNote(vm.newNote)']")
    private WebElement addNoteButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "button[data-ng-click='vm.cancelNote()']")
    private Button cancelButton;

    @Name("creator Name")
    @FindBy(css = "[data-ng-bind='vm.currentUser']")
    private WebElement currentUser;

    @Name("Current date in Add Note Form")
    @FindBy(css = ".add-note-date.ng-binding")
    private WebElement currentDate;

    @Step("Set the note into Note text field")
    public NoteSubPage setNoteTextBox(String note)
    {
        noteTextBox.clear();
        noteTextBox.sendKeys(note);
        return this;
    }

    @Step("Is Add Note Button Enabled")
    public boolean isAddNoteButtonEnabled()
    {
        return addNoteButton.isEnabled();
    }

    @Step("Is Cancel Button Enabled")
    public boolean isCancelButtonEnabled()
    {
        return cancelButton.isEnabled();
    }

    @Step("Get the current user")
    public String getCurrentUser()
    {
        return currentUser.getText().toString();
    }

    @Step("Click LR And Customer Button")
    public NoteSubPage clickLrAndCustomerButton()
    {
        lrAndCustomerButton.click();
        return this;
    }

    @Step("Click LR Internal Button")
    public NoteSubPage clickLrInternalButton()
    {
        lrInternalButton.click();
        return this;
    }

    @Step("Click All Button")
    public NoteSubPage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Click Add Note Button")
    public AttachmentsAndNotesPage clickAddNoteButton()
    {
        addNoteButton.click();
        waitForJavascriptFrameworkToFinish();
        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.cssSelector("[data-ng-click='vm.toggleDetails(attachment)']")));
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Click form Cancel Button")
    public AttachmentsAndNotesPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Get Date from Add note Form")
    public Date getNoteDate()
    {
        String dateStr = currentDate.getText();
        Date date = null;
        try
        {
            date = FRONTEND_TIME_FORMAT.parse(dateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Step("Get Note text field")
    public String getNoteText()
    {
        return noteTextBox.getAttribute("value");
    }

    @Step("Get Note text field")
    public String getHelperText()
    {
        return noteTextBox.getAttribute("placeholder");
    }

    @Step("Refresh Page")
    public void refreshPage()
    {
        driver.navigate().refresh();
        new WebDriverWait(BaseTest.getDriver(), 25).until(
                ExpectedConditions.visibilityOfElementLocated
                        (By.cssSelector("#logo-asset-header-logo"))
        );
    }

    @Step("Get Note Text Box Helper Text")
    public String getNoteTextBoxHelperText()
    {
        return noteTextBox.getAttribute("placeholder").trim();
    }

    @Step("Get Note Text Box Character limit")
    public int getNoteTextBoxCharacterLimit()
    {
        return Integer.valueOf(noteTextBox.getAttribute("data-ng-maxlength").trim());
    }

    @Step("Is LR internal button enabled")
    public boolean isLrInternalButtonEnabled()
    {
        return lrInternalButton.isEnabled();
    }

    @Step("Is LR and customer button enabled")
    public boolean isLrAndCustomerButtonEnabled()
    {
        return lrAndCustomerButton.isEnabled();
    }

    @Step("Is All button enabled")
    public boolean isAllButtonEnabled()
    {
        return allButton.isEnabled();
    }

}
