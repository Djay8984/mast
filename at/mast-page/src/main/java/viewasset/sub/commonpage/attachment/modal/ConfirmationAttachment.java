package viewasset.sub.commonpage.attachment.modal;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;

public class ConfirmationAttachment extends BasePage<ConfirmationAttachment>
{

    @Name("Cancel Attachment confirmation Button")
    @FindBy(css = "[ng-click='$close(true)']")
    private WebElement cancelButton;

    public AttachmentsAndNotesPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }
}
