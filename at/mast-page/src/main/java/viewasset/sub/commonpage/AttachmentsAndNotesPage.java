package viewasset.sub.commonpage;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.attachment.AttachmentSubPage;
import viewasset.sub.commonpage.note.NoteDetailsSubPage;
import viewasset.sub.commonpage.note.NoteSubPage;
import viewasset.sub.commonpage.note.element.NoteAndAttachmentElement;
import viewasset.sub.jobs.details.assetnotes.DetailsPage;
import workhub.AddNewAssetPage;

import java.util.List;

public class AttachmentsAndNotesPage extends BasePage<AttachmentsAndNotesPage>
{

    @Visible
    @Name("Add Attachment Button")
    @FindBy(id = "add-attachment")
    private WebElement addAttachmentButton;

    @Visible
    @Name("Add Note Button")
    @FindBy(id = "add-note")
    private WebElement addNoteButton;

    @Visible
    @Name("Close Button")
    @FindBy(css = ".close-button")
    private WebElement closeButton;

    @Name("Load More Button")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private List<WebElement> loadMoreButton;

    @Name("Note cards")
    private List<NoteAndAttachmentElement> noteCards;
    private List<NoteAndAttachmentElement> noteAndAttachmentCards;

    @Name("Note creation date Details")
    @FindBy(css = "#note-creation-date")
    private WebElement creationDate;

    @Name("Description in note Details")
    @FindBy(css = "[data-ng-show='!vm.editNote']")
    private WebElement noteDescription;

    @Name("no Attachments Or Notes Found Message")
    @FindBy(css = "[data-ng-if='!vm.attachments.length']")
    private WebElement noAttachmentsOrNotesFoundMessage;

    @Step("Add Note Button")
    public NoteSubPage clickAddNoteButton()
    {
        addNoteButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(NoteSubPage.class);
    }

    @Step("Click Close Button")
    public DetailsPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(DetailsPage.class);
    }

    @Step("Get Add New Asset Page")
    public AddNewAssetPage getAddNewAssetPage()
    {
        return PageFactory.newInstance(AddNewAssetPage.class);
    }

    @Step("Click note by note Text")
    public NoteDetailsSubPage clickOnNoteByNoteText(String noteText)
    {
        noteAndAttachmentCards.stream().filter(e -> e.getNoteText().equals(noteText))
                .findFirst().get().click();
        return PageFactory.newInstance(NoteDetailsSubPage.class);
    }

    @Step("Is note by note Text Displayed")
    public boolean isNoteByNoteTextDisplayed(String noteText)
    {
        return noteAndAttachmentCards.stream().filter(e -> e.getNoteText().equals(noteText))
                .findFirst().isPresent();
    }

    @Step("Is Close Button Present & Displayed")
    public boolean isCloseButtonDisplayed()
    {
        return closeButton.isDisplayed();
    }

    @Step("Check If load More Button Displayed")
    public boolean isLoadMoreButtonPresent()
    {
        return loadMoreButton.size() > 0;
    }

    @Step("Click Load More Button")
    public AttachmentsAndNotesPage clickLoadMoreButton()
    {
        loadMoreButton.get(0).click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get Note Description In Note Details")
    public String getNoteDescription()
    {
        return noteDescription.getText();
    }

    @Step("Add Attachment Button")
    public AttachmentSubPage clickAddAttachmentButton()
    {
        addAttachmentButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentSubPage.class);
    }

    @Step("Check if attachment Or Notes is/are present")
    public String getAttachmentAndNotesMessage()
    {
        return noAttachmentsOrNotesFoundMessage.getText();
    }

    public List<NoteAndAttachmentElement> getNoteAndAttachmentCards() {
        return noteCards;
    }

}
