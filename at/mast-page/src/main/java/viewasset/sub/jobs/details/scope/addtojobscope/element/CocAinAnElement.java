package viewasset.sub.jobs.details.scope.addtojobscope.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import helper.TestDataHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;

public class CocAinAnElement extends HtmlElement
{

    @Visible
    @Name("check box")
    @FindBy(css = "[data-ng-model^='node.checkBoxModel']")
    private CheckBox checkBox;

    @Name("Children")
    @FindBy(xpath = "./following-sibling::div//ol//div[contains(@class,'job-scope-hierarchy-node')]")
    private List<CocAinAnElement> children;

    @Name("Defect Node")
    @FindBy(css = "[data-ng-if='node.defect']")
    private List<WebElement> defectNode;

    @Name("Service code")
    @FindBy(css = "[data-ng-bind='service.code']")
    private WebElement serviceCode;

    @Name("Service Name")
    @FindBy(css = "[data-ng-bind='service.name']")
    private WebElement serviceName;

    @Visible
    @Name("Further details arrow")
    @FindBy(css = ".glyph.float-right.grey")
    private WebElement furtherDetailsArrow;

    @Name("+/- Icon")
    @FindBy(css = "[class='node-expand-toggle']")
    private WebElement PlusOrMinusIcon;

    @Name("Imposed Date")
    @FindBy(css = "[data-ng-bind^='node.imposedDate']")
    private WebElement imposedDate;

    @Name("Job ScopeService Type")
    @FindBy(id = "job-scope-type")
    private WebElement jobScopeServiceType;

    @Visible
    @Name("Node Status")
    @FindBy(css = ".node-status span")
    private WebElement nodeStatus;

    @Visible
    @Name("Due Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.dueDate |  moment']")
    private WebElement dueDate;

    @Visible
    @Name("Lower Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.lowerRangeDate | moment']")
    private WebElement lowerRangeDate;

    @Visible
    @Name("Upper Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.lowerRangeDate | moment']")
    private WebElement upperRangeDate;

    @Name("Warning message")
    @FindBy(css = ".grid-content.small-2.node-service-warning")
    private WebElement warningMessage;

    @Step("Select check box")
    public CocAinAnElement selectCheckBox()
    {
        AppHelper.scrollToBottom();
        if (!(checkBox.isSelected()))
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Get check box")
    public CheckBox getCheckbox()
    {
        return checkBox;
    }

    @Step("DeSelect check box")
    public CocAinAnElement deSelectCheckBox()
    {
        AppHelper.scrollToBottom();
        if (checkBox.isSelected())
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Click on expand icon")
    public CocAinAnElement clickPlusOrMinusIcon()
    {
        AppHelper.scrollToBottom();
        PlusOrMinusIcon.click();
        AppHelper.waitForOnScreenAnimation();
        return this;
    }

    @Step("Get warning message")
    public String getWarningMessage()
    {
        return warningMessage.getText();
    }

    @Step("Get service name")
    public String getServiceName()
    {
        return serviceName.getText();
    }

    @Step("Get service code")
    public String getServiceCode()
    {
        return serviceCode.getText();
    }

    @Step("Get lower range date")
    public Date getLowerRangeDate()
    {
        return TestDataHelper.toDate(LocalDate.parse(lowerRangeDate.getText(), FRONTEND_TIME_FORMAT_DTS));
    }

    @Step("Get upper range date")
    public Date getUpperRangeDate()
    {
        return TestDataHelper.toDate(LocalDate.parse(upperRangeDate.getText(), FRONTEND_TIME_FORMAT_DTS));
    }

    @Step("Get Node Status ")
    public String getNodeStatus()
    {
        return nodeStatus.getText().trim();
    }

    @Step("Get children")
    public List<CocAinAnElement> getChildren()
    {
        return children;
    }

    /**
     * Click on the clickFurtherDetailsArrow
     *
     * @param pageObjectClass- The pageObject which is expected to be returned
     *                         by clicking the futher details arrow
     *                         Possible PageObjects: ViewConditionOfClassPage.class
     *                         ViewActionableItemsPage.class
     *                         ViewAssetNotesPage
     * @param <T>The           implementing type
     */

    @Step("Click further details Arrow")
    public <T extends BasePage<T>> T clickFurtherDetailsArrow(Class<T> pageObjectClass)
    {
        AppHelper.scrollToBottom();
        furtherDetailsArrow.click();
        AppHelper.waitForOnScreenAnimation();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Get Due Date")
    public Date getDueDate()
    {
        String dateStr = dueDate.getText();
        Date date = null;
        try
        {
            date = FRONTEND_TIME_FORMAT.parse(dateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Step("Get Imposed Date")
    public Date getImposedDate()
    {
        String dateStr = imposedDate.getText();
        Date date = null;
        try
        {
            date = FRONTEND_TIME_FORMAT.parse(dateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    @Step("Get job scope service type text")
    public String getJobScopeServiceType()
    {
        return jobScopeServiceType.getText().trim();
    }

    @Step("Is Defect Node present")
    public boolean isDefectNodePresent()
    {
        return defectNode.size() > 0;
    }
}
