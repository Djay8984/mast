package viewasset.sub.jobs.details.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import util.AppHelper;
import viewasset.sub.jobs.details.defects.defectcategory.element.ModalCheckboxElement;

import java.util.List;

public class SelectItemPage extends BasePage<SelectItemPage>
{
    @Visible
    @Name("Close Button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Name("assign Selected Button")
    @FindBy(css = ".grid-content.modal-footer .primary-button")
    private Button assignSelectedButton;

    @Name("Cancel Button")
    @FindBy(css = ".grid-content.modal-footer [data-ng-click=\"$dismiss()\"]")
    private Button cancelButton;

    @Name("Select Item List")
    @FindBy(css = ".hierarchy")
    private List<ModalCheckboxElement> selectItemCheckBoxes;

    @Step("Click Assign Selected Button")
    public AddDefectPage clickAssignSelectedButton()
    {
        AppHelper.scrollToBottom();
        assignSelectedButton.click();
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @Step("Click Cancel Button")
    public AddDefectPage clickCancelButton()
    {
        AppHelper.scrollToBottom();
        cancelButton.click();
        return PageFactory.newInstance(AddDefectPage.class);
    }
}
