package viewasset.sub.jobs.details.crediting.base;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.crediting.element.InfoElement;

import java.util.List;

public class BaseInfoTable extends HtmlElement
{

    @FindBy(css = ".codicil-row")
    private List<InfoElement> infoElements;

    @Step("Get rows")
    public List<InfoElement> getRows()
    {
        return infoElements;
    }
}
