package viewasset.sub.jobs.details.defects.defectcategory;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.defects.AddDefectPage;

public class ElectroTechnicalPage extends BasePage<ElectroTechnicalPage>
{

    @Visible
    @Name("Defect Type Button")
    @FindBy(id = "defect-type")
    private WebElement defectTypeButton;

    @Visible
    @Name("From Affected Section Text Box")
    @FindBy(id = "add-defect-affected-section-from")
    private WebElement fromAffectedSectionTextBox;

    @Visible
    @Name("To Affected Section Text Box")
    @FindBy(id = "add-defect-affected-section-to")
    private WebElement toAffectedSectionTextBox;

    @Visible
    @Name("Select Pollution Type Button")
    @FindBy(id = "pollution-type")
    private WebElement selectPollutionTypeButton;

    @Step("Get Add Defect page")
    public AddDefectPage getAddDefectPage()
    {
        return PageFactory.newInstance(AddDefectPage.class);
    }
}
