package viewasset.sub.jobs.details.conditionofclass.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class DueDatePage extends BasePage<DueDatePage>
{

    @Visible
    @Name("From Due Date Text Box")
    @FindBy(id = "codicil-search-filter-due-date-min")
    private WebElement fromDueDateTextBox;

    @Visible
    @Name("To Due Date Text Box")
    @FindBy(id = "codicil-search-filter-due-date-max")
    private WebElement toDueDateTextBox;

    @Step("Set From Due Date")
    public DueDatePage setFromDueDate(String fromDate)
    {
        fromDueDateTextBox.clear();
        fromDueDateTextBox.sendKeys(fromDate);
        fromDueDateTextBox.sendKeys(Keys.ENTER);
        return PageFactory.newInstance(DueDatePage.class);
    }

    @Step("Set To Due Date")
    public DueDatePage setToDueDate(String toDate)
    {
        toDueDateTextBox.clear();
        toDueDateTextBox.sendKeys(toDate);
        toDueDateTextBox.sendKeys(Keys.ENTER);
        return PageFactory.newInstance(DueDatePage.class);
    }

    @Step("Get From Due Date Text")
    public String getFromDueDateText()
    {
        return fromDueDateTextBox.getAttribute("value");
    }

    @Step("Get To Due Date Text")
    public String getToDueDateText()
    {
        return toDueDateTextBox.getAttribute("value");
    }
}
