package viewasset.sub.jobs.details.conditionofclass.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class CategoryFilterSubPage extends BasePage<CategoryFilterSubPage>
{
    @Visible
    @Name("Asset note check box")
    @FindBy(css = ".type-filter-container button")
    private WebElement assetNoteCheckBox;

    @Step("Select asset note check box")
    public CategoryFilterSubPage selectAssetNoteCheckBox()
    {
        assetNoteCheckBox.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }
}
