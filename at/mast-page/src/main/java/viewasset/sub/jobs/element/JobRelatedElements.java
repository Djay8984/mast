package viewasset.sub.jobs.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("Job Cocs Elements")
@FindBy(css = "[data-ng-repeat='codicil in vm.jobCodicils']")
public class JobRelatedElements extends HtmlElement
{

    @Visible
    @Name("cocID")
    @FindBy(css = ".codicil-title")
    private WebElement cocID;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Item Status Name")
    @FindBy(css = "[ng-bind='vm.item.status.name']")
    private WebElement itemStatus;

    @Step("Get Actionable Title")
    public String getCocID()
    {
        Pattern pat = Pattern.compile("\\d+$");
        Matcher m = pat.matcher(cocID.getText());
        while (m.find())
        {
            return m.group();
        }
        return null;
    }

    @Step("Get Item Status")
    public String getItemStatus()
    {
        return itemStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public ViewConditionOfClassPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
