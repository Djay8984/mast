package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.actionableitems.base.BaseTable;

public class SelectAssetItemPage extends BasePage<SelectAssetItemPage>
{

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".modal-footer [data-ng-click='$dismiss()']")
    private WebElement cancelButton;

    @Visible
    @Name("Assign Selected Button")
    @FindBy(css = ".modal-footer [data-ng-click='$close(true)']")
    private WebElement assignSelectedButton;

    @Visible
    @Name("X button")
    @FindBy(id = "close-button")
    private WebElement xButton;

    @Name("Asset Item Table")
    private AssetItemTable assetItemTable;

    @Step("Get Asset Item Table")
    public AssetItemTable getAssetItemTable()
    {
        return assetItemTable;
    }

    @Step("Click Assign Selected Button")
    public AddActionableItemPage clickAssignSelectedButton()
    {
        assignSelectedButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click X Button")
    public void clickXButton()
    {
        xButton.click();
    }

    @Step("Is Assign Selected Button Enabled")
    public boolean isAssignSelectedButtonEnabled()
    {
        return assignSelectedButton.isEnabled();
    }

    @Step("Click cancel button")
    public AddActionableItemPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @FindBy(css = "div[data-tpl='result']")
    public class AssetItemTable extends BaseTable
    {
    }

}
