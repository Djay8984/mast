package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class DarFarDsrConfirmationPage extends BasePage<DarFarDsrConfirmationPage>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "[data-ng-click='$close(action.result)']")
    private WebElement okButton;

    /**
     * Click on the Ok Button
     *
     * @param pageObjectClass - The pageObject which is expected to be returned by clicking the OK button
     *                        Possible PageObjects: DARPage.class
     *                        FARPage.class
     *                        DSRPage.class
     * @param <T>             The implementing type
     */
    @Step("Click Ok Button")
    public <T extends BasePage<T>> T clickOkButton(Class<T> pageObjectClass)
    {
        okButton.click();
        /** Clicking on Ok Button will bring up an wired page in runtime
         * Couldn't find a better was to over overcome this
         */
        try
        {
            Thread.sleep(500);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return PageFactory.newInstance(pageObjectClass);
    }
}
