package viewasset.sub.jobs.details.defects.base;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.defects.element.DataElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Data Elements")
    private List<DataElement> dataElements;

    @Step("Get All Data Elements")
    public List<DataElement> getRows()
    {
        return dataElements;
    }
}
