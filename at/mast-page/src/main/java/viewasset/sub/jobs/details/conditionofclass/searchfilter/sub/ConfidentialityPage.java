package viewasset.sub.jobs.details.conditionofclass.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ConfidentialityPage extends BasePage<ConfidentialityPage>
{

    @Visible
    @Name("Select all button")
    @FindBy(id = "codicil-search-filter-confidentiality-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear Button")
    @FindBy(id = "codicil-search-filter-confidentiality-clear-all")
    private WebElement clearButton;

    @Visible
    @Name("Confidentiality checkBoxes")
    @FindBy(css = ".checkbox-container:not(.grid-content)")
    private List<WebElement> confidentialityStatusCheckBoxes;

    @Step("Click clear all button")
    public ConfidentialityPage clickClearAllButton()
    {
        clearButton.click();
        return this;
    }
}
