package viewasset.sub.jobs.details.scope.addtojobscope;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseInfoTable;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Footer;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Header;

import java.util.List;

public class DueAndOverduePage extends BasePage<DueAndOverduePage>
{

    @Name("Number of Services selected")
    @FindBy(css = ".grid-content.small-12.job-scope-services-options span")
    private List<WebElement> numberOfServicesSelected;
    @Name("No Services Text")
    @FindBy(css = "[data-ng-if='vm.emptyService('overdue')']")
    private List<WebElement> noServicesText;
    @Name("Classification Table")
    //must use tight > css relationship to separate the tables
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/services.html'\"]>div>div[class*='asset-model-hierarchy']:nth-child(1)")
    private DueAndOverduePage.ClassificationTable classificationTable;
    @Name("Statutory Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/services.html'\"]>div>div[class*='asset-model-hierarchy']:nth-child(2)")
    private DueAndOverduePage.StatutoryTable statutoryTable;
    @Name("Conditions of Class Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/cocs.html'\"]")
    private DueAndOverduePage.ConditionsOfClassTable conditionsOfClassTable;
    @Name("Actionable items Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/actionable-items.html'\"]")
    private DueAndOverduePage.ActionableItemsTable actionableItemsTable;
    @Name("Asset node Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/asset-notes.html'\"]")
    private DueAndOverduePage.AssetNotesTable assetNotesTable;

    public Footer getFooter()
    {
        return PageFactory.newInstance(Footer.class);
    }

    public Header getHeader()
    {
        return PageFactory.newInstance(Header.class);
    }

    @Step("Get the number of selected services")
    public String getNumberOfServicesSelected()
    {
        AppHelper.scrollToBottom();
        String text = numberOfServicesSelected.get(0).getText();
        return text.replaceAll("[^0-9]", "");
    }

    @Step("Get No Services Text")
    public String getNoServicesText()
    {
        return noServicesText.get(0).getText();
    }

    @Step("Check if No Services Text is Displayed")
    public boolean isNoServicesTextDisplayed()
    {
        return noServicesText.size() > 0;
    }

    @Step("Get Classification Table")
    public DueAndOverduePage.ClassificationTable getClassificationTable()
    {
        return classificationTable;
    }

    @Step("Get Statutory Table")
    public DueAndOverduePage.StatutoryTable getStatutoryTable()
    {
        return statutoryTable;
    }

    @Step("Get ConditionOfClass Table")
    public DueAndOverduePage.ConditionsOfClassTable getConditionOfClassTable()
    {
        return conditionsOfClassTable;
    }

    @Step("Get ActionableItems Table")
    public DueAndOverduePage.ActionableItemsTable getActionableItemsTable()
    {
        return actionableItemsTable;
    }

    @Step("Get AssetNotes Table")
    public DueAndOverduePage.AssetNotesTable getAssetNotesTable()
    {
        return assetNotesTable;
    }

    public class ClassificationTable extends BaseServiceTable
    {
    }

    public class StatutoryTable extends BaseServiceTable
    {
    }

    public class ConditionsOfClassTable extends BaseInfoTable
    {
    }

    public class ActionableItemsTable extends BaseInfoTable
    {
    }

    public class AssetNotesTable extends BaseInfoTable
    {
    }
}
