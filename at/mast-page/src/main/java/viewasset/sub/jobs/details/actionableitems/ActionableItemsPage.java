package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.HeaderPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.actionableitems.base.BaseTable;
import viewasset.sub.jobs.details.actionableitems.searchfilter.SearchFilterPage;

public class ActionableItemsPage extends BasePage<ActionableItemsPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Visible
    @Name("Add New Button")
    @FindBy(id = "add-attributes-button")
    private WebElement addNewButton;

    @Visible
    @Name("Search Filter Actionable Items Button")
    @FindBy(css = "[data-ng-click='vm.searchMode = !vm.searchMode']")
    private WebElement searchFilterActionableItemsButton;

    @Name("Job related Table")
    private ActionableItemsPage.JobRelatedTable jobRelatedTable;

    @Name("Others Table")
    private ActionableItemsPage.OthersTable othersTable;

    @Step("Get header")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Get Job Related Table")
    public ActionableItemsPage.JobRelatedTable getJobRelatedTable()
    {
        return jobRelatedTable;
    }

    @Step("Get Job Related Table")
    public ActionableItemsPage.OthersTable getOthersTable()
    {
        return othersTable;
    }

    @Step("Click Add New Button")
    public AddActionableItemPage clickAddNewButton()
    {
        addNewButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click Search Filter Actionable Item Button")
    public SearchFilterPage clickSearchFilterActionableItemButton()
    {
        searchFilterActionableItemsButton.click();
        return PageFactory.newInstance(SearchFilterPage.class);
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    public class JobRelatedTable extends BaseTable
    {
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    public class OthersTable extends BaseTable
    {

    }

}
