package viewasset.sub.jobs.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.conditionofclass.sub.JobStatusFilterSubPage;
import viewasset.sub.jobs.details.conditionofclass.sub.SDOFilterSubPage;
import viewasset.sub.jobs.searchfilter.sub.SurveyorNameFilterSubPage;

public class SearchFilterJobsPage extends BasePage<SearchFilterJobsPage>
{

    private final static String SEARCH_INPUT_SELECTOR = "job-search-query";
    @Name("Search/Filter Jobs SubPage container")
    @FindBy(css = ".small-12.grid-block.row.ng-scope")
    private WebElement searchFilterJobsSubPageContainer;
    @Visible
    @Name("Search Input Filter")
    @FindBy(id = SEARCH_INPUT_SELECTOR)
    private WebElement searchInput;
    @Visible
    @Name("Start date Filter")
    @FindBy(css = "#job-search-filter-row--col-0-toggle-selected")
    private WebElement startDateFilterButton;
    @Visible
    @Name("Surveyor name Filter")
    @FindBy(css = "#job-search-filter-row--col-1-toggle-selected")
    private WebElement surveyorNameFilterButton;
    @Visible
    @Name("Job status Filter")
    @FindBy(css = "#job-search-filter-row--col-2-toggle-selected")
    private WebElement jobStatusFilterButton;
    @Visible
    @Name("SDO Filter")
    @FindBy(css = "span[aria-label='Select SDO filter']")
    private WebElement sdoFilterFilterButton;
    @Name("Reset Button")
    @FindBy(id = "job-search-reset")
    @Visible
    private WebElement resetButton;
    @Name("Search Button")
    @FindBy(id = "job-search-apply")
    private WebElement searchButton;

    public SearchFilterJobsPage setSearchInput(String input)
    {
        searchInput.clear();
        searchInput.sendKeys(input);
        return this;
    }

    @Step("Click Surveyor name filter")
    public SurveyorNameFilterSubPage clickSurveyorNameFilterButton()
    {
        surveyorNameFilterButton.click();
        return PageFactory.newInstance(SurveyorNameFilterSubPage.class);
    }

    @Step("Click Job status filter")
    public JobStatusFilterSubPage clickJobStatusFilterButton()
    {
        jobStatusFilterButton.click();
        return PageFactory.newInstance(JobStatusFilterSubPage.class);
    }

    @Step("Click SDO filter")
    public SDOFilterSubPage clickSDOFilterButton()
    {
        sdoFilterFilterButton.click();
        return PageFactory.newInstance(SDOFilterSubPage.class);
    }

    @Step("Click Reset Button")
    public JobsSubPage clickResetButton()
    {
        resetButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(SEARCH_INPUT_SELECTOR)));
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Click on search button ")
    public JobsSubPage clickSearchButton()
    {
        searchButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(SEARCH_INPUT_SELECTOR)));
        return PageFactory.newInstance(JobsSubPage.class);
    }

    public boolean isSearchButtonDisabled()
    {
        return !searchButton.isEnabled();
    }

}
