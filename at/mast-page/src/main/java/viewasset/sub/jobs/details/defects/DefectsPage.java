package viewasset.sub.jobs.details.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.defects.base.BaseTable;

public class DefectsPage extends BasePage<DefectsPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Visible
    @Name("Search Filter Defects Button")
    @FindBy(css = "[data-ng-click='vm.searchMode = !vm.searchMode']")
    private WebElement searchFilterDefectsButton;

    @Visible
    @Name("Add New Button")
    @FindBy(id = "add-attributes-button")
    private WebElement addNewButton;

    @Name("Job related Table")
    private JobRelatedTable jobRelatedTable;

    @Name("Others Table")
    private OthersTable othersTable;

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Get Job Related Table")
    public JobRelatedTable getJobRelatedTable()
    {
        return jobRelatedTable;
    }

    @Step("Get Job Related Table")
    public OthersTable getOthersTable()
    {
        return othersTable;
    }

    @Step("Click Add New Button")
    public AddDefectPage clickAddNewButton()
    {
        addNewButton.click();
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    public class JobRelatedTable extends BaseTable
    {
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    public class OthersTable extends BaseTable
    {
    }

}
