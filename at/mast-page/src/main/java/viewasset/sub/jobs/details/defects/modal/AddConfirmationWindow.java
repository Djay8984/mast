package viewasset.sub.jobs.details.defects.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.defects.ViewDefectPage;

public class AddConfirmationWindow extends BasePage<AddConfirmationWindow>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "[data-ng-click='$close(action.result)']")
    private WebElement okButton;

    @Step("Click OK button")
    public ViewDefectPage clickOkButton()
    {
        okButton.click();
        return PageFactory.newInstance(ViewDefectPage.class);
    }
}
