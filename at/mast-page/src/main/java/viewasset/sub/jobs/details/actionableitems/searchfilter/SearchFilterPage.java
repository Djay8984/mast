package viewasset.sub.jobs.details.actionableitems.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.actionableitems.searchfilter.sub.StatusPage;

public class SearchFilterPage extends BasePage<SearchFilterPage>
{

    @Visible
    @Name("Search Text Box")
    @FindBy(id = "codicil-search-query")
    private WebElement searchTextBox;

    @Visible
    @Name("Reset Button")
    @FindBy(id = "codicil-search-reset")
    private WebElement resetButton;

    @Visible
    @Name("Search Button")
    @FindBy(id = "codicil-search-button")
    private WebElement searchButton;

    @Visible
    @Name("Category filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(1) .codicil-filter-button")
    private WebElement categoryFilterButton;

    @Visible
    @Name("Due date filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(2) .codicil-filter-button")
    private WebElement dueDateFilterButton;

    @Visible
    @Name("Status filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(3) .codicil-filter-button")
    private WebElement statusFilterButton;

    @Visible
    @Name("Item type filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(1) .codicil-filter-button")
    private WebElement itemTypeFilterButton;

    @Visible
    @Name("SDO filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(2) .codicil-filter-button")
    private WebElement sdoFilterButton;

    @Visible
    @Name("Confidentiality filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(3) .codicil-filter-button")
    private WebElement confidentialityFilterButton;

    @Step("Set the search type into Search Text Box")
    public SearchFilterPage setSearchText(String title)
    {
        searchTextBox.clear();
        searchTextBox.sendKeys(title);
        return this;
    }

    @Step("Click Search Button")
    public ActionableItemsPage clickSearchButton()
    {
        AppHelper.scrollToBottom();
        searchButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Status filter button")
    public StatusPage clickStatusFilterButton()
    {
        statusFilterButton.click();
        return PageFactory.newInstance(StatusPage.class);
    }

    @Step("Click Reset Button")
    public ActionableItemsPage clickResetButton()
    {
        resetButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

}
