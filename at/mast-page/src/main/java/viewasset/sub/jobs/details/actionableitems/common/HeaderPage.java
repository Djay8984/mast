package viewasset.sub.jobs.details.actionableitems.common;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class HeaderPage extends BasePage<HeaderPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Click on Navigate Back Button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
