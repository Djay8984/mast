package viewasset.sub.jobs.details.conditionofclass.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class SdoPage extends BasePage<SdoPage>
{

    @Visible
    @Name("SDO Text Box")
    @FindBy(id = "codicil-sdo")
    private WebElement sdoTextBox;

    @Step("Set SDO type")
    public SdoPage setSdoText(String sdoText)
    {
        sdoTextBox.clear();
        sdoTextBox.sendKeys(sdoText);
        return this;
    }
}
