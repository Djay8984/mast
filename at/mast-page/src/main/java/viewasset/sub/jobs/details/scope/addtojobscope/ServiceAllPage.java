package viewasset.sub.jobs.details.scope.addtojobscope;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseInfoTable;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ActionableItemsElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.AssetNotesElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ConditionsOfClassElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.JobScopeServiceTaskElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.CocAiAnServiceElements;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Footer;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Header;

import java.util.List;

public class ServiceAllPage extends BasePage<ServiceAllPage>
{

    @Deprecated
    @Name("Conditions of Class Expand Icon")
    @FindBy(css = "[data-ng-if='vm.jobCocs.length'] .node-expand-toggle")
    private WebElement cocExpandIcon;

    @Deprecated
    @Name("Actionable Items Expand Icon")
    @FindBy(css = "[data-ng-if='vm.jobActionableItems.length'] .node-expand-toggle")
    private WebElement actionableItemsExpandIcon;

    @Deprecated
    @Name("Asset Notes Expand Icon")
    @FindBy(css = "[data-ng-if='vm.jobAssetNotes.length'] .node-expand-toggle")
    private WebElement assetNotesExpandIcon;

    @Name("Cancel button")
    @FindBy(id = "edit-jobs-preview-cancel")
    private WebElement cancelButton;

    //must use tight > css relationship to separate the tables
    @Name("Classification Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/services.html'\"]>div>div[class*='asset-model-hierarchy']")
    private List<ServiceTable> serviceTable;

    @Name("Conditions of Class Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/cocs.html'\"]")
    private ConditionsOfClassTable conditionsOfClassTable;

    @Name("Actionable items Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/actionable-items.html'\"]")
    private ActionableItemsTable actionableItemsTable;

    @Name("Asset node Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/asset-notes.html'\"]")
    private AssetNotesTable assetNotesTable;
    @Name("Conditions Of Class Elements")
    private List<ConditionsOfClassElements> cocItems;
    @Name("Actionable Item Elements")
    private List<ActionableItemsElements> actionableItems;
    @Name("Asset Notes Elements")
    private List<AssetNotesElements> assetNotesElements;
    @Name("Asset Classification Elements")
    private List<JobScopeServiceTaskElements> jobScopeServiceTaskElements;
    @Name("Coc AI AN Service Elements")
    private List<CocAiAnServiceElements> cocAiAnServiceElements;

    public Footer getFooter()
    {
        return PageFactory.newInstance(Footer.class);
    }

    public Header getHeader()
    {
        return PageFactory.newInstance(Header.class);
    }

    @Step("Get Classification Table")
    public BaseServiceTable getClassificationTable()
    {
        return serviceTable.stream().filter(a -> a.getFamilyName().equals("Classification")).findFirst().get();
    }

    @Step("Get Statutory Table")
    public BaseServiceTable getStatutoryTable()
    {
        return serviceTable.stream().filter(a -> a.getFamilyName().equals("Statutory")).findFirst().get();
    }

    @Step("Get Conditions of class Table")
    public ConditionsOfClassTable getConditionsOfClassTable()
    {
        return conditionsOfClassTable;
    }

    @Step("Get Actionable Items Table")
    public ActionableItemsTable getActionableItemsTable()
    {
        return actionableItemsTable;
    }

    @Step("Get Asset Note Table")
    public AssetNotesTable getAssetNotesTable()
    {
        return assetNotesTable;
    }

    @Deprecated
    @Step("Click coc expand icon")
    public ServiceAllPage clickCocExpandIcon()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        cocExpandIcon);
        wait.until(ExpectedConditions.elementToBeClickable(cocExpandIcon));
        cocExpandIcon.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[data-ng-if='vm.jobCocs.length'] .is-expanded")));
        return this;
    }

    @Deprecated
    @Step("Click Actionable Item expand icon")
    public ServiceAllPage clickActionableItemExpandIcon()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        actionableItemsExpandIcon);
        wait.until(ExpectedConditions.elementToBeClickable(actionableItemsExpandIcon));
        actionableItemsExpandIcon.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[data-ng-if='vm.jobActionableItems.length'] .is-expanded")));
        return this;
    }

    @Deprecated
    @Step("Click Asset Notes expand icon")
    public ServiceAllPage clickAssetNotesExpandIcon()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        assetNotesExpandIcon);
        wait.until(ExpectedConditions.elementToBeClickable(assetNotesExpandIcon));
        assetNotesExpandIcon.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[data-ng-if='vm.jobAssetNotes.length'] .is-expanded")));
        return this;
    }

    @Deprecated
    @Step("Get COC node title by index")
    public String getCocNodeTitleByIndex(int index)
    {
        return cocItems.get(index).getNodeTitle();
    }

    @Deprecated
    @Step("Select Coc checkbox by index")
    public ServiceAllPage selectCocCheckBoxByIndex(int index)
    {
        cocItems.get(index).selectCheckBox();
        return this;
    }

    @Deprecated
    @Step("Get Actionable Items node title by index")
    public String getActionableItemsNodeTitleByIndex(int index)
    {
        return actionableItems.get(index).getNodeTitle();
    }

    @Deprecated
    @Step("Select Actionable Items checkbox by index")
    public ServiceAllPage selectActionableItemsCheckBoxByIndex(int index)
    {
        actionableItems.get(index).selectCheckBox();
        return this;
    }

    @Deprecated
    @Step("Get Asset Notes node title by index")
    public String getAssetNotesNodeTitleByIndex(int index)
    {
        return assetNotesElements.get(index).getNodeTitle();
    }

    @Deprecated
    @Step("Select Asset Notes checkbox by index")
    public ServiceAllPage selectAssetNotesCheckBoxByIndex(int index)
    {
        assetNotesElements.get(index).selectCheckBox();
        return this;
    }

    @Deprecated
    @Step("Get All Classification Elements")
    public List<JobScopeServiceTaskElements> getJobScopeServiceTaskElements()
    {
        return jobScopeServiceTaskElements;

    }

    @Deprecated
    @Step("Get Classification Elements")
    public JobScopeServiceTaskElements getJobScopeServiceTaskByName(String name)
    {
        return jobScopeServiceTaskElements.stream()
                .filter(e -> e.getNodeName().equals(name))
                .findFirst().get();
    }

    @Deprecated
    @Step("Get Coc AI AN elements by name")
    public CocAiAnServiceElements getCocAiAnServiceElementsByName(String service)
    {
        return cocAiAnServiceElements.stream()
                .filter(e -> e.getJobScopeServiceTypeText()
                        .contains(service))
                .findFirst().get();
    }

    public class ConditionsOfClassTable extends BaseInfoTable
    {
    }

    public class ActionableItemsTable extends BaseInfoTable
    {
    }

    public class ServiceTable extends BaseServiceTable
    {

    }

    public class AssetNotesTable extends BaseInfoTable
    {
    }

}
