package viewasset.sub.jobs.details.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;
import util.AppHelper;
import viewasset.sub.jobs.details.defects.defectcategory.ElectroTechnicalPage;
import viewasset.sub.jobs.details.defects.modal.AddConfirmationWindow;

public class AddDefectPage extends BasePage<AddDefectPage>
{
    @Visible
    @Name("Title Text Box")
    @FindBy(id = "add-defect-title")
    private WebElement titleTextBox;

    @Visible
    @Name("Defect Category")
    @FindBy(css = "[data-ng-click='vm.toggleCategory(category, row)']")
    private WebElement defectCategory;

    @Visible
    @Name("Description")
    @FindBy(id = "add-defect-pollution-description")
    private WebElement descriptionTextBox;

    @Visible
    @Name("Occurred Or Reported Date")
    @FindBy(id = "add-defect-date-occurred-or-reported")
    private WebElement occurredOrReportedTextBox;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "add-defect-preview-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Save Button")
    @FindBy(id = "add-defect-preview-save")
    private WebElement saveButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Electro-technical Button")
    @FindBy(css = "[data-ng-repeat^='subCategory in vm.subCategories']:nth-child(1)")
    private RadioButton electroTechnicalButton;

    @Name("Hull Button")
    @FindBy(css = "[data-ng-repeat^='subCategory in vm.subCategories']:nth-child(2)")
    private RadioButton hullButton;

    @Name("Lifting appliance button")
    @FindBy(css = "[data-ng-repeat^='subCategory in vm.subCategories']:nth-child(3)")
    private RadioButton liftingApplianceButton;

    @Name("Machinery Button")
    @FindBy(css = "[data-ng-repeat^='subCategory in vm.subCategories']:nth-child(4)")
    private RadioButton machineryButton;

    @Name("Refrigeration Button")
    @FindBy(css = "[data-ng-repeat^='subCategory in vm.subCategories']:nth-child(5)")
    private RadioButton refrigerationButton;

    @Step("Set Title")
    public AddDefectPage setTitle(String title)
    {
        titleTextBox.clear();
        titleTextBox.sendKeys(title);
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @Step("Set Description")
    public AddDefectPage setDescription(String description)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(description);
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @Step("Click Defect Category")
    public AddDefectPage clickDefectCategory()
    {
        defectCategory.click();
        AppHelper.waitForOnScreenAnimation();
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @Step("Click Electro-technical button")
    public ElectroTechnicalPage clickElectroTechnicalButton()
    {
        electroTechnicalButton.click();
        return PageFactory.newInstance(ElectroTechnicalPage.class);
    }

    @Step("Click save button")
    public AddConfirmationWindow clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(AddConfirmationWindow.class);
    }

}
