package viewasset.sub.jobs.details.scope.addtojobscope;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseInfoTable;
import viewasset.sub.jobs.details.scope.addtojobscope.base.BaseServiceTable;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Footer;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope.Header;

import java.util.List;

public class ServiceSchedulePage extends BasePage<ServiceSchedulePage>
{

    @Name("No Service Text")
    @FindBy(css = "[data-ng-if='vm.emptyService('schedule')']")
    private List<WebElement> noServicesText;
    @Name("Classification Table")
    //must use tight > css relationship to separate the tables
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/services.html'\"]>div>div[class*='asset-model-hierarchy']:nth-child(1)")
    private ServiceSchedulePage.ClassificationTable classificationTable;
    @Name("Statutory Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/services.html'\"]>div>div[class*='asset-model-hierarchy']:nth-child(2)")
    private ServiceSchedulePage.StatutoryTable statutoryTable;
    @Name("Conditions of Class Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/cocs.html'\"]")
    private ServiceSchedulePage.ConditionsOfClassTable conditionsOfClassTable;
    @Name("Actionable items Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/actionable-items.html'\"]")
    private ServiceSchedulePage.ActionableItemsTable actionableItemsTable;
    @Name("Asset node Table")
    @FindBy(css = "[data-ng-include=\"'app/asset/jobs/add-scope/partials/asset-notes.html'\"]")
    private ServiceSchedulePage.AssetNotesTable assetNotesTable;

    public Footer getFooter()
    {
        return PageFactory.newInstance(Footer.class);
    }

    public Header getHeader()
    {
        return PageFactory.newInstance(Header.class);
    }

    @Step("get no services text")
    public String getNoServicesText()
    {
        return noServicesText.get(0).getText();
    }

    @Step("Check if No Services Text is Displayed")
    public boolean isNoServicesTextDisplayed()
    {
        return noServicesText.size() > 0;
    }

    @Step("Get AssetNotes Table")
    public ServiceSchedulePage.AssetNotesTable getAssetNotesTable()
    {
        return assetNotesTable;
    }

    @Step("Get Classification Table")
    public ServiceSchedulePage.ClassificationTable getClassificationTable()
    {
        return classificationTable;
    }

    @Step("Get Statutory Table")
    public ServiceSchedulePage.StatutoryTable getStatutoryTable()
    {
        return statutoryTable;
    }

    @Step("Get ConditionOfClass Table")
    public ServiceSchedulePage.ConditionsOfClassTable getConditionOfClassTable()
    {
        return conditionsOfClassTable;
    }

    @Step("Get ActionableItems Table")
    public ServiceSchedulePage.ActionableItemsTable getActionableItemsTable()
    {
        return actionableItemsTable;
    }

    public class ClassificationTable extends BaseServiceTable
    {
    }

    public class StatutoryTable extends BaseServiceTable
    {
    }

    public class ConditionsOfClassTable extends BaseInfoTable
    {
    }

    public class ActionableItemsTable extends BaseInfoTable
    {
    }

    public class AssetNotesTable extends BaseInfoTable
    {
    }

}
