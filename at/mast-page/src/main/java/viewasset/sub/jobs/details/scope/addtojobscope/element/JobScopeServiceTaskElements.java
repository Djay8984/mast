package viewasset.sub.jobs.details.scope.addtojobscope.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.JobScopeServiceTypeElements;

import java.util.List;

@Deprecated
@Name("Product catalogue elements")
@FindBy(css = "[data-ng-repeat='(family, value) in vm.scopeServices() track by $index']")
public class JobScopeServiceTaskElements extends HtmlElement
{

    @Name("Expand Icon")
    @FindBy(css = "[data-ng-click='value.expanded = !value.expanded'] [data-ng-if='!value.expanded']")
    private WebElement expandIcon;

    @Name("Closable Icon")
    @FindBy(css = "[data-ng-click='value.expanded = !value.expanded'] [data-ng-if='value.expanded']")
    private List<WebElement> collapsibleIcon;

    @Name("Node Name")
    @FindBy(css = ".node-name h5")
    private WebElement nodeName;

    @Name("Classification Service Elements")
    private List<JobScopeServiceTypeElements> jobScopeServiceTypeElement;

    @Step("Get node name")
    public String getNodeName()
    {
        return nodeName.getText().trim();
    }

    @Step("Click on expand icon")
    public JobScopeServiceTaskElements clickOnExpandIcon()
    {
        expandIcon.click();
        return this;
    }

    @Step("Is product catalogue expanded ")
    public boolean isProductCatalogueExpanded()
    {
        return collapsibleIcon.size() == 0;
    }

    @Step("Get Job Scope Service Type Cards")
    public List<JobScopeServiceTypeElements> getJobScopeServiceTypeCards()
    {
        return jobScopeServiceTypeElement;
    }

    @Step("Get All product Catalogue elements")
    public JobScopeServiceTypeElements getJobScopeServiceTypeByName(String name)
    {
        return jobScopeServiceTypeElement.stream()
                .filter(e -> e.getNodeName().equals(name))
                .findFirst().get();
    }
}
