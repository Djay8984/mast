package viewasset.sub.jobs.details.scope.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Job Scope Elements")
@FindBy(css = ".job-scope-list.ng-scope")
public class JobScopeElements extends HtmlElement
{

    @Name("Name of the elements")
    @FindBy(css = "[data-ng-bind='node.name']")
    private WebElement name;

    @Step("Get Job Name")
    public String getName()
    {
        return name.getText().trim();
    }
}
