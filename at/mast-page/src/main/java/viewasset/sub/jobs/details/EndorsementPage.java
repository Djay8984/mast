package viewasset.sub.jobs.details;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.CheckBox;

import java.util.List;

public class EndorsementPage extends BasePage<EndorsementPage>
{

    @Name("Check Box")
    @FindBy(css = "button.checkbox-dir")
    private CheckBox checkBox;

    @Name("Completed Fields")
    @FindBy(css = "b.ng-binding.ng-scope")
    private WebElement completedBy;

    @Name("Completed Tick")
    @FindBy(css = "i.svg-icon-Tick")
    private List<WebElement> completedTick;

    @Name("ReOpen Button")
    @FindBy(css = "button.white-button")
    private List<WebElement> reopenButton;

    @Name("Back Button")
    @FindBy(css = "div.back-button")
    private WebElement backButton;

    @Name("Complete Button")
    @FindBy(css = "button.ng-binding.ng-scope.primary-button")
    private WebElement completeButton;

    @Step("Select Check Box")
    public EndorsementPage selectEicCaseCheckBox()
    {
        if (!(checkBox.isSelected()))
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Get Completed Fields")
    public String getCompletedBy()
    {
        return completedBy.getText();
    }

    @Step("Is Completed Tick Present")
    public boolean isCompletedTickPresent()
    {
        return completedTick.size() > 0;
    }

    @Step("Is ReOpen Button Present")
    public boolean isReopenButtonPresent()
    {
        return reopenButton.size() > 0;
    }

    @Step("Click The Back Button")
    public <T extends BasePage<T>> T clickBackButton(Class<T> pageObjectClass)
    {
        backButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Complete Button")
    public EndorsementPage clickCompleteButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(completeButton));
        completeButton.click();
        return this;
    }

}
