package viewasset.sub.jobs.addnewjob;

import static constant.LloydsRegister.JobData.description;
import static constant.LloydsRegister.JobData.location;
import static constant.LloydsRegister.JobData.sdoCode;
import static constant.LloydsRegister.JobData.strEtaDate;
import static constant.LloydsRegister.JobData.strEtdDate;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Select;
import typifiedelement.RadioButton;
import util.ClickClearHelper;
import viewasset.sub.jobs.JobsSubPage;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

public class AddNewJobPage extends BasePage<AddNewJobPage>
{

    @Name("Zero Visit Radio Button Yes")
    @FindBy(id = "add-job-zero-visit-yes")
    private RadioButton zeroVisitRadioButtonYes;

    @Name("Zero Visit Radio Button No")
    @FindBy(id = "add-job-zero-visit-no")
    private RadioButton zeroVisitRadioButtonNo;

    @Name("Job Category Radio Button Survey")
    @FindBy(id = "add-job-job-category-survey")
    private RadioButton jobCategoryRadioButtonSurvey;

    @Name("Job Category Radio Button Audit")
    @FindBy(id = "add-job-job-category-audit")
    private RadioButton jobCategoryRadioButtonAudit;

    @Visible
    @Name("SDO Text Box")
    @FindBy(id = "add-job-sdo")
    private WebElement sdoTextBox;

    @Visible
    @Name("Cases Select Dropdown")
    @FindBy(id = "add-job-case")
    private WebElement casesSelectDropDown;

    @Visible
    @Name("Job Location Text Box")
    @FindBy(id = "add-job-location")
    private WebElement jobLocationTextBox;

    @Visible
    @Name("Job Description Text Box")
    @FindBy(id = "add-job-description")
    private WebElement jobDescriptionTextBox;

    @Visible
    @Name("Change of Flag Checkbox")
    @FindBy(id = "add-job-change-of-flag")
    private WebElement changeOfFlagCheckbox;

    @Name("Proposed Flag Text Box")
    @FindBy(id = "add-job-proposed-flag")
    private WebElement proposedFlagTextbox;

    @Visible
    @Name("ETA text box")
    @FindBy(id = "add-job-eta")
    private WebElement etaTextBox;

    @Visible
    @Name("ETD text box")
    @FindBy(id = "add-job-etd")
    private WebElement etdTextBox;

    @Visible
    @Name("Requested attendance date text box")
    @FindBy(id = "add-job-rad")
    private WebElement requestedAttendanceDateTextBox;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "add-job-preview-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Save Button")
    @FindBy(id = "add-job-preview-save")
    private WebElement saveButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement backButton;

    @Name("Validation message DataElement")
    @FindBy(id = "validation-message-add-job-case-required")
    private WebElement validationMessage;

    @Name("Validation message for job location")
    @FindBy(id = "validation-message-add-job-location-required")
    private WebElement locationRequiredValidationMessage;

    @Name("Validation message for job description")
    @FindBy(id = "validation-message-add-job-description-required")
    private WebElement descriptionRequiredValidationMessage;

    @Name("Validation message for ETD")
    @FindBy(id = "validation-message-add-job-etd-required")
    private WebElement etdRequiredValidationMessage;

    @Name("Validation message for ETA")
    @FindBy(id = "validation-message-add-job-date-required")
    private WebElement etaRequiredValidationMessage;

    @Step("set SDO Code")
    public AddNewJobPage setSdoCode(final String sdoCode)
    {
        sdoTextBox.clear();
        sdoTextBox.sendKeys(sdoCode);
        sdoTextBox.sendKeys(Keys.ENTER);
        return PageFactory.newInstance(AddNewJobPage.class);
    }

    @Step("set SDO Code without ENTER")
    public AddNewJobPage setSdoCodeWithoutEnter(final String sdoCode)
    {
        sdoTextBox.sendKeys(sdoCode);
        return PageFactory.newInstance(AddNewJobPage.class);
    }

    @Step("Is save button Clickable")
    public boolean isSaveButtonClickable()
    {
        return saveButton.isEnabled();
    }

    @Step("Click cancel button")
    public JobsSubPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Click save button")
    public ViewJobDetailsPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Select Zero Visit Yes")
    public AddNewJobPage selectZeroVisitYes()
    {
        zeroVisitRadioButtonYes.select();
        return this;
    }

    @Step("Select Zero Visit No")
    public AddNewJobPage selectZeroVisitNo()
    {
        zeroVisitRadioButtonNo.select();
        return this;
    }

    @Step("Select Job Category Survey")
    public AddNewJobPage selectJobCategorySurvey()
    {
        jobCategoryRadioButtonSurvey.select();
        return this;
    }

    @Step("Select Job Category Audit")
    public AddNewJobPage selectJobCategoryAudit()
    {
        jobCategoryRadioButtonAudit.select();
        return this;
    }

    @Step("Select Case type")
    public AddNewJobPage selectCaseTypeByVisibleText(final String casesType)
    {
        new Select(casesSelectDropDown).selectByVisibleText(casesType);
        return this;
    }

    @Step("Select Case type By Index")
    public AddNewJobPage selectCaseTypeByIndex(final int index)
    {
        new Select(casesSelectDropDown).selectByIndex(index);
        return this;
    }

    @Step("Set Job Location")
    public AddNewJobPage setJobLocation(final String jobLocation)
    {
        jobLocationTextBox.clear();
        jobLocationTextBox.sendKeys(jobLocation);
        return this;
    }

    @Step("Set Job Description")
    public AddNewJobPage setJobDescription(final String jobDescription)
    {
        jobDescriptionTextBox.clear();
        jobDescriptionTextBox.sendKeys(jobDescription);
        return this;
    }

    @Step("Set ETA Date")
    public AddNewJobPage setEtaDate(final String etaDate)
    {
        etaTextBox.clear();
        etaTextBox.sendKeys(etaDate);
        etaTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set ETD Date")
    public AddNewJobPage setEtdDate(final String etdDate)
    {
        etdTextBox.clear();
        etdTextBox.sendKeys(etdDate);
        etdTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Requested attendance Date")
    public AddNewJobPage setRequestedAttendanceDate(final String requestedAttendanceDate)
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        requestedAttendanceDateTextBox);
        requestedAttendanceDateTextBox.clear();
        requestedAttendanceDateTextBox.sendKeys(requestedAttendanceDate);
        requestedAttendanceDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("see if the SDO error icon is displayed")
    public boolean isSdoErrorIconDisplayed(final type type)
    {
        return sdoTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("Get Validation Message Text")
    public String getValidationMessageText()
    {
        return validationMessage.getText().trim();
    }

    @Step("Get Validation Message Text for location")
    public String getLocationRequiredValidationMessageText()
    {
        return locationRequiredValidationMessage.getText().trim();
    }

    @Step("Get Validation Message Text for description")
    public String getDescriptionRequiredValidationMessageText()
    {
        return descriptionRequiredValidationMessage.getText().trim();
    }

    @Step("Get Validation Message Text for ETA")
    public String getEtaValidationMessageText()
    {
        return etaRequiredValidationMessage.getText().trim();
    }

    @Step("Get Validation Message Text for ETD")
    public String getEtdValidationMessageText()
    {
        return etdRequiredValidationMessage.getText().trim();
    }

    @Step("see if the job location error icon is displayed")
    public boolean isJobLocationErrorIconDisplayed(final type type)
    {
        return jobLocationTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("see if the job description error icon is displayed")
    public boolean isJobDescriptionErrorIconDisplayed(final type type)
    {
        return jobDescriptionTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("see if ETA error icon is displayed")
    public boolean isEtaErrorIconDisplayed(final type type)
    {
        return etaTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("see if ETD error icon is displayed")
    public boolean isEtdErrorIconDisplayed(final type type)
    {
        return etdTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("see if Requested attendance date error icon is displayed")
    public boolean isRequestedAttendanceDateErrorIconDisplayed(final type type)
    {
        return requestedAttendanceDateTextBox.getCssValue("background-image")
                .contains(type.image);
    }

    @Step("Clear SDO Text Field")
    public AddNewJobPage clearSdoTextField()
    {
        ClickClearHelper.clickClearText(sdoTextBox);
        return this;
    }

    @Step("Clear Job Location Text Field")
    public AddNewJobPage clearJobLocationTextField()
    {
        jobLocationTextBox.clear();
        return this;
    }

    @Step("Clear Job Description Text Field")
    public AddNewJobPage clearJobDescriptionTextField()
    {
        jobDescriptionTextBox.clear();
        return this;
    }

    @Step("Clear ETA Text Field")
    public AddNewJobPage clearEtaTextField()
    {
        etaTextBox.clear();
        return this;
    }

    @Step("Clear ETD Text Field")
    public AddNewJobPage clearEtdTextField()
    {
        etdTextBox.clear();
        return this;
    }

    @Step("Clear Requested attendance date Text Field")
    public AddNewJobPage clearRequestedAttendanceDateTextField()
    {
        requestedAttendanceDateTextBox.clear();
        return this;
    }

    @Step("Is SDO field is type ahead function")
    public boolean isSdoFieldTypeAhead(final String sdoCode)
    {
        sdoTextBox.clear();
        sdoTextBox.sendKeys(sdoCode);
        waitForJavascriptFrameworkToFinish();
        return driver.findElement(By.tagName("li")).isDisplayed();
    }

    @Step("is Job location field is type ahead function")
    public boolean isJobLocationFieldTypeAhead(final String sdoCode)
    {
        jobLocationTextBox.clear();
        jobLocationTextBox.sendKeys(sdoCode);
        waitForJavascriptFrameworkToFinish();
        return driver.findElement(By.tagName("li")).isDisplayed();
    }

    @Step("Get Default Selected Case type")
    public String getDefaultSelectedCaseType()
    {
        return new Select(casesSelectDropDown)
                .getFirstSelectedOption().getText().trim();
    }

    @Step("Get Case dropdown Size")
    public int getCaseDropDownSize()
    {
        return new Select(casesSelectDropDown).getOptions().size();
    }

    @Step("Get job location character size")
    public String getJobLocationCharacterSize()
    {
        return jobLocationTextBox.getAttribute("maxlength");
    }

    @Step("Get job description character size")
    public String getJobDescriptionCharacterSize()
    {
        return jobDescriptionTextBox.getAttribute("maxlength");
    }

    @Step("Click on Navigate Back button")
    public JobsSubPage clickNavigateBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Select DataElement from the Type Ahead Suggestions DropBox By Index {0}")
    public AddNewJobPage selectTypeAheadSuggestionDropBoxByIndex(final int index)
    {
        final List<WebElement> suggestions = driver.findElements(By.cssSelector("[data-ng-repeat='match in matches'] a.ng-binding"));
        suggestions.get(index).click();
        return this;
    }

    /**
     * Create A New Job
     *
     * @return ViewJobDetailsPage
     */
    @Step("Create a New Job ")
    public ViewJobDetailsPage addNewJob(final String location)
    {
        return selectJobCategorySurvey()
                .setSdoCode(sdoCode)
                .selectCaseTypeByIndex(1)
                .setJobLocation(location)
                .setJobDescription(description)
                .setEtaDate(strEtaDate)
                .setEtdDate(strEtdDate)
                .setRequestedAttendanceDate(strEtdDate)
                .clickSaveButton();
    }

    public ViewJobDetailsPage addNewJob()
    {
        return addNewJob(location);
    }

    public enum type
    {
        ERROR("images/red_warning.png");
        private final String image;

        type(final String image)
        {
            this.image = image;
        }
    }

}
