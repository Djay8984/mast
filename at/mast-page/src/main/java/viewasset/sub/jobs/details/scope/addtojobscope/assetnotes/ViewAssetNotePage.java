package viewasset.sub.jobs.details.scope.addtojobscope.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.aicocan.Header;

public class ViewAssetNotePage extends BasePage<ViewAssetNotePage>
{

    @Visible
    @Name("Description")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement description;

    @Visible
    @Name("Attachment Link")
    @FindBy(css = "[attachments='data.value']")
    private WebElement attachmentLink;

    @Step("Get Header")
    public Header getHeader()
    {
        return PageFactory.newInstance(Header.class);
    }
}
