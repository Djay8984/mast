package viewasset.sub.jobs.details.scope.addtojobscope.element.sub;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub.JobScopeHierarchyNodeElements;

import java.util.List;
import java.util.stream.Collectors;

@Deprecated
@Name("Job Scope Service Type Elements")
@FindBy(css = "[data-ng-repeat='(product, item) in value track by $index'")
public class JobScopeServiceTypeElements extends HtmlElement
{

    @Name("Expand Icon")
    @FindBy(css = "[data-ng-click='item.expanded = !item.expanded'] [data-ng-if='!item.expanded']")
    private WebElement expandIcon;

    @Name("Collapsible Icon")
    @FindBy(css = "[data-ng-click='item.expanded = !item.expanded'] [data-ng-if='item.expanded']")
    private List<WebElement> collapsibleIcon;

    @Name("Node Name Text")
    @FindBy(css = "[data-ng-bind='product']")
    private WebElement nodeNameText;

    @Name("Service Item Elements")
    private List<JobScopeHierarchyNodeElements> jobScopeHierarchyNodeElements;

    @Step("Get Node Name")
    public String getNodeName()
    {
        return nodeNameText.getText().trim();
    }

    @Step("Click on expand icon")
    public JobScopeServiceTypeElements clickOnExpandIcon()
    {
        expandIcon.click();
        return this;
    }

    @Step("Is product catalogue expanded ")
    public boolean isJobScopeServiceTypeExpanded()
    {
        return collapsibleIcon.size() == 0;
    }

    @Step("Get all JobScopeHierarchyNodeElements")
    public List<JobScopeHierarchyNodeElements> getJobScopeHierarchyNodeElements()
    {
        return jobScopeHierarchyNodeElements;
    }

    @Step("Get all Selectable service Item")
    public List<JobScopeHierarchyNodeElements> getAllJobScopeHierarchyNodeElements()
    {
        return jobScopeHierarchyNodeElements.stream()
                .filter(e -> e.isCheckBoxEnabled())
                .collect(Collectors.toList());
    }

}
