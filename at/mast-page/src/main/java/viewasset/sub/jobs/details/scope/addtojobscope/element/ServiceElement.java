package viewasset.sub.jobs.details.scope.addtojobscope.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.TestDataHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

import java.time.LocalDate;
import java.util.Date;

import static constant.LloydsRegister.FRONTEND_TIME_FORMAT_DTS;

public class ServiceElement extends HtmlElement
{

    WebDriver driver = BaseTest.getDriver();

    @Visible
    @Name("check box")
    @FindBy(css = "button.checkbox-dir")
    private CheckBox checkBox;

    @Visible
    @Name("Service Code")
    @FindBy(css = "[data-ng-bind='service.code']")
    private WebElement serviceCode;

    @Visible
    @Name("Node title")
    @FindBy(css = "[data-ng-bind='service.name']")
    private WebElement serviceName;

    @Visible
    @Name("Due Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.dueDate |  moment']")
    private WebElement dueDate;

    @Visible
    @Name("Lower Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.lowerRangeDate | moment']")
    private WebElement lowerRangeDate;

    @Visible
    @Name("Upper Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.lowerRangeDate | moment']")
    private WebElement upperRangeDate;

    @Name("Warning message")
    @FindBy(css = ".grid-content.small-2.node-service-warning")
    private WebElement warningMessage;

    @Step("Select check box")
    public ServiceElement selectCheckBox()
    {

        AppHelper.scrollToBottom();
        if (!(checkBox.isSelected()))
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Get check box")
    public CheckBox getCheckbox()
    {
        return checkBox;
    }

    @Step("DeSelect check box")
    public ServiceElement deSelectCheckBox()
    {
        AppHelper.scrollToBottom();
        if (checkBox.isSelected())
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Get service code")
    public String getServiceCode()
    {
        return serviceCode.getText();
    }

    @Step("Get service name")
    public String getServiceName()
    {
        return serviceName.getText();
    }

    @Step("Get due date")
    public Date getDueDate()
    {
        return TestDataHelper.toDate(LocalDate.parse(dueDate.getText(), FRONTEND_TIME_FORMAT_DTS));
    }

    @Step("Get lower range date")
    public Date getLowerRangeDate()
    {
        return TestDataHelper.toDate(LocalDate.parse(lowerRangeDate.getText(), FRONTEND_TIME_FORMAT_DTS));
    }

    @Step("Get upper range date")
    public Date getUpperRangeDate()
    {
        return TestDataHelper.toDate(LocalDate.parse(upperRangeDate.getText(), FRONTEND_TIME_FORMAT_DTS));
    }

    @Step("Get warning message")
    public String getWarningMessage()
    {
        return warningMessage.getText();
    }
}
