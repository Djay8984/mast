package viewasset.sub.jobs.details.scope.addtojobscope.conditionofclass;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ViewDefectPage extends BasePage<ViewDefectPage>
{

    @Visible
    @Name("back button")
    @FindBy(css = ".back-button")
    private WebElement backButton;

    @Visible
    @Name("Defect Id")
    @FindBy(id = "view-defect-id")
    private WebElement defectId;
}
