package viewasset.sub.jobs.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.AssetNotePage;

@Name("Asset Notes Elements")
@FindBy(css = "[ng-if='vm.otherCodicils.length'] [item-type='vm.itemType']")
public class AssetNotesElements extends HtmlElement
{

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Asset Status Name")
    @FindBy(css = "[data-ng-bind='vm.item.status.name']")
    private WebElement itemStatus;

    @Step("Get Asset Status")
    public String getItemStatus()
    {
        return itemStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public AssetNotePage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(AssetNotePage.class);
    }
}
