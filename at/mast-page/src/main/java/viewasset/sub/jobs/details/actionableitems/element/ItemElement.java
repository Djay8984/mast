package viewasset.sub.jobs.details.actionableitems.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.RadioButton;

import java.util.List;

public class ItemElement extends HtmlElement
{

    @FindBy(xpath = "./following-sibling::ul/li/div/div/div[@class='title']")
    private List<ItemElement> children;

    @Name("Minus Icon")
    @FindBy(css = ".minus")
    private WebElement minusIcon;

    @Name("Plus Icon")
    @FindBy(css = ".plus")
    private WebElement plusIcon;

    @Name("Radio Button")
    @FindBy(css = "[data-ng-click='data.select()'] .radio-box")
    private RadioButton radioButton;

    @Name("Item Name")
    @FindBy(css = "[data-ng-bind='data.item.name']")
    private WebElement itemName;

    @Step("Click Radio Button")
    public ItemElement clickRadioButton()
    {
        radioButton.select();
        return this;
    }

    @Step("Click on Plus Icon")
    public ItemElement clickPlusIcon()
    {
        plusIcon.click();
        return this;
    }

    @Step("Get Child Items")
    public List<ItemElement> getChildren()
    {
        return children;
    }

    @Step("Is Plus Icon displayed")
    public boolean isPlusIconDisplayed()
    {
        return plusIcon.isDisplayed();
    }

    @Step("Is Radio button Selected")
    public boolean isRadioButtonSelected()
    {
        return radioButton.isSelected();
    }

    @Step("Get Item Name")
    public String getItemName()
    {
        return itemName.getText().trim();
    }

}
