package viewasset.sub.jobs.details.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.actionableitems.AddActionableItemPage;

public class ViewDefectPage extends BasePage<ViewDefectPage>
{

    @Visible
    @Name("Add COC button")
    @FindBy(css = "[data-ng-click='vm.addCodicil('coc')']")
    private WebElement addCocButton;

    @Visible
    @Name("Back Button")
    @FindBy(css = ".back-button")
    private WebElement backButton;

    @Visible
    @Name("Add Actionable Item button")
    @FindBy(css = "[data-ng-click='vm.addCodicil('actionable-item')']")
    private WebElement addActionableItemButton;

    @Step("Click Add Actionable Item button")
    public AddActionableItemPage clickAddActionableItemButton()
    {
        addActionableItemButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click Back button")
    public DefectsPage clickNavigateBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(DefectsPage.class);
    }

}
