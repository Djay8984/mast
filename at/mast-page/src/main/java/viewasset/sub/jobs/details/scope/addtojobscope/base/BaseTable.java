package viewasset.sub.jobs.details.scope.addtojobscope.base;

import com.frameworkium.core.ui.js.JavascriptWait;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.DataElement;

import java.util.List;

@Deprecated
public class BaseTable extends HtmlElement
{

    private static final WebDriver driver = BaseTest.getDriver();
    private static final WebDriverWait wait = new WebDriverWait(driver, 10);
    private static final JavascriptWait javascriptWait = new JavascriptWait(driver, wait);

    @Name("Data Elements")
    private List<DataElement> dataElements;
    @Name("Service Elements")
    @FindBy(css = "[data-ng-click='item.expanded = !item.expanded']")
    private List<WebElement> serviceElements;
    @Name("Other Elements")
    @FindBy(css = "span[data-ng-click^='vm.expand']")
    private List<WebElement> cocAiAnElements;

    @Step("Get All Data Elements")
    public List<DataElement> getRows()
    {
        return dataElements;
    }

    @Step("Expand Services Table Elements")
    public void expandServiceElements()
    {
        for (WebElement serviceElement : serviceElements)
        {
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(true);",
                            serviceElement);
            serviceElement.click();
            javascriptWait.waitForJavascriptEventsOnLoad();
        }
    }

    @Step("Expand Coc AI AN table elements")
    public void expandCocAiAnElements()
    {
        for (WebElement OtherElement : cocAiAnElements)
        {
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(true);",
                            OtherElement);
            OtherElement.click();
            javascriptWait.waitForJavascriptEventsOnLoad();
        }
    }
}
