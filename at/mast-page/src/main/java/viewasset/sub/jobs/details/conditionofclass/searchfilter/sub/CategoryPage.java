package viewasset.sub.jobs.details.conditionofclass.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class CategoryPage extends BasePage<CategoryPage>
{

    @Visible
    @Name("Select All Button")
    @FindBy(id = "codicil-search-filter-status-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear All Button")
    @FindBy(id = "codicil-search-filter-status-clear-all")
    private WebElement clearAllButton;

    @Step("Click Select All Button")
    public CategoryPage clickSelectAllButton()
    {
        selectAllButton.click();
        return this;
    }

    @Step("Click Clear All Button")
    public CategoryPage clickClearAllButton()
    {
        clearAllButton.click();
        return this;
    }
}
