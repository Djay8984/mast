package viewasset.sub.jobs;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.addnewjob.AddNewJobPage;
import viewasset.sub.jobs.element.AllJobElement;
import viewasset.sub.jobs.element.MyJobsElements;
import viewasset.sub.jobs.searchfilter.SearchFilterJobsPage;
import workhub.sub.PaginationCountSubPage;

import java.util.List;

public class JobsSubPage extends BasePage<JobsSubPage>
{

    private final String loadMoreButtonCss = "[data-ng-if='vm.hasMorePages'] button";
    @Name("Search/Filter Jobs Button")
    @FindBy(css = "button[data-ng-click='vm.filterOpen = !vm.filterOpen']")
    @Visible
    private WebElement searchFilterJobsButton;
    @Name("All Jobs")
    private List<AllJobElement> allJobs;
    @Name("All Jobs")
    private List<MyJobsElements> myJobs;
    @Name("Load More button")
    @FindBy(css = loadMoreButtonCss)
    private WebElement loadMoreButton;

    @Name("Message text")
    @FindBy(css = "[data-ng-if='!vm.otherJobs.length']")
    private WebElement messageText;

    @Visible
    @Name("Add New Job Button")
    @FindBy(css = "button[class='primary-button'] span")
    private WebElement addNewJobButton;

    @Step("Clicking the search/filter button")
    public SearchFilterJobsPage clickSearchFilterJobsButton()
    {
        searchFilterJobsButton.click();
        return PageFactory.newInstance(SearchFilterJobsPage.class);
    }

    @Step("Clicking load more button")
    public JobsSubPage clickLoadMoreButton()
    {
        executeJS("document.querySelector(\"" + loadMoreButtonCss + "\").scrollIntoView(false);");
        loadMoreButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName("spinner")));
        return this;
    }

    @Step("Return all jobs")
    public List<AllJobElement> getAllJobsElements()
    {
        return allJobs;
    }

    @Step("Returning the amount of jobs under all jobs")
    public int getAllJobsCount()
    {
        return allJobs.size();
    }

    @Step("Returning an My Job card by Job Number")
    public MyJobsElements getMyJobsByJobNumber(String number)
    {
        return myJobs.stream()
                .filter(c -> c.getMyJobNumber().equals(number))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an My Job card by Job Number")
    public AllJobElement getMyJobsByJobStatus(String status)
    {
        return allJobs.stream()
                .filter(c -> c.getJobStatus().equals(status))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Returning a card by Index")
    public MyJobsElements getJobsByIndex(int index)
    {
        return myJobs.get(index);
    }

    @Step("Returning a card by Index")
    public AllJobElement getJobByIndex(int index)
    {
        return allJobs.get(index);
    }

    @Step("Returning All Jobs by Index")
    public AllJobElement getAllJobsByIndex(int index)
    {
        return allJobs.get(index);
    }

    @Step("Get no active jobs message")
    public String getMessage()
    {
        return messageText.getText();
    }

    @Step("Get Pagination Page")
    public PaginationCountSubPage getPaginationPage()
    {
        return PageFactory.newInstance(PaginationCountSubPage.class);
    }

    @Step("Check that Load More button is visible")
    public boolean isLoadMoreButtonPresent()
    {
        return driver
                .findElements(By.cssSelector(loadMoreButtonCss))
                .size() > 0;
    }

    @Step("Is search/filter button clickable")
    public boolean isSearchFilterJobsButtonClickable()
    {
        return searchFilterJobsButton.isEnabled();
    }

    @Step("Click Add New Job Button")
    public AddNewJobPage clickAddNewJobButton()
    {
        addNewJobButton.click();
        return PageFactory.newInstance(AddNewJobPage.class);
    }

    @Step("Is Add New Job Button Clickable")
    public boolean isAddNewJobButtonClickable()
    {
        return addNewJobButton.isEnabled();
    }

    @Step("Returning an All Job card by Job ID")
    public AllJobElement getAllJobsByJobId(String jobId)
    {
        return allJobs.stream()
                .filter(c -> c.getJobNumber().equals(jobId))
                .findFirst()
                .orElse(null);
    }
}
