package viewasset.sub.jobs.details.crediting.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class InfoElement extends HtmlElement
{

    @Name("name")
    @FindBy(css = ".dark-blue.crediting-title.ng-binding")
    private WebElement name;

    @Name("ID")
    @FindBy(css = "[data-ng-bind='vm.creditable.id | zeropad: 5']")
    private WebElement id;

    @Name("Credited On Date")
    @FindBy(css = "[data-ng-bind$='dateOfCrediting | moment']")
    private WebElement creditedOnDate;

    @Name("Credited By")
    @FindBy(css = "[data-ng-if$='creditedBy'] strong")
    private WebElement creditedBy;

    @Step("Get the name of Coc")
    public String getName()
    {
        return name.getText();
    }

    @Step("Get the ID of Coc Card")
    public String getId()
    {
        return id.getText();
    }

    @Step("Get CreditedOn Date")
    public String getCreditedOnDate()
    {
        return creditedOnDate.getText();
    }

    @Step("Get Credited By Field")
    public String getCreditedBy()
    {
        return creditedBy.getText();
    }

    @Step("Check if Credited by field is editable")
    public boolean isCreditedByFieldEditable()
    {
        return creditedBy.getTagName().contains("input");
    }

    @Step("check if Credited On field is editable")
    public boolean isCreditedOnFieldEditable()
    {
        return creditedOnDate.getTagName().contains("input");
    }
}
