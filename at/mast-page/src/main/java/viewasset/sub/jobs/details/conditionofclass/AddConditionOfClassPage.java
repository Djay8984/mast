package viewasset.sub.jobs.details.conditionofclass;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.TextArea;
import util.AppHelper;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import workhub.sub.SearchFilterSubPage;

public class AddConditionOfClassPage extends BasePage<AddConditionOfClassPage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(id = "title")
    private WebElement titleTextBox;

    @Visible
    @Name("Description Text Box")
    @FindBy(id = "description")
    private WebElement descriptionTextBox;

    @Visible
    @Name("Inherited Check Box")
    @FindBy(id = "inherited-flag")
    private WebElement inheritedCheckBox;

    @Visible
    @Name("Imposed Date Text Box")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextBox;

    @Visible
    @Name("Due Date Text Box")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextBox;

    @Visible()
    @Name("LR Internal Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(1)")
    private WebElement lrInternalButton;

    @Visible()
    @Name("LR and Customer Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(2)")
    private WebElement lrAndCustomerButton;

    @Visible()
    @Name("All Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(3)")
    private WebElement allButton;

    @Visible
    @Name("Attachments Link")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement attachmentLink;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".ng-scope.transparent-dark-button")
    private WebElement cancelButton;

    @Visible
    @Name("Save Button")
    @FindBy(css = ".ng-scope.primary-button")
    private WebElement saveButton;

    @Visible
    @Name("Navigate back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Imposed Date Error Message Text")
    @FindBy(css = "#tpl-imposedDate [data-ng-if='vm.showMessage']")
    private WebElement imposedDateErrorMessageText;

    @Name("Due Date Error Message Text")
    @FindBy(css = "#tpl-dueDate [data-ng-if='vm.showMessage']")
    private WebElement dueDateErrorMessageText;

    @Name("Typified by text area")
    @FindBy(tagName = "textarea")
    private TextArea textArea;

    @Step("Set value to Title Text Box")
    public AddConditionOfClassPage setTitle(String title)
    {
        titleTextBox.clear();
        titleTextBox.sendKeys(title);
        return this;
    }

    @Step("Set value to Description Text Box")
    public AddConditionOfClassPage setDescription(String title)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(title);
        return this;
    }

    @Step("Select Inherited checkbox")
    public AddConditionOfClassPage selectInheritedCheckBox()
    {
        inheritedCheckBox.click();
        return this;
    }

    @Step("Set Imposed Date")
    public AddConditionOfClassPage setImposedDate(String imposedDate)
    {
        imposedDateTextBox.clear();
        imposedDateTextBox.sendKeys(imposedDate);
        imposedDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Due Date")
    public AddConditionOfClassPage setDueDate(String imposedDate)
    {
        dueDateTextBox.clear();
        dueDateTextBox.sendKeys(imposedDate);
        dueDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Click LR Internal Button")
    public AddConditionOfClassPage clickLrInternalButton()
    {
        lrInternalButton.click();
        return this;
    }

    @Step("Click LR and Customer Button")
    public AddConditionOfClassPage clickLrAndCustomerButton()
    {
        AppHelper.scrollToBottom();
        lrAndCustomerButton.click();
        return this;
    }

    @Step("Click All Button")
    public AddConditionOfClassPage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Click on attachments")
    public AttachmentsAndNotesPage clickAttachmentButton()
    {
        attachmentLink.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    /**
     * @return CodicilsAndDefectsPage or ConditionsOfClassPage
     */
    @Step("Click Save Button")
    public <T extends BasePage<T>> T clickSaveButton(Class<T> pageObjectClass)
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        saveButton);
        saveButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Cancel Button")
    public ConditionsOfClassPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Click navigate back button")
    public ConditionsOfClassPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Check for error icon is displayed in Imposed Date TextBox ")
    public boolean isErrorIconDisplayedInImposedDateTextBox(type type)
    {
        return imposedDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Check for error icon is displayed in Due Date TextBox ")
    public boolean isErrorIconDisplayedInDueDateTextBox(type type)
    {
        return dueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Get Due Date Error Message Text")
    public String getDueDateErrorMessageText()
    {
        return dueDateErrorMessageText.getText().trim();
    }

    @Step("Get Imposed Date Error Message Text")
    public String getImposedDateErrorMessageText()
    {
        return imposedDateErrorMessageText.getText().trim();
    }

    @Step("Is Due Date Field Enabled")
    public boolean isDueDateFieldEnabled()
    {
        return dueDateTextBox.isEnabled();
    }

    @Step("Get title field helper text")
    public String getTitleFieldHelperText()
    {
        return titleTextBox.getAttribute("placeholder").trim();
    }

    @Step("Is Save button Enabled")
    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }

    @Step("Get description field helper text")
    public String getTitleDescriptionFieldHelperText()
    {
        return descriptionTextBox.getAttribute("placeholder").trim();
    }

    @Step("Is LR Internal button enabled")
    public boolean isLrInternalButtonEnabled()
    {
        return lrInternalButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is LR and customer button enabled")
    public boolean isLrAndCustomerButtonEnabled()
    {
        return lrAndCustomerButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is All button enabled")
    public boolean isAllButtonEnabled()
    {
        return allButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Get Title Text")
    public String getTitleText()
    {
        return titleTextBox.getAttribute("value").trim();
    }

    @Step("Get Description Text")
    public String getDescriptionText()
    {
        return descriptionTextBox.getAttribute("value").trim();
    }

    @Step("Get Imposed Date Text")
    public String getImposedDateText()
    {
        return imposedDateTextBox.getAttribute("value").trim();
    }

    @Step("Get Due Date Text")
    public String getDueDateText()
    {
        return dueDateTextBox.getAttribute("value").trim();
    }

    @Step("Get Style attribute on Description Text box")
    public Dimension getTextAreaDimension()
    {
        return descriptionTextBox.getSize();
    }

    @Step("Get Style attribute on Description Text box")
    public AddConditionOfClassPage setTextAreaSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END)
                .perform();
        textArea.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    public enum type
    {
        ERROR("images/red_warning.png");

        private final String image;

        type(String image)
        {
            this.image = image;
        }
    }

}
