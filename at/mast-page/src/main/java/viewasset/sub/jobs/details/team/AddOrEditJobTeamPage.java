package viewasset.sub.jobs.details.team;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.ClickClearHelper;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

public class AddOrEditJobTeamPage extends BasePage<AddOrEditJobTeamPage>
{

    @Visible
    @Name("SDO textBox")
    @FindBy(id = "team-job-sdo-coordinator")
    private WebElement sdoCoordinatorTextBox;

    @Visible
    @Name("Lead Surveyor TextBox")
    @FindBy(id = "team-job-surveyor-0")
    private WebElement leadSurveyorTextBox;

    @Visible
    @Name("Authorising Surveyor textBox")
    @FindBy(id = "team-job-authorising-surveyor")
    private WebElement authorisingSurveyorTextBox;

    @Visible
    @Name("Fleet Services Specialist text Box")
    @FindBy(id = "team-job-eic-manager")
    private WebElement fleetServicesSpecialistTextBox;

    @Name("Inline Error Message in Lead Surveyor text Box")
    @FindBy(css = "[class*= 'form-control']:nth-child(3) li a")
    private WebElement inLineErrorInLeadSurveyorTextBox;

    @Name("Error Message for Lead Surveyor text Box")
    @FindBy(id = "validation-message-team-job-surveyor-required")
    private WebElement errorMessageInLeadSurveyorTextBox;

    @Name("Duplicate Error Message for Lead Surveyor text Box")
    @FindBy(id = "validation-message-team-job-surveyor-duplicate")
    private WebElement duplicateErrorMessageInLeadSurveyorTextBox;

    @Visible
    @Name("Cancel button")
    @FindBy(id = "team-job-preview-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Save button")
    @FindBy(id = "team-job-preview-save")
    private WebElement saveButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Navigate Back Button Jobs SubPage")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Check If SDO Coordinator text field is enabled")
    public boolean isSdoCoordinatorTextBoxEnabled()
    {
        return sdoCoordinatorTextBox.findElements(By.cssSelector("[disabled]")).size() <= 0;
    }

    @Step("Check if Lead Surveyor field is enabled")
    public boolean isLeadSurveyorTextBoxEnabled()
    {
        return leadSurveyorTextBox.findElements(By.cssSelector("[disabled]")).size() <= 0;
    }

    @Step("Check if Authorising Surveyor text field is enabled")
    public boolean isAuthorisingSurveyorTextBoxEnabled()
    {
        return authorisingSurveyorTextBox.findElements(By.cssSelector("[disabled]")).size() <= 0;
    }

    @Step("Check if Fleet Services Specialist text field is enabled")
    public boolean isFleetServicesSpecialistTextBoxEnabled()
    {
        return fleetServicesSpecialistTextBox.findElements(By.cssSelector("[disabled]")).size() <= 0;
    }

    @Step("Check if SDO Coordinator text field is optional")
    public boolean isSdoCoordinatorTextBoxOptional()
    {
        return sdoCoordinatorTextBox.findElements(By.cssSelector("[required]")).size() <= 0;
    }

    @Step("Set lead surveyor {0}")
    public AddOrEditJobTeamPage setLeadSurveyorTextBox(final String surveyor)
    {
        leadSurveyorTextBox.clear();
        waitForJavascriptFrameworkToFinish();
        leadSurveyorTextBox.sendKeys(surveyor);
        leadSurveyorTextBox.sendKeys(Keys.ENTER);
        return this;
    }

    @Step("Set lead surveyor {0} without enter")
    public AddOrEditJobTeamPage setLeadSurveyorTextBoxWithoutEnter(final String surveyor)
    {
        waitForJavascriptFrameworkToFinish();
        leadSurveyorTextBox.sendKeys(surveyor);
        return this;
    }

    @Step("Set SDO Coordinator {0}")
    public AddOrEditJobTeamPage setSdoCoordinatorTextBox(final String input)
    {
        sdoCoordinatorTextBox.clear();
        sdoCoordinatorTextBox.sendKeys(input);
        sdoCoordinatorTextBox.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set Authorising Surveyor {0}")
    public AddOrEditJobTeamPage setAuthorisingSurveyorTextBox(final String input)
    {
        authorisingSurveyorTextBox.clear();
        authorisingSurveyorTextBox.sendKeys(input);
        authorisingSurveyorTextBox.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set Fleet Services Specialist {0}")
    public AddOrEditJobTeamPage setFleetServicesSpecialistTextBox(final String input)
    {
        fleetServicesSpecialistTextBox.clear();
        fleetServicesSpecialistTextBox.sendKeys(input);
        fleetServicesSpecialistTextBox.sendKeys(Keys.ENTER);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select DataElement from the Type Ahead Suggestions DropBox By Index {0}")
    public AddOrEditJobTeamPage selectTypeAheadSuggestionDropBoxByIndex(final int index)
    {
        final List<WebElement> suggestions = driver.findElements(By.cssSelector("[data-ng-repeat='match in matches'] a.ng-binding"));
        suggestions.get(index).click();
        return this;
    }

    @Step("Click Clear button of Lead Surveyor Textbox")
    public AddOrEditJobTeamPage clearLeadSurveyorTextbox()
    {
        ClickClearHelper.clickClearText(leadSurveyorTextBox);
        return this;
    }

    @Step("Get Required Message In Lead Surveyor Text Box")
    public String getErrorMessageInLeadSurveyorTextBox()
    {
        return errorMessageInLeadSurveyorTextBox.getText();
    }

    @Step("Get Duplicate Error Message In Lead Surveyor Text Box")
    public String getDuplicateErrorMessageInLeadSurveyorTextBox()
    {
        return duplicateErrorMessageInLeadSurveyorTextBox.getText();
    }

    @Step("Get error message from In Surveyor Text Box")
    public String getInLineErrorInLeadSurveyorTextBox()
    {
        return inLineErrorInLeadSurveyorTextBox.getText();
    }

    @Step("Get Lead Surveyor")
    public String getLeadSurveyor()
    {
        return leadSurveyorTextBox.getAttribute("value");
    }

    @Step("Get SDO Coordinator")
    public String getSdoCoordinator()
    {
        return sdoCoordinatorTextBox.getAttribute("value");
    }

    @Step("Get Authorising Surveyor")
    public String getAuthorisingSurveyor()
    {
        return authorisingSurveyorTextBox.getAttribute("value");
    }

    @Step("Get FleetServiceSpecialist")
    public String getFleetServiceSpecialist()
    {
        return fleetServicesSpecialistTextBox.getAttribute("value");
    }

    @Step("Click on Cancel button")
    public ViewJobDetailsPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Click on Save button")
    public ViewJobDetailsPage clickSaveButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(saveButton));
        saveButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}
