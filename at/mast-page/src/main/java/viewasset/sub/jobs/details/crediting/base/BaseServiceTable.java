package viewasset.sub.jobs.details.crediting.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.crediting.element.ServiceElement;

import java.util.List;

public class BaseServiceTable extends HtmlElement
{
    //gets the first generation rows/elements
    //must use xpath because it allows reference to current node
    // have to wait for CSS4 to be able to do this with CSS
    @FindBy(xpath = "./div[2]/crediting-hierarchy/section/ul/li/div[1]")
    private List<ServiceElement> serviceElements;

    @Name("Group Name")
    @FindBy(css = "[data-ng-bind='group.name']")
    private WebElement groupName;

    @Step("Get Group Name")
    public String getGroupName()
    {
        return groupName.getText().trim();
    }

    @Step("Get rows")
    public List<ServiceElement> getRows()
    {
        return serviceElements;
    }
}
