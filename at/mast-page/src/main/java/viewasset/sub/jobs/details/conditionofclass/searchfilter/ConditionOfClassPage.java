package viewasset.sub.jobs.details.conditionofclass.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.conditionofclass.searchfilter.sub.*;

public class ConditionOfClassPage extends BasePage<ConditionOfClassPage>
{

    @Visible
    @Name("Search Text Box")
    @FindBy(id = "codicil-search-query")
    private WebElement searchTextBox;

    @Visible
    @Name("Reset Button")
    @FindBy(id = "codicil-search-reset")
    private WebElement resetButton;

    @Visible
    @Name("Search Button")
    @FindBy(id = "codicil-search-button")
    private WebElement searchButton;

    @Visible
    @Name("Add Button")
    @FindBy(id = "add-attributes-button")
    private WebElement addButton;

    @Visible
    @Name("Category filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(1) .codicil-filter-button")
    private WebElement categoryFilterButton;

    @Visible
    @Name("Due date filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(2) .codicil-filter-button")
    private WebElement dueDateFilterButton;

    @Visible
    @Name("Status filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(3) .codicil-filter-button")
    private WebElement statusFilterButton;

    @Visible
    @Name("SDO filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(1) .codicil-filter-button")
    private WebElement sdoFilterButton;

    @Visible
    @Name("Confidentiality filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(2) .codicil-filter-button")
    private WebElement confidentialityFilterButton;

    @Visible
    @Name("View All Button")
    @FindBy(css = "label[for='view-type-all']")
    private WebElement viewAllButton;

    @Visible
    @Name("View Job related only Button")
    @FindBy(css = "label[for='view-type-job']")
    private WebElement viewJobRelatedOnlyButton;

    @Visible
    @Name("View Others only Button")
    @FindBy(css = "label[for='view-type-other']")
    private WebElement viewOthersOnlyButton;

    @Step("Click Search Button")
    public ConditionsOfClassPage clickSearchButton()
    {
        AppHelper.scrollToBottom();
        searchButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Click Reset Button")
    public ConditionsOfClassPage clickResetButton()
    {
        resetButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Get Search Text")
    public String getSearchText()
    {
        return searchTextBox.getAttribute("value");
    }

    @Step("Set the search type into Search Text Box")
    public ConditionOfClassPage setSearchText(String title)
    {
        searchTextBox.clear();
        searchTextBox.sendKeys(title);
        return this;
    }

    @Step("Is Search button enabled")
    public boolean isSearchButtonEnabled()
    {
        return searchButton.isEnabled();
    }

    @Step("Is Category filter button enabled")
    public boolean isCategoryFilterButtonEnabled()
    {
        return categoryFilterButton.isEnabled();
    }

    @Step("Click View All Button")
    public ConditionOfClassPage clickViewAllButton()
    {
        viewAllButton.click();
        return this;
    }

    @Step("Click View Job and related only button")
    public ConditionOfClassPage clickViewJobRelatedOnlyButton()
    {
        viewJobRelatedOnlyButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Others Only Button")
    public ConditionOfClassPage clickViewOtherOnlyButton()
    {
        viewOthersOnlyButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Get View All Button Background Colour")
    public String getViewAllBackgroundColour()
    {
        return viewAllButton.getCssValue("background-color");
    }

    @Step("Get View Job Related only Button Background Colour")
    public String getViewJobRelatedOnlyBackgroundColour()
    {
        return viewJobRelatedOnlyButton.getCssValue("background-color");
    }

    @Step("Get View Others Only Button Background Colour")
    public String getViewOtherOnlyBackgroundColour()
    {
        return viewOthersOnlyButton.getCssValue("background-color");
    }

    @Step("Click Category Filter button")
    public CategoryPage clickCategoryFilterButton()
    {
        categoryFilterButton.click();
        return PageFactory.newInstance(CategoryPage.class);
    }

    @Step("Click Due date Filter button")
    public DueDatePage clickDueDateFilterButton()
    {
        dueDateFilterButton.click();
        return PageFactory.newInstance(DueDatePage.class);
    }

    @Step("Click Status Filter button")
    public StatusPage clickStatusFilterButton()
    {
        statusFilterButton.click();
        return PageFactory.newInstance(StatusPage.class);
    }

    @Step("Click SDO Filter button")
    public SdoPage clickSdoFilterButton()
    {
        sdoFilterButton.click();
        return PageFactory.newInstance(SdoPage.class);
    }

    @Step("Click Confidentiality Filter button")
    public ConfidentialityPage clickConfidentialityFilterButton()
    {
        confidentialityFilterButton.click();
        return PageFactory.newInstance(ConfidentialityPage.class);
    }
}
