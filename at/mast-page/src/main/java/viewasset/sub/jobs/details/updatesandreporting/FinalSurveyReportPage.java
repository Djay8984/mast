package viewasset.sub.jobs.details.updatesandreporting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.HeaderPage;

public class FinalSurveyReportPage extends BasePage<FinalSurveyReportPage>
{

    @Visible
    @Name("Generate PDF Button")
    @FindBy(id = "report-generate-pdf-button")
    private WebElement generatePdfButton;

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

}
