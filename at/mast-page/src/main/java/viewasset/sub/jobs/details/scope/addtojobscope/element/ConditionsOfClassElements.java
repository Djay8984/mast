package viewasset.sub.jobs.details.scope.addtojobscope.element;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Deprecated
@Name("Conditions Of Class Elements")
@FindBy(css = "[data-ng-repeat^='node in vm.jobCocs'],[data-ng-repeat^='node in vm.wipCoCs']")
public class ConditionsOfClassElements extends HtmlElement
{

    @Visible
    @Name("check box")
    @FindBy(css = "[data-ng-model^='node.checkBoxMode'] span")
    private WebElement checkBox;

    @Visible
    @Name("Node title")
    @FindBy(css = "[data-ng-bind='node.title']")
    private WebElement nodeTitle;

    @Visible
    @Name("Further details arrow")
    @FindBy(css = ".glyph.float-right.grey")
    private WebElement furtherDetailsArrow;

    @Step("Select check box")
    public ConditionsOfClassElements selectCheckBox()
    {
        checkBox.click();
        return this;
    }

    @Step("Get node title")
    public String getNodeTitle()
    {
        return nodeTitle.getText().trim();
    }

}
