package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

public class HeaderPage extends BasePage<HeaderPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = "[id*='back-button'] svg")
    private WebElement navigateBackButton;

    @Step("Click Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        AppHelper.scrollToBottom();
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}
