package viewasset.sub.jobs.details.actionableitems.base;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.actionableitems.element.DataElement;
import viewasset.sub.jobs.details.actionableitems.element.ItemElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Data Elements")
    private List<DataElement> dataElements;

    @Name("Item Elements")
    @FindBy(xpath = "./child::div[@id='tpl-']/div/div[@class='title']")
    private List<ItemElement> itemElements;

    @Step("Get All Data Elements")
    public List<DataElement> getRows()
    {
        return dataElements;
    }

    @Step("Get All Item Elements")
    public List<ItemElement> getItems()
    {
        return itemElements;
    }
}
