package viewasset.sub.jobs.details.scope.addtojobscope.conditionofclass.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.conditionofclass.ViewDefectPage;

@Name("Linked Defect Elements")
@FindBy(css = "ul.item-list")
public class LinkedDefectElements extends HtmlElement
{

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = "[data-ui-sref^='defect.view']")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Defect Status")
    @FindBy(css = ".status strong")
    private WebElement defectStatus;

    @Step("Click on further details arrow")
    public ViewDefectPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewDefectPage.class);
    }
}
