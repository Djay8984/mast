package viewasset.sub.jobs.details.assetnotes.elements;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.assetnotes.DetailsPage;

@FindBy(css = "codicil-info")
public class AssetNoteElement extends HtmlElement
{

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = "div:nth-child(6) a")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Asset Status Name")
    @FindBy(css = "[data-ng-bind='data.obj.status.name']")
    private WebElement itemStatus;

    @Step("Get Asset Status")
    public String getItemStatus()
    {
        return itemStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public DetailsPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(DetailsPage.class);
    }

    @Step("Click Asset Note")
    public AssetNoteElement clickAssetNote()
    {
        this.click();
        return this;
    }
}

