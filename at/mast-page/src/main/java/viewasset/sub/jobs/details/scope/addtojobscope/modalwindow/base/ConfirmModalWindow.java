package viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfirmModalWindow extends BasePage<ConfirmModalWindow>
{

    @Visible
    @Name("OK Button")
    @FindBy(css = "modal-footer.ng-scope .primary-button")
    private WebElement okButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$close(false)']")
    private WebElement cancelButton;

    @Step("Click Cancel Button")
    public <T extends BasePage<T>> T clickCancelButton(Class<T> pageObjectClass)
    {
        cancelButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click OK Button")
    public <T extends BasePage<T>> T clickOkButton(Class<T> pageObjectClass)
    {
        okButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

}
