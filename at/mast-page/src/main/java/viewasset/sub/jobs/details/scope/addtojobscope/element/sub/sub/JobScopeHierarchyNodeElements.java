package viewasset.sub.jobs.details.scope.addtojobscope.element.sub.sub;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Job Scope Hierarchy Node Elements")
@FindBy(css = "[data-ng-class='{'unselectable':service.unselectable}']")
public class JobScopeHierarchyNodeElements extends HtmlElement {

    @Visible
    @Name("check box")
    @FindBy(css = "[data-ng-model^='service.checkBoxModel'] span")
    private WebElement checkBox;

    @Name("Service code Text")
    @FindBy(css = "[data-ng-bind='service.code']")
    private WebElement serviceCodeText;

    @Name("Service Type Text")
    @FindBy(css = "[data-ng-bind='service.name']")
    private WebElement serviceTypeText;

    @Step("Is check box enable")
    public boolean isCheckBoxEnabled()
    {
        return checkBox.isEnabled();
    }

    @Step("Select check box")
    public JobScopeHierarchyNodeElements selectCheckBox()
    {
        checkBox.click();
        return this;
    }

    @Step("Get service code text")
    public String getServiceCodeText()
    {
        return serviceCodeText.getText().trim();
    }

    @Step("Get Service Type Text")
    public String getServiceTypeText()
    {
        return serviceTypeText.getText().trim();
    }
}
