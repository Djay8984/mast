package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.DARPage;

public class GenerateDarForPage extends BasePage<GenerateDarForPage>
{

    @Visible
    @Name("Date Field")
    @FindBy(id = "first-visit-date")
    private WebElement dateField;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$dismiss()']")
    private WebElement cancelButton;

    @Visible
    @Name("Generate DAR Button")
    @FindBy(css = "[data-ng-click='vm.saveDAR()']")
    private WebElement generateDarButton;

    @Step("Click Generate DAR Button")
    public DARPage clickGenerateDarButton()
    {
        generateDarButton.click();
        return PageFactory.newInstance(DARPage.class);
    }

    @Step("Set Date")
    public GenerateDarForPage setDate(String date)
    {
        dateField.clear();
        dateField.sendKeys(date);
        dateField.sendKeys(Keys.TAB);
        return PageFactory.newInstance(GenerateDarForPage.class);
    }
}
