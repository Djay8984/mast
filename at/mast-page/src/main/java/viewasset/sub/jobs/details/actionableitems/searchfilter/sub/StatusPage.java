package viewasset.sub.jobs.details.actionableitems.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class StatusPage extends BasePage<StatusPage>
{

    @Visible
    @Name("Select all button")
    @FindBy(id = "codicil-search-filter-status-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear Button")
    @FindBy(id = "codicil-search-filter-status-clear-all")
    private WebElement clearButton;

    @Visible
    @Name("Status checkBoxes")
    @FindBy(css = ".checkbox-container:not(.grid-content)")
    private List<WebElement> StatusCheckBoxes;

    @Step("Click clear all button")
    public StatusPage clickClearAllButton()
    {
        clearButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click select all button")
    public StatusPage clickSelectAllButton()
    {
        selectAllButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

}
