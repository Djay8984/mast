package viewasset.sub.jobs.details.scope.addtojobscope.base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.RowElement;

import java.util.List;

public class BaseServiceTable extends HtmlElement
{

    //first row
    @FindBy(xpath = "./descendant::ul/li/div")
    private List<RowElement> rowElement;

    @Name("Family Name")
    @FindBy(css = "[data-ng-bind='family']")
    private WebElement familyName;

    @Step("Get Family Name")
    public String getFamilyName()
    {
        return familyName.getText().trim();
    }

    @Step("Get rows")
    public List<RowElement> getRows()
    {
        return rowElement;
    }
}
