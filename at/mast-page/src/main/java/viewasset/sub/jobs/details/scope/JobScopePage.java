package viewasset.sub.jobs.details.scope;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ActionableItemsElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.AssetNotesElements;
import viewasset.sub.jobs.details.scope.addtojobscope.element.ConditionsOfClassElements;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.ConfirmScopeModalWindow;
import viewasset.sub.jobs.details.scope.addtojobscope.modalwindow.base.ConfirmModalWindow;
import viewasset.sub.jobs.details.scope.element.JobScopeElements;

public class JobScopePage extends BasePage<JobScopePage>
{

    @Visible
    @Name("Add Services COC AIs Button")
    @FindBy(id = "edit-job-preview-cancel")
    private WebElement addServicesCocAisButton;

    @Name("Confirm job scope Button")
    @FindBy(css = "button[data-ng-click='vm.confirmJobScope()']")
    private WebElement confirmJobScopeButton;

    @Name("Job Scope Status")
    @FindBy(css = "[data-ng-if=\"!vm.isScopeEmpty\"]>div span.dark-blue.ng-scope")
    private WebElement jobScopeStatus;

    @Name("Confirmed Date")
    @FindBy(css = "[data-ng-bind='vm.scopeConfirmedDate | moment']")
    private List<WebElement> confirmedDate;

    @Name("Confirmed By")
    @FindBy(css = "[data-ng-bind='vm.scopeConfirmedBy']")
    private List<WebElement> confirmedBy;

    @Name("Conditions Of Class Elements")
    private List<ConditionsOfClassElements> cocItems;

    @Name("Actionable Item Elements")
    private List<ActionableItemsElements> actionableItems;

    @Name("Asset Notes Elements")
    private List<AssetNotesElements> assetNotesElements;

    @Name("Job Related Table")
    private JobGeneratedTable jobGeneratedTable;

    @Name("Condition Of Class Table")
    private ConditionOfClassTable conditionOfClassTable;
    @Name("Remove Selected Link")
    @FindBy(css = "h6 a[data-ng-click='vm.deleteSelected()']")
    private WebElement removeSelectedLink;
    @Name("Service removed confirmation element")
    @FindBy(css = "[data-ng-if='vm.isScopeEmpty']")
    private WebElement serviceRemovedConfirmationElement;
    @Name("Select All link")
    @FindBy(css = "[data-ng-click='vm.setSelectedValueForAll(1)']")
    private WebElement selectAllLink;
    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Get Job Generated Table")
    public JobGeneratedTable getJobGeneratedTable()
    {
        return jobGeneratedTable;
    }

    @Step("Get Condition Of Class Table")
    public ConditionOfClassTable getConditionOfClassTable()
    {
        return conditionOfClassTable;
    }

    @Step("Click Add Services COC AIs Button ")
    public AddToJobScopePage clickAddServicesCocAIsButton()
    {
        addServicesCocAisButton.click();
        waitForJavascriptFrameworkToFinish();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".button-group.segmented.service-group")));
        return PageFactory.newInstance(AddToJobScopePage.class);
    }

    @Step("Get COC node title by index")
    @Deprecated
    public String getCocNodeTitleByIndex(final int index)
    {
        return cocItems.get(index).getNodeTitle();
    }

    @Step("Select Coc checkbox by index")
    @Deprecated
    public JobScopePage selectCocCheckBoxByIndex(final int index)
    {
        AppHelper.scrollToBottom();
        cocItems.get(index).selectCheckBox();
        return this;
    }

    @Step("Get Job Scope status")
    public String getJobScopeStatus()
    {
        return jobScopeStatus.getText();
    }

    @Step("Check if confirmed date Present")
    public boolean isConfirmedDatePresent()
    {
        return confirmedDate.size() > 0;
    }

    @Step("Check if confirmedBy element Present")
    public boolean isConfirmedByPresent()
    {
        return confirmedBy.size() > 0;
    }

    @Step("Get Actionable Items node title by index")
    @Deprecated
    public String getActionableItemsNodeTitleByIndex(final int index)
    {
        return actionableItems.get(index).getNodeTitle();
    }

    @Step("Select Actionable Items checkbox by index")
    @Deprecated
    public JobScopePage selectActionableItemsCheckBoxByIndex(final int index)
    {
        AppHelper.scrollToBottom();
        actionableItems.get(index).selectCheckBox();
        return this;
    }

    @Step("Get Asset Notes node title by index")
    @Deprecated
    public String getAssetNotesNodeTitleByIndex(final int index)
    {
        return assetNotesElements.get(index).getNodeTitle();
    }

    @Step("Select Asset Notes checkbox by index")
    @Deprecated
    public JobScopePage selectAssetNotesCheckBoxByIndex(final int index)
    {
        AppHelper.scrollToBottom();
        assetNotesElements.get(index).selectCheckBox();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click on remove selected link")
    public ConfirmModalWindow clickRemoveSelectedLink()
    {
        AppHelper.scrollToBottom();
        wait.until(ExpectedConditions.elementToBeClickable(removeSelectedLink));
        removeSelectedLink.click();
        return PageFactory.newInstance(ConfirmModalWindow.class);
    }

    @Step("Get Service Removed ConfirmationModalPage Text ")
    public String getServiceRemovedConfirmationText()
    {
        return serviceRemovedConfirmationElement.getText().trim();
    }

    @Step("Is Coc Items present")
    public boolean isCocItemsPresent()
    {
        return cocItems.size() > 0;
    }

    @Step("Is Actionable Items present")
    public boolean isActionableItemsPresent()
    {
        return actionableItems.size() > 0;
    }

    @Step("Is Asset Notes present")
    public boolean isAssetNotesPresent()
    {
        return assetNotesElements.size() > 0;
    }

    @Step("Click Confirm Job Scope Button ")
    public ConfirmScopeModalWindow clickConfirmJobScopeButton()
    {
        AppHelper.scrollToBottom();
        confirmJobScopeButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ConfirmScopeModalWindow.class);
    }

    @Step("Click select all link")
    public JobScopePage clickSelectAllLink()
    {
        selectAllLink.click();
        return this;
    }

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return helper.PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Name("Job Generated Table")
    @FindBy(css = "[data-ng-include^=\"'app/asset/jobs/\"]>div:nth-child(3)")
    public class JobGeneratedTable extends HtmlElement
    {

        @Name("Job Generated Elements")
        @FindBy(css = "[data-ng-init=\"type='vm.jobGeneratedSurveys'\"]>div")

        private List<JobScopeElements> elements;

        @Step("Get All the elements in JobGenerated Elements")
        public List<JobScopeElements> getRows()
        {
            return elements;
        }
    }

    @Name("Condition Of Class Table")
    @FindBy(css = "[data-ng-if=\"!vm.isScopeEmpty\"]>div:nth-child(5)")
    public class ConditionOfClassTable extends HtmlElement
    {

        @Name("Condition Of Class Elements")
        @FindBy(css = "[data-ng-init=\"type='vm.wipCoCs'\"]>div")
        private List<JobScopeElements> elements;

        @Step("Get all elements in Condition of Class")
        public List<JobScopeElements> getRows()
        {
            return elements;
        }
    }

}
