package viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class Footer extends BasePage<Footer>
{

    @Name("Add Button")
    @FindBy(css = "[data-ng-click='vm.save()']")
    private WebElement addButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='vm.cancel()']")
    private WebElement cancelButton;

    /**
     * @param pageObjectClass - the expected class
     *                        JobSCopePage
     *                        ConfirmModalWindow
     * @param <T>
     * @return
     */
    @Step("Click on Add button")
    public <T extends BasePage<T>> T clickAddButton(Class<T> pageObjectClass)
    {
        addButton.click();
        return helper.PageFactory.newInstance(pageObjectClass);
    }
}
