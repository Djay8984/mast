package viewasset.sub.jobs.details.scope.addtojobscope;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import header.HeaderPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

public class AddToJobScopePage extends BasePage<AddToJobScopePage>
{

    @Visible
    @Name("All Tab")
    @FindBy(css = ".button-group.segmented.service-group li:nth-child(1)")
    private WebElement allTab;

    @Visible
    @Name("Service schedule Tab")
    @FindBy(css = ".button-group.segmented.service-group li:nth-child(2)")
    private WebElement serviceScheduleTab;

    @Visible
    @Name("Due and overdue Tab")
    @FindBy(css = ".button-group.segmented.service-group li:nth-child(3)")
    private WebElement dueAndOverdueTab;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return helper.PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Click on all tab")
    public ServiceAllPage clickAllTab()
    {
        allTab.click();
        return PageFactory.newInstance(ServiceAllPage.class);
    }

    @Step("Click on service schedule tab")
    public ServiceSchedulePage clickServiceScheduleTab()
    {
        serviceScheduleTab.click();
        return PageFactory.newInstance(ServiceSchedulePage.class);
    }

    @Step("Click on due and overdue tab")
    public DueAndOverduePage clickDueAndOverdueTab()
    {
        dueAndOverdueTab.click();
        return PageFactory.newInstance(DueAndOverduePage.class);
    }

    /**
     * Use this to get DueAndOverduePage when it is returned as default page to be displayed
     * after navigating from AddToJobScopePage
     *
     * @return DueAndOverduePage
     */
    @Step("Get Due and OverDueSubPage")
    public DueAndOverduePage getDueAndOverdueSubPage()
    {
        return PageFactory.newInstance(DueAndOverduePage.class);
    }

    @Step("Check that All Tab is selected")
    public boolean isAllTabSelected()
    {
        return allTab.findElements(By.className("ng-valid-parse")).size() > 0;
    }

    @Step("Check that Service Schedule Tab is selected")
    public boolean isServiceScheduleTabSelected()
    {
        return serviceScheduleTab.findElements(By.className("ng-valid-parse")).size() > 0;
    }

    @Step("Check that Due And Overdue Tab is selected")
    public boolean isDueAndOverdueTabSelected()
    {
        return dueAndOverdueTab.findElements(By.className("ng-valid-parse")).size() > 0;
    }

    @Step("Return Header Page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
