package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.modal.SubmitDsrPage;

public class DSRPage extends BasePage<DSRPage>
{

    @Name("Submit DSR Button")
    @FindBy(id = "submit-dar")
    private WebElement submitDsrButton;

    @Name("Cancel Button")
    @FindBy(id = "report-preview-cancel")
    private WebElement cancelButton;

    @Step("Click Submit DSR Button")
    public SubmitDsrPage clickSubmitDsrButton()
    {
        submitDsrButton.click();
        return PageFactory.newInstance(SubmitDsrPage.class);
    }

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
