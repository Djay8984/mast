package viewasset.sub.jobs.details.scope.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

import java.util.List;

@Name("Service Item Elements")
@FindBy(css = "[data-ng-repeat^='node in vm']")
public class ServiceItemElements extends HtmlElement
{

    @Visible
    @Name("check box")
    @FindBy(css = "[data-ng-model^='node.checkBoxMode'] span")
    private WebElement checkBox;

    @Visible
    @Name("Node title")
    @FindBy(css = "[data-ng-bind='node.title']")
    private WebElement nodeTitle;

    @Visible
    @Name("Further details arrow")
    @FindBy(css = ".glyph.float-right.grey")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Node Status")
    @FindBy(css = ".node-status span")
    private WebElement nodeStatus;

    @Name("Due Date")
    @FindBy(css = "[data-ng-bind^='node.dueDate']")
    private WebElement dueDate;

    @Name("Imposed Date")
    @FindBy(css = "[data-ng-bind^='node.imposedDate']")
    private WebElement imposedDate;

    @Name("Defect Node")
    @FindBy(css = "[data-ng-if='node.defect']")
    private List<WebElement> defectNode;

    @Step("Select check box")
    public ServiceItemElements selectCheckBox()
    {
        checkBox.click();
        return this;
    }

    @Step("Get node title")
    public String getNodeTitle()
    {
        return nodeTitle.getText().trim();
    }

    @Step("Get Node Status Text")
    public String getNodeStatusText()
    {
        return nodeStatus.getText().trim();
    }

    /**
     * Click on the clickFurtherDetailsArrow
     *
     * @param pageObjectClass- The pageObject which is expected to be returned
     *                         by clicking the futher details arrow
     *                         Possible PageObjects: ViewConditionOfClassPage.class
     *                         ViewActionableItemsPage.class
     *                         ViewAssetNotesPage
     * @param <T>The           implementing type
     */

    @Step("Click further details Arrow")
    public <T extends BasePage<T>> T clickFurtherDetailsArrow(Class<T> pageObjectClass)
    {
        AppHelper.scrollToBottom();
        furtherDetailsArrow.click();
        AppHelper.waitForOnScreenAnimation();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Get Due Date")
    public String getDueDate()
    {
        return dueDate.getText().trim();
    }

    @Step("Get Imposed Date")
    public String getImposedDate()
    {
        return imposedDate.getText().trim();
    }

    @Step("Is Defect Node present")
    public boolean isDefectNodePresent()
    {
        return defectNode.size() > 0;
    }
}
