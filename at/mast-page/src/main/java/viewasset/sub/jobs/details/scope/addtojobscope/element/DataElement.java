package viewasset.sub.jobs.details.scope.addtojobscope.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Deprecated
@Name("Data Elements")
@FindBy(css = "div.grid-block.no-border")
public class DataElement extends HtmlElement
{

    public final static String CHECKBOX_SELECTOR = ".checkbox-container.node-checkbox>button";
    private static final WebDriver driver = BaseTest.getDriver();
    private static final WebDriverWait wait = new WebDriverWait(driver, 10);
    @Visible
    @Name("check box")
    @FindBy(css = CHECKBOX_SELECTOR)
    private WebElement checkBox;

    @Name("Due Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.dueDate |  moment']")
    private WebElement dueDate;

    @Name("Lower Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.lowerRangeDate | moment']")
    private WebElement lowerRangeDate;

    @Name("Upper Range Date")
    @FindBy(css = "[data-ng-bind='service.scheduledService.upperRangeDate | moment']")
    private WebElement upperRangeDate;

    @Name("Service code")
    @FindBy(css = "[data-ng-bind='service.code']")
    private WebElement serviceCode;

    @Name("Service Name")
    @FindBy(css = "[data-ng-bind='service.name']")
    private WebElement serviceName;

    @Visible
    @Name("Further details arrow")
    @FindBy(css = ".glyph.float-right.grey")
    private WebElement furtherDetailsArrow;

    @Step("Select check box")
    public DataElement selectCheckBox()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        checkBox);
        if (!(checkBox.getAttribute("class").contains("checked")))
        {
            checkBox.click();
        }
        return this;
    }

    @Step("DeSelect check box")
    public DataElement deSelectCheckBox()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        checkBox);
        if (checkBox.getAttribute("class").contains("checked"))
        {
            checkBox.click();
        }
        return this;
    }

    @Step("Check if check box is selected")
    public boolean isCheckBoxSelected()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        checkBox);
        return checkBox.getAttribute("class").contains("checked");
    }

    @Step("Check if Service code is displayed")
    public boolean isServiceCodeDisplayed()
    {
        return serviceCode.isDisplayed();
    }

    @Step("Check if Service Name is displayed")
    public boolean isServiceNameDisplayed()
    {
        return serviceName.isDisplayed();
    }

    @Step("Check if Due date is displayed")
    public boolean isDueDateDisplayed()
    {
        return dueDate.isDisplayed();
    }

    @Step("Check if Lower Range Date is displayed")
    public boolean isLowerRangeDateDisplayed()
    {
        return lowerRangeDate.isDisplayed();
    }

    @Step("Check if Upper Range date is displayed")
    public boolean isUpperRangeDateDisplayed()
    {
        return upperRangeDate.isDisplayed();
    }

    @Step("Check For the condition of due ")
    public boolean isDueDateCondition()
    {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate localLowerRangeDate = LocalDate.parse(lowerRangeDate.getText(), formatter);
        LocalDate localUpperRangeDate = LocalDate.parse(lowerRangeDate.getText(), formatter);
        return (currentDate.isBefore(localUpperRangeDate) || currentDate.equals(localUpperRangeDate)) && (currentDate.isAfter(localLowerRangeDate));
    }

    @Step("Check for the condition of Over due")
    public boolean isOverDueDateCondition()
    {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate localUpperRangeDate = LocalDate.parse(lowerRangeDate.getText(), formatter);
        return (currentDate.isAfter(localUpperRangeDate));
    }
}
