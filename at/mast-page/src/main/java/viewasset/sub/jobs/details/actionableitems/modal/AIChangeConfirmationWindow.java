package viewasset.sub.jobs.details.actionableitems.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.actionableitems.ViewActionableItemPage;

public class AIChangeConfirmationWindow extends BasePage<AIChangeConfirmationWindow>
{

    @Visible
    @Name("Close Confirmation Window")
    @FindBy(id = "modal-discard-changes")
    private WebElement confirmationWindow;

    @Visible
    @Name("Go Back Button")
    @FindBy(css = ".transparent-button-white")
    private WebElement goBackButton;

    @Visible
    @Name("Delete Button")
    @FindBy(css = ".primary-button")
    private WebElement deleteButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".primary-button")
    private WebElement cancelButton;

    @Visible
    @Name("Request closure Button")
    @FindBy(css = ".primary-button")
    private WebElement requestClosureButton;

    @Visible
    @Name("Request change Button")
    @FindBy(css = ".primary-button")
    private WebElement requestChangeButton;

    @Step("Click Go Back Button")
    public ViewActionableItemPage clickGoBackButton()
    {
        goBackButton.click();
        return PageFactory.newInstance(ViewActionableItemPage.class);
    }

    @Step("Click Delete Button")
    public ActionableItemsPage clickDeleteButton()
    {
        deleteButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Cancel Button")
    public ActionableItemsPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Request closure button")
    public ActionableItemsPage clickRequestClosureButton()
    {
        requestClosureButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Request change button")
    public ActionableItemsPage clickRequestChangeButton()
    {
        requestChangeButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }
}
