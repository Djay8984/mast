package viewasset.sub.jobs.details.scope.addtojobscope.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.scope.JobScopePage;

public class ConfirmScopeModalWindow extends BasePage<ConfirmScopeModalWindow>
{

    @Visible
    @Name("Confirm Scope Button")
    @FindBy(css = "modal-footer.ng-scope .primary-button")
    private WebElement confirmScopeButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$close(false)']")
    private WebElement cancelButton;

    @Step("Click Cancel Button")
    public JobScopePage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(JobScopePage.class);
    }

    @Step("Click OK Button")
    public ViewJobDetailsPage clickConfirmScopeButton()
    {
        confirmScopeButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}
