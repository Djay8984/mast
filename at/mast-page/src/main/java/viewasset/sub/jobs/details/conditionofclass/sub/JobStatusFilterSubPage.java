package viewasset.sub.jobs.details.conditionofclass.sub;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class JobStatusFilterSubPage extends BasePage<JobStatusFilterSubPage>
{

    public static final String JOB_STATUS_FORM = "#job-search-filter-job-status-form";
    @Name("Job Search Filter Job Status Form")
    @FindBy(css = JOB_STATUS_FORM)
    private WebElement jobStatusForm;
    @Name("Job status Select All ")
    @FindBy(css = "#job-search-filter-job-status-select-all")
    private WebElement selectAll;
    @Name("Job status Clear ")
    @FindBy(css = "#job-search-filter-job-status-clear-all")
    private WebElement clear;
    @Name("Job Statuses CheckBoxes")
    @FindBy(css = ".checkbox-container")
    private List<WebElement> jobStatuses;

    @Step("Click Select All ")
    public JobStatusFilterSubPage clickSelectAll()
    {
        selectAll.click();
        return this;
    }

    @Step("Click Clear ")
    public JobStatusFilterSubPage clickClear()
    {
        clear.click();
        return this;
    }

    @Step("Selecting job status based on name")
    public JobStatusFilterSubPage selectJobStatus(String jobStatusName)
    {
        jobStatuses.stream()
                .filter(o -> o.findElement(By.cssSelector("label"))
                        .getText().trim().equals(jobStatusName))
                .findFirst()
                .orElse(null)
                .findElement(By.tagName("button"))
                .click();

        return this;
    }

    @Step("Verify Job Statuses Check boxes are selected")
    public boolean isJobStatusChecked(String text)
    {
        return jobStatuses.stream()
                .filter(e -> e.findElement(By.cssSelector("label")).getText().equals(text))
                .findFirst()
                .get()
                .findElement(By.tagName("button"))
                .getAttribute("class")
                .contains("checked");
    }

    @Step("Verify If All Job Statuses Check boxes are selected")
    public boolean isAllJobStatusChecked()
    {
        return jobStatuses.stream().allMatch(
                jobStatus -> jobStatus.findElement(By.tagName("button")).getAttribute("class").contains("checked")

        );
    }

    @Step("Verify If All Display Job Statuses can be check one by one")
    public void verifyIfAllDisplayJobStatusCheckedInvidually()
    {
        clickClear();
        for (WebElement jobStatus : jobStatuses)
        {
            jobStatus.findElement(By.tagName("button")).click();
            assert_().withFailureMessage(jobStatus.findElement(By.cssSelector("label")).getText() + " can be checked")
                    .that(jobStatus.findElement(By.tagName("button")).getAttribute("class").contains("checked"))
                    .isTrue();
        }
        return;

    }

    @Step("Retrieve all display job status name")
    public List<String> allDisplayJobStatusName()
    {
        return jobStatuses.stream()
                .map(jobStatus -> jobStatus.findElement(By.cssSelector("label")).getText())
                .collect(Collectors.toList());
    }

}
