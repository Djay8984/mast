package viewasset.sub.jobs.details.conditionofclass.sub;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.ClickClearHelper;

import java.util.List;

public class SDOFilterSubPage extends BasePage<SDOFilterSubPage>
{

    private final static String TYPEAHEAD_RESULTS = "ul[query='query'] li";
    @Name("SDO Input Field")
    @FindBy(id = "job-search-filter-sdo-name")
    private WebElement sdoInputField;
    @Name("Validation Error")
    @FindBy(id = "validation-message-surveyor-editable")
    private WebElement validationError;
    private List<WebElement> typeaheadResults;

    @Step("Returning the SDL Input Field")
    public String getSdoInputField()
    {
        return sdoInputField.getAttribute("value");
    }

    @Step("Setting the SDO Input Field")
    public void setSdoInputField(String input)
    {
        sdoInputField.clear();
        sdoInputField.sendKeys(input);
    }

    @Step("Returning whether the results are empty")
    public boolean isResultsEmpty()
    {
        setTypeaheadResults();
        return typeaheadResults.size() != 0 && typeaheadResults.get(0).getText().equals("No matches found");
    }

    @Step("Returning the amount of SDO search results")
    public int getSDOSearchResultCount()
    {
        setTypeaheadResults();
        return typeaheadResults.size();
    }

    @Step("Clicking an SDO search result by name")
    public void clickSDOResultByName(String name)
    {
        setTypeaheadResults();
        typeaheadResults
                .stream()
                .filter(s -> s.getText().equals(name))
                .findFirst()
                .orElse(null)
                .click();
    }

    @Step("Clicking an SDO search result by index")
    public void clickSDOResultByIndex(int index)
    {
        setTypeaheadResults();
        typeaheadResults.get(index).click();
    }

    @Step("Returning the placeholder text")
    public String getPlaceHolderText()
    {
        return sdoInputField.getAttribute("placeholder");
    }

    @Step("Clicking clear in the input field")
    public void clickClear()
    {
        ClickClearHelper.clickClearText(sdoInputField);
    }

    @Step("Returning the validation errors")
    public String getValidationErrors()
    {
        return validationError.getText();
    }

    @Step("Checking whether another search filter can be applied")
    public boolean isOtherSearchBlocked()
    {
        driver.findElement(By.cssSelector("[aria-label='Select Job status filter']")).click();
        return sdoInputField.isDisplayed();
    }

    private void setTypeaheadResults()
    {
        waitForJavascriptFrameworkToFinish();
        typeaheadResults = driver.findElements(By.cssSelector(TYPEAHEAD_RESULTS));
    }

}
