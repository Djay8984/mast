package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.actionableitems.modal.AIChangeConfirmationWindow;

import java.util.List;

public class ViewActionableItemPage extends BasePage<ViewActionableItemPage>
{

    @Visible
    @Name("Navigation back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Recommend closure button")
    @FindBy(id = "header-action-recommend-closure")
    private List<WebElement> recommendClosureButton;

    @Name("Recommend amendment button")
    @FindBy(id = "header-action-recommend-amendment")
    private List<WebElement> recommendAmendmentButton;

    @Name("Edit Button")
    @FindBy(id = "header-action-edit")
    private List<WebElement> editButton;

    @Name("Cancel Button")
    @FindBy(id = "header-action-cancel")
    private List<WebElement> cancelButton;

    @Name("Close Button")
    @FindBy(id = "header-action-close")
    private List<WebElement> closeButton;

    @Name("Description Text")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement descriptionText;

    @Step("Click Navigate Back Button")
    public ActionableItemsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Is Cancel Button Displayed")
    public boolean isCancelButtonDisplayed()
    {
        return cancelButton.size() > 0;
    }

    @Step("Is Close Button Displayed")
    public boolean isCloseButtonDisplayed()
    {
        return closeButton.size() > 0;
    }

    @Step("Is Edit Button Displayed")
    public boolean isEditButtonDisplayed()
    {
        return editButton.size() > 0;
    }

    @Step("Click Cancel Button")
    public AIChangeConfirmationWindow clickCancelButton()
    {
        cancelButton.get(0).click();
        return PageFactory.newInstance(AIChangeConfirmationWindow.class);
    }

    @Step("Click Close Button")
    public AIChangeConfirmationWindow clickCloseButton()
    {
        closeButton.get(0).click();
        return PageFactory.newInstance(AIChangeConfirmationWindow.class);
    }

    @Step("Is Recommend closure button displayed")
    public boolean isRecommendClosureButtonDisplayed()
    {
        return recommendClosureButton.size() > 0;
    }

    @Step("Is Recommend amendment button displayed")
    public boolean isRecommendAmendmentButtonDisplayed()
    {
        return recommendAmendmentButton.size() > 0;
    }

    @Step("Click Recommend closure button ")
    public AIChangeConfirmationWindow clickRecommendClosureButton()
    {
        recommendClosureButton.get(0).click();
        return PageFactory.newInstance(AIChangeConfirmationWindow.class);
    }

    @Step("Click Recommend Amendment Button")
    public AIChangeConfirmationWindow clickRecommendAmendmentButton()
    {
        recommendAmendmentButton.get(0).click();
        return PageFactory.newInstance(AIChangeConfirmationWindow.class);
    }

    @Step("Click Edit Button")
    public EditActionableItemPage clickEditButton()
    {
        editButton.get(0).click();
        return PageFactory.newInstance(EditActionableItemPage.class);
    }

    @Step("Is Edit button Enabled")
    public boolean isEditButtonEnabled()
    {
        return editButton.get(0).isEnabled();
    }

    @Step("Get Description")
    public String getDescription()
    {
        return descriptionText.getText().trim();
    }
}


