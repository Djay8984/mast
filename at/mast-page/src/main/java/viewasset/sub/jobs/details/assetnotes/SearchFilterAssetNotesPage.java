package viewasset.sub.jobs.details.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.conditionofclass.sub.CategoryFilterSubPage;

public class SearchFilterAssetNotesPage extends BasePage<SearchFilterAssetNotesPage>
{

    @Visible
    @Name("Category filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(1) .codicil-filter-button")
    private WebElement categoryFilterButton;

    @Visible
    @Name("Status filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(2) .codicil-filter-button")
    private WebElement statusFilterButton;

    @Visible
    @Name("Item type filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(3) .codicil-filter-button")
    private WebElement itemTypeFilterButton;

    @Visible
    @Name("SDO filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(1) .codicil-filter-button")
    private WebElement sdoFilterButton;

    @Visible
    @Name("Confidentiality filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(2) .codicil-filter-button")
    private WebElement confidentialityFilterButton;

    @Visible
    @Name("Search Button")
    @FindBy(id = "codicil-search-button")
    private WebElement searchButton;

    @Visible
    @Name("Reset Button")
    @FindBy(id = "codicil-search-reset")
    private WebElement resetButton;

    @Step("Click Category Filter Button")
    public CategoryFilterSubPage clickCategoryFilterButton()
    {
        categoryFilterButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(CategoryFilterSubPage.class);
    }

    @Step("Click search button")
    public AssetNotesPage clickSearchButton()
    {
        searchButton.click();
        return PageFactory.newInstance(AssetNotesPage.class);
    }

}
