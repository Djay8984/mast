package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.actionableitems.common.HeaderPage;

public class EditActionableItemPage extends BasePage<EditActionableItemPage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(id = "title")
    private WebElement titleTextBox;

    @Visible
    @Name("Start from a template button")
    @FindBy(css = "[data-ng-show='!vm.data.value'] [data-ng-click='vm.selectTemplate()']")
    private WebElement startFromTemplateButton;

    @Visible
    @Name("Class Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(1)")
    private WebElement classButton;

    @Visible
    @Name("Statutory Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(2)")
    private WebElement statutoryButton;

    @Visible
    @Name("External Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(3)")
    private WebElement externalButton;

    @Visible
    @Name("Description Text Box")
    @FindBy(id = "description")
    private WebElement descriptionTextBox;

    @Visible
    @Name("Surveyor guidance Text Box")
    @FindBy(id = "surveyor-guidance")
    private WebElement surveyorGuidanceTextBox;

    @Visible
    @Name("Imposed Date Text Box")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextBox;

    @Visible
    @Name("Due Date Text Box")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextBox;

    @Visible
    @Name("Visible for LR Internal Button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] li:nth-child(1)")
    private WebElement visibleForLrInternalButton;

    @Visible
    @Name("Visible for LR and Customer Button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] li:nth-child(2)")
    private WebElement visibleForLrCustomerButton;

    @Visible
    @Name("Visible for All Button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] li:nth-child(3)")
    private WebElement visibleForAllButton;

    @Visible
    @Name("Requires approval for change radio button")
    @FindBy(id = "require-approval")
    private WebElement requiresApprovalForChangeRadioButton;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "[id='tpl-'] .transparent-dark-button")
    private WebElement cancelButton;

    @Visible
    @Name("Save button")
    @FindBy(css = "[id='tpl-'] .primary-button")
    private WebElement saveButton;

    @Step("Is Visible for LR Internal button enabled")
    public boolean isVisibleForLrInternalButtonEnabled()
    {
        return visibleForLrInternalButton
                .findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is Visible for LR and Customer Button enabled")
    public boolean isVisibleForLrAndCustomerButtonEnabled()
    {
        return visibleForLrCustomerButton
                .findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is Visible for All Button enabled")
    public boolean isVisibleForAllButtonEnabled()
    {
        return visibleForAllButton
                .findElement(By.tagName("input")).isEnabled();
    }

    @Step("Set Description")
    public EditActionableItemPage setDescription(String description)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(description);
        return PageFactory.newInstance(EditActionableItemPage.class);
    }

    @Step("Click save button")
    public ViewActionableItemPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ViewActionableItemPage.class);
    }

    @Step("Get Header")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
