package viewasset.sub.jobs;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;

import java.util.List;

public class AssetNotePage extends BasePage<AssetNotePage>
{

    @Visible
    @Name("Navigate back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Visible
    @Name("Asset Note Name")
    @FindBy(css = "[data-ng-bind='vm.assetNote.title']")
    private WebElement assetNoteName;

    @Name("Edit Button")
    @FindBy(css = "[data-ui-sref='^.edit']")
    private List<WebElement> editButton;

    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='vm.cancelAssetNote()']")
    private List<WebElement> cancelButton;

    @Name("Close Button")
    @FindBy(css = "[data-ng-click='vm.deleteAssetNote()']")
    private List<WebElement> closeButton;

    @Step("Is edit button Displayed")
    public boolean isEditButtonDisplayed()
    {
        return editButton.size() > 0;
    }

    @Step("Is cancel button Displayed")
    public boolean isCancelButtonDisplayed()
    {
        return cancelButton.size() > 0;
    }

    @Step("Is close button Displayed")
    public boolean isCloseButtonDisplayed()
    {
        return closeButton.size() > 0;
    }

    @Step("Click Navigate Back Button")
    public AssetNotesPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(AssetNotesPage.class);
    }

    @Step("Get asset note name text")
    public String getAssetNoteNameText()
    {
        return assetNoteName.getText().trim();
    }
}
