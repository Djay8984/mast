package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.modal.DarFarDsrConfirmationPage;

public class FARPage extends BasePage<FARPage>
{

    @Name("Cancel Button")
    @FindBy(id = "report-preview-cancel")
    private WebElement cancelButton;

    @Name("Submit FAR Button")
    @FindBy(id = "submit-dar")
    private WebElement submitFarButton;

    @Visible
    @Name("Generate PDF Button")
    @FindBy(id = "report-generate-pdf-button")
    private WebElement generatePdfButton;

    @Step("Click Submit FAR Button")
    public DarFarDsrConfirmationPage clickSubmitFarButton()
    {
        submitFarButton.click();
        return PageFactory.newInstance(DarFarDsrConfirmationPage.class);
    }

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
