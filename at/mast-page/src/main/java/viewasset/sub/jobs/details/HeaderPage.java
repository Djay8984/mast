package viewasset.sub.jobs.details;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.jobs.JobsSubPage;

public class HeaderPage extends BasePage<HeaderPage>
{
    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Visible
    @Name("Job Status")
    @FindBy(css = "#view-job-status")
    private WebElement jobStatus;

    @Visible
    @Name("Attachment Button")
    @FindBy(css = ".attachment-button")
    private WebElement attachmentButton;

    @Visible
    @Name("Edit button")
    @FindBy(css = "[data-ui-sref='asset.jobs.edit({ jobId: vm.job.id })']")
    private WebElement editButton;

    @Visible
    @Name("Cancel job button")
    @FindBy(css = ".align-right.btn-container button:nth-child(6)")
    private WebElement cancelJobButton;

    @Visible
    @Name("Abort job button")
    @FindBy(css = ".align-right.btn-container button:nth-child(7)")
    private WebElement abortJobButton;

    @Visible
    @Name("Information Icon")
    @FindBy(css = ".circle-button")
    private WebElement informationIcon;

    @Name("Job ID Text")
    @FindBy(css = "[data-ng-bind^='vm.job.id']")
    private WebElement jobIdText;

    @Name("Cancel job information text")
    @FindBy(css = ".tooltip.arrowed-top p:nth-child(1)")
    private WebElement cancelJobInformationText;

    @Name("Abort job information text")
    @FindBy(css = ".tooltip.arrowed-top p:nth-child(2)")
    private WebElement abortJobInformationText;

    @Name("Service Type")
    @FindBy(css = "[data-ng-bind^='(vm.job.serviceType']")
    private WebElement serviceType;

    @Name("SDO Text")
    @FindBy(css = "[data-ng-bind='vm.jobSDO']")
    private WebElement sdoText;

    @Name("Location Text")
    @FindBy(css = "[data-ng-bind=\"vm.job.location || '-'\"]")
    private WebElement locationText;

    @Name("Start Date")
    @FindBy(css = "[data-ng-bind^='vm.job.startDate']")
    private WebElement startDate;

    @Name("ETA Date")
    @FindBy(css = "[data-ng-bind=\"vm.job.etaDate | moment: '-'\"]")
    private WebElement etaDate;

    @Name("ETA Date")
    @FindBy(css = "[data-ng-bind=\"vm.job.etdDate | moment: '-'\"]")
    private WebElement etdDate;

    @Name("Close Date")
    @FindBy(css = ".grid-block.job-summary div:nth-child(3) div:nth-child(2)")
    private WebElement closeDate;

    @Step("Navigate Back Button Jobs SubPage")
    public JobsSubPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Returning the job Status")
    public String getStatus()
    {
        return jobStatus.getText();
    }

    @Step("Click Attachment Button")
    public AttachmentsAndNotesPage clickAttachmentButton()
    {
        attachmentButton.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Is the edit button enabled")
    public boolean isEditButtonEnabled()
    {
        return editButton.isEnabled();
    }

    @Step("Click on the edit button")
    public EditJobPage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(EditJobPage.class);
    }

    @Step("Get Job ID")
    public String getJobId()
    {
        return jobIdText.getText().trim();
    }

    @Step("Get cancel job information text")
    public String getCancelJobInformationText()
    {
        new Actions(driver).moveToElement(informationIcon).perform();
        return cancelJobInformationText.getText().trim();
    }

    @Step("Get abort job information text")
    public String getAbortJobInformationText()
    {
        new Actions(driver).moveToElement(informationIcon).perform();
        return abortJobInformationText.getText().trim();
    }

    @Step("Get Service type value")
    public String getServiceTypeText()
    {
        return serviceType.getText().replace("-", "");
    }

    @Step("get SDO")
    public String getSdoText()
    {
        return sdoText.getText().trim();
    }

    @Step("get location Text")
    public String getLocation()
    {
        return locationText.getText();
    }

    @Step("get start date")
    public String getStartDate()
    {
        return startDate.getText();
    }

    @Step("get start date")
    public String getEtaDate()
    {
        return etaDate.getText();
    }

    @Step("get start date")
    public String getEtdDate()
    {
        return etdDate.getText();
    }

}
