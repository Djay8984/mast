package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;

public class LeaveConfirmationPage extends BasePage<LeaveConfirmationPage>
{
    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[ng-click='$close(false)']")
    private WebElement cancelButton;

    @Visible
    @Name("Save Changes Button")
    @FindBy(css = "[ng-click='$close('save')']")
    private WebElement saveChangesButton;

    @Visible
    @Name("Continue Without Saving Button")
    @FindBy(css = "[ng-click='$close('doDiscard')']")
    private WebElement discardChangesButton;

    @Step("Click Cancel Button")
    public CreditingPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(CreditingPage.class);
    }

    @Step("Click Save Changes Button")
    public ViewJobDetailsPage clickSaveChangesButton()
    {
        saveChangesButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Click Continue Without Saving Button")
    public ViewJobDetailsPage clickDiscardChangesButton()
    {
        discardChangesButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}
