package viewasset.sub.jobs.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

@Name("My Jobs element")
@FindBy(css = "[data-ng-repeat='currentJob in vm.otherJobs']")
public class MyJobsElements extends HtmlElement
{

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = "a[data-ng-click='vm.visit()']")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("My Job Number")
    @FindBy(css = "[data-ng-bind='vm.job.jobNumber']")
    private WebElement myJobNumber;

    @Step("Get My Job Number")
    public String getMyJobNumber()
    {
        return myJobNumber.getText();
    }

    @Step("Click on Further Details Arrow")
    public ViewJobDetailsPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
