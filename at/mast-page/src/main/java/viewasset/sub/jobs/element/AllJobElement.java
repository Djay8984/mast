package viewasset.sub.jobs.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

@Name("All Job DataElement")
@FindBy(css = "[data-ng-repeat='currentJob in vm.otherJobs']")
public class AllJobElement extends HtmlElement
{

    @Name("Job Number")
    @Visible
    @FindBy(css = "[data-ng-bind~='vm.job.id']")
    private WebElement jobNumberText;

    @Name("Job Status")
    @Visible
    @FindBy(css = "[data-ng-bind~='vm.jobStatus()']")
    private WebElement jobStatusText;

    @Name("Job Office")
    @Visible
    @FindBy(css = "div:nth-child(2) > div:nth-child(1) > strong")
    private WebElement jobOfficeText;

    @Name("Job Service Type")
    @Visible
    @FindBy(css = "div:nth-child(2) > div:nth-child(2) > strong")
    private WebElement jobServiceTypeText;

    @Name("Job ETA")
    @Visible
    @FindBy(css = "[data-ng-bind~='vm.job.etdDate']")
    private WebElement jobETAText;

    @Name("Job ETD")
    @Visible
    @FindBy(css = "[data-ng-bind~='vm.job.etaDate']")
    private WebElement jobETDText;

    @Name("Navigate to job specific page button")
    @Visible
    @FindBy(css = " [data-ng-click='vm.visit()']")
    private WebElement navigateToJobArrowButton;

    @Step("Returning Job Number Text value")
    public String getJobNumber()
    {
        return jobNumberText.getText();
    }

    @Step("Returning Job Status value")
    public String getJobStatus()
    {
        return jobStatusText.getText();
    }

    @Step("Returning Job Office value")
    public String getJobOffice()
    {
        return jobOfficeText.getText();
    }

    @Step("Returning Job Service Type value")
    public String getJobServiceType()
    {
        return jobServiceTypeText.getText();
    }

    @Step("Returning Job ETA value")
    public String getJobETA()
    {
        return jobETAText.getText();
    }

    @Step("Returning Job ETD value")
    public String getJobETD()
    {
        return jobETDText.getText();
    }

    @Step("Click on navigate to job page button")
    public ViewJobDetailsPage clickNavigateToJobArrowButton()
    {
        navigateToJobArrowButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Check that job number is displayed")
    public boolean isJobNumberDisplayed()
    {
        return jobNumberText.isDisplayed();
    }

    @Step("Check that job status is displayed")
    public boolean isJobStatusDisplayed()
    {
        return jobStatusText.isDisplayed();
    }

    @Step("Check that job office is displayed")
    public boolean isJobOfficeDisplayed()
    {
        return jobOfficeText.isDisplayed();
    }

    @Step("Check that job service type is displayed")
    public boolean isJobServiceTypeDisplayed()
    {
        return jobServiceTypeText.isDisplayed();
    }

    @Step("Check that job ETA is displayed")
    public boolean isJobETADisplayed()
    {
        return jobETAText.isDisplayed();
    }

    @Step("Check that job ETD is displayed")
    public boolean isJobETDDisplayed()
    {
        return jobETDText.isDisplayed();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
