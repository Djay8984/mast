package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;

import java.util.List;

public class ConfirmEditChangesPage extends BasePage<ConfirmEditChangesPage>
{

    @Visible
    @Name("Cancel button")
    @FindBy(css = "[ng-click='$close(false)']")
    private WebElement cancelButton;

    @Visible
    @Name("Save Changes Button")
    @FindBy(css = "[ng-click='$close('save')']")
    private WebElement saveChangesButton;

    @Visible
    @Name("Discard Changes Button")
    @FindBy(css = "[ng-click='$close('doDiscard')']")
    private WebElement discardChangesButton;

    @Name("Leave Page modal window")
    @FindBy(id = "modal-confirm-navigation")
    private List<WebElement> leavePageModalWindow;

    @Step("Click cancel button")
    public CreditingPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(CreditingPage.class);
    }

    @Step("Click save changes button")
    public ViewJobDetailsPage clickSaveChangesButton()
    {
        saveChangesButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Click discard changes button")
    public ViewJobDetailsPage clickDiscardChangesButton()
    {
        discardChangesButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Is Leave Page Model Window Present")
    public boolean isLeavePageModelWindowPresent()
    {
        return leavePageModalWindow.size() > 0;
    }
}
