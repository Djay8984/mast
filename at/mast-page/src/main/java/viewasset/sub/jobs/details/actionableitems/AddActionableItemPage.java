package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.TextArea;
import util.AppHelper;
import viewasset.sub.jobs.details.actionableitems.common.HeaderPage;
import viewasset.sub.jobs.details.actionableitems.modal.ConfirmAddedActionableItemWindow;

import java.util.List;

public class AddActionableItemPage extends BasePage<AddActionableItemPage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(id = "title")
    private WebElement titleTextBox;

    @Name("Start from a template button")
    @FindBy(css = "[data-ng-show='!vm.data.value'] [data-ng-click='vm.selectTemplate()']")
    private WebElement startFromTemplateButton;

    @Visible
    @Name("Class Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(1)")
    private WebElement classButton;

    @Visible
    @Name("Statutory Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(2)")
    private WebElement statutoryButton;

    @Visible
    @Name("External Button")
    @FindBy(css = "[id='tpl-category.id'] li:nth-child(3)")
    private WebElement externalButton;

    @Visible
    @Name("Description Text Box")
    @FindBy(id = "description")
    private TextArea descriptionTextBox;

    @Visible
    @Name("Surveyor guidance Text Box")
    @FindBy(id = "surveyor-guidance")
    private TextArea surveyorGuidanceTextBox;

    @Visible
    @Name("Imposed Date Text Box")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextBox;

    @Visible
    @Name("Due Date Text Box")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextBox;

    @Visible
    @Name("Visible for LR Internal Button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] li:nth-child(1)")
    private WebElement visibleForLrInternalButton;

    @Visible
    @Name("Visible for All Button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] li:nth-child(3)")
    private WebElement visibleForAllButton;

    @Name("Requires approval for change radio button")
    @FindBy(id = "require-approval")
    private WebElement requiresApprovalForChangeRadioButton;

    @Visible
    @Name("Cancel button")
    @FindBy(css = "[id='tpl-'] .transparent-dark-button")
    private WebElement cancelButton;

    @Visible
    @Name("Save button")
    @FindBy(css = "[id='tpl-'] .primary-button")
    private WebElement saveButton;

    @Name("Select Item Button")
    @FindBy(css = "[data-ng-if='!vm.data.selected.length']#actionable-item-item")
    private WebElement selectItemButton;

    @Name("Change Selection Button")
    @FindBy(css = "[data-ng-if='vm.data.values.length']#actionable-item-item")
    private List<WebElement> changeSelectionButton;

    @Name("Error Message")
    @FindBy(css = "[data-ng-if='errorCtrl.showMessage']")
    private WebElement errorMessage;

    @Name("Selected Item")
    @FindBy(css = "#tpl-assetItem [data-ng-bind='item.name']")
    private List<WebElement> selectedItem;

    @Name("Remove Item Icon")
    @FindBy(css = "[data-ng-click='vm.removeItem(item)']")
    private List<WebElement> removeItemIcon;

    @Step("Set Imposed Date")
    public AddActionableItemPage setImposedDate(String imposedDate)
    {
        imposedDateTextBox.clear();
        imposedDateTextBox.sendKeys(imposedDate);
        imposedDateTextBox.sendKeys(Keys.TAB);
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Set Due Date")
    public AddActionableItemPage setDueDate(String dueDate)
    {
        dueDateTextBox.clear();
        dueDateTextBox.sendKeys(dueDate);
        dueDateTextBox.sendKeys(Keys.TAB);
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click Visible for All button")
    public AddActionableItemPage clickVisibleForAllButton()
    {
        AppHelper.scrollToBottom();
        visibleForAllButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click Save Button")
    public ConfirmAddedActionableItemWindow clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ConfirmAddedActionableItemWindow.class);
    }

    @Step("Click Class Category button")
    public AddActionableItemPage clickClassCategoryButton()
    {
        classButton.click();
        return helper.PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Select required Approval for changes check box")
    public AddActionableItemPage selectRequiredApprovalCheckBox()
    {
        requiresApprovalForChangeRadioButton.click();
        return helper.PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get Title")
    public String getTitle()
    {
        return titleTextBox.getAttribute("value").trim();
    }

    @Step("Set Title")
    public AddActionableItemPage setTitle(String title)
    {
        titleTextBox.clear();
        titleTextBox.sendKeys(title);
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get Description")
    public String getDescription()
    {
        return descriptionTextBox.getAttribute("value").trim();
    }

    @Step("Set Description")
    public AddActionableItemPage setDescription(String description)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(description);
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get Surveyor guidance")
    public String getSurveyorGuidance()
    {
        return surveyorGuidanceTextBox.getAttribute("value").trim();
    }

    @Step("Set Surveyor guidance")
    public AddActionableItemPage setSurveyorGuidance(String surveyorGuidance)
    {
        surveyorGuidanceTextBox.clear();
        surveyorGuidanceTextBox.sendKeys(surveyorGuidance);
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get title field helper text")
    public String getTitleFieldHelperText()
    {
        return titleTextBox.getAttribute("placeholder").trim();
    }

    @Step("Get description field helper text")
    public String getDescriptionFieldHelperText()
    {
        return descriptionTextBox.getAttribute("placeholder").trim();
    }

    @Step("Get Surveyor guidance field helper text")
    public String getSurveyorGuidanceFieldHelperText()
    {
        return surveyorGuidanceTextBox.getAttribute("placeholder").trim();
    }

    @Step("Is Required Approval CheckBox Selected")
    public boolean isRequiredApprovalCheckBoxSelected()
    {
        return requiresApprovalForChangeRadioButton
                .getAttribute("class")
                .contains("checked");
    }

    @Step("Get Description Field Dimension")
    public Dimension getDescriptionFieldDimension()
    {
        return descriptionTextBox.getSize();
    }

    @Step("Get Surveyor Guidance Field Dimension")
    public Dimension getSurveyorGuidanceFieldDimension()
    {
        return surveyorGuidanceTextBox.getSize();
    }

    @Step("Resize Description Text box")
    public AddActionableItemPage setDescriptionTextBoxSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END).perform();
        descriptionTextBox.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Resize  Surveyor Guidance Text box")
    public AddActionableItemPage setSurveyorGuidanceTextBoxSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END).perform();
        surveyorGuidanceTextBox.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Is Save Button Enabled")
    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }

    @Step("Get Error Message")
    public String getErrorMessage()
    {
        return errorMessage.getText().trim();
    }

    @Step("see if the error icon is displayed in Title Text Box")
    public boolean isTitleTextBoxErrorIconDisplayed(Type type)
    {
        return titleTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in Description Text Box")
    public boolean isDescriptionTextBoxErrorIconDisplayed(Type type)
    {
        return descriptionTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in Imposed Date Text Box")
    public boolean isImposedDateTextBoxErrorIconDisplayed(Type type)
    {
        return imposedDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in Due Date Text Box")
    public boolean isDueDateTextBoxErrorIconDisplayed(Type type)
    {
        return dueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Click Start from Template button")
    public ActionableItemTemplatePage clickStartFromTemplateButton()
    {
        startFromTemplateButton.click();
        return PageFactory.newInstance(ActionableItemTemplatePage.class);
    }

    @Step("Click Select Item Button")
    public SelectAssetItemPage clickSelectItemButton()
    {
        selectItemButton.click();
        return PageFactory.newInstance(SelectAssetItemPage.class);
    }

    @Step("Get Selected Item Text")
    public String getSelectedItemText()
    {
        return selectedItem.get(0).getText().trim();
    }

    @Step("Is Selected Item Displayed")
    public boolean isSelectedItemDisplayed()
    {
        return selectedItem.size() > 0;
    }

    @Step("Click Remove Item Icon")
    public AddActionableItemPage clickRemoveItemIcon()
    {
        removeItemIcon.get(0).click();
        return this;
    }

    @Step("Is Remove Item Icon displayed")
    public boolean isRemoveItemIconDisplayed()
    {
        return removeItemIcon.size() > 0;
    }

    @Step("Is Change Selection Button is expected to be displayed")
    public boolean isChangeSelectionButtonDisplayed()
    {
        return changeSelectionButton.size() > 0;
    }

    @Step("Click Change Selection Button ")
    public SelectAssetItemPage clickChangeSelectionButton()
    {
        changeSelectionButton.get(0).click();
        return PageFactory.newInstance(SelectAssetItemPage.class);
    }

    @Step("Get Header")
    public HeaderPage getHeaderPage()
    {
        return helper.PageFactory.newInstance(HeaderPage.class);
    }

    public enum Type
    {
        ERROR("images/red_warning.png");
        private final String image;

        Type(String image)
        {
            this.image = image;
        }
    }

}

