package viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.aicocan;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.scope.addtojobscope.AddToJobScopePage;

public class Header extends BasePage<Header>
{

    @Visible
    @Name("back button")
    @FindBy(css = ".back-button")
    private WebElement backButton;

    @Visible
    @Name("Item Status")
    @FindBy(css = ".status.ng-binding")
    private WebElement itemStatus;

    @Step("Click back button")
    public AddToJobScopePage clickNavigateBackButton()
    {
        backButton.click();
        return PageFactory.newInstance(AddToJobScopePage.class);
    }
}
