package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.modal.DarFarDsrConfirmationPage;

public class DARPage extends BasePage<DARPage>
{

    @Name("Notes Text Box")
    @FindBy(id = "notes")
    private WebElement notesTextBox;

    @Name("Confidential Notes Text Box")
    @FindBy(id = "confidentialNotes")
    private WebElement confidentialNotesTextBox;

    @Name("Cancel Button")
    @FindBy(id = "report-preview-cancel")
    private WebElement cancelButton;

    @Name("Submit DAR Button")
    @FindBy(id = "submit-dar")
    private WebElement submitDarButton;

    @Visible
    @Name("Generate PDF Button")
    @FindBy(id = "report-generate-pdf-button")
    private WebElement generatePdfButton;

    @Step("Click Submit DAR Button")
    public DarFarDsrConfirmationPage clickSubmitDarButton()
    {
        submitDarButton.click();
        return PageFactory.newInstance(DarFarDsrConfirmationPage.class);
    }

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

}
