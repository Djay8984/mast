package viewasset.sub.jobs.details.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

public class ApproveDSRPage extends BasePage<ApproveDSRPage>
{

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "cancel-modal-action")
    private WebElement cancelButton;

    @Visible
    @Name("Approve DSR Button")
    @FindBy(id = "approve-dsr-modal-action")
    private WebElement approveDsrButton;

    @Step("Click Approve DSR Button")
    public ViewJobDetailsPage clickApproveDSRButton()
    {
        approveDsrButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}
