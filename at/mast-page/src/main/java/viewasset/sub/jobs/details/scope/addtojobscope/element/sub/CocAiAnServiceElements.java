package viewasset.sub.jobs.details.scope.addtojobscope.element.sub;

import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.element.ServiceItemElements;

import java.util.List;

@Deprecated
@Name("COC AI and AN Service Elements")
@FindBy(css = "[data-ng-include^=\"'app/asset/jobs\"]>.asset-model-hierarchy")
public class CocAiAnServiceElements extends HtmlElement
{

    @Name("Expand Icon")
    @FindBy(css = ".node-expand-toggle")
    private WebElement expandIcon;

    @Name("collapsible Icon")
    @FindBy(css = "[data-ng-if='vm.jobCocs.length && vm.jobCocs.expanded']")
    private List<WebElement> collapsibleIcon;

    @Name("Job ScopeService Type Text")
    @FindBy(id = "job-scope-type")
    private WebElement jobScopeServiceTypeText;

    @Name("Service Item Elements")
    private List<ServiceItemElements> serviceItemElements;

    @Step("Click on expand icon")
    public CocAiAnServiceElements clickExpandIcon()
    {
        WebDriver driver = BaseTest.getDriver();
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        expandIcon);
        expandIcon.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions
                .visibilityOfElementLocated(By.cssSelector("#job-scope-node.is-selected")));
        return this;
    }

    @Step("Is service item expanded ")
    public boolean isServiceItemExpanded()
    {
        return collapsibleIcon.size() == 0;
    }

    @Step("Get job scope service type text")
    public String getJobScopeServiceTypeText()
    {
        return jobScopeServiceTypeText.getText().trim();
    }

    @Step("Get All Service Item Elements")
    public List<ServiceItemElements> getServiceItemElements()
    {
        return serviceItemElements;
    }
}
