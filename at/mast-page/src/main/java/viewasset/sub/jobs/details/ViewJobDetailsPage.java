package viewasset.sub.jobs.details;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.jobs.details.actionableitems.ActionableItemsPage;
import viewasset.sub.jobs.details.assetnotes.AssetNotesPage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import viewasset.sub.jobs.details.crediting.CreditingPage;
import viewasset.sub.jobs.details.crediting.DSRPage;
import viewasset.sub.jobs.details.defects.DefectsPage;
import viewasset.sub.jobs.details.modal.ApproveDSRPage;
import viewasset.sub.jobs.details.team.AddOrEditJobTeamPage;
import viewasset.sub.jobs.details.updatesandreporting.FinalSurveyReportPage;

public class ViewJobDetailsPage extends BasePage<ViewJobDetailsPage>
{

    final String GOTO_CREDITING_BUTTON_CSS = "[data-ui-sref='crediting.manage({ jobId: vm.job.id " +
            "})']";
    @Name("Add/ go to job team button")
    @FindBy(css = "[data-ui-sref='asset.jobs.team({ jobId: vm.job.id })']")
    private WebElement addOrGoToJobTeamButton;

    @Visible
    @Name("Go to Cocs Button")
    @FindBy(css = "[data-ui-sref^='crediting.cocs.list']")
    private WebElement goToCocsButton;

    @Visible
    @Name("Go to asset notes button")
    @FindBy(css = "[data-ui-sref^='crediting.assetNotes.list']")
    private WebElement goToAssetNotesButton;

    @Visible
    @Name("Go to defects button")
    @FindBy(css = "[data-ui-sref^='crediting.defects.list']")
    private WebElement goToDefectsButton;

    @Visible
    @Name("Go to Actionable Items button")
    @FindBy(css = "[data-ui-sref^='crediting.actionableItems.list']")
    private WebElement goToActionableItemsButton;

    @Name("Go to Edit button")
    @FindBy(id = "view-job-edit")
    private WebElement editJobButton;

    @Visible
    @Name("Job Scope Section")
    @FindBy(css = ".job-section-content div:nth-child(1) h4")
    private WebElement jobScopeSection;

    @Name("Job Scope Text")
    @FindBy(css = ".job-section-content span")
    private WebElement jobScopeText;

    @Visible
    @Name("Job Team Section")
    @FindBy(css = ".job-section-content div:nth-child(2) h4")
    private WebElement jobTeamSection;

    @Visible
    @Name("Job updates & reporting block header")
    @FindBy(css = ".grid-block.vertical.view-job div:nth-child(3) h3")
    private WebElement jobUpdatesReportingBlockHeader;

    @Visible
    @Name("Technical Review block header")
    @FindBy(css = ".grid-block.vertical.view-job div:nth-child(4) h3")
    private WebElement technicalReviewBlockHeader;

    @Visible
    @Name("Endorsement block header")
    @FindBy(css = ".grid-block.vertical.view-job div:nth-child(5) h3")
    private WebElement endorsementBlockHeader;

    @Name("Go to job scope button")
    @FindBy(css = ".secondary-button.right.ng-scope")
    private WebElement addOrGoToJobScopeButton;

    @Name("Job Scope confirmation Text")
    @FindBy(css = ".job-section-content div:nth-child(1) span")
    private WebElement jobScopeConfirmationText;

    @Name("Generate DSR Button")
    @FindBy(css = "[data-ng-click='vm.generateDSR()']")
    private WebElement generateDsrButton;

    @Name("Approve Technical review Button")
    @FindBy(css = ".tr-approve-button")
    private WebElement approveTechnicalReviewButton;

    @Name("Job progress cards")
    @FindBy(css = "[job='vm.job'] job-report-item")
    private List<WebElement> jobProgressCards;

    @Name("Go to crediting button")
    @FindBy(css = GOTO_CREDITING_BUTTON_CSS)
    private WebElement goToCreditingButton;

    @Name("Add P17 Button")
    @FindBy(css = "[data-ng-show='vm.job.scopeConfirmed'] button")
    private WebElement addToP17Button;

    @Name("Percentage Completion Icon")
    @FindBy(css = "[data-ng-if='vm.jobScopeConfirmed'] .circle-button.percentage")
    private List<WebElement> percentageCompletionIcon;

    @Name("Service type code")
    @FindBy(css = "[data-ng-bind^='serviceType.code']")
    private List<WebElement> serviceTypeCodeText;

    @Name("DSR Status")
    @FindBy(css = "[data-ng-if='vm.latestDSRPending'] span[data-ng-if]")
    private WebElement dsrStatus;

    @Name("FSR Status")
    @FindBy(css = "[data-ng-if='vm.latestFSR'] span[data-ng-if]")
    private WebElement fsrStatus;

    @Name("Technical Review Status")
    @FindBy(css = "[data-ng-if='vm.latestDSR'] span[data-ng-if*='vm.item']")
    private WebElement technicalReviewStatus;

    @Name("DSR Card")
    @FindBy(css = "[data-ng-if='vm.latestDSRPending']")
    private List<WebElement> draftSurveyReport;

    @Name("Final Survey Report")
    @FindBy(css = "[data-ng-if='vm.latestFSR']")
    private List<WebElement> finalSurveyReport;

    @Name("View Actions Button")
    @FindBy(css = "a.secondary-button")
    private WebElement viewActionsButton;

    @Name("Close Job Button")
    @FindBy(css = "button.primary-button")
    private WebElement closeJobButton;

    @Name("Confirm Close Job Button")
    @FindBy(css = "button#close-job-modal-action.ng-binding.ng-scope.primary-button")
    private WebElement confirmCloseJobButton;

    @Name("Job Closed Tick")
    @FindBy(css = "i.svg-icon-Tick")
    private List<WebElement> closedJobTick;

    @Name("Job Status")
    @FindBy(css = "div.ng-binding")
    private WebElement jobStatus;

    @Step("Get header")
    public HeaderPage getHeader()
    {
        return helper.PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Click on the AddOrGoto Job Team button")
    public AddOrEditJobTeamPage clickAddOrGoToJobTeamButton()
    {
        addOrGoToJobTeamButton.click();
        return PageFactory.newInstance(AddOrEditJobTeamPage.class);
    }

    /**
     * @param pageObjectClass JobScopePage or AddToJobScopePage
     * @param <T>
     * @return JobScopePage or AddToJobScopePage
     */
    @Step("Click on the Add Or GoTo JobScope button")
    public <T extends BasePage<T>> T clickAddOrGoToJobScopeButton(final Class<T> pageObjectClass)
    {
        addOrGoToJobScopeButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Getting the text from the job team button")
    public String getJobTeamButtonText()
    {
        return addOrGoToJobTeamButton.getText();
    }

    @Step("Click Go to Cocs Button")
    public ConditionsOfClassPage clickGoToCocsButton()
    {
        goToCocsButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Click Go to Cocs Button")
    public ActionableItemsPage clickGoToActionableItemsButton()
    {
        goToActionableItemsButton.click();
        return PageFactory.newInstance(ActionableItemsPage.class);
    }

    @Step("Click Go to Cocs Button")
    public DefectsPage clickGoToDefectsButton()
    {
        goToDefectsButton.click();
        return PageFactory.newInstance(DefectsPage.class);
    }

    @Step("Click on Go To Asset Notes Button")
    public AssetNotesPage clickGoToAssetNotesButton()
    {
        goToAssetNotesButton.click();
        return PageFactory.newInstance(AssetNotesPage.class);
    }

    @Step("Get Job Scope Header Text")
    public String getJobScopeHeaderText()
    {
        return jobScopeSection.getText().trim();
    }

    @Step("Get Job Team Header Text")
    public String getJobTeamHeaderText()
    {
        return jobTeamSection.getText().trim();
    }

    @Step("Get Job updates & reporting Header Text")
    public String getJobUpdatesReportingHeaderText()
    {
        return jobUpdatesReportingBlockHeader.getText().trim();
    }

    @Step("Get Technical review header Text")
    public String getTechnicalReviewHeaderText()
    {
        return technicalReviewBlockHeader.getText().trim();
    }

    @Step("Get Endorsement header Text")
    public String getEndorsementHeaderText()
    {
        return endorsementBlockHeader.getText().trim();
    }

    @Step("Get job scope text")
    public String getJobScopeText()
    {
        return jobScopeText.getText().trim();
    }

    @Step("Is Go to crediting button clickable")
    public boolean isGoToCreditingButtonEnabled()
    {
        return goToCreditingButton.isEnabled();
    }

    @Step("is Add To P17 button displayed")
    public boolean isAddToP17ButtonDisplayed()
    {
        return addToP17Button.isDisplayed();
    }

    @Step("Get Job Scope ConfirmationModalPage Text")
    public String getJobScopeConfirmationText()
    {
        return jobScopeConfirmationText.getText().trim();
    }

    @Step("Click Go to crediting button ")
    public CreditingPage clickGoToCreditingButton()
    {
        goToCreditingButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(GOTO_CREDITING_BUTTON_CSS)));
        return PageFactory.newInstance(CreditingPage.class);
    }

    @Step("Is Percentage completed is present")
    public boolean isPercentageCompleteIconPresent()
    {
        return percentageCompletionIcon.size() > 0;
    }

    @Step("Get Service Code Text")
    public String getServiceCodeText()
    {
        waitForJavascriptFrameworkToFinish();
        return serviceTypeCodeText.get(0).getText().trim();
    }

    @Step("is service code present")
    public boolean isServiceCodePresent()
    {
        return serviceTypeCodeText.size() > 0;
    }

    @Step("Get Job Scope Button Text")
    public String getTextOfAddOrGoToJobScopeButton()
    {
        return addOrGoToJobScopeButton.getText();
    }

    @Step("Click DSR Button")
    public DSRPage clickGenerateDsrButton()
    {
        generateDsrButton.click();
        return PageFactory.newInstance(DSRPage.class);
    }

    @Step("Click approve technical review button")
    public ApproveDSRPage clickApproveTechnicalReviewButton()
    {
        approveTechnicalReviewButton.click();
        return PageFactory.newInstance(ApproveDSRPage.class);
    }

    @Step("Get DSR Status")
    public String getDSRStatus()
    {
        return dsrStatus.getText().trim();
    }

    @Step("Get FSR Status")
    public String getFSRStatus()
    {
        return fsrStatus.getText().trim();
    }

    @Step("Get Technical Review Status")
    public String getTechnicalReviewStatus()
    {
        return technicalReviewStatus.getText().trim();
    }

    @Step("Get Job In Progress Card Title")
    public List<String> getJobInProgressCardsTitle()
    {
        AppHelper.scrollToBottom();
        return jobProgressCards
                .stream()
                .map(e -> e.findElement(By.cssSelector(".title"))
                        .getText())
                .collect(Collectors.toList());
    }

    @Step("Is DSR Card Displayed")
    public boolean isDsrCardDisplayed()
    {
        return draftSurveyReport.size() > 0;
    }

    @Step("Click DSR Card")
    public DSRPage clickDsrCard()
    {
        draftSurveyReport.get(0).click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(DSRPage.class);
    }

    @Step("Click on the Edit Job button")
    public EditJobPage clickEditJobButton()
    {
        this.editJobButton.click();
        return PageFactory.newInstance(EditJobPage.class);
    }

    @Step("Is Final Survey Report Displayed")
    public boolean isFinalSurveyReport()
    {
        return finalSurveyReport.size() > 0;
    }

    @Step("Click final survey report ")
    public FinalSurveyReportPage clickFinalSurveyReport()
    {
        AppHelper.scrollToBottom();
        finalSurveyReport.get(0).click();
        return PageFactory.newInstance(FinalSurveyReportPage.class);
    }

    @Step("Click View Actions Button")
    public <T extends BasePage<T>> T clickViewActionsButton(final Class<T> pageObjectClass)
    {
        viewActionsButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Close Job Button")
    public ViewJobDetailsPage clickCloseJobButton()
    {
        if ((closeJobButton.isDisplayed()))
        {
            closeJobButton.click();
        }
        return this;
    }

    @Step("Click Confirm Close Job Button")
    public <T extends BasePage<T>> T clickConfirmCloseJobButton(final Class<T> pageObjectClass)
    {
        confirmCloseJobButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    public ViewJobDetailsPage clickConfirmCloseJobButton()
    {
        AppHelper.scrollToBottom();
        confirmCloseJobButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Get Closed Job Tick")
    public boolean getClosedJobTick()
    {
        return closedJobTick.size() > 0;
    }

    @Step("Get Job Status")
    public String getJobStatus()
    {
        return jobStatus.getText();
    }
}
