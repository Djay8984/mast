package viewasset.sub.jobs.details.conditionofclass.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;

@Name("Data Elements")
@FindBy(css = "[item-type='vm.itemType']")
public class DataElement extends HtmlElement
{

    @Visible
    @Name("cocText")
    @FindBy(css = ".codicil-title")
    private WebElement cocText;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Item Status Name")
    @FindBy(css = "[data-ng-bind='vm.item.status.name']")
    private WebElement itemStatus;

    @Step("Get Coc ID")
    public String getCocId()
    {
        return cocText.getText().replaceAll("[^\\d]", "");
    }

    @Step("Get coc title")
    public String getCocTitle()
    {
        return cocText.getText().replaceAll("\\d+$", "").trim();
    }

    @Step("Get Item Status")
    public String getItemStatus()
    {
        return itemStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public ViewConditionOfClassPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }
}
