package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class SubmitDsrPage extends BasePage<SubmitDsrPage>
{

    @Visible
    @Name("Submit DSR Button")
    @FindBy(id = "submit-dsr-modal-action")
    private WebElement submitDsrButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "cancel-modal-action")
    private WebElement cancelButton;

    @Step("Click Submit DSR Button")
    public DarFarDsrConfirmationPage clickSubmitDsrButton()
    {
        submitDsrButton.click();
        return PageFactory.newInstance(DarFarDsrConfirmationPage.class);
    }
}
