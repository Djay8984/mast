package viewasset.sub.jobs.details.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.assetnotes.elements.AssetNoteElement;

import java.util.List;

public class AssetNotesPage extends BasePage<AssetNotesPage>
{

    @Visible
    @Name("Search/Filter Asset Notes Button")
    @FindBy(css = "button[data-ng-click='vm.searchMode = !vm.searchMode']")
    private WebElement searchFilterAssetNotesButton;

    @Name("Add New Button")
    @FindBy(id = "add-attributes-button")
    private WebElement addNewButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("No Items found")
    @FindBy(css = ".grid-block.background-pure-white.codicil-no-result.ng-scope")
    private WebElement errorMessage;

    @Visible
    @Name("Job related Section Header")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    private WebElement jobRelatedSectionHeader;

    @Visible
    @Name("Others Section Header")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    private WebElement othersSectionHeader;

    @Name("Job Related Table")
    private JobRelatedTable jobRelatedTable;

    @Name("Other Table")
    private OtherTable otherTable;

    @Step("Get Job Related Table")
    public JobRelatedTable getJobRelatedTable()
    {
        return jobRelatedTable;
    }

    @Step("Get Other Table")
    public OtherTable getOtherTable()
    {
        return otherTable;
    }

    @Step("Click search filter asset notes button")
    public SearchFilterAssetNotesPage clickSearchFilterAssetNotesButton()
    {
        searchFilterAssetNotesButton.click();
        return PageFactory.newInstance(SearchFilterAssetNotesPage.class);
    }

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return helper.PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Check If items are displayed")
    public String getErrorMessage()
    {
        return errorMessage.getText();
    }

    @Name("Asset Note Elements in Job Related Table")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    public class JobRelatedTable extends HtmlElement
    {

        @Name("All Asset Notes element in Job Table")
        @FindBy(css = "[data-ng-repeat='codicil in vm.jobCodicils'] [data-ng-switch='vm.itemType']")
        private List<AssetNoteElement> assetNotes;

        @Step("Get All Asset Notes element in Job Table")
        public List<AssetNoteElement> getRows()
        {
            return assetNotes;
        }
    }

    @Name("Asset Note Elements in Other Table")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    public class OtherTable extends HtmlElement
    {

        @Name("All Asset Notes element in Other Table")
        @FindBy(css = "[data-ng-repeat='codicil in vm.otherCodicils'] [data-ng-switch='vm.itemType']")
        private List<AssetNoteElement> assetNotes;

        @Step("Get All Asset in Other Table")
        public List<AssetNoteElement> getRows()
        {
            return assetNotes;
        }

    }

}
