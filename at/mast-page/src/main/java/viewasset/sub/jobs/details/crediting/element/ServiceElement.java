package viewasset.sub.jobs.details.crediting.element;

import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.RadioButton;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.jobs.details.crediting.EditSurveyPage;

import java.util.List;

public class ServiceElement extends HtmlElement
{

    @Name("Plus/Expand button")
    @FindBy(css = "[data-ng-click^='survey.expanded'],[data-ng-click*='node.expanded']")
    private WebElement plusButton;

    @Name("Edit Survey Icon")
    @FindBy(css = "[data-ng-click=\"vm.editStart(survey, 'survey')\"]")
    private WebElement editSurveyIcon;

    @Name("Cancel edit survey icon")
    @FindBy(css = "[data-ng-click='vm.editCancel(survey)']")
    private WebElement cancelEditSurveyIcon;

    @Name("Part held/P Radio button")
    @FindBy(css = ".node-credit div:nth-child(1)>div label")
    private RadioButton partHeldRadioButton;

    @Name("Complete/X Radio button")
    @FindBy(css = ".node-credit div:nth-child(2)>div")
    private RadioButton completeRadioButton;

    @Name("Not Started/NS Radio button")
    @FindBy(css = ".node-credit div:nth-child(3)>div")
    private RadioButton notStartedRadioButton;

    @Name("Task Number")
    @FindBy(css = "[data-ng-bind='node.taskNumber']")
    private WebElement taskNumber;

    @Name("Service Type Text")
    @FindBy(css = "[data-ng-bind='survey.name']")
    private WebElement serviceTypeText;

    @Name("Attachments Icon")
    @FindBy(css = "[data-attachments='survey.attachments']")
    private WebElement attachmentsIcon;

    @Name("Children elements")
    //must use xpath which allows current node reference via ./ and absolute pathing to avoid
    // selecting grandchildren
    @FindBy(xpath = "./following-sibling::div/ol/li/div")
    private List<ServiceElement> children;

    @Step("Click plus button")
    public ServiceElement clickPlusButton()
    {
        plusButton.click();
        return this;
    }

    @Step("Get Service type text")
    public String getServiceType()
    {
        return serviceTypeText.getText().trim();
    }

    @Step("Click part help/P radio button")
    public ServiceElement clickPartHeldRadioButton()
    {
        partHeldRadioButton.click();
        return this;
    }

    @Step("Click Not Started/NS radio button")
    public ServiceElement clickNotStartRadioButton()
    {
        notStartedRadioButton.click();
        return this;
    }

    @Step("Click Complete/X Radio button")
    public ServiceElement clickCompleteRadioButton()
    {
        completeRadioButton.click();
        return this;
    }

    public EditSurveyPage clickEditSurveyIcon()
    {
        editSurveyIcon.click();
        return PageFactory.newInstance(EditSurveyPage.class);
    }

    @Step("Is Part held/P Radio button selected")
    public boolean isPartHeldRadioButtonSelected()
    {
        return partHeldRadioButton.isSelected();
    }

    @Step("Is Complete/X Radio button selected")
    public boolean isCompleteRadioButtonSelected()
    {
        return completeRadioButton.isSelected();
    }

    @Step("Is Part Not Started/NS button selected")
    public boolean isNotStartedRadioButtonSelected()
    {
        return notStartedRadioButton.isSelected();
    }

    @Step("Click Attachments Icon")
    public AttachmentsAndNotesPage clickAttachmentIcon()
    {
        attachmentsIcon.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Get Service Elements")
    public List<ServiceElement> getChildren()
    {
        return children;
    }

    @Step("Get Task Number")
    public String getTaskNumber()
    {
        return taskNumber.getText().trim();
    }
}
