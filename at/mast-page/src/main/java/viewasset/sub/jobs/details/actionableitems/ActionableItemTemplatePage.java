package viewasset.sub.jobs.details.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ActionableItemTemplatePage extends BasePage<ActionableItemTemplatePage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(css = "[data-ng-model='data.search.title']")
    private WebElement titleTextBox;

    @Visible
    @Name("Item Type Text Box")
    @FindBy(id = "item-type-search")
    private WebElement itemTypeTextBox;

    @Visible
    @Name("Live Button")
    @FindBy(css = ".segmented.light li:nth-child(1)")
    private WebElement liveButton;

    @Visible
    @Name("All Button")
    @FindBy(css = ".segmented.light li:nth-child(2)")
    private WebElement allButton;

    @Visible
    @Name("Find Template Button")
    @FindBy(css = "[data-ng-click='vm.findTemplate()'")
    private WebElement findTemplateButton;

    @Visible
    @Name("X- Button")
    @FindBy(id = "close-button")
    private WebElement xButton;

    @Step("Click X button")
    public AddActionableItemPage clickXButton()
    {
        xButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }
}
