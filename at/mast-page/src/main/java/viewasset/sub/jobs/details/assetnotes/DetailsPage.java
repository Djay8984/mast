package viewasset.sub.jobs.details.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;

import java.util.List;

public class DetailsPage extends BasePage<DetailsPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("NotesAndAttachmentsIcon")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement notesAndAttachmentsIcon;

    @Name("Asset Note Name")
    @FindBy(css = "[data-ng-bind='vm.assetNote.title']")
    private WebElement assetNoteName;

    @Name("Edit Button")
    @FindBy(css = "[data-ui-sref='^.edit']")
    private List<WebElement> editButton;

    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='vm.cancelAssetNote()']")
    private List<WebElement> cancelButton;

    @Name("Close Button")
    @FindBy(css = "[data-ng-click='vm.deleteAssetNote()']")
    private List<WebElement> closeButton;

    @Step("Navigate Back Button Jobs SubPage")
    public AssetNotesPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(AssetNotesPage.class);
    }

    @Step("Click Notes And Attachment Icon")
    public AttachmentsAndNotesPage clickNotesAndAttachmentIcon()
    {
        notesAndAttachmentsIcon.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Is edit button Displayed")
    public boolean isEditButtonPresent()
    {
        return editButton.size() > 0;
    }

    @Step("Is cancel button Displayed")
    public boolean isCancelButtonPresent()
    {
        return cancelButton.size() > 0;
    }

    @Step("Is close button Displayed")
    public boolean isCloseButtonPresent()
    {
        return closeButton.size() > 0;
    }

    @Step("Get asset note name text")
    public String getAssetNoteNameText()
    {
        return assetNoteName.getText().trim();
    }
}
