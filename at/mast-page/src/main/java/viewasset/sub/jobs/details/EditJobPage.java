package viewasset.sub.jobs.details;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import util.ClickClearHelper;
import viewasset.sub.jobs.JobsSubPage;

public class EditJobPage extends BasePage<EditJobPage>
{

    @Visible
    @Name("SDO text field")
    @FindBy(id = "edit-job-sdo")
    private WebElement SdoTextField;

    @Visible
    @Name("Case drop down")
    @FindBy(id = "edit-job-case")
    private WebElement caseDropDown;

    @Visible
    @Name("Location text field")
    @FindBy(id = "edit-job-location")
    private WebElement locationTextField;

    @Visible
    @Name("Description text field")
    @FindBy(id = "edit-job-description")
    private WebElement descriptionTextField;

    @Visible
    @Name("ETA text field")
    @FindBy(id = "edit-job-eta")
    private WebElement etaTextField;

    @Visible
    @Name("ETD text field")
    @FindBy(id = "edit-job-etd")
    private WebElement etdTextField;

    @Visible
    @Name("Requested attendance text field")
    @FindBy(id = "edit-job-rad")
    private WebElement requestedAttendanceTextField;

    @Visible
    @Name("Cancel button")
    @FindBy(id = "edit-job-preview-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Save button")
    @FindBy(id = "edit-job-preview-save")
    private WebElement saveButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Navigate Back Button Jobs SubPage")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Is the SDO text field enabled")
    public boolean isSdoTextFieldEnabled()
    {
        return SdoTextField.isEnabled();
    }

    @Step("Returning the value of the SDO")
    public String getSdo()
    {
        return SdoTextField.getAttribute("value");
    }

    @Step("Enter an SDO into the text field")
    public void enterSDO(final String sdo)
    {
        ClickClearHelper.clickClearText(SdoTextField);
        SdoTextField.sendKeys(sdo + Keys.ENTER);
    }

    @Step("Returning the value in case")
    public String getCase()
    {
        return caseDropDown.getAttribute("value");
    }

    @Step("Select a new case")
    public void selectCase(final String aCase)
    {
        final Select caseSelect = new Select(caseDropDown);
        caseSelect.selectByVisibleText(aCase);
    }

    @Step("Returning the location")
    public String getLocation()
    {
        return locationTextField.getAttribute("value");
    }

    @Step("Enter a new location")
    public void enterLocation(final String location)
    {
        locationTextField.clear();
        locationTextField.sendKeys(location);
    }

    @Step("Returning the description")
    public String getDescription()
    {
        return descriptionTextField.getAttribute("value");
    }

    @Step("Enter a new description")
    public EditJobPage enterDescription(final String description)
    {
        descriptionTextField.clear();
        descriptionTextField.sendKeys(description);
        return this;
    }

    @Step("Returning the ETA date")
    public String getEtaDate()
    {
        return etaTextField.getAttribute("value");
    }

    @Step("Enter a new ETA date")
    public EditJobPage enterEtaDate(final String etaDate)
    {
        etaTextField.clear();
        etaTextField.sendKeys(etaDate);
        return this;
    }

    @Step("Returning the ETD date")
    public String getEtdDate()
    {
        return etdTextField.getAttribute("value");
    }

    @Step("Enter a new ETD date")
    public EditJobPage enterEtdDate(final String etdDate)
    {
        etdTextField.clear();
        etdTextField.sendKeys(etdDate);
        return this;
    }

    @Step("Returning the requested attendance date")
    public String getRequestedAttendanceDate()
    {
        return requestedAttendanceTextField.getAttribute("value");
    }

    @Step("Enter a new requested attendance date")
    public EditJobPage enterRequestedAttendanceDate(final String date)
    {
        requestedAttendanceTextField.clear();
        requestedAttendanceTextField.sendKeys(date);
        return this;
    }

    @Step("Click the cancel button")
    public JobsSubPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(JobsSubPage.class);
    }

    @Step("Is the save button enabled")
    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }

    @Step("Click the save button")
    public ViewJobDetailsPage clickSaveButton()
    {
        AppHelper.waitForOnScreenAnimation();
        saveButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Step("Click the save button expecting staleness")
    public void clickSaveButtonExpectingStaleness()
    {
        saveButton.click();
    }
}
