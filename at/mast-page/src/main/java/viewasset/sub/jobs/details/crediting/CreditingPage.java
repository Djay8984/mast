package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.HeaderPage;
import viewasset.sub.jobs.details.crediting.base.BaseInfoTable;
import viewasset.sub.jobs.details.crediting.base.BaseServiceTable;
import viewasset.sub.jobs.details.crediting.modal.GenerateDarForPage;
import viewasset.sub.jobs.details.crediting.modal.GenerateFarForPage;

import java.util.List;

public class CreditingPage extends BasePage<CreditingPage>
{

    @Visible
    @Name("Save Button")
    @FindBy(id = "crediting-save")
    private WebElement saveButton;

    @Visible
    @Name("Generate DAR Button")
    @FindBy(css = "[data-ng-click='vm.generateDAR()']")
    private WebElement generateDarButton;

    @Visible
    @Name("Generate FAR Button")
    @FindBy(css = "[data-ng-click='vm.generateFAR()']")
    private WebElement generateFarButton;

    @Visible
    @Name("Navigate Back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    // this css selector must be the element 2nd before the immediate parent to the row selectors
    // this is because the row selectors will use the intermediatory to grab immediate child and
    // ignore grandchildren
    @Name("Service Table")
    @FindBy(css = "form[name='vm.creditingForm'] [data-ng-if='vm.hasSurveys(group)']")
    private List<ServiceTable> serviceTable;

    @Name("Coc table")
    @FindBy(css = "[data-ng-show='vm.cocs.length']")
    private CocsTable cocsTable;

    @Name("Actionable items table")
    @FindBy(css = "[data-ng-show='vm.actionableItems.length']")
    private ActionableItemsTable actionableItemsTable;

    @Step("Get Header")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Get classification table")
    public BaseServiceTable getClassificationTable()
    {
        return serviceTable.stream().filter(a -> a.getGroupName().equals("Classification")).findFirst().get();
    }

    @Step("Get statutory table")
    public BaseServiceTable getStatutoryTable()
    {
        return serviceTable.stream().filter(a -> a.getGroupName().equals("Statutory")).findFirst().get();
    }

    @Step("Get Cocs table")
    public CocsTable getCocsTable()
    {
        return cocsTable;
    }

    @Step("Get Actionable items table")
    public ActionableItemsTable getActionableItemsTable()
    {
        return actionableItemsTable;
    }

    @Step("Click Save Button")
    public CreditingPage clickSaveButton()
    {
        saveButton.click();
        return this;
    }

    /**
     * @param expectedReturnPage
     * @param <T>
     * @return possible returns - ViewJobDetailsPage
     * - ConfirmEditChangesPage
     */
    @Step("Navigate Back to the expected Page")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> expectedReturnPage)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(expectedReturnPage);
    }

    @Step("Click Generate DAR Button")
    public GenerateDarForPage clickGenerateDarButton()
    {
        generateDarButton.click();
        return PageFactory.newInstance(GenerateDarForPage.class);
    }

    @Step("Click Generate FAR Button")
    public GenerateFarForPage clickGenerateFarButton()
    {
        generateFarButton.click();
        return PageFactory.newInstance(GenerateFarForPage.class);
    }

    public class ServiceTable extends BaseServiceTable
    {
    }

    public class CocsTable extends BaseInfoTable
    {
    }

    public class ActionableItemsTable extends BaseInfoTable
    {
    }
}
