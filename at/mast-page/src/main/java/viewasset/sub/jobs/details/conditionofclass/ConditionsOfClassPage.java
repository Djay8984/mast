package viewasset.sub.jobs.details.conditionofclass;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.jobs.details.ViewJobDetailsPage;
import viewasset.sub.jobs.details.conditionofclass.base.BaseTable;
import viewasset.sub.jobs.details.conditionofclass.searchfilter.ConditionOfClassPage;
import viewasset.sub.jobs.element.JobRelatedElements;
import viewasset.sub.jobs.element.OtherElements;

import java.util.List;

public class ConditionsOfClassPage extends BasePage<ConditionsOfClassPage>
{

    @Visible
    @Name("Coc Add New Button")
    @FindBy(id = "add-attributes-button")
    private Button addNewButton;

    @Visible
    @Name("Search/Filter codicils and defect button")
    @FindBy(css = "[data-ng-click='vm.searchMode = !vm.searchMode']")
    private WebElement searchFilterConditionsOfClassButton;

    @Name("Job related Section Header")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    private List<WebElement> jobRelatedSectionHeader;

    @Name("Others Section Header")
    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    private List<WebElement> othersSectionHeader;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Deprecated
    @Name("Job Coc cards")
    private List<JobRelatedElements> jobCocCards;

    @Deprecated
    @Name("Others Coc cards")
    private List<OtherElements> otherCocCards;

    @Name("Job related Table")
    private JobRelatedTable jobRelatedTable;

    @Name("Others Table")
    private OthersTable othersTable;

    @Step("Get Asset Note Table")
    public JobRelatedTable getJobRelatedTable()
    {
        return jobRelatedTable;
    }

    @Step("Get Others Table")
    public OthersTable getOthersTable()
    {
        return othersTable;
    }

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }

    @Deprecated
    @Step("Returning an Job Related Coc card by Item Status")
    public JobRelatedElements getFirstJobRelatedCoc(String status)
    {
        return jobCocCards.stream().filter(e -> e.getItemStatus().equals(status))
                .findFirst().get();
    }

    @Deprecated
    @Step("Get All Job Related Coc Count")
    public int getJobRelatedCocCount()
    {
        return jobCocCards.size();
    }

    @Deprecated
    @Step("Get All Others Coc Count")
    public int getOthersCocCount()
    {
        return otherCocCards.size();
    }

    @Step("Click Add New Button")
    public AddConditionOfClassPage clickAddNewButton()
    {
        addNewButton.click();
        return PageFactory.newInstance(AddConditionOfClassPage.class);
    }

    @Step("Click search Filter Conditions Of Class Button")
    public ConditionOfClassPage clickSearchFilterConditionsOfClassButton()
    {
        searchFilterConditionsOfClassButton.click();
        return PageFactory.newInstance(ConditionOfClassPage.class);
    }

    @Step("Is Job Related Section Header Displayed")
    public boolean isJobRelatedSectionHeaderDisplayed()
    {
        return jobRelatedSectionHeader.size() > 0;
    }

    @Step("Is Other section Section Header Displayed")
    public boolean isOthersSectionHeaderDisplayed()
    {
        return othersSectionHeader.size() > 0;
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'job'\"]")
    public class OthersTable extends BaseTable
    {
    }

    @FindBy(css = "[data-ng-if=\"vm.viewType !== 'other'\"]")
    public class JobRelatedTable extends BaseTable
    {
    }

}
