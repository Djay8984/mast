package viewasset.sub.jobs.details.scope.addtojobscope.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

import java.util.List;

public class RowElement extends HtmlElement
{
    @Name("Children")
    //subsequent service rows - not expecting more children, can terminate with descendant
    @FindBy(xpath = "./following-sibling::div[@data-ng-if='value.expanded']/descendant::div[@class='dark-blue']")
    private List<RowElement> children;

    @Name("Service Elements")
    //not expecting more children, can terminate with //
    @FindBy(xpath = "./following-sibling::ol//div[contains(@class,'job-scope-hierarchy-node')]")
    private List<ServiceElement> serviceElements;

    @Name("+/- Icon")
    @FindBy(css = "[class='node-expand-toggle']")
    private WebElement plusOrMinusIcon;

    @Step("Click on expand icon")
    public RowElement clickPlusOrMinusIcon()
    {
        AppHelper.scrollToBottom();
        plusOrMinusIcon.click();
        return this;
    }

    @Step("Get children")
    public List<RowElement> getChildren()
    {
        return children;
    }

    @Step("Get Services Rows")
    public List<ServiceElement> getServiceRows()
    {
        return serviceElements;
    }
}
