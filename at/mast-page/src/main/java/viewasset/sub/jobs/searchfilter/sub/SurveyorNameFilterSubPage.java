package viewasset.sub.jobs.searchfilter.sub;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.ClickClearHelper;

import java.util.List;

public class SurveyorNameFilterSubPage extends BasePage<SurveyorNameFilterSubPage>
{

    private final static String TYPEAHEAD_RESULTS = "ul[query='query'] li";
    @Name("Surveyors name Label")
    @FindBy(css = "[for=job-search-filter-surveyor-name]")
    private WebElement surveyorsNameLabel;
    @Name("Surveyors name input Field")
    @FindBy(id = "job-search-filter-surveyor-name")
    private WebElement surveyorsNameInputField;
    @Name("Surveyors name input Field With Typeaheaed Clearable")
    @FindBy(css = ".typeahead-clearable")
    private WebElement surveyorsNameInputFieldWithTypeaheaedClearable;
    @Name("Validation Error")
    @FindBy(id = "validation-message-surveyor-editable")
    private WebElement validationError;

    @Step("Click on Surveyors name label")
    public void clickSurveyorsnameLabel()
    {
        surveyorsNameLabel.click();
    }

    @Step("Returning the Surveyors name Input Field")
    public String getSurveyorsnameInputField()
    {
        return surveyorsNameInputField.getAttribute("value");
    }

    @Step("Setting the Surveyors name Input Field")
    public void setSurveyorsnameInputField(String input)
    {
        surveyorsNameInputField.clear();
        surveyorsNameInputField.sendKeys(input);
    }

    @Step("Verify if Surveyors Name Input Field With Typeaheaed Clearable is displayed")
    public boolean IsSurveyorsNameInputFieldWithTypeaheaedClearableIsDisplayed()
    {
        return surveyorsNameInputFieldWithTypeaheaedClearable.isDisplayed();
    }

    @Step("Verify if Surveyors Name Input Field With Typeaheaed Clearable is enable for edit")
    public boolean IsSurveyorsNameInputFieldWithTypeaheaedClearableIsEnabled()
    {
        return surveyorsNameInputFieldWithTypeaheaedClearable.isEnabled();
    }

    @Step("Returning Typeahead Results")
    public List<WebElement> getTypeaheadResults()
    {
        waitForJavascriptFrameworkToFinish();
        return driver.findElements(By.cssSelector(TYPEAHEAD_RESULTS));
    }

    @Step("Returning whether the results are empty")
    public boolean isResultsEmpty()
    {
        return getTypeaheadResults().size() != 0 && getTypeaheadResults().get(0).getText().equals("No matches");
    }

    @Step("Returning the amount of Surveyors name search results")
    public int getSurveyorNameSearchResultCount()
    {
        return getTypeaheadResults().size();
    }

    @Step("Clicking a SurveyorName search result by name")
    public void clickSurveyorNameResultByName(String name)
    {
        getTypeaheadResults()
                .stream()
                .filter(s -> s.getText().equals(name))
                .findFirst()
                .orElse(null)
                .click();
    }

    @Step("Returning the placeholder text")
    public String getPlaceHolderText()
    {
        return surveyorsNameInputField.getAttribute("placeholder");
    }

    @Step("Returning the validation errors")
    public String getValidationErrors()
    {
        return validationError.getText();
    }

    @Step("Clicking clear in the input field")
    public void clickClear()
    {
        ClickClearHelper.clickClearText(surveyorsNameInputField);
    }

    @Step("Checking whether another search filter can be applied")
    public boolean isOtherSearchBlocked()
    {
        driver.findElement(By.cssSelector("[aria-label='Select Job status filter']")).click();
        return surveyorsNameLabel.isDisplayed();
    }
}
