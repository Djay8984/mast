package viewasset.sub.jobs.details.scope.addtojobscope.headersandfooters.scope;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.scope.addtojobscope.DueAndOverduePage;

public class Header extends BasePage<Header>
{
    @Visible
    @Name("Select All Link")
    @FindBy(css = "[data-ng-click='vm.setSelectedStateForAllServices(1)']")
    private WebElement selectAllLink;

    @Visible
    @Name("Clear All Link")
    @FindBy(css = "[data-ng-click='vm.setSelectedStateForAllServices(0)']")
    private WebElement clearAllLink;

    @Step("Click Select All Link")
    public DueAndOverduePage clickSelectAllLink()
    {
        selectAllLink.click();
        return PageFactory.newInstance(DueAndOverduePage.class);
    }

    @Step("Click Select All Link")
    public DueAndOverduePage clickClearAllLink()
    {
        clearAllLink.click();
        return PageFactory.newInstance(DueAndOverduePage.class);
    }

}
