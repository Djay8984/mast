package viewasset.sub.jobs.details.conditionofclass.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;

public class AddCocConfirmationPage extends BasePage<AddCocConfirmationPage>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "[data-ng-click='$close(action.result)']")
    private WebElement okButton;

    @Step("Click OK button")
    public ConditionsOfClassPage clickOkButton()
    {
        okButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }
}
