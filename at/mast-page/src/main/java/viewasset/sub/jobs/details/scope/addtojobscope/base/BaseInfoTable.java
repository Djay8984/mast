package viewasset.sub.jobs.details.scope.addtojobscope.base;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.scope.addtojobscope.element.CocAinAnElement;

import java.util.List;

public class BaseInfoTable extends HtmlElement
{
    @Name("Info elements")
    @FindBy(xpath = "./descendant::div[@id='job-scope-node']")
    private List<CocAinAnElement> cocAinAnElements;

    @Step("Get rows")
    public List<CocAinAnElement> getRows()
    {
        return cocAinAnElements;
    }
}
