package viewasset.sub.jobs.details.defects.defectcategory.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import util.AppHelper;
import viewasset.sub.jobs.details.defects.AddDefectPage;
import viewasset.sub.jobs.details.defects.defectcategory.element.ModalCheckboxElement;

import java.util.List;

public class DefectTypePage extends BasePage<DefectTypePage>
{
    @Visible
    @Name("Close Button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Name("Select Button")
    @FindBy(css = "modal-footer.ng-scope button.primary-button")
    private Button selectButton;

    @Name("Cancel Button")
    @FindBy(css = "modal-footer.ng-scope button.transparent-button-white")
    private Button cancelButton;

    @Name("Pollution Type List")
    private List<ModalCheckboxElement> defectCheckBoxes;

    @Step("Select check box by name")
    public DefectTypePage selectCheckBoxByName(String name)
    {
        defectCheckBoxes.stream();
        return this;
    }

    @Step("Click Select Button")
    public AddDefectPage clickSelectButton()
    {
        AppHelper.scrollToBottom();
        selectButton.click();
        return PageFactory.newInstance(AddDefectPage.class);
    }

    @Step("Click Cancel Button")
    public AddDefectPage clickCancelButton()
    {
        AppHelper.scrollToBottom();
        cancelButton.click();
        return PageFactory.newInstance(AddDefectPage.class);
    }
}
