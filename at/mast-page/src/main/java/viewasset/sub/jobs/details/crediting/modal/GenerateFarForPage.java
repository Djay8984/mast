package viewasset.sub.jobs.details.crediting.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.crediting.FARPage;

public class GenerateFarForPage extends BasePage<GenerateFarForPage>
{

    @Visible
    @Name("Date Field")
    @FindBy(id = "last-visit-date")
    private WebElement dateField;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$dismiss()']")
    private WebElement cancelButton;

    @Visible
    @Name("Generate FAR Button")
    @FindBy(css = "[data-ng-click='vm.saveFAR()']")
    private WebElement generateFarButton;

    @Step("Set Date")
    public GenerateFarForPage setDate(String date)
    {
        dateField.clear();
        dateField.sendKeys(date);
        dateField.sendKeys(Keys.TAB);
        return PageFactory.newInstance(GenerateFarForPage.class);
    }

    @Step("Click Generate FAR Button")
    public FARPage clickGenerateFarButton()
    {
        generateFarButton.click();
        waitForJavascriptFrameworkToFinish();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-ng-click='vm.submit()']")));
        return PageFactory.newInstance(FARPage.class);
    }
}
