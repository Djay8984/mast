package viewasset.sub.jobs.details.defects.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.jobs.details.defects.ViewDefectPage;

@Name("Data Elements")
@FindBy(css = "[item-type='vm.itemType']")
public class DataElement extends HtmlElement
{

    @Visible
    @Name("Title Text")
    @FindBy(css = ".codicil-title")
    private WebElement titleText;

    @Visible
    @Name("Further details arrow")
    @FindBy(css = ".float-right.view-codicil svg")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Item Status Name")
    @FindBy(css = "[data-ng-bind='vm.item.defectStatus.name']")
    private WebElement itemStatusName;

    @Step("Get Title Text")
    public String getTitleText()
    {
        return titleText.getText().trim();
    }

    @Step("Click further details arrow")
    public ViewDefectPage clickFurtherDetailsArrowButton()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewDefectPage.class);
    }

    @Step("Get Item Status")
    public String getItemStatus()
    {
        return itemStatusName.getText().trim();
    }
}
