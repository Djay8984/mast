package viewasset.sub.jobs.details.crediting;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.TextArea;

public class EditSurveyPage extends BasePage<EditSurveyPage>
{

    @Visible
    @Name("Narrative text box")
    @FindBy(css = "[data-ng-model='survey.narrative']")
    private WebElement narrativeTextBox;

    @Name("Save Edit survey button")
    @FindBy(css = "[data-ng-click='vm.editSave(survey)']")
    private WebElement saveButton;

    @Name("Requires followup action check box")
    @FindBy(css = "[data-ng-model='survey.requiresFollowUp']")
    private WebElement requiresFollowupActionCheckBox;

    @Name("Typified by text area")
    @FindBy(tagName = "textarea")
    private TextArea textArea;

    @Step("Check if Narrative text box is enabled")
    public boolean isNarrativeTextBoxEnabled()
    {
        return narrativeTextBox.isEnabled();
    }

    @Step("Check if Save button is enabled ")
    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }

    @Step("Click Save button")
    public CreditingPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(CreditingPage.class);
    }

    @Step("Get text from Narrative Text box")
    public String getNarrativeTextBox()
    {
        return narrativeTextBox.getAttribute("value").trim();
    }

    /**
     * There is an hidden action which will take entered text in random manner
     * This is a workaround for it
     */
    @Step("Set Narrative Text box")
    public EditSurveyPage setNarrativeTextBox(String text)
    {
        narrativeTextBox.clear();
        for (int i = 0; i < text.length(); i++)
            narrativeTextBox.sendKeys(text.substring(i, i + 1));
        return this;
    }

    @Step("Select RequiresFollow up checkbox")
    public EditSurveyPage selectRequiresFollowupCheckBox()
    {
        requiresFollowupActionCheckBox.click();
        return this;
    }

}
