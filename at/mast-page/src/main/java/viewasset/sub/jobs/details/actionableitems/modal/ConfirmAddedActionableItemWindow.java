package viewasset.sub.jobs.details.actionableitems.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfirmAddedActionableItemWindow extends BasePage<ConfirmAddedActionableItemWindow>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "[data-ng-click='$close(action.result)']")
    private WebElement okButton;

    /**
     * Click on the furtherDetailsArrow
     *
     * @param pageObjectClass - The pageObject which is expected to be returned by clicking on OK button
     *                        Possible PageObjects: ActionableItemsPage.class
     *                        ViewDefectPage.class
     * @param <T>             The implementing type
     */

    @Step("Click Ok Button")
    public <T extends BasePage<T>> T clickOkButton(Class<T> pageObjectClass)
    {
        okButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

}
