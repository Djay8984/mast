package viewasset.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.ViewAssetPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.SearchWithinAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;

public class AssetModelSubPage extends BasePage<AssetModelSubPage>
{

    @Name("List view button")
    @FindBy(css = "[data-ng-click*='list']")
    @Visible
    private WebElement listViewButton;

    @Name("Hierarchy view button")
    @FindBy(css = "[data-ng-click*='hierarchy']")
    @Visible
    private WebElement hierarchyViewButton;

    @Name("Search within asset model button")
    @FindBy(css = "[data-ng-click='vm.toggleSearch()']")
    @Visible
    private WebElement searchWithinAssetModelButton;

    @Name("Breadcrumbs")
    @FindBy(css = "[data-ng-repeat='item in vm.breadcrumb track by item.id']")
    private WebElement breadcrumb;

    @Name("Check Out For Editing Button")
    @FindBy(css = "[data-ng-click='vm.toggleBuildMode()']")
    private WebElement checkOutForEditingButton;

    @Step("Click on List view button")
    public ListViewSubPage clickListViewButton()
    {
        listViewButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @Step("Click on the Hierarchy view button")
    public HierarchyViewSubPage clickHierarchyViewButton()
    {
        hierarchyViewButton.click();
        return PageFactory.newInstance(HierarchyViewSubPage.class);
    }

    @Step("Click Checkout For Editing Button")
    public BuildAssetModelPage clickCheckOutForEditingButton()
    {
        checkOutForEditingButton.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    /**
     * Use this get to return List View Subpage after
     * MyWorkAssetElement.clickViewAssetButton()
     * AllAssetsAssetElement.clickViewAssetButton()
     * <p>
     * This is because methods above returns ViewAssetPage which defaults
     * to viewasset.assetmodel.AssetModelSubPage and viewasset.assetmodel.assetmodel
     * .ListViewSubPage.
     *
     * @return ListViewSubPage
     */

    @Step("Get List View SubPage")
    public ListViewSubPage getListViewSubPage()
    {
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @Step("Check that List view button is selected")
    public boolean isListViewButtonSelected()
    {
        return listViewButton.getAttribute("data-ng-class").contains("list");
    }

    @Step("Get breadcrumb text value")
    public String getBreadcrumbText()
    {
        return breadcrumb.getText();
    }

    @Step("Click Search Within Asset Model Button")
    public SearchWithinAssetModelPage clickSearchWithinAssetModelButton()
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 0);");
        searchWithinAssetModelButton.click();
        return PageFactory.newInstance(SearchWithinAssetModelPage.class);
    }

    @Step("Is Checkout For Editing Button Enabled")
    public boolean isCheckOutForEditingButtonEnabled()
    {
        return checkOutForEditingButton.isEnabled();
    }

    @Step("Is List View Button Enabled")
    public boolean isListViewButtonEnabled()
    {
        return listViewButton.isEnabled();
    }

    @Step("Is Hierarchy View Button Enabled")
    public boolean isHierarchyViewButtonEnabled()
    {
        return hierarchyViewButton.isEnabled();
    }

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Get Asset Model Subpage")
    public ViewAssetPage getViewAssetPage()
    {
        return PageFactory.newInstance(ViewAssetPage.class);
    }
}
