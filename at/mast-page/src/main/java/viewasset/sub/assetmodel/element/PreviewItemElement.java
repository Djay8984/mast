package viewasset.sub.assetmodel.element;

import com.frameworkium.core.ui.js.JavascriptWait;
import com.frameworkium.core.ui.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;

import java.util.ArrayList;
import java.util.List;

public class PreviewItemElement
{

    private final static String PREVIEW_ITEM_BIT_SELECTOR = "div[id^='hierarchy-node-']";
    private final static JavascriptWait javascriptWait = new JavascriptWait(BaseTest.getDriver(), BaseTest.getWait());

    @Name("Expand Icon")
    private WebElement expandIcon;

    @Name("Item Checkbox")
    private WebElement itemCheckbox;

    @Name("Item Name")
    private WebElement itemName;

    @Name("Nested Preview Item Elements")
    private List<PreviewItemElement> nestedPreviewItemElements = new ArrayList<>();
    private WebElement nestedItem;

    @Name("Child Nested Preview Item Elements")
    private List<PreviewItemElement> childNestedPreviewItemElements = new ArrayList<>();

    public PreviewItemElement(WebElement nestedItem)
    {
        this.nestedItem = nestedItem;
        this.expandIcon = nestedItem.findElement(By.cssSelector("[data-ng-click*='vm.expand(']"));
        if (getItemCheckboxId() != null && nestedItem.findElements(By.id(getItemCheckboxId())).size() > 0)
        {
            this.itemCheckbox = nestedItem.findElement(By.id(getItemCheckboxId()));
        }
        this.itemName = nestedItem.findElement(By.cssSelector("[id$='-name']"));
    }

    @Step("Expanding item")
    public PreviewItemElement clickExpandIcon()
    {
        expandIcon.click();
        BaseTest.getWait().until(itemIsExpanded());
        return this;
    }

    @Step("Collapse item")
    public PreviewItemElement clickCollapseIcon()
    {
        expandIcon.click();
        javascriptWait.waitForJavascriptFramework();
        BaseTest.getWait().until(ExpectedConditions
                .visibilityOfElementLocated(
                        By.cssSelector(".ng-scope.has-items")));
        return this;

    }

    @Step("Returning the nested items under the item")
    public List<PreviewItemElement> getNestedItems()
    {
        setNestedItems();
        return nestedPreviewItemElements;
    }

    public List<PreviewItemElement> getNestedRelatedItems()
    {
        setNestedRelatedItems();
        return nestedPreviewItemElements;
    }

    @Step("Returning the child Nested items under the item")
    public List<PreviewItemElement> getChildNestedItems()
    {
        setChildNestedItems();
        return childNestedPreviewItemElements;
    }

    @Step("Returning a nested item based on its name")
    public PreviewItemElement getNestedItemByName(String itemName)
    {
        setNestedItems();
        return nestedPreviewItemElements.stream()
                .filter(i -> i.getItemName().equals(itemName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning the item name")
    public String getItemName()
    {
        return itemName.getText();
    }

    @Step("Clicking the checkbox to copy the item")
    public void clickItemCheckbox()
    {
        boolean checkboxChecked = isItemCheckboxSelected();
        AppHelper.scrollToBottom();
        nestedItem.findElement(By.id(getItemCheckboxId())).click();
        javascriptWait.waitForJavascriptFramework();
        BaseTest.getWait().until((checkboxChecked) ? checkboxUnchecked() : checkboxChecked());
    }

    @Step("is Item checkbox selected")
    public boolean isItemCheckboxSelected()
    {
        return itemCheckbox.getAttribute("class").contains("checked");
    }

    @Step("Returning whether checkbox is displayed")
    public boolean isItemCheckboxPresent()
    {
        return getItemCheckboxId() != null && nestedItem.findElements(By.id(getItemCheckboxId())).size() > 0;
    }

    private void setNestedItems()
    {
        this.nestedItem.findElements(By
                .cssSelector("li[data-ng-repeat*='vm.assetModel.items']"))
                .stream().forEach(e -> this
                .nestedPreviewItemElements.add(new PreviewItemElement(e)));
    }

    private void setNestedRelatedItems()
    {
        this.nestedItem.findElements(By
                .cssSelector("li[data-ng-repeat*='vm.assetModel.items']"))
                .stream().forEach(e -> this
                .nestedPreviewItemElements.add(new PreviewItemElement(e)));
    }

    private void setChildNestedItems()
    {
        this.nestedItem.findElements(By
                .cssSelector("li[data-ng-repeat*='node in node.items']"))
                .stream().forEach(e -> this
                .childNestedPreviewItemElements.add(new PreviewItemElement(e)));
    }

    private String getItemCheckboxId()
    {
        if (this.nestedItem.findElements(By.cssSelector(PREVIEW_ITEM_BIT_SELECTOR)).size() == 0)
            return null;
        return this.nestedItem.findElement(By
                .cssSelector(PREVIEW_ITEM_BIT_SELECTOR)).getAttribute("id")
                + "-checkbox";
    }

    private String getItemRadioButtonId()
    {
        return this.nestedItem.findElement(By
                .cssSelector(PREVIEW_ITEM_BIT_SELECTOR)).getAttribute("id")
                + "-radio-button";
    }

    @Step("is Item radioButton selected")
    public boolean isItemRadioButtonSelected()
    {
        return nestedItem.findElement(By.cssSelector(PREVIEW_ITEM_BIT_SELECTOR +
                " div input")).getAttribute("class")
                .contains("ng-valid-parse");
    }

    @Step("is Item radioButton Enabled")
    public boolean isItemRadioButtonEnabled()
    {
        return nestedItem.findElement(By.cssSelector(PREVIEW_ITEM_BIT_SELECTOR +
                " div label"))
                .isSelected();
    }

    @Step("Clicking the checkbox to copy the item")
    public void clickItemRadioButton()
    {
        nestedItem.findElement(By.cssSelector("div[id^='hierarchy-node-'] div label")).click();
        javascriptWait.waitForJavascriptFramework();
    }

    private ExpectedCondition<Boolean> itemIsExpanded()
    {
        return driver ->
        {
            setNestedItems();
            setChildNestedItems();
            return nestedPreviewItemElements.size() > 0 || childNestedPreviewItemElements.size() > 0;
        };
    }

    private ExpectedCondition<Boolean> checkboxChecked()
    {
        return driver -> isItemCheckboxSelected();
    }

    private ExpectedCondition<Boolean> checkboxUnchecked()
    {
        return driver -> !isItemCheckboxSelected();
    }
}
