package viewasset.sub.assetmodel.buildassetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.mysql.fabric.xmlrpc.base.Data;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.AssetModelSubPage;
import viewasset.sub.assetmodel.ListViewSubPage;
import viewasset.sub.assetmodel.buildassetmodel.base.BaseTable;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;
import viewasset.sub.assetmodel.element.BuildAssetModelElement;

import java.util.List;

public class BuildAssetModelPage extends BasePage<BuildAssetModelPage>
{

    @Name("Copy from asset model button")
    @FindBy(id = "asset-model-copy-from-asset-model")
    @Visible
    private WebElement copyFromAssetModelButton;

    @Name("Add from reference data button")
    @FindBy(id = "asset-model-add-from-reference-data")
    @Visible
    private WebElement addFromReferenceDataButton;

    @Name("Finish Button")
    @FindBy(id = "asset-model-finish")
    @Visible
    private WebElement finishButton;

    @Deprecated
    @Name("Root item")
    @FindBy(css = ".asset-model-hierarchy-build #hierarchy-node-")
    private WebElement rootItem;

    @Name("Save confirmation message window")
    @FindBy(id = "save-confirmation-dialog")
    private WebElement saveConfirmationMessageWindow;

    @Name("Modal Overlay")
    @FindBy(css = ".modal-overlay")
    private WebElement modalOverlay;

    @Name("Save Button")
    @FindBy(id = "asset-model-finish")
    private WebElement saveButton;

    @Deprecated
    @Name("Build Asset Model Items")
    private List<BuildAssetModelElement> buildAssetModelItems;

    @Name("Currently Selected Item")
    @FindBy(css = ".is-selected")
    private WebElement selectedItem;

    @Step("Clicking copy from asset model button")
    public CopyFromAssetModelPage clickCopyFromAssetModelButton()
    {
        copyFromAssetModelButton.click();
        return PageFactory.newInstance(CopyFromAssetModelPage.class);
    }

    @Step("Clicking add from reference data button")
    public AddFromReferenceDataPage clickAddFromReferenceDataButton()
    {
        addFromReferenceDataButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("build-header-back")));
        return PageFactory.newInstance(AddFromReferenceDataPage.class);
    }

    @Step("Clicking finish button")
    public AssetModelSubPage clickFinishButton()
    {
        finishButton.click();
        if (modalOverlay.isDisplayed())
            AppHelper.waitForModal();
        return PageFactory.newInstance(AssetModelSubPage.class);
    }

    @Deprecated
    @Step("Get Build Asset Model Elements Items")
    public List<BuildAssetModelElement> getItems()
    {
        return buildAssetModelItems;
    }

    @Deprecated
    @Step("Get Data Elements Items")
    public List<DataElement> getTableItems()
    {
        return getBuildAssetModelTable().getRows();
    }

    @Deprecated
    @Step("Returning an asset item based on item name")
    public DataElement getItemByItemName(String itemName)
    {
        return buildAssetModelTable.getRows().stream()
                .filter(b -> b.getItemName().equals(itemName)).findFirst().orElse(null);
    }

    @Step("Returning an asset item based on row number")
    public DataElement getItemByIndex(int rowNumber)
    {
        return (buildAssetModelTable.getRows().size() >= rowNumber)
                ? buildAssetModelTable.getRows().get(rowNumber) : null;
    }

    @Step("Get Root Item From BaseTable")
    public DataElement getRootItem()
    {
        return buildAssetModelTable.getRootItem();
    }

    @Step("Click Build Asset Model Button")
    @Deprecated
    public ListViewSubPage clickBuildAssetModelButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @Step("Click the save button")
    public ListViewSubPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @FindBy(css = "div.hierarchy")
    public class BuildAssetModelTable extends BaseTable
    {
    }

    @Name("Build Asset Model Table")
    private BuildAssetModelTable buildAssetModelTable;

    @Step("Get Build Asset Model Table")
    public BuildAssetModelTable getBuildAssetModelTable()
    {
        return buildAssetModelTable;
    }

    @Step("Get Currently Selected Item")
    public String getSelectedItem()
    {
        return selectedItem.getText();
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
