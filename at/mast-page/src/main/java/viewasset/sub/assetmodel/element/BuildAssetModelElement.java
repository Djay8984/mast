package viewasset.sub.assetmodel.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;

import java.util.List;

@Name("Build Asset Model Elements")
@FindBy(css = "div.hierarchy")
public class BuildAssetModelElement extends HtmlElement
{

    @Name("Item name")
    @FindBy(css = "div.desc.ng-binding")
    @Visible
    private WebElement itemName;

    @Name("Duplicate Icon")
    @FindBy(css = "[data-ng-click='vm.createDuplicate(data)']")
    private List<WebElement> duplicateIcon;

    private final String moveUpIconCss = "[data-ng-click='vm.moveItem(data, -1)']";
    @Name("Move Up Icon")
    @FindBy(css = moveUpIconCss)
    private WebElement moveUpIcon;

    private final String moveDownIconCss = "[data-ng-click='vm.moveItem(data, 1)']";
    @Name("Move Down Icon")
    @FindBy(css = moveDownIconCss)
    private WebElement moveDownIcon;

    @Name("Delete Item Icon")
    @FindBy(css = "[data-ng-click*='vm.deleteItem']")
    private WebElement deleteIcon;

    @Name("Expand/Collapse Icon")
    @FindBy(css = ".node-expand-toggle")
    private WebElement expandCollapseIcon;

    @FindBy(css = ".ng-scope.is-expanded.last-expanded")
    private WebElement expandElement;

    @Name("Build Asset Model Items")
    private List<BuildAssetModelElement> buildAssetModelItems;

    @Step("Returning item name")
    public String getItemName()
    {
        AppHelper.scrollToBottom();
        return itemName.getText();
    }

    @Step("Clicking item name")
    public BuildAssetModelPage clickItemName()
    {
        Wait<WebDriver> wait = BaseTest.getWait();
        AppHelper.scrollToBottom();
        Boolean isItemExpanded = isItemExpanded();
        itemName.click();
        wait.until(itemIsSelected());
        if (isItemExpanded)
        {
            wait.until(itemIsCollapsed());
        }
        else if (isItemExpandable())
        {
            wait.until(itemIsExpanded());
        }
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Is duplicateIcon displayed")
    public boolean isDuplicateIconDisplayed()
    {
        return duplicateIcon.size() > 0;
    }

    @Step("Is Move Up Icon displayed")
    public boolean isMoveUpIconPresent()
    {
        return this.findElements(By.cssSelector(moveUpIconCss)).size() > 0;
    }

    @Step("Is Move Down Icon displayed")
    public boolean isMoveDownIconPresent()
    {
        return this.findElements(By.cssSelector(moveDownIconCss)).size() > 0;
    }

    @Step("Is Delete Icon displayed")
    public boolean isDeleteIconDisplayed()
    {
        return deleteIcon.isDisplayed();
    }

    @Step("Click on move up icon")
    public BuildAssetModelElement clickMoveUpIcon()
    {
        AppHelper.scrollToBottom();
        moveUpIcon.click();
        AppHelper.waitForOnScreenAnimation();
        return this;
    }

    @Step("Click on move down icon")
    public BuildAssetModelElement clickMoveDownIcon()
    {
        AppHelper.scrollToBottom();
        moveDownIcon.click();
        AppHelper.waitForOnScreenAnimation();
        return this;
    }

    @Step("Click Collapse Icon")
    public BuildAssetModelElement clickExpandCollapseIcon()
    {
        if (buildAssetModelItems.size() == 0)
        {
            expandCollapseIcon.click();
            BaseTest.getWait().until(itemIsExpanded());
        }
        else
        {
            expandCollapseIcon.click();
            BaseTest.getWait().until(itemIsCollapsed());
        }
        return this;
    }

    @Step("Get Build Asset Model Elements Items")
    public List<BuildAssetModelElement> getChildElements()
    {
        return buildAssetModelItems;
    }

    @Step("Verify if item selected ")
    public boolean isItemSelected()
    {
        return findElements(By.cssSelector("div.item-element.ng-scope.is-selected")).size() > 0;
    }

    @Step("Click delete item icon")
    public BuildAssetModelPage clickDeleteItemIcon()
    {
        deleteIcon.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Finding out if an item is expandable")
    public boolean isItemExpandable()
    {
        return expandCollapseIcon.getAttribute("data-ng-click").contentEquals("icon");
    }

    @Step("Has the item been expanded")
    public boolean isItemExpanded()
    {
        return this.findElement(By.cssSelector("div.icon.expanded"))
                .isDisplayed();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    private ExpectedCondition<Boolean> itemIsSelected()
    {
        return driver -> isItemSelected();
    }

    private ExpectedCondition<Boolean> itemIsCollapsed()
    {
        return driver -> buildAssetModelItems.size() == 0;
    }

    private ExpectedCondition<Boolean> itemIsExpanded()
    {
        return driver -> buildAssetModelItems.size() > 0;
    }

    @Step("Click duplicate item icon")
    public BuildAssetModelPage clickDuplicateItemIcon()
    {
        duplicateIcon.get(0).click();
        AppHelper.waitForOnScreenAnimation();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }
}
