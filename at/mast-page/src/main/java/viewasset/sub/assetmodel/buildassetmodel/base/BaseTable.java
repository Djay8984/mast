package viewasset.sub.assetmodel.buildassetmodel.base;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.assetmodel.buildassetmodel.element.DataElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Root item")
    @FindBy(css = "div[hierarchy='data'] div[data-tpl='result'] > div > div > div.title")
    private DataElement rootItem;

    @Name("Data Elements")
    @FindBy(xpath = "./div/div/div/ul/li[@data-ng-repeat='item in result track by item.id']")
    private List<DataElement> dataElements;

    @Step("Get Data Elements")
    public List<DataElement> getRows()
    {
        return dataElements;
    }

    @Step("Get Root Item")
    public DataElement getRootItem()
    {
        return rootItem;
    }

    @Step("Is Root Item Selected")
    public boolean isRootItemSelected()
    {
        return rootItem.getAttribute("class").equals("is-selected has-items");
    }

}
