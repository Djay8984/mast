package viewasset.sub.assetmodel.buildassetmodel.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;

import java.util.List;

public class DataElement extends HtmlElement
{

    private final String moveUpIconCss = "[data-ng-click='data.actions.moveUp(data)']";
    private final String moveDownIconCss = "[data-ng-click='data.actions.moveDown(data)']";

    @Name("Children")
    @FindBy(xpath = "./div/div/ul/li")
    private List<DataElement> children;

    @Name("Item name")
    @FindBy(css = "div.desc span.ng-binding")
    @Visible
    private WebElement itemName;

    @Name("Duplicate Icon")
    @FindBy(css = "[data-ng-click='data.actions.duplicate(data)']")
    private List<WebElement> duplicateIcon;

    @Name("Move Up Icon")
    @FindBy(css = moveUpIconCss)
    private WebElement moveUpIcon;

    @Name("Move Down Icon")
    @FindBy(css = moveDownIconCss)
    private WebElement moveDownIcon;

    @Name("Delete Item Icon")
    @FindBy(css = "[data-ng-click*='data.actions.delete']")
    private WebElement deleteIcon;

    @Name("Expand Icon")
    @FindBy(css = "[data-ng-click*='data.expand']")
    private WebElement expandCollapseIcon;

    @FindBy(css = ".ng-scope.is-expanded.last-expanded")
    private WebElement expandElement;

    @Step("Returning item name")
    public String getItemName()
    {
        AppHelper.scrollToBottom();
        return itemName.getText();
    }

    @Step("Clicking item name")
    public BuildAssetModelPage clickItemName()
    {
        AppHelper.scrollToBottom();
        itemName.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Is duplicateIcon displayed")
    public boolean isDuplicateIconDisplayed()
    {
        return duplicateIcon.size() > 0;
    }

    @Step("Is Move Up Icon displayed")
    public boolean isMoveUpIconPresent()
    {
        return this.findElements(By.cssSelector(moveUpIconCss)).size() > 0;
    }

    @Step("Is Move Down Icon displayed")
    public boolean isMoveDownIconPresent()
    {
        return this.findElements(By.cssSelector(moveDownIconCss)).size() > 0;
    }

    @Step("Is Delete Icon displayed")
    public boolean isDeleteIconDisplayed()
    {
        return deleteIcon.isDisplayed();
    }

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = "[data-ng-click='data.actions.open(data)']")
    private WebElement furtherDetailsArrow;

    @Step("Click Further Details Arrow")
    public ItemsDetailsPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ItemsDetailsPage.class);
    }

    @Step("Click on move up icon")
    public DataElement clickMoveUpIcon()
    {
        AppHelper.scrollToBottom();
        moveUpIcon.click();
        AppHelper.waitForOnScreenAnimation();
        return this;
    }

    @Step("Click on move down icon")
    public DataElement clickMoveDownIcon()
    {
        AppHelper.scrollToBottom();
        moveDownIcon.click();
        AppHelper.waitForOnScreenAnimation();
        return this;
    }

    @Step("Get Build Asset Model Elements Items")
    public List<DataElement> getChildren()
    {
        return children;
    }

    @Step("Verify if item selected ")
    public boolean isItemSelected()
    {
        return findElements(By.cssSelector("div[id*='hierarchy-node-'].is-selected")).size() > 0;
    }

    @Step("Click delete item icon")
    public BuildAssetModelPage clickDeleteItemIcon()
    {
        deleteIcon.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Finding out if an item is expandable")
    public boolean isItemExpandable()
    {
        return expandCollapseIcon.findElements(By.tagName("svg")).size() > 0;
    }

    @Step("Has the item been expanded")
    public boolean isItemExpanded()
    {
        return this.findElements(By.tagName("circle")).size() > 0;
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    @Step("Click duplicate item icon")
    public BuildAssetModelPage clickDuplicateItemIcon()
    {
        duplicateIcon.get(0).click();
        AppHelper.waitForOnScreenAnimation();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Click on Expand Icon")
    public DataElement clickExpandIcon()
    {
        expandCollapseIcon.click();
        BaseTest.getWait().until(itemIsExpanded());
        return this;
    }

    @Step("Click on Collapse Icon")
    public DataElement clickCollapseIcon(){
        expandCollapseIcon.click();
        BaseTest.getWait().until(itemIsCollapsed());
        return this;
    }

    @Step("Is Item Selected")
    private ExpectedCondition<Boolean> itemIsSelected()
    {
        return driver -> isItemSelected();
    }

    @Step("Is Item Expanded")
    private ExpectedCondition<Boolean> itemIsExpanded()
    {
        return driver -> children.size() > 0;
    }

    @Step("Is Item Collapsed")
    private ExpectedCondition<Boolean> itemIsCollapsed()
    {
        return driver -> children.size() == 0;
    }
}
