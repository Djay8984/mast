package viewasset.sub.assetmodel.buildassetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.element.AllAssetsAssetElement;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.PaginationCountSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CopyFromAssetModelPage extends BasePage<CopyFromAssetModelPage>
{

    @Name("Item Name")
    @FindBy(css = "[data-ng-bind='item.name']")
    @Visible
    private WebElement itemName;

    @Name("Search/Filter Assets Button")
    @FindBy(id = "build-copy-asset-search-filter-toggle")
    @Visible
    private WebElement searchFilterAssetsButton;

    @Name("Navigate Back Button")
    @FindBy(css = ".back-button polygon")
    @Visible
    private WebElement navigateBackButton;

    @Name("No results found Text Container")
    @FindBy(css = ".grid-block.asset-card-container.ng-scope")
    @Visible
    private WebElement noResultsFoundTextContainer;

    @Name("Asset sort order dropdown")
    @FindBy(css = "[data-ng-model='vm.sortOrder']")
    private WebElement assetSortOrderDropdown;

    @Name("Load more button")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private List<WebElement> loadMoreButton;

    @Name("Breadcrumbs")
    @FindBy(css = "li[data-ng-repeat='item in vm.breadcrumb track by item.id']")
    @Visible
    private List<WebElement> breadcrumb;

    @Name("Build Asset Model cards")
    private List<AllAssetsAssetElement> buildModelCards;

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Returning item name")
    public String getItemName()
    {
        return itemName.getText();
    }

    @Step("Clicking search/filter assets button")
    public SearchFilterSubPage clickSearchFilterAssetsButton()
    {
        searchFilterAssetsButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .id(SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));

        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(SearchFilterSubPage.class);
    }

    @Step("Returning the all assets commonpage page")
    public AllAssetsSubPage getAllAssetsSubPage()
    {
        return PageFactory.newInstance(AllAssetsSubPage.class);
    }

    @Step("Retrieve No results found text")
    public String getNoResultsFoundContainerText()
    {
        return noResultsFoundTextContainer.getText();
    }

    @Step("Get Number of Build Asset Model Cards Displayed ")
    public int getNumberOfCardsDisplayed()
    {
        return buildModelCards.size();
    }

    @Step("Click Navigate Back Button")
    public BuildAssetModelPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Get breadcrumb text value")
    public String getBreadcrumbTextByIndex(int index)
    {
        return breadcrumb.get(index).getText();
    }

    @Step("Get breadcrumb text size")
    public int getBreadCrumbTextSize()
    {
        return breadcrumb.size();
    }

    @Step("Is breadcrumb Clickable")
    public boolean isBreadcrumbClickable(int index)
    {
        return breadcrumb.get(index)
                .getAttribute("class").contains("actionable");
    }

    @Step("Returning an asset card by asset name")
    public AllAssetsAssetElement getAssetCardByAssetName(String assetName)
    {
        return buildModelCards.stream()
                .filter(c -> c.getAssetName().equals(assetName))
                .findFirst()
                .orElse(null);
    }

    @Step("Check the sort dropdown text displayed")
    public boolean isSortDropDownTextDisplayed(String dropDownText)
    {
        return new Select(assetSortOrderDropdown)
                .getOptions().stream().filter(e -> e.getText().equals(dropDownText))
                .findFirst().isPresent();
    }

    @Step("Returning the pagination count object")
    public PaginationCountSubPage getPaginationCountPage()
    {
        return PageFactory.newInstance(PaginationCountSubPage.class);
    }

    @Step("Clicking load more button")
    public CopyFromAssetModelPage clickLoadMoreButton()
    {
        if (loadMoreButton.size() > 0)
        {
            loadMoreButton.get(0).click();
        }
        new WebDriverWait(driver, 10)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions
                                .invisibilityOfElementLocated(By
                                        .cssSelector(AllAssetsSubPage.ASSET_SEARCH_LOADING_CSS))
                );
        return this;
    }

}
