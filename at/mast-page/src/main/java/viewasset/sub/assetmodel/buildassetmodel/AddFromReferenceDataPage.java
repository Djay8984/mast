package viewasset.sub.assetmodel.buildassetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.element.ReferenceItemElement;

import java.util.List;

public class AddFromReferenceDataPage extends BasePage<AddFromReferenceDataPage>
{

    @Name("Back button")
    @Visible
    @FindBy(id = "build-header-back")
    private WebElement backButton;

    @Name("Add selected button")
    @FindBy(id = "asset-model-build-add-selected-items")
    private WebElement addSelected;

    @Name("Cancel button")
    @FindBy(id = "asset-model-build-add-cancel")
    private WebElement cancel;

    @Name("Reference items")
    private List<ReferenceItemElement> referenceItems;

    @Name("Breadcrumbs")
    @FindBy(css = "#build-container [role='menuitem']")
    private List<WebElement> breadcrumb;

    @Name("Number of item types")
    @FindBy(css = "#asset-model-build-add-footer [data-ng-bind='vm.selectedCount']")
    private WebElement numItemTypes;

    @Step("Clicking on the back button")
    public BuildAssetModelPage clickBackButton()
    {
        backButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-model-copy-from-asset-model")));
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Clicking on the add selected button")
    public BuildAssetModelPage clickAddSelected()
    {
        addSelected.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-model-copy-from-asset-model")));
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Is the add selected button displayed")
    public boolean isAddSelectedDisplayed()
    {
        return addSelected.isDisplayed();
    }

    @Step("Clicking on the cancel button")
    public BuildAssetModelPage clickCancelButton()
    {
        cancel.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-model-copy-from-asset-model")));
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

    @Step("Is the cancel button displayed")
    public boolean isCancelButtonDisplayed()
    {
        return cancel.isDisplayed();
    }

    @Step("Returning an asset item based on item name")
    public ReferenceItemElement getItemByItemName(String itemName)
    {
        return referenceItems.stream()
                .filter(o -> o.getReferenceItemName().equals(itemName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an asset item based on row number")
    public ReferenceItemElement getItemByIndex(int rowNumber)
    {
        ReferenceItemElement element = null;
        if (referenceItems.size() >= rowNumber)
        {
            element = referenceItems.get(rowNumber - 1);
        }
        return element;
    }

    @Step("Returning all asset items ")
    public List<ReferenceItemElement> getReferenceItems()
    {
        return referenceItems;
    }

    @Step("Get breadcrumb text value")
    public String getBreadcrumbTextByIndex(int index)
    {
        return breadcrumb.get(index).getText();
    }

    @Step("Returning the number of items selected")
    public String getNumItemTypes()
    {
        return numItemTypes.getText();
    }
}
