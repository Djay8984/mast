package viewasset.sub.assetmodel.buildassetmodel;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.buildassetmodel.modal.CopyAcrossAttributeValuesWindow;
import viewasset.sub.assetmodel.element.PreviewItemElement;

import java.util.List;

public class PreviewAssetModelPage extends BasePage<PreviewAssetModelPage>
{

    private final static String COPY_SELECTED_BUTTON_ID = "asset-model-preview-copy";
    @Name("Root asset item")
    @FindBy(css = "li[data-ng-class*='vm.assetModel.expanded']")
    private WebElement rootItem;
    @Name("Copy selected button")
    @FindBy(id = COPY_SELECTED_BUTTON_ID)
    private WebElement copySelectedButton;
    @Name("Breadcrumbs")
    @FindBy(css = "li[data-ng-repeat='item in vm.breadcrumb track by item.id']")
    private List<WebElement> breadcrumb;
    @Name("Breadcrumb Text")
    @FindBy(css = ".sticky-footer.white div>div:nth-child(1)")
    private WebElement breadcrumbText;
    @Name("Cancel Button")
    @FindBy(id = "asset-model-preview-cancel")
    private WebElement cancelButton;

    @Step("Returning the root preview item")
    public PreviewItemElement getRootItem()
    {
        return new PreviewItemElement(rootItem);
    }

    @Step("Returning whether the copy selected button is visible")
    public boolean isCopySelectedButtonDisplayed()
    {
        return driver.findElements(By.id(COPY_SELECTED_BUTTON_ID)).size() > 0;
    }

    @Step("Get breadcrumb text value")
    public String getBreadcrumbTextByIndex(int index)
    {
        return
                breadcrumb.get(index).getText();
    }

    @Step("Get breadcrumb text size")
    public int getBreadCrumbTextSize()
    {
        return
                breadcrumb.size();
    }

    @Step("Get Breadcrumb text")
    public String getBreadCrumbText()
    {
        return breadcrumbText.getText().trim();
    }

    @Step("Check for Cancel Button")
    public boolean isCancelButtonDisplayed()
    {
        return cancelButton.isDisplayed();
    }

    @Step("Click copy select button")
    public CopyAcrossAttributeValuesWindow clickCopySelectButton()
    {
        copySelectedButton.click();
        return PageFactory.newInstance(CopyAcrossAttributeValuesWindow.class);
    }

}
