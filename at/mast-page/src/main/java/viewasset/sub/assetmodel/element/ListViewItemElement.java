package viewasset.sub.assetmodel.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import com.paulhammant.ngwebdriver.NgWebDriver;
import helper.PageFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.assetmodel.ListViewSubPage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("List View Items")
@FindBy(css = "tr[data-ng-repeat*='item in vm.items']")
public class ListViewItemElement extends HtmlElement
{

    @Name("Item name") //only child becomes 'span', links becomes 'a'
    @FindBy(css = "a.ng-scope,span.ng-scope")
    @Visible
    private WebElement itemNameLink;

    @Name("Item Name Column")
    @FindBy(css = "[data-ng-click='vm.selectItem(item)']")
    @Visible
    private WebElement itemNameColumn;

    @Name("Status")
    @FindBy(css = "[data-ng-if~='item.reviewed']")
    @Visible
    private WebElement statusIndicator;

    @Name("Review complete")
    @FindBy(css = "[data-ng-repeat*='item in vm.items'] td:nth-child(2)")
    @Visible
    private WebElement reviewCompleteValues;

    @Name("Further details arrow")
    @FindBy(css = "[data-ng-click='vm.openItem(item)']")
    @Visible
    private WebElement furtherDetailsArrow;

    private NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) BaseTest.getDriver());

    @Step("Clicking item name")
    public ListViewSubPage clickItemName()
    {
        itemNameLink.click();
        BaseTest.getWait().until(waitUntilItemsChange());
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @Step("Check that item is clickable")
    public boolean isItemNameClickable()
    {
        return itemNameLink.getAttribute("data-ng-if").equals("item.items.length");
    }

    @Step("Check that Review complete values is displayed")
    public boolean isReviewCompleteDisplayed()
    {
        return reviewCompleteValues.isDisplayed();
    }

    @Step("Check that Status Indicator is displayed")
    public boolean isStatusIndicatorDisplayed()
    {
        return statusIndicator.isDisplayed();
    }

    @Step("Returning item name")
    public String getItemName()
    {
        return itemNameColumn.getAttribute("title");
    }

    @Step("If the item can be clicked on (does it have child items)")
    public boolean isItemClickable()
    {
        return itemNameLink.findElements(By.cssSelector("[data-ng-if='item.items.length === 0']"))
                .size() > 0;
    }

    @Step("Check that furtherDetailsArrow is displayed ")
    public boolean isFurtherDetailsArrowDisplayed()
    {
        return furtherDetailsArrow.isDisplayed();
    }

    @Step("Click Further Details Arrow Link")
    public ItemsDetailsPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ItemsDetailsPage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    @Step("Returning review complete values")
    public String getReviewCompleteValues()
    {
        Pattern pat = Pattern.compile("^[/]\\d+");
        Matcher m = pat.matcher(reviewCompleteValues.getText());
        while (m.find())
        {
            return m.group();
        }
        return null;
    }

    @Step("Returning review inprogress values")
    public String getReviewInProgressValues()
    {
        Pattern pat = Pattern.compile("[/]$\\d");
        Matcher m = pat.matcher(reviewCompleteValues.getText());
        while (m.find())
        {
            return m.group();
        }
        return "0";
    }

    @Step
    public boolean isStatusComplete()
    {
        return statusIndicator.findElement(By.cssSelector("path"))
                .getAttribute("d")
                .equals("M 30 55 L 40 65 L 70 35");
    }

    private ExpectedCondition<Boolean> waitUntilItemsChange()
    {
        //Horrible, just horrible, but the only way... :(
        return (driver) -> {
            try
            {
                itemNameLink.getText();
                return false;
            }
            catch (StaleElementReferenceException e)
            {
                return true;
            }
        };
    }
}
