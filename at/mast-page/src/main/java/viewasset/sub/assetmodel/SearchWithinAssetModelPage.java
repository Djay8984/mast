package viewasset.sub.assetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.item.ItemsDetailsPage;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static constant.LloydsRegister.SHOWING_REGEX;
import static constant.LloydsRegister.TOTAL_REGEX;

public class SearchWithinAssetModelPage extends BasePage<SearchWithinAssetModelPage>
{

    @Name("Item TextBox")
    @Visible
    @FindBy(css = "#in-asset-model-search-filter-0")
    private WebElement itemTextBox;

    @Name("Attribute TextBox")
    @Visible
    @FindBy(id = "in-asset-model-search-filter-1")
    private WebElement attributeTextBox;

    @Name("Attribute Value TextBox")
    @Visible
    @FindBy(id = "in-asset-model-search-filter-2")
    private WebElement attributeValueTextBox;

    @Name("Cancel Search Button")
    @Visible
    @FindBy(css = "#in-asset-model-search-cancel")
    private WebElement cancelSearchButton;

    @Name("Search Button")
    @Visible
    @FindBy(css = "#in-asset-model-search-search")
    private WebElement searchButton;

    @Name("Item name")
    @FindBy(css = "[data-ng-repeat='item in vm.searchItems']")
    private List<WebElement> itemNames;

    @Name("Error Message Text")
    @FindBy(css = "[data-ng-if^='vm.searchCompleted'] p")
    private WebElement errorMessageText;

    @Name("Results Count Text")
    @FindBy(css = ".grid-content.small-6.ng-binding")
    private WebElement resultsText;

    @Name("Load more button")
    @FindBy(css = ".button.ng-scope")
    private WebElement loadMoreButton;

    @Name("Search within asset model button")
    @FindBy(css = "[data-ng-click='vm.toggleSearch()']")
    @Visible
    private WebElement searchWithinAssetModelButton;

    @Step("Setting the Item search input")
    public SearchWithinAssetModelPage setItemSearchInput(String input)
    {
        itemTextBox.clear();
        itemTextBox.sendKeys(input);
        return this;
    }

    @Step("Clear the Item search input")
    public SearchWithinAssetModelPage clearItemSearchInput()
    {
        itemTextBox.clear();
        return this;
    }

    @Step("Setting the Attribute search input")
    public SearchWithinAssetModelPage setAttributeSearchInput(String input)
    {
        attributeTextBox.clear();
        attributeTextBox.sendKeys(input);
        return this;
    }

    @Step("Setting the Attribute Value search input")
    public SearchWithinAssetModelPage setAttributeValueSearchInput(String input)
    {
        attributeValueTextBox.clear();
        attributeValueTextBox.sendKeys(input);
        return this;
    }

    @Step("Clear Attribute Value search input")
    public SearchWithinAssetModelPage clearAttributeValueSearchInput()
    {
        attributeValueTextBox.clear();
        return this;
    }

    @Step("Clear Attribute  search input")
    public SearchWithinAssetModelPage clearAttributeSearchInput()
    {
        attributeTextBox.clear();
        return this;
    }

    @Step("Click Search Button")
    public SearchWithinAssetModelPage clickSearchButton()
    {
        searchButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(SearchWithinAssetModelPage.class);
    }

    @Step("Click Search Button")
    public SearchWithinAssetModelPage clickLoadMoreButton()
    {
        Actions actions = new Actions(driver);
        actions.moveToElement(loadMoreButton).click().perform();
        wait.until(
                ExpectedConditions.invisibilityOfElementLocated(By
                        .cssSelector("[data-ng-hide='vm.loaded']")));
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Wait for text to be visible")
    public boolean isNavigationOptionDisplayed(String text)
    {
        return itemNames.stream()
                .filter(e -> e.getText().equals(text))
                .findFirst().filter(e -> e.findElement(By.cssSelector(".right"))
                        .isDisplayed())
                .isPresent();
    }

    @Step("Get Item Name Text")
    public List<String> getSearchResultText()
    {
        return itemNames.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("Click arrow to item detail page")
    public ItemsDetailsPage clickArrowToItemDetailPageBaseOnItemName(String itemName)
    {
        itemNames.stream()
                .filter(x -> x.getText().equals(itemName))
                .findFirst()
                .get()
                .findElement(By.tagName("svg"))
                .click();

        return PageFactory.newInstance(ItemsDetailsPage.class);
    }

    @Step("Get Item Field Text")
    public String getItemFieldText()
    {
        return itemTextBox.getAttribute("value").trim();
    }

    @Step("Get Attribute Field Text")
    public String getAttributeTypeNameText()
    {
        return attributeTextBox.getAttribute("value").trim();
    }

    @Step("Get Get Error Message Text")
    public String getErrorMessageText()
    {
        return errorMessageText.getText().trim();
    }

    @Step("Is Search Button Clickable")
    public boolean isSearchButtonClickable()
    {
        waitForJavascriptFrameworkToFinish();
        return searchButton.getAttribute("class").contains("disabled");
    }

    @Step("Get Get Error Message Text")
    public int getResultItemCount()
    {
        return itemNames.size();
    }

    @Step("Returning how many items are shown")
    public int getShownAmount()
    {
        return matchText(SHOWING_REGEX);
    }

    @Step("Returning the total amount of items")
    public int getTotalAmount()
    {
        return matchText(TOTAL_REGEX);
    }

    private int matchText(String regex)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(resultsText.getText());
        assertThat(matcher.find()).isTrue();
        return Integer.parseInt(matcher.group(1));
    }

    @Step("Is Item Text Box Displayed")
    public boolean isItemTextBoxDisplayed()
    {
        return itemTextBox.isDisplayed();
    }

    @Step("Is Attribute Text Box Displayed")
    public boolean isAttributeTextBoxDisplayed()
    {
        return attributeTextBox.isDisplayed();
    }

    @Step("Is Attribute Value Text Box Displayed")
    public boolean isAttributeValueTextBoxDisplayed()
    {
        return attributeValueTextBox.isDisplayed();
    }

    @Step("Check for search results are displayed ")
    public boolean isSearchResultsDisplayed()
    {
        return itemNames.size() > 0;
    }

    @Step("Click Search Within Asset Model Button")
    public void clickSearchWithinAssetModelButton()
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 0);");
        searchWithinAssetModelButton.click();
        waitForJavascriptFrameworkToFinish();
    }

}
