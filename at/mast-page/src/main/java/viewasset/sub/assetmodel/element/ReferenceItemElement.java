package viewasset.sub.assetmodel.element;

import com.frameworkium.core.ui.tests.BaseTest;
import com.paulhammant.ngwebdriver.NgWebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

@Name("Reference items")
@FindBy(css = "[data-ng-repeat='item in vm.availableItems track by item.id']")
public class ReferenceItemElement extends HtmlElement
{

    @Name("Checkbox")
    @FindBy(tagName = "button")
    private WebElement checkbox;

    @Name("Reference item")
    @FindBy(css = "[data-ng-bind='::item.name']")
    private WebElement referenceItem;

    private NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) BaseTest.getDriver());

    @Step("Return the reference item name")
    public boolean isCheckBoxEnabled()
    {
        return checkbox.isEnabled();
    }

    @Step("Click on the checkbox")
    public ReferenceItemElement clickCheckbox()
    {
        AppHelper.scrollToBottom();
        checkbox.click();
        ngWebDriver.waitForAngularRequestsToFinish();
        return this;
    }

    @Step("Return the reference item name")
    public String getReferenceItemName()
    {
        AppHelper.scrollToBottom();
        ngWebDriver.waitForAngularRequestsToFinish();
        return referenceItem.getText();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
