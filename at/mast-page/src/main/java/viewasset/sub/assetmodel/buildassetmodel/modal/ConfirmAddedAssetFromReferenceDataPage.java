package viewasset.sub.assetmodel.buildassetmodel.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;

public class ConfirmAddedAssetFromReferenceDataPage extends BasePage<ConfirmAddedAssetFromReferenceDataPage>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "#modal-item-name-updated button")
    private WebElement okButton;

    @Step("Click Ok Button")
    public BuildAssetModelPage clickOkButton()
    {
        okButton.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }

}
