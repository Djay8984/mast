package viewasset.sub.assetmodel.buildassetmodel.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.buildassetmodel.BuildAssetModelPage;

public class CopyAcrossAttributeValuesWindow extends BasePage<CopyAcrossAttributeValuesWindow>
{

    @Visible
    @Name("No Button")
    @FindBy(css = "modal-footer button:nth-child(1)")
    private WebElement noButton;

    @Visible
    @Name("Yes Button")
    @FindBy(css = "modal-footer button:nth-child(2)")
    private WebElement yesButton;

    @Step("Click Yes Button")
    public BuildAssetModelPage clickYesButton()
    {
        yesButton.click();
        return PageFactory.newInstance(BuildAssetModelPage.class);
    }
}
