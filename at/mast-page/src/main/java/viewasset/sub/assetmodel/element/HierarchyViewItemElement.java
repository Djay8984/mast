package viewasset.sub.assetmodel.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import com.paulhammant.ngwebdriver.NgWebDriver;
import helper.PageFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.item.ItemsDetailsPage;
import viewasset.sub.assetmodel.HierarchyViewSubPage;

import java.util.List;

@Name("Hierarchy View Items")
@FindBy(css = "ol > li")
public class HierarchyViewItemElement extends HtmlElement
{

    @Name("Item name")
    @FindBy(css = ".asset-model-hierarchy .node-name.ng-binding")
    @Visible
    private WebElement itemName;

    @Name("Node icon")
    @FindBy(className = "node-expand-toggle")
    private WebElement nodeIcon;

    @Name("Attributes link")
    @FindBy(tagName = "path")
    private WebElement attributesLink;

    @Name("Collapse Icon")
    @FindBy(className = "node-expand-toggle")
    private WebElement collapseIcon;

    @Name("Asset items")
    private List<HierarchyViewItemElement> assetItems;

    private NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) BaseTest.getDriver());

    @Step("Returning item name")
    public String getItemName()
    {
        WebDriver driver = BaseTest.getDriver();
        //scroll to the item that are off screen
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(false);",
                        itemName);
        //scrolling into view may cause staleness, wait for it
        ngWebDriver.waitForAngularRequestsToFinish();
        return itemName.getText();
    }

    @Step("Clicking on the item node")
    public HierarchyViewSubPage clickCollapseIcon()
    {
        BaseTest.getDriver().executeScript("arguments[0].scrollIntoView(false);", itemName);
        BaseTest.getWait().until(ExpectedConditions.elementToBeClickable(itemName));
        collapseIcon.click();
        BaseTest.getWait().until(childItemsToBeDisplayed());
        return PageFactory.newInstance(HierarchyViewSubPage.class);
    }

    @Step("Finding out if an item is expandable")
    public boolean isItemExpandable()
    {
        return nodeIcon.findElements(By.tagName("svg")).size() > 0;
    }

    @Step("Has the item been expanded")
    public boolean isItemExpanded()
    {
        return this.findElements(By.tagName("circle")).size() > 0;
    }

    @Step("Clicking on the item attribute link")
    public ItemsDetailsPage clickItemAttributesLink()
    {
        WebDriver driver = BaseTest.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(false);",
                        attributesLink);
        wait.until(ExpectedConditions.elementToBeClickable(attributesLink));
        attributesLink.click();

        return PageFactory.newInstance(ItemsDetailsPage.class);
    }

    @Step("Hover over the item attribute link")
    public boolean hoverOverItemAttributeLink()
    {
        WebDriver driver = BaseTest.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(false);",
                        attributesLink);
        wait.until(ExpectedConditions.elementToBeClickable(attributesLink));

        Actions action = new Actions(driver);
        action.moveToElement(attributesLink).perform();

        //TODO - The hover over is not implemented yet - this line will need updating
        return attributesLink.findElements(By.cssSelector("Open")).size() > 0;
    }

    @Step("Hover over the item attribute link")
    public List<HierarchyViewItemElement> getAssetItems()
    {
        return assetItems;
    }

    @Step("Verify if item selected ")
    public boolean isItemSelected()
    {
        return findElements(By.tagName("div")).stream().anyMatch(x -> x.getAttribute("class").contains("is-selected"));
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    private ExpectedCondition<Boolean> childItemsToBeDisplayed()
    {
        return driver -> assetItems.size() > 0;
    }
}
