package viewasset.sub.assetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.element.HierarchyViewItemElement;

import java.util.List;

public class HierarchyViewSubPage extends BasePage<HierarchyViewSubPage>
{

    @Name("Hierarchy View Page container")
    @FindBy(className = "asset-model-hierarchy")
    @Visible
    private WebElement pageContainer;

    @Name("Selected Asset Item")
    @FindBy(className = "is-selected")
    private WebElement selectedAssetItem;

    @Name("Root item")
    @FindBy(id = "hierarchy-node-")
    private WebElement rootItem;

    @Name("Asset items")
    private List<HierarchyViewItemElement> assetItems;

    @Step("Check that Hierarchy view page is displayed")
    public boolean isHierarchyViewPageDisplayed()
    {
        return pageContainer.isDisplayed();
    }

    @Step("Return the name of the selected asset item")
    public String getSelectedAssetItemName()
    {
        return selectedAssetItem.getText();
    }

    @Step("Returning an asset item based on item name")
    public HierarchyViewItemElement getItemBasedOnItemName(String itemName)
    {
        return assetItems.stream()
                .filter(o -> o.getItemName().equals(itemName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning the position of an item")
    public int getItemNamePosition(String itemName)
    {
        int i = 0;
        for (HierarchyViewItemElement assetItem : assetItems)
        {
            if (assetItem.getItemName().equals(itemName))
            {
                return i;
            }
            else
            {
                i++;
            }
        }
        return 0;
    }

    @Step("Getting the text of the root item")
    public String getRootItemText()
    {
        return rootItem.getText();
    }

    public List<HierarchyViewItemElement> getAssetItems()
    {
        return assetItems;
    }

}
