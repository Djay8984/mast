package viewasset.sub.assetmodel;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.element.ListViewItemElement;

import java.util.List;

public class ListViewSubPage extends BasePage<ListViewSubPage>
{

    @Name("List View Page container")
    @FindBy(className = ("asset-model-list"))
    @Visible
    private WebElement pageContainer;

    //Breadcrumbs belongs here because it changes when items are clicked
    @Name("Breadcrumbs")
    @FindBy(css = "li[data-ng-repeat='item in vm.breadcrumb track by item.id']")
    private List<WebElement> breadcrumb;

    @Name("Load more button")
    @FindBy(css = "[data-ng-click='vm.showMorePages()']")
    private WebElement loadMoreButton;

    @Name("Selected asset item")
    @FindBy(css = ".breadcrumbs li:nth-last-child(1)")
    @Visible
    private WebElement selectedAssetItem;

    @Name("No associated items")
    @FindBy(css = "tbody [ng-if='!vm.items.length']")
    private WebElement noAssociatedItems;

    @Name("Asset items")
    private List<ListViewItemElement> assetItems;

    @Step("Check that List view page is displayed")
    public boolean isListViewPageDisplayed()
    {
        return pageContainer.isDisplayed();
    }

    @Step("Click breadcrumb link index \"{0}\"")
    public ListViewSubPage clickBreadcrumbLinkByIndex(int index)
    {
        int assetItemsCount = assetItems.size();
        breadcrumb.get(index).click();
        wait.until(forItemsToChange(assetItemsCount));
        return PageFactory.newInstance(ListViewSubPage.class);
    }

    @Step("Get breadcrumb text value")
    public String getBreadcrumbTextByIndex(int index)
    {
        return breadcrumb.get(index).getText();
    }

    @Step("Get breadcrumb text size")
    public int getBreadCrumbTextSize()
    {
        return breadcrumb.size();
    }

    @Step("Returning an asset item based on item name")
    public ListViewItemElement getItemByItemName(String itemName)
    {
        return assetItems.stream()
                .filter(o -> o.getItemName().equals(itemName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an asset item based on row number")
    public ListViewItemElement getItemByIndex(int rowNumber)
    {
        ListViewItemElement element = null;
        if (assetItems.size() >= rowNumber)
        {
            element = assetItems.get(rowNumber - 1);
        }
        return element;
    }

    @Step("Get the selected asset item")
    public String getSelectedAssetItem()
    {
        return selectedAssetItem.getText();
    }

    @Step("If the item can be clicked on (does it have child items)")
    public boolean isItemClickable(String itemName)
    {
        ListViewItemElement itemElement = getItemByItemName(itemName);
        return itemElement.findElements(By.cssSelector("[data-ng-if='item.items.length === 0']"))
                .size() == 0;
    }

    public List<ListViewItemElement> getAssetItems()
    {
        return assetItems;
    }

    @Step("Check that Load More button is displayed")
    public boolean isLoadMoreButtonDisplayed()
    {
        return loadMoreButton.isDisplayed();
    }

    @Step("Finding out if the no associated items text appears")
    public boolean isNoAssociatedItemsMessageShown()
    {
        return driver.findElements(By.cssSelector("tbody [ng-if='!vm.items.length']")).size() > 0;
    }

    @Step("Get the text of no associated items")
    public String noAssociatedItemsMessageText()
    {
        return noAssociatedItems.getText();
    }

    @Deprecated
    @Step("is asset item displayed")
    public boolean isAssetItemDisplayed(String itemName)
    {
        return assetItems.stream()
                .filter(o -> o.getItemName().equals(itemName))
                .findFirst().get().isDisplayed();
    }

    private ExpectedCondition<Boolean> forItemsToChange(int itemCount)
    {
        return (driver) -> assetItems.size() != itemCount;
    }
}
