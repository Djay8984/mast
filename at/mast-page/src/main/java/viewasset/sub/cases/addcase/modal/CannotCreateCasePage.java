package viewasset.sub.cases.addcase.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.cases.addcase.SelectCaseTypePage;

public class CannotCreateCasePage extends BasePage<CannotCreateCasePage>
{

    @Visible
    @Name("Cannot Create Case Header")
    @FindBy(css = "[data-ng-transclude='header']")
    private WebElement cannotCreateCaseHeader;

    @Visible
    @Name("OK Button")
    @FindBy(css = "[data-ng-click='$close(false)']")
    private WebElement okButton;

    @Step("Click OK Button")
    public SelectCaseTypePage clickOkButton()
    {
        okButton.click();
        return PageFactory.newInstance(SelectCaseTypePage.class);
    }

    @Step("Get Header Text")
    public String getHeaderText()
    {
        return cannotCreateCaseHeader.getText();
    }
}
