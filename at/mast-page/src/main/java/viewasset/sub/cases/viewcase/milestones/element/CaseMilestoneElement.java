package viewasset.sub.cases.viewcase.milestones.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;

@Name("Cases Element")
@FindBy(css = "[data-ng-repeat='milestone in vm.milestones | filter:{ inScope: vm.scope }']")
public class CaseMilestoneElement extends HtmlElement
{
    @Name("Milestone status")
    @FindBy(css = ".milestone-status")
    private WebElement milestoneStatus;

    @Name("Milestone completed")
    @FindBy(css = "[data-ng-if='milestone.completionDate']")
    private WebElement milestoneCompleted;

    @Name("Milestone element checkbox")
    @FindBy(css = "[data-ng-model='milestone.reviewed']")
    private WebElement milestoneCheckbox;

    @Name("Milestone Description")
    @FindBy(css = "[data-ng-bind='milestone.milestone.description']")
    private WebElement milestoneDescription;

    @Step("Check if the completed milestone tag is visible")
    public boolean isMilestoneCompleted()
    {
        return milestoneCompleted.isDisplayed();
    }

    public CaseMilestoneElement clickMilestoneCheckbox()
    {
        AppHelper.scrollToBottom();
        milestoneCheckbox.click();
        return this;
    }

    public boolean isMilestoneCheckboxDisplayed()
    {
        return milestoneCheckbox.isDisplayed();
    }

    public String getMilestoneDescription()
    {
        return milestoneDescription.getText();
    }
}
