package viewasset.sub.cases.addcase;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import util.AppHelper;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.addcase.select.SelectOffice;
import viewasset.sub.cases.addcase.select.SelectRuleSet;
import viewasset.sub.cases.sub.Footer;

import java.util.List;

import static constant.LloydsRegister.CaseData.*;

public class SpecifyDetailsPage extends BasePage<SpecifyDetailsPage>
{

    @Visible
    @Name("Contract Reference TextBox")
    @FindBy(css = "#contract-reference-number")
    private WebElement contractReference;

    @Visible
    @Name("Case Acceptance Date")
    @FindBy(css = "#case-acceptance-date")
    private WebElement caseAcceptanceDate;

    @Visible
    @Name("Proposed Flag")
    @FindBy(css = "#proposed-flag-state")
    private WebElement proposedFlag;

    @Visible
    @Name("Case Office")
    @FindBy(css = "#case-sdo")
    private WebElement caseOffice;

    @Visible
    @Name("Case Owner")
    @FindBy(css = "#case-owner")
    private WebElement caseOwner;

    @Visible
    @Name("Case Admin")
    @FindBy(css = "#case-admin")
    private WebElement caseAdmin;

    @Visible
    @Name("Case CFO")
    @FindBy(css = "#case-cfo")
    private WebElement caseCfo;

    @Visible
    @Name("Asset Name TextBox")
    @FindBy(css = "#case-asset-name")
    private WebElement assetName;

    @Visible
    @Name("Asset Type TextBox")
    @FindBy(css = "#case-asset-type")
    private WebElement assetType;

    @Visible
    @Name("Port Of Registry")
    @FindBy(css = "#registered-port")
    private WebElement portOfRegistry;

    @Visible
    @Name("Contract Holder")
    @FindBy(css = "#contract-holder")
    private WebElement contractHolder;

    @Visible
    @Name("Job SDO")
    @FindBy(css = "#job-sdo")
    private WebElement jobSdo;

    @Visible
    @Name("Lead Surveyor")
    @FindBy(css = "#lead-surveyor")
    private WebElement leadSurveyor;

    @Name("Class Status")
    @FindBy(css = "#case-class-status")
    private Select classStatus;

    @Name("Flag State")
    @FindBy(css = "#flag-state")
    private WebElement currentFlag;

    @Visible
    @Name("Add Office Button")
    @FindBy(css = "[data-ng-click='vm.selectOfficeType()']")
    private Button addOfficeButton;

    @Name("Gross Tonnage Button")
    @FindBy(css = "#case-gross-tonnage")
    private Button grossTonnage;

    @Name("Class Maintenance Regime Single button")
    @FindBy(css = "[for='maintainence-regime-0']")
    private WebElement singleButton;

    @Name("Estimated Build Date")
    @FindBy(css = "#estimated-build-date")
    private WebElement estimatedBuildDate;

    @Name("Class Maintenance Regime Dual button")
    @FindBy(css = "[for='maintainence-regime-1']")
    private WebElement dualButton;

    @Name("Build RuleSet Button")
    @FindBy(css = "[data-ng-click='vm.buildRuleSet()']")
    private Button buildRuleSetButton;

    @Name("Favourable Risk Assessment Check")
    @FindBy(css = "[for='risk-assessment-status-type-0']")
    private Button favourableRiskAssessmentCheck;

    @Name("Favourable Pre Inspection Check")
    @FindBy(css = "[for='pre-eic-inspection-status-type-0']")
    private Button favourablePreInspectionCheck;

    @Name("Losing Society")
    @FindBy(css = "#losing-society")
    private Button losingSociety;

    @Step("Get Footer")
    public Footer getFooter()
    {
        return PageFactory
                .newInstance(Footer.class);
    }

    @Step("Select Favourable in Risk Assessment Check")
    public SpecifyDetailsPage clickFavourableRiskAssessmentCheck()
    {
        AppHelper.scrollToBottom();
        favourableRiskAssessmentCheck.click();
        return this;
    }

    @Step("Get Port Of Registry")
    public String getPortOfRegistry()
    {
        AppHelper.scrollToBottom();
        return portOfRegistry.getAttribute("value");
    }

    @Step("Select Favourable in Pre-Inspection Check")
    public SpecifyDetailsPage clickFavourablePreInspectionCheck()
    {
        AppHelper.scrollToBottom();
        favourablePreInspectionCheck.click();
        return this;
    }

    @Step("Click Add Office Button")
    public SelectOffice clickAddOfficeButton()
    {
        AppHelper.scrollToBottom();
        addOfficeButton.click();
        return PageFactory.newInstance(SelectOffice.class);
    }

    @Step("Click Add Office Button")
    public SelectRuleSet clickBuildRuleSetButton()
    {
        AppHelper.scrollToBottom();
        buildRuleSetButton.click();
        return PageFactory.newInstance(SelectRuleSet.class);
    }

    @Step("Set Date in Case Acceptance Date")
    public SpecifyDetailsPage setCaseAcceptanceDate(String date)
    {
        AppHelper.scrollToBottom();
        caseAcceptanceDate.sendKeys(date);
        return this;
    }

    @Step("Get AssetName")
    public String getAssetName()
    {
        AppHelper.scrollToBottom();
        return assetName.getAttribute("value");
    }

    @Step("Get Asset Type")
    public String getAssetType()
    {
        AppHelper.scrollToBottom();
        return assetType.getAttribute("value");
    }

    @Step("Select Class Status")
    public SpecifyDetailsPage selectClassStatus(int index)
    {
        AppHelper.scrollToBottom();
        classStatus.selectByIndex(index);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Single Button")
    public SpecifyDetailsPage clickSingleButton()
    {
        AppHelper.scrollToBottom();
        singleButton.click();
        return this;
    }

    @Step("Click Dual Button")
    public SpecifyDetailsPage clickDualButton()
    {
        AppHelper.scrollToBottom();
        dualButton.click();
        return this;
    }

    /**
     * Set the text in the specified text box which has the TypeAhead suggestions
     *
     * @param e       - The text box as a webElement
     * @param textStr - The String to be set on the TextBox
     * @return SpecifyDetailsPage
     */
    @Step("set Text box that has Type ahead Suggestions")
    public SpecifyDetailsPage setTextBasedOnTypeAheadSuggestions(WebElement e, String textStr)
    {
        AppHelper.scrollToBottom();
        e.sendKeys(textStr);
        wait.until(suggestionsShown());
        List<WebElement> suggestions = driver.findElements(
                By.cssSelector("[data-ng-repeat='match in matches'] a.ng-binding")
        );
        AppHelper.scrollToBottom();
        suggestions.get(0).click();
        return this;
    }

    /**
     * Set the text in the specified text box
     *
     * @param e       - The text box as a webElement
     * @param textStr - The String to be set on the TextBox
     * @return SpecifyDetailsPage
     */
    @Step("set Text box")
    public SpecifyDetailsPage setTextInTextBox(WebElement e, String textStr)
    {
        AppHelper.scrollToBottom();
        e.sendKeys(textStr);
        return this;
    }

    private ExpectedCondition<Boolean> suggestionsShown()
    {
        return driverExpected -> driver.findElements(
                By.cssSelector("[data-ng-repeat='match in matches'] a.ng-binding")).size() > 0;
    }

    /**
     * Create Case Of AIMC-In(ship) type
     *
     * @return CasesPage
     */
    @Step("Create AIMC In Ship Case")
    public CasesPage createAimcInShipCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .then().setCaseAcceptanceDate(caseAcceptanceDateData)
                .then().setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .then().setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .then().setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .then().setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .then().getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createAimcInShipCase()
    {
        return createAimcInShipCase(contractReferenceData);
    }

    /**
     * Create Case Of AIC type
     *
     * @return CasesPage
     */
    @Step("Create AIC Case")
    public CasesPage createAicCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .setCaseAcceptanceDate(caseAcceptanceDateData)
                .setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .clickBuildRuleSetButton()
                .clickSelectRuleSet()
                .selectRuleSetByIndex(1)
                .clickSelectButton()
                .selectClassStatus(1)
                .clickSingleButton()
                .setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createAicCase()
    {
        return createAicCase(contractReferenceData);
    }

    /**
     * Create Case Of First Entry type
     *
     * @return CasesPage
     */
    @Step("Create First Entry Case")
    public CasesPage createFirstEntryCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .then().setCaseAcceptanceDate(caseAcceptanceDateData)
                .then().setTextInTextBox(estimatedBuildDate, estimatedBuildDateData)
                .then().setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .then().setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .then().setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .then().clickBuildRuleSetButton()
                .then().clickSelectRuleSet()
                .then().selectRuleSetByIndex(1)
                .then().clickSelectButton()
                .then().selectClassStatus(1)
                .then().clickSingleButton()
                .then().setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .then().setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .then().getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createFirstEntryCase()
    {
        return createFirstEntryCase(contractReferenceData);
    }

    /**
     * Create Case Of TOC In type
     *
     * @return CasesPage
     */
    @Step("Create TOC In Case")
    public CasesPage createTocInCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .then().setCaseAcceptanceDate(caseAcceptanceDateData)
                .then().clickFavourablePreInspectionCheck()
                .then().clickFavourableRiskAssessmentCheck()
                .then().setTextBasedOnTypeAheadSuggestions(losingSociety, losingSocietyData)
                .then().setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .then().setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .then().setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .then().setTextBasedOnTypeAheadSuggestions(currentFlag, currentFlagData)
                .then().clickBuildRuleSetButton()
                .then().clickSelectRuleSet()
                .then().selectRuleSetByIndex(1)
                .then().clickSelectButton()
                .then().selectClassStatus(1)
                .then().clickSingleButton()
                .then().setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .then().setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .then().getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createTocInCase()
    {
        return createTocInCase(contractReferenceData);
    }

    /**
     * Create Case Of TOMC In Ship type
     *
     * @return CasesPage
     */
    @Step("Create TOMC In Ship Case")
    public CasesPage createTomcInShipCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .then().setCaseAcceptanceDate(caseAcceptanceDateData)
                .then().setTextBasedOnTypeAheadSuggestions(losingSociety, losingSocietyData)
                .then().setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .then().setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .then().setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .then().setTextBasedOnTypeAheadSuggestions(currentFlag, currentFlagData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .then().setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .then().getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createTomcInShipCase()
    {
        return createTomcInShipCase(contractReferenceData);
    }

    /**
     * Create Case Of Non Classed type
     *
     * @return CasesPage
     */
    @Step("Create Non Classed Case")
    public CasesPage createNonClassedCase(String contractReferenceData)
    {
        return setTextInTextBox(contractReference, contractReferenceData)
                .then().setCaseAcceptanceDate(caseAcceptanceDateData)
                .then().setTextInTextBox(grossTonnage, String.valueOf(grossTonnageData))
                .then().setTextBasedOnTypeAheadSuggestions(portOfRegistry, portOfRegistryData)
                .then().setTextBasedOnTypeAheadSuggestions(proposedFlag, proposedFlagData)
                .then().setTextBasedOnTypeAheadSuggestions(currentFlag, currentFlagData)
                .then().clickBuildRuleSetButton()
                .then().clickSelectRuleSet()
                .then().selectRuleSetByIndex(1)
                .then().clickSelectButton()
                .then().setTextBasedOnTypeAheadSuggestions(caseOffice, caseOfficeData)
                .then().setTextBasedOnTypeAheadSuggestions(caseOwner, caseOwnerData)
                .then().setTextBasedOnTypeAheadSuggestions(caseAdmin, caseAdminData)
                .then().getFooter().clickSaveAndCommitButton();
    }

    public CasesPage createNonClassedCase()
    {
        return createNonClassedCase(contractReferenceData);
    }

}
