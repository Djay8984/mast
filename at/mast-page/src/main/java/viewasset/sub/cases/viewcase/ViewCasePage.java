package viewasset.sub.cases.viewcase;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.cases.CasesPage;
import viewasset.sub.cases.viewcase.casedetails.CaseDetailsPage;
import viewasset.sub.cases.viewcase.milestones.MilestonesSubPage;
import viewasset.sub.cases.viewcase.modal.CancelCaseConfirmationWindow;

public class ViewCasePage extends BasePage<ViewCasePage>
{

    @Visible
    @Name("Navigate Back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Cancel case button")
    @FindBy(css = ".grid-block.small-5.align-right.btn-container button:nth-child(2)")
    private WebElement cancelCaseButton;

    @Name("Reason for cancelling message")
    @FindBy(css = "[data-ng-if='vm.isCancelled']")
    private WebElement reasonForCancellingMessage;

    @Name("Case status")
    @FindBy(css = "[data-ng-bind='vm.caseStatusName()']")
    private WebElement caseStatus;

    @Name("Milestones Tab")
    @FindBy(id = "asset-cases-view-milestones")
    private WebElement milestonesTab;

    @Name("Case details Tab")
    @FindBy(id = "asset-cases-view-details")
    private WebElement caseDetailsTab;

    @Step("Click Navigate Back button")
    public CasesPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Click Milestones tab")
    public MilestonesSubPage clickMilestonesTab()
    {
        AppHelper.scrollToBottom();
        milestonesTab.click();
        return PageFactory.newInstance(MilestonesSubPage.class);
    }

    @Step("Click Cancel case button")
    public CancelCaseConfirmationWindow clickCancelCaseButton()
    {
        cancelCaseButton.click();
        return PageFactory.newInstance(CancelCaseConfirmationWindow.class);
    }

    @Step("Get reason for cancelling")
    public String getReasonForCancelling()
    {
        return reasonForCancellingMessage.getText();
    }

    @Step("Get case status")
    public String getCaseStatus()
    {
        return caseStatus.getText();
    }

    @Step("Click Case Details Tab")
    public CaseDetailsPage clickCaseDetailsTab()
    {
        AppHelper.scrollToBottom();
        caseDetailsTab.click();
        return PageFactory.newInstance(CaseDetailsPage.class);
    }
}
