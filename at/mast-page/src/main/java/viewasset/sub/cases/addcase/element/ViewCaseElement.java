package viewasset.sub.cases.addcase.element;

import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.cases.viewcase.ViewCasePage;

@Name("Cases")
@FindBy(css = "[data-ng-repeat='case in vm.cases']")
public class ViewCaseElement extends HtmlElement
{

    @Name("Case Id")
    @FindBy(css = "[class='grid-content small-4'] > [class='ng-binding']")
    private WebElement caseId;

    @Name("Case Status")
    @FindBy(css = "[data-ng-bind='case.caseStatus.name']")
    private WebElement caseStatus;

    @Name("Case Type")
    @FindBy(css = "[data-ng-bind='case.caseType.name']")
    private WebElement caseType;

    @Name("Case CFO")
    @FindBy(css = "[data-ng-bind*='cfo']")
    private WebElement caseCFO;

    @Name("Case SDO")
    @FindBy(css = "[data-ng-bind*='caseSdo']")
    private WebElement caseSDO;

    @Name("Case owner")
    @FindBy(css = "[data-ng-bind*='vm.caseSurveyor']")
    private WebElement caseOwner;

    @Name("View case details")
    @FindBy(css = "[data-ng-click='vm.viewCase()']")
    private WebElement caseDetails;

    @Step("Returning the case ID")
    public String getCaseId()
    {
        return caseId.getText();
    }

    @Step("Returning the case status")
    public String getCaseStatus()
    {
        return caseStatus.getText();
    }

    @Step("Returning the case type")
    public String getCaseType()
    {
        return caseType.getText();
    }

    @Step("Returning the case CFO")
    public String getCaseCFO()
    {
        return caseCFO.getText();
    }

    @Step("Returning the case SDO")
    public String getCaseSDO()
    {
        return caseSDO.getText();
    }

    @Step("Returning the case owner")
    public String getCaseOwner()
    {
        return caseOwner.getText();
    }

    @Step("Is the view case details button present")
    public boolean isCaseDetailsButtonPresent()
    {
        return caseDetails.isDisplayed();
    }

    @Step("Click on the case details button")
    public ViewCasePage clickCaseDetails()
    {
        caseDetails.click();
        return PageFactory.newInstance(ViewCasePage.class);
    }
}
