package viewasset.sub.cases.addcase;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;
import viewasset.sub.cases.CasesPage;

public class SelectCaseTypePage extends BasePage<SelectCaseTypePage>
{

    @Visible
    @Name("AIMC-In (ship) Radio Button")
    @FindBy(css = "[for='case-type-1']")
    private RadioButton aimcInShipRadio;

    @Visible
    @Name("First Entry Radio Button")
    @FindBy(css = "[for='case-type-2']")
    private RadioButton firstEntryRadio;

    @Visible
    @Name("TOC In Radio Button")
    @FindBy(css = "[for='case-type-3']")
    private RadioButton tocInRadio;

    @Visible
    @Name("TOMC In Radio Button")
    @FindBy(css = "[for='case-type-4']")
    private RadioButton tomcInShipRadio;

    @Visible
    @Name("Non Classed Radio Button")
    @FindBy(css = "[for='case-type-5']")
    private RadioButton nonClassedRadio;

    @Visible
    @Name("AIC Radio Button")
    @FindBy(css = "[for='case-type-0']")
    private RadioButton aicRadio;

    @Visible
    @Name("Next Button")
    @FindBy(css = "[data-ng-click='vm.nextPage()']")
    private WebElement nextButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Click on Navigate Back Button")
    public CasesPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Select AIMC-In(ship) Radio Button")
    public SelectCaseTypePage selectAimcInShipRadioButton()
    {
        aimcInShipRadio.click();
        return this;
    }

    @Step("Select AIC Radio Button")
    public SelectCaseTypePage selectAicRadioButton()
    {
        aicRadio.click();
        return this;
    }

    @Step("Select FirstEntry Radio Button")
    public SelectCaseTypePage selectTocInRadioButton()
    {
        tocInRadio.click();
        return this;
    }

    @Step("Select FirstEntry Radio Button")
    public SelectCaseTypePage selectTomcInShipRadioButton()
    {
        tomcInShipRadio.click();
        return this;
    }

    @Step("Select FirstEntry Radio Button")
    public SelectCaseTypePage selectNonClassedRadioButton()
    {
        nonClassedRadio.click();
        return this;
    }

    @Step("Select FirstEntry Radio Button")
    public SelectCaseTypePage selectFirstEntryRadioButton()
    {
        firstEntryRadio.click();
        return this;
    }

    /**
     * @param pageObjectClass
     * @param <T>             SpecifyDetailsPage
     *                        CannotCreateCasePage
     * @return
     */
    @Step("Click Next Button")
    public <T extends BasePage<T>> T clickNextButton(Class<T> pageObjectClass)
    {
        nextButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

}
