package viewasset.sub.cases;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import header.HeaderPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.addcase.SelectCaseTypePage;
import viewasset.sub.cases.addcase.element.ViewCaseElement;
import workhub.WorkHubPage;

import java.util.List;

public class CasesPage extends BasePage<CasesPage>
{

    @Name("Asset Header Logo")
    @FindBy(id = "logo-asset-header-logo")
    private WebElement assetHeaderLogo;

    @Name("Asset Sub header")
    @FindBy(id = "asset-subheader-container")
    private WebElement assetSubHeader;

    @Name("Add new case")
    @Visible
    @FindBy(className = "primary-button")
    private WebElement addNewCase;

    @Name("No active cases message")
    @FindBy(css = "[data-ng-if='!vm.cases.length']")
    private WebElement noActiveCases;

    @Name("Amount of cases shown")
    @FindBy(css = "[data-ng-bind='vm.numberOfElements']")
    private WebElement amountOfCases;

    @Name("Total amount of cases")
    @FindBy(css = "[data-ng-bind='vm.totalElements']")
    private WebElement totalAmountOfCases;

    @Name("Load more")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private WebElement loadMore;

    @Name("Cases")
    private List<ViewCaseElement> cases;

    @Step("Click Mast Logo")
    public WorkHubPage clickMastLogo()
    {
        HeaderPage headerPage = PageFactory.newInstance(HeaderPage.class);
        return headerPage.clickMastLogo();
    }

    @Step("Is the add case button present")
    public boolean isAddCaseButtonPresent()
    {
        return driver.findElements(By.className("primary-button")).size() > 0;
    }

    @Step("Clicking on the add case button")
    public SelectCaseTypePage clickAddCaseButton()
    {
        waitForJavascriptFrameworkToFinish();
        addNewCase.click();
        return PageFactory.newInstance(SelectCaseTypePage.class);
    }

    @Step("Is the no active cases message shown")
    public boolean isNoActiveCasesMessageShown()
    {
        return noActiveCases.isDisplayed();
    }

    @Step("return the amount of cases shown")
    public String getCasesShown()
    {
        return amountOfCases.getText();
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("return the total amount of cases")
    public String getTotalCases()
    {
        return totalAmountOfCases.getText();
    }

    @Step("click the load more button")
    public CasesPage clickLoadMoreButton()
    {
        loadMore.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Returning the amount of cases on the page")
    public String amountOfCases()
    {
        return String.valueOf(cases.size());
    }

    @Step("Returning an attribute based on its name")
    public ViewCaseElement getCaseByCaseStatus(String caseStatus)
    {
        return cases.stream()
                .filter(o -> o.getCaseStatus().equals(caseStatus))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an attribute based on its name")
    public ViewCaseElement getCaseByCaseType(String caseType)
    {
        return cases.stream()
                .filter(o -> o
                        .getCaseType()
                        .contains(caseType))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an attribute based on row number")
    public ViewCaseElement getCaseByIndex(int rowNumber)
    {
        AppHelper.scrollToBottom();
        return cases.get(rowNumber);
    }

    @Step("Click Asset Header Logo")
    public WorkHubPage clickAssetHeaderLogo()
    {
        assetHeaderLogo.click();
        return PageFactory.newInstance(WorkHubPage.class);
    }

    @Step(" Check for Asset commonpage-header")
    public boolean isAssetSubHeaderDisplayed()
    {
        return assetSubHeader.isDisplayed();
    }

    @Step("Get Asset Model Subpage")
    public ViewAssetPage getViewAssetPage()
    {
        return PageFactory.newInstance(ViewAssetPage.class);
    }
}
