package viewasset.sub.cases.viewcase.casedetails;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class CaseDetailsPage extends BasePage<CaseDetailsPage>
{

    @Visible
    @Name("Edit Button")
    @FindBy(css = "[data-ng-if='vm.canEditCase']")
    private WebElement editButton;

    //Case Details
    @Name("Contract Reference")
    @FindBy(css = "[data-ng-bind*='contractReferenceNumber']")
    private WebElement contractReference;

    @Name("Case Creation")
    @FindBy(css = "[data-ng-bind*='assetCase.createdOn']")
    private WebElement caseCreation;

    @Name("Pre inspection checks")
    @FindBy(css = "[data-ng-bind*='preInspectionChecks']")
    private WebElement preInspectionChecks;

    @Name("TOC Acceptance Date")
    @FindBy(css = "[data-ng-bind*='tocAcceptanceDate']")
    private WebElement tOCAcceptanceDate;

    @Name("Losing Society")
    @FindBy(css = "[data-ng-bind*='losingSociety']")
    private WebElement losingSociety;

    //Asset Details
    @Name("Asset Name")
    @FindBy(css = "[data-ng-bind*='assetCase.assetName']")
    private WebElement assetName;

    @Name("IMo Number")
    @FindBy(css = "[data-ng-bind*='assetCase.imoNumber']")
    private WebElement iMoNumber;

    @Name("Gross tonnage")
    @FindBy(css = "[data-ng-show='vm.assetCase.grossTonnage']")
    private WebElement grossTonnage;

    @Name("Lifecycle Status")
    @FindBy(css = "[data-ng-bind*='vm.lifecycleStatus']")
    private WebElement lifecycleStatus;

    @Name("Asset Type")
    @FindBy(css = "[data-ng-bind*='vm.assetType']")
    private WebElement assetType;

    @Name("Class Status")
    @FindBy(css = "[data-ng-bind*='vm.classStatus']")
    private WebElement classStatus;

    //Flag Details
    @Name("Current Flag")
    @FindBy(css = "[data-ng-bind*='vm.currentFlag']")
    private WebElement currentFlag;

    @Name("Port of Registry")
    @FindBy(css = "[data-ng-bind*='vm.registeredPort']")
    private WebElement portOfRegistry;

    @Name("Proposed flag ")
    @FindBy(css = "[data-ng-bind*='vm.proposedFlag']")
    private WebElement proposedFlag;

    //Rule set
    @Name("Build rule set")
    @FindBy(css = "[data-ng-bind*='vm.ruleSet']")
    private WebElement buildRuleSet;

    //Class maintenance regime
    @Name("Class maintenance regime")
    @FindBy(css = "[data-ng-bind*='classMaintenanceStatus']")
    private WebElement classMaintenanceRegime;

    //Key dates
    @Name("Date of Build")
    @FindBy(css = "[data-ng-bind*='assetCase.buildDate']")
    private WebElement dateOfBuild;

    @Name("Keel laying date")
    @FindBy(css = "[data-ng-bind*='assetCase.keelLayingDate']")
    private WebElement keelLayingDate;

    //Offices and Surveyors
    @Name("Case SDO")
    @FindBy(css = "[data-ng-bind*='caseSdo']")
    private WebElement caseSdo;

    @Name("Case Owner")
    @FindBy(css = "span[data-ng-bind*='management']")
    private WebElement caseOwner;

    @Name("Case Admin")
    @FindBy(css = "span[data-ng-bind*='eicAdmin']")
    private WebElement caseAdmin;

    @Name("CFO")
    @FindBy(css = "span[data-ng-bind*='cfo']")
    private WebElement cfo;

    @Name("Job SDO")
    @FindBy(css = "span[data-ng-bind*='jobSdo']")
    private WebElement jobSdo;

    @Name("TSO")
    @FindBy(css = "[data-ng-bind*='tso']")
    private WebElement tso;

    @Name("TSO Stat")
    @FindBy(css = "[data-ng-bind*='tsoStat']")
    private WebElement tsoStat;

    @Name("Lead surveyor")
    @FindBy(css = "[data-ng-bind*='leadSurveyor']")
    private WebElement leadSurveyor;

    @Step("get Asset Name")
    public String getAssetName()
    {
        return assetName.getText();
    }

    @Step("get Case Owner")
    public String getCaseOwner()
    {
        return caseOwner.getText();
    }

    @Step("get Case Admin")
    public String getCaseAdmin()
    {
        return caseAdmin.getText();
    }

    @Step("get Contract Reference")
    public String getContractReference()
    {
        return contractReference.getText();
    }

    @Step("get Contract Reference")
    public String getCaseSdo()
    {
        return caseSdo.getText();
    }

    @Step("Click Edit button")
    public EditCasePage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(EditCasePage.class);
    }

    @Step("Get Case Creation")
    public String getCaseCreation()
    {
        return caseCreation.getText().trim();
    }

    @Step("Get Pre Inspection Checks")
    public String getPreInspectionChecks()
    {
        return preInspectionChecks.getText().trim();
    }

    @Step("Get TOCAcceptance Date")
    public String getTOCAcceptanceDate()
    {
        return tOCAcceptanceDate.getText().trim();
    }

    @Step("Get Losing Society")
    public String getLosingSociety()
    {
        return losingSociety.getText().trim();
    }

    @Step("Get IMoNumber")
    public String getIMoNumber()
    {
        return iMoNumber.getText().trim();
    }

    @Step("Get Gross Tonnage")
    public String getGrossTonnage()
    {
        return grossTonnage.getText().trim();
    }

    @Step("Get Lifecycle Status")
    public String getLifecycleStatus()
    {
        return lifecycleStatus.getText().trim();
    }

    @Step("Get Asset Type")
    public String getAssetType()
    {
        return assetType.getText().trim();
    }

    @Step("Get Class Status")
    public String getClassStatus()
    {
        return classStatus.getText().trim();
    }

    @Step("Get Current Flag")
    public String getCurrentFlag()
    {
        return currentFlag.getText().trim();
    }

    @Step("Get Port Of Registry")
    public String getPortOfRegistry()
    {
        return portOfRegistry.getText().trim();
    }

    @Step("Get Proposed Flag")
    public String getProposedFlag()
    {
        return proposedFlag.getText().trim();
    }

    @Step("Get BuildRule Set")
    public String getBuildRuleSet()
    {
        return buildRuleSet.getText().trim();
    }

    @Step("Get Class Maintenance Regime")
    public String getClassMaintenanceRegime()
    {
        return classMaintenanceRegime.getText().trim();
    }

    @Step("Get DateOfBuild")
    public String getDateOfBuild()
    {
        return dateOfBuild.getText().trim();
    }

    @Step("Get KeelLaying Date")
    public String getKeelLayingDate()
    {
        return keelLayingDate.getText().trim();
    }

    @Step("Get CFO")
    public String getCfo()
    {
        return cfo.getText().trim();
    }

    @Step("Get Job SDO")
    public String getJobSdo()
    {
        return jobSdo.getText().trim();
    }

    @Step("Get TSO")
    public String getTso()
    {
        return tso.getText().trim();
    }

    @Step("Get TSO Stat")
    public String getTsoStat()
    {
        return tsoStat.getText().trim();
    }

    @Step("Get Lead Surveyor")
    public String getLeadSurveyor()
    {
        return leadSurveyor.getText().trim();
    }
}
