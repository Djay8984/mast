package viewasset.sub.cases.viewcase.milestones;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.cases.viewcase.ViewCasePage;
import viewasset.sub.cases.viewcase.milestones.element.CaseMilestoneElement;

import java.util.List;
import java.util.stream.Collectors;

public class MilestonesSubPage extends BasePage<MilestonesSubPage>
{

    @Name("Manage Milestones Button")
    @FindBy(css = "[data-ui-sref='asset.cases.view.manage-milestones']")
    private WebElement manageMileStonesButton;

    @Name("Milestone Element")
    @FindBy(css = "[data-ng-repeat='milestone in vm.milestones | filter:{ inScope: vm.scope }']")
    private List<CaseMilestoneElement> milestoneList;

    @Name("Milestone Checkbox")
    @FindBy(css = "[data-ng-model='milestone.reviewed']")
    private List<WebElement> milestoneCheckbox;

    @Name("Save Changes to Completed Milestones Button")
    @FindBy(id = "save-changes-button")
    private WebElement saveChangesToMilestonesButton;

    @Name("Confirm Complete Milestone Button")
    @FindBy(css = "[data-ng-click='$close(true)']")
    private WebElement confirmCompleteMilestoneButton;

    @Step("Click manage milestones button to navigate to the milestone manager.")
    public ManageMilestonesPage clickManageMilestonesButton()
    {
        manageMileStonesButton.click();
        return PageFactory.newInstance(ManageMilestonesPage.class);
    }

    @Step("Check a milestone from the list (for completion) by the index in the list.")
    public MilestonesSubPage checkMilestoneCheckboxByIndex(int index)
    {
        AppHelper.scrollToBottom();
        milestoneCheckbox.get(index).click();
        return this;
    }

    @Step("Click to save the changes to the milestones.")
    public MilestonesSubPage clickSaveChangesToMilestones()
    {
        saveChangesToMilestonesButton.click();
        return this;
    }

    @Step("Click to confirm the changes from the pop up that appears.")
    public MilestonesSubPage clickConfirmCompleteMilestoneButton()
    {
        confirmCompleteMilestoneButton.click();
        ViewCasePage updatedViewCasePage = PageFactory.newInstance(ViewCasePage.class);
        return updatedViewCasePage.clickMilestonesTab();
    }

    @Step("Return a milestone element by index")
    public CaseMilestoneElement getMilestoneByIndex(int index)
    {
        return milestoneList.get(index);
    }

    @Step("Get the text for the completion status")
    public String getCompletionText(int index)
    {
        return milestoneList.get(index).findElement(By.className("completed")).getText();
    }

    @Step("Return a list of the available checkboxes")
    public List<CaseMilestoneElement> getAllCheckboxes()
    {
        return milestoneList.stream()
                .filter(e -> e.isEnabled())
                .collect(Collectors.toList());
    }
}
