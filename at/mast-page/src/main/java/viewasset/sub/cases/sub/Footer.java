package viewasset.sub.cases.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.cases.CasesPage;

public class Footer extends BasePage<Footer>
{

    @Name("Save In Progress Button")
    @FindBy(css = "#add-case-save-in-progress")
    private WebElement saveInProgressButton;

    @Name("Save And Commit Button")
    @FindBy(css = "#add-case-save-and-commit")
    private WebElement saveAndCommitButton;

    @Visible
    @Name("Cancel Add Case Button")
    @FindBy(css = "#add-case-cancel")
    private WebElement cancelAddCaseButton;

    @Step("Click on SaveInProgress button")
    public CasesPage clickSaveInProgressButton()
    {
        saveInProgressButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Click on Save And Commit button")
    public CasesPage clickSaveAndCommitButton()
    {
        AppHelper.scrollToBottom();
        saveAndCommitButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Click on Cancel Add Case button")
    public CasesPage clickCancelAddCaseButton()
    {
        cancelAddCaseButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

}
