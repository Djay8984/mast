package viewasset.sub.cases.viewcase.casedetails;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Select;
import util.AppHelper;

public class EditCasePage extends BasePage<EditCasePage>
{

    final String selectorElement = "[data-ng-repeat='match in matches'] a.ng-binding";

    //Case Details
    @Visible
    @Name("Contract Reference TextBox")
    @FindBy(id = "contract-reference-number")
    private WebElement contractReference;

    //Asset Details
    @Visible
    @Name("Asset Name")
    @FindBy(id = "case-asset-name")
    private WebElement assetName;

    @Visible
    @Name("Gross tonnage")
    @FindBy(id = "case-gross-tonnage")
    private WebElement grossTonnage;

    @Visible
    @Name("Lifecycle status")
    @FindBy(id = "case-asset-lifecycle-status")
    private Select lifecycleStatusDropDown;

    @Visible
    @Name("Class status")
    @FindBy(id = "case-class-status")
    private Select classStatusDropDown;

    //Flag Details
    @Visible
    @Name("Port of registry Text Box")
    @FindBy(id = "registered-port")
    private WebElement portOfRegistryTextBox;

    @Visible
    @Name("Current Flag")
    @FindBy(id = "flag-state")
    private WebElement currentFlagTextBox;

    @Visible
    @Name("Proposed flag")
    @FindBy(id = "proposed-flag-state")
    private WebElement proposedFlagTextBox;

    //Rule set
    @Visible
    @Name("Build Rule Set Button")
    @FindBy(css = "[data-ng-click='vm.buildRuleSet()']")
    private WebElement buildRuleSetButton;

    //Class maintenance regime
    @Visible
    @Name("Single Button")
    @FindBy(css = "[data-ng-repeat*='classMaintenanceStatus']:nth-child(1)")
    private WebElement singleButton;

    @Visible
    @Name("Dual Button")
    @FindBy(css = "[data-ng-repeat*='classMaintenanceStatus']:nth-child(2)")
    private WebElement dualButton;

    @Name("Co-classification society ")
    @FindBy(id = "co-classificiation-society")
    private WebElement coClassificationSociety;

    //Key dates
    @Visible
    @Name("Keel laying date")
    @FindBy(css = "[data-ng-model='vm.model.keelLayingDate']")
    private WebElement keelLayingDate;

    //Offices and surveyors
    @Visible
    @Name("Case Office")
    @FindBy(id = "case-sdo")
    private WebElement caseOffice;

    @Visible
    @Name("Case Owner")
    @FindBy(id = "case-owner")
    private WebElement caseOwner;

    @Visible
    @Name("Case Admin")
    @FindBy(id = "case-admin")
    private WebElement caseAdmin;

    @Visible
    @Name("Case CFO")
    @FindBy(id = "case-cfo")
    private WebElement caseCfo;

    @Visible
    @Name("Job SDO")
    @FindBy(id = "job-sdo")
    private WebElement jobSdo;

    @Visible
    @Name("Lead Surveyor")
    @FindBy(id = "lead-surveyor")
    private WebElement leadSurveyor;

    @Visible
    @Name("TSO")
    @FindBy(id = "tso")
    private WebElement tso;

    @Visible
    @Name("TSO stat")
    @FindBy(id = "tso-stat")
    private WebElement tsoStat;

    @Visible
    @Name("Save Button")
    @FindBy(css = "[data-ng-click='vm.save(true)']")
    private WebElement saveButton;

    @Step("set Text in Case Office")
    public EditCasePage setContractReference(String text)
    {
        AppHelper.scrollToBottom();
        contractReference.clear();
        contractReference.sendKeys(text);
        return this;
    }

    @Step("set Text in Case Office")
    public EditCasePage setAssetName(String asset)
    {
        AppHelper.scrollToBottom();
        assetName.clear();
        assetName.sendKeys(asset);
        return this;
    }

    @Step("Set Gross Tonnage")
    public EditCasePage setGrossTonnage(String grossTon)
    {
        AppHelper.scrollToBottom();
        grossTonnage.clear();
        grossTonnage.sendKeys(grossTon);
        return this;
    }

    @Step("Select Lifecycle Status ")
    public EditCasePage selectLifecycleStatusDropdown(int index)
    {
        AppHelper.scrollToBottom();
        lifecycleStatusDropDown.selectByIndex(index);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select Class Status ")
    public EditCasePage selectClassStatusDropdown(int index)
    {
        AppHelper.scrollToBottom();
        classStatusDropDown.selectByIndex(index);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set Port of registry")
    public EditCasePage setPortOfRegistry(String text)
    {
        AppHelper.scrollToBottom();
        portOfRegistryTextBox.clear();
        portOfRegistryTextBox.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set Current Flag")
    public EditCasePage setCurrentFlag(String text)
    {
        AppHelper.scrollToBottom();
        currentFlagTextBox.clear();
        currentFlagTextBox.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set Proposed Flag")
    public EditCasePage setProposedFlag(String text)
    {
        AppHelper.scrollToBottom();
        proposedFlagTextBox.clear();
        proposedFlagTextBox.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set Case CFO")
    public EditCasePage setCaseCfo(String text)
    {
        AppHelper.scrollToBottom();
        caseCfo.clear();
        caseCfo.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set Job SDO")
    public EditCasePage setJobSdo(String text)
    {
        AppHelper.scrollToBottom();
        jobSdo.clear();
        jobSdo.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set Lead surveyor")
    public EditCasePage setLeadSurveyor(String text)
    {
        AppHelper.scrollToBottom();
        leadSurveyor.clear();
        leadSurveyor.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set TSO")
    public EditCasePage setTso(String text)
    {
        AppHelper.scrollToBottom();
        tso.clear();
        tso.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Set TSO Stat")
    public EditCasePage setTsoStat(String text)
    {
        AppHelper.scrollToBottom();
        tsoStat.clear();
        tsoStat.sendKeys(text);
        driver.findElements(By.cssSelector
                (selectorElement))
                .get(0)
                .click();
        return this;
    }

    @Step("Click Single Button")
    public EditCasePage clickSingleButton()
    {
        AppHelper.scrollToBottom();
        singleButton.click();
        return this;
    }

    @Step("Click Dual Button")
    public EditCasePage clickDualButton()
    {
        dualButton.click();
        return this;
    }

    @Step("Click Save Button")
    public CaseDetailsPage clickSaveButton()
    {
        AppHelper.scrollToBottom();
        saveButton.click();
        return PageFactory.newInstance(CaseDetailsPage.class);
    }

    @Step("Get  Lifecycle Status by Index")
    public String getLifecycleStatusDropdownTest(int index)
    {
        return lifecycleStatusDropDown.getOptions().get(index).getText().trim();
    }

    @Step("Get  Class Status by Index")
    public String getClassStatusDropdownText(int index)
    {
        return classStatusDropDown.getOptions().get(index).getText().trim();
    }
}
