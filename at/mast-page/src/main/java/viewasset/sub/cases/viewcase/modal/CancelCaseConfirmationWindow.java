package viewasset.sub.cases.viewcase.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.cases.viewcase.ViewCasePage;

public class CancelCaseConfirmationWindow extends BasePage<CancelCaseConfirmationWindow>
{

    @Visible
    @Name("Cancel case button")
    @FindBy(css = "[data-ng-click='vm.close(true)']")
    private WebElement cancelCaseButton;

    @Visible
    @Name("Reason for cancelling textarea")
    @FindBy(css = "[data-ng-if='vm.isCancel']")
    private WebElement reasonForCancellingTextarea;

    @Step("Click Cancel case button")
    public ViewCasePage clickCancelCaseButton()
    {
        cancelCaseButton.click();
        return PageFactory.newInstance(ViewCasePage.class);
    }

    @Step("Set reason for cancelling")
    public CancelCaseConfirmationWindow setReasonForCancelling(String reason)
    {
        reasonForCancellingTextarea.sendKeys(reason);
        return PageFactory.newInstance(CancelCaseConfirmationWindow.class);
    }
}
