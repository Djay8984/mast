package viewasset.sub.cases.addcase.select;

import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Select;
import viewasset.sub.cases.addcase.SpecifyDetailsPage;

public class SelectRuleSet extends BasePage<SelectRuleSet>
{

    @Name("Select Button")
    @FindBy(css = "modal-footer .primary-button")
    private Button selectButton;

    @Name("Cancel Button")
    @FindBy(css = "modal-footer [data-ng-click='$dismiss()']")
    private Button cancelButton;

    @Name("Select Rule Set DropDown")
    @FindBy(css = "modal-body #rule-set")
    private Select selectRuleSetDropdown;

    @Step("Selecting a rule set from the dropdown")
    public SelectRuleSet selectRuleSetByIndex(int index)
    {
        selectRuleSetDropdown.selectByIndex(index);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Selecting a rule set from the dropdown")
    public SelectRuleSet clickSelectRuleSet()
    {
        selectRuleSetDropdown.click();
        return this;
    }

    @Step("Click Select Button")
    public SpecifyDetailsPage clickSelectButton()
    {
        selectButton.click();
        return PageFactory.newInstance(SpecifyDetailsPage.class);
    }

}
