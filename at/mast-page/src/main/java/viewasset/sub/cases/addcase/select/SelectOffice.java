package viewasset.sub.cases.addcase.select;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;

public class SelectOffice extends BasePage<SelectOffice>
{

    @Name("assign Selected Button")
    @FindBy(css = "modal-footer .primary-button")
    private Button assignSelectedButton;

    @Name("Cancel Button")
    @FindBy(css = "modal-footer [data-ng-click='$dismiss()']")
    private Button cancelButton;

    @Name("Select Office DropDown")
    @FindBy(css = "#office-types")
    private Button selectOfficeDropDown;

    @Name("Office Code")
    @FindBy(css = "[data-ng-repeat=\"office in vm.extraOffices\"]  .ng-pristine")
    private Button officeCode;

}
