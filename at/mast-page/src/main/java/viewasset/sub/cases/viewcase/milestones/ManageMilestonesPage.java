package viewasset.sub.cases.viewcase.milestones;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.cases.viewcase.milestones.element.CaseMilestoneElement;

import java.util.List;
import java.util.stream.Collectors;

public class ManageMilestonesPage extends BasePage<ManageMilestonesPage>
{

    @Name("In scope tab")
    @Visible
    @FindBy(id = "milestone-management-view-inscope")
    private WebElement inScopeTab;

    @Name("Out of Scope Tab")
    @Visible
    @FindBy(id = "milestone-management-view-outscope")
    private WebElement outScopeTab;

    @Name("Manage milestone elements")
    @FindBy(css = "[data-ng-repeat='milestone in vm.milestones | filter:{ inScope: vm.scope }']")
    private List<CaseMilestoneElement> manageMilestonesElements;

    @Name("Mark as out of scope button")
    @FindBy(css = "[data-ng-click='vm.changeScope(false, vm.inScope)'")
    private WebElement markAsOutOfScopeButton;

    @Name("Mark as in scope button")
    @FindBy(css = "[data-ng-click='vm.changeScope(true, vm.inScope)']")
    private WebElement markAsInScopeButton;

    @Name("Save and confirm button")
    @FindBy(css = "[data-ng-click='vm.save()']")
    private WebElement saveAndConfirmButton;

    public CaseMilestoneElement getManageMilestoneElementByIndex(int index)
    {
        return manageMilestonesElements.get(index);
    }

    public List<CaseMilestoneElement> getManageMilestoneCheckboxes()
    {
        return manageMilestonesElements;
    }

    @Step("Get all the available checkboxes")
    public List<String> getScopeChangeableDescriptions()
    {
        return getManageMilestoneCheckboxes()
                .stream()
                .filter(CaseMilestoneElement::isMilestoneCheckboxDisplayed)
                .map(CaseMilestoneElement::getMilestoneDescription)
                .collect(Collectors.toList());
    }

    @Step("Check all available checkboxes")
    public ManageMilestonesPage checkAvailableCheckboxes()
    {
        getManageMilestoneCheckboxes()
                .stream()
                .filter(CaseMilestoneElement::isMilestoneCheckboxDisplayed)
                .forEach(CaseMilestoneElement::clickMilestoneCheckbox);
        return this;
    }

    @Step("Click the out of scope button")
    public ManageMilestonesPage clickMilestoneOutOfScope()
    {
        AppHelper.scrollToBottom();
        waitForJavascriptFrameworkToFinish();
        markAsOutOfScopeButton.click();
        return this;
    }

    @Step("Click the out of scope button")
    public ManageMilestonesPage clickMilestoneInScope()
    {
        AppHelper.scrollToBottom();
        waitForJavascriptFrameworkToFinish();
        markAsInScopeButton.click();
        return this;
    }

    @Step("Click the save and confirm button")
    public MilestonesSubPage clickSaveAndConfirm()
    {
        saveAndConfirmButton.click();
        return PageFactory.newInstance(MilestonesSubPage.class);
    }

    @Step("Change to the out of scope tab")
    public ManageMilestonesPage clickOutOfScopeTab()
    {
        outScopeTab.click();
        return this;
    }

    @Step("Click the in scope tab")
    public ManageMilestonesPage clickInScopeTab()
    {
        AppHelper.scrollToBottom();
        wait.until(ExpectedConditions.visibilityOf(inScopeTab));
        inScopeTab.click();
        return this;
    }
}
