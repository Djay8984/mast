package viewasset.sub.codicilsanddefects.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class TypesAndCategorySubPage extends BasePage<TypesAndCategorySubPage>
{

    @Visible
    @Name("Select All Button")
    @FindBy(id = "codicil-search-filter-status-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear All Button")
    @FindBy(id = "codicil-search-filter-status-clear-all")
    private WebElement clearAllButton;

    @Visible
    @Name("Type and Category Filter CheckBoxes")
    @FindBy(css = "#codicil-category-filter .grid-content.ng-scope")
    private List<WebElement> typeAndCategoryFilterCheckBoxes;

    @Visible
    @Name("Asset Note Category Filter CheckBoxes")
    @FindBy(css = "#codicil-category-filter div:nth-child(1) li")
    private List<WebElement> assetNoteFilterCheckBoxes;

    @Visible
    @Name("Actionable Item Filter CheckBoxes")
    @FindBy(css = "#codicil-category-filter .grid-block.horizontal>div:nth-child(2) li")
    private List<WebElement> actionableItemFilterCheckBoxes;

    @Visible
    @Name("Condition Of Class Filter CheckBoxes")
    @FindBy(css = "#codicil-category-filter div:nth-child(3) li")
    private List<WebElement> conditionOfClassFilterCheckBoxes;

    @Visible
    @Name("Defect Filter CheckBoxes")
    @FindBy(css = "#codicil-category-filter div:nth-child(4) li")
    private List<WebElement> defectFilterCheckBoxes;

    @Step("Check for Types and Category CheckBoxes are selected")
    public boolean isTypeAndCategoryFilterCheckBoxesSelected(String type)
    {
        return typeAndCategoryFilterCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector(".ng-binding"))
                        .getText().equals(type))
                .findFirst()
                .get()
                .findElement(By.cssSelector(".checkbox-dir"))
                .getAttribute("class").contains("checked");
    }

    @Step("Check for Asset Note commonpage checkboxes are selected")
    public boolean isAssetNoteSubCheckBoxesSelected(String type)
    {
        return assetNoteFilterCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector(".ng-binding"))
                        .getText().equals(type))
                .findFirst()
                .get()
                .findElement(By.cssSelector(".checkbox-dir"))
                .getAttribute("class").contains("checked");
    }

    @Step("Check for Actionable Items commonpage checkboxes are selected")
    public boolean isActionableSubCheckBoxesSelected(String type)
    {
        return actionableItemFilterCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector(".ng-binding"))
                        .getText().equals(type))
                .findFirst()
                .get()
                .findElement(By.cssSelector(".checkbox-dir"))
                .getAttribute("class").contains("checked");
    }

    @Step("Check for Coc commonpage checkboxes are selected")
    public boolean isCocSubCheckBoxesSelected(String type)
    {
        return conditionOfClassFilterCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector(".ng-binding"))
                        .getText().equals(type))
                .findFirst()
                .get()
                .findElement(By.cssSelector(".checkbox-dir"))
                .getAttribute("class").contains("checked");
    }

    @Step("Check for Defect commonpage checkboxes are selected")
    public boolean isDefectSubCheckBoxesSelected(String type)
    {
        return defectFilterCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector(".ng-binding"))
                        .getText().equals(type))
                .findFirst()
                .get()
                .findElement(By.cssSelector(".checkbox-dir"))
                .getAttribute("class").contains("checked");
    }

    @Step("Click Select All Button")
    public TypesAndCategorySubPage clickSelectAllButton()
    {
        selectAllButton.click();
        return this;
    }

    @Step("Click Clear All Button")
    public TypesAndCategorySubPage clickClearAllButton()
    {
        clearAllButton.click();
        return this;
    }
}
