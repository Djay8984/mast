package viewasset.sub.codicilsanddefects.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;

import static constant.LloydsRegister.NUMBER_REGEX;

@Name("Conditions Of Class")
@FindBy(css = "[data-ng-repeat ='item in vm.cocs']")
public class ConditionsOfClassElement extends HtmlElement
{

    @Visible
    @Name("COC Name")
    @FindBy(className = "codicil-title")
    private WebElement cocTitle;

    @Visible
    @Name("COC Id")
    @FindBy(css = ".codicil-title small")
    private WebElement cocId;

    @Name("Repairs")
    @FindBy(css = "[data-ng-if='vm.item.repairs.length']")
    private WebElement repairs;

    @Visible
    @Name("Impose Date")
    @FindBy(css = "[data-ng-bind='vm.imposedDate']")
    private WebElement imposeDate;

    @Visible
    @Name("Due Date")
    @FindBy(css = "[data-ng-bind='vm.dueDate']")
    private WebElement dueDate;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Coc status")
    @FindBy(css = "[data-ng-bind='vm.getCodicilStatus(item.status.id)']")
    private WebElement cocStatus;

    @Step("Get COC ID")
    public String getCoCId()
    {
        return cocId.getText();
    }

    @Step("Get COC Name")
    public String getCocTitle()
    {
        return cocTitle.getText().replaceAll(NUMBER_REGEX, "");
    }

    @Step("Return impose date")
    public String getImposeDate()
    {
        return imposeDate.getText();
    }

    @Step("Return due date")
    public String getDueDate()
    {
        return dueDate.getText();
    }

    @Step("Return coc Status")
    public String getCocStatus()
    {
        return cocStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public ViewConditionOfClassPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }

    @Step("Get COC status Text")
    public String getCocStatusText()
    {
        return cocStatus.getText().trim();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

}
