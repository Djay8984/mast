package viewasset.sub.codicilsanddefects.actionableitems.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.ViewActionableItemPage;

public class ConfirmCancelPage extends BasePage<ConfirmCancelPage>
{

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(2)")
    private WebElement cancelButton;

    @Visible
    @Name("Go back button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(1)")
    private WebElement goBackButton;

    @Step("Click Go Back button")
    public ViewActionableItemPage clickGoBackButton()
    {
        goBackButton.click();
        return PageFactory.newInstance(ViewActionableItemPage.class);
    }

    @Step("Click Cancel button")
    public CodicilsAndDefectsPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }
}
