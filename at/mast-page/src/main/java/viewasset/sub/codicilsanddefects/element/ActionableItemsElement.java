package viewasset.sub.codicilsanddefects.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.actionableitems.ActionableItemPage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static constant.LloydsRegister.NUMBER_REGEX;

@Name("Actionable Items")
@FindBy(css = "[data-ng-repeat='item in vm.actionableItems']")
public class ActionableItemsElement extends HtmlElement
{

    @Visible
    @Name("Actionable Items Title ")
    @FindBy(css = ".codicil-title")
    private WebElement actionableItemsTitle;

    @Visible
    @Name("Actionable item ID")
    @FindBy(css = "[data-ng-bind*='vm.item.id']")
    private WebElement actionableItemId;

    @Name("Category")
    @FindBy(css = ".codicil-content>li")
    private WebElement category;

    @Name("Defect")
    @FindBy(css = ".codicil-content li:nth child(2)")
    private WebElement defect;

    @Name("Item")
    @FindBy(css = "[data-ng-bind*='vm.item.assetItem.name || vm.item.asset.name']")
    private WebElement item;

    @Visible
    @Name("Impose Date")
    @FindBy(css = "[data-ng-bind*='vm.item.imposedDate']")
    private WebElement imposeDate;

    @Visible
    @Name("Due Date")
    @FindBy(css = "[data-ng-bind*='vm.item.dueDate']")
    private WebElement dueDate;

    @Visible
    @Name("Status")
    @FindBy(css = "[data-ng-bind='vm.item.status.name']")
    private WebElement status;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Step("Get Actionable Items Title")
    public String getActionableItemsTitle()
    {
        return actionableItemsTitle.getText().replaceAll(NUMBER_REGEX, "");
    }

    @Step("Return the actionable item ID")
    public String getActionableItemId()
    {
        return actionableItemId.getText();
    }

    @Step("Return the actionable item category")
    public String getActionableItemCategory()
    {
        return category.getText();
    }

    @Step("Return the actionable item defect")
    public String getActionableItemDefect()
    {
        return defect.getText();
    }

    @Step("Return the item affected")
    public String getItemAffected()
    {
        return item.getText();
    }

    @Step("Return the impose date")
    public String getImposeDate()
    {
        return imposeDate.getText();
    }

    @Step("Return the due date")
    public String getDueDate()
    {
        return dueDate.getText();
    }

    @Step("Return the actionable item status")
    public String getActionableItemStatus()
    {
        return status.getText();
    }

    @Step("Click on Further Details Arrow")
    public ActionableItemPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(ActionableItemPage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
