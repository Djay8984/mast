package viewasset.sub.codicilsanddefects.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;

import static constant.LloydsRegister.NUMBER_REGEX;

public class ActionableItemPage extends BasePage<ActionableItemPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private Button navigateBackButton;

    @Name("Actionable Item Name")
    @FindBy(css = "[data-ng-bind='data.obj.title']")
    private WebElement actionableItemText;

    @Name("Actionable Item Description")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement actionableItemDescription;

    @Step("Get Actionable Item Description Text")
    public String getActionableItemDescriptionText()
    {
        return actionableItemDescription.getText()
                .trim().replaceAll(NUMBER_REGEX, "").trim().toLowerCase();
    }

    @Step("Click Navigate Back Button")
    public CodicilsAndDefectsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Get Actionable Item Text")
    public String getActionableItemTitleText()
    {
        return actionableItemText.getText().toLowerCase().trim();
    }
}
