package viewasset.sub.codicilsanddefects.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.assetnotes.AssetNotePage;

@Name("Asset Notes")
@FindBy(css = "[data-ng-repeat='item in vm.assetNotes']")
public class AssetNotesElement extends HtmlElement
{

    @Visible
    @Name("Asset Note Title ")
    @FindBy(css = ".codicil-title")
    private WebElement assetNoteTitle;

    @Visible
    @Name("Asset note ID")
    @FindBy(css = "[data-ng-bind*='vm.item.id']")
    private WebElement assetNoteId;

    @Visible
    @Name("Asset note category")
    @FindBy(css = "[data-ng-bind='vm.item.category.name']")
    private WebElement assetNoteCategory;

    @Name("Asset note item")
    @FindBy(css = "data-ng-bind*='vm.item.assetItem.name || vm.item.asset.name]")
    private WebElement assetNoteItem;

    @Visible
    @Name("Asset note impose date")
    @FindBy(css = "[data-ng-bind*='vm.item.imposedDate']")
    private WebElement assetNoteImposeDate;

    @Visible
    @Name("Asset note status")
    @FindBy(css = "[data-ng-bind='vm.item.status.name']")
    private WebElement assetNoteStatus;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Step("Get asset note title")
    public String getAssetNoteTitle()
    {
        return assetNoteTitle.getText();
    }

    @Step("Return the asset note ID")
    public String getAssetNoteId()
    {
        return assetNoteId.getText();
    }

    @Step("Return the asset note category")
    public String getAssetNoteCategory()
    {
        return assetNoteCategory.getText();
    }

    @Step("Return the asset note impose date")
    public String getAssetNoteImposeDate()
    {
        return assetNoteImposeDate.getText();
    }

    @Step("Return the asset note status")
    public String getAssetNoteStatus()
    {
        return assetNoteStatus.getText();
    }

    @Step("Click on Further Details Arrow")
    public AssetNotePage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(AssetNotePage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
