package viewasset.sub.codicilsanddefects.actionableitems.template;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Radio;
import util.AppHelper;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;

import java.util.List;

public class AddActionableItemTemplatePage extends BasePage<AddActionableItemTemplatePage>
{

    @Visible
    @Name("Close button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Visible
    @Name("Title text input")
    @FindBy(id = "title-search")
    private WebElement titleSearchTextInput;

    @Visible
    @Name("Item type text input")
    @FindBy(id = "item-type-search")
    private WebElement itemTypeTextInput;

    @Name("Item type drop down")
    @FindBy(css = "#item-type-search + ul[data-ng-style]")
    private WebElement itemTypeDropDown;

    @Visible
    @Name("Live button")
    @FindBy(css = "[class='button-group segmented light'] li:nth-child(1)")
    private WebElement liveButton;

    @Name("Select Template button")
    @FindBy(css = "[data-ng-click='vm.select()']")
    private WebElement selectTemplateButton;

    @Visible
    @Name("All button")
    @FindBy(css = "[class='button-group segmented light'] li:nth-child(2)")
    private WebElement allButton;

    @Visible
    @Name("Find template button")
    @FindBy(css = "[data-ng-click='vm.findTemplate()']")
    private WebElement findTemplateButton;

    @Name("Message")
    @FindBy(css = "[data-ng-show='data.list.length === 0']")
    private WebElement searchMessage;

    @Name("Template search result table")
    private TemplateSearchResultTable templateSearchResultTable;

    @Step("Click on close button")
    public AddActionableItemPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get Title text")
    public String getTitleText()
    {
        return titleSearchTextInput.getAttribute("value");
    }

    @Step("Set Title text '{0}'")
    public AddActionableItemTemplatePage setTitleText(String input)
    {
        titleSearchTextInput.clear();
        titleSearchTextInput.sendKeys(input);
        return this;
    }

    @Step("Click on Live button")
    public AddActionableItemTemplatePage clickLiveButton()
    {
        liveButton.click();
        return this;
    }

    @Step("Check that Live button is selected")
    public boolean isLiveButtonSelected()
    {
        return liveButton.findElement(By.cssSelector("input")).getCssValue("color").contains
                ("rgba(35, 81, 126, 1)");
    }

    @Step("Click on All button")
    public AddActionableItemTemplatePage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Click on Find template button")
    public TemplateSearchResultTable clickFindTemplateButton()
    {
        findTemplateButton.click();
        waitForJavascriptFrameworkToFinish();
        return templateSearchResultTable;
    }

    @Step("Click on Find template button")
    public AddActionableItemPage clickSelectTemplateButton()
    {
        AppHelper.scrollToBottom();
        selectTemplateButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Get search message")
    public String getSearchMessage()
    {
        return searchMessage.getText();
    }

    @FindBy(css = "table.template-search")
    public class TemplateSearchResultTable extends HtmlElement
    {

        @Name("Data row")
        private List<DataRow> dataRows;

        @Step("Get data rows")
        public List<DataRow> getRows()
        {
            return dataRows;
        }
    }

    @FindBy(css = ".template-search-row")
    public class DataRow extends HtmlElement
    {

        @Visible
        @Name("Name radio button")
        @FindBy(css = ".template-search .name")
        private WebElement name;

        @Name("Name radio button")
        @FindBy(css = "div.radio label")
        private Radio nameRadioButton;

        @Visible
        @Name("Description text")
        @FindBy(css = ".description")
        private WebElement descriptionText;

        @Visible
        @Name("Status text")
        @FindBy(css = ".template-search [class^='status']")
        private WebElement statusText;

        @Visible
        @Name("Applied to assets text")
        @FindBy(css = "[class^='assets']")
        private WebElement appliedToAssetsText;

        @Step("Click name radio button")
        public DataRow clickNameRadioButton()
        {
            nameRadioButton.click();
            return this;
        }

        @Step("Chack if Radio button is displayed")
        public boolean isRadioButtonDisplayed()
        {
            return nameRadioButton.isDisplayed();
        }

        @Step("Get name")
        public String getName()
        {
            return name.getText();
        }

        @Step("Get description text")
        public String getDescriptionText()
        {
            return descriptionText.getText();
        }

        @Step("Get status")
        public String getStatus()
        {
            return statusText.getText();
        }

        @Step("Get Applied to assets ")
        public String getAppliedToAssets()
        {
            return appliedToAssetsText.getText();
        }
    }

}
