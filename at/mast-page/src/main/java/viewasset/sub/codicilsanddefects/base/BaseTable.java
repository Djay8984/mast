package viewasset.sub.codicilsanddefects.base;

import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.element.DataElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Load More Button")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private List<WebElement> loadMoreButton;

    @Name("Data Elements")
    private List<DataElement> elements;

    @Step("Get All Data Elements")
    public List<DataElement> getRows()
    {
        return elements;
    }

    @Step("Click on Load More Button")
    public CodicilsAndDefectsPage clickLoadMoreButton()
    {
        loadMoreButton.get(0).click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Is load more button displayed")
    public boolean isLoadMoreButtonDisplayed()
    {
        return loadMoreButton.size() > 0;
    }
}

