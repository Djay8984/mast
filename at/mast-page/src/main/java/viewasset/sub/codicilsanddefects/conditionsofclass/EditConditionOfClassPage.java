package viewasset.sub.codicilsanddefects.conditionsofclass;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

public class EditConditionOfClassPage extends BasePage<EditConditionOfClassPage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(id = "title")
    private WebElement titleTextBox;

    @Visible
    @Name("Description Text Box")
    @FindBy(id = "description")
    private WebElement descriptionTextBox;

    @Visible
    @Name("Inherited Check Box")
    @FindBy(id = "inherited-flag")
    private WebElement inheritedCheckBox;

    @Visible
    @Name("Imposed Date Text Box")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextBox;

    @Visible
    @Name("Due Date Text Box")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextBox;

    @Visible()
    @Name("LR Internal Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(1)")
    private WebElement lrInternalButton;

    @Visible()
    @Name("LR and Customer Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(2)")
    private WebElement lrAndCustomerButton;

    @Visible()
    @Name("All Button")
    @FindBy(css = "[data-ng-repeat='item in data.list']:nth-child(3)")
    private WebElement allButton;

    @Visible
    @Name("Attachments Link")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement attachmentLink;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".ng-scope.transparent-dark-button")
    private WebElement cancelButton;

    @Visible
    @Name("Save Button")
    @FindBy(css = ".ng-scope.primary-button")
    private WebElement saveButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Close Editing Button")
    @FindBy(id = "header-action-cancel")
    private List<WebElement> closeEditingButton;

    /**
     * @return ViewConditionOfClassPage or EditConfirmationWindow
     */
    @Step("Click Navigate Back Button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click on attachments")
    public AttachmentsAndNotesPage clickAttachmentButton()
    {
        attachmentLink.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Is Description Field Editable")
    public boolean isDescriptionFieldEditable()
    {
        return descriptionTextBox.isEnabled();
    }

    @Step("Is Due Date Field Editable")
    public boolean isDueDateFieldEditable()
    {
        return dueDateTextBox.isEnabled();
    }

    @Step("Is Close Editing Button Displayed")
    public boolean isCloseEditButtonDisplayed()
    {
        return closeEditingButton.size() > 0;
    }

    @Step("Click Save Button")
    public ViewConditionOfClassPage clickSaveButton()
    {
        saveButton.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }

    @Step("Click Save Button Expecting Staleness")
    public void clickSaveButtonExpectingStaleness()
    {
        saveButton.click();
    }

    @Step("Click Cancel Button")
    public ViewConditionOfClassPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }

    @Step("Set value to Description Text Box")
    public EditConditionOfClassPage setDescription(String title)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(title);
        return this;
    }

    @Step("Set Due Date")
    public EditConditionOfClassPage setDueDate(String imposedDate)
    {
        dueDateTextBox.clear();
        dueDateTextBox.sendKeys(imposedDate);
        dueDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Get Description Text")
    public String getDescriptionText()
    {
        return descriptionTextBox.getAttribute("value").trim();
    }

    @Step("Get Due Date Text")
    public String getDueDateText()
    {
        return dueDateTextBox.getAttribute("value").trim();
    }
}
