package viewasset.sub.codicilsanddefects.actionableitems.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;

public class BreakLinkToTemplateModalPage extends BasePage<BreakLinkToTemplateModalPage>
{
    @Visible
    @Name("BreakLink Button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(2)")
    private WebElement breakLinkButton;

    @Visible
    @Name("Go back button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(1)")
    private WebElement cancelButton;

    @Step("Click Go Back button")
    public AddActionableItemPage clickBreakLinkButton()
    {
        breakLinkButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Click Cancel button")
    public AddActionableItemPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }
}
