package viewasset.sub.codicilsanddefects.actionableitems.element;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;
import viewasset.sub.codicilsanddefects.actionableitems.select.SelectAssetItemPage;

import java.util.List;

public class DataElement extends HtmlElement
{

    private static final WebDriver driver = BaseTest.getDriver();

    @Name("Children")
    @FindBy(xpath = "./following-sibling::ul/li/div/div/div[@class='title']")
    private List<DataElement> children;

    @Name("Item Name")
    @FindBy(css = ".desc.ng-binding")
    private WebElement itemName;

    @Name("select RadioBox")
    @FindBy(css = "[data-ng-click='data.select()']>div")
    private WebElement selectRadioBox;

    @Name("+/- Icon")
    @FindBy(css = "[data-ng-click='data.expand =! data.expand;']")
    private WebElement plusOrMinusIcon;

    @Step("Check if radio box is selecetd")
    public boolean isSelectRadioBoxSelected()
    {
        AppHelper.scrollToBottom();
        System.out.println(selectRadioBox.getAttribute("class"));
        System.out.println(itemName.getText());
        return selectRadioBox.getAttribute("class").contains("checked");
    }

    @Step("Check if radio box is selected")
    public SelectAssetItemPage clickSelectRadioBox()
    {
        AppHelper.scrollToBottom();
        selectRadioBox.click();
        return PageFactory.newInstance(SelectAssetItemPage.class);
    }

    @Step("Expand Item")
    public SelectAssetItemPage clickPlusOrMinusIcon()
    {
        if (!(plusOrMinusIcon.getAttribute("class").contains("hidden")))
        {
            AppHelper.scrollToBottom();
            plusOrMinusIcon.click();
        }
        return PageFactory.newInstance(SelectAssetItemPage.class);
    }

    @Step("Get Item Name")
    public String getItemName()
    {
        AppHelper.scrollToBottom();
        return itemName.getText();
    }

    @Step("Get children")
    public List<DataElement> getChildren()
    {
        return children;
    }
}
