package viewasset.sub.codicilsanddefects.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

@Name("Codicil Elements")
@FindBy(css = "codicil-info")
public class DataElement extends HtmlElement
{

    @Visible
    @Name("Items Name ")
    @FindBy(css = ".codicil-title")
    private WebElement itemsName;

    @Visible
    @Name("Item ID")
    @FindBy(css = "[data-ng-bind*='vm.item.id']")
    private WebElement itemId;

    @Name("Category")
    @FindBy(css = ".codicil-content li:nth-child(1)")
    private WebElement category;

    @Name("Defect")
    @FindBy(css = ".codicil-content li:nth-child(2)")
    private List<WebElement> defect;

    @Name("Item Name")
    @FindBy(css = "[data-ng-bind*='vm.item.assetItem.name || vm.item.asset.name']")
    private WebElement itemName;

    @Visible
    @Name("Impose Date")
    @FindBy(css = "[data-ng-bind*='imposedDate']")
    private WebElement imposeDate;

    @Visible
    @Name("Due Date")
    @FindBy(css = "[data-ng-bind*='dueDate']")
    private WebElement dueDate;

    @Visible
    @Name("Status")
    @FindBy(css = "[data-ng-bind*='status']")
    private WebElement status;

    @Name("Repair")
    @FindBy(css = "[data-ng-if='vm.item.repairs.length']")
    private List<WebElement> repair;

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Step("Get Items Title")
    public String getItemName()
    {
        return itemsName.getText();
    }

    @Step("Return the itemName ID")
    public String getItemId()
    {
        return itemId.getText();
    }

    @Step("Return the itemName category")
    public String getCategory()
    {
        return category.getText();
    }

    @Step("Return the itemName defect")
    public String getDefect()
    {
        return defect.get(0).getText();
    }

    @Step("Return the itemName defect")
    public boolean isDefectPresent()
    {
        return defect.size() > 0;
    }

    @Step("Return the itemName affected")
    public String getItemAffected()
    {
        return itemName.getText();
    }

    @Step("Return the impose date")
    public String getImposeDate()
    {
        return imposeDate.getText();
    }

    @Step("Return the due date")
    public String getDueDate()
    {
        return dueDate.getText();
    }

    @Step("Return the itemName status")
    public String getStatus()
    {
        return status.getText();
    }

    /**
     * Click on the furtherDetailsArrow
     *
     * @param pageObjectClass - The pageObject which is expected to be returned by clicking the futher details arrow
     *                        Possible PageObjects: ViewConditionOfClassPage
     *                        ActionableItemPage
     *                        AssetNotePage
     *                        DefectsPage
     * @param <T>             The implementing type
     */
    @Step("Click on Further Details Arrow")
    public <T extends BasePage<T>> T clickFurtherDetailsArrow(Class<T> pageObjectClass)
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("If Repair is displayed")
    public boolean isRepairPresent()
    {
        return repair.size() > 0;
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
