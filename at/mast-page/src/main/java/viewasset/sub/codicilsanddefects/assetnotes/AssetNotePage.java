package viewasset.sub.codicilsanddefects.assetnotes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;

import static constant.LloydsRegister.NUMBER_REGEX;

public class AssetNotePage extends BasePage<AssetNotePage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private Button navigateBackButton;

    @Name("Asset Note Description")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement assetNoteDescription;

    @Name(" Asset Note Item Name ")
    @FindBy(css = "[data-ng-bind='data.obj.title']")
    private WebElement assetNoteItemText;

    @Step("Get Asset Note Description Text")
    public String getAssetNoteDescriptionText()
    {
        return assetNoteDescription.getText().trim()
                .replaceAll(NUMBER_REGEX, "").trim().toLowerCase();
    }

    @Step("Click Navigate Back Button")
    public CodicilsAndDefectsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Get Asset Note Item Text")
    public String getAssetNoteItemTitleText()
    {
        return assetNoteItemText.getText().toLowerCase().trim();
    }
}
