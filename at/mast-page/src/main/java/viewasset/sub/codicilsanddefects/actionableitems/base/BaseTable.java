package viewasset.sub.codicilsanddefects.actionableitems.base;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.actionableitems.element.DataElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Data Elements")
    @FindBy(css = "[data-tpl='result']>div>div>div.title")
    private List<DataElement> dataElements;

    @Step("Get All Data Elements")
    public List<DataElement> getRows()
    {
        return dataElements;
    }

}
