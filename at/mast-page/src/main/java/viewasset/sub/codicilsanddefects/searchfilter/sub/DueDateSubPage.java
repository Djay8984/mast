package viewasset.sub.codicilsanddefects.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class DueDateSubPage extends BasePage<DueDateSubPage>
{

    @Visible
    @Name("From Due Date Text Box")
    @FindBy(id = "codicil-search-filter-due-date-min")
    private WebElement fromDueDateTextBox;

    @Visible
    @Name("To Due Date Text Box")
    @FindBy(id = "codicil-search-filter-due-date-max")
    private WebElement toDueDateTextBox;

    @Name("Invalid Date Format Message Text")
    @FindBy(id = "validation-message-due-date-format")
    private WebElement invalidDateFormatMessageText;

    @Name("Non existing Date Format Message Text")
    @FindBy(id = "validation-message-non-existent-format")
    private WebElement nonExistingDateFormatMessageText;

    @Name("In valid To Date Message Text")
    @FindBy(id = "validation-message-due-date-min")
    private WebElement inValidToDateMessageText;

    @Step("Set From Due Date")
    public DueDateSubPage setFromDueDate(String fromDate)
    {
        fromDueDateTextBox.clear();
        fromDueDateTextBox.sendKeys(fromDate);
        fromDueDateTextBox.sendKeys(Keys.ENTER);
        return PageFactory.newInstance(DueDateSubPage.class);
    }

    @Step("Set To Due Date")
    public DueDateSubPage setToDueDate(String toDate)
    {
        toDueDateTextBox.clear();
        toDueDateTextBox.sendKeys(toDate);
        toDueDateTextBox.sendKeys(Keys.ENTER);
        return PageFactory.newInstance(DueDateSubPage.class);
    }

    @Step("Clear From Due Date Text Box")
    public DueDateSubPage clearFromDueDate()
    {
        fromDueDateTextBox.clear();
        return this;
    }

    @Step("Clear To Due Date Text Box")
    public DueDateSubPage clearToDueDate()
    {
        toDueDateTextBox.clear();
        return this;
    }

    @Step("Get From Due Date Text")
    public String getFromDueDateText()
    {
        return fromDueDateTextBox.getAttribute("value");
    }

    @Step("Get To Due Date Text")
    public String getToDueDateText()
    {
        return toDueDateTextBox.getAttribute("value");
    }

    @Step("see if the error icon is displayed in from date Text Box")
    public boolean isErrorIconDisplayedInFromDateTextBox(type type)
    {
        return fromDueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in To date Text Box")
    public boolean isErrorIconDisplayedInToDateTextBox(type type)
    {
        return toDueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Get Invalid Date Format Message Text")
    public String getInvalidDateMessageText()
    {
        return invalidDateFormatMessageText.getText().trim();
    }

    @Step("Get Non Existing Date Format Message Text")
    public String getNonExistingDateMessageText()
    {
        return nonExistingDateFormatMessageText.getText().trim();
    }

    @Step("Get in valid To date message text")
    public String getInvalidToDateMessageText()
    {
        return inValidToDateMessageText.getText().trim();
    }

    public enum type
    {
        ERROR("images/red_warning.png");

        private final String image;

        type(String image)
        {
            this.image = image;
        }
    }

}
