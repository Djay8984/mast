package viewasset.sub.codicilsanddefects.conditionsofclass;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.conditionsofclass.element.BaseElement;

import java.util.List;

public class ViewConditionOfClassPage extends BasePage<ViewConditionOfClassPage>
{

    @Name("Edit Button")
    private final static String EDIT_BUTTON_ID = "header-action-edit";
    @Name("Remove Button")
    private final static String REMOVE_BUTTON_ID = "header-action-remove";
    @Name("Cancel Button")
    private final static String CANCEL_BUTTON_ID = "header-action-cancel";
    @Name("Close Button")
    private final static String CLOSE_BUTTON_ID = "header-action-cancel";
    @Name("Defect Pane")
    private final static String LINKED_DEFECT_DETAILS_ID = "tpl-defect";
    @Name("Repair Pane")
    private final static String REPAIR_DETAILS_ID = "tpl-repairs";
    @FindBy(id = EDIT_BUTTON_ID)
    private Button editButton;
    @FindBy(id = REMOVE_BUTTON_ID)
    private Button removeButton;
    @FindBy(id = CANCEL_BUTTON_ID)
    private Button cancelButton;
    @FindBy(id = CLOSE_BUTTON_ID)
    private Button closeButton;
    @Name("Description")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement description;
    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private Button navigateBackButton;
    @Name("ID")
    @FindBy(css = "#tpl- [data-ng-bind='data.value']")
    private WebElement id;
    @Name("Due Date Text")
    @FindBy(css = "#tpl-dueDate  [data-ng-bind^='data.value']")
    private WebElement dueDateText;
    @Name("Inherited")
    @FindBy(css = "#tpl-inheritedFlag .view-text span")
    private WebElement inherited;
    @Name("Imposed Date")
    @FindBy(css = "#tpl-imposedDate [data-ng-bind='data.value | moment']")
    private WebElement imposedDate;
    @Name("Visible For")
    @FindBy(css = "[id^='tpl-confidentialityType'] .view-text span")
    private WebElement visibleFor;
    @Name("Attachment")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement attachment;
    @Name("Title")
    @FindBy(css = "[data-ng-bind='data.obj.title']")
    private WebElement titleText;
    @FindBy(id = LINKED_DEFECT_DETAILS_ID)
    private WebElement linkedDefectDetails;
    @FindBy(id = REPAIR_DETAILS_ID)
    private WebElement repairDetails;
    @Name("repair Table")
    private RepairTable repairTable;

    @Step("Click Edit Button")
    public EditConditionOfClassPage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(EditConditionOfClassPage.class);
    }

    @Step("Get COC Description Text")
    public String getCocDescriptionText()
    {
        return description.getText().trim()
                .toLowerCase();
    }

    /**
     * @return CodicilsAndDefectsPage or ConditionsOfClassPage
     */
    @Step("Click Navigate Back Button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Get COC ID")
    public String getId()
    {
        return id.getText().trim();
    }

    @Step("Get Coc Inherited Type")
    public String getCocInheritedType()
    {
        return inherited.getText().trim();
    }

    @Step("Get Coc Impose date")
    public String getCocImposeDate()
    {
        return imposedDate.getText().trim();
    }

    @Step("Get Due date Text")
    public String getDueDateText()
    {
        return dueDateText.getText().trim();
    }

    @Step("Get Coc Visible for Text")
    public String getCocVisibleForText()
    {
        return visibleFor.getText().trim();
    }

    @Step("Is Attachment Displayed")
    public boolean isAttachmentDisplayed()
    {
        return attachment.isDisplayed();
    }

    @Step("Get Coc Title Text")
    public String getTitleText()
    {
        return titleText.getText().toLowerCase().trim();
    }

    @Step("Is Edit Button Enabled")
    public boolean isEditButtonEnabled()
    {
        return editButton.isEnabled();
    }

    @Step("Is Edit Button Displayed")
    public boolean isEditButtonDisplayed()
    {
        return driver.findElements(By.id(EDIT_BUTTON_ID)).size() > 0;
    }

    @Step("Is Cancel Button Displayed")
    public boolean isCancelButtonDisplayed()
    {
        return driver.findElements(By.id(CANCEL_BUTTON_ID)).size() > 0;
    }

    @Step("Is Close Button Enabled")
    public boolean isCloseButtonDisplayed()
    {
        return driver.findElements(By.id(CLOSE_BUTTON_ID)).size() > 0;
    }

    @Step("Is defect Pane displayed")
    public boolean isLinkedDefectDetailsDisplayed()
    {
        return driver.findElements(By.id(LINKED_DEFECT_DETAILS_ID)).size() > 0;
    }

    @Step("Is Repair Pane displayed")
    public boolean isRepairDetailsDisplayed()
    {
        return driver.findElements(By.id(REPAIR_DETAILS_ID)).size() > 0;
    }

    @Step("Get Repair Table")
    public RepairTable getRepairTable()
    {
        return repairTable;
    }

    @FindBy(css = "#tpl-repairs")
    public class RepairTable extends HtmlElement
    {

        @Name("Data Elements")
        private List<BaseElement> elements;

        @Step("Get All Data Elements")
        public List<BaseElement> getRows()
        {
            return elements;
        }
    }

}
