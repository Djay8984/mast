package viewasset.sub.codicilsanddefects.actionableitems.select;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.base.BaseTable;

public class SelectAssetItemPage extends BasePage<SelectAssetItemPage>
{

    @Visible
    @Name("Close Button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Name("assign Selected Button")
    @FindBy(css = ".grid-content.modal-footer .primary-button")
    private Button assignSelectedButton;

    @Name("Cancel Button")
    @FindBy(css = ".grid-content.modal-footer [data-ng-click=\"$dismiss()\"]")
    private Button cancelButton;

    @Name("Item Table")
    private ItemTable itemTable;

    @Step("Get Asset Note Table")
    public ItemTable getItemTable()
    {
        return itemTable;
    }

    public boolean isAssignSelectedButtonEnabled()
    {
        return assignSelectedButton.isEnabled();
    }

    public AddActionableItemPage clickAssignSelectedButton()
    {
        assignSelectedButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    public AddActionableItemPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @FindBy(css = ".hierarchy")
    public class ItemTable extends BaseTable
    {
    }

}
