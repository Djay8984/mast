package viewasset.sub.codicilsanddefects.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ItemTypeSubPage extends BasePage<ItemTypeSubPage>
{

    @Visible
    @Name("Item Type TextBox")
    @FindBy(id = "codicil-item-type")
    private WebElement itemTypeTextBox;

    @Step("Set Item Type Search")
    public ItemTypeSubPage setItemTypeSearch(String ItemType)
    {
        itemTypeTextBox.sendKeys(ItemType);
        return this;
    }

    @Step("Get Item Type Text")
    public String getItemTypeText()
    {
        return itemTypeTextBox.getAttribute("value");
    }
}
