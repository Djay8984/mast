package viewasset.sub.codicilsanddefects.element;

import com.frameworkium.core.ui.annotations.Visible;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.sub.codicilsanddefects.defects.DefectsPage;

@Name("Defects")
@FindBy(css = "[data-ng-repeat='item in vm.defects']")
public class DefectsElement extends HtmlElement
{

    @Visible
    @Name("Further Details Arrow")
    @FindBy(css = ".float-right.view-codicil")
    private WebElement furtherDetailsArrow;

    @Visible
    @Name("Defect Title ")
    @FindBy(css = ".codicil-title")
    private WebElement defectTitle;

    @Visible
    @Name("Defects Status")
    @FindBy(css = "[data-ng-bind='vm.item.defectStatus.name']")
    private WebElement defectsStatus;

    @Step("Get Defect Title")
    public String getDefectTitle()
    {
        return defectTitle.getText();
    }

    @Step("Click on Further Details Arrow")
    public DefectsPage clickFurtherDetailsArrow()
    {
        furtherDetailsArrow.click();
        return PageFactory.newInstance(DefectsPage.class);
    }

    @Step("Get Defects Status Text")
    public String getDefectsStatusText()
    {
        return defectsStatus.getText().trim();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
