package viewasset.sub.codicilsanddefects.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;

import static constant.LloydsRegister.NUMBER_REGEX;

public class DefectsPage extends BasePage<DefectsPage>
{

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private Button navigateBackButton;

    @Name("Incident Description")
    @FindBy(id = "view-defect-description")
    private WebElement defectsDescription;

    @Name("Defect Item Name Text")
    @FindBy(id = "view-defect-header-name")
    private WebElement defectItemNameText;

    @Step("Click Navigate Back Button")
    public CodicilsAndDefectsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Get Defects Description Text")
    public String getDefectsDescriptionText()
    {
        return defectsDescription.getText().trim()
                .replaceAll(NUMBER_REGEX, "").trim().toLowerCase();
    }

    @Step("Get Defect Item Name Text")
    public String getDefectItemNameText()
    {
        return defectItemNameText.getText().toLowerCase().trim();
    }

}
