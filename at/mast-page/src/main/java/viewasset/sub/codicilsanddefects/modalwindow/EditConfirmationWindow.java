package viewasset.sub.codicilsanddefects.modalwindow;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;
import viewasset.sub.codicilsanddefects.conditionsofclass.ViewConditionOfClassPage;

public class EditConfirmationWindow extends BasePage<EditConfirmationWindow>
{

    @Visible
    @Name("Cancel Button")
    @FindBy(css = ".ng-scope.transparent-button-white")
    private WebElement cancelButton;

    @Visible
    @Name("Save Changes Button")
    @FindBy(css = "[data-ng-repeat='action in actions'].primary-button")
    private WebElement saveChangesButton;

    @Visible
    @Name("Continue Without Saving Button")
    @FindBy(css = "[data-ng-repeat='action in actions'].secondary-button")
    private WebElement continueWithoutSavingButton;

    @Step("Click Cancel Button")
    public EditConditionOfClassPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(EditConditionOfClassPage.class);
    }

    @Step("Click Save Changes Button")
    public ViewConditionOfClassPage clickSaveChangesButton()
    {
        saveChangesButton.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }

    @Step("Click Continue Without Saving Button")
    public ViewConditionOfClassPage clickContinueWithoutSaveChangesButton()
    {
        continueWithoutSavingButton.click();
        return PageFactory.newInstance(ViewConditionOfClassPage.class);
    }
}
