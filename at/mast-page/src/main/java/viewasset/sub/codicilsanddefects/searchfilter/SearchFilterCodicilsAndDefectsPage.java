package viewasset.sub.codicilsanddefects.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.searchfilter.sub.*;

public class SearchFilterCodicilsAndDefectsPage extends BasePage<SearchFilterCodicilsAndDefectsPage>
{

    @Visible
    @Name("Search Text Box")
    @FindBy(id = "codicil-search-query")
    private WebElement searchTextBox;

    @Visible
    @Name("Reset Button")
    @FindBy(id = "codicil-search-reset")
    private WebElement resetButton;

    @Visible
    @Name("Search Button")
    @FindBy(id = "codicil-search-button")
    private WebElement searchButton;

    @Visible
    @Name("Type and category filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(1) .codicil-filter-button")
    private WebElement typeAndCategoryFilterButton;

    @Visible
    @Name("Due date filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(2) .codicil-filter-button")
    private WebElement dueDateFilterButton;

    @Visible
    @Name("Status filter button")
    @FindBy(css = ".grid-content.vertical div:nth-child(2)>div div:nth-child(3) .codicil-filter-button")
    private WebElement statusFilterButton;

    @Visible
    @Name("Item type filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(1) .codicil-filter-button")
    private WebElement itemTypeFilterButton;

    @Visible
    @Name("Confidentiality filter Button")
    @FindBy(css = ".grid-content.vertical div:nth-child(3)>div div:nth-child(2) .codicil-filter-button")
    private WebElement confidentialityFilterButton;

    @Step("Click Search Button")
    public CodicilsAndDefectsPage clickSearchButton()
    {
        AppHelper.scrollToBottom();
        searchButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Click type and category filter button")
    public TypesAndCategorySubPage clickTypeAndCategoryFilterButton()
    {
        typeAndCategoryFilterButton.click();
        return PageFactory.newInstance(TypesAndCategorySubPage.class);
    }

    @Step("Click Due date filter button")
    public DueDateSubPage clickDueDateFilterButton()
    {
        dueDateFilterButton.click();
        return PageFactory.newInstance(DueDateSubPage.class);
    }

    @Step("Click Status filter button")
    public StatusSubPage clickStatusFilterButton()
    {
        statusFilterButton.click();
        return PageFactory.newInstance(StatusSubPage.class);
    }

    @Step("Click Item type filter button")
    public ItemTypeSubPage clickItemTypeFilterButton()
    {
        itemTypeFilterButton.click();
        return PageFactory.newInstance(ItemTypeSubPage.class);
    }

    @Step("Click Confidentiality filter button")
    public ConfidentialitySubPage clickConfidentialityFilterButton()
    {
        confidentialityFilterButton.click();
        return PageFactory.newInstance(ConfidentialitySubPage.class);
    }

    @Step("Click Reset Button")
    public CodicilsAndDefectsPage clickResetButton()
    {
        resetButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Get Search Text")
    public String getSearchText()
    {
        return searchTextBox.getAttribute("value");
    }

    @Step("Set the search type into Search Text Box")
    public SearchFilterCodicilsAndDefectsPage setSearchText(String title)
    {
        searchTextBox.clear();
        searchTextBox.sendKeys(title);
        return this;
    }

    @Step("Is Search button enabled")
    public boolean isSearchButtonEnabled()
    {
        return searchButton.isEnabled();
    }

    @Step("Is type and category filter button enabled")
    public boolean isTypeAndCategoryFilterButtonEnabled()
    {
        return typeAndCategoryFilterButton.isEnabled();
    }

    @Step("Is status filter button enabled")
    public boolean isStatusFilterButtonEnabled()
    {
        return statusFilterButton.isEnabled();
    }
}
