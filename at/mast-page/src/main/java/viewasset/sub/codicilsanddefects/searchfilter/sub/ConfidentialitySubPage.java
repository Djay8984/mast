package viewasset.sub.codicilsanddefects.searchfilter.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class ConfidentialitySubPage extends BasePage<ConfidentialitySubPage>
{

    @Visible
    @Name("Select all button")
    @FindBy(id = "codicil-search-filter-confidentiality-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear Button")
    @FindBy(id = "codicil-search-filter-confidentiality-clear-all")
    private WebElement clearButton;

    @Visible
    @Name("Confidentiality checkBoxes")
    @FindBy(css = ".checkbox-container:not(.grid-content)")
    private List<WebElement> confidentialityStatusCheckBoxes;

    @Step("Verify Confidentiality Check boxes are selected")
    public boolean isConfidentialityCheckBoxSelected(String text)
    {
        return confidentialityStatusCheckBoxes.stream()
                .filter(e -> e.findElement(By.cssSelector("label")).getText().equals(text))
                .findFirst()
                .get()
                .findElement(By.tagName("button"))
                .getAttribute("class")
                .contains("checked");
    }

    @Step("Click clear all button")
    public ConfidentialitySubPage clickClearAllButton()
    {
        clearButton.click();
        return this;
    }
}
