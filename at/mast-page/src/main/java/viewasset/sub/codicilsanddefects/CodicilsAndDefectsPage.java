package viewasset.sub.codicilsanddefects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import util.AppHelper;
import viewasset.sub.codicilsanddefects.actionableitems.AddActionableItemPage;
import viewasset.sub.codicilsanddefects.base.BaseTable;
import viewasset.sub.codicilsanddefects.conditionsofclass.AddConditionOfClassPage;
import viewasset.sub.codicilsanddefects.element.ActionableItemsElement;
import viewasset.sub.codicilsanddefects.element.AssetNotesElement;
import viewasset.sub.codicilsanddefects.element.ConditionsOfClassElement;
import viewasset.sub.codicilsanddefects.element.DefectsElement;
import viewasset.sub.codicilsanddefects.searchfilter.SearchFilterCodicilsAndDefectsPage;

import java.util.List;
import java.util.stream.Collectors;

import static constant.LloydsRegister.NUMBER_REGEX;

public class CodicilsAndDefectsPage extends BasePage<CodicilsAndDefectsPage>
{

    @Visible
    @Name("Search/Filter codicils and defect button")
    @FindBy(css = "[data-ng-click='vm.searchMode = !vm.searchMode']")
    private WebElement searchFilterCodicilsAndDefectsButton;

    @Visible
    @Name("Coc Add New Button")
    @FindBy(css = ".primary-button.no-margin.float-right.add")
    private Button cocAddNewButton;

    @Name("Conditions of class no items found")
    @FindBy(css = "[data-ng-if='!vm.cocs.length']")
    private WebElement cocNoItems;

    @Name("Coc Load More Button")
    @FindBy(css = "[data-pageable='vm.cocs'] [data-ng-click='vm.loadMore()']")
    private List<WebElement> cocLoadMoreButton;

    @Visible
    @Name("Actionable item add new button")
    @FindBy(css = "[ui-sref*='actionableItem.add']")
    private WebElement actionableItemAddNewButton;

    @Name("Actionable item count")
    @FindBy(css = ".grid-block:nth-child(5) strong:nth-child(2)")
    private WebElement actionableItemCount;

    @Name("Actionable items no items found")
    @FindBy(css = "[data-ng-if='!vm.actionableItems.length']")
    private WebElement ActionableItemsNoItems;

    @Name("Actionable Items Load More Button")
    @FindBy(css = "[data-pageable='vm.actionableItems'] [data-ng-click='vm.loadMore()']")
    private List<WebElement> actionableItemsLoadMoreButton;

    @Visible
    @Name("Asset notes add new button")
    @FindBy(css = "[ui-sref*='assetNote.add']")
    private WebElement assetNoteAddNewButton;

    @Name("Asset notes no items found")
    @FindBy(css = "[data-ng-if='!vm.assetNotes.length']")
    private WebElement assetNotesNoItems;

    @Name("Asset Notes Load More Button")
    @FindBy(css = "[data-pageable='vm.assetNotes'] [data-ng-click='vm.loadMore()']")
    private List<WebElement> assetNotesLoadMoreButton;

    @Name("Defect Title")
    @FindBy(css = ".grid-block:nth-child(10)")
    private WebElement defectTitle;

    @Name("Defects no items found")
    @FindBy(css = "[data-ng-if='!vm.defects.length']")
    private WebElement defectsNoItems;

    @Name("Defects Load More Button")
    @FindBy(css = "[data-pageable='vm.defects'] [data-ng-click='vm.loadMore()']")
    private List<WebElement> defectsLoadMoreButton;

    @Name("Titles")
    @FindBy(css = "h3 > strong")
    private List<WebElement> titles;

    @Name("Codicil Labels")
    @FindBy(css = "h5 strong:nth-child(1)")
    private List<WebElement> codicilLabels;

    @Name("Codicil Counts")
    @FindBy(css = "h5 strong:nth-child(2)")
    private List<WebElement> codicilCounts;

    @Name("Total Codicil Counts")
    @FindBy(css = "h3 small strong")
    private List<WebElement> totalCounts;

    @Name("Coc cards")
    private List<ConditionsOfClassElement> cocCards;

    @Name("Actionable Items Elements")
    private List<ActionableItemsElement> actionableItemsElements;

    @Name("Asset Notes Elements")
    private List<AssetNotesElement> assetNotesElements;

    @Name("Defects Elements")
    private List<DefectsElement> defectsElements;

    @Name("Codicil Table")
    private CoCTable cocTable;

    @Name("Actionable Items Table")
    private ActionableItemsTable actionableItemsTable;

    @Name("Asset Note Table")
    private AssetNotesTable assetNotesTable;

    @Name("Defects Table")
    private DefectsTable defectsTable;

    @Step("Get Codicil Table")
    public CoCTable getCocTable()
    {
        return cocTable;
    }

    @Step("Get Actionable Items Table")
    public ActionableItemsTable getActionableItemsTable()
    {
        return actionableItemsTable;
    }

    @Step("Get Asset Note Table")
    public AssetNotesTable getAssetNoteTable()
    {
        return assetNotesTable;
    }

    @Step("Get Defects Table")
    public DefectsTable getDefectsTable()
    {
        return defectsTable;
    }

    @Step("Returning the total codicils count")
    public String getTotalCodicilsCount()
    {
        return totalCounts.get(0).getText();
    }

    @Step("Returning the total defects count")
    public String getTotalDefectsCount()
    {
        return totalCounts.get(1).getText();
    }

    @Step("Returning the CoC Label")
    public String cocLabel()
    {
        return codicilLabels.get(0).getText();
    }

    @Step("Returning the number of CoCs")
    public String cocNumber()
    {
        return codicilCounts.get(0).getText();
    }

    @Step("Returning an Coc card by Coc ID")
    public ConditionsOfClassElement getCocById(String cocId)
    {
        return cocCards.stream()
                .filter(c -> c.getCoCId().equals(cocId))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an Coc card by Title")
    public ConditionsOfClassElement getCocByTitle(String title)
    {
        return cocCards.stream()
                .filter(c -> c.getCocTitle().trim().equals(title.trim()))
                .findFirst()
                .orElse(null);
    }

    @Step("Is the CoC no items found label shown")
    public boolean isCocNoItemsFound()
    {
        return cocNoItems.isDisplayed();
    }

    @Step("Clicking on the coc load more button")
    public CodicilsAndDefectsPage clickCocLoadMoreButton()
    {
        if (cocLoadMoreButton.size() > 0)
        {
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(false);",
                            cocLoadMoreButton.get(0));
            cocLoadMoreButton.get(0).click();
            waitForJavascriptFrameworkToFinish();
            wait.until(ExpectedConditions.attributeContains(cocLoadMoreButton.get(0), "class", "button"));
            wait.until(ExpectedConditions.invisibilityOfElementLocated
                    (By.cssSelector("[data-ng-if=vm.loading']")));
        }
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Returning the actionable item label")
    public String actionableItemLabel()
    {
        return codicilLabels.get(1).getText();
    }

    @Step("Is the actionable item add new button displayed")
    public boolean isActionableItemAddNewButtonDisplayed()
    {
        return actionableItemAddNewButton.isDisplayed();
    }

    @Step("Click Add Actionable Item")
    public AddActionableItemPage clickActionableItemAddNewButton()
    {
        actionableItemAddNewButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Returning the actionable item count")
    public String actionableItemCount()
    {
        return codicilCounts.get(1).getText();
    }

    @Step("Returning an Actionable Items By Title")
    public ActionableItemsElement getActionableItemByTitle(String title)
    {
        return actionableItemsElements.stream()
                .filter(c -> c.getActionableItemsTitle().replaceAll(NUMBER_REGEX, "").trim().equals(title.trim()))
                .findFirst()
                .orElse(null);
    }

    @Step("is the actionable items no items found label shown")
    public boolean isActionableItemsNoItemsFound()
    {
        return ActionableItemsNoItems.isDisplayed();
    }

    @Step("Clicking on the actionable items load more button")
    public CodicilsAndDefectsPage clickActionableItemLoadMoreButton()
    {
        while(actionableItemsLoadMoreButton.size()>0)
        {
            AppHelper.scrollToBottom();
            actionableItemsLoadMoreButton.get(0).click();
        }
            return this;
    }

    @Step("Returning the asset notes label")
    public String assetNoteLabel()
    {
        return codicilLabels.get(2).getText();
    }

    @Step("Is the asset note add new button displayed")
    public boolean isAssetNoteAddNewButtonDisplayed()
    {
        return assetNoteAddNewButton.isDisplayed();
    }

    @Step("Returning the asset notes count")
    public String assetNotesCount()
    {
        return codicilCounts.get(2).getText();
    }

    @Step("Returning an Asset Notes By Title")
    public AssetNotesElement getAssetNotesByTitle(String title)
    {
        return assetNotesElements.stream()
                .filter(c -> c.getAssetNoteTitle().replaceAll(NUMBER_REGEX, "").trim().equals(title.trim()))
                .findFirst()
                .orElse(null);
    }

    @Step("is the asset notes no items found label shown")
    public boolean isAssetNotesNoItemsFound()
    {
        return assetNotesNoItems.isDisplayed();
    }

    @Step("Clicking on the asset notes load more button")
    public CodicilsAndDefectsPage clickAssetNotesLoadMoreButton()
    {

        while(assetNotesLoadMoreButton.size()>0)
        {
            AppHelper.scrollToBottom();
            assetNotesLoadMoreButton.get(0).click();
        }
        return this;
    }

    @Step("Clicking on the Defects load more button")
    public CodicilsAndDefectsPage clickDefectsLoadMoreButton()
    {
        if (defectsLoadMoreButton.size() > 0)
        {
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(false);",
                            defectsLoadMoreButton.get(0));
            defectsLoadMoreButton.get(0).click();
            wait.until(ExpectedConditions.attributeContains(defectsLoadMoreButton.get(0), "class", "button"));
            wait.until(ExpectedConditions.invisibilityOfElementLocated
                    (By.cssSelector("[data-ng-if='vm.loaded']")));
            waitForJavascriptFrameworkToFinish();
        }
        return this;
    }

    @Step("Returning the defect title")
    public String getDefectTitle()
    {
        return titles.get(1).getText();
    }

    @Step("Returning an Defects By Title")
    public DefectsElement getDefectsByTitle(String title)
    {
        return defectsElements.stream()
                .filter(c -> c.getDefectTitle().replaceAll(NUMBER_REGEX, "")
                        .trim().toLowerCase().equals(title))
                .findFirst()
                .orElse(null);
    }

    @Step("is defects no items found label shown")
    public boolean isDefectNoItemsFound()
    {
        return defectsNoItems.isDisplayed();
    }

    @Step("Is the Coc Add New Button displayed")
    public boolean isCocAddNewButtonDisplayed()
    {
        return cocAddNewButton.isDisplayed();
    }

    @Step("Click on Coc Add New Button")
    public AddConditionOfClassPage clickCocAddNewButton()
    {
        cocAddNewButton.click();
        return PageFactory.newInstance(AddConditionOfClassPage.class);
    }

    @Step("Search/Filter codicils and defect button")
    public SearchFilterCodicilsAndDefectsPage clickSearchFilterCodicilsAndDefectsButton()
    {
        searchFilterCodicilsAndDefectsButton.click();
        return PageFactory.newInstance(SearchFilterCodicilsAndDefectsPage.class);
    }

    @Step("Get all Coc Names")
    public List<String> getCocTitle()
    {
        if (cocCards.size() > 0)
        {
            if (cocLoadMoreButton.size() > 0)
            {
                cocLoadMoreButton.get(0).click();
                waitForJavascriptFrameworkToFinish();
            }
            return cocCards.stream()
                    .map(e -> e.getCocTitle().replaceAll(NUMBER_REGEX, "").trim().toLowerCase())
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Step("Get all Actionable Items Names")
    public List<String> getActionableItemsTitle()
    {
        if (actionableItemsElements.size() > 0)
        {
            if (actionableItemsLoadMoreButton.size() > 0)
            {
                actionableItemsLoadMoreButton.get(0).click();
                waitForJavascriptFrameworkToFinish();
            }
            return actionableItemsElements.stream()
                    .map(e -> e.getActionableItemsTitle()
                            .replaceAll(NUMBER_REGEX, "").trim().toLowerCase())
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Step("Get all Asset Note Names")
    public List<String> getAssetNotesTitle()
    {
        if (assetNotesElements.size() > 0)
        {
            if (assetNotesLoadMoreButton.size() > 0)
            {
                assetNotesLoadMoreButton.get(0).click();
                waitForJavascriptFrameworkToFinish();
            }
            return assetNotesElements.stream()
                    .map(e -> e.getAssetNoteTitle().replaceAll(NUMBER_REGEX, "")
                            .trim().toLowerCase())
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Step("Get all Defects Names")
    public List<String> getDefectsTitle()
    {
        if (defectsElements.size() > 0)
        {
            if (defectsLoadMoreButton.size() > 0)
            {
                defectsLoadMoreButton.get(0).click();
                waitForJavascriptFrameworkToFinish();
            }
            return defectsElements.stream()
                    .map(e -> e.getDefectTitle().replaceAll(NUMBER_REGEX, "")
                            .trim().toLowerCase())
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Step("Get All COC Items")
    public List<ConditionsOfClassElement> getCocItems()
    {
        return cocCards;
    }

    @Step("Get All Actionable Items")
    public List<ActionableItemsElement> getActionableItems()
    {
        return actionableItemsElements;
    }

    @Step("Get All Asset Note")
    public List<AssetNotesElement> getAssetNote()
    {
        return assetNotesElements;
    }

    @Step("Get All Defects")
    public List<DefectsElement> getDefects()
    {
        return defectsElements;
    }

    @Step("Is Coc load more button displayed")
    public boolean isCocLoadMoreButtonDisplayed()
    {
        return cocLoadMoreButton.size() > 0;
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @FindBy(css = "[data-pageable='vm.cocs']")
    public class CoCTable extends BaseTable
    {
    }

    @FindBy(css = "[data-pageable ='vm.actionableItems']")
    public class ActionableItemsTable extends BaseTable
    {
    }

    @FindBy(css = "[data-pageable ='vm.assetNotes']")
    public class AssetNotesTable extends BaseTable
    {
    }

    @FindBy(css = "[data-pageable='vm.defects']")
    public class DefectsTable extends BaseTable
    {
    }
}
