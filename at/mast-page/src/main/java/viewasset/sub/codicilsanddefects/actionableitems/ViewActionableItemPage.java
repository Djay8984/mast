package viewasset.sub.codicilsanddefects.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import viewasset.sub.codicilsanddefects.actionableitems.modal.ConfirmCancelPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.ConfirmDeletePage;
import viewasset.sub.codicilsanddefects.conditionsofclass.EditConditionOfClassPage;

public class ViewActionableItemPage extends BasePage<ViewActionableItemPage>
{

    @Name("Edit Button")
    @FindBy(id = "header-action-edit")
    private Button editButton;

    @Name("Remove Button")
    @FindBy(id = "header-action-remove")
    private Button removeButton;

    @Name("Cancel Button")
    @FindBy(id = "header-action-cancel")
    private Button cancelButton;

    @Name("Close Button")
    @FindBy(id = "header-action-cancel")
    private Button closeButton;

    @Name("Description")
    @FindBy(css = "#tpl-description [data-ng-bind='data.value']")
    private WebElement description;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private Button navigateBackButton;

    @Name("ID")
    @FindBy(css = "#tpl- [data-ng-bind='data.value']")
    private WebElement id;

    @Name("Due Date Text")
    @FindBy(css = "#tpl-dueDate  [data-ng-bind^='data.value']")
    private WebElement dueDateText;

    @Name("Imposed Date")
    @FindBy(css = "#tpl-imposedDate [data-ng-bind='data.value | moment']")
    private WebElement imposedDate;

    @Name("Visible For")
    @FindBy(css = "[id^='tpl-confidentialityType'] .view-text span")
    private WebElement visibleFor;

    @Name("Attachment")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement attachment;

    @Name("Title")
    @FindBy(css = "[data-ng-bind='data.obj.title']")
    private WebElement titleText;

    @Step("Click Edit Button")
    public EditConditionOfClassPage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(EditConditionOfClassPage.class);
    }

    @Step("Get Description Text")
    public String getDescriptionText()
    {
        return description.getText().trim()
                .toLowerCase();
    }

    /**
     * @return CodicilsAndDefectsPage or ConditionsOfClassPage
     */
    @Step("Click Navigate Back Button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("click Cancel Button")
    public ConfirmCancelPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ConfirmCancelPage.class);
    }

    @Step("click Close Button")
    public ConfirmDeletePage clickCloseButton()
    { //this button does delete action
        closeButton.click();
        return PageFactory.newInstance(ConfirmDeletePage.class);
    }

    @Step("Get ID")
    public String getId()
    {
        return id.getText().trim();
    }

    @Step("Get Impose date")
    public String getImposeDate()
    {
        return imposedDate.getText().trim();
    }

    @Step("Get Due date Text")
    public String getDueDateText()
    {
        return dueDateText.getText().trim();
    }

    @Step("Get Visible for Text")
    public String getVisibleForText()
    {
        return visibleFor.getText().trim();
    }

    @Step("Is Attachment Displayed")
    public boolean isAttachmentDisplayed()
    {
        return attachment.isDisplayed();
    }

    @Step("Get Coc Title Text")
    public String getTitleText()
    {
        return titleText.getText().toLowerCase().trim();
    }

    @Step("Is Edit Button Enabled")
    public boolean isEditButtonEnabled()
    {
        return editButton.isEnabled();
    }

    @Step("Is Edit Button Displayed")
    public boolean isEditButtonDisplayed()
    {
        return editButton.isDisplayed();
    }

    @Step("Is Cancel Button Displayed")
    public boolean isCancelButtonDisplayed()
    {
        return cancelButton.isDisplayed();
    }

    @Step("Is Close Button Enabled")
    public boolean isCloseButtonDisplayed()
    {
        return closeButton.isDisplayed();
    }
}
