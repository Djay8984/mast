package viewasset.sub.codicilsanddefects.conditionsofclass.element;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("CoC Elements")
@FindBy(css = "[data-ng-repeat='item in data.value']")
public class BaseElement extends HtmlElement
{

    @Name("Items Title ")
    @FindBy(css = ".title.ng-binding")
    private WebElement title;

    @Name("ID")
    @FindBy(css = "[data-ng-bind='item.id | zeropad: 5']")
    private WebElement id;

    @Name("Date Added")
    @FindBy(css = "[data-ng-bind^='item.updatedDate']")
    private WebElement dateAdded;

    @Name("Date Added")
    @FindBy(css = "[data-ng-bind^='item.repairAction.name'] ")
    private WebElement actionName;

    @Step("Check If title is displayed")
    public boolean isTitleDisplayed()
    {
        return title.isDisplayed();
    }

    @Step("Check If id is displayed")
    public boolean isIdDisplayed()
    {
        return id.isDisplayed();
    }

    @Step("Check If dateAdded is displayed")
    public boolean isDateAddedDisplayed()
    {
        return dateAdded.isDisplayed();
    }

    @Step("Check If Action Name is displayed")
    public boolean isActionNameDisplayed()
    {
        return actionName.isDisplayed();
    }

    @Step("Get the id")
    public String getId()
    {
        return id.getText();
    }
}
