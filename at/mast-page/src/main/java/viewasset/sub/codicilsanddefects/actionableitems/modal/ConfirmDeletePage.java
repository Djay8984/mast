package viewasset.sub.codicilsanddefects.actionableitems.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.ViewActionableItemPage;

public class ConfirmDeletePage extends BasePage<ConfirmDeletePage>
{

    @Visible
    @Name("Delete Button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(2)")
    private WebElement deleteButton;

    @Visible
    @Name("Go back button")
    @FindBy(css = "[data-ng-transclude=\"footer\"] button:nth-child(1)")
    private WebElement goBackButton;

    @Step("Click Go Back button")
    public ViewActionableItemPage clickGoBackButton()
    {
        goBackButton.click();
        return PageFactory.newInstance(ViewActionableItemPage.class);
    }

    @Step("Click Delete button")
    public CodicilsAndDefectsPage clickDeleteButton()
    {
        deleteButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }
}
