package viewasset.sub.codicilsanddefects.defects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.jobs.details.ViewJobDetailsPage;

@Deprecated
public class DefectsSubPage extends BasePage<DefectsSubPage>
{
    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Click on Navigate Back Button")
    public ViewJobDetailsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(ViewJobDetailsPage.class);
    }
}

