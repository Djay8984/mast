package viewasset.sub.codicilsanddefects.actionableitems;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import typifiedelement.TextArea;
import viewasset.sub.codicilsanddefects.CodicilsAndDefectsPage;
import viewasset.sub.codicilsanddefects.actionableitems.modal.BreakLinkToTemplateModalPage;
import viewasset.sub.codicilsanddefects.actionableitems.select.SelectAssetItemPage;
import viewasset.sub.codicilsanddefects.actionableitems.template.AddActionableItemTemplatePage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import viewasset.sub.jobs.details.actionableitems.ActionableItemTemplatePage;
import viewasset.sub.jobs.details.conditionofclass.ConditionsOfClassPage;
import workhub.sub.SearchFilterSubPage;

public class AddActionableItemPage extends BasePage<AddActionableItemPage>
{

    @Visible
    @Name("Title Text Box")
    @FindBy(css = "#tpl-title input")
    private WebElement titleTextBox;

    @Visible
    @Name("Description Text Box")
    @FindBy(id = "description")
    private TextArea descriptionTextBox;

    @Visible
    @Name("Imposed Date Text Box")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextBox;

    @Visible
    @Name("Due Date Text Box")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextBox;

    @Name("Selected Item")
    @FindBy(css = "[data-ng-repeat='item in vm.data.selected']")
    private WebElement selectedItem;

    @Name("Select Item Button")
    @FindBy(id = "actionable-item-item")
    private WebElement selectItemOrChangeSelectionButton;

    @Name("Start from a template button")
    @FindBy(css = "[data-ng-click='vm.selectTemplate()']")
    private WebElement startFromATemplateButton;

    @Name("Start from a template button")
    @FindBy(css = "[data-ng-click='vm.selectTemplate()'].secondary-button")
    private WebElement changeTemplate;

    @Name("LR Internal button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(2)")
    private WebElement lrInternalButton;

    @Name("LR and Customer button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(1)")
    private WebElement lrAndCustomerButton;

    @Name("All button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(3)")
    private WebElement allButton;

    @Name("Statutory button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(2)")
    private WebElement statutoryButton;

    @Name("Class button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(1)")
    private WebElement classButton;

    @Name("External button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(3)")
    private WebElement externalButton;

    @Name("Surveyour guidance text input")
    @FindBy(id = "surveyor-guidance")
    private WebElement surveyorTextInput;

    @Name("Attachments Icon")
    @FindBy(css = ".attachment-directive-icon-light")
    private WebElement attachmentIcon;

    @Name("Typified by text area")
    @FindBy(tagName = "textarea")
    private TextArea textArea;

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "#tpl- button:nth-child(1)")
    private WebElement cancelButton;

    @Visible
    @Name("Save Button")
    @FindBy(css = "#tpl- button:nth-child(2)")
    private WebElement saveButton;

    @Name("Save Button")
    @FindBy(css = "[data-ng-click='vm.breakLink()']")
    private WebElement breakLinkToTemplateButton;

    @Visible
    @Name("Navigate back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Name("Imposed Date Error Message Text")
    @FindBy(css = "#tpl-imposedDate [data-ng-if='vm.showMessage']")
    private WebElement imposedDateErrorMessageText;

    @Name("Due Date Error Message Text")
    @FindBy(css = "#tpl-dueDate [data-ng-if='vm.showMessage']")
    private WebElement dueDateErrorMessageText;

    @Name("Requires approval for change checkbox")
    @FindBy(css = "#tpl-requireApproval button")
    private CheckBox requiresApprovalCheckbox;

    @Name("Surveyor guidance text input")
    @FindBy(id = "surveyor-guidance")
    private TextArea surveyorGuidanceTextBox;

    @Visible
    @Name("Start from a template button")
    @FindBy(css = "[data-ng-show='!vm.data.value'] [data-ng-click='vm.selectTemplate()']")
    private WebElement startFromTemplateButton;

    @Name("Template Name")
    @FindBy(css = "[data-ng-bind='vm.data.selected.title']")
    private WebElement templateName;

    @Name("Error Message")
    @FindBy(css = "[data-ng-if=\"errorCtrl.showMessage\"]")
    private WebElement errorMessage;

    @Step("Get Template Name")
    public String getTemplateName()
    {
        return templateName.getText();
    }

    @Step("Click select Item Button")
    public SelectAssetItemPage clickSelectItemOrChangeSelectionButton()
    {
        selectItemOrChangeSelectionButton.click();
        return PageFactory.newInstance(SelectAssetItemPage.class);
    }

    @Step("Get select Item Button Text")
    public String getSelectItemOrChangeSelectionButtonText()
    {
        return selectItemOrChangeSelectionButton.getText();
    }

    @Step("get item selected text")
    public String getSelectedItemText()
    {
        return selectedItem.getText();
    }

    @Step("Get error message")
    public String getErrorMessage()
    {
        return errorMessage.getText();
    }

    @Step("Check if Break Link To template button is displayed")
    public boolean isBreakLinkToTemplateButtonDisplayed()
    {
        return breakLinkToTemplateButton.isDisplayed();
    }

    @Step("Check if Change template button is displayed")
    public boolean isChangeTemplateButtonDisplayed()
    {
        return changeTemplate.isDisplayed();
    }

    @Step("Click Change template button")
    public AddActionableItemTemplatePage clickChangeTemplate()
    {
        changeTemplate.click();
        return PageFactory.newInstance(AddActionableItemTemplatePage.class);
    }

    @Step("Click Break Link To template button")
    public BreakLinkToTemplateModalPage clickBreakLinkToTemplate()
    {
        breakLinkToTemplateButton.click();
        return PageFactory.newInstance(BreakLinkToTemplateModalPage.class);
    }

    @Step("Set value to Description Text Box")
    public AddActionableItemPage setDescription(String title)
    {
        descriptionTextBox.clear();
        descriptionTextBox.sendKeys(title);
        return this;
    }

    @Step("Get value from Title Text Box")
    public String getTitle()
    {
        return titleTextBox.getText();
    }

    @Step("Set value to Title Text Box")
    public AddActionableItemPage setTitle(String title)
    {
        titleTextBox.clear();
        titleTextBox.sendKeys(title);
        return this;
    }

    @Step("Set Imposed Date")
    public AddActionableItemPage setImposedDate(String imposedDate)
    {
        imposedDateTextBox.clear();
        imposedDateTextBox.sendKeys(imposedDate);
        imposedDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Set Due Date")
    public AddActionableItemPage setDueDate(String imposedDate)
    {
        dueDateTextBox.clear();
        dueDateTextBox.sendKeys(imposedDate);
        dueDateTextBox.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Click LR Internal Button")
    public AddActionableItemPage clickLrInternalButton()
    {
        lrInternalButton.click();
        return this;
    }

    @Step("Click LR and Customer Button")
    public AddActionableItemPage clickLrAndCustomerButton()
    {
        lrAndCustomerButton.click();
        return this;
    }

    @Step("Click All Button")
    public AddActionableItemPage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Click on Statutory button")
    public AddActionableItemPage clickStatutoryButton()
    {
        statutoryButton.click();
        return this;
    }

    @Step("Check that Statutory button is selected")
    public boolean isStatutoryButtonSelected()
    {
        return statutoryButton.findElement(By.cssSelector("input")).getAttribute("checked")
                .contains("true");
    }

    @Step("Click on Class button")
    public AddActionableItemPage clickClassButton()
    {
        classButton.click();
        return this;
    }

    @Step("Check that Class button is selected")
    public boolean isClassButtonSelected()
    {
        return classButton.findElement(By.cssSelector("input")).getAttribute("checked")
                .contains("true");
    }

    @Step("Click on External button")
    public AddActionableItemPage clickExternalButton()
    {
        externalButton.click();
        return this;
    }

    @Step("Set Surveyor text input with '{0}'")
    public AddActionableItemPage setSurveyorTextInput(String input)
    {
        surveyorTextInput.sendKeys(input);
        return this;
    }

    @Step("Click on attachments")
    public AttachmentsAndNotesPage clickAttachmentButton()
    {
        attachmentIcon.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Click on Start from a template button")
    public AddActionableItemTemplatePage clickStartFromATemplateButton()
    {
        startFromATemplateButton.click();
        waitForJavascriptFrameworkToFinish();
        return com.frameworkium.core.ui.pages.PageFactory.newInstance(AddActionableItemTemplatePage.class);
    }

    /**
     * @return CodicilsAndDefectsPage or ConditionsOfClassPage
     */
    @Step("Click Save Button")
    public <T extends BasePage<T>> T clickSaveButton(Class<T> pageObjectClass)
    {
        saveButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click Cancel Button")
    public ConditionsOfClassPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ConditionsOfClassPage.class);
    }

    @Step("Click navigate back button")
    public CodicilsAndDefectsPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(CodicilsAndDefectsPage.class);
    }

    @Step("Is Save button Enabled")
    public boolean isSaveButtonEnabled()
    {
        return saveButton.isEnabled();
    }

    @Step("Is Due Date Field Enabled")
    public boolean isDueDateFieldEnabled()
    {
        return dueDateTextBox.isEnabled();
    }

    @Step("Check for error icon is displayed in Imposed Date TextBox ")
    public boolean isErrorIconDisplayedInImposedDateTextBox(AddActionableItemPage.Type type)
    {
        return imposedDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Check for error icon is displayed in Due Date TextBox ")
    public boolean isErrorIconDisplayedInDueDateTextBox(AddActionableItemPage.Type type)
    {
        return dueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Get Due Date Error Message Text")
    public String getDueDateErrorMessageText()
    {
        return dueDateErrorMessageText.getText().trim();
    }

    @Step("Get Imposed Date Error Message Text")
    public String getImposedDateErrorMessageText()
    {
        return imposedDateErrorMessageText.getText().trim();
    }

    @Step("Get title field helper text")
    public String getTitleFieldHelperText()
    {
        return titleTextBox.getAttribute("placeholder").trim();
    }

    @Step("Get description field helper text")
    public String getTitleDescriptionFieldHelperText()
    {
        return descriptionTextBox.getAttribute("placeholder").trim();
    }

    @Step("Is LR Internal button enabled")
    public boolean isLrInternalButtonEnabled()
    {
        return lrInternalButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is LR and customer button enabled")
    public boolean isLrAndCustomerButtonEnabled()
    {
        return lrAndCustomerButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Is LR and customer button selected")
    public boolean isLrAndCustomerButtonSelected()
    {
        return lrAndCustomerButton.findElement(By.tagName("input")).getAttribute("checked").contains("true");
    }

    @Step("Is LR Internal button selected")
    public boolean isLrInternalButtonSelected()
    {
        return lrInternalButton.findElement(By.tagName("input")).getAttribute("checked").contains("true");
    }

    @Step("Is All button selected")
    public boolean isAllButtonSelected()
    {
        return allButton.findElement(By.tagName("input")).getAttribute("checked").contains("true");
    }

    @Step("Is All button enabled")
    public boolean isAllButtonEnabled()
    {
        return allButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Get Title Text")
    public String getTitleText()
    {
        return titleTextBox.getAttribute("value").trim();
    }

    @Step("Get Description Text")
    public String getDescriptionText()
    {
        return descriptionTextBox.getAttribute("value").trim();
    }

    @Step("Get Imposed Date Text")
    public String getImposedDateText()
    {
        return imposedDateTextBox.getAttribute("value").trim();
    }

    @Step("Get Due Date Text")
    public String getDueDateText()
    {
        return dueDateTextBox.getAttribute("value").trim();
    }

    @Step("Check if Title is enabled")
    public boolean isTitleEnabled()
    {
        return titleTextBox.isEnabled();
    }

    @Step("Check if Description field is enabled")
    public boolean isDescriptionEnabled()
    {
        return descriptionTextBox.isEnabled();
    }

    @Step("Check if Surveyor guidance field is enabled")
    public boolean isSurveyorGuidanceTextEnabled()
    {
        return surveyorTextInput.isEnabled();
    }

    @Step("Get Style attribute on Description Text box")
    public Dimension getTextAreaDimension()
    {
        return descriptionTextBox.getSize();
    }

    @Step("Get Style attribute on Description Text box")
    public AddActionableItemPage setTextAreaSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END)
                .perform();
        textArea.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Click Class Category button")
    public AddActionableItemPage clickClassCategoryButton()
    {
        classButton.click();
        return PageFactory.newInstance(AddActionableItemPage.class);
    }

    @Step("Is Required Approval for Change Selected")
    public boolean isRequiredApprovalForChangeSelected()
    {
        return requiresApprovalCheckbox.getAttribute("class").contains("checked");
    }

    @Step("Get description field helper text")
    public String getDescriptionFieldHelperText()
    {
        return descriptionTextBox.getAttribute("placeholder").trim();
    }

    @Step("Get Surveyor guidance field helper text")
    public String getSurveyorGuidanceFieldHelperText()
    {
        return surveyorGuidanceTextBox.getAttribute("placeholder").trim();
    }

    @Step("Resize Description Text box")
    public AddActionableItemPage setDescriptionTextBoxSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END).perform();
        descriptionTextBox.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Resize  Surveyor Guidance Text box")
    public AddActionableItemPage setSurveyorGuidanceTextBoxSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END).perform();
        surveyorGuidanceTextBox.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Get Description Field Dimension")
    public Dimension getDescriptionFieldDimension()
    {
        return descriptionTextBox.getSize();
    }

    @Step("Get Surveyor Guidance Field Dimension")
    public Dimension getSurveyorGuidanceFieldDimension()
    {
        return surveyorGuidanceTextBox.getSize();
    }

    @Step("see if the error icon is displayed in Title Text Box")
    public boolean isTitleTextBoxErrorIconDisplayed(Type type)
    {
        return titleTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in Description Text Box")
    public boolean isDescriptionTextBoxErrorIconDisplayed(Type type)
    {
        return descriptionTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("see if the error icon is displayed in Imposed Date Text Box")
    public boolean isImposedDateTextBoxErrorIconDisplayed(Type type)
    {
        return imposedDateTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Click Start from Template button")
    public ActionableItemTemplatePage clickStartFromTemplateButton()
    {
        startFromTemplateButton.click();
        return com.frameworkium.core.ui.pages.PageFactory.newInstance(ActionableItemTemplatePage.class);
    }

    @Step("see if the error icon is displayed in Due Date Text Box")
    public boolean isDueDateTextBoxErrorIconDisplayed(Type type)
    {
        return dueDateTextBox.getCssValue("background-image").contains(type.image);
    }

    public enum Type
    {
        ERROR("images/red_warning.png");

        private final String image;

        Type(String image)
        {
            this.image = image;
        }
    }

}
