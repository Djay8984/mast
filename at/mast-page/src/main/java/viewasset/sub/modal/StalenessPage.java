package viewasset.sub.modal;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class StalenessPage extends BasePage<StalenessPage>
{

    @Name("Header")
    @FindBy(tagName = "h1")
    private WebElement header;

    @Name("Body")
    @FindBy(css = "[data-ng-bind='message']")
    private WebElement body;

    @Name("Reload Button")
    @FindBy(id = "reload-modal-action")
    private WebElement reloadButton;

    @Step("Retrieve header text from staleness dialog.")
    public String getHeader()
    {
        return header.getText();
    }

    @Step("Retrieve body message from staleness dialog.")
    public String getBody()
    {
        return body.getText();
    }

    @Step("Click Reload Button")
    public <T extends BasePage<T>> T clickReloadButton(Class<T> clazz)
    {
        reloadButton.click();
        return PageFactory.newInstance(clazz);
    }

}
