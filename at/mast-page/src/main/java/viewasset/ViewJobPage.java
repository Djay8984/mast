package viewasset;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import header.HeaderPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ViewJobPage extends BasePage<ViewJobPage>
{

    @Name("Job Page Header Container")
    @FindBy(css = ".grid-content.small-4 h2")
    @Visible
    private WebElement jobPageHeaderContainer;

    @Name("Job Name Text")
    @FindBy(css = "[data-ng-bind*='vm.job.id']")
    @Visible
    private WebElement jobNameText;

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Verify if Job Page is displayed")
    public boolean isJobPageDisplayed()
    {
        return jobPageHeaderContainer.getText().contains("Job");
    }

}




