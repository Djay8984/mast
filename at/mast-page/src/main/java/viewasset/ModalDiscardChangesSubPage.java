package viewasset;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ModalDiscardChangesSubPage extends BasePage<ModalDiscardChangesSubPage>
{

    @Name("Header")
    @FindBy(tagName = "h1")
    private WebElement header;

    @Name("Body")
    @FindBy(tagName = "modal-body")
    private WebElement body;

    //button OK and Cancel will not appear at the same time
    @Name("OK Button")
    @FindBy(css = "modal-footer [data-ng-click='$close(false)']")
    private WebElement oKButton;

    @Name("Cancel Button")
    @FindBy(css = "modal-footer [data-ng-click='$close(false)']")
    private WebElement cancelButton;

    @Name("Delete item Button")
    @FindBy(css = "modal-footer [data-ng-click='$close(true)']")
    private WebElement deleteItemButton;

    @Step("Retrieve header text")
    public String getHeader()
    {
        return header.getText();
    }

    @Step("Retrieve body message from delete item message window")
    public String getBody()
    {
        return body.getText();
    }

    @Step("Click OK Button")
    public void clickOkButton()
    {
        oKButton.click();
    }

    @Step("Click Cancel Button")
    public void clickCancelButton()
    {
        cancelButton.click();
    }

    @Step("Click Delete item Button")
    public void clickDeleteItemButton()
    {
        deleteItemButton.click();
    }

}
