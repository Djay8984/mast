package viewasset.assetheaderadditionalinformation;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.ViewAssetPage;

public class AttachmentsAndNotesPage extends BasePage<AttachmentsAndNotesPage>
{

    @Visible
    @Name("Close Button")
    @FindBy(css = ".close-button")
    private WebElement closeButton;

    @Step("Click close button")
    public ViewAssetPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(ViewAssetPage.class);
    }
}
