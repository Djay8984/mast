package viewasset.assetheaderadditionalinformation.edit;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.assetheaderadditionalinformation.edit.assetdetails.EditAssetDetailsPage;

public class SpecifyDetailsPage extends BasePage<SpecifyDetailsPage>
{
    @Name("Asset details tab")
    @FindBy(css = "#asset-edit-details")
    @Visible
    private WebElement assetDetailsTab;

    @Name("Customer and offices tab")
    @FindBy(css = "#asset-edit-customers")
    @Visible
    private WebElement customerAndOfficesTab;

    public EditAssetDetailsPage getEditAssetDetailsPage()
    {
        return PageFactory.newInstance(EditAssetDetailsPage.class);
    }
}
