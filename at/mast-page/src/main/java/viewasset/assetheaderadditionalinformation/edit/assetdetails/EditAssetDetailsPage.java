package viewasset.assetheaderadditionalinformation.edit.assetdetails;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.ViewAssetPage;

public class EditAssetDetailsPage extends BasePage<EditAssetDetailsPage>
{
    @Visible
    @Name("Asset Name Text Box")
    @FindBy(id = "asset-asset-name")
    private WebElement assetNameTextBox;

    @Visible
    @Name("Builder Text Box")
    @FindBy(id = "asset-builder")
    private WebElement builderTextBox;

    @Visible
    @Name("Save Button")
    @FindBy(id = "edit-asset-and-save")
    private WebElement saveButton;

    @Visible
    @Name("AssetTab")
    @FindBy(id = "logo-asset-header-asset-container")
    private WebElement assetHeaderTab;

    @Step("Clicking on save button")
    public <T extends BasePage<T>> T clickSaveButton(Class<T> clazz)
    {
        saveButton.click();
        return PageFactory.newInstance(clazz);
    }

    @Step("Get Asset Name Text")
    public String getNameText()
    {
        return this.assetNameTextBox.getAttribute("value");
    }

    @Step("Set name text")
    public EditAssetDetailsPage setNameText(final String newText)
    {
        assetNameTextBox.clear();
        assetNameTextBox.sendKeys(newText);
        return this;
    }

    @Step("Get Builder Text")
    public String getBuilderText()
    {
        return this.builderTextBox.getAttribute("value");
    }

    public ViewAssetPage clickAssetHeaderTab()
    {
        assetHeaderTab.click();
        return PageFactory.newInstance(ViewAssetPage.class);
    }
}
