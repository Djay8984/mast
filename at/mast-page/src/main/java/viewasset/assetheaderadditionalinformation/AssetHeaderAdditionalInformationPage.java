package viewasset.assetheaderadditionalinformation;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.assetheaderadditionalinformation.assetdetails.AssetDetailsPage;

public class AssetHeaderAdditionalInformationPage extends BasePage<AssetHeaderAdditionalInformationPage>
{

    @Name("Asset details tab")
    @FindBy(css = "#asset-cases-view-details")
    @Visible
    private WebElement assetDetailsTab;

    @Name("Customer and offices tab")
    @FindBy(css = "#asset-cases-view-customers-and-offices")
    @Visible
    private WebElement customerAndOfficesTab;

    public AssetDetailsPage getAssetDetailsPage()
    {
        return PageFactory.newInstance(AssetDetailsPage.class);
    }
}
