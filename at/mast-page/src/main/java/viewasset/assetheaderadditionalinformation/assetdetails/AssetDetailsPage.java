package viewasset.assetheaderadditionalinformation.assetdetails;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import header.HeaderPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.assetheaderadditionalinformation.edit.SpecifyDetailsPage;

public class AssetDetailsPage extends BasePage<AssetDetailsPage>
{
    @Visible
    @Name("View More Button")
    @FindBy(css = "[data-ui-sref*='asset.details']")
    private WebElement editButton;

    @Name("Asset Name")
    @FindBy(css = "h2[data-ng-bind*='vm.asset.name']")
    private WebElement assetName;

    @Name("Attachments And Notes Icon")
    @FindBy(css = "div.grid-content.attachments")
    private WebElement attachmentAndNotesIcon;

    @Step("Clicking on edit button")
    public SpecifyDetailsPage clickEditButton()
    {
        editButton.click();
        return PageFactory.newInstance(SpecifyDetailsPage.class);
    }

    @Step("Get Asset Name")
    public String getAssetName()
    {
        return assetName.getText().trim();
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return helper.PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Check if AttachmentsAndNotes Icon Present")
    public boolean isAttachmentAndNotesIconDisplayed()
    {
        return attachmentAndNotesIcon.isDisplayed();
    }

}
