package viewasset.item.attributes.sub.element;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@Name("Add Attribute Items")
@FindBy(css = "tr[data-ng-repeat*='attr in vm.attributeList']")
public class AddAttributeElement extends HtmlElement
{

    @Name("Attribute name")
    @FindBy(css = "[data-ng-bind='attr.name']")
    @Visible
    private WebElement attributeName;

    @Name("Checkbox")
    @FindBy(css = "[data-ng-model='attr.selected']")
    @Visible
    private WebElement checkbox;

    @Name("isMultiple")
    @FindBy(css = "[data-ng-repeat*='attr in vm.attributeList'] td:nth-child(2)")
    @Visible
    private WebElement multiple;

    @Name("Naming order")
    @FindBy(css = "[data-ng-repeat*='attr in vm.attributeList'] td:nth-child(3)")
    @Visible
    private WebElement namingOrder;

    @Name("Minus button")
    @FindBy(id = "-minus")
    private WebElement minusButton;

    @Name("Instances input")
    @FindBy(tagName = "input")
    private WebElement instancesInput;

    @Name("Plus button")
    @FindBy(id = "-plus")
    private WebElement plusButton;

    @Step("Click on the checkbox")
    public void clickCheckbox()
    {
        checkbox.click();
    }

    @Step("Returning the attribute text")
    public String getAttributeName()
    {
        return attributeName.getText();
    }

    @Step("Returning the multiple text")
    public boolean isMultiple()
    {
        return multiple.getText().equals("Yes");
    }

    @Step("Returning the naming order")
    public String getNamingOrder()
    {
        return namingOrder.getText();
    }

    @Step("Clicking on the minus button")
    public void clickMinusButton()
    {
        minusButton.click();
    }

    @Step("Is instances textbox enabled")
    public boolean isInstancesEnabled()
    {
        return instancesInput.isEnabled();
    }

    @Step("Is checkbox checked")
    public boolean isCheckboxChecked()
    {
        return checkbox.getAttribute("class").contains("checked");
    }

    @Step("Enter a value into instances")
    public void enterInstances(String value)
    {
        instancesInput.clear();
        instancesInput.sendKeys(value);
    }

    @Step("Get the value of instances")
    public String getInstancesValue()
    {
        return instancesInput.getAttribute("value");
    }

    @Step("Click on plus button")
    public void clickPlusButton()
    {
        plusButton.click();
    }

    @Step("Is plus button Clickable")
    public boolean isPlusButtonClickable()
    {
        return plusButton.isEnabled();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
