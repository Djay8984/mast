package viewasset.item.attributes;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.item.attributes.base.BaseTable;
import viewasset.item.attributes.element.AttributeElement;
import viewasset.item.attributes.modal.ItemNameUpdatePage;
import viewasset.item.attributes.sub.AddAttributePage;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

public class AttributesSubPage extends BasePage<AttributesSubPage>
{

    private final String NUMBER_OF_ATTRIBUTES_REGEX = "(\\d+) attributes";
    @Name("All Attribute Elements")
    List<AttributeElement> allAttributeElements;
    //Same label is used to display message
    // "No attributes have been added to this item."
    //and count of attributes
    @Name("Attribute Count Label Text")
    @FindBy(css = "#attribute-count-label")
    private WebElement attributeCountLabelText;
    @Name("Navigate back button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;
    @Name("Count number of attribute text")
    @FindBy(css = "#attribute-count-label > strong")
    private WebElement countNumberOfAttributeText;
    @Name("Add attributes Button")
    @FindBy(id = "add-attributes-button")
    @Visible
    private WebElement addAttributesButton;
    @Name("Attribute name column header")
    @FindBy(css = " thead > tr > th:nth-child(1)")
    private WebElement attributeNameColumnHeader;
    @Name("Value column header")
    @FindBy(css = " thead > tr > th:nth-child(2)")
    private WebElement valueColumnHeader;
    @Name("Decorator column header")
    @FindBy(css = " thead > tr > th:nth-child(3)")
    private WebElement decoratorColumnHeader;
    @Name("No Attribute Notice DataElement")
    @FindBy(css = "[data-ng-if='!vm.numAttributes']")
    private WebElement attributeNoticeElement;

    @Name("Updated Item Model Window")
    @FindBy(id = "modalwindow-item-name-updated")
    private List<WebElement> updateChangeModelWindow;

    @Name("Save Changes Button")
    @FindBy(id = "save-changes-button")
    @Visible
    private WebElement saveChangesButton;

    @Name("Discard Changes Button")
    @FindBy(id = "discard-changes-button")
    @Visible
    private WebElement discardChangesButton;

    @Name("Sticky Footer Container")
    @FindBy(css = ".sticky-footer.white")
    @Visible
    private WebElement stickyFooterContainer;

    @Name("Discard Change Model Window")
    @FindBy(id = "modal-discard-changes")
    private List<WebElement> discardChangeModelWindow;

    @Name("Cancel Discard changes Button")
    @FindBy(css = "#modalwindow-discard-changes button[ng-click='$close(false)']")
    private WebElement cancelDiscardChangesButton;

    @Name("Discard changes Button")
    @FindBy(css = "#modalwindow-discard-changes button[ng-click='$close(true)']")
    private WebElement acceptDiscardChangesButton;

    @Name("Attribute Table")
    private AttributeTable attributeTable;

    @Step("Get Attribute Table")
    public AttributeTable getAttributeTable()
    {
        return attributeTable;
    }

    @Step("Retrieve text from Attribute Count Label")
    public String getAttributeCountLabelText()
    {
        return attributeCountLabelText.getText();

    }

    @Step("Retrieve number of Attribute from Attribute Count Label")
    public int getNumberOfAttribute()
    {
        Pattern pattern = Pattern.compile(NUMBER_OF_ATTRIBUTES_REGEX);
        Matcher matcher = pattern.matcher(countNumberOfAttributeText.getText());
        assertThat(matcher.find()).isTrue();
        return Integer.parseInt(matcher.group(1));
    }

    @Step("Verify If Add attributes Button is displayed")
    public boolean isAddAttributesButtonDisplayed()
    {
        return addAttributesButton.isDisplayed();
    }

    @Step("Click Add attribute button")
    public AddAttributePage clickAddAttributesButton()
    {
        addAttributesButton.click();
        return PageFactory.newInstance(AddAttributePage.class);
    }

    @Step("Verify If Attribute Name Column Header Text Is Displayed")
    public boolean isAttributeNameColumnHeaderTextDisplayed()
    {
        return attributeNameColumnHeader.isDisplayed();
    }

    @Step("Retrieve Attribute Name Column Header Text")
    public String getAttributeNameColumnHeaderText()
    {
        return attributeNameColumnHeader.getText();
    }

    @Step("Verify If Value Column Header Text Is Displayed")
    public boolean isValueColumnHeaderTextDisplayed()
    {
        return valueColumnHeader.isDisplayed();
    }

    @Step("Retrieve Value Column Header Text")
    public String getValueColumnHeaderText()
    {
        return valueColumnHeader.getText();
    }

    @Step("Verify If Decorator Column Header Text Is Displayed")
    public boolean isDecoratorColumnHeaderTextDisplayed()
    {
        return decoratorColumnHeader.isDisplayed();
    }

    @Step("Retrieve Decorator Column Header Text")
    public String getDecoratorColumnHeaderText()
    {
        return decoratorColumnHeader.getText();
    }

    @Step("Retrieve all display attribute elements")
    public List<AttributeElement> getAllDisplayedAttributeElement()
    {
        return allAttributeElements;
    }

    @Step("Retrieve all display attribute elements name")
    public List<String> getAllDisplayedAttributeElementNames()
    {
        return allAttributeElements.stream()
                .map(x -> x.getAttributeNameText())
                .collect(Collectors.toList());
    }

    @Step("Get Attribute DataElement by attribute name")
    public AttributeElement getAttributeElementByName(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(x -> x.getAttributeNameText().equals(attributeName))
                .findFirst()
                .orElse(null);
    }

    @Step("Get Attribute Notice Text")
    public String getAttributeNoticeText()
    {
        return attributeNoticeElement.getText().trim();
    }

    /**
     * Click on the clickNavigateBackButton
     *
     * @param pageObjectClass - The pageObject which is expected to be returned
     *                        by clicking the clickNavigateBackButton
     *                        Possible PageObjects: AssetModelSubPage.class
     *                        HierarchyViewSubPage.class
     * @param <T>             The implementing type
     */
    @Step("CLick Navigate Back Button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Set Attribute Value")
    public AttributesSubPage setAttributeValue(String attributeName, String value)
    {
        allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get()
                .setAttributeValue(value);
        return this;
    }

    @Step("Get Attribute Value Text")
    public String getAttributeValue(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getAttributeValue();
    }

    @Step("Select Attribute check box")
    public AttributesSubPage selectAttributeCheckBox(String attributeName)
    {
        WebElement checkBox = allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get();
        if (!checkBox.getAttribute("class").contains("checked"))
            checkBox.findElement(By.cssSelector("span")).click();
        return this;
    }

    @Step("Is Attribute check box selected")
    public boolean isAttributeCheckBoxSelected(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getAttribute("class").contains("checked");
    }

    @Step("Select by visible text")
    public AttributesSubPage selectAttributeDropDownByVisibleText(String attributeName, String value)
    {
        allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().selectDropDownByVisibleText(value);
        return this;
    }

    @Step("Get Dropdown text")
    public String getSelectedDropDownText(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getSelectedDropDownText();
    }

    @Step("Get Attribute Tag Name")
    public String getAttributeTagName(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get()
                .getAttributeValueType();
    }

    @Step("Get Attribute of Attribute Value")
    public String getAttributeOfAttributeValue(String attributeName, String attribute)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get()
                .getAttribute(attribute);
    }

    @Step("Check if the error icon is displayed")
    public boolean isErrorIconDisplayed(String attributeName, type type)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getCssValue("background-image").contains(type.image);
    }

    @Step("get validation message")
    public String getValidationErrorMessage(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getValidationMessage();
    }

    @Step("Clear Attribute Value Text")
    public AttributesSubPage clearAttributeValue(String attributeName)
    {
        allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().clearAttributeValue();
        return this;
    }

    @Step("Un select Attribute check box")
    public AttributesSubPage unSelectAttributeCheckBox(String attributeName)
    {
        WebElement checkBox = allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get();

        if (checkBox.getAttribute("class").contains("checked"))
            checkBox.findElement(By.cssSelector("span")).click();
        return this;
    }

    @Step("Get Attribute drop down values")
    public List<String> getAttributeDropDownValues(String attributeName)
    {
        return allAttributeElements.stream()
                .filter(e -> e.getAttributeNameText().equals(attributeName))
                .findFirst().get().getAttributeDropdownValues();
    }

    @Step("Clicking Discard Changes Button")
    public AttributesSubPage clickDiscardChangesButton()
    {
        discardChangesButton.click();
        return this;
    }

    @Step("Verify if Discard Changes Button is enabled")
    public boolean isDiscardChangesButtonEnabled()
    {
        return discardChangesButton.isEnabled();
    }

    @Step("Clicking Save Changes Button")
    public ItemNameUpdatePage clickSaveChangesButton()
    {
        saveChangesButton.click();
        return PageFactory.newInstance(ItemNameUpdatePage.class);
    }

    @Step("Verify if Save Changes Button is enabled")
    public boolean isSaveChangesButtonEnabled()
    {
        return saveChangesButton.isEnabled();
    }

    @Step("Is Discard Changes Button in sticky footer")
    public boolean isDiscardChangesButtonInStickyFooter()
    {
        return discardChangesButton.findElement(By.id("discard-changes-button")).isDisplayed();
    }

    @Step("Is Save Changes Button in sticky footer")
    public boolean isSaveChangesButtonInStickyFooter()
    {
        return saveChangesButton.findElement(By.id("save-changes-button")).isDisplayed();
    }

    @Step("Click for discard changes model window")
    public boolean isDiscardChangesModelWindowDisplayed()
    {
        return discardChangeModelWindow.get(0).isDisplayed();

    }

    @Step("Click Cancel discard changes button")
    public AttributesSubPage clickCancelDiscardChangesButton()
    {
        cancelDiscardChangesButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Accept discard changes button")
    public AttributesSubPage clickAcceptDiscardChangesButton()
    {
        acceptDiscardChangesButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    public enum type
    {
        ERROR("images/red_warning.png");
        private final String image;

        type(String image)
        {
            this.image = image;
        }
    }

    @FindBy(css = "section[data-ng-if='vm.numAttributes']")
    public class AttributeTable extends BaseTable
    {
    }
}
