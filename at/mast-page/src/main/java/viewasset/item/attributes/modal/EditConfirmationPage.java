package viewasset.item.attributes.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.AssetModelSubPage;

public class EditConfirmationPage extends BasePage<EditConfirmationPage>
{

    @Visible
    @Name("Cancel Button")
    @FindBy(css = "[ng-click='$close(false)']")
    private WebElement cancelButton;

    @Visible
    @Name("Save Changes Button")
    @FindBy(css = "[ng-click='$close('save')']")
    private WebElement saveChangesButton;

    @Visible
    @Name("Discard Changes Button")
    @FindBy(css = "[ng-click='$close('doDiscard')']")
    private WebElement discardChangesButton;

    @Step("Click Save Changes Button")
    public AssetModelSubPage clickSaveChangesButton()
    {
        saveChangesButton.click();
        return PageFactory.newInstance(AssetModelSubPage.class);
    }
}
