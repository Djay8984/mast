package viewasset.item.attributes.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.item.attributes.AttributesSubPage;

public class ItemNameUpdatePage extends BasePage<ItemNameUpdatePage>
{

    @Visible
    @Name("Ok Button")
    @FindBy(css = "#modal-item-name-updated button")
    private WebElement okButton;

    @Step("Click on Ok save changes button")
    public AttributesSubPage clickOkButton()
    {
        wait.until(ExpectedConditions.visibilityOf(okButton));
        okButton.click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }
}
