package viewasset.item.attributes.element;

import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.item.attributes.AttributesSubPage;

import java.util.List;
import java.util.stream.Collectors;

@Name("Attributes element")
@FindBy(css = "#attribute-table tbody tr")
public class AttributeElement extends HtmlElement
{

    @Name("Attribute Name")
    @FindBy(css = "[data-ng-bind='attr.attributeType.name']")
    private WebElement attributeNameText;

    @Name("Attribute Value")
    @FindBy(css = "[data-ng-model='vm.ngModel']")
    private WebElement attributeValue;

    @Name("Attribute Decorator")
    @FindBy(css = "td:nth-child(3) > div")
    private WebElement attributeDecoratorText;

    @Name("Last column container")
    @FindBy(css = "td:nth-last-child(1) span")
    private WebElement lastColumnContainer;

    @Name("Delete Attribute Icon")
    @FindBy(css = "td:nth-last-child(1) > [data-ng-click]")
    private List<WebElement> deleteAttributeIcon;

    @Name("Validation message element")
    @FindBy(css = ".validation-light.ng-scope[id|='validation-message-dynamic']")
    private WebElement validationMessage;

    @Step("Returning Attribute Name Text")
    public String getAttributeNameText()
    {
        return attributeNameText.getText();
    }

    @Step("Returning Attribute Value Text")
    public String getAttributeValue()
    {
        return attributeValue.getAttribute("value");
    }

    @Step("Set Attribute Value")
    public AttributeElement setAttributeValue(String value)
    {
        attributeValue.clear();
        attributeValue.sendKeys(value);
        attributeValue.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Returning Attribute Decorator Text")
    public String getAttributeDecoratorText()
    {
        return attributeDecoratorText.getText();
    }

    @Step("Select by visible text")
    public AttributesSubPage selectDropDownByVisibleText(String text)
    {
        Select select = new Select(attributeValue);
        select.selectByVisibleText(text);
        return com.frameworkium.core.ui.pages.PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Get Selected dropDown options text")
    public String getSelectedDropDownText()
    {
        Select select = new Select(attributeValue);
        return select.getFirstSelectedOption().getText();
    }

    @Step("Get for validation message")
    public String getValidationMessage()
    {
        return validationMessage.getText().trim();
    }

    @Step("Clear Attribute Value")
    public AttributeElement clearAttributeValue()
    {
        attributeValue.clear();
        return this;
    }

    @Step("Get all dropDown options")
    public List<String> getAttributeDropdownValues()
    {
        Select select = new Select(attributeValue);
        return select.getOptions().stream().map(WebElement::getText).collect(Collectors.toList());
    }

    @Step("Click Delete Attribute Icon")
    public AttributesSubPage clickDeleteAttributeIcon()
    {
        deleteAttributeIcon.get(0).findElement(By.tagName("svg")).click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Is Mandatory Text in last column container")
    public boolean isMandatoryTextDisplayInLastColumnContainer()
    {
        return lastColumnContainer.getText().equals("Mandatory");
    }

    @Step("Get last column container Text")
    public String getLastColumnContainerText()
    {
        return lastColumnContainer.getText().trim();
    }

    @Step("Is Delete Icon display in last column container")
    public boolean isDeleteIconDisplay()
    {
        return deleteAttributeIcon.size() > 0;
    }

    @Step("Returning the attribute value type")
    public String getAttributeValueType()
    {
        return attributeValue.getTagName();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

}
