package viewasset.item.attributes.base;

import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.item.attributes.element.AttributeElement;

import java.util.List;

public class BaseTable extends HtmlElement
{

    @Name("Attribute Elements")
    private List<AttributeElement> attributeElements;

    @Step("Get Attribute Elements")
    public List<AttributeElement> getRows()
    {
        return attributeElements;
    }

}
