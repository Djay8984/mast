package viewasset.item.attributes.sub;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.attributes.sub.element.AddAttributeElement;

import java.util.List;

public class AddAttributePage extends BasePage<AddAttributePage>
{

    @Name("Close button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Name("Attribute name header")
    @FindBy(css = "[role='columnheader']:nth-child(1)")
    private WebElement attributeNameHeader;

    @Name("isMultiple header")
    @FindBy(css = "[role='columnheader']:nth-child(2)")
    private WebElement multipleHeader;

    @Name("Naming order header")
    @FindBy(css = "[role='columnheader']:nth-child(3)")
    private WebElement namingOrderHeader;

    @Name("Number Instances header")
    @FindBy(css = "[role='columnheader']:nth-child(4)")
    private WebElement numberInstancesHeader;

    @Name("Attributes")
    private List<AddAttributeElement> attributes;

    @Name("Count attributes label")
    @FindBy(id = "attribute-selected-count-label")
    private WebElement countAttributes;

    @Name("Add attributes button")
    @FindBy(id = "add-attribute-button")
    private WebElement addAttributeButton;

    @Step("Clicking on the close button")
    public AttributesSubPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Return the attribute name header")
    public String attributeHeaderText()
    {
        return attributeNameHeader.getText();
    }

    @Step("Return the multiple name header")
    public String multipleHeaderText()
    {
        return multipleHeader.getText();
    }

    @Step("Return the naming order header")
    public String namingHeaderText()
    {
        return namingOrderHeader.getText();
    }

    @Step("Return the instances header")
    public String instancesHeaderText()
    {
        return numberInstancesHeader.getText();
    }

    @Step("Return a list of attributes")
    public List<AddAttributeElement> getAttributes()
    {
        return attributes;
    }

    @Step("Returning an attribute based on its name")
    public AddAttributeElement getAttributeByName(String name)
    {
        return attributes.stream()
                .filter(o -> o.getAttributeName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an attribute based on its name")
    public AddAttributeElement getNonMultipleAttribute()
    {
        return attributes.stream()
                .filter(o -> !o.isMultiple())
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an attribute based on row number")
    public AddAttributeElement getAttributeByIndex(int rowNumber)
    {
        AddAttributeElement element = null;
        if (attributes.size() >= rowNumber)
        {
            element = attributes.get(rowNumber - 1);
        }
        return element;
    }

    @Step("Click the add attribute button")
    public AttributesSubPage clickAddAttributeButton()
    {
        addAttributeButton.click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Is the add attribute button disabled")
    public boolean isAddAttributeButtonDisabled()
    {
        return driver.findElements(By.cssSelector("#add-attribute-button[disabled]")).size() > 0;
    }

    @Step("Return the amount of selected attributes")
    public String countAttributesSelected()
    {
        return countAttributes.getText();
    }
}
