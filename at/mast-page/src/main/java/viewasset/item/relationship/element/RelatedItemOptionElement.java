package viewasset.item.relationship.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import com.paulhammant.ngwebdriver.NgWebDriver;
import helper.PageFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import util.AppHelper;
import viewasset.item.relationship.AddRelatedItemPage;

import java.util.List;

@Name("Related Item Option element")
@FindBy(css = "li[data-ng-class*='is-expanded']:not(.is-expanded)")
public class RelatedItemOptionElement extends HtmlElement
{

    @Name("Item name")
    @FindBy(css = ".asset-model-hierarchy-selector .ng-binding")
    @Visible
    private WebElement itemName;

    @Name("Node icon")
    @FindBy(className = "node-expand-toggle")
    private WebElement nodeIcon;

    @Name("Item Radio Button")
    @FindBy(css = ".radio")
    private WebElement itemRadioButton;

    @Name("Child Items")
    private List<RelatedItemOptionElement> childItems;

    private NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) BaseTest.getDriver());

    @Step("Returning item name")
    public String getItemName()
    {
        AppHelper.scrollToBottom();
        ngWebDriver.waitForAngularRequestsToFinish();
        return itemName.getText();
    }

    @Step("Finding out if an item is expandable")
    public boolean isItemExpandable()
    {
        return nodeIcon.findElements(By.cssSelector("span[data-ng-if*='!node.expanded']")).size() > 0;
    }

    @Step("Has the item been expanded")
    public boolean isItemExpanded()
    {
        return this.findElements(By.tagName("circle")).size() > 0;
    }

    @Step("Clicking on the item node")
    public RelatedItemOptionElement clickItemNode()
    {
        WebDriver driver = BaseTest.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(false);",
                        nodeIcon);
        wait.until(ExpectedConditions.elementToBeClickable(nodeIcon));
        nodeIcon.click();
        wait.until(itemExpanded());
        return this;
    }

    @Step("Is Item radio button selected")
    public boolean isItemRadioButtonSelected()
    {
        return itemRadioButton.findElement(By.tagName("input")).getAttribute("class").contains("ng-valid-parse");
    }

    @Step("Is item radio button disabled")
    public boolean isItemRadioButtonEnabled()
    {
        return itemRadioButton.findElement(By.tagName("input")).isEnabled();
    }

    @Step("Select item radio button")
    public AddRelatedItemPage selectItemRadioButton()
    {
        itemRadioButton.findElement(By.tagName("label")).click();
        return PageFactory.newInstance(AddRelatedItemPage.class);
    }

    @Step("Get Child Items")
    public List<RelatedItemOptionElement> getChildItems()
    {
        return childItems;
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    private ExpectedCondition<Boolean> itemExpanded()
    {
        return (driver) -> !isItemExpandable();
    }

}
