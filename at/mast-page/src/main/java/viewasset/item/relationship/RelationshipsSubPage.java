package viewasset.item.relationship;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.item.relationship.element.RelatedItemElement;
import viewasset.sub.AssetModelSubPage;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assert_;

public class RelationshipsSubPage extends BasePage<RelationshipsSubPage>
{

    private final String NUMBER_OF_ITEMS_REGEX = "(\\d+) items";

    @Name("Items Count For Relationship Label Text")
    @FindBy(css = "#relationship-count-label")
    private WebElement relatedItemsCountLabelText;

    @Name("Count number of related items relationship text")
    @FindBy(css = "#relationship-count-label > strong")
    private WebElement countNumberOfRelatedItemsText;

    @Name("Create new relationship button")
    @FindBy(id = "create-new-relationship-button")
    @Visible
    private WebElement createNewRelationshipButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = "div#item-header-back.ng-isolate-scope")
    private WebElement navigateBackButton;

    @Name("All Related Item Elements")
    private List<RelatedItemElement> allRelatedItemElements;

    @Step("Retrieve text from related items count Label")
    public String getRelatedItemsCountLabelText()
    {
        return relatedItemsCountLabelText.getText();
    }

    @Step("Retrieve number of Attribute from Attribute Count Label")
    public int getNumberOfRelatedItem()
    {
        Pattern pattern = Pattern.compile(NUMBER_OF_ITEMS_REGEX);
        Matcher matcher = pattern.matcher(countNumberOfRelatedItemsText.getText());
        assert_().withFailureMessage("Number of related items is expected to be displayed in format [Showing n items] ").that(matcher.find())
                .isTrue();
        return Integer.parseInt(matcher.group(1));
    }

    @Step("Verify if create new relationship button is displayed")
    public boolean isCreateNewRelationshipButtonDisplayed()
    {
        return createNewRelationshipButton.isDisplayed();
    }

    @Step("Verify if create new relationship button is enabled")
    public boolean isCreateNewRelationshipButtonEnabled()
    {
        return createNewRelationshipButton.isEnabled();
    }

    @Step("Clicking create new relationship button")
    public AddRelatedItemPage clickCreateNewRelationshipButton()
    {
        createNewRelationshipButton.click();
        return PageFactory.newInstance(AddRelatedItemPage.class);
    }

    @Step("Retrieve all display related item elements")
    public List<RelatedItemElement> getAllDisplayedRelatedItemElement()
    {
        return allRelatedItemElements;
    }

    @Step("Retrieve all display related item elements name")
    public List<String> getAllDisplayedRelatedItemElementNames()
    {
        return allRelatedItemElements.stream()
                .map(RelatedItemElement::getRelatedItemNameText)
                .collect(Collectors.toList());
    }

    @Step("Click Navigate Back Button")
    public AssetModelSubPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(AssetModelSubPage.class);
    }

}

