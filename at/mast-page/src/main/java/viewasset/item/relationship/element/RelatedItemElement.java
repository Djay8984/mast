package viewasset.item.relationship.element;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.item.relationship.RelationshipsSubPage;

import java.util.Collections;

@Name("Related Item element")
@FindBy(css = "[data-ng-repeat]")
public class RelatedItemElement extends HtmlElement
{

    @Name("Related Item Name Text")
    @FindBy(css = "[data-ng-bind='relation.toItem.name']")
    private WebElement relatedItemNameText;

    @Name("Related Item Delete Link")
    @FindBy(css = "[data-ng-click='vm.remove(relation)'] svg")
    private WebElement relatedItemDeleteLink;

    @Step("Returning Related Item Name Text")
    public String getRelatedItemNameText()
    {
        return relatedItemNameText.getText();
    }

    @Step("Verify if related item name displayed")
    public boolean isRelatedItemNameTextDisplayed()
    {
        return relatedItemNameText.isDisplayed();
    }

    @Step("Verify if delete related item link displayed")
    public boolean isRelatedItemDeleteLinkDisplayed()
    {
        return relatedItemDeleteLink.isDisplayed();
    }

    @Step("Clicking on delete related item link")
    public RelationshipsSubPage clickDeleteItemRelationshipLink()
    {
        relatedItemDeleteLink.click();
        BaseTest.getWait()
                .until(ExpectedConditions.invisibilityOfAllElements(Collections.singletonList(relatedItemDeleteLink)));
        return PageFactory.newInstance(RelationshipsSubPage.class);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

}
