package viewasset.item.relationship;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import viewasset.item.ItemsDetailsPage;
import viewasset.item.relationship.element.RelatedItemOptionElement;
import viewasset.sub.assetmodel.element.PreviewItemElement;

import java.util.List;
import java.util.stream.Collectors;

public class AddRelatedItemPage extends BasePage<AddRelatedItemPage>
{
    private final static String RESULTS_SELECTOR = "[data-ng-repeat='item in vm.searchResults'] label";

    @Name("All Related item option DataElement")
    List<RelatedItemOptionElement> allRelatedItemOptionElements;

    @Name("Header Add related item")
    @FindBy(css = ".grid-block h2")
    private WebElement headerAddRelatedItem;

    @Name("Close button")
    @FindBy(id = "close-button")
    @Visible
    private WebElement closeButton;

    @Name("Container of item DataElement ")
    @FindBy(css = ".asset-model-hierarchy-selector")
    private WebElement itemElementContainer;

    @Name("Firt root item name")
    @FindBy(css = "ul > [data-ng-class] > #hierarchy-node- > .node-name")
    private WebElement rootItemName;

    @Name("Root item")
    @FindBy(id = "hierarchy-node-")
    private WebElement rootItem;

    @Name("Create relationship Button")
    @FindBy(id = "create-relationship-button")
    private WebElement createRelationshipButton;

    @Visible
    @Name("Search for an item text box")
    @FindBy(id = "create-relationship-search-query")
    private WebElement searchItemTextBox;

    @Visible
    @Name("Search Button")
    @FindBy(id = "create-relationship-search")
    private WebElement searchButton;

    @Name("pagination Count Text")
    @FindBy(css = "p.ng-binding.ng-scope")
    private WebElement paginationCountText;

    @Step("Retrieve Header Add related item text")
    public String getHeaderAddRelatedItemText()
    {
        return headerAddRelatedItem.getText();
    }

    @Step("Retrieve Header Add related item text")
    public ItemsDetailsPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(ItemsDetailsPage.class);
    }

    @Step("Finding out if an item is expandable")
    public boolean isRootItemExpandable()
    {
        return rootItem.findElements(By.cssSelector("span[data-ng-if*='!vm.assetModel.expanded']")).size() > 0;
    }

    @Step("Has the item been expanded")
    public boolean isFirstRootItemExpanded()
    {
        return rootItem.findElements(By.tagName("circle")).size() > 0;
    }

    @Step("Click root item")
    public void clickToExpandRootItem()
    {
        wait.until(ExpectedConditions.elementToBeClickable(rootItem));
        rootItem.click();
        waitForJavascriptFrameworkToFinish();
    }

    @Step("Returning first root item name")
    public String getRootItemName()
    {
        return rootItemName.getText();
    }

    @Step("Returning all related item option DataElement name")
    public List<RelatedItemOptionElement> getAllRelatedItemOptionElements()
    {
        wait.until(ExpectedConditions.visibilityOf(itemElementContainer));
        return allRelatedItemOptionElements;
    }

    @Step("Returning all related item option DataElement name")
    public List<String> getAllRelatedItemOptionElementNames()
    {
        return getAllRelatedItemOptionElements()
                .stream()
                .map(RelatedItemOptionElement::getItemName)
                .collect(Collectors.toList());
    }

    @Step("Returning an item based on item name")
    public RelatedItemOptionElement getItemBasedOnItemName(String itemName)
    {
        return getAllRelatedItemOptionElements().stream()
                .filter(o -> o.getItemName().equals(itemName))
                .findFirst()
                .orElse(null);
    }

    @Step("Clicking button create relationship")
    public RelationshipsSubPage clickCreateRelationshipButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(createRelationshipButton));
        createRelationshipButton.click();
        wait.until(relationshipsDisplayedUponCreation());
        return PageFactory.newInstance(RelationshipsSubPage.class);
    }

    @Step("Verify if create relationship button is enabled")
    public boolean isCreateRelationshipButtonEnabled()
    {
        return createRelationshipButton.isEnabled();
    }

    @Step("Type into Search Text Box")
    public AddRelatedItemPage setSearchItemText(String item)
    {
        searchItemTextBox.clear();
        searchItemTextBox.click();
        searchItemTextBox.sendKeys(item);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Is clear Button is displayed ")
    public boolean isClearButtonDisplayed()
    {
        return searchItemTextBox.getAttribute("class").contains("typeahead-clearable");
    }

    @Step("Returning the root preview item")
    public PreviewItemElement getRootItem()
    {
        return new PreviewItemElement(rootItem);
    }

    @Step("Click Search Button Expect Results")
    public AddRelatedItemPage clickSearchButtonExpectResults()
    {
        Integer currentResultSize = getResults().size();
        searchButton.click();
        AppHelper.waitForOnScreenAnimation();
        wait.until(waitForSearchResultsExpectResults(currentResultSize));
        return PageFactory.newInstance(AddRelatedItemPage.class);
    }

    @Step("Click Search Button Expect Results")
    public AddRelatedItemPage clickSearchButtonExpectNoResults()
    {
        searchButton.click();
        AppHelper.waitForOnScreenAnimation();
        wait.until(waitForSearchResultsExpectNoResults());
        return PageFactory.newInstance(AddRelatedItemPage.class);
    }

    @Step("Get All result sets")
    public List<String> getResults()
    {
        waitForJavascriptFrameworkToFinish();
        return driver.findElements(By.cssSelector(RESULTS_SELECTOR))
                .stream().map(WebElement::getText).collect(Collectors.toList());
    }

    @Step("Clear Search Text Box")
    public AddRelatedItemPage clearSearchItemText()
    {
        searchItemTextBox.clear();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Returning the page count text")
    public String getPaginationCountText()

    {
        waitForJavascriptFrameworkToFinish();
        return paginationCountText.getText();
    }

    private ExpectedCondition<Boolean> waitForSearchResultsExpectResults(Integer currentResultSize)
    {
        return (driver) -> getResults().size() != currentResultSize;
    }

    private ExpectedCondition<Boolean> waitForSearchResultsExpectNoResults()
    {
        return (driver) -> BaseTest.getDriver()
                .findElement(By.cssSelector("[data-ng-show='!vm.searchResults.length']")).isDisplayed();
    }

    private ExpectedCondition<Boolean> relationshipsDisplayedUponCreation()
    {
        return (driverExpect) -> driver.findElements(By.cssSelector("[data-ng-repeat]")).size() > 0;
    }
}
