package viewasset.item;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.ModalDiscardChangesSubPage;
import viewasset.item.attributes.AttributesSubPage;
import viewasset.item.relationship.RelationshipsSubPage;
import viewasset.item.tasks.TasksSubPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import workhub.sub.SearchFilterSubPage;

public class ItemsDetailsPage extends BasePage<ItemsDetailsPage>
{

    @Name("Item header back button")
    @FindBy(id = "item-header-back")
    @Visible
    private WebElement navigateBackButton;

    @Name("Item Page Title")
    @FindBy(css = "#body-container h2")
    @Visible
    private WebElement itemPageTitle;

    @Name("Item name")
    @FindBy(id = "item-header-item-name")
    @Visible
    private WebElement itemNameText;

    @Name("Attribute values content text")
    @FindBy(css = ".grid-block.vertical.small-6 > .grid-content.noscroll")
    private WebElement attributeValuesContentText;

    @Name("Mark as complete button")
    @FindBy(id = "item-header-mark-as-complete")
    private WebElement markAsCompleteButton;

    @Name("Remove item button")
    @FindBy(id = "item-header-remove-item")
    private WebElement removeItemButton;

    @Name("Attachments And Notes Icon")
    @FindBy(css = "[modal-title='vm.item.name']")
    private WebElement attachmentAndNotesIcon;

    @Name("Active Tab")
    @FindBy(css = ".is-active")
    @Visible
    private WebElement activeTab;

    @Name("Attributes Tab")
    @FindBy(id = "item-tab-attributes")
    @Visible
    private WebElement attributesTab;

    @Name("Relationships Tab")
    @FindBy(id = "item-tab-relationships")
    @Visible
    private WebElement relationshipsTab;

    @Name("Tasks Tab")
    @FindBy(id = "item-tab-tasks")
    @Visible
    private WebElement tasksTab;

    /**
     * Click on the furtherDetailsArrow
     *
     * @param pageObjectClass - The pageObject which is expected to be returned by clicking the futher details arrow
     *                        Possible PageObjects: AssetModelSubPage.class
     *                        HierarchyViewSubPage.class
     * @param <T>             The implementing type
     */
    @Step("Click on remove item button")
    public <T extends BasePage<T>> T clickNavigateBackButton(Class<T> pageObjectClass)
    {
        navigateBackButton.click();
        try
        {
            return PageFactory.newInstance(pageObjectClass);
        }
        catch (TimeoutException e)
        {
            Assert.fail("Unexpected page was loaded or an element inside the page has changed! Page Expected: "
                    + pageObjectClass.getName());
            return null;
        }
    }

    @Step("Retrieve page body header text")
    public String getItemPageTitleText()
    {
        return itemPageTitle.getText();
    }

    @Step("Retrieve item name text")
    public String getItemNameText()
    {
        return itemNameText.getText();
    }

    @Step("Retrieving Attribute Values Content Text")
    public String getAttributeValuesContentText()
    {
        return attributeValuesContentText.getText();
    }

    @Step("Click on remove item button")
    public ModalDiscardChangesSubPage clickRemoveItemButton()
    {
        removeItemButton.click();
        return PageFactory.newInstance(ModalDiscardChangesSubPage.class);
    }

    @Step("Click on attachments")
    public AttachmentsAndNotesPage clickAttachmentAndNotesIcon()
    {
        attachmentAndNotesIcon.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Retrieve selected active tab name")
    public String getSelectedActiveTabName()
    {
        return activeTab.getText();
    }

    @Step("Retrieve the Attributes tab")
    public AttributesSubPage getAttributesTab()
    {
        attributesTab.click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Clicking the Attributes tab")
    public AttributesSubPage clickAttributesTab()
    {
        attributesTab.click();
        return PageFactory.newInstance(AttributesSubPage.class);
    }

    @Step("Clicking the Relationships tab")
    public RelationshipsSubPage clickRelationshipsTab()
    {
        relationshipsTab.click();
        return PageFactory.newInstance(RelationshipsSubPage.class);
    }

    @Step("Clicking the Tasks tab")
    public TasksSubPage clickTasksTab()
    {
        tasksTab.click();
        return PageFactory.newInstance(TasksSubPage.class);
    }

    @Step("Clicking the mark as complete button")
    public void clickMarkAsComplete()
    {
        markAsCompleteButton.click();
    }

    @Step("Returning whether the mark as complete button is enabled")
    public boolean isMarkAsCompleteButtonDisplayed()
    {
        return markAsCompleteButton.isDisplayed();
    }

    @Step("Refreshing page")
    public void refreshPage()
    {
        driver.navigate().refresh();
        waitForJavascriptFrameworkToFinish();
    }

    @Step("Check if remove item button Displayed")
    public boolean isRemoveItemButtonDisplayed()
    {
        return removeItemButton.isDisplayed();
    }

    @Step("Check if AttachmentsAndNotes Icon Present")
    public boolean isAttachmentAndNotesIconDisplayed()
    {
        return attachmentAndNotesIcon.isDisplayed();
    }

    @Step("Check if Tasks tab is displayed")
    public boolean isTasksTabDisplayed()
    {
        return tasksTab.isDisplayed();
    }

    @Step("Check if Relationship tab is displayed")
    public boolean isRelationshipTabDisplayed()
    {
        return relationshipsTab.isDisplayed();
    }

    @Step("Check if navigate Back Button is displayed")
    public boolean isNavigateBackButtonDisplayed()
    {
        return navigateBackButton.isDisplayed();
    }

    @Step("Check if markAsCompleteButton is enabled")
    public boolean isMarkAsCompleteButtonEnabled()
    {
        return markAsCompleteButton.isEnabled();
    }

    @Step("Navigate back to the previous page")
    public void clickNavigateBackButton()
    {
        navigateBackButton.click();
    }

    @Step("Get Header Page")
    public HeaderPage getHeaderPage()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }
}
