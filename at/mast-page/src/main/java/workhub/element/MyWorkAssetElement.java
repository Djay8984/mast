package workhub.element;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.CasesPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.truth.Truth.assertThat;

@Name("Asset cards")
@FindBy(tagName = "asset-card")
public class MyWorkAssetElement extends HtmlElement
{

    private final String NUMBER_OF_CASES_REGEX = "(\\d+) case";

    @Name("Asset Emblem")
    @FindBy(css = "[data-asset-image='vm.asset.assetType']")
    @Visible
    private WebElement assetEmblem;

    @Name("Asset Name")
    @FindBy(css = "[data-ng-if='vm.asset.name']")
    @Visible
    private WebElement assetName;

    @Name("Asset Type")
    @FindBy(css = "[data-ng-if='vm.asset.assetType'] div:nth-child(2)")
    @Visible
    private WebElement assetType;

    @Name("Build Date")
    @FindBy(css = "[data-ng-if='vm.asset.buildDate'] div:nth-child(2)")
    @Visible
    private WebElement buildDate;

    @Name("IMO Number")
    @FindBy(css = "[data-ng-if='vm.asset.ihsAsset.id'] div:nth-child(2)")
    @Visible
    private WebElement imoNumber;

    @Name("Gross Tonnage")
    @FindBy(css = "[data-ng-if='vm.asset.grossTonnage'] div:nth-child(2)")
    @Visible
    private WebElement grossTonnage;

    @Name("Flag")
    @FindBy(css = "[data-ng-if='vm.asset.flagState'] div:nth-child(2)")
    @Visible
    private WebElement flag;

    @Name("Class Status")
    @FindBy(css = "[data-ng-if='vm.asset.classStatus'] div:nth-child(2)")
    @Visible
    private WebElement classStatus;

    @Name("View Asset Button")
    @FindBy(css = "a[href*='asset-mode']")
    @Visible
    private WebElement viewAssetButton;

    @Name("Job Count Button")
    @FindBy(css = "[data-ui-sref*='asset.jobs.list']")
    @Visible
    private WebElement jobCountButton;

    @Name("Cases Count Button")
    @FindBy(css = "[data-ui-sref*='asset.cases.list']")
    @Visible
    private WebElement caseCountButton;

    @Name("Close Asset Title")
    @FindBy(css = "span .icon-small-minus.icon-small-open-close")
    private WebElement closeAssetTitle;

    @Name("Push Down Panel")
    @FindBy(css = ".assetDetailOverlayPanel.panel.ng-isolate-scope.panel-left.is-active")
    private WebElement pushDownPanel;

    @Name("My Work Asset Cases")
    private List<MyWorkAssetCase> myWorkAssetCases;

    @Name("My Work Asset Jobs")
    private List<MyWorkAssetJob> myWorkAssetJobs;

    public String getAssetName()
    {
        return assetName.getText();
    }

    public String getAssetType()
    {
        return assetType.getText();
    }

    public String getBuildDate()
    {
        return buildDate.getText();
    }

    public String getImoNumber()
    {
        return imoNumber.getText();
    }

    public String getGrossTonnage()
    {
        return grossTonnage.getText();
    }

    public String getFlag()
    {
        return flag.getText();
    }

    public String getClassStatus()
    {
        return classStatus.getText();
    }

    public ViewAssetPage clickViewAssetButton()
    {
        viewAssetButton.click();
        WebDriver driver = BaseTest.getDriver();
        new WebDriverWait(driver, 10)
                .pollingEvery(1000, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions.invisibilityOfElementLocated(By.id
                                (SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));
        return PageFactory.newInstance(ViewAssetPage.class);
    }

    public CasesPage clickCaseCountButton()
    {
        caseCountButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    public ViewAssetPage clickJobCountButton()
    {
        jobCountButton.click();
        WebDriver driver = BaseTest.getDriver();
        new WebDriverWait(driver, 10)
                .pollingEvery(1000, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions.invisibilityOfElementLocated(By.id
                                (SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));
        return PageFactory.newInstance(ViewAssetPage.class);
    }

    public CasesPage clickViewAllCasesButton()
    {
        caseCountButton.click();

        WebDriver driver = BaseTest.getDriver();
        new WebDriverWait(driver, 10)
                .pollingEvery(1000, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions.invisibilityOfElementLocated(By.id
                                (SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));
        return PageFactory.newInstance(CasesPage.class);
    }

    public ViewAssetPage clickViewAllJobsButton()
    {

        return PageFactory.newInstance(ViewAssetPage.class);
    }

    public boolean isViewAssetButtonDisplayed()
    {
        return viewAssetButton.isDisplayed();
    }

    public boolean isViewAllCasesOrJobsButtonDisplayed()
    {
        return viewAssetButton.isDisplayed();
    }

    public int getNumberOfCase()
    {
        String caseCountText = caseCountButton
                .findElement(By.cssSelector("span:not(.ng-hide)"))
                .getText();
        Pattern pattern = Pattern.compile(NUMBER_OF_CASES_REGEX);
        Matcher matcher = pattern.matcher(caseCountText);
        assertThat(matcher.find()).isTrue();
        return Integer.parseInt(matcher.group(1));
    }

    public int getNumberOfJob()
    {
        return Integer.valueOf(jobCountButton
                .findElement(By.cssSelector("span:not(.ng-hide) strong"))
                .getText());
    }

    public List<MyWorkAssetCase> getMyWorkAssetCases()
    {
        return myWorkAssetCases;
    }

    public MyWorkAssetCase getMyWorkAssetCaseByIndex(int index)
    {
        return myWorkAssetCases.get(index);
    }

    public boolean isPushDownPanelDisplayed()
    {
        return pushDownPanel.isDisplayed();
    }

    @Step("Click on close Asset Title Icon")
    public AllAssetsSubPage clickCloseAssetTitle()
    {
        closeAssetTitle.click();
        new WebDriverWait(BaseTest.getDriver(), 25).until(ExpectedConditions.
                invisibilityOfElementLocated(By
                        .cssSelector(".asset-card.expanded")));
        return PageFactory.newInstance(AllAssetsSubPage.class);
    }

    public List<MyWorkAssetJob> getMyWorkAssetJobs()
    {
        return myWorkAssetJobs;
    }

    public MyWorkAssetJob getMyWorkAssetJobByIndex(int index)
    {
        return myWorkAssetJobs.get(index);
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
