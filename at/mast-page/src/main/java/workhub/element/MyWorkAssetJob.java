package workhub.element;

import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.HoverOver;
import viewasset.ViewJobPage;

@Name("My Work Asset Jobs")
@FindBy(css = "[data-ng-repeat*='job in vm.selectedItems[0]']")
public class MyWorkAssetJob extends HtmlElement
{
    @Name("Asset Card")
    @FindBy(tagName = "asset-card")
    private MyWorkAssetElement assetCard;

    @Name("Job Number")
    @FindBy(css = "[data-ng-bind*='job.imoNumber']")
    private WebElement jobNumber;

    @Name("Job Status")
    @FindBy(css = "[data-ng-bind='job.jobStatus.name']")
    private WebElement jobStatus;

    @Name("Job Office")
    @FindBy(css = "[data-ng-bind='job.location']")
    private WebElement jobOffice;

    @Name("Job Office Role")
    @FindBy(className = "job-type-sdo")
    private WebElement jobOfficeRole;

    @Name("Job Service Type")
    @FindBy(className = "job.serviceType")
    private WebElement jobServiceType;

    @Name("Job ETA")
    @FindBy(css = "[data-ng-bind*='job.etaDate']")
    private WebElement jobETA;

    @Name("Job ETD")
    @FindBy(css = "[data-ng-bind*='job.etdDate']")
    private WebElement jobETD;

    @Name("Lead Surveyor")
    @FindBy(css = "[data-ng-bind='vm.jobSurveyor(job)']")
    private WebElement leadSurveyor;

    @Name("Navigate to job specific page button")
    @FindBy(css = "[data-ng-click='vm.jobView(job, job.asset)']")
    private WebElement navigateToJobArrowButton;

    @Name("Information hover over")
    @FindBy(css = "[id*='my-work-show-asset-']")
    private HoverOver assetInformationIcon;

    @Step("Returning Job Number value")
    public String getJobNumnber()
    {
        return jobNumber.getText();
    }

    @Step("Returning Job Status value")
    public String getJobStatus()
    {
        return jobStatus.getText();
    }

    @Step("Returning Job Office value")
    public String getJobOffice()
    {
        return jobOffice.getText();
    }

    @Step("Return job office role")
    public String getJobOfficeRole()
    {
        return jobOfficeRole.getText();
    }

    @Step("Returning Job Service Type value")
    public String getJobServiceType()
    {
        return jobServiceType.getText();
    }

    @Step("Returning Job ETA value")
    public String getJobETA()
    {
        return jobETA.getText();
    }

    @Step("Returning Job ETD value")
    public String getJobETD()
    {
        return jobETD.getText();
    }

    @Step("Return the name of the lead surveyor")
    public String getJobLeadSurveyor()
    {
        return leadSurveyor.getText();
    }

    @Step("Click on navigate to job page button")
    public ViewJobPage clickNavigateToJobArrowButton()
    {
        navigateToJobArrowButton.click();
        return PageFactory.newInstance(ViewJobPage.class);
    }

    @Step("Check that Navigate to job arrow button is displayed")
    public boolean isNavigateToJobArrowButtonDisplayed()
    {
        return navigateToJobArrowButton.isDisplayed();
    }

    @Step("Mouseover asset information of job with index")
    public MyWorkAssetElement mouseoverAssetInformation()
    {
        return assetInformationIcon.openHoverOverElement(assetCard);
    }

    @Step("Close mouseover information")
    public void closeMouseoverPane(){assetInformationIcon.closeHoverOverElement();}

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
