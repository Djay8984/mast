package workhub.element;

import com.frameworkium.core.ui.tests.BaseTest;
import helper.PageFactory;
import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import viewasset.ViewAssetPage;
import viewasset.sub.assetmodel.buildassetmodel.PreviewAssetModelPage;
import viewasset.sub.assetmodel.buildassetmodel.modal.CopyAcrossAttributeValuesWindow;
import viewasset.sub.cases.CasesPage;

import static helper.PageFactory.newInstance;

@Name("Asset cards")
@FindBy(css = "asset-card[data-asset='asset']")
public class AllAssetsAssetElement extends HtmlElement
{

    private final static String IMO_NUMBER_SELECTOR = "[data-ng-bind='vm.asset.ihsAsset.id']";
    private final static String PREVIEW_BUTTON_SELECTOR = "[id*='asset-card-preview-button']";
    private final static String COPY_BUTTON_SELECTOR = "[id*='asset-card-copy-button']";
    private final static String ASSET_ID_SELECTOR = "[data-ng-bind='vm.asset.id']";
    private final String GROSS_TONNAGE_SELECTOR = ".asset-card.corner-cut [data-ng-if='vm.asset.grossTonnage'] div:nth-child(2)";
    private final String CLASS_STATUS_SELECTOR = ".asset-card.corner-cut [data-ng-if='vm.asset.classStatus'] div:nth-child(2)";

    @Name("Asset Name")
    @FindBy(css = "[data-ng-if='vm.asset.name']")
    private WebElement assetName;

    @Name("Asset Type")
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.assetType'] div:nth-child(2)")
    private WebElement assetType;

    @Name("Build Date")
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.buildDate'] div:nth-child(2)")
    private WebElement buildDate;

    @Name("IMO Number")
    @FindBy(css = IMO_NUMBER_SELECTOR)
    private WebElement imoNumber;

    @Name("Gross Tonnage")
    @FindBy(css = GROSS_TONNAGE_SELECTOR)
    private WebElement grossTonnage;

    @Name("Class Status")
    @FindBy(css = CLASS_STATUS_SELECTOR)
    private WebElement classStatus;

    @Name("Flag")
    @FindBy(css = "[data-ng-if='vm.asset.flagState'] div:nth-child(2)")
    private WebElement flag;

    @Name("View Asset Or Preview Button")
    @FindBy(css = "asset-card-cta > div:nth-child(1)")
    private WebElement viewAssetOrPreviewButton;

    @Name("Case or Job Count Button Or Copy")
    @FindBy(css = "asset-card-cta > div:nth-child(2)")
    private WebElement jobButton;

    @Name("Case or Job Count Button Or Copy")
    @FindBy(css = "asset-card-cta > div:nth-child(3)")
    private WebElement caseButton;

    @Name("Open Asset Title")
    @FindBy(css = ".icon-small-plus.icon-small-open-close.ng-scope")
    private WebElement openAssetTitle;

    @Name("Preview Asset Button")
    @FindBy(css = PREVIEW_BUTTON_SELECTOR)
    private WebElement previewButton;

    @Name("Copy Asset Button")
    @FindBy(css = COPY_BUTTON_SELECTOR)
    private WebElement copyButton;

    @Name("Asset Select element")
    @FindBy(css = "asset-card[data-asset='asset'] div>div:nth-child(1)")
    private WebElement assetSelectElement;

    @Name("Asset ID")
    @FindBy(css = ASSET_ID_SELECTOR)
    private WebElement assetId;

    public String getAssetName()
    {
        return WordUtils.capitalize(assetName.getText());
    }

    public String getAssetType()
    {
        return assetType.getText();
    }

    public String getBuildDate()
    {
        return buildDate.getText();
    }

    public String getImoNumber()
    {
        return imoNumber.getText();
    }

    public String getAssetId()
    {
        return assetId.getText().trim();
    }

    public String getGrossTonnage()
    {
        if (this.findElements(By.cssSelector(GROSS_TONNAGE_SELECTOR)).size() > 0)
        {
            return grossTonnage.getText().trim();
        }
        else
        {
            return "0";
        }
    }

    public String getClassStatus()
    {
        if (this.findElements(By.cssSelector(CLASS_STATUS_SELECTOR)).size() > 0)
        {
            return classStatus.getText().trim();
        }
        else
        {
            return null;
        }
    }

    @Step("returning the flag text")
    public String getFlag()
    {
        return flag.getText().trim();
    }

    @Step("returning the case count text")
    public String getCaseCountText()
    {
        return caseButton.getText().trim();
    }

    @Step("clicking on view asset button")
    public ViewAssetPage clickViewAssetButton()
    {
        viewAssetOrPreviewButton.click();
        return newInstance(ViewAssetPage.class);
    }

    @Step("clicking on case or job count button")
    public ViewAssetPage clickJobButton()
    {
        jobButton.click();
        return newInstance(ViewAssetPage.class);
    }

    public CasesPage clickCasesButton()
    {
        caseButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Clicking preview asset button")
    public PreviewAssetModelPage clickPreviewButton()
    {
        previewButton.click();
        return newInstance(PreviewAssetModelPage.class);
    }

    @Step("Clicking copy asset button")
    public CopyAcrossAttributeValuesWindow clickCopyButton()
    {
        copyButton.click();
        return PageFactory.newInstance(CopyAcrossAttributeValuesWindow.class);
    }

    @Step("Is Asset Open Title Button Clickable")
    public boolean isOpenAssetIconClickable()
    {
        return openAssetTitle.getAttribute("class").contains("icon-disabled");

    }

    @Step("Click on Open Asset Title Icon")
    public MyWorkAssetElement clickOpenAssetTitle()
    {
        openAssetTitle.click();
        new WebDriverWait(BaseTest.getDriver(), 25).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".asset-card.expanded")));
        return org.openqa.selenium.support.PageFactory.initElements(BaseTest
                .getDriver(), MyWorkAssetElement.class);
    }

    public boolean isViewAssetButtonDisplayed()
    {
        return viewAssetOrPreviewButton.isDisplayed();
    }

    @Step("Is Preview Button Displayed")
    public boolean isPreviewButtonDisplayed()
    {
        return findElements(By.cssSelector(PREVIEW_BUTTON_SELECTOR)).size() > 0;
    }

    @Step("Is Copy Button Displayed")
    public boolean isCopyButtonDisplayed()
    {
        return findElements(By.cssSelector(COPY_BUTTON_SELECTOR)).size() > 0;
    }

    @Step("Is IMO Displayed")
    public boolean isIMODisplayed()
    {
        return findElements(By.cssSelector(IMO_NUMBER_SELECTOR)).size() > 0;
    }

    @Step("Is Asset ID Displayed")
    public boolean isAssetIdDisplayed()
    {
        return findElements(By.cssSelector(ASSET_ID_SELECTOR)).size() > 0;
    }

    @Step("Is Assets Selected ")
    public boolean isAssetSelected()
    {
        return assetSelectElement.getAttribute("class").contains("selected");
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }
}
