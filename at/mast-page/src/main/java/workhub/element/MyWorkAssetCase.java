package workhub.element;

import helper.PageFactory;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.HoverOver;
import viewasset.sub.cases.CasesPage;

@Name("My Work Asset Cases")
@FindBy(css = "[data-ng-repeat*='case in vm.selectedItems']")
public class MyWorkAssetCase extends HtmlElement
{
    @Name("Asset Card")
    @FindBy(tagName = "asset-card")
    private MyWorkAssetElement assetCard;

    @Name("Case ID")
    @FindBy(css = "[data-ng-bind*='case.id']")
    private WebElement caseId;

    @Name("Case Status")
    @FindBy(css = "[data-ng-bind='case.caseStatus.name']")
    private WebElement caseStatus;

    @Name("Case Type")
    @FindBy(css = "[data-ng-bind='case.caseType.name']")
    private WebElement caseType;

    @Name("CFO")
    @FindBy(css = "[data-ng-bind*='cfo']")
    private WebElement cfo;

    @Name("Case Surveyor")
    @FindBy(css = "[data-ng-bind*='vm.caseSurveyor']")
    private WebElement caseSurveyor;

    @Name("SDO")
    @FindBy(css = "[data-ng-bind*='caseSdo']")
    private WebElement sdo;

    @Name("Navigate to case specific case page button")
    @FindBy(css = ".icon-small-arrow-right")
    private WebElement navigateToCaseArrowButton;

    @Name("Asset information hover over")
    @FindBy(css = "[data-ng-mouseover*='vm.showAssetCard']")
    private HoverOver assetInformationIcon;

    public String getCaseStatus()
    {
        return caseStatus.getText();
    }

    public String getCaseType()
    {
        return caseType.getText();
    }

    public String getCfo()
    {
        return cfo.getText();
    }

    public String getCaseSurveyor()
    {
        return caseSurveyor.getText();
    }

    public String getSdo()
    {
        return sdo.getText();
    }

    @Step("Click on navigate to case page button")
    public CasesPage clickNavigateToCaseArrowButton()
    {
        navigateToCaseArrowButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Check that Navigate to case arrow button is displayed")
    public boolean isNavigateToCaseArrowButtonDisplayed()
    {
        return navigateToCaseArrowButton.isDisplayed();
    }

    @Override
    public Rectangle getRect()
    {
        return null;
    }

    @Step("Return the case ID")
    public Long getID(){
        return Long.valueOf(caseId.getText());
    }

    @Step("Hover Over Asset Information Icon")
    public MyWorkAssetElement hoverOverInformationIcon(){
        return assetInformationIcon.openHoverOverElement(assetCard);
    }
}
