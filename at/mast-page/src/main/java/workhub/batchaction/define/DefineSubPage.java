package workhub.batchaction.define;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.template.ItemTemplatePage;

public class DefineSubPage extends BasePage<DefineSubPage>
{
    @Visible
    @Name("Asset note radio button")
    @FindBy(css = "[data-ng-include*='app/batch-actions/partials/define.html'] div.grid-block div:nth-child(2) label")
    private WebElement assetNoteRadioButton;

    @Visible
    @Name("Actionable item radio button")
    @FindBy(css = "[data-ng-include*='app/batch-actions/partials/define.html'] div.grid-block " +
            "div:nth-child(3) label")
    private WebElement actionableItemRadioButton;

    @Name("Start from a template button")
    @FindBy(css = "[data-ng-click='vm.selectTemplate()']")
    private WebElement startFromATemplateButton;

    @Name("Title text input")
    @FindBy(id = "title")
    private WebElement titleTextInput;

    @Name("Statutory button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(1)")
    private WebElement statutoryButton;

    @Name("Class button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(2)")
    private WebElement classButton;

    @Name("External button")
    @FindBy(css = "[id='tpl-category.id'] ul li:nth-child(3)")
    private WebElement externalButton;

    @Name("Description text input")
    @FindBy(id = "description")
    private WebElement descriptionTextInput;

    @Name("Surveyor guidance text input")
    @FindBy(id = "surveyor-guidance")
    private WebElement surveyorTextInput;

    @Name("Imposed date text input")
    @FindBy(id = "imposedDate")
    private WebElement imposedDateTextInput;

    @Name("Due date text input")
    @FindBy(id = "dueDate")
    private WebElement dueDateTextInput;

    @Name("LR Internal button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(1)")
    private WebElement lrInternalButton;

    @Name("LR and Customer button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(2)")
    private WebElement lrAndCustomerButton;

    @Name("All button")
    @FindBy(css = "[id='tpl-confidentialityType.id'] ul li:nth-child(3)")
    private WebElement allButton;

    @Name("Attachment icon")
    @FindBy(css = "attachment-icon")
    private WebElement attachmentIcon;

    @Name("Requires approval for change checkbox")
    @FindBy(css = "#tpl-requireApproval button")
    private WebElement requiresApprovalCheckbox;

    @Visible
    @Name("Cancel button")
    @FindBy(id = "add-case-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Next button")
    @FindBy(id = "batch-action-next")
    private WebElement nextButton;

    @Step("Click on Actionable item radio button")
    public DefineSubPage clickActionableItemRadioButton()
    {
        actionableItemRadioButton.click();
        return this;
    }

    @Step("Click on Asset Note radio button")
    public DefineSubPage clickAssetNoteRadioButton()
    {
        assetNoteRadioButton.click();
        return this;
    }

    @Step("Click on Start from a template button")
    public ItemTemplatePage clickStartFromATemplateButton()
    {
        startFromATemplateButton.click();
        return PageFactory.newInstance(ItemTemplatePage.class);
    }

    @Step("Get Title text")
    public String getTitleTextInput()
    {
        return titleTextInput.getAttribute("value");
    }

    @Step("Set Title text with '{0}'")
    public DefineSubPage setTitleTextInput(String input)
    {
        titleTextInput.sendKeys(input);
        return this;
    }

    @Step("Click on Statutory button")
    public DefineSubPage clickStatutoryButton()
    {
        statutoryButton.click();
        return this;
    }

    @Step("Check that Statutory button is selected")
    public boolean isStatutoryButtonSelected()
    {
        return statutoryButton.findElement(By.cssSelector("input")).getAttribute("checked")
                .contains("true");
    }

    @Step("Click on Class button")
    public DefineSubPage clickClassButton()
    {
        classButton.click();
        return this;
    }

    @Step("Click on External button")
    public DefineSubPage clickExternalButton()
    {
        externalButton.click();
        return this;
    }

    @Step("Get Description text input")
    public String getDescriptionTextInput()
    {
        return descriptionTextInput.getAttribute("value");
    }

    @Step("Set Description text input with '{0}'")
    public DefineSubPage setDescriptionTextInput(String input)
    {
        descriptionTextInput.sendKeys(input);
        return this;
    }

    @Step("Set Surveyor text input with '{0}'")
    public DefineSubPage setSurveyorTextInput(String input)
    {
        surveyorTextInput.sendKeys(input);
        return this;
    }

    @Step("Get Imposed date")
    public String getImposedDate()
    {
        return imposedDateTextInput.getAttribute("value");
    }

    @Step("Set Imposed date with '{0}'")
    public DefineSubPage setImposedDate(String date)
    {
        imposedDateTextInput.clear();
        imposedDateTextInput.sendKeys(date);
        imposedDateTextInput.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Get Due date")
    public String getDueDate()
    {
        return dueDateTextInput.getAttribute("value");
    }

    @Step("Set Due date with '{0}'")
    public DefineSubPage setDueDate(String date)
    {
        dueDateTextInput.clear();
        dueDateTextInput.sendKeys(date);
        dueDateTextInput.sendKeys(Keys.TAB);
        return this;
    }

    @Step("Click on LR Internal button")
    public DefineSubPage clickLrInternalButton()
    {
        lrInternalButton.click();
        return this;
    }

    @Step("Check that LR Internal button is selected")
    public boolean isLrInternalButtonSelected()
    {
        return lrInternalButton.findElement(By.cssSelector("input")).getAttribute("checked")
                .contains("true");
    }

    @Step("Click on LR and Customer button")
    public DefineSubPage clickLrAndCustomerButton()
    {
        lrAndCustomerButton.click();
        return this;
    }

    @Step("Click on All button")
    public DefineSubPage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Click Next button")
    public ApplySubPage clickNextButton()
    {
        wait.until(ExpectedConditions.elementToBeClickable(nextButton));
        nextButton.click();
        return PageFactory.newInstance(ApplySubPage.class);
    }
}
