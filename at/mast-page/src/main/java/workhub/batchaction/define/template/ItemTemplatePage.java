package workhub.batchaction.define.template;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import typifiedelement.RadioButton;
import workhub.batchaction.define.DefineSubPage;

import java.util.List;
import java.util.stream.Collectors;

public class ItemTemplatePage extends BasePage<ItemTemplatePage>
{

    @Visible
    @Name("Close button")
    @FindBy(id = "close-button")
    private WebElement closeButton;

    @Visible
    @Name("Title text input")
    @FindBy(id = "title-search")
    private WebElement titleSearchTextInput;

    @Visible
    @Name("Item type text input")
    @FindBy(id = "item-type-search")
    private WebElement itemTypeTextInput;

    @Name("Item type drop down")
    @FindBy(css = "#item-type-search + ul[data-ng-style]")
    private WebElement itemTypeDropDown;

    @Visible
    @Name("Live button")
    @FindBy(css = "[class='button-group segmented light'] li:nth-child(1)")
    private WebElement liveButton;

    @Visible
    @Name("All button")
    @FindBy(css = "[class='button-group segmented light'] li:nth-child(2)")
    private WebElement allButton;

    @Visible
    @Name("Find template button")
    @FindBy(css = "[data-ng-click='vm.findTemplate()']")
    private WebElement findTemplateButton;

    @Name("Select template button")
    @FindBy(css = "[data-ng-click='vm.select()']")
    private WebElement selectTemplateButton;

    @Name("Message")
    @FindBy(css = "[data-ng-show='data.list.length === 0']")
    private WebElement searchMessage;

    @Name("Template search result table")
    private TemplateSearchResultTable templateSearchResultTable;

    @Step("Click on close button")
    public DefineSubPage clickCloseButton()
    {
        closeButton.click();
        return PageFactory.newInstance(DefineSubPage.class);
    }

    @Step("Get Title text")
    public String getTitleText()
    {
        return titleSearchTextInput.getAttribute("value");
    }

    @Step("Set Title text '{0}'")
    public ItemTemplatePage setTitleText(String input)
    {
        titleSearchTextInput.clear();
        titleSearchTextInput.sendKeys(input);
        return this;
    }

    @Step("Get Item type text")
    public String getItemTypeText()
    {
        return itemTypeTextInput.getAttribute("value");
    }

    @Step("Set Item text '{0}'")
    public ItemTemplatePage setItemTypeText(String input)
    {
        itemTypeTextInput.clear();
        itemTypeTextInput.sendKeys(input);
        return this;
    }

    @Step("Get item type dropdown options")
    public List<String> getItemTypeDropDownOptions()
    {
        return itemTypeDropDown.findElements(By.cssSelector("li")).stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Step("Select from item type dropdown")
    public ItemTemplatePage selectItemTypeDropdownByIndex(int index)
    {
        itemTypeDropDown.findElements(By.cssSelector("li")).get(index)
                .click();
        return this;
    }

    @Step("Select from item type dropdown")
    public String getItemTypeDropDownTextByIndex(int index)
    {
        return itemTypeDropDown.findElements(By.cssSelector("li")).get(index).getText();
    }

    @Step("Click on Live button")
    public ItemTemplatePage clickLiveButton()
    {
        liveButton.click();
        return this;
    }

    @Step("Check that Live button is selected")
    public boolean isLiveButtonSelected()
    {
        return liveButton.findElement(By.cssSelector("input")).getCssValue("color").contains
                ("rgba(35, 81, 126, 1)");
    }

    @Step("Click on All button")
    public ItemTemplatePage clickAllButton()
    {
        allButton.click();
        return this;
    }

    @Step("Check that All button is selected")
    public boolean isAllButtonSelected()
    {
        return allButton.findElement(By.cssSelector("input")).getCssValue("color").contains
                ("rgba(35, 81, 126, 1)");
    }

    @Step("Click on Find template button")
    public TemplateSearchResultTable clickFindTemplateButton()
    {
        findTemplateButton.click();
        waitForJavascriptFrameworkToFinish();
        return templateSearchResultTable;
    }

    @Step("Click on Select template button")
    public DefineSubPage clickSelectTemplateButton()
    {
        selectTemplateButton.click();
        return PageFactory.newInstance(DefineSubPage.class);
    }

    @Step("Get search message")
    public String getSearchMessage()
    {
        return searchMessage.getText();
    }

    @FindBy(css = "table.template-search")
    public class TemplateSearchResultTable extends HtmlElement
    {

        @Name("Data row")
        private List<DataRow> dataRows;

        @Step("Get data rows")
        public List<DataRow> getDataRows()
        {
            return dataRows;
        }
    }

    @FindBy(css = ".template-search-row")
    public class DataRow extends HtmlElement
    {

        @Visible
        @Name("Name text")
        @FindBy(css = ".name")
        private WebElement nameText;

        @Visible
        @Name("Name radio button")
        @FindBy(css = "[data-ng-value='template']")
        private RadioButton nameRadioButton;

        @Visible
        @Name("Description text")
        @FindBy(css = ".description text-left")
        private WebElement descriptionText;

        @Visible
        @Name("Status text")
        @FindBy(css = "[class='status text-center']")
        private WebElement statusText;

        @Visible
        @Name("Applied to assets text")
        @FindBy(css = "[class='assets text-center']")
        private WebElement appliedToAssetsText;

        @Step("Click name radio button")
        public DataRow clickNameRadioButton()
        {
            nameRadioButton.select();
            return this;
        }

        @Step("Get name")
        public String getName()
        {
            return nameText.getText();
        }

        @Step("Get description text")
        public String getDescriptionText()
        {
            return descriptionText.getText();
        }

        @Step("Get status")
        public String getStatus()
        {
            return statusText.getText();
        }

        @Step("Get Applied to assets ")
        public String getAppliedToAssets()
        {
            return appliedToAssetsText.getText();
        }

        @Step("Get Item Template page")
        public ItemTemplatePage getItemTemplatePage()
        {
            return PageFactory.newInstance(ItemTemplatePage.class);
        }
    }
}
