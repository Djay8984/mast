package workhub.batchaction.element.base;

import com.frameworkium.core.ui.annotations.Visible;
import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

public class BaseElement extends HtmlElement
{
    @Name("Asset Name")
    @Visible
    @FindBy(css = "[data-ng-if='vm.asset.name']")
    private WebElement assetName;

    @Name("Asset Type")
    @Visible
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.assetType'] div:nth-child(2)")
    private WebElement assetType;

    @Name("Build Date")
    @Visible
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.buildDate'] div:nth-child(2)")
    private WebElement buildDate;

    @Name("IMO Number")
    @Visible
    @FindBy(css = "[data-ng-bind='vm.asset.ihsAsset.id']")
    private WebElement imoNumber;

    @Name("Gross Tonnage")
    @Visible
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.grossTonnage'] div:nth-child(2)")
    private WebElement grossTonnage;

    @Name("Class Status")
    @Visible
    @FindBy(css = ".asset-card.corner-cut [data-ng-if='vm.asset.classStatus'] div:nth-child(2)")
    private WebElement classStatus;

    @Name("Flag Text")
    @Visible
    @FindBy(css = "[data-ng-if='vm.asset.flagState'] div:nth-child(2)")
    private WebElement flag;

    @Name("Added To Selected button")
    @FindBy(css = ".secondary-button.icon-button")
    private WebElement addedToSelectedButton;

    @Name("Remove from selection button")
    @FindBy(css = "[data-ng-click='vm.deselect(asset)']")
    private WebElement removeFromSelectionButton;

    @Step("Get Asset Name")
    public String getAssetName()
    {
        return WordUtils.capitalize(assetName.getText());
    }

    @Step("Get Asset IMO Number")
    public String getAssetImoNumber()
    {
        return imoNumber.getText().trim();
    }

    @Step("Is Added to selection button displayed")
    public boolean isAddedToSelectionButtonDisplayed()
    {
        return addedToSelectedButton.isDisplayed();
    }
}
