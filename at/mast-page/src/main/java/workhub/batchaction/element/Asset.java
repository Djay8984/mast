package workhub.batchaction.element;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import workhub.batchaction.element.base.BaseElement;

//This extends BaseElement, make your changes at BaseElement
public class Asset extends BaseElement
{
    @Name("Add asset button")
    @Visible
    @FindBy(css = "asset-card-cta")
    private WebElement addButton;

    @Step("Click Add Asset Button ")
    public Asset clickAddAssetButton()
    {
        AppHelper.scrollToBottom();
        addButton.click();
        return this;
    }

    @Step("Get Add Asset button text")
    public String getAddButtonText()
    {
        return addButton.getText();
    }
}
