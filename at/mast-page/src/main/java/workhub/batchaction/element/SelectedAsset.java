package workhub.batchaction.element;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.batchaction.element.base.BaseElement;

//This extends BaseElement, make your changes at BaseElement
public class SelectedAsset extends BaseElement
{
    @Name("Remove from selection button")
    @Visible
    @FindBy(css = "asset-card-cta")
    private WebElement removeFromSelectionButton;

    @Step("Click remove from selection button")
    public SelectedAsset clickRemoveFromSelectionButton()
    {
        removeFromSelectionButton.click();
        return this;
    }
}
