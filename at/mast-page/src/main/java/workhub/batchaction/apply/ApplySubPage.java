package workhub.batchaction.apply;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import workhub.batchaction.element.Asset;
import workhub.batchaction.element.SelectedAsset;
import workhub.batchaction.modal.SuccessfulPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

public class ApplySubPage extends BasePage<ApplySubPage>
{

    @Visible
    @Name("Search Reset Filter button")
    @FindBy(id = "dashboard-asset-search-filter-toggle")
    private WebElement searchResetFilterButton;

    @Name("Add All Button")
    @FindBy(css = "[data-ng-if^='vm.selected.total']")
    private WebElement addAllButton;

    @Name("View selected asset button")
    @FindBy(id = "selected-assets-button")
    private WebElement viewSelectedAssetButton;

    @Name("Remove All Button")
    @FindBy(css = "[data-ng-click='vm.deselectAll()'] span")
    private WebElement removeAllButton;

    @Name("Complete button")
    @FindBy(id = "finish-batch-action")
    private WebElement completeButton;

    @Name("Assets section")
    @FindBy(css = "[class='tab-content']")
    private AssetSection assetSection;

    @Name("Selected section")
    @FindBy(id = "selected-content")
    private SelectedAssetSection selectedAssetSection;

    @Step("Get all assets section")
    public AssetSection getAssetSection()
    {
        return assetSection;
    }

    @Step("Get selected assets section")
    public SelectedAssetSection getSelectedSection()
    {
        return selectedAssetSection;
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Clicking the search/filter assets button")
    public SearchFilterSubPage clickSearchFilterAssetsButton()
    {
        searchResetFilterButton.click();
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("asset-search-query"))
        );
        return PageFactory.newInstance(SearchFilterSubPage.class);
    }

    @Step("Get all asset count displayed on Add button")
    public int getAllAssetCountFromAddAllButton()
    {
        return Integer.parseInt(addAllButton.getText().replaceAll("\\D+", "").trim());
    }

    @Step("Click remove all button")
    public ApplySubPage clickRemoveAllButton()
    {
        removeAllButton.click();
        return this;
    }

    @Step("Click View selected asset button")
    public ApplySubPage clickViewSelectedAssetButton()
    {
        viewSelectedAssetButton.click();
        return this;
    }

    @Step("Click Complete button")
    public SuccessfulPage clickCompleteButton()
    {
        completeButton.click();
        return PageFactory.newInstance(SuccessfulPage.class);
    }

    public class AssetSection extends HtmlElement
    {
        @Name("Asset cards")
        @FindBy(css = "asset-card[data-asset='asset']")
        private List<Asset> cards;

        @Step("Get all asset ")
        public List<Asset> getCards()
        {
            return cards;
        }
    }

    public class SelectedAssetSection extends HtmlElement
    {
        @Name("Asset cards")
        @FindBy(css = "asset-card[data-asset='asset']")
        private List<SelectedAsset> cards;

        @Step("Get all asset ")
        public List<SelectedAsset> getCards()
        {
            return cards;
        }
    }

}
