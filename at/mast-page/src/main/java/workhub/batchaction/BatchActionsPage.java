package workhub.batchaction;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.DefineSubPage;

public class BatchActionsPage extends BasePage<BatchActionsPage>
{
    @Visible
    @Name("Define tab")
    @FindBy(css = "#batch-action-define, [data-ng-click='vm.defineTab()']")
    private WebElement defineTab;

    @Visible
    @Name("Apply tab")
    //freaking element switches between these two css...
    @FindBy(css = "#batch-actions-apply, [data-ng-click='vm.next()']")
    private WebElement applyTab;

    /**
     * @param pageObjectClass
     * @param <T>
     * @return modal.ConfirmationPage or DefinePage
     */
    @Step("Click on Define tab")
    public <T extends BasePage<T>> T clickDefineTab(Class<T> pageObjectClass)
    {
        defineTab.click();
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Click on Define tab")
    public ApplySubPage clickApplyTab()
    {
        applyTab.click();
        return PageFactory.newInstance(ApplySubPage.class);
    }

    @Step("Get Define commonpage page")
    public DefineSubPage getDefineSubPage()
    {
        return PageFactory.newInstance(DefineSubPage.class);
    }
}
