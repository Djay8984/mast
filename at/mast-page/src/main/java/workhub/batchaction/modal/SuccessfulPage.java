package workhub.batchaction.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.pages.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.WorkHubPage;

import java.util.List;
import java.util.stream.Collectors;

public class SuccessfulPage extends BasePage<SuccessfulPage>
{

    @Visible
    @Name("OK button")
    @FindBy(css = "[data-ng-click='$close()']")
    private WebElement okButton;

    @Visible
    @Name("Selected assets")
    @FindBy(css = "[data-ng-repeat='(i, item) in ::vm.savedItems']")
    private List<WebElement> selectedAssets;

    @Step("Click OK Button")
    public WorkHubPage clickOKButton()
    {
        okButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(WorkHubPage.class);
    }

    @Step("Get list of selected assets")
    public List<String> getSelectedAssets()
    {
        return selectedAssets.stream().map(asset -> asset.getText()).collect(Collectors.toList());
    }
}
