package workhub.batchaction.modal;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.batchaction.apply.ApplySubPage;
import workhub.batchaction.define.DefineSubPage;

public class ConfirmationPage extends BasePage<ConfirmationPage>
{

    @Name("")
    @FindBy(css = "modal-body")
    private WebElement text;

    @Name("Cancel Button")
    @FindBy(css = "[data-ng-click='$close(false)']")
    private WebElement cancelButton;

    @Name("Continue to Step 1 Button")
    @FindBy(css = "[data-ng-click='$close(true)']")
    private WebElement continueButton;

    @Step("Click on Cancel button")
    public ApplySubPage clickCancelButton()
    {
        cancelButton.click();
        return PageFactory.newInstance(ApplySubPage.class);
    }

    @Step("Click on Continue button")
    public DefineSubPage clickContinueButton()
    {
        continueButton.click();
        return PageFactory.newInstance(DefineSubPage.class);
    }

    @Step("Get text from modal body")
    public String getText()
    {
        return text.getText();
    }
}
