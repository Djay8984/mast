package workhub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;
import viewasset.ViewAssetPage;
import viewasset.sub.cases.CasesPage;
import workhub.sub.sub.IMORegisteredAssetPage;
import workhub.sub.sub.NonIMORegisteredAssetPage;
import workhub.sub.sub.util.ImoAsset;

import static constant.LloydsRegister.AssetData.*;
import static constant.LloydsRegister.RandomiseType.LETTERS;
import static constant.LloydsRegister.randomiseConstant;

public class AddNewAssetPage extends BasePage<AddNewAssetPage>
{

    @Visible
    @Name("IMO Registered Asset Radio Button")
    @FindBy(css = "div[class='grid-block shrink']:nth-child(2)")
    private RadioButton imoRegisteredAssetRadioButton;

    @Visible
    @Name("Non IMO Registered Asset Radio Button")
    @FindBy(css = "div[class='grid-block shrink']:nth-child(3)")
    private RadioButton nonImoRegisteredAssetRadioButton;

    @Visible
    @Name("Create Asset Button")
    @FindBy(id = "create-asset-create")
    private WebElement createAssetButton;

    @Visible
    @Name("Cancel Button")
    @FindBy(id = "create-asset-cancel")
    private WebElement cancelButton;

    @Visible
    @Name("Navigate Back Button")
    @FindBy(css = ".back-button")
    private WebElement navigateBackButton;

    @Step("Click IMO Registered Asset Radio Button")
    public IMORegisteredAssetPage clickImoRegisteredAssetRadioButton()
    {
        imoRegisteredAssetRadioButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-imo")));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(IMORegisteredAssetPage.class);
    }

    @Step("Click Non IMO Registered Asset Radio Button")
    public NonIMORegisteredAssetPage clickNonImoRegisteredAssetRadioButton()
    {
        nonImoRegisteredAssetRadioButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("asset-builder")));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(NonIMORegisteredAssetPage.class);
    }

    @Step("Check IMO Registered Asset Button is selected")
    public boolean isIMORegisteredAssetButtonIsSelected()
    {
        return imoRegisteredAssetRadioButton.isSelected();
    }

    @Step("Click Navigate Back Button")
    public WorkHubPage clickNavigateBackButton()
    {
        navigateBackButton.click();
        return PageFactory.newInstance(WorkHubPage.class);
    }

    @Step("Check Non IMO Registered Asset Button is selected")
    public boolean isNonIMORegisteredAssetButtonSelected() {
        return nonImoRegisteredAssetRadioButton.findElement(By.cssSelector("input")).getAttribute("class").contains("ng-valid-parse");
    }

    @Step("Is Create Asset Button Clickable")
    public boolean isCreateAssetButtonClickable()
    {
        return createAssetButton.isEnabled();
    }

    @Step("Click Create Asset Button")
    public CasesPage clickCreateAssetButton()
    {
        createAssetButton.click();
        return PageFactory.newInstance(CasesPage.class);
    }

    @Step("Is Create Asset Button Enabled ")
    public boolean isCreateAssetButtonEnabled()
    {
        return createAssetButton.isEnabled();
    }

    /**
     * Create Non IMO Registered Asset
     * Creation of Non IMO registered assert will return ViewAssetPage
     *
     * @return ViewAssetPage
     */
    @Step("Create Non IMO Registered Asset ")
    public ViewAssetPage createNonImoRegisteredAsset(String builder, String yardNumber, String assetName)
    {
        return clickNonImoRegisteredAssetRadioButton()
                .setBuilderName(builder)
                .setYardNumber(yardNumber)
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .setClassSection(classSection)
                .clickSelectAssetTypeSelectButton()
                .setDateOfBuild(dateOfBuild)
                .clickCreateAsset();
    }

    public ViewAssetPage createNonImoRegisteredAsset()
    {
        return createNonImoRegisteredAsset
                (randomiseConstant(builder, LETTERS), randomiseConstant(yardNumber), randomiseConstant(assetName));
    }

    public ViewAssetPage createNonImoRegisteredAsset(String assetName)
    {
        return createNonImoRegisteredAsset
                (randomiseConstant(builder, LETTERS), randomiseConstant(yardNumber), assetName);
    }

    /**
     * Create IMO Registered Asset
     * Creation of IMO registered assert will return ViewAssetPage
     *
     * @return ViewAssetPage
     */
    @Step("Create IMO Registered Asset ")
    public ViewAssetPage createImoRegisteredAsset(String assetName)
    {
        return clickImoRegisteredAssetRadioButton()
                .setIMONumber(new ImoAsset().getUniqueImoNumber())
                .setAssetName(assetName)
                .clickAssetCategoryButton()
                .selectAssetCategoryByName(assetCategory)
                .clickAssetTypeButton()
                .setStatcodeOneDropdown(statCode1)
                .setStatcodeTwoDropdown(statCode2)
                .setStatcodeThreeDropdown(statCode3)
                .setStatcodeFourDropdown(statCode4)
                .setStatcodeFiveDropdown(statCode5)
                .setClassSection(classSection)
                .clickSelectAssetTypeSelectButton()
                .setClassSection(classSection)
                .setDateOfBuild(dateOfBuild)
                .clickCreateAsset();
    }

    @Step("Create IMO Registered Asset")
    public ViewAssetPage createImoRegisteredAsset()
    {
        return createImoRegisteredAsset(randomiseConstant(assetName));
    }

}
