package workhub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import com.frameworkium.core.ui.tests.BaseTest;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import testhub.TestHubPage;
import workhub.batchaction.BatchActionsPage;
import workhub.sub.AllAssetsSubPage;
import workhub.sub.MyWorkSubPage;
import workhub.sub.PaginationCountSubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.concurrent.TimeUnit;

public class WorkHubPage extends BasePage<WorkHubPage>
{

    private final String BUTTON_SELECTED = "white-button arrowed";

    @Name("Search/Filter Assets Button")
    @FindBy(id = "dashboard-asset-search-filter-toggle")
    @Visible
    private WebElement searchFilterAssetsButton;

    @Name("My/Team's work tab")
    @FindBy(id = "dashboard-tab-my-work")
    @Visible
    private WebElement myWorkTab;

    @Name("All assets tab")
    @FindBy(linkText = "All assets")
    @Visible
    private WebElement allAssetsTab;

    @Name("Test Hub tab")
    @FindBy(linkText = "Test Hub")
    private WebElement testHubTab;

    @Name("Add new asset")
    @FindBy(css = "[data-ng-if='vm.isCaseUser'] .icon-small-add")
    private WebElement addNewAssetButton;

    @Name("Batch actions")
    @FindBy(css = "[data-ng-if='vm.isCaseUser'] [data-ui-sref*='batch-actions.define']")
    private WebElement batchActionsButton;

    @Name("Notification Message Text")
    @FindBy(css = "[data-ng-if='!vm.filtered']")
    private WebElement notificationMessageText;

    @Name("Finding the active tab")
    @FindBy(className = "is-active")
    private WebElement activeTab;

    public static WorkHubPage open()
    {
        new WebDriverWait(BaseTest.getDriver(), 90).until(
                ExpectedConditions.visibilityOfElementLocated(By.id("dashboard-asset-search-filter-toggle")));
        return PageFactory.newInstance(WorkHubPage.class);
    }

    @Step("Clicking the search/filter assets button")
    public SearchFilterSubPage clickSearchFilterAssetsButton()
    {
        searchFilterAssetsButton.click();
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id(SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));
        return PageFactory.newInstance(SearchFilterSubPage.class);
    }

    @Step("Clicking the My Work tab")
    public MyWorkSubPage clickMyWorkTab()
    {
        myWorkTab.click();
        return PageFactory.newInstance(MyWorkSubPage.class);
    }

    @Step("Clicking the All assets tab")
    public AllAssetsSubPage clickAllAssetsTab()
    {
        allAssetsTab.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        return PageFactory.newInstance(AllAssetsSubPage.class);
    }

    @Step("Click on test hub tab")
    public TestHubPage clickTestHubTab()
    {
        testHubTab.click();
        return PageFactory.newInstance(TestHubPage.class);
    }

    @Step("Clicking on the add new asset button")
    public AddNewAssetPage clickAddNewAsset()
    {
        addNewAssetButton.click();
        return PageFactory.newInstance(AddNewAssetPage.class);
    }

    @Step("Clicking on the batch actions button")
    public BatchActionsPage clickBatchActions()
    {
        batchActionsButton.click();
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(BatchActionsPage.class);
    }

    @Step("Getting the active tab")
    public String getActiveTab()
    {
        return activeTab.getText();
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    @Step("Get notification message text")
    public String getNotificationMessageText()
    {
        return notificationMessageText.getText();
    }

    @Step("Returning the pagination count object")
    public PaginationCountSubPage getPaginationCountPage()
    {
        return PageFactory.newInstance(PaginationCountSubPage.class);
    }

    @Step("Closing the search/filter assets section")
    public void closeSearchFilterAssetsButton()
    {
        searchFilterAssetsButton.click();
        new WebDriverWait(driver, 10)
                .pollingEvery(1000, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions.invisibilityOfElementLocated(By.id(SearchFilterSubPage.ASSET_SEARCH_INPUT_ID)));
    }

    @Step("Check that Search Filter Assets button is displayed")
    public boolean isSearchFilterAssetsButtonDisplayed()
    {
        return searchFilterAssetsButton.isDisplayed();
    }

    @Step("Check that Batch action button is displayed")
    public boolean isBatchActionsButtonDisplayed()
    {
        return batchActionsButton.isDisplayed();
    }

    @Step("")
    public boolean isAddNewAssetButtonDisplayed()
    {
        return addNewAssetButton.isDisplayed();
    }

    @Step("Check that All asset button is selected")
    public boolean isAllAssetTabDisplayed()
    {
        return allAssetsTab.isDisplayed();
    }

    @Step("Check that All asset button is selected")
    public boolean isAllAssetTabSelected()
    {
        return allAssetsTab.getAttribute("class").contains(BUTTON_SELECTED);
    }

    @Step("Is Add New Asset Button is Clickable")
    public boolean isAddNewAssetButtonIsClickable()
    {
        return addNewAssetButton.isEnabled();
    }

}
