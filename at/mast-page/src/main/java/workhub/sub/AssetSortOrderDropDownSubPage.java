package workhub.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class AssetSortOrderDropDownSubPage extends BasePage<AssetSortOrderDropDownSubPage>
{

    //note that this is different from AllAssetSubPage assetSortOrderDropDown
    // as they return different pages
    @Name("Asset sort order dropdown")
    @FindBy(css = "[data-ng-model='vm.sortOrder']")
    @Visible
    private WebElement assetSortOrderDropdown;

    @Step("Selecting a sort order from the dropdown")
    public MyWorkSubPage setAssetSortOrder(String order)
    {
        new Select(assetSortOrderDropdown).selectByVisibleText(order);
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        return PageFactory.newInstance(MyWorkSubPage.class);
    }

    @Step("Get selected sort order from Asset sort order dropdown")
    public String getSelectedOptionsFromAssetSortOrderDropdown()
    {
        return new Select(assetSortOrderDropdown).getFirstSelectedOption().getText();
    }

    public enum AssetOrderBy
    {
        NAME_ASC("Asset Name A>Z"),
        NAME_DESC("Asset Name Z>A"),
        BUILD_DATE_ASC("Date of build Ascending"),
        BUILD_DATE_DESC("Date of build Descending");

        private String assetOrderBy;

        AssetOrderBy(String assetOrderBy)
        {
            this.assetOrderBy = assetOrderBy;
        }

        public String getString()
        {
            return assetOrderBy;
        }
    }

}
