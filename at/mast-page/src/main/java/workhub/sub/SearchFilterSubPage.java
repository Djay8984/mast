package workhub.sub;

import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import model.customer.Customer;
import model.referencedata.ReferenceDataMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import util.AppHelper;
import workhub.sub.searchfilter.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static constant.LloydsRegister.FRONTEND_TIME_FORMAT;
import static model.referencedata.ReferenceDataMap.RefData.*;
import static model.referencedata.ReferenceDataMap.Subset.ASSET;

public class SearchFilterSubPage extends BasePage<SearchFilterSubPage>
{

    public static final String ASSET_SEARCH_INPUT_ID = "asset-search-query";
    public static final String ASSET_SEARCH_LOADING_CSS = "[data-ng-hide='vm.loaded']";
    public static final String BUTTON_SELECTED_CSS = "white-button arrowed";

    @Name("Search Filter SubPage container")
    @FindBy(css = "#dashboard-tab-container >div[data-ng-show]:nth-child(3)")
    private WebElement searchFilterSubPageContainer;

    @Name("Asset Search Input")
    @FindBy(id = ASSET_SEARCH_INPUT_ID)
    @Visible
    private WebElement assetSearchInput;

    @Name("Display sister assets checkbox")
    @FindBy(css = ".grid-content.checkbox-container > button")
    private WebElement displaySisterAssetsCheckBox;

    @Name("Asset type filter button")
    @FindBy(css = "[aria-label='Select Asset type filter']")
    @Visible
    private WebElement assetTypeFilterButton;

    @Name("Lifecycle status filter button")
    @FindBy(css = "[aria-label='Select Lifecycle status filter']")
    @Visible
    private WebElement lifecycleStatusFilterButton;

    @Name("Class status filter button")
    @FindBy(css = "[aria-label='Select Class status filter']")
    @Visible
    private WebElement classStatusFilterButton;

    @Name("Build location filter button")
    @FindBy(css = "[aria-label='Select Build location filter']")
    @Visible
    private WebElement buildLocationFilterButton;

    @Name("Build date filter button")
    @FindBy(css = "[aria-label='Select Build date filter']")
    @Visible
    private WebElement buildDateFilterButton;

    @Name("Flag filter button")
    @FindBy(css = "[aria-label='Select Flag filter']")
    @Visible
    private WebElement flagFilterButton;

    @Name("Gross tonnage filter button")
    @FindBy(css = "[aria-label='Select Gross tonnage filter']")
    @Visible
    private WebElement grossTonnageFilterButton;

    @Name("Customer filter button")
    @FindBy(css = "[aria-label='Select Customer filter']")
    @Visible
    private WebElement customerFilterButton;

    @Name("Asset Search Reset Button")
    @FindBy(id = "asset-search-reset")
    @Visible
    private WebElement assetSearchResetButton;

    @Name("Asset Search Apply Button")
    @FindBy(id = "asset-search-apply")
    @Visible
    private WebElement assetSearchApplyButton;

    @Name("Filter Icon")
    @FindBy(css = "[data-ng-if='filter.active']")
    private List<WebElement> filterIcon;

    @Step("Setting the asset search input")
    public SearchFilterSubPage setAssetSearchInput(String input)
    {
        assetSearchInput.clear();
        assetSearchInput.sendKeys(input);
        return this;
    }

    @Step("Verify if display sister assets check box is displayed")
    public boolean isDisplaySisterAssetsCheckboxIsDisplayed()
    {
        return displaySisterAssetsCheckBox.isDisplayed();
    }

    @Step("Retrieve status of display sister assets check box")
    public boolean isDisplaySisterAssetsCheckboxChecked()
    {
        return displaySisterAssetsCheckBox.getAttribute("class").contains("checked");
    }

    @Step("Clicking the asset search reset button")
    public void clickAssetSearchResetButton()
    {
        assetSearchResetButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName("spinner")));
    }

    @Deprecated
    @Step("Clicking the asset search apply button")
    public void clickAssetSearchApplyButton()
    {
        AppHelper.scrollToBottom();
        assetSearchApplyButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName("spinner")));
    }

    /**
     * Click on the AssetSearchApplyButton
     *
     * @param pageObjectClass - The pageObject which is expected to be returned by clicking the AssetSearchApplyButton
     *                        Possible PageObjects: MyWorkSubPage.class
     *                        AllAssetsSubPage.class
     * @param <T>             The implementing type
     */
    @Step("Click asset search apply button ")
    public <T extends BasePage<T>> T clickAssetSearchApplyButton(Class<T> pageObjectClass)
    {
        AppHelper.scrollToBottom();
        assetSearchApplyButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.tagName("spinner")));
        return PageFactory.newInstance(pageObjectClass);
    }

    @Step("Is the asset search button clickable")
    public boolean isSearchButtonClickable()
    {
        return assetSearchApplyButton.getAttribute("disabled").equals("true");
    }

    @Step("Is asset search apply button displayed")
    public boolean isAssetSearchApplyButtonDisplayed()
    {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(ASSET_SEARCH_INPUT_ID)));
        return assetSearchApplyButton.isDisplayed();
    }

    @Step("Applying the asset type filter")
    public AssetTypeFilterSubPage clickAssetTypeFilterButton()
    {
        assetTypeFilterButton.click();
        return PageFactory.newInstance(AssetTypeFilterSubPage.class);
    }

    @Step("Applying the lifecycle status filter")
    public LifecycleStatusFilterSubPage clickLifecycleStatusFilterButton()
    {
        lifecycleStatusFilterButton.click();
        return PageFactory.newInstance(LifecycleStatusFilterSubPage.class);
    }

    @Step("Applying the class status filter")
    public ClassStatusFilterSubPage clickClassStatusFilterButton()
    {
        classStatusFilterButton.click();
        return PageFactory.newInstance(ClassStatusFilterSubPage.class);
    }

    @Step("Applying the build location filter")
    public BuildLocationFilterSubPage clickBuildLocationFilterButton()
    {
        buildLocationFilterButton.click();
        return PageFactory.newInstance(BuildLocationFilterSubPage.class);
    }

    @Step("Applying the build date filter")
    public BuildDateFilterSubPage clickBuildDateFilterButton()
    {
        buildDateFilterButton.click();
        return PageFactory.newInstance(BuildDateFilterSubPage.class);
    }

    @Step("Applying the flag filter")
    public FlagSearchSubPage clickFlagFilterButton()
    {
        flagFilterButton.click();
        return PageFactory.newInstance(FlagSearchSubPage.class);
    }

    @Step("Applying the gross tonnage filter")
    public GrossTonnageSearchSubPage clickGrossTonnageFilterButton()
    {
        grossTonnageFilterButton.click();
        return PageFactory.newInstance(GrossTonnageSearchSubPage.class);
    }

    @Step("Applying the customer filter")
    public CustomerFilterSubPage clickCustomerFilterButton()
    {
        customerFilterButton.click();
        return PageFactory.newInstance(CustomerFilterSubPage.class);
    }

    @Step("Check that Search Filter SubPage is displayed")
    public boolean isSearchFilterSubPageContainerDisplayed()
    {
        return !searchFilterSubPageContainer.getAttribute("class")
                .contains("ng-hide");
    }

    @Step("Check that Asset type filter button is selected")
    public boolean isAssetTypeFilterButtonSelected()
    {
        return assetTypeFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Lifecycle status filter button is selected")
    public boolean isLifecycleStatusFilterButtonSelected()
    {
        return lifecycleStatusFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Lifecycle status filter button is selected")
    public boolean isClassStatusFilterButtonSelected()
    {
        return classStatusFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Build location filter button is selected")
    public boolean isBuildLocationFilterButtonSelected()
    {
        return buildLocationFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Build date filter button is selected")
    public boolean isBuildDateFilterButtonSelected()
    {
        return buildDateFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Flag filter button is selected")
    public boolean isFlagFilterButtonSelected()
    {
        return flagFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Gross tonnage filter button is selected")
    public boolean isGrossTonnageFilterButtonSelected()
    {
        return grossTonnageFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Check that Customer filter button is selected")
    public boolean isCustomerFilterButtonSelected()
    {
        return customerFilterButton
                .findElement(By.cssSelector("span"))
                .getAttribute("class")
                .contains(BUTTON_SELECTED_CSS);
    }

    @Step("Get the text of asset search reset button")
    public String getAssetSearchResetButtonText()
    {
        return assetSearchResetButton.getText();

    }

    @Step("Get the colour of asset search reset button")
    public String getAssetSearchResetButtonColor()
    {
        return assetSearchResetButton.getCssValue("background-color");
    }

    @Step("Is asset search apply button clickable")
    public boolean isAssetSearchApplyButtonClickable()
    {
        return assetSearchApplyButton.isEnabled();
    }

    @Step("Is asset Reset button clickable")
    public boolean isAssetResetButtonClickable()
    {
        return assetSearchResetButton.isEnabled();
    }

    @Step("Get the asset search input")
    public String getAssetSearchInputText()
    {
        return assetSearchInput.getAttribute("value");
    }

    @Step("Is Filter Icon Displayed")
    public boolean isFilterIconDisplayed(){
        return filterIcon.size() > 0;
    }


    /*
     TODO: Filters which haven't been taken into consideration due to missing PO's
         - Build Location
         - Flag
      */
    @Step("Applying search filters based on the asset query")
    public void applySearchFiltersBasedOnQuery(AssetQueryDto assetQueryDto)
    {
        //Asset Types
        List<Long> assetTypes = assetQueryDto.getAssetTypeId();
        if (assetTypes != null)
        {
            AssetTypeFilterSubPage assetTypeFilterSubPage = clickAssetTypeFilterButton();
            if (assetTypes.size() > 0)
            {
                assetTypeFilterSubPage.setStatcodeOneDropdown(assetTypes.get(0));
                if (assetTypes.size() > 1)
                {
                    assetTypeFilterSubPage.setStatcodeTwoDropdown(assetTypes.get(1));
                    if (assetTypes.size() > 2)
                    {
                        assetTypeFilterSubPage.setStatcodeThreeDropdown(assetTypes.get(2));
                        if (assetTypes.size() > 3)
                        {
                            assetTypeFilterSubPage.setStatcodeFourDropdown(assetTypes.get(3));
                            if (assetTypes.size() > 4)
                            {
                                assetTypeFilterSubPage.setStatcodeFiveDropdown(assetTypes.get(4));
                            }
                        }
                    }
                }
            }
        }

        //Lifecycle Statuses
        ReferenceDataMap lifeCycleStatusReferenceData = new ReferenceDataMap(ASSET, LIFECYCLE_STATUSES);
        List<Long> lifecycleStatuses = assetQueryDto.getLifecycleStatusId();
        List<ReferenceDataDto> lifeCycleStatusNames = new ArrayList<>();
        if (lifecycleStatuses != null)
        {
            LifecycleStatusFilterSubPage lifecycleStatusFilterSubPage = clickLifecycleStatusFilterButton();
            lifecycleStatuses.forEach(
                    o -> lifeCycleStatusNames.add(
                            lifeCycleStatusReferenceData.getReferenceDataArrayList().stream()
                                    .filter(l -> l.getId().equals(o))
                                    .findFirst()
                                    .orElse(null)
                    )
            );
            lifecycleStatusFilterSubPage.clickClearButton();
            lifeCycleStatusNames.forEach(
                    r -> lifecycleStatusFilterSubPage.selectLifeCycleStatus(r.getName())
            );
        }

        //Class Statuses
        ReferenceDataMap classStatusReferenceData = new ReferenceDataMap(ASSET, CLASS_STATUSES);
        List<Long> classStatuses = assetQueryDto.getClassStatusId();
        List<ReferenceDataDto> classStatusNames = new ArrayList<>();
        if (classStatuses != null)
        {
            ClassStatusFilterSubPage classStatusFilterSubPage = clickClassStatusFilterButton();
            classStatuses.forEach(
                    o -> classStatusNames.add(
                            classStatusReferenceData.getReferenceDataArrayList().stream()
                                    .filter(c -> c.getId().equals(o))
                                    .findFirst()
                                    .orElse(null)
                    )
            );
            classStatusFilterSubPage.clickClearButton();
            classStatusNames.forEach(
                    r -> classStatusFilterSubPage.selectClassStatus(r.getName())
            );
        }

        //Build Date
        Date fromDate = assetQueryDto.getBuildDateMin();
        Date toDate = assetQueryDto.getBuildDateMax();
        if (fromDate != null || toDate != null)
        {
            BuildDateFilterSubPage buildDateFilterSubPage = clickBuildDateFilterButton();
            if (fromDate != null)
                buildDateFilterSubPage.setFrBuildDate(FRONTEND_TIME_FORMAT.format(fromDate));
            if (toDate != null)
                buildDateFilterSubPage.setToBuildDate(FRONTEND_TIME_FORMAT.format(toDate));
        }

        //Gross Tonnage
        Double minimumGrossTonnage = assetQueryDto.getGrossTonnageMin();
        Double maximumGrossTonnage = assetQueryDto.getGrossTonnageMax();
        if (minimumGrossTonnage != null || maximumGrossTonnage != null)
        {
            GrossTonnageSearchSubPage grossTonnageSearchSubPage = clickGrossTonnageFilterButton();
            if (minimumGrossTonnage != null)
                grossTonnageSearchSubPage.setMinGrossTonnage(minimumGrossTonnage.toString());
            if (maximumGrossTonnage != null)
                grossTonnageSearchSubPage.setMaxGrossTonnage(maximumGrossTonnage.toString());
        }

        //Customer
        List<Long> partyIds = assetQueryDto.getPartyId();
        List<String> partyIdNames = new ArrayList<>();
        CustomerFilterSubPage customerFilterSubPage = clickCustomerFilterButton();
        if (partyIds != null)
        {
            partyIds
                    .forEach(
                            o -> partyIdNames.add(new Customer(o.intValue()).getDto().getName())
                    );
            customerFilterSubPage.setCustomerName(partyIdNames.get(0));
        }
        List<Long> partyRoleIds = assetQueryDto.getPartyRoleId();
        if (partyRoleIds != null)
        {
            ReferenceDataMap referenceDataMap = new ReferenceDataMap(ASSET, PARTY_ROLES);
            String partyRole = referenceDataMap.getReferenceDataArrayList().stream()
                    .filter(o -> o.getId().equals(partyRoleIds.get(0)))
                    .findFirst()
                    .orElse(null)
                    .getName();
            customerFilterSubPage.setCustomerRoleDropdown(partyRole);
        }
        List<Long> partyFunctionTypeIds = assetQueryDto.getPartyFunctionTypeId();
        if (partyFunctionTypeIds != null)
        {
            ReferenceDataMap referenceDataMap = new ReferenceDataMap(ASSET, PARTY_FUNCTIONS);
            String partyFunction = referenceDataMap.getReferenceDataArrayList().stream()
                    .filter(o -> o.getId().equals(partyFunctionTypeIds.get(0)))
                    .findFirst()
                    .orElse(null)
                    .getName();
            customerFilterSubPage.setCustomerFunctionDropdown(partyFunction);
        }
    }
}
