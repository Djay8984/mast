package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class BuildDateFilterSubPage extends BasePage<BuildDateFilterSubPage>
{

    @Name("From Build Date")
    @FindBy(id = "asset-search-filter-build-date-min")
    @Visible
    private WebElement frBuildDate;

    @Name("From Build Date Invalid Date Message")
    @FindBy(css = "[data-ng-init='vm.filters.buildDate.form = assetSearchFilterBuildDate']  div:nth-child(1) ul.validation-error")
    @Visible
    private WebElement frBuildDateInvalidDateMessage;

    @Name("To Build Date")
    @FindBy(id = "asset-search-filter-build-date-max")
    @Visible
    private WebElement toBuildDate;

    @Name("To Build Date Invalid Date Message")
    @FindBy(css = "[data-ng-init='vm.filters.buildDate.form = assetSearchFilterBuildDate']  div:nth-child(2) ul.validation-error")
    @Visible
    private WebElement toBuildDateInvalidDateMessage;

    @Step("Get From Build Date")
    public String getFrBuildDate()
    {
        return frBuildDate.getAttribute("value");
    }

    @Step("Set From Build Date")
    public BuildDateFilterSubPage setFrBuildDate(String value)
    {
        frBuildDate.clear();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('value', '" + value + "')", frBuildDate);
        return this;
    }

    @Step("Return if From Build Date is invalid")
    public boolean isFrBuildDateInvalid()
    {
        return frBuildDate.getAttribute("class").contains("invalid");
    }

    @Step("Get From Build Date Invalid Date Message")
    public String getFrBuildDateInvalidDateMessage()
    {
        return frBuildDateInvalidDateMessage.getText();
    }

    @Step("Get To Build Date")
    public String getToBuildDate()
    {
        return toBuildDate.getAttribute("value");
    }

    @Step("Set To Build Date")
    public void setToBuildDate(String value)
    {
        toBuildDate.clear();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('value', '" + value + "')", toBuildDate);
        toBuildDate.sendKeys(Keys.TAB);
    }

    @Step("Return if To Build Date is invalid")
    public boolean isToBuildDateInvalid()
    {
        return toBuildDate.getAttribute("class").contains("invalid");
    }

    @Step("Get To Build Date Invalid Date Message")
    public String getToBuildDateInvalidDateMessage()
    {
        return toBuildDateInvalidDateMessage.getText();
    }

    @Step("Get To Build Date Invalid Date Message")
    public boolean isToBuildDateBeforeFrBuildDate()
    {

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        try
        {
            if (sdf.parse(getToBuildDate()).compareTo(sdf.parse(getFrBuildDate())) < 0)
            {
                return true;
            }
        }
        catch (ParseException ex)
        {
            ex.printStackTrace();
        }
        return false;

    }

}
