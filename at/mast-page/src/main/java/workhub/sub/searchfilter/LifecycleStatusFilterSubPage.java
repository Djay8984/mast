package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.WorkHubPage;
import workhub.sub.SearchFilterSubPage;

import java.util.ArrayList;
import java.util.List;

public class LifecycleStatusFilterSubPage extends BasePage<LifecycleStatusFilterSubPage>
{

    @Visible
    @Name("Lifecycle Statuses CheckBoxes")
    @FindBy(css = ".checkbox-container:not(.grid-content)")
    private List<WebElement> lifecycleStatus;

    @Visible
    @Name("Select All Button")
    @FindBy(id = "asset-search-filter-lifecycle-status-select-all")
    private WebElement selectAllButton;

    @Visible
    @Name("Clear Button")
    @FindBy(id = "asset-search-filter-lifecycle-status-clear-all")
    private WebElement clearButton;

    @Visible
    @Name("Reset Button")
    @FindBy(id = "asset-search-reset")
    private WebElement resetButton;

    @Step("Selecting a life cycle status based on name")
    public LifecycleStatusFilterSubPage selectLifeCycleStatus(String lifecycleStatusName)
    {
        lifecycleStatus.stream()
                .filter(o -> o.findElement(By.cssSelector("label"))
                        .getText().trim().equals(lifecycleStatusName))
                .findFirst()
                .orElse(null)
                .findElement(By.tagName("button"))
                .click();
        return this;
    }

    @Step("Verify LifecycleStatus Check boxes are selected")
    public boolean isLifecycleStatusSelected(String text)
    {
        return lifecycleStatus.stream()
                .filter(e -> e.findElement(By.cssSelector("label")).getText().equals(text))
                .findFirst()
                .get()
                .findElement(By.tagName("button"))
                .getAttribute("class")
                .contains("checked");
    }

    @Step("Verify LifecycleStatus Check boxes is Enabled")
    public boolean isLifecycleStatusEnabled(String text)
    {
        return lifecycleStatus.stream()
                .filter(e -> e.findElement(By.cssSelector("label")).getText().equals(text))
                .findFirst()
                .get()
                .findElement(By.tagName("button"))
                .isEnabled();
    }

    @Step("Deselection of LifecycleStatus Check boxes")
    public LifecycleStatusFilterSubPage deSelectLifecycleStatusesCheckBoxes(String text)
    {
        for (WebElement checkbox : lifecycleStatus)
        {
            if (checkbox.findElement(By.cssSelector("label")).getText().trim().equals(text))
                checkbox.findElement(By.tagName("button")).click();
        }
        return this;
    }

    @Step("Click Select All Button")
    public LifecycleStatusFilterSubPage clickSelectAllButton()
    {
        selectAllButton.click();
        return this;
    }

    @Step("Click Clear Button")
    public LifecycleStatusFilterSubPage clickClearButton()
    {
        clearButton.click();
        return this;
    }

    @Step("Click Reset Button")
    public WorkHubPage clickResetButton()
    {
        resetButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        return PageFactory.newInstance(WorkHubPage.class);
    }

    @Step("Return the checked status of all the checkboxes")
    public List<Boolean> getAllCheckboxCheckedStatus()
    {
        List<Boolean> status = new ArrayList<Boolean>();
        for (WebElement checkbox : lifecycleStatus)
        {
            if (checkbox.findElement(By.tagName("button")).
                    getAttribute("class").contains("checked"))
                status.add(true);
        }
        return status;
    }
}
