package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.LookAheadSelect;

import java.util.List;

public class CustomerFilterSubPage extends BasePage<CustomerFilterSubPage>
{
    @Name("Customer name input")
    @FindBy(id = "asset-search-filter-customer-name")
    @Visible
    private WebElement customerNameInput;

    @Name("Customer name dropdown")
    @FindBy(css = "[data-ng-init='vm.filters.customer.form = assetSearchFilterCustomer'] ul")
    private LookAheadSelect customerNameDropdown;

    @Name("Customer role dropdown")
    @FindBy(id = "asset-search-filter-customer-role")
    @Visible
    private WebElement customerRoleDropdown;

    @Name("Customer function dropdown")
    @FindBy(id = "asset-search-filter-customer-function")
    @Visible
    private WebElement customerFunctionDropdown;

    @Name("Customer validation message")
    @FindBy(id = "validation-message-customer-editable")
    private WebElement customerValidationMessage;

    @Step("Setting the customer name search input")
    public CustomerFilterSubPage setCustomerName(String input)
    {
        customerNameInput.clear();
        customerNameInput.sendKeys(input);
        // if input does not match EXACTLY (case sensitive) with testdata,
        // expect the dropdown to pop up
        try
        {
            wait.until(ExpectedConditions.or(ExpectedConditions.visibilityOf(customerNameDropdown),
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//li/a"))));
            customerNameDropdown.selectText(input);
        }catch(Exception e)
        {}
        return this;
    }

    @Step("Get the customer name text")
    public String getCustomerNameText()
    {
        return customerNameInput.getAttribute("value").trim();
    }

    @Step("Setting customer role dropdown")
    public CustomerFilterSubPage setCustomerRoleDropdown(String value)
    {
        new Select(customerRoleDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting customer function dropdown")
    public CustomerFilterSubPage setCustomerFunctionDropdown(String value)
    {
        new Select(customerFunctionDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Click on customer role dropdown")
    public CustomerFilterSubPage clickCustomerRoleDropdown()
    {
        customerRoleDropdown.click();
        return this;
    }

    @Step("Click on customer function dropdown")
    public CustomerFilterSubPage clickCustomerFunctionDropdown()
    {
        customerFunctionDropdown.click();
        return this;
    }

    @Step("Select from Customer name dropdown by first matched value")
    public CustomerFilterSubPage selectFromCustomerNameDropdownByFirstMatch
            (String value)
    {
        List<WebElement> options = customerNameDropdown.findElements(By
                .cssSelector("a"));
        WebElement matchedOption = options.stream()
                .filter(o -> o.getText().toLowerCase().contains(value.toLowerCase()))
                .findFirst()
                .orElse(null);
        matchedOption.click();
        //The entry is matched and click upon, giving 100% guarantee that
        // validation message goes away
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By
                .cssSelector("#validation-message-customer-editable")));
        return this;
    }

    @Step("Check that customer name input is displayed")
    public boolean isCustomerNameInputDisplayed()
    {
        return customerNameInput.isDisplayed();
    }

    @Step("Check that customer name dropdown is displayed")
    public boolean isCustomerNameDropdownDisplayed()
    {
        return customerNameDropdown.isDisplayed();
    }

    @Step("Check that customer role dropdown is displayed")
    public boolean isCustomerRoleDropdownDisplayed()
    {
        return customerRoleDropdown.isDisplayed();
    }

    @Step("Check that customer role dropdown is displayed")
    public boolean isCustomerFunctionDropdownDisplayed()
    {
        return customerFunctionDropdown.isDisplayed();
    }

    @Step("Get the customer validation message")
    public String getCustomerValidationMessage()
    {
        //customerValidationMessage is destroyed when there is no message
        try
        {
            return customerValidationMessage.getText();
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Step("Get Default Customer Role Dropdown Value")
    public String getCustomerRoleDefaultDropdownValue()
    {
        return new Select(customerRoleDropdown).getFirstSelectedOption().getText().trim();
    }

    @Step("Get Default Customer Function Dropdown Value")
    public String getCustomerFunctionDefaultDropdownValue()
    {
        return new Select(customerFunctionDropdown).getFirstSelectedOption().getText().trim();
    }
}
