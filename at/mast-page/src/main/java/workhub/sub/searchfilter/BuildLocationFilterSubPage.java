package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Select;
import typifiedelement.LookAheadSelect;

import java.util.List;

public class BuildLocationFilterSubPage extends BasePage<BuildLocationFilterSubPage>
{

    @Name("Primary Builder input")
    @FindBy(id = "asset-search-filter-build-location-primary-builder")
    @Visible
    private WebElement primaryBuilderInput;

    @Name("Yard Number Input")
    @FindBy(id = "asset-search-filter-build-location-yard-number")
    private WebElement yardNumberInput;

    @Name("Primary Builder dropdown")
    @FindBy(css = "[data-ng-init='vm.filters.buildLocation.form = assetSearchFilterBuildLocation'] ul")
    private LookAheadSelect primaryBuilderDropdown;

    @Name("Primary Builder validation message")
    @FindBy(id = "validation-message-build-location-editable")
    private WebElement primaryBuilderValidationMessage;

    @Name("Yard Number validation message")
    @FindBy(id = "validation-message-build-location-pattern")
    private WebElement yardNumberValidationMessage;

    @Step("Setting the primary builder search input")
    public BuildLocationFilterSubPage setPrimaryBuilderSearchInput(String input)
    {
        primaryBuilderInput.clear();
        primaryBuilderInput.sendKeys(input);
        // if input does not match EXACTLY (case sensitive) with testdata,
        // expect the dropdown to pop up
        try
        {
            wait.until(ExpectedConditions.or(ExpectedConditions.visibilityOf(primaryBuilderDropdown),
                    ExpectedConditions.visibilityOfElementLocated(By.xpath("//li/a"))));
            primaryBuilderDropdown.selectText(input);
        }catch(Exception e)
        {}
        return this;
    }

    @Step("Clear primary builder search input")
    public BuildLocationFilterSubPage clearPrimaryBuilderSearchInput()
    {
        primaryBuilderInput.clear();
        return this;
    }

    @Step("Clear primary builder search input")
    public BuildLocationFilterSubPage clearYardNumberSearchInput()
    {
        yardNumberInput.clear();
        return this;
    }

    @Step("Get primary builder search input text")
    public String getPrimaryBuilderText()
    {
        return primaryBuilderInput.getAttribute("value");
    }

    @Step("Get yard nummber search input text")
    public String getYardNumberSarchText()
    {
        return yardNumberInput.getAttribute("value");
    }

    @Step("Setting the Yard number search input")
    public BuildLocationFilterSubPage setYardNumberSearchInput(String input)
    {
        yardNumberInput.clear();
        yardNumberInput.sendKeys(input);
        return this;
    }

    @Step("Check that Primary Builder input is displayed")
    public boolean isPrimaryBuilderInputDisplayed()
    {
        return primaryBuilderInput.isDisplayed();
    }

    @Step("Check that Yard Number input is displayed")
    public boolean isYardNumberInputDisplayed()
    {
        return yardNumberInput.isDisplayed();
    }

    @Step("Check that customer name dropdown is displayed")
    public boolean isPrimaryBuilderDropdownDisplayed()
    {
        return primaryBuilderDropdown.isDisplayed();
    }

    @Step("Select from Primary Builder dropdown by first matched value")
    public BuildLocationFilterSubPage selectFromPrimaryBuilderDropdownByFirstMatch
            (String value)
    {
        List<WebElement> options = primaryBuilderDropdown.findElements(By
                .cssSelector("a"));
        WebElement matchedOption = options.stream()
                .filter(o -> o.getText().toLowerCase().contains(value.toLowerCase()))
                .findFirst()
                .orElse(null);
        matchedOption.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By
                .cssSelector("#validation-message-build-location-editable")));
        return this;
    }

    @Step("Get the Primary Builder validation message")
    public String getPrimaryBuilderValidationMessage()
    {
        //Primary Builder ValidationMessage is destroyed when there is no message
        try
        {
            return primaryBuilderValidationMessage.getText();
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Step("Get the Primary Builder validation message")
    public String getYardNumberValidationMessage()
    {
        //yard Number ValidationMessage is destroyed when there is no message
        try
        {
            return yardNumberValidationMessage.getText();
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
