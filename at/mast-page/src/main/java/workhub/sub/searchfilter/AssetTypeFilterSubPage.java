package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class AssetTypeFilterSubPage extends BasePage<AssetTypeFilterSubPage>
{

    @Name("Statcode 1 dropdown")
    @FindBy(id = "asset-search-filter-asset-type-statcode-1")
    @Visible
    private WebElement statcodeOneDropdown;

    @Name("Statcode 2 dropdown")
    @FindBy(id = "asset-search-filter-asset-type-statcode-2")
    private WebElement statcodeTwoDropdown;

    @Name("Statcode 3 dropdown")
    @FindBy(id = "asset-search-filter-asset-type-statcode-3")
    private WebElement statcodeThreeDropdown;

    @Name("Statcode 4 dropdown")
    @FindBy(id = "asset-search-filter-asset-type-statcode-4")
    private WebElement statcodeFourDropdown;

    @Name("Statcode 5 dropdown")
    @FindBy(id = "asset-search-filter-asset-type-statcode-5")
    private WebElement statcodeFiveDropdown;

    @Step("Setting statcode one dropdown")
    public AssetTypeFilterSubPage setStatcodeOneDropdown(String value)
    {
        new Select(statcodeOneDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode one dropdown by value")
    public void setStatcodeOneDropdown(Long id)
    {
        new Select(statcodeOneDropdown).selectByValue(Long.toString(id));
    }

    @Step("Setting statcode two dropdown")
    public AssetTypeFilterSubPage setStatcodeTwoDropdown(String value)
    {
        new Select(statcodeTwoDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode two dropdown by value")
    public void setStatcodeTwoDropdown(Long id)
    {
        new Select(statcodeTwoDropdown).selectByValue(Long.toString(id));
    }

    @Step("Setting statcode three dropdown")
    public AssetTypeFilterSubPage setStatcodeThreeDropdown(String value)
    {
        new Select(statcodeThreeDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode three dropdown by value")
    public void setStatcodeThreeDropdown(Long id)
    {
        new Select(statcodeThreeDropdown).selectByValue(Long.toString(id));
    }

    @Step("Setting statcode four dropdown")
    public AssetTypeFilterSubPage setStatcodeFourDropdown(String value)
    {
        new Select(statcodeFourDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode four dropdown by value")
    public void setStatcodeFourDropdown(Long id)
    {
        new Select(statcodeFourDropdown).selectByValue(Long.toString(id));
    }

    @Step("Setting statcode five dropdown")
    public AssetTypeFilterSubPage setStatcodeFiveDropdown(String value)
    {
        new Select(statcodeFiveDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode five dropdown by value")
    public void setStatcodeFiveDropdown(Long id)
    {
        new Select(statcodeFiveDropdown).selectByValue(Long.toString(id));
    }

    @Step("Returning whether statcode one dropdown is visible")
    public boolean isStatcodeOneDropdownVisible()
    {
        return statcodeOneDropdown.isDisplayed();
    }

    @Step("Returning whether statcode two dropdown is visible")
    public boolean isStatcodeTwoDropdownVisible()
    {
        return statcodeTwoDropdown.isDisplayed();
    }

    @Step("Returning whether statcode three dropdown is visible")
    public boolean isStatcodeThreeDropdownVisible()
    {
        return statcodeThreeDropdown.isDisplayed();
    }

    @Step("Returning whether statcode four dropdown is visible")
    public boolean isStatcodeFourDropdownVisible()
    {
        return statcodeFourDropdown.isDisplayed();
    }

    @Step("Returning whether statcode five dropdown is visible")
    public boolean isStatcodeFiveDropdownVisible()
    {
        return statcodeFiveDropdown.isDisplayed();
    }

    @Step("Returning value of statcode one dropdown")
    public String getStatcodeOneDropdownValue()
    {
        return new Select(statcodeOneDropdown).getFirstSelectedOption().getText();
    }

    @Step("Returning value of statcode two dropdown")
    public String getStatcodeTwoDropdownValue()
    {
        return new Select(statcodeTwoDropdown).getFirstSelectedOption().getText();
    }

    @Step("Returning value of statcode three dropdown")
    public String getStatcodeThreeDropdownValue()
    {
        return new Select(statcodeThreeDropdown).getFirstSelectedOption().getText();
    }

    @Step("Returning value of statcode four dropdown")
    public String getStatcodeFourDropdownValue()
    {
        return new Select(statcodeFourDropdown).getFirstSelectedOption().getText();
    }

    @Step("Returning value of statcode five dropdown")
    public String getStatcodeFiveDropdownValue()
    {
        return new Select(statcodeFiveDropdown).getFirstSelectedOption().getText();
    }

}
