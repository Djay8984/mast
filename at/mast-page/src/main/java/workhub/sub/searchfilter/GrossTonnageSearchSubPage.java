package workhub.sub.searchfilter;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class GrossTonnageSearchSubPage extends BasePage<GrossTonnageSearchSubPage>
{

    @Name("Minimum gross tonnage text field")
    @FindBy(id = "asset-search-filter-gross-tonnage-min")
    private WebElement minGrossTonnageText;

    @Name("Maximum gross tonnage text field")
    @FindBy(id = "asset-search-filter-gross-tonnage-max")
    private WebElement maxGrossTonnageText;

    @Step("Get the entered min gross tonnage")
    public String getMinGrossTonnage()
    {
        return minGrossTonnageText.getAttribute("value");
    }

    @Step("Set a min gross tonnage")
    public GrossTonnageSearchSubPage setMinGrossTonnage(String minTonnage)
    {
        minGrossTonnageText.clear();
        minGrossTonnageText.sendKeys(minTonnage);
        return this;
    }

    @Step("Get the entered max gross tonnage")
    public String getMaxGrossTonnage()
    {
        return maxGrossTonnageText.getAttribute("value");
    }

    @Step("Set a max gross tonnage")
    public GrossTonnageSearchSubPage setMaxGrossTonnage(String maxTonnage)
    {
        maxGrossTonnageText.clear();
        maxGrossTonnageText.sendKeys(maxTonnage);
        return this;
    }

    @Step("Is the gross tonnage error message displayed")
    public boolean isGrossTonnageMinErrorDisplayed()
    {
        return driver.findElements(By.id("validation-message-gross-tonnage-min")).size() > 0;
    }

    @Step("Is the gross tonnage error message displayed")
    public boolean isGrossTonnageMaxErrorDisplayed()
    {
        return driver.findElements(By.id("validation-message-gross-tonnage-max")).size() > 0;
    }

    @Step("Is the gross tonnage area displayed")
    public boolean isGrossTonnageAreaDisplayed()
    {
        return driver.findElements(By.id("asset-search-filter-gross-tonnage-min")).size() > 0;
    }

    @Step("Get the data length of min gross tonnage")
    public String getDataLengthOfMinGrossTonnage()
    {
        return minGrossTonnageText.getAttribute("data-length-limit");
    }

    @Step("Clear min gross tonnage")
    public GrossTonnageSearchSubPage clearMinGrossTonnageTextBox()
    {
        minGrossTonnageText.clear();
        return this;
    }

    @Step("Get the data length of Max gross tonnage")
    public String getDataLengthOfMaxGrossTonnage()
    {
        return maxGrossTonnageText.getAttribute("data-length-limit");
    }

    @Step("Clear max gross tonnage")
    public GrossTonnageSearchSubPage clearMaxGrossTonnageTextBox()
    {
        maxGrossTonnageText.clear();
        return this;
    }

    @Step("Get the min gross tonnage Text")
    public String getMinGrossTonnageText()
    {
        return minGrossTonnageText.getAttribute("value");
    }

    @Step("Get the max gross tonnage Text")
    public String getMaxGrossTonnageText()
    {
        return maxGrossTonnageText.getAttribute("value");
    }
}
