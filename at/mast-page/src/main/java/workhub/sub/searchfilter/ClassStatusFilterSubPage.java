package workhub.sub.searchfilter;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.ArrayList;
import java.util.List;

public class ClassStatusFilterSubPage extends BasePage<ClassStatusFilterSubPage>
{

    @Visible
    @Name("Class Statuses CheckBoxes")
    @FindBy(css = ".checkbox-container:not(.grid-content)")
    private List<WebElement> classStatusCheckBoxes;

    @Visible
    @Name("Clear Button")
    @FindBy(id = "asset-search-filter-class-status-clear-all")
    private WebElement clearButton;

    @Visible
    @Name("Select All Button")
    @FindBy(id = "asset-search-filter-class-status-select-all")
    private WebElement selectAllButton;

    @Step("Selecting a class status based on name")
    public void selectClassStatus(String classStatusName)
    {
        classStatusCheckBoxes.stream()
                .filter(c -> c.findElement(By.cssSelector("label")).getText().trim().equals(classStatusName))
                .findFirst()
                .orElse(null)
                .findElement(By.tagName("button"))
                .click();
    }

    @Step("Return the checked status of all the checkboxes")
    public List<Boolean> getAllCheckboxCheckedStatus()
    {
        List<Boolean> status = new ArrayList<>();
        for (WebElement checkbox : classStatusCheckBoxes)
        {
            if (checkbox.findElement(By.tagName("button")).
                    getAttribute("class").contains("checked"))
                status.add(true);
        }
        return status;
    }

    @Step("Click Select All Button")
    public ClassStatusFilterSubPage clickSelectAllButton()
    {
        selectAllButton.click();
        return this;
    }

    @Step("Click Clear Button")
    public ClassStatusFilterSubPage clickClearButton()
    {
        clearButton.click();
        return this;
    }

    @Step("Deselection of Class Status Check boxes")
    public ClassStatusFilterSubPage deSelectClassStatusesCheckBoxes(String text)
    {
        classStatusCheckBoxes.stream().filter(a -> a.findElement(By.cssSelector("label"))
                .getText().trim().equals(text)).findFirst()
                .get().findElement(By.tagName("button")).click();
        return this;
    }

    @Step("Verify Class Status Check boxes are selected")
    public boolean isClassStatusSelected(String text)
    {
        return classStatusCheckBoxes.stream().filter(a -> a.findElement(By.
                cssSelector("label")).getText().trim().equals(text)).findFirst()
                .get().findElement(By.tagName("button")).getAttribute("class")
                .contains("checked");
    }
}
