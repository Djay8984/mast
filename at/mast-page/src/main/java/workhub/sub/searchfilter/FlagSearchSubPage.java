package workhub.sub.searchfilter;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.LookAheadSelect;
import util.ClickClearHelper;

public class FlagSearchSubPage extends BasePage<FlagSearchSubPage>
{

    @Name("Flag text field")
    @FindBy(id = "asset-search-filter-flag-code")
    private LookAheadSelect flagTextField;

    @Name("Flag drop down")
    @FindBy(css = "[data-ng-init='vm.filters.flag.form = assetSearchFilterFlag'] ul")
    private LookAheadSelect flagDropDown;

    @Name("Flag error")
    @FindBy(css = "#asset-search-filter-flag-code .ng-invalid-editable")
    private WebElement flagError;

    @Step("Set a value into the flag text field")
    public FlagSearchSubPage setFlagCode(String flag)
    {
        flagTextField.clear();
        flagTextField.sendKeys(flag);
        flagDropDown.selectByIndex(0);
        return this;
    }

    @Step("Click on the clear field")
    public void clickClearFlagText()
    {
        ClickClearHelper.clickClearText(flagTextField);
    }

    @Step("see if the error/ clear icon is displayed")
    public boolean isIconDisplayed(type type)
    {
        return flagTextField.getCssValue("background-image").contains(type.image);
    }

    @Step("Selecting a flag from the dropdown")
    public void selectFlagFromDropdown(String flag)
    {
        flagDropDown.selectByVisibleText(flag);
        waitForJavascriptFrameworkToFinish();
    }

    @Step("Clear the flag text field")
    public FlagSearchSubPage clearFlagText()
    {
        flagTextField.clear();
        return this;
    }

    @Step("Get flag code text Value")
    public String getFlagCodeText()
    {
        return flagTextField.getAttribute("value");
    }

    public enum type
    {
        ERROR("images/red_warning.png"),
        CLEAR("images/clear.png");

        private final String image;

        type(String image)
        {
            this.image = image;
        }
    }
}
