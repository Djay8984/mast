package workhub.sub;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import model.asset.Asset;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.element.AllAssetsAssetElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AllAssetsSubPage extends BasePage<AllAssetsSubPage>
{

    public static final String ASSET_SEARCH_LOADING_CSS = "[data-ng-hide='vm" +
            ".loaded']";
    public static final String PUSH_DOWN_PANEL_CSS = ".assetDetailOverlayPanel" +
            ".panel.ng-isolate-scope.panel-left.is-active";
    //note that this is different from MyWorkSubPage assetSortOrderDropDown
    // as they return different pages
    @Name("Asset sort order dropdown")
    @FindBy(css = "[data-ng-model='vm.sortOrder']")
    private WebElement assetSortOrderDropdown;

    @Name("Load more button")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private WebElement loadMoreButton;

    @Name("Asset cards")
    private List<AllAssetsAssetElement> assetCards;

    @Step("Selecting a sort order from the dropdown")
    public AllAssetsSubPage setAssetSortOrder(String order)
    {
        new Select(assetSortOrderDropdown).selectByVisibleText(order);
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        return this;
    }

    @Step("Get selected sort order from Asset sort order dropdown")
    public String getSelectedOptionsFromAssetSortOrderDropdown()
    {
        return new Select(assetSortOrderDropdown)
                .getFirstSelectedOption().getText();
    }

    public List<AllAssetsAssetElement> getAssetCards()
    {
        return assetCards;
    }

    @Step("Returning an asset card by index")
    public AllAssetsAssetElement getAssetCardByIndex(int index)
    {
        return assetCards.get(index);
    }

    @Step("Returning an asset card by asset name")
    public AllAssetsAssetElement getAssetCardByAssetName(String assetName)
    {
        return assetCards.stream()
                .filter(c -> c.getAssetName().equals(assetName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an asset card by asset id")
    public AllAssetsAssetElement getAssetCardByAssetId(int assetId)
    {
        return getAssetCardByAssetName(new Asset(assetId).getDto().getName());
    }

    @Step("Returning an asset card by IMO number")
    public AllAssetsAssetElement getAssetCardByImoNumber(Long imoNumber)
    {
        return assetCards.stream()
                .filter(c -> c.getImoNumber().equals(imoNumber.toString()))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning the amount of asset cards")
    public int getAssetCardCount()
    {
        return assetCards.size();
    }

    @Step("Returning the pagination count object")
    public PaginationCountSubPage getPaginationCountPage()
    {
        return PageFactory.newInstance(PaginationCountSubPage.class);
    }

    @Step("Clicking load more button")
    public AllAssetsSubPage clickLoadMoreButton()
    {
        loadMoreButton.click();
        new WebDriverWait(driver, 10)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .until(
                        ExpectedConditions
                                .invisibilityOfElementLocated(By
                                        .cssSelector(ASSET_SEARCH_LOADING_CSS))
                );
        return this;
    }

    @Step("Check that Load more button is displayed")
    public boolean isLoadMoreButtonDisplayed()
    {
        return loadMoreButton.isDisplayed();
    }

    @Step("Check push down Panel is Displayed")
    public boolean isPushDownPanelDisplayed()
    {
        return driver.findElements(By.
                cssSelector(PUSH_DOWN_PANEL_CSS)).size() > 0;
    }

    @Step("Check the sort dropdown text displayed")
    public boolean isSortDropDownTextDisplayed(String dropDownText)
    {
        return new Select(assetSortOrderDropdown).getOptions().stream().filter(e -> e
                .getText().equals(dropDownText)).findFirst().isPresent();
    }

    @Step("Is View Asset Button is displayed")
    public List<Boolean> isViewAssetButtonDisplayed()
    {
        List<Boolean> status = new ArrayList<>();
        for (WebElement viewStatus : assetCards)
        {
            if (viewStatus.findElement(By.cssSelector("a[href*='asset']")).isDisplayed())
                status.add(true);
        }
        return status;
    }
}
