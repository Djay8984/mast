package workhub.sub;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.truth.Truth.assertThat;
import static constant.LloydsRegister.SHOWING_REGEX;
import static constant.LloydsRegister.TOTAL_REGEX;

public class PaginationCountSubPage extends BasePage<PaginationCountSubPage>
{

    @Name("Pagination Count Text")
    @FindBy(css = "[class='grid-content small-3']:nth-child(1)")
    private WebElement paginationCountText;

    @Name("Pagination search message text")
    @FindBy(css = "[data-ng-if='vm.filtered']")
    private WebElement paginationSearchMessageText;

    @Step("Returning the page count text")
    public String getPaginationCountText()
    {
        return paginationCountText.getText();
    }

    @Step("Returning how many items are shown")
    public int getShownAmount()
    {
        return matchText(SHOWING_REGEX);
    }

    @Step("Returning the total amount of items")
    public int getTotalAmount()
    {
        return matchText(TOTAL_REGEX);
    }

    @Step("Returning the search message text")
    public String getSearchMessageText()
    {
        return paginationSearchMessageText.getText();
    }

    private int matchText(String regex)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(paginationCountText.getText());
        assertThat(matcher.find()).isTrue();
        return Integer.parseInt(matcher.group(1));
    }

}
