package workhub.sub.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;
import util.AppHelper;
import viewasset.ViewAssetPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;
import workhub.sub.SearchFilterSubPage;

import java.util.List;

public class NonIMORegisteredAssetPage extends BasePage<NonIMORegisteredAssetPage>
{

    @Visible
    @Name("Builder Text Box")
    @FindBy(id = "asset-builder")
    private WebElement builderTextBox;

    @Visible
    @Name("Yard Number Text Box")
    @FindBy(id = "asset-yard")
    private WebElement yardNumberTextBox;

    @Visible
    @Name("Asset Name Text Box")
    @FindBy(id = "asset-name")
    private WebElement assetNameTextBox;

    @Visible
    @Name("Asset Category Button")
    @FindBy(css = "#asset-category button")
    private WebElement assetCategoryButton;

    @Visible
    @Name("Asset Type Button")
    @FindBy(css = "#asset-type button")
    private WebElement assetTypeButton;

    @Name("Date Of Build")
    @FindBy(id = "asset-build-date")
    @Visible
    private WebElement dateOfBuild;

    @Visible
    @Name("Add Attachments Button")
    @FindBy(id = "asset-attachments")
    private WebElement addAttachmentsButton;

    @Name("Asset Category List")
    @FindBy(css = "[data-ng-bind='option.name']")
    private List<WebElement> assetCategoryList;

    @Name("Asset Category select button")
    @FindBy(css = "[data-ng-click='vm.confirm()']")
    private WebElement assetCategorySelectButton;

    @Name("Select Asset Type Button")
    @FindBy(css = "[data-ng-click='vm.confirm()']")
    private WebElement assetTypeSelectButton;

    @Name("IMO Number Text Box")
    @FindBy(id = "asset-imo")
    private List<WebElement> imoNumberTextBox;

    @Name("Selected Asset Category element")
    @FindBy(css = "#asset-category [data-ng-repeat='item in vm.selection']")
    private List<WebElement> selectedAssetCategory;

    @Name("Selected Asset Type element")
    @FindBy(css = "#asset-type [data-ng-repeat='item in vm.selection']")
    private List<WebElement> selectedAssetType;

    @Name("Class Section dropdown")
    @FindBy(css = "[name='class-section']")
    private WebElement classSectionDropdown;

    @Name("Statcode 1 dropdown")
    @FindBy(id = "asset-type-picker-statcode-1")
    private WebElement statcodeOneDropdown;

    @Name("Statcode 2 dropdown")
    @FindBy(id = "asset-type-picker-statcode-2")
    private WebElement statcodeTwoDropdown;

    @Name("Statcode 3 dropdown")
    @FindBy(id = "asset-type-picker-statcode-3")
    private WebElement statcodeThreeDropdown;

    @Name("Statcode 4 dropdown")
    @FindBy(id = "asset-type-picker-statcode-4")
    private WebElement statcodeFourDropdown;

    @Name("Statcode 5 dropdown")
    @FindBy(id = "asset-type-picker-statcode-5")
    private WebElement statcodeFiveDropdown;

    @Name("Validation message element")
    @FindBy(id = "validation-message-date-format")
    private WebElement validationMessage;

    @Name("Create asset")
    @FindBy(id = "create-asset-create")
    private WebElement createAssetButton;

    @Name("Non IMO Registered Asset Radio Button")
    @FindBy(css = "div[class='grid-block shrink']:nth-child(3) input")
    private RadioButton nonImoRegisteredAssetRadioButton;

    @Step("Entering a value into the Builder Text Box")
    public NonIMORegisteredAssetPage setBuilderName(String builderName)
    {
        builderTextBox.clear();
        builderTextBox.sendKeys(builderName);
        return this;
    }

    @Step("Entering a value into the Yard Number Text Box")
    public NonIMORegisteredAssetPage setYardNumber(String yardNumber)
    {
        yardNumberTextBox.clear();
        yardNumberTextBox.sendKeys(yardNumber);
        return this;
    }

    @Step("Entering a value into the Asset Name Text Box")
    public NonIMORegisteredAssetPage setAssetName(String assetName)
    {
        assetNameTextBox.clear();
        assetNameTextBox.sendKeys(assetName);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Set Date Of Build")
    public NonIMORegisteredAssetPage setDateOfBuild(String buildDate)
    {
        AppHelper.scrollToBottom();
        dateOfBuild.clear();
        dateOfBuild.sendKeys(buildDate);
        dateOfBuild.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Select an Asset from Category list By Name")
    public NonIMORegisteredAssetPage selectAssetCategoryByName(String assetCategory)
    {
        assetCategoryList.stream()
                .filter(e -> e.getText().toLowerCase().trim().equals(assetCategory.toLowerCase()))
                .findFirst()
                .get()
                .click();
        waitForJavascriptFrameworkToFinish();
        assetCategorySelectButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Click Asset Category Button")
    public NonIMORegisteredAssetPage clickAssetCategoryButton()
    {
        AppHelper.scrollToBottom();
        assetCategoryButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Click Asset Type Button")
    public NonIMORegisteredAssetPage clickAssetTypeButton()
    {
        AppHelper.scrollToBottom();
        assetTypeButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Click on Add Attachments Button")
    public AttachmentsAndNotesPage clickAddAttachmentsButton()
    {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);",
                        addAttachmentsButton);
        addAttachmentsButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(SearchFilterSubPage.ASSET_SEARCH_LOADING_CSS)));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("Get the text from Builder Text Box ")
    public String getBuilderText()
    {
        return builderTextBox.getText();
    }

    @Step("Get the text from yard number Text Box ")
    public String getYardNumberText()
    {
        return yardNumberTextBox.getText();
    }

    @Step("Get the text from Asset Name Text Box ")
    public String getAssetNameText()
    {
        return assetNameTextBox.getText();
    }

    @Step("Get the text from Date of build Text Box ")
    public String getDateOfBuildText()
    {
        return dateOfBuild.getText();
    }

    @Step("Is selected Asset Category Displayed")
    public boolean isSelectedAssetCategoryDisplayed()
    {
        return selectedAssetCategory.size() > 0;
    }

    @Step("Is selected Asset Type Displayed")
    public boolean isSelectedAssetTypeDisplayed()
    {
        return selectedAssetType.size() > 0;
    }

    @Step("see if the Add Attachments Button is clickable")
    public boolean isAddAttachmentsButtonClickable()
    {
        return addAttachmentsButton.isEnabled();
    }

    @Step("Is IMO Number Text Field Displayed")
    public boolean isImoNumberFieldDisplayed()
    {
        return imoNumberTextBox.size() > 0;
    }

    @Step("Setting statcode One dropdown")
    public NonIMORegisteredAssetPage setStatcodeOneDropdown(String value)
    {
        new Select(statcodeOneDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode two dropdown ")
    public NonIMORegisteredAssetPage setStatcodeTwoDropdown(String value)
    {
        new Select(statcodeTwoDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode three dropdown")
    public NonIMORegisteredAssetPage setStatcodeThreeDropdown(String value)
    {
        new Select(statcodeThreeDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode Four dropdown by value")
    public NonIMORegisteredAssetPage setStatcodeFourDropdown(String value)
    {
        new Select(statcodeFourDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode five dropdown")
    public NonIMORegisteredAssetPage setStatcodeFiveDropdown(String value)
    {
        new Select(statcodeFiveDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting the class section dropdown")
    public NonIMORegisteredAssetPage setClassSection(String value)
    {
        new Select(classSectionDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Confirming the asset type button")
    public NonIMORegisteredAssetPage clickSelectAssetTypeSelectButton()
    {
        assetTypeSelectButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".modalwindow.is-active")));
        return this;
    }

    @Step("Check if the error icon is displayed in Asset Name Field")
    public boolean isErrorIconDisplayedInAssetNameField(Type type)
    {
        return assetNameTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Get for validation message")
    public String getValidationMessage()
    {
        return validationMessage.getText().trim();
    }

    @Step("Check if the error icon is displayed in Date of Build Field")
    public boolean isErrorIconDisplayedInDateOfBuildField(Type type)
    {
        return dateOfBuild.getCssValue("background-image").contains(type.image);
    }

    @Step("Check if the error icon is displayed in Builder Field")
    public boolean isErrorIconDisplayedInBuilderField(Type type)
    {
        return builderTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Clicking on the create asset button")
    public ViewAssetPage clickCreateAsset()
    {
        createAssetButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("create-asset-create")));
        return PageFactory.newInstance(ViewAssetPage.class);
    }

    @Step("Check Non IMO Registered Asset Button is selected")
    public boolean isNonIMORegisteredAssetButtonIsSelected()
    {
        return nonImoRegisteredAssetRadioButton.isSelected();
    }

    public enum Type
    {
        ERROR("images/red_warning.png");
        private final String image;

        Type(String image)
        {
            this.image = image;
        }
    }
}
