package workhub.sub.sub.util;

import helper.DatabaseHelper;
import org.testng.Assert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ImoAsset
{

    public String getUniqueImoNumber()
    {
        ArrayList<String> imoNumbers = new ArrayList<>();
        ResultSet rs = DatabaseHelper.executeQuery("SELECT * FROM MAST_ASSET_VERSIONEDASSET ;");
        ArrayList<String> lrNumbers = new ArrayList<>();
        try
        {
            while (rs.next())
            {
                String ids = rs.getString("imo_number");
                imoNumbers.add(ids);
            }
            rs = DatabaseHelper.executeQuery("SELECT * FROM absd_ship_search;", DatabaseHelper.Database.IHS);
            while (rs.next())
            {
                String ids = rs.getString("LRNO");
                lrNumbers.add(ids);
            }
        }
        catch (SQLException e)
        {
            Assert.fail(e.getMessage());
        }
        return lrNumbers.stream()
                .filter(e -> !imoNumbers.contains(e)).findFirst().get();
    }
}
