package workhub.sub.sub;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;
import typifiedelement.RadioButton;
import util.AppHelper;
import viewasset.ViewAssetPage;
import viewasset.sub.commonpage.AttachmentsAndNotesPage;

import java.util.List;

public class IMORegisteredAssetPage extends BasePage<IMORegisteredAssetPage>
{

    @Visible
    @Name("IMO Number Text Box")
    @FindBy(id = "asset-imo")
    private WebElement imoNumberTextBox;

    @Visible
    @Name("Look Up IHS Data Button")
    @FindBy(id = "ihs-lookup")
    private Button lookUpIhsDataButton;

    @Visible
    @Name("Asset Name Text Box")
    @FindBy(id = "asset-name")
    private TextInput assetNameTextBox;

    @Name("Date Of Build")
    @FindBy(id = "asset-build-date")
    @Visible
    private WebElement dateOfBuild;

    @Visible
    @Name("Add Attachments Button")
    @FindBy(id = "asset-attachments")
    private WebElement addAttachmentsButton;

    @Visible
    @Name("Asset Category Button")
    @FindBy(css = "[data-ng-model='vm.asset.category'] button")
    private WebElement assetCategoryButton;

    @Visible
    @Name("Asset Type Button")
    @FindBy(css = "[data-ng-model='vm.asset.type'] button")
    private WebElement assetTypeButton;

    @Name("Selected Asset Category element")
    @FindBy(css = "#asset-category [data-ng-repeat='item in vm.selection']")
    private List<WebElement> selectedAssetCategory;

    @Name("Selected Asset Type element")
    @FindBy(css = "#asset-type [data-ng-repeat='item in vm.selection']")
    private List<WebElement> selectedAssetType;

    @Name("Asset Category List")
    @FindBy(css = "[data-ng-bind='option.name']")
    private List<WebElement> assetCategoryList;

    @Name("Select Asset Category select Button")
    @FindBy(css = "[data-ng-click='vm.confirm()']")
    private WebElement selectAssetCategorySelectButton;

    @Name("Class Section dropdown")
    @FindBy(css = "[name='class-section']")
    private WebElement classSectionDropdown;

    @Name("Statcode 1 dropdown")
    @FindBy(id = "asset-type-picker-statcode-1")
    private WebElement statcodeOneDropdown;

    @Name("Statcode 2 dropdown")
    @FindBy(id = "asset-type-picker-statcode-2")
    private WebElement statcodeTwoDropdown;

    @Name("Statcode 3 dropdown")
    @FindBy(id = "asset-type-picker-statcode-3")
    private WebElement statcodeThreeDropdown;

    @Name("Statcode 4 dropdown")
    @FindBy(id = "asset-type-picker-statcode-4")
    private WebElement statcodeFourDropdown;

    @Name("Statcode 5 dropdown")
    @FindBy(id = "asset-type-picker-statcode-5")
    private WebElement statcodeFiveDropdown;

    @Name("Select Asset Type Button")
    @FindBy(css = "modal-footer.ng-scope [data-ng-click='vm.confirm()']")
    private WebElement selectAssetTypeSelectButton;

    @Name("Create asset")
    @FindBy(id = "create-asset-create")
    private WebElement createAssetButton;

    @Name("IMO Registered Asset Radio Button")
    @FindBy(css = "div[class='grid-block shrink']:nth-child(2) input")
    private RadioButton imoRegisteredAssetRadioButton;

    @Step("Entering a value into the IMO Number text field")
    public IMORegisteredAssetPage setIMONumber(String imoNumber)
    {
        imoNumberTextBox.clear();
        imoNumberTextBox.sendKeys(imoNumber);
        return this;
    }

    @Step("Set Date Of Build")
    public IMORegisteredAssetPage setDateOfBuild(String buildDate)
    {
        dateOfBuild.clear();
        dateOfBuild.sendKeys(buildDate);
        dateOfBuild.sendKeys(Keys.TAB);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("see if the error icon is displayed")
    public boolean isErrorIconDisplayed(type type)
    {
        return imoNumberTextBox.getCssValue("background-image").contains(type.image);
    }

    @Step("Click Look Up IHS Data Button")
    public IMORegisteredAssetPage clickLookUpIhsDataButton()
    {
        lookUpIhsDataButton.click();
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Add Attachment Button")
    public AttachmentsAndNotesPage clickAddAttachmentsButton()
    {
        addAttachmentsButton.click();
        return PageFactory.newInstance(AttachmentsAndNotesPage.class);
    }

    @Step("see if the Add Attachments Button is clickable")
    public boolean isAddAttachmentsButtonClickable()
    {
        return addAttachmentsButton.isEnabled();
    }

    @Step("Get the text from IMO Number Text Box ")
    public String getImoNumberText()
    {
        return imoNumberTextBox.getText();
    }

    @Step("Get the text from Asset Name Text Box ")
    public String getAssetNameText()
    {
        return assetNameTextBox.getText();
    }

    @Step("Is selected Asset Category Displayed")
    public boolean isSelectedAssetCategoryDisplayed()
    {
        return selectedAssetCategory.size() > 0;
    }

    @Step("Is selected Asset Type Displayed")
    public boolean isSelectedAssetTypeDisplayed()
    {
        return selectedAssetType.size() > 0;
    }

    @Step("Get the text from Date of build Text Box ")
    public String getDateOfBuildText()
    {
        return dateOfBuild.getText();
    }

    @Step("Entering a value into the Asset Name Text Box")
    public IMORegisteredAssetPage setAssetName(String assetName)
    {
        assetNameTextBox.clear();
        assetNameTextBox.sendKeys(assetName);
        waitForJavascriptFrameworkToFinish();
        return this;
    }

    @Step("Click Asset Category Button")
    public IMORegisteredAssetPage clickAssetCategoryButton()
    {
        AppHelper.scrollToBottom();
        assetCategoryButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Click Asset Type Button")
    public IMORegisteredAssetPage clickAssetTypeButton()
    {
        assetTypeButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated
                (By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Select an Asset from Category list By Name")
    public IMORegisteredAssetPage selectAssetCategoryByName(String assetCategory)
    {
        assetCategoryList.stream()
                .filter(e -> e.getText().trim().equals(assetCategory))
                .findFirst()
                .get()
                .click();
        waitForJavascriptFrameworkToFinish();
        selectAssetCategorySelectButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.cssSelector(".modal.is-active")));
        return this;
    }

    @Step("Setting statcode One dropdown")
    public IMORegisteredAssetPage setStatcodeOneDropdown(String value)
    {
        new Select(statcodeOneDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode two dropdown ")
    public IMORegisteredAssetPage setStatcodeTwoDropdown(String value)
    {
        new Select(statcodeTwoDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode three dropdown")
    public IMORegisteredAssetPage setStatcodeThreeDropdown(String value)
    {
        new Select(statcodeThreeDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode Four dropdown by value")
    public IMORegisteredAssetPage setStatcodeFourDropdown(String value)
    {
        new Select(statcodeFourDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting statcode five dropdown")
    public IMORegisteredAssetPage setStatcodeFiveDropdown(String value)
    {
        new Select(statcodeFiveDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Setting the class section dropdown")
    public IMORegisteredAssetPage setClassSection(String value)
    {
        new Select(classSectionDropdown).selectByVisibleText(value);
        return this;
    }

    @Step("Click Select asset type Select button")
    public IMORegisteredAssetPage clickSelectAssetTypeSelectButton()
    {
        selectAssetTypeSelectButton.click();
        return this;
    }

    @Step("Clicking on the create asset button")
    public ViewAssetPage clickCreateAsset()
    {
        createAssetButton.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("create-asset-create")));
        return PageFactory.newInstance(ViewAssetPage.class);
    }

    @Step("Check IMO Registered Asset Button is selected")
    public boolean isIMORegisteredAssetButtonIsSelected()
    {
        return imoRegisteredAssetRadioButton.isSelected();
    }

    public enum type
    {
        ERROR("images/red_warning.png");
        private final String image;

        type(String image)
        {
            this.image = image;
        }
    }
}
