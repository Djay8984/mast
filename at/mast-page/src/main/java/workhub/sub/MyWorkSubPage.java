package workhub.sub;

import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import model.asset.Asset;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import workhub.element.MyWorkAssetCase;
import workhub.element.MyWorkAssetElement;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import workhub.element.MyWorkAssetElement;
import workhub.element.MyWorkAssetJob;

import java.util.List;

public class MyWorkSubPage extends BasePage<MyWorkSubPage>
{
    @Name("Asset cards")
    private List<MyWorkAssetElement> assetCards;

    @Name("Job items")
    private List<MyWorkAssetJob> jobItems;

    @Name("Case Items")
    private List<MyWorkAssetCase> caseItems;

    @Name("Load more button")
    @FindBy(css = "[data-ng-click='vm.loadMore()']")
    private WebElement loadMoreButton;

    @Step("Returning the AssetSortOrderDropDownSubPage object")
    public AssetSortOrderDropDownSubPage getAssetSortOrderDropDownSubPage()
    {
        return PageFactory.newInstance(AssetSortOrderDropDownSubPage.class);
    }

    public List<MyWorkAssetElement> getAssetCards()
    {
        return assetCards;
    }

    @Step("Returning an asset card")
    public MyWorkAssetElement getAssetCardByIndex(int index)
    {
        return assetCards.get(index);
    }

    @Step("Returning an asset card by asset name")
    public MyWorkAssetElement getAssetCardByAssetName(String assetName)
    {
        return assetCards.stream()
                .filter(c -> c.getAssetName().equals(assetName))
                .findFirst()
                .orElse(null);
    }

    @Step("Returning an asset card by asset id")
    public MyWorkAssetElement getAssetCardByAssetId(int assetId)
    {
        return getAssetCardByAssetName(new Asset(assetId).getDto().getName());
    }

    @Step("Returning the amount of asset cards")
    public int getAssetCardCount()
    {
        return assetCards.size();
    }

    @Step("Returning the pagination count object")
    public PaginationCountSubPage getPaginationCountPage()
    {
        return PageFactory.newInstance(PaginationCountSubPage.class);
    }

    @Step("Clicking load more button")
    public MyWorkSubPage clickLoadMoreButton()
    {
        loadMoreButton.click();
        return this;
    }

    @Step("Check that Load more button is displayed")
    public boolean isLoadMoreButtonDisplayed()
    {
        return loadMoreButton.isDisplayed();
    }

    @Step("Change how assets are sorted")
    public MyWorkSubPage changeAssetSort(String sort)
    {
        Select assetSort = new Select(driver.findElement(By.cssSelector("[data-ng-model='vm.sortOrder']")));
        assetSort.selectByVisibleText(sort);
        return PageFactory.newInstance(MyWorkSubPage.class);
    }

    @Step("Return job items")
    public List<MyWorkAssetJob> getJobItems()
    {
        return jobItems;
    }

    @Step("Return all case items")
    public List<MyWorkAssetCase> getCaseItems(){
        return caseItems;
    }
}
