package header.element;

import header.modal.CheckInModal;
import header.modal.DiscardCheckoutModal;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(css = "[data-ng-repeat='item in checkIn.list track by item.id']")
public class CheckinHeaderElement extends HtmlElement
{

    @Name("Asset Name")
    @FindBy(css = "[data-ng-bind='item.name']")
    private WebElement assetName;

    @Name("Check-In Button")
    @FindBy(css = "[data-ng-click='checkIn.checkIn(item);']")
    private WebElement checkinButton;

    @Name("Discard check out button")
    @FindBy(css = "[data-ng-click='checkIn.discard(item)']")
    private WebElement discardCheckoutButton;

    @Step("Returning the asset name")
    public String getAssetName()
    {
        return assetName.getText();
    }

    @Step("Clicking the Check-in button")
    public CheckInModal clickCheckinButton()
    {
        checkinButton.click();
        return PageFactory.newInstance(CheckInModal.class);
    }

    @Step("Clicking the Discard checkout button")
    public DiscardCheckoutModal clickDiscardCheckoutButton()
    {
        discardCheckoutButton.click();
        return PageFactory.newInstance(DiscardCheckoutModal.class);
    }
}
