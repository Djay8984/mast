package header.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.sub.assetmodel.ListViewSubPage;

public class CheckInModal extends BasePage<CheckInModal>
{

    @Name("Check-In Button")
    @FindBy(id = "check-in-modal-action")
    @Visible
    private WebElement checkinButton;

    @Step("Click Check-In button")
    public ListViewSubPage clickCheckinButton()
    {
        checkinButton.click();
        return PageFactory.newInstance(ListViewSubPage.class);
    }

}
