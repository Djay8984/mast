package header.modal;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class DiscardCheckoutModal extends BasePage<DiscardCheckoutModal>
{

    @Name("Discard Checkout Button")
    @FindBy(id = "discard-check-out-modal-action")
    @Visible
    private WebElement checkoutButton;

    @Step("Click Discard Checkout button")
    public <T extends BasePage<T>> T clickDiscardCheckoutButton(Class<T> pageObjectClass)
    {
        checkoutButton.click();
        return PageFactory.newInstance(pageObjectClass);
    }
}
