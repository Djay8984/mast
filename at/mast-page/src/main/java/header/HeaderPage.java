package header;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import configurationpage.ConfigurationPage;
import header.element.CheckinHeaderElement;
import helper.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import viewasset.assetheaderadditionalinformation.assetdetails.AssetDetailsPage;
import workhub.WorkHubPage;

import java.util.List;

public class HeaderPage extends BasePage<HeaderPage>
{

    @Name("MAST Logo")
    @FindBy(id = "logo-asset-header-logo")
    @Visible
    private WebElement mastLogoLink;

    @Name("MAST Text")
    @FindBy(id = "logo-asset-header-logo-text")
    @Visible
    private WebElement mastLogoText;

    @Name("Config Button")
    @FindBy(id = "config-icon")
    private WebElement configButton;

    @Name("Asset Tab in Header")
    @FindBy(css = ".application-job-container.grid-block.ng-scope")
    private List<WebElement> assetHeaderTab;

    @Name("View More Button")
    @FindBy(css = "[data-ui-sref^='asset.details']")
    private WebElement viewMoreButton;

    @Name("Current User Name")
    @FindBy(id = "info-header-username")
    private WebElement currentUser;

    @Name("Check-In Asset Elements")
    private List<CheckinHeaderElement> checkInAssetElements;

    public WorkHubPage clickMastLogo()
    {
        mastLogoLink.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logo-asset-header-logo")));
        waitForJavascriptFrameworkToFinish();
        return PageFactory.newInstance(WorkHubPage.class);
    }

    public WorkHubPage clickMastText()
    {
        mastLogoText.click();
        return PageFactory.newInstance(WorkHubPage.class);
    }

    public ConfigurationPage clickConfigPage()
    {
        configButton.click();
        return PageFactory.newInstance(ConfigurationPage.class);
    }

    @Step("Get current user from the header")
    public String getCurrentUser()
    {
        return currentUser.getText();
    }

    @Step("Check if Asset Header Tab is Present")
    public boolean isAssetHeaderTabDisplayed()
    {
        return assetHeaderTab.size() > 0;
    }

    @Step("Returning Check-In Asset Element")
    public CheckinHeaderElement getCheckinAssetElement(String assetName)
    {
        return checkInAssetElements.stream()
                .filter(cAE -> cAE.getAssetName().equals(assetName)).findFirst().orElse(null);
    }

    @Step("Returning the asset details page")
    public AssetDetailsPage getAssetDetailsPage()
    {
        return helper.PageFactory.newInstance(AssetDetailsPage.class);
    }

    @Step("Discarding checkout of asset for teardown")
    public void discardCheckoutForAllAssets()
    {
        clickMastLogo();
        checkInAssetElements
                .forEach(cAE -> cAE.clickDiscardCheckoutButton().clickDiscardCheckoutButton(WorkHubPage.class));
    }
}
