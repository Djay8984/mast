package configurationpage;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import header.HeaderPage;
import helper.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;

public class ConfigurationPage extends BasePage<ConfigurationPage>
{

    @Name("MAST groups input")
    @FindBy(id = "MAST_GROUPS")
    @Visible
    private WebElement mastGroupsInput;

    @Name("MAST user full name text")
    @FindBy(id = "MAST_USER_FULL_NAME")
    @Visible
    private WebElement userFullNameInput;

    @Name("Save config button")
    @FindBy(className = "primary-button")
    @Visible
    private WebElement saveConfigButton;

    public static void setMastGroup(ConfigurationPage configPage, String mastGroup)
    {
        configPage.setMastGroupsInput(mastGroup).then().clickSaveButton().then().getHeader().clickMastLogo();
    }

    @Step("Returning the header of the page")
    public HeaderPage getHeader()
    {
        return PageFactory.newInstance(HeaderPage.class);
    }

    public ConfigurationPage setMastGroupsInput(String group)
    {
        mastGroupsInput.clear();
        mastGroupsInput.sendKeys(group);
        return this;
    }

    public ConfigurationPage setMastUserFullName(String user)
    {
        userFullNameInput.clear();
        userFullNameInput.sendKeys(user);
        return this;
    }

    public ConfigurationPage clickSaveButton()
    {
        saveConfigButton.click();
        driver.navigate().back();
        wait.until(ExpectedConditions.visibilityOf(saveConfigButton));
        return PageFactory.newInstance(ConfigurationPage.class);
        //returns a new config page - do not "return this;"
    }
}
