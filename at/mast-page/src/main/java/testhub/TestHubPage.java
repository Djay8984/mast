package testhub;

import com.frameworkium.core.ui.pages.BasePage;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Name;
import typifiedelement.RadioButton;
import typifiedelement.TextArea;
import util.AppHelper;

public class TestHubPage extends BasePage<TestHubPage>
{

    @Name("Typified by text area")
    @FindBy(tagName = "textarea")
    private TextArea textArea;

    @Name("Typified by radio button")
    @FindBy(id = "radio_yes")
    private RadioButton yesRadioButton;

    @Name("Typified by radio button")
    @FindBy(css = "#radio_no")
    private RadioButton noRadioButton;

    @Step("Text Area - Chain moves works")
    public TestHubPage setTextAreaSizeByOffset(int xOffset, int yOffset)
    {
        new Actions(driver).sendKeys(Keys.END)
                .perform();
        textArea.setSizeByOffset(xOffset, yOffset);
        return this;
    }

    @Step("Text Area - Get size works")
    public Dimension getTextAreaDimension()
    {
        return textArea.getSize();
    }

    @Step("Text Area - Experimental - ignore this, NOTE that you need to unwrap the element to do " +
            "Locatable")
    public void move()
    {
        new Actions(driver).sendKeys(Keys.PAGE_UP)
                .sendKeys(Keys.PAGE_UP)
                .moveToElement(textArea.getWrappedElement())
                .perform();
    }

    @Step("Radio Button  - select")
    public TestHubPage selectYesRadioButton()
    {
        AppHelper.scrollToBottom();
        yesRadioButton.select();
        return this;
    }

    @Step("Radio Button - deselect")
    public TestHubPage selectNoRadioButton()
    {
        noRadioButton.select();
        return this;
    }

    @Step("Radio Button - isSelected")
    public boolean isYesRadioButtonSelected()
    {
        return yesRadioButton.isSelected();
    }
}



