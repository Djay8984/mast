package com.baesystems.ai.lloydsregister.facts;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;

public class FactCollectionTest
{

    @Test(expected = IllegalStateException.class)
    public void testAddFact_Exception()
    {
        Service service1 = new Service();
        Service service2 = new Service();

        Fact fact1 = new Fact("service", service1);
        Fact fact2 = new Fact("service", service2);

        FactCollection factCollection = new FactCollection();

        factCollection.addFact(fact1);
        factCollection.addFact(fact2);
    }

    @Test
    public void testGetFacts()
    {
        Service service = new Service();
        RenewalRelationship relationship = new RenewalRelationship();

        Fact fact1 = new Fact("service", service);
        Fact fact2 = new Fact("renewalRelationship", relationship);

        FactCollection factCollection = new FactCollection();

        factCollection.addFact(fact1);
        factCollection.addFact(fact2);

        List<Fact> facts = factCollection.getFacts();

        assertEquals(2, facts.size());
    }

}
