package com.baesystems.ai.lloydsregister.facts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.detica.ukdc.drools.service.model.Service;

public class FactTest
{
    private Service service;
    private Fact fact;

    @Before
    public void setUp()
    {
        service = new Service();

        fact = new Fact("service", service);
    }

    @Test
    public void testHasName()
    {
        assertTrue(fact.hasName());
    }

    @Test
    public void testGetFactName()
    {
        assertEquals("service", fact.getFactName());
    }

    @Test
    public void testGetFact()
    {
        assertTrue(fact.getFact() instanceof Service);
    }

}
