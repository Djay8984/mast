package com.baesystems.ai.lloydsregister.exception;

/**
 * Exception that should be throw whenever there
 * is an error is returned by the execution server
 * @author HMiah2
 *
 */
public class ExecutionServerException extends Exception
{

    public ExecutionServerException()
    {
        super();
    }

    public ExecutionServerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message,cause,enableSuppression,writableStackTrace);
    }

    public ExecutionServerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ExecutionServerException(String message)
    {
        super(message);
    }

    public ExecutionServerException(Throwable cause)
    {
        super(cause);
    }



}
