package com.baesystems.ai.lloydsregister.facts;

import java.util.Calendar;
import java.util.TimeZone;

import org.apache.camel.Message;
import org.apache.camel.PropertyInject;

/**
 * Abstract class that should be extended by all other FactAdder classes. Provides access to common fields and a
 * constructor that should be used when creating a FactAdder object. This class also specifies the
 * abstract method that needs to be implemented by all other subclasses.
 *
 * @author HMiah2
 *
 */
public abstract class AbstractFactAdder
{
    protected final String headerName;

    @PropertyInject(value = "time.zone.code")
    private String timeZoneCode;

    protected TimeZone timeZone;

    public AbstractFactAdder(final String headerName)
    {
        this.headerName = headerName;

        if (timeZoneCode == null || timeZoneCode.isEmpty() || timeZoneCode.equals(" "))
        {
            // The time zone code has not been set in the properties file,
            // we are going to set it to the system's time zone
            TimeZone systemTimeZone = Calendar.getInstance().getTimeZone();
            this.timeZoneCode = systemTimeZone.getID();
        }

        timeZone = TimeZone.getTimeZone(this.timeZoneCode);
    }

    /**
     * Abstract method that needs to implemented by all concrete
     * subclasses. This method will make the facts and add them to the collection
     * @param message The original camel message where the facts are stored in the
     * header
     */
    abstract void makeFacts(final Message message);

}
