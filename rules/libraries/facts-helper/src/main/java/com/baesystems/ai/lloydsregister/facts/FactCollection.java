package com.baesystems.ai.lloydsregister.facts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FactCollection
{
    private List<Fact> facts;
    private final Set<String> factNames;

    public FactCollection()
    {
        this.facts = new ArrayList<>();
        this.factNames = new HashSet<>();
    }

    public void addFact(final Fact fact)
    {
        if (fact.hasName() && factNames.contains(fact.getFactName()))
        {
            throw new IllegalStateException("Adding a fact with a name that already exists.");
        }

        this.facts.add(fact);

        if (fact.hasName())
        {
            this.factNames.add(fact.getFactName());
        }
    }

    public List<Fact> getFacts()
    {
        return new ArrayList<>(this.facts);
    }

    public void setFacts(List<Fact> facts)
    {
        this.facts = facts;
    }
}
