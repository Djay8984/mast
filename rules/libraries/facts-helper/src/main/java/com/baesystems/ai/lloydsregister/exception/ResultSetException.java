package com.baesystems.ai.lloydsregister.exception;

/**
 * Exception that should be thrown when a MySQL query returns no results or there are other errors with the results from
 * the database
 * 
 * @author Hussain
 *
 */
public class ResultSetException extends Exception
{
    private static final long serialVersionUID = 1L;

    public ResultSetException()
    {
        super();
    }

    public ResultSetException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ResultSetException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ResultSetException(String message)
    {
        super(message);
    }

    public ResultSetException(Throwable cause)
    {
        super(cause);
    }

}
