package com.baesystems.ai.lloydsregister.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Message;
import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.drools.core.command.runtime.rule.InsertObjectCommand;

import com.baesystems.ai.lloydsregister.facts.Fact;
import com.baesystems.ai.lloydsregister.facts.FactCollection;

public class CommandAssembler
{
    private final String factsHeaderName;
    private final String lookupHeaderName;

    public CommandAssembler(final String factsHeaderName, final String lookUpHeaderName)
    {
        this.factsHeaderName = factsHeaderName;
        this.lookupHeaderName = lookUpHeaderName;
    }

    public void setRequestBody(final Message message)
    {
        String lookUpName = message.getHeader(this.lookupHeaderName, String.class);
        final FactCollection factCollection = message.getHeader(this.factsHeaderName, FactCollection.class);
        final List<Fact> facts = factCollection.getFacts();

        // One command for each fact plus the FireRules command and dispose command.
        final List<GenericCommand<?>> commands = new ArrayList<>(facts.size() + 2);
        for (final Fact fact : facts)
        {
            final InsertObjectCommand insertCommand;
            if (fact.hasName())
            {
                insertCommand = new InsertObjectCommand(fact.getFact(), fact.getFactName());
            }
            else
            {
                insertCommand = new InsertObjectCommand(fact.getFact());
            }
            commands.add(insertCommand);
        }
        commands.add(new FireAllRulesCommand());

        final BatchExecutionCommandImpl execCommand = new BatchExecutionCommandImpl(commands, lookUpName);

        message.setBody(execCommand);
    }
}
