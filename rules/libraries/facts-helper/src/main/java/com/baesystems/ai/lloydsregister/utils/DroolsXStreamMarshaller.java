package com.baesystems.ai.lloydsregister.utils;

import org.kie.internal.runtime.helper.BatchExecutionHelper;

public class DroolsXStreamMarshaller
{
    public DroolsXStreamMarshaller()
    {
    }

    public String marshalToXml(final Object objectToMarshal)
    {
        return BatchExecutionHelper.newXStreamMarshaller().toXML(objectToMarshal);
    }

    public <T> T unmarshalFromXml(final Object objectToUnmarshal, Class<T> returnType)
    {
        return returnType.cast(BatchExecutionHelper.newXStreamMarshaller().fromXML((String) objectToUnmarshal));
    }
}
