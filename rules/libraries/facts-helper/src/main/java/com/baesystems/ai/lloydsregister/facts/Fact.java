package com.baesystems.ai.lloydsregister.facts;

public class Fact
{
    private final String factName;
    private final Object fact;

    public Fact(final Object fact)
    {
        this(null, fact);
    }

    public Fact(final String factName, final Object fact)
    {
        if (factName != null && factName.length() == 0)
        {
            throw new IllegalArgumentException("Zero length name provided for fact.");
        }
        this.factName = factName;
        this.fact = fact;
    }

    public boolean hasName()
    {
        return factName != null;
    }

    public String getFactName()
    {
        return this.factName;
    }

    public Object getFact()
    {
        return this.fact;
    }
}
