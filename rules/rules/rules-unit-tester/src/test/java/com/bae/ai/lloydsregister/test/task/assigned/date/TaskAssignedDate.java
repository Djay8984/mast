package com.bae.ai.lloydsregister.test.task.assigned.date;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.CertifiedTaskRelationship;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Unit test cases to test
 * the assigned date rule set for
 * task scheduling
 * @author HMiah2
 *
 */
public class TaskAssignedDate extends BaseTest
{

    @BeforeClass
    public static void startTest() throws Exception
    {
        String filePath = "./src/test/resources/task/assigned_date/task-assigned-date.xls";
        BaseTest.init(filePath);
    }

    @Test
    public void _09_01()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
        Task certifiedTask = new Task();
        Task associatedTask = new Task();

        certifiedTask.setTaskStatus("Complete");
        certifiedTask.setCompletionDate(new GregorianCalendar(2012, 01, 01));

        certifiedTaskRelationship.setCertifiedTask(certifiedTask);
        certifiedTaskRelationship.setAssociatedTask(associatedTask);

        kieSession.insert(certifiedTaskRelationship);

        kieSession.fireAllRules();
        kieSession.dispose();

        assertTrue(Functions.DatesAreEqual(new GregorianCalendar(2012, 01, 01), certifiedTaskRelationship.getAssociatedTask().getAssignedDate()));
    }

    @Test
    public void _09_02()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();

        certifiedTaskRelationship.setAssociatedTask(new Task());

        kieSession.insert(certifiedTaskRelationship);

        kieSession.fireAllRules();
        kieSession.dispose();

        assertNull(certifiedTaskRelationship.getAssociatedTask().getAssignedDate());
    }

}
