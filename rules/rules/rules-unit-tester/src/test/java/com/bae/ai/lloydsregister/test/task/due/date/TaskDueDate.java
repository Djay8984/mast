package com.bae.ai.lloydsregister.test.task.due.date;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.CertifiedTaskRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Unit test cases for testing
 * the due date rule set for task scheduling
 * @author HMiah2
 *
 */
public class TaskDueDate extends BaseTest
{
    @BeforeClass
    public static void startTest() throws Exception
    {
        String filePath = "./src/test/resources/task/due_date/task-due-date.xls";
        BaseTest.init(filePath);
    }

    @Test
    public void _10_01()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
        Task associatedTask = new Task();
        Service service = new Service();

        service.setServiceDueDate(new GregorianCalendar(2001, 01, 01));
        service.setServiceCatalogue(new ServiceCatalogue());

        associatedTask.setService(service);

        certifiedTaskRelationship.setAssociatedTask(associatedTask);

        kieSession.insert(certifiedTaskRelationship);
        kieSession.fireAllRules();
        kieSession.dispose();

        Calendar expected = new GregorianCalendar(2001, 01, 01);

        assertTrue(Functions.DatesAreEqual(expected, associatedTask.getDueDate()));

    }

    @Test
    public void _10_02()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
        Task certifiedTask = new Task();
        Task associatedTask = new Task();
        Service associcatedTaskService = new Service();
        ServiceCatalogue associcatedTaskServiceCatalogue = new ServiceCatalogue();

        associatedTask.setAssignedDate(new GregorianCalendar(2001, 01, 01));

        associcatedTaskServiceCatalogue.setContinuousIndicator(true);
        associcatedTaskServiceCatalogue.setCyclePeriodicity(0);

        associcatedTaskService.setServiceCatalogue(associcatedTaskServiceCatalogue);

        associatedTask.setService(associcatedTaskService);

        certifiedTaskRelationship.setCertifiedTask(certifiedTask);
        certifiedTaskRelationship.setAssociatedTask(associatedTask);

        kieSession.insert(certifiedTaskRelationship);
        kieSession.fireAllRules();

        Calendar expected = new GregorianCalendar(2001, 01, 01);

        assertTrue(Functions.DatesAreEqual(expected, associatedTask.getDueDate()));
    }

    @Test
    public void _10_04()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
        Task certifiedTask = new Task();
        Task associatedTask = new Task();
        Service associatedTaskService = new Service();
        ServiceCatalogue associatedTaskServiceCatalogue = new ServiceCatalogue();

        associatedTask.setAssignedDate(new GregorianCalendar(2001, 01, 01));

        associatedTaskServiceCatalogue.setContinuousIndicator(false);

        associatedTaskService.setServiceCatalogue(associatedTaskServiceCatalogue);

        associatedTask.setService(associatedTaskService);

        certifiedTaskRelationship.setCertifiedTask(certifiedTask);
        certifiedTaskRelationship.setAssociatedTask(associatedTask);

        kieSession.insert(certifiedTaskRelationship);
        kieSession.fireAllRules();
        kieSession.dispose();

        assertNull(associatedTask.getDueDate());
    }

    @Test
    public void _10_05()
    {
        KieSession kieSession = kieContainer.newKieSession();

        CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
        Task certifiedTask = new Task();
        Task associatedTask = new Task();
        Service associatedTaskService = new Service();
        ServiceCatalogue associcatedTaskServiceCatalogue = new ServiceCatalogue();

        associatedTask.setAssignedDate(new GregorianCalendar(2001, 01, 01));

        associatedTaskService.setServiceDueDate(new GregorianCalendar(2001, 01, 01));
        associcatedTaskServiceCatalogue.setContinuousIndicator(false);
        associatedTaskService.setServiceCatalogue(associcatedTaskServiceCatalogue);

        associatedTask.setService(associatedTaskService);

        certifiedTaskRelationship.setCertifiedTask(certifiedTask);
        certifiedTaskRelationship.setAssociatedTask(associatedTask);

        kieSession.insert(certifiedTaskRelationship);
        kieSession.fireAllRules();
        kieSession.dispose();

        Calendar expected = new GregorianCalendar(2001, 01, 01);

        assertTrue(Functions.DatesAreEqual(expected, associatedTask.getDueDate()));
    }
}
