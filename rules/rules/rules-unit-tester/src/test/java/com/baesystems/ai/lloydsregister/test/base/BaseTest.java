package com.baesystems.ai.lloydsregister.test.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.junit.AfterClass;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.Message;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.io.ResourceFactory;

public class BaseTest
{
    protected static KieContainer kieContainer;

    public static void init(String filePath) throws Exception
    {
        FileInputStream fis = new FileInputStream(filePath);

        SpreadsheetCompiler compiler = new SpreadsheetCompiler();

        String drl = compiler.compile(fis, InputType.XLS);

        File drlFile = _saveDRLFile(drl);

        KieServices ks = KieServices.Factory.get();
        KieRepository kr = ks.getRepository();
        KieFileSystem kfs = ks.newKieFileSystem();

        kfs.write(ResourceFactory.newFileResource(drlFile));

        KieBuilder kb = ks.newKieBuilder(kfs);

        kb.buildAll();

        if (kb.getResults().hasMessages(Message.Level.ERROR))
        {
            throw new Exception("Build Errors: " + kb.getResults().toString());
        }

        kieContainer = ks.newKieContainer(kr.getDefaultReleaseId());
    }

    private static File _saveDRLFile(String drl) throws Exception
    {
        File workingDir = new File("./working");

        // The working directory does not exist - make the working directory
        if (!workingDir.exists())
        {
            workingDir.mkdirs();
        }

        File drlFile = new File(workingDir, "drlFile.drl");

        FileUtils.writeStringToFile(drlFile, drl, Charset.defaultCharset());

        if (drlFile.exists() && FileUtils.sizeOf(drlFile) > 0)
        {
            System.out.println("The DRL file has been saved");
            return drlFile;
        }

        throw new IOException("There was an error when trying to save the DRL file");
    }

    public StatelessKieSession getSession(final String sessionName)
    {
        StatelessKieSession kSession = null;
        try
        {
            KieServices kServices = KieServices.Factory.get();
            KieContainer kContainer = kServices.getKieClasspathContainer();
            kSession = kContainer.newStatelessKieSession(sessionName);

            StringBuilder logFileBldr = new StringBuilder("./logs/").append("wassup");

            KieRuntimeLogger logger = kServices.getLoggers().newFileLogger(kSession, logFileBldr.toString());
        }
        catch (Exception ex)
        {
            throw new IllegalStateException("Could not get session", ex);
        }

        return kSession;
    }

    @AfterClass
    public static void cleanUp()
    {
        FileUtils.deleteQuietly(new File("./working"));
    }
}
