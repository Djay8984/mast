package com.baesystems.ai.lloydsregister.test.due.date;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.CertifiedRelationship;
import com.detica.ukdc.drools.service.model.HullSurvey;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

public class TestDueDate extends BaseTest
{

    @Test
    public void _05_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setSchedulingTypeDue("None");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getServiceDueDate());
    }

    @Test
    public void _05_02_01_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));
        asset.setClassStatus("In Class (Laid Up)");

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        hullServiceCatalogue.setCyclePeriodicity(0);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_01_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(0);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2019, 07, 24);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_01_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullServiceCatalogue.setCyclePeriodicity(0);
        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_01_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2019, 07, 24);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_01_05()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(42);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(0);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 01, 24);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_01_06()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("DS");

        asset.setHarmonisationDate(Calendar.getInstance());
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullServiceCatalogue.setCyclePeriodicity(0);
        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 24);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2018, 9, 17));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(1999, 0, 1));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2021, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_02_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2017, 00, 01));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2019, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_02_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(1999, 00, 01));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2001, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_02_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(3030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("PASS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(3032, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_03_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();
        HullSurvey hullSurvey = new HullSurvey();

        Calendar certifiedServiceAssignDate = new GregorianCalendar(2010, 01, 02, 11, 11);
        Calendar assetHarmonisationDate = new GregorianCalendar(2010, 02, 02, 11, 11);
        Calendar serviceAssignedDate = new GregorianCalendar(2012, 03, 02, 11, 11);

        serviceCatalogue.setCyclePeriodicity(36);
        service.setAssignedDate(serviceAssignedDate);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("DS");

        asset.setHarmonisationDate(assetHarmonisationDate);
        asset.setClassStatus("Random");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        hullServiceCatalogue.setCyclePeriodicity(60);
        hullServiceCatalogue.setContinuousIndicator(false);
        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedServiceCatalogue.setContinuousIndicator(false);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(certifiedServiceAssignDate);
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2015, 02, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_03_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("DS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2021, 00, 01));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2019, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_03_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(36);
        service.setAssignedDate(new GregorianCalendar(1990, 00, 01));

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("DS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1992, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_02_03_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service hullService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        HullSurvey hullSurvey = new HullSurvey();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(3030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(36);

        serviceCatalogue.setSchedulingTypeDue("2 Surveys Due In Special Cycle");
        serviceCatalogue.setServiceCode("DS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        asset.setClassStatus("Class");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        hullServiceCatalogue.setCyclePeriodicity(60);

        hullServiceCatalogue.setContinuousIndicator(false);

        hullService.setServiceCatalogue(hullServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        hullSurvey.setAssociatedService(service);
        hullSurvey.setHullSurvey(hullService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(hullSurvey, "hull"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(3032, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_01_01_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        service.setServiceDueDate(null);
        service.setAssignedDate(null);
        serviceCatalogue.setCyclePeriodicity(3);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Full");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 10, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_01_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setServiceDueDate(null);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(3);
        serviceCatalogue.setLowerRangePeriod(3);
        serviceCatalogue.setServiceType("Annual");

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Full");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceCatalogue(serviceCatalogue);
        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_01_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setServiceDueDate(null);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(3);
        serviceCatalogue.setLowerRangePeriod(3);
        serviceCatalogue.setServiceType("Annual");

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Full");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceCatalogue(serviceCatalogue);
        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_01_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setServiceDueDate(null);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(3);
        serviceCatalogue.setLowerRangePeriod(3);
        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setProductFamilyName("Classification");
        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Full");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceCatalogue(serviceCatalogue);
        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2017, 07, 24));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 07, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_01_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setServiceDueDate(null);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(3);
        serviceCatalogue.setLowerRangePeriod(3);
        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setProductFamilyName("Statutory");
        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Full");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceCatalogue(serviceCatalogue);
        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2017, 07, 24));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 07, 24);

        assertNull(returnedService.getServiceDueDate());
    }

    @Test
    public void _05_03_02_01_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalServiceCatalogue.setCyclePeriodicity(0);

        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(2);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 00, 01));
        renewalService.setAssignedDate(new GregorianCalendar(2016, 00, 01));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 00, 02);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(2);
        serviceCatalogue.setCyclePeriodicity(12);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalRelationship.setAssociatedService(service);

        certifiedServiceCatalogue.setServiceType("Annual");
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(2);
        serviceCatalogue.setCyclePeriodicity(12);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalRelationship.setAssociatedService(service);

        certifiedServiceCatalogue.setServiceType("Renewal");
        certifiedServiceCatalogue.setCyclePeriodicity(33);
        certifiedService.setLowerRangeDate(new GregorianCalendar(2015, 7, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setUpperRangeDate(new GregorianCalendar(2017, 7, 25));
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 7, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_03(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setAssignedDate(new GregorianCalendar(2016, 4, 1));
        service.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCompletionDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(2010, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship,"renewal"));
        commands.add(commandFactory.newInsert(certifiedRelationship,"creditedRelationship"));

        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 4, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_04(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        service.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(48);
        creditedService.setCompletionDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(2010, 1, 1));

        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(creditedRelationship, "credited"));

        final BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecutionCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2010, 1, 2);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_05(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        service.setAssignedDate(new GregorianCalendar(2015, 1, 1));
        service.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(48);
        creditedService.setCompletionDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(creditedRelationship, "credited"));

        final BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecutionCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2015, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_06(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(72);
        creditedService.setCompletionDate(new GregorianCalendar(2010, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(2016, 1, 1));

        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(creditedRelationship, "credited"));

        final BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecutionCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 1, 2);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_07(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setAssignedDate(new GregorianCalendar(2016, 1, 1));
        service.setCyclePeriodicity(72);

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(72);
        creditedService.setCompletionDate(new GregorianCalendar(2016, 9, 9));
        creditedService.setServiceDueDate(new GregorianCalendar(1998, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(creditedRelationship, "credited"));

        final BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecutionCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2022, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_02_08(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        service.setAssignedDate(new GregorianCalendar(2016, 1, 1));
        service.setCyclePeriodicity(20);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setServiceCatalogue(serviceCatalogue);

        renewalRelationship.setAssociatedService(service);

        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCompletionDate(new GregorianCalendar(2011, 1, 1));
        creditedService.setUpperRangeDate(new GregorianCalendar(2000, 1, 1));
        creditedService.setCyclePeriodicity(33);

        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        commands.add(commandFactory.newInsert(creditedRelationship, "credited"));

        final BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecutionCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 9, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2015, 8, 25));

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setProductFamilyName("Classification");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setAssignedDate(new GregorianCalendar(2015, 8, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_03_02_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2015, 8, 25));

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setAssignedDate(new GregorianCalendar(2015, 8, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(service.getServiceDueDate());
    }

    @Test
    public void _05_03_03_01_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("None");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_03_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(10);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("None");
        serviceCatalogue.setLowerRangePeriod(2);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceDueDate(new GregorianCalendar(2030, 00, 01));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_03_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(10);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("None");
        serviceCatalogue.setLowerRangePeriod(2);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 5, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_03_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Classification");

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("None");
        serviceCatalogue.setLowerRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 04, 17));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_03_03_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("None");

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Annual");
        serviceCatalogue.setHarmonisationType("None");
        serviceCatalogue.setLowerRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 04, 17));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }


    @Test
    public void _05_04_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(1999, 01, 01));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Lifting Annual");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_04_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(3030, 0, 2));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Lifting Annual");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        renewalService.setServiceDueDate(new GregorianCalendar(3030, 0, 1));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(3030, 0, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_04_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(3030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Lifting Annual");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(3030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_05()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Assigned Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(1999, 00, 01));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1998, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(1999, 00, 01));
        serviceCatalogue.setCyclePeriodicity(0);
        service.setAsset(asset);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1998, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(48);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 7, 25);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_02_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(72);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(1999, 01, 01));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2022, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_02_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(1999, 01, 01));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_03_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(3030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(60);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setServiceDueDate(new GregorianCalendar(3027, 03, 01));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(3034, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_06_02_03_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(72);

        serviceCatalogue.setSchedulingTypeDue("Assigned Or Due Date");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2035, 11, 31);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_01_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("Control");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getServiceDueDate());
    }

    @Test
    public void _05_07_01_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(null);
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalServiceCatalogue.setCyclePeriodicity(0);

        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_01_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(null);
        serviceCatalogue.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("None");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_01_07()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setServiceDueDate(null);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setCyclePeriodicity(3);
        serviceCatalogue.setHarmonisationType("None");

        serviceCatalogue.setSchedulingTypeDue("Periodic");

        asset.setHarmonisationDate(Calendar.getInstance());

        certifiedServiceCatalogue.setServiceType("Initial");

        certifiedService.setServiceStatus("Complete");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);
        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 10, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_01_07_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingTypeDue("Harmonised Intermediate");
        serviceCatalogue.setHarmonisationType("Full");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 1, 1));
        service.setCyclePeriodicity(48);
        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(new Asset());

        creditedService.setServiceStatus("Complete");
        creditedService.setCompletionDate(Calendar.getInstance());
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));

    }

    @Test
    public void _05_07_03_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 10, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedServiceCatalogue.setServiceType("Intermediate");

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 10, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 10, 25));

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 10, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_03_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 10, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 10, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 10, 25));

        certifiedServiceCatalogue.setServiceType("Intermediate");

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(service.getServiceDueDate());
    }

    @Test
    public void _05_07_04_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Classification");

        service.setAssignedDate(new GregorianCalendar(2016, 10, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedServiceCatalogue.setServiceType("Annual");

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(service.getServiceDueDate());
    }

    @Test
    public void _05_07_04_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 10, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("Control");
        serviceCatalogue.setLowerRangePeriod(0);

        asset.setHarmonisationDate(new GregorianCalendar(2016, 10, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");

        certifiedServiceCatalogue.setServiceType("Annual");

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 10, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 10, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");

        certifiedServiceCatalogue.setServiceType("Annual");

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 10, 25));
        renewalService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2017, 10, 26);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));
        service.setCyclePeriodicity(0);

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");

        certifiedServiceCatalogue.setServiceType("Renewal");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2015, 7, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 7, 25));
        certifiedService.setUpperRangeDate(new GregorianCalendar(2017, 7, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 7, 25));
        renewalService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_02(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setAssignedDate(new GregorianCalendar(1990, 1, 1));
        service.setCyclePeriodicity(72);

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCompletionDate(new GregorianCalendar(2001, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(2010, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1996, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));

    }


    @Test
    public void _05_07_04_02_02_03(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setCyclePeriodicity(48);
        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(48);
        creditedService.setCompletionDate(new GregorianCalendar(2001, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(1999, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);


        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 1, 2);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_04(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setSchedulingTypeDue("Class Non Harmonised / SCI Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 1, 1));
        service.setCyclePeriodicity(48);
        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(48);
        creditedService.setCompletionDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(1993, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_05(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setCyclePeriodicity(72);
        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(72);
        creditedService.setCompletionDate(new GregorianCalendar(2001, 1, 1));
        creditedService.setLowerRangeDate(new GregorianCalendar(1999, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2022, 1, 2);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_06(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(1999, 1, 1));
        service.setCyclePeriodicity(72);
        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(72);
        creditedService.setCompletionDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setServiceDueDate(new GregorianCalendar(1990, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2005, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_02_07(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setSchedulingTypeDue("Standard Statutory Intermediate");
        serviceCatalogue.setHarmonisationType("Partial");
        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 1, 1));
        service.setCyclePeriodicity(48);

        service.setServiceCatalogue(serviceCatalogue);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCyclePeriodicity(0);
        creditedService.setCompletionDate(new GregorianCalendar(2016, 1, 1));
        creditedService.setUpperRangeDate(new GregorianCalendar(2015, 1, 1));
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        renewalRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2020, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue certifiedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setProductFamilyName("Statutory");

        service.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        serviceCatalogue.setSchedulingTypeDue("Class Harmonised / HSSC Intermediate");
        serviceCatalogue.setHarmonisationType("None");
        serviceCatalogue.setLowerRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");

        certifiedServiceCatalogue.setServiceType("Annual");

        certifiedService.setServiceCatalogue(certifiedServiceCatalogue);

        renewalService.setAssignedDate(new GregorianCalendar(2016, 7, 25));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 25);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_07_04_02_04(){
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service creditedService = new Service();
        Asset asset = new Asset();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue creditedServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship creditedRelationship = new CertifiedRelationship();

        service.setAssignedDate(new GregorianCalendar(2001, 1, 1));
        asset.setHarmonisationDate(new GregorianCalendar(2009, 1, 1));
        service.setCyclePeriodicity(48);

        serviceCatalogue.setSchedulingTypeDue("Harmonised Intermediate");
        serviceCatalogue.setHarmonisationType("Full");
        serviceCatalogue.setProductFamilyName("Classification");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        creditedService.setServiceStatus("Complete");
        creditedServiceCatalogue.setServiceType("Renewal");
        creditedService.setCompletionDate(Calendar.getInstance());
        creditedService.setServiceCatalogue(creditedServiceCatalogue);

        creditedRelationship.setAssociatedService(service);
        creditedRelationship.setCertifiedService(creditedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(creditedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2013, 1, 1);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_08_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        Calendar assetHarmonisationDate = new GregorianCalendar(2010, 01, 01, 11, 11);

        serviceCatalogue.setCyclePeriodicity(3);

        serviceCatalogue.setSchedulingTypeDue("Harmonised Renewal");

        asset.setHarmonisationDate(assetHarmonisationDate);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2010, 03, 30);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_08_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        Calendar certifiedServiceCompletionDate = new GregorianCalendar(2009, 05, 01, 11, 11);
        Calendar assetHarmonisationDate = new GregorianCalendar(2010, 01, 01, 11, 11);

        service.setServiceDueDate(null);
        serviceCatalogue.setCyclePeriodicity(3);

        serviceCatalogue.setSchedulingTypeDue("Harmonised Renewal");

        asset.setHarmonisationDate(assetHarmonisationDate);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(certifiedServiceCompletionDate);
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2010, 03, 30);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_08_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        Calendar certifiedServiceCompletionDate = new GregorianCalendar(2011, 01, 01, 11, 11);
        Calendar assetHarmonisationDate = new GregorianCalendar(2010, 01, 01, 11, 11);

        service.setServiceDueDate(null);
        serviceCatalogue.setCyclePeriodicity(3);

        serviceCatalogue.setSchedulingTypeDue("Harmonised Renewal");

        asset.setHarmonisationDate(assetHarmonisationDate);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(certifiedServiceCompletionDate);
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2010, 03, 30);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_09_01()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 25));
        serviceCatalogue.setSchedulingTypeDue("HSSC 1 Year Renewal");
        serviceCatalogue.setCyclePeriodicity(0);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_09_02()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setSchedulingTypeDue("HSSC 1 Year Renewal");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 24));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 7, 24));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getServiceDueDate()));
    }

    @Test
    public void _05_09_03()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        Calendar certifiedServiceCompletionDate = new GregorianCalendar(2012, 01, 01);
        Calendar certifiedServiceLowerRangeDate = new GregorianCalendar(2012, 01, 01);
        Calendar serviceAssignDate = new GregorianCalendar(2012, 01, 01, 11, 11);

        service.setServiceDueDate(null);
        service.setAssignedDate(serviceAssignDate);
        serviceCatalogue.setCyclePeriodicity(3);
        service.setAsset(new Asset());

        serviceCatalogue.setSchedulingTypeDue("HSSC 1 Year Renewal");

        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(certifiedServiceCompletionDate);
        certifiedService.setLowerRangeDate(certifiedServiceLowerRangeDate);
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2012, 03, 30);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_09_04()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setAssignedDate(new GregorianCalendar(2016, 7, 24));

        serviceCatalogue.setSchedulingTypeDue("HSSC 1 Year Renewal");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 7, 24));
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 7, 24));
        certifiedService.setServiceCatalogue(new ServiceCatalogue());

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 23);
        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

    @Test
    public void _05_10()
    {
        StatelessKieSession kieSession = getSession("serviceduedate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);
        service.setAssignedDate(new GregorianCalendar(2016, 7, 24));

        serviceCatalogue.setSchedulingTypeDue("Laker Docking Survey");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 7, 24));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 7, 23);

        assertTrue(Functions.DatesAreEqual(expected, service.getServiceDueDate()));
    }

}
