package com.baesystems.ai.lloydsregister.test.assigned.date;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.CertifiedRelationship;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import java.util.ArrayList;
import java.util.Collection;
import org.kie.api.definition.KiePackage;
import java.util.List;

public class TestAssignedDate extends BaseTest
{

    @Test
    public void _04_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        service.setSchedulingTypeAssign("None");

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getAssignedDate());
    }

    @Test
    public void _04_02_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingRegime("MMS");

        service.setSchedulingTypeAssign("Current, Previous Or Next Harmonisation Date");

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getAssignedDate());
    }

    @Test
    public void _04_02_02_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        serviceCatalogue.setSchedulingRegime("Test");

        service.setSchedulingTypeAssign("Current, Previous Or Next Harmonisation Date");
        serviceCatalogue.setServiceType("Annual");

        service.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getAssignedDate());
    }

    @Test
    public void _04_02_02_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setSchedulingRegime("Standard");
        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceType("Renewal");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(new Service());

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_03_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Current, Previous Or Next Harmonisation Date");
        serviceCatalogue.setLowerRangePeriod(1);

        certifiedService.setServiceStatus("Complete");
        Calendar completionDate = new GregorianCalendar(2030, 01, 01, 12, 12);
        certifiedService.setCompletionDate(completionDate);

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2021, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_03_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        service.setSchedulingTypeAssign("Current, Previous Or Next Harmonisation Date");
        serviceCatalogue.setLowerRangePeriod(0);

        asset.setHarmonisationDate(new GregorianCalendar(2030, 01, 01));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 01, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_04()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        Calendar completionDate = new GregorianCalendar(2016, 07, 25);

        service.setSchedulingTypeAssign("Date On Completion");

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(completionDate);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_05_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Date On Completion Or Due Date");
        serviceCatalogue.setLowerRangePeriod(0);

        serviceCatalogue.setCyclePeriodicity(3);
        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(1990, 01, 01));

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setCertifiedService(certifiedService);
        certifiedRelationship.setAssociatedService(service);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1990, 01, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_05_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Date On Completion Or Due Date");
        serviceCatalogue.setLowerRangePeriod(0);

        certifiedService.setServiceStatus("Complete");
        serviceCatalogue.setCyclePeriodicity(2);
        certifiedService.setLowerRangeDate(new GregorianCalendar(1800, 01, 01));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(1900, 01, 01));

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_05_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Date On Completion Or Due Date");
        serviceCatalogue.setLowerRangePeriod(0);

        certifiedService.setServiceStatus("Complete");
        serviceCatalogue.setCyclePeriodicity(2);
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2021, 00, 01));

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2021, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_06()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        service.setSchedulingTypeAssign("Harmonisation Date");
        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_01_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(60);

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_01_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Asset asset = new Asset();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("AS");

        serviceCatalogue.setCyclePeriodicity(0);

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1990, 00, 01));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_01_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Asset asset = new Asset();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setCyclePeriodicity(0);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 07, 24));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_02_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(60);

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_02_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Asset asset = new Asset();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("AS");

        asset.setHarmonisationDate(new GregorianCalendar(1990, 00, 01));

        serviceCatalogue.setCyclePeriodicity(0);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_07_02_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Asset asset = new Asset();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(1990, 00, 01));

        serviceCatalogue.setCyclePeriodicity(0);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_08_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("HSSC 1 Year Renewal");

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2016, 07, 24));

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 24);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_08_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Calendar dueDate = new GregorianCalendar(2016, 07, 25);
        Calendar lowerRangeDate = new GregorianCalendar(2009, 01, 01, 11, 11);
        Calendar completionDate = new GregorianCalendar(2016, 07, 25);
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("HSSC 1 Year Renewal");

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(lowerRangeDate);
        certifiedService.setServiceDueDate(dueDate);
        certifiedService.setCompletionDate(completionDate);

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_08_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        service.setSchedulingTypeAssign("HSSC 1 Year Renewal");

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2050, 00, 01));

        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2049, 07, 26);

        assertTrue(Functions.DatesAreEqual(expected, service.getAssignedDate()));
    }

    @Test
    public void _04_09_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        service.setSchedulingTypeAssign("Non Class Date On Completion Or Due Date");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1999, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_09_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        service.setSchedulingTypeAssign("Non Class Date On Completion Or Due Date");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2010, 00, 00));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 26);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_09_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        service.setSchedulingTypeAssign("Non Class Date On Completion Or Due Date");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_09_04()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(0);

        service.setSchedulingTypeAssign("Non Class Date On Completion Or Due Date");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2030, 00, 01));

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2030, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_01_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(60);

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);
        certifiedService.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_01_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("AS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setCyclePeriodicity(0);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1990, 00, 01));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));
        certifiedService.setServiceCatalogue(serviceCatalogue);

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_01_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setCyclePeriodicity(0);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(1990, 00, 01));
        certifiedService.setAssignedDate(new GregorianCalendar(2016, 07, 25));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 07, 25);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_02_01()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(60);

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2040, 00, 01));

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2040, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_02_02()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("AS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setCyclePeriodicity(0);
        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2050, 00, 01));

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2050, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

    @Test
    public void _04_10_02_03()
    {
        StatelessKieSession kieSession = getSession("assigneddate");

        Service service = new Service();
        Service certifiedService = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        service.setSchedulingTypeAssign("Partial / Non Harmonised Intermediate");
        serviceCatalogue.setServiceCode("SS");

        asset.setHarmonisationDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setCyclePeriodicity(0);
        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        certifiedService.setServiceStatus("Complete");
        certifiedService.setCompletionDate(new GregorianCalendar(2050, 00, 01));

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        certifiedRelationship.setAssociatedService(service);
        certifiedRelationship.setCertifiedService(certifiedService);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(certifiedRelationship, "certified"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2050, 00, 01);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getAssignedDate()));
    }

}
