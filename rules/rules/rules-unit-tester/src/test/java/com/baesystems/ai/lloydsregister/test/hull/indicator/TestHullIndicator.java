package com.baesystems.ai.lloydsregister.test.hull.indicator;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import java.util.ArrayList;
import java.util.List;

public class TestHullIndicator extends BaseTest
{

    @Test
    public void _08_00_01()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setServiceCode("AS");

        asset.setHullIndicator(Integer.valueOf(2).byteValue());

        service.setServiceCatalogue(serviceCatalogue);
        service.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("2"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_00_02()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setServiceCode("SS");

        asset.setHullIndicator(Integer.valueOf(2).byteValue());

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);
        service.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("2"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_00_03()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        service.setServiceDueDate(null);

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("2"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("2"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_01_01()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(1990, 00, 01, 11, 11));

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("0"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("1"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_01_02()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(1990, 00, 01, 11, 11));

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setCommissionDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("0"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("1"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_02_01()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2017, 8, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("1"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_02_02()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2017, 8, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setCommissionDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("1"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_03_01()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2017, 11, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("2"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_03_02()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2017, 11, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setCommissionDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("2"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_04_01()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2020, 11, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("4"), returnedService.getAsset().getHullIndicator());
    }

    @Test
    public void _08_01_04_02()
    {
        StatelessKieSession kieSession = getSession("assethullindicator");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        Asset asset = new Asset();

        serviceCatalogue.setCyclePeriodicity(3);
        service.setServiceDueDate(new GregorianCalendar(2020, 11, 10));

        serviceCatalogue.setServiceCode("SS");

        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));
        asset.setCommissionDate(new GregorianCalendar(2016, 07, 25));
        asset.setHullIndicator(new Byte("1"));

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(new Byte("4"), returnedService.getAsset().getHullIndicator());
    }

}
