package com.baesystems.ai.lloydsregister.test.service.dates;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

public class TestUpperRangeDate extends BaseTest
{

    @Test
    public void _07_01()
    {
        Service service = new Service();

        service.setServiceCatalogue(new ServiceCatalogue());

        StatelessKieSession kieSession = getSession("upperrangedate");

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getUpperRangeDate());
    }

    @Test
    public void _07_02_01()
    {
        StatelessKieSession kieSession = getSession("upperrangedate");
        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        service.setServiceDueDate(Calendar.getInstance());

        serviceCatalogue.setUpperRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = service.getServiceDueDate();

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getUpperRangeDate()));
    }

    @Test
    public void _07_02_02()
    {
        StatelessKieSession kieSession = getSession("upperrangedate");
        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setUpperRangePeriod(1);
        serviceCatalogue.setProductFamilyName("Classification");
        service.setServiceDueDate(Calendar.getInstance());
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(service.getServiceDueDate());

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = service.getServiceDueDate();
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getUpperRangeDate()));
    }

    @Test
    public void _07_02_03_01()
    {
        StatelessKieSession kieSession = getSession("upperrangedate");
        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setUpperRangePeriod(1);
        serviceCatalogue.setProductFamilyName("Classification");
        service.setServiceDueDate(new GregorianCalendar(2016, 06, 01));
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(1999, 01, 01));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(2);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 06, 31);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getUpperRangeDate()));
    }

    @Test
    public void _07_02_03_02()
    {
        StatelessKieSession kieSession = getSession("upperrangedate");
        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        serviceCatalogue.setProductFamilyName("Test");

        service.setServiceDueDate(new GregorianCalendar(1990, 00, 01));

        serviceCatalogue.setUpperRangePeriod(1);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1990, 00, 31);

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getUpperRangeDate()));
    }

    @Test
    public void _07_02_03_03()
    {
        StatelessKieSession kieSession = getSession("upperrangedate");
        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setUpperRangePeriod(1);
        service.setServiceDueDate(new GregorianCalendar(2016, 06, 01));

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(2016, 06, 31);
        assertTrue(Functions.DatesAreEqual(expected, service.getUpperRangeDate()));
    }

}
