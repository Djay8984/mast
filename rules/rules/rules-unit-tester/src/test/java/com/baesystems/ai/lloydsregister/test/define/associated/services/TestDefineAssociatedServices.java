package com.baesystems.ai.lloydsregister.test.define.associated.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.CodeFamily;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

public class TestDefineAssociatedServices extends BaseTest
{

    @Test
    public void _03_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Not Started");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.01";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_04_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Finished");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-B - Core Hull Cycle (General)");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(3, associatedService.size());

        String key = "03.04.01.01";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.01.02";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.01.03";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }
    
    @Test
    public void _03_04_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();
        asset.setIwwCoastalIndicator(Boolean.TRUE);

        codeFamily.setAnnual("AS");
        codeFamily.setPeriodic("ITSS");

        service.setServiceStatus("Finished");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-D - Core Hull Cycle (IWW)");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(3, associatedService.size());

        String key = "03.04.02.01.01";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.02.01.02";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.02.01.03";
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }
    
    @Test
    public void _03_04_02_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();
        asset.setIwwCoastalIndicator(Boolean.FALSE);

        codeFamily.setAnnual("AS");
        codeFamily.setPeriodic("ITSS");

        service.setServiceStatus("Finished");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-D - Core Hull Cycle (IWW)");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(3, associatedService.size());

        String key = "03.04.02.02.01";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.02.02.02";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.02.02.03";
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }
    
    @Test
    public void _03_04_03_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();
        asset.setSscPrivateIndicator(Boolean.TRUE);

        codeFamily.setAnnual("AS");
        codeFamily.setPeriodic("ITSS");

        service.setServiceStatus("Finished");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-E - Core Hull Cycle (SSC)");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(3, associatedService.size());

        String key = "03.04.03.01.01";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.03.01.02";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.03.01.03";
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }
    
    @Test
    public void _03_04_03_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();
        asset.setSscPrivateIndicator(Boolean.FALSE);

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");

        service.setServiceStatus("Finished");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-E - Core Hull Cycle (SSC)");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertEquals(3, associatedService.size());

        String key = "03.04.03.02.01";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.03.02.02";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        key = "03.04.03.02.03";
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setRenewal("AS");
        codeFamily.setIntermediate("PASS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("ST-E - Renewal and Intermediate (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.01.01";
        String key2 = "03.05.01.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setRenewal("AS");
        codeFamily.setAnnual("PASS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Initial");
        serviceCatalogue.setSchedulingTypeAssociated("CL-G - Renewal and Annual (6, 5 or 4 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.02.01";
        String key2 = "03.05.02.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setRenewal("AS");
        codeFamily.setAnnual("PASS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("CL-G - Renewal and Annual (6, 5 or 4 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.02.01.01";
        String key2 = "03.05.02.01.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.03.01";
        String key2 = "03.05.03.02";
        String key3 = "03.05.03.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_04()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("CL-F - Renewal Only (6 or 5 year cycle) or S/A 2 in SC");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.04";

        assertEquals(1, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_05()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setPeriodic("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("MM-A - Renewal and Periodic (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.05.01";
        String key2 = "03.05.05.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_06_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.06.01.01";
        String key2 = "03.05.06.01.02";
        String key3 = "03.05.06.01.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_06_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2016, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.06.02.01";
        String key2 = "03.05.06.02.02";
        String key3 = "03.05.06.02.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_06_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("S11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.06.03.01";
        String key2 = "03.05.06.03.02";
        String key3 = "03.05.06.03.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_06_03_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(499D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.06.03.01.01";
        String key2 = "03.05.06.03.01.02";
        String key3 = "03.05.06.03.01.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_07_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-B - Special Case - Safety Equipment (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.07.01.01";
        String key2 = "03.05.07.01.02";
        String key3 = "03.05.07.01.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_07_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2000, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-B - Special Case - Safety Equipment (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.07.02.01";
        String key2 = "03.05.07.02.02";
        String key3 = "03.05.07.02.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_07_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("S11ABBB");
        asset.setGrossTonnage(498D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-B - Special Case - Safety Equipment (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.07.03.01";
        String key2 = "03.05.07.03.02";
        String key3 = "03.05.07.03.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_05_07_03_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        asset.setAssetType("A11ABBB");
        asset.setGrossTonnage(499D);
        asset.setBuildDate(new GregorianCalendar(2000, 07, 25));

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("ST-B - Special Case - Safety Equipment (Non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.05.07.03.01.01";
        String key2 = "03.05.07.03.01.02";
        String key3 = "03.05.07.03.01.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");
        codeFamily.setIntermediate("ITSS");
        codeFamily.setRenewal("SS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.08.02.03.01";
        String key2 = "03.08.02.03.02";
        String key3 = "03.08.02.03.03";

        assertEquals(3, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_06_01_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(1999, 01, 01));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Periodic");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.06.01.01";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_06_01_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Periodic");
        serviceCatalogue.setSchedulingTypeAssociated("ST-C - Special Case - Safcon (non HSSC)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.06.01.02";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_06_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("PASS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(1999, 00, 01));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Intermediate");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.06.02.01.01";
        String key2 = "03.06.02.01.02";

        assertEquals(2, associatedService.size());

        assertEquals("Redundant", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_06_02_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("PASS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2030, 00, 01));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Intermediate");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.06.02.02.01";
        String key2 = "03.06.02.02.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_05()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2000, 00, 01));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Intermediate");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.08.02.05";

        assertEquals(1, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_06()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Intermediate");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(3);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.08.02.06";

        assertEquals(1, associatedService.size());

        assertEquals("Redundant", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_01_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("CL-B - Core Hull Cycle (General)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setUpperRangePeriod(0);
        serviceCatalogue.setCyclePeriodicity(0);

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(60);
        renewalService.setAssignedDate(new GregorianCalendar(2015, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.07.01.01";

        assertEquals(1, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_01_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("CL-B - Core Hull Cycle (General)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setUpperRangePeriod(0);
        serviceCatalogue.setCyclePeriodicity(24);

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(60);
        renewalService.setAssignedDate(new GregorianCalendar(2017, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.07.01.02";

        assertEquals(1, associatedService.size());

        assertEquals("Redundant", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        service.setLowerRangeDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-F - Renewal and Annual (5 or 4 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");
        serviceCatalogue.setUpperRangePeriod(0);
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setCyclePeriodicity(0);

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(60);
        renewalService.setServiceDueDate(new GregorianCalendar(2017, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.07.02.01";

        assertEquals(1, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_02_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(3030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(0);
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-F - Renewal and Annual (5 or 4 year cycle)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.07.02.02";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_03_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(1030, 00, 01));
        serviceCatalogue.setCyclePeriodicity(0);
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(0);
        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.07.03.01";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_03_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setIntermediate("ITSS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2018, 07, 25));
        serviceCatalogue.setCyclePeriodicity(0);
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(60);
        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2021, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.07.03.02.01";
        String key2 = "03.07.03.02.02";

        assertEquals(2, associatedService.size());

        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

        assertEquals("Redundant", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_03_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setIntermediate("ITSS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 25));
        serviceCatalogue.setCyclePeriodicity(0);
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalServiceCatalogue.setCyclePeriodicity(60);
        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.07.03.03";

        assertEquals(1, associatedService.size());

        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_07_03_04()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setCyclePeriodicity(0);
        service.setCompletionDate(new GregorianCalendar(2030, 00, 01));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("ST-G - Renewal, Intermediate and Annual (5 year cycle)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.07.03.04";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_04()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setCyclePeriodicity(0);
        service.setCompletionDate(new GregorianCalendar(2030, 00, 01));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Annual");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.08.02.04";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setCyclePeriodicity(0);
        service.setCompletionDate(new GregorianCalendar(2030, 00, 01));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Docking");
        serviceCatalogue.setSchedulingTypeAssociated("CL-B - Core Hull Cycle (General)");
        serviceCatalogue.setLowerRangePeriod(0);
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setServiceDueDate(new GregorianCalendar(2016, 07, 25));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.08.01";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 24));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Docking");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setDateOnCompletion(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.08.02.01";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_08_02_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        Service renewalService = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setRenewal("SS");
        codeFamily.setAnnual("AS");

        service.setServiceStatus("Complete");
        service.setCompletionDate(new GregorianCalendar(2016, 07, 26));
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Docking");
        serviceCatalogue.setSchedulingTypeAssociated("CL-C - Core Hull Cycle (Laker)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("DS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        renewalService.setDateOnCompletion(new GregorianCalendar(2016, 07, 25));

        renewalRelationship.setAssociatedService(service);
        renewalRelationship.setRenewalService(renewalService);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        commands.add(commandFactory.newInsert(renewalRelationship, "renewal"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key1 = "03.08.02.02.01";
        String key2 = "03.08.02.02.02";
        String key3 = "03.08.02.02.03";
        String key4 = "03.08.02.02.04";

        assertEquals(4, associatedService.size());

        assertEquals("Required", associatedService.get(key1).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key1).getServiceCatalogue().getServiceGroup());
        assertEquals("DS", associatedService.get(key1).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key1).getSchedulingRegime());
        assertNotNull(associatedService.get(key1).getAsset());
        assertEquals(1, associatedService.get(key1).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key2).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key2).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key2).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key2).getSchedulingRegime());
        assertNotNull(associatedService.get(key2).getAsset());
        assertEquals(1, associatedService.get(key2).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key3).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key3).getServiceCatalogue().getServiceGroup());
        assertEquals("AS", associatedService.get(key3).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key3).getSchedulingRegime());
        assertNotNull(associatedService.get(key3).getAsset());
        assertEquals(1, associatedService.get(key3).getServiceOccurenceNumber().intValue());

        assertEquals("Required", associatedService.get(key4).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key4).getServiceCatalogue().getServiceGroup());
        assertEquals("ITSS", associatedService.get(key4).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key4).getSchedulingRegime());
        assertNotNull(associatedService.get(key4).getAsset());
        assertEquals(1, associatedService.get(key4).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_02_01()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Miscellaneous");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.02.01";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }

    @Test
    public void _03_02_02()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Renewal");
        serviceCatalogue.setSchedulingTypeAssociated("CL-A - No Rescheduling Required");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.02.02";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

    }

    @Test
    public void _03_02_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Additional");
        serviceCatalogue.setSchedulingTypeAssociated("MM-A - Renewal and Periodic (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");
        serviceCatalogue.setServiceCode("SS");

        service.setServiceCatalogue(serviceCatalogue);
        service.setAsset(asset);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.02.03";

        assertEquals(1, associatedService.size());
        assertEquals("Redundant", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("SS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());

    }

    @Test
    public void _03_03()
    {
        StatelessKieSession kieSession = getSession("defineassociatedservices");

        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
        CodeFamily codeFamily = new CodeFamily();
        Map<String, Service> associatedService = new HashMap<>();
        Asset asset = new Asset();

        codeFamily.setInitial("PASS");

        service.setServiceStatus("Complete");
        serviceCatalogue.setSchedulingRegime("Standard");
        service.setServiceOccurenceNumber(1);

        serviceCatalogue.setServiceType("Interim");
        serviceCatalogue.setSchedulingTypeAssociated("MM-A - Renewal and Periodic (5 year cycle)");
        serviceCatalogue.setServiceGroup("Hull");

        service.setAsset(asset);
        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(4);
        commands.add(commandFactory.newInsert(service, "service"));
        commands.add(commandFactory.newInsert(codeFamily, "codeFamily"));
        commands.add(commandFactory.newInsert(associatedService, "associatedService"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        String key = "03.03";

        assertEquals(1, associatedService.size());
        assertEquals("Required", associatedService.get(key).getWorkflowInstruction());
        assertEquals("Hull", associatedService.get(key).getServiceCatalogue().getServiceGroup());
        assertEquals("PASS", associatedService.get(key).getServiceCatalogue().getServiceCode());
        assertEquals("Standard", associatedService.get(key).getSchedulingRegime());
        assertNotNull(associatedService.get(key).getAsset());
        assertEquals(1, associatedService.get(key).getServiceOccurenceNumber().intValue());
    }
}
