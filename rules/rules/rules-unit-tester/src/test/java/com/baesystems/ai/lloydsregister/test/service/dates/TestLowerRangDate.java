package com.baesystems.ai.lloydsregister.test.service.dates;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.baesystems.ai.lloydsregister.test.base.BaseTest;
import com.detica.ukdc.drools.service.functions.Functions;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;

public class TestLowerRangDate extends BaseTest
{

    @Test
    public void _06_01()
    {
        StatelessKieSession kieSession = getSession("lowerrangedate");
        Service service = new Service();

        service.setServiceDueDate(null);
        service.setLowerRangeDate(null);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        assertNull(returnedService.getLowerRangeDate());
    }

    @Test
    public void _06_02_01()
    {
        StatelessKieSession kieSession = getSession("lowerrangedate");
        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        service.setServiceDueDate(Calendar.getInstance());

        serviceCatalogue.setLowerRangePeriod(0);

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = service.getServiceDueDate();

        assertTrue(Functions.DatesAreEqual(expected, returnedService.getLowerRangeDate()));
    }

    @Test
    public void _06_02_02()
    {
        StatelessKieSession kieSession = getSession("lowerrangedate");
        Service service = new Service();
        ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        service.setServiceDueDate(new GregorianCalendar(1999, 00, 12));

        serviceCatalogue.setLowerRangePeriod(12);

        service.setServiceCatalogue(serviceCatalogue);

        final CommandFactory commandFactory = new CommandFactory();
        final List<Command> commands = new ArrayList<>(1);
        commands.add(commandFactory.newInsert(service, "service"));
        final BatchExecutionCommand batchExecCommand = commandFactory.newBatchExecution(commands);
        final ExecutionResults results = kieSession.execute(batchExecCommand);
        final Service returnedService = (Service) results.getValue("service");

        Calendar expected = new GregorianCalendar(1998, 00, 13);
        assertTrue(Functions.DatesAreEqual(expected, returnedService.getLowerRangeDate()));
    }

}
