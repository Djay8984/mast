package com.bae.ai.lloydsregister.task.translator.configuration;

/**
 * An Exception that is thrown when a configuration paramter is not specified and has no default.
 */
public class ConfigurationMissingException extends Exception
{
  /**
   * The configuration parameter that is missing.
   */
  private final String configurationParameter;
  
  /**
   * Creates a new ConfigurationMissingException associated with the given paramter.
   *
   * @param configurationParameter The missing configuration paramter. Must not be null.
   *
   * @throws IllegalArgumentException Thrown if the configuration paramter is null.
   */
  public ConfigurationMissingException(final String configurationParameter)
  {
    super("Missing the following mandatory piece of configuration: " + configurationParameter);
    
    if(configurationParameter == null)
    {
      throw new IllegalArgumentException("Null configuration parameter given to ConfigurationMissingException.");
    }
    
    this.configurationParameter = configurationParameter;
  }
  
  /**
   * Returns the missing parameter.
   *
   * @return The missing configuration parameter.
   */
  public String getMissingParameter()
  {
    return this.configurationParameter;
  }
}