package com.bae.ai.lloydsregister.task.translator.observer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;

/**
 * This class is used to control the handing over of read TaskRules to be written
 * to the next observer in a thread-safe manner.
 */
public class ThreadSafeBridge implements NewRuleObserver, Runnable
{
  /**
   * The logger to use for this class.
   */
  private static final Logger log = Logger.getLogger("task-rules-translator");
  
  /**
   * The next observer in the chain to pass rules to.
   */
  private final NewRuleObserver next;
  
  /**
   * The hook that signals that there will be no more Rules.
   */
  private final CountDownLatch completionHook;
  
  /**
   * The queue holding rules that have been read but not yet passed on.
   */
  private final BlockingQueue<TaskRule> ruleQueue;
  
  /**
   * Creates a new ThreadSafeBridge instance that controls the rules handed over to the 
   * given NewRuleObserver. The provided completionHook will be counted down once the IO
   * activities are finished.
   *
   * @param next The observer to pass new rules on to. Must not be null.
   * @param completionHook The hook that will be counted down once operations are complete. 
   *                       Must not be null.
   */
  public ThreadSafeBridge(final NewRuleObserver next, final CountDownLatch completionHook)
  {
    if(next == null)
    {
      throw new IllegalArgumentException("Null NewRuleObserver given to ThreadSafeBridge.");
    }
    if(completionHook == null)
    {
      throw new IllegalArgumentException("Null completion hook given to ThreadSafeBridge.");
    }
    
    this.next = next;
    this.completionHook = completionHook;
    
    this.ruleQueue = new LinkedBlockingQueue<TaskRule>();
  }
  
  /**
   * Continuously passes new rules one-by-one to the given NewRuleObserver until 
   * onComplete is called then it terminates.
   */
  public void run()
  {
    do
    {
      try
      {
        final TaskRule rule = this.ruleQueue.poll(100L, TimeUnit.MILLISECONDS);
        
        if(rule != null)
        {
          this.next.onNewRule(rule);
        }
      }
      catch(final InterruptedException ex)
      {
        log.log(Level.WARNING, "Thread interrupted while waiting for new TaskRule.", ex);
      }
    }
    while(this.completionHook.getCount() != 0);
  }
  
  /**
   * Enqueues the given rule to be passed to the next observer.
   */
  public void onNewRule(final TaskRule rule)
  {
    try
    {
      this.ruleQueue.put(rule);
    }
    catch(final InterruptedException ex)
    {
      log.log(Level.WARNING, "Interrupted when enqueuing rule. This should not happen...", ex);
    }
  }
  
  /**
   * Calls onComplete on the next observer and signals the write loop to finish.
   */
  public void onComplete()
  {
    this.next.onComplete();
    this.completionHook.countDown();
  }
}