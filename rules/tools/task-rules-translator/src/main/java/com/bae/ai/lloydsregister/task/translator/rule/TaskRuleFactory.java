package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.List;

/**
 * TaskRuleFactory instances are responsible for creating TaskRule instances.
 */
public interface TaskRuleFactory
{
  /**
   * Creates a new TaskRule out of the input parameters that the application is configured to take into account.
   *
   * @param assetItemType The type of the Asset Item that this rule refers to.
   * @param attributeName The name of the attribute to check. If this is null then the rule does not apply to 
   *                      any attributes.
   * @param attributeValue The value of the attribute to check against.
   * @param assetType The type of Asset that this rule refers to.
   * @param classNotation The Class Notation to check against.
   * @param serviceCodes The Service Codes that apply to this rule.
   * @param regulationCode The Regulation Code that applies to this rule.
   * @param taskType The type of the Task that this rule guards.
   *
   * @return An appropriate TaskRule that represents the inputs within the scope of the Factory.
   */
  TaskRule createRule(final String assetItemType, final String attributeName, final String attributeValue, 
                      final String assetType, final String classNotation, final List<String> serviceCodes,
                      final String regulationCode, final String taskType);
}