package com.bae.ai.lloydsregister.task.translator.configuration;

import java.util.Properties;

/**
 * This class is responsible for generating application configuration given a set of command
 * line inputs and default values.
 */
public class ConfigurationGenerator
{
  /**
   * This method create the configuration for the application by choosing between specified
   * and default values.
   *
   * The configuration values it requires are:
   * - input.file
   * - output.file
   * - template.location
   * - parse.mode
   * - input.sheet.name
   * - output.start.row
   * - output.start.column
   * - always.multithread
   *
   * @param args The command line arguments to the application. Must not be null.
   * @param defaults The default values to use if some configuration was not specified via the command line.
   *                 Must not be null.
   *
   * @return The configuration to use for this application.
   *
   * @throws IllegalArgumentException Thrown if any argument is invalid with regars to the above.
   * @throws ConfigurationMissingException Thrown if a mandatory configuration parameter is missing.
   * @throws InvalidConfigurationException Thrown if a given configuration value is invalid.
   */
  public ApplicationConfiguration generateConfig(final String[] args, final Properties defaults) throws ConfigurationMissingException,
                                                                                                        InvalidConfigurationException
  {
    if(args == null)
    {
      throw new IllegalArgumentException("Null args given to generateConfig.");
    }
    if(defaults == null)
    {
      throw new IllegalArgumentException("Null defaults given to generateConfig.");
    }
    
    String inputFile = defaults.getProperty("input.file", null);
    String outputFile = defaults.getProperty("output.file", null);
    String templateLocation = defaults.getProperty("template.location", null);
    ParseMode parseMode = null;
    String parseModeString = defaults.getProperty("parse.mode", null);
    if(parseModeString != null)
    {
      if("simplified".equalsIgnoreCase(parseModeString))
      {
        parseMode = ParseMode.SIMPLIFIED;
      }
      else if("full".equalsIgnoreCase(parseModeString))
      {
        parseMode = ParseMode.FULL;
      }
      else
      {
        throw new InvalidConfigurationException("parse.mode", "The parse mode must be either simplified or full", parseModeString);
      }
    }
    String inputSheetName = defaults.getProperty("input.sheet.name", null);
    Short outputStartColumn = null;
    String outputStartColumnString = defaults.getProperty("output.start.column", null);
    if(outputStartColumnString != null)
    {
      try
      {
        outputStartColumn = new Short(outputStartColumnString);
      }
      catch(NumberFormatException ex)
      {
        throw new InvalidConfigurationException("output.start.column", "Not a valid number.", outputStartColumnString);
      }
    }
    Short outputStartRow = null;
    String outputStartRowString = defaults.getProperty("output.start.row", null);
    if(outputStartRowString != null)
    {
      try
      {
        outputStartRow = new Short(outputStartRowString);
      }
      catch(NumberFormatException ex)
      {
        throw new InvalidConfigurationException("output.start.row", "Not a valid number.", outputStartRowString);
      }
    }
    Boolean alwaysMultiThread = null;
    String alwaysMultiThreadString = defaults.getProperty("always.multithread", null);
    if(alwaysMultiThreadString != null)
    {
      alwaysMultiThread = new Boolean(alwaysMultiThreadString);
    }
    
    // Override defaults with arguments.
    for(final String arg : args)
    {
      if(arg.startsWith("input.file="))
      {
        inputFile = arg.substring("input.file=".length());
      }
      else if(arg.startsWith("output.file="))
      {
        outputFile = arg.substring("output.file=".length());
      }
      else if(arg.startsWith("template.location="))
      {
        templateLocation = arg.substring("template.location=".length());
      }
      else if(arg.startsWith("parse.mode="))
      {
        parseModeString = arg.substring("parse.mode=".length());
        if(parseModeString != null)
        {
          if("simplified".equalsIgnoreCase(parseModeString))
          {
            parseMode = ParseMode.SIMPLIFIED;
          }
          else if("full".equalsIgnoreCase(parseModeString))
          {
            parseMode = ParseMode.FULL;
          }
          else
          {
            throw new InvalidConfigurationException("parse.mode", "The parse mode must be either simplified or full", parseModeString);
          }
        }
      }
      else if(arg.startsWith("input.sheet.name="))
      {
        inputSheetName = arg.substring("input.sheet.name=".length());
      }
      else if(arg.startsWith("output.start.row="))
      {
        outputStartRowString = arg.substring("output.start.row=".length());
        if(outputStartRowString != null)
        {
          try
          {
            outputStartRow = new Short(outputStartRowString);
          }
          catch(NumberFormatException ex)
          {
            throw new InvalidConfigurationException("output.start.row", "Not a valid number.", outputStartRowString);
          }
        }
      }
      else if(arg.startsWith("output.start.column="))
      {
        outputStartColumnString = arg.substring("output.start.column=".length());
        if(outputStartColumnString != null)
        {
          try
          {
            outputStartColumn = new Short(outputStartColumnString);
          }
          catch(NumberFormatException ex)
          {
            throw new InvalidConfigurationException("output.start.column", "Not a valid number.", outputStartColumnString);
          }
        }
      }
      else if(arg.startsWith("always.multithread="))
      {
        alwaysMultiThread = new Boolean(arg.substring("always.multithread=".length()));
      }
    }
    
    if(inputFile == null)
    {
      throw new ConfigurationMissingException("input.file");
    }
    if(outputFile == null)
    {
      throw new ConfigurationMissingException("output.file");
    }
    if(templateLocation == null)
    {
      throw new ConfigurationMissingException("template.location");
    }
    if(parseMode == null)
    {
      throw new ConfigurationMissingException("parse.mode");
    }
    if(inputSheetName == null)
    {
      throw new ConfigurationMissingException("input.sheet.name");
    }
    if(outputStartColumn == null)
    {
      throw new ConfigurationMissingException("output.start.column");
    }
    if(outputStartRow == null)
    {
      throw new ConfigurationMissingException("output.start.row");
    }
    if(alwaysMultiThread == null)
    {
      throw new ConfigurationMissingException("always.multithread");
    }
    
    if(!templateLocation.startsWith("classpath:") && !templateLocation.startsWith("file:"))
    {
      throw new InvalidConfigurationException("template.location", "The template location must start with classpath: or file:", templateLocation);
    }
    if(outputStartColumn < 0)
    {
      throw new InvalidConfigurationException("output.start.column", "The column index must be at least 0.", outputStartColumn);
    }
    if(outputStartRow < 0)
    {
      throw new InvalidConfigurationException("output.start.row", "The row index must be at least 0.", outputStartRow);
    }
    
    
    return new ApplicationConfiguration(inputFile, outputFile, templateLocation, parseMode, 
                                        inputSheetName, outputStartColumn, outputStartRow, alwaysMultiThread);
  }
}