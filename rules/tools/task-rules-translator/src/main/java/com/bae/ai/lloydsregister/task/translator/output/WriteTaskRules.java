package com.bae.ai.lloydsregister.task.translator.output;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.bae.ai.lloydsregister.task.translator.observer.NewRuleObserver;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;

/**
 * This class is notified of new Task Rules and is responsible for writing them
 * out to the specified file at the specified location.
 */
public class WriteTaskRules implements NewRuleObserver
{
  /**
   * The logger to use for this class.
   */
  private static final Logger log = Logger.getLogger("task-rules-translator");
  
  /**
   * Output file to write to.
   */
  private final File outputFile;
  
  /**
   * The POIFSFileSystem instance used to for the output spreadsheet.
   */
  private final POIFSFileSystem fileSystem;
  
  /**
   * The HSSFWorkbook instance representing the Workbook we are editing.
   */
  private final HSSFWorkbook workbook;
  
  /**
   * The sheet that we are adding rules to.
   */
  private final HSSFSheet sheet;
  
  /**
   * The column to start adding rules at.
   */
  private final short startColumnIndex;
  
  /**
   * The row we started writing rules at.
   */
  private final short startRowIndex;
  
  /**
   * The current row to add to.
   */
  private short currentRowIndex;
  
  /**
   * Creates a new instance of the class to write Task Rules that it receives to
   * a specified output file.
   *
   * @param outputFile The file to which the rules will be written. Must not be null.
   * @param outputStartColumn The index of the column to start writing rules. Must not
   *                          be less than zero.
   * @param outputStartRow The index of the row to start writing rules. Must not be 
   *                       less than zero.
   *
   * @throws IllegalArgumentException Thrown if any argument is invalid in regards to the
   *                                  conditions specified above.
   */
  public WriteTaskRules(final File outputFile, final short outputStartColumn, final short outputStartRow) throws IOException
  {
    if(outputFile == null)
    {
      throw new IllegalArgumentException("Null output file given to WriteTaskRules.");
    }
    if(outputStartColumn < 0)
    {
      throw new IllegalArgumentException("The start column must be at least zero: " + outputStartColumn);
    }
    if(outputStartRow < 0)
    {
      throw new IllegalArgumentException("The start row must be at least zero: " + outputStartRow);
    }
    this.outputFile = outputFile;
    this.startColumnIndex = outputStartColumn;
    this.startRowIndex = outputStartRow;
    this.currentRowIndex = outputStartRow;
    
    this.fileSystem = new POIFSFileSystem(outputFile);
    this.workbook = new HSSFWorkbook(this.fileSystem);
    // We just use the first sheet.
    this.sheet = this.workbook.getSheetAt(0);    
  }
  
  public void onNewRule(final TaskRule rule)
  {
    final HSSFRow row;
    
    if(null != this.sheet.getRow(this.currentRowIndex))
    {
      row = this.sheet.getRow(this.currentRowIndex);
    }
    else
    {
      row = this.sheet.createRow(this.currentRowIndex);
    }
    
    int currentColumn = this.startColumnIndex;
    
    // Write description
    final HSSFCell descriptionCell = row.createCell(currentColumn);
    descriptionCell.setCellValue("TaskRule-" + (this.currentRowIndex + 1 - this.startRowIndex));
    
    currentColumn += 1;
    
    // Write Asset Item Type
    final HSSFCell assetItemTypeCell = row.createCell(currentColumn);
    assetItemTypeCell.setCellValue(rule.getAssetItemType());
    
    currentColumn += 1;
    
    // Write Attribute
    if(rule.hasAttribute())
    {
      final HSSFCell attributeCell = row.createCell(currentColumn);
      final StringBuilder attributeBuilder = new StringBuilder();
      attributeBuilder.append('[').append(rule.getAttributeName()).append("][").append(rule.getAttributeValue()).append(']');
      attributeCell.setCellValue(attributeBuilder.toString());
    }
    
    currentColumn += 1;
    
    if(!rule.getServiceCodes().isEmpty())
    {
      final HSSFCell serviceCell = row.createCell(currentColumn);
      serviceCell.setCellValue("true");
    }
    
    currentColumn += 1;
    
    // Write either a single service code or a list.
    if(rule.getServiceCodes().size() > 1)
    {
      currentColumn += 1;
      final StringBuilder codesBuilder = new StringBuilder();
      for(final String code : rule.getServiceCodes())
      {
        codesBuilder.append('"').append(code).append("\",");
      }
      codesBuilder.deleteCharAt(codesBuilder.length() - 1);
      
      final HSSFCell codesCell = row.createCell(currentColumn);
      codesCell.setCellValue(codesBuilder.toString());
    }
    else
    {
      if(!rule.getServiceCodes().isEmpty())
      {
        final HSSFCell codeCell = row.createCell(currentColumn);
        codeCell.setCellValue(rule.getServiceCodes().get(0));
      }
      currentColumn += 1;
    }
    
    currentColumn += 1;
    
    // Check Asset Type and Class Notation and Regulation Code
    if((rule.getAssetType() != null) || (rule.getClassNotation() != null) || (rule.getRegulationCode() != null))
    {
      // Asset Glue column
      final HSSFCell assetGlueCell = row.createCell(currentColumn);
      assetGlueCell.setCellValue("true");
      
      currentColumn += 1;
      if(rule.getClassNotation() != null)
      {
        // Write Class Notation
        final HSSFCell classNotationCell = row.createCell(currentColumn);
        classNotationCell.setCellValue(rule.getClassNotation());
      }
      
      currentColumn += 1;
      if(rule.getRegulationCode() != null)
      {
        // Write Regulation Code
        final HSSFCell regulationCodeCell = row.createCell(currentColumn);
        regulationCodeCell.setCellValue(rule.getRegulationCode());
      }
    
      currentColumn += 1;
      if(rule.getAssetType() != null)
      {
        // Write Asset Type
        final HSSFCell assetTypeCell = row.createCell(currentColumn);
        assetTypeCell.setCellValue(rule.getAssetType());
      }
      
      currentColumn += 1;
    }
    else
    {
      currentColumn += 4;
    }
    
    // Item Tasks glue column
    final HSSFCell itemTasksCell = row.createCell(currentColumn);
    itemTasksCell.setCellValue("true");
    
    currentColumn += 1;
    
    // Task Type column
    final HSSFCell taskTypeCell = row.createCell(currentColumn);
    taskTypeCell.setCellValue(rule.getTaskType());
    
    currentColumn += 1;
    
    // Logging column
    final HSSFCell logCell = row.createCell(currentColumn);
    logCell.setCellValue("TaskRule-" + (this.currentRowIndex + 1 - this.startRowIndex));
    
    currentRowIndex += 1;
  }
  
  public void onComplete()
  {
    try
    {
      final File tempFile = File.createTempFile("WriteTaskRules", "xls");
      final OutputStream out = new FileOutputStream(tempFile);
      this.workbook.write(out);
      out.close();
      this.workbook.close();
      this.fileSystem.close();
      this.outputFile.delete();
      tempFile.renameTo(this.outputFile);
    }
    catch(final IOException ex)
    {
      ex.printStackTrace();
      log.log(Level.WARNING, "Unable to finish writing the rules to the target file.", ex);
    }
  }
}