package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.List;

/**
 * Instances of this interface represent task rules.
 */
public interface TaskRule
{
  /**
   * Returns which type of Asset Item this rule should apply to.
   *
   * @return The Asset Item type.
   */
  String getAssetItemType();
  
  /**
   * Indicates whether this rule applies to Asset Items with a particular attribute value.
   *
   * @return Whether this rule checks an attribute.
   */
  boolean hasAttribute();
  
  /**
   * The name of the attribute to check. The results are undefined if hasAttribute returns false.
   *
   * @return The name of the attribute to check.
   */
  String getAttributeName();
  
  /**
   * The value of the attribute to check. The results are undefined if hasAttribute returns false.
   *
   * @return The value of the attribute to check.
   */
  String getAttributeValue();
  
  /**
   * The type of the Asset that this rule applies to. If null then there is no Asset restriction for the rule.
   *
   * @return The type of the Asset which this rule applies to.
   */
  String getAssetType();
  
  /**
   * The Class Notation that this rule applies to. If null then there is no Class Notation restriction for the rule.
   *
   * @return The Class Notation that this rule applies to.
   */
  String getClassNotation();
  
  /**
   * The Service Codes that this rule applies to. If an empty list then no Service Code restrictions apply.
   *
   * @return The Service Codes that the rule applies to.
   */
  List<String> getServiceCodes();
  
  /**
   * The Regulation that this rule applies to. If null then there is no Regulation restriction for the rule.
   *
   * @return The Regulation that the rule applies to.
   */
  String getRegulationCode();
  
  /**
   * Returns the name of the type of the task that this rule determines eligibility for.
   *
   * @return The type of the task that this rule checks conditions for.
   */
  String getTaskType();
}