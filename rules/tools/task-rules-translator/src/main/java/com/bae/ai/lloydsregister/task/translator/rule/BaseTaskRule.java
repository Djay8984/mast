package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.ArrayList;
import java.util.List;

/**
 * A base class for types of task rules to extend.
 */
public abstract class BaseTaskRule implements TaskRule
{
  /**
   * The Asset Item type this rule applies to.
   */
  private final String assetItemType;
  
  /**
   * The Asset type this rule applies to.
   */
  private final String assetType;
  
  /**
   * The Class Notation this rule applies to.
   */
  private final String classNotation;
  
  /**
   * The list of Service Codes this rule applies to.
   */
  private final List<String> serviceCodes;
  
  /**
   * The regulation code that this rule applies to.
   */
  private final String regulationCode;
  
  /**
   * The Task Type that this rule guards.
   */
  private final String taskType;
  
  /**
   * Create a new base task rule with the given conditions and outputs.
   *
   * @param assetItemType The type of Asset Item that this rule applies to. Must not be null.
   * @param assetType The type of Asset that this rule applies to.
   * @param classNotation The Class Notation that this rule applies to.
   * @param serviceCodes The Service Codes that this rule applies to. Must not be null.
   * @param regulationCode The Regulation Code that this rule applies to.
   * @param taskType The type of the task that this rule recommends. Must not be null.
   *
   * @throws IllegalArgumentException Thrown if any argument is invalid in regards to the comments above.
   */
  public BaseTaskRule(final String assetItemType, final String assetType, final String classNotation,
                      final List<String> serviceCodes, final String regulationCode, final String taskType)
  {
    if(assetItemType == null)
    {
      throw new IllegalArgumentException("Null Asset Item type given to BaseTaskRule.");
    }
    if(serviceCodes == null)
    {
      throw new IllegalArgumentException("Null Serivce Codes list given to BaseTaskRule");
    }
    if(taskType == null)
    {
      throw new IllegalArgumentException("Null Task Type given to BaseTaskRule.");
    }
    
    this.assetItemType = assetItemType;
    this.assetType = assetType;
    this.classNotation = classNotation;
    this.serviceCodes = new ArrayList<>(serviceCodes);
    this.regulationCode = regulationCode;
    this.taskType = taskType;
  }

  public String getAssetItemType()
  {
    return this.assetItemType;
  }
  
  public String getAssetType()
  {
    return this.assetType;
  }
  
  public String getClassNotation()
  {
    return this.classNotation;
  }
  
  public List<String> getServiceCodes()
  {
    return this.serviceCodes;
  }
  
  public String getRegulationCode()
  {
    return this.regulationCode;
  }
  
  public String getTaskType()
  {
    return this.taskType;
  }
}