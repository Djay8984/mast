package com.bae.ai.lloydsregister.task.translator.observer;

import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;

/**
 * Implementations of this interface will be notified by the Rules input reader
 * when it had read a new Rule.
 */
public interface NewRuleObserver
{
  /**
   * Called when a new TaskRule has been created from the input reader.
   *
   * @param rule The new TaskRule read from the input.
   */
  void onNewRule(final TaskRule rule);
  
  /**
   * Called when the input has finished being read.
   */
  void onComplete();
}