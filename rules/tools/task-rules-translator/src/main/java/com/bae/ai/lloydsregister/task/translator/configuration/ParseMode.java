package com.bae.ai.lloydsregister.task.translator.configuration;

/**
 * Lists the different parse modes that the application supports.
 */
public enum ParseMode
{
  /**
   * A parse mode that does not parse parent relationships or conditional tasks.
   */
  SIMPLIFIED,
  /**
   * A parse mode that will parse the full specification of the rule set.
   */
  FULL;
}