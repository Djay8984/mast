package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.List;

/**
 * Instances of this class represent simplified task rules that check an attribute of
 * an Asset Item.
 */
public class SimplifiedTaskAttributeRule extends BaseTaskRule
{
  /**
   * The name of the attribute to check.
   */
  private final String attributeName;
  
  /**
   * The value of the attribute to check against.
   */
  private final String attributeValue;
  
  /**
   * @param attributeName The name of the attribute to check. Must not be null.
   * @param attributeValue The value of the attribute to compare against. Must not be null.
   *
   * @throws IllegalArgumentException Thrown if any argument is not valid.
   */
  public SimplifiedTaskAttributeRule(final String assetItemType, final String assetType, final String classNotation,
                                     final List<String> serviceCodes, final String regulationCode, final String taskType, 
                                     final String attributeName, final String attributeValue)
  {
    super(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    if(attributeName == null)
    {
      throw new IllegalArgumentException("Null attribute name given to SimplifiedTaskAttributeRule.");
    }
    if(attributeValue == null)
    {
      throw new IllegalArgumentException("Null attribute value given to SimplifiedTaskAttributeRule.");
    }
    
    this.attributeName = attributeName;
    this.attributeValue = attributeValue;
  }
  
  /**
   * Returns true.
   *
   * @return true.
   */
  public boolean hasAttribute()
  {
    return true;
  }
  
  public String getAttributeName()
  {
    return this.attributeName;
  }
  
  public String getAttributeValue()
  {
    return this.attributeValue;
  }
}