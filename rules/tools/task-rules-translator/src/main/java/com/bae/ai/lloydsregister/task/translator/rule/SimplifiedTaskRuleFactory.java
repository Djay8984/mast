package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.List;

/**
 * This class is a factory that builds TaskRules to the simplified specification.
 */
public class SimplifiedTaskRuleFactory implements TaskRuleFactory
{
  public TaskRule createRule(final String assetItemType, final String attributeName, final String attributeValue, 
                             final String assetType, final String classNotation, final List<String> serviceCodes,
                             final String regulationCode, final String taskType)
  {
    final TaskRule rule;
    if((attributeName == null) || (attributeValue == null))
    {
      rule = new SimplifiedTaskNoAttributeRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    }
    else
    {
      rule = new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes, 
                                             regulationCode, taskType, attributeName, attributeValue);
    }
    return rule;
  }
}