package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.List;

/**
 * Instances of this class represent simplified task rules that do not check an attribute of
 * an Asset Item.
 */
public class SimplifiedTaskNoAttributeRule extends BaseTaskRule
{
  public SimplifiedTaskNoAttributeRule(final String assetItemType, final String assetType, final String classNotation,
                                       final List<String> serviceCodes, final String regulationCode, final String taskType)
  {
    super(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
  }
  
  /**
   * Returns false.
   *
   * @return false.
   */
  public boolean hasAttribute()
  {
    return false;
  }
  
  public String getAttributeName()
  {
    return null;
  }
  
  public String getAttributeValue()
  {
    return null;
  }
}