package com.bae.ai.lloydsregister.task.translator.configuration;

/**
 * This Exception is thrown when a configuration value exists but is invalid.
 */
public class InvalidConfigurationException extends Exception
{
  /**
   * The parameter that was invalid.
   */
  private final String parameter;
  
  /**
   * The error message associated with the parameter.
   */
  private final String error;
  
  /**
   * The value associated with the parameter.
   */
  private final Object value;
  
  /**
   * Constructs a new InvalidConfigurationException that represents an error with the given parameter having
   * the given value.
   *
   * @param parameter The parameter that is invalid. Must not be null.
   * @param error The error message describing what is wrong with the paramter value. Must not be null.
   * @param value The invalid value of the paramter.
   *
   * @throws IllegalArgumentException Thrown if any argument is invalid in regars to the above.
   */
  public InvalidConfigurationException(final String parameter, final String error, final Object value)
  {
    super("The configuration parameter " + parameter + " with the value" + value + " is invalid: " + error);
    
    if(parameter == null)
    {
      throw new IllegalArgumentException("Null parameter name given to InvalidConfigurationException.");
    }
    if(error == null)
    {
      throw new IllegalArgumentException("Null error given to InvalidConfigurationException.");
    }
    
    this.parameter = parameter;
    this.error = error;
    this.value = value;
  }
  
  /**
   * Returns the name of the parameter that is invalid.
   *
   * @return The name of the invalid parameter.
   */
  public String getParameter()
  {
    return this.parameter;
  }
  
  /**
   * Returns the error associated with the invalid value.
   *
   * @return The error associated with the value.
   */
  public String getError()
  {
    return this.error;
  }
  
  /**
   * The invalid value of the configuration parameter.
   *
   * @return The invalid value.
   */
  public Object getValue()
  {
    return this.value;
  }
}