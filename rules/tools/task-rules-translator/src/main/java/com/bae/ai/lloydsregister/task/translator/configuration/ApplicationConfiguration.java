package com.bae.ai.lloydsregister.task.translator.configuration;

/**
 * Instances of this class represent the configuration for the translator application.
 *
 * This class and its sub-classes must be immutable.
 */
public class ApplicationConfiguration
{
  /**
   * The location of the input file.
   */
  private final String inputFileLocation;
  
  /**
   * The location of the output file.
   */
  private final String outputFileLocation;
  
  /**
   * The location of the template file.
   */
  private final String templateFileLocation;
  
  /**
   * The parse mode of the application.
   */
  private final ParseMode parseMode;
  
  /**
   * The name of the sheet in the input file that contains the rules.
   */
  private final String inputRulesSheetName;
  
  /**
   * The column index to start writing rules at.
   */
  private final short outputStartColumn;
  
  /**
   * The row index to start writing rules at.
   */
  private final short outputStartRow;
  
  /**
   * The flag representing whether to force multi-threaded mode.
   */
  private final boolean forceMultiThreaded;
  
  /**
   * Creates a new ApplicationConfiguration instance representing the configuration for the 
   * application.
   *
   * @param inputFileLocation The file path to the input file which specifies the rules. Must not
   *                          be null.
   * @param outputFileLocation The file path pointing to where the translated rules spreadsheet will
   *                           be written. Must not be null.
   * @param templateFileLocation The URL to the template file used to generate the translated rules 
   *                             file. Must be prefixed with either file: or classpath: and must not
   *                             be null.
   * @param parseMode The parsing mode to use with the input file. Must not be null.
   * @param inputRulesSheetName The name of the sheet in the input file containing the rules. Must
   *                            not be null.
   * @param outputStartColumn The index of the column to start writing rules at. Must be equal to
   *                          or greater than zero.
   * @param outputStartRow The index of the row to start writing rules at. Must be equal to or 
   *                       greater than zero.
   * @param forceMultiThreaded Whether to force the application to run with multiple threads.
   *
   * @throws InllegalArgumentException Thrown if any argument does not match the conditions above.
   */
  public ApplicationConfiguration(final String inputFileLocation, final String outputFileLocation, 
                                  final String templateFileLocation, final ParseMode parseMode,
                                  final String inputRulesSheetName, final short outputStartColumn,
                                  final short outputStartRow, final boolean forceMultiThreaded)
  {
    if(inputFileLocation == null)
    {
      throw new IllegalArgumentException("Null input file location given to ApplicationConfiguration.");
    }
    if(outputFileLocation == null)
    {
      throw new IllegalArgumentException("Null output file location given to ApplicationConfiguration.");
    }
    if(templateFileLocation == null)
    {
      throw new IllegalArgumentException("Null template file location given to ApplicationConfiguration.");
    }
    if(!templateFileLocation.startsWith("file:") && !templateFileLocation.startsWith("classpath:"))
    {
      throw new IllegalArgumentException("Invalid prefix to template file location. Must be one of file: or classpath:." + templateFileLocation);
    }
    if(parseMode == null)
    {
      throw new IllegalArgumentException("Null parse mode given to ApplicationConfiguration.");
    }
    if(inputRulesSheetName == null)
    {
      throw new IllegalArgumentException("Null input rules sheet name given to ApplicationConfiguration.");
    }
    if(outputStartColumn < 0)
    {
      throw new IllegalArgumentException("Negative output start column given to ApplicationConfiguration: " + outputStartColumn);
    }
    if(outputStartRow < 0)
    {
      throw new IllegalArgumentException("Negative output start row given to ApplicationConfiguration: " + outputStartRow);
    }
    
    this.inputFileLocation = inputFileLocation;
    this.outputFileLocation = outputFileLocation;
    this.templateFileLocation = templateFileLocation;
    this.parseMode = parseMode;
    this.inputRulesSheetName = inputRulesSheetName;
    this.outputStartColumn = outputStartColumn;
    this.outputStartRow = outputStartRow;
    this.forceMultiThreaded = forceMultiThreaded;
  }
  
  /**
   * Returns the configured file path of the input file.
   *
   * @return A String representing a file path.
   */
  public String getInputFileLocation()
  {
    return this.inputFileLocation;
  }
  
  /**
   * Returns the configured file path to write the translated rules to.
   *
   * @return A String representing a file path.
   */
  public String getOutputFileLocation()
  {
    return this.outputFileLocation;
  }
  
  /**
   * Returns the configured location of the template file with on the classpath (prefixed with classpath:) 
   * or on the filesystem (prefixed with file:).
   *
   * @return The location of the template file.
   */
  public String getTemplateFileLocation()
  {
    return this.templateFileLocation;
  }
  
  /**
   * Returns the configured parsing mode for this application.
   *
   * @return The configured parse mode.
   */
  public ParseMode getParseMode()
  {
    return this.parseMode;
  }
  
  /**
   * Returns the name of the sheet in the input file that contains the rules.
   *
   * @return The name of the rules sheet.
   */
  public String getInputRulesSheetName()
  {
    return this.inputRulesSheetName;
  }
  
  /**
   * Returns the index of the column in the template where rules should start to be
   * inserted.
   *
   * @return The column at which to start writing rules.
   */
  public short getOutputStartColumn()
  {
    return this.outputStartColumn;
  }
  
  /**
   * Returns the index of the row in the template where rules should start to be
   * inserted.
   *
   * @return The row at which to start writing rules.
   */
  public short getOutputStartRow()
  {
    return this.outputStartRow;
  }
  
  /**
   * Returns whether the application should be forced to run in multi-threaded mode.
   *
   * @return Whether the application should be forced into multi-threaded mode.
   */
  public boolean forceMultiThreaded()
  {
    return this.forceMultiThreaded;
  }
}