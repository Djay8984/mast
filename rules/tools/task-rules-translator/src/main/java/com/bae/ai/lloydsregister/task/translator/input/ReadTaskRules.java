package com.bae.ai.lloydsregister.task.translator.input;

import java.io.File;
import java.io.InputStream;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bae.ai.lloydsregister.task.translator.observer.NewRuleObserver;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRuleFactory;

import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.CellRecord;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RowRecord;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 * This class is responsible for reading task rules from an input spreadsheet.
 */
public class ReadTaskRules implements Runnable
{
  /**
   * The logger to use for this class.
   */
  private static final Logger log = Logger.getLogger("task-rules-translator");
  
  /**
   * Callback for the parsing of the spreadsheet.
   */
  private class SpreadsheetParser implements HSSFListener
  {
    private boolean correctSheet;
    private boolean foundSheet;
    private int currentSheetNumber;
    private int sheetNumber;
    
    private SSTRecord sstRecord;
    
    private volatile String assetItemType;
    private volatile String attributeName;
    private volatile String attributeValue;
    private volatile String assetType;
    private volatile String classNotation;
    private volatile List<String> serviceCodes;
    private volatile String regulationCode;
    private volatile String taskType;
    
    public void processRecord(final Record record)
    {
      switch(record.getSid())
      {
        
        case BOFRecord.sid:
        {
          final BOFRecord bof = (BOFRecord) record;
          if(bof.getType() == BOFRecord.TYPE_WORKSHEET)
          {
            correctSheet = sheetNumber == currentSheetNumber;
            currentSheetNumber += 1;
          }
          break;
        }
        /**
         * Flag whether we are on the task rules sheet.
         */
        case BoundSheetRecord.sid:
        {
          final BoundSheetRecord bsr = (BoundSheetRecord) record;
          if(sheetName.equals(bsr.getSheetname()))
          {
            foundSheet = true;
          }
          else if(!foundSheet)
          {
            sheetNumber += 1;
          }
          break;
        }
        
        /**
         * Get the Shared String Table that we are currently using.
         */
        case SSTRecord.sid:
        {
          sstRecord = (SSTRecord) record;
          break;
        }
        
        case NumberRecord.sid:
        {
          final NumberRecord numRecord = (NumberRecord) record;
          taskType = "" + Math.round(numRecord.getValue());
          break;
        }
        
        /**
         * Set the parameters used to build the TaskRule.
         */
        case LabelSSTRecord.sid:
        {
          final LabelSSTRecord labelRecord = (LabelSSTRecord) record;
          if(correctSheet && labelRecord.getRow() > 0)
          {
            final String stringContents = sstRecord.getString(labelRecord.getSSTIndex()).getString();
            switch(labelRecord.getColumn())
            {
              case 0:
                System.out.println("Task type is SST record.");
                taskType = stringContents;
                break;
              case 1:
                assetItemType = stringContents;
                break;
              case 2:
                if(!"NULL".equals(stringContents))
                {
                  attributeName = stringContents;
                }
                break;
              case 3:
                if(!"NULL".equals(stringContents))
                {
                  attributeValue = stringContents;
                }
                break;
              case 4:
                if(!"NULL".equals(stringContents))
                {
                  assetType = stringContents;
                }
                break;
              case 5:
                if(!"NULL".equals(stringContents))
                {
                  classNotation = stringContents;
                }
                break;
              case 6:
                serviceCodes = Arrays.asList(stringContents.split("/"));
                break;
              case 7:
              {
                final TaskRule rule = ruleFactory.createRule(assetItemType, attributeName, attributeValue, assetType,
                                                         classNotation, serviceCodes, regulationCode, taskType);
                observer.onNewRule(rule);
                assetItemType = null;
                attributeName = null;
                attributeValue = null;
                assetType = null;
                classNotation = null;
                serviceCodes = null;
                regulationCode = null;
                taskType = null;
                break;
              }
            }
          }
          
          break;
        }
        
        default:
        {
          if(record instanceof CellRecord)
          {
            System.out.println("Unknown sid: " + record.getSid());
            System.out.println("Record class: " + record.getClass());
          }
        }
      }
      
    }
    
    
  }
  
  /**
   * The input file containing the Task Rules to be read in.
   */
  private final File inputFile;
  
  /**
   * The observer to which new Task Rules are reported.
   */
  private final NewRuleObserver observer;
  
  /**
   * The name of the sheet in thr workbook that contains the Rule definitions.
   */
  private final String sheetName;
  
  /**
   * The factory used to build Task Rules.
   */
  private final TaskRuleFactory ruleFactory;
  
  /**
   * Create a new ReadTaskRules instance that reads rules from the given input file.
   *
   * @param inputFile The file containing the task rules. Must not be null.
   * @param observer The observer to be notified when a new TaskRule has been created from the input.
   *                 Must not be null.
   * @param sheetName The name of the sheet that contains the TaskRules. Must not be null.
   * @param ruleFactory The factory used to create the TaskRules from the input. Must not be null.
   *
   * @throws IllegalArgumentException Thrown if one of the arguments does not meet the above conditions.
   */
  public ReadTaskRules(final File inputFile, final NewRuleObserver observer,
                       final String sheetName, final TaskRuleFactory ruleFactory)
  {
    if(inputFile == null)
    {
      throw new IllegalArgumentException("Null input file given to ReadTaskRules.");
    }
    if(observer == null)
    {
      throw new IllegalArgumentException("Null observer given to ReadTaskRules.");
    }
    if(sheetName == null)
    {
      throw new IllegalArgumentException("Null sheet name given to ReadTaskRules.");
    }
    if(ruleFactory == null)
    {
      throw new IllegalArgumentException("Null rule factory given to ReadTaskRules.");
    }
    
    this.inputFile = inputFile;
    this.observer = observer;
    this.sheetName = sheetName;
    this.ruleFactory = ruleFactory;
  }
  
  /**
   * This method reads the TaskRules from the input file and notifies the observer accordingly.
   */
  public void run()
  {
    try(final POIFSFileSystem fileSystem = new POIFSFileSystem(this.inputFile))
    {
      try(final InputStream in = fileSystem.createDocumentInputStream("Workbook"))
      {
        final HSSFRequest request = new HSSFRequest();
        final SpreadsheetParser parser = new SpreadsheetParser();
        request.addListenerForAllRecords(parser);
        
        final HSSFEventFactory eventFactory = new HSSFEventFactory();
        eventFactory.processEvents(request, in);
      }
    }
    catch(final Exception ex)
    {
      log.log(Level.SEVERE, "Could not complete reading the input file.", ex);
    }
    finally
    {
      this.observer.onComplete();
    }
  }
}