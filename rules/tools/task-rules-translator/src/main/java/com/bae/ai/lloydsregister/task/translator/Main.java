package com.bae.ai.lloydsregister.task.translator;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;

import java.net.URISyntaxException;
import java.net.URL;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bae.ai.lloydsregister.task.translator.configuration.ApplicationConfiguration;
import com.bae.ai.lloydsregister.task.translator.configuration.ConfigurationGenerator;
import com.bae.ai.lloydsregister.task.translator.configuration.ConfigurationMissingException;
import com.bae.ai.lloydsregister.task.translator.configuration.InvalidConfigurationException;
import com.bae.ai.lloydsregister.task.translator.configuration.ParseMode;
import com.bae.ai.lloydsregister.task.translator.input.ReadTaskRules;
import com.bae.ai.lloydsregister.task.translator.observer.NewRuleObserver;
import com.bae.ai.lloydsregister.task.translator.output.WriteTaskRules;
import com.bae.ai.lloydsregister.task.translator.rule.SimplifiedTaskRuleFactory;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRuleFactory;

public class Main
{
  private static final Logger log = Logger.getLogger("task-rules-translator");
  
  public static void main(final String[] args) throws URISyntaxException, IOException
  {
    final Properties defaults = new Properties();
    final InputStream defaultsIn = Main.class.getClassLoader().getResourceAsStream("defaults.properties");
    if(defaultsIn != null)
    {
      try
      {
        defaults.load(defaultsIn);
        defaultsIn.close();
      }
      catch(final IOException ex)
      {
        log.log(Level.WARNING, "Could not read or close the defaults input stream. Continuing on with what we have anyway.", ex);
      }
    }
    else
    {
      log.warning("Could not find a defaults.properties file to load on the classpath.");
    }
    
    ApplicationConfiguration config = null;
    try
    {
      final ConfigurationGenerator configGen = new ConfigurationGenerator();
      config = configGen.generateConfig(args, defaults);
    }
    catch(ConfigurationMissingException | InvalidConfigurationException ex)
    {
      System.out.println(ex.getMessage());
      System.exit(-1);
    }
    
    final File inputFile = new File(config.getInputFileLocation());
    if(!inputFile.exists())
    {
      System.out.println("Could not find input file: " + inputFile);
      System.exit(-1);
    }
    
    final File outputFile = new File(config.getOutputFileLocation());
    
    if(config.getTemplateFileLocation().startsWith("classpath"))
    {
      try
      {
        final InputStream templateIn = Main.class.getResourceAsStream(config.getTemplateFileLocation().substring("classpath:".length()));
        Files.copy(templateIn, outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }
      catch(final IOException ex)
      {
        System.out.println("Unable to create the output file: " + outputFile);
        System.exit(-1);
      }
    }
    else
    {
      try
      {
        final File templateFile = new File(config.getTemplateFileLocation().substring("file:".length()));
        if(!templateFile.exists())
        {
          System.out.println("Could not find template file: " + templateFile);
          System.exit(-1);
        }
        Files.copy(templateFile.toPath(), outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }
      catch(final IOException ex)
      {
        System.out.println("Unable to create the output file: " + outputFile);
        System.exit(-1);
      }
    }
    
    final TaskRuleFactory factory;
    
    if(config.getParseMode() == ParseMode.SIMPLIFIED)
    {
      factory = new SimplifiedTaskRuleFactory();
    }
    else
    {
      factory = null;
    }
    
    final NewRuleObserver output = new WriteTaskRules(outputFile, config.getOutputStartColumn(), config.getOutputStartRow());
    final ReadTaskRules input = new ReadTaskRules(inputFile, output, config.getInputRulesSheetName(), factory);
    input.run();
  }
}