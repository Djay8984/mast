package com.bae.ai.lloydsregister.task.translator.configuration;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ConfigurationMissingExceptionTest
{
  /**
   * Test that passing in a null parameter name causes an IllegalArgumentException to be thrown.
   */   
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullParameterName()
  {
    final String configurationParameter = null;
    
    new ConfigurationMissingException(configurationParameter);
  }
  
  /**
   * Test that we can get the name of the parameter that is missing.
   */
  @Test
  public void test_getMissingParameter()
  {
    final String configurationParameter = "input";
    
    final ConfigurationMissingException ex = new ConfigurationMissingException(configurationParameter);
    
    final String returnedParameter = ex.getMissingParameter();
    
    assertEquals("The returned parameter name should be input.", "input", returnedParameter);
  }
}