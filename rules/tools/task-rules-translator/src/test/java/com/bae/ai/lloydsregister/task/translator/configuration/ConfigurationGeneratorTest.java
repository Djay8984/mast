package com.bae.ai.lloydsregister.task.translator.configuration;

import java.util.Properties;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ConfigurationGeneratorTest
{
  /**
   * Test that passing in a null args argument will cause an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_generateConfig_nullArgs() throws Exception
  {
    final String[] args = null;
    final Properties defaults = new Properties();
    defaults.setProperty("input.file", "InputRules.xls");
    defaults.setProperty("output.file", "OutputRules.xls");
    defaults.setProperty("template.location", "file:template.xls");
    defaults.setProperty("parse.mode", "simplified");
    defaults.setProperty("input.sheet.name", "Rules");
    defaults.setProperty("output.start.row", "1");
    defaults.setProperty("output.start.column", "1");
    defaults.setProperty("always.multithread", "true");
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    configGen.generateConfig(args, defaults);
  }
  
  /**
   * Test that passing in a null defaults argument will cause an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_generateConfig_nullDefaults() throws Exception
  {
    final String[] args = new String[]{
      "input.file=InputRules.xls",
      "output.file=OutputRules.xls",
      "template.location=file:template.xls",
      "parse.mode=simplified",
      "input.sheet.name=Rules",
      "output.start.row=1",
      "output.start.column=1",
      "always.multithread=true"
    };
    final Properties defaults = null;
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    configGen.generateConfig(args, defaults);
  }
  
  /**
   * Test that generating configuration from only defaults yields the requested configuration.
   */
  @Test
  public void test_generateConfig_allDefaults() throws Exception
  {
    final String[] args = new String[0];
    final Properties defaults = new Properties();
    defaults.setProperty("input.file", "InputRules.xls");
    defaults.setProperty("output.file", "OutputRules.xls");
    defaults.setProperty("template.location", "file:template.xls");
    defaults.setProperty("parse.mode", "simplified");
    defaults.setProperty("input.sheet.name", "Rules");
    defaults.setProperty("output.start.row", "1");
    defaults.setProperty("output.start.column", "1");
    defaults.setProperty("always.multithread", "true");
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    final ApplicationConfiguration config = configGen.generateConfig(args, defaults);
    
    assertEquals("The configured input file should be InputRules.xls", "InputRules.xls", config.getInputFileLocation());
    assertEquals("The configured output file should be OutputRules.xls", "OutputRules.xls", config.getOutputFileLocation());
    assertEquals("The configured template file should be file:template.xls", "file:template.xls", config.getTemplateFileLocation());
    assertEquals("The configured parse mode should be simplified.", ParseMode.SIMPLIFIED, config.getParseMode());
    assertEquals("The configured input sheet name should be Rules", "Rules", config.getInputRulesSheetName());
    assertEquals("The configured output start row should be 1", (short)1, config.getOutputStartRow());
    assertEquals("The configured output start column should be 1", (short)1, config.getOutputStartColumn());
    assertTrue("The configured force multi-threading should be true", config.forceMultiThreaded());
  }
  
  /**
   * Test that generating configuration from only arguments yields the requested configuration.
   */
  @Test
  public void test_generateConfig_allArgs() throws Exception
  {
    final String[] args = new String[]{
      "input.file=InputRules.xls",
      "output.file=OutputRules.xls",
      "template.location=file:template.xls",
      "parse.mode=simplified",
      "input.sheet.name=Rules",
      "output.start.row=1",
      "output.start.column=1",
      "always.multithread=true"
    };
    final Properties defaults = new Properties();
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    final ApplicationConfiguration config = configGen.generateConfig(args, defaults);
    
    assertEquals("The configured input file should be InputRules.xls", "InputRules.xls", config.getInputFileLocation());
    assertEquals("The configured output file should be OutputRules.xls", "OutputRules.xls", config.getOutputFileLocation());
    assertEquals("The configured template file should be file:template.xls", "file:template.xls", config.getTemplateFileLocation());
    assertEquals("The configured parse mode should be simplified.", ParseMode.SIMPLIFIED, config.getParseMode());
    assertEquals("The configured input sheet name should be Rules", "Rules", config.getInputRulesSheetName());
    assertEquals("The configured output start row should be 1", (short)1, config.getOutputStartRow());
    assertEquals("The configured output start column should be 1", (short)1, config.getOutputStartColumn());
    assertTrue("The configured force multi-threading should be true", config.forceMultiThreaded());
  }
  
  /**
   * Test that an argument overrides a default configuration.
   */
  @Test
  public void test_generateConfig_argOverride() throws Exception
  {
    final String[] args = new String[]
    {
      "input.file=InputRules2.xls"
    };
    final Properties defaults = new Properties();
    defaults.setProperty("input.file", "InputRules.xls");
    defaults.setProperty("output.file", "OutputRules.xls");
    defaults.setProperty("template.location", "file:template.xls");
    defaults.setProperty("parse.mode", "simplified");
    defaults.setProperty("input.sheet.name", "Rules");
    defaults.setProperty("output.start.row", "1");
    defaults.setProperty("output.start.column", "1");
    defaults.setProperty("always.multithread", "true");
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    final ApplicationConfiguration config = configGen.generateConfig(args, defaults);
    
    assertEquals("The configured input file should be InputRules2.xls", "InputRules2.xls", config.getInputFileLocation());
    assertEquals("The configured output file should be OutputRules.xls", "OutputRules.xls", config.getOutputFileLocation());
    assertEquals("The configured template file should be file:template.xls", "file:template.xls", config.getTemplateFileLocation());
    assertEquals("The configured parse mode should be simplified.", ParseMode.SIMPLIFIED, config.getParseMode());
    assertEquals("The configured input sheet name should be Rules", "Rules", config.getInputRulesSheetName());
    assertEquals("The configured output start row should be 1", (short)1, config.getOutputStartRow());
    assertEquals("The configured output start column should be 1", (short)1, config.getOutputStartColumn());
    assertTrue("The configured force multi-threading should be true", config.forceMultiThreaded());
  }
  
  /**
   * Test that leaving out a configuration value causes a ConfigurationMissingException to be thrown.
   */
  @Test
  public void test_generateConfig_missingInputFile() throws Exception
  {
    final String[] args = new String[0];
    final Properties defaults = new Properties();
    defaults.setProperty("output.file", "OutputRules.xls");
    defaults.setProperty("template.location", "file:template.xls");
    defaults.setProperty("parse.mode", "simplified");
    defaults.setProperty("input.sheet.name", "Rules");
    defaults.setProperty("output.start.row", "1");
    defaults.setProperty("output.start.column", "1");
    defaults.setProperty("always.multithread", "true");
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    try
    {
      configGen.generateConfig(args, defaults);
      fail("A ConfigurationMissingException should be thrown.");
    }
    catch(final ConfigurationMissingException ex)
    {
      assertEquals("The missing parameter should be input.file", "input.file", ex.getMissingParameter());
    }
  }
  
  /**
   * Test that passing in an invalid configuration value causes a InvalidConfigurationException to be thrown.
   */
  @Test
  public void test_generateConfig_invalidTemplateFile() throws Exception
  {
    final String[] args = new String[0];
    final Properties defaults = new Properties();
    defaults.setProperty("input.file", "InputRules.xls");
    defaults.setProperty("output.file", "OutputRules.xls");
    defaults.setProperty("template.location", "http:template.xls");
    defaults.setProperty("parse.mode", "simplified");
    defaults.setProperty("input.sheet.name", "Rules");
    defaults.setProperty("output.start.row", "1");
    defaults.setProperty("output.start.column", "1");
    defaults.setProperty("always.multithread", "true");
    
    final ConfigurationGenerator configGen = new ConfigurationGenerator();
    try
    {
      configGen.generateConfig(args, defaults);
      fail("An InvalidConfigurationException should be thrown.");
    }
    catch(final InvalidConfigurationException ex)
    {
      assertEquals("The invalid parameter should be template.location", "template.location", ex.getParameter());
      assertEquals("The invalid value should be http:template.xls", "http:template.xls", ex.getValue());
    }
  }
}