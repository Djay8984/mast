package com.bae.ai.lloydsregister.task.translator.configuration;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ApplicationConfigurationTest
{
  /**
   * Test that passing in a null input file location causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullInputFileLocation()
  {
    final String inputFileLocation = null;
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a null output file location causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullOutputFileLocation()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = null;
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a null template file location causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullTemplateFileLocation()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = null;
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in an invalid template file prefix causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_invalidTemplateFilePrefix()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "http:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a null parse mode causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullParseMode()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = null;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a null input rules sheet name causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullInputRulesSheetName()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = null;
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a negative output start column causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_negativeOutputStartColumn()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = -1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that passing in a negative output start row causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_negativeOutputStartRow()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = -1;
    final boolean forceMultiThreaded = true;
    
    new ApplicationConfiguration(inputFileLocation, outputFileLocation, templateFileLocation,
                                 parseMode, inputRulesSheetName, outputStartColumn,
                                 outputStartRow, forceMultiThreaded);
  }
  
  /**
   * Test that we can get the configured input file location.
   */
  @Test
  public void test_getInputFileLocation()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final String returnedInputFileLocation = config.getInputFileLocation();
    
    assertEquals("The expected input file location is InputRules.xls", "InputRules.xls", 
      returnedInputFileLocation);
  }
  
  /**
   * Test that we can get the configured output file location.
   */
  @Test
  public void test_getOutputFileLocation()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final String returnedOutputFileLocation = config.getOutputFileLocation();
    
    assertEquals("The expected output file location is OutputRules.xls", "OutputRules.xls", 
      returnedOutputFileLocation);
  }
  
  /**
   * Test that we can get the configured template file location.
   */
  @Test
  public void test_getTemplateFileLocation()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final String returnedTemplateFileLocation = config.getTemplateFileLocation();
    
    assertEquals("The expected template file location is file:template.xls", "file:template.xls", 
      returnedTemplateFileLocation);
  }
  
  /**
   * Test that we can get the configured parse mode.
   */
  @Test
  public void test_getParseMode()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final ParseMode returnedParseMode = config.getParseMode();
    
    assertEquals("The expected parse mode is simplified", ParseMode.SIMPLIFIED, 
      returnedParseMode);
  }
  
  /**
   * Test that we can get the configured input sheet name.
   */
  @Test
  public void test_getInputRulesSheetName()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final String returnedInputRulesSheetName = config.getInputRulesSheetName();
    
    assertEquals("The expected input file location is Rules", "Rules", 
      returnedInputRulesSheetName);
  }
  
  /**
   * Test that we can get the configured output start column.
   */
  @Test
  public void test_getOutputStartColumn()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final short returnedOutputStartColumn = config.getOutputStartColumn();
    
    assertEquals("The expected output start column is 1", (short)1, 
      returnedOutputStartColumn);
  }
  
  /**
   * Test that we can get the configured output start row.
   */
  @Test
  public void test_getOutputStartRow()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final short returnedOutputStartRow = config.getOutputStartRow();
    
    assertEquals("The expected output start row is 11", (short)11, 
      returnedOutputStartRow);
  }
  
  /**
   * Test that we can get the configured force mutli-threaded flag.
   */
  @Test
  public void test_getForceMultiThreaded()
  {
    final String inputFileLocation = "InputRules.xls";
    final String outputFileLocation = "OutputRules.xls";
    final String templateFileLocation = "file:template.xls";
    final ParseMode parseMode = ParseMode.SIMPLIFIED;
    final String inputRulesSheetName = "Rules";
    final short outputStartColumn = 1;
    final short outputStartRow = 11;
    final boolean forceMultiThreaded = true;
    
    final ApplicationConfiguration config = new ApplicationConfiguration(inputFileLocation, outputFileLocation, 
                                                                         templateFileLocation, parseMode, inputRulesSheetName, 
                                                                         outputStartColumn, outputStartRow, forceMultiThreaded);
    
    final boolean returnedForceMultiThreaded = config.forceMultiThreaded();
    
    assertTrue("Expected force multi-threaded to be true.", returnedForceMultiThreaded);
  }
}