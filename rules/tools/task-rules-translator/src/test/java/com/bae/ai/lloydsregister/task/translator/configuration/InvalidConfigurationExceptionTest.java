package com.bae.ai.lloydsregister.task.translator.configuration;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class InvalidConfigurationExceptionTest
{
  /**
   * Test that an IllegalArgumentException is thrown if passing in a null parameter name.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullParameter()
  {
    final String parameter = null;
    final String error = "Invalid value.";
    final Object value = new Object();
    
    new InvalidConfigurationException(parameter, error, value);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown if passing in a null error.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullError()
  {
    final String parameter = "input";
    final String error = null;
    final Object value = new Object();
    
    new InvalidConfigurationException(parameter, error, value);
  }
  
  /**
   * Test that we can get the invalid parameter name.
   */
  @Test
  public void test_getParameter()
  {
    final String parameter = "input";
    final String error = "Invalid value.";
    final Object value = new Object();
    
    final InvalidConfigurationException ex = new InvalidConfigurationException(parameter, error, value);
    
    final String returnedParameter = ex.getParameter();
    
    assertEquals("The returned parameter should have the value input.", "input", returnedParameter);
  }
  
  /**
   * Test that we can get the error message associated with the parameter.
   */
  @Test
  public void test_getError()
  {
    final String parameter = "input";
    final String error = "Invalid value.";
    final Object value = new Object();
    
    final InvalidConfigurationException ex = new InvalidConfigurationException(parameter, error, value);
    
    final String returnedError = ex.getError();
    
    assertEquals("The returned error should have the value Invalid value.", "Invalid value.", returnedError);
  }
  
  /**
   * Test that we can get the value associated with the parameter.
   */
  @Test
  public void test_getValue()
  {
    final String parameter = "input";
    final String error = "Invalid value.";
    final Object value = new Object();
    
    final InvalidConfigurationException ex = new InvalidConfigurationException(parameter, error, value);
    
    final Object returnedValue = ex.getValue();
    
    assertEquals("The returned value shoudl be the same as was passed in.", value, returnedValue);
  }
}