package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class BaseTaskRuleTest
{
  private static class StubbedBaseTaskRule extends BaseTaskRule
  {
    private StubbedBaseTaskRule(final String assetItemType, final String assetType, final String classNotation,
                                final List<String> serviceCodes, final String regulationCode, final String taskType)
    {
      super(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    }
    
    public boolean hasAttribute()
    {
      return false;
    }
    
    public String getAttributeName()
    {
      return null;
    }
    
    public String getAttributeValue()
    {
      return null;
    }
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when passing in a null Asset Item type.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullAssetItemType()
  {
    final String assetItemType = null;
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when passing in a null list of service codes.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullServiceCodes()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = null;
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when passing in a null task type.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullTaskType()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = null;
    
    new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
  }
  
  /**
   * Test that we can get the Asset Item type.
   */
  @Test
  public void test_getAssetItemType()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final String returnedAssetItemType = rule.getAssetItemType();
    
    assertEquals("The returned asset item type should be CHAIN CABLE.", "CHAIN CABLE", returnedAssetItemType);
  }
  
  /**
   * Test that we can get the Asset type.
   */
  @Test
  public void test_getAssetType()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final String returnedAssetType = rule.getAssetType();
    
    assertEquals("The returned asset type should be A11.", "A11", returnedAssetType);
  }
  
  /**
   * Test that can get the Class Notation.
   */
  @Test
  public void test_getClassNotation()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final String returnedClassNotation = rule.getClassNotation();
    
    assertEquals("The returned class notation should be UMS", "UMS", returnedClassNotation);
  }
  
  /**
   * Test that we can get the Service Codes.
   */
  @Test
  public void test_getServiceCodes()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    serviceCodes.add("SS");
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final List<String> returnedServiceCodes = rule.getServiceCodes();
    
    assertNotNull("The returned service codes list should not be null.", returnedServiceCodes);
    assertEquals("The number of service codes should be 1.", 1, returnedServiceCodes.size());
    assertEquals("The returned service code should be SS.", "SS", returnedServiceCodes.get(0));
  }
  
  /**
   * Test that we can get the Regulation Code.
   */
  @Test
  public void test_getRegulationCode()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final String returnedRegulationCode = rule.getRegulationCode();
    
    assertEquals("The returned regulation code should be ANY.", "ANY", returnedRegulationCode);
  }
  
  /**
   * Test that we can get the Task Type.
   */
  @Test
  public void test_getTaskType()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final BaseTaskRule rule = new StubbedBaseTaskRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final String returnedTaskType = rule.getTaskType();
    
    assertEquals("The returned task tyoe should be Examine.", "Examine", returnedTaskType);
  }
}