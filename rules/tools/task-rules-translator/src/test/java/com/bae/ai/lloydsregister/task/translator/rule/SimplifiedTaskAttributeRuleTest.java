package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SimplifiedTaskAttributeRuleTest
{
  /**
   * Test that passing in a null attribute name causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullAttributeName()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = null;
    final String attributeValue = "FRESH WATER";
    
    new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode,
                                    taskType, attributeName, attributeValue);
  }
  
  /**
   * Test that passing in a null attribute value causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullAttributeValue()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = "PRIMARY SERVICE";
    final String attributeValue = null;
    
    new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode,
                                    taskType, attributeName, attributeValue);
  }
  
  /**
   * Test that hasAttribute returns true.
   */
  @Test
  public void test_hasAttribute()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = "PRIMARY SERVICE";
    final String attributeValue = "FRESH WATER";
    
    SimplifiedTaskAttributeRule rule = new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes,
                                                                       regulationCode, taskType, attributeName, attributeValue);
    
    final boolean returnedHasAttribute = rule.hasAttribute();
    
    assertTrue("The value of hasAttribute should be true.", returnedHasAttribute);
  }
  
  /**
   * Test that we can get the name of the attribute.
   */
  @Test
  public void test_getAttributeName()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = "PRIMARY SERVICE";
    final String attributeValue = "FRESH WATER";
    
    SimplifiedTaskAttributeRule rule = new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes,
                                                                       regulationCode, taskType, attributeName, attributeValue);
    
    final String returnedAttributeName = rule.getAttributeName();
    
    assertEquals("The returned attribute name should be PRIMARY SERVICE.", "PRIMARY SERVICE", returnedAttributeName);
  }
  
  /**
   * Test that we can get the value of the attribute.
   */
  @Test
  public void test_getAttributeValue()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = "PRIMARY SERVICE";
    final String attributeValue = "FRESH WATER";
    
    SimplifiedTaskAttributeRule rule = new SimplifiedTaskAttributeRule(assetItemType, assetType, classNotation, serviceCodes,
                                                                       regulationCode, taskType, attributeName, attributeValue);
    
    final String returnedAttributeValue = rule.getAttributeValue();
    
    assertEquals("The returned attribute value should be FRESH WATER.", "FRESH WATER", returnedAttributeValue);
  }
}