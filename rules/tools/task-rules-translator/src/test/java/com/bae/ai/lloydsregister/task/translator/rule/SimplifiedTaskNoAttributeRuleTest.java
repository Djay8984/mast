package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SimplifiedTaskNoAttributeRuleTest
{
  /**
   * Test that hasAttribute returns false.
   */
  @Test
  public void test_hasAttribute()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    
    final SimplifiedTaskNoAttributeRule rule = 
      new SimplifiedTaskNoAttributeRule(assetItemType, assetType, classNotation, serviceCodes, regulationCode, taskType);
    
    final boolean returnedHasAttribute = rule.hasAttribute();
    
    assertFalse("The value of hasAttribute should be false.", returnedHasAttribute);
  }
}