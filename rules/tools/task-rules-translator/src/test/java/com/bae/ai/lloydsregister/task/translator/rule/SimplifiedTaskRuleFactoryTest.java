package com.bae.ai.lloydsregister.task.translator.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SimplifiedTaskRuleFactoryTest
{
  /**
   * Test that we can create a TaskRule that checks an attribute.
   */
  @Test
  public void test_createRuleAttribute()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = "PRIMARY SERVICE";
    final String attributeValue = "FRESH WATER";
    
    final SimplifiedTaskRuleFactory factory = new SimplifiedTaskRuleFactory();
    final TaskRule rule = factory.createRule(assetItemType, attributeName, attributeValue, assetType,
                                             classNotation, serviceCodes, regulationCode, taskType);
    
    assertEquals("The Asset Item type should be CHAIN CABLE.", "CHAIN CABLE", rule.getAssetItemType());
    assertEquals("The Asset type should be A11.", "A11", rule.getAssetType());
    assertEquals("The Class Notation should be UMS.", "UMS", rule.getClassNotation());
    assertEquals("The list of Service Codes should be empty", 0, rule.getServiceCodes().size());
    assertEquals("The Task Type should be Examine.", "Examine", rule.getTaskType());
    assertTrue("The value of hasAttribute should be true.", rule.hasAttribute());
    assertEquals("The name of the attribute should be PRIMARY SERVICE.", "PRIMARY SERVICE", rule.getAttributeName());
    assertEquals("The value of the attribute should be FRESH WATER.", "FRESH WATER", rule.getAttributeValue());
  }
  
  /**
   * Test that we can create a TaskRule that does not check an attribute.
   */
  @Test
  public void test_createRuleNoAttribute()
  {
    final String assetItemType = "CHAIN CABLE";
    final String assetType = "A11";
    final String classNotation = "UMS";
    final List<String> serviceCodes = new ArrayList<>();
    final String regulationCode = "ANY";
    final String taskType = "Examine";
    final String attributeName = null;
    final String attributeValue = null;
    
    final SimplifiedTaskRuleFactory factory = new SimplifiedTaskRuleFactory();
    final TaskRule rule = factory.createRule(assetItemType, attributeName, attributeValue, assetType,
                                             classNotation, serviceCodes, regulationCode, taskType);
    
    assertEquals("The Asset Item type should be CHAIN CABLE.", "CHAIN CABLE", rule.getAssetItemType());
    assertEquals("The Asset type should be A11.", "A11", rule.getAssetType());
    assertEquals("The Class Notation should be UMS.", "UMS", rule.getClassNotation());
    assertEquals("The list of Service Codes should be empty", 0, rule.getServiceCodes().size());
    assertEquals("The Task Type should be Examine.", "Examine", rule.getTaskType());
    assertFalse("The value of hasAttribute should be false.", rule.hasAttribute());
  }
}