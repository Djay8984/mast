package com.bae.ai.lloydsregister.task.translator.input;

import java.io.File;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import com.bae.ai.lloydsregister.task.translator.observer.NewRuleObserver;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;
import com.bae.ai.lloydsregister.task.translator.rule.TaskRuleFactory;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class ReadTaskRulesTest
{
  /**
   * Test that an IllegalArgumentException is thrown when a null input file
   * is passed in.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullInputFile()
  {
    final File inputFile = null;
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = "Rules";
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    
    new ReadTaskRules(inputFile, observer, sheetName, factory);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when a null observer
   * is passed in.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullObserver() throws Exception
  {
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input1.xls").getFile());
    final NewRuleObserver observer = null;
    final String sheetName = "Rules";
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    
    new ReadTaskRules(inputFile, observer, sheetName, factory);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when a null sheet name
   * is passed in.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullSheetName() throws Exception
  {
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input1.xls").getFile());
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = null;
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    
    new ReadTaskRules(inputFile, observer, sheetName, factory);
  }
  
  /**
   * Test that an IllegalArgumentException is thrown when a null task rule factory
   * is passed in.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullTaskRuleFactory() throws Exception
  {
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input1.xls").getFile());
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = "Rules";
    final TaskRuleFactory factory = null;
    
    new ReadTaskRules(inputFile, observer, sheetName, factory);
  }
  
  /**
   * Test that we can read an input file with two rules in and these rules get pushed to the
   * observer.
   */
  @Test
  public void test_run_readTwoRules()
  {
    final TaskRule firstRule = mock(TaskRule.class);
    final TaskRule secondRule = mock(TaskRule.class);

    final List<String> serviceCodes = Arrays.asList(new String[]{"SS","DS"});
    
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input1.xls").getFile());
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = "Rules";
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    when(factory.createRule(eq("CHAIN CABLE"), isNull(String.class), isNull(String.class), isNull(String.class), 
                            isNull(String.class), eq(serviceCodes), isNull(String.class), eq("DS#001")))
    .thenReturn(firstRule);
    
    when(factory.createRule(eq("CHAIN CABLE"), isNull(String.class), isNull(String.class), isNull(String.class), 
                            isNull(String.class), eq(serviceCodes), isNull(String.class), eq("BS#022")))
    .thenReturn(secondRule);
    
    final ReadTaskRules reader = new ReadTaskRules(inputFile, observer, sheetName, factory);
    reader.run();
    
    verify(observer).onNewRule(firstRule);
    verify(observer).onNewRule(secondRule);
    verify(observer).onComplete();
  }
  
  /**
   * Test that we can read an input file with one rule that contains an attribute condition.
   */
  @Test
  public void test_run_readAttributeRule()
  {
    final TaskRule rule = mock(TaskRule.class);
    
    final List<String> serviceCodes = Arrays.asList(new String[]{"SS","DS"});
    
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input2.xls").getFile());
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = "Rules";
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    when(factory.createRule(eq("CHAIN LOCKER"), eq("PROTECTIVE COATING FITTED?"), eq("YES"), isNull(String.class), 
                            isNull(String.class), eq(serviceCodes), isNull(String.class), eq("DS#001")))
    .thenReturn(rule);
    
    final ReadTaskRules reader = new ReadTaskRules(inputFile, observer, sheetName, factory);
    reader.run();
    
    verify(observer).onNewRule(rule);
    verify(observer).onComplete();
  }
  
  /**
   * Test that if there is no sheet matching the specified rule sheet then no rules get read.
   */
  @Test
  public void test_run_readWrongSheet()
  {
    final File inputFile = new File(this.getClass().getClassLoader().getResource("input3.xls").getFile());
    final NewRuleObserver observer = mock(NewRuleObserver.class);
    final String sheetName = "Rules";
    final TaskRuleFactory factory = mock(TaskRuleFactory.class);
    
    final ReadTaskRules reader = new ReadTaskRules(inputFile, observer, sheetName, factory);
    reader.run();
    
    verify(observer).onComplete();
    verifyNoMoreInteractions(observer);
  }
}