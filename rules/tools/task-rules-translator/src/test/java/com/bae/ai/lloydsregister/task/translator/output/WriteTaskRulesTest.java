package com.bae.ai.lloydsregister.task.translator.output;

import java.io.File;

import java.util.ArrayList;

import com.bae.ai.lloydsregister.task.translator.rule.TaskRule;
import com.bae.ai.lloydsregister.task.translator.rule.SimplifiedTaskNoAttributeRule;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class WriteTaskRulesTest
{
  /**
   * Test that passing in a null output file causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullOutputFile() throws Exception
  {
    final File outputFile = null;
    final short outputColumn = 0;
    final short outputRow = 0;
    
    new WriteTaskRules(outputFile, outputColumn, outputRow);
  }
  
  /**
   * Test that passing in a negative column number causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_negativeColumn() throws Exception
  {
    final File outputFile = File.createTempFile("test_constructor_negativeColumn", "xls");
    final short outputColumn = -1;
    final short outputRow = 0;
    
    new WriteTaskRules(outputFile, outputColumn, outputRow);
  }
  
  /**
   * Test that passing in a negative row number causes and IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_negativeRow() throws Exception
  {
    final File outputFile = File.createTempFile("test_constructor_negativeRow", "xls");
    final short outputColumn = 0;
    final short outputRow = -1;
    
    new WriteTaskRules(outputFile, outputColumn, outputRow);
  }
  
  /**
   * Dummy test
   */
  //@Test
  public void test_dummyTest() throws Exception
  {
    final File outputFile = new File("D:\\Users\\PATitcombe\\test.xls");
    final short outputColumn = 0;
    final short outputRow = 10;
    
    final WriteTaskRules writer = new WriteTaskRules(outputFile, outputColumn, outputRow);
    
    final TaskRule rule = new SimplifiedTaskNoAttributeRule("itemType", "assetType", "classNotation", 
                                                            new ArrayList<String>(), "ANY", "taskType");
    writer.onNewRule(rule);
    writer.onComplete();
  }
}