-- --------------------------------------------------------
-- Host:                         10.20.2.92
-- Server version:               5.1.73 - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for MAST_DB
DROP DATABASE IF EXISTS `MAST_DB`;
CREATE DATABASE IF NOT EXISTS `MAST_DB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `MAST_DB`;

SET FOREIGN_KEY_CHECKS=0;


-- Dumping structure for table MAST_DB.MAST_ASSET_Asset
CREATE TABLE IF NOT EXISTS `MAST_ASSET_Asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imo_number` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `build_date` datetime DEFAULT NULL,
  `building_contract_date` date DEFAULT NULL,
  `call_sign` varchar(18) DEFAULT NULL,
  `commission_date` date DEFAULT NULL,
  `class_status_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `keel_laying_date` datetime DEFAULT NULL,
  `gross_tonnage` double DEFAULT NULL,
  `harmonisation_date` datetime DEFAULT NULL,
  `hull_indicator` tinyint(4) DEFAULT NULL,
  `ihs_asset_type` varchar(30) DEFAULT NULL,
  `ihs_yard_number` varchar(20) DEFAULT NULL,
  `lead_asset_flag` tinyint(1) DEFAULT NULL,
  `lead_imo` bigint(20) DEFAULT NULL,
  `primary_yard_number` varchar(10) DEFAULT NULL,
  `yard_name` varchar(100) DEFAULT NULL,
  `asset_category_id` smallint(6) DEFAULT NULL,
  `asset_lifecycle_status_id` smallint(6) DEFAULT NULL,
  `asset_type_id` smallint(6) DEFAULT NULL,
  `builder` char(100) DEFAULT NULL,
  `build_country_id` smallint(6) DEFAULT NULL,
  `class_maintenance_status_id` smallint(6) DEFAULT NULL,
  `class_department_id` smallint(6) DEFAULT NULL,
  `class_status_id` smallint(6) DEFAULT NULL,
  `society_regulations_id` smallint(6) DEFAULT NULL,
  `previous_society_regulations_id` smallint(6) DEFAULT NULL,
  `current_flag_state_id` smallint(6) DEFAULT NULL,
  `proposed_flag_state_id` smallint(6) DEFAULT NULL,
  `registered_port_id` smallint(6) DEFAULT NULL,
  `migration_status_id` smallint(6) DEFAULT NULL,
  `service_ruleset_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `checked_out_to_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `checked_out_to_id` (`checked_out_to_id`),
  KEY `class_maintenance_status_id` (`class_maintenance_status_id`),
  KEY `class_department_id` (`class_department_id`),
  KEY `build_country_id` (`build_country_id`),
  KEY `current_flag_state_id` (`current_flag_state_id`),
  KEY `migration_status_id` (`migration_status_id`),
  KEY `service_ruleset_id` (`service_ruleset_id`),
  KEY `asset_lifecycle_status_id` (`asset_lifecycle_status_id`),
  KEY `asset_type_id` (`asset_type_id`),
  KEY `asset_category_id` (`asset_category_id`),
  KEY `class_status_id` (`class_status_id`),
  KEY `proposed_flag_state_id` (`proposed_flag_state_id`),
  KEY `registered_port_id` (`registered_port_id`),
  KEY `society_regulations_id` (`society_regulations_id`),
  KEY `previous_society_regulations_id` (`previous_society_regulations_id`),
  KEY `IND_imo_number` (`imo_number`),
  CONSTRAINT `FK_ASSET_RULESET2_PREVIOUS` FOREIGN KEY (`previous_society_regulations_id`) REFERENCES `MAST_REF_RegulationsSet` (`id`),
  CONSTRAINT `FK_ASSET_ASSET_LIFECYCLE_STATUS` FOREIGN KEY (`asset_lifecycle_status_id`) REFERENCES `MAST_REF_AssetLifecycleStatus` (`id`),
  CONSTRAINT `FK_ASSET_ASSET_TYPE` FOREIGN KEY (`asset_type_id`) REFERENCES `MAST_REF_AssetType` (`id`),
  CONSTRAINT `FK_ASSET_CATEGORY` FOREIGN KEY (`asset_category_id`) REFERENCES `MAST_REF_AssetCategory` (`id`),
  CONSTRAINT `FK_ASSET_CLASS_STATUS` FOREIGN KEY (`class_status_id`) REFERENCES `MAST_REF_ClassStatus` (`id`),
  CONSTRAINT `FK_ASSET_FLAG_ADMINISTRATION` FOREIGN KEY (`proposed_flag_state_id`) REFERENCES `MAST_REF_FlagAdministration` (`id`),
  CONSTRAINT `FK_Asset_LrEmployee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_ASSET_PORT` FOREIGN KEY (`registered_port_id`) REFERENCES `MAST_REF_Port` (`id`),
  CONSTRAINT `FK_ASSET_RULESET2` FOREIGN KEY (`society_regulations_id`) REFERENCES `MAST_REF_RegulationsSet` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_LRP_Employee` FOREIGN KEY (`checked_out_to_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_ClassMaintenanceStatus` FOREIGN KEY (`class_maintenance_status_id`) REFERENCES `MAST_REF_ClassMaintenanceStatus` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_ClassSection` FOREIGN KEY (`class_department_id`) REFERENCES `MAST_REF_ClassDepartment` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_Country` FOREIGN KEY (`build_country_id`) REFERENCES `MAST_REF_Country` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_FlagAdministration` FOREIGN KEY (`current_flag_state_id`) REFERENCES `MAST_REF_FlagAdministration` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_MigrationStatus` FOREIGN KEY (`migration_status_id`) REFERENCES `MAST_REF_MigrationStatus` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_MAST_REF_ServiceRuleset` FOREIGN KEY (`service_ruleset_id`) REFERENCES `MAST_REF_ServiceRuleset` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_Asset: ~6 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_Asset` DISABLE KEYS */;
INSERT INTO `MAST_ASSET_Asset` (`id`, `imo_number`, `name`, `build_date`, `building_contract_date`, `call_sign`, `commission_date`, `class_status_date`, `delivery_date`, `keel_laying_date`, `gross_tonnage`, `harmonisation_date`, `hull_indicator`, `ihs_asset_type`, `ihs_yard_number`, `lead_asset_flag`, `lead_imo`, `primary_yard_number`, `yard_name`, `asset_category_id`, `asset_lifecycle_status_id`, `asset_type_id`, `builder`, `build_country_id`, `class_maintenance_status_id`, `class_department_id`, `class_status_id`, `society_regulations_id`, `previous_society_regulations_id`, `current_flag_state_id`, `proposed_flag_state_id`, `registered_port_id`, `migration_status_id`, `service_ruleset_id`, `deleted`, `last_update_date`, `last_update_user_id`, `checked_out_to_id`) VALUES
	(1, 1000019, 'Yeolde asset', '2006-04-05 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 100, '2016-04-05 00:00:00', 2, NULL, NULL, 1, NULL, 'Yard1', NULL, 12, 3, 7, '1', NULL, NULL, NULL, 1, NULL, NULL, NULL, 100, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(2, 1000021, 'HMS Ship', '2014-08-19 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 1000, '2001-01-02 00:00:00', 0, NULL, NULL, 1, NULL, '12', NULL, 3, 2, 2, '2', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(3, 1000033, 'Other Ship', '2013-08-19 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 2000, '2001-01-03 00:00:00', 0, NULL, NULL, 0, NULL, '12', NULL, 3, 3, 3, '1', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(5, 1000045, 'Complete Ship', '2011-08-19 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 2000, '2005-01-01 00:00:00', 1, NULL, NULL, 0, NULL, '12', NULL, 5, 3, 5, '1', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(6, 1000057, 'Incomplete Ship', '2016-04-05 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 2000, '2006-01-01 00:00:00', 1, NULL, NULL, 0, NULL, '12', NULL, 7, 3, 6, '1', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL),
	(7, 1000069, 'ABouyC', '2016-04-05 00:00:00', NULL, NULL, '2016-04-20', NULL, NULL, NULL, 2, '2001-07-01 00:00:00', 0, NULL, NULL, 0, NULL, '6', NULL, 6, 3, 7, '1', NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_ASSET_Asset` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_AssetItem
CREATE TABLE IF NOT EXISTS `MAST_ASSET_AssetItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_name` varchar(200) NOT NULL,
  `long_name` varchar(500) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `item_type_id` smallint(6) NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `parent_item_id` int(11) DEFAULT NULL,
  `review_status_id` smallint(6) NOT NULL,
  `item_status_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `long_name` (`long_name`),
  KEY `item_status_id` (`item_status_id`),
  KEY `review_status_id` (`review_status_id`),
  KEY `asset_id` (`asset_id`),
  KEY `parent_item_id` (`parent_item_id`),
  KEY `item_type_id` (`item_type_id`),
  CONSTRAINT `FK_ITEM_ITEM_TYPE` FOREIGN KEY (`item_type_id`) REFERENCES `MAST_REF_AssetItemType` (`id`),
  CONSTRAINT `FK_ITEM_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_ITEM_ITEM` FOREIGN KEY (`parent_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetItem_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetItem_MAST_REF_AssetItemReferenceCode` FOREIGN KEY (`long_name`) REFERENCES `MAST_REF_AssetItemReferenceCode` (`asset_item_long_name`),
  CONSTRAINT `FK_MAST_ASSET_AssetItem_MAST_REF_ItemStatus` FOREIGN KEY (`item_status_id`) REFERENCES `MAST_REF_ItemStatus` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetItem_MAST_REF_ReviewStatus` FOREIGN KEY (`review_status_id`) REFERENCES `MAST_REF_ItemReviewStatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_AssetItem: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_AssetItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_AssetItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_AssetItemAttribute
CREATE TABLE IF NOT EXISTS `MAST_ASSET_AssetItemAttribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `attribute_type_id` smallint(6) NOT NULL,
  `value` varchar(250) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `attribute_type_id` (`attribute_type_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `FK_ATTRIBUTE_ITEM` FOREIGN KEY (`item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_ATTRIBUTE_ATTRIBUTE_TYPE` FOREIGN KEY (`attribute_type_id`) REFERENCES `MAST_REF_AttributeDataType` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetAttribute_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_AssetItemAttribute: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_AssetItemAttribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_AssetItemAttribute` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_AssetItemRelationship
CREATE TABLE IF NOT EXISTS `MAST_ASSET_AssetItemRelationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_item_id` int(11) NOT NULL,
  `to_item_id` int(11) NOT NULL,
  `relationship_type_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `from_item_id` (`from_item_id`),
  KEY `to_item_id` (`to_item_id`),
  KEY `relationship_type_id` (`relationship_type_id`),
  CONSTRAINT `FK_ITEM_RELATIONSHIP_RELATIONSHIP_TYPE` FOREIGN KEY (`relationship_type_id`) REFERENCES `MAST_REF_AssetItemRelationshipType` (`id`),
  CONSTRAINT `FK_ITEM_RELATIONSHIP_ITEM_FROM` FOREIGN KEY (`from_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_ITEM_RELATIONSHIP_ITEM_TO` FOREIGN KEY (`to_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetItemRelationship_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_AssetItemRelationship: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_AssetItemRelationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_AssetItemRelationship` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_AssetProduct
CREATE TABLE IF NOT EXISTS `MAST_ASSET_AssetProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_catalogue_id` smallint(6) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `asset_id` (`asset_id`),
  KEY `product_catalogue_id` (`product_catalogue_id`),
  CONSTRAINT `FK_PRODUCT_PRODUCT_CATALOGUE` FOREIGN KEY (`product_catalogue_id`) REFERENCES `MAST_REF_ProductCatalogue` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetProduct_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_PRODUCT_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_AssetProduct: ~18 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_AssetProduct` DISABLE KEYS */;
INSERT INTO `MAST_ASSET_AssetProduct` (`id`, `product_catalogue_id`, `asset_id`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 1, 1, 1, NULL, NULL),
	(2, 2, 2, 0, NULL, NULL),
	(3, 3, 1, 0, NULL, NULL),
	(4, 1, 2, 0, NULL, NULL),
	(5, 5, 5, 0, NULL, NULL),
	(6, 5, 1, 0, NULL, NULL),
	(7, 7, 1, 0, NULL, NULL),
	(8, 8, 1, 0, NULL, NULL),
	(9, 17, 1, 0, NULL, NULL),
	(10, 25, 1, 0, NULL, NULL),
	(11, 38, 1, 0, NULL, NULL),
	(12, 44, 1, 0, NULL, NULL),
	(13, 46, 1, 0, NULL, NULL),
	(14, 2, 1, 0, NULL, NULL),
	(15, 10, 1, 0, NULL, NULL),
	(16, 27, 1, 0, NULL, NULL),
	(17, 34, 1, 0, NULL, NULL),
	(18, 32, 1, 0, NULL, NULL);
/*!40000 ALTER TABLE `MAST_ASSET_AssetProduct` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_AssetRelationship
CREATE TABLE IF NOT EXISTS `MAST_ASSET_AssetRelationship` (
  `id` int(11) NOT NULL,
  `from_asset_id` int(11) DEFAULT NULL,
  `to_asset_id` int(11) DEFAULT NULL,
  `relationship_type_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `relationship_type_id` (`relationship_type_id`),
  KEY `from_asset_id` (`from_asset_id`),
  KEY `to_asset_id` (`to_asset_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AssetRelationship_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_AssetRelationship_MAST_REF_AssetRelationshipType` FOREIGN KEY (`relationship_type_id`) REFERENCES `MAST_REF_AssetRelationshipType` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetRelationship_MAST_ASSET_Asset` FOREIGN KEY (`from_asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetRelationship_MAST_ASSET_Asset_02` FOREIGN KEY (`to_asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_AssetRelationship: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_AssetRelationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_AssetRelationship` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_Asset_ClassSociety
CREATE TABLE IF NOT EXISTS `MAST_ASSET_Asset_ClassSociety` (
  `id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `class_society_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_id` (`asset_id`),
  KEY `class_society_id` (`class_society_id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_ClassSociety_MAST_REF_ClassSociety` FOREIGN KEY (`class_society_id`) REFERENCES `MAST_REF_ClassSociety` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_ClassSociety_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_Asset_ClassSociety: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_Asset_ClassSociety` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_Asset_ClassSociety` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_Asset_Party
CREATE TABLE IF NOT EXISTS `MAST_ASSET_Asset_Party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `party_role_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `party_role_id` (`party_role_id`),
  KEY `asset_id` (`asset_id`),
  KEY `party_id` (`party_id`),
  CONSTRAINT `FK_CUSTOMER_FUNCTION_PARTY` FOREIGN KEY (`party_id`) REFERENCES `MAST_CDH_Party` (`id`),
  CONSTRAINT `FK_CUSTOMER_FUNCTION_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_Party_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_Party_MAST_REF_PartyRole` FOREIGN KEY (`party_role_id`) REFERENCES `MAST_REF_PartyRole` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_Asset_Party: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_Asset_Party` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_Asset_Party` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_Asset_ServiceCatalogue
CREATE TABLE IF NOT EXISTS `MAST_ASSET_Asset_ServiceCatalogue` (
  `id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `service_catalogue_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_id` (`asset_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `service_catalogue_id` (`service_catalogue_id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_ServiceCatalogue_MAST_REF_ServiceCatalogue` FOREIGN KEY (`service_catalogue_id`) REFERENCES `MAST_REF_ServiceCatalogue` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_ServiceCatalogue_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Asset_ServiceCatalogue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_Asset_ServiceCatalogue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_Asset_ServiceCatalogue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_Asset_ServiceCatalogue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_Codicil
CREATE TABLE IF NOT EXISTS `MAST_ASSET_Codicil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_code` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `imposed_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `narrative` varchar(2000) NOT NULL,
  `action_taken_id` smallint(6) DEFAULT NULL,
  `inherited_flag` tinyint(1) NOT NULL,
  `editable_by_surveyor` tinyint(1) DEFAULT NULL,
  `surveyor_guidance` varchar(2000) DEFAULT NULL,
  `defect_id` int(11) DEFAULT NULL,
  `status_id` smallint(6) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_item_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `category_id` smallint(6) NOT NULL,
  `wip_flag` tinyint(1) NOT NULL,
  `confidentiality_type_id` smallint(6) NOT NULL,
  `codicil_type_id` tinyint(4) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_item_id` (`asset_item_id`),
  KEY `category_id` (`category_id`),
  KEY `template_id` (`template_id`),
  KEY `codicil_type_id` (`codicil_type_id`),
  KEY `confidentiality_type_id` (`confidentiality_type_id`),
  KEY `defect_id` (`defect_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `asset_id` (`asset_id`),
  KEY `action_taken_id` (`action_taken_id`),
  KEY `employee_id` (`employee_id`),
  KEY `job_id` (`job_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `FK_Codicil_CodicilStatus` FOREIGN KEY (`status_id`) REFERENCES `MAST_REF_CodicilStatus` (`id`),
  CONSTRAINT `FK_Codicil_AssetItem` FOREIGN KEY (`asset_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_Codicil_CodicilCategory` FOREIGN KEY (`category_id`) REFERENCES `MAST_REF_CodicilCategory` (`id`),
  CONSTRAINT `FK_Codicil_CodicilTemplate` FOREIGN KEY (`template_id`) REFERENCES `MAST_REF_CodicilTemplate` (`id`),
  CONSTRAINT `FK_Codicil_CodicilType` FOREIGN KEY (`codicil_type_id`) REFERENCES `MAST_REF_CodicilType` (`id`),
  CONSTRAINT `FK_Codicil_ConfidentialityType` FOREIGN KEY (`confidentiality_type_id`) REFERENCES `MAST_REF_ConfidentialityType` (`id`),
  CONSTRAINT `FK_Codicil_Defect` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`),
  CONSTRAINT `FK_Codicil_Employee` FOREIGN KEY (`employee_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_Codicil_Job` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_Codicil_LRPEmployee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Codicil_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_ASSET_Codicil_MAST_REF_ActionTaken` FOREIGN KEY (`action_taken_id`) REFERENCES `MAST_REF_ActionTaken` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_Codicil: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_Codicil` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_ASSET_Codicil` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_ASSET_ScheduledService
CREATE TABLE IF NOT EXISTS `MAST_ASSET_ScheduledService` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `service_catalogue_id` smallint(6) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cycle_periodicity` int(11) NOT NULL,
  `assigned_date` datetime NOT NULL,
  `due_date` date DEFAULT NULL,
  `lower_range_date` date DEFAULT NULL,
  `upper_range_date` date DEFAULT NULL,
  `scheduling_regime_id` smallint(6) NOT NULL,
  `service_status_id` smallint(6) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `provisional_date_flag` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `service_credit_status_id` smallint(6) DEFAULT NULL,
  `crediting_date` datetime DEFAULT NULL,
  `service_cycle_number` smallint(6) DEFAULT NULL,
  `service_occurence_number` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_id` (`asset_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `service_credit_status_id` (`service_credit_status_id`),
  KEY `scheduling_regime_id` (`scheduling_regime_id`),
  KEY `service_catalogue_id` (`service_catalogue_id`),
  KEY `FK_MAST_ASSET_ScheduledService_MAST_REF_ServiceStatus` (`service_status_id`),
  CONSTRAINT `FK_MAST_ASSET_ScheduledService_MAST_REF_ServiceStatus` FOREIGN KEY (`service_status_id`) REFERENCES `MAST_REF_ServiceStatus` (`id`),
  CONSTRAINT `FK_MAST_ASSET_ScheduledService_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_ASSET_ScheduledService_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_ASSET_ScheduledService_MAST_REF_ServiceCreditStatus` FOREIGN KEY (`service_credit_status_id`) REFERENCES `MAST_REF_ServiceCreditStatus` (`id`),
  CONSTRAINT `FK_SERVICE_SCHEDULING_Regime` FOREIGN KEY (`scheduling_regime_id`) REFERENCES `MAST_REF_SchedulingRegime` (`id`),
  CONSTRAINT `FK_SERVICE_SERVICE_CATALOGUE` FOREIGN KEY (`service_catalogue_id`) REFERENCES `MAST_REF_ServiceCatalogue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_ASSET_ScheduledService: ~5 rows (approximately)
/*!40000 ALTER TABLE `MAST_ASSET_ScheduledService` DISABLE KEYS */;
INSERT INTO `MAST_ASSET_ScheduledService` (`id`, `asset_id`, `service_catalogue_id`, `product_id`, `cycle_periodicity`, `assigned_date`, `due_date`, `lower_range_date`, `upper_range_date`, `scheduling_regime_id`, `service_status_id`, `last_update_date`, `completion_date`, `last_update_user_id`, `provisional_date_flag`, `deleted`, `service_credit_status_id`, `crediting_date`, `service_cycle_number`, `service_occurence_number`) VALUES
	(1, 1, 1, 3, 12, '2015-01-01 00:00:00', '2017-01-01', NULL, NULL, 1, 7, NULL, '2016-04-19', NULL, 0, 0, NULL, NULL, NULL, 1),
	(2, 1, 6, 3, 60, '0000-00-00 00:00:00', NULL, NULL, NULL, 1, 2, NULL, '2015-11-20', NULL, 0, 0, NULL, NULL, NULL, 1),
	(4, 2, 10, 4, 12, '0000-00-00 00:00:00', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL),
	(6, 1, 1, 3, 3, '0000-00-00 00:00:00', '2016-04-04', NULL, NULL, 1, 2, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1),
	(7, 7, 11, 3, 60, '0000-00-00 00:00:00', '2016-04-04', NULL, NULL, 1, 2, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1);
/*!40000 ALTER TABLE `MAST_ASSET_ScheduledService` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CASE_Case
CREATE TABLE IF NOT EXISTS `MAST_CASE_Case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `case_type_id` smallint(6) NOT NULL,
  `case_status_id` smallint(6) NOT NULL,
  `pre_eic_inspection_status_id` smallint(6) DEFAULT NULL,
  `risk_assessment_status_id` smallint(6) DEFAULT NULL,
  `business_process_id` smallint(6) DEFAULT NULL,
  `toc_acceptance_date` datetime DEFAULT NULL,
  `flag_state_id` smallint(6) DEFAULT NULL,
  `proposed_iacs_society_id` smallint(6) DEFAULT NULL,
  `contract_reference` varchar(30) DEFAULT NULL,
  `_maintainence_regime` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_MAST_CASE_Case_mast_lrp_employee` (`last_update_user_id`),
  KEY `asset_id` (`asset_id`),
  KEY `business_process_id` (`business_process_id`),
  KEY `case_status_id` (`case_status_id`),
  KEY `case_type_id` (`case_type_id`),
  KEY `proposed_iacs_society_id` (`proposed_iacs_society_id`),
  KEY `flag_state_id` (`flag_state_id`),
  KEY `pre_eic_inspection_status_id` (`pre_eic_inspection_status_id`),
  KEY `risk_assessment_status_id` (`risk_assessment_status_id`),
  CONSTRAINT `FK_CASE_RISK_ASSESSMENT_STATUS` FOREIGN KEY (`risk_assessment_status_id`) REFERENCES `MAST_REF_RiskAssessmentStatus` (`id`),
  CONSTRAINT `FK_CASE_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_CASE_BUSINESS_PROCESS` FOREIGN KEY (`business_process_id`) REFERENCES `MAST_REF_BusinessProcess` (`id`),
  CONSTRAINT `FK_CASE_CASE_STATUS` FOREIGN KEY (`case_status_id`) REFERENCES `MAST_REF_CaseStatus` (`id`),
  CONSTRAINT `FK_CASE_CASE_TYPE` FOREIGN KEY (`case_type_id`) REFERENCES `MAST_REF_CaseType` (`id`),
  CONSTRAINT `FK_CASE_CLASS_SOCIETY` FOREIGN KEY (`proposed_iacs_society_id`) REFERENCES `MAST_REF_ClassSociety` (`id`),
  CONSTRAINT `FK_CASE_FLAG_STATE` FOREIGN KEY (`flag_state_id`) REFERENCES `MAST_REF_FlagAdministration` (`id`),
  CONSTRAINT `FK_CASE_PRE_EIC_INSPECTION_STATUS` FOREIGN KEY (`pre_eic_inspection_status_id`) REFERENCES `MAST_REF_PreEICInspectionStatus` (`id`),
  CONSTRAINT `FK_MAST_CASE_Case_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CASE_Case: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CASE_Case` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CASE_Case` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CASE_CaseMilestone
CREATE TABLE IF NOT EXISTS `MAST_CASE_CaseMilestone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `completion_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `in_scope_flag` tinyint(1) NOT NULL DEFAULT '1',
  `milestone_status_id` smallint(6) NOT NULL DEFAULT '1',
  `milestone_id` smallint(6) NOT NULL,
  `case_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `case_id` (`case_id`),
  KEY `milestone_id` (`milestone_id`),
  KEY `milestone_status_id` (`milestone_status_id`),
  CONSTRAINT `FK_CASE_MILESTONE_MILESTONE_STATUS` FOREIGN KEY (`milestone_status_id`) REFERENCES `MAST_REF_MilestoneStatus` (`id`),
  CONSTRAINT `FK_CASE_MILESTONE_CASE` FOREIGN KEY (`case_id`) REFERENCES `MAST_CASE_Case` (`id`),
  CONSTRAINT `FK_CASE_MILESTONE_MILESTONE` FOREIGN KEY (`milestone_id`) REFERENCES `MAST_REF_Milestone` (`id`),
  CONSTRAINT `FK_MAST_CASE_CaseMilestone_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CASE_CaseMilestone: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CASE_CaseMilestone` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CASE_CaseMilestone` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CASE_CaseOffice
CREATE TABLE IF NOT EXISTS `MAST_CASE_CaseOffice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `office_role_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `case_id` (`case_id`),
  KEY `office_id` (`office_id`),
  KEY `office_role_id` (`office_role_id`),
  CONSTRAINT `FK_CASE_OFFICE_OFFICE_ROLE` FOREIGN KEY (`office_role_id`) REFERENCES `MAST_REF_OfficeRole` (`id`),
  CONSTRAINT `FK_CASE_OFFICE_CASE` FOREIGN KEY (`case_id`) REFERENCES `MAST_CASE_Case` (`id`),
  CONSTRAINT `FK_CASE_OFFICE_LR_OFFICE` FOREIGN KEY (`office_id`) REFERENCES `MAST_LRP_Office` (`id`),
  CONSTRAINT `FK_MAST_CASE_CaseOffice_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CASE_CaseOffice: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CASE_CaseOffice` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CASE_CaseOffice` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CASE_CaseResource
CREATE TABLE IF NOT EXISTS `MAST_CASE_CaseResource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `type_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `case_id` (`case_id`),
  KEY `employee_id` (`employee_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `FK_REF_CASE_RESOURCE_MAST_REF_CASE_RESOURCE_TYPE` FOREIGN KEY (`type_id`) REFERENCES `MAST_REF_CaseResourceType` (`id`),
  CONSTRAINT `FK_CASE_RESOURCE_CASE` FOREIGN KEY (`case_id`) REFERENCES `MAST_CASE_Case` (`id`),
  CONSTRAINT `FK_CASE_RESOURCE_LR_EMPLOYEE` FOREIGN KEY (`employee_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_CASE_CaseResource_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CASE_CaseResource: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CASE_CaseResource` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CASE_CaseResource` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CDH_Address
CREATE TABLE IF NOT EXISTS `MAST_CDH_Address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_line1` varchar(100) NOT NULL,
  `address_line2` varchar(100) DEFAULT NULL,
  `address_line3` varchar(100) DEFAULT NULL,
  `town` varchar(30) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `FK_ADDRESS_COUNTRY` FOREIGN KEY (`country_id`) REFERENCES `MAST_REF_Country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CDH_Address: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CDH_Address` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CDH_Address` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CDH_Organisation
CREATE TABLE IF NOT EXISTS `MAST_CDH_Organisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lr_approved_flag` tinyint(1) NOT NULL,
  `party_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `builder_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_mast_cdh_organisation_party_id` (`party_id`),
  KEY `party_id` (`party_id`),
  CONSTRAINT `FK_ORGANISATION_PARTY` FOREIGN KEY (`party_id`) REFERENCES `MAST_CDH_Party` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CDH_Organisation: ~29 rows (approximately)
/*!40000 ALTER TABLE `MAST_CDH_Organisation` DISABLE KEYS */;
INSERT INTO `MAST_CDH_Organisation` (`id`, `lr_approved_flag`, `party_id`, `deleted`, `last_update_date`, `builder_code`) VALUES
	(1, 1, 1, NULL, NULL, NULL),
	(2, 1, 2, NULL, NULL, NULL),
	(3, 1, 3, NULL, NULL, NULL),
	(4, 1, 4, NULL, NULL, NULL),
	(5, 1, 5, NULL, NULL, NULL),
	(6, 1, 6, NULL, NULL, NULL),
	(7, 1, 7, NULL, NULL, NULL),
	(8, 1, 8, NULL, NULL, NULL),
	(9, 1, 9, NULL, NULL, NULL),
	(10, 1, 10, NULL, NULL, NULL),
	(11, 1, 11, NULL, NULL, NULL),
	(12, 1, 12, NULL, NULL, NULL),
	(13, 1, 13, NULL, NULL, NULL),
	(14, 1, 14, NULL, NULL, NULL),
	(15, 1, 15, NULL, NULL, NULL),
	(16, 1, 16, NULL, NULL, NULL),
	(17, 1, 17, NULL, NULL, NULL),
	(18, 1, 18, NULL, NULL, NULL),
	(19, 1, 19, NULL, NULL, NULL),
	(20, 1, 20, NULL, NULL, NULL),
	(21, 1, 21, NULL, NULL, NULL),
	(22, 1, 22, NULL, NULL, NULL),
	(23, 1, 23, NULL, NULL, NULL),
	(24, 1, 24, NULL, NULL, NULL),
	(25, 1, 25, NULL, NULL, NULL),
	(26, 1, 26, NULL, NULL, NULL),
	(27, 1, 27, NULL, NULL, NULL),
	(28, 1, 28, NULL, NULL, NULL),
	(29, 1, 29, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_CDH_Organisation` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CDH_Party
CREATE TABLE IF NOT EXISTS `MAST_CDH_Party` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status_id` smallint(6) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `fax_number` varchar(20) DEFAULT NULL,
  `imo_number` int(11) DEFAULT NULL,
  `cdh_customer_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `office_id` (`office_id`),
  KEY `IND_imo_number` (`imo_number`),
  CONSTRAINT `FK_MAST_CDH_PARTY_MAST_LRP_OFFICE1` FOREIGN KEY (`office_id`) REFERENCES `MAST_LRP_Office` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CDH_Party: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CDH_Party` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CDH_Party` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CDH_PartyContact
CREATE TABLE IF NOT EXISTS `MAST_CDH_PartyContact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(5) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `extension` bigint(20) DEFAULT NULL,
  `party_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `party_id` (`party_id`),
  CONSTRAINT `FK_CONTACT_PARTY` FOREIGN KEY (`party_id`) REFERENCES `MAST_CDH_Party` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CDH_PartyContact: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CDH_PartyContact` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CDH_PartyContact` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_CDH_Party_Address
CREATE TABLE IF NOT EXISTS `MAST_CDH_Party_Address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `party_id` (`party_id`),
  CONSTRAINT `FK_PARTY_ADDRESS_PARTY` FOREIGN KEY (`party_id`) REFERENCES `MAST_CDH_Party` (`id`),
  CONSTRAINT `FK_PARTY_ADDRESS_ADDRESS` FOREIGN KEY (`address_id`) REFERENCES `MAST_CDH_Address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_CDH_Party_Address: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_CDH_Party_Address` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_CDH_Party_Address` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Defect
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Defect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defect_code` varchar(50) DEFAULT NULL,
  `first_frame_number` int(11) DEFAULT NULL,
  `last_frame_number` int(11) DEFAULT NULL,
  `grounding_involved_flag` tinyint(1) DEFAULT NULL,
  `incident_date` datetime DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `defect_type_id` smallint(6) DEFAULT NULL,
  `defect_category_id` smallint(6) NOT NULL,
  `defect_subcategory_id` smallint(6) DEFAULT NULL,
  `loading_condition_id` smallint(6) DEFAULT NULL,
  `defect_status_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_subcategory_id` (`defect_subcategory_id`),
  KEY `defect_type_id` (`defect_type_id`),
  KEY `asset_id` (`asset_id`),
  KEY `defect_category_id` (`defect_category_id`),
  KEY `loading_condition_id` (`loading_condition_id`),
  KEY `defect_status_id` (`defect_status_id`),
  CONSTRAINT `FK_DEFECT_STATUS` FOREIGN KEY (`defect_status_id`) REFERENCES `MAST_REF_DefectStatus` (`id`),
  CONSTRAINT `FK_DEFECT_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_DEFECT_DEFECT_CATEGORY` FOREIGN KEY (`defect_category_id`) REFERENCES `MAST_REF_DefectCategory` (`id`),
  CONSTRAINT `FK_DEFECT_LOADING_CONDITION` FOREIGN KEY (`loading_condition_id`) REFERENCES `MAST_REF_LoadingCondition` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Defect_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Defect_MAST_REF_DefectCategory` FOREIGN KEY (`defect_subcategory_id`) REFERENCES `MAST_REF_DefectCategory` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Defect_MAST_REF_DefectType` FOREIGN KEY (`defect_type_id`) REFERENCES `MAST_REF_DefectType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Defect: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Defect` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Defect` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Defect_AssetItem
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Defect_AssetItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `defect_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_id` (`defect_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `FK_DEFECT_ITEM_ITEM` FOREIGN KEY (`item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_DEFECT_ITEM_DEFECT` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Defect_AssetItem_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Defect_AssetItem: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Defect_AssetItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Defect_AssetItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Defect_DefectValue
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Defect_DefectValue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_other` varchar(50) DEFAULT NULL,
  `defect_value_id` smallint(6) NOT NULL,
  `defect_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_id` (`defect_id`),
  KEY `defect_value_id` (`defect_value_id`),
  CONSTRAINT `FK_DEFECT_DEFECT_VALUE_DEFECT_VALUE` FOREIGN KEY (`defect_value_id`) REFERENCES `MAST_REF_DefectValue` (`id`),
  CONSTRAINT `FK_DEFECT_DEFECT_VALUE_DEFECT` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Defect_DefectValue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Defect_DefectValue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Defect_DefectValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Defect_DefectValue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Repair
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(5000) DEFAULT NULL,
  `prompt_thorough_flag` tinyint(1) DEFAULT NULL,
  `repair_date` datetime DEFAULT NULL,
  `repair_action_id` smallint(6) DEFAULT NULL,
  `defect_id` int(11) NOT NULL,
  `confirmed_flag` tinyint(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_id` (`defect_id`),
  KEY `repair_action_id` (`repair_action_id`),
  CONSTRAINT `FK_REPAIR_REPAIR_ACTION_TYPE` FOREIGN KEY (`repair_action_id`) REFERENCES `MAST_REF_RepairAction` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Repair_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_REPAIR_DEFECT` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Repair: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Repair` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Repair_AssetItem
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Repair_AssetItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defect_item_id` int(11) NOT NULL,
  `repair_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_item_id` (`defect_item_id`),
  KEY `repair_id` (`repair_id`),
  CONSTRAINT `FK_REPAIR_ITEM_REPAIR` FOREIGN KEY (`repair_id`) REFERENCES `MAST_DEFECT_Repair` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Repair_AssetItem_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_REPAIR_ITEM_DEFECT_ITEM` FOREIGN KEY (`defect_item_id`) REFERENCES `MAST_DEFECT_Defect_AssetItem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Repair_AssetItem: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_AssetItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_AssetItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Repair_Material
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Repair_Material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_id` int(11) NOT NULL,
  `material_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `material_id` (`material_id`),
  KEY `repair_id` (`repair_id`),
  CONSTRAINT `FK_REPAIR_MATERIAL_REPAIR` FOREIGN KEY (`repair_id`) REFERENCES `MAST_DEFECT_Repair` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Repair_Material_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_REPAIR_MATERIAL_MATERIAL_USED` FOREIGN KEY (`material_id`) REFERENCES `MAST_REF_Material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Repair_Material: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_Material` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_Material` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_DEFECT_Repair_RepairType
CREATE TABLE IF NOT EXISTS `MAST_DEFECT_Repair_RepairType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_id` int(11) NOT NULL,
  `repair_type_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `repair_id` (`repair_id`),
  KEY `repair_type_id` (`repair_type_id`),
  CONSTRAINT `FK_REPAIR_REPAIR_TYPE_REPAIR_TYPE` FOREIGN KEY (`repair_type_id`) REFERENCES `MAST_REF_RepairType` (`id`),
  CONSTRAINT `FK_MAST_DEFECT_Repair_RepairType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_REPAIR_REPAIR_TYPE_REPAIR` FOREIGN KEY (`repair_id`) REFERENCES `MAST_DEFECT_Repair` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_DEFECT_Repair_RepairType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_RepairType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_DEFECT_Repair_RepairType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_BerthingEvent
CREATE TABLE IF NOT EXISTS `MAST_JOB_BerthingEvent` (
  `id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `port_id` smallint(6) DEFAULT NULL,
  `estimated_arrival_date` datetime DEFAULT NULL,
  `estimated_departure_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_id` (`asset_id`),
  KEY `job_id` (`job_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `port_id` (`port_id`),
  CONSTRAINT `FK_MAST_JOB_BerthingEvent_MAST_REF_Port` FOREIGN KEY (`port_id`) REFERENCES `MAST_REF_Port` (`id`),
  CONSTRAINT `FK_MAST_JOB_BerthingEvent_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_JOB_BerthingEvent_MAST_JOB_Job` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_MAST_JOB_BerthingEvent_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_BerthingEvent: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_BerthingEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_BerthingEvent` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_Certificate
CREATE TABLE IF NOT EXISTS `MAST_JOB_Certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_number` varchar(25) NOT NULL,
  `certificate_status_id` smallint(6) DEFAULT NULL,
  `certificate_type_id` smallint(6) NOT NULL,
  `certificate_action_id` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `extended_date` datetime DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `lr_office_id` int(11) DEFAULT NULL,
  `lr_employee_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `wip_flag` tinyint(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `action_taken` varchar(18) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `version` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `certificate_status_id` (`certificate_status_id`),
  KEY `certificate_type_id` (`certificate_type_id`),
  KEY `job_id` (`job_id`),
  KEY `lr_employee_id` (`lr_employee_id`),
  KEY `lr_office_id` (`lr_office_id`),
  CONSTRAINT `FK_CERTIFICATE_LR_OFFICE` FOREIGN KEY (`lr_office_id`) REFERENCES `MAST_LRP_Office` (`id`),
  CONSTRAINT `FK_CERTIFICATE_CERTIFICATE_STATUS` FOREIGN KEY (`certificate_status_id`) REFERENCES `MAST_REF_CertificateStatus` (`id`),
  CONSTRAINT `FK_CERTIFICATE_CERTIFICATE_TYPE` FOREIGN KEY (`certificate_type_id`) REFERENCES `MAST_REF_CertificateType` (`id`),
  CONSTRAINT `FK_CERTIFICATE_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_CERTIFICATE_LR_EMPLOYEE` FOREIGN KEY (`lr_employee_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_Certificate: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_Certificate` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_Certificate` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_CertificateWIP
CREATE TABLE IF NOT EXISTS `MAST_JOB_CertificateWIP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_number` varchar(25) NOT NULL,
  `certificate_status_id` smallint(6) DEFAULT NULL,
  `certificate_type_id` smallint(6) NOT NULL,
  `certificate_action_id` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `extended_date` datetime DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `lr_office_id` int(11) DEFAULT NULL,
  `lr_employee_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `parent_id` (`parent_id`),
  KEY `certificate_status_id` (`certificate_status_id`),
  KEY `certificate_type_id` (`certificate_type_id`),
  KEY `job_id` (`job_id`),
  KEY `lr_employee_id` (`lr_employee_id`),
  KEY `lr_office_id` (`lr_office_id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_LR_OFFICE` FOREIGN KEY (`lr_office_id`) REFERENCES `MAST_LRP_Office` (`id`),
  CONSTRAINT `FK_MAST_ASSET_wip_certificate_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_CERTIFICATE` FOREIGN KEY (`parent_id`) REFERENCES `MAST_JOB_Certificate` (`id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_CERTIFICATE_STATUS` FOREIGN KEY (`certificate_status_id`) REFERENCES `MAST_REF_CertificateStatus` (`id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_CERTIFICATE_TYPE` FOREIGN KEY (`certificate_type_id`) REFERENCES `MAST_REF_CertificateType` (`id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_WIP_CERTIFICATE_LR_EMPLOYEE` FOREIGN KEY (`lr_employee_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_CertificateWIP: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_CertificateWIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_CertificateWIP` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_CodicilWIP
CREATE TABLE IF NOT EXISTS `MAST_JOB_CodicilWIP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `imposed_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `narrative` varchar(2000) NOT NULL,
  `action_taken_id` smallint(6) DEFAULT NULL,
  `inherited_flag` tinyint(1) NOT NULL,
  `editable_by_surveyor` tinyint(1) DEFAULT NULL,
  `surveyor_guidance` varchar(2000) DEFAULT NULL,
  `defect_id` int(11) DEFAULT NULL,
  `status_id` smallint(6) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_item_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `category_id` smallint(6) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `confidentiality_type_id` smallint(6) DEFAULT NULL,
  `codicil_type_id` tinyint(4) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `status_id` (`status_id`),
  KEY `category_id` (`category_id`),
  KEY `template_id` (`template_id`),
  KEY `codicil_type_id` (`codicil_type_id`),
  KEY `confidentiality_type_id` (`confidentiality_type_id`),
  KEY `defect_id` (`defect_id`),
  KEY `asset_item_id` (`asset_item_id`),
  KEY `asset_id` (`asset_id`),
  KEY `action_taken_id` (`action_taken_id`),
  KEY `employee_id` (`employee_id`),
  KEY `job_id` (`job_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `FK_Codicil_WIP_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `MAST_ASSET_Codicil` (`id`),
  CONSTRAINT `FK_CodicilWIP_CodicilStatus` FOREIGN KEY (`status_id`) REFERENCES `MAST_REF_CodicilStatus` (`id`),
  CONSTRAINT `FK_Codicil_WIP_CodicilCategory` FOREIGN KEY (`category_id`) REFERENCES `MAST_REF_CodicilCategory` (`id`),
  CONSTRAINT `FK_Codicil_WIP_CodicilTemplate` FOREIGN KEY (`template_id`) REFERENCES `MAST_REF_CodicilTemplate` (`id`),
  CONSTRAINT `FK_Codicil_WIP_CodicilType` FOREIGN KEY (`codicil_type_id`) REFERENCES `MAST_REF_CodicilType` (`id`),
  CONSTRAINT `FK_Codicil_WIP_ConfidentialityType` FOREIGN KEY (`confidentiality_type_id`) REFERENCES `MAST_REF_ConfidentialityType` (`id`),
  CONSTRAINT `FK_Codicil_WIP_Defect` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`),
  CONSTRAINT `FK_MAST_JOB_CodicilWIP_MAST_ASSET_Asset` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_JOB_CodicilWIP_MAST_REF_ActionTaken` FOREIGN KEY (`action_taken_id`) REFERENCES `MAST_REF_ActionTaken` (`id`),
  CONSTRAINT `FK_MAST_JOB_Codicil_WIP_MAST_ASSET_AssetItem` FOREIGN KEY (`asset_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK__Codicil_WIP_EMPLOYEE` FOREIGN KEY (`employee_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK__Codicil_WIP_Job` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK__Codicil_WIP_LRPEmployee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_CodicilWIP: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_CodicilWIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_CodicilWIP` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_Job
CREATE TABLE IF NOT EXISTS `MAST_JOB_Job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_code` varchar(30) DEFAULT NULL,
  `narrative` varchar(50) DEFAULT NULL,
  `notes` varchar(2500) DEFAULT NULL,
  `request_by_telephone_flag` tinyint(1) DEFAULT NULL,
  `service_required_date` datetime DEFAULT NULL,
  `status_id` smallint(6) NOT NULL,
  `status_reason` varchar(500) DEFAULT NULL,
  `status_change_date` datetime DEFAULT NULL,
  `work_order_number` bigint(20) DEFAULT NULL,
  `written_service_request_received_date` datetime DEFAULT NULL,
  `written_service_response_sent_date` datetime DEFAULT NULL,
  `asset_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `actual_start_date` datetime DEFAULT NULL,
  `actual_end_date` datetime DEFAULT NULL,
  `endorsement_type_id` smallint(6) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `survey_at_sea_ind` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `scoped_confirmed` tinyint(1) NOT NULL,
  `scope_confirmed_by_user_id` int(11) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `endorsement_type_id` (`endorsement_type_id`),
  KEY `asset_id` (`asset_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `FK_JOB_JOB_STATUS` FOREIGN KEY (`status_id`) REFERENCES `MAST_REF_JobStatus` (`id`),
  CONSTRAINT `FK_JOB_ASSET` FOREIGN KEY (`asset_id`) REFERENCES `MAST_ASSET_Asset` (`id`),
  CONSTRAINT `FK_MAST_JOB_Job_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_JOB_Job_MAST_REF_EndorsementType` FOREIGN KEY (`endorsement_type_id`) REFERENCES `MAST_REF_EndorsementType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_Job: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_Job` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_Job` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_JobOffice
CREATE TABLE IF NOT EXISTS `MAST_JOB_JobOffice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `office_role_id` smallint(6) NOT NULL,
  `lr_office_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `job_id` (`job_id`),
  KEY `lr_office_id` (`lr_office_id`),
  KEY `office_role_id` (`office_role_id`),
  CONSTRAINT `FK_JOB_OFFICE_OFFICE_ROLE` FOREIGN KEY (`office_role_id`) REFERENCES `MAST_REF_OfficeRole` (`id`),
  CONSTRAINT `FK_JOB_OFFICE_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_JOB_OFFICE_LR_OFFICE` FOREIGN KEY (`lr_office_id`) REFERENCES `MAST_LRP_Office` (`id`),
  CONSTRAINT `FK_MAST_JOB_JobOffice_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_JobOffice: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_JobOffice` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_JobOffice` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_JobResource
CREATE TABLE IF NOT EXISTS `MAST_JOB_JobResource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `lr_employee_id` int(11) NOT NULL,
  `employee_role_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `employee_role_id` (`employee_role_id`),
  KEY `job_id` (`job_id`),
  KEY `lr_employee_id` (`lr_employee_id`),
  CONSTRAINT `FK_JOB_RESOURCE_LR_EMPLOYEE` FOREIGN KEY (`lr_employee_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_JOB_RESOURCE_EMPLOYEE_ROLE` FOREIGN KEY (`employee_role_id`) REFERENCES `MAST_REF_EmployeeRole` (`id`),
  CONSTRAINT `FK_JOB_RESOURCE_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_MAST_JOB_JobResource_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_JobResource: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_JobResource` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_JobResource` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_JobServiceInstance
CREATE TABLE IF NOT EXISTS `MAST_JOB_JobServiceInstance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `job_id` int(11) NOT NULL,
  `service_catalogue_id` smallint(6) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `technical_status_id` smallint(6) DEFAULT NULL,
  `crediting_date` datetime DEFAULT NULL,
  `credited_by_user_id` int(11) DEFAULT NULL,
  `schedule_dates_updated_flag` tinyint(1) NOT NULL,
  `wip_flag` tinyint(1) NOT NULL,
  `completion_date` datetime DEFAULT NULL,
  `service_credit_status_id` smallint(6) DEFAULT NULL,
  `part_held_flag` tinyint(1) NOT NULL,
  `service_cycle_number` smallint(6) DEFAULT NULL,
  `service_occurence_number` smallint(6) DEFAULT NULL,
  `narrative` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `endorsement_notes` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_credit_status_id` (`service_credit_status_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `job_id` (`job_id`),
  KEY `credited_by_user_id` (`credited_by_user_id`),
  KEY `service_id` (`service_id`),
  KEY `service_catalogue_id` (`service_catalogue_id`),
  KEY `technical_status_id` (`technical_status_id`),
  CONSTRAINT `FK_SURVEY_SURVEY_STATUS` FOREIGN KEY (`technical_status_id`) REFERENCES `MAST_REF_TechnicalStatus` (`id`),
  CONSTRAINT `FK_JobServiceInstance_ServiceCreditStatus` FOREIGN KEY (`service_credit_status_id`) REFERENCES `MAST_REF_ServiceCreditStatus` (`id`),
  CONSTRAINT `FK_mast_job_survey_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_SURVEY_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_SURVEY_LR_EMPLOYEE` FOREIGN KEY (`credited_by_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_SURVEY_SERVICE` FOREIGN KEY (`service_id`) REFERENCES `MAST_ASSET_ScheduledService` (`id`),
  CONSTRAINT `FK_SURVEY_SERVICE_CATALOGUE` FOREIGN KEY (`service_catalogue_id`) REFERENCES `MAST_REF_ServiceCatalogue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_JobServiceInstance: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_JobServiceInstance` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_JobServiceInstance` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_JobWorkItem
CREATE TABLE IF NOT EXISTS `MAST_JOB_JobWorkItem` (
  `id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `resolution_status_id` smallint(6) NOT NULL,
  `job_service_instance_id` int(11) NOT NULL,
  `work_item_id` int(11) NOT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `resolved_by_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_service_instance_id` (`job_service_instance_id`),
  KEY `work_item_id` (`work_item_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `assigned_to_id` (`assigned_to_id`),
  KEY `resolved_by_id` (`resolved_by_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_JobWorkItem: 0 rows
/*!40000 ALTER TABLE `MAST_JOB_JobWorkItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_JobWorkItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_Job_Defect
CREATE TABLE IF NOT EXISTS `MAST_JOB_Job_Defect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `defect_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_id` (`defect_id`),
  KEY `job_id` (`job_id`),
  CONSTRAINT `FK_JOB_DEFECT_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`),
  CONSTRAINT `FK_JOB_DEFECT_DEFECT` FOREIGN KEY (`defect_id`) REFERENCES `MAST_DEFECT_Defect` (`id`),
  CONSTRAINT `FK_MAST_JOB_Job_Defect_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_Job_Defect: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_Job_Defect` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_Job_Defect` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_Report
CREATE TABLE IF NOT EXISTS `MAST_JOB_Report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_version` smallint(6) DEFAULT NULL,
  `type_id` smallint(6) NOT NULL,
  `job_id` int(11) NOT NULL,
  `generated_content` blob,
  `recommendation` varchar(2000) DEFAULT NULL,
  `actual_start_date` datetime DEFAULT NULL,
  `actual_end_date` datetime DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `job_id` (`job_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `FK_REPORT_REPORT_TYPE` FOREIGN KEY (`type_id`) REFERENCES `MAST_REF_ReportType` (`id`),
  CONSTRAINT `FK_MAST_JOB_Report_mast_lrp_employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_REPORT_JOB` FOREIGN KEY (`job_id`) REFERENCES `MAST_JOB_Job` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_Report: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_Report` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_Report` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_ScheduledServiceWorkItem
CREATE TABLE IF NOT EXISTS `MAST_JOB_ScheduledServiceWorkItem` (
  `id` int(11) NOT NULL,
  `scheduled_service_id` int(11) DEFAULT NULL,
  `work_item_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `scheduled_service_id` (`scheduled_service_id`),
  KEY `work_item_id` (`work_item_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_JOB_ScheduledServiceWorkItem_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_JOB_ScheduledServiceWorkItem_MAST_ASSET_ScheduledService` FOREIGN KEY (`scheduled_service_id`) REFERENCES `MAST_ASSET_ScheduledService` (`id`),
  CONSTRAINT `FK_MAST_JOB_ScheduledServiceWorkItem_MAST_JOB_WorkItem` FOREIGN KEY (`work_item_id`) REFERENCES `MAST_JOB_WorkItem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_ScheduledServiceWorkItem: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_ScheduledServiceWorkItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_ScheduledServiceWorkItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_WorkItem
CREATE TABLE IF NOT EXISTS `MAST_JOB_WorkItem` (
  `id` int(11) NOT NULL,
  `reference_code` varchar(14) NOT NULL,
  `service_code` int(11) DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `long_description` varchar(500) DEFAULT NULL,
  `asset_item_id` int(11) NOT NULL,
  `work_item_type_id` tinyint(4) NOT NULL,
  `work_item_action_id` smallint(6) DEFAULT NULL,
  `attribute_data_type_id` smallint(6) DEFAULT NULL,
  `attribute_mandatory_flag` tinyint(1) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `codicil_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asset_item_id` (`asset_item_id`),
  KEY `codicil_id` (`codicil_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `attribute_data_type_id` (`attribute_data_type_id`),
  KEY `work_item_action_id` (`work_item_action_id`),
  KEY `work_item_type_id` (`work_item_type_id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_REF_WorkItemType` FOREIGN KEY (`work_item_type_id`) REFERENCES `MAST_REF_WorkItemType` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_ASSET_AssetItem` FOREIGN KEY (`asset_item_id`) REFERENCES `MAST_ASSET_AssetItem` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_ASSET_Codicil` FOREIGN KEY (`codicil_id`) REFERENCES `MAST_ASSET_Codicil` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_REF_AttributeDataType` FOREIGN KEY (`attribute_data_type_id`) REFERENCES `MAST_REF_AttributeDataType` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItem_MAST_REF_WorkItemAction` FOREIGN KEY (`work_item_action_id`) REFERENCES `MAST_REF_WorkItemAction` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_WorkItem: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_WorkItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_WorkItem` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_JOB_WorkItemAttribute
CREATE TABLE IF NOT EXISTS `MAST_JOB_WorkItemAttribute` (
  `id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `attribute_value_date` date DEFAULT NULL,
  `attribute_value_float` float DEFAULT NULL,
  `attribute_value_lookup` int(11) DEFAULT NULL,
  `attribute_value_string` varchar(500) DEFAULT NULL,
  `work_item_id` int(11) DEFAULT NULL,
  `allowed_work_item_attribute_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `work_item_id` (`work_item_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `allowed_work_item_attribute_id` (`allowed_work_item_attribute_id`),
  CONSTRAINT `FK_MAST_JOB_WorkItemAttribute_MAST_REF_AWIA` FOREIGN KEY (`allowed_work_item_attribute_id`) REFERENCES `MAST_REF_AllowedWorkItemAttributeValue` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItemAttribute_MAST_JOB_WorkItem` FOREIGN KEY (`work_item_id`) REFERENCES `MAST_JOB_WorkItem` (`id`),
  CONSTRAINT `FK_MAST_JOB_WorkItemAttribute_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_JOB_WorkItemAttribute: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_JOB_WorkItemAttribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_JOB_WorkItemAttribute` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Employee
CREATE TABLE IF NOT EXISTS `MAST_LRP_Employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_user_id` varchar(150) DEFAULT NULL,
  `oneworld_number` varchar(30) DEFAULT NULL,
  `job_title` varchar(150) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(150) NOT NULL,
  `email_address` varchar(240) DEFAULT NULL,
  `work_phone` varchar(60) DEFAULT NULL,
  `work_mobile` varchar(60) DEFAULT NULL,
  `department` varchar(240) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Employee: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_LRP_Employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Employee` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Employee_Office
CREATE TABLE IF NOT EXISTS `MAST_LRP_Employee_Office` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lr_office_id` int(11) NOT NULL,
  `lr_employee_id` int(11) NOT NULL,
  `primary_flag` tinyint(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lr_employee_id` (`lr_employee_id`),
  KEY `lr_office_id` (`lr_office_id`),
  CONSTRAINT `FK_LR_OFFICE_LR_EMPLOYEE_LR_OFFICE` FOREIGN KEY (`lr_office_id`) REFERENCES `MAST_LRP_Office` (`id`),
  CONSTRAINT `FK_LR_OFFICE_LR_EMPLOYEE_LR_EMPLOYEE` FOREIGN KEY (`lr_employee_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Employee_Office: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_LRP_Employee_Office` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Employee_Office` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Employee_Role
CREATE TABLE IF NOT EXISTS `MAST_LRP_Employee_Role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lr_employee_id` int(11) NOT NULL,
  `employee_role_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lr_employee_id` (`lr_employee_id`),
  KEY `employee_role_id` (`employee_role_id`),
  CONSTRAINT `FK_LR_EMPLOYEE_EMPLOYEE_ROLE_REF_EMPLOYEE_ROLE` FOREIGN KEY (`employee_role_id`) REFERENCES `MAST_REF_EmployeeRole` (`id`),
  CONSTRAINT `FK_LR_EMPLOYEE_EMPLOYEE_ROLE_LR_EMPLOYEE` FOREIGN KEY (`lr_employee_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Employee_Role: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_LRP_Employee_Role` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Employee_Role` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Office
CREATE TABLE IF NOT EXISTS `MAST_LRP_Office` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `office_role_id` smallint(6) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `office_role_id` (`office_role_id`),
  CONSTRAINT `FK_LR_OFFICE_OFFICE_ROLE` FOREIGN KEY (`office_role_id`) REFERENCES `MAST_REF_OfficeRole` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Office: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_LRP_Office` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Office` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Office_Country
CREATE TABLE IF NOT EXISTS `MAST_LRP_Office_Country` (
  `id` int(11) NOT NULL,
  `lr_office_id` int(11) NOT NULL,
  `country_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user` (`last_update_user`),
  KEY `lr_office_id` (`lr_office_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Office_Country: 0 rows
/*!40000 ALTER TABLE `MAST_LRP_Office_Country` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Office_Country` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_LRP_Office_Port
CREATE TABLE IF NOT EXISTS `MAST_LRP_Office_Port` (
  `id` int(11) NOT NULL,
  `lr_office_id` int(11) NOT NULL,
  `port_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user` (`last_update_user`),
  KEY `lr_office_id` (`lr_office_id`),
  KEY `port_id` (`port_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_LRP_Office_Port: 0 rows
/*!40000 ALTER TABLE `MAST_LRP_Office_Port` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_LRP_Office_Port` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ActionTaken
CREATE TABLE IF NOT EXISTS `MAST_REF_ActionTaken` (
  `id` smallint(6) NOT NULL,
  `name` varchar(5) NOT NULL,
  `description` varchar(170) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ActionTaken: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ActionTaken` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ActionTaken` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AllowedAssetAttributeValue
CREATE TABLE IF NOT EXISTS `MAST_REF_AllowedAssetAttributeValue` (
  `id` smallint(6) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AllowedAssetAttributeValue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AllowedAssetAttributeValue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AllowedAssetAttributeValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AllowedAssetAttributeValue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AllowedWorkItemAttributeValue
CREATE TABLE IF NOT EXISTS `MAST_REF_AllowedWorkItemAttributeValue` (
  `id` smallint(6) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `display_order` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AllowedWorkItemAttributeValue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AllowedWorkItemAttributeValue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AllowedWorkItemAttributeValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AllowedWorkItemAttributeValue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetAttributeType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetAttributeType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `naming_order` int(11) DEFAULT NULL,
  `description_order` int(11) DEFAULT NULL,
  `default` tinyint(1) NOT NULL,
  `naming_decoration` varchar(20) DEFAULT NULL,
  `copy_id` tinyint(4) NOT NULL,
  `item_type_id` smallint(6) NOT NULL,
  `attribute_datatype_id` smallint(6) NOT NULL,
  `min_occurance` int(11) NOT NULL,
  `max_occurance` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `copy_id` (`copy_id`),
  KEY `attribute_datatype_id` (`attribute_datatype_id`),
  KEY `item_type_id` (`item_type_id`),
  CONSTRAINT `FK_ATTRIBUTE_TYPE_RELATIONSHIP_ITEM_TYPE` FOREIGN KEY (`item_type_id`) REFERENCES `MAST_REF_AssetItemType` (`id`),
  CONSTRAINT `FK_ATTRIBUTE_TYPE_RELATIONSHIP_ATTRIBUTE_TYPE` FOREIGN KEY (`attribute_datatype_id`) REFERENCES `MAST_REF_AttributeDataType` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetAttributeType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetAttributeType_MAST_REF_AttributeCopyRule` FOREIGN KEY (`copy_id`) REFERENCES `MAST_REF_AttributeCopyRule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetAttributeType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetAttributeType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetAttributeType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetCategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `business_process_id` smallint(6) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `FK_ASSET_CATEGORY_BUSINESS_PROCESS` (`business_process_id`),
  CONSTRAINT `FK_ASSET_CATEGORY_BUSINESS_PROCESS` FOREIGN KEY (`business_process_id`) REFERENCES `MAST_REF_BusinessProcess` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetCategory_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetCategory: ~12 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetCategory` DISABLE KEYS */;
INSERT INTO `MAST_REF_AssetCategory` (`id`, `name`, `business_process_id`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'ANY', 10, NULL, NULL, NULL, NULL),
	(2, 'ASSET', 10, NULL, NULL, NULL, NULL),
	(3, 'ASSET TEMPLATE', 10, NULL, NULL, NULL, NULL),
	(4, 'COMPONENT TEMPLATE', 10, NULL, NULL, NULL, NULL),
	(5, 'ACV', 10, NULL, NULL, NULL, NULL),
	(6, 'BUOY', 10, NULL, NULL, NULL, NULL),
	(7, 'FLOATING DOCK', 10, NULL, NULL, NULL, NULL),
	(8, 'LINKSPAN', 10, NULL, NULL, NULL, NULL),
	(9, 'MANAGEMENT SYSTEM', 10, NULL, NULL, NULL, NULL),
	(10, 'ROU PLATFORM', 10, NULL, NULL, NULL, NULL),
	(11, 'SHIPLIFT', 10, NULL, NULL, NULL, NULL),
	(12, 'VESSEL', 10, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_AssetCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetItemReferenceCode
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetItemReferenceCode` (
  `asset_item_long_name` varchar(500) NOT NULL,
  `reference_code` varchar(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`asset_item_long_name`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AssetItemReferenceCode_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetItemReferenceCode: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetItemReferenceCode` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetItemReferenceCode` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetItemRelationshipType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetItemRelationshipType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_relationship_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetItemRelationshipType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetItemRelationshipType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetItemRelationshipType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetItemType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetItemType` (
  `id` smallint(6) NOT NULL,
  `item_class_id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `column1` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `item_class_id` (`item_class_id`),
  CONSTRAINT `FK_ITEM_TYPE_ITEM_CLASS` FOREIGN KEY (`item_class_id`) REFERENCES `MAST_REF_ItemClass` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetItemType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetItemType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetItemType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetItemType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetItemTypeRelationship
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetItemTypeRelationship` (
  `id` smallint(6) NOT NULL,
  `from_item_type_id` smallint(6) NOT NULL,
  `to_item_type_id` smallint(6) NOT NULL,
  `relationship_type_id` smallint(6) NOT NULL,
  `min_occurance` int(11) NOT NULL,
  `max_occurance` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `from_item_type_id` (`from_item_type_id`),
  KEY `to_item_type_id` (`to_item_type_id`),
  KEY `relationship_type_id` (`relationship_type_id`),
  CONSTRAINT `FK_ITEM_TYPE_RELATIONSHIP_RELATIONSHIP_TYPE` FOREIGN KEY (`relationship_type_id`) REFERENCES `MAST_REF_AssetItemRelationshipType` (`id`),
  CONSTRAINT `FK_ITEM_TYPE_RELATIONSHIP_ITEM_TYPE_FROM` FOREIGN KEY (`from_item_type_id`) REFERENCES `MAST_REF_AssetItemType` (`id`),
  CONSTRAINT `FK_ITEM_TYPE_RELATIONSHIP_ITEM_TYPE_TO` FOREIGN KEY (`to_item_type_id`) REFERENCES `MAST_REF_AssetItemType` (`id`),
  CONSTRAINT `FK_MAST_REF_AssetItemTypeRelationship_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetItemTypeRelationship: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetItemTypeRelationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetItemTypeRelationship` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetLifecycleStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetLifecycleStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AssetLifecycleStatus_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetLifecycleStatus: ~11 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetLifecycleStatus` DISABLE KEYS */;
INSERT INTO `MAST_REF_AssetLifecycleStatus` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Under Construction', NULL, NULL, NULL, NULL),
	(2, 'Build Suspended', NULL, NULL, NULL, NULL),
	(3, 'In service', NULL, NULL, NULL, NULL),
	(4, 'Laid up', NULL, NULL, NULL, NULL),
	(5, 'Under repair', NULL, NULL, NULL, NULL),
	(6, 'In casualty', NULL, NULL, NULL, NULL),
	(7, 'Converting', NULL, NULL, NULL, NULL),
	(8, 'To be broken up', NULL, NULL, NULL, NULL),
	(9, 'Decommisioned', NULL, NULL, NULL, NULL),
	(10, 'To be handed over', NULL, NULL, NULL, NULL),
	(11, 'Cancelled', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_AssetLifecycleStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetRelationshipType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetRelationshipType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetRelationshipType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetRelationshipType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AssetRelationshipType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssetType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssetType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_asset_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssetType: ~20 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssetType` DISABLE KEYS */;
INSERT INTO `MAST_REF_AssetType` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Cargo Carrying', '3123213', NULL, NULL, 1),
	(2, 'Work Vessel', '', NULL, NULL, 1),
	(3, 'Non Seagoing Merchant ships', '', NULL, NULL, 1),
	(4, 'Non Merchant ', '', NULL, NULL, 1),
	(5, 'Non Propelled', '', NULL, NULL, 1),
	(6, 'Non Ship Structures', '', NULL, NULL, 1),
	(7, 'Tankers', 'Tankers', NULL, NULL, 2),
	(8, 'Bulk Carriers', 'Bulk Carriers', NULL, NULL, 2),
	(9, 'Dry Cargo/Passenger', 'Dry Cargo/Passenger', NULL, NULL, 2),
	(10, 'Fishing', 'Fishing', NULL, NULL, 2),
	(11, 'Offshore', 'Offshore', NULL, NULL, 2),
	(12, 'Miscellaneous', 'Miscellaneous', NULL, NULL, 2),
	(13, 'Inland Waterways', 'Inland Waterways', NULL, NULL, 2),
	(14, 'Non Merchant', 'Non Merchant', NULL, NULL, 2),
	(15, 'Non Propelled', 'Non Propelled', NULL, NULL, 2),
	(16, 'Non Ship Structures', 'Non Ship Structures', NULL, NULL, 2),
	(17, 'Liquefied Gas', 'Liquefied Gas', NULL, NULL, 3),
	(18, 'Chemical', 'Chemical', NULL, NULL, 3),
	(19, 'Oil', 'Oil', NULL, NULL, 3),
	(20, 'Other Liquids', 'Other Liquids', NULL, NULL, 3);
/*!40000 ALTER TABLE `MAST_REF_AssetType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssignedSchedulingType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssignedSchedulingType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AssignedSchedulingType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssignedSchedulingType: ~2 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssignedSchedulingType` DISABLE KEYS */;
INSERT INTO `MAST_REF_AssignedSchedulingType` (`id`, `name`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Date On Completion', NULL, NULL, NULL),
	(2, 'Harmonisation Date', NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_AssignedSchedulingType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AssociatedSchedulingType
CREATE TABLE IF NOT EXISTS `MAST_REF_AssociatedSchedulingType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_AssociatedSchedulingType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AssociatedSchedulingType: ~2 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AssociatedSchedulingType` DISABLE KEYS */;
INSERT INTO `MAST_REF_AssociatedSchedulingType` (`id`, `name`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'CL-B - Core Hull Cycle (General)', NULL, NULL, NULL),
	(2, 'Test', NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_AssociatedSchedulingType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AttachmentLocation
CREATE TABLE IF NOT EXISTS `MAST_REF_AttachmentLocation` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_attachment_location_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AttachmentLocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AttachmentLocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AttachmentLocation` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AttachmentMIMEType
CREATE TABLE IF NOT EXISTS `MAST_REF_AttachmentMIMEType` (
  `id` smallint(6) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AttachmentMIMEType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AttachmentMIMEType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AttachmentMIMEType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AttachmentType
CREATE TABLE IF NOT EXISTS `MAST_REF_AttachmentType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_attachment_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AttachmentType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AttachmentType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AttachmentType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AttributeCopyRule
CREATE TABLE IF NOT EXISTS `MAST_REF_AttributeCopyRule` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AttributeCopyRule: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AttributeCopyRule` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AttributeCopyRule` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_AttributeDataType
CREATE TABLE IF NOT EXISTS `MAST_REF_AttributeDataType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `value_type_id` smallint(6) NOT NULL,
  `unit` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `value_type_id` (`value_type_id`),
  CONSTRAINT `FK_ATTRIBUTE_TYPE_ALLOWED_ATTRIBUTE_VALUE` FOREIGN KEY (`value_type_id`) REFERENCES `MAST_REF_AllowedAssetAttributeValue` (`id`),
  CONSTRAINT `FK_MAST_REF_AttributeDataType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_AttributeDataType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_AttributeDataType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_AttributeDataType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_BusinessProcess
CREATE TABLE IF NOT EXISTS `MAST_REF_BusinessProcess` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `asset_lifecycle_status_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `asset_lifecycle_status_id` (`asset_lifecycle_status_id`),
  CONSTRAINT `FK_BUSINESS_PROCESS_ASSET_LIFECYCLE_STATUS` FOREIGN KEY (`asset_lifecycle_status_id`) REFERENCES `MAST_REF_AssetLifecycleStatus` (`id`),
  CONSTRAINT `FK_MAST_REF_BusinessProcess_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_BusinessProcess: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_BusinessProcess` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_BusinessProcess` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CaseResourceType
CREATE TABLE IF NOT EXISTS `MAST_REF_CaseResourceType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_case_resource_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CaseResourceType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CaseResourceType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CaseResourceType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CaseStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_CaseStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `can_save_with_errors` tinyint(1) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_case_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CaseStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CaseStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CaseStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CaseType
CREATE TABLE IF NOT EXISTS `MAST_REF_CaseType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_case_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CaseType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CaseType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CaseType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CertificateStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_CertificateStatus` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_certificate_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CertificateStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CertificateStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CertificateStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CertificateType
CREATE TABLE IF NOT EXISTS `MAST_REF_CertificateType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `form_number` varchar(30) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `service_catalogue_id` smallint(6) DEFAULT NULL,
  `display_order` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `service_catalogue_id` (`service_catalogue_id`),
  CONSTRAINT `FK_CERTIFICATE_TYPE_SERVICE_CATALOGUE` FOREIGN KEY (`service_catalogue_id`) REFERENCES `MAST_REF_ServiceCatalogue` (`id`),
  CONSTRAINT `FK_MAST_REF_CertificateType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CertificateType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CertificateType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CertificateType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ClassDepartment
CREATE TABLE IF NOT EXISTS `MAST_REF_ClassDepartment` (
  `id` smallint(6) NOT NULL,
  `code` varchar(1) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ClassDepartment: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ClassDepartment` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ClassDepartment` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ClassMaintenanceStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ClassMaintenanceStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ClassMaintenanceStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ClassMaintenanceStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ClassMaintenanceStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ClassSociety
CREATE TABLE IF NOT EXISTS `MAST_REF_ClassSociety` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_class_society_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ClassSociety: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ClassSociety` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ClassSociety` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ClassStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ClassStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_class_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ClassStatus: ~7 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ClassStatus` DISABLE KEYS */;
INSERT INTO `MAST_REF_ClassStatus` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Class Contemplated', NULL, NULL, NULL, NULL),
	(2, 'In Class (Pending Class Exec Approval)', NULL, NULL, NULL, NULL),
	(3, 'In Class', NULL, NULL, NULL, NULL),
	(4, 'In Class (Laid Up)', NULL, NULL, NULL, NULL),
	(5, 'Class Suspended', NULL, NULL, NULL, NULL),
	(6, 'Cancelled', NULL, NULL, NULL, NULL),
	(7, 'Class Withdrawn(Disclassed)', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ClassStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CodicilCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_CodicilCategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CodicilCategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CodicilCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CodicilCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CodicilStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_CodicilStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_codicil_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CodicilStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CodicilStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CodicilStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CodicilTemplate
CREATE TABLE IF NOT EXISTS `MAST_REF_CodicilTemplate` (
  `id` int(11) NOT NULL,
  `category_id` smallint(6) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `confidentiality_type_id` smallint(6) DEFAULT NULL,
  `editable_by_surveyor` tinyint(1) DEFAULT NULL,
  `narrative` varchar(2000) DEFAULT NULL,
  `surveyor_guidance` varchar(2000) DEFAULT NULL,
  `template_status_id` smallint(6) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `codicil_type_id` tinyint(4) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `codicil_type_id` (`codicil_type_id`),
  KEY `confidentiality_type_id` (`confidentiality_type_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_CodicilTemplate_LRPEmployee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_CodicilTemplate_CodicilCategory` FOREIGN KEY (`category_id`) REFERENCES `MAST_REF_CodicilCategory` (`id`),
  CONSTRAINT `FK_CodicilTemplate_CodicilType` FOREIGN KEY (`codicil_type_id`) REFERENCES `MAST_REF_CodicilType` (`id`),
  CONSTRAINT `FK_CodicilTemplate_ConfidentialityType` FOREIGN KEY (`confidentiality_type_id`) REFERENCES `MAST_REF_ConfidentialityType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CodicilTemplate: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CodicilTemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CodicilTemplate` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_CodicilType
CREATE TABLE IF NOT EXISTS `MAST_REF_CodicilType` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_CodicilType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_CodicilType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_CodicilType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ConfidentialityType
CREATE TABLE IF NOT EXISTS `MAST_REF_ConfidentialityType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_confidentiality_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ConfidentialityType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ConfidentialityType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ConfidentialityType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_Country
CREATE TABLE IF NOT EXISTS `MAST_REF_Country` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_country_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_Country: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_Country` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_Country` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectCategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `category_letter` varchar(1) DEFAULT NULL,
  `parent_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `FK_DEFECT_CATEGORY_DEFECT_CATEGORY` FOREIGN KEY (`parent_id`) REFERENCES `MAST_REF_DefectCategory` (`id`),
  CONSTRAINT `FK_MAST_REF_DefectCategory_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectCategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectCategory_DefectValue
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectCategory_DefectValue` (
  `id` smallint(6) NOT NULL,
  `defect_value_id` smallint(6) NOT NULL,
  `defect_category_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_category_id` (`defect_category_id`),
  KEY `defect_value_id` (`defect_value_id`),
  CONSTRAINT `FK_DEFECT_CATEGORY_DEFECT_VALUE_DEFECT_VALUE` FOREIGN KEY (`defect_value_id`) REFERENCES `MAST_REF_DefectValue` (`id`),
  CONSTRAINT `FK_DEFECT_CATEGORY_DEFECT_VALUE_DEFECT_CATEGORY` FOREIGN KEY (`defect_category_id`) REFERENCES `MAST_REF_DefectCategory` (`id`),
  CONSTRAINT `FK_MAST_REF_DefectCategory_DefectValue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectCategory_DefectValue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectCategory_DefectValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectCategory_DefectValue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectDescriptor
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectDescriptor` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_defect_descriptor_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectDescriptor: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectDescriptor` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectDescriptor` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectDescriptorCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectDescriptorCategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_defect_value_severity_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectDescriptorCategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectDescriptorCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectDescriptorCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectStatus` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_DefectStatus_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectType
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_DefectValue
CREATE TABLE IF NOT EXISTS `MAST_REF_DefectValue` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `severity_id` smallint(6) DEFAULT NULL,
  `defect_descriptor_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_descriptor_id` (`defect_descriptor_id`),
  KEY `severity_id` (`severity_id`),
  CONSTRAINT `FK_DEFECT_VALUE_DEFECT_VALUE_SEVERITY` FOREIGN KEY (`severity_id`) REFERENCES `MAST_REF_DefectDescriptorCategory` (`id`),
  CONSTRAINT `FK_DEFECT_VALUE_DEFECT_DESCRIPTOR` FOREIGN KEY (`defect_descriptor_id`) REFERENCES `MAST_REF_DefectDescriptor` (`id`),
  CONSTRAINT `FK_MAST_REF_DefectValue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_DefectValue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_DefectValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_DefectValue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_EmployeeRole
CREATE TABLE IF NOT EXISTS `MAST_REF_EmployeeRole` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `job_group` varchar(700) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_EmployeeRole_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_EmployeeRole: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_EmployeeRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_EmployeeRole` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_EndorsementType
CREATE TABLE IF NOT EXISTS `MAST_REF_EndorsementType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_EndorsementType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_EndorsementType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_EndorsementType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_FlagAdministration
CREATE TABLE IF NOT EXISTS `MAST_REF_FlagAdministration` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `flag_image` varchar(5000) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_FlagAdministration_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_FlagAdministration: ~573 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_FlagAdministration` DISABLE KEYS */;
INSERT INTO `MAST_REF_FlagAdministration` (`id`, `name`, `flag_image`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(2, 'No Longer Needed ?  (LLP)', NULL, NULL, NULL, NULL),
	(3, 'Australian Antarctic Territory', NULL, NULL, NULL, NULL),
	(4, 'Antigua & Barbuda', NULL, NULL, NULL, NULL),
	(5, 'U.A.E. (Abu Dhabi) (Do Not Use)', NULL, NULL, NULL, NULL),
	(6, 'American Caribbean Is. (Do Not Use', NULL, NULL, NULL, NULL),
	(7, 'Aden (British)', NULL, NULL, NULL, NULL),
	(8, 'Anglo-Egyptian Sudan', NULL, NULL, NULL, NULL),
	(9, 'Afghanistan', NULL, NULL, NULL, NULL),
	(10, 'Africa', NULL, NULL, NULL, NULL),
	(11, 'South Arabian Federation', NULL, NULL, NULL, NULL),
	(12, 'Algeria (French)', NULL, NULL, NULL, NULL),
	(13, 'Anguilla', NULL, NULL, NULL, NULL),
	(14, 'Austria-Hungary', NULL, NULL, NULL, NULL),
	(15, 'Australian Indian Ocean Islands', NULL, NULL, NULL, NULL),
	(16, 'U.A.E. (Ajman) (Do Not Use)', NULL, NULL, NULL, NULL),
	(17, 'Angola', NULL, NULL, NULL, NULL),
	(18, 'Albania', NULL, NULL, NULL, NULL),
	(19, 'Algeria', NULL, NULL, NULL, NULL),
	(20, 'Andorra', NULL, NULL, NULL, NULL),
	(21, 'Angola, Portuguese', NULL, NULL, NULL, NULL),
	(22, 'Antigua (British)', NULL, NULL, NULL, NULL),
	(23, 'Australasia', NULL, NULL, NULL, NULL),
	(24, 'United States Pacific Islands', NULL, NULL, NULL, NULL),
	(25, 'Arctic', NULL, NULL, NULL, NULL),
	(26, 'Falkland Is (Argentine-occupied)', NULL, NULL, NULL, NULL),
	(27, 'Argentina', NULL, NULL, NULL, NULL),
	(28, 'Armenia', NULL, NULL, NULL, NULL),
	(29, 'Aruba', NULL, NULL, NULL, NULL),
	(30, 'American Samoa', NULL, NULL, NULL, NULL),
	(31, 'Asia', NULL, NULL, NULL, NULL),
	(32, 'Australia', NULL, NULL, NULL, NULL),
	(33, 'Antarctic', NULL, NULL, NULL, NULL),
	(34, 'Atlantic', NULL, NULL, NULL, NULL),
	(35, 'Austria', NULL, NULL, NULL, NULL),
	(36, 'Virgin Islands, US', NULL, NULL, NULL, NULL),
	(37, 'Azerbaijan', NULL, NULL, NULL, NULL),
	(38, 'Azores', NULL, NULL, NULL, NULL),
	(39, 'Bahamas', NULL, NULL, NULL, NULL),
	(40, 'Baltic Sea', NULL, NULL, NULL, NULL),
	(41, 'British Antarctic Territory', NULL, NULL, NULL, NULL),
	(42, 'Barbados', NULL, NULL, NULL, NULL),
	(43, 'Bahamas (British)', NULL, NULL, NULL, NULL),
	(44, 'Burma (British)', NULL, NULL, NULL, NULL),
	(45, 'Brunei (British)', NULL, NULL, NULL, NULL),
	(46, 'Ceylon (British)', NULL, NULL, NULL, NULL),
	(47, 'Belgian Congo', NULL, NULL, NULL, NULL),
	(48, 'Cyprus (British)', NULL, NULL, NULL, NULL),
	(49, 'Burundi', NULL, NULL, NULL, NULL),
	(50, 'Belarus', NULL, NULL, NULL, NULL),
	(51, 'Benin', NULL, NULL, NULL, NULL),
	(52, 'Bermuda', NULL, NULL, NULL, NULL),
	(53, 'Fiji (British)', NULL, NULL, NULL, NULL),
	(54, 'British Guiana', NULL, NULL, NULL, NULL),
	(55, 'British Honduras', NULL, NULL, NULL, NULL),
	(56, 'British Indian Ocean Territory', NULL, NULL, NULL, NULL),
	(57, 'Jamaica (British)', NULL, NULL, NULL, NULL),
	(58, 'Burkina Faso', NULL, NULL, NULL, NULL),
	(59, 'Black Sea', NULL, NULL, NULL, NULL),
	(60, 'Belgium', NULL, NULL, NULL, NULL),
	(61, 'Belize (British)', NULL, NULL, NULL, NULL),
	(62, 'Burma', NULL, NULL, NULL, NULL),
	(63, 'Malta (British)', NULL, NULL, NULL, NULL),
	(64, 'Mauritius (British)', NULL, NULL, NULL, NULL),
	(65, 'British North Borneo', NULL, NULL, NULL, NULL),
	(66, 'Bangladesh', NULL, NULL, NULL, NULL),
	(67, 'Bolivia', NULL, NULL, NULL, NULL),
	(68, 'Bonaire', NULL, NULL, NULL, NULL),
	(69, 'Bosnia & Herzegovina', NULL, NULL, NULL, NULL),
	(70, 'Pitcairn', NULL, NULL, NULL, NULL),
	(71, 'Palestine (British Mandate)', NULL, NULL, NULL, NULL),
	(72, 'British India', NULL, NULL, NULL, NULL),
	(73, 'Bahrain', NULL, NULL, NULL, NULL),
	(74, 'Brunei', NULL, NULL, NULL, NULL),
	(75, 'Brazil', NULL, NULL, NULL, NULL),
	(76, 'British South Africa', NULL, NULL, NULL, NULL),
	(77, 'Singapore (British)', NULL, NULL, NULL, NULL),
	(78, 'British Somaliland', NULL, NULL, NULL, NULL),
	(79, 'Straits Settlements (British)', NULL, NULL, NULL, NULL),
	(80, 'Seychelles (British)', NULL, NULL, NULL, NULL),
	(81, 'Botswana', NULL, NULL, NULL, NULL),
	(82, 'Bulgaria', NULL, NULL, NULL, NULL),
	(83, 'Virgin Islands, British', NULL, NULL, NULL, NULL),
	(84, 'Belize', NULL, NULL, NULL, NULL),
	(85, 'Zanzibar (British)', NULL, NULL, NULL, NULL),
	(86, 'Central African Republic', NULL, NULL, NULL, NULL),
	(87, 'Cambodia', NULL, NULL, NULL, NULL),
	(88, 'Canada', NULL, NULL, NULL, NULL),
	(89, 'Caribbean', NULL, NULL, NULL, NULL),
	(90, 'Cayman Islands', NULL, NULL, NULL, NULL),
	(91, 'Congo (Democratic Republic)', NULL, NULL, NULL, NULL),
	(92, 'Ceylon', NULL, NULL, NULL, NULL),
	(93, 'Congo Free State', NULL, NULL, NULL, NULL),
	(94, 'Chad', NULL, NULL, NULL, NULL),
	(95, '', NULL, NULL, NULL, NULL),
	(96, 'China, Japanese Occupied', NULL, NULL, NULL, NULL),
	(97, 'Chile', NULL, NULL, NULL, NULL),
	(98, 'Chinese Empire', NULL, NULL, NULL, NULL),
	(99, 'China, People\'s Republic of', NULL, NULL, NULL, NULL),
	(100, 'Chinese Taipei', NULL, NULL, NULL, NULL),
	(101, 'Cook Islands', NULL, NULL, NULL, NULL),
	(102, 'Cambodia', NULL, NULL, NULL, NULL),
	(103, 'French Cameroun', NULL, NULL, NULL, NULL),
	(104, 'Christmas Island', NULL, NULL, NULL, NULL),
	(105, 'Cameroon', NULL, NULL, NULL, NULL),
	(106, 'Comoros', NULL, NULL, NULL, NULL),
	(107, 'Canada -Lakes', NULL, NULL, NULL, NULL),
	(108, 'Canary Islands', NULL, NULL, NULL, NULL),
	(109, 'Cocos (Keeling) Islands', NULL, NULL, NULL, NULL),
	(110, 'Congo (Kinshasa)', NULL, NULL, NULL, NULL),
	(111, 'Colombia', NULL, NULL, NULL, NULL),
	(112, 'Comores (French)', NULL, NULL, NULL, NULL),
	(113, 'Congo', NULL, NULL, NULL, NULL),
	(114, 'Costa Rica', NULL, NULL, NULL, NULL),
	(115, 'Croatia', NULL, NULL, NULL, NULL),
	(116, 'Croatia', NULL, NULL, NULL, NULL),
	(117, 'Spain (CSR)', NULL, NULL, NULL, NULL),
	(118, 'Cuba', NULL, NULL, NULL, NULL),
	(119, 'Curacao', NULL, NULL, NULL, NULL),
	(120, 'Cape Verde Is. (Portuguese)', NULL, NULL, NULL, NULL),
	(121, 'Cyprus', NULL, NULL, NULL, NULL),
	(122, 'Czechoslovakia', NULL, NULL, NULL, NULL),
	(123, 'Czech Republic', NULL, NULL, NULL, NULL),
	(124, 'Dahomey', NULL, NULL, NULL, NULL),
	(125, 'Danzig Free City', NULL, NULL, NULL, NULL),
	(126, 'Denmark', NULL, NULL, NULL, NULL),
	(127, 'Denmark (DIS)', NULL, NULL, NULL, NULL),
	(128, 'Djibouti', NULL, NULL, NULL, NULL),
	(129, 'Dominica', NULL, NULL, NULL, NULL),
	(130, 'Dominica (British)', NULL, NULL, NULL, NULL),
	(131, 'Dominican Republic', NULL, NULL, NULL, NULL),
	(132, 'U.A.E. (Dubai) (Do Not Use)', NULL, NULL, NULL, NULL),
	(133, 'East Africa', NULL, NULL, NULL, NULL),
	(134, 'Ecuador', NULL, NULL, NULL, NULL),
	(135, 'Eastern Europe', NULL, NULL, NULL, NULL),
	(136, 'Egypt (British)', NULL, NULL, NULL, NULL),
	(137, 'Egypt (Turkish)', NULL, NULL, NULL, NULL),
	(138, 'Egypt', NULL, NULL, NULL, NULL),
	(139, 'Eire', NULL, NULL, NULL, NULL),
	(140, 'El Salvador', NULL, NULL, NULL, NULL),
	(141, 'East Pakistan', NULL, NULL, NULL, NULL),
	(142, 'Equatorial Guinea', NULL, NULL, NULL, NULL),
	(143, 'Eritrea', NULL, NULL, NULL, NULL),
	(144, 'Eritrea', NULL, NULL, NULL, NULL),
	(145, 'Estonia (pre-1940)', NULL, NULL, NULL, NULL),
	(146, 'Ethiopia', NULL, NULL, NULL, NULL),
	(147, 'Timor-Leste', NULL, NULL, NULL, NULL),
	(148, 'Estonia', NULL, NULL, NULL, NULL),
	(149, 'Europe', NULL, NULL, NULL, NULL),
	(150, 'Falkland Islands', NULL, NULL, NULL, NULL),
	(151, 'Faeroe Islands', NULL, NULL, NULL, NULL),
	(152, 'Faeroes (FAS)', NULL, NULL, NULL, NULL),
	(153, 'French Antarctic Territory', NULL, NULL, NULL, NULL),
	(154, 'French Congo', NULL, NULL, NULL, NULL),
	(155, 'Far East', NULL, NULL, NULL, NULL),
	(156, 'French Guiana', NULL, NULL, NULL, NULL),
	(157, 'French Indo-China', NULL, NULL, NULL, NULL),
	(158, 'South Georgia', NULL, NULL, NULL, NULL),
	(159, 'Fiji', NULL, NULL, NULL, NULL),
	(160, 'Finland', NULL, NULL, NULL, NULL),
	(161, 'France (FIS)', NULL, NULL, NULL, NULL),
	(162, 'French Morocco', NULL, NULL, NULL, NULL),
	(163, 'Flag not required', NULL, NULL, NULL, NULL),
	(164, 'French Polynesia', NULL, NULL, NULL, NULL),
	(165, 'France', NULL, NULL, NULL, NULL),
	(166, 'French India', NULL, NULL, NULL, NULL),
	(167, 'French Somaliland', NULL, NULL, NULL, NULL),
	(168, 'Tunisia (French)', NULL, NULL, NULL, NULL),
	(169, 'U.A.E. (Fujairah) (Do Not Use)', NULL, NULL, NULL, NULL),
	(170, 'French West Africa', NULL, NULL, NULL, NULL),
	(171, 'Gabon', NULL, NULL, NULL, NULL),
	(172, 'Gambia', NULL, NULL, NULL, NULL),
	(173, 'London Greek', NULL, NULL, NULL, NULL),
	(174, 'United Kingdom (Historic)', NULL, NULL, NULL, NULL),
	(175, 'United Kingdom', NULL, NULL, NULL, NULL),
	(176, 'United Kingdom (Minor)', NULL, NULL, NULL, NULL),
	(177, 'United Kingdom (London)', NULL, NULL, NULL, NULL),
	(178, 'Gilbert Islands', NULL, NULL, NULL, NULL),
	(179, 'Guadeloupe', NULL, NULL, NULL, NULL),
	(180, 'German Democratic Republic', NULL, NULL, NULL, NULL),
	(181, 'German East Africa', NULL, NULL, NULL, NULL),
	(182, 'Georgia', NULL, NULL, NULL, NULL),
	(183, 'Germany (pre-1949)', NULL, NULL, NULL, NULL),
	(184, 'Germany', NULL, NULL, NULL, NULL),
	(185, 'Germany, Federal Rep. of (ISR)', NULL, NULL, NULL, NULL),
	(186, 'Germany, Federal Republic of', NULL, NULL, NULL, NULL),
	(187, 'Ghana', NULL, NULL, NULL, NULL),
	(188, 'Gibraltar', NULL, NULL, NULL, NULL),
	(189, 'Gilbert & Ellice Islands', NULL, NULL, NULL, NULL),
	(190, 'Guinea-Bissau', NULL, NULL, NULL, NULL),
	(191, 'Portuguese India', NULL, NULL, NULL, NULL),
	(192, 'German-occupied Belgium', NULL, NULL, NULL, NULL),
	(193, 'Bohemia-Moravia', NULL, NULL, NULL, NULL),
	(194, 'German-occupied Denmark', NULL, NULL, NULL, NULL),
	(195, 'German-occupied Ostland', NULL, NULL, NULL, NULL),
	(196, 'German-occupied France', NULL, NULL, NULL, NULL),
	(197, 'German-occupied Greece', NULL, NULL, NULL, NULL),
	(198, 'German-occupied Netherlands', NULL, NULL, NULL, NULL),
	(199, 'German-occupied Italy', NULL, NULL, NULL, NULL),
	(200, 'Gold Coast', NULL, NULL, NULL, NULL),
	(201, 'German-occupied Norway', NULL, NULL, NULL, NULL),
	(202, 'German-occupied Poland', NULL, NULL, NULL, NULL),
	(203, 'German-occupied Ukraine', NULL, NULL, NULL, NULL),
	(204, 'German-occupied Yugoslavia', NULL, NULL, NULL, NULL),
	(205, 'German Papua', NULL, NULL, NULL, NULL),
	(206, 'Greece', NULL, NULL, NULL, NULL),
	(207, 'Grenada', NULL, NULL, NULL, NULL),
	(208, 'Greenland', NULL, NULL, NULL, NULL),
	(209, 'German South West Africa', NULL, NULL, NULL, NULL),
	(210, 'Guatemala', NULL, NULL, NULL, NULL),
	(211, 'Guernsey', NULL, NULL, NULL, NULL),
	(212, 'Guinea', NULL, NULL, NULL, NULL),
	(213, 'The Gulf', NULL, NULL, NULL, NULL),
	(214, 'Guam', NULL, NULL, NULL, NULL),
	(215, 'Guyana', NULL, NULL, NULL, NULL),
	(216, 'Haiti', NULL, NULL, NULL, NULL),
	(217, 'Hawaii, American', NULL, NULL, NULL, NULL),
	(218, 'El Hejaz (Turkish)', NULL, NULL, NULL, NULL),
	(219, 'Tonga (British)', NULL, NULL, NULL, NULL),
	(220, 'Tangier International Zone', NULL, NULL, NULL, NULL),
	(221, 'Tanganyika (British)', NULL, NULL, NULL, NULL),
	(222, 'Thailand', NULL, NULL, NULL, NULL),
	(223, 'Trieste International Zone', NULL, NULL, NULL, NULL),
	(224, 'Turks & Caicos Islands', NULL, NULL, NULL, NULL),
	(225, 'Tokelau', NULL, NULL, NULL, NULL),
	(226, 'Tuvalu', NULL, NULL, NULL, NULL),
	(227, 'Togo', NULL, NULL, NULL, NULL),
	(228, 'Tonga', NULL, NULL, NULL, NULL),
	(229, 'Trinidad & Tobago (British)', NULL, NULL, NULL, NULL),
	(230, 'Trinidad & Tobago', NULL, NULL, NULL, NULL),
	(231, 'Turkey', NULL, NULL, NULL, NULL),
	(232, 'Trucial States', NULL, NULL, NULL, NULL),
	(233, 'Pacific Is., U.S. Trust Territory', NULL, NULL, NULL, NULL),
	(234, 'Tunisia', NULL, NULL, NULL, NULL),
	(235, 'Turkmenistan', NULL, NULL, NULL, NULL),
	(236, 'Tuvalu (British)', NULL, NULL, NULL, NULL),
	(237, 'Taiwan (Do Not Use)', NULL, NULL, NULL, NULL),
	(238, 'Tanzania (Zanzibar)', NULL, NULL, NULL, NULL),
	(239, 'United Arab Emirates', NULL, NULL, NULL, NULL),
	(240, 'Uganda', NULL, NULL, NULL, NULL),
	(241, 'Ukraine', NULL, NULL, NULL, NULL),
	(242, 'Ukraine (pre-1922)', NULL, NULL, NULL, NULL),
	(243, 'U.A.E. (Umm Al Qaiwain) (Do Not Use)', NULL, NULL, NULL, NULL),
	(244, 'Unknown', NULL, NULL, NULL, NULL),
	(245, 'Upper Volta', NULL, NULL, NULL, NULL),
	(246, 'Uruguay', NULL, NULL, NULL, NULL),
	(247, 'United States of America', NULL, NULL, NULL, NULL),
	(248, 'New York Greek', NULL, NULL, NULL, NULL),
	(249, 'United States of America-Lakes', NULL, NULL, NULL, NULL),
	(250, 'United States of America', NULL, NULL, NULL, NULL),
	(251, 'Philippine Islands (U.S.A.)', NULL, NULL, NULL, NULL),
	(252, 'U.S.S.R.', NULL, NULL, NULL, NULL),
	(253, 'Uzbekistan', NULL, NULL, NULL, NULL),
	(254, 'Vanuatu', NULL, NULL, NULL, NULL),
	(255, 'Vatican City', NULL, NULL, NULL, NULL),
	(256, 'Vichy France', NULL, NULL, NULL, NULL),
	(257, 'Venezuela', NULL, NULL, NULL, NULL),
	(258, 'North Vietnam', NULL, NULL, NULL, NULL),
	(259, 'South Vietnam', NULL, NULL, NULL, NULL),
	(260, 'Vietnam', NULL, NULL, NULL, NULL),
	(261, 'West Africa', NULL, NULL, NULL, NULL),
	(262, 'Western Europe', NULL, NULL, NULL, NULL),
	(263, 'Wallis & Futuna', NULL, NULL, NULL, NULL),
	(264, 'Pakistan', NULL, NULL, NULL, NULL),
	(265, 'Western Samoa', NULL, NULL, NULL, NULL),
	(266, 'Western Sahara', NULL, NULL, NULL, NULL),
	(267, 'Yemen Arab Republic', NULL, NULL, NULL, NULL),
	(268, 'Yemen', NULL, NULL, NULL, NULL),
	(269, 'South Yemen', NULL, NULL, NULL, NULL),
	(270, 'Yemen (Turkish)', NULL, NULL, NULL, NULL),
	(271, 'Yemen, People\'s Democratic Republic of', NULL, NULL, NULL, NULL),
	(272, 'Yugoslavia', NULL, NULL, NULL, NULL),
	(273, 'Zaire', NULL, NULL, NULL, NULL),
	(274, 'Zambia', NULL, NULL, NULL, NULL),
	(275, 'Zanzibar', NULL, NULL, NULL, NULL),
	(276, 'Zimbabwe', NULL, NULL, NULL, NULL),
	(277, 'Tanzania (SUMATRA)', NULL, NULL, NULL, NULL),
	(278, 'Najd & Hejaz', NULL, NULL, NULL, NULL),
	(279, 'Hong Kong, China', NULL, NULL, NULL, NULL),
	(280, 'Honduras', NULL, NULL, NULL, NULL),
	(281, 'Hungary', NULL, NULL, NULL, NULL),
	(282, 'Owners System Only', NULL, NULL, NULL, NULL),
	(283, 'Iceland', NULL, NULL, NULL, NULL),
	(284, 'Indonesia', NULL, NULL, NULL, NULL),
	(285, 'Italian Eritrea', NULL, NULL, NULL, NULL),
	(286, 'Irish Free State', NULL, NULL, NULL, NULL),
	(287, 'Cyrenaica & Tripolitania', NULL, NULL, NULL, NULL),
	(288, 'India', NULL, NULL, NULL, NULL),
	(289, 'International Areas', NULL, NULL, NULL, NULL),
	(290, 'Indian Ocean', NULL, NULL, NULL, NULL),
	(291, 'Iraqi-occupied Kuwait', NULL, NULL, NULL, NULL),
	(292, 'Lebanon (Israeli-occupied)', NULL, NULL, NULL, NULL),
	(293, 'Isle of Man', NULL, NULL, NULL, NULL),
	(294, 'Iran', NULL, NULL, NULL, NULL),
	(295, 'Irish Republic', NULL, NULL, NULL, NULL),
	(296, 'Iraq', NULL, NULL, NULL, NULL),
	(297, 'Indian Sub-Continent', NULL, NULL, NULL, NULL),
	(298, 'Italian Somaliland', NULL, NULL, NULL, NULL),
	(299, 'Israel', NULL, NULL, NULL, NULL),
	(300, 'Italy', NULL, NULL, NULL, NULL),
	(301, 'Cote d\'Ivoire', NULL, NULL, NULL, NULL),
	(302, 'Ivory Coast (French)', NULL, NULL, NULL, NULL),
	(303, 'Jamaica', NULL, NULL, NULL, NULL),
	(304, 'Japan', NULL, NULL, NULL, NULL),
	(305, 'Jersey', NULL, NULL, NULL, NULL),
	(306, 'Japanese Mandate Islands', NULL, NULL, NULL, NULL),
	(307, 'Japanese-occupied Malaya', NULL, NULL, NULL, NULL),
	(308, 'Jordan', NULL, NULL, NULL, NULL),
	(309, 'Formosa (Japanese)', NULL, NULL, NULL, NULL),
	(310, 'Japanese Kwantung', NULL, NULL, NULL, NULL),
	(311, 'Japanese Liaotung', NULL, NULL, NULL, NULL),
	(312, 'Japan', NULL, NULL, NULL, NULL),
	(313, 'Kamerun (German)', NULL, NULL, NULL, NULL),
	(314, 'Kazakhstan', NULL, NULL, NULL, NULL),
	(315, 'Kenya (British)', NULL, NULL, NULL, NULL),
	(316, 'Kenya', NULL, NULL, NULL, NULL),
	(317, 'Khmer Republic', NULL, NULL, NULL, NULL),
	(318, 'Kiribati', NULL, NULL, NULL, NULL),
	(319, 'China, Kuomintang Mainland', NULL, NULL, NULL, NULL),
	(320, 'Kampuchea', NULL, NULL, NULL, NULL),
	(321, 'Korea', NULL, NULL, NULL, NULL),
	(322, 'Japanese Korea', NULL, NULL, NULL, NULL),
	(323, 'Korea, North', NULL, NULL, NULL, NULL),
	(324, 'Korea, South', NULL, NULL, NULL, NULL),
	(325, 'Kuwait', NULL, NULL, NULL, NULL),
	(326, 'Kyrgyzstan', NULL, NULL, NULL, NULL),
	(327, 'Latin America', NULL, NULL, NULL, NULL),
	(328, 'Laos', NULL, NULL, NULL, NULL),
	(329, 'Latvia (pre-1940)', NULL, NULL, NULL, NULL),
	(330, 'Latvia', NULL, NULL, NULL, NULL),
	(331, 'Libya', NULL, NULL, NULL, NULL),
	(332, 'Lebanon', NULL, NULL, NULL, NULL),
	(333, 'Lesotho', NULL, NULL, NULL, NULL),
	(334, 'Levant', NULL, NULL, NULL, NULL),
	(335, 'Liberia', NULL, NULL, NULL, NULL),
	(336, 'Liechtenstein', NULL, NULL, NULL, NULL),
	(337, 'Lithuania (pre-1940)', NULL, NULL, NULL, NULL),
	(338, 'Lithuania', NULL, NULL, NULL, NULL),
	(339, 'Luxembourg', NULL, NULL, NULL, NULL),
	(340, 'Macau, China', NULL, NULL, NULL, NULL),
	(341, 'Madagascar (French)', NULL, NULL, NULL, NULL),
	(342, 'Marshall Islands', NULL, NULL, NULL, NULL),
	(343, 'Malaysia', NULL, NULL, NULL, NULL),
	(344, 'Portugal (MAR)', NULL, NULL, NULL, NULL),
	(345, 'Mauritania', NULL, NULL, NULL, NULL),
	(346, 'Macedonia', NULL, NULL, NULL, NULL),
	(347, 'Manchuria', NULL, NULL, NULL, NULL),
	(348, 'Manchukuo (Japanese)', NULL, NULL, NULL, NULL),
	(349, 'Maldive Islands', NULL, NULL, NULL, NULL),
	(350, 'Middle East', NULL, NULL, NULL, NULL),
	(351, 'Mediterranean', NULL, NULL, NULL, NULL),
	(352, 'Mesopotamia (Turkish)', NULL, NULL, NULL, NULL),
	(353, 'Mexico', NULL, NULL, NULL, NULL),
	(354, 'Mongolia', NULL, NULL, NULL, NULL),
	(355, 'Madagascar', NULL, NULL, NULL, NULL),
	(356, 'Micronesia', NULL, NULL, NULL, NULL),
	(357, 'Malaya (British)', NULL, NULL, NULL, NULL),
	(358, 'Mali', NULL, NULL, NULL, NULL),
	(359, 'Malaya', NULL, NULL, NULL, NULL),
	(360, 'Mocambique (Portuguese)', NULL, NULL, NULL, NULL),
	(361, 'Moldova', NULL, NULL, NULL, NULL),
	(362, 'Monaco', NULL, NULL, NULL, NULL),
	(363, 'Morocco', NULL, NULL, NULL, NULL),
	(364, 'Mozambique', NULL, NULL, NULL, NULL),
	(365, 'Montserrat', NULL, NULL, NULL, NULL),
	(366, 'Malta', NULL, NULL, NULL, NULL),
	(367, 'Montenegro', NULL, NULL, NULL, NULL),
	(368, 'Martinique', NULL, NULL, NULL, NULL),
	(369, 'Mauritius', NULL, NULL, NULL, NULL),
	(370, 'Muscat & Oman', NULL, NULL, NULL, NULL),
	(371, 'British Maldive Islands', NULL, NULL, NULL, NULL),
	(372, 'Malawi', NULL, NULL, NULL, NULL),
	(373, 'Myanmar', NULL, NULL, NULL, NULL),
	(374, 'Mayotte', NULL, NULL, NULL, NULL),
	(375, 'North Africa', NULL, NULL, NULL, NULL),
	(376, 'North America', NULL, NULL, NULL, NULL),
	(377, 'North Atlantic', NULL, NULL, NULL, NULL),
	(378, 'Nauru', NULL, NULL, NULL, NULL),
	(379, 'Namibia', NULL, NULL, NULL, NULL),
	(380, 'New Caledonia', NULL, NULL, NULL, NULL),
	(381, 'Northern Cyprus', NULL, NULL, NULL, NULL),
	(382, 'Netherlands Antilles', NULL, NULL, NULL, NULL),
	(383, 'Netherlands East Indies', NULL, NULL, NULL, NULL),
	(384, 'Nepal', NULL, NULL, NULL, NULL),
	(385, 'Norfolk Island', NULL, NULL, NULL, NULL),
	(386, 'Newfoundland', NULL, NULL, NULL, NULL),
	(387, 'Niger', NULL, NULL, NULL, NULL),
	(388, 'New Hebrides', NULL, NULL, NULL, NULL),
	(389, 'Nicaragua', NULL, NULL, NULL, NULL),
	(390, 'Nigeria', NULL, NULL, NULL, NULL),
	(391, 'Norway (NIS)', NULL, NULL, NULL, NULL),
	(392, 'Niue', NULL, NULL, NULL, NULL),
	(393, 'Neutral Zone', NULL, NULL, NULL, NULL),
	(394, 'Northern Mariana Islands', NULL, NULL, NULL, NULL),
	(395, 'Norway', NULL, NULL, NULL, NULL),
	(396, 'Netherlands', NULL, NULL, NULL, NULL),
	(397, 'Norwegian Antarctic Territory', NULL, NULL, NULL, NULL),
	(398, 'New Zealand Antarctic Territory', NULL, NULL, NULL, NULL),
	(399, 'New Zealand', NULL, NULL, NULL, NULL),
	(400, 'Oceania', NULL, NULL, NULL, NULL),
	(401, 'Okinawa (US occupation)', NULL, NULL, NULL, NULL),
	(402, 'Ryukyu Islands (Japan)', NULL, NULL, NULL, NULL),
	(403, 'Oman', NULL, NULL, NULL, NULL),
	(404, 'Austrian Empire', NULL, NULL, NULL, NULL),
	(405, 'Pacific', NULL, NULL, NULL, NULL),
	(406, 'Pakistan', NULL, NULL, NULL, NULL),
	(407, 'Palau', NULL, NULL, NULL, NULL),
	(408, 'Panama', NULL, NULL, NULL, NULL),
	(409, 'Papua (Australian)', NULL, NULL, NULL, NULL),
	(410, 'Paraguay', NULL, NULL, NULL, NULL),
	(411, 'Panama Canal Zone', NULL, NULL, NULL, NULL),
	(412, 'Peru', NULL, NULL, NULL, NULL),
	(413, 'Portuguese Guinea', NULL, NULL, NULL, NULL),
	(414, 'Philippines', NULL, NULL, NULL, NULL),
	(415, 'Philippine Islands (Japanese)', NULL, NULL, NULL, NULL),
	(416, 'Madeira', NULL, NULL, NULL, NULL),
	(417, 'Papua New Guinea', NULL, NULL, NULL, NULL),
	(418, 'Poland', NULL, NULL, NULL, NULL),
	(419, 'Puerto Rico', NULL, NULL, NULL, NULL),
	(420, 'Sao Tome & Principe', NULL, NULL, NULL, NULL),
	(421, 'Portugal', NULL, NULL, NULL, NULL),
	(422, 'Portuguese Timor', NULL, NULL, NULL, NULL),
	(423, 'Qatar', NULL, NULL, NULL, NULL),
	(424, 'U.A.E. (Ras Al Khaimah)  (Do Not Use)', NULL, NULL, NULL, NULL),
	(425, 'Cape Verde', NULL, NULL, NULL, NULL),
	(426, 'Red Sea', NULL, NULL, NULL, NULL),
	(427, 'Reunion', NULL, NULL, NULL, NULL),
	(428, 'Rhodesia', NULL, NULL, NULL, NULL),
	(429, 'Romania (RIFA)', NULL, NULL, NULL, NULL),
	(430, 'Russian Liaotung', NULL, NULL, NULL, NULL),
	(431, 'Russia', NULL, NULL, NULL, NULL),
	(432, 'Romania', NULL, NULL, NULL, NULL),
	(433, 'Russia', NULL, NULL, NULL, NULL),
	(434, 'White Russia', NULL, NULL, NULL, NULL),
	(435, 'Rwanda', NULL, NULL, NULL, NULL),
	(436, 'Saba', NULL, NULL, NULL, NULL),
	(437, 'South Africa', NULL, NULL, NULL, NULL),
	(438, 'South America', NULL, NULL, NULL, NULL),
	(439, 'South Atlantic', NULL, NULL, NULL, NULL),
	(440, 'Saudi Arabia', NULL, NULL, NULL, NULL),
	(441, 'Serbia & Montenegro', NULL, NULL, NULL, NULL),
	(442, 'St-Barthelemy', NULL, NULL, NULL, NULL),
	(443, 'Scandinavia', NULL, NULL, NULL, NULL),
	(444, 'South East Asia', NULL, NULL, NULL, NULL),
	(445, 'Senegal', NULL, NULL, NULL, NULL),
	(446, 'Serbia', NULL, NULL, NULL, NULL),
	(447, 'St Eustatius', NULL, NULL, NULL, NULL),
	(448, 'Seychelles', NULL, NULL, NULL, NULL),
	(449, 'South Africa, Union of', NULL, NULL, NULL, NULL),
	(450, 'U.A.E. (Sharjah) (Do Not Use)', NULL, NULL, NULL, NULL),
	(451, 'Shanghai, French Concession', NULL, NULL, NULL, NULL),
	(452, 'Shanghai International Zone', NULL, NULL, NULL, NULL),
	(453, 'Sierra Leone', NULL, NULL, NULL, NULL),
	(454, 'St Maarten', NULL, NULL, NULL, NULL),
	(455, 'St Kitts & Nevis', NULL, NULL, NULL, NULL),
	(456, 'Soviet Kwantung', NULL, NULL, NULL, NULL),
	(457, 'Sierra Leone (British)', NULL, NULL, NULL, NULL),
	(458, 'St Lucia', NULL, NULL, NULL, NULL),
	(459, 'Solomon Islands', NULL, NULL, NULL, NULL),
	(460, 'Slovenia', NULL, NULL, NULL, NULL),
	(461, 'San Marino', NULL, NULL, NULL, NULL),
	(462, 'Samoa', NULL, NULL, NULL, NULL),
	(463, 'Spanish Morocco (Do Not Use)', NULL, NULL, NULL, NULL),
	(464, 'St-Martin', NULL, NULL, NULL, NULL),
	(465, 'Southern Africa', NULL, NULL, NULL, NULL),
	(466, 'Singapore', NULL, NULL, NULL, NULL),
	(467, 'Suriname', NULL, NULL, NULL, NULL),
	(468, 'British Solomon Islands', NULL, NULL, NULL, NULL),
	(469, 'Somalia', NULL, NULL, NULL, NULL),
	(470, 'Spanish Guinea', NULL, NULL, NULL, NULL),
	(471, 'St Pierre & Miquelon', NULL, NULL, NULL, NULL),
	(472, 'Spain', NULL, NULL, NULL, NULL),
	(473, 'Philippine Islands (Spanish)', NULL, NULL, NULL, NULL),
	(474, 'Spanish Equatorial Guinea', NULL, NULL, NULL, NULL),
	(475, 'Serbia', NULL, NULL, NULL, NULL),
	(476, 'Sri Lanka', NULL, NULL, NULL, NULL),
	(477, 'South Sudan', NULL, NULL, NULL, NULL),
	(478, 'Spanish Sahara', NULL, NULL, NULL, NULL),
	(479, 'St Helena', NULL, NULL, NULL, NULL),
	(480, 'St Kitts-Nevis (British)', NULL, NULL, NULL, NULL),
	(481, 'St Lucia (British)', NULL, NULL, NULL, NULL),
	(482, 'St Vincent (British)', NULL, NULL, NULL, NULL),
	(483, 'Sudan', NULL, NULL, NULL, NULL),
	(484, 'Surinam, Netherlands', NULL, NULL, NULL, NULL),
	(485, 'Svalbard (Spitsbergen)', NULL, NULL, NULL, NULL),
	(486, 'St Vincent & The Grenadines', NULL, NULL, NULL, NULL),
	(487, 'Slovakia', NULL, NULL, NULL, NULL),
	(488, 'South West Africa', NULL, NULL, NULL, NULL),
	(489, 'Sweden', NULL, NULL, NULL, NULL),
	(490, 'Switzerland', NULL, NULL, NULL, NULL),
	(491, 'Syria (French Mandate)', NULL, NULL, NULL, NULL),
	(492, 'Syria (Lebanese Occupied)', NULL, NULL, NULL, NULL),
	(493, 'Syria', NULL, NULL, NULL, NULL),
	(494, 'Syria (Turkish)', NULL, NULL, NULL, NULL),
	(495, 'Swaziland', NULL, NULL, NULL, NULL),
	(496, 'Terr. of The Afars & Issas', NULL, NULL, NULL, NULL),
	(497, 'Tajikistan', NULL, NULL, NULL, NULL),
	(498, 'Tanzania', NULL, NULL, NULL, NULL),
	(499, 'Cyprus, Turkish-occupied', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_FlagAdministration` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_HarmonisationType
CREATE TABLE IF NOT EXISTS `MAST_REF_HarmonisationType` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(10) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_HarmonisationType: ~3 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_HarmonisationType` DISABLE KEYS */;
INSERT INTO `MAST_REF_HarmonisationType` (`id`, `name`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Full', NULL, NULL, NULL),
	(2, 'Partial', NULL, NULL, NULL),
	(3, 'Control', NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_HarmonisationType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ItemClass
CREATE TABLE IF NOT EXISTS `MAST_REF_ItemClass` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_item_class_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ItemClass: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ItemClass` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ItemClass` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ItemReviewStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ItemReviewStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ItemReviewStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ItemReviewStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ItemReviewStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ItemStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ItemStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ItemStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ItemStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ItemStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_JobStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_JobStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `display_order` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_job_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_JobStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_JobStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_JobStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_LoadingCondition
CREATE TABLE IF NOT EXISTS `MAST_REF_LoadingCondition` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_loading_condition_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_LoadingCondition: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_LoadingCondition` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_LoadingCondition` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_Material
CREATE TABLE IF NOT EXISTS `MAST_REF_Material` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `material_type_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `material_type_id` (`material_type_id`),
  CONSTRAINT `FK_MATERIAL_USED_MATERIAL_TYPE` FOREIGN KEY (`material_type_id`) REFERENCES `MAST_REF_MaterialCategory` (`id`),
  CONSTRAINT `FK_MAST_REF_Material_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_Material: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_Material` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_Material` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_MaterialCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_MaterialCategory` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_material_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_MaterialCategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_MaterialCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_MaterialCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_MigrationStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_MigrationStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_MigrationStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_MigrationStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_MigrationStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_Milestone
CREATE TABLE IF NOT EXISTS `MAST_REF_Milestone` (
  `id` smallint(6) NOT NULL,
  `description` varchar(256) NOT NULL,
  `display_order` int(11) NOT NULL,
  `milestone_type_id` smallint(6) NOT NULL,
  `business_process_id` smallint(6) NOT NULL,
  `is_part_of_milestone_id` smallint(6) DEFAULT NULL,
  `employee_role_id` smallint(6) NOT NULL,
  `due_months_after_toc_acceptance` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `employee_role_id` (`employee_role_id`),
  KEY `business_process_id` (`business_process_id`),
  KEY `is_part_of_milestone_id` (`is_part_of_milestone_id`),
  KEY `milestone_type_id` (`milestone_type_id`),
  CONSTRAINT `FK_MILESTONE_MILESTONE_TYPE` FOREIGN KEY (`milestone_type_id`) REFERENCES `MAST_REF_MilestoneType` (`id`),
  CONSTRAINT `FK_MAST_REF_Milestone_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_REF_Milestone_MAST_REF_EmployeeRole` FOREIGN KEY (`employee_role_id`) REFERENCES `MAST_REF_EmployeeRole` (`id`),
  CONSTRAINT `FK_MILESTONE_BUSINESS_PROCESS` FOREIGN KEY (`business_process_id`) REFERENCES `MAST_REF_BusinessProcess` (`id`),
  CONSTRAINT `FK_MILESTONE_MILESTONE` FOREIGN KEY (`is_part_of_milestone_id`) REFERENCES `MAST_REF_Milestone` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_Milestone: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_Milestone` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_Milestone` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_MilestoneStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_MilestoneStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `last_update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_MilestoneStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_MilestoneStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_MilestoneStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_MilestoneType
CREATE TABLE IF NOT EXISTS `MAST_REF_MilestoneType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_milestone_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_MilestoneType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_MilestoneType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_MilestoneType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_OfficeRole
CREATE TABLE IF NOT EXISTS `MAST_REF_OfficeRole` (
  `id` smallint(6) NOT NULL,
  `name` varchar(5) NOT NULL,
  `description` varchar(170) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_office_role_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_OfficeRole: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_OfficeRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_OfficeRole` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_PartyRole
CREATE TABLE IF NOT EXISTS `MAST_REF_PartyRole` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `party_role_type_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `party_role_type_id` (`party_role_type_id`),
  CONSTRAINT `FK_MAST_REF_PartyRole_MAST_REF_PartyRoleType` FOREIGN KEY (`party_role_type_id`) REFERENCES `MAST_REF_PartyRoleType` (`id`),
  CONSTRAINT `FK_mast_ref_party_role_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_PartyRole: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_PartyRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_PartyRole` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_PartyRoleType
CREATE TABLE IF NOT EXISTS `MAST_REF_PartyRoleType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_customer_function_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_PartyRoleType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_PartyRoleType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_PartyRoleType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_Port
CREATE TABLE IF NOT EXISTS `MAST_REF_Port` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_port_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_Port: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_Port` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_Port` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_PortFlagAdministration
CREATE TABLE IF NOT EXISTS `MAST_REF_PortFlagAdministration` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `flag_state_id` smallint(6) NOT NULL,
  `port_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `flag_state_id` (`flag_state_id`),
  KEY `port_id` (`port_id`),
  CONSTRAINT `FK_PORT_FLAG_ADMINISTRATION_PORT` FOREIGN KEY (`port_id`) REFERENCES `MAST_REF_Port` (`id`),
  CONSTRAINT `FK_MAST_REF_PortFlagAdministration_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_PORT_FLAG_ADMINISTRATION_FLAG_ADMINISTRATION` FOREIGN KEY (`flag_state_id`) REFERENCES `MAST_REF_FlagAdministration` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_PortFlagAdministration: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_PortFlagAdministration` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_PortFlagAdministration` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_PreEICInspectionStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_PreEICInspectionStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_pre_eic_inspection_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_PreEICInspectionStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_PreEICInspectionStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_PreEICInspectionStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ProductCatalogue
CREATE TABLE IF NOT EXISTS `MAST_REF_ProductCatalogue` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `product_group_id` smallint(6) NOT NULL,
  `display_order` int(11) NOT NULL,
  `product_type_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `display_order_UNIQUE` (`display_order`),
  KEY `product_group_id` (`product_group_id`),
  KEY `product_type_id` (`product_type_id`),
  CONSTRAINT `FK_PRODUCT_CATALOGUE_PRODUCT_TYPE` FOREIGN KEY (`product_type_id`) REFERENCES `MAST_REF_ProductFamily` (`id`),
  CONSTRAINT `FK_MAST_REF_ProductCatalogue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_PRODUCT_CATALOGUE_PRODUCT_GROUP` FOREIGN KEY (`product_group_id`) REFERENCES `MAST_REF_ProductGroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ProductCatalogue: ~47 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ProductCatalogue` DISABLE KEYS */;
INSERT INTO `MAST_REF_ProductCatalogue` (`id`, `name`, `description`, `product_group_id`, `display_order`, `product_type_id`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Anti-Fouling System', NULL, 4, 2, 4, NULL, NULL, NULL),
	(2, 'Ballast Water', NULL, 5, 3, 4, NULL, NULL, NULL),
	(3, 'Hull', NULL, 1, 4, 1, NULL, NULL, NULL),
	(4, 'Lifting', NULL, 1, 5, 1, NULL, NULL, NULL),
	(5, 'Machinery', NULL, 1, 6, 1, NULL, NULL, NULL),
	(6, 'Miscellaneous Classification', NULL, 1, 7, 1, NULL, NULL, NULL),
	(7, 'Refrigeration', NULL, 1, 8, 1, NULL, NULL, NULL),
	(8, 'Transfer of Class', NULL, 1, 9, 1, NULL, NULL, NULL),
	(9, 'CP Combined', NULL, 6, 10, 4, NULL, NULL, NULL),
	(10, 'CP Sailing', NULL, 6, 11, 4, NULL, NULL, NULL),
	(11, 'CP Workboats', NULL, 6, 12, 4, NULL, NULL, NULL),
	(12, 'Megayacht', NULL, 6, 13, 4, NULL, NULL, NULL),
	(13, 'MODU', NULL, 7, 14, 4, NULL, NULL, NULL),
	(14, 'Flag State Inspection', NULL, 8, 15, 4, NULL, NULL, NULL),
	(15, 'Chemical Code', NULL, 9, 16, 4, NULL, NULL, NULL),
	(16, 'Chemical Support Vessel', NULL, 9, 17, 4, NULL, NULL, NULL),
	(17, 'Gas Code', NULL, 9, 18, 4, NULL, NULL, NULL),
	(18, 'Internat. Chem. Code', NULL, 9, 19, 4, NULL, NULL, NULL),
	(19, 'Internat. Gas Code', NULL, 9, 20, 4, NULL, NULL, NULL),
	(20, 'ADNR', NULL, 10, 21, 4, NULL, NULL, NULL),
	(21, 'International Safety Management', NULL, 2, 22, 2, NULL, NULL, NULL),
	(22, 'Intl. Ship & Port Security Code', NULL, 2, 23, 2, NULL, NULL, NULL),
	(23, 'Load Line', NULL, 11, 24, 4, NULL, NULL, NULL),
	(24, 'Quadrennial Certification', NULL, 12, 25, 4, NULL, NULL, NULL),
	(25, 'MLC', NULL, 3, 26, 2, NULL, NULL, NULL),
	(26, 'Marpol Air', NULL, 13, 27, 4, NULL, NULL, NULL),
	(27, 'Marpol Cond. Assessment', NULL, 13, 28, 4, NULL, NULL, NULL),
	(28, 'Marpol Noxious Subs.', NULL, 13, 29, 4, NULL, NULL, NULL),
	(29, 'Marpol Oil', NULL, 13, 30, 4, NULL, NULL, NULL),
	(30, 'Marpol Sewage', NULL, 13, 31, 4, NULL, NULL, NULL),
	(31, 'Canadian Pollution', NULL, 14, 32, 4, NULL, NULL, NULL),
	(32, 'Canadian Safety', NULL, 14, 33, 4, NULL, NULL, NULL),
	(33, 'Cert. of Seaworthiness', NULL, 14, 34, 4, NULL, NULL, NULL),
	(34, 'Cert of Structural Strength', NULL, 15, 35, 4, NULL, NULL, NULL),
	(35, 'Cert. Escape-Evaluation', NULL, 15, 36, 4, NULL, NULL, NULL),
	(36, 'Certificate of Stability', NULL, 15, 37, 4, NULL, NULL, NULL),
	(37, 'Bulk Cargoes', NULL, 16, 38, 4, NULL, NULL, NULL),
	(38, 'Bulk Cargoes App A&C', NULL, 16, 39, 4, NULL, NULL, NULL),
	(39, 'Dangerous Goods', NULL, 16, 40, 4, NULL, NULL, NULL),
	(40, 'Dangerous Goods Reg.19', NULL, 16, 41, 4, NULL, NULL, NULL),
	(41, 'Passenger Dangerous Goods', NULL, 16, 42, 4, NULL, NULL, NULL),
	(42, 'Passenger Ship Lightweight', NULL, 16, 43, 4, NULL, NULL, NULL),
	(43, 'Passenger Ship Safety', NULL, 16, 44, 4, NULL, NULL, NULL),
	(44, 'Radiotel.', NULL, 16, 45, 4, NULL, NULL, NULL),
	(45, 'Safcon', NULL, 16, 46, 4, NULL, NULL, NULL),
	(46, 'Safety Equipment', NULL, 16, 47, 4, NULL, NULL, NULL),
	(47, 'Tonnage', NULL, 17, 48, 4, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ProductCatalogue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ProductFamily
CREATE TABLE IF NOT EXISTS `MAST_REF_ProductFamily` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_product_family_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ProductFamily: ~4 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ProductFamily` DISABLE KEYS */;
INSERT INTO `MAST_REF_ProductFamily` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Classification', NULL, NULL, NULL, NULL),
	(2, 'MMS', NULL, NULL, NULL, NULL),
	(3, 'MSA', NULL, NULL, NULL, NULL),
	(4, 'Statutory', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ProductFamily` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ProductGroup
CREATE TABLE IF NOT EXISTS `MAST_REF_ProductGroup` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `display_order` int(11) NOT NULL,
  `product_family_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `display_order_UNIQUE` (`display_order`),
  KEY `product_family_id` (`product_family_id`),
  CONSTRAINT `FK_PRODUCT_GROUP_PRODUCT_FAMILY` FOREIGN KEY (`product_family_id`) REFERENCES `MAST_REF_ProductFamily` (`id`),
  CONSTRAINT `FK_MAST_REF_ProductGroup_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ProductGroup: ~17 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ProductGroup` DISABLE KEYS */;
INSERT INTO `MAST_REF_ProductGroup` (`id`, `name`, `description`, `display_order`, `product_family_id`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Classification', NULL, 2, 1, NULL, NULL, NULL),
	(2, 'ISM & ISP', NULL, 3, 2, NULL, NULL, NULL),
	(3, 'Maritime Labour Convention zation', NULL, 4, 2, NULL, NULL, NULL),
	(4, 'Anti-Fouling System', NULL, 5, 4, NULL, NULL, NULL),
	(5, 'Ballast Water Management', NULL, 6, 4, NULL, NULL, NULL),
	(6, 'Code of practice', NULL, 7, 4, NULL, NULL, NULL),
	(7, 'Energy/Offshore/Mobile Offshore Drilling Unit', NULL, 8, 4, NULL, NULL, NULL),
	(8, 'Flag State Inspection', NULL, 9, 4, NULL, NULL, NULL),
	(9, 'Gas & Chemical Codes', NULL, 10, 4, NULL, NULL, NULL),
	(10, 'Inland Waterways', NULL, 11, 4, NULL, NULL, NULL),
	(11, 'Loadline', NULL, 12, 4, NULL, NULL, NULL),
	(12, 'Marine/Statutory/ILO/Lifting Appliances', NULL, 13, 4, NULL, NULL, NULL),
	(13, 'MARPOL', NULL, 14, 4, NULL, NULL, NULL),
	(14, 'National Administration Specials', NULL, 15, 4, NULL, NULL, NULL),
	(15, 'Naval', NULL, 16, 4, NULL, NULL, NULL),
	(16, 'SOLAS', NULL, 17, 4, NULL, NULL, NULL),
	(17, 'Tonnage', NULL, 18, 4, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ProductGroup` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RegulationsSet
CREATE TABLE IF NOT EXISTS `MAST_REF_RegulationsSet` (
  `id` smallint(6) NOT NULL,
  `name` varchar(500) NOT NULL,
  `category` varchar(500) NOT NULL,
  `_is_lr_ruleset` tinyint(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_RegulationsSet_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RegulationsSet: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RegulationsSet` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RegulationsSet` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RepairAction
CREATE TABLE IF NOT EXISTS `MAST_REF_RepairAction` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_repair_action_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RepairAction: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RepairAction` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RepairAction` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RepairType
CREATE TABLE IF NOT EXISTS `MAST_REF_RepairType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_RepairType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RepairType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RepairType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RepairType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ReportType
CREATE TABLE IF NOT EXISTS `MAST_REF_ReportType` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_report_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ReportType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ReportType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ReportType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RiskAssessmentStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_RiskAssessmentStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_risk_assessment_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RiskAssessmentStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RiskAssessmentStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RiskAssessmentStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RulesetCategory
CREATE TABLE IF NOT EXISTS `MAST_REF_RulesetCategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_ruleset_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RulesetCategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RulesetCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RulesetCategory` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_RulesetCategory_ProductCatalogue
CREATE TABLE IF NOT EXISTS `MAST_REF_RulesetCategory_ProductCatalogue` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `ruleset_id` smallint(6) NOT NULL,
  `product_catalogue_id` smallint(6) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `product_catalogue_id` (`product_catalogue_id`),
  KEY `ruleset_id` (`ruleset_id`),
  CONSTRAINT `FK_RULESET_PRODUCT_CATALOGUE_RULESET` FOREIGN KEY (`ruleset_id`) REFERENCES `MAST_REF_RulesetCategory` (`id`),
  CONSTRAINT `FK_MAST_REF_Ruleset_ProductCatalogue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_RULESET_PRODUCT_CATALOGUE_PRODUCT_CATALOGUE` FOREIGN KEY (`product_catalogue_id`) REFERENCES `MAST_REF_ProductCatalogue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_RulesetCategory_ProductCatalogue: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_RulesetCategory_ProductCatalogue` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_RulesetCategory_ProductCatalogue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_SchedulingDueType
CREATE TABLE IF NOT EXISTS `MAST_REF_SchedulingDueType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_SchedulingDueType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_SchedulingDueType: ~1 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_SchedulingDueType` DISABLE KEYS */;
INSERT INTO `MAST_REF_SchedulingDueType` (`id`, `name`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Harmonised Renewal', NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_SchedulingDueType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_SchedulingRegime
CREATE TABLE IF NOT EXISTS `MAST_REF_SchedulingRegime` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_SchedulingRegime: ~9 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_SchedulingRegime` DISABLE KEYS */;
INSERT INTO `MAST_REF_SchedulingRegime` (`id`, `name`, `description`) VALUES
	(1, 'Standard', NULL),
	(2, 'IWW', NULL),
	(3, 'Laker', NULL),
	(4, 'Naval', NULL),
	(5, 'Ship', NULL),
	(6, 'Yacht', NULL),
	(7, 'Non HSSC', NULL),
	(8, 'HSSC', NULL),
	(9, 'MMS', NULL);
/*!40000 ALTER TABLE `MAST_REF_SchedulingRegime` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_SchedulingType
CREATE TABLE IF NOT EXISTS `MAST_REF_SchedulingType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `scheduling_due_type_id` smallint(6) NOT NULL,
  `assigned_scheduling_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `assigned_scheduling_type_id` (`assigned_scheduling_type_id`),
  KEY `scheduling_due_type_id` (`scheduling_due_type_id`),
  CONSTRAINT `FK_MAST_REF_SchedulingType_MAST_REF_SchedulingDueType` FOREIGN KEY (`scheduling_due_type_id`) REFERENCES `MAST_REF_SchedulingDueType` (`id`),
  CONSTRAINT `FK_MAST_REF_SchedulingType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_REF_SchedulingType_MAST_REF_AssignedSchedulingType` FOREIGN KEY (`assigned_scheduling_type_id`) REFERENCES `MAST_REF_AssignedSchedulingType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_SchedulingType: ~2 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_SchedulingType` DISABLE KEYS */;
INSERT INTO `MAST_REF_SchedulingType` (`id`, `name`, `deleted`, `last_update_date`, `last_update_user_id`, `scheduling_due_type_id`, `assigned_scheduling_type_id`) VALUES
	(1, '1', NULL, NULL, NULL, 1, 1),
	(2, '2', NULL, NULL, NULL, 1, 2);
/*!40000 ALTER TABLE `MAST_REF_SchedulingType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceCatalogue
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceCatalogue` (
  `id` smallint(6) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  `product_catalogue_id` smallint(6) NOT NULL,
  `display_order` int(11) NOT NULL,
  `cycle_periodicity` int(11) NOT NULL,
  `cycle_periodicity_editable` tinyint(1) NOT NULL,
  `lower_range_date_offset_months` int(11) NOT NULL,
  `upper_range_date_offset_months` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `service_category` varchar(1) DEFAULT NULL,
  `survey_type_id` smallint(6) DEFAULT NULL,
  `service_type_id` smallint(6) DEFAULT NULL,
  `defect_category_id` smallint(6) DEFAULT NULL,
  `service_ruleset_id` smallint(6) DEFAULT NULL,
  `service_group_id` smallint(6) NOT NULL,
  `scheduling_type_id` smallint(6) NOT NULL,
  `continuous_indicator` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `harmonisation_type_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `defect_category_id` (`defect_category_id`),
  KEY `scheduling_type_id` (`scheduling_type_id`),
  KEY `service_group_id` (`service_group_id`),
  KEY `service_ruleset_id` (`service_ruleset_id`),
  KEY `service_type_id` (`service_type_id`),
  KEY `display_order_UNIQUE` (`display_order`),
  KEY `product_catalogue_id` (`product_catalogue_id`),
  KEY `survey_type_id` (`survey_type_id`),
  KEY `FK_MAST_REF_ServiceCatalogue_MAST_REF_HarmonisationType` (`harmonisation_type_id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_HarmonisationType` FOREIGN KEY (`harmonisation_type_id`) REFERENCES `MAST_REF_HarmonisationType` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_DefectCategory` FOREIGN KEY (`defect_category_id`) REFERENCES `MAST_REF_DefectCategory` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_SchedulingType` FOREIGN KEY (`scheduling_type_id`) REFERENCES `MAST_REF_SchedulingType` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_ServiceGroup` FOREIGN KEY (`service_group_id`) REFERENCES `MAST_REF_ServiceGroup` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_ServiceRuleset` FOREIGN KEY (`service_ruleset_id`) REFERENCES `MAST_REF_ServiceRuleset` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_MAST_REF_ServiceType` FOREIGN KEY (`service_type_id`) REFERENCES `MAST_REF_ServiceType` (`id`),
  CONSTRAINT `FK_SERVICE_CATALOGUE_PRODUCT_CATALOGUE` FOREIGN KEY (`product_catalogue_id`) REFERENCES `MAST_REF_ProductCatalogue` (`id`),
  CONSTRAINT `FK_SERVICE_CATALOGUE_SURVEY_TYPE` FOREIGN KEY (`survey_type_id`) REFERENCES `MAST_REF_SurveyType` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceCatalogue: ~12 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceCatalogue` DISABLE KEYS */;
INSERT INTO `MAST_REF_ServiceCatalogue` (`id`, `code`, `name`, `product_catalogue_id`, `display_order`, `cycle_periodicity`, `cycle_periodicity_editable`, `lower_range_date_offset_months`, `upper_range_date_offset_months`, `description`, `service_category`, `survey_type_id`, `service_type_id`, `defect_category_id`, `service_ruleset_id`, `service_group_id`, `scheduling_type_id`, `continuous_indicator`, `deleted`, `last_update_date`, `last_update_user_id`, `harmonisation_type_id`) VALUES
	(1, 'SS', 'Hull Intermediate', 3, 2, 24, 0, 3, 3, NULL, 'H', 2, 3, NULL, 2, 1, 1, 0, NULL, NULL, NULL, 1),
	(2, 'AS', 'Docking', 3, 3, 12, 0, 3, 0, NULL, 'H', 2, 4, NULL, 1, 1, 2, 0, NULL, NULL, NULL, 1),
	(3, 'ITSS', 'Continuous - Hull', 3, 4, 24, 0, 3, 0, NULL, 'H', 1, 1, NULL, 1, 1, 2, 0, NULL, NULL, NULL, 1),
	(4, 'SS', 'Special', 3, 5, 60, 0, 3, 0, NULL, 'H', 2, 4, NULL, 1, 2, 2, 0, NULL, NULL, NULL, 1),
	(5, 'AS', 'Annual', 3, 6, 12, 0, 3, 3, NULL, 'H', 1, 1, NULL, 1, 2, 2, 0, NULL, NULL, NULL, 1),
	(6, 'SS', 'Special', 3, 7, 60, 0, 3, 0, NULL, 'H', 1, 3, NULL, 1, 1, 2, 0, NULL, NULL, NULL, 1),
	(7, 'CSM', 'Continuous - Machinery', 3, 8, 60, 0, 3, 0, NULL, 'M', 1, 3, NULL, 2, 2, 2, 0, NULL, NULL, NULL, 1),
	(8, 'ES', 'Engine Special', 5, 9, 60, 0, 3, 0, NULL, NULL, 2, 8, NULL, 1, 2, 2, 0, NULL, NULL, NULL, 1),
	(9, 'COCM', 'Condition of Class Machinery', 6, 10, 0, 0, 3, 0, NULL, NULL, 2, 8, NULL, 2, 2, 2, 1, NULL, NULL, NULL, 1),
	(10, 'CSH', 'Machinery - Other Surveys', 6, 11, 0, 0, 3, 0, NULL, NULL, 2, 8, NULL, 2, 2, 2, 0, NULL, NULL, NULL, 1),
	(11, 'SS', 'Special', 3, 7, 60, 0, 3, 0, NULL, 'H', 1, 3, NULL, 2, 3, 2, 0, NULL, NULL, NULL, 1),
	(12, 'SS', 'Special', 3, 7, 60, 0, 3, 0, NULL, NULL, 1, 3, NULL, 2, 3, 2, NULL, NULL, NULL, NULL, 1);
/*!40000 ALTER TABLE `MAST_REF_ServiceCatalogue` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceCatalogue_SchedulingRegime
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceCatalogue_SchedulingRegime` (
  `id` smallint(6) NOT NULL,
  `service_catalogue_id` smallint(6) NOT NULL,
  `scheduling_regime_id` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `scheduling_regime_id` (`scheduling_regime_id`),
  KEY `service_catalogue_id` (`service_catalogue_id`),
  CONSTRAINT `FK_SERVICE_CATALOGUE_SCHEDULING_RULE_TYPE_SERVICE_CATALOGUE` FOREIGN KEY (`service_catalogue_id`) REFERENCES `MAST_REF_ServiceCatalogue` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceCatalogue_SchedulingRegime_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_SERVICE_CATALOGUE_SCHEDULING_RULE_TYPE` FOREIGN KEY (`scheduling_regime_id`) REFERENCES `MAST_REF_SchedulingRegime` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceCatalogue_SchedulingRegime: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceCatalogue_SchedulingRegime` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ServiceCatalogue_SchedulingRegime` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceCreditStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceCreditStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceCreditStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceCreditStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_ServiceCreditStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceGroup
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceGroup` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `associated_scheduling_type` smallint(6) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `associated_scheduling_type` (`associated_scheduling_type`),
  CONSTRAINT `FK_MAST_REF_ServiceGroup_MAST_REF_AssociatedSchedulingType` FOREIGN KEY (`associated_scheduling_type`) REFERENCES `MAST_REF_AssociatedSchedulingType` (`id`),
  CONSTRAINT `FK_MAST_REF_ServiceGroup_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceGroup: ~3 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceGroup` DISABLE KEYS */;
INSERT INTO `MAST_REF_ServiceGroup` (`id`, `name`, `description`, `associated_scheduling_type`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Hull', NULL, 1, NULL, NULL, NULL),
	(2, 'Test', NULL, 2, NULL, NULL, NULL),
	(3, 'Hull', NULL, 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ServiceGroup` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceRuleset
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceRuleset` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_ServiceRuleset_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceRuleset: ~2 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceRuleset` DISABLE KEYS */;
INSERT INTO `MAST_REF_ServiceRuleset` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'RuleSet', NULL, NULL, NULL, NULL),
	(2, 'Random', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ServiceRuleset` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mast_ref_service_status_MAST_LRP_Employee` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_service_status_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceStatus: ~7 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceStatus` DISABLE KEYS */;
INSERT INTO `MAST_REF_ServiceStatus` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Planned', NULL, NULL, NULL, NULL),
	(2, 'Complete', NULL, NULL, NULL, NULL),
	(3, 'Part-Held', NULL, NULL, NULL, NULL),
	(4, 'Postponed', NULL, NULL, NULL, NULL),
	(5, 'Completed with Defect Found', NULL, NULL, NULL, NULL),
	(6, 'Not Started', NULL, NULL, NULL, NULL),
	(7, 'Finished', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ServiceStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_ServiceType
CREATE TABLE IF NOT EXISTS `MAST_REF_ServiceType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_service_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_ServiceType: ~10 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_ServiceType` DISABLE KEYS */;
INSERT INTO `MAST_REF_ServiceType` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'Intermediate', NULL, NULL, NULL, NULL),
	(2, 'Docking', NULL, NULL, NULL, NULL),
	(3, 'Renewal', NULL, NULL, NULL, NULL),
	(4, 'Annual', NULL, NULL, NULL, NULL),
	(5, 'Alternate', NULL, NULL, NULL, NULL),
	(6, 'Miscellaneous', NULL, NULL, NULL, NULL),
	(7, 'Initial', NULL, NULL, NULL, NULL),
	(8, 'Periodic', NULL, NULL, NULL, NULL),
	(9, 'Stand-Alone 2 in Special Cycle', NULL, NULL, NULL, NULL),
	(10, 'Interim', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_ServiceType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_SurveyType
CREATE TABLE IF NOT EXISTS `MAST_REF_SurveyType` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_mast_ref_survey_type_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_SurveyType: ~2 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_SurveyType` DISABLE KEYS */;
INSERT INTO `MAST_REF_SurveyType` (`id`, `name`, `description`, `deleted`, `last_update_date`, `last_update_user_id`) VALUES
	(1, 'DAMAGE', NULL, NULL, NULL, NULL),
	(2, 'REPAIR', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `MAST_REF_SurveyType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_TechnicalStatus
CREATE TABLE IF NOT EXISTS `MAST_REF_TechnicalStatus` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_TechnicalStatus_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_TechnicalStatus: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_TechnicalStatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_TechnicalStatus` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_WorkItemAction
CREATE TABLE IF NOT EXISTS `MAST_REF_WorkItemAction` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `task_category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_WorkItemAction_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_WorkItemAction: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_WorkItemAction` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_WorkItemAction` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_REF_WorkItemType
CREATE TABLE IF NOT EXISTS `MAST_REF_WorkItemType` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  CONSTRAINT `FK_MAST_REF_WorkItemType_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_REF_WorkItemType: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_REF_WorkItemType` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_REF_WorkItemType` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_XXX_AttachmentLink
CREATE TABLE IF NOT EXISTS `MAST_XXX_AttachmentLink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defect_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `codicil_id` int(11) DEFAULT NULL,
  `codicilwip_id` int(11) DEFAULT NULL,
  `codicil_template_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `repair_id` int(11) DEFAULT NULL,
  `certificate_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `location_id` smallint(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `codicil_template_id` (`codicil_template_id`),
  KEY `codicil_id` (`codicil_id`),
  KEY `location_id` (`location_id`),
  KEY `codicilwip_id` (`codicilwip_id`),
  CONSTRAINT `FK_ATTACHMENT_LINK_WIP_CODICIL` FOREIGN KEY (`codicilwip_id`) REFERENCES `MAST_JOB_CodicilWIP` (`id`),
  CONSTRAINT `FK_ATTACHMENT_LINK_ATTACHMENT` FOREIGN KEY (`id`) REFERENCES `MAST_XXX_SupplementaryInformation` (`id`),
  CONSTRAINT `FK_ATTACHMENT_LINK_ATTACHMENT_LOCATION` FOREIGN KEY (`location_id`) REFERENCES `MAST_REF_AttachmentLocation` (`id`),
  CONSTRAINT `FK_ATTACHMENT_LINK_CODICIL` FOREIGN KEY (`codicil_id`) REFERENCES `MAST_ASSET_Codicil` (`id`),
  CONSTRAINT `FK_MAST_XXX_AttachmentLink_MAST_REF_CodicilTemplate` FOREIGN KEY (`codicil_template_id`) REFERENCES `MAST_REF_CodicilTemplate` (`id`),
  CONSTRAINT `FK_MAST_XXX_Attachment_Link_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_XXX_AttachmentLink: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_XXX_AttachmentLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_XXX_AttachmentLink` ENABLE KEYS */;


-- Dumping structure for table MAST_DB.MAST_XXX_SupplementaryInformation
CREATE TABLE IF NOT EXISTS `MAST_XXX_SupplementaryInformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `attachment_url` varchar(500) NOT NULL,
  `asset_location_viewpoint` varchar(100) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `attachment_type_id` smallint(6) DEFAULT NULL,
  `confidentiality_type_id` smallint(6) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `last_update_date` datetime DEFAULT NULL,
  `last_update_user_id` int(11) DEFAULT NULL,
  `attachment_mime_type_id` smallint(6) DEFAULT NULL,
  `file_size_kb` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `last_update_user_id` (`last_update_user_id`),
  KEY `attachment_type_id` (`attachment_type_id`),
  KEY `confidentiality_type_id` (`confidentiality_type_id`),
  CONSTRAINT `FK_ATTACHMENT_CONFIDENTIALITY_TYPE` FOREIGN KEY (`confidentiality_type_id`) REFERENCES `MAST_REF_ConfidentialityType` (`id`),
  CONSTRAINT `FK_ATTACHMENT_ATTACHMENT_TYPE` FOREIGN KEY (`attachment_type_id`) REFERENCES `MAST_REF_AttachmentType` (`id`),
  CONSTRAINT `FK_mast_attachment_MAST_LRP_Employee` FOREIGN KEY (`author_id`) REFERENCES `MAST_LRP_Employee` (`id`),
  CONSTRAINT `FK_MAST_XXX_SupplementaryInformation_MAST_LRP_Employee` FOREIGN KEY (`last_update_user_id`) REFERENCES `MAST_LRP_Employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table MAST_DB.MAST_XXX_SupplementaryInformation: ~0 rows (approximately)
/*!40000 ALTER TABLE `MAST_XXX_SupplementaryInformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `MAST_XXX_SupplementaryInformation` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
SET FOREIGN_KEY_CHECKS=1;
