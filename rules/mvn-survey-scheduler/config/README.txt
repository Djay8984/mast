Lloyd's Register Survey Scheduling Service
======================================================

The purpose of this README is to give a quick installation guide of the LR Survey Scheduling Service

Contained within this folder are 3 files:
	- mast_db_dump.sql: This is the schema and exmaple test data for the MAST application
	- survey-scheduling-service.properties: This is the properties file that is used by the web application (survey-scheduling-service)
	- setenv.[bat|sh]: This is the set environment script that is used by tomat to set the CATALINA_OPTS at start up


INSTALLATION STEPS
====================
	1. Copy the setenv.[bat|sh] file to {catalina_base}/bin. Make sure you choose the right file for your environment
	2. Navigate to {catalina_base} and create a folder called app-config
	3. Copy the properties file (survey-scheduling-service.properties) to the app-config folder you just created. Edit the properties within the file
	   to reflect your own machine settings. 
	4. Edit the file catalina.properties, located within {catalina_base}/conf
		Look for the "shared.loader" property and set its value to ${catalina.base}/app-config
		Save and exit the file
	5. Edit the file context.xml, located within {catalina_base}/conf
		Add the following element within the <Context> attribute <Loader delegate="true" />
	6. Execute the mysql script mast_db_dump.sql to load the schema and data into the database
	7. Copy the web application - survey-scheduling-service - to the webapps folder and start tomcat by executing startup.[bin|sh]
