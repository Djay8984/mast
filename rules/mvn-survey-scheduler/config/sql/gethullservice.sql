SELECT MAST_REF_ServiceCatalogue.cycle_periodicity,
MAST_REF_ServiceCatalogue.continuous_indicator
FROM MAST_ASSET_ScheduledService
JOIN MAST_REF_ServiceCatalogue
ON MAST_ASSET_ScheduledService.service_catalogue_id = MAST_REF_ServiceCatalogue.id
JOIN MAST_ASSET_Asset
ON MAST_ASSET_ScheduledService.asset_id = MAST_ASSET_Asset.id
WHERE (MAST_REF_ServiceCatalogue.code = "SS"
        OR MAST_REF_ServiceCatalogue.code= "CSH")
AND  MAST_ASSET_ScheduledService.asset_id = :#serviceScheduling_assetId
AND MAST_ASSET_ScheduledService.deleted != 1
ORDER BY MAST_ASSET_ScheduledService.completion_date DESC
LIMIT 1
