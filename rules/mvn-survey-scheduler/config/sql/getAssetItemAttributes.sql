-- Retrieves attributes associated with one of a given list of Asset Items.
-- The value of the attribute can be one of the value_bool or value_string entries but not both.
-- The data type returned indicates which column holds the attribute value.
-- We return the Asset Item's ID so that we know which Item the attribute belongs to.

SELECT item_id AS id, 
       value_string, 
       value_bool, 
       MAST_REF_AssetAttributeType.name as attribute_name, 
       MAST_REF_AttributeDataType.name as data_type 
FROM MAST_ASSET_AssetItemAttribute 
JOIN MAST_REF_AssetAttributeType 
     ON MAST_ASSET_AssetItemAttribute.asset_attribute_type_id = MAST_REF_AssetAttributeType.id 
JOIN MAST_REF_AttributeDataType 
     ON MAST_REF_AssetAttributeType.attribute_datatype_id = MAST_REF_AttributeDataType.id 
WHERE MAST_ASSET_AssetItemAttribute.item_id IN (:#in:assetItemIds)
AND MAST_ASSET_AssetItemAttribute.deleted != 1