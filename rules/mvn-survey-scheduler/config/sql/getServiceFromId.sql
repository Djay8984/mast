-- Retrieves the Service Code of the specified Service.
-- The Service Code comes from the direct link to the ServiceCatalogue table if it 
-- is a one-off Service added to the Job otherwise it comes from the associated
-- ScheduledService.
-- If the Service is scoped to a particular Asset Item then its ID is retrieved.

SELECT MAST_ASSET_ScheduledService.asset_item_id,
       MAST_REF_ServiceCatalogue.code
FROM MAST_ASSET_ScheduledService 
JOIN MAST_REF_ServiceCatalogue
     ON MAST_ASSET_ScheduledService.service_catalogue_id = MAST_REF_ServiceCatalogue.id
WHERE MAST_ASSET_ScheduledService.id = :#assetServiceId
AND MAST_ASSET_ScheduledService.deleted != 1