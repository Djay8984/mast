SELECT MAST_ASSET_ScheduledService.due_date,
MAST_ASSET_ScheduledService.due_date_manual,
MAST_REF_ServiceCatalogue.cycle_periodicity,
MAST_ASSET_ScheduledService.assigned_date,
MAST_ASSET_ScheduledService.assigned_date_manual
FROM MAST_ASSET_ScheduledService
JOIN MAST_REF_ServiceCatalogue
ON MAST_ASSET_ScheduledService.service_catalogue_id = MAST_REF_ServiceCatalogue.id
JOIN MAST_REF_ServiceType
ON MAST_REF_ServiceCatalogue.service_type_id = MAST_REF_ServiceType.id
JOIN MAST_REF_ServiceCreditStatus
ON MAST_ASSET_ScheduledService.service_credit_status_id = MAST_REF_ServiceCreditStatus.id
JOIN MAST_REF_SchedulingRegime
ON MAST_REF_ServiceCatalogue.scheduling_regime_id = MAST_REF_SchedulingRegime.id
WHERE MAST_REF_ServiceCatalogue.service_group_id = :#serviceScheduling_serviceGroupId
AND MAST_REF_SchedulingRegime.name = :#serviceScheduling_schedulingRegimeName
AND MAST_ASSET_ScheduledService.asset_id = :#serviceScheduling_assetId
AND  MAST_ASSET_ScheduledService.id != :#creditedServiceId
AND MAST_REF_ServiceType.name = "Renewal"
AND (MAST_REF_ServiceCreditStatus.name="Not started"
        OR MAST_REF_ServiceCreditStatus.name="Part held"
        OR MAST_REF_ServiceCreditStatus.name= "Postponed")
AND MAST_ASSET_ScheduledService.deleted != 1
