-- Retrieve the Level 5 type code of the specified Asset and get the name of its current regulation set.

SELECT MAST_REF_AssetType.code as type_code 
FROM MAST_ASSET_Asset 
JOIN MAST_REF_AssetType 
     ON MAST_ASSET_Asset.asset_type_id = MAST_REF_AssetType.id 
WHERE MAST_ASSET_Asset.id=:#assetId
AND MAST_ASSET_Asset.deleted != 1
