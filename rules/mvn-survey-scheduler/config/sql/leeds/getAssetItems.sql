-- Retrieve all Asset Items that are linked to the specified Asset and are marked as relevant for task generation.

SELECT MAST_ASSET_AssetItem.id, 
       MAST_REF_AssetItemType.name as asset_item_type 
FROM MAST_ASSET_AssetItem 
JOIN MAST_REF_AssetItemType 
     ON MAST_ASSET_AssetItem.item_type_id = MAST_REF_AssetItemType.id 
WHERE MAST_ASSET_AssetItem.asset_id = :#assetId
AND MAST_ASSET_AssetItem.deleted != 1
