-- Retrieves attributes associated with one of a given list of Asset Items.
-- The value of the attribute can be one of the value_bool or value_string entries but not both.
-- The data type returned indicates which column holds the attribute value.
-- We return the Asset Item's ID so that we know which Item the attribute belongs to.

SELECT item_id AS id, 
       value as value_string, 
       MAST_REF_AssetAttributeType.name as attribute_name, 
       "string" as data_type 
FROM MAST_ASSET_AssetItemAttribute 
JOIN MAST_REF_AssetAttributeType 
     ON MAST_ASSET_AssetItemAttribute.attribute_type_id = MAST_REF_AssetAttributeType.id 
WHERE MAST_ASSET_AssetItemAttribute.item_id IN (:#in:assetItemIds)
AND MAST_ASSET_AssetItemAttribute.deleted !=1
