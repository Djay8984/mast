-- Retrieve the class notation for the specified Asset.

SELECT name 
FROM MAST_REF_ClassNotation 
JOIN MAST_ASSET_Asset_ClassNotation 
     ON MAST_REF_ClassNotation.id = MAST_ASSET_Asset_ClassNotation.class_notation_id 
WHERE MAST_ASSET_Asset_ClassNotation.asset_id=:#assetId
AND MAST_REF_ClassNotation.deleted != 1
