SELECT MAST_ASSET_Asset.hull_indicator,
MAST_ASSET_Asset.build_date,
MAST_ASSET_Asset.id,
MAST_REF_ClassStatus.name as class_status,
-- MAST_ASSET_Asset.commission_date,
MAST_ASSET_Asset.gross_tonnage,
MAST_REF_AssetType.code as level_5_code
FROM MAST_ASSET_ScheduledService
JOIN MAST_ASSET_Asset
ON MAST_ASSET_ScheduledService.asset_id = MAST_ASSET_Asset.id
JOIN  MAST_REF_ClassStatus
ON MAST_ASSET_Asset.class_status_id = MAST_REF_ClassStatus.id
JOIN MAST_REF_AssetType
ON MAST_ASSET_Asset.asset_type_id = MAST_REF_AssetType.id
WHERE MAST_ASSET_ScheduledService.id =  :#creditedServiceId
AND MAST_ASSET_ScheduledService.deleted != 1
