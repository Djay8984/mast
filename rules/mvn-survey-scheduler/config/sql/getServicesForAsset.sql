-- Retrieves the Service Codes of all active Services on an Asset.
-- A Service is considered active if its credit status is one of 
-- "Not Started" or "Part held".
-- If the Service is a one-off Service then its Service Code comes
-- from the link from JobServiceInstance otherwise it comes from the
-- link from ScheduledService.
-- A non-null ID of an Asset Item will be returned if the Service is 
-- a scheduled service and is scoped to an Asset Item rather than the
-- full Asset.

SELECT MAST_ASSET_ScheduledService.id,
       MAST_ASSET_ScheduledService.asset_item_id,
       MAST_REF_ServiceCatalogue.code
FROM MAST_ASSET_ScheduledService 
JOIN MAST_REF_ServiceCreditStatus
     ON MAST_ASSET_ScheduledService.service_credit_status_id=MAST_REF_ServiceCreditStatus.id
JOIN MAST_REF_ServiceCatalogue
     ON MAST_ASSET_ScheduledService.service_catalogue_id = MAST_REF_ServiceCatalogue.id
WHERE MAST_ASSET_ScheduledService.asset_id = :#assetId 
AND MAST_REF_ServiceCreditStatus.name IN ("Not started", "Part held")
AND MAST_ASSET_ScheduledService.deleted != 1;
