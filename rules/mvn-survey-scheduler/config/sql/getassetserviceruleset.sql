SELECT MAST_REF_ServiceRuleset.name
from MAST_ASSET_ScheduledService
join MAST_ASSET_Asset
on MAST_ASSET_ScheduledService.asset_id = MAST_ASSET_Asset.id
left join MAST_REF_ServiceRuleset
on MAST_ASSET_Asset.service_ruleset_id = MAST_REF_ServiceRuleset.id
where MAST_ASSET_ScheduledService.id = :#creditedServiceId
AND MAST_ASSET_ScheduledService.deleted != 1