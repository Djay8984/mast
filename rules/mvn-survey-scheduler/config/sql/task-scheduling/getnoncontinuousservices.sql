select
MAST_ASSET_ScheduledService.due_date
from MAST_JOB_WorkItem
join MAST_ASSET_ScheduledService
on MAST_JOB_WorkItem.scheduled_service_id = MAST_ASSET_ScheduledService.id
where MAST_JOB_WorkItem.id in (:#in:taskIds)