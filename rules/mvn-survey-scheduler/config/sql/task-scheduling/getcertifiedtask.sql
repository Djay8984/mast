select 
MAST_REF_ResolutionStatus.name as task_status,
MAST_JOB_WorkItem.resolution_date as completion_date,
MAST_JOB_WorkItem.id
from MAST_JOB_WorkItem
join MAST_REF_ResolutionStatus
on MAST_JOB_WorkItem.resolution_status_id = MAST_REF_ResolutionStatus.id
where MAST_JOB_WorkItem.id IN (:#in:taskIds)
