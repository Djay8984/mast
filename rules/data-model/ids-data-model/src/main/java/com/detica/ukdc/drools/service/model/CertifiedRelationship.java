package com.detica.ukdc.drools.service.model;

public class CertifiedRelationship
{

    private Service certifiedService;
    private Service associatedService;

    public Service getCertifiedService()
    {
        return this.certifiedService;
    }

    public void setCertifiedService(Service certifiedService)
    {
        this.certifiedService = certifiedService;
    }

    public Service getAssociatedService()
    {
        return this.associatedService;
    }

    public void setAssociatedService(Service associatedService)
    {
        this.associatedService = associatedService;
    }

}
