package com.detica.ukdc.drools.service.model;

import java.io.Serializable;

/**
 * Relates to Service Catalogue defined in the Logical Data Model.
 */
public class ServiceCatalogue implements Serializable
{
    private Integer identifier;
    /**
     * Relates to the Service Code attribute in the LDM.
     */
    private String serviceCode;
    /**
     * Relates to the Product Name.Name attribute in the LDM.
     */
    private String productName;
    /**
     * Relates to the Service Type.Name attribute in the LDM.
     */
    private String serviceType;
    /**
     * Relates to the Upper Range Period attribute in the LDM.
     */
    private Integer upperRangePeriod;
    /**
     * Relates to the Lower Range Period attribute in the LDM.
     */
    private Integer lowerRangePeriod;
    /**
     * Relates to the Set Service Due Date reference data.
     */
    private String schedulingTypeDue;
    /**
     * Relates to the Harmonisation Type attribute in the LDM.
     */
    private String harmonisationType;
    /**
     * Relates to the Continuous Indicator attribute in the LDM.
     */
    private Boolean continuousIndicator;
    /**
     * Relates to the Set Service Assign Date reference data.
     */
    private String schedulingTypeAssign;

    private Integer cyclePeriodicity;

    private String schedulingRegime;
    /**
     * Relates to the Product Family.Name attribute in the LDM.
     */
    private String productFamilyName;

    /**
     * Relates to the Scheduling Type Associated reference data.
     */
    private String schedulingTypeAssociated;
    /**
     * Relates to the Service Group.Name attribute in the LDM.
     */
    private String serviceGroup;

    private String serviceRuleset;

    /**
     * Relates to the Service Code attribute in the LDM.
     */
    public String getServiceCode()
    {
        return serviceCode;
    }

    /**
     * Relates to the Service Code attribute in the LDM.
     */
    public void setServiceCode(String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    /**
     * Relates to the Product Name.Name attribute in the LDM.
     */
    public String getProductName()
    {
        return productName;
    }

    /**
     * Relates to the Product Name.Name attribute in the LDM.
     */
    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    /**
     * Relates to the Service Type.Name attribute in the LDM.
     */
    public String getServiceType()
    {
        return serviceType;
    }

    /**
     * Relates to the Service Type.Name attribute in the LDM.
     */
    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    /**
     * Relates to the Upper Range Period attribute in the LDM.
     */
    public Integer getUpperRangePeriod()
    {
        return upperRangePeriod;
    }

    /**
     * Relates to the Upper Range Period attribute in the LDM.
     */
    public void setUpperRangePeriod(int upperRangePeriod)
    {
        this.upperRangePeriod = upperRangePeriod;
    }

    /**
     * Relates to the Lower Range Period attribute in the LDM.
     */
    public Integer getLowerRangePeriod()
    {
        return lowerRangePeriod;
    }

    /**
     * Relates to the Lower Range Period attribute in the LDM.
     */
    public void setLowerRangePeriod(int lowerRanagePeriod)
    {
        this.lowerRangePeriod = lowerRanagePeriod;
    }

    /**
     * Relates to the Set Service Due Date reference data.
     */
    public String getSchedulingTypeDue()
    {
        return schedulingTypeDue;
    }

    /**
     * Relates to the Set Service Due Date reference data.
     */
    public void setSchedulingTypeDue(String schedulingTypeDue)
    {
        this.schedulingTypeDue = schedulingTypeDue;
    }

    /**
     * Relates to the Harmonisation Type attribute in the LDM.
     */
    public String getHarmonisationType()
    {
        return harmonisationType;
    }

    /**
     * Relates to the Harmonisation Type attribute in the LDM.
     */
    public void setHarmonisationType(String harmonisationType)
    {
        this.harmonisationType = harmonisationType;
    }

    /**
     * Relates to the Continuous Indicator attribute in the LDM.
     */
    public Boolean getContinuousIndicator()
    {
        return continuousIndicator;
    }

    /**
     * Relates to the Continuous Indicator attribute in the LDM.
     */
    public void setContinuousIndicator(Boolean continuousIndicator)
    {
        this.continuousIndicator = continuousIndicator;
    }

    /**
     * Relates to the Set Service Assign Date reference data.
     */
    public String getSchedulingTypeAssign()
    {
        return schedulingTypeAssign;
    }

    /**
     * Relates to the Set Service Assign Date reference data.
     */
    public void setSchedulingTypeAssign(String schedulingTypeAssign)
    {
        this.schedulingTypeAssign = schedulingTypeAssign;
    }

    /**
     * Relates to the Scheduling Type Associated reference data.
     */
    public String getSchedulingTypeAssociated()
    {
        return schedulingTypeAssociated;
    }

    /**
     * Relates to the Scheduling Type Associated reference data.
     */
    public void setSchedulingTypeAssociated(String schedulingTypeAssociated)
    {
        this.schedulingTypeAssociated = schedulingTypeAssociated;
    }

    /**
     * Relates to the Service Group.Name attribute in the LDM.
     */
    public String getServiceGroup()
    {
        return serviceGroup;
    }

    /**
     * Relates to the Service Group.Name attribute in the LDM.
     */
    public void setServiceGroup(String serviceGroup)
    {
        this.serviceGroup = serviceGroup;
    }

    public Integer getCyclePeriodicity()
    {
        return cyclePeriodicity;
    }

    public void setCyclePeriodicity(Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public String getSchedulingRegime()
    {
        return schedulingRegime;
    }

    public void setSchedulingRegime(String schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    public String getServiceRuleset()
    {
        return serviceRuleset;
    }

    public void setServiceRuleset(String serviceRuleset)
    {
        this.serviceRuleset = serviceRuleset;
    }

    /**
     * Relates to the Product Family.Name attribute in the LDM.
     */
    public String getProductFamilyName()
    {
        return productFamilyName;
    }

    /**
     * Relates to the Product Family.Name attribute in the LDM.
     */
    public void setProductFamilyName(String productFamilyName)
    {
        this.productFamilyName = productFamilyName;
    }

    public Integer getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(Integer identifier)
    {
        this.identifier = identifier;
    }
}
