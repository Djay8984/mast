package com.detica.ukdc.drools.service.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Relates to the Asset Item concept in the Logical Data Model. This class is not thread-safe.
 */
public class AssetItem
{
    /**
     * Unique identifier for the AssetItem that this object represents.
     */
    private Integer identifier;

    /**
     * Refers to the parent Asset of this Asset Item.
     */
    private Asset parentAsset;

    /**
     * Relates to the Asset Item Type.Name attribute in the Logical Data Model.
     */
    private String assetItemType;

    /**
     * Stores attributes associated with this asset item. Maps Asset Attribute Type.Name -> Asset Item Attribute.Value
     * String.
     */
    private Map<String, String> attributes;

    /**
     * Unique identifier for the AssetItem that this object represents.
     */
    public Integer getIdentifier()
    {
        return this.identifier;
    }

    /**
     * Unique identifier for the AssetItem that this object represents.
     */
    public void setIdentifier(Integer identifier)
    {
        this.identifier = identifier;
    }

    /**
     * Refers to the parent Asset of this Asset Item.
     */
    public Asset getParentAsset()
    {
        return parentAsset;
    }

    /**
     * Refers to the parent Asset of this Asset Item.
     */
    public void setParentAsset(Asset parentAsset)
    {
        this.parentAsset = parentAsset;
    }

    /**
     * Relates to the Asset Item Type.Name attribute in the Logical Data Model.
     */
    public String getAssetItemType()
    {
        return assetItemType;
    }

    /**
     * Relates to the Asset Item Type.Name attribute in the Logical Data Model.
     */
    public void setAssetItemType(String assetItemType)
    {
        this.assetItemType = assetItemType;
    }

    /**
     * Returns a map containing the attributes associated with this Asset Item. If there are no attributes an empty map
     * is returned. Changes to the returned Map will be reflected in this object.
     */
    public Map<String, String> getAttributes()
    {
        if (attributes == null)
        {
            attributes = new HashMap<>();
        }
        return attributes;
    }
}
