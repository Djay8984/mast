package com.detica.ukdc.drools.service.model;

public class HullSurvey
{
    private Service hullSurvey;
    private Service associatedService;

    public Service getHullSurvey()
    {
        return this.hullSurvey;
    }

    public void setHullSurvey(Service hullSurvey)
    {
        this.hullSurvey = hullSurvey;
    }

    public Service getAssociatedService()
    {
        return this.associatedService;
    }

    public void setAssociatedService(Service associatedService)
    {
        this.associatedService = associatedService;
    }

}
