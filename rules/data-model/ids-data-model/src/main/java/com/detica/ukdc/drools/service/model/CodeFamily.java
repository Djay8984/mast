package com.detica.ukdc.drools.service.model;

import java.io.Serializable;

/**
 * This class holds a collection of Service Codes for different Service Types for the same Service.
 */
public class CodeFamily implements Serializable
{
    /**
     * The Service Code for the Initial type of the Service.
     */
    private String initial;
    /**
     * The Service Code for the Renewal type of the Service.
     */
    private String renewal;
    /**
     * The Service Code for the Annual type of the Service.
     */
    private String annual;
    /**
     * The Service Code for the Intermediate type of the Service.
     */
    private String intermediate;
    /**
     * The Service code for the Periodic type of the Service.
     */
    private String periodic;

    /**
     * The Service Code for the Initial type of the Service.
     */
    public String getInitial()
    {
        return initial;
    }

    /**
     * The Service Code for the Initial type of the Service.
     */
    public void setInitial(String initial)
    {
        this.initial = initial;
    }

    /**
     * The Service Code for the Renewal type of the Service.
     */
    public String getRenewal()
    {
        return renewal;
    }

    /**
     * The Service Code for the Renewal type of the Service.
     */
    public void setRenewal(String renewal)
    {
        this.renewal = renewal;
    }

    /**
     * The Service Code for the Annual type of the Service.
     */
    public String getAnnual()
    {
        return annual;
    }

    /**
     * The Service Code for the Annual type of the Service.
     */
    public void setAnnual(String annual)
    {
        this.annual = annual;
    }

    /**
     * The Service Code for the Intermediate type of the Service.
     */
    public String getIntermediate()
    {
        return intermediate;
    }

    /**
     * The Service Code for the Intermediate type of the Service.
     */
    public void setIntermediate(String intermediate)
    {
        this.intermediate = intermediate;
    }

    /**
     * The Service code for the Periodic type of the Service.
     */
    public String getPeriodic()
    {
        return periodic;
    }

    /**
     * The Service code for the Periodic type of the Service.
     */
    public void setPeriodic(String periodic)
    {
        this.periodic = periodic;
    }
}
