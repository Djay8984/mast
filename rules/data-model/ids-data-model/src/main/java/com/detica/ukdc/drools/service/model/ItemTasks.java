package com.detica.ukdc.drools.service.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Instances of this class store a mapping from Asset Items to their associated Tasks (under some service).
 *
 * This class is not thread-safe.
 */
public class ItemTasks
{
    /**
     * This class represents a pairing of an Asset Item and a Service that is carried out on that Asset Item.
     *
     * Neither the Asset Item or the Service should ever be null.
     */
    public static class ServiceAssetItemPair
    {
        private final Service service;
        private final AssetItem assetItem;

        public ServiceAssetItemPair(final Service service, final AssetItem assetItem)
        {
            if (service == null)
            {
                throw new IllegalArgumentException("Null service given to ServiceAssetItemPair.");
            }
            if (assetItem == null)
            {
                throw new IllegalArgumentException("null asset item given to ServiceAssetItemPair.");
            }
            this.service = service;
            this.assetItem = assetItem;
        }

        public Service getService()
        {
            return this.service;
        }

        public AssetItem getAssetItem()
        {
            return this.assetItem;
        }

        @Override
        public boolean equals(final Object other)
        {
            final boolean isEqual;

            if (other == null || !(other instanceof ServiceAssetItemPair))
            {
                isEqual = false;
            }
            else
            {
                final ServiceAssetItemPair otherPair = (ServiceAssetItemPair) other;
                isEqual = this.service.equals(otherPair.service) &&
                        this.assetItem.equals(otherPair.assetItem);
            }

            return isEqual;
        }

        @Override
        public int hashCode()
        {
            return (37 * this.service.hashCode()) * (41 * this.assetItem.hashCode());
        }
    }

    /**
     * A map of an Asset Item to its Tasks.
     */
    private final Map<ServiceAssetItemPair, List<String>> itemTasks = new HashMap<>();

    /**
     * Associate the given task with the given AssetItem.
     */
    public void add(Service service, AssetItem item, String task)
    {
        final ServiceAssetItemPair key = new ServiceAssetItemPair(service, item);
        if (!itemTasks.containsKey(key))
        {
            itemTasks.put(key, new ArrayList<String>());
        }
        itemTasks.get(key).add(task);
    }

    /**
     * Get the asset item and task associations.
     */
    public Map<ServiceAssetItemPair, List<String>> getItemTasks()
    {
        return itemTasks;
    }
}
