package com.detica.ukdc.drools.service.model;

public class CertifiedTaskRelationship
{
    private Task certifiedTask;
    private Task associatedTask;

    public Task getCertifiedTask()
    {
        return this.certifiedTask;
    }

    public Task getAssociatedTask()
    {
        return this.associatedTask;
    }

    public void setCertifiedTask(final Task certifiedTask)
    {
        this.certifiedTask = certifiedTask;
    }

    public void setAssociatedTask(final Task associatedTask)
    {
        this.associatedTask = associatedTask;
    }

}
