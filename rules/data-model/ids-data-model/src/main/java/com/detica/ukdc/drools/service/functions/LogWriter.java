package com.detica.ukdc.drools.service.functions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogWriter
{
    private static final Logger LOG = LoggerFactory.getLogger(LogWriter.class);

    public static void log(String message)
    {
        LOG.info("The rule " + message + " has been fired");
    }

}
