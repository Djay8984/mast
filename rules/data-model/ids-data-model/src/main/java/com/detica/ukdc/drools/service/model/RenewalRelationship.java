package com.detica.ukdc.drools.service.model;

public class RenewalRelationship
{
    private Service renewalService;
    private Service associatedService;

    public Service getRenewalService()
    {
        return this.renewalService;
    }

    public void setRenewalService(Service renewalService)
    {
        this.renewalService = renewalService;
    }

    public Service getAssociatedService()
    {
        return this.associatedService;
    }

    public void setAssociatedService(Service associatedService)
    {
        this.associatedService = associatedService;
    }

}
