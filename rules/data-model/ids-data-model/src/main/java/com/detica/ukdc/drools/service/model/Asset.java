package com.detica.ukdc.drools.service.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Relates to Asset defined in the Logical Data Model.
 */
public class Asset
{
    /**
     * Relates to the Date Of Build attribute in the Logical Data Model.
     */
    private Calendar buildDate;

    /**
     * Relates to the Date Of Commission attribute in the Logical Data Model.
     */
    private Calendar commissionDate;

    /**
     * Relates to the Harmonisation Date attribute in the Logical Data Model.
     */
    private Calendar harmonisationDate;

    /**
     * Relates to the Hull Indicator attribute in the Logical Data Model.
     */
    private Byte hullIndicator;

    /**
     * Relates to the Asset Type.Name attribute in the Logical Data Model.
     */
    private String assetType;

    /**
     * Stores references to the Asset Items associated with this Asset.
     */
    private final Set<AssetItem> items = new HashSet<>();

    /**
     * Relates to the Class Notations that this Asset has.
     */
    private final List<String> classNotations = new ArrayList<>();

    /**
     * Relates to the Society Regulations attribute in the LDM.
     */
    private String regulationCode;

    /**
     * Stores the class status of the asset
     */
    private String classStatus;

    /**
     * Relates to the gross Tonnage supported by the Asset.
     */
    private Double grossTonnage;
    
    /**
     * Indicates whether the Asset can be classed as SSC Private.
     */
    private Boolean sscPrivateIndicator;
    
    /**
     * Indicates whether the Asset can be classed as IWW Coastal.
     */
    private Boolean iwwCoastalIndicator;

    /**
     * Relates to the Date Of Build attribute in the Logical Data Model.
     */
    public Calendar getBuildDate()
    {
        final Calendar returnedDate;
        if (this.buildDate == null)
        {
            returnedDate = null;
        }
        else
        {
            returnedDate = (Calendar) this.buildDate.clone();
        }
        return returnedDate;
    }

    /**
     * Relates to the Date Of Build attribute in the Logical Data Model.
     */
    public void setBuildDate(final Calendar buildDate)
    {
        this.buildDate = (Calendar) buildDate.clone();
    }

    /**
     * Relates to the Date Of Commission attribute in the Logical Data Model.
     */
    public Calendar getCommissionDate()
    {
        final Calendar returnedDate;
        if (this.commissionDate == null)
        {
            returnedDate = null;
        }
        else
        {
            returnedDate = (Calendar) this.commissionDate.clone();
        }
        return returnedDate;
    }

    /**
     * Relates to the Date Of Commission attribute in the Logical Data Model.
     */
    public void setCommissionDate(final Calendar commissionDate)
    {
        this.commissionDate = (Calendar) commissionDate.clone();
    }

    /**
     * Relates to the Hull Indicator attribute in the Logical Data Model.
     */
    public Byte getHullIndicator()
    {
        return this.hullIndicator;
    }

    /**
     * Relates to the Hull Indicator attribute in the Logical Data Model.
     */
    public void setHullIndicator(final Byte hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    /**
     * Relates to the Harmonisation Date attribute in the Logical Data Model.
     */
    public Calendar getHarmonisationDate()
    {
        return harmonisationDate;
    }

    /**
     * Relates to the Harmonisation Date attribute in the Logical Data Model.
     */
    public void setHarmonisationDate(Calendar harmonisationDate)
    {
        this.harmonisationDate = harmonisationDate;
    }

    /**
     * Relates to the Asset Type.Name attribute in the Logical Data Model.
     */
    public String getAssetType()
    {
        return assetType;
    }

    /**
     * Relates to the Asset Type.Name attribute in the Logical Data Model.
     */
    public void setAssetType(String assetType)
    {
        this.assetType = assetType;
    }

    /**
     * 
     * The Asset Items associated with this Asset.
     */
    public Set<AssetItem> getItems()
    {
        return items;
    }

    public void setClassStatus(String classStatus)
    {
        this.classStatus = classStatus;
    }

    public String getClassStatus()
    {
        return this.classStatus;
    }

    /**
     * The Class Notations associated with the Asset.
     */
    public List<String> getClassNotations()
    {
        return this.classNotations;
    }

    /**
     * Relates to the Society Regulations attribute in the LDM.
     */
    public String getRegulationCode()
    {
        return this.regulationCode;
    }

    /**
     * Relates to the Society Regulations attribute in the LDM.
     */
    public void setRegulationCode(final String regulationCode)
    {
        this.regulationCode = regulationCode;
    }

    /**
     * Relates to the gross Tonnage supported by the Asset.
     */
    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    /**
     * Relates to the gross Tonnage supported by the Asset.
     */
    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }
    
    /**
     * Indicates whether the Asset can be classed as SSC Private.
     */
    public Boolean getSscPrivateIndicator()
    {
      return this.sscPrivateIndicator;
    }
    
    /**
     * Indicates whether the Asset can be classed as SSC Private.
     */
    public void setSscPrivateIndicator(final Boolean sscPrivateIndicator)
    {
      this.sscPrivateIndicator = sscPrivateIndicator;
    }
     
    /**
     * Indicates whether the Asset can be classed as IWW Coastal.
     */
    public Boolean getIwwCoastalIndicator()
    {
      return this.iwwCoastalIndicator;
    }
    
    /**
     * Indicates whether the Asset can be classed as IWW Coastal.
     */
    public void setIwwCoastalIndicator(final Boolean iwwCoastalIndicator)
    {
      this.iwwCoastalIndicator = iwwCoastalIndicator;
    }
}
