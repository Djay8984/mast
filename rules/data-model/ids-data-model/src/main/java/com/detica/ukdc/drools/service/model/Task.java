package com.detica.ukdc.drools.service.model;

import java.util.Calendar;

public class Task
{
    private int id;
    private String taskStatus;
    private Calendar completionDate;
    private Calendar assignedDate;
    private Calendar dueDate;
    private Service service;

    public String getTaskStatus()
    {
        return this.taskStatus;
    }

    public Calendar getCompletionDate()
    {
        return this.completionDate;
    }

    public Calendar getAssignedDate()
    {
        return this.assignedDate;
    }

    public Calendar getDueDate()
    {
        return this.dueDate;
    }

    public Service getService()
    {
        return this.service;
    }

    public int getId()
    {
        return this.id;
    }

    public void setTaskStatus(final String taskStatus)
    {
        this.taskStatus = taskStatus;
    }

    public void setCompletionDate(final Calendar completionDate)
    {
        this.completionDate = completionDate;
    }

    public void setAssignedDate(final Calendar assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public void setDueDate(final Calendar dueDate)
    {
        this.dueDate = dueDate;
    }

    public void setService(final Service service)
    {
        this.service = service;
    }

    public void setId(int id)
    {
        this.id = id;
    }

}
