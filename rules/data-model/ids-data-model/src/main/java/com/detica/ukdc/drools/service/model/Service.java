package com.detica.ukdc.drools.service.model;

import java.util.Calendar;

/**
 * Relates to the Scheduled Service entity in the LDM.
 */
public class Service
{

    private Integer scenarioNumber;
    /**
     * Relates to the Lower Range Date attribute in the LDM.
     */
    private Calendar lowerRangeDate;
    /**
     * Relates to the Upper Range Date attribute in the LDM.
     */
    private Calendar upperRangeDate;
    /**
     * Relates to the Due Date attribute in the LDM.
     */
    private Calendar serviceDueDate;
    /**
     * Relates to the Assigned Date attribute in the LDM.
     */
    private Calendar assignedDate;
    /**
     * Relates to the Completion Date attribute in the LDM.
     */
    private Calendar completionDate;

    /**
     * Relates to the Recommended Date attribute in the LDM.
     */
    private Calendar dateOnCompletion;
    /**
     * Reference to the Service Catalogue entity related to this Service.
     */
    private ServiceCatalogue serviceCatalogue;
    /**
     * Relates to the Service Catalogue.Lower Range Period attribute in the LDM.
     */
    private Integer lowerRangePeriod;
    /**
     * Relates to the Service Catalogue.Upper Range Period attribute in the LDM.
     */
    private Integer upperRangePeriod;
    private Service activeRenewalService;
    /**
     * Relates to the Service Type.Name attribute related to this Service.
     */
    private String serviceType;
    /**
     * Reference to the Asset associated with this Service.
     */
    private Asset asset;
    /**
     * Reference to the Asset Item associated with this Service.
     */
    private AssetItem assetItem;
    /**
     * Relates to the Cycle Periodicity attribute in the LDM.
     */
    private Integer cyclePeriodicity;
    /**
     * Relates to the Scheduling Regime.Name attribute in the LDM.
     */
    private String schedulingRegime;
    /**
     * Relates to the Scheduling Sub Regime.Name attribute in the LDM.
     */
    private String schedulingSubRegime;

    /**
     * Relates to the Service Status attribute in the LDM.
     */
    private String serviceStatus;
    /**
     * Relates to the Service Occurence Number attribute in the LDM.
     */
    private Integer serviceOccurenceNumber;
    /**
     * Relates to the internal attribute driving control flow Worklow Instruction.
     */
    private String workflowInstruction;
    /**
     * Relates to the ID of this Service.
     */
    private Integer serviceIdentifier;
    /**
     * Relates to the scheduling type assign of this service's credited service.
     */
    private String schedulingTypeAssign;

    public Integer getScenarioNumber()
    {
        return scenarioNumber;
    }

    public void setScenarioNumber(Integer scenarioNumber)
    {
        this.scenarioNumber = scenarioNumber;
    }

    /**
     * Relates to the Lower Range Date attribute in the LDM.
     */
    public Calendar getLowerRangeDate()
    {
        return lowerRangeDate;
    }

    /**
     * Relates to the Lower Range Date attribute in the LDM.
     */
    public void setLowerRangeDate(Calendar lowerRangeDate)
    {
        this.lowerRangeDate = lowerRangeDate;
    }

    /**
     * Relates to the Upper Range Date attribute in the LDM.
     */
    public Calendar getUpperRangeDate()
    {
        return upperRangeDate;
    }

    /**
     * Relates to the Upper Range Date attribute in the LDM.
     */
    public void setUpperRangeDate(Calendar upperDateDate)
    {
        this.upperRangeDate = upperDateDate;
    }

    /**
     * Relates to the Due Date attribute in the LDM.
     */
    public Calendar getServiceDueDate()
    {
        return serviceDueDate;
    }

    /**
     * Relates to the Due Date attribute in the LDM.
     */
    public void setServiceDueDate(Calendar serviceDueDate)
    {
        this.serviceDueDate = serviceDueDate;
    }

    /**
     * Reference to the Service Catalogue entity related to this Service.
     */
    public ServiceCatalogue getServiceCatalogue()
    {
        return this.serviceCatalogue;
    }

    /**
     * Reference to the Service Catalogue entity related to this Service.
     */
    public void setServiceCatalogue(ServiceCatalogue serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    /**
     * Relates to the Service Catalogue.Lower Range Period attribute in the LDM.
     */
    public Integer getLowerRangePeriod()
    {
        this.lowerRangePeriod = serviceCatalogue.getLowerRangePeriod();
        return this.lowerRangePeriod;
    }

    /**
     * Relates to the Service Catalogue.Lower Range Period attribute in the LDM.
     */
    public Integer getUpperRangePeriod()
    {
        this.upperRangePeriod = serviceCatalogue.getUpperRangePeriod();
        return this.upperRangePeriod;
    }

    public Service getActiveRenewalService()
    {
        return activeRenewalService;
    }

    public void setActiveRenewalService(Service activeRenewalService)
    {
        this.activeRenewalService = activeRenewalService;
    }

    /**
     * Relates to the Service Type.Name attribute related to this Service.
     */
    public String getServiceType()
    {
        this.serviceType = this.serviceCatalogue.getServiceType();
        return this.serviceType;
    }

    /**
     * Reference to the Asset associated with this Service.
     */
    public void setAsset(Asset asset)
    {
        this.asset = asset;
    }

    /**
     * Reference to the Asset associated with this Service.
     */
    public Asset getAsset()
    {
        return this.asset;
    }

    /**
     * Relates to the Cycle Periodicity attribute in the LDM.
     */
    public void setCyclePeriodicity(Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    /**
     * Relates to the Cycle Periodicity attribute in the LDM.
     */
    public Integer getCyclePeriodicity()
    {
        if (this.cyclePeriodicity == null)
        {
            this.cyclePeriodicity = this.serviceCatalogue.getCyclePeriodicity();
        }
        return this.cyclePeriodicity;
    }

    /**
     * Relates to the Assigned Date attribute in the LDM.
     */
    public Calendar getAssignedDate()
    {
        return assignedDate;
    }

    /**
     * Relates to the Assigned Date attribute in the LDM.
     */
    public void setAssignedDate(Calendar assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    /**
     * Relates to the Completion Date attribute in the LDM.
     */
    public Calendar getCompletionDate()
    {
        return completionDate;
    }

    /**
     * Relates to the Completion Date attribute in the LDM.
     */
    public void setCompletionDate(Calendar completionDate)
    {
        this.completionDate = completionDate;
    }

    /**
     * Relates to the Scheduling Regime.Name attribute in the LDM.
     */
    public String getSchedulingRegime()
    {
        this.schedulingRegime = serviceCatalogue.getSchedulingRegime();
        return this.schedulingRegime;
    }

    public void setSchedulingRegime(String schedulingRegime)
    {
        serviceCatalogue.setSchedulingRegime(schedulingRegime);
    }

    /**
     * Relates to the Service Status attribute in the LDM.
     */
    public String getServiceStatus()
    {
        return serviceStatus;
    }

    /**
     * Relates to the Service Status attribute in the LDM.
     */
    public void setServiceStatus(String serviceStatus)
    {
        this.serviceStatus = serviceStatus;
    }

    /**
     * Relates to the Scheduling Sub Regime.Name attribute in the LDM.
     */
    public String getSchedulingSubRegime()
    {
        return schedulingSubRegime;
    }

    /**
     * Relates to the Scheduling Sub Regime.Name attribute in the LDM.
     */
    public void setSchedulingSubRegime(String schedulingSubRegime)
    {
        this.schedulingSubRegime = schedulingSubRegime;
    }

    /**
     * Relates to the Service Occurence Number attribute in the LDM.
     */
    public Integer getServiceOccurenceNumber()
    {
        return serviceOccurenceNumber;
    }

    /**
     * Relates to the Service Occurence Number attribute in the LDM.
     */
    public void setServiceOccurenceNumber(Integer serviceOccurenceNumber)
    {
        this.serviceOccurenceNumber = serviceOccurenceNumber;
    }

    /**
     * Relates to the internal attribute driving control flow Workflow Instruction.
     */
    public String getWorkflowInstruction()
    {
        return workflowInstruction;
    }

    /**
     * Relates to the internal attribute driving control flow Workflow Instruction.
     */
    public void setWorkflowInstruction(String workflowInstruction)
    {
        this.workflowInstruction = workflowInstruction;
    }

    public Calendar getDateOnCompletion()
    {
        return this.dateOnCompletion;
    }

    public void setDateOnCompletion(Calendar dateOnCompletion)
    {
        this.dateOnCompletion = dateOnCompletion;
    }

    /**
     * Reference to the Asset Item associated with this Service.
     */
    public AssetItem getAssetItem()
    {
        return assetItem;
    }

    /**
     * Reference to the Asset Item associated with this Service.
     */
    public void setAssetItem(AssetItem assetItem)
    {
        this.assetItem = assetItem;
    }

    /**
     * Relates to the ID of this Service.
     */
    public Integer getIdentifier()
    {
        return serviceIdentifier;
    }

    /**
     * Relates to the ID of this Service.
     */
    public void setIdentifier(Integer serviceIdentifier)
    {
        this.serviceIdentifier = serviceIdentifier;
    }

    /**
     * Relates to the shceduling type assign of the service.
     */
    public String getSchedulingTypeAssign()
    {
        return schedulingTypeAssign;
    }

    /**
     * Relates to the scheduling type assign of the service.
     */
    public void setSchedulingTypeAssign(final String schedulingTypeAssign)
    {
        this.schedulingTypeAssign = schedulingTypeAssign;
    }
}
