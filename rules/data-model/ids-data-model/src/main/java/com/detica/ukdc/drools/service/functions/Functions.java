package com.detica.ukdc.drools.service.functions;

import java.util.Calendar;

import com.detica.ukdc.drools.service.model.AssetItem;
import com.detica.ukdc.drools.service.model.Service;

/**
 * This class contains re-usable functions for use in Drools Rules.
 */
public class Functions
{
    /**
     * Determines whether two Calendar objects represent the same date (year, month, day) irrespective of hour, minute,
     * second or milli-second values.
     *
     * @param firstDate One of the dates to be compared.
     * @param secondDate The other date to be compared.
     *
     * @return true if both parameters are not null and represent the same date, false otherwise.
     */
    public static boolean DatesAreEqual(final Calendar firstDate, final Calendar secondDate)
    {
        return (firstDate != null) && (secondDate != null) &&
                (firstDate.get(Calendar.DAY_OF_MONTH) == secondDate.get(Calendar.DAY_OF_MONTH)) &&
                (firstDate.get(Calendar.MONTH) == secondDate.get(Calendar.MONTH)) &&
                (firstDate.get(Calendar.YEAR) == secondDate.get(Calendar.YEAR));
    }

    /**
     * Returns the ceiling value of the number of years between the first and second date.
     *
     * The ceiling is taken on an absolute value of the difference.
     *
     * If the first argument represents a date that is after the date represented by the second argument then a negative
     * value of the difference may be returned.
     *
     * This operation only takes into account the year month and day of the two dates.
     *
     * @param The source date to determine the difference from. Must not be null.
     * @param The target date used to determine the difference. Must not be null.
     *
     * @return The ceiling value of the difference in years between the two dates. This will be negative if the source
     *         date is more than a year later than the target date.
     *
     * @throws IllegalArgumentException Thrown if either argument is null.
     */
    public static int YearDifferenceCeiling(final Calendar firstDate, final Calendar secondDate)
    {
        if (firstDate == null)
        {
            throw new IllegalArgumentException("The first argument to YearDifferenceCeiling is null.");
        }
        if (secondDate == null)
        {
            throw new IllegalArgumentException("The second argument to YearDifferenceCeiling is null.");
        }

        final Calendar localFirstDate = (Calendar) firstDate.clone();
        final Calendar localSecondDate = (Calendar) secondDate.clone();

        final boolean secondDateIsLatest = localSecondDate.after(localFirstDate);

        int yearDifference = localSecondDate.get(Calendar.YEAR) - localFirstDate.get(Calendar.YEAR);

        if ((localFirstDate.get(Calendar.MONTH) < localSecondDate.get(Calendar.MONTH)))
        {
            yearDifference += 1;
        }
        else if ((localFirstDate.get(Calendar.MONTH) == localSecondDate.get(Calendar.MONTH)) &&
                (localFirstDate.get(Calendar.DAY_OF_MONTH) < localSecondDate.get(Calendar.DAY_OF_MONTH)))
        {
            yearDifference += 1;
        }

        return yearDifference;
    }

    public static int YearDifferenceCeiling(final double inputTimeInterval)
    {
        return (int) (Math.ceil(inputTimeInterval));
    }

    /**
     * A function to add the given values to the year of the given Calendar object. This function does not modify the
     * Calendar object passed in. Null year values will be ignored.
     *
     * @param baseDate The base date value to which to add the given year values. Must not be null.
     * @param yearsToAdd A series of numbers to increase the base date's year by. Must not be null.
     *
     * @return A representation of the base date incremented by the given numbers of years.
     *
     * @throws IllegalArgumentException Thrown if the baseDate or an array for yearsToAdd is null.
     */
    public static Calendar AddYears(final Calendar baseDate, final Integer... yearsToAdd)
    {
        if (baseDate == null)
        {
            throw new IllegalArgumentException("The base date given to AddYears is null.");
        }
        if (yearsToAdd == null)
        {
            throw new IllegalArgumentException("An array given to AddYears is null.");
        }

        final Calendar localBaseDate = (Calendar) baseDate.clone();
        for (final Integer numberOfYears : yearsToAdd)
        {
            if (numberOfYears != null)
            {
                localBaseDate.add(Calendar.YEAR, numberOfYears);
            }
        }

        return localBaseDate;
    }

    /**
     * A function to add the given values to the month of the given Calendar object. This function does not modify the
     * Calendar object passed in. Null month values will be ignored.
     *
     * Incrementing past the last month in the year will increment the returned year value.
     *
     * @param baseDate The base date value to which to add the given month values. Must not be null.
     * @param monthsToAdd A series of numbers to increase the base date's month by. Must not be null.
     *
     * @return A representation of the base date incremented by the given numbers of months.
     *
     * @throws IllegalArgumentException Thrown if the baseDate or an array for monthsToAdd is null.
     */
    public static Calendar AddMonths(final Calendar baseDate, final Integer... monthsToAdd)
    {
        if (baseDate == null)
        {
            throw new IllegalArgumentException("A null base date was given to AddMonths.");
        }
        if (monthsToAdd == null)
        {
            throw new IllegalArgumentException("A null array of months to add was given to AddMonths.");
        }

        final Calendar localBaseDate = (Calendar) baseDate.clone();
        for (final Integer numberOfMonths : monthsToAdd)
        {
            if (numberOfMonths != null)
            {
                localBaseDate.add(Calendar.MONTH, numberOfMonths);
            }
        }

        return localBaseDate;
    }

    /**
     * A function to add the given values to the day of the given Calendar object. This function does not modify the
     * Calendar object passed in. Null day values will be ignored.
     *
     * Incrementing past the last day in a month will increment the month value and incrementing past the last month in
     * the year will increment the returned year value.
     *
     * @param baseDate The base date value to which to add the given numebr of days. Must not be null.
     * @param monthsToAdd A series of numbers to increase the base date's day value by. Must not be null.
     *
     * @return A representation of the base date incremented by the given numbers of days.
     *
     * @throws IllegalArgumentException Thrown if the baseDate or an array for daysToAdd is null.
     */
    public static Calendar AddDays(final Calendar baseDate, final Integer... daysToAdd)
    {
        if (baseDate == null)
        {
            throw new IllegalArgumentException("The base date given to AddDays is null.");
        }
        if (daysToAdd == null)
        {
            throw new IllegalArgumentException("The daysToAdd array given to AddDays is null.");
        }

        final Calendar localBaseDate = (Calendar) baseDate.clone();
        for (final Integer numberOfDays : daysToAdd)
        {
            if (numberOfDays != null)
            {
                localBaseDate.add(Calendar.DAY_OF_MONTH, numberOfDays);
            }
        }

        return localBaseDate;
    }

    public static Calendar calculateLowerRangeDate(Service service, int incrementDays)
    {
        Calendar lowerRangeDate = (Calendar) service.getLowerRangeDate();
        int lowerRangePeriod = service.getLowerRangePeriod();

        // Record the date of the service's due date
        int serviceDate = service.getServiceDueDate().get(Calendar.DATE);

        // Subtract the lower range period from the lower range date - month
        lowerRangeDate.add(Calendar.MONTH, lowerRangePeriod * -1);

        // Get the number of days in the lower range date
        int numberOfDays = lowerRangeDate.getActualMaximum(Calendar.DAY_OF_MONTH);

        // Warp around check
        if (serviceDate > numberOfDays)
        {
            // Set the lower range date to the first of next month
            lowerRangeDate.set(Calendar.DAY_OF_MONTH, 1);
            lowerRangeDate.add(Calendar.MONTH, 1);
        }
        else
        {
            // Add the increment days to the lower range date
            lowerRangeDate.add(Calendar.DAY_OF_MONTH, incrementDays);
        }
        return lowerRangeDate;
    }

    public static Calendar calculateUpperRangeDate(Service service, int decrementDays)
    {
        Calendar upperRangeDate = (Calendar) service.getUpperRangeDate().clone();
        int upperRangePeriod = service.getUpperRangePeriod();
        // Record the date of the service due date
        int serviceDate = service.getServiceDueDate().get(Calendar.DAY_OF_MONTH);

        // Add the upper range period to the upper range date - month
        upperRangeDate.add(Calendar.MONTH, upperRangePeriod);

        // Get the number of days in the upper range date
        int numberOfDays = upperRangeDate.getActualMaximum(Calendar.DAY_OF_MONTH);

        // Warp around check
        if (serviceDate > numberOfDays)
        {
            // Set the upper range date to the last day of the month
            upperRangeDate.set(Calendar.DAY_OF_MONTH, numberOfDays);
        }
        else
        {
            // Subtract the decrement days from the month
            upperRangeDate.add(Calendar.DAY_OF_MONTH, decrementDays * -1);
        }

        return upperRangeDate;
    }

    public static boolean harmonisationDateFunction(Calendar harmonisationDate, Calendar completionDate, int cyclePeriodicity, int lowerRangePeriod)
    {
        long harmonisationDateLong = harmonisationDate.getTimeInMillis();

        Calendar modifiedDate = (Calendar) completionDate.clone();
        modifiedDate.add(Calendar.MONTH, cyclePeriodicity * -1);
        modifiedDate.add(Calendar.MONTH, lowerRangePeriod);

        long modifiedDateLong = modifiedDate.getTimeInMillis();

        return (harmonisationDateLong <= modifiedDateLong);
    }

    public static boolean completionDateFunction(Calendar completionDate, Calendar lowerRangeDate, Calendar serviceDueDate, int reschedulingThreshold)
    {
        long completionDateLong = completionDate.getTimeInMillis();
        long lowerRangeDateLong = lowerRangeDate.getTimeInMillis();

        Calendar modifiedDate = (Calendar) serviceDueDate.clone();
        modifiedDate.add(Calendar.MONTH, reschedulingThreshold);

        long modifiedDateLong = modifiedDate.getTimeInMillis();

        return (completionDateLong >= lowerRangeDateLong) && (completionDateLong < modifiedDateLong);
    }

    /**
     * This function checks that the given AssetItem has the attribute represented by the given String.
     *
     * The format of the attribute String is [attribute name][attribute value].
     *
     * @param item The Asset Item to check for the attribute. Must not be null.
     * @param attribute The attribute to check for. Must not be null and must conform to the format specified above.
     *
     * @return true if the AssetItem contains the attribute, false otherwise.
     *
     * @throws IllegalArgumentException Thrown if either argument is invalid in regards to the above conditions.
     */
    public static boolean CheckAttribute(final AssetItem item, final String attribute)
    {
        if (item == null)
        {
            throw new IllegalArgumentException("Null AssetItem given to CheckAttribute.");
        }
        if (attribute == null)
        {
            throw new IllegalArgumentException("Null attribute given to CheckAttribute.");
        }
        if (attribute.charAt(0) != '[')
        {
            throw new IllegalArgumentException("The attribute does not begin with [. " + attribute);
        }

        final int nameEndIndex = attribute.indexOf(']', 1);

        if (nameEndIndex == -1)
        {
            throw new IllegalArgumentException("The attribute has no closing ] on the attribute name. " + attribute);
        }

        final int valueStartIndex = attribute.indexOf('[', nameEndIndex + 1);

        if (valueStartIndex == -1)
        {
            throw new IllegalArgumentException("The attribute has no opening [ on the attribute value. " + attribute);
        }

        final int valueEndIndex = attribute.indexOf(']', valueStartIndex + 1);

        if (valueEndIndex == -1)
        {
            throw new IllegalArgumentException("The attribute has no closing ] on the attribute value. " + attribute);
        }

        final String name = attribute.substring(1, nameEndIndex);
        final String value = attribute.substring(valueStartIndex + 1, valueEndIndex);

        return item.getAttributes().containsKey(name) &&
                item.getAttributes().get(name).equalsIgnoreCase(value);
    }
}
