package com.detica.ukdc.drools.service.model;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ServiceTest
{
    /**
     * Test that we can set and get the lower range date.
     */
    @Test
    public void test_setLowerRangeDate_getLowerRangeDate()
    {
        final Calendar lowerRangeDate = new GregorianCalendar();

        final Service service = new Service();

        service.setLowerRangeDate(lowerRangeDate);
        final Calendar returnedLowerRangeDate = service.getLowerRangeDate();

        assertEquals("The returned lower range date should be the same as the given lower range date.",
                lowerRangeDate, returnedLowerRangeDate);
    }

    /**
     * Test that we can set and get the upper range date.
     */
    @Test
    public void test_setUpperRangeDate_getUpperRangeDate()
    {
        final Calendar upperRangeDate = new GregorianCalendar();

        final Service service = new Service();

        service.setUpperRangeDate(upperRangeDate);
        final Calendar returnedUpperRangeDate = service.getUpperRangeDate();

        assertEquals("The returned upper range date should be the same as the given upper range date.",
                upperRangeDate, returnedUpperRangeDate);
    }

    /**
     * Test that we can set and get the service due date.
     */
    @Test
    public void test_setServiceDueDate_getServiceDueDate()
    {
        final Calendar serviceDueDate = new GregorianCalendar();

        final Service service = new Service();

        service.setServiceDueDate(serviceDueDate);
        final Calendar returnedServiceDueDate = service.getServiceDueDate();

        assertEquals("The returned service due date should be the same as the given service due date.",
                serviceDueDate, returnedServiceDueDate);
    }

    /**
     * Test that we can set and get service catalogue.
     */
    @Test
    public void test_setServiceCatalogue_getServiceCatalogue()
    {
        final ServiceCatalogue serviceCatalogue = new ServiceCatalogue();

        final Service service = new Service();

        service.setServiceCatalogue(serviceCatalogue);
        final ServiceCatalogue returnedServiceCatalogue = service.getServiceCatalogue();

        assertEquals("The returned service catalogue should be the same as the given service catalogue.",
                serviceCatalogue, returnedServiceCatalogue);
    }

    /**
     * Test that we can get and set the asset.
     */
    @Test
    public void test_setAsset_getAsset()
    {
        final Asset asset = new Asset();

        final Service service = new Service();

        service.setAsset(asset);
        final Asset returnedAsset = service.getAsset();

        assertEquals("The returned asset should be the same as the given asset.",
                asset, returnedAsset);
    }

    /**
     * Test that we can get and set the cycle periodicity.
     */
    /*
     * @Test public void test_setCyclePeriodicity_getCyclePeriodicity() { final Integer cyclePeriodicity = 60;
     * 
     * final Service service = new Service();
     * 
     * service.setCyclePeriodicity(cyclePeriodicity); final Integer returnedCyclePeriodicity =
     * service.getCyclePeriodicity();
     * 
     * assertEquals("The returned cycle periodicity should be the same as the given cycle periodicity.",
     * cyclePeriodicity, returnedCyclePeriodicity); }
     */

    /**
     * Test that we can get and set the scheduling regime.
     */
    /*
     * @Test public void test_setSchedulingRegime_getSchedulingRegime() { final String schedulingRegime = "Standard";
     * 
     * final Service service = new Service();
     * 
     * service.setSchedulingRegime(schedulingRegime); final String returnedSchedulingRegime =
     * service.getSchedulingRegime();
     * 
     * assertEquals("The returned scheduling regime should be the same as the given scheduling regime.",
     * schedulingRegime, returnedSchedulingRegime); }
     */

    /**
     * Test that we can get and set the service status.
     */
    @Test
    public void test_setServiceStatus_getServiceStatus()
    {
        final String serviceStatus = "Not Started";

        final Service service = new Service();

        service.setServiceStatus(serviceStatus);
        final String returnedServiceStatus = service.getServiceStatus();

        assertEquals("The returned service status should be the same as the given service status.",
                serviceStatus, returnedServiceStatus);
    }

    /**
     * Test that we can set and get the scheduling sub-regime.
     */
    @Test
    public void test_setSchedulingSubRegime_getSchedulingSubRegime()
    {
        final String schedulingSubRegime = "Standard";

        final Service service = new Service();

        service.setSchedulingSubRegime(schedulingSubRegime);
        final String returnedSchedulingSubRegime = service.getSchedulingSubRegime();

        assertEquals("The returned scheduling sub-regime should be the same as the given scheduling sub-regime.",
                schedulingSubRegime, returnedSchedulingSubRegime);
    }

    /**
     * Test that we can set and get the service occurence number.
     */
    @Test
    public void test_setServiceOccurenceNumber_getServiceOccurenceNumber()
    {
        final Integer serviceOccurenceNumber = 1;

        final Service service = new Service();

        service.setServiceOccurenceNumber(serviceOccurenceNumber);
        final Integer returnedServiceOccurenceNumber = service.getServiceOccurenceNumber();

        assertEquals("The returned service occurence number should be the same as the given service occurence number.",
                serviceOccurenceNumber, returnedServiceOccurenceNumber);
    }

    /**
     * Test that we can set and get the workflow instruction.
     */
    @Test
    public void test_setWorkflowInstruction_getWorkflowInstruction()
    {
        final String workflowInstruction = "Required";

        final Service service = new Service();

        service.setWorkflowInstruction(workflowInstruction);
        final String returnedWorkflowInstruction = service.getWorkflowInstruction();

        assertEquals("The returned workflow instruction should be the same as the given workflow instruction.",
                workflowInstruction, returnedWorkflowInstruction);
    }

    /**
     * Test that we can set and get the asset item.
     */
    @Test
    public void test_setAssetItem_getAssetItem()
    {
        final AssetItem assetItem = new AssetItem();

        final Service service = new Service();

        service.setAssetItem(assetItem);
        final AssetItem returnedAssetItem = service.getAssetItem();

        assertEquals("The returned Asset Item should be the same as the given Asset Item.",
                assetItem, returnedAssetItem);
    }
}
