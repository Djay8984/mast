package com.detica.ukdc.drools.service.model;

import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AssetItemTest
{
    /**
     * Test that we can set and get the identifier.
     */
    @Test
    public void test_setIdentifier_getIdentifier()
    {
        final Integer identifier = 1;

        final AssetItem assetItem = new AssetItem();

        assetItem.setIdentifier(identifier);
        final Integer returnedIdentifier = assetItem.getIdentifier();

        assertEquals("The asset identifier that is returned should be 1.", new Integer(1), returnedIdentifier);
    }

    /**
     * Test that we can get and set the asset item type.
     */
    @Test
    public void test_setAssetItemType_getAssetItemType()
    {
        final String assetItemType = "Chain Cable";

        final AssetItem assetItem = new AssetItem();

        assetItem.setAssetItemType(assetItemType);
        final String returnedAssetItemType = assetItem.getAssetItemType();

        assertEquals("The asset item type that is returned should be the same as the one that we passed in.",
                assetItemType, returnedAssetItemType);
    }

    /**
     * Test that we can set and get the parent asset.
     */
    @Test
    public void test_setParentAsset_getParentAsset()
    {
        final Asset asset = new Asset();

        final AssetItem assetItem = new AssetItem();

        assetItem.setParentAsset(asset);
        final Asset returnedParentAsset = assetItem.getParentAsset();

        assertEquals("The parent asset that is returned should be the same as the one that we passed in.",
                asset, returnedParentAsset);
    }

    /**
     * Test that we can manipulate the attributes of the asset item.
     */
    @Test
    public void test_getAttributes()
    {
        final AssetItem assetItem = new AssetItem();

        final Map<String, String> attributes = assetItem.getAttributes();
        assertNotNull("The returned attributes map should not be null", attributes);
        assertEquals("The returned attributes map should have no entries.", 0, attributes.size());

        attributes.put("Primary Service", "Fresh Water");

        final Map<String, String> returnedAttributes = assetItem.getAttributes();
        assertEquals("The returned attributes map should have one entry.", 1, returnedAttributes.size());
        assertTrue("The returned attributes map should have an entry for Primary Service.", returnedAttributes.containsKey("Primary Service"));
        assertEquals("The value associated with Primary Service should be Fresh Water.", "Fresh Water", returnedAttributes.get("Primary Service"));
    }
}
