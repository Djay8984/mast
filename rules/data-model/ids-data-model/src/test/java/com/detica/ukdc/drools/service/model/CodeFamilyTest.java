package com.detica.ukdc.drools.service.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CodeFamilyTest
{
    /**
     * Test that we can set and get the initial code.
     */
    @Test
    public void test_setInitial_getInitial()
    {
        final String initial = "ECIL";

        final CodeFamily codeFamily = new CodeFamily();

        codeFamily.setInitial(initial);
        final String returnedInitial = codeFamily.getInitial();

        assertEquals("The returned initial code should be the same as the given initial code.",
                initial, returnedInitial);
    }

    /**
     * Test that we can set and get the renewal code.
     */
    @Test
    public void test_setRenewal_getRenewal()
    {
        final String renewal = "DIRP";

        final CodeFamily codeFamily = new CodeFamily();

        codeFamily.setRenewal(renewal);
        final String returnedRenewal = codeFamily.getRenewal();

        assertEquals("The returned renewal code should be the same as the given renewal code.",
                renewal, returnedRenewal);
    }

    /**
     * Test that we can set and get the annual code.
     */
    @Test
    public void test_setAnnual_getAnnual()
    {
        final String annual = "ECAS";

        final CodeFamily codeFamily = new CodeFamily();

        codeFamily.setAnnual(annual);
        final String returnedAnnual = codeFamily.getAnnual();

        assertEquals("The returned annual code should be the same as the given annual code.",
                annual, returnedAnnual);
    }

    /**
     * Test that we can set and get the intermediate code.
     */
    @Test
    public void test_setIntermediate_getIntermediate()
    {
        final String intermediate = "ITSS";

        final CodeFamily codeFamily = new CodeFamily();

        codeFamily.setIntermediate(intermediate);
        final String returnedIntermediate = codeFamily.getIntermediate();

        assertEquals("The returned intermediate code should be the same as the given intermediate code.",
                intermediate, returnedIntermediate);
    }

    /**
     * Test that we can set and get the periodic code.
     */
    @Test
    public void test_setPeriodic_getPeriodic()
    {
        final String periodic = "IEX";

        final CodeFamily codeFamily = new CodeFamily();

        codeFamily.setPeriodic(periodic);
        final String returnedPeriodic = codeFamily.getPeriodic();

        assertEquals("The returned periodic code should be the same as the given periodic code.",
                periodic, returnedPeriodic);
    }
}
