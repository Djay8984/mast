package com.detica.ukdc.drools.service.model;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AssetTest
{
    /**
     * Test that we can set and then get a build date.
     */
    @Test
    public void test_setBuildDate_getBuildDate()
    {
        final Calendar buildDate = new GregorianCalendar();

        final Asset asset = new Asset();

        asset.setBuildDate(buildDate);
        final Calendar returnedBuildDate = asset.getBuildDate();

        assertEquals("The build date that we pass in should be the same one that has been returned.",
                buildDate, returnedBuildDate);
    }

    /**
     * Test that we can set and then get a commission date.
     */
    @Test
    public void test_setCommissionDate_getBuildDate()
    {
        final Calendar commissionDate = new GregorianCalendar();

        final Asset asset = new Asset();

        asset.setCommissionDate(commissionDate);
        final Calendar returnedCommissionDate = asset.getCommissionDate();

        assertEquals("The commission date that we pass in should be the same one that has been returned.",
                commissionDate, returnedCommissionDate);
    }

    /**
     * Test that we can set and then get a harmonisation date.
     */
    @Test
    public void test_setHarmonisationDate_getHarmonisationDate()
    {
        final Calendar harmonisationDate = new GregorianCalendar();

        final Asset asset = new Asset();

        asset.setHarmonisationDate(harmonisationDate);
        final Calendar returnedHarmonisationDate = asset.getHarmonisationDate();

        assertEquals("The harmonisation date that we pass in should be the same one that has been returned.",
                harmonisationDate, returnedHarmonisationDate);
    }

    /**
     * Test that we can set and get the asset type.
     */
    @Test
    public void test_setAssetType_getAssetType()
    {
        final String assetType = "A311";

        final Asset asset = new Asset();

        asset.setAssetType(assetType);
        final String returnedAssetType = asset.getAssetType();

        assertEquals("The asset type that we pass in should be the same one that has been returned.",
                assetType, returnedAssetType);
    }

    /**
     * Test that we can manipulate the class notations of an Asset.
     */
    @Test
    public void test_getClassNotations()
    {
        final Asset asset = new Asset();

        final List<String> firstClassNotations = asset.getClassNotations();

        assertNotNull("The returned list of class notations should not be null.", firstClassNotations);
        assertTrue("The returned list of class notations should be empty.", firstClassNotations.isEmpty());

        firstClassNotations.add("OPS");

        final List<String> secondClassNotations = asset.getClassNotations();

        assertNotNull("The returned list of class notations should not be null.", secondClassNotations);
        assertEquals("There should be one listed class notation.", 1, secondClassNotations.size());
        assertEquals("The returned class notation should be OPS.", "OPS", secondClassNotations.get(0));
    }

    /**
     * Test that we can set and get the regulation code for an Asset.
     */
    @Test
    public void test_setRegulationCode_getRegulationCode()
    {
        final String regulationCode = "ANY";

        final Asset asset = new Asset();

        asset.setRegulationCode(regulationCode);
        final String returnedRegulationCode = asset.getRegulationCode();

        assertEquals("The returned regulation code should be ANY.", "ANY", returnedRegulationCode);
    }
}
