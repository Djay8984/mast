package com.detica.ukdc.drools.service.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ServiceCatalogueTest
{
    /**
     * Test that we can set and get the service code.
     */
    @Test
    public void test_setServiceCatalogue_getServiceCatalogue()
    {
        final String serviceCode = "CSH";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setServiceCode(serviceCode);
        final String returnedServiceCode = catalogue.getServiceCode();

        assertEquals("The returned service code should be the same as the service code given.",
                serviceCode, returnedServiceCode);
    }

    /**
     * Test that we can set and get the product name.
     */
    @Test
    public void test_setProductName_getProductName()
    {
        final String productName = "Name";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setProductName(productName);
        final String returnedProductName = catalogue.getProductName();

        assertEquals("The returned product name should be the same as the product name given.",
                productName, returnedProductName);
    }

    /**
     * Test that we can set and get service type.
     */
    @Test
    public void test_setServiceType_getServiceType()
    {
        final String serviceType = "Annual";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setServiceType(serviceType);
        final String returnedServiceType = catalogue.getServiceType();

        assertEquals("The returned service type should be the same as the service type given.",
                serviceType, returnedServiceType);
    }

    /**
     * Test that we can set and get upper range period.
     */
    @Test
    public void test_setUpperRangePeriod_getUpperRangePeriod()
    {
        final Integer upperRangePeriod = 5;

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setUpperRangePeriod(upperRangePeriod);
        final Integer returnedUpperRangePeriod = catalogue.getUpperRangePeriod();

        assertEquals("The returned upper range period should be the same as the upper range period given.",
                upperRangePeriod, returnedUpperRangePeriod);
    }

    /**
     * Test that we can set and get lower range period.
     */
    @Test
    public void test_setLowerRangePeriod_getLowerRangePeriod()
    {
        final Integer lowerRangePeriod = 5;

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setLowerRangePeriod(lowerRangePeriod);
        final Integer returnedLowerRangePeriod = catalogue.getLowerRangePeriod();

        assertEquals("The returned lower range period should be the same as the lower range period given.",
                lowerRangePeriod, returnedLowerRangePeriod);
    }

    /**
     * Test that we can set and get scheduling type due.
     */
    @Test
    public void test_setSchedulingTypeDue_getSchedulingTypeDue()
    {
        final String schedulingTypeDue = "Harmonised Renewal";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setSchedulingTypeDue(schedulingTypeDue);
        final String returnedSchedulingTypeDue = catalogue.getSchedulingTypeDue();

        assertEquals("The returned scheduling type due should be the same as the given scheduling type due.",
                schedulingTypeDue, returnedSchedulingTypeDue);
    }

    /**
     * Test that we can set and get harmonisation type.
     */
    @Test
    public void test_setHarmonisationType_getHarmonisationType()
    {
        final String harmonisationType = "Full";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setHarmonisationType(harmonisationType);
        final String returnedHarmonisationType = catalogue.getHarmonisationType();

        assertEquals("The returned harmonisation type should be the same as the given harmonisation type.",
                harmonisationType, returnedHarmonisationType);
    }

    /**
     * Test that we can set and get the continuous indicator.
     */
    @Test
    public void test_setContinuousIndicator_getContinuousIndicator()
    {
        final Boolean continuousIndicator = Boolean.TRUE;

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setContinuousIndicator(continuousIndicator);
        final Boolean returnedContinuousIndicator = catalogue.getContinuousIndicator();

        assertEquals("The returned continuous indicator should be the same as the given continuous indicator.",
                continuousIndicator, returnedContinuousIndicator);
    }

    /**
     * Test that we can set and get scheduling type assign.
     */
    @Test
    public void test_setSchedulingTypeAssign_getSchedulingTypeAssign()
    {
        final String schedulingTypeAssign = "Date On Completion";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setSchedulingTypeAssign(schedulingTypeAssign);
        final String returnedSchedulingTypeAssign = catalogue.getSchedulingTypeAssign();

        assertEquals("The returned scheduling type assign should be the same as the given scheduling type assign.",
                schedulingTypeAssign, returnedSchedulingTypeAssign);
    }

    /**
     * Test that we can set and get scheduling type associated.
     */
    @Test
    public void test_setSchedulingTypeAssociated_getSchedulingTypeAssociated()
    {
        final String schedulingTypeAssociated = "CL-B - Core Hull Cycle (General)";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setSchedulingTypeAssociated(schedulingTypeAssociated);
        final String returnedSchedulingTypeAssociated = catalogue.getSchedulingTypeAssociated();

        assertEquals("The returned scheduling type associated should be the same as the given scheduling type associated.",
                schedulingTypeAssociated, returnedSchedulingTypeAssociated);
    }

    /**
     * Test that we can set and get the service group.
     */
    @Test
    public void test_setServiceGroup_getServiceGroup()
    {
        final String serviceGroup = "Hull";

        final ServiceCatalogue catalogue = new ServiceCatalogue();

        catalogue.setServiceGroup(serviceGroup);
        final String returnedServiceGroup = catalogue.getServiceGroup();

        assertEquals("The returned service group should be the same as the given service group.",
                serviceGroup, returnedServiceGroup);
    }
}
