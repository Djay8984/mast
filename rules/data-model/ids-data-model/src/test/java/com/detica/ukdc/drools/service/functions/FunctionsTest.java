package com.detica.ukdc.drools.service.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.detica.ukdc.drools.service.model.AssetItem;

@RunWith(JUnit4.class)
public class FunctionsTest
{
    /**
     * Test that DatesAreEqual returns false if the first date argument is null.
     */
    @Test
    public void test_DatesAreEqual_nullFirstDate()
    {
        final Calendar firstDate = null;
        final Calendar secondDate = new GregorianCalendar();

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns false if the second date argument is null.
     */
    @Test
    public void test_DatesAreEqual_nullSecondDate()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = null;

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns false if both date arguments are null.
     */
    @Test
    public void test_DatesAreEqual_bothDatesNull()
    {
        final Calendar firstDate = null;
        final Calendar secondDate = null;

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns false if the dates differ by a year.
     */
    @Test
    public void test_DatesAreEqual_yearDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.YEAR, 2010);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.YEAR, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns false if the dates differ by a month.
     */
    @Test
    public void test_DatesAreEqual_monthDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.MONTH, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.MONTH, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns false if the dates differ by a day.
     */
    @Test
    public void test_DatesAreEqual_dayDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.DAY_OF_MONTH, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.DAY_OF_MONTH, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertFalse("The returned value should indicate that the dates are not equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns true if the dates differ by an hour.
     */
    @Test
    public void test_DatesAreEqual_hourDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.HOUR, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.HOUR, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertTrue("The returned value should indicate that the dates are equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns true if the dates differ by a minute.
     */
    @Test
    public void test_DatesAreEqual_minuteDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.MINUTE, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.MINUTE, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertTrue("The returned value should indicate that the dates are equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns true if the dates differ by a second.
     */
    @Test
    public void test_DatesAreEqual_secondDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.SECOND, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.SECOND, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertTrue("The returned value should indicate that the dates are equal.", datesEqual);
    }

    /**
     * Test that DatesAreEqual returns true if the dates differ by a millisecond.
     */
    @Test
    public void test_DatesAreEqual_millisecondDifference()
    {
        final Calendar firstDate = new GregorianCalendar();
        firstDate.set(Calendar.MILLISECOND, 10);
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.MILLISECOND, 1);

        final boolean datesEqual = Functions.DatesAreEqual(firstDate, secondDate);

        assertTrue("The returned value should indicate that the dates are equal.", datesEqual);
    }

    /**
     * Test that YearDifferenceCeiling throws an IllegalArgumentException if given a null source date.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_YearDifferenceCeiling_nullFirstDate()
    {
        final Calendar firstDate = null;
        final Calendar secondDate = new GregorianCalendar();

        Functions.YearDifferenceCeiling(firstDate, secondDate);
    }

    /**
     * Test that YearDifferenceCeiling throws an IllegalArgumentException if given a null target date.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_YearDifferenceCeiling_nullSecondDate()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = null;

        Functions.YearDifferenceCeiling(firstDate, secondDate);
    }

    /**
     * Test that YearDifferenceCeiling returns zero when the same two dates are given.
     */
    @Test
    public void test_YearDifferenceCeiling_sameDates()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be zero.", 0, difference);
    }

    /**
     * Test that YearDifferenceCeiling returns one when the target date is one day later than the source date.
     */
    @Test
    public void test_YearDifferenceCeiling_dayLater()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.DAY_OF_MONTH, 1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be one.", 1, difference);
    }

    /**
     * Test that YearDifferenceCeiling returns one when the target date is one year later than the source date.
     */
    @Test
    public void test_YearDifferenceCeiling_yearLater()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.YEAR, 1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be one.", 1, difference);
    }

    /**
     * Test that YearDifferenceCeiling returns two when the target date is one year and one day later than the source
     * date.
     */
    @Test
    public void test_YearDifferenceCeiling_yearDayLater()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.YEAR, 1);
        secondDate.add(Calendar.DAY_OF_MONTH, 1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be two.", 2, difference);
    }

    /**
     * Test that the difference is zero when the target date is one day earlier that the source date.
     */
    @Test
    public void test_YearDifferenceCeiling_dayEarlier()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.DAY_OF_MONTH, -1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be zero.", 0, difference);
    }

    /**
     * Test that the difference is negative one when the target date is one year earlier than the source date.
     */
    @Test
    public void test_YearDifferenceCeiling_yearEarlier()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.YEAR, -1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be negative one.", -1, difference);
    }

    /**
     * Test that the difference is negative one when the target date is one year and one day earlier than the source
     * date.
     */
    @Test
    public void test_YearDifferenceCeiling_yearDayEarlier()
    {
        final Calendar firstDate = new GregorianCalendar();
        final Calendar secondDate = (Calendar) firstDate.clone();
        secondDate.add(Calendar.YEAR, -1);
        secondDate.add(Calendar.DAY_OF_MONTH, -1);

        final int difference = Functions.YearDifferenceCeiling(firstDate, secondDate);

        assertEquals("The returned difference should be negative one.", -1, difference);
    }

    @Test
    public void test_RoundUpYears_Half()
    {
        assertEquals(18, Functions.YearDifferenceCeiling(35 / 2d));
    }

    @Test
    public void test_RoundUpYears_Less_Than_Half()
    {
        assertEquals(1, Functions.YearDifferenceCeiling((10 / 100d)));
    }

    @Test
    public void test_RoundUpYears_Whole_Number()
    {
        assertEquals(15, Functions.YearDifferenceCeiling((30 / 2d)));
    }

    /**
     * Test that AddYears throws an IllegalArgumentException if a null base date is given.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddYears_nullBaseDate()
    {
        final Calendar baseDate = null;
        final Integer[] yearsToAdd = new Integer[]{1, 2, 3};

        Functions.AddYears(baseDate, yearsToAdd);
    }

    /**
     * Test that AddYears throws an IllegalArgumentException if a null array of years to add is given.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddYears_nullYearsToAdd()
    {
        final Calendar baseDate = new GregorianCalendar();
        final Integer[] yearsToAdd = null;

        Functions.AddYears(baseDate, yearsToAdd);
    }

    /**
     * Test that AddYears leaves the base date unchanged if not years to add are provided.
     */
    @Test
    public void test_AddYears_noYears()
    {
        final Calendar baseDate = new GregorianCalendar();
        final Integer[] yearsToAdd = new Integer[]{};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The two year values should be the same.", baseDate.get(Calendar.YEAR), returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears increments the year by the value provided.
     */
    @Test
    public void test_AddYears_addYearSingleValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{3};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2013.", 2013, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears increments the year by the sum of the two values provided.
     */
    @Test
    public void test_AddYears_addYearTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{2, 3};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2015.", 2015, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears decrements the year by the value provided.
     */
    @Test
    public void test_AddYears_subtractYearSingleValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{-2};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2008.", 2008, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears decrements the year by the sum of the two values provided.
     */
    @Test
    public void test_AddYears_subtractYearTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{-2, -3};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2005.", 2005, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears can handle positive and negative values.
     */
    @Test
    public void test_AddYears_incrementAndDecrement()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{-2, 3};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2011.", 2011, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddYears ignore null values.
     */
    @Test
    public void test_AddYears_ignoreNulls()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 5);
        baseDate.set(Calendar.YEAR, 2010);
        final Integer[] yearsToAdd = new Integer[]{null, 3};

        final Calendar returnedDate = Functions.AddYears(baseDate, yearsToAdd);

        assertEquals("The returned date should be of the year 2013.", 2013, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddMonths throws an IllegalArgumentException when given a null base date.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddMonths_nullBaseDate()
    {
        final Calendar baseDate = null;
        final Integer[] monthsToAdd = new Integer[]{1, 2};

        Functions.AddMonths(baseDate, monthsToAdd);
    }

    /**
     * Test that AddMonths throws an IllegalArgumentException when given a null array of months.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddMonths_nullMonthsToAdd()
    {
        final Calendar baseDate = new GregorianCalendar();
        final Integer[] monthsToAdd = null;

        Functions.AddMonths(baseDate, monthsToAdd);
    }

    /**
     * Test that AddMonths can increment the number of months by a single value.
     */
    @Test
    public void test_AddMonths_addMonthSingleValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{2};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the month of July.", 6, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that AddMonths can increment the number of months by the sum of two values.
     */
    @Test
    public void test_AddMonths_addMonthTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{2, 3};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the month of October.", 9, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that AddMonths can decrement the number of months by the given value.
     */
    @Test
    public void test_AddMonths_subtractMonthSingleValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{-1};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the month of April.", 3, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that AddMonths can decrement by the sum of the two values given.
     */
    @Test
    public void test_AddMonths_subtractMonthTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{-1, -2};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the month of February.", 1, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that AddMonths ignores null values of months.
     */
    @Test
    public void test_AddMonths_ignoreNulls()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{null, 1};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the month of June.", 5, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that mixes of positive and negative values are handled.
     */
    @Test
    public void test_AddMonths_positiveAndNegative()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.MONTH, 4);
        final Integer[] monthsToAdd = new Integer[]{-2, 3};

        final Calendar returnedDate = Functions.AddMonths(baseDate, monthsToAdd);

        assertEquals("The returned date should be in the mont of June", 5, returnedDate.get(Calendar.MONTH));
    }

    /**
     * Test that a date of 19/11/2020 minus 60 months plus 21 months equals 19/8/2017.
     */
    @Test
    public void test_AddMonths_minus39months()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 19);
        baseDate.set(Calendar.MONTH, 10);
        baseDate.set(Calendar.YEAR, 2020);

        final Calendar returnedDate = Functions.AddMonths(baseDate, -60, 21);

        assertEquals("The returned day should be 19.", 19, returnedDate.get(Calendar.DAY_OF_MONTH));
        assertEquals("The returned month should be August.", 7, returnedDate.get(Calendar.MONTH));
        assertEquals("The returned year should be 2017.", 2017, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that a date of 26/10/2001 plus 12 months plus 180 months equals 26/10/2017.
     */
    @Test
    public void test_AddMonths_hullIndicator4()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 26);
        baseDate.set(Calendar.MONTH, 9);
        baseDate.set(Calendar.YEAR, 2001);

        final Calendar returnedDate = Functions.AddMonths(baseDate, 12, 180);

        assertEquals("The returned day should be 26.", 26, returnedDate.get(Calendar.DAY_OF_MONTH));
        assertEquals("The returned month should be October.", 9, returnedDate.get(Calendar.MONTH));
        assertEquals("The returned year should be 2017.", 2017, returnedDate.get(Calendar.YEAR));
    }

    /**
     * Test that AddDays throws an IllegalArgumentException when given a null base date.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddDays_nullBaseDate()
    {
        final Calendar baseDate = null;
        final Integer[] daysToAdd = new Integer[]{1, 2};

        Functions.AddDays(baseDate, daysToAdd);
    }

    /**
     * Test that an IllegalArgumentException is thrown when a null array of days to add is given.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_AddDays_nullDaysToAdd()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = null;

        Functions.AddDays(baseDate, daysToAdd);
    }

    /**
     * Test that the day is incremented by the single day value.
     */
    @Test
    public void test_AddDays_addDaySingleValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{2};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the day 17.", 17, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that the day is incremented by the sum of the two values.
     */
    @Test
    public void test_AddDays_addDayTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{2, 3};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the day 20.", 20, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that the day is decremented by a negative value.
     */
    @Test
    public void test_AddDays_subtractDaysOneValue()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{-2};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the day 13.", 13, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that the day is decremented by the sum of two negative values.
     */
    @Test
    public void test_AddDays_subtractDaysTwoValues()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{-2, -3};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the day 10.", 10, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that null values are ignored when adding days.
     */
    @Test
    public void test_AddDays_ignoreNulls()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{null, 4};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the day 19.", 19, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that mixes of positive and negative values are supported.
     */
    @Test
    public void test_AddDays_positiveAndNegative()
    {
        final Calendar baseDate = new GregorianCalendar();
        baseDate.set(Calendar.DAY_OF_MONTH, 15);
        final Integer[] daysToAdd = new Integer[]{-2, 3};

        final Calendar returnedDate = Functions.AddDays(baseDate, daysToAdd);

        assertEquals("The returned date should have the value 16.", 16, returnedDate.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Test that passing in a null AssetItem causes an IllegalArgumentException to be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_CheckAttribute_nullAssetItem()
    {
        final AssetItem assetItem = null;
        final String attribute = "[HAS BILGE WELLS][YES]";

        Functions.CheckAttribute(assetItem, attribute);
    }

    /**
     * Test that passing in a null attribute causes an IllegalArgumentException to be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_CheckAttribute_nullAttribute()
    {
        final AssetItem assetItem = new AssetItem();
        final String attribute = null;

        Functions.CheckAttribute(assetItem, attribute);
    }

    /**
     * Test that passing in a malformed attribute causes an IllegalArgumentException to be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test_CheckAttribute_malformedAttribute()
    {
        final AssetItem assetItem = new AssetItem();
        final String attribute = "[HAS BILGE WELLS]";

        Functions.CheckAttribute(assetItem, attribute);
    }

    /**
     * Test that an attribute that is not present on the AssetItem generates a false value.
     */
    @Test
    public void test_CheckAttribute_noAttribute()
    {
        final AssetItem assetItem = new AssetItem();
        final String attribute = "[HAS BILGE WELLS][YES]";

        final boolean match = Functions.CheckAttribute(assetItem, attribute);

        assertFalse("The AssetItem should not have the attribute HAS BILGE WELLS", match);
    }

    /**
     * Test that an attribute which has a different value than on the AssetItem generates a false value.
     */
    @Test
    public void test_CheckAttribute_differentValue()
    {
        final AssetItem assetItem = new AssetItem();
        assetItem.getAttributes().put("HAS BILGE WELLS", "NO");
        final String attribute = "[HAS BILGE WELLS][YES]";

        final boolean match = Functions.CheckAttribute(assetItem, attribute);

        assertFalse("The AssetItem should not have a matching value for HAS BILGE WELLS.", match);
    }

    /**
     * Test that an attribute with a matching value on the AssetItem generates a true value.
     */
    @Test
    public void test_CheckAttribute_matchingAttribute()
    {
        final AssetItem assetItem = new AssetItem();
        assetItem.getAttributes().put("HAS BILGE WELLS", "YES");
        final String attribute = "[HAS BILGE WELLS][YES]";

        final boolean match = Functions.CheckAttribute(assetItem, attribute);

        assertTrue("The AssetItem should have a HAS BILGE WELLS attribute with the value YES.", match);
    }
}
