package com.detica.ukdc.drools.service.model;

import java.util.List;
import java.util.Map;

import com.detica.ukdc.drools.service.model.ItemTasks.ServiceAssetItemPair;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ItemTasksTest
{
    /**
     * Test that changes made via the add method are reflected.
     */
    @Test
    public void test_add_getItemTasks()
    {
        final ItemTasks itemTasks = new ItemTasks();

        final Map<ServiceAssetItemPair, List<String>> firstItemTasks = itemTasks.getItemTasks();
        assertNotNull("The returned item tasks should not be null.", firstItemTasks);
        assertEquals("The returned item tasks should have no entries", 0, firstItemTasks.size());

        final Service firstService = new Service();
        final AssetItem firstItem = new AssetItem();
        final String task = "Examine";
        itemTasks.add(firstService, firstItem, task);

        final Map<ServiceAssetItemPair, List<String>> secondItemTasks = itemTasks.getItemTasks();
        assertEquals("The returned item tasks should have one entry.", 1, secondItemTasks.size());
        final ServiceAssetItemPair firstKey = new ServiceAssetItemPair(firstService, firstItem);
        assertTrue("The returned item tasks should have an entry for the given asset item.", secondItemTasks.containsKey(firstKey));
        final List<String> firstAssociatedTasks = secondItemTasks.get(firstKey);
        assertEquals("There should be one task associated with the asset item.", 1, firstAssociatedTasks.size());
        assertTrue("The task that is associated should be Examine.", firstAssociatedTasks.contains("Examine"));

        final String secondTask = "Overall Survey";
        itemTasks.add(firstService, firstItem, secondTask);

        final Map<ServiceAssetItemPair, List<String>> thirdItemTasks = itemTasks.getItemTasks();
        assertEquals("The returned item tasks should have one entry.", 1, thirdItemTasks.size());
        assertTrue("The returned item tasks should have an entry for the given asset item.", thirdItemTasks.containsKey(firstKey));
        final List<String> secondAssociatedTasks = thirdItemTasks.get(firstKey);
        assertEquals("There should be two tasks associated with the asset item.", 2, secondAssociatedTasks.size());
        assertTrue("One of the associated tasks is Examine.", secondAssociatedTasks.contains("Examine"));
        assertTrue("One of the associated tasks is Overall Survey.", secondAssociatedTasks.contains("Overall Survey"));
    }
}
