package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.detica.ukdc.drools.service.model.AssetItem;
import com.detica.ukdc.drools.service.model.ItemTasks.ServiceAssetItemPair;
import com.detica.ukdc.drools.service.model.ItemTasks;

/**
 * This class is used to generate different types of responses for different API calls.
 */
public class ResponseFactory
{
  /**
   * This method creates a response that adheres to the format specified by the API when generating 
   * Tasks for a particular Service for a particular Asset.
   *
   * The body's ExecutionResultImpl should allow the retrieval of an ItemTasks instance under 
   * the itemTasks name.
   *
   * The generated Response will be of type AssetServiceResponse and its AssetServiceTasks will
   * have a null Service Identifier to stop the attribute from being displayed.
   *
   * @param message A message containing an instance of ExecutionResultImpl in its body.
   *
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if
   *                                 the body is null.
   */
  public void createAssetServiceTaskResponse(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createAssetServiceTaskResponse.");
    }
    
    final ExecutionResultImpl results = message.getMandatoryBody(ExecutionResultImpl.class);
    final ItemTasks itemTasks = (ItemTasks)results.getResults().get("itemTasks");
    
    final Map<ServiceAssetItemPair, List<String>> itemsToTasks = itemTasks.getItemTasks();
    
    final List<AssetItemTasks> assetItemTasks = new ArrayList<>(itemsToTasks.size());
    
    for(final Map.Entry<ServiceAssetItemPair, List<String>> itemToTasks : itemsToTasks.entrySet())
    {
      final Integer assetItemId = itemToTasks.getKey().getAssetItem().getIdentifier();
      assetItemTasks.add(new AssetItemTasks(assetItemId, itemToTasks.getValue()));
    }
    
    final AssetServiceTasks assetServiceTasks = new AssetServiceTasks(null, assetItemTasks);
    final AssetServiceResponse assetServiceResponse = new AssetServiceResponse(assetServiceTasks);
    
    message.setBody(assetServiceResponse);
  }
  
  /**
   * This method creates a response that adheres to the format specified by the API when generating 
   * Tasks for all active Services for a particular Asset.
   *
   * The body's ExecutionResultImpl should allow the retrieval of an ItemTasks instance under 
   * the itemTasks name.
   *
   * The generated Response will be of type AssetResponse.
   *
   * @param message A message containing an instance of ExecutionResultImpl in its body.
   *
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if
   *                                 the body is null.
   */
  public void createAssetResponse(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createAssetResponse.");
    }
    
    final ExecutionResultImpl results = message.getMandatoryBody(ExecutionResultImpl.class);
    final ItemTasks itemTasks = (ItemTasks)results.getResults().get("itemTasks");
    
    final Map<ServiceAssetItemPair, List<String>> itemsToTasks = itemTasks.getItemTasks();
    final Map<Integer, List<AssetItemTasks>> serviceIdToItemTasks = new HashMap<>();
    
    for(final Map.Entry<ServiceAssetItemPair, List<String>> itemToTasks : itemsToTasks.entrySet())
    {
      final Integer assetItemId = itemToTasks.getKey().getAssetItem().getIdentifier();
      final Integer serviceId = itemToTasks.getKey().getService().getIdentifier();
      AssetItemTasks assetItemTasks = new AssetItemTasks(assetItemId, itemToTasks.getValue());
      
      if(serviceIdToItemTasks.containsKey(serviceId))
      {
        serviceIdToItemTasks.get(serviceId).add(assetItemTasks);
      }
      else
      {
        final List<AssetItemTasks> assetItemTasksList = new ArrayList<>();
        assetItemTasksList.add(assetItemTasks);
        serviceIdToItemTasks.put(serviceId, assetItemTasksList);
      }
    }
    
    final List<AssetServiceTasks> assetServiceTasks = new ArrayList<>(serviceIdToItemTasks.size());
    for(final Map.Entry<Integer, List<AssetItemTasks>> assetService : serviceIdToItemTasks.entrySet())
    {
      assetServiceTasks.add(new AssetServiceTasks(assetService.getKey(), assetService.getValue()));
    }
    
    final AssetResponse assetResponse = new AssetResponse(assetServiceTasks);
    message.setBody(assetResponse);
  }
  
  /**
   * This method is used to create a response that represents the failed processing of the 
   * task generation rules.
   *
   * The message should have a body of a ServiceResponse. The Msg attribute of this ServiceResponse
   * will be used as the FailureResponse message.
   *
   * @param message The message contaning the failure message and on which the FailureResponse
   *                will be set.
   *
   * @throws IllegalArgumentException Thrown if a null message is given.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if the body is null.
   */
  public void createFailureResponse(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createFailureResponse.");
    }
    
    final ServiceResponse serviceResponse = message.getMandatoryBody(ServiceResponse.class);
    
    final FailureResponse failureResponse = new FailureResponse(serviceResponse.getMsg());
    
    message.setBody(failureResponse);
  }
}