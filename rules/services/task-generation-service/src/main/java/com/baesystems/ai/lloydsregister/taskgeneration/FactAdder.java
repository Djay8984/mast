package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.facts.Fact;
import com.baesystems.ai.lloydsregister.facts.FactCollection;

import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.AssetItem;
import com.detica.ukdc.drools.service.model.ItemTasks;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * This class is responsible for creating the necessary Facts from the 
 * output of database queries.
 */
public class FactAdder
{
  /**
   * The name of the header used to store the Asset instance in between invocations.
   */
  private static final String ASSET_HEADER = "temp_AssetFact";
  
  /**
   * The name of the header used to store AssetItem instances between invocations.
   */
  private static final String ASSET_ITEM_HEADER = "temp_AssetItemFacts";
  
  /**
   * The header used to store the FactCollection.
   */
  private final String factCollectionHeaderName;
  
  /**
   * Creates a new FactAdder that populates a FactCollection in the given header.
   *
   * @param factCollectionHeaderName The name of the header that stores the FactCollection.
   *                                 Must not be null.
   *
   * @throws IllegalArgumentException Thrown if factCollectionHeaderName is null.
   */
  public FactAdder(final String factCollectionHeaderName)
  {
    if(factCollectionHeaderName == null)
    {
      throw new IllegalArgumentException("Null FactCollection header name given to FactAdder.");
    }
    this.factCollectionHeaderName = factCollectionHeaderName;
  }
  
  /**
   * This method creates an Asset from the results of the asset type query and 
   * sets it to a temporary header to be completed when the class notation results are available.
   *
   * This method expects the message body to contain a List with one entry which is a Map of 
   * String to Object. This Map should have a key of type_code with the asset type code as its
   * value.
   *
   * @param message The message containing the query result and whose header will be set with the
   *                Asset.
   *
   * @throws IllegalArgumentException Thrown if a null message is given.
   * @throws InvalidPayloadException Thrown by the getMandatoryBody method on the message if it does 
   *                                 not have a body.
   * @throws InvalidInputCondition Thrown if the query results do not have a single List entry.
   * @throws IllegalStateException Thrown if one of the required keys is not present in the Map.
   */
  public void createAsset(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createAsset.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    
    if(queryResult.size() != 1)
    {
      throw new InvalidInputException("No Asset with the given ID exists in the Database.");
    }
    
    final Map<String, Object> resultRow = queryResult.get(0);
    
    if(!resultRow.containsKey("type_code"))
    {
      throw new IllegalStateException("No Asset Type Code in the query result.");
    }
    
    final Asset asset = new Asset();
    
    asset.setAssetType((String) resultRow.get("type_code"));
    
    message.setHeader(ASSET_HEADER, asset);
    
    // Remove the below six lines when structured Class Notation reference data is introduced into IDS.
    final FactCollection factCollection = message.getHeader(this.factCollectionHeaderName, FactCollection.class);
    if(factCollection !=null)
    {
      final Fact assetFact = new Fact("asset", asset);
      factCollection.addFact(assetFact);
    }
  }
  
  /**
   * This method adds the results of the class notations query to the Asset stored in the temporary 
   * header. After doing this it creates the Fact instance for the Asset and adds it to the Fact 
   * Collection.
   *
   * This method expects the message body to be a List containing zero to many Maps of String to Object. The 
   * Maps should have a name key that has an associated class notation value that is added to the Asset.
   *
   * @param message The message containing the query results, temporary Asset and the FactCollection.
   *
   * @throws IllegalArgumentException Thrown if the message argument is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if the body is null.
   * @throws IllegalStateException Thrown if the message does not have an Asset in the temporary header or
   *                               there is no FactCollection in the configured header.
   */
  public void addClassNotations(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to addClassNotations.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    final Asset asset = message.getHeader(ASSET_HEADER, Asset.class);
    if(asset == null)
    {
      throw new IllegalStateException("No temporary asset on the input message.");
    }
    final FactCollection factCollection = message.getHeader(this.factCollectionHeaderName, FactCollection.class);
    if(factCollection == null)
    {
      throw new IllegalStateException("No Fact Collection on the message under header " + this.factCollectionHeaderName);
    }
    
    for(final Map<String, Object> resultRow : queryResult)
    {
      asset.getClassNotations().add((String) resultRow.get("name"));
    }
    
    final Fact assetFact = new Fact("asset", asset);
    factCollection.addFact(assetFact);
  }
  
  /**
   * This method creates AssetItem instances from the results of the query and sets them to a temporary 
   * header to be completed when their attributes are retrieved.
   *
   * A comma-separated list of Asset Item IDs are also stored in the assetItemIds header. If no Asset Item
   * IDs are found then the string is set to "null".
   *
   * The body is expected to be a List containing zero to many Maps each representing an Asset Item. 
   * Each Map instance should have a property of asset_item_type with an associated value of the Asset 
   * Item's type. It should also have an id key with the Asset Item's ID. The Asset in the temporary
   * Asset header is set as the parent Asset for each Asset Item.
   *
   * @param message The message containing the query results and asset header.
   
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if the 
   *                                 body is null.
   * @throws IllegalStateException Thrown if there is no temporary Asset or if there is no id or 
   *                               no asset_item_type key on a Map instance.
   */
  public void createAssetItemsAndIdList(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createAssetItemsAndIdList.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    final Asset asset = message.getHeader(ASSET_HEADER, Asset.class);
    if(asset == null)
    {
      throw new IllegalStateException("No asset on the input message.");
    }
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(queryResult.size());
    final List<Integer> idList = new ArrayList<>(queryResult.size());
    
    for(final Map<String, Object> result : queryResult)
    {
      final AssetItem assetItem = new AssetItem();
      final String assetItemType = (String)result.get("asset_item_type");
      final Integer assetItemId = (Integer)result.get("id");
      
      if(assetItemId == null)
      {
        throw new IllegalStateException("Null asset item id received.");
      }
      if(assetItemType == null)
      {
        throw new IllegalStateException("Null asset item type received for id " + assetItemId);
      }
      assetItem.setAssetItemType(assetItemType);
      assetItem.setParentAsset(asset);
      assetItem.setIdentifier(assetItemId);
      idToAssetItemMap.put(assetItemId, assetItem);
      
      idList.add(assetItemId);
    }
    
    message.setHeader("assetItemIds", idList);
    message.setHeader(ASSET_ITEM_HEADER, idToAssetItemMap);
  }
  
  /**
   * This method adds attributes to the previously collected AssetItems and 
   * then adds them to the FactCollection.
   *
   * The body of the message is expected to be a List of Maps, each Map representing an attribute 
   * on an Asset Item. Each Map is expected to contain an id key that refers to the Asset Item that
   * the attribute sits on. It should also contain an attribute_name key that is associated with the 
   * name of the attribute and a value_string or value_bool key that is associated with the value of 
   * the attribute. If the data_type is not boolean or string then the attribute will be ignored.
   *
   * @param message The message with the query results and the interim AssetItems and FactCollection.
   *
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method if the body is 
   *                                 null.
   * @throws IllegalStateException Thrown if there is no ID to AssetItem Map orFactCollection on the message. 
   *                               Or if any of the maps in the query result do not contain id, data_type or
   *                               attribute_name keys. Or if the correct value type field is not set.
   */
  public void addAttributesToAssetItems(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to addAttributesToAssetItems.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    final Map<Integer, AssetItem> idToAssetItemMap = (Map<Integer, AssetItem>) message.getHeader(ASSET_ITEM_HEADER);
    
    if(idToAssetItemMap == null)
    {
      throw new IllegalStateException("No ID to Asset Item map available.");
    }
    
    final FactCollection factCollection = message.getHeader(this.factCollectionHeaderName, FactCollection.class);
    if(factCollection == null)
    {
      throw new IllegalStateException("No Fact Collection on the message under header " + this.factCollectionHeaderName);
    }
    
    for(final Map<String, Object> result: queryResult)
    {
      final Integer assetItemId = (Integer) result.get("id");
      if(assetItemId == null)
      {
        throw new IllegalStateException("Null asset item id from attribute.");
      }
      final String attributeName = (String) result.get("attribute_name");
      if(attributeName == null)
      {
        throw new IllegalStateException("Null attribute name for asset item with id " + assetItemId);
      }
      final String dataType = (String) result.get("data_type");
      if(dataType == null)
      {
        throw new IllegalStateException("Null data type for attribute " + attributeName + " for asset item " + assetItemId);
      }
      
      String attributeValue = null;
      if("boolean".equals(dataType))
      {
        final Boolean value = (Boolean) result.get("value_bool");
        if(value == null)
        {
          throw new IllegalStateException("Null bool value for attribute " + attributeName + " for asset item " + assetItemId);
        }
        attributeValue = value? "YES" : "NO";
      }
      else if("string".equals(dataType))
      {
        attributeValue = (String) result.get("value_string");
        if(attributeValue == null)
        {
          throw new IllegalStateException("Null string value for attribute " + attributeName + " for asset item " + assetItemId);
        }
      }
      
      if(attributeValue != null)
      {
        final AssetItem assetItem = idToAssetItemMap.get(assetItemId);
        assetItem.getAttributes().put(attributeName, attributeValue);
      }
    }
    
    for(final AssetItem assetItem : idToAssetItemMap.values())
    {
      final Fact assetItemFact = new Fact(assetItem);
      factCollection.addFact(assetItemFact);
    }
  }
  
  /**
   * This method creates Services from the query results and adds them as Facts to the FactCollection. It also
   * creates the ItemTasks instance used to report back results.
   *
   * The body is expected to be a List containing one Map which represents a Service to be created. The Map
   * should have a code key with a value that is the Service's code. The Map should have an asset_item_id 
   * key with a value of null if the Service is scoped to the whole Asset or an ID of the specific Asset 
   * Item that it is scoped to.
   *
   * @param message The message containing the query result and FactCollection.
   *
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method 
   *                                 if the body is null.
   * @throws InvalidInputCondition Thrown if zero or more than one rows are returned by the query.
   * @throws IllegalStateException Thrown if there is no FactCollection or ID to AssetItem Map on the message or if one 
   *                               of the Maps do not contain a code key.
   */
  public void createService(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createService.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    
    if(queryResult.size() != 1)
    {
      throw new InvalidInputException("No Service with the given ID exists in the database.");
    }
    
    this.createServices(message);
  }
  
  /**
   * This method creates Services from the query results and adds them as Facts to the FactCollection. It also
   * creates the ItemTasks instance used to report back results.
   *
   * The body is expected to be a List containing Maps which represents a Services to be created. The Maps
   * should have a code key with a value that is the Service's code. The Maps should have an asset_item_id 
   * key with a value of null if the Service is scoped to the whole Asset or an ID of the specific Asset 
   * Item that it is scoped to.
   *
   * @param message The message containing the query result and FactCollection.
   *
   * @throws IllegalArgumentException Thrown if the given message is null.
   * @throws InvalidPayloadException Thrown by the message's getMandatoryBody method 
   *                                 if the body is null.
   * @throws IllegalStateException Thrown if there is no FactCollection or ID to AssetItem Map on the message or if one 
   *                               of the Maps do not contain a code key.
   */
  public void createServices(final Message message) throws InvalidPayloadException
  {
    if(message == null)
    {
      throw new IllegalArgumentException("Null message given to createServices.");
    }
    
    final List<Map<String, Object>> queryResult = (List<Map<String, Object>>) message.getMandatoryBody();
    
    final FactCollection factCollection = message.getHeader(this.factCollectionHeaderName, FactCollection.class);
    if(factCollection == null)
    {
      throw new IllegalStateException("No Fact Collection on the message under header " + this.factCollectionHeaderName);
    }
    
    final Asset asset = (Asset) message.getHeader(ASSET_HEADER, Asset.class);
    if(asset == null)
    {
      throw new IllegalStateException("No Asset on the message.");
    }
    
    final Map<Integer, AssetItem> idToAssetItemMap = (Map<Integer, AssetItem>) message.getHeader(ASSET_ITEM_HEADER);
    if(idToAssetItemMap == null)
    {
      throw new IllegalStateException("No ID to Asset Item mapping on the message.");
    }
    
    for(final Map<String, Object> result : queryResult)
    {
      final String serviceCode = (String) result.get("code");
      if(serviceCode == null)
      {
        throw new IllegalStateException("Null service code found.");
      }
      final Integer assetItemId = (Integer) result.get("asset_item_id");
      final Integer serviceId = (Integer) result.get("id");
      
      final Service service = new Service();
      final ServiceCatalogue catalogue = new ServiceCatalogue();
      catalogue.setServiceCode(serviceCode);
      service.setServiceCatalogue(catalogue);
      service.setAsset(asset);
      // Add the service's ID if available. Not necessary for single Service queries.
      if(serviceId != null)
      {
        service.setIdentifier(serviceId);
      }
      
      if(assetItemId != null)
      {
        if(!idToAssetItemMap.containsKey(assetItemId))
        {
          throw new IllegalStateException("Service is associated with an asset item on a different asset."
            + " Asset Item ID " + assetItemId);
        }
        service.setAssetItem(idToAssetItemMap.get(assetItemId));
      }
      
      final Fact serviceFact = new Fact(service);
      factCollection.addFact(serviceFact);
    }
    
    final ItemTasks itemTasks = new ItemTasks();
    final Fact itemTasksFact = new Fact("itemTasks", itemTasks);
    factCollection.addFact(itemTasksFact);
  }
}