package com.baesystems.ai.lloydsregister.taskgeneration;

/**
 * This class holds the response from Execution Server in such a way 
 * that it can be serialised to JSON and match the defined service API.
 */
public class AssetServiceResponse
{ 
  /**
   * The service that the request was for.
   */
  private final AssetServiceTasks service;
  
  /**
   * Creates a new AssetServiceResponse that reports the given AssetServiceTasks instance.
   *
   * @param service The AssetServiceTasks instance generated from the results of the rules.
   */
  public AssetServiceResponse(final AssetServiceTasks service)
  {
    this.service = service;
  }
  
  /**
   * Returns the status of the Response which should always be SUCCESS.
   *
   * @return The String SUCCESS.
   */
  public String getStatus()
  {
    return "SUCCESS";
  }
  
  /**
   * Returns the Service associated with the Response.
   *
   * @return The service that the rules were run against.
   */
  public AssetServiceTasks getService()
  {
    return this.service;
  }
}