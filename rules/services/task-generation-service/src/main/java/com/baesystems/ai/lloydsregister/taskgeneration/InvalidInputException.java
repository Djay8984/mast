package com.baesystems.ai.lloydsregister.taskgeneration;

/**
 * This Exception is thrown when it is determined that the input parameters 
 * provided to the service are invalid.
 */
public class InvalidInputException extends RuntimeException
{
  /**
   * Creates a new InvalidInputException with the given message.
   */
  public InvalidInputException(final String message)
  {
    super(message);
  }
}