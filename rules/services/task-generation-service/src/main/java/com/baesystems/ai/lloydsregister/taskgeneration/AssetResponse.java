package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.List;

/**
 * This class represents responses to requests to run task geenration rules for 
 * all Services on an Asset.
 */
public class AssetResponse
{
  /**
   * The Services that are associated with the Asset that have had Tasks generated for them.
   */
  private final List<AssetServiceTasks> services;
  
  /**
   * Creates a new AssetResponse which holds the given Services.
   *
   * @param services The Services associateed with this Asset. Not defensively copied.
   */
  public AssetResponse(final List<AssetServiceTasks> services)
  {
    this.services = services;
  }
  
  /**
   * Returns the status of the processing which should always be SUCCESS.
   *
   * @return SUCCESS
   */
  public String getStatus()
  {
    return "SUCCESS";
  }
  
  /**
   * The Services which have generated Tasks and are associated with this Asset.
   *
   * @return The Services with generated tasks.
   */
  public List<AssetServiceTasks> getServices()
  {
    return this.services;
  }
}