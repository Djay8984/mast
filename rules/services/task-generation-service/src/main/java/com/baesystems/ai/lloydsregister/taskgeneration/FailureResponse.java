package com.baesystems.ai.lloydsregister.taskgeneration;

import org.apache.camel.Exchange;

/**
 * This class represents an API response that reports a failure.
 */
public class FailureResponse
{
  /**
   * The JSON representation of of this FailureResponse.
   */
  private final String json;
  
  /**
   * The failure message to report back to the client of the service.
   */
  private final String message;
  
  /**
   * Creates a new response that represents a processing failure.
   *
   * @param message The human-readable message describing the failure.
   */
  public FailureResponse(final String message)
  {
    this.message = message;
    final StringBuilder jsonBuilder = new StringBuilder();
    jsonBuilder.append("{\"status\":")
    .append("\"FAILURE\"")
    .append(",\"message\":\"")
    .append(message)
    .append("\"}");
    this.json = jsonBuilder.toString();
  }
  
  /**
   * Returns the status of the request, in this case always FAILURE.
   *
   * @return FAILURE
   */
  public String getStatus()
  {
    return "FAILURE";
  }
  
  /**
   * Returns the human-readable message associated with the failure.
   *
   * @return The failure message.
   */
  public String getMessage()
  {
    return this.message;
  }
  
  /**
   * Prints a JSON representation of this object.
   *
   * @return The JSON of this object.
   */
  @Override
  public String toString()
  {
    return this.json;
  }
}