package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class represents how a Service containing Tasks associated 
 * with AssetItems is serialised in the API response.
 */
@JsonInclude(Include.NON_NULL)
public class AssetServiceTasks
{
  /**
   * The identifier of the Service in question. Set to null to not include.
   */
  private final Integer serviceIdentifier;
  
  /**
   * The AssetItem to Task relationships relevant for this service.
   */
  private final List<AssetItemTasks> assetItems;
  
  /**
   * Creates a new AssetServiceTasks holding the given service identifier and asset items.
   *
   * Setting either of the arguments to null will cause that element not to appear in the response.
   *
   * @param serviceIdentifier The ID of the Service that this object relates to.
   * @param assetItems The AssetItems with associated tasks that are related to this Service.
   */
  public AssetServiceTasks(final Integer serviceIdentifier, final List<AssetItemTasks> assetItems)
  {
    this.serviceIdentifier = serviceIdentifier;
    this.assetItems = assetItems;
  }

  /**
   * Returns the ID of the service that this object relates to.
   *
   * @return The service identifier.
   */
  public Integer getServiceIdentifier()
  {
    return this.serviceIdentifier;
  }
  
  /**
   * Returns the AssetItems and related Tasks that are relevant to this service.
   *
   * Not defensively copied.
   *
   * @return The AssetItems related to this Service.
   */
  public List<AssetItemTasks> getAssetItems()
  {
    return this.assetItems;
  }
}