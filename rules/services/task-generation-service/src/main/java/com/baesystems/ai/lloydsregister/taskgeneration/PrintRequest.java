package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.Map;
import org.apache.camel.Message;

public class PrintRequest
{
  public void printRequest(Message message)
  {
    Map<String, Object> headers = message.getHeaders();
    
    System.out.println("Headers");
    for(Map.Entry<String, Object> header: headers.entrySet())
    {
      System.out.println(header.getKey() + ": " + header.getValue());
    }
  }
}