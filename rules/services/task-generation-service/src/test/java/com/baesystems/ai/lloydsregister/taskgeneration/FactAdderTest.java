package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Message;

import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.AssetItem;
import com.detica.ukdc.drools.service.model.ItemTasks;
import com.detica.ukdc.drools.service.model.Service;

import com.baesystems.ai.lloydsregister.facts.Fact;
import com.baesystems.ai.lloydsregister.facts.FactCollection;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class FactAdderTest
{
  /**
   * Test that passing in a null fact collection header name causes an IllegalArgumentException
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_constructor_nullFactCollectionHeaderName()
  {
    final String factCollectionHeaderName = null;
    
    new FactAdder(factCollectionHeaderName);
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_createAsset_nullMessage() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = null;
    
    factAdder.createAsset(message);
  }
  
  /**
   * Test that passing in an empty List in the message body causes an InvalidInputException
   * to be thrown.
   */
  @Test(expected=InvalidInputException.class)
  public void test_createAsset_emptyList() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    when(message.getMandatoryBody()).thenReturn(new ArrayList<Map<String, Object>>(0));
    
    factAdder.createAsset(message);
  }
  
  /**
   * Test that passing in a List with more than one entry in the message body causes an 
   * InvalidInputException to be thrown.
   */
  @Test(expected=InvalidInputException.class)
  public void test_createAsset_multipleListEntries() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(2);
    final Map<String, Object> result1 = new HashMap<>(1);
    result1.put("type_code", "A11");
    queryResults.add(result1);
    final Map<String, Object> result2 = new HashMap<>(1);
    result2.put("type_code", "A12B");
    queryResults.add(result2);
    
    final Message message = mock(Message.class);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAsset(message);
  }
  
  /**
   * Test that passing in a query result without a type_code key causes an 
   * IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createAsset_noTypeCode() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(0);
    queryResults.add(result);
    
    final Message message = mock(Message.class);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAsset(message);
  }
  
  /**
   * Test that an Asset is created in the correct header if given a valid query result.
   */
  @Test
  public void test_createAsset_assetCreated() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("type_code", "A21A");
    queryResults.add(result);
    
    final Message message = mock(Message.class);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAsset(message);
    
    final ArgumentCaptor<Asset> assetCaptor = ArgumentCaptor.forClass(Asset.class);
    
    verify(message).setHeader(eq("temp_AssetFact"), assetCaptor.capture());
    
    final Asset capturedAsset = assetCaptor.getValue();
    assertEquals("The created Asset should have a type of A21A.", "A21A", capturedAsset.getAssetType());
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_addClassNotations_nullMessage() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = null;
    
    factAdder.addClassNotations(message);
  }
  
  /**
   * Test that passing in a message without a temporary Asset in the right header causes
   * an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addClassNotations_noAssetHeader() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Message message = mock(Message.class);
    
    when(message.getMandatoryBody()).thenReturn(new ArrayList<Map<String, Object>>(0));
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addClassNotations(message);
  }
  
  /**
   * Test that passing in a message without a FactCollection in the right header causes
   * an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addClassNotations_noFactCollection() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Asset asset = mock(Asset.class);
    final Message message = mock(Message.class);
    
    when(message.getMandatoryBody()).thenReturn(new ArrayList<Map<String, Object>>(0));
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    
    factAdder.addClassNotations(message);
  }
  
  /**
   * Test that the Asset Fact is created with no Class Notations added if the 
   * query results are empty.
   */
  @Test
  public void test_addClassNotations_noClassNotations() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Message message = mock(Message.class);
    
    final List<String> classNotations = new ArrayList<>(0);
    
    when(asset.getClassNotations()).thenReturn(classNotations);
    
    when(message.getMandatoryBody()).thenReturn(new ArrayList<Map<String, Object>>(0));
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addClassNotations(message);
    
    assertTrue("No class notations should have been added to the Asset", classNotations.isEmpty());
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    
    verify(factCollection).addFact(factCaptor.capture());
    
    final Fact capturedFact = factCaptor.getValue();
    
    assertEquals("The Asset contained in the Fact should be the one contained in the temporary header.", 
                 asset, capturedFact.getFact());
  }
  
  /**
   * Test that the Asset Fact is created with a Class Notation added from the query result.
   */
  @Test
  public void test_addClassNotations_oneClassNotation() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Message message = mock(Message.class);
    
    final List<String> classNotations = new ArrayList<>(1);
    
    when(asset.getClassNotations()).thenReturn(classNotations);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("name", "CCS");
    queryResults.add(result);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addClassNotations(message);
    
    assertEquals("One class notation should have been added to the Asset", 1, classNotations.size());
    assertEquals("A class notation value of CCS should have been added to the Asset.", "CCS", classNotations.get(0));
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    
    verify(factCollection).addFact(factCaptor.capture());
    
    final Fact capturedFact = factCaptor.getValue();
    
    assertEquals("The Asset contained in the Fact should be the one contained in the temporary header.", 
                 asset, capturedFact.getFact());
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_createAssetItemsAndIdList_nullMessage() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = null;
    
    factAdder.createAssetItemsAndIdList(message);
  }
  
  /**
   * Test that passing in a message with no Asset in a temporary header causes an 
   * IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createAssetItemsAndIdList_noAssetHeader() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    when(message.getMandatoryBody()).thenReturn(new ArrayList<Map<String, Object>>(0));
    
    factAdder.createAssetItemsAndIdList(message);
  }
  
  /**
   * Test that passing in a message with a Map that has no id key causes an
   * IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createAssetItemsAndIdList_noIdKey() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    final Asset asset = mock(Asset.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<Map<String, Object>>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("asset_item_type", "CHAIN CABlE");
    queryResults.add(result);
    
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAssetItemsAndIdList(message);
  }
  
  /**
   * Test that passing in a message with a Map that has no asset_item_type key 
   * causes an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createAssetItemsAndIdList_noAssetItemTypeKey() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    final Asset asset = mock(Asset.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("id", 1);
    queryResults.add(result);
    
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAssetItemsAndIdList(message);
  }
  
  /**
   * Test that passing in a message with no Asset Items causes a "null" string to be set as
   * the ID List and an empty Asset Item Map added to the message.
   */
  @Test
  public void test_createAssetItemsAndIdList_noAssetItems() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    final Asset asset = mock(Asset.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(0);
    
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAssetItemsAndIdList(message);
    
    final ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
    verify(message).setHeader(eq("temp_AssetItemFacts"), mapCaptor.capture());
    final Map<Integer, AssetItem> capturedMap = (Map<Integer, AssetItem>) mapCaptor.getValue();
    assertTrue("There should not have been any AssetItems added.", capturedMap.isEmpty());
    
    verify(message).setHeader("assetItemIds", new ArrayList<Integer>(0));
  }
  
  /**
   * Test that passing in a message with one Asset Item causes that item to appear in the 
   * Map and its ID to appear in the ID list.
   */
  @Test
  public void test_createAssetItemsAndIdList_oneAssetItem() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    final Asset asset = mock(Asset.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(2);
    result.put("asset_item_type", "CHAIN CABLE");
    result.put("id", 1);
    queryResults.add(result);
    
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAssetItemsAndIdList(message);
    
    final ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
    verify(message).setHeader(eq("temp_AssetItemFacts"), mapCaptor.capture());
    final Map<Integer, AssetItem> capturedMap = (Map<Integer, AssetItem>) mapCaptor.getValue();
    assertEquals("There should be one AssetItem created.", 1, capturedMap.size());
    assertTrue("The Map should contain an Asset Item with ID 1.", capturedMap.containsKey(1));
    final AssetItem assetItem = capturedMap.get(1);
    assertEquals("The Asset Item's type should be CHAIN CABLE.", "CHAIN CABLE", assetItem.getAssetItemType());
    assertEquals("The Asset Item's parent Asset should be the Asset held in the temporary header", 
                 asset, assetItem.getParentAsset());
    
    final List<Integer> expectedIds = new ArrayList<>(1);
    expectedIds.add(1);
    verify(message).setHeader("assetItemIds", expectedIds);
  }
  
  /**
   * Test that when passing in a message with two Asset Items both are added to the Map and both
   * IDs appear in the ID list.
   */
  @Test
  public void test_createAssetItemsAndIdList_twoAssetItems() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    final Asset asset = mock(Asset.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result1 = new HashMap<>(2);
    result1.put("asset_item_type", "CHAIN CABLE");
    result1.put("id", 1);
    queryResults.add(result1);
    final Map<String, Object> result2 = new HashMap<>(2);
    result2.put("asset_item_type", "LEG");
    result2.put("id", 2);
    queryResults.add(result2);
    
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createAssetItemsAndIdList(message);
    
    final ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
    verify(message).setHeader(eq("temp_AssetItemFacts"), mapCaptor.capture());
    final Map<Integer, AssetItem> capturedMap = (Map<Integer, AssetItem>) mapCaptor.getValue();
    assertEquals("There should be two AssetItems created.", 2, capturedMap.size());
    
    assertTrue("The Map should contain an Asset Item with ID 1.", capturedMap.containsKey(1));
    final AssetItem firstAssetItem = capturedMap.get(1);
    assertEquals("The Asset Item's type should be CHAIN CABLE.", "CHAIN CABLE", firstAssetItem.getAssetItemType());
    assertEquals("The Asset Item's parent Asset should be the Asset held in the temporary header.", 
                 asset, firstAssetItem.getParentAsset());
    
    assertTrue("The Map should contain an Asset Item with ID 2.", capturedMap.containsKey(2));
    final AssetItem secondAssetItem = capturedMap.get(2);
    assertEquals("The Asset Item's type should be LEG.", "LEG", secondAssetItem.getAssetItemType());
    assertEquals("The Asset Item's parent Asset should be Asset held in the temporary header.",
                 asset, secondAssetItem.getParentAsset());
    
    final List<Integer> expectedIds = new ArrayList<>(2);
    expectedIds.add(1);
    expectedIds.add(2);
    verify(message).setHeader("assetItemIds", expectedIds);
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_addAttributesToAssetItems_nullMessage() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = null;
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in a message that does not contain an AssetItem Map header 
   * causes an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addAttributesToAssetItems_noAssetItemMapHeader() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(3);
    result.put("id", 1);
    result.put("attribute_name", "DRIVE TYPE");
    result.put("value_string", "HYDRAULIC MOTOR");
    result.put("data_type", "string");
    queryResults.add(result);
    
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in a message without the FactCollection header causes an IllegalStateException
   * to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addAttributesToAssetItems_noFactCollectionHeader() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(3);
    result.put("id", 3);
    result.put("attribute_name", "DRIVE TYPE");
    result.put("value_string", "HYDRAULIC MOTOR");
    result.put("data_type", "string");
    queryResults.add(result);
    
    final AssetItem firstAssetItem = mock(AssetItem.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(3, firstAssetItem);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in a message with an attribute map that does not contain an id key causes
   * an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addAttributesToAssetItems_noIdKey() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(2);
    result.put("attribute_name", "DRIVE TYPE");
    result.put("value_string", "HYDRAULIC MOTOR");
    result.put("data_type", "string");
    queryResults.add(result);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(0);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in a message with an attribute map that does not contain an attribute_name 
   * key causes an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addAttributesToAssetItems_noAttributeNameKey() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(2);
    result.put("id", 1);
    result.put("value_string", "HYDRAULIC MOTOR");
    result.put("data_type", "string");
    queryResults.add(result);
    
    final AssetItem firstAssetItem = mock(AssetItem.class);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(1, firstAssetItem);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in a message with an attribute map that does not contain a data_type key
   * causes an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_addAttributesToAssetItems_noDataTypeKey() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(2);
    result.put("id", 1);
    result.put("attribute_name", "DRIVE TYPE");
    result.put("value_string", "HYDRAULIC MOTOR");
    queryResults.add(result);
    
    final AssetItem firstAssetItem = mock(AssetItem.class);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(1, firstAssetItem);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
  }
  
  /**
   * Test that passing in query results with no attributes defined means that any Asset Items
   * are unchanged and are added as Facts to the FactCollection.
   */
  @Test
  public void test_addAttributesToAssetItems_noAttributes() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(0);
    
    final Map<String, String> firstAttributesMap = new HashMap<>(0);
    final AssetItem firstAssetItem = mock(AssetItem.class);
    when(firstAssetItem.getAttributes()).thenReturn(firstAttributesMap);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(1, firstAssetItem);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    verify(factCollection).addFact(factCaptor.capture());
    final Fact capturedFact = factCaptor.getValue();
    assertEquals("The new Fact should contain the only Asset Item", firstAssetItem, capturedFact.getFact());
    assertTrue("No attributes should be added to the Asset Item.", firstAttributesMap.isEmpty());
  }
  
  /**
   * Test that multiple attributes for one Asset Item are all added.
   */
  @Test
  public void test_addAttributesToAssetItems_multipleAttributesOneAssetItem() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(2);
    final Map<String, Object> result1 = new HashMap<>(3);
    result1.put("id", 1);
    result1.put("attribute_name", "DRIVE TYPE");
    result1.put("value_string", "HYDRAULIC MOTOR");
    result1.put("data_type", "string");
    queryResults.add(result1);
    final Map<String, Object> result2 = new HashMap<>(3);
    result2.put("id", 1);
    result2.put("attribute_name", "JACKING GEAR PROVIDED");
    result2.put("value_bool", true);
    result2.put("data_type", "boolean");
    queryResults.add(result2);
    
    final Map<String, String> firstAttributesMap = new HashMap<>(2);
    final AssetItem firstAssetItem = mock(AssetItem.class);
    when(firstAssetItem.getAttributes()).thenReturn(firstAttributesMap);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(1, firstAssetItem);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    verify(factCollection).addFact(factCaptor.capture());
    final Fact capturedFact = factCaptor.getValue();
    assertEquals("The new Fact should contain the only Asset Item", firstAssetItem, capturedFact.getFact());
    
    assertEquals("There should be two attributes added to the Asset Item.", 2, firstAttributesMap.size());
    
    assertTrue("There should be an attribute with the name DRIVE TYPE.", firstAttributesMap.containsKey("DRIVE TYPE"));
    assertEquals("The value associated with the DRIVE TYPE attribute should be HYDRAULIC MOTOR.", 
                 "HYDRAULIC MOTOR", firstAttributesMap.get("DRIVE TYPE"));
    
    assertTrue("There should be a JACKING GEAR PROVIDED attribute on the Asset Item.", 
               firstAttributesMap.containsKey("JACKING GEAR PROVIDED"));
    assertEquals("The value associated with the JACKING GEAR PROVIDED attribute should be YES.",
                 "YES", firstAttributesMap.get("JACKING GEAR PROVIDED"));
  }
  
  /**
   * Test that when passing in attributes for different Asset Items that the attributes are added 
   * to the right Asset Items.
   */
  @Test
  public void test_addAttributesToAssetItems_attributesForDifferentItems() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(2);
    final Map<String, Object> result1 = new HashMap<>(3);
    result1.put("id", 1);
    result1.put("attribute_name", "DRIVE TYPE");
    result1.put("value_string", "HYDRAULIC MOTOR");
    result1.put("data_type", "string");
    queryResults.add(result1);
    final Map<String, Object> result2 = new HashMap<>(3);
    result2.put("id", 2);
    result2.put("attribute_name", "JACKING GEAR PROVIDED");
    result2.put("value_bool", true);
    result2.put("data_type", "boolean");
    queryResults.add(result2);
    
    final Map<String, String> firstAttributesMap = new HashMap<>(1);
    final AssetItem firstAssetItem = mock(AssetItem.class);
    when(firstAssetItem.getAttributes()).thenReturn(firstAttributesMap);
    
    final Map<String, String> secondAttributesMap = new HashMap<>(1);
    final AssetItem secondAssetItem = mock(AssetItem.class);
    when(secondAssetItem.getAttributes()).thenReturn(secondAttributesMap);
    
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(2);
    idToAssetItemMap.put(1, firstAssetItem);
    idToAssetItemMap.put(2, secondAssetItem);
    final FactCollection factCollection = mock(FactCollection.class);
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    
    factAdder.addAttributesToAssetItems(message);
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    verify(factCollection, times(2)).addFact(factCaptor.capture());
    final List<Fact> capturedFacts = factCaptor.getAllValues();
    assertEquals("There should be two Facts captured.", 2, capturedFacts.size());
    
    final List<AssetItem> capturedAssetItems = new ArrayList<>(2);
    capturedAssetItems.add((AssetItem)capturedFacts.get(0).getFact());
    capturedAssetItems.add((AssetItem)capturedFacts.get(1).getFact());
    
    assertTrue("One of the Facts should contain the first Asset Item.", capturedAssetItems.contains(firstAssetItem));
    assertTrue("One of the Facts should contain the second Asset Item.", capturedAssetItems.contains(secondAssetItem));
    
    assertEquals("The first Asset Item should have one attribute set.", 1, firstAttributesMap.size());
    assertTrue("The first Asset Item should have a DRIVE TYPE attribute set.", firstAttributesMap.containsKey("DRIVE TYPE"));
    assertEquals("The first Asset Item should have a DRIVE TYPE attribute with a value of HYDRAULIC MOTOR.",
                 "HYDRAULIC MOTOR", firstAttributesMap.get("DRIVE TYPE"));
    
    assertEquals("The second Asset Item should have one attribute set.", 1, secondAttributesMap.size());
    assertTrue("The second Asset Item should have a JACKING GEAR PROVIDED attribute provided.", 
               secondAttributesMap.containsKey("JACKING GEAR PROVIDED"));
    assertEquals("The second Asset Item should have a JACKING GEAR PROVIDED attribute with a values of YES.",
                 "YES", secondAttributesMap.get("JACKING GEAR PROVIDED"));
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_createService_nullMessage() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = null;
    
    factAdder.createService(message);
  }
  
  /**
   * Test that passing in a message with no Services in the query result causes an InvalidInputException
   * to be thrown.
   */
  @Test(expected=InvalidInputException.class)
  public void test_createService_noServices() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(0);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>();
    
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
  }
  
  /**
   * Test that passing in a message with multiple Services in the query result causes an InvalidInputException
   * to be thrown.
   */
  @Test(expected=InvalidInputException.class)
  public void test_createService_multipleServices() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(2);
    final Map<String, Object> result1 = new HashMap<>(1);
    result1.put("code", "SS");
    queryResults.add(result1);
    final Map<String, Object> result2 = new HashMap<>(1);
    result2.put("code", "CSH");
    queryResults.add(result2);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>();
    
    when(message.getHeader("FactCollection")).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
  }
  
  /**
   * Test that passing in a message without a FactCollection causes an IllegalStateException
   * to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createService_noFactCollection() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("code", "SS");
    queryResults.add(result);
    
    final Asset asset = mock(Asset.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>();
    
    when(message.getMandatoryBody()).thenReturn(queryResults);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    
    factAdder.createService(message);
  }
  
  /**
   * Test that passing in a message with no ID to Asset Item map in the header causes 
   * an IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createService_noIdToAssetItemMap() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(0);
    queryResults.add(result);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
  }
  
  /**
   * Test that passing in a result with no code specified for the Service causes an
   * IllegalStateException to be thrown.
   */
  @Test(expected=IllegalStateException.class)
  public void test_createService_noCode() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(0);
    queryResults.add(result);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>();
    
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
  }
  
  /**
   * Test that attempting to build one valid service works and an ItemTasks Fact also gets added.
   */
  @Test
  public void test_createService_oneService() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("code", "SS");
    queryResults.add(result);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>();
    
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    verify(factCollection, times(2)).addFact(factCaptor.capture());
    final List<Fact> capturedFacts = factCaptor.getAllValues();
    assertEquals("There should be two Facts added.", 2, capturedFacts.size());
    
    final Service serviceFact;
    final ItemTasks itemTasksFact;
    
    if(capturedFacts.get(0).getFact() instanceof Service)
    {
      serviceFact = (Service) capturedFacts.get(0).getFact();
      itemTasksFact = (ItemTasks) capturedFacts.get(1).getFact();
    }
    else
    {
      serviceFact = (Service) capturedFacts.get(1).getFact();
      itemTasksFact = (ItemTasks) capturedFacts.get(0).getFact();
    }
    
    assertEquals("The created Service should have a code of SS.", "SS", serviceFact.getServiceCatalogue().getServiceCode());
    
    assertTrue("There should be no Asset Item tasks.", itemTasksFact.getItemTasks().isEmpty());
  }
  
  /**
   * Test that attempting to build one service that is scoped to a particular Asset Item works 
   * and an ItemTasks fact also gets added.
   */
  @Test
  public void test_createService_oneAssetItemService() throws Exception
  {
    final String factCollectionHeaderName = "FactCollection";
    
    final FactAdder factAdder = new FactAdder(factCollectionHeaderName);
    
    final Message message = mock(Message.class);
    
    final List<Map<String, Object>> queryResults = new ArrayList<>(1);
    final Map<String, Object> result = new HashMap<>(1);
    result.put("code", "SS");
    result.put("asset_item_id", 1);
    queryResults.add(result);
    
    final FactCollection factCollection = mock(FactCollection.class);
    final Asset asset = mock(Asset.class);
    final AssetItem assetItem = mock(AssetItem.class);
    final Map<Integer, AssetItem> idToAssetItemMap = new HashMap<>(1);
    idToAssetItemMap.put(1, assetItem);
    
    when(message.getHeader("FactCollection", FactCollection.class)).thenReturn(factCollection);
    when(message.getHeader("temp_AssetFact", Asset.class)).thenReturn(asset);
    when(message.getHeader("temp_AssetItemFacts")).thenReturn(idToAssetItemMap);
    when(message.getMandatoryBody()).thenReturn(queryResults);
    
    factAdder.createService(message);
    
    final ArgumentCaptor<Fact> factCaptor = ArgumentCaptor.forClass(Fact.class);
    verify(factCollection, times(2)).addFact(factCaptor.capture());
    final List<Fact> capturedFacts = factCaptor.getAllValues();
    assertEquals("There should be two Facts added.", 2, capturedFacts.size());
    
    final Service serviceFact;
    final ItemTasks itemTasksFact;
    
    if(capturedFacts.get(0).getFact() instanceof Service)
    {
      serviceFact = (Service) capturedFacts.get(0).getFact();
      itemTasksFact = (ItemTasks) capturedFacts.get(1).getFact();
    }
    else
    {
      serviceFact = (Service) capturedFacts.get(1).getFact();
      itemTasksFact = (ItemTasks) capturedFacts.get(0).getFact();
    }
    
    assertEquals("The created Service should have a code of SS.", "SS", serviceFact.getServiceCatalogue().getServiceCode());
    assertEquals("The created Service should have be scoped to the specified Asset Item.", assetItem, serviceFact.getAssetItem());
    
    assertTrue("There should be no Asset Item tasks.", itemTasksFact.getItemTasks().isEmpty());
  }
}