package com.baesystems.ai.lloydsregister.taskgeneration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FailureResponseTest
{
  /**
   * Test that we can retrieve the given message from the FailureResponse.
   */
  @Test
  public void test_getMessage()
  {
    final String failureMessage = "Could not find an Asset with the given ID.";
    
    final FailureResponse response = new FailureResponse(failureMessage);
    
    final String returnedMessage = response.getMessage();
    
    assertEquals("The returned message should be the same as the given message.", 
      failureMessage, returnedMessage);
  }
  
  /**
   * Test that the response reports a status of FAILURE.
   */
  @Test
  public void test_getStatus()
  {
    final String failureMessage = "Could not find an Asset with the given ID.";
    
    final FailureResponse response = new FailureResponse(failureMessage);
    
    final String returnedStatus = response.getStatus();
    
    assertEquals("The returned status should be FAILURE.", "FAILURE", returnedStatus);
  }
}