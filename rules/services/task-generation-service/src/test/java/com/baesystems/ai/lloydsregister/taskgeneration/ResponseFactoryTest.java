package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Message;

import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.detica.ukdc.drools.service.model.AssetItem;
import com.detica.ukdc.drools.service.model.ItemTasks;
import com.detica.ukdc.drools.service.model.ItemTasks.ServiceAssetItemPair;
import com.detica.ukdc.drools.service.model.Service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class ResponseFactoryTest
{
  /**
   * Test that passing in a null message causes an IllegalArgumentException 
   * to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_createAssetServiceTaskResponse_nullMessage() throws Exception
  {
    final ResponseFactory responseFactory = new ResponseFactory();
    
    final Message message = null;
    
    responseFactory.createAssetServiceTaskResponse(message);
  }
  
  /**
   * Test that we can build a AssetServiceResponse when a valid set of results is 
   * present.
   */
  @Test
  public void test_createAssetServiceTasksResponse_oneAssetItem() throws Exception
  {
    final ResponseFactory responseFactory = new ResponseFactory();
    
    final Message message = mock(Message.class);
    
    final Service service = mock(Service.class);
    final AssetItem assetItem = mock(AssetItem.class);
    when(assetItem.getIdentifier()).thenReturn(1);
    final ServiceAssetItemPair pair = mock(ServiceAssetItemPair.class);
    when(pair.getService()).thenReturn(service);
    when(pair.getAssetItem()).thenReturn(assetItem);
    final List<String> tasks = new ArrayList<>(1);
    tasks.add("EXAMINE");
    final Map<ServiceAssetItemPair, List<String>> itemsToTasks = new HashMap<>(1);
    itemsToTasks.put(pair, tasks);
    
    final ItemTasks itemTasks = mock(ItemTasks.class);
    when(itemTasks.getItemTasks()).thenReturn(itemsToTasks);
    final Map<String, Object> results = new HashMap<>(1);
    results.put("itemTasks", itemTasks);
    final ExecutionResultImpl resultImpl = mock(ExecutionResultImpl.class);
    when(resultImpl.getResults()).thenReturn(results);
    
    when(message.getMandatoryBody(ExecutionResultImpl.class)).thenReturn(resultImpl);
    
    responseFactory.createAssetServiceTaskResponse(message);
    
    final ArgumentCaptor<AssetServiceResponse> responseCaptor = ArgumentCaptor.forClass(AssetServiceResponse.class);
    verify(message).setBody(responseCaptor.capture());
    final AssetServiceResponse response = responseCaptor.getValue();
    
    assertNull("The service identifier should be null.", response.getService().getServiceIdentifier());
    
    final List<AssetItemTasks> assetItemTasks = response.getService().getAssetItems();
    assertEquals("There should be one AssetItemTasks on the response.", 1, assetItemTasks.size());
    
    final AssetItemTasks assetItemTask = assetItemTasks.get(0);
    
    assertEquals("The Asset Item Identifier should be 1.", new Integer(1), assetItemTask.getAssetItemIdentifier());
    
    final List<String> taskList = assetItemTask.getTasks();
    
    assertEquals("There should be one Task for the Asset Item.", 1, taskList.size());
    assertEquals("The task associated with the Asset Item should be EXAMINE", "EXAMINE", taskList.get(0));
  }
  
  /**
   * Test that passing in a null message causes an IllegalArgumentException to be thrown.
   */
  @Test(expected=IllegalArgumentException.class)
  public void test_createFailureResponse_nullMessage() throws Exception
  {
    final ResponseFactory responseFactory = new ResponseFactory();
    
    final Message message = null;
    
    responseFactory.createFailureResponse(message);
  }
  
  /**
   * Test that we can create a FailureResponse if giving a failure message in the ServiceResponse
   * body.
   */
  @Test
  public void test_createFailureResponse() throws Exception
  {
    final ResponseFactory responseFactory = new ResponseFactory();
    
    final String failureMessage = "Unexpected item in the bagging area.";
    
    final ServiceResponse serviceResponse = mock(ServiceResponse.class);
    when(serviceResponse.getMsg()).thenReturn(failureMessage);
    
    final Message message = mock(Message.class);
    when(message.getMandatoryBody(ServiceResponse.class)).thenReturn(serviceResponse);
    
    responseFactory.createFailureResponse(message);
    
    final ArgumentCaptor<FailureResponse> responseCaptor = ArgumentCaptor.forClass(FailureResponse.class);
    verify(message).setBody(responseCaptor.capture());
    final FailureResponse response = responseCaptor.getValue();
    
    assertEquals("The failure message contained in the ServiceResponse instance should be set as the message on the response",
        failureMessage, response.getMessage());
  }
}