package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class AssetServiceTasksTest
{
  /**
   * Test that we can retrieve the given Service Identifier.
   */
  @Test
  public void test_getServiceIdentifier()
  {
    final Integer serviceIdentifier = 2;
    final List<AssetItemTasks> assetItemTasks = new ArrayList<>(1);
    final AssetItemTasks assetItemTask = mock(AssetItemTasks.class);
    assetItemTasks.add(assetItemTask);
    
    final AssetServiceTasks assetServiceTasks = new AssetServiceTasks(serviceIdentifier, assetItemTasks);
    
    final Integer returnedServiceIdentifier = assetServiceTasks.getServiceIdentifier();
    
    assertEquals("The returned Service Identifier should be the same as the given Service Identifier.",
        serviceIdentifier, returnedServiceIdentifier);
  }
  
  /**
   * Test that that we can retrieve the given List of AssetItemTasks.
   */
  @Test
  public void test_getAssetItems()
  {
    final Integer serviceIdentifier = 2;
    final List<AssetItemTasks> assetItemTasks = new ArrayList<>(1);
    final AssetItemTasks assetItemTask = mock(AssetItemTasks.class);
    assetItemTasks.add(assetItemTask);
    
    final AssetServiceTasks assetServiceTasks = new AssetServiceTasks(serviceIdentifier, assetItemTasks);
    
    final List<AssetItemTasks> returnedAssetItems = assetServiceTasks.getAssetItems();
    
    assertEquals("The returned list of AssetItemTasks should contain one entry.", 1, returnedAssetItems.size());
    assertEquals("The returned list of AssetItemTasks should contain the given AssetItemTasks instance",
        assetItemTask, returnedAssetItems.get(0));
  }
}