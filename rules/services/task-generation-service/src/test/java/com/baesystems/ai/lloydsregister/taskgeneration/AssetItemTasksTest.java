package com.baesystems.ai.lloydsregister.taskgeneration;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AssetItemTasksTest
{
  /**
   * Test that we can retrieve the given Asset Item Identifier.
   */
  @Test
  public void test_getAssetItemIdentifier()
  {
    final Integer assetItemIdentifier = 1;
    final List<String> tasks = new ArrayList<>(1);
    tasks.add("Examine");
    
    final AssetItemTasks assetItemTasks = new AssetItemTasks(assetItemIdentifier, tasks);
    
    final Integer returnedIdentifier = assetItemTasks.getAssetItemIdentifier();
    
    assertEquals("The returned Asset Item Identifier should be the same as the given Asset Item Identifier.",
      assetItemIdentifier, returnedIdentifier);
  }
  
  /**
   * Test that we can retrieve the given Tasks List.
   */
  @Test
  public void test_getTasks()
  {
    final Integer assetItemIdentifier = 1;
    final List<String> tasks = new ArrayList<>(1);
    tasks.add("Examine");
    
    final AssetItemTasks assetItemTasks = new AssetItemTasks(assetItemIdentifier, tasks);
    
    final List<String> returnedTasks = assetItemTasks.getTasks();
    
    assertEquals("The returned list should contain one Task.", 1, returnedTasks.size());
    assertEquals("The returned Task should be Examine", "Examine", returnedTasks.get(0));
  }
}