package com.baesystems.ai.lloydsregister.taskgeneration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class AssetServiceResponseTest
{
  /**
   * Test that the status of the response is SUCCESS.
   */
  @Test
  public void test_getStatus()
  {
    final AssetServiceTasks assetServiceTasks = mock(AssetServiceTasks.class);
    
    final AssetServiceResponse response = new AssetServiceResponse(assetServiceTasks);
    
    final String returnedStatus = response.getStatus();
    
    assertEquals("The returned status should be SUCCESS.", "SUCCESS", returnedStatus);
  }
  
  /**
   * Test that we can retrieve the given AssetServiceTasks instance.
   */
  @Test
  public void test_getService()
  {
    final AssetServiceTasks assetServiceTasks = mock(AssetServiceTasks.class);
    
    final AssetServiceResponse response = new AssetServiceResponse(assetServiceTasks);
    
    final AssetServiceTasks returnedService = response.getService();
    
    assertEquals("The returned AssetServiceTasks instance should be the same as the given AssetServiceTasks instance.",
        assetServiceTasks, returnedService);
  }
}