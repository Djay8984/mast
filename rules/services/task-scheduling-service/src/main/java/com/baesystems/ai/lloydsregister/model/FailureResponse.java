package com.baesystems.ai.lloydsregister.model;

/**
 * A failure response for when there is an error - there shouldn't really be any errors in this any way!
 * 
 * @author HMiah2
 *
 */
public class FailureResponse
{
    private final String message;

    public FailureResponse(final String message)
    {
        this.message = message;
    }

    public String getStatus()
    {
        return "failure";
    }

    public String getMessage()
    {
        return this.message;
    }
}
