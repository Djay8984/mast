package com.baesystems.ai.lloydsregister.facts;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.exception.ResultSetException;
import com.detica.ukdc.drools.service.model.CertifiedTaskRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Class to create the facts for the task scheduling rules for a credited task
 *
 * @author HMiah2
 *
 */
public class CertifiedTaskFactAdder extends AbstractFactAdder
{

    public CertifiedTaskFactAdder(String headerName)
    {
        super(headerName);
    }

    /**
     * Create and add the credited task
     * @param message Camel message that contains the results from the SQL
     * query in its body
     * @throws ResultSetException If no results from the database have been returned
     * @throws InvalidPayloadException If there was an error when trying to cast the body of the message
     * as a list of results that have been returned by the database
     */
    public void createAndAddCertifiedTask(final Message message) throws ResultSetException, InvalidPayloadException
    {
        List<Task> listOfCertifiedTasks = new ArrayList<>();

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();

        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No credited tasks have been returned from the database");
        }

        for (Map<String, Object> certifiedTaskRow : queryResults)
        {
            String taskStatus = (String) certifiedTaskRow.get("task_status");

            Calendar completionDate = new GregorianCalendar(timeZone);
            if (certifiedTaskRow.get("completion_date") != null)
            {
                completionDate.setTime((Date) certifiedTaskRow.get("completion_date"));
            }
            else
            {
                completionDate = null;
            }

            Task certifiedTask = new Task();
            certifiedTask.setTaskStatus(taskStatus);
            certifiedTask.setId((Integer) certifiedTaskRow.get("id"));
            certifiedTask.setCompletionDate(completionDate);

            listOfCertifiedTasks.add(certifiedTask);
        }

        message.setHeader("listOfCertifiedTasks", listOfCertifiedTasks);
    }


    /**
     * Create and add the scheduled service that is linked to the tasks
     * @param message Camel message that contains the results from the database in its body
     * @throws ResultSetException If zero or more than one scheduled service has been returned by the database
     * @throws InvalidPayloadException If there was an error when trying to parse the message body as a list of results
     * that have been returned by the database
     */
    public void createAndAddScheduledService(final Message message) throws ResultSetException, InvalidPayloadException
    {
        Service scheduledService = new Service();
        ServiceCatalogue scheduledServiceCatalogue = new ServiceCatalogue();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.size() != 1)
        {
            throw new ResultSetException(
                    String.format("An invalid service id has been provided. The database has returned %d services but was expecting 1",
                            queryResults.size()));
        }

        Map<String, Object> scheduledServiceResult = queryResults.get(0);
        if (scheduledServiceResult.get("due_date") != null && scheduledServiceResult.get("due_date_manual") == null)
        {
            Calendar dueDate = new GregorianCalendar(timeZone);
            dueDate.setTime((Date) scheduledServiceResult.get("due_date"));
            scheduledService.setServiceDueDate(dueDate);
        }
        else if (scheduledServiceResult.get("due_date_manual") != null)
        {
            Calendar dueDate = new GregorianCalendar(timeZone);
            dueDate.setTime((Date) scheduledServiceResult.get("due_date_manual"));
            scheduledService.setServiceDueDate(dueDate);
        }

        scheduledServiceCatalogue.setContinuousIndicator((Boolean) scheduledServiceResult.get("continuous_indicator"));
        scheduledServiceCatalogue.setCyclePeriodicity((Integer) scheduledServiceResult.get("cycle_periodicity"));

        scheduledService.setServiceCatalogue(scheduledServiceCatalogue);

        message.setHeader("scheduledService", scheduledService);
    }

    @Override
    public void makeFacts(final Message message)
    {
        final List<Task> listOfCertifiedTasks = message.getHeader("listOfCertifiedTasks", List.class);
        final Service scheduledService = message.getHeader("scheduledService", Service.class);
        final FactCollection factCollection = message.getHeader("Facts", FactCollection.class);
        for (Task certifiedTask : listOfCertifiedTasks)
        {
            Task associatedTask = new Task();
            associatedTask.setService(scheduledService);
            associatedTask.setId(certifiedTask.getId());
            CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();

            certifiedTaskRelationship.setCertifiedTask(certifiedTask);
            certifiedTaskRelationship.setAssociatedTask(associatedTask);

            final String associatedTaskFactName = String.format("associatedTask%d", certifiedTask.getId());
            final String certifiedTaskRelationshipFactName = String.format("certifiedTaskRelationship%d", certifiedTask.getId());
            final Fact associatedTaskFact = new Fact(associatedTaskFactName, associatedTask);
            final Fact certifiedTaskRelationshipFact = new Fact(certifiedTaskRelationshipFactName, certifiedTaskRelationship);

            factCollection.addFact(associatedTaskFact);
            factCollection.addFact(certifiedTaskRelationshipFact);
        }
    }

}
