package com.baesystems.ai.lloydsregister.model;

import java.util.List;

/**
 * Class that represents a success response that is sent back to the  client
 *
 * @author HMiah2
 *
 */
public class TaskSuccessResponse
{
    private final List<SuccessTask> listOfTasks;

    public TaskSuccessResponse(List<SuccessTask> listOfTasks)
    {
        this.listOfTasks = listOfTasks;
    }

    public String getStatus()
    {
        return "SUCCESS";
    }

    public List<SuccessTask> getTasks()
    {
        return this.listOfTasks;
    }
}
