package com.baesystems.ai.lloydsregister.facts;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.exception.ResultSetException;
import com.detica.ukdc.drools.service.model.CertifiedTaskRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Fact Adder class for tasks that belong to non-continuous services
 *
 * @author HMiah2
 *
 */
public class NonContinuousFactAdder extends AbstractFactAdder
{

    public NonContinuousFactAdder(String headerName)
    {
        super(headerName);
    }

    /**
     * Make the facts for the uncredited tasks
     * @param message Camel message that contains the results of the SQL
     * query in its body
     * @throws ResultSetException If no tasks have been returned by the database
     * @throws InvalidPayloadException If there was an error when trying to cast the message body as a list
     * of results that have been returned by the database
     */
    public void makeNonContinuousFacts(final Message message) throws ResultSetException, InvalidPayloadException
    {
        final String taskIdString = message.getHeader("taskIds", String.class);

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        List<CertifiedTaskRelationship> listOfCertifiedTasks = new ArrayList<>();

        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No tasks have been returned by the database");
        }
        String[] taskIds = taskIdString.split(",");
        for (int i = 0; i < taskIds.length; i++)
        {
            int taskId = Integer.parseInt(taskIds[i]);
            Map<String, Object> serviceResult = queryResults.get(i);

            Calendar dueDate = new GregorianCalendar(timeZone);
            if (serviceResult.get("due_date") != null)
            {
                dueDate.setTime((Date) serviceResult.get("due_date"));
            }
            else
            {
                dueDate = null;
            }

            Service service = new Service();
            service.setServiceDueDate(dueDate);

            Task associatedTask = new Task();
            associatedTask.setId(taskId);
            associatedTask.setService(service);

            CertifiedTaskRelationship certifiedTaskRelationship = new CertifiedTaskRelationship();
            certifiedTaskRelationship.setAssociatedTask(associatedTask);

            listOfCertifiedTasks.add(certifiedTaskRelationship);
        }

        message.setHeader("listOfCertifiedTasks", listOfCertifiedTasks);
    }

    @Override
    public void makeFacts(final Message message)
    {
        final List<CertifiedTaskRelationship> listOfCertifiedTasks = message.getHeader("listOfCertifiedTasks", List.class);
        final FactCollection factCollection = message.getHeader(headerName, FactCollection.class);
        for (CertifiedTaskRelationship certifiedTaskRelationship : listOfCertifiedTasks)
        {
            final Task associatedTask = certifiedTaskRelationship.getAssociatedTask();
            final String associatedTaskFactName = String.format("associatedTask%d", associatedTask.getId());
            final String certifiedTaskRelationshipFactName = String.format("certifiedTaskRelationship%d", associatedTask.getId());

            Fact associatedTaskFact = new Fact(associatedTaskFactName, associatedTask);
            Fact certifiedTaskRelationshipTaskFact = new Fact(certifiedTaskRelationshipFactName, certifiedTaskRelationship);

            factCollection.addFact(associatedTaskFact);
            factCollection.addFact(certifiedTaskRelationshipTaskFact);
        }
    }

}
