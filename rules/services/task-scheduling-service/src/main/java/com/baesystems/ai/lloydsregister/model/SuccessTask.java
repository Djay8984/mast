package com.baesystems.ai.lloydsregister.model;

/**
 * POJO object to describe a task that is returned in a success task response
 * 
 * @author HMiah2
 *
 */
public class SuccessTask
{
    private final int id;
    private final String assignedDate;
    private final String dueDate;

    public SuccessTask(int id, String assignedDate, String dueDate)
    {
        this.id = id;
        this.assignedDate = assignedDate;
        this.dueDate = dueDate;
    }

    public int getId()
    {
        return id;
    }

    public String getAssignedDate()
    {
        if (assignedDate.isEmpty())
        {
            return null;
        }
        return assignedDate;
    }

    public String getDueDate()
    {
        if (dueDate.isEmpty())
        {
            return null;
        }
        return dueDate;
    }

}
