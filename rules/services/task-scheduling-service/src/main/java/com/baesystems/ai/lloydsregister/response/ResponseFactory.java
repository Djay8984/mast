package com.baesystems.ai.lloydsregister.response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.model.FailureResponse;
import com.baesystems.ai.lloydsregister.model.SuccessTask;
import com.baesystems.ai.lloydsregister.model.TaskSuccessResponse;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Create a response that will be sent back to the  client
 *
 * @author HMiah2
 *
 */
public class ResponseFactory
{
    private static final String PATTERN = "yyyy-MM-dd";

    private DroolsXStreamMarshaller marshaller;

    public ResponseFactory()
    {
        marshaller = new DroolsXStreamMarshaller();
    }


    /**
     * Make a success response that is sent back to the client
     * @param message Camel message that contains the response from the execution server
     * @return A Task Success response
     * @throws ExecutionServerException If there was an error when trying to parse the response from the
     * server
     * @throws InvalidPayloadException If there was an error when trying to parse the response from the server
     */
    public TaskSuccessResponse makeTaskSuccessReponse(final Message message) throws ExecutionServerException, InvalidPayloadException
    {
        String serviceResponse = message.getMandatoryBody(String.class);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN);
        List<SuccessTask> listOfTasks = new ArrayList<>();

        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);
        if (response.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when getting the response from the KIE Execution Server");
        }

        ExecutionResultImpl executionResults = marshaller.unmarshalFromXml(response.getResult(), ExecutionResultImpl.class);
        Map<String, Object> results = executionResults.getResults();

        Collection<Object> resultValues = results.values();
        for (Object result : resultValues)
        {
            if (result instanceof Task)
            {
                Task task = (Task) result;

                String assignedDate = "";
                if (task.getAssignedDate() != null)
                {
                    assignedDate = simpleDateFormat.format(task.getAssignedDate().getTime());
                }

                String dueDate = "";
                if (task.getDueDate() != null)
                {
                    dueDate = simpleDateFormat.format(task.getDueDate().getTime());
                }
                listOfTasks.add(new SuccessTask(task.getId(), assignedDate, dueDate));
            }
        }
        return new TaskSuccessResponse(listOfTasks);
    }

    /**
     * Create a response for when there is an error
     * @param message The camel message containing the error message in the body
     * @return A Failure response
     * @throws InvalidPayloadException If there was an error when trying to cast the
     * message body to a String
     */
    public FailureResponse makeFailureResponse(final Message message) throws InvalidPayloadException
    {
        final String serviceResponse = message.getMandatoryBody(String.class);
        System.out.println("message: " + serviceResponse);

        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);

        return new FailureResponse(response.getMsg());
    }

}
