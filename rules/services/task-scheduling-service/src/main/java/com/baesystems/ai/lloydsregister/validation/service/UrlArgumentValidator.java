package com.baesystems.ai.lloydsregister.validation.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.exception.ValidationException;

/**
 * Validation Service to validate the arguments that are passed in the URL.
 *
 * @author HMiah2
 *
 */
public class UrlArgumentValidator
{

    // Regular expression that matches the task ids (digits only) that are passed
    // in the URL in a comma separated values format.
    private static final String csvRegularExpression = "^\\d+(,*\\d+)*(([\\d+])?)$";
    private static final Pattern pattern = Pattern.compile(csvRegularExpression);

    // Header value to validate against
    private final String headerKey;

    public UrlArgumentValidator(String headerKey)
    {
        this.headerKey = headerKey;
    }

    /**
     * Validate the the specific header that is passed in the URL
     *
     * @param message
     * @throws ValidationException
     */
    public void validateHeader(final Message message) throws ValidationException
    {
        String headerArg = message.getHeader(headerKey, String.class);
        if (headerArg == null || headerArg.isEmpty() || headerArg.equals(" "))
        {
            throw new ValidationException(String.format("The header key %s is missing from the URL.", headerKey));
        }
    }

    /**
     * Validates that the specific header matches the CSV regular expression.
     *
     * @param message
     * @throws ValidationException
     */
    public void validateCSVFormat(final Message message) throws ValidationException
    {
        final String taskIds = message.getHeader(headerKey, String.class);
        Matcher matcher = pattern.matcher(taskIds);
        if (!matcher.find())
        {
            throw new ValidationException("The URL does not specify the arguments in a CSV format- or it contains alphanumeric elements");
        }
    }

}
