package com.baesystems.ai.lloydsregister.response;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.facts.Fact;
import com.baesystems.ai.lloydsregister.facts.FactCollection;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.CertifiedTaskRelationship;
import com.detica.ukdc.drools.service.model.Task;

/**
 * Class to replace the task fact when processing the request from the assigned date and service due date rule sets.
 *
 * @author HMiah
 *
 */
public class ReplaceTask
{
    private final String factHeaderName;
    private DroolsXStreamMarshaller marshaller;

    public ReplaceTask(String factHeaderName)
    {
        this.factHeaderName = factHeaderName;
        this.marshaller = new DroolsXStreamMarshaller();
    }

    /**
     * Replace the task facts from the response that we get back from KIE Execution server
     *
     * @param message Camel message that contains the response from the Execution server
     * @throws ExecutionServerException When there is an error when trying to parse the response
     * from the execution server
     * @throws InvalidPayloadException If there was an error when trying to cast the
     * message body - which contains the response from the server - to a String
     */
    public void replaceTask(final Message message) throws ExecutionServerException, InvalidPayloadException
    {
        final String serviceReponse = message.getMandatoryBody(String.class);
        final Map<String, Object> results = _unmarshallResponse(serviceReponse);

        final FactCollection factCollection = message.getHeader(factHeaderName, FactCollection.class);
        List<Fact> listOfFacts = factCollection.getFacts();

        ListIterator<Fact> factsIterator = listOfFacts.listIterator();
        while (factsIterator.hasNext())
        {
            Fact fact = factsIterator.next();
            String factName = fact.getFactName();
            Object responseObject = results.get(factName);
            if (responseObject instanceof Task)
            {
                Task associatedTask = (Task) responseObject;
                fact = new Fact(factName, associatedTask);
                factsIterator.set(fact);
            }
            else if (responseObject instanceof CertifiedTaskRelationship)
            {
                CertifiedTaskRelationship certifiedTaskRelationship = (CertifiedTaskRelationship) responseObject;
                fact = new Fact(factName, certifiedTaskRelationship);
                factsIterator.set(fact);
            }
        }
        factCollection.setFacts(listOfFacts);
    }

    /**
     * Utility method to help with the processing of the response
     *
     * @param serviceResponse A String representation of the response from Execution server
     * @return A Service object
     * @throws ExecutionServerException If there was an error when trying to parse
     * the response from the server
     */
    private Map<String, Object> _unmarshallResponse(String serviceResponse) throws ExecutionServerException
    {
        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);

        if (response.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when trying to unmarshall the response from the KIE Execution Server");
        }

        Map<String, Object> results = (marshaller.unmarshalFromXml(response.getResult(), ExecutionResultImpl.class)).getResults();

        return results;
    }

}
