package org.drools.core.command.runtime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.impl.KnowledgeCommandContext;
import org.kie.api.runtime.KieSession;
import org.kie.internal.command.Context;

/**
 * Class that is got from the github repo of Drools. This command should be used to tell Execution Server to dispose a
 * session once it is done processing it
 * 
 * @author HMiah2
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class DisposeCommand implements GenericCommand<Void>
{

    public DisposeCommand()
    {

    }

    public Void execute(Context context)
    {
        KieSession ksession = ((KnowledgeCommandContext) context).getKieSession();
        ksession.dispose();
        return null;
    }

    public String toString()
    {
        return "ksession.dispose();";
    }
}
