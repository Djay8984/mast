package com.baesystems.ai.lloydsregister.response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.model.AssociatedService;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceWrapper;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.Service;

/**
 * Process the response from calling the associated services container in execution server
 *
 * @author HMiah2
 *
 */
public class AssociatedServicesResponse
{
    private static final String pattern = "yyyy-MM-dd";

    private DroolsXStreamMarshaller marshaller;

    public AssociatedServicesResponse()
    {
        marshaller = new DroolsXStreamMarshaller();
    }

    /**
     * Process the response from the execution server when
     * calling the associated services ruleset
     * @param message This is the original camel message that contains the
     * response from the server in its body
     * @return A associated services wrapper which contains a scheduled service
     * with is service due dates populated
     * @throws InvalidPayloadException If there is an error when trying to cast the message
     * body as a String that contains the response from the Execution Server
     * @throws ExecutionServerException If there is an error when trying to parse the response from the execution
     * server
     */
    public AssociatedServiceWrapper processResponse(final Message message) throws InvalidPayloadException, ExecutionServerException
    {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Service certifiedService = message.getHeader("serviceScheduling_certifiedService", Service.class);
        final String serviceResponse = message.getMandatoryBody(String.class);

        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);
        if (response.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when trying to parse the message from the KIE Execution Server");
        }
        ExecutionResultImpl results = marshaller.unmarshalFromXml(response.getResult(), ExecutionResultImpl.class);
        Service service = (Service) results.getResults().get("service");

        String assignDate = "";
        if (service.getAssignedDate() != null)
        {
            Date assignedDate = service.getAssignedDate().getTime();
            assignDate = simpleDateFormat.format(assignedDate);
        }

        String dueDate = "";
        if (service.getServiceDueDate() != null)
        {
            Date serviceDueDate = service.getServiceDueDate().getTime();
            dueDate = simpleDateFormat.format(serviceDueDate);
        }

        String lowerRangeDate = "";
        if (service.getLowerRangeDate() != null)
        {
            Date serviceLowerRangeDate = service.getLowerRangeDate().getTime();
            lowerRangeDate = simpleDateFormat.format(serviceLowerRangeDate);
        }

        String upperRangeDate = "";
        if (service.getUpperRangeDate() != null)
        {
            Date serviceUpperRangeDate = service.getUpperRangeDate().getTime();
            upperRangeDate = simpleDateFormat.format(serviceUpperRangeDate);
        }

        Integer occurenceNumber = certifiedService.getServiceOccurenceNumber();
        int serviceCatalogueIdentifier = service.getServiceCatalogue().getIdentifier();
        int serviceSchedulingRegime = Integer.parseInt(message.getHeader("serviceScheduling_schedulingRegimeId", String.class));

        AssociatedService aService = new AssociatedService(serviceCatalogueIdentifier, serviceSchedulingRegime,
                occurenceNumber, assignDate, dueDate, lowerRangeDate, upperRangeDate);

        List<AssociatedService> services = new ArrayList<>();
        services.add(aService);

        AssociatedServiceWrapper wrapper = new AssociatedServiceWrapper(services);

        final Byte originalIndicator = (Byte) message.getHeader("serviceScheduling_original_hull_indicator");
        if (service.getAsset().getHullIndicator() != null && !service.getAsset().getHullIndicator().equals(originalIndicator))
        {
            wrapper.setHullIndicator(service.getAsset().getHullIndicator());
        }

        return wrapper;
    }

}
