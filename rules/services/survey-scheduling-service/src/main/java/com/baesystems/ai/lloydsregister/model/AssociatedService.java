package com.baesystems.ai.lloydsregister.model;

/**
 * Object that gets returned in our response to MAST
 * 
 * @author HMiah2
 *
 */
public class AssociatedService
{
    private int serviceCatalogueIdentifier;
    private int serviceSchedulingRegime;
    private Integer occurenceNumber;
    private String assignedDate;
    private String dueDate;
    private String lowerRangeDate;
    private String upperRangeDate;

    public AssociatedService(int serviceCatalogueIdentifier, int serviceSchedulingRegime, Integer occurenceNumber,
            String assignedDate, String dueDate, String lowerRangeDate, String upperRangeDate)
    {
        this.serviceCatalogueIdentifier = serviceCatalogueIdentifier;
        this.serviceSchedulingRegime = serviceSchedulingRegime;
        this.occurenceNumber = occurenceNumber;
        this.assignedDate = assignedDate;
        this.dueDate = dueDate;
        this.lowerRangeDate = lowerRangeDate;
        this.upperRangeDate = upperRangeDate;
    }

    public int getServiceCatalogueIdentifier()
    {
        return serviceCatalogueIdentifier;
    }

    public int getSchedulingRegime()
    {
        return serviceSchedulingRegime;
    }

    public Integer getOccurenceNumber()
    {
        return occurenceNumber;
    }

    public String getAssignedDate()
    {
        if (assignedDate.isEmpty())
        {
            return null;
        }
        return assignedDate;
    }

    public String getDueDate()
    {
        if (dueDate.isEmpty())
        {
            return null;
        }
        return dueDate;
    }

    public String getLowerRangeDate()
    {
        if (lowerRangeDate.isEmpty())
        {
            return null;
        }
        return lowerRangeDate;
    }

    public String getUpperRangeDate()
    {
        if (upperRangeDate.isEmpty())
        {
            return null;
        }
        return upperRangeDate;
    }

}
