package com.baesystems.ai.lloydsregister.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Success response for manual entry services
 * 
 * @author HMiah2
 *
 */
public class UncreditedSuccessResponse
{
    private final AssociatedService service;

    @JsonInclude(Include.NON_NULL)
    private final Byte hullIndicator;

    public UncreditedSuccessResponse(AssociatedService service, Byte hullIndicator)
    {
        this.service = service;
        this.hullIndicator = hullIndicator;
    }

    public String getStatus()
    {
        return "SUCCESS";
    }

    public AssociatedService getService()
    {
        return service;
    }

    public Byte getHullIndicator()
    {
        return hullIndicator;
    }

}
