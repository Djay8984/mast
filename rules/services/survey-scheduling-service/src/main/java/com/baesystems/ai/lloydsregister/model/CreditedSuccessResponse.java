package com.baesystems.ai.lloydsregister.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The certified service success response
 * 
 * @author HMiah2
 *
 */
public class CreditedSuccessResponse
{
    private final List<AssociatedService> services;

    @JsonInclude(Include.NON_NULL)
    private final Byte hullIndicator;

    public CreditedSuccessResponse(final List<AssociatedService> services, Byte hullIndicator)
    {
        this.services = services;
        this.hullIndicator = hullIndicator;
    }

    public String getStatus()
    {
        return "SUCCESS";
    }

    public List<AssociatedService> getServices()
    {
        return this.services;
    }

    public Byte getHullIndicator()
    {
        return this.hullIndicator;
    }

}
