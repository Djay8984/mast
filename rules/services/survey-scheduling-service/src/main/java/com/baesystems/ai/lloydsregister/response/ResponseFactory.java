package com.baesystems.ai.lloydsregister.response;

import java.text.SimpleDateFormat;
import java.util.Collections;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.model.AssociatedService;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceWrapper;
import com.baesystems.ai.lloydsregister.model.CreditedSuccessResponse;
import com.baesystems.ai.lloydsregister.model.FailureResponse;
import com.baesystems.ai.lloydsregister.model.UncreditedSuccessResponse;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.Service;

/**
 * Make all the different types of responses and
 * return a suitable object to be returned to the MAST client
 *
 * @author HMiah2
 *
 */
public class ResponseFactory
{
    private static final String PATTERN = "yyyy-MM-dd";

    private DroolsXStreamMarshaller marshaller;

    public ResponseFactory()
    {
        marshaller = new DroolsXStreamMarshaller();
    }

    /**
     * Make a credited success response
     * @param wrapper A wrapper object that contains a list of associated services
     * @return A credited success response that will be sent back to the client. This response
     * contains a list of associated services (if there are any) and the hull indicator (if this has changed)
     */
    public CreditedSuccessResponse makeCreditedSuccessResponse(final AssociatedServiceWrapper wrapper)
    {
        if (wrapper == null)
        {
            //This is the case when no associated services have been returned by
            //the Define Associated Services container in the execution server
            return new CreditedSuccessResponse(Collections.<AssociatedService>emptyList(), null);
        }
        return new CreditedSuccessResponse(wrapper.getServices(), wrapper.getHullIndicator());
    }

    /**
     * Make a response that is used for the manual entry endpoint - the input is an uncredited service
     * @param message Camel message that contains the response from the server in its body
     * @return A uncredited success response to the client, containing the scheduling information
     * for the service
     * @throws ExecutionServerException If there was an error when trying to parse the response from
     * server
     */
    public UncreditedSuccessResponse makeUncreditedSuccessResponse(final Message message) throws ExecutionServerException
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN);
        String serviceResponseString = message.getBody(String.class);

        ServiceResponse serviceResponse = marshaller.unmarshalFromXml(serviceResponseString, ServiceResponse.class);
        if (serviceResponse.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when trying to parse the response from the KIE Execution Server");
        }
        ExecutionResultImpl executionResults = marshaller.unmarshalFromXml(serviceResponse.getResult(), ExecutionResultImpl.class);
        Service manualService = (Service) executionResults.getResults().get("service");
        int serviceCatalogueIdentifier = message.getHeader("serviceScheduling_serviceCatalogueIdentifier", Integer.class);
        int serviceSchedulingRegime = message.getHeader("serviceScheduling_schedulingRegimeId", Integer.class);
        Integer occurenceNumber = message.getHeader("serviceScheduling_occurenceNumber", Integer.class);

        String assignDate = "";
        if (manualService.getAssignedDate() != null)
        {
            assignDate = simpleDateFormat.format(manualService.getAssignedDate().getTime());
        }

        String dueDate = "";
        if (manualService.getServiceDueDate() != null)
        {
            dueDate = simpleDateFormat.format(manualService.getServiceDueDate().getTime());
        }

        String lowerRangeDate = "";
        if (manualService.getLowerRangeDate() != null)
        {
            lowerRangeDate = simpleDateFormat.format(manualService.getLowerRangeDate().getTime());
        }

        String upperRangeDate = "";
        if (manualService.getUpperRangeDate() != null)
        {
            upperRangeDate = simpleDateFormat.format(manualService.getUpperRangeDate().getTime());
        }

        // Set hull indicator to null if not changed so it is not included in the response.
        final Byte hullIndicator;
        if (manualService.getAsset().getHullIndicator() == null ||
                manualService.getAsset().getHullIndicator().equals(message.getHeader("serviceScheduling_hull_indicator_original")))
        {
            hullIndicator = null;
        }
        else
        {
            hullIndicator = manualService.getAsset().getHullIndicator();
        }

        AssociatedService associatedService = new AssociatedService(serviceCatalogueIdentifier, serviceSchedulingRegime,
                occurenceNumber, assignDate, dueDate, lowerRangeDate, upperRangeDate);

        return new UncreditedSuccessResponse(associatedService, hullIndicator);
    }

    /**
     * Make a failure response to send back to the client
     * @param message The camel message that contains the error message that
     * was returned by the Execution Server
     * @return A failure response to be sent back to the client
     * @throws InvalidPayloadException If there was an error when trying to cast the
     * message body as a String to be processed further
     */
    public FailureResponse makeFailureResponse(final Message message) throws InvalidPayloadException
    {
        final String serviceResponse = message.getMandatoryBody(String.class);

        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);

        return new FailureResponse(response.getMsg());
    }
}
