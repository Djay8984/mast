package com.baesystems.ai.lloydsregister.facts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.cache.CacheManager;
import com.baesystems.ai.lloydsregister.exception.ResultSetException;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.CertifiedRelationship;
import com.detica.ukdc.drools.service.model.HullSurvey;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * Fact Adder for Manual Entry Services
 *
 * @author HMiah2
 *
 */
public class ManualEntryFactAdder extends AbstractFactAdder
{

    public ManualEntryFactAdder(String headerName)
    {
        super(headerName);
    }

    /**
     * Create and add the service for the manual entry service
     * @param message The original camel message that contains the results from the SQL query in its body
     * @throws ResultSetException This exception gets thrown if zero or more than one scheduled service has been returned
     * from the database
     * @throws InvalidPayloadException If there is an error when trying to cast the camel
     * message body as a list of results from the database
     */
    public void createAndAddService(final Message message) throws ResultSetException, InvalidPayloadException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);

        Service service = new Service();
        service.setScenarioNumber(message.getHeader("scenarioNumber", Integer.class));

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.size() != 1)
        {
            throw new ResultSetException("An invalid ID was entered for the scheduled service as no "
                    + "scheduled services were returned from the database or more than one scheduled service has been returned"
                    + "from the database");
        }
        Map<String, Object> serviceRow = queryResults.get(0);
        service.setCyclePeriodicity((Integer) serviceRow.get("cycle_periodicity"));

        if (serviceRow.get("assigned_date_manual") != null)
        {
            Calendar assignedDateManual = new GregorianCalendar(timeZone);
            assignedDateManual.setTime((Date) serviceRow.get("assigned_date_manual"));
            service.setAssignedDate(assignedDateManual);
        }

        if (serviceRow.get("due_date_manual") != null)
        {
            Calendar dueDateManual = new GregorianCalendar(timeZone);
            dueDateManual.setTime((Date) serviceRow.get("due_date_manual"));
            service.setServiceDueDate(dueDateManual);
        }

        if (serviceRow.get("lower_range_date_manual") != null)
        {
            Calendar lowerRangeDateManual = new GregorianCalendar(timeZone);
            lowerRangeDateManual.setTime((Date) serviceRow.get("lower_range_date_manual"));
            service.setLowerRangeDate(lowerRangeDateManual);
        }

        if (serviceRow.get("upper_range_date_manual") != null)
        {
            Calendar upperRangeDateManual = new GregorianCalendar(timeZone);
            upperRangeDateManual.setTime((Date) serviceRow.get("upper_range_date_manual"));
            service.setUpperRangeDate(upperRangeDateManual);
        }

        message.setHeader(serviceKey, service);
    }

    /**
     * Create and add the service catalogue that is associated with the manual
     * entry service. As this method uses the cache (and it is the first method within the manual entry route to use such a
     * cache from the cache manager) it will wait for the cache manager count down to reach zero before continuing. This acts as a
     * precaution to make sure the cache has seen populated before it is used.
     * @param message This is the original camel message to set the headers of
     * @throws InterruptedException Exception that gets thrown when there is an error while waiting for the
     * count down to reach zero
     */
    public void createAndAddServiceCatalogue(final Message message) throws InterruptedException
    {
        CacheManager.getCountDownLatch().await();
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Integer serviceCatalogueId = message.getHeader("serviceScheduling_serviceCatalogueIdentifier", Integer.class);
        Service manualEntryService = message.getHeader(serviceKey, Service.class);
        Map<Integer, ServiceCatalogue> serviceCatalogueCache = CacheManager.getServiceCatalogueCache();
        Map<String, Integer> serviceTypeCache = CacheManager.getServiceTypeCache();
        Map<String, Integer> serviceGroupCache = CacheManager.getServiceGroupCache();
        Map<String, Integer> schedulingRegimeCache = CacheManager.getSchedulingRegimeCache();
        ServiceCatalogue serviceCatalogue = serviceCatalogueCache.get(serviceCatalogueId);
        manualEntryService.setServiceCatalogue(serviceCatalogue);
        manualEntryService.setSchedulingTypeAssign(serviceCatalogue.getSchedulingTypeAssign());

        message.setHeader("serviceScheduling_serviceTypeId", serviceTypeCache.get(serviceCatalogue.getServiceType()));
        message.setHeader("serviceScheduling_serviceGroupId", serviceGroupCache.get(serviceCatalogue.getServiceGroup()));
        message.setHeader("serviceScheduling_schedulingRegimeName", serviceCatalogue.getSchedulingRegime());
        message.setHeader("serviceScheduling_schedulingRegimeId", schedulingRegimeCache.get(serviceCatalogue.getSchedulingRegime()));
    }

    /**
     * Create and add the asset that is associated with this manual entry service
     * @param message The original camel message that contains the results of the SQL query in
     * its body.
     * @throws ResultSetException If more than one asset is returned for the scheduled service
     * @throws InvalidPayloadException If there is an error when trying to cast the message body as a list
     * of results that have been returned by the database
     * @throws ParseException If there is an error when trying to parse the harmonisation date of the
     * asset
     */
    public void createAndAddAsset(final Message message) throws ResultSetException, InvalidPayloadException, ParseException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);

        Service service = message.getHeader(serviceKey, Service.class);

        Asset asset = new Asset();

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.size() == 0)
        {
            service.setAsset(null);
            return;
        }

        if (queryResults.size() > 1)
        {
            throw new ResultSetException(
                    "Greater than one Asset was returned: " + queryResults.size());
        }

        if (message.getHeader("assetHarmonisationDate") != null)
        {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            simpleDateFormat.setLenient(false);
            Calendar harmonisationDate = new GregorianCalendar(timeZone);
            String date = message.getHeader("assetHarmonisationDate", String.class);
            harmonisationDate.setTime(simpleDateFormat.parse(date));
            asset.setHarmonisationDate(harmonisationDate);
        }

        Map<String, Object> assetRow = queryResults.get(0);

        if (assetRow.get("commission_date") != null)
        {
            Calendar commissionDate = new GregorianCalendar(timeZone);
            commissionDate.setTime((Date) assetRow.get("commission_date"));
            asset.setCommissionDate(commissionDate);
        }

        if (assetRow.get("build_date") != null)
        {
            Calendar buildDate = new GregorianCalendar(timeZone);
            buildDate.setTime((Date) assetRow.get("build_date"));
            asset.setBuildDate(buildDate);
        }

        if (assetRow.get("hull_indicator") != null)
        {
            asset.setHullIndicator(((Integer) assetRow.get("hull_indicator")).byteValue());
            message.setHeader("serviceScheduling_hull_indicator_original", asset.getHullIndicator());
        }
        asset.setClassStatus((String) assetRow.get("class_status"));
        asset.setGrossTonnage((Double) assetRow.get("gross_tonnage"));
        asset.setAssetType((String) assetRow.get("level_5_code"));

        service.setAsset(asset);
    }

    /**
     * Create and add the renewal service that is associated with this manual entry service
     * @param message The original camel message that contains the results of the SQL query in its body
     * @throws ResultSetException If more than one renewal service has been returned
     * @throws InvalidPayloadException If there is an error when trying to cast the message body as a list
     * of results that have been returned by the database
     */
    public void createAndAddRenewalService(final Message message) throws ResultSetException, InvalidPayloadException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service associatedService = message.getHeader(serviceKey, Service.class);
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Service renewalService = new Service();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            renewalRelationship.setRenewalService(null);
            renewalRelationship.setAssociatedService(associatedService);
            message.setHeader("serviceScheduling_renewalService", renewalRelationship);
            return;
        }
        if (queryResults.size() > 1)
        {
            throw new ResultSetException(
                    "Greater than one Renewal Service was returned: " + queryResults.size());
        }
        Map<String, Object> renewalServiceRow = queryResults.get(0);
        if (renewalServiceRow.get("due_date") != null && renewalServiceRow.get("due_date_manual") == null)
        {
            Calendar dueDate = new GregorianCalendar(timeZone);
            dueDate.setTime((Date) renewalServiceRow.get("due_date"));
            renewalService.setServiceDueDate(dueDate);
        }
        else if (renewalServiceRow.get("due_date_manual") != null)
        {
            Calendar dueDateManual = new GregorianCalendar(timeZone);
            dueDateManual.setTime((Date) renewalServiceRow.get("due_date_manual"));
            renewalService.setServiceDueDate(dueDateManual);
        }

        if (renewalServiceRow.get("assigned_date") != null && renewalServiceRow.get("assigned_date_manual") == null)
        {
            Calendar assignedDate = new GregorianCalendar(timeZone);
            assignedDate.setTime((Date) renewalServiceRow.get("assigned_date"));
            renewalService.setAssignedDate(assignedDate);
        }
        else if (renewalServiceRow.get("assigned_date_manual") != null)
        {
            Calendar assignedDateManual = new GregorianCalendar(timeZone);
            assignedDateManual.setTime((Date) renewalServiceRow.get("assigned_date_manual"));
            renewalService.setAssignedDate(assignedDateManual);
        }

        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        renewalServiceCatalogue.setCyclePeriodicity((Integer) renewalServiceRow.get("cycle_periodicity"));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);
        renewalService.setCyclePeriodicity((Integer) renewalServiceRow.get("service_cycle_periodicity"));

        renewalRelationship.setAssociatedService(associatedService);
        renewalRelationship.setRenewalService(renewalService);

        message.setHeader("serviceScheduling_renewalService", renewalRelationship);
    }

    /**
     * Create and add the hull survey that is associated with this manual entry
     * service
     * @param message The original camel message that contains the results from the SQL
     * query in its body
     * @throws InvalidPayloadException If there is an error when trying to cast
     * the message body as a list of results that have been returned by the database
     */
    public void createAndAddHullSurvey(final Message message) throws InvalidPayloadException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service associatedService = message.getHeader(serviceKey, Service.class);

        HullSurvey hullSurvey = new HullSurvey();

        Service hullService = new Service();

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            hullSurvey.setHullSurvey(null);
            hullSurvey.setAssociatedService(associatedService);
            message.setHeader("serviceScheduling_hullSurvey", hullSurvey);
            return;
        }
        Map<String, Object> hullSurveyRow = queryResults.get(0);

        ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
        hullServiceCatalogue.setContinuousIndicator((Boolean) hullSurveyRow.get("continuous_indicator"));

        hullServiceCatalogue.setCyclePeriodicity((Integer) hullSurveyRow.get("cycle_periodicity"));
        hullService.setCyclePeriodicity((Integer) hullSurveyRow.get("service_cycle_periodicity"));
        hullService.setServiceCatalogue(hullServiceCatalogue);
        hullSurvey.setAssociatedService(associatedService);
        hullSurvey.setHullSurvey(hullService);
        message.setHeader("serviceScheduling_hullSurvey", hullSurvey);
    }

    /**
     * Create and add the credited service. A manual entry service does not
     * have a credited service so this field will be null in the credited relationship.
     * @param message The original camel message to set the headers of.
     */
    public void createAndAddCertifiedRelationship(final Message message)
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service associcatedService = message.getHeader(serviceKey, Service.class);

        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();

        certifiedRelationship.setAssociatedService(associcatedService);

        message.setHeader("serviceScheduling_certifiedRelationship", certifiedRelationship);
    }

    @Override
    public void makeFacts(Message message)
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        FactCollection facts = message.getHeader(this.headerName, FactCollection.class);

        Service manualService = message.getHeader(serviceKey, Service.class);
        HullSurvey hullSurvey = message.getHeader("serviceScheduling_hullSurvey", HullSurvey.class);
        RenewalRelationship renewalRelationship = message.getHeader("serviceScheduling_renewalService", RenewalRelationship.class);
        CertifiedRelationship certifiedRelationship = message.getHeader("serviceScheduling_certifiedRelationship", CertifiedRelationship.class);

        Fact manualServiceFact = new Fact("service", manualService);
        Fact hullSurveyFact = new Fact("hullSurvey", hullSurvey);
        Fact renewalFact = new Fact("renewalRelationshipFact", renewalRelationship);
        Fact certifiedRelationshipFact = new Fact("certifiedService", certifiedRelationship);

        facts.addFact(manualServiceFact);
        facts.addFact(hullSurveyFact);
        facts.addFact(renewalFact);
        facts.addFact(certifiedRelationshipFact);
    }

}
