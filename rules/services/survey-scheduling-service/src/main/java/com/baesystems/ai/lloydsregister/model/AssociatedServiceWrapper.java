package com.baesystems.ai.lloydsregister.model;

import java.util.List;

/**
 * Wrapper class to hold the hull indicator and the list of services that are returned to MAST
 * 
 * @author HMiah2
 *
 */
public class AssociatedServiceWrapper
{
    private List<AssociatedService> services;
    private Byte hullIndicator;

    public AssociatedServiceWrapper(List<AssociatedService> services)
    {
        this.services = services;
    }

    public List<AssociatedService> getServices()
    {
        return services;
    }

    public void setServices(List<AssociatedService> services)
    {
        this.services = services;
    }

    public Byte getHullIndicator()
    {
        return hullIndicator;
    }

    public void setHullIndicator(Byte hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }
}
