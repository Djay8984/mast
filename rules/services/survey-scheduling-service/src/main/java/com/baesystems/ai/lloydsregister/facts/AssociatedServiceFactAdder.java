package com.baesystems.ai.lloydsregister.facts;

import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.cache.CacheManager;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceCatalogueKey;
import com.detica.ukdc.drools.service.model.CertifiedRelationship;
import com.detica.ukdc.drools.service.model.HullSurvey;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * Fact Adder for Associated Services that have been returned from Execution Server
 *
 * @author HMiah2
 *
 */
public class AssociatedServiceFactAdder extends AbstractFactAdder
{

    public AssociatedServiceFactAdder(final String headerName)
    {
        super(headerName);
    }

    /**
     * Set the headers with the parameters that are needed later on
     * when querying the database.
     * @param service This is one (of possibly many) services that have been returned from
     * the define associated services rules set
     * @param message The original camel message to set the headers to
     */
    public void setHeaders(final Service service, final Message message)
    {
        String serviceCode = service.getServiceCatalogue().getServiceCode();
        Service certifiedService = message.getHeader("serviceScheduling_certifiedService", Service.class);
        String serviceGroup = certifiedService.getServiceCatalogue().getServiceGroup();

        message.setHeader("serviceScheduling_service", service);
        message.setHeader("serviceScheduling_serviceCode", serviceCode);
        message.setHeader("serviceScheduling_serviceGroup", serviceGroup);
    }

    /**
     * Create an add the service catalogue for the associated service and add this
     * service to the header of the message.
     * @param message The original camel message to set the header to when the service catalogue has been
     * created and linked to the associated service
     */
    public void createAndAddServiceCatalogue(final Message message)
    {
        Map<AssociatedServiceCatalogueKey, ServiceCatalogue> assocLookup = CacheManager.getAssocLookup();

        Service associatedService = message.getHeader("serviceScheduling_service", Service.class);
        Service certifiedService = message.getHeader("serviceScheduling_certifiedService", Service.class);
        String serviceCode = message.getHeader("serviceScheduling_serviceCode", String.class);
        String serviceGroup = message.getHeader("serviceScheduling_serviceGroup", String.class);
        String assetRuleset = message.getHeader("serviceScheduling_assetRuleSet", String.class);
        if (assetRuleset == null)
        {
            assetRuleset = "None";
        }
        String schedulingRegime = message.getHeader("serviceScheduling_schedulingRegimeName", String.class);

        final AssociatedServiceCatalogueKey key = new AssociatedServiceCatalogueKey(serviceCode, serviceGroup, assetRuleset, schedulingRegime);
        final ServiceCatalogue serviceCatalogue = assocLookup.get(key);
        associatedService.setServiceCatalogue(serviceCatalogue);

        associatedService.setSchedulingTypeAssign(certifiedService.getServiceCatalogue().getSchedulingTypeAssign());
    }

    /**
     * Create and add the hull survey for the associated service
     * @param message Message that contains the results from the sql query and to set the
     * header at the end
     * @throws InvalidPayloadException An exception is thrown if there is an error when trying to
     * cast the message body as a list of results from the database
     */
    public void createAndAddHullSurvey(final Message message) throws InvalidPayloadException
    {
        HullSurvey hullSurvey = new HullSurvey();

        // Check to see if the certified service is the hull survey
        Service certifiedService = message.getHeader("serviceScheduling_certifiedService", Service.class);
        Service associatedService = message.getHeader("serviceScheduling_service", Service.class);

        if ((certifiedService.getServiceCatalogue().getServiceCode().equals("SS")
                || certifiedService.getServiceCatalogue().getServiceCode().equals("CSH")))
        {
            // The certified service is the hull survey
            hullSurvey.setHullSurvey(certifiedService);
            hullSurvey.setAssociatedService(associatedService);
            message.setHeader("serviceScheduling_hullSurvey", hullSurvey);
        }
        else
        {
            // The certified service was not the hull survey -
            // process the database results for a hull survey
            Service hullService = new Service();
            List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
            if (queryResults.size() == 0)
            {
                hullSurvey.setAssociatedService(associatedService);
                hullSurvey.setHullSurvey(null);
                message.setHeader("serviceScheduling_hullSurvey", hullSurvey);
                return;
            }
            Map<String, Object> hullSurveyResults = queryResults.get(0);

            ServiceCatalogue hullServiceCatalogue = new ServiceCatalogue();
            hullServiceCatalogue.setCyclePeriodicity((Integer) hullSurveyResults.get("cycle_periodicity"));
            hullServiceCatalogue.setContinuousIndicator((Boolean) hullSurveyResults.get("continuous_indicator"));

            hullService.setServiceCatalogue(hullServiceCatalogue);
            hullService.setCyclePeriodicity((Integer) hullSurveyResults.get("service_cycle_periodicity"));

            hullSurvey.setAssociatedService(associatedService);
            hullSurvey.setHullSurvey(hullService);
            message.setHeader("serviceScheduling_hullSurvey", hullSurvey);
        }
    }

    /**
     * Create and add the credited service for this associated service
     * @param message The camel message contains the credited service in its header,
     * so there is no need to fetch it from the database
     */
    public void createAndAddCertifiedRelationship(final Message message)
    {
        CertifiedRelationship certifiedRelationship = new CertifiedRelationship();
        certifiedRelationship.setCertifiedService(message.getHeader("serviceScheduling_certifiedService", Service.class));
        certifiedRelationship.setAssociatedService(message.getHeader("serviceScheduling_service", Service.class));
        message.setHeader("serviceScheduling_certifiedRelationship", certifiedRelationship);
    }

    /**
     * Create and add the renewal service for this associated service
     * @param message The camel message contains the renewal service in its header,
     * so there is no need to fetch it from the database
     */
    public void createAndAddRenewalService(final Message message)
    {
        RenewalRelationship renewalRelationship = message.getHeader("serviceScheduling_renewalRelationship", RenewalRelationship.class);
        Service associatedService = message.getHeader("serviceScheduling_service", Service.class);

        RenewalRelationship newRenewal = new RenewalRelationship();
        newRenewal.setRenewalService(renewalRelationship.getRenewalService());
        newRenewal.setAssociatedService(associatedService);

        message.setHeader("serviceScheduling_renewalRelationship", newRenewal);
    }

    @Override
    public void makeFacts(final Message message)
    {
        final FactCollection facts = message.getHeader(this.headerName, FactCollection.class);
        final Service service = message.getHeader("serviceScheduling_service", Service.class);
        final CertifiedRelationship certifiedRelationship = message.getHeader("serviceScheduling_certifiedRelationship", CertifiedRelationship.class);
        final RenewalRelationship renewalRelationship = message.getHeader("serviceScheduling_renewalRelationship", RenewalRelationship.class);
        final HullSurvey hullSurvey = message.getHeader("serviceScheduling_hullSurvey", HullSurvey.class);
        final Fact serviceFact = new Fact("service", service);
        final Fact certifiedRelationshipFact = new Fact("certifiedService", certifiedRelationship);
        final Fact renewalRelationshipFact = new Fact("renewalRelationshipFact", renewalRelationship);
        final Fact hullSurveyFact = new Fact("hullSurvey", hullSurvey);
        facts.addFact(serviceFact);
        facts.addFact(certifiedRelationshipFact);
        facts.addFact(renewalRelationshipFact);
        facts.addFact(hullSurveyFact);
    }
}
