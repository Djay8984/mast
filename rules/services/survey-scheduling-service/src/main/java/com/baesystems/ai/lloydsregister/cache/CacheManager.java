package com.baesystems.ai.lloydsregister.cache;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lloydsregister.model.AssociatedServiceCatalogueKey;
import com.detica.ukdc.drools.service.model.CodeFamily;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * This class gets called when Tomcat is first starting up. The purpose of this class is to create all the cache objects
 * that will be later populated and used
 *
 * @author HMiah2
 *
 */
public class CacheManager implements ServletContextListener
{
    private static final Logger LOG = LoggerFactory.getLogger(CacheManager.class);

    // Countdown latch for the all the different caches
    // We will decrease from the countdown each time the cache has been updated - this is to
    // ensure that a request does not use a empty cache
    private static final CountDownLatch latch = new CountDownLatch(6);

    // Mapping: Service Catalogue Id -> Service Catalogue Object
    private static final AtomicReference<Map<Integer, ServiceCatalogue>> serviceCatalogueCache = new AtomicReference<>();

    // Mapping: Code, Group, Ruleset, Scheduling Regime -> Service Catalogue
    private static final AtomicReference<Map<AssociatedServiceCatalogueKey, ServiceCatalogue>> assocLookup = new AtomicReference<>();

    // Mapping: Service Regime Name -> Scheduling Regime Id
    private static final AtomicReference<Map<String, Integer>> schedulingRegimeCache = new AtomicReference<>();

    // Mapping: Service Group name -> Service Group Id
    private static final AtomicReference<Map<String, Integer>> serviceGroupCache = new AtomicReference<>();

    // Mapping: Service Type name -> Service Type Id
    private static final AtomicReference<Map<String, Integer>> serviceTypeCache = new AtomicReference<>();

    // Mapping: Code Family Key (Service Group Id, Scheduling Regime) -> Code Family
    private static final AtomicReference<Map<Integer, CodeFamily>> codeFamilyCache = new AtomicReference<>();

    /*
     * Method that gets called when Tomcat first starts up. Start creating the space for the caches (non-Javadoc)
     *
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        // There is nothing to initialise
    }

    /*
     * Method that gets called when Tomcat is shutting down. Clean up all the caches (non-Javadoc)
     *
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        // There is nothing to close
    }

    /**
     * Get the service catalogue cache
     *
     * @return ServiceCatalogue cache
     */
    public static Map<Integer, ServiceCatalogue> getServiceCatalogueCache()
    {
        return serviceCatalogueCache.get();
    }

    /**
     * Get the associated services lookup cache
     * @return Associated services lookup cache
     */
    public static Map<AssociatedServiceCatalogueKey, ServiceCatalogue> getAssocLookup()
    {
        return assocLookup.get();
    }

    /**
     * Get the scheduling regime cache
     *
     * @return SchedulingRegime cache
     */
    public static Map<String, Integer> getSchedulingRegimeCache()
    {
        return schedulingRegimeCache.get();
    }

    /**
     * Get the service group cache
     *
     * @return ServiceGroup cache
     */
    public static Map<String, Integer> getServiceGroupCache()
    {
        return serviceGroupCache.get();
    }

    /**
     * Get the service type cache
     *
     * @return ServiceType cache
     */
    public static Map<String, Integer> getServiceTypeCache()
    {
        return serviceTypeCache.get();
    }

    /**
     * Get the code family cache
     *
     * @return CodeFamily cache
     */
    public static Map<Integer, CodeFamily> getCodeFamilyCache()
    {
        return codeFamilyCache.get();
    }

    /**
     * Update the service catalogue cache with the latest version
     *
     * @param serviceCatalogueRefData Updated version of the service catalogue cache
     */
    public static void updateServiceCatalogueCache(Map<Integer, ServiceCatalogue> serviceCatalogueRefData)
    {
        serviceCatalogueCache.set(serviceCatalogueRefData);
        LOG.info("The service catalogue cache has been updated");
        latch.countDown();
    }

    /**
     * Update the associated services cache
     * @param assocLookupCache The new updated cache to replace the old cache
     */
    public static void updateAssociatedCatalogueCache(Map<AssociatedServiceCatalogueKey, ServiceCatalogue> assocLookupCache)
    {
        assocLookup.set(assocLookupCache);
        LOG.info("The associated service catalogue lookup cache has been updated.");
        latch.countDown();
    }

    /**
     * Update the scheduling regime cache with the latest version
     *
     * @param schedulingRegimeRefData Updated version of the scheduling regime cache
     */
    public static void updateSchedulingRegimeCache(Map<String, Integer> schedulingRegimeRefData)
    {
        schedulingRegimeCache.set(schedulingRegimeRefData);
        LOG.info("The scheduling regime cache has been updated");
        latch.countDown();
    }

    /**
     * Update the service group cache with the latest version
     *
     * @param serviceGroupRefData Updated version of the service group cache
     */
    public static void updateServiceGroupCache(Map<String, Integer> serviceGroupRefData)
    {
        serviceGroupCache.set(serviceGroupRefData);
        LOG.info("The service group cache has been updated");
        latch.countDown();
    }

    /**
     * Update the service type cache with the latest version
     *
     * @param serviceTypeRefData Updated version of the service type cache
     */
    public static void updateServiceTypeCache(Map<String, Integer> serviceTypeRefData)
    {
        serviceTypeCache.set(serviceTypeRefData);
        LOG.info("The service type cache has been updated");
        latch.countDown();
    }

    /**
     * Update the code family cache with the latest version
     *
     * @param codeFamilyRefData Updated version of the code family cache
     */
    public static void updateCodeFamilyCache(Map<Integer, CodeFamily> codeFamilyRefData)
    {
        codeFamilyCache.set(codeFamilyRefData);
        LOG.info("The code family cache has been updated");
        latch.countDown();
    }

    /**
     * Get the count down latch for the updates to the cache
     *
     * @return A count down object that will decremented every time there is an update to a cache. Size of count down: 6
     *         [The number of caches in the cache manager]
     */
    public static CountDownLatch getCountDownLatch()
    {
        return latch;
    }
}
