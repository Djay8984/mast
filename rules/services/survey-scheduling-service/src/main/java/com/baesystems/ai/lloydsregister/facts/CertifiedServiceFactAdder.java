package com.baesystems.ai.lloydsregister.facts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.cache.CacheManager;
import com.baesystems.ai.lloydsregister.exception.ResultSetException;
import com.detica.ukdc.drools.service.model.Asset;
import com.detica.ukdc.drools.service.model.CodeFamily;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * Fact Adder for Certified Services
 *
 * @author HMiah2
 *
 */
public class CertifiedServiceFactAdder extends AbstractFactAdder
{

    public CertifiedServiceFactAdder(final String headerName)
    {
        super(headerName);
    }

    /**
     * Create and add the credited service from the database
     * @param message Camel message to set the header for when the credited
     * service has been retrieved from the database
     * @throws ResultSetException If zero or more than one scheduled service has been returned from the database
     * @throws InvalidPayloadException If there is an error when trying to cast the message body as a list of
     * results that have been returned from the database
     * @throws ParseException If there is an error when trying to parse the completion date of the credited service
     */
    public void createAndAddCertifiedService(final Message message) throws ResultSetException, InvalidPayloadException, ParseException
    {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setLenient(false);
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service certifiedService = new Service();
        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.size() != 1)
        {
            throw new ResultSetException("An invalid ID was entered for the scheduled service as no "
                    + "scheduled services were returned from the database or more than one scheduled service has been returned by the database");
        }

        final Map<String, Object> certifiedServiceRow = queryResults.get(0);
        certifiedService.setScenarioNumber(message.getHeader("scenarioNumber", Integer.class));
        certifiedService.setServiceStatus((String) certifiedServiceRow.get("service_status"));
        certifiedService.setCyclePeriodicity((Integer) certifiedServiceRow.get("cycle_periodicity"));
        if (message.getHeader("completionDate") != null)
        {
            final String completionDateString = message.getHeader("completionDate", String.class);
            Calendar completionDate = new GregorianCalendar(timeZone);
            completionDate.setTime(simpleDateFormat.parse(completionDateString));
            certifiedService.setCompletionDate(completionDate);
        }

        certifiedService.setServiceOccurenceNumber((Integer) certifiedServiceRow.get("service_occurence_number"));

        if (certifiedServiceRow.get("assigned_date") != null && certifiedServiceRow.get("assigned_date_manual") == null)
        {
            Calendar assignedDate = new GregorianCalendar(timeZone);
            assignedDate.setTime((Date) certifiedServiceRow.get("assigned_date"));
            certifiedService.setAssignedDate(assignedDate);
        }
        else if (certifiedServiceRow.get("assigned_date_manual") != null)
        {
            Calendar assignedDateManual = new GregorianCalendar(timeZone);
            assignedDateManual.setTime((Date) certifiedServiceRow.get("assigned_date_manual"));
            certifiedService.setAssignedDate(assignedDateManual);
        }

        if (certifiedServiceRow.get("due_date") != null && certifiedServiceRow.get("due_date_manual") == null)
        {
            Calendar dueDate = new GregorianCalendar(timeZone);
            dueDate.setTime((Date) certifiedServiceRow.get("due_date"));
            certifiedService.setServiceDueDate(dueDate);
        }
        else if (certifiedServiceRow.get("due_date_manual") != null)
        {
            Calendar dueDateManual = new GregorianCalendar(timeZone);
            dueDateManual.setTime((Date) certifiedServiceRow.get("due_date_manual"));
            certifiedService.setServiceDueDate(dueDateManual);
        }

        if (certifiedServiceRow.get("lower_range_date") != null && certifiedServiceRow.get("lower_range_date_manual") == null)
        {
            Calendar lowerRangeDate = new GregorianCalendar(timeZone);
            lowerRangeDate.setTime((Date) certifiedServiceRow.get("lower_range_date"));
            certifiedService.setLowerRangeDate(lowerRangeDate);
        }
        else if (certifiedServiceRow.get("lower_range_date_manual") != null)
        {
            Calendar lowerRangeDateManual = new GregorianCalendar(timeZone);
            lowerRangeDateManual.setTime((Date) certifiedServiceRow.get("lower_range_date_manual"));
            certifiedService.setLowerRangeDate(lowerRangeDateManual);
        }

        if (certifiedServiceRow.get("upper_range_date") != null && certifiedServiceRow.get("upper_range_date_manual") == null)
        {
            Calendar upperRangeDate = new GregorianCalendar(timeZone);
            upperRangeDate.setTime((Date) certifiedServiceRow.get("upper_range_date"));
            certifiedService.setUpperRangeDate(upperRangeDate);
        }
        else if (certifiedServiceRow.get("upper_range_date_manual") != null)
        {
            Calendar upperRangeDateManual = new GregorianCalendar(timeZone);
            upperRangeDateManual.setTime((Date) certifiedServiceRow.get("upper_range_date_manual"));
            certifiedServiceRow.get(upperRangeDateManual);
        }

        message.setHeader(serviceKey, certifiedService);
    }

    /**
     * Create and add the service catalogue that is associated
     * with this credited service. As this is a method that uses the cache (and it the first method
     * to use such a cache from the cache manager) there is no need to get information from the database, but
     * this method does however wait for the CountDownLatch of the cache manager to reach zero before continuing - this
     * is a precaution to make sure the cache has been populated at the start before we attempt to get information out of it
     * @param message The original camel message to set the header
     * @throws InterruptedException If there is an error while waiting for the count down to
     * reach zero
     */
    public void createAndAddServiceCatalogue(final Message message) throws InterruptedException
    {
        CountDownLatch latch = CacheManager.getCountDownLatch();
        latch.await();
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service service = message.getHeader(serviceKey, Service.class);
        Integer serviceCatalogueId = message.getHeader("serviceScheduling_serviceCatalogueId", Integer.class);

        Map<Integer, ServiceCatalogue> serviceCatalogueCache = CacheManager.getServiceCatalogueCache();
        Map<String, Integer> schedulingRegimeCache = CacheManager.getSchedulingRegimeCache();

        ServiceCatalogue serviceCatalogue = serviceCatalogueCache.get(serviceCatalogueId);
        service.setServiceCatalogue(serviceCatalogue);

        Integer schedulingRegimeId = schedulingRegimeCache.get(serviceCatalogue.getSchedulingRegime());

        message.setHeader("serviceScheduling_schedulingRegimeName", serviceCatalogue.getSchedulingRegime());
        message.setHeader("serviceScheduling_schedulingRegimeId", schedulingRegimeId);

        _setHeaders(message);
    }

    /**
     * Set the headers that will be later used in the routes
     * @param message The original camel message to set the headers of
     */
    private void _setHeaders(final Message message)
    {
        Integer serviceCatalogueId = message.getHeader("serviceScheduling_serviceCatalogueId", Integer.class);

        Map<Integer, ServiceCatalogue> serviceCatalogueCache = CacheManager.getServiceCatalogueCache();

        Map<String, Integer> serviceGroupCache = CacheManager.getServiceGroupCache();
        Map<String, Integer> serviceTypeCache = CacheManager.getServiceTypeCache();

        ServiceCatalogue serviceCatalogue = serviceCatalogueCache.get(serviceCatalogueId);

        Integer serviceGroupId = serviceGroupCache.get(serviceCatalogue.getServiceGroup());
        Integer serviceTypeId = serviceTypeCache.get(serviceCatalogue.getServiceType());

        message.setHeader("serviceScheduling_serviceGroupId", serviceGroupId);
        message.setHeader("serviceScheduling_serviceTypeId", serviceTypeId);
        message.setHeader("serviceScheduling_schedulingRegimeName", serviceCatalogue.getSchedulingRegime());
    }

    /**
     * Create and add the renewal service that is associated with this credited service
     * @param message The original camel message to set the headers of
     * @throws ResultSetException If more than one renewal service has been returned by the database
     * @throws InvalidPayloadException If there was an error when trying to cast the results of the
     * database as list of results
     */
    public void createAndAddRenewalService(final Message message) throws ResultSetException, InvalidPayloadException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        Service associatedService = message.getHeader(serviceKey, Service.class);
        RenewalRelationship renewalRelationship = new RenewalRelationship();
        Service renewalService = new Service();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            renewalRelationship.setRenewalService(null);
            renewalRelationship.setAssociatedService(associatedService);
            message.setHeader("serviceScheduling_renewalService", renewalRelationship);
            return;
        }
        if (queryResults.size() > 1)
        {
            throw new ResultSetException(
                    "Greater than one Renewal Service was returned: " + queryResults.size());
        }
        Map<String, Object> renewalServiceRow = queryResults.get(0);
        if (renewalServiceRow.get("due_date") != null && renewalServiceRow.get("due_date_manual") == null)
        {
            Calendar dueDate = new GregorianCalendar(timeZone);
            dueDate.setTime((Date) renewalServiceRow.get("due_date"));
            renewalService.setServiceDueDate(dueDate);
        }
        else if (renewalServiceRow.get("due_date_manual") != null)
        {
            Calendar dueDateManual = new GregorianCalendar(timeZone);
            dueDateManual.setTime((Date) renewalServiceRow.get("due_date_manual"));
            renewalService.setServiceDueDate(dueDateManual);
        }

        if (renewalServiceRow.get("assigned_date") != null && renewalServiceRow.get("assigned_date_manual") == null)
        {
            Calendar assignedDate = new GregorianCalendar(timeZone);
            assignedDate.setTime((Date) renewalServiceRow.get("assigned_date"));
            renewalService.setAssignedDate(assignedDate);
        }
        else if (renewalServiceRow.get("assigned_date_manual") != null)
        {
            Calendar assignedDateManual = new GregorianCalendar(timeZone);
            assignedDateManual.setTime((Date) renewalServiceRow.get("assigned_date_manual"));
            renewalService.setAssignedDate(assignedDateManual);
        }

        ServiceCatalogue renewalServiceCatalogue = new ServiceCatalogue();
        renewalServiceCatalogue.setCyclePeriodicity((Integer) renewalServiceRow.get("cycle_periodicity"));
        renewalService.setCyclePeriodicity((Integer) renewalServiceRow.get("service_cycle_periodicity"));
        renewalService.setServiceCatalogue(renewalServiceCatalogue);

        renewalRelationship.setAssociatedService(associatedService);
        renewalRelationship.setRenewalService(renewalService);

        message.setHeader("serviceScheduling_renewalService", renewalRelationship);
    }

    /**
     * Create and add the code family that is associated with this
     * credited service
     * @param message The original camel message to set the headers of
     */
    public void createAndAddCodeFamily(final Message message)
    {
        Integer serviceCatalogueId = message.getHeader("serviceScheduling_serviceCatalogueId", Integer.class);

        Map<Integer, CodeFamily> codeFamilyCache = CacheManager.getCodeFamilyCache();

        CodeFamily codeFamily = codeFamilyCache.get(serviceCatalogueId);
        if (codeFamily == null)
        {
            codeFamily = new CodeFamily();
        }
        message.setHeader("serviceScheduling_codeFamily", codeFamily);
    }

    /**
     * Create and add the asset that is associated with this
     * credited service.
     * @param message The original camel message to set the headers of
     * @throws ResultSetException If more than one asset has been returned by the database
     * @throws InvalidPayloadException If there was an error when trying to cast the message body as a list
     * of results that have been returned by the database
     * @throws ParseException If there was an error when trying to parse the harmonisation date of the
     * asset
     */
    public void createAndAddAsset(final Message message) throws ResultSetException, InvalidPayloadException, ParseException
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);

        Service service = message.getHeader(serviceKey, Service.class);

        Asset asset = new Asset();

        List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.size() == 0)
        {
            service.setAsset(null);
            return;
        }

        if (queryResults.size() > 1)
        {
            throw new ResultSetException(
                    "Greater than one Asset was returned: " + queryResults.size());
        }

        if (message.getHeader("assetHarmonisationDate") != null)
        {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            simpleDateFormat.setLenient(false);
            Calendar harmonisationDate = new GregorianCalendar(timeZone);
            String date = message.getHeader("assetHarmonisationDate", String.class);
            harmonisationDate.setTime(simpleDateFormat.parse(date));
            asset.setHarmonisationDate(harmonisationDate);
        }

        Map<String, Object> assetRow = queryResults.get(0);

        if (assetRow.get("commission_date") != null)
        {
            Calendar commissionDate = new GregorianCalendar(timeZone);
            commissionDate.setTime((Date) assetRow.get("commission_date"));
            asset.setCommissionDate(commissionDate);
        }

        if (assetRow.get("build_date") != null)
        {
            Calendar buildDate = new GregorianCalendar(timeZone);
            buildDate.setTime((Date) assetRow.get("build_date"));
            asset.setBuildDate(buildDate);
        }

        if (assetRow.get("hull_indicator") != null)
        {
            asset.setHullIndicator(((Integer) assetRow.get("hull_indicator")).byteValue());
            message.setHeader("serviceScheduling_original_hull_indicator", ((Integer) assetRow.get("hull_indicator")).byteValue());
        }
        asset.setClassStatus((String) assetRow.get("class_status"));
        asset.setGrossTonnage((Double) assetRow.get("gross_tonnage"));
        asset.setAssetType((String) assetRow.get("level_5_code"));
        asset.setSscPrivateIndicator((Boolean) assetRow.get("ssc_private_indicator"));
        asset.setIwwCoastalIndicator((Boolean) assetRow.get("iww_coastal_indicator"));

        service.setAsset(asset);
    }

    @Override
    public void makeFacts(final Message message)
    {
        String serviceKey = message.getHeader("serviceScheduling_serviceKey", String.class);
        final FactCollection facts = message.getHeader(this.headerName, FactCollection.class);
        final Service certifiedService = message.getHeader(serviceKey, Service.class);
        final RenewalRelationship renewalRelationship = message.getHeader("serviceScheduling_renewalService", RenewalRelationship.class);
        final CodeFamily codeFamily = message.getHeader("serviceScheduling_codeFamily", CodeFamily.class);
        final Fact certifiedServiceFact = new Fact(serviceKey, certifiedService);
        final Fact renewalRelationshipFact = new Fact("renewalRelationship", renewalRelationship);
        final Fact codeFamilyFact = new Fact("codeFamily", codeFamily);
        final Fact emptyMap = new Fact("associatedServicesMap", new HashMap<String, Service>());
        facts.addFact(certifiedServiceFact);
        facts.addFact(renewalRelationshipFact);
        facts.addFact(codeFamilyFact);
        facts.addFact(emptyMap);
    }

}
