package com.baesystems.ai.lloydsregister.aggregator;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import com.baesystems.ai.lloydsregister.model.AssociatedService;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceWrapper;

/**
 * Class to aggregate the results from the split function in the route. This class takes two parameters: Old Exchange,
 * New Exchange The functions purpose is to the append the New Exchange to the Old Exchange and return the Old Exchange
 * (modified)
 * 
 * @author HMiah2
 *
 */
public class ServiceAggregationStrategy implements AggregationStrategy
{

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange)
    {
        if (oldExchange == null)
        {
            return newExchange;
        }

        AssociatedServiceWrapper oldWrapper = oldExchange.getIn().getBody(AssociatedServiceWrapper.class);
        AssociatedServiceWrapper newWrapper = newExchange.getIn().getBody(AssociatedServiceWrapper.class);

        List<AssociatedService> oldList = oldWrapper.getServices();
        List<AssociatedService> newList = newWrapper.getServices();

        newList.addAll(oldList);

        newWrapper.setServices(newList);

        if (oldWrapper.getHullIndicator() != null)
        {
            newWrapper.setHullIndicator(oldWrapper.getHullIndicator());
        }

        oldExchange.getIn().setBody(newWrapper, AssociatedServiceWrapper.class);

        return oldExchange;
    }

}
