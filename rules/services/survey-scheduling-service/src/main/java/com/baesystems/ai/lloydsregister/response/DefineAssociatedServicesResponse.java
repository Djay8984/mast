package com.baesystems.ai.lloydsregister.response;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;

/**
 * Process the response from the call to the define associated services container in Execution server
 * @author HMiah2
 *
 */
public class DefineAssociatedServicesResponse
{
    private static final String REDUNDANT = "Redundant";

    /**
     * The logger to use for this class.
     */
    private static final Logger logger = LoggerFactory.getLogger(DefineAssociatedServicesResponse.class);

    private DroolsXStreamMarshaller marshaller;

    public DefineAssociatedServicesResponse()
    {
        marshaller = new DroolsXStreamMarshaller();
    }


    /**
     * Process the response from the execution server when calling the
     * Define Associated Services container
     * @param message The camel message that contains the response from the server in the body
     * @throws InvalidPayloadException If there is an error when trying to cast the message body as a
     * String
     * @throws ExecutionServerException If there is an error when trying to parse the response from the
     * execution server
     */
    public void processResponse(final Message message) throws InvalidPayloadException, ExecutionServerException
    {
        String serviceResponse = message.getMandatoryBody(String.class);

        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);

        if (response.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when converting the response from the KIE Server");
        }
        Map<String, Object> results = (marshaller.unmarshalFromXml(response.getResult(), ExecutionResultImpl.class)).getResults();

        RenewalRelationship renewalRelationship = (RenewalRelationship) results.get("renewalRelationship");
        Service certifiedService = renewalRelationship.getAssociatedService();
        Map<String, Service> associatedServices = (Map<String, Service>) results.get("associatedServicesMap");
        int numberOfAssociatedServices = associatedServices.size();
        if (numberOfAssociatedServices == 0)
        {
            logger.warn("No associated services have been returned");
        }
        Collection<Service> associatedServicesList = associatedServices.values();

        _removeRedundantServices(associatedServicesList);

        if (associatedServicesList.isEmpty())
        {
            message.setBody(null);
        }

        message.setHeader("serviceScheduling_renewalRelationship", renewalRelationship);
        message.setHeader("serviceScheduling_certifiedService", certifiedService);
        message.setHeader("serviceScheduling_numberOfAssociatedServices", numberOfAssociatedServices);
        message.setHeader("serviceScheduling_associatedServicesList", associatedServicesList);
    }

    /**
     * Remove all the redundant services that have been returned
     * @param associactedServices A list of associated services that have been returned by
     * execution server
     */
    private void _removeRedundantServices(Collection<Service> associactedServices)
    {
        final Iterator<Service> serviceIterator = associactedServices.iterator();
        while (serviceIterator.hasNext())
        {
            final Service service = serviceIterator.next();
            if (service.getWorkflowInstruction().equals(REDUNDANT))
            {
                serviceIterator.remove();
            }
        }
    }

}
