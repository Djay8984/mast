package com.baesystems.ai.lloydsregister.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;

import com.baesystems.ai.lloydsregister.exception.ResultSetException;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceCatalogueKey;
import com.baesystems.ai.lloydsregister.model.CodeFamilyKey;
import com.detica.ukdc.drools.service.model.CodeFamily;
import com.detica.ukdc.drools.service.model.ServiceCatalogue;

/**
 * The purpose of this class is to take the results from the database and update the cache with the updated data.
 *
 * @author HMiah2
 *
 */
public class CacheReferenceData
{
    /*--- Constants for code family computation ---*/
    private static final String RENEWAL = "Renewal";
    private static final String INITIAL = "Initial";
    private static final String PERIODIC = "Periodic";
    private static final String ANNUAL = "Annual";
    private static final String INTERMEDIATE = "Intermediate";

    public CacheReferenceData()
    {
    }

    /**
     * Cache the service catalogue reference data
     *
     * @param message This is the camel message that contains the results from the database query
     * @throws ResultSetException If no results have been returned from the database
     * @throws InvalidPayloadException If there is an error when trying to parse the camel
     * message body as a list of results from the database
     */
    public void cacheServiceCatalogue(final Message message) throws ResultSetException, InvalidPayloadException
    {
        Map<Integer, ServiceCatalogue> serviceCatalogueRefData = new HashMap<>();

        Map<AssociatedServiceCatalogueKey, ServiceCatalogue> assocCatalogueLookup = new HashMap<>();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No service catalogue records were returned from the database");
        }

        // Process the results
        for (Map<String, Object> serviceCatalogueRow : queryResults)
        {
            ServiceCatalogue serviceCatalogue = new ServiceCatalogue();
            serviceCatalogue.setServiceCode((String) serviceCatalogueRow.get("code"));
            serviceCatalogue.setCyclePeriodicity((Integer) serviceCatalogueRow.get("cycle_periodicity"));
            serviceCatalogue.setLowerRangePeriod((Integer) serviceCatalogueRow.get("lower_range_date_offset_months"));
            serviceCatalogue.setUpperRangePeriod((Integer) serviceCatalogueRow.get("upper_range_date_offset_months"));
            serviceCatalogue.setServiceType((String) serviceCatalogueRow.get("service_type"));
            serviceCatalogue.setSchedulingRegime((String) serviceCatalogueRow.get("scheduling_regime_name"));
            serviceCatalogue.setSchedulingTypeDue((String) serviceCatalogueRow.get("scheduling_type_due"));
            serviceCatalogue.setSchedulingTypeAssign((String) serviceCatalogueRow.get("scheduling_type_assign"));
            if (serviceCatalogueRow.get("harmonisation_type") != null)
            {
                serviceCatalogue.setHarmonisationType((String) serviceCatalogueRow.get("harmonisation_type"));
            }
            else
            {
                //If we did not get a harmonisation type back from the database, we set it to "None".
                serviceCatalogue.setHarmonisationType("None");
            }
            serviceCatalogue.setContinuousIndicator((Boolean) serviceCatalogueRow.get("continuous_indicator"));
            serviceCatalogue.setSchedulingTypeAssociated((String) serviceCatalogueRow.get("scheduling_type_associated"));
            serviceCatalogue.setServiceGroup((String) serviceCatalogueRow.get("service_group_name"));
            if (serviceCatalogueRow.get("service_ruleset") != null)
            {
                serviceCatalogue.setServiceRuleset((String) serviceCatalogueRow.get("service_ruleset"));
            }
            else
            {
                //If we did not get a service ruleset back from the database, we set it to "None".
                serviceCatalogue.setServiceRuleset("None");
            }
            serviceCatalogue.setProductFamilyName((String) serviceCatalogueRow.get("product_family_name"));

            Integer id = new Integer((Integer) serviceCatalogueRow.get("id"));
            serviceCatalogue.setIdentifier(id);

            serviceCatalogueRefData.put(id, serviceCatalogue);

            final AssociatedServiceCatalogueKey assocKey = new AssociatedServiceCatalogueKey(serviceCatalogue.getServiceCode(),
                    serviceCatalogue.getServiceGroup(),
                    serviceCatalogue.getServiceRuleset(), serviceCatalogue.getSchedulingRegime());

            assocCatalogueLookup.put(assocKey, serviceCatalogue);
        }

        // Update the service catalogue caches
        CacheManager.updateServiceCatalogueCache(serviceCatalogueRefData);
        CacheManager.updateAssociatedCatalogueCache(assocCatalogueLookup);
    }

    /**
     * Cache the scheduling regime. The scheduling regime cache is a reverse cache - The name maps to the Id.
     *
     * @param message This is the camel message that contains the results from the database query
     * @throws ResultSetException If no results have been returned from the database
     * @throws InvalidPayloadException If there is an error when trying to cast the message body
     * as a list of results that have been returned from the database
     */
    public void cacheSchedulingRegime(final Message message) throws ResultSetException, InvalidPayloadException
    {
        Map<String, Integer> schedulingRegimeRefData = new HashMap<>();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No scheduling regime records were returned from the database");
        }

        for (Map<String, Object> schedulingRegimeRow : queryResults)
        {
            String schedulingRegime = (String) schedulingRegimeRow.get("scheduling_regime");
            Integer schedulingRegimeId = (Integer) schedulingRegimeRow.get("id");

            schedulingRegimeRefData.put(schedulingRegime, schedulingRegimeId);
        }
        CacheManager.updateSchedulingRegimeCache(schedulingRegimeRefData);
    }

    /**
     * Cache the service group. The service group cache is a reverse cache - The name maps to the Id.
     *
     * @param message This is the camel message that contains the results from the database query
     * @throws ResultSetException If no results have been returned from the database
     * @throws InvalidPayloadException If there is an error when trying to cast the message body
     * as a list of results that have been returned from the database
     */
    public void cacheServiceGroup(final Message message) throws ResultSetException, InvalidPayloadException
    {
        Map<String, Integer> serviceGroupRefData = new HashMap<>();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No service group records were returned from the database");
        }

        for (Map<String, Object> serviceGroupRow : queryResults)
        {
            String serviceGroupName = (String) serviceGroupRow.get("name");
            Integer serviceGroupId = (Integer) serviceGroupRow.get("id");

            serviceGroupRefData.put(serviceGroupName, serviceGroupId);
        }

        CacheManager.updateServiceGroupCache(serviceGroupRefData);
    }

    /**
     * Cache the service type. The service type cache is a reverse cache - The name maps to the Id.
     *
     * @param message This is the camel message that contains the results from the database query
     * @throws ResultSetException If no results have been returned from the database
     * @throws InvalidPayloadException If there is an error when trying to cast the message body
     * as a list of results that have been returned from the database
     */
    public void cacheServiceType(final Message message) throws ResultSetException, InvalidPayloadException
    {
        Map<String, Integer> serviceTypeRefData = new HashMap<>();

        final List<Map<String, Object>> queryResults = (List<Map<String, Object>>) message.getMandatoryBody();
        if (queryResults.isEmpty())
        {
            throw new ResultSetException("No service type records were returned from the database");
        }

        for (Map<String, Object> serviceTypeRow : queryResults)
        {
            String serviceTypeName = (String) serviceTypeRow.get("name");
            Integer serviceTypeId = (Integer) serviceTypeRow.get("id");

            serviceTypeRefData.put(serviceTypeName, serviceTypeId);
        }

        CacheManager.updateServiceTypeCache(serviceTypeRefData);
    }

    /**
     * Compute and cache the code family. In order to compute the code families of service codes - we use the service
     * catalogue instead of talking to the database
     */
    public void cacheCodeFamily()
    {
        Map<CodeFamilyKey, CodeFamily> compositeCodeFamilyCache = new HashMap<>();
        Map<Integer, CodeFamily> codeFamilyCache = new HashMap<>();
        Map<CodeFamilyKey, List<Integer>> compositeServiceCatalogueMap = new HashMap<>();
        Map<Integer, ServiceCatalogue> serviceCatalogueCache = CacheManager.getServiceCatalogueCache();
        Map<String, Integer> serviceGroupCache = CacheManager.getServiceGroupCache();

        for(Map.Entry<Integer, ServiceCatalogue> serviceCatalogueEntry : serviceCatalogueCache.entrySet())
        {
            ServiceCatalogue serviceCatalogue = serviceCatalogueEntry.getValue();
            String serviceType = serviceCatalogue.getServiceType();
            String serviceCode = serviceCatalogue.getServiceCode();
            String serviceGroup = serviceCatalogue.getServiceGroup();

            //This is the composite key
            String schedulingRegime = serviceCatalogue.getSchedulingRegime();
            Integer serviceGroupId = serviceGroupCache.get(serviceGroup);

            CodeFamilyKey codeFamilyKey = new CodeFamilyKey(serviceGroupId, schedulingRegime);
            if(!compositeCodeFamilyCache.containsKey(codeFamilyKey))
            {
                CodeFamily codeFamily = new CodeFamily();
                switch (serviceType)
                {
                    case INITIAL:
                        codeFamily.setInitial(serviceCode);
                        break;
                    case RENEWAL:
                        codeFamily.setRenewal(serviceCode);
                        break;
                    case ANNUAL:
                        codeFamily.setAnnual(serviceCode);
                        break;
                    case INTERMEDIATE:
                        codeFamily.setIntermediate(serviceCode);
                        break;
                    case PERIODIC:
                        codeFamily.setPeriodic(serviceCode);
                        break;
                }
                compositeCodeFamilyCache.put(codeFamilyKey, codeFamily);
                compositeServiceCatalogueMap.put(codeFamilyKey, new ArrayList<Integer>(serviceCatalogue.getIdentifier()));
                codeFamilyCache.put(serviceCatalogue.getIdentifier(), codeFamily);
            }
            else
            {
                CodeFamily codeFamily = compositeCodeFamilyCache.get(codeFamilyKey);
                // empty string denotes clashing catalogue codes.
                switch (serviceType)
                {
                    case INITIAL:
                        if (!"".equals(codeFamily.getInitial()))
                        {
                            codeFamily.setInitial(serviceCode);
                        }
                        else if (codeFamily.getInitial() != null && !codeFamily.getInitial().equals(serviceCode))
                        {
                            codeFamily.setInitial("");
                        }
                        break;
                    case RENEWAL:
                        if (!"".equals(codeFamily.getRenewal()))
                        {
                            codeFamily.setRenewal(serviceCode);
                        }
                        else if (codeFamily.getRenewal() != null && !codeFamily.getRenewal().equals(serviceCode))
                        {
                            codeFamily.setRenewal("");
                        }
                        break;
                    case ANNUAL:
                        if (!"".equals(codeFamily.getAnnual()))
                        {
                            codeFamily.setAnnual(serviceCode);
                        }
                        else if (codeFamily.getAnnual() != null && !codeFamily.getAnnual().equals(serviceCode))
                        {
                            codeFamily.setAnnual("");
                        }
                        break;
                    case INTERMEDIATE:
                        if (!"".equals(codeFamily.getIntermediate()))
                        {
                            codeFamily.setIntermediate(serviceCode);
                        }
                        else if (codeFamily.getIntermediate() != null && !codeFamily.getIntermediate().equals(serviceCode))
                        {
                            codeFamily.setIntermediate("");
                        }
                        break;
                    case PERIODIC:
                        if (!"".equals(codeFamily.getPeriodic()))
                        {
                            codeFamily.setPeriodic(serviceCode);
                        }
                        else if (codeFamily.getPeriodic() != null && !codeFamily.getPeriodic().equals(serviceCode))
                        {
                            codeFamily.setPeriodic("");
                        }
                        break;
                }
                compositeCodeFamilyCache.put(codeFamilyKey, codeFamily);
                List<Integer> serviceCatalogueIds = compositeServiceCatalogueMap.get(codeFamilyKey);
                serviceCatalogueIds.add(serviceCatalogue.getIdentifier());
                compositeServiceCatalogueMap.put(codeFamilyKey, serviceCatalogueIds);
            }
        }
        for(Map.Entry<CodeFamilyKey, CodeFamily> compositeMapping : compositeCodeFamilyCache.entrySet())
        {
            CodeFamilyKey key = compositeMapping.getKey();
            CodeFamily value = compositeMapping.getValue();
            List<Integer> serviceCatalogueIds = compositeServiceCatalogueMap.get(key);
            for(Integer identifier : serviceCatalogueIds)
            {
                codeFamilyCache.put(identifier, value);
            }
        }
        CacheManager.updateCodeFamilyCache(codeFamilyCache);
    }

}
