package com.baesystems.ai.lloydsregister.response;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.camel.InvalidPayloadException;
import org.apache.camel.Message;
import org.drools.core.runtime.impl.ExecutionResultImpl;
import org.kie.server.api.model.ServiceResponse;

import com.baesystems.ai.lloydsregister.exception.ExecutionServerException;
import com.baesystems.ai.lloydsregister.facts.Fact;
import com.baesystems.ai.lloydsregister.facts.FactCollection;
import com.baesystems.ai.lloydsregister.utils.DroolsXStreamMarshaller;
import com.detica.ukdc.drools.service.model.CertifiedRelationship;
import com.detica.ukdc.drools.service.model.HullSurvey;
import com.detica.ukdc.drools.service.model.RenewalRelationship;
import com.detica.ukdc.drools.service.model.Service;

/**
 * Class to replace the service fact when processing the request from the assigned date and service due date rule sets.
 *
 * @author HMiah
 *
 */
public class ReplaceService
{
    private final String factsHeaderName;
    private DroolsXStreamMarshaller marshaller;

    public ReplaceService(String factsHeaderName)
    {
        this.factsHeaderName = factsHeaderName;
        marshaller = new DroolsXStreamMarshaller();
    }

    /**
     * Replace the service facts from the response that we get back from KIE Execution server
     *
     * @param message Camel message that contains the response from the Execution server
     * @throws InvalidPayloadException If there is an error when trying to
     * cast the message body as a String to be further processed
     * @throws ExecutionServerException If there is an error when trying to parse the
     * response from the server
     */
    public void replaceServiceFact(final Message message) throws InvalidPayloadException, ExecutionServerException
    {
        final Service service = _unmarshallResponse(message.getMandatoryBody(String.class));

        final FactCollection factCollection = message.getHeader(this.factsHeaderName, FactCollection.class);

        List<Fact> listOfFacts = factCollection.getFacts();

        ListIterator<Fact> factsIterator = listOfFacts.listIterator();
        while (factsIterator.hasNext())
        {
            Fact fact = factsIterator.next();
            if (fact.getFactName().equals("service"))
            {
                fact = new Fact("service", service);
                factsIterator.set(fact);
            }
            if (fact.getFactName().equals("certifiedService"))
            {
                CertifiedRelationship certifiedRelationship = (CertifiedRelationship) fact.getFact();
                certifiedRelationship.setAssociatedService(service);
                fact = new Fact("certifiedService", certifiedRelationship);
                factsIterator.set(fact);
            }
            if (fact.getFactName().equals("renewalRelationshipFact"))
            {
                RenewalRelationship renewalRelationship = (RenewalRelationship) fact.getFact();
                renewalRelationship.setAssociatedService(service);
                fact = new Fact("renewalRelationshipFact", renewalRelationship);
                factsIterator.set(fact);
            }
            if (fact.getFactName().equals("hullSurvey"))
            {
                HullSurvey hullSurvey = (HullSurvey) fact.getFact();
                hullSurvey.setAssociatedService(service);
                fact = new Fact("hullSurvey", hullSurvey);
                factsIterator.set(fact);
            }
        }
        factCollection.setFacts(listOfFacts);
    }

    /**
     * Utility method to help with the processing of the response
     *
     * @param serviceResponse A String representation of the response from Execution server
     * @return A Service object
     * @throws Exception If there is an error unmarshalling the response
     */
    private Service _unmarshallResponse(String serviceResponse) throws ExecutionServerException
    {
        ServiceResponse response = marshaller.unmarshalFromXml(serviceResponse, ServiceResponse.class);

        if (response.getResult() == null)
        {
            throw new ExecutionServerException("There was an error when trying to unmarshall the response from the KIE Execution Server");
        }
        Map<String, Object> results = (marshaller.unmarshalFromXml(response.getResult(), ExecutionResultImpl.class)).getResults();
        return (Service) results.get("service");
    }

}
