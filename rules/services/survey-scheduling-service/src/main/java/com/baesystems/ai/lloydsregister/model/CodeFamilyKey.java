package com.baesystems.ai.lloydsregister.model;

import java.io.Serializable;

/**
 * This object is used to model the composite key of service group Id and scheduling regime name that is used to
 * uniquely identify a service code. This composite key is used for the computation of the code family [caching]
 * 
 * @author HMiah2
 *
 */
public class CodeFamilyKey implements Serializable
{
    private final Integer serviceGroupId;
    private final String schedulingRegimeName;
    private final int hashCode;

    public CodeFamilyKey(final Integer serviceGroupId, final String schedulingRegimeName)
    {
        this.serviceGroupId = serviceGroupId;
        this.schedulingRegimeName = schedulingRegimeName;
        int hash = 7;
        hash = 31 * hash + this.serviceGroupId;
        hash = 37 * hash + schedulingRegimeName.hashCode();
        this.hashCode = hash;
    }

    @Override
    public int hashCode()
    {
        return hashCode;
    }

    @Override
    public boolean equals(Object object)
    {
        final boolean isEqual;

        if (object == this)
        {
            isEqual = true;
        }
        else if (object instanceof CodeFamilyKey)
        {
            final CodeFamilyKey other = (CodeFamilyKey) object;

            isEqual = serviceGroupId == other.serviceGroupId &&
                    schedulingRegimeName.equals(other.schedulingRegimeName);
        }
        else
        {
            isEqual = false;
        }

        return isEqual;
    }

}
