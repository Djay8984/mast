package com.baesystems.ai.lloydsregister.model;

import java.io.Serializable;

/**
 * Instances of this class serve as keys to look up an associated service's Service Catalogue entry in the cache.
 */
public class AssociatedServiceCatalogueKey implements Serializable
{
    /**
     * The Service Code assigned to the associated service.
     */
    private final String serviceCode;

    /**
     * The Service Group of the associated service.
     */
    private final String serviceGroup;

    /**
     * The Service Ruleset of the associated service. A value of None indicates that any Service Ruleset can be matched.
     */
    private final String serviceRuleset;

    /**
     * The Scheduling Regime of the associated service.
     */
    private final String schedulingRegime;

    /**
     * The pre-computed hash code for this key.
     */
    private final int hashCode;

    /**
     * Creates a new AssociatedServiceCatalogueKey using the given four values as lookup parameters for a Service
     * Catalogue entry.
     *
     * @param serviceCode The service code assigned to the associated service. Must not be null.
     * @param serviceGroup The service group assigned to the associated service. Must not be null.
     * @param serviceRuleset The service ruleset assigned to the associated service. Set to None if no ruleset is
     *        relevant. Must not be null.
     * @param schedulingRegime the scheduling regime assigned to the associated service. Must not be null.
     *
     *        8 @throws IllegalArgumentException Thrown if a null parameter is given.
     */
    public AssociatedServiceCatalogueKey(final String serviceCode, final String serviceGroup,
            final String serviceRuleset, final String schedulingRegime)
    {
        if (serviceCode == null)
        {
            throw new IllegalArgumentException("Null service code given to AssociatedServiceCatalogueKey.");
        }
        if (serviceGroup == null)
        {
            throw new IllegalArgumentException("Null service group given to AssociatedServiceCatalogueKey.");
        }
        if (serviceRuleset == null)
        {
            throw new IllegalArgumentException("Null service ruleset given to AssociatedServiceCatalogueKey. Did you mean None?");
        }
        if (schedulingRegime == null)
        {
            throw new IllegalArgumentException("Null scheduling regime given to AssociatedServiceCatalogueKey.");
        }

        this.serviceCode = serviceCode;
        this.serviceGroup = serviceGroup;
        this.serviceRuleset = serviceRuleset;
        this.schedulingRegime = schedulingRegime;

        this.hashCode = (31 * serviceCode.hashCode()) +
                (37 * serviceGroup.hashCode()) +
                (41 * schedulingRegime.hashCode());
    }

    @Override
    public int hashCode()
    {
        return hashCode;
    }

    @Override
    public boolean equals(final Object other)
    {
        final boolean isEqual;

        if (other == null)
        {
            isEqual = false;
        }
        else
        {
            final AssociatedServiceCatalogueKey otherKey = (AssociatedServiceCatalogueKey) other;

            isEqual = this.serviceCode.equals(otherKey.serviceCode) &&
                    this.serviceGroup.equals(otherKey.serviceGroup) &&
                    this.schedulingRegime.equals(otherKey.schedulingRegime) &&
                    (this.serviceRuleset.equals("None") || otherKey.serviceRuleset.equals("None") ||
                            this.serviceRuleset.equals(otherKey.serviceRuleset));
        }

        return isEqual;
    }
}
