package com.baesystems.ai.lloydsregister.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Before;
import org.junit.Test;

import com.baesystems.ai.lloydsregister.aggregator.ServiceAggregationStrategy;
import com.baesystems.ai.lloydsregister.model.AssociatedService;
import com.baesystems.ai.lloydsregister.model.AssociatedServiceWrapper;

public class ServiceAggregationStrategyTest extends CamelTestSupport
{
    private Exchange oldExchange;
    private Exchange newExchange;

    private AssociatedServiceWrapper wrapper1;
    private AssociatedServiceWrapper wrapper2;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();

        AssociatedService service1 = new AssociatedService(1, 2, 1, "2020-01-02", "2020-01-01", "2929-01-01", "2020-01-02");
        AssociatedService service2 = new AssociatedService(1, 12, 1, "2021-01-01", "1900-01-01", "2120-01-01", "2020-01-01");

        List<AssociatedService> list1 = new ArrayList<>();
        List<AssociatedService> list2 = new ArrayList<>();

        list1.add(service1);
        list2.add(service2);

        wrapper1 = new AssociatedServiceWrapper(list1);
        wrapper2 = new AssociatedServiceWrapper(list2);

        wrapper2.setHullIndicator(new Byte("3"));

        oldExchange = createExchangeWithBody(wrapper1);
        newExchange = createExchangeWithBody(wrapper2);

    }

    @Test
    public void testServiceAggregationStrategy()
    {
        ServiceAggregationStrategy serviceAggregationStrategy = new ServiceAggregationStrategy();

        Exchange exchange = serviceAggregationStrategy.aggregate(oldExchange, newExchange);

        assertEquals(2, exchange.getIn().getBody(AssociatedServiceWrapper.class).getServices().size());
    }

}
