package com.baesystems.ai.lloydsregister.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AssociatedServiceWrapperTest
{

    @Test
    public void testGetter()
    {
        int serviceCatalogueIdentifier = 1;
        int serviceSchedulingRegime = 1;
        int occurenceNumber = 1;

        String assignedDate = "2000-01-01";
        String dueDate = "2000-01-01";
        String lowerRangeDate = "2000-01-01";
        String upperRangeDate = "2000-01-01";

        AssociatedService associatedService = new AssociatedService(serviceCatalogueIdentifier, serviceSchedulingRegime, occurenceNumber,
                assignedDate, dueDate, lowerRangeDate, upperRangeDate);

        List<AssociatedService> services = new ArrayList<>();
        services.add(associatedService);

        Byte hullIndicator = new Byte("1");

        AssociatedServiceWrapper associatedServiceWrapper = new AssociatedServiceWrapper(services);
        associatedServiceWrapper.setHullIndicator(hullIndicator);

        assertTrue(associatedServiceWrapper.getServices().containsAll(services));
        assertEquals(hullIndicator, associatedServiceWrapper.getHullIndicator());
    }

}
