package com.baesystems.ai.lloydsregister.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AssociatedServiceTest
{

    @Test
    public void testGetters()
    {
        int serviceCatalogueIdentifier = 1;
        int serviceSchedulingRegime = 1;
        int occurenceNumber = 1;

        String assignedDate = "2000-01-01";
        String dueDate = "2000-01-01";
        String lowerRangeDate = "2000-01-01";
        String upperRangeDate = "2000-01-01";

        AssociatedService associatedService = new AssociatedService(serviceCatalogueIdentifier, serviceSchedulingRegime, occurenceNumber,
                assignedDate, dueDate, lowerRangeDate, upperRangeDate);

        assertEquals(1, associatedService.getServiceCatalogueIdentifier());
        assertEquals(1, associatedService.getSchedulingRegime());
        assertEquals(new Integer(1), associatedService.getOccurenceNumber());

        assertEquals("2000-01-01", associatedService.getAssignedDate());
        assertEquals("2000-01-01", associatedService.getDueDate());
        assertEquals("2000-01-01", associatedService.getLowerRangeDate());
        assertEquals("2000-01-01", associatedService.getUpperRangeDate());
    }
}
