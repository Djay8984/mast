const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

/** Array of matches that should not be parsed, esLinted etc. */
const excludeList = [
  path.join(__dirname, 'node_modules'),
  path.join(__dirname, 'src', 'app', 'components', 'print', 'pdfmake2.min.js'),
  path.join(__dirname, 'src', 'app', 'components', 'print', 'vfs_fonts.js'),
  path.join(__dirname, 'src', 'lib', 'spellchecker', '**')
];

const config = {
  cache: true,
  devtool: 'eval-source-map',
  debug: true,

  entry: {
    'app': './src/app',
    'bootstrapper': './src/main',
    'css': './src/content/stylesheets'
  },

  output: {
    path: path.join(__dirname, '/build/'),
    filename: '[name].js',
    sourceMapFilename: '[name].js.map',
    chunkFilename: '[id].chunk.js'
  },

  devServer: {
    contentBase: 'build/',
    port: 9001,
    historyApiFallback: true,
    noInfo: true,
    debug: true,
    proxy: {
      '/mast/*': {
        target: 'http://localhost:8080'
      }
    }
  },

  resolve: {
    root: path.join(__dirname, 'src'),
    extensions: ['', '.js', '.json', '.css', '.scss', '.html'],
    packageMains: ['webpack', 'browser', 'web', 'browserify', ['jam', 'main'], 'main']
  },

  resolveLoader: {
    alias: {
      'unnecessary-JSON-Parse-Loader': path.join(__dirname, 'webpack', './unnecessary-JSON-Parse-Loader.js'),
      'consoler': path.join(__dirname, 'webpack', './consoler.js'),
      'disable-eslint': path.join(__dirname, 'webpack', './disable-eslint.js')
    }
  },

  babel: {
    presets: ['stage-0', 'es2015'],
    plugins: ['transform-runtime', 'transform-decorators-legacy']
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'source-map',
        exclude: excludeList
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        loaders: [
          'ng-annotate',
          'babel',
          'eslint'
        ],
        exclude: excludeList
      },

      // https://www.npmjs.com/package/font-awesome-webpack
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        exclude: path.join(__dirname, 'src/content/images/inline')
      },
      {
        test: /\.(scss|css)$/,
        loader: 'file?name=bundle.css!extract!css!resolve-url!sass'
      },
      {
        test: /\.html$/,
        loader: `ngtemplate?relativeTo=${__dirname}/src/!html?removeRedundantAttributes=false&interpolate`,
        exclude: [
          /node_modules/,
          path.join(__dirname, 'src', 'index.html')
        ]
      },
      { test: /semver\.browser\.js/, loaders: ['imports?define=>undefined'] },

      {
        test: /index\.html$/,
        loader: 'file',
        include: path.join(__dirname, 'src', 'index.html'),
        query: {
          name: '[name].[ext]'
        }
      },

      {
        test: /package\.json$/,
        loader: 'file',
        include: path.join(__dirname, 'package.json'),
        query: {
          name: '[name].[ext]'
        }
      },

      {
        test: /\.node$/,
        loader: 'node',
        include: path.join(__dirname, 'src', 'lib', 'spellchecker')
      },

      {
        test: /\.*$/,
        loader: 'disable-eslint!tojson!raw!val!babel',
        include: path.join(__dirname, 'src', 'config.js')
      },

      {
        test: /\.(jpg|jpeg|png|gif|svg)$/i,
        loader: 'file',
        include: path.join(__dirname, 'src/content/images/'),
        exclude: path.join(__dirname, 'src/content/images/inline/'),
        query: {
          name: 'images/[name].[ext]'
        }
      },

      {
        test: /\.json$/,
        loader: 'raw',
        include: path.join(__dirname, 'spec/fixtures/')
      },

      {
        test: /\.svg$/,
        loader: 'raw',
        include: path.join(__dirname, 'src/content/images/inline/')
      }
    ]
  },

  noParse: excludeList,

  externals: [
    {
      'web-frame': 'commonjs web-frame'
    }
  ],

  target: 'electron',

  node: {
    console: false,
    process: false,
    global: false,
    buffer: false,
    __filename: false,
    __dirname: false
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: 'extensions', to: 'extensions' }
    ])
  ]
};

if (~process.argv.indexOf('--dev-server')) {
  const electron = require('electron-prebuilt');
  const spawn = require('child_process').spawn;
  const devServer = path.join(
    __dirname, 'node_modules', 'webpack-dev-server',
    'bin', 'webpack-dev-server.js'
  );
  const wdsArgs = ['--hot', '--inline', '--colors'];
  const electronOpts = {
    stdio: 'inherit',
    env: {
      'WEBPACK_ENV': 'dev'
    }
  };

  spawn('node', [devServer].concat(wdsArgs), { stdio: 'inherit' }, (error) => {
    console.log(error);
  });

  spawn(electron, ['build'], electronOpts, (error) => {
    console.log(error);
  });
}

module.exports = config;
