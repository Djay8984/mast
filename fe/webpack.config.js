/* eslint-disable */
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HappyPack = require('happypack');


/** Array of matches that should not be parsed, esLinted etc. */
const excludeList = [
  path.join(__dirname, 'node_modules'),
  path.join(__dirname, 'src', 'app', 'components', 'print', 'pdfmake2.min.js'),
  path.join(__dirname, 'src', 'app', 'components', 'print', 'vfs_fonts.js')
];

const config = {
  cache: true,
  devtool: 'eval-source-map',
  debug: true,
  entry: {
    'brains': ['./src/app'],
    'beauty': ['./src/content/stylesheets/index.scss']
  },

  output: {
    path: __dirname + '/build',
    filename: '[name].js',
    sourceMapFilename: '[name].js.map',
    chunkFilename: '[id].chunk.js',
    publicPath: 'http://localhost:9001/build/'
  },

  devServer: {
    contentBase: './',
    port: 9001,
    historyApiFallback: true,
    noInfo: false,
    debug: true,
    proxy: {
      '/images/*': {
        target: '/src/content/images/'
      },
      '/mast/*': {
        target: 'http://localhost:8080'
      }
    }
  },

  resolve: {
    root: path.join(__dirname, 'src'),
    extensions: ['', '.js', '.json', '.css', '.scss', '.html'],
    packageMains: ['webpack', 'browser', 'web', 'browserify', ['jam', 'main'], 'main']
  },

  babel: {
    presets: ['stage-0', 'es2015'],
    plugins: ['transform-runtime', 'transform-decorators-legacy']
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'source-map',
        exclude: excludeList
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'happypack/loader?id=js' ],
        exclude: excludeList,
      },
      //https://www.npmjs.com/package/font-awesome-webpack
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&minetype=application/font-woff'
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        exclude: path.join(__dirname, 'src/content/images/inline')
      },
      { test: /semver\.browser\.js/, loaders: ['imports?define=>undefined'] },
      { test: /\.scss$/,
        loader: 'style-loader!css-loader?sourceMap!resolve-url!sass-loader?sourceMap'
      },
      {
        test: /\.html$/,
        loader: 'ngtemplate?relativeTo=' + __dirname + '/src/!html?removeRedundantAttributes=false&interpolate',
        exclude: [
          /node_modules/,
          path.join(__dirname, 'src', 'index.html')
        ]
      },

      {
        test: /index\.html$/,
        loader: 'file',
        include: path.join(__dirname, 'src', 'index.html'),
        query: {
          name: '[name].[ext]'
        }
      },

      {
        test: /package\.json$/,
        loader: 'file',
        include: path.join(__dirname, 'package.json'),
        query: {
          name: '[name].[ext]'
        }
      },

      {
        test: /\.*$/,
        loader: 'tojson!raw!val!babel',
        include: path.join(__dirname, 'src', 'config.json')
      },

      {
        test: /\.(jpg|jpeg|png|gif|svg)$/i,
        loader: 'file',
        include: path.join(__dirname, 'src/content/images/'),
        exclude: path.join(__dirname, 'src/content/images/inline/'),
        query: {
          name: 'images/[name].[ext]'
        }
      },

      {
        test: /\.(json|aff|dic)$/,
        loader: 'raw',
        include: path.join(__dirname, 'spec/fixtures/')
      },

      {
        test: /\.zip$/,
        loader: 'binary',
        include: path.join(__dirname, 'spec/fixtures/')
      },

      {
        test: /\.svg$/,
        loader: 'raw',
        include: path.join(__dirname, 'src/content/images/inline/')
      }
    ]
  },

  noParse: excludeList,

  externals: [
    {
      'web-frame': 'commonjs web-frame'
    }
  ],

  target: 'electron',

  node: {
    console: false,
    process: false,
    global: false,
    buffer: false,
    __filename: false,
    __dirname: false
  },

  plugins: [
    new CopyWebpackPlugin([
      { from: 'extensions', to: 'extensions' }
    ]),
    new HappyPack({
      id: 'js',
      threads: 4,
      loaders: [
        'ng-annotate',
        'babel?plugins[]=transform-runtime,plugins[]=transform-decorators-legacy,presets[]=stage-0,presets[]=es2015',
        'eslint'
      ]
    })
  ]
};

if (~process.argv.indexOf('--dev-server')) {
  const electron = require('electron-prebuilt');
  const spawn = require('child_process').spawn;
  const devServer = path.join(
    __dirname, 'node_modules', 'webpack-dev-server',
    'bin', 'webpack-dev-server.js'
  );
  const wdsArgs = ['--hot', '--inline', '--colors'];
  const electronOpts = {
    stdio: 'inherit',
    env: {
      'WEBPACK_ENV': 'dev'
    }
  };

  spawn('node', [devServer].concat(wdsArgs), { stdio: 'inherit' }, (error) => {
    console.log(error);
  });

  spawn(electron, ['.'], electronOpts, (error) => {
    console.log(error);
  });
}

module.exports = config;
