/**
   EXPERIMENTAL ALTERNATIVE
   for npm run dev, which doesn't break node/electron gracefully on windows.
   Run using:
   $ node webpack/run-dev.js
**/

const runAll = require('npm-run-all');

runAll(['start', 'dev-server'], {
  parallel: true,
  stdin: process.stdin,
  stdout: process.stdout,
  stderr: process.stderr
})
.then(() => {
  console.log('done!');
})
.catch(err => {
  console.log('failed!');
});
