var path = require('path');
var webpackConf = require('./webpack.config.js');

webpackConf.entry = {};
webpackConf.devtool = 'inline-source-map';
webpackConf.resolve.alias = {
  spec: path.join(__dirname, 'spec')
};
webpackConf.isparta = {
  embedSource: true,
  noAutoWrap: true,
  babel: webpackConf.babel
};
webpackConf.module.loaders.shift();
webpackConf.module.preLoaders = [
  {
    test: /\.js$/,
    include: path.resolve('spec'),
    loaders: [
      'babel',
      'eslint'
    ]
  },

  {
    test: /\.js$/,
    include: path.resolve('src'),
    loader: 'isparta'
  }
];

module.exports = function (config) {
  config.set({
    basePath: '.',

    client: {
      captureConsole: true
    },

    browsers: ['Electron'],

    frameworks: ['jasmine'],

    reporters: ['mocha', 'coverage'],

    files: [
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'spec/index.js'
    ],

    singleRun: false,

    autoWatch: true,

    browserConsoleLogOptions: {
      terminal: true
    },

    electronOpts: {
      show: false
    },

    preprocessors: {
      'spec/index.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConf,

    webpackMiddleware: {
      noInfo: true
    },

    coverageReporter: {
      dir: 'coverage',

      reporters: [
        { type: 'text-summary' },
        { type: 'html' },
        { type: 'json' },
        { type: 'cobertura' }
      ],

      instrumenterOptions: {
        istanbul: { noCompact: true }
      }
    }
  });
};
