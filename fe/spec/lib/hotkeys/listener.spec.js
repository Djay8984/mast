import 'spec/spec-helper';
import HotkeyListener from 'lib/hotkeys/listener';
import HotkeyMap from 'lib/hotkeys/map';
import { ipcRenderer as ipc } from 'electron';

describe('Hotkeys listener', () => {
  let eventListener, listener, map;

  beforeEach(() => {
    map = jasmine.createSpyObj('map', [ 'KeyBinding', 'UnknownHotkey' ]);
    map.hasOwnProperty = (val) => val === 'KeyBinding';

    spyOn(HotkeyMap, 'get').and.returnValue(map);
    spyOn(ipc, 'on').and.callFake((name, fn) => eventListener = fn);

    window.hasHotkeyListener = false;
    listener = new HotkeyListener();
  });

  describe('#listen', () => {
    beforeEach(() => listener.listen());

    describe('first call', () => {
      it('listens for hotkey events', () => {
        expect(ipc.on).toHaveBeenCalledWith('hotkey', jasmine.any(Function));
      });

      it('remembers it has been called', () => {
        expect(window.hasHotkeyListener).toBe(true);
      });
    });

    describe('subsequent calls', () => {
      beforeEach(() => {
        ipc.on.calls.reset();
        listener.listen();
      });

      it('does not add extra ipc listeners', () => {
        expect(ipc.on).not.toHaveBeenCalled();
      });
    });

    it('handles known messages', () => {
      eventListener(null, 'KeyBinding');

      expect(map.KeyBinding).toHaveBeenCalled();
    });

    it('ignores unknown messages', () => {
      eventListener(null, 'UnknownHotkey');

      expect(map.UnknownHotkey).not.toHaveBeenCalled();
    });
  });
});
