import 'spec/spec-helper';
import Keymap from 'lib/hotkeys/map';

describe('Hotkeys', () => {
  let map, webFrame;

  beforeEach(() => {
    webFrame = jasmine.createSpyObj('webFrame', [
      'setZoomLevel',
      'getZoomLevel'
    ]);
    webFrame.getZoomLevel.and.returnValue(0);

    map = Keymap.get(webFrame);
  });

  it('Ctrl-+ increases the text size', () => {
    map['CmdOrCtrl+Plus']();

    expect(webFrame.setZoomLevel).toHaveBeenCalledWith(1);
  });

  it('Ctrl-- decreases the text size', () => {
    map['CmdOrCtrl+-']();

    /* eslint no-magic-numbers: 0 */
    expect(webFrame.setZoomLevel).toHaveBeenCalledWith(-1);
  });

  it('Ctrl-0 resets the text size', () => {
    map['CmdOrCtrl+0']();

    expect(webFrame.setZoomLevel).toHaveBeenCalledWith(0);
  });
});
