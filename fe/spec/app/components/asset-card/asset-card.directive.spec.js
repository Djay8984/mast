import 'spec/spec-helper';
import services from 'app/services';
import assetCard from 'app/components/asset-card';

describe('New Asset Card directive', () => {
  let $compile;
  let element;
  let $rootScope;
  let caseService;
  let caseStatus;
  let compiled;
  let compiledEl;
  let buildInjections;

  beforeEach(() => {
    angular.mock.module(assetCard, ($provide) => {
      $provide.service('$state', () => true);
    });

    angular.mock.module(services, ($provide) => {
      caseService = jasmine.createSpyObj('caseService', ['isOpenStatus']);
      caseService.isOpenStatus.and.returnValue({});
      $provide.value('caseService', caseService);

      const caseStatuses = {
        uncommitted: 1,
        populate: 2,
        onHold: 3,
        jobPhase: 4,
        validateAndUpdate: 5,
        closed: 6,
        cancelled: 7,
        deleted: 8,
        takeOffHold: 9
      };
      caseStatus = {
        get: (field) => caseStatuses[field],
        toObject: () => caseStatuses
      };
      $provide.value('caseStatus', caseStatus);
    });

    buildInjections = () => {
      inject((_$rootScope_, _$compile_) => {
        $rootScope = _$rootScope_;
        $compile = _$compile_;

        element = '<div><div asset-card asset="asset"><asset-card-cta></asset-card-cta></div></div>';
        compiled = $compile(element)($rootScope);
        compiledEl = compiled.find('div');
        $rootScope.$digest();
      });
    };

    buildInjections();
  });

  it('should have applied template', async () => {
    expect(compiledEl.html()).not.toEqual('');
  });
});
