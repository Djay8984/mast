import 'spec/spec-helper';
import services from 'app/services';

import AssetCardController from
  'app/components/asset-card/asset-card.controller';

describe('Asset Card Controller', () => {
  let vm;
  let $stateMock;
  let $element;
  let caseService;
  let caseStatus;
  let assetMock;
  let buildServices;
  let buildInjections;

  beforeEach(async () => {
    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        caseService = jasmine.createSpyObj('caseService', ['isOpenStatus']);
        caseService.isOpenStatus.and.returnValue(true);
        $provide.value('caseService', caseService);
      });
    };
    buildInjections = () => {
      inject((_$controller_) => {
        $stateMock = jasmine.createSpyObj('$state', ['go']);
        $stateMock.go.and.callFake(() => true);

        const caseStatuses = {
          uncommitted: 1,
          populate: 2,
          onHold: 3,
          jobPhase: 4,
          validateAndUpdate: 5,
          closed: 6,
          cancelled: 7,
          deleted: 8,
          takeOffHold: 9
        };
        caseStatus = {
          get: (field) => caseStatuses[field],
          toObject: () => caseStatuses
        };

        $element = {};

        assetMock = {
          id: 1
        };

        vm = _$controller_(AssetCardController,
          {
            $state: $stateMock,
            $element,
            caseService,
            caseStatus
          },
          {
            asset: assetMock
          });
      });
    };

    buildServices();
    buildInjections();
  });

  /* eslint no-loop-func: 0 */
  const methods = ['cases', 'viewCase'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  describe('Return Values', () => {
    it('"cases" should return an array', async () => {
      const cases = await vm.cases;
      expect(angular.isArray(cases)).toBe(true);
    });
  });

  it('viewing an open case should move page to details', async () => {
    // Open statuses: 1-5
    const assetCase = {
      caseStatus: {
        id: 1
      }
    };
    await vm.viewCase(assetCase);
    expect(caseService.isOpenStatus).toHaveBeenCalled();
    expect($stateMock.go).toHaveBeenCalledWith('asset.cases.view.details',
      jasmine.any(Object), jasmine.any(Object));
  });

  it('viewing a closed case should move page to milestones', async () => {
    const assetCase = {
      caseStatus: {
        id: 7
      }
    };
    caseService.isOpenStatus.and.returnValue(false);
    await vm.viewCase(assetCase);
    expect(caseService.isOpenStatus).toHaveBeenCalled();
    expect($stateMock.go).toHaveBeenCalledWith('asset.cases.view.milestones',
      jasmine.any(Object), jasmine.any(Object));
  });
});
