import 'spec/spec-helper';
import dropdown from 'app/components/dropdown';

describe('dropdown.service', () => {
  const _dropdowns = [];
  let $rootScope, dropdownService, element, scope;

  beforeEach(angular.mock.module(dropdown));

  beforeEach(inject((_dropdownService_, _$rootScope_, $compile) => {
    dropdownService = _dropdownService_;
    $rootScope = _$rootScope_;

    scope = $rootScope.$new();
    scope.value = true;
    element = $compile(`<div dropdown-select="vm.ddSelectOptions"
      dropdown-model="vm.ddSelectSelected"
      dropdown-item-label="text"
      dropdown-align="left"></div>`)(scope);
    $rootScope.$digest();
  }));

  it('on starting: should have an empty _dropdowns array (nothing "active")',
    () => {
      expect(_dropdowns).toBeDefined();
      expect(_dropdowns.length).toBeLessThan(1);
    });

  it('on clicking dropdown, should have "active" class', () => {
    expect(element.hasClass('active')).toBe(false);
    element.triggerHandler('click');
    $rootScope.$digest();
    expect(element.hasClass('active')).toBe(true);
  });

  it('on clicking away, should not have "active" class present', () => {
    angular.element(document.body).triggerHandler('click');
    expect(element.hasClass('active')).toBe(false);
  });

  it('should have a "register" function', () => {
    expect(angular.isFunction(dropdownService.register)).toBe(true);
  });

  it('should have an "unregister" function', () => {
    expect(angular.isFunction(dropdownService.unregister)).toBe(true);
  });

  it('should have an "toggleActive" function', () => {
    expect(angular.isFunction(dropdownService.toggleActive)).toBe(true);
  });

  it('should have a "clearActive" function', () => {
    expect(angular.isFunction(dropdownService.clearActive)).toBe(true);
  });

  it('should have an "isActive" function', () => {
    expect(angular.isFunction(dropdownService.isActive)).toBe(true);
  });
});
