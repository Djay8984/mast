import 'spec/spec-helper';
import dropdown from 'app/components/dropdown';

describe('Select directive', () => {
  let $compile, $outerScope, $rootScope, element, scope;

  beforeEach(() => {
    angular.mock.module(dropdown);

    inject((_$compile_, _$rootScope_) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $outerScope = $rootScope.$new();

      element = $compile(`<div dropdown-select="ddSelectOptions"
        dropdown-model="ddSelectSelected"
        dropdown-item-label="text"
        dropdown-align="left">
      </div>`)($outerScope);

      scope = element.scope();

      scope.$digest();
    });
  });

  it('replaces the element with the appropriate template content', () => {
    expect(element.html()).toContain('<span class="selected');
  });

  describe('Initial status', () => {
    it('if set in controller, should have a value already selected', () => {
      $outerScope.ddSelectSelected = {
        text: 'Please select an option:'
      };

      $rootScope.$digest();

      expect(element.find('span.selected').text()).toBe($outerScope.ddSelectSelected.text);
    });

    it('if two options are set, two dropdown should show when clicked', () => {
      $outerScope.ddSelectOptions = [
        {
          text: 'Option 17 - Long Day',
          value: 'Some value'
        },
        {
          text: 'HMS Pinnafore',
          value: 'Another value'
        }
      ];

      $rootScope.$apply();

      element.triggerHandler('click');

      $rootScope.$apply();

      expect(element.hasClass('active')).toBe(true);

      expect(element.find('ul.dropdown li').length).toEqual(2);
    });
  });
});
