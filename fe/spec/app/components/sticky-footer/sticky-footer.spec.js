import 'spec/spec-helper';
import stickyFooter from 'app/components/sticky-footer';

describe('Sticky footer directive', () => {
  let $rootScope, element;

  beforeEach(() => {
    angular.mock.module(stickyFooter);

    inject((_$rootScope_, $compile) => {
      $rootScope = _$rootScope_;
      element = $compile('<div><div data-sticky-footer>This is some content</div></div>')($rootScope);
      $rootScope.$digest();
    });
  });

  it('should contain content', () => {
    const angularElement = angular.element(
      element[0].getElementsByClassName('sticky-footer'));
    expect(angularElement).not.toBeNull();
  });
});
