import * as _ from 'lodash';
import services from 'app/services';
import 'spec/spec-helper';
import 'angular-moment';

import CaseMilestoneListController
  from 'app/components/case-milestone-list/case-milestone-list.controller';

describe('Case Milestone List', () => {
  let controller;
  let $log;
  let $scope;
  let rootScope;
  let caseMock;
  let dateFormatMock;
  let dueStatusMock;
  let milestoneMock;
  let milestoneService;
  let milestoneDueDateReference;
  let buildInjections;
  let buildServices;

  beforeEach(() => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(($exceptionHandlerProvider) => {
        $exceptionHandlerProvider.mode('log');
      });
      angular.mock.module(services, ($provide) => {
        milestoneService =
          jasmine.createSpyObj('milestoneService', ['saveDueDate']);
        milestoneService.saveDueDate.and.returnValue({});
        $provide.value('milestoneService', milestoneService);
      });
    };

    buildInjections = () => {
      inject((_$controller_, _$rootScope_, _$log_) => {
        rootScope = _$rootScope_;
        $log = _$log_;

        $scope = rootScope.$new();

        dateFormatMock = 'DD MMM YYYY';

        const dueStatuses = {
          notDue: 1,
          due: 2,
          overdue: 3
        };
        dueStatusMock = {
          get: (field) => dueStatuses[field]
        };

        caseMock = {
          id: 1,
          surveyors: [
            {
              id: 2,
              surveyor: {
                name: 'Jing Yu'
              },
              employeeRole: {
                id: 4
              }
            },
            {
              id: 3,
              surveyor: {
                name: 'Allan Hyde'
              },
              employeeRole: {
                id: 5
              }
            }
          ]
        };

        milestoneMock = [
          {
            id: 1,
            inScope: true,
            completionDate: '2016-09-02',
            dueDate: '2017-09-01',
            milestone: {
              id: 1,
              owner: {
                name: 'Joe Bloggs'
              },
              childMilestones: [
                {
                  id: 2
                }
              ],
              mandatory: true,
              predecessorMilestones: [ ]
            }
          },
          {
            id: 2,
            inScope: true,
            completionDate: '2016-09-07',
            dueDate: '2017-09-01',
            milestone: {
              id: 2,
              owner: {
                name: 'John Smith'
              },
              childMilestones: [
                {
                  id: 3
                }
              ],
              mandatory: true,
              predecessorMilestones: [
                {
                  id: 1
                }
              ]
            }
          },
          {
            id: 3,
            inScope: true,
            completionDate: '2016-09-07',
            dueDate: '2017-09-01',
            milestone: {
              id: 3,
              owner: {
                name: 'David Cameron'
              },
              childMilestones: [ ],
              mandatory: false,
              predecessorMilestones: [
                {
                  id: 2
                }
              ]
            }
          }
        ];

        const milestoneDueDates = {
          caseAcceptenceDate: 1,
          estimatedBuildDate: 2,
          milestoneCompletion: 3,
          manualEntry: 4
        };
        milestoneDueDateReference = {
          get: (field) => milestoneDueDates[field]
        };

        controller = _$controller_(CaseMilestoneListController,
          {
            dateFormat: dateFormatMock,
            dueStatus: dueStatusMock,
            milestoneService,
            milestoneDueDateReference,
            $scope
          },
          {
            case: caseMock,
            dueStatus: dueStatusMock,
            milestone: milestoneMock,
            milestones: milestoneMock
          }
        );
      });
    };

    buildServices();
    buildInjections();
  });

  it('should know when a milestone is overdue', async () => {
    rootScope.$digest();

    const milestone = {
      dueStatus: {
        id: 1
      }
    };

    // Check the status, it should be false;
    let isMilestoneDue = controller.isOverdue(milestone);
    expect(isMilestoneDue).toBe(false);

    // Change the status of the milestone, and expect true;
    milestone.dueStatus.id = 3;
    isMilestoneDue = controller.isOverdue(milestone);
    expect(isMilestoneDue).toBe(true);
  });

  it('knows who a milestone\'s owner is', async () => {
    let name = await controller.getOwner(_.first(controller.milestone));
    expect(name).toBe('Joe Bloggs');
    _.set(_.first(controller.case.surveyors), 'employeeRole.id', 1);
    name = await controller.getOwner(_.first(controller.milestone));
    expect(name).toBe('Jing Yu');
  });

  it('should recursively check milestones for mandatory children',
    async () => {
      let mandatory =
        controller.hasMandatoryChildren(_.first(controller.milestones), true);
      expect(mandatory).toBe(true);

      // Now set mandatory to false to trigger the if() statement;
      mandatory =
        controller.hasMandatoryChildren(_.last(controller.milestones), false);
      expect(mandatory).toBe(false);
    });

  it('should set due date correctly', async () => {
    $scope.editDueDate = null;
    await controller.setDueDate(10);
    expect(await controller.getDueDate()).toEqual(10);

    // Since setDueDate can only be called once, this last test should fail;
    await controller.setDueDate(15);
    expect(await controller.getDueDate()).not.toEqual(15);
  });

  it('should save due date correctly', async () => {
    await controller.saveDueDate(_.first(controller.milestones),
      '2016-09-08');
    expect($log.error.logs[0]).toEqual(['Milestone save error']);
  });

  it('should get the attachment path correctly', async () => {
    const path = await controller.getAttachmentPath(1);
    expect(path).toEqual('case/1/milestone/1');
  });

  it('should get the manual entry due date correctly', async () => {
    const manual = await controller.dueDateManual;
    expect(manual).toEqual(4);
  });

  it('should get the date format correctly', async () => {
    const dFormat = await controller.dateFormat;
    expect(dFormat).toEqual('DD MMM YYYY');
  });

  it('should enable the checkboxes', async () => {
    spyOn(controller, 'hasMandatoryChildren').and.callFake(() => true);
    controller.manage = true;
    const milestone = _.first(controller.milestones);
    const enabled = await controller.enableCheckbox(milestone);
    expect(controller.hasMandatoryChildren)
      .toHaveBeenCalledWith(milestone, milestone.milestone.mandatory);
    expect(enabled).toBe(false);
  });
});
