import 'spec/spec-helper';
import datePicker from 'app/components/datepicker';
import 'angular-moment';

describe('Date Picker Utility Service', () => {
  let $rootScope;
  let datePickerUtils;
  let buildInjections;

  beforeEach(() => {
    angular.mock.module('angularMoment');
    angular.mock.module(datePicker);

    buildInjections = () => {
      inject((_$rootScope_, _datePickerUtils_) => {
        $rootScope = _$rootScope_;
        datePickerUtils = _datePickerUtils_;
      });
    };

    buildInjections();
  });

  const myFunctions = ['createNewDate', 'getVisibleMinutes', 'getVisibleWeeks',
    'getVisibleYears', 'getDaysOfWeek', 'getVisibleMonths', 'getVisibleHours',
    'isAfter', 'isBefore', 'isSameYear', 'isSameMonth', 'isSameDay',
    'isSameHour', 'isSameMinutes', 'setParams', 'scopeSearch', 'findFunction',
    'findParam', 'createMoment', 'getDate', 'eventIsForPicker'];

  for (let i = 0; i < myFunctions.length; i += 1) {
    /* eslint no-loop-func: 0 */
    it(`should have a "${myFunctions[i]}" function`, async () => {
      expect(`$rootScope.${myFunctions[i]}`).toBeDefined();
    });
  }
});
