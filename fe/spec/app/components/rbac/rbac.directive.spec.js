import 'spec/spec-helper';

import rbac from 'app/components/rbac';

describe('rbac.directive', () => {
  let element = null;
  let scope = null;
  let rbacServiceSpy = null;

  /**
   * @param {object} fixture options to pass to the directive
   * @param {boolean} digest whether or not to follow setup with scope digest
   * @return {function} setup method for use in a beforeEach
   */
  function setup(fixture, digest = false) {
    return () => {
      angular.mock.module(rbac, ($provide) => {
        rbacServiceSpy = jasmine.createSpyObj('rbacService', ['can']);
        $provide.value('rbacService', rbacServiceSpy);
      });

      inject((_$compile_, _$rootScope_) => {
        scope = _$rootScope_.$new();
        element = _$compile_(
          `<div data-rbac=${JSON.stringify(fixture)}></div>`)(scope);

        if (digest) {
          scope.$digest();
        }
      });
    };
  }

  describe('groups & action', () => {
    const rbacOptionsFixture = {
      groups: ['admin'],
      action: 'createAssetModel'
    };

    beforeEach(setup(rbacOptionsFixture, true));

    it('should be evaluated using rbac service', () => {
      expect(rbacServiceSpy.can).toHaveBeenCalledWith(
        rbacOptionsFixture.groups, rbacOptionsFixture.action);
    });
  });

  describe('disabling', () => {
    const rbacOptionsFixture = {
      groups: ['admin'],
      action: 'createAssetModel',
      disable: true
    };

    beforeEach(setup(rbacOptionsFixture));

    describe('rbac valid', () => {
      beforeEach(() => {
        rbacServiceSpy.can.and.returnValue(true);
        scope.$digest();
      });

      it('should not be disabled', () =>
        expect(element.prop('disabled')).toBeFalsy());
    });

    describe('rbac invalid', () => {
      beforeEach(() => {
        rbacServiceSpy.can.and.returnValue(false);
        scope.$digest();
      });

      it('should be disabled', () =>
        expect(element.prop('disabled')).toBe(true));
    });
  });

  describe('hiding', () => {
    const rbacOptionsFixture = {
      groups: ['admin'],
      action: 'createAssetModel',
      disable: false
    };

    beforeEach(setup(rbacOptionsFixture));

    describe('rbac valid', () => {
      beforeEach(() => {
        rbacServiceSpy.can.and.returnValue(true);
        scope.$digest();
      });

      it('should not be hidden', () =>
        expect(element.css('display')).not.toEqual('none'));
    });

    describe('rbac invalid', () => {
      beforeEach(() => {
        rbacServiceSpy.can.and.returnValue(false);
        scope.$digest();
      });

      it('should be hidden', () =>
        expect(element.css('display')).toEqual('none'));
    });
  });
});
