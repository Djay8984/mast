describe('Checkbox button', () => {
  let element;
  let chkGrpElement;
  let scope;

  beforeEach(inject(($compile, $rootScope) => {
    scope = $rootScope.$new();
    scope.value = true;
    element = $compile('<input type="checkbox" role="checkbox">')(scope);
    chkGrpElement = element;
  }));

  it('should be a checkbox', () => {
    // Checkboxes should have the role='checkbox' attribute;
    expect(chkGrpElement.eq(0).attr('role')).toEqual('checkbox');
  });
});
