describe('Radio button', () => {
  it('should be a radio button', inject(($compile, $rootScope) => {
    const element = $compile('<input type="radio" role="radio">')($rootScope);
    const radioGrpElement = element;

    // Radio buttons should have the role='radio' attribute;
    expect(radioGrpElement.eq(0).attr('role')).toEqual('radio');
  }));
});
