import 'spec/spec-helper';
import checkbox from 'app/components/checkbox';

describe('Checkbox directive', () => {
  let $compile, $rootScope, element, scope;

  beforeEach(() => {
    angular.mock.module(checkbox);

    inject((_$compile_, _$rootScope_) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;

      compile({});
    });
  });

  function compile(model) {
    scope = $rootScope.$new();

    Object.assign(scope, model);

    element = $compile(`<div class="checkbox-container">
      <label for="checkbox-name">Label Before: Truth or Lies, size "largest"</label>
      <checkbox
        ng-model="checkboxModel[0]"
        name="checkbox-name"
        id="checkbox-name"
        ng-true-value="foo"
        ng-false-value="not-foo"
        largest></checkbox>
      </div>`)(scope);

    scope = element.scope();

    scope.$digest();
  }

  it('replaces the checkbox element with the appropriate template content', () => {
    expect(element.find('button').hasClass('checkbox-dir')).toBe(true);
  });

  xdescribe('Initial status', () => {
    it('if set to true in controller, should have a value already selected', () => {
      compile({ checkboxModel: [ 'foo' ] });

      expect(element.find('button').hasClass('checked')).toBe(true);
    });

    it('should set unchecked when clicked', () => {
      compile({ checkboxModel: [ 'foo' ] });

      expect(element.find('button').hasClass('checked')).toBe(true);

      element.find('button').triggerHandler('click');

      $rootScope.$digest();

      expect(element.find('button').hasClass('checked')).not.toBe(true);

      expect(scope.checkboxModel).toEqual([ 'not-foo' ]);
    });
  });
});
