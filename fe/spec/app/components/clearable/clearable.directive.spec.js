import 'spec/spec-helper';
import clearable from 'app/components/clearable';

describe('Clearable directive', () => {
  let $compile;
  let $rootScope;
  let $timeout;
  let element;
  let scope;
  let buttonScope;

  beforeEach(() => {
    angular.mock.module(clearable);

    inject((_$compile_, _$rootScope_, _$timeout_) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $timeout = _$timeout_;
      scope = $rootScope.$new();
      element = $compile(`<div><input data-clearable type="text" ng-model="clearableModel" id="team-job-sdo"></div>`)(scope);
      buttonScope = element.find('button').scope();

      scope.$digest();
    });
  });

  it('data-clearable should add the pos-rel class to the parent of the input',
    () => {
      expect(element.hasClass('pos-rel')).toBe(true);
    }
  );

  it('clear button is added', () => {
    expect(element.find('button')).toEqual(jasmine.any(Object));
  });

  describe('"clear" function', () => {
    it('has "clear" function in directive link', () => {
      expect(angular.isFunction(buttonScope.clear)).toBe(true);
    });

    it('"clear" removes typeahead-clearable', () => {
      element.find('button').triggerHandler('click');
      expect(element.find('input').hasClass('typeahead-clearable')).toBe(false);
    });

    it('"clear" function clears value from input', () => {
      element.find('input').val('test value');
      element.find('button').triggerHandler('click');
      expect(element.find('input').val()).toBe('');
    });

    it('"clear" function re-enables the input', () => {
      element.find('input').prop('disabled', true);
      element.find('button').triggerHandler('click');
      expect(element.find('input').prop('disabled')).toBe(false);
    });
  });

  describe('clear button ngHide', () => {
    it('clear button is shown when the ngModel.$modelValue has a value',
      () => {
        element.find('input').val('test value').triggerHandler('input');
        $timeout.flush();
        expect(buttonScope.hideClear).toBe(false);
      }
    );

    it('clear button is hidden when the ngModel.$modelValue is empty string',
      () => {
        element.find('input').val('').triggerHandler('input');
        $timeout.flush();
        expect(buttonScope.hideClear).toBe(true);
      }
    );
  });
});
