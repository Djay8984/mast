import services from 'app/services';
import checkIn from 'app/components/check-in';

describe('CheckIn directive', () => {
  let $compile, $rootScope, element, scope, vm;
  let $state, pageService;
  let apiMock, checkedInAsset, checkedOutAsset, modal;

  beforeEach(() => {
    apiMock = [{ name: 'Test', id: 5 }, { name: 'Other test', id: 8 }];
    checkedInAsset = { id: 7, name: 'Some ship' };
    checkedOutAsset = { id: 8, name: 'Some other ship' };
    modal = false;

    angular.mock.module(checkIn);
    angular.mock.module(services, ($provide) => {
      const stateMock = jasmine.createSpyObj('stateMock', ['go']);
      stateMock.go.and.callFake((name, data) => {
        stateMock.data = data;
        stateMock.name = name;
      });
      stateMock.current = {};

      pageService = jasmine.createSpyObj('pageService', ['getCheckedOut']);
      pageService.getCheckedOut.and.returnValue({});
      $provide.value('pageService', pageService);

      $provide.value('$state', stateMock);
      $provide.value('$stateParams', { assetId: 1 });
      $provide.value('assetDecisionService',
        { checkedOut: () => apiMock });
      $provide.value('MastModalFactory', () => (
        {
          activate: () => {
            modal = true; return 'save';
          }
        }));
    });

    inject((_$compile_, _$rootScope_, _$controller_, _$state_, _pageService_) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      pageService = _pageService_;
      $state = _$state_;

      compile();
    });
  });

  function compile(model) {
    scope = $rootScope.$new();
    element = $compile('<check-in></check-in>')(scope);
    scope = element.scope();
    scope.$digest();
    vm = scope.checkIn;
  }

  xit('Should update checked out list when get new data', async () => {
    expect(vm.list.length).toBe(0);
    expect(scope.checkIn.list.length).toBe(0);
    await pageService.getCheckedOut();
    expect(vm.list.length).toBe(apiMock.length);
  });

  xit('Should open modal and call checkIn method in pageService', async () => {
    expect(modal).not.toBe(true);
    spyOn(pageService, 'checkIn');
    await vm.checkIn(checkedOutAsset);
    expect(modal).toBe(true);
    expect(pageService.checkIn).toHaveBeenCalled();
  });

  xit('Should open modal and call discard method in pageService', async () => {
    expect(modal).not.toBe(true);
    spyOn(pageService, 'checkIn');
    await vm.discard(checkedInAsset);
    expect(modal).toBe(true);
    expect(pageService.checkIn).toHaveBeenCalled();
  });

  it('Should navigate to asset', () => {
    vm.openAsset(checkedOutAsset);
    expect($state.go).toHaveBeenCalled();
    expect($state.data.assetId).toBe(checkedOutAsset.id);
  });
});
