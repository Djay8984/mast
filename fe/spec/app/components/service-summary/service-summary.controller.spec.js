import * as _ from 'lodash';
import 'spec/spec-helper';
import 'angular-moment';

import statusConstants from 'app/constants/status.constants';
import typeConstants from 'app/constants/type.constants';

import services from 'app/services';
import ServiceModel from 'app/models/service.model';
import ServiceSummaryController from
  'app/components/service-summary/service-summary.controller';

describe('Service Summary Controller', async () => {
  let vm;
  let stateMock;
  let $rootScope;
  let scopeMock;
  let moment;
  let dueStatus;
  let productType;

  const id = _.random(100);

  beforeEach(async () => {
    // add modules to injector
    angular.mock.module('angularMoment');
    angular.mock.module(statusConstants);
    angular.mock.module(typeConstants);

    angular.mock.module(services, ($provide) => {
      stateMock = jasmine.createSpyObj('$state spy', ['go']);
      stateMock.go.and.callFake(() => true);
      $provide.value('$state', stateMock);
    });

    inject((_$controller_, _$rootScope_, _moment_, _dueStatus_, _productType_) => {
      $rootScope = _$rootScope_;

      moment = _moment_;

      // constants
      dueStatus = _dueStatus_;
      productType = _productType_;

      vm = _$controller_(ServiceSummaryController,
        {
          $state: stateMock,
          moment,
          dueStatus,
          productType
        },
        {
          serviceModel: new ServiceModel({
            id,
            dueStatus: {},
            serviceCreditStatus: {
              name: 'molly'
            },
            rangeStatus: {
              name: 'SS'
            }
          }),
          serviceCatalogueModel: {
            model: {
              code: 'SS'
            }
          }
        });
    });
  });

  /* eslint no-loop-func: 0 */
  const getters = ['productTypeName', 'isOverDue'];
  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  const methods = ['viewService', 'displayClassificatoinServiceStatus',
    'displayStatutoryServiceType', 'displayCreditingStatus',
    'displayProvisionalStatus', 'displaySpecialServiceStatus',
    'displayRangeStatus'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  describe('isOverdue', () => {
    it('should return TRUE when the service is overdue', () => {
      _.set(vm.serviceModel, 'model.dueStatus.id', dueStatus.get('overdue'));
      expect(vm.isOverdue).toBeTruthy();
    });

    it('should return FALSE if the service is not overdue', () => {
      _.set(vm.serviceModel, 'model.dueStatus.id', dueStatus.get('notDue'));
      expect(vm.isOverdue).toBeFalsy();
    });
  });

  describe('viewService', () => {
    it('should call $state.go', () => {
      vm.viewService();
      expect(stateMock.go)
        .toHaveBeenCalledWith('asset.serviceSchedule.viewService',
          jasmine.any(Object));
    });
  });

  describe('Helper Functions', () => {
    it('getter "productTypeName" should return a string', async () => {
      expect(angular.isString(await vm.productTypeName)).toBe(true);
    });
    it('"viewService" should move the page on', async () => {
      await vm.viewService();
      expect(stateMock.go)
        .toHaveBeenCalledWith('asset.serviceSchedule.viewService',
          jasmine.any(Object));
    });
    it('"displayClassificatoinServiceStatus" should return a string', async () => {
      expect(angular.isString(await vm.displayClassificationServiceStatus())).toBe(true);
    });
    it('"displayStatutoryServiceType" should return a string', async () => {
      expect(angular.isString(await vm.displayStatutoryServiceType())).toBe(true);
    });
    it('"displayCreditingStatus" should return a string', async () => {
      const result = await vm.displayCreditingStatus();
      expect(angular.isString(result)).toBe(true);
      expect(result).toEqual('molly');
    });
    it('"displayProvisionalStatus" should return a string', async () => {
      expect(angular.isString(await vm.displayProvisionalStatus())).toBe(true);
    });
    it('"displaySpecialServiceStatus" should return a string', async () => {
      expect(await vm.displaySpecialServiceStatus()).toBeTruthy();
      vm.serviceCatalogueModel.model.code = 'CSS';
      expect(await vm.displaySpecialServiceStatus()).toBeFalsy();
    });
  });
});
