import 'spec/spec-helper';
import breadcrumbs from 'app/components/breadcrumbs';
import BreadcrumbsController from 'app/components/breadcrumbs/breadcrumbs.controller';

describe('Breadcrumbs directive', () => {
  let $compile;
  let $element;
  let $rootScope;
  let $outerScope;
  let $scope;
  let controller;
  let ctrl;

  beforeEach(() => {
    angular.mock.module(breadcrumbs);

    inject((_$compile_, _$rootScope_, $controller) => {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $outerScope = $rootScope.$new();

      $outerScope.model = { };

      controller =
        $controller(BreadcrumbsController, { $scope: $outerScope }, true);

      $element =
        $compile(`<breadcrumbs
          data-selected-item="model"></breadcrumbs>`)($outerScope);

      $rootScope.$digest();
      $scope = $element.scope();
    });
  });

  const createModel = (inputModel) => {
    Reflect.defineProperty(inputModel, 'parents', {
      get: () => {
        const parents = [];
        let item = inputModel;

        while (item.parent) {
          parents.push(item);
          item = item.parent;
        }

        parents.push(item);

        return parents;
      }
    });

    $outerScope.model = inputModel;
    $rootScope.$digest();
  };

  // We look for a second menu item;
  it('should have a horizontal menu structure', () => {
    expect($element.find('ul').length).not.toBe(null);
    expect($element.find('li').length).toBeLessThan(2);
  });

  it('has a controller', () => {
    expect(controller).toBeDefined();
    expect(controller).not.toBeNull();
  });

  it('can set readOnly from the scope', () => {
    controller.instance.readOnly = true;

    ctrl = controller();

    expect(ctrl.readOnly).toBe(true);
  });

  it('can set preAmble from the scope', () => {
    controller.instance.preAmble = 'You are here: ';

    ctrl = controller();

    expect(ctrl.preAmble).toBe('You are here: ');
  });

  xdescribe('with a single menu item', () => {
    beforeEach(() => {
      createModel({
        id: 0,
        select: () => Object.create(),
        model: { name: 'Vessel' }
      });
    });

    // This should get the first li menu item then check for an <a> link;
    it('should not have a link on the first menu item', () => {
      const firstEl = $element.find('ul li:first-child');

      // It should exist;
      expect(firstEl).toBeTruthy();

      // And it should not have a link;
      expect(firstEl.find('span')).toBeTruthy();
    });
  });

  describe('with multiple menu items', () => {
    beforeEach(() => {
      createModel({
        id: 1,
        select: () => Object.create(),
        model: { name: 'Vessel' },
        parent: {
          id: 2,
          select: () => Object.create(),
          model: { name: 'Other Ship' }
        }
      });
    });

    it('should have a link on the first menu item', () => {
      expect($element.find('li:first-child a')).toBeTruthy();
    });

    it('should not have a link on the last menu item', () => {
      expect($element.find('li:last-child span')).toBeTruthy();
    });

    xit('should have a working click handler', () => {
      const firstEl = $element.find('li:first-child a');

      expect($scope.vm.selectedItem.model.name).toEqual('Vessel');

      firstEl.triggerHandler('click');

      $rootScope.$digest();

      expect($scope.vm.selectedItem.model.name).toEqual('Other Ship');
    });
  });
});
