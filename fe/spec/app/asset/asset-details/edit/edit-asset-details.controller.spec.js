import * as _ from 'lodash';
import services from 'app/services';
import 'spec/spec-helper';
import EditAssetDetailsController
  from 'app/asset/asset-details/edit/edit-asset-details.controller.js';

describe('EditAssetDetailsController', () => {
  let buildServices;
  let buildInjections;
  let referenceDataMock;
  let ihsService;
  let mockIHS;
  let mockAsset;
  let $controller;
  let moment;
  let typeaheadService;
  let caseService;
  let pageService;
  let assetOnlineService;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    const stateMock = jasmine.createSpyObj('stateMock', ['go']);

    mockAsset = {
      id: 123,
      ihsAsset: { id: 456 },
      classStatus: null,
      effectiveDate: null
    };

    buildServices = (errorThrown = false) => {
      angular.mock.module(services, ($provide) => {
        mockIHS = {
          id: mockAsset.ihsAsset.id,
          name: 'I am a ship'
        };
        ihsService = jasmine.createSpyObj('ihsService', ['get']);
        ihsService.get.and.returnValue(Promise.resolve(mockIHS));
        $provide.value('ihsService', ihsService);
        $provide.value('$state', stateMock);
        $provide.value('$stateParams', { assetId: mockAsset.id });
        $provide.value('appConfig', { });
        $provide.value('settings', { });
        $provide.value('imoNumberLength', 7);

        caseService = jasmine.createSpyObj('caseService', ['forEmployee']);
        $provide.value('caseService', caseService);

        pageService =
          jasmine.createSpyObj('pageService', ['checkIn', 'checkOut']);
        $provide.value('pageService', pageService);

        assetOnlineService =
          jasmine.createSpyObj('assetOnlineService', ['update']);
        $provide.value('assetOnlineService', assetOnlineService);

        $provide.value('dateFormat', 'DD MMM YYYY');
        $provide.value('Restangular', jasmine.createSpyObj('Restangular', ['one', 'get']));
        $provide.value('MastModalFactory', () => ({ activate: () => 'save' }));

        typeaheadService =
          jasmine.createSpyObj('typeaheadService', ['query']);
        typeaheadService.query.and.returnValue([]);
        $provide.value('typeaheadService', typeaheadService);
      });
    };

    buildInjections = () => {
      inject((_$controller_, _$state_, _moment_) => {
        // $state = _$state_;

        $controller = _$controller_;
        moment = _moment_;
      });

      referenceDataMock = {
        asset: { }
      };
    };

    buildServices();
    buildInjections();
  });

  const makeController = (injectData) => $controller(
    EditAssetDetailsController,
    injectData
  );

  it('make IHS in fully loaded', async () => {
    const controller = makeController({
      referenceData: referenceDataMock,
      asset: mockAsset
    });

    expect(ihsService.get).toHaveBeenCalledWith(mockAsset.ihsAsset.id);
    expect(_.get(controller, 'ihsAsset')).not.toBeUndefined();
  });

  it('check class status is changed from init', () => {
    mockAsset.classStatus = null;
    const controller = makeController({
      referenceData: referenceDataMock,
      asset: mockAsset
    });

    expect(controller.isClassStatusChanged()).toBeFalsy();
    mockAsset.classStatus = { id: 2 };
    expect(controller.isClassStatusChanged()).toBeTruthy();
    mockAsset.classStatus = null;
    expect(controller.isClassStatusChanged()).toBeFalsy();
  });

  it('check class status is changed from status to another status', () => {
    mockAsset.classStatus = { id: 1 };
    const controller = makeController({
      referenceData: referenceDataMock,
      asset: mockAsset
    });

    expect(controller.isClassStatusChanged()).toBeFalsy();
    mockAsset.classStatus = { id: 2 };
    expect(controller.isClassStatusChanged()).toBeTruthy();
    mockAsset.classStatus = { id: 1 };
    expect(controller.isClassStatusChanged()).toBeFalsy();
  });

  it('make sure effective date is set to today after class status changed', () => {
    const defaultDate = '2016-01-01';
    mockAsset.classStatus = { id: 1 };
    mockAsset.effectiveDate = defaultDate;

    const controller = makeController({
      referenceData: referenceDataMock,
      asset: mockAsset
    });

    expect(controller.asset.effectiveDate).toEqual(defaultDate);
    mockAsset.classStatus = { id: 2 };
    controller.classStatusChanged();
    expect(controller.asset.effectiveDate).not.toEqual(defaultDate);
    expect(controller.asset.effectiveDate).toEqual(moment({
      hour: 0,
      minute: 0,
      second: 0
    }).toISOString());

  });
});
