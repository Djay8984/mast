import * as _ from 'lodash';
import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';
import AssetModelController
  from 'app/asset/asset-model/asset-model.controller.js';

describe('Asset Model Controller', () => {
  let vm;
  let stateMock;
  let apiMock;
  let pageService;

  beforeEach(async () => {
    angular.mock.module('angularMoment');
    angular.mock.module(services, ($provide) => {
      apiMock = [{ name: 'Test', id: 5 }, { name: 'Other test', id: 8 }];

      stateMock = jasmine.createSpyObj('stateMock', ['go']);
      stateMock.go.and.callFake((name, params) => {
        if (!_.isNil(params.searchMode)) {
          params.searchMode = params.searchMode.toString();
        }

        if (!_.isNil(params.buildMode)) {
          params.buildMode = params.buildMode.toString();
        }

        stateMock.params = params;
        stateMock.name = name;
      });
      stateMock.params = {};

      pageService =
        jasmine.createSpyObj('pageService', ['isCheckedOut', 'checkOut']);
      pageService.isCheckedOut.and.returnValue({});
      pageService.checkOut.and.returnValue({});
      $provide.value('pageService', pageService);

      $provide.value('referenceDataService', { get: () => Object.create() });
      $provide.value('Restangular',
        jasmine.createSpyObj('Restangular', ['one', 'get']));
      $provide.value('$state', stateMock);
      $provide.value('$stateParams', { assetId: 1 });
      $provide.value('appConfig', { });
      $provide.value('settings', { });
      $provide.value('asset', { id: 1 });
      $provide.value('assetModel', { id: 1 });
      $provide.value('assetDecisionService',
        {
          checkedOut: () => apiMock,
          checkOut: (asset) => {
            apiMock = [asset]; return true;
          }
        });
      $provide.value('MastModalFactory', () => (
        {
          activate: () => {
            modal = true;
            return 'save';
          }
        }));
    });

    inject((_$controller_, _moment_, _$rootScope_) => {
      vm = _$controller_(AssetModelController,
        {},
        {});
    });
  });

  describe('Change views', () => {
    it('Init view should be "list"', () => {
      expect(vm.display).toEqual('buildMode');
    });

    it('Change to hierarchy view', () => {
      vm.changeView('hierarchy');
      expect(stateMock.go).toHaveBeenCalled();
    });

    it('Change to search mode', () => {
      expect(vm.searchMode).toBe(false);
      vm.toggleSearch();
      expect(vm.searchMode).toBe(true);
      expect(stateMock.go).toHaveBeenCalled();
    });

    it('Change to build mode', async () => {
      expect(Boolean(vm.buildMode)).toBe(true);

      await vm.toggleBuildMode();
      expect(pageService.checkOut).toHaveBeenCalled();
    });
  });
});
