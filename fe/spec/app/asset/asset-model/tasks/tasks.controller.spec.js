import * as _ from 'lodash';

import 'spec/spec-helper';

import services from 'app/services';
import TasksController
  from 'app/asset/asset-model/item/tasks/tasks.controller.js';


import assetFixtureRaw from 'spec/fixtures/asset.json';
import itemFixtureRaw from 'spec/fixtures/item.json';

describe('tasks.controller', () => {
  let tasksController;
  let $rootScope;
  let $scope;
  let assetFixture;
  let availableTasks;
  let itemFixture;
  let printService;

  beforeEach(async () => {
    angular.mock.module(services, ($provide) => {
      assetFixture = angular.fromJson(assetFixtureRaw);
      itemFixture = angular.fromJson(itemFixtureRaw);

      $provide.value('$rootScope', $rootScope);
      $provide.value('$scope', $scope);
      $provide.value('asset', assetFixture);
      $provide.value('availableTasks', availableTasks);
      $provide.value('item', itemFixture);
      $provide.value('printService', printService);
    });

    inject((_$controller_, _moment_, _$rootScope_) => {
      tasksController = _$controller_(
        TasksController);
    });


    describe('get data', () => {
      it('should retrieve item', () =>
        expect(tasksController.item).toEqual(
          itemFixture));

      it('should retrieve tasks', () =>
        expect(tasksController.tasks).toEqual(
          availableTasks));
    });
  });
});
