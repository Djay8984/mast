import 'spec/spec-helper';
import 'angular-moment';
import * as _ from 'lodash';

import services from 'app/services';
import AddScopeJobsController from
  'app/asset/jobs/add-scope/add-scope-jobs.controller.js';

describe('Add Job Scope Controller', () => {
  let vm;
  let $q;
  let scopeMock;
  let rootScope;
  let stateMock;
  let stateParamsMock;
  let actionableItemsMock;
  let assetMock;
  let assetNotesMock;
  let cocsMock;
  let dataForServiceListMock;
  let dueStatus;
  let jobMock;
  let jobStatusMock;
  let MastModalFactory;
  let momentMock;
  let modelMock;
  let referenceDataMock;
  let wipActionableItemsMock;
  let wipAssetNotesMock;
  let wipCoCsMock;
  let serviceStatusMock;
  let serviceCreditStatusMock;
  let serviceTypeStatusMock;
  let surveyorRoleMock;
  let userMock;
  let buildInjections;
  let codicilDecisionService;
  let defectOnlineService;
  let jobDecisionService;
  let navigationService;

  describe('Service schedule services', () => {
    beforeEach(() => {
      buildInjections = () => {
        angular.mock.module('angularMoment');
        angular.mock.module(services, ($provide) => {
          codicilDecisionService =
            jasmine.createSpyObj('codicilDecisionService', ['addJob']);
          codicilDecisionService.addJob.and.returnValue({});
          $provide.service('codicilDecisionService', codicilDecisionService);

          jobDecisionService =
            jasmine.createSpyObj('jobDecisionService',
              ['addJob', 'saveNewSurveyForJob']);
          jobDecisionService.addJob.and.returnValue({});
          jobDecisionService.saveNewSurveyForJob.and.returnValue({});
          $provide.service('jobDecisionService', jobDecisionService);

          navigationService =
            jasmine.createSpyObj('navigationService', ['back']);
          navigationService.back.and.returnValue({});
          $provide.service('navigationService', navigationService);

          defectOnlineService =
            jasmine.createSpyObj('defectOnlineService',
              ['addJob', 'get', 'save']);
          defectOnlineService.get.and.returnValue(
            Object.create({
              id: 6,
              defectCategory: { name: 'Hull Defect' },
              repairCount: 1,
              then: () => true
            })
          );
          defectOnlineService.addJob.and.returnValue({});
          defectOnlineService.save.and.returnValue(
            Object.create({
              id: 998
            })
          );
          $provide.service('defectOnlineService', defectOnlineService);

          MastModalFactory =
            jasmine.createSpyObj('MastModalFactory', ['activate']);
          MastModalFactory.activate.and.returnValue({});
          $provide.service('MastModalFactory', MastModalFactory);
        });

        inject((_$controller_, _$rootScope_, _moment_, _$q_) => {
          $q = _$q_;

          stateMock = jasmine.createSpyObj('$state', ['go']);
          stateMock.go.and.callFake(() => true);

          rootScope = _$rootScope_;
          rootScope.OFFLINE = false;
          scopeMock = rootScope.$new();

          momentMock = _moment_;

          // mock up state params
          stateParamsMock = {
            id: 1,
            jobId: 1,
            assetId: 1
          };

          assetMock = {
            id: 2,
            flagState: {
              id: 3
            },
            productRuleSet: {
              id: 4
            }
          };

          const dueStatuses = {
            notDue: 1,
            due: 2,
            overdue: 3
          };
          dueStatus = {
            get: (field) => dueStatuses[field],
            toObject: () => dueStatuses
          };

          dataForServiceListMock = {
            services: {
              plain: () => true
            },
            productModel: { },
            wipSurveys: { }
          };

          jobMock = {
            id: 1,
            jobStatus: {
              id: 1
            },
            employees: [
              {
                id: 1,
                lrEmployee: {
                  id: 1,
                  name: 'Jing Yu'
                },
                employeeRole: {
                  id: 11
                }
              }
            ]
          };

          modelMock = {
            services: 'schedule'
          };

          actionableItemsMock = { };
          assetMock = { };
          assetNotesMock = { };
          cocsMock = { };

          // Mock up jobStatus;
          const jobStatuses = {
            sdoAssigned: 1,
            resourceAssigned: 2,
            underSurvey: 3,
            underReporting: 4
          };
          jobStatusMock = {
            toObject: () => jobStatuses,
            get: (field) => jobStatuses[field]
          };

          // Mock up user;
          userMock = {
            fullName: 'Jing Yu',
            id: 1
          };

          referenceDataMock = {
            service: {
              serviceCatalogues: [
                {
                  id: 101,
                  productCatalogue: {
                    id: 104
                  },
                  serviceType: {
                    id: 913
                  }
                }
              ]
            },
            product: {
              productTypes: [
                {
                  id: 102
                }
              ],
              productGroups: [
                {
                  id: 103
                }
              ],
              productCatalogues: [
                {
                  id: 104,
                  name: 'product104',
                  productGroup: {
                    id: 103
                  },
                  productType: {
                    id: 102
                  }
                }
              ]
            }
          };

          // Mock up surveyorRole;
          const surveyorRoles = {
            businessSupport: 1,
            surveyor: 2,
            management: 3,
            salesCRM: 4,
            specialistLeadership: 5,
            authorisingSurveyor: 6,
            eicCaseSurveyor: 7,
            eicAdmin: 8,
            sdo: 9,
            eicManager: 10,
            leadSurveyor: 11,
            sdoCoordinator: 12
          };
          surveyorRoleMock = {
            toObject: () => surveyorRoles,
            get: (field) => surveyorRoles[field]
          };

          // Mock up serviceCreditStatus;
          const serviceCredits = {
            notStarted: 1,
            partHeld: 2,
            postponed: 3,
            complete: 4,
            finished: 5
          };
          serviceCreditStatusMock = {
            get: (field) => serviceCredits[field]
          };

          // Mock up serviceStatus;
          const serviceStatuses = {
            notStarted: 1,
            partHeld: 2,
            held: 3,
            complete: 4
          };
          serviceStatusMock = {
            toObject: () => serviceStatuses
          };

          serviceTypeStatusMock = { };
          wipActionableItemsMock = { };
          wipAssetNotesMock = { };
          wipCoCsMock = { };

          vm = _$controller_(AddScopeJobsController,
            {
              $q,
              $rootScope: rootScope,
              $scope: scopeMock,
              $state: stateMock,
              $stateParams: stateParamsMock,
              actionableItems: actionableItemsMock,
              asset: assetMock,
              assetNotes: assetNotesMock,
              cocs: cocsMock,
              dataForServiceList: dataForServiceListMock,
              dueStatus,
              job: jobMock,
              jobStatus: jobStatusMock,
              moment: momentMock,
              MastModalFactory,
              referenceData: referenceDataMock,
              wipActionableItems: wipActionableItemsMock,
              wipAssetNotes: wipAssetNotesMock,
              wipCoCs: wipCoCsMock,
              serviceStatus: serviceStatusMock,
              serviceCreditStatus: serviceCreditStatusMock,
              serviceTypeStatus: serviceTypeStatusMock,
              surveyorRole: surveyorRoleMock,
              codicilDecisionService,
              defectOnlineService,
              jobDecisionService,
              navigationService,
              user: userMock
            },
            {
              _job: jobMock,
              _selectedScopeItems: [ { id: 1 }, { id: 2 } ],
              _scheduledServices: [ ],
              _groupScheduledServices: {
                'MMS': {
                  'Hull': [
                    { 'code': 'GPCR' },
                    { 'code': 'FOO' }
                  ]
                },
                'Classification': {
                  'Lifting': [
                    { 'code': 'LDAM' },
                    { 'code': 'LRPS' }
                  ]
                }
              },
              model: modelMock,
              _selectedServices: { }
            });

          spyOn(vm, 'saveDefect').and
            .returnValue(Promise.resolve([ { id: 1 }, { id: 2 } ]));

          scopeMock.$digest();
        });
      };

      buildInjections();
    });

    afterEach(async () => {
      // rootScope.$apply();
    });

    /* eslint no-loop-func: 0 */
    const methods = ['getServiceCatalogueItem', 'getServiceCatalogueFamily',
      'getServiceCatalogueProduct', 'getServiceCatalogueFamilyFromName',
      'emptyService', 'switchService', 'reOrderServices', 'scopeServices',
      'setUpBasicService', 'setUpModelServices', 'createModelServices',
      'createNewModelServices', 'setUpScheduledServices', 'setUpServices',
      'createScheduledServices', 'expandServices', 'getDueOverdueServices',
      'groupServices', 'unGroupServices', 'singleCodicilLink',
      'singleDefectLink', 'groupData', 'groupWipData',
      'setSelectedStateForAllServices', 'visibleServices',
      'addDefectAndRepairCount', 'disableAlreadySelectedSurveys',
      'disableAlreadySelectedItems', 'addCheckBoxes', 'requiresWarning',
      'addSelectedScopeItems', 'editableService', 'ctaVisible', 'validForm',
      'serviceNameDialog', 'selectedServices', 'saveSelectedServices',
      'getServiceType', 'shouldContinueWithNewServiceNames', 'saveDefect',
      'save', 'cancel'];
    for (let i = 0; i < methods.length; i += 1) {
      it(`should have a "${methods[i]}" method`, async () => {
        expect(`$rootScope.${methods[i]}`).toBeDefined();
      });
    }

    /* eslint no-loop-func: 0 */
    const getters = ['job', 'jobAssetNotes', 'jobActionableItems', 'jobCocs',
      'wipAssetNotes', 'wipActionableItems', 'wipCoCs', 'selectedScopeItemSize',
      'totalScopeItemsSize'];
    for (let i = 0; i < getters.length; i += 1) {
      it(`should have a "${getters[i]}" getter`, async () => {
        expect(`$rootScope.${getters[i]}`).toBeDefined();
      });
    }

    describe('Should prepare services', () => {
      it('setUpBasicService should set up basic services', async () => {
        spyOn(vm, 'getServiceCatalogueItem').and.returnValue({
          flags: {
            name: 'UK'
          },
          productCatalogue: {
            productType: {
              id: 1
            },
            productGroup: {
              id: 2
            }
          }
        });
        spyOn(vm, 'getServiceCatalogueFamily').and.callFake(() => true);
        spyOn(vm, 'getServiceCatalogueProduct').and.returnValue({
          name: 'productGroup'
        });
        spyOn(vm, 'getServiceCatalogueFamilyFromName').and.callFake(() => true);
        const service = {
          group: 'group'
        };
        let scheduled = false;
        let basicService = await vm.setUpBasicService(service, scheduled);
        expect(vm.getServiceCatalogueItem).toHaveBeenCalled();
        expect(vm.getServiceCatalogueFamily).toHaveBeenCalled();
        expect(vm.getServiceCatalogueFamilyFromName).toHaveBeenCalled();
        expect(basicService.serviceProduct).toEqual('group');
        scheduled = true;
        basicService = await vm.setUpBasicService(service, scheduled);
        expect(vm.getServiceCatalogueProduct).toHaveBeenCalled();
        expect(basicService.serviceProduct).toEqual('productGroup');
      });

      it('Should create scheduled services correctly', async () => {
        spyOn(vm, 'createScheduledServices').and.callFake(() => true);
        await vm.setUpScheduledServices();
        expect(vm.createScheduledServices).toHaveBeenCalled();
      });

      it('Should create model services correctly', async () => {
        spyOn(vm, 'createModelServices').and.callFake(() => true);
        spyOn(vm, 'createNewModelServices').and.callFake(() => true);
        await vm.setUpModelServices();
        expect(vm.createModelServices).toHaveBeenCalled();
        expect(vm.createNewModelServices).toHaveBeenCalled();
      });

      it('Should group services by type for display', async () => {
        spyOn(vm, 'groupServices').and.callFake(() => true);
        spyOn(vm, 'getDueOverdueServices').and.callThrough();
        spyOn(vm, 'expandServices').and.callFake(() => true);
        await vm.setUpServices();
        expect(vm.groupServices).toHaveBeenCalled();
        expect(vm.getDueOverdueServices).toHaveBeenCalled();
        expect(vm.expandServices).toHaveBeenCalled();
      });

      it('should ungroup services when called', async () => {
        const group = {
          MMS: [ { id: 1 }, { id: 2 } ],
          Classification: [ { id: 3 }, { id: 4 } ]
        };
        const unGroup = await vm.unGroupServices(group);
        expect(angular.isObject(unGroup)).toBe(true);
        expect(_.size(unGroup)).toBe(2);
      });

      it('Should create checkboxes for each service and/or codicil', async () => {
        spyOn(vm, 'addCheckBoxes').and.callFake(() => true);
        spyOn(vm, 'disableAlreadySelectedItems').and.callFake(() => true);
        spyOn(vm, 'disableAlreadySelectedSurveys').and.callFake(() => true);
        spyOn(vm, 'addDefectAndRepairCount').and.callFake(() => true);
        await vm.setUpData();
        expect(vm.addCheckBoxes).toHaveBeenCalled();
        expect(vm.disableAlreadySelectedItems).toHaveBeenCalled();
        expect(vm.disableAlreadySelectedSurveys).toHaveBeenCalled();
        expect(vm.addDefectAndRepairCount).toHaveBeenCalled();
      });
    });

    describe('Codicils: CoCs, AIs, and ANs', () => {
      it('Should display codicils', async () => {
        const codicilsFunctions =
          ['jobAssetNotes', 'jobActionableItems', 'jobCocs', 'wipAssetNotes',
          'wipActionableItems', 'wipCoCs'];
        vm._assetNotes = [ { id: 1 } ];
        vm._actionableItems = [ { id: 2 } ];
        vm._cocs = [ { id: 3 } ];
        vm._wipAssetNotes = [ { id: 4 } ];
        vm._wipActionableItems = [ { id: 5 } ];
        vm._wipCoCs = [ { id: 6 } ];
        for (let i = 0; i < codicilsFunctions.length; i += 1) {
          expect(vm[codicilsFunctions[i]].length).toEqual(1);
        }
      });
    });

    describe('Saving', () => {
      xit('Selecting "Add" moves to the edit scope page', async () => {
        vm._selectedScopeItems = [
          {
            id: null,
            route: 'coc',
            title: 'test',
            parent: {
              id: null
            },
            confidentialityType: {
              id: 1
            },
            job: {
              id: 1
            },
            category: { },
            imposedDate: (new Date()).toISOString(),
            dueDate: (new Date()).toISOString()
          }
        ];
        spyOn(vm, 'shouldContinueWithNewServiceNames').and.callFake(() => true);
        spyOn(vm, 'saveSelectedServices').and.callFake(() => true);
        await vm.save();
        expect(vm.shouldContinueWithNewServiceNames).toHaveBeenCalled();
        expect(vm.saveSelectedServices).toHaveBeenCalled();
        expect(vm.saveDefect).toHaveBeenCalledWith(jasmine.any(Object));
      });
      it('"saveDefect" should save a defect on a codicil correctly', async () => {
        const saveDefect = {
          defect: {
            id: 2
          },
          job: {
            id: jobMock.id
          }
        };
        const defect = {
          job: {
            id: jobMock.id
          },
          parent: {
            id: saveDefect.defect.id
          },
          id: null
        };
        const result = await vm.saveDefect(saveDefect);
        _.assign(saveDefect.defect, defect);
        expect(angular.isObject(result)).toBe(true);
      });
    });

    describe('Button states', () => {
      it('Should show the CTA buttons in the correct states', async () => {
        let states = [1, 2];
        let visible;

        // At the start the job status is 1 or 2;
        spyOn(vm, 'ctaVisible').and.callThrough();
        for (const i in states) {
          if (states.hasOwnProperty(i)) {
            vm._job.jobStatus.id = states[i];
            visible = await vm.ctaVisible();
            expect(visible).toBe(true);
          }
        }

        // Reset the job status to 5 - 8;
        states = [5, 6, 7, 8];
        for (const i in states) {
          if (states.hasOwnProperty(i)) {
            vm._job.jobStatus.id = states[i];
            visible = await vm.ctaVisible();
            expect(visible).toBe(false);
          }
        }
      });
    });

    // There are three buttons at the top of the job scope page - two actually
    // change the data, those are the ones we'll test;
    describe('Switching service types', () => {
      it('Switching service types should change data', async () => {
        spyOn(vm, 'groupServices').and.callFake(() => true);
        spyOn(vm, 'getDueOverdueServices').and.callFake(() => true);
        spyOn(vm, 'expandServices').and.callFake(() => true);

        // Define the data we're working with;
        vm._productServices = [ { code: 'GHI' }, { code: 'JKL' } ];
        vm._scheduledServices = [ { code: 'ABC' }, { code: 'DEF' } ];

        // Expect the method to be defined, and to be a function;
        expect(vm.switchService).toBeDefined();
        expect(angular.isFunction(vm.switchService)).toBe(true);

        // Call the method with 'all';
        await vm.switchService('all');
        expect(vm.groupServices).toHaveBeenCalled();

        // Call the method with 'overdue';
        await vm.switchService('overdue');
        expect(vm.getDueOverdueServices).toHaveBeenCalled();
        expect(vm.expandServices).toHaveBeenCalled();

        // Call the method with 'schedule';
        await vm.switchService('schedule');
        expect(vm.expandServices).toHaveBeenCalled();
      });
    });

    describe('Defect/Codicil links', () => {
      it('Codicil link should trigger $state.go', async () => {
        spyOn(vm, 'singleCodicilLink').and.callThrough();
        const codicil = {
          route: 'coc',
          id: 1
        };
        await vm.singleCodicilLink(codicil);
        expect(stateMock.go).toHaveBeenCalledWith('coc.view', jasmine.any(Object));
      });

      it('Defect link should trigger $state.go', async () => {
        spyOn(vm, 'singleDefectLink').and.callThrough();
        const codicil = {
          route: 'coc',
          id: 2,
          defect: {
            id: 3
          }
        };
        await vm.singleDefectLink(codicil);
        expect(stateMock.go).toHaveBeenCalledWith('defect.view', jasmine.any(Object));
      });
    });

    describe('Labelling services correctly', () => {
      it('Label service as "overdue" or "due"', async () => {
        const service = {
          scheduledService: {
            id: 3,
            dueStatus: {
              id: 2
            }
          }
        };
        let warning = await vm.requiresWarning(service);
        expect(warning).toEqual('Due');
        service.scheduledService.dueStatus.id = 3;
        warning = await vm.requiresWarning(service);
        expect(warning).toEqual('Overdue');
      });
    });

    describe('Helper functions', () => {
      it('"getServiceCatalogueItem" should be defined', async () => {
        expect(vm.getServiceCatalogueItem).toBeDefined();
        const retValue = await vm.getServiceCatalogueItem(101);
        expect(retValue).toEqual({ id: 101, productCatalogue: { id: 104 }, serviceType: { id: 913 } });
      });

      it('"getServiceCatalogueItem" should return an object', async () => {
        expect(vm.getServiceCatalogueItem).toBeDefined();
        const retValue = await vm.getServiceCatalogueItem(101);
        expect(angular.isObject(retValue)).toBe(true);
      });

      it('"getServiceCatalogueFamily" should be defined', async () => {
        expect(vm.getServiceCatalogueFamily).toBeDefined();
        const retValue = await vm.getServiceCatalogueFamily(102);
        expect(retValue).toEqual({ id: 102 });
      });

      it('"getServiceCatalogueFamily" should return an object', async () => {
        expect(vm.getServiceCatalogueFamily).toBeDefined();
        const retValue = await vm.getServiceCatalogueFamily(102);
        expect(angular.isObject(retValue)).toBe(true);
      });

      it('"getServiceCatalogueProduct" should be defined', async () => {
        expect(vm.getServiceCatalogueProduct).toBeDefined();
        const retValue = await vm.getServiceCatalogueProduct(103);
        expect(retValue).toEqual({ id: 103 });
      });

      it('"getServiceCatalogueProduct" should return an object', async () => {
        expect(vm.getServiceCatalogueProduct).toBeDefined();
        const retValue = await vm.getServiceCatalogueProduct(103);
        expect(angular.isObject(retValue)).toBe(true);
      });

      it('"getServiceCatalogueFamilyFromName" should be defined', async () => {
        expect(vm.getServiceCatalogueFamilyFromName).toBeDefined();
        const retValue =
          await vm.getServiceCatalogueFamilyFromName('product104');
        expect(retValue).toEqual(102);
      });

      it('"getServiceCatalogueFamilyFromName" should return an object', async () => {
        expect(vm.getServiceCatalogueFamilyFromName).toBeDefined();
        const retValue =
          await vm.getServiceCatalogueFamilyFromName('product104');
        expect(angular.isNumber(retValue)).toBe(true);
      });

      it('"reOrderServices" should be defined', async () => {
        expect(vm.reOrderServices).toBeDefined();
        const retValue =
          await vm.reOrderServices([{ id: 1 }, { id: 2 }, { id: 3 }]);
        expect(angular.isObject(retValue)).toBe(true);
      });

      it('"scopeServices" should return a boolean', async () => {
        spyOn(vm, 'reOrderServices').and.callFake(() => true);
        vm.model.services = 'schedule';
        let ss = await vm.scopeServices();
        expect(vm.reOrderServices).toHaveBeenCalledWith(vm._groupScheduledServices);
        expect(ss).toMatch(/true|false/);
        vm.model.services = 'overdue';
        ss = await vm.scopeServices();
        expect(vm.reOrderServices).toHaveBeenCalledWith(vm._selectedServices);
        expect(ss).toMatch(/true|false/);
      });

      it('"editableService" should be defined', async () => {
        expect(vm.editableService).toBeDefined();
        let retValue = await vm.editableService();

        // This job's status is 1 (sdoAssigned) so we need to verify that this
        // method works as it should (true);
        expect(retValue).toBe(true);

        // Then we need to reset the value to something that isn't accounted
        // for, and it should be false;
        vm._job.jobStatus.id = 9;
        retValue = await vm.editableService();
        expect(retValue).toBe(false);
      });

      it('"validForm" should be defined', async () => {
        spyOn(vm, 'selectedServices').and.callFake(() => true);
        expect(vm.validForm).toBeDefined();
        await vm.validForm();
        expect(vm.selectedServices).toHaveBeenCalled();
      });

      it('"emptyService" should be defined', async () => {
        expect(vm.emptyService).toBeDefined();
      });

      it('"emptyService" should know diff. between schedule and overdue', async () => {
        let empty = await vm.emptyService('schedule');
        expect(empty).toMatch(/true|false/);
        empty = await vm.emptyService('overdue');
        expect(empty).toMatch(/true|false/);
        empty = await vm.emptyService('foo');
        expect(empty).toBe(false);
      });

      it('"addDefectAndRepairCount" sets "repairCount" on codicils', async () => {
        vm.data = {
          'coc': [
            {
              id: 24,
              defect: {
                id: 6,
                defectCategory: {
                  name: 'Hull'
                },
                repairCount: 1
              },
              asset: { id: 2 },
              route: 'coc'
            }
          ]
        };

        await vm.addDefectAndRepairCount();

        // We expect 3 calls because there are 3 objects in vm.data;
        expect(defectOnlineService.get).toHaveBeenCalled();
        expect(defectOnlineService.get.calls.count()).toEqual(1);
        expect(defectOnlineService.get.calls.count()).not.toEqual(2);
      });

      it('"addSelectedScopeItems" should add to the scope list', async () => {
        vm._selectedScopeItems = [
          {
            id: 2,
            route: 'coc',
            serviceCatalogue: 2
          },
          {
            id: 10,
            route: 'actionable-item',
            serviceCatalogue: 3
          }
        ];
        let obj = { id: 1, checkBoxModel: 0, route: 'coc' };
        await vm.addSelectedScopeItems(obj);
        expect(vm._selectedScopeItems).toContain(obj);

        obj = { id: 2, checkBoxModel: 1, route: 'coc', serviceCatalogue: 2 };
        await vm.addSelectedScopeItems(obj);
        expect(_.size(vm._selectedScopeItems)).toEqual(2);
      });

      it('"expand" should set the expanded flag', async () => {
        const item = { expanded: false };
        const ex = await vm.expand(item);
        expect(item.expanded).toEqual(true);
        expect(angular.isObject(ex)).toBe(true);
      });

      it('"selectedServices" should return an object', async () => {
        spyOn(vm, 'unGroupServices').and.callFake(() => true);
        const ss = await vm.selectedServices();
        expect(vm.unGroupServices).toHaveBeenCalled();
        expect(angular.isObject(ss)).toBe(true);
      });

      it('"saveSelectedServices" should return an object', async () => {
        expect(vm.saveSelectedServices).toBeDefined();
        const fakeServices = [
          {
            checkBoxModel: 1,
            code: 'NCPM',
            job: {
              id: 1
            },
            name: 'ADNR Misc.'
          }
        ];
        spyOn(vm, 'selectedServices').and.callFake(() => fakeServices);
        rootScope.OFFLINE = true;
        let sss = await vm.saveSelectedServices();
        expect(vm.selectedServices).toHaveBeenCalled();
        expect(jobDecisionService.saveNewSurveyForJob)
          .toHaveBeenCalledWith(vm._job.id, jasmine.any(Object));
        expect(angular.isObject(sss)).toBe(true);

        rootScope.OFFLINE = false;
        sss = await vm.saveSelectedServices();
        expect(vm.selectedServices).toHaveBeenCalled();
        expect(jobDecisionService.saveNewSurveyForJob)
          .toHaveBeenCalledWith(vm._job.id, jasmine.any(Object));
        expect(angular.isObject(sss)).toBe(true);
      });

      it('"cancel" should navigate to asset.jobs.view', async () => {
        await vm.cancel();
        expect(stateMock.go).toHaveBeenCalledWith('asset.jobs.view',
          vm._$stateParams);
      });
    });
  });
});
