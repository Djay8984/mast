import services from 'app/services';
import 'spec/spec-helper';
import TeamJobController from
  'app/asset/jobs/team/team-job.controller.js';

describe('Add Jobs Team Controller', () => {
  let vm;
  let logMock;
  let scopeMock;
  let stateMock;
  let stateParamsMock;
  let userMock;
  let employeeReferenceDataMock;
  let jobMock;
  let jobStatusMock;
  let jobReferenceDataMock;
  let surveyorRoleMock;
  let typeaheadService;
  let jobDecisionService;
  let buildServices;
  let buildInjections;
  let rootScope;
  let teamJobForm;
  let editableStatuses;

  describe('Job Team', () => {
    beforeEach(async () => {
      angular.mock.module(($exceptionHandlerProvider) => {
        $exceptionHandlerProvider.mode('log');
      });

      buildServices = (errorThrown = false) => {
        angular.mock.module(services, ($provide) => {
          // Job decision service;
          jobDecisionService =
            jasmine.createSpyObj('jobDecisionService', ['saveJobTeam']);
          if (errorThrown) {
            jobDecisionService.saveJobTeam.and.throwError();
          } else {
            jobDecisionService.saveJobTeam.and.returnValue({});
          }
          $provide.value('jobDecisionService', jobDecisionService);

          typeaheadService =
            jasmine.createSpyObj('typeaheadService', ['query']);
          typeaheadService.query.and.returnValue([]);
          $provide.value('typeaheadService', typeaheadService);
        });
      };

      buildInjections = () => {
        inject((_$controller_, _$rootScope_, _$exceptionHandler_, _$log_) => {
          logMock = _$log_;

          stateMock = jasmine.createSpyObj('$state spy', ['go']);
          stateMock.go.and.callFake(() => true);

          rootScope = _$rootScope_;
          scopeMock = rootScope.$new();

          // mock up state params
          stateParamsMock = {
            id: 1
          };

          // Mock up user;
          userMock = {
            fullName: 'Jing Yu'
          };

          // Mock up job;
          jobMock = {
            id: 1,
            employees: [
              {
                id: 1,
                employeeRole: {
                  id: 11
                },
                lrEmployee: {
                  id: 1,
                  firstName: 'Jing',
                  lastName: 'Yu',
                  name: 'Jing Yu'
                }
              }
            ],
            jobStatus: {
              id: 1
            }
          };

          // Mock up jobStatus;
          const jobStatuses = {
            'sdoAssigned': 1,
            'resourceAssigned': 2,
            'underSurvey': 3,
            'underReporting': 4,
            'awaitingTechnicalReviewerAssignment': 5,
            'closed': 11
          };
          jobStatusMock = {
            toObject: () => jobStatuses,
            get: (field) => jobStatuses[field]
          };

          // Mock up jobStatusReferenceData
          jobReferenceDataMock = {
            'job': {
              'jobStatuses': [
                {
                  id: 1,
                  name: 'SDO Assigned'
                },
                {
                  id: 2,
                  name: 'Resource Assigned'
                },
                {
                  id: 3,
                  name: 'Under Survey'
                },
                {
                  id: 4,
                  name: 'Under Reporting'
                }
              ]
            }
          };

          // Mock up surveyorRole;
          const surveyorRoles = {
            leadSurveyor: 11,
            eicManager: 10,
            authorisingSurvey: 6,
            sdoCoordinator: 12
          };
          surveyorRoleMock = {
            toObject: () => surveyorRoles,
            get: (field) => surveyorRoles[field]
          };

          teamJobForm = {
            authorisingSurveyor: {
              $valid: true
            },
            eicManager: {
              $valid: true
            },
            leadSurveyor: {
              $valid: true
            },
            sdoCoordinator: {
              $valid: true
            },
            surveyor0: {
              $valid: true
            },
            $invalid: false,
            $pristine: false
          };

          employeeReferenceDataMock = {
            employee: {
              employee: [
                { id: 1, name: 'test' },
                { id: 2, name: 'another test' }
              ]
            }
          };

          editableStatuses = [ 1, 2, 3, 4 ];

          /* eslint babel/object-shorthand: 0 */
          vm = _$controller_(TeamJobController,
            {
              $log: logMock,
              $scope: scopeMock,
              $state: stateMock,
              $stateParams: stateParamsMock,
              user: userMock,
              employeeReferenceData: employeeReferenceDataMock,
              job: jobMock,
              jobStatus: jobStatusMock,
              jobStatusReferenceData: jobReferenceDataMock,
              surveyorRole: surveyorRoleMock,
              jobDecisionService
            },
            {
              jobModel: jobMock,
              _job: jobMock,
              _user: userMock,
              teamJobForm: teamJobForm,
              _authorisingSurveyor: null,
              _fleetServiceSpecialist: null,
              _leadSurveyor: null,
              _editableStatuses: editableStatuses
            });

          vm.jobModel = jobMock;
        });
      };

      buildServices();
      buildInjections();
    });

    /* eslint no-loop-func: 0 */
    const methods = [
      'addOriginalGeneralSurveyor',
      'setOriginalAuthorisingSurveyor',
      'setOriginalFleetServicesSpecialist',
      'setOriginalLeadSurveyor',
      'setOriginalSdoCoordinator',
      'setUpData',
      'cancel',
      'save',
      'fieldValidation',
      'validForm',
      'validFormItem',
      'surveyorTypeahead',
      'onSurveyorSelect',
      'isTeamEditable',
      'jobTeamExists',
      'isGeneralSurveyorLead',
      'removeGeneralSurveyor',
      'surveyorPreFilter',
      'userCanEditField'
    ];
    for (let i = 0; i < methods.length; i += 1) {
      it(`should have a "${methods[i]}" method`, async () => {
        expect(`$rootScope.{methods[i]}`).toBeDefined();
      });
    }

    /* eslint no-loop-func: 0 */
    const getters = [
      'job',
      'jobTeamEmployees',
      'currentJobStatus',
      'jobTeamEmployeesAssigned'
    ];
    for (let i = 0; i < getters.length; i += 1) {
      it(`should have a "${getters[i]}" getter`, async () => {
        expect(`$rootScope.{getters[i]}`).toBeDefined();
      });
    }

    xdescribe('On controller creation', () => {
      it('should manipulate the job employees internally', async () => {
        await vm.setUpData();
        const role = _.first(vm._job.employees);
        expect(role.lrEmployee.name).toEqual(vm._leadSurveyor);
        expect(vm.jobModel.leadSurveyor).toEqual(role.lrEmployee.name);
      });
    });

    xdescribe('isTeamEditable', () => {
      it('returns boolean correctly based on statuses', async () => {
        expect(await vm.isTeamEditable()).toBe(true);

        // If job status is not 1-4, or 11, it should be true;
        _.set(vm._job.jobStatus, 'id', 5);
        expect(await vm.isTeamEditable()).toBe(true);

        // If job status is 11 (closed), it should be false;
        _.set(vm._job.jobStatus, 'id', 11);
        expect(await vm.isTeamEditable()).toBe(false);

        // If job status is 3 (under survey) then ... it should be true
        // because our name is in the employees list;
        _.set(vm._job.jobStatus, 'id', 3);
        expect(await vm.isTeamEditable()).toBe(true);

        // ... if our name isn't in the job employees, should be false;
        _.set(vm._user, 'fullName', 'Joe Bloggs');
        expect(await vm.isTeamEditable()).toBe(false);
      });
    });

    // Saving the job team, success or failure;
    xdescribe('Saving the Job Team', () => {
      it('can save the job team', async () => {
        spyOn(vm, 'setUpData').and.callFake(() => true);
        spyOn(vm, 'cancel').and.callThrough();
        await vm.save();
        expect(jobDecisionService.saveJobTeam).toHaveBeenCalled();
        expect(vm.setUpData).toHaveBeenCalled();
        expect(vm.cancel).toHaveBeenCalled();
        expect(stateMock.go).toHaveBeenCalled();
      });
      it('throws error correctly when failing to save', async () => {
        buildServices(true);
        await vm.save();
        expect(jobDecisionService.saveJobTeam).toHaveBeenCalled();
        expect(logMock.error).toHaveBeenCalled();
      });
    });

    describe('Helper functions', () => {
      it('setOriginalAuthorisingSurveyor should work just once', async () => {
        await vm.setOriginalAuthorisingSurveyor('Joe Bloggs');
        expect(vm._authorisingSurveyor).toEqual('Joe Bloggs');
        await vm.setOriginalAuthorisingSurveyor('John Smith');
        expect(vm._authorisingSurveyor).not.toEqual('John Smith');
      });
      it('setOriginalFleetServicesSpecialist should work just once', async () => {
        await vm.setOriginalFleetServicesSpecialist('Joe Bloggs');
        expect(vm._fleetServiceSpecialist).toEqual('Joe Bloggs');
        await vm.setOriginalFleetServicesSpecialist('John Smith');
        expect(vm._fleetServiceSpecialist).not.toEqual('John Smith');
      });
      it('setOriginalLeadSurveyor should work just once', async () => {
        // This bit is a cheat, as it also tests the mock set up, and the real
        // setUpData which sets up the surveyors;
        await vm.setOriginalLeadSurveyor('Joe Bloggs');
        expect(vm._leadSurveyor).toEqual('Jing Yu');
        await vm.setOriginalLeadSurveyor('John Smith');
        expect(vm._leadSurveyor).not.toEqual('John Smith');
      });
      it('getter "job" should return an object', async () => {
        expect(angular.isObject(await vm.job)).toBe(true);
      });
      it('"cancel" should trigger $state.go', async () => {
        spyOn(vm, 'cancel').and.callThrough();
        await vm.cancel();
        expect(stateMock.go).toHaveBeenCalledWith('asset.jobs.view',
          jasmine.any(Object), jasmine.any(Object));
      });
      xit('"validForm" should return a boolean', async () => {
        expect(await vm.validForm()).toMatch(/true|false/);
      });
      xit('"validForm" should understand the form values', async () => {
        vm._teamJobFormSaved = false;
        expect(await vm.validForm()).toBe(true);
      });
      it('"jobTeamExists" should return a boolean', async () => {
        expect(await vm.jobTeamExists()).toMatch(/true|false/);
      });
      xit('"surveyorTypeahead" should return an array', async () => {
        vm._teamJobFormSaved = true;
        const surv = vm.surveyorTypeahead('');
        expect(vm._teamJobFormSaved).toEqual(false);
        expect(angular.isArray(surv)).toBe(true);
      });
    });
  });
});
