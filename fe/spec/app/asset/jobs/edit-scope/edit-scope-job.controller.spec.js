import services from 'app/services';
import 'spec/spec-helper';
import 'angular-moment';
import EditScopeJobsController from
  'app/asset/jobs/edit-scope/edit-scope-jobs.controller.js';

describe('Edit Scope Controller', () => {
  let vm;
  let qMock;
  let scopeMock;
  let rootScope;
  let stateMock;
  let stateParamsMock;
  let attachmentCountMock;
  let attachmentPathMock;
  let MastModalFactoryMock;
  let dueStatusMock;
  let employeeRoleMock;
  let jobMock;
  let jobStatusMock;
  let productTypeMock;
  let referenceDataMock;
  let surveysMock;
  let userMock;
  let wipAssetNotesMock;
  let wipActionableItemsMock;
  let wipCoCsMock;

  let buildServices;
  let buildInjections;

  let codicilDecisionService;
  let defectDecisionService;
  let jobDecisionService;
  let navigationService;
  let referenceDataService;
  let surveyDecisionService;

  describe('Edit Job Scope methods', () => {
    beforeEach(async () => {
      angular.mock.module('angularMoment');

      buildServices = (errorThrown = false) => {
        angular.mock.module(services, ($provide) => {
          // Codicil Decision Service;
          codicilDecisionService =
            jasmine.createSpyObj('codicilDecisionService',
              ['removeCoC', 'removeAssetNote', 'removeActionableItem']);
          if (errorThrown) {
            codicilDecisionService.removeCoC.and.throwError();
            codicilDecisionService.removeAssetNote.and.throwError();
            codicilDecisionService.removeActionableItem.and.throwError();
          } else {
            codicilDecisionService.removeCoC.and.returnValue({});
            codicilDecisionService.removeAssetNote.and.returnValue({});
            codicilDecisionService.removeActionableItem.and.returnValue({});
          }
          $provide.value('codicilDecisionService', codicilDecisionService);

          // Defect Decision Service;
          defectDecisionService =
            jasmine.createSpyObj('defectDecisionService', ['get']);
          if (errorThrown) {
            defectDecisionService.get.and.throwError();
          } else {
            defectDecisionService.get.and.callFake(() => qMock.when());
          }
          $provide.value('defectDecisionService', defectDecisionService);

          // Job Decision Service;
          jobDecisionService =
            jasmine.createSpyObj('jobDecisionService', ['updateJob']);
          if (errorThrown) {
            jobDecisionService.updateJob.and.throwError();
          } else {
            jobDecisionService.updateJob.and.returnValue({});
          }
          $provide.value('jobDecisionService', jobDecisionService);

          // Navigation Service;
          navigationService = jasmine.createSpyObj('navigationService', ['back']);
          navigationService.back.and.returnValue({});
          $provide.value('navigationService', navigationService);

          // Survey Decision Service;
          surveyDecisionService =
            jasmine.createSpyObj('surveyDecisionService', ['removeSurvey']);
          surveyDecisionService.removeSurvey.and.returnValue({});
          $provide.value('surveyDecisionService', surveyDecisionService);

          attachmentCountMock = {};
          $provide.value('attachmentCount', attachmentCountMock);

          attachmentPathMock = '';
          $provide.value('attachmentPath', attachmentPathMock);
        });
      };

      buildInjections = () => {
        inject((_$controller_, _$rootScope_, _moment_, _$q_) => {
          stateMock = jasmine.createSpyObj('$state', ['go']);
          stateMock.go.and.callFake(() => true);

          rootScope = _$rootScope_;
          scopeMock = rootScope.$new();

          qMock = _$q_;

          // mock up state params
          stateParamsMock = {
            id: 1,
            jobId: 1,
            assetId: 1,
            addition: true
          };

          // Mock up employeeRole;
          const employeeRoles = {
            leadSurveyor: 11,
            eicManager: 10,
            authorisingSurvey: 6,
            sdoCoordinator: 12
          };
          employeeRoleMock = {
            toObject: () => employeeRoles,
            get: (field) => employeeRoles[field]
          };

          // Mock up jobStatus;
          const jobStatuses = {
            sdoAssigned: 1,
            resourceAssigned: 2,
            underSurvey: 3,
            underReporting: 4,
            awaitingTechnicalReviewerAssignment: 5,
            underTechnicalReview: 6,
            awaitingEndorserAssignment: 7,
            underEndorsement: 8,
            cancelled: 9,
            aborted: 10,
            closed: 11
          };
          jobStatusMock = {
            toObject: () => jobStatuses,
            get: (field) => jobStatuses[field]
          };

          jobMock = {
            jobStatus: {
              id: 1
            },
            _meta: {
              id: 1
            },
            scopeConfirmed: false,
            scopeConfirmedBy: null,
            scopeConfirmedDate: null
          };

          const dueStatuses = {
            notDue: 1,
            due: 2,
            overdue: 3
          };
          dueStatusMock = {
            get: (field) => dueStatuses[field]
          };

          const productTypes = {
            classification: 1,
            mms: 2,
            statutory: 3
          };
          productTypeMock = {
            get: (field) => productTypes[field]
          };

          MastModalFactoryMock = {
            activate: () => true
          };

          // Mock up user;
          userMock = {
            fullName: 'Jing Yu'
          };

          /* eslint arrow-body-style: 0 */
          referenceDataService = {
            get: (field) => { }
          };
          referenceDataMock = {
            service: {
              codicilStatuses: [
                {
                  id: 129,
                  name: 'foo'
                },
                {
                  id: 130,
                  name: 'bar'
                }
              ]
            }
          };
          surveysMock = [
            {
              id: 1,
              productCatalogue: {
                productGroup: {
                  id: 3
                }
              },
              serviceCatalogue: {
                id: 1
              }
            }
          ];
          wipActionableItemsMock = [
            {
              id: 1,
              job: {
                id: 1
              },
              route: 'actionable-item'
            }
          ];
          wipAssetNotesMock = [
            {
              id: 11,
              job: {
                id: 1
              },
              route: 'asset-note'
            }
          ];
          wipCoCsMock = [
            {
              id: 21,
              job: {
                id: 1
              },
              route: 'coc',
              defect: {
                id: 22,
                defectCategory: {
                  name: 'Ships'
                },
                repairCount: 2
              }
            }
          ];

          vm = _$controller_(EditScopeJobsController,
            {
              $q: qMock,
              $rootScope: rootScope,
              $scope: scopeMock,
              $state: stateMock,
              $stateParams: stateParamsMock,
              attachmentCount: attachmentCountMock,
              attachmentPath: attachmentPathMock,
              dueStatus: dueStatusMock,
              employeeRole: employeeRoleMock,
              job: jobMock,
              jobStatus: jobStatusMock,
              productType: productTypeMock,
              moment: _moment_,
              MastModalFactory: MastModalFactoryMock,
              referenceDataService,
              surveys: surveysMock,
              referenceData: referenceDataMock,
              wipActionableItems: wipActionableItemsMock,
              wipAssetNotes: wipAssetNotesMock,
              wipCoCs: wipCoCsMock,
              codicilDecisionService,
              defectDecisionService,
              jobDecisionService,
              navigationService,
              surveyDecisionService,
              user: userMock
            },
            {
              _selectedScopeItems: [
                { id: 101, route: 'coc' },
                { id: 2, route: 'coc' }
              ]
            });

          scopeMock.$digest();
        });
      };

      buildServices();
      buildInjections();
    });

    /* eslint no-loop-func: 0 */
    const methods = ['assignProductFamily', 'updateActiveJob', 'setUpData',
      'confirmJobScope', 'getDisplayStatus', 'deleteSelected',
      'singleDefectLink', 'singleCodicilLink', 'ctaVisible',
      'setSelectedValueForAll', 'addSelectedScopeItems', 'getSurveyCode',
      'serviceStatusWarning', 'editableSDO', 'validForm', 'loadAddScopePage'];
    for (let i = 0; i < methods.length; i += 1) {
      it(`should have a "${methods[i]}" method`, async () => {
        expect(`$rootScope.${methods[i]}`).toBeDefined();
      });
    }

    const getters = ['jobStatusAllowsButtons', 'currentUserIsLeadSurveyor',
      'codicilStatuses', 'selectedScopeItemsSize', 'serviceCatalogues', 'job',
      'productTypes', 'classificationWipSurveys', 'statutoryWipSurveys',
      'mmsWipSurveys', 'jobGeneratedSurveys', 'wipAssetNotes', 'wipCoCs',
      'wipActionableItems', 'isScopeEmpty'];
    for (let i = 0; i < getters.length; i += 1) {
      it(`should have a "${getters[i]}" getter`, async () => {
        expect(`$rootScope.${getters[i]}`).toBeDefined();
      });
    }

    describe('on controller creation', () => {
      it('"setUpData" should', async () => {
        spyOn(vm, 'updateActiveJob').and.callFake(() => true);
        spyOn(vm, 'assignProductFamily').and.callFake(() => true);
        await vm.setUpData();
        expect(defectDecisionService.get).toHaveBeenCalled();
        expect(vm.updateActiveJob).toHaveBeenCalled();
        expect(vm.assignProductFamily).toHaveBeenCalled();
      });
      it('"updateActiveJob" should set status to unconfirmed', async () => {
        await vm.updateActiveJob();
        expect(vm._job.scopeConfirmed).toBe(false);
        expect(vm._job.scopeConfirmedBy).toBe(null);
        expect(vm._job.scopeConfirmedDate).toBe(null);
        expect(jobDecisionService.updateJob).toHaveBeenCalled();

        stateParamsMock.addition = false;
        jobMock.scopeConfirmed = true;
        jobMock.scopeConfirmedBy = 'Jing Yu';
        jobMock.scopeConfirmedDate = '2016-09-01';
        await vm.updateActiveJob();
        expect(vm._job.scopeConfirmed).toBe(true);
        expect(vm._job.scopeConfirmedBy).toBe('Jing Yu');
        expect(vm._job.scopeConfirmedDate).toBe('2016-09-01');
      });
      xit('"assignProductFamily" should set product family', async () => {
        const family = await vm.assignProductFamily();
      });
    });

    describe('CTA buttons correctly visible', () => {
      it('disables CTA buttons in certain job statuses', async () => {
        expect(await vm.ctaVisible()).toBe(true);

        vm.job.jobStatus.id = 2;
        expect(await vm.ctaVisible()).toBe(true);

        vm.job.jobStatus.id = 3;
        expect(await vm.ctaVisible()).toBe(true);

        vm.job.jobStatus.id = 4;
        expect(await vm.ctaVisible()).toBe(true);

        vm.job.jobStatus.id = 5;
        expect(await vm.ctaVisible()).toBe(false);
      });
    });

    describe('Defect/Codicil links', () => {
      it('Defect link should trigger $state.go', async () => {
        spyOn(vm, 'singleDefectLink').and.callThrough();
        const codicil = {
          route: 'wip-coc',
          id: 2,
          defect: {
            id: 3
          },
          job: {
            id: 1
          }
        };
        await vm.singleDefectLink(codicil);
        expect(stateMock.go).toHaveBeenCalledWith('crediting.defects.view', jasmine.any(Object));
      });
      it('Codicil link should trigger $state.go', async () => {
        spyOn(vm, 'singleCodicilLink').and.callThrough();
        const codicil = {
          route: 'wip-asset-note',
          id: 1,
          parent: {
            id: 2
          }
        };
        await vm.singleCodicilLink(codicil);
        expect(stateMock.go).toHaveBeenCalledWith('crediting.assetNotes.view', jasmine.any(Object));
      });
    });

    describe('Labelling services correctly', () => {
      it('Label service as "overdue" or "due"', async () => {
        const service = {
          scheduledService: {
            id: 3,
            dueStatus: {
              id: 3
            }
          }
        };
        let warning = await vm.serviceStatusWarning(service);
        expect(warning).toEqual('Overdue');
        service.scheduledService.dueStatus.id = 2;
        warning = await vm.serviceStatusWarning(service);
        expect(warning).toEqual('Due');
        service.scheduledService = null;
        warning = await vm.serviceStatusWarning(service);
        expect(warning).toEqual();
      });
    });

    describe('Confirm job scope', () => {
      it('adds items to scope correctly', async () => {
        const obj = {
          checkBoxModel: 0,
          id: 101,
          route: 'coc'
        };
        const items = angular.copy(vm._selectedScopeItems);
        await vm.addSelectedScopeItems(obj);
        expect(_.size(vm._selectedScopeItems)).not.toEqual(_.size(items));
      });
      it('clicking on the "Confirm job scope" button', async () => {
        await vm.confirmJobScope();
        expect(jobDecisionService.updateJob).toHaveBeenCalled();
      });
    });

    describe('Helper Functions', () => {
      it('getter "selectedScopeItemsSize" should return a number', async () => {
        expect(angular.isNumber(await vm.selectedScopeItemsSize)).toBe(true);
      });
      it('getter "codicilStatuses" should return an object', async () => {
        const status = await vm.codicilStatuses;
        expect(angular.isObject(status)).toBe(true);
      });
      it('getter "jobStatusAllowsButtons" should return a boolean', async () => {
        expect(await vm.jobStatusAllowsButtons).toMatch(/true|false/);
      });
      it('getter "currentUserIsLeadSurveyor" should return a boolean', async () => {
        expect(await vm.currentUserIsLeadSurveyor).toMatch(/true|false/);
      });
      it('getter "job" should return an object', async () => {
        expect(angular.isObject(await vm.job)).toBe(true);
      });
      it('getter "productTypes" should return an object', async () => {
        expect(angular.isObject(await vm.productTypes)).toBe(true);
      });
      it('getter "classificationWipSurveys" should return an object', async () => {
        expect(angular.isObject(await vm.classificationWipSurveys)).toBe(true);
      });
      it('getter "statutoryWipSurveys" should return an object', async () => {
        expect(angular.isObject(await vm.statutoryWipSurveys)).toBe(true);
      });
      it('getter "mmsWipSurveys" should return an object', async () => {
        expect(angular.isObject(await vm.mmsWipSurveys)).toBe(true);
      });
      it('getter "jobGeneratedSurveys" should return an object', async () => {
        expect(angular.isObject(await vm.jobGeneratedSurveys)).toBe(true);
      });
      it('getter "wipAssetNotes" should return an object', async () => {
        expect(angular.isObject(await vm.wipAssetNotes)).toBe(true);
      });
      it('getter "wipActionableItems" should return an object', async () => {
        expect(angular.isObject(await vm.wipActionableItems)).toBe(true);
      });
      it('getter "wipCoCs" should return an object', async () => {
        expect(angular.isObject(await vm.wipCoCs)).toBe(true);
      });
      it('getter "isScopeEmpty" should return a boolean', async () => {
        expect(await vm.isScopeEmpty).toMatch(/true|false/);
      });
      it('"getDisplayStatus" should return an object', async () => {
        const status = await vm.getDisplayStatus(129);
        expect(angular.isString(status)).toBe(true);
      });
      it('"loadAddScopePage" should use $state.go', async () => {
        spyOn(vm, 'loadAddScopePage').and.callThrough();
        vm.loadAddScopePage();
        expect(stateMock.go).toHaveBeenCalledWith('asset.jobs.add-scope',
          jasmine.any(Object), jasmine.any(Object));
      });
      it('"getSurveyCode" should return a string', async () => {
        expect(angular.isString(await vm.getSurveyCode(1))).toBe(true);
      });
      it('"editableSDO" should return a boolean', async () => {
        expect(await vm.editableSDO()).toMatch(/true|false/);
      });
    });
  });
});
