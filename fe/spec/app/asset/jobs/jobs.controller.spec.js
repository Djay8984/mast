import _ from 'lodash';

import 'spec/spec-helper';

import AssetJobsController from
  'app/asset/jobs/jobs.controller';
import jobsFixture from 'spec/fixtures/asset-jobs-data.json';

xdescribe('Asset Model Jobs Controller', () => {
  let controller, jobsForAsset;

  beforeEach(() => {
    jobsForAsset = angular.fromJson(jobsFixture);

    inject((_$rootScope_, _$controller_) => {
      const $rootScope = _$rootScope_;
      controller = _$controller_(
        AssetJobsController, {
          user: { fullName: 'Allan Hyde' },
          jobs: jobsForAsset
        }, {});
      $rootScope.$apply();
    });
  });

  describe('Make sure data setup is correct', () => {
    it('should have same number of jobs as jobForAsset', () => {
      expect(!_.isEmpty(jobsForAsset));
      expect(_.size(jobsForAsset)).toEqual(_.size(controller.jobs));
    });
  });
  describe('Should get reduced set of jobs when username provided', () => {
    it('should list one job', () => {
      expect(_.size(controller.jobsForCurrentUser)).toEqual(1);
    });
  });

  describe('Should get reduced set of jobs for other employee list', () => {
    it('you had one job', () => {
      expect(_.size(controller.jobsForOtherEmployee)).toEqual(1);
    });
  });
});
