import * as _ from 'lodash';

import services from 'app/services';
import 'spec/spec-helper';
import AddJobsController from
  'app/asset/jobs/add/add-job.controller.js';
import 'angular-moment';

describe('Add Jobs Controller', () => {
  let $rootScope;
  let vm;
  let scopeMock;
  let stateMock;
  let stateParamsMock;
  let assetFixture;
  let jobFixture;
  let dateFormatMock;
  let jobOnlineService;
  let navigationService;
  let referenceDataMock;
  let caseIdsMock;
  let buildInjections;
  let buildServices;
  let addJobsForm;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = (errorThrown = false) => {
      angular.mock.module(services, ($provide) => {
        // Job Online Service;
        jobOnlineService = jasmine.createSpyObj('jobOnlineService', ['addJob']);
        if (errorThrown) {
          jobOnlineService.addJob.and.throwError();
        } else {
          jobOnlineService.addJob.and.returnValue({});
        }
        $provide.value('jobOnlineService', jobOnlineService);

        // Navigation Service;
        navigationService = jasmine.createSpyObj('navigationService', ['back']);
        navigationService.back.and.returnValue({});
      });
    };

    buildInjections = () => {
      inject((_$controller_, _moment_, _$rootScope_, $compile) => {
        stateMock = jasmine.createSpyObj('$state spy', ['go']);
        stateMock.go.and.callFake(() => true);

        $rootScope = _$rootScope_;
        scopeMock = $rootScope.$new();

        // mock up state params
        stateParamsMock = {
          id: 1
        };

        assetFixture = {
          id: 1
        };

        caseIdsMock = [1];

        jobFixture = {
          caseId: _.first(caseIdsMock),
          location: 'Homs',
          description: 'This is a job',
          etaDate: '2016-08-02T23:00:00.000Z',
          etdDate: '2016-08-25T11:00:00.000Z',
          requestedAttendanceDate: '2016-08-10T11:00:00.000Z',
          asset: { 'id': 1 },
          offices: [
            { office: { 'id': 50 },
            officeRole: { 'id': 1 }
            }
          ],
          aCase: { 'id': 1 },
          jobStatus: { 'id': 1 },
          scopeConfirmed: false,
          zeroVisitJob: false
        };

        addJobsForm = {
          etaDate: {
            $valid: true
          },
          etdDate: {
            $valid: true
          },
          requestedAttendanceDate: {
            $valid: true,
            $setValidity: (field, val) =>
              addJobsForm[field].$valid = val
          },
          min: {
            $valid: true
          },
          max: {
            $valid: true
          }
        };

        dateFormatMock = 'DD MMM YYYY';

        referenceDataMock = {};

        caseIdsMock = [1];

        /* eslint babel/object-shorthand: 0 */
        vm = _$controller_(AddJobsController,
          {
            $scope: scopeMock,
            $state: stateMock,
            $stateParams: stateParamsMock,
            asset: assetFixture,
            dateFormat: dateFormatMock,
            moment: _moment_,
            referenceData: referenceDataMock,
            caseIds: caseIdsMock,
            jobOnlineService,
            navigationService
          },
          {
            jobmodel: jobFixture,
            addJobsForm: addJobsForm
          });

        vm.jobmodel = jobFixture;
      });
    };
  });

  afterEach(async () => {
    angular.module(services, []);
  });

  /* eslint no-loop-func: 0 */
  const getters = ['selectDisabled', 'jobETA', 'jobETD', 'dateFormat',
    'employeeOffices', 'caseIds', 'jobCategory'];
  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  /* eslint no-loop-func: 0 */
  const methods = [ 'setUpData', 'validForm', 'cancel', 'save', 'sdoTypeahead',
    'back', 'validFormItem'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" methods`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  describe('data', () => {
    // Ignored until I can figure out how to check today's date properly
    xit('initialises dates', () => {
      scopeMock.vm.setUpData();

      expect(scopeMock.vm.dates.etaDate.toISOString())
        .toEqual('-007983-10-11T11:00:00.000Z');
      expect(scopeMock.vm.dates.etdDate.toISOString())
        .toEqual('+012015-10-11T11:00:00.000Z');
      expect(scopeMock.vm.dates.svcReqdDate.toISOString())
        .toEqual('+012015-10-11T11:00:00.000Z');
    });

    describe('initialises jobmodel', () => {
      it('with case Id on $stateParams', () => {
        buildInjections({ stateParams: { id: 1, caseId: 2 } });
        scopeMock.vm = vm;

        scopeMock.vm.setUpData();

        expect(scopeMock.vm.jobmodel.zeroVisitJob).toEqual(false);
        expect(scopeMock.vm.jobmodel.sdo).toEqual('');
        expect(scopeMock.vm.jobmodel.caseId).toEqual(2);
        expect(scopeMock.vm._selectDisabled).toEqual(true);
      });

      it('without case Id on $stateParams', () => {
        scopeMock.vm.setUpData();

        expect(scopeMock.vm.jobmodel.zeroVisitJob).toEqual(false);
        expect(scopeMock.vm.jobmodel.sdo).toEqual('');
        expect(scopeMock.vm.jobmodel.caseId).toEqual(1);
        expect(scopeMock.vm._selectDisabled).toEqual(true);
      });
    });
  });

  // Does the basic saving work?
  it('can save a basic job', async () => {
    buildServices();
    buildInjections();
    await vm.save();
    expect(jobOnlineService.addJob).toHaveBeenCalled();
    expect(stateMock.go).toHaveBeenCalled();
  });

  // If we cause an error in saving, does the catch work?
  it('can save to throw an error', async () => {
    buildServices(true);
    buildInjections();
    await vm.save();
    expect(jobOnlineService.addJob).toHaveBeenCalled();
    expect(stateMock.go).toHaveBeenCalledWith('asset.jobs.view',
      jasmine.any(Object));
  });

  // If setting etaDate to greater than the requested attendance date triggers
  // the validation;
  it('setting incorrect etaDate sets form to invalid', () => {
    buildServices();
    buildInjections();
    scopeMock.vm = vm;

    // Setting the etaDate to higher than requestedAttendanceDate will trigger
    // the validity of min to be false;
    scopeMock.$apply('vm.jobmodel.etaDate = "2016-09-16T11:00:00.000Z"');
    expect(scopeMock.vm.addJobsForm.min.$valid).toBe(false);

    // Setting the etaDate to lower than the requestedAttendanceDate will
    // trigger the validity of min to be true;
    scopeMock.$apply('vm.jobmodel.etaDate = "2016-04-16T11:00:00.000Z"');
    expect(scopeMock.vm.addJobsForm.min.$valid).toBe(true);
  });

  // If setting etdDate to lower than the requested attendance date triggers
  // the validation;
  it('setting incorrect etdDate sets form to invalid', () => {
    buildServices();
    buildInjections();
    scopeMock.vm = vm;

    scopeMock.$apply('vm.jobmodel.etdDate = "2016-09-16T11:00:00.000Z"');
    expect(scopeMock.vm.addJobsForm.max.$valid).toBe(true);

    scopeMock.$apply('vm.jobmodel.etdDate = "2016-04-16T11:00:00.000Z"');
    expect(scopeMock.vm.addJobsForm.max.$valid).toBe(false);
  });

  describe('Helper Functions', () => {
    it('getter "jobCategory" should return an object', async () => {
      expect(angular.isObject(vm.jobCategory)).toBe(true);
    });

    it('getter "caseIds" should return an array', async () => {
      expect(angular.isArray(vm.employeeOffices)).toBe(true);
    });

    it('"sdoTypeahead" should return an array', async () => {
      expect(angular.isArray(vm.sdoTypeahead(''))).toBe(true);
    });

    it('"back" should use the navigation service', async () => {
      await vm.back();
      expect(navigationService.back).toHaveBeenCalled();
    });
  });
});
