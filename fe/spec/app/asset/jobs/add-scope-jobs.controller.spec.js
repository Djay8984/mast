import * as _ from 'lodash';
import services from 'app/services';
import 'spec/spec-helper';
import AddScopeJobsController from
  'app/asset/jobs/add-scope/add-scope-jobs.controller.js';
import 'angular-moment';

describe('Add Job Scope Controller', () => {
  let vm;
  let qMock;
  let scopeMock;
  let rootScope;
  let stateMock;
  let stateParamsMock;
  let actionableItemsMock;
  let assetMock;
  let assetNotesMock;
  let cocsMock;
  let dataForServiceListMock;
  let dueStatusMock;
  let jobMock;
  let jobStatusMock;
  let MastModalFactoryMock;
  let referenceDataMock;
  let wipActionableItemsMock;
  let wipAssetNotesMock;
  let wipCoCsMock;
  let serviceStatusMock;
  let serviceCreditStatusMock;
  let serviceTypeStatusMock;
  let surveyorRoleMock;
  let userMock;
  let buildServices;
  let buildInjections;
  let codicilDecisionService;
  let defectOnlineService;
  let jobDecisionService;
  let navigationService;

  describe('Service schedule services', () => {
    beforeEach(async () => {
      angular.mock.module('angularMoment');

      buildServices = (errorThrown = false) => {
        angular.mock.module(services, ($provide) => {
          // Codicil Decision Service;
          codicilDecisionService =
            jasmine.createSpyObj('codicilDecisionService', ['addJob']);
          if (errorThrown) {
            codicilDecisionService.addJob.and.throwError();
          } else {
            codicilDecisionService.addJob.and.returnValue({});
          }
          $provide.value('codicilDecisionService', codicilDecisionService);

          // Defect Online Service;
          defectOnlineService =
            jasmine.createSpyObj('defectOnlineService', ['addJob']);
          if (errorThrown) {
            defectOnlineService.addJob.and.throwError();
          } else {
            defectOnlineService.addJob.and.returnValue({});
          }
          $provide.value('defectOnlineService', defectOnlineService);

          // Job Decision Service;
          jobDecisionService = jasmine.createSpyObj('jobDecisionService', ['addJob']);
          if (errorThrown) {
            jobDecisionService.addJob.and.throwError();
          } else {
            jobDecisionService.addJob.and.returnValue({});
          }
          $provide.value('jobDecisionService', jobDecisionService);

          // Navigation Service;
          navigationService = jasmine.createSpyObj('navigationService', ['back']);
          navigationService.back.and.returnValue({});
        });
      };

      buildInjections = () => {
        inject((_$controller_, _$rootScope_, _moment_, _$q_) => {
          stateMock = jasmine.createSpyObj('$state', ['go']);
          stateMock.go.and.callFake(() => true);

          rootScope = _$rootScope_;
          scopeMock = rootScope.$new();

          qMock = _$q_;

          // mock up state params
          stateParamsMock = {
            id: 1,
            jobId: 1,
            assetId: 1
          };

          assetMock = {
            id: 2,
            flagState: {
              id: 3
            },
            productRuleSet: {
              id: 4
            }
          };

          const dueStatuses = {
            notDue: 1,
            due: 2,
            overdue: 3
          };
          dueStatusMock = {
            get: (field) => dueStatuses[field]
          };

          dataForServiceListMock = {
            services: {
              plain: () => true
            },
            productModel: { },
            wipSurveys: { }
          };

          jobMock = {
            jobStatus: {
              id: 1
            },
            employees: [
              {
                id: 1,
                lrEmployee: {
                  id: 1,
                  name: 'Jing Yu'
                },
                employeeRole: {
                  id: 11
                }
              }
            ]
          };

          actionableItemsMock = { };

          assetMock = { };

          assetNotesMock = { };

          cocsMock = { };

          // Mock up jobStatus;
          const jobStatuses = {
            sdoAssigned: 1,
            resourceAssigned: 2,
            underSurvey: 3,
            underReporting: 4
          };
          jobStatusMock = {
            toObject: () => jobStatuses,
            get: (field) => jobStatuses[field]
          };

          MastModalFactoryMock = {
            activate: () => true
          };

          // Mock up user;
          userMock = {
            fullName: 'Jing Yu'
          };

          referenceDataMock = { };

          // Mock up surveyorRole;
          const surveyorRoles = {
            leadSurveyor: 11,
            eicManager: 10,
            authorisingSurvey: 6,
            sdoCoordinator: 12
          };
          surveyorRoleMock = {
            toObject: () => surveyorRoles,
            get: (field) => surveyorRoles[field]
          };

          serviceCreditStatusMock = { };

          const serviceStatuses = {
            notStarted: 1,
            partHeld: 2,
            held: 3,
            complete: 4
          };
          serviceStatusMock = {
            toObject: () => serviceStatuses
          };

          serviceTypeStatusMock = { };

          wipActionableItemsMock = { };

          wipAssetNotesMock = { };

          wipCoCsMock = { };

          vm = _$controller_(AddScopeJobsController,
            {
              $q: qMock,
              $rootScope: rootScope,
              $scope: scopeMock,
              $state: stateMock,
              $stateParams: stateParamsMock,
              actionableItems: actionableItemsMock,
              asset: assetMock,
              assetNotes: assetNotesMock,
              cocs: cocsMock,
              dataForServiceList: dataForServiceListMock,
              dueStatus: dueStatusMock,
              job: jobMock,
              jobStatus: jobStatusMock,
              moment: _moment_,
              MastModalFactory: MastModalFactoryMock,
              referenceData: referenceDataMock,
              wipActionableItems: wipActionableItemsMock,
              wipAssetNotes: wipAssetNotesMock,
              wipCoCs: wipCoCsMock,
              serviceStatus: serviceStatusMock,
              serviceCreditStatus: serviceCreditStatusMock,
              serviceTypeStatus: serviceTypeStatusMock,
              surveyorRole: surveyorRoleMock,
              codicilDecisionService,
              defectOnlineService,
              jobDecisionService,
              navigationService,
              user: userMock
            },
            {
              _job: jobMock
            });
        });
      };
    });

    describe('Should prepare services', () => {
      it('Should create scheduled services correctly', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'createScheduledServices').and.callFake(() => true);
        await vm.setUpScheduledServices();
        expect(vm.createScheduledServices).toHaveBeenCalled();
      });
      it('Should create model services correctly', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'createModelServices').and.callFake(() => true);
        spyOn(vm, 'createNewModelServices').and.callFake(() => true);
        await vm.setUpModelServices();
        expect(vm.createModelServices).toHaveBeenCalled();
        expect(vm.createNewModelServices).toHaveBeenCalled();
      });
      it('Should group services by type for display', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'groupServices').and.callFake(() => true);
        spyOn(vm, 'getDueOverdueServices').and.callFake(() => true);
        spyOn(vm, 'expandServices').and.callFake(() => true);
        await vm.setUpServices();
        expect(vm.groupServices).toHaveBeenCalled();
        expect(vm.getDueOverdueServices).toHaveBeenCalled();
        expect(vm.expandServices).toHaveBeenCalled();
      });
      it('Should create checkboxes for each service and/or codicil', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'addCheckBoxes').and.callFake(() => true);
        spyOn(vm, 'disableAlreadySelectedItems').and.callFake(() => true);
        spyOn(vm, 'disableAlreadySelectedSurveys').and.callFake(() => true);
        spyOn(vm, 'addDefectAndRepairCount').and.callFake(() => true);
        await vm.setUpData();
        expect(vm.addCheckBoxes).toHaveBeenCalled();
        expect(vm.disableAlreadySelectedItems).toHaveBeenCalled();
        expect(vm.disableAlreadySelectedSurveys).toHaveBeenCalled();
        expect(vm.addDefectAndRepairCount).toHaveBeenCalled();
      });
    });

    describe('Codicils: CoCs, AIs, and ANs', () => {
      it('Should display codicils', async () => {
        buildServices();
        buildInjections();
        const codicilsFunctions =
          ['jobAssetNotes', 'jobActionableItems', 'jobCocs', 'wipAssetNotes',
          'wipActionableItems', 'wipCoCs'];
        vm._assetNotes = [ { id: 1 } ];
        vm._actionableItems = [ { id: 2 } ];
        vm._cocs = [ { id: 3 } ];
        vm._wipAssetNotes = [ { id: 4 } ];
        vm._wipActionableItems = [ { id: 5 } ];
        vm._wipCoCs = [ { id: 6 } ];
        for (let i = 0; i < codicilsFunctions.length; i += 1) {
          expect(vm[codicilsFunctions[i]].length).toEqual(1);
        }
      });
    });

    describe('Saving', () => {
      it('Selecting "Add" moves to the edit scope page', async () => {
        buildServices();
        buildInjections();
        vm._selectedScopeItems = [
          {
            id: 1,
            route: 'coc',
            title: 'test',
            parent: {
              id: null
            },
            confidentialityType: {
              id: 1
            },
            job: {
              id: 1
            },
            category: { },
            imposedDate: (new Date()).toISOString(),
            dueDate: (new Date()).toISOString()
          }
        ];
        spyOn(vm, 'shouldContinueWithNewServiceNames').and.callFake(() => true);
        spyOn(vm, 'saveSelectedServices').and.callFake(() => true);
        spyOn(vm, 'resolveSavePromises').and.callFake(() => true);
        spyOn(vm, 'moveToEditScope').and.callFake(() => true);
        await vm.save();
        expect(vm.shouldContinueWithNewServiceNames).toHaveBeenCalled();
        expect(vm.saveSelectedServices).toHaveBeenCalled();
        expect(vm.resolveSavePromises).toHaveBeenCalled();
        expect(vm.moveToEditScope).toHaveBeenCalled();
        expect(stateMock.go).toHaveBeenCalled();
      });
    });

    describe('Button states', () => {
      it('Should show the CTA buttons in the correct states', async () => {
        buildServices();
        buildInjections();

        let states = [1, 2];

        // At the start the job status is 1 or 2;
        spyOn(vm, 'ctaVisible').and.callThrough();
        for (const i in states) {
          if (states.hasOwnProperty(i)) {
            vm._job.jobStatus.id = states[i];
            const visible = await vm.ctaVisible();
            expect(visible).toBe(true);
          }
        }

        // Reset the job status to 5 - 8;
        states = [5, 6, 7, 8];
        for (const i in states) {
          if (states.hasOwnProperty(i)) {
            vm._job.jobStatus.id = states[i];
            const visible = await vm.ctaVisible();
            expect(visible).toBe(false);
          }
        }
      });
    });

    // There are three buttons at the top of the job scope page - two actually
    // change the data, those are the ones we'll test;
    describe('Switching service types', () => {
      it('Switching service types should change data', async () => {
        buildServices();
        buildInjections();

        spyOn(vm, 'groupServices').and.callFake(() => true);
        spyOn(vm, 'getDueOverdueServices').and.callFake(() => true);
        spyOn(vm, 'expandServices').and.callFake(() => true);

        // Define the data we're working with;
        vm._productServices = [ { code: 'GHI' }, { code: 'JKL' } ];
        vm._scheduledServices = [ { code: 'ABC' }, { code: 'DEF' } ];

        // Expect the method to be defined, and to be a function;
        expect(vm.switchService).toBeDefined();
        expect(angular.isFunction(vm.switchService)).toBe(true);

        // Call the method with 'all';
        await vm.switchService('all');
        expect(vm.groupServices).toHaveBeenCalled();

        // Call the method with 'overdue';
        await vm.switchService('overdue');
        expect(vm.getDueOverdueServices).toHaveBeenCalled();
        expect(vm.expandServices).toHaveBeenCalled();

        // Call the method with 'schedule';
        await vm.switchService('schedule');
        expect(vm.expandServices).toHaveBeenCalled();
      });
    });

    describe('Defect/Codicil links', () => {
      it('Codicil link should trigger $state.go', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'singleCodicilLink').and.callFake(() => true);
        const codicil = {
          route: 'coc',
          id: 1
        };
        await vm.singleCodicilLink(codicil);
      });

      it('Defect link should trigger $state.go', async () => {
        buildServices();
        buildInjections();
        spyOn(vm, 'singleDefectLink').and.callFake(() => true);
        await vm.singleDefectLink();
      });
    });

    describe('Labelling services correctly', () => {
      it('Label service as "overdue" or "due"', async () => {
        buildServices();
        buildInjections();
        const service = {
          dueStatus: {
            id: 2
          },
          scheduledService: {
            id: 3
          }
        };
        let warning = await vm.requiresWarning(service);
        expect(warning).toEqual('Due');
        service.dueStatus.id = 3;
        warning = await vm.requiresWarning(service);
        expect(warning).toEqual('Overdue');
      });
    });
  });
});
