import 'spec/spec-helper';

import * as _ from 'lodash';
import 'angular-moment';

import services from 'app/services';
import EditJobController from 'app/asset/jobs/edit/edit-job.controller.js';
import employeeOfficesMock from 'spec/fixtures/edit-job-reference-data.json';

describe('Edit Job Controller', () => {
  let $rootScope;
  let vm;
  let scopeMock;
  let stateMock;
  let stateParamsMock;
  let assetFixture;
  let jobFixture;
  let jobStatus;
  let dateFormatMock;
  let jobDecisionService;
  let referenceDataMock;
  let caseIdsMock;
  let closedStatuses;
  let officeRole;
  let typeaheadService;
  let buildInjections;
  let buildServices;
  let editJobForm;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = (errorThrown = false) => {
      angular.mock.module(services, ($provide) => {
        // Job Online Service;
        jobDecisionService = jasmine.createSpyObj('jobDecisionService', ['updateJob']);
        if (errorThrown) {
          jobDecisionService.updateJob.and.throwError();
        } else {
          jobDecisionService.updateJob.and.returnValue({});
        }
        $provide.value('jobDecisionService', jobDecisionService);

        typeaheadService =
          jasmine.createSpyObj('typeaheadService', ['query']);
        typeaheadService.query.and.returnValue([]);
        $provide.value('typeaheadService', typeaheadService);
      });
    };

    buildInjections = () => {
      inject((_$controller_, _moment_, _$rootScope_, $compile) => {
        stateMock = jasmine.createSpyObj('$state spy', ['go']);
        stateMock.go.and.callFake(() => true);

        $rootScope = _$rootScope_;
        scopeMock = $rootScope.$new();

        // mock up state params
        stateParamsMock = {
          id: 1
        };

        assetFixture = {
          id: 1
        };

        caseIdsMock = [1];

        jobFixture = {
          caseId: _.first(caseIdsMock),
          location: 'Homs',
          description: 'This is a job',
          etaDate: '2016-08-02T23:00:00.000Z',
          etdDate: '2016-08-25T11:00:00.000Z',
          requestedAttendanceDate: '2016-08-10T11:00:00.000Z',
          asset: { id: 1 },
          offices: [
            { office: { id: 50 },
            officeRole: { id: 1 }
            }
          ],
          aCase: { id: 1 },
          jobStatus: { id: 1 },
          scopeConfirmed: false
        };

        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4,
          awaitingTechnicalReviewerAssignment: 5,
          underTechnicalReview: 6,
          awaitingEndorserAssignment: 7,
          underEndorsement: 8,
          cancelled: 9,
          aborted: 10,
          closed: 11
        };
        jobStatus = {
          get: (field) => jobStatuses[field],
          toObject: () => jobStatuses
        };

        editJobForm = {
          etaDate: {
            $valid: true
          },
          etdDate: {
            $valid: true
          },
          requestedAttendanceDate: {
            $valid: true,
            $setValidity: (field, val) =>
              editJobForm[field].$valid = val,
            $setDirty: (val) =>
              editJobForm.requestedAttendanceDate.$dirty = val
          },
          min: {
            $valid: true
          },
          max: {
            $valid: true
          },
          $valid: true,
          $pristine: false
        };

        const officeRoles = {
          sdo: 1,
          cfo: 2,
          cfo3: 3,
          mmsSdo: 4,
          mmso: 5,
          tso: 6,
          tsoStat: 7,
          classGroup: 8,
          dce: 9,
          eic: 10,
          mds: 11,
          tsdo: 12,
          tssao: 13,
          caseSdo: 14,
          jobSdo: 15
        };
        officeRole = {
          get: (field) => officeRoles[field],
          toObject: () => officeRoles
        };

        dateFormatMock = 'DD MMM YYYY';

        referenceDataMock = {
          get: () => angular.fromJson(employeeOfficesMock)
        };

        caseIdsMock = [1];

        const closedStatusList = [9, 10, 11];
        closedStatuses = {
          get: (field) => closedStatusList[field],
          toObject: () => closedStatusList
        };

        /* eslint babel/object-shorthand: 0 */
        vm = _$controller_(EditJobController,
          {
            $scope: scopeMock,
            $state: stateMock,
            $stateParams: stateParamsMock,
            moment: _moment_,
            jobDecisionService,
            typeaheadService,
            asset: assetFixture,
            closedStatuses,
            caseIds: caseIdsMock,
            job: jobFixture,
            jobStatus,
            referenceData: referenceDataMock,
            officeRole,
            dateFormat: dateFormatMock
          },
          {
            jobmodel: jobFixture,
            editJobForm: editJobForm
          });

        vm.jobmodel = jobFixture;
      });
    };
  });

  afterEach(async () => {
    angular.module(services, []);
  });

  /* eslint no-loop-func: 0 */
  const getters = ['closedStatusIds', 'jobETA', 'jobETD', 'employeeOffices',
    'caseIds', 'jobCategory', 'jobLocationDisabled'];
  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  /* eslint no-loop-func: 0 */
  const methods = ['setUpData', 'setUpJob', 'saveJob', 'validForm',
    'validFormItem', 'editableSDO', 'editableForm', 'findSDO', 'back'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" methods`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  describe('setup', () => {
    beforeEach(() => {
      buildServices();
      buildInjections();
      scopeMock.vm = vm;
    });

    describe('data', () => {
      it('initialises dates', () => {
        scopeMock.vm.setUpData();

        expect(_.size(vm.dates)).toEqual(3);
      });
    });

    describe('job', () => {
      it('with case Id on $stateParams', () => {
        buildInjections({ stateParams: { id: 1, caseId: 2 } });
        scopeMock.vm = vm;

        scopeMock.vm.setUpJob();

        expect(scopeMock.vm.jobmodel.zeroVisitJob).toBeFalsy();
        expect(scopeMock.vm.jobmodel.caseId)
          .toEqual(scopeMock.vm._job.aCase.id);
      });
    });
  });

  describe('save', () => {
    it('can save a basic job', async () => {
      buildServices();
      buildInjections();
      await vm.saveJob(jobFixture);
      expect(jobDecisionService.updateJob)
        .toHaveBeenCalledWith(jasmine.any(Object), true);
      expect(stateMock.go).toHaveBeenCalled();
    });
  });

  it('setting incorrect etaDate sets form to invalid', () => {
    buildServices();
    buildInjections();
    scopeMock.vm = vm;

    scopeMock.$apply('vm.jobmodel.etaDate = "2016-09-16T11:00:00.000Z"');
    expect(scopeMock.vm.editJobForm.min.$valid).toBe(false);

    scopeMock.$apply('vm.jobmodel.etaDate = "2016-04-16T11:00:00.000Z"');
    expect(scopeMock.vm.editJobForm.min.$valid).toBe(true);
  });

  it('setting incorrect etdDate sets form to invalid', () => {
    buildServices();
    buildInjections();
    scopeMock.vm = vm;

    scopeMock.$apply('vm.jobmodel.etdDate = "2016-09-16T11:00:00.000Z"');
    expect(scopeMock.vm.editJobForm.max.$valid).toBe(true);

    scopeMock.$apply('vm.jobmodel.etdDate = "2016-04-16T11:00:00.000Z"');
    expect(scopeMock.vm.editJobForm.max.$valid).toBe(false);
  });

  describe('Helper Functions', () => {
    it('getter "jobLocationDisabled" should return a boolean', async () => {
      expect(_.isBoolean(vm.jobLocationDisabled)).toBe(true);
    });

    it('getter "jobCategory" should return an object', async () => {
      expect(angular.isObject(vm.jobCategory)).toBe(true);
    });

    it('getter "caseIds" should return an array', async () => {
      expect(angular.isArray(await vm.caseIds)).toBe(true);
    });

    it('"back" should use the navigation service', async () => {
      await vm.back();
      expect(stateMock.go).toHaveBeenCalled();
    });

    it('getter "jobETA" should return a string', async () => {
      expect(angular.isString(await vm.jobETA)).toBe(true);
    });

    it('getter "jobETD" should return a string', async () => {
      expect(angular.isString(await vm.jobETA)).toBe(true);
    });

    it('"validForm" should return a boolean', async () => {
      expect(await vm.validForm()).toMatch(/true|false/);
    });

    it('"validFormItem" should return a boolean', async () => {
      expect(await vm.validFormItem()).toMatch(/true|false/);
    });

    it('"editableSDO" should return a boolean', async () => {
      expect(await vm.editableSDO()).toMatch(/true|false/);
    });
  });
});
