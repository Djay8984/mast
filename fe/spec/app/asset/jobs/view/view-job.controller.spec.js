import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';
import ViewJobController from 'app/asset/jobs/view/view-job.controller.js';

describe('View Job Controller', () => {
  let vm;
  let qMock;
  let scopeMock;
  let rootScope;
  let stateMock;
  let stateParamsMock;
  let actionableItemsMock;
  let activeBundleMock;
  let assetMock;
  let assetNotesMock;
  let allowedSpecialCharactersRegexMock;
  let cocsMock;
  let dateFormatMock;
  let jobMock;
  let jobStatusMock;
  let MastModalFactoryMock;
  let reportTypeMock;
  let wipActionableItemsMock;
  let wipAssetNotesMock;
  let wipCoCsMock;
  let surveyorRoleMock;
  let surveysMock;
  let userMock;
  let buildServices;
  let buildInjections;
  let jobDecisionService;
  let navigationService;
  let jobOnlineService;
  let officeRoleMock;
  let serviceTypeTaskCompletionsMock;
  let reportDecisionService;
  let printService;
  let reportsMock;
  let resolveMock;

  beforeEach(async () => {
    buildServices = (errorThrown = false) => {
      angular.mock.module('angularMoment');
      angular.mock.module(services, ($provide) => {
        // Job Decision Service;
        jobDecisionService =
          jasmine.createSpyObj('jobDecisionService', ['get', 'updateJob']);
        if (errorThrown) {
          jobDecisionService.get.and.throwError();
          jobDecisionService.updateJob.and.throwError();
        } else {
          jobDecisionService.get.and.returnValue({});
          jobDecisionService.updateJob.and.returnValue({});
        }
        $provide.value('jobDecisionService', jobDecisionService);

        // Job Online Service;
        jobOnlineService = jasmine.createSpyObj('jobOnlineService', ['get']);
        if (errorThrown) {
          jobOnlineService.get.and.throwError();
        } else {
          jobOnlineService.get.and.returnValue({});
        }
        $provide.value('jobOnlineService', jobOnlineService);

        // Navigation Service;
        navigationService = jasmine.createSpyObj(
          'navigationService', ['back']);
        navigationService.back.and.returnValue({});

        // Print Service;
        printService = jasmine.createSpyObj(
          'printService', ['stateName', 'pagesToPrint']);
        printService.stateName.and.returnValue('job.state');
        printService.pagesToPrint.and.returnValue(1);

        // Report Decision Service;
        reportDecisionService =
          jasmine.createSpyObj('reportDecisionService', ['createReport', 'rejectReport', 'get']);
        if (errorThrown) {
          reportDecisionService.createReport.and.throwError();
          reportDecisionService.rejectReport.and.throwError();
          reportDecisionService.get.and.throwError();
        } else {
          reportDecisionService.createReport.and.returnValue({});
          reportDecisionService.rejectReport.and.returnValue({});
          reportDecisionService.get.and.returnValue({});
        }
        $provide.value('reportDecisionService', reportDecisionService);
      });
    };

    buildInjections = () => {
      inject((_$controller_, _$rootScope_, _moment_, _$q_) => {
        stateMock = jasmine.createSpyObj('$state', ['go']);
        stateMock.go.and.callFake(() => true);

        rootScope = _$rootScope_;
        scopeMock = rootScope.$new();

        qMock = _$q_;

        dateFormatMock = 'DD MMM YYYY';

        // mock up state params
        stateParamsMock = {
          id: 1,
          jobId: 1,
          assetId: 1
        };

        assetMock = {
          id: 2,
          flagState: {
            id: 3
          },
          productRuleSet: {
            id: 4
          }
        };

        jobMock = {
          jobStatus: {
            id: 1
          },
          employees: [
            {
              id: 1,
              lrEmployee: {
                id: 1,
                name: 'Jing Yu'
              },
              employeeRole: {
                id: 11
              }
            }
          ]
        };

        actionableItemsMock = { };
        assetMock = { };
        assetNotesMock = { };
        cocsMock = { };

        // Mock up jobStatus;
        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4,
          awaitingTechnicalReviewerAssignment: 5
        };
        jobStatusMock = {
          toObject: () => jobStatuses,
          get: (field) => jobStatuses[field]
        };

        // Mock up surveyorRole;
        const surveyorRoles = {
          leadSurveyor: 11,
          eicManager: 10,
          authorisingSurvey: 6,
          sdoCoordinator: 12
        };
        surveyorRoleMock = {
          toObject: () => surveyorRoles,
          get: (field) => surveyorRoles[field]
        };

        MastModalFactoryMock = {
          activate: () => true
        };

        vm = _$controller_(ViewJobController, {
          $q: qMock,
          $rootScope: rootScope,
          $scope: scopeMock,
          $state: stateMock,
          $stateParams: stateParamsMock,
          actionableItems: actionableItemsMock,
          asset: assetMock,
          assetNotes: assetNotesMock,
          activeBundle: activeBundleMock,
          allowedSpecialCharactersRegex: allowedSpecialCharactersRegexMock,
          cocs: cocsMock,
          job: jobMock,
          jobStatus: jobStatusMock,
          moment: _moment_,
          MastModalFactory: MastModalFactoryMock,
          wipActionableItems: wipActionableItemsMock,
          wipAssetNotes: wipAssetNotesMock,
          wipCoCs: wipCoCsMock,
          dateFormat: dateFormatMock,
          reportType: reportTypeMock,
          surveyorRole: surveyorRoleMock,
          officeRole: officeRoleMock,
          serviceTypeTaskCompletions: serviceTypeTaskCompletionsMock,
          surveys: surveysMock,
          user: userMock,
          reports: reportsMock,
          jobDecisionService,
          printService,
          resolve: resolveMock
        }, {
          _job: jobMock
        });

        scopeMock.$digest();
      });
    };

    buildServices();
    buildInjections();
  });

  afterEach(async () => {
    rootScope.$apply();
  });

  it('should have a "setUpData" method', async () => {
    expect(vm.setUpData).toBeDefined();
  });

  it('should have a "showJobDatesEditor" method', async () => {
    expect(vm.showJobDatesEditor).toBeDefined();
  });

  describe('Updating first and last visit dates', () => {
    it('canEditJobDates should return a boolean', async () => {
      expect(vm.canEditJobDates).toBeDefined();

      const retValue = vm.canEditJobDates;
      expect(retValue).toMatch(/true|false/);
    });

    it('firstVisitDate should return a string', async () => {
      expect(vm.firstVisitDate).toBeDefined();
      const retValue = vm.firstVisitDate;
      expect(angular.isString(retValue)).toBe(true);
    });

    it('lastVisitDate should return a string', async () => {
      expect(vm.lastVisitDate).toBeDefined();
      const retValue = vm.lastVisitDate;
      expect(angular.isString(retValue)).toBe(true);
    });
  });
});
