import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';

import ManageCreditingController from
  'app/asset/jobs/view/crediting/manage/manage-crediting.controller.js';

describe('Job View', () => {
  let vm;
  let $log;
  let $rootScope;
  let $scopeMock;
  let $stateMock;
  let $state;
  let $stateParamsMock;
  let actionableItemsMock;
  let assetNotesMock;
  let cocsMock;
  let creditingMock;
  let creditStatusesMock;
  let employeeRoleMock;
  let farSubmissionStateMock;
  let jobMock;
  let jobAttachmentCountMock;
  let jobAttachmentPathMock;
  let jobStatusMock;
  let MastModalFactory;
  let reportTypeMock;
  let surveyCreditStatusesMock;
  let userMock;

  let creditingDecisionService;
  let defectDecisionService;
  let navigationConfirmationService;
  let printService;
  let reportDecisionService;
  let buildServices;
  let buildInjections;

  beforeEach(() => {
    buildServices = () => {
      angular.mock.module('angularMoment');

      angular.mock.module(services, ($provide) => {
        creditingDecisionService =
          jasmine.createSpyObj('creditingDecisionService',
            ['update', 'groupCreditingByProductType']);
        creditingDecisionService.update.and.returnValue({});
        creditingDecisionService.groupCreditingByProductType.and.returnValue({});
        $provide.value('creditingDecisionService', creditingDecisionService);

        defectDecisionService = jasmine.createSpy('defectDecisionService');
        $provide.value('defectDecisionService', defectDecisionService);

        navigationConfirmationService =
          jasmine.createSpyObj('navigationConfirmationService',
            ['setConfirmationCase']);
        navigationConfirmationService.setConfirmationCase.and.returnValue({});
        $provide.value('navigationConfirmationService',
          navigationConfirmationService);

        printService = jasmine.createSpyObj('printService', ['pagesToPrint']);
        printService.pagesToPrint.and.returnValue({});
        $provide.value('printService', printService);

        reportDecisionService =
          jasmine.createSpyObj('reportDecisionService',
            ['getVersions', 'createReport']);
        reportDecisionService.getVersions.and.returnValue({});
        reportDecisionService.createReport.and.returnValue({});
        $provide.value('reportDecisionService', reportDecisionService);

        $stateMock = jasmine.createSpyObj('$state', ['go']);
        $stateMock.go.and.callFake(() => true);
        $stateMock.current = { name: 'crediting.manage' };
        $provide.value('$state', $stateMock);
      });
    };
    buildInjections = () => {
      inject((_$rootScope_, _$controller_, _$log_, _$state_) => {
        $rootScope = _$rootScope_;
        $scopeMock = $rootScope.$new();
        $log = _$log_;
        $stateParamsMock = {
          jobId: 1,
          name: 'crediting.manage',
          current: {
            name: 'crediting.manage'
          }
        };
        $state = _$state_;
        actionableItemsMock = { id: 1 };
        assetNotesMock = { id: 1 };
        cocsMock = { id: 1 };
        creditingMock = [
          {
            id: 1,
            name: 'Classification',
            surveys: { }
          }
        ];
        const creditStatuses = {
          confirmed: 1,
          completed: 2,
          waived: 3,
          uncredited: 4
        };
        creditStatusesMock = {
          get: (field) => creditStatuses[field]
        };

        employeeRoleMock = { id: 1 };
        farSubmissionStateMock = { id: 1 };
        jobMock = {
          id: 1,
          asset: {
            id: 6
          },
          surveys: [
            { id: 1, autoGenerated: true },
            { id: 2, autoGenerated: true },
            { id: 3, autoGenerated: false }
          ]
        };
        jobAttachmentCountMock = 1;
        jobAttachmentPathMock = '/some/path';

        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4,
          awaitingTechnicalReviewerAssignment: 5,
          underTechnicalReview: 6,
          awaitingEndorserAssignment: 7,
          underEndorsement: 8,
          cancelled: 9,
          aborted: 10,
          closed: 11
        };
        jobStatusMock = {
          get: (field) => jobStatuses[field],
          toObject: () => jobStatuses
        };

        MastModalFactory = () => ({
          activate: () => true
        });

        const reportTypes = {
          fsr: 1,
          far: 2,
          dsr: 3,
          dar: 4
        };
        reportTypeMock = {
          get: (field) => reportTypes[field],
          toObject: () => reportTypes
        };

        const surveyCreditStatuses = {
          notStarted: 1,
          partHeld: 2,
          postponed: 3,
          complete: 4,
          finished: 5
        };
        surveyCreditStatusesMock = {
          get: (field) => surveyCreditStatuses[field],
          toObject: () => surveyCreditStatuses
        };

        userMock = { id: 1 };

        vm = _$controller_(ManageCreditingController,
          {
            $log,
            $rootScope,
            $scope: $scopeMock,
            $state,
            $stateParams: $stateParamsMock,
            actionableItems: actionableItemsMock,
            asset: assetNotesMock,
            cocs: cocsMock,
            crediting: creditingMock,
            creditStatuses: creditStatusesMock,
            employeeRole: employeeRoleMock,
            farSubmissionState: farSubmissionStateMock,
            job: jobMock,
            jobAttachmentCount: jobAttachmentCountMock,
            jobAttachmentPath: jobAttachmentPathMock,
            jobStatus: jobStatusMock,
            MastModalFactory,
            reportType: reportTypeMock,
            surveyCreditStatuses: surveyCreditStatusesMock,
            user: userMock,
            creditingDecisionService,
            defectDecisionService,
            navigationConfirmationService,
            printService,
            reportDecisionService
          },
          {
            creditingForm: {
              $setPristine: () => true
            }
          });
      });
    };

    buildServices();
    buildInjections();
  });

  const methods = ['editCancel', 'editSave', 'editStart', 'save', 'hasSurveys',
    'filterAutoGeneratedSurveys', 'filterNonAutoGeneratedSurveys',
    'darReportVersions', 'generateDAR', 'generateFAR', 'needNavConfirmation',
    'doDiscard', 'viewJob', 'saveAndViewJob'];
  for (let i = 0; i < methods.length; i += 1) {
    /* eslint no-loop-func: 0 */
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  const getters = ['job', 'jobAttachmentCount', 'jobAttachmentPath',
    'crediting', 'creditStatuses', 'hasGeneratedSurveys', 'codicilsCreditable',
    'actionableItems', 'cocs', 'farSubmissionState',
    'currentUserIsLeadSurveyor', 'currentUserIsInTeam', 'generateDARActive',
    'generateFARActive', 'surveyCreditStatuses'];
  for (let i = 0; i < getters.length; i += 1) {
    /* eslint no-loop-func: 0 */
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  describe('on controller creation', () => {
    it('should call methods', async () => {
      expect(navigationConfirmationService.setConfirmationCase).toHaveBeenCalled();
    });
  });

  describe('saving crediting information', () => {
    it('should make two calls to the crediting services', async () => {
      await vm.save();
      expect(creditingDecisionService.update).toHaveBeenCalled();
      expect(creditingDecisionService.groupCreditingByProductType).toHaveBeenCalled();
    });

    it('should set the form to be pristine', async () => {
      spyOn(vm.creditingForm, '$setPristine');
      await vm.save();
      expect(vm.creditingForm.$setPristine).toHaveBeenCalled();
    });
  });

  describe('filters', () => {
    it('auto-generated surveys', async () => {
      // In the job mock we're using, we have one job with one surveys array
      // in it, but of the 3 surveys, only two are auto-generated;
      const fakeSurveys = [
        { id: 1, autoGenerated: true },
        { id: 2, autoGenerated: true },
        { id: 3, autoGenerated: true }
      ];
      const result = await vm.filterAutoGeneratedSurveys(fakeSurveys);
      expect(_.size(result)).not.toEqual(_.size(fakeSurveys));
      expect(_.size(result)).toEqual(2);
    });
  });

  describe('should generate reports', () => {
    it('should generate a DAR', async () => {
      spyOn(vm, 'save').and.callThrough();
      await vm.generateDAR();
      expect(vm.save).toHaveBeenCalled();
    });

    it('should generate a FAR', async () => {
      spyOn(vm, 'save').and.callThrough();
      await vm.generateFAR();
      expect(vm.save).toHaveBeenCalled();
    });
  });
});
