import 'spec/spec-helper';
import 'angular-moment';

import GenerateDSRController from
  'app/asset/jobs/view/crediting/manage/generate-dsr.controller';
import services from 'app/services';

describe('Generate DSR Controller', () => {
  let vm;
  let $modalInstanceMock;
  let dateFormatMock;
  let jobMock;
  let reportTypeMock;
  let reportDecisionService;
  let buildServices;
  let buildInjections;

  beforeEach(async () => {
    buildServices = (errorThrown = false) => {
      angular.mock.module('angularMoment');
      angular.mock.module(services, ($provide) => {
        reportDecisionService =
          jasmine.createSpyObj('reportDecisionService', ['getVersions']);
        reportDecisionService.getVersions.and.returnValue({});
        $provide.value('reportDecisionService', reportDecisionService);
      });
    };

    buildInjections = () => {
      inject((_$controller_) => {
        $modalInstanceMock = {
          close: () => true
        };

        jobMock = {
          id: 1
        };

        dateFormatMock = 'DD MMM YYYY';

        const reportTypes = {
          fsr: 1,
          far: 2,
          dsr: 3,
          dar: 4
        };
        reportTypeMock = {
          get: (field) => reportTypes[field]
        };

        vm = _$controller_(GenerateDSRController,
          {
            $modalInstance: $modalInstanceMock,
            dateFormat: dateFormatMock,
            job: jobMock,
            reportType: reportTypeMock,
            reportDecisionService
          },
          {
            generateDARForm: {
              firstVisitDate: {
                $modelValue: {
                  value: '2016-09-01 11:29:03',
                  _d: {
                    toISOString: () => _.toString
                  }
                }
              }
            },
            _reports: [ ]
          });
      });
    };
  });

  it('should save a DSR', async () => {
    buildServices();
    buildInjections();

    spyOn(vm._$modalInstance, 'close').and.callFake(() => true);

    await vm.saveDAR();

    // Finally the modal instance should close;
    expect(vm._$modalInstance.close).toHaveBeenCalled();
  });
});
