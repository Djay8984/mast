import 'spec/spec-helper';
import 'angular-moment';

import GenerateDARController from
  'app/asset/jobs/view/crediting/manage/generate-dar.controller';
import services from 'app/services';

describe('Generate DAR Controller', () => {
  let vm;
  let $modalInstanceMock;
  let dateFormatMock;
  let jobMock;
  let reportTypeMock;
  let jobDecisionService;
  let reportDecisionService;
  let buildServices;
  let buildInjections;

  beforeEach(async () => {
    buildServices = (errorThrown = false) => {
      angular.mock.module('angularMoment');
      angular.mock.module(services, ($provide) => {
        // Job Decision Service;
        jobDecisionService =
          jasmine.createSpyObj('jobDecisionService', ['get', 'updateJob']);
        if (errorThrown) {
          jobDecisionService.get.and.throwError();
          jobDecisionService.updateJob.and.throwError();
        } else {
          jobDecisionService.get.and.returnValue({ id: 101, job: { } });
          jobDecisionService.updateJob.and.returnValue({});
        }
        $provide.value('jobDecisionService', jobDecisionService);

        reportDecisionService =
          jasmine.createSpyObj('reportDecisionService', ['getVersions']);
        reportDecisionService.getVersions.and.returnValue({});
        $provide.value('reportDecisionService', reportDecisionService);
      });
    };

    buildInjections = () => {
      inject((_$controller_) => {
        $modalInstanceMock = {
          close: () => true
        };

        jobMock = {
          id: 1
        };

        dateFormatMock = 'DD MMM YYYY';

        const reportTypes = {
          fsr: 1,
          far: 2,
          dsr: 3,
          dar: 4
        };
        reportTypeMock = {
          get: (field) => reportTypes[field]
        };

        vm = _$controller_(GenerateDARController,
          {
            $modalInstance: $modalInstanceMock,
            dateFormat: dateFormatMock,
            job: jobMock,
            reportType: reportTypeMock
          },
          {
            generateDARForm: {
              firstVisitDate: {
                $modelValue: '2015-11-16'
              }
            },
            _reports: [ ]
          });
      });
    };
  });

  it('should save a DAR', async () => {
    buildServices();
    buildInjections();

    spyOn(vm._$modalInstance, 'close').and.callFake(() => true);

    await vm.saveDAR();

    // Since this._reports will be empty we can expect jobDecisionService.get
    // to be called;
    expect(jobDecisionService.get).toHaveBeenCalled();
    expect(jobDecisionService.updateJob).not.toHaveBeenCalled();

    // Finally the modal instance should close;
    expect(vm._$modalInstance.close).toHaveBeenCalled();
  });
});
