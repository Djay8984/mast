import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';
import EditJobDatesModalController from 'app/asset/jobs/view/edit-job-visit-dates-modal.controller.js';

describe('Edit Job Visit Dates Controller', () => {
  let vm;
  let scopeMock;
  let rootScope;
  let stateMock;
  let dateFormatMock;
  let jobMock;
  let buildInjections;
  let buildServices;
  let jobDecisionService;
  let navigationService;
  let $modalInstanceMock;

  beforeEach(async () => {
    buildServices = (errorThrown = false) => {
      angular.mock.module('angularMoment');
      angular.mock.module(services, ($provide) => {
        // Job Decision Service;
        jobDecisionService =
          jasmine.createSpyObj('jobDecisionService', ['get', 'updateJob']);
        if (errorThrown) {
          jobDecisionService.get.and.throwError();
          jobDecisionService.updateJob.and.throwError();
        } else {
          jobDecisionService.get.and.returnValue({});
          jobDecisionService.updateJob.and.returnValue({});
        }
        $provide.value('jobDecisionService', jobDecisionService);

        // Navigation Service;
        navigationService = jasmine.createSpyObj(
          'navigationService', ['back']);
        navigationService.back.and.returnValue({});
      });
    };

    buildInjections = () => {
      inject((_$controller_, _$rootScope_, _moment_, _$q_) => {
        stateMock = jasmine.createSpyObj('$state', ['go']);
        stateMock.go.and.callFake(() => true);

        dateFormatMock = 'DD MMM YYYY';

        jobMock = {
          jobStatus: {
            id: 1
          },
          employees: [
            {
              id: 1,
              lrEmployee: {
                id: 1,
                name: 'Jing Yu'
              },
              employeeRole: {
                id: 11
              }
            }
          ]
        };

        $modalInstanceMock = jasmine.createSpyObj('$modalInstanceMock', ['close']);
        $modalInstanceMock.close.and.callFake(() => true);

        vm = _$controller_(EditJobDatesModalController, {
          $state: stateMock,
          $modalInstance: $modalInstanceMock,
          job: jobMock,
          moment: _moment_,
          dateFormat: dateFormatMock,
          jobDecisionService
        }, {
          _job: jobMock
        });
      });
    };

    buildServices();
    buildInjections();
  });

  it('should have a "setUpData" method', async () => {
    expect(vm.setUpData).toBeDefined();
    expect(vm.dates).toBeDefined();
    expect(vm.jobmodel).toBeDefined();
  });

  it('should have a "dateFormat" method', async () => {
    expect(vm.dateFormat).toBeDefined();
    const retValue = vm.dateFormat;
    expect(angular.isString(retValue)).toBe(true);
  });

  it('"dateFormat" should return a string', async () => {
    const retValue = vm.dateFormat;
    expect(angular.isString(retValue)).toBe(true);
  });

  it('should have a "validForm" method', async () => {
    expect(vm.validForm).toBeDefined();
  });

  describe('Updating first and last visit dates', () => {
    it('should allow dates to be saved', async () => {
      const job = {};
      await vm.saveVisitDates(job);

      // grab the job record
      expect(jobDecisionService.get).toHaveBeenCalled();

      // update the job record
      expect(jobDecisionService.updateJob).toHaveBeenCalled();

      // close the modal
      expect($modalInstanceMock.close).toHaveBeenCalled();

      // reload the state
      expect(stateMock.go).toHaveBeenCalled();
    });

    it('minLastVisitDate should return a string', async () => {
      expect(vm.minLastVisitDate).toBeDefined();
      const retValue = vm.minLastVisitDate;
      expect(angular.isString(retValue)).toBe(true);
    });

    it('maxFirstVisitDate should return a string', async () => {
      expect(vm.maxFirstVisitDate).toBeDefined();
      const retValue = vm.maxFirstVisitDate;
      expect(angular.isString(retValue)).toBe(true);
    });
  });
});
