import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';
import EditServiceController from
  'app/asset/service-schedule/edit-service/edit-service.controller.js';

describe('Service Schedule, Edit Service', () => {
  let vm;
  let rootScope;
  let stateParamsMock;
  let stateMock;
  let scopeMock;
  let dateFormatMock;
  let editFormMock;
  let assetMock;
  let assignedServicesMock;
  let serviceMock;
  let serviceCataloguesMock;
  let productCataloguesMock;
  let refServicesStatusesMock;
  let serviceStatusMock;
  let serviceTypeMock;
  let serviceScheduleService;
  let buildInjections;
  let buildServices;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        serviceScheduleService =
          jasmine.createSpyObj('serviceScheduleService',
            ['updateAssignedAssetService']);
        serviceScheduleService.updateAssignedAssetService.and.returnValue({});
        $provide.service('serviceScheduleService', serviceScheduleService);
      });
    };

    buildInjections = () => {
      inject((_$controller_, _moment_, _$rootScope_) => {
        stateMock = jasmine.createSpyObj('$state spy', ['go']);
        stateMock.go.and.callFake(() => true);

        rootScope = _$rootScope_;
        scopeMock = rootScope.$new();

        stateParamsMock = {
          id: 1
        };

        assetMock = {
          id: 1
        };

        assignedServicesMock = {
          id: 1
        };

        serviceMock = {
          serviceCatalogueId: 1,
          plain: () => serviceMock,
          completionDate: '4th April, 2016',
          assignedDate: 1375680491246,
          dueDate: 1415680491246,
          lowerRangeDate: 1375680491246,
          upperRangeDate: 1495680491246
        };

        serviceCataloguesMock = [
          {
            id: 1,
            code: 'AFSM',
            harmonisationType: {
              id: 1
            },
            harmonisationDate: {
              _d: 1415680491246
            },
            productCatalogue: {
              id: 1,
              name: 'Anti-Fouling System',
              productGroup: {
                id: 9
              },
              productType: {
                id: 3
              }
            },
            serviceType: {
              id: 6
            }
          }
        ];

        productCataloguesMock = [
          {
            id: 1
          },
          {
            id: 22
          }
        ];

        const serviceTypes = {
          alternate: 1,
          annual: 2,
          docking: 3,
          initial: 4,
          interim: 5,
          intermediate: 6,
          miscellaneous: 7,
          periodic: 8,
          renewal: 9,
          standAlone2InSpecialCycle: 10,
          additional: 11
        };
        serviceTypeMock = {
          toObject: () => serviceTypes,
          get: (field) => serviceTypes[field]
        };

        const serviceStatuses = {
          notStarted: 1,
          partHeld: 2,
          held: 3,
          complete: 4
        };
        serviceStatusMock = {
          toObject: () => serviceStatuses
        };

        refServicesStatusesMock = [
          { id: 1 },
          { id: 2 },
          { id: 3 },
          { id: 4 }
        ];

        dateFormatMock = '3rd April, 2016';

        editFormMock = {
          assignedDatePicker: {
            $pristine: false
          },
          dueDatePicker: {
            $pristine: false
          },
          lowerRangeDatePicker: {
            $pristine: false
          },
          upperRangeDatePicker: {
            $pristine: false
          },
          periodicityNumber: {
            $pristine: false
          },
          $valid: true,
          $pristine: false
        };

        vm = _$controller_(EditServiceController,
          {
            $state: stateMock,
            $stateParams: stateParamsMock,
            $scope: scopeMock,
            moment: _moment_,
            asset: assetMock,
            assignedServices: assignedServicesMock,
            dateFormat: dateFormatMock,
            productCatalogues: productCataloguesMock,
            service: serviceMock,
            serviceCatalogues: serviceCataloguesMock,
            serviceType: serviceTypeMock,
            serviceScheduleService,
            serviceStatus: serviceStatusMock,
            refServicesStatuses: refServicesStatusesMock
          },
          {
            editConfig: {
              assignedDate: 1375680491246,
              dueDate: 1415680491246,
              lowerRangeDate: 1375680491246,
              upperRangeDate: 1495680491246
            },
            editForm: editFormMock,
            _momentNow: _moment_()
          });
      });
    };

    buildServices();
    buildInjections();
  });

  const methods = ['disableLowerRange', 'disableUpperRange',
    'hasAssociatedItem', 'revealHeldStatusElaboration', 'displayName',
    'validForm', 'back', 'confirm', 'getCompletionDate'];

  /* eslint no-loop-func: 0 */
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  const getters = ['asset', 'service', 'serviceCatalogues', 'serviceCatalogue',
    'serviceTypeId', 'productCatalogue', 'serviceStatuses',
    'provisionalStatuses', 'isHarmonised', 'hasHarmonisationDate',
    'isAssignedDateDisabled', 'isDueDateDisabled', 'isAssignedDateRequired',
    'isRangeDateRequired', 'isPeriodicityEditable', 'minDueDate',
    'maxAssignedDate', 'minLowerRangeDate', 'maxLowerRangeDate',
    'minUpperRangeDate', 'dateFormat'];

  /* eslint no-loop-func: 0 */
  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  it('getter "minDueDate" returns min selectable date', async () => {
    let minDueDate = (await vm.minDueDate)._d.toString();
    expect(minDueDate).toBe(vm._momentNow._d.toString());
    vm.editConfig.assignedDate = angular.copy(vm._momentNow).add(-7, 'days');
    minDueDate = (await vm.minDueDate)._d.toString();
    expect(minDueDate).toBe(angular.copy(vm._momentNow).add(-6, 'days')._d.toString());
  });

  it('getter "maxAssignedDate" adds 3 months', async () => {
    // Create our own '3 months hence' date, then ask the method, these two
    // dates should be equal if the method is working correctly;
    const adjustedDate = vm._momentNow.add(3, 'months')._d.toString();
    const assignedDate = (await vm.maxAssignedDate)._d.toString();
    expect(adjustedDate).toBe(assignedDate);
  });

  describe('Return Values', () => {
    it('getter "asset" should return an object', async () => {
      expect(angular.isObject(await vm.asset)).toBe(true);
    });

    it('getter "service" should return an object', async () => {
      expect(angular.isObject(await vm.service)).toBe(true);
    });

    it('getter "serviceCatalogues" should return an array', async () => {
      expect(angular.isArray(await vm.serviceCatalogues)).toBe(true);
    });

    it('getter "serviceCatalogue" should return an object', async () => {
      expect(angular.isObject(await vm.serviceCatalogue)).toBe(true);
    });

    it('getter "serviceTypeId" should return an int', async () => {
      expect(angular.isNumber(await vm.serviceTypeId)).toBe(true);
    });

    it('getter "productCatalogue" should return an object', async () => {
      expect(angular.isObject(await vm.productCatalogue)).toBe(true);
    });

    it('getter "serviceStatuses" should return an array', async () => {
      expect(angular.isArray(await vm.serviceStatuses)).toBe(true);
    });

    it('getter "provisionalStatuses" should return an object', async () => {
      expect(angular.isObject(await vm.provisionalStatuses)).toBe(true);
    });

    it('getter "isHarmonised" should return a boolean', async () => {
      expect(await vm.isHarmonised).toMatch(/true|false/);
    });

    it('getter "hasHarmonisationDate" should return a boolean', async () => {
      expect(await vm.hasHarmonisationDate).toMatch(/true|false/);
    });

    it('"disableLowerRange" should return a boolean', async () => {
      vm.editConfig = {
        completionDate: '4th April, 2016',
        assignedDate: 1375680491246,
        dueDate: {
          _d: 1415680491246
        },
        lowerRangeDate: 1375680491246,
        upperRangeDate: 1495680491246
      };

      expect(await vm.disableLowerRange()).toMatch(/true/);
      serviceCataloguesMock[0].lowerRangeDateOffsetMonths = 3;
      expect(await vm.disableLowerRange()).toMatch(/false/);
    });

    it('"disableUpperRange" should return a boolean', async () => {
      vm.editConfig = {
        completionDate: '4th April, 2016',
        assignedDate: 1375680491246,
        dueDate: {
          _d: 1415680491246
        },
        lowerRangeDate: 1375680491246,
        upperRangeDate: 1495680491246
      };

      expect(await vm.disableUpperRange()).toMatch(/true/);
      serviceCataloguesMock[0].upperRangeDateOffsetMonths = 3;
      expect(await vm.disableUpperRange()).toMatch(/false/);
    });

    it('getter "isAssignedDateDisabled" should return a boolean', async () => {
      const result = await vm.isAssignedDateDisabled;
      expect(result).toMatch(/true|false/);
    });

    it('getter "isDueDateDisabled" should return a boolean', async () => {
      expect(await vm.isDueDateDisabled).toMatch(/true|false/);
      serviceCataloguesMock[0].serviceType.id = 9; // renewal
      expect(await vm.isDueDateDisabled).toBeTruthy();
      serviceCataloguesMock[0].serviceType.id = 10;
      expect(await vm.isDueDateDisabled).toBeFalsy();
    });

    it('getter "isAssignedDateRequired" should return a boolean', async () => {
      serviceCataloguesMock[0].serviceType.id = 9;
      expect(await vm.isAssignedDateRequired).toMatch(/true|false/);
    });

    it('"hasAssociatedItem" should return false', async () => {
      expect(await vm.hasAssociatedItem()).toBe(false);
    });

    it('"revealHeldStatusElaboration" should return a boolean', async () => {
      expect(await vm.revealHeldStatusElaboration()).toMatch(/true|false/);
    });

    it('getter "isPeriodicityEditable" should return a boolean', async () => {
      expect(await vm.isPeriodicityEditable).toMatch(/true|false/);
    });

    it('"displayName" should return a string', async () => {
      expect(angular.isString(await vm.displayName())).toBe(true);
    });

    it('getter "minDueDate" should return an object', async () => {
      expect(angular.isObject(await vm.minDueDate)).toBe(true);
    });

    it('getter "maxAssignedDate" should return an object', async () => {
      expect(angular.isObject(await vm.maxAssignedDate)).toBe(true);
    });

    it('getter "minLowerRangeDate" should return an object', async () => {
      expect(angular.isObject(await vm.minLowerRangeDate)).toBe(true);
    });

    it('getter "maxLowerRangeDate" should return an object', async () => {
      expect(angular.isObject(await vm.maxLowerRangeDate)).toBe(false);
      vm.editConfig.dueDate = {
        _d: 1375680491246
      };
      expect(angular.isObject(await vm.maxLowerRangeDate)).toBe(true);
    });

    it('getter minUpperRangeDate" should return an object', async () => {
      expect(angular.isObject(await vm.minUpperRangeDate)).toBe(true);
    });

    it('"validForm" should return a boolean', async () => {
      expect(await vm.validForm()).toMatch(/true|false/);
    });

    it('"back" should move the user to another page', async () => {
      await vm.back();
      expect(stateMock.go)
        .toHaveBeenCalledWith('asset.serviceSchedule.viewService',
          jasmine.any(Object), jasmine.any(Object));
    });

    it('"confirm" should move the user to another page', async () => {
      const {
        assignedDate, dueDate, lowerRangeDate, upperRangeDate
      } = vm.editConfig;
      spyOn(vm, 'getCompletionDate').and.callThrough();
      spyOn(vm, 'revealHeldStatusElaboration').and.callThrough();
      spyOn(vm, 'back').and.callThrough();
      await vm.confirm();
      expect(vm.finalService.assignedDateManual).toEqual(null);
      expect(vm.getCompletionDate).toHaveBeenCalled();
      expect(vm.revealHeldStatusElaboration).toHaveBeenCalled();
      expect(serviceScheduleService.updateAssignedAssetService).toHaveBeenCalled();
      expect(vm.back).toHaveBeenCalledWith(true);
    });

    it('getter "dateFormat" should return a string', async () => {
      const result = await vm.dateFormat;
      expect(angular.isString(result)).toBe(true);
      expect(result).toEqual('3rd April, 2016');
    });

    it('"getCompletionDate" returns a string or null', async () => {
      let returnVal = true;
      spyOn(vm, 'revealHeldStatusElaboration').and.callFake(() => returnVal);
      let result = await vm.getCompletionDate();
      expect(vm.revealHeldStatusElaboration).toHaveBeenCalled();
      expect(result).toEqual('4th April, 2016');
      returnVal = false;
      result = await vm.getCompletionDate();
      expect(result).toEqual(null);
    });
  });
});
