import 'spec/spec-helper';
import 'angular-moment';
import SpecifyDetailsController from
  'app/asset/service-schedule/add-service/specify-details/specify-details.controller.js';

describe('Service Schedule, Specify Details Controller', () => {
  let vm;
  let $logMock;
  let stateParamsMock;
  let stateMock;
  let MastModalFactoryMock;
  let assetMock;
  let assetModelMock;
  let assignedProductsMock;
  let assignedServicesMock;
  let dateFormatMock;
  let serviceTypeMock;
  let serviceRelationshipTypeMock;
  let scheduledServiceStatusMock;
  let serviceCreditStatusMock;
  let serviceScheduleServiceMock;
  let serviceStatusMock;
  let productMock;
  let productTypeMock;
  let refServicesStatusesMock;
  let buildInjections;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildInjections = () => {
      inject((_$controller_, _moment_, _$rootScope_) => {
        stateMock = jasmine.createSpyObj('$state spy', ['go']);
        stateMock.go.and.callFake(() => true);

        serviceScheduleServiceMock = jasmine.createSpyObj(
          'serviceScheduleService spy', ['assignAssetService']);
        serviceScheduleServiceMock.assignAssetService.and.callFake(() => true);

        $logMock = { };

        MastModalFactoryMock = { id: 1 };
        stateParamsMock = { id: 1 };
        assetMock = { id: 1 };
        assetModelMock = { id: 1 };
        assignedProductsMock = { id: 1 };
        assignedServicesMock = { id: 1 };
        serviceTypeMock = { id: 1 };
        serviceRelationshipTypeMock = { id: 1 };
        scheduledServiceStatusMock = { id: 1 };
        serviceStatusMock = {
          toObject: () => ({
            notStarted: 1,
            partHeld: 2,
            held: 3,
            complete: 4
          })
        };
        serviceCreditStatusMock = { id: 1 };
        productMock = { id: 1 };
        productTypeMock = { id: 1 };
        refServicesStatusesMock = [
          { id: 1 },
          { id: 2 },
          { id: 3 },
          { id: 4 }
        ];

        vm = _$controller_(SpecifyDetailsController,
          {
            $log: $logMock,
            $state: stateMock,
            $stateParams: stateParamsMock,
            moment: _moment_,
            MastModalFactory: MastModalFactoryMock,
            asset: assetMock,
            assetModel: assetModelMock,
            assignedProducts: assignedProductsMock,
            assignedServices: assignedServicesMock,
            dateFormat: dateFormatMock,
            serviceType: serviceTypeMock,
            serviceRelationshipType: serviceRelationshipTypeMock,
            scheduledServiceStatus: scheduledServiceStatusMock,
            serviceCreditStatus: serviceCreditStatusMock,
            serviceScheduleService: serviceScheduleServiceMock,
            serviceStatus: serviceStatusMock,
            product: productMock,
            productType: productTypeMock,
            refServicesStatuses: refServicesStatusesMock
          },
          {
            _momentNow: _moment_()
          });
      });
    };
  });

  const methods = ['setup', 'asset', 'product', 'services', 'provisionalStatuses',
    'maxAssignedDate', 'isHarmonisationInvalid', 'isFieldInvalid', 'validForms',
    'enableDateRanges', 'save', 'back'];

  for (let i = 0; i < methods.length; i += 1) {
    it(`"${methods[i]}" method should exist`, async () => {
      expect(`${methods[i]}`).toBeDefined();
    });
  }

  it('getter "maxAssignedDate" adds 3 months', async () => {
    buildInjections();

    // Create our own '3 months hence' date, then ask the method, these two
    // dates should be equal if the method is working correctly;
    const adjustedDate = vm._momentNow.add(3, 'months')._d.toString();
    const assignedDate = (await vm.maxAssignedDate)._d.toString();
    expect(adjustedDate).toBe(assignedDate);
  });

  it('"enableDateRanges" method should return a boolean', async () => {
    buildInjections();
    const retValue = vm.enableDateRanges;
    expect(retValue).toMatch(/true|false/);
  });

  it('"save" method should call serviceScheduleService and $_state.go', async () => {
    buildInjections();
    await vm.save();
    expect(serviceScheduleServiceMock.assignAssetService).toHaveBeenCalled();
    expect(stateMock.go).toHaveBeenCalled();
  });

  it('"back" method should call $_state.go', async () => {
    buildInjections();
    await vm.back();
    expect(stateMock.go).toHaveBeenCalled();
  });
});
