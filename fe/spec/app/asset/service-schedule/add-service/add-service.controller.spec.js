import 'spec/spec-helper';
import 'angular-moment';

import AddServiceController from
  'app/asset/service-schedule/add-service/add-service.controller.js';

describe('Service Schedule, Add Service', () => {
  let vm;
  let $rootScope;
  let scope;
  let stateMock;
  let stateParamsMock;
  let assetMock;
  let assignedProductsMock;
  let assignedServicesMock;
  let serviceTypeMock;
  let serviceRelationshipTypeMock;
  let productMock;
  let productTypeMock;
  let productServiceCataloguesMock;
  let buildInjections;
  let buildServices;

  beforeEach(async () => {
    buildServices = () => {
      angular.mock.module('angularMoment');
    };

    buildInjections = () => {
      inject((_$rootScope_, _$controller_) => {
        $rootScope = _$rootScope_;
        scope = $rootScope.$new();

        stateMock = jasmine.createSpyObj('$state', ['go']);
        stateMock.go.and.callFake(() => true);

        stateParamsMock = {
          id: 1
        };

        assetMock = {
          id: 1
        };

        assignedProductsMock = {
          id: 1
        };

        assignedServicesMock = {
          id: 1
        };

        const serviceTypes = {
          alternate: 1,
          annual: 2,
          docking: 3,
          initial: 4,
          interim: 5,
          intermediate: 6,
          miscellaneous: 7,
          periodic: 8,
          renewal: 9,
          standAlone2InSpecialCycle: 10,
          additional: 11
        };
        serviceTypeMock = {
          toObject: () => serviceTypes
        };

        serviceRelationshipTypeMock = {
          id: 1
        };

        productServiceCataloguesMock = {
          id: 1
        };

        const serviceRelationshipTypes = {
          dependent: 1,
          counterpart: 2
        };
        serviceRelationshipTypeMock = {
          toObject: () => serviceRelationshipTypes
        };

        productMock = {
          id: 1
        };

        const productTypes = {
          classification: 1,
          mms: 2,
          statutory: 3
        };
        productTypeMock = {
          get: (field) => productTypes[field],
          toObject: () => productTypes
        };

        vm = _$controller_(AddServiceController,
          {
            $state: stateMock,
            $stateParams: stateParamsMock,
            asset: assetMock,
            assignedProducts: assignedProductsMock,
            assignedServices: assignedServicesMock,
            serviceType: serviceTypeMock,
            serviceRelationshipType: serviceRelationshipTypeMock,
            product: productMock,
            productType: productTypeMock,
            productServiceCatalogues: productServiceCataloguesMock
          });

        scope.$digest();
      });
    };

    buildServices();
    buildInjections();
  });

  const methods = ['isAssigned', 'isProductType', 'isSameSchedulingRegime',
    'getAvailableOptions', 'showOptions', 'toggleOption',
    'toggleCounterpart', 'hasSelectedCatalogues', 'next', 'back'];

  for (let i = 0; i < methods.length; i += 1) {
    /* eslint no-loop-func: 0 */
    it(`should have a "${methods[i]}" method`, () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  const getters = ['asset', 'assignedProducts', 'assignedServices',
    'assignedCatalogueIds', 'addedCatalogues', 'product', 'productId',
    'productTypeId', 'selectedCatalogueIds'];

  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  describe('on controller creation', () => {
    it('should get available options', async () => {
      spyOn(vm, 'isProductType').and.callThrough();
      const options = await vm.getAvailableOptions();
      expect(vm.isProductType).toHaveBeenCalled();
      expect(angular.isArray(options)).toBe(true);
    });
  });

  describe('CTA buttons', () => {
    it('"next" should go to specify details', async () => {
      await vm.next();
      expect(stateMock.go).toHaveBeenCalled();
      expect(stateMock.go).toHaveBeenCalledWith(
        'asset.serviceSchedule.addService.specifyDetails');
    });

    it('"back" should go to schedule service list', async () => {
      await vm.back();
      expect(stateMock.go).toHaveBeenCalled();
      expect(stateMock.go).toHaveBeenCalledWith('asset.serviceSchedule.list');
    });
  });
});
