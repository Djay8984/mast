import 'spec/spec-helper';
import 'angular-moment';
import services from 'app/services';

describe('Batch Action Service', () => {
  let Restangular;
  let actionableItemStatusMock;
  let assetNoteStatusMock;
  let batchActionService;
  let referenceDataService;
  let buildServices;
  let buildInjections;

  beforeEach(() => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        Restangular =
          jasmine.createSpyObj('Restangular', ['one', 'customPOST']);
        Restangular.one.and.returnValue(Restangular);
        Restangular.customPOST.and.returnValue({});
        $provide.value('Restangular', Restangular);

        referenceDataService =
          jasmine.createSpyObj('referenceDataService', ['get']);
        referenceDataService.get.and.returnValue({});
        $provide.value('referenceDataService', referenceDataService);

        const actionableItemStatus = {
          draft: 1,
          inactive: 2,
          open: 3,
          changeRecommended: 4,
          deleted: 5,
          deletedForCurrentJob: 6,
          cancelled: 7
        };
        actionableItemStatusMock = {
          toObject: () => actionableItemStatus
        };
        $provide.value('actionableItemStatus', actionableItemStatusMock);

        const assetNoteStatus = {
          draft: 8,
          inactive: 9,
          open: 10,
          changeRecommended: 11,
          deleted: 12,
          cancelled: 13
        };
        assetNoteStatusMock = {
          toObject: () => assetNoteStatus
        };
        $provide.value('assetNoteStatus', assetNoteStatusMock);
      });
    };

    buildInjections = () => {
      inject((_batchActionService_) => {
        batchActionService = _batchActionService_;
      });
    };

    buildServices();
    buildInjections();
  });

  it('Codicil status', async () => {
    const status = await batchActionService.codicilStatus('asset-note', { });
    expect(angular.isNumber(status)).toBe(true);
  });

  describe('Helper Functions', () => {
    it('"codicilTypes" should return an object', async () => {
      const result = await batchActionService.codicilTypes();
      expect(angular.isObject(result)).toBe(true);
    });

    it('"saveBatchActionCodicils" should return an object', async () => {
      spyOn(batchActionService, 'codicilStatus').and.callThrough();
      const result =
        await batchActionService.saveBatchActionCodicils(
          [1, 2], { }, 'asset-note', null);
      expect(batchActionService.codicilStatus)
        .toHaveBeenCalledWith('asset-note', jasmine.any(Object));
      expect(Restangular.one).toHaveBeenCalled();
      expect(Restangular.customPOST).toHaveBeenCalled();
      expect(angular.isObject(result)).toBe(true);
    });
  });
});
