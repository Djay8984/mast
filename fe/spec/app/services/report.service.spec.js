import 'spec/spec-helper';
import 'angular-moment';
import reports from 'app/services/report';

describe('Report Decision Service', () => {
  let reportDecisionService;

  let $rootScope;

  let qMock;
  let dateFormat;
  let node;
  let Restangular;
  let cocStatus;
  let defectStatus;
  let jobStatus;
  let reportType;
  let serviceCreditStatus;
  let surveyorRole;

  let codicilDecisionService;
  let creditingDecisionService;
  let defectDecisionService;
  let employeeDecisionService;
  let harmonisationDateService;
  let jobDecisionService;
  let surveyDecisionService;
  let offlinular;
  let taskDecisionService;

  let buildServices;
  let buildInjections;

  beforeEach(() => {
    buildServices = () => {
      angular.mock.module('angularMoment');
      angular.mock.module(reports, ($provide) => {
        Restangular = jasmine.createSpyObj('Restangular',
          ['one', 'all', 'get', 'getList', 'customPOST', 'customPUT']);
        Restangular.one.and.returnValue(Restangular);
        Restangular.all.and.returnValue(Restangular);
        Restangular.get.and.returnValue(Restangular);
        Restangular.getList.and.returnValue([]);
        Restangular.customPOST.and.returnValue({});
        Restangular.customPUT.and.returnValue({});
        $provide.value('Restangular', Restangular);

        offlinular = jasmine.createSpyObj('offlinular', ['one']);
        offlinular.one.and.returnValue({});
        $provide.value('offlinular', offlinular);

        codicilDecisionService =
          jasmine.createSpyObj('codicilDecisionService',
            ['getActionableItem', 'getAssetNote', 'getCoC']);
        codicilDecisionService.getAssetNote.and.returnValue({});
        codicilDecisionService.getCoC.and.returnValue({});
        codicilDecisionService.getActionableItem.and.returnValue({});
        $provide.value('codicilDecisionService', codicilDecisionService);

        creditingDecisionService =
          jasmine.createSpyObj('creditingDecisionService',
            ['get', 'getServiceTypeTaskCompletions']);
        creditingDecisionService.get.and.returnValue({});
        creditingDecisionService.getServiceTypeTaskCompletions.and.returnValue({});
        $provide.value('creditingDecisionService', creditingDecisionService);

        defectDecisionService =
          jasmine.createSpyObj('defectDecisionService', ['getDefectsForJob']);
        defectDecisionService.getDefectsForJob.and.returnValue({});
        $provide.value('defectDecisionService', defectDecisionService);

        employeeDecisionService =
          jasmine.createSpyObj('employeeDecisionService', ['getCurrentUser']);
        employeeDecisionService.getCurrentUser.and.returnValue(
          Object.create({}, { fullName: { value: 'Joe Bloggs' } })
        );
        $provide.value('employeeDecisionService', employeeDecisionService);

        harmonisationDateService =
          jasmine.createSpyObj('harmonisationDateService',
            ['getHarmonisationDate']);
        harmonisationDateService.getHarmonisationDate.and.returnValue({});
        $provide.value('harmonisationDateService', harmonisationDateService);

        jobDecisionService =
          jasmine.createSpyObj('jobDecisionService',
            ['get', 'updateJob', 'getSurveysForJob']);
        jobDecisionService.get.and.returnValue({});
        jobDecisionService.updateJob.and.returnValue({});
        jobDecisionService.getSurveysForJob.and.returnValue({});
        $provide.value('jobDecisionService', jobDecisionService);

        surveyDecisionService =
          jasmine.createSpyObj('surveyDecisionService', ['getAllSurveys']);
        surveyDecisionService.getAllSurveys.and.returnValue({});
        $provide.value('surveyDecisionService', surveyDecisionService);

        taskDecisionService =
          jasmine.createSpyObj('taskDecisionService', ['query']);
        taskDecisionService.query.and.returnValue({});
        $provide.value('taskDecisionService', taskDecisionService);

        dateFormat = 'DD MMM YYYY';
        $provide.value('dateFormat', dateFormat);

        qMock = { };
        $provide.value('qMock', qMock);

        node = { };
        $provide.value('node', node);

        const cocStatuses = {
          open: 14,
          deleted: 15,
          cancelled: 16
        };
        cocStatus = {
          get: (field) => cocStatuses[field]
        };
        $provide.value('cocStatus', cocStatus);

        const defectStatuses = {
          open: 1,
          closed: 2,
          cancelled: 3
        };
        defectStatus = {
          get: (field) => defectStatuses[field]
        };
        $provide.value('defectStatus', defectStatus);

        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4,
          awaitingTechnicalReviewerAssignment: 5,
          underTechnicalReview: 6,
          awaitingEndorserAssignment: 7,
          underEndorsement: 8,
          cancelled: 9,
          aborted: 10,
          closed: 11
        };
        jobStatus = {
          get: (field) => jobStatuses[field]
        };
        $provide.value('jobStatus', jobStatus);

        const reportTypes = {
          fsr: 1,
          far: 2,
          dsr: 3,
          dar: 4
        };
        reportType = {
          get: (field) => reportTypes[field]
        };
        $provide.value('reportType', reportType);

        const serviceCreditStatuses = {
          notStarted: 1,
          partHeld: 2,
          postponed: 3,
          complete: 4,
          finished: 5
        };
        serviceCreditStatus = {
          get: (field) => serviceCreditStatuses[field],
          toObject: () => serviceCreditStatuses
        };
        $provide.value('serviceCreditStatus', serviceCreditStatus);

        const surveyorRoles = {
          businessSupport: 1,
          surveyor: 2,
          management: 3,
          salesCRM: 4,
          specialistLeadership: 5,
          authorisingSurveyor: 6,
          eicCaseSurveyor: 7,
          eicAdmin: 8,
          sdo: 9,
          eicManager: 10,
          leadSurveyor: 11,
          sdoCoordinator: 12
        };
        surveyorRole = {
          get: (field) => surveyorRoles[field]
        };
        $provide.value('surveyorRole', surveyorRole);
      });
    };
    buildInjections = () => {
      inject((_reportDecisionService_, _$rootScope_, _moment_) => {
        reportDecisionService = _reportDecisionService_;
        $rootScope = _$rootScope_;
      });
    };

    buildServices();
    buildInjections();
  });

  /* eslint no-loop-func: 0 */
  const methods = ['create', 'update', 'get', 'reduceCodicils', 'filterReport',
    'generateTasks', 'prepareTasks', 'stripTasks', 'stripModel', 'createReport',
    'rejectReport', 'createBasicReport', 'checkDSRBeforeSubmission',
    'checkBeforeSubmission', 'hasOpenCoCWithDefectId', 'getReportsForJob',
    'getFARSubmissionState', 'getVersions', '_core'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  describe('Creating a Report', () => {
    xit('should create a basic report object', async () => {
      const reportParams = [
        1, 'dar', '2015-11-25 00:00:00', 'false', 'false', { }
      ];
      await reportDecisionService.createReport(...reportParams);
      expect(reportDecisionService.createBasicReport).toHaveBeenCalled();
      expect(codicilDecisionService.getActionableItem).toHaveBeenCalled();
      expect(codicilDecisionService.getAssetNote).toHaveBeenCalled();
      expect(codicilDecisionService.getCoC).toHaveBeenCalled();
      expect(jobDecisionService.getSurveysForJob).toHaveBeenCalledWith(1);
    });
  });

  describe('Checking for CoCs with defect IDs', () => {
    it('returns false for a CoC with no defect', () => {
      const defectIds = [1, 2, 3, 4, 5];
      const openCocs = [{ }];
      expect(reportDecisionService.hasOpenCoCWithDefectId(openCocs, defectIds))
        .toBeFalsy();
    });

    it('returns false for a CoC whose defect ID is not in the list', () => {
      const defectIds = [1, 2, 3, 4, 5];
      const openCocs = [{ defect: { id: 6 } }];
      expect(reportDecisionService.hasOpenCoCWithDefectId(openCocs, defectIds))
        .toBeFalsy();
    });

    it('Returns true for a CoC whose defect ID is in the list', () => {
      const defectIds = [1, 2, 3, 4, 5];
      const openCocs = [{ defect: { id: 3 } }, { }];
      expect(reportDecisionService.hasOpenCoCWithDefectId(openCocs, defectIds))
        .toBeTruthy();
    });
  });

  describe('Helper Functions', () => {
    it('"_core" should return an object', async () => {
      expect(angular.isObject(await reportDecisionService._core(1, 1)))
        .toBe(true);
    });

    it('"create" should return an object', async () => {
      expect(angular.isObject(await reportDecisionService.create(1, {})))
        .toBe(true);
    });

    it('"update" should return an object', async () => {
      expect(angular.isObject(await reportDecisionService.update(1, 1, {})))
        .toBe(true);
    });

    it('"get" should return an object', async () => {
      expect(angular.isObject(await reportDecisionService.get(1, 1)))
        .toBe(true);
    });

    it('"reduceCodicils" should return an array of objects', async () => {
      const codicils = [
        {
          actionTakenDate: 123123123145,
          creditedBy: {
            name: 'Joe Bloggs'
          },
          id: 1
        },
        {
          creditedBy: {
            name: 'John Smith'
          },
          id: 2
        }
      ];
      expect(angular.isArray(
        await reportDecisionService.reduceCodicils(codicils))).toBe(true);
    });

    it('"generateTasks" should return an object', async () => {
      const result =
        await reportDecisionService.generateTasks([{ id: 1 }, { id: 2 }]);
      expect(angular.isObject(result)).toBe(true);
    });

    it('"prepareTasks" should return an array', async () => {
      const result =
        await reportDecisionService.prepareTasks([{ id: 1 }, { id: 2 }]);
      expect(jobDecisionService.get).toHaveBeenCalled();
      expect(creditingDecisionService.get).toHaveBeenCalled();
      expect(creditingDecisionService.getServiceTypeTaskCompletions)
        .toHaveBeenCalled();
      expect(angular.isArray(result)).toBe(true);
    });

    it('"stripTasks" should return an array', async () => {
      const result =
        await reportDecisionService.stripTasks([{ id: 1 }, { id: 2 }]);
      expect(angular.isArray(result)).toBe(true);
      expect(_.size(result)).toEqual(2);
    });

    it('"stripModel" should return an array', async () => {
      const result =
        await reportDecisionService.stripModel([{ id: 1 }, { id: 2 }]);
      expect(angular.isArray(result)).toBe(true);
      expect(_.size(result)).toEqual(2);
    });

    it('"createReport" should return an object', async () => {
      const result = reportDecisionService.createReport();
      expect(angular.isObject(result)).toBe(true);
    });

    it('"rejectReport" should return a function if type is dsr', async () => {
      spyOn(reportDecisionService, 'get').and.callThrough();
      reportDecisionService.rejectReport(1, 1, 'dsr', 'foo');
      expect(jobDecisionService.get).toHaveBeenCalledWith(1);
    });

    it('"createBasicReport"', async () => {
      expect(angular.isObject(
        await reportDecisionService.createBasicReport(1, 'dsr'))).toBe(true);
    });

    it('"checkDSRBeforeSubmission"', async () => {
      const input = {
        content: {
          coc: {}
        }
      };
      spyOn(reportDecisionService, 'generateTasks').and.callThrough();
      const result = reportDecisionService.checkDSRBeforeSubmission(1, input);
      expect(angular.isFunction(result.then)).toBe(true);
    });

    it('"checkBeforeSubmission"', async () => {
      const input = {
        content: {
          coc: { }
        }
      };
      const result =
        await reportDecisionService.checkBeforeSubmission(1, input);
      expect(defectDecisionService.getDefectsForJob).toHaveBeenCalledWith(1);
      expect(codicilDecisionService.getCoC).toHaveBeenCalled();
      expect(angular.isNumber(result)).toBe(true);
    });

    it('"hasOpenCoCWithDefectId"', async () => {
      const result = await reportDecisionService
        .hasOpenCoCWithDefectId([{ id: 1 }, { id: 2 }], [1, 2, 3]);
      expect(result).toBeFalsy();
    });

    xit('"getReportsForJob"', async () => {
      spyOn(reportDecisionService, 'populateAttributes').and.callThrough();
      expect(angular.isArray(
        await reportDecisionService.getReportsForJob(1))).toBe(true);
      expect(reportDecisionService.populateAttributes).toHaveBeenCalled();
    });

    it('"getFARSubmissionState" should return a boolean', async () => {
      spyOn(reportDecisionService, 'getVersions').and.callThrough();
      const result = await reportDecisionService.getFARSubmissionState(1);
      expect(result).toMatch(/true|false/);
    });

    xit('"getVersions" should return an array', async () => {
      spyOn(reportDecisionService, 'populateAttributes').and.callThrough();
      expect(angular.isArray(
        await reportDecisionService.getVersions(1, 1))).toBe(true);
      expect(reportDecisionService.populateAttributes).toHaveBeenCalled();
    });
  });
});
