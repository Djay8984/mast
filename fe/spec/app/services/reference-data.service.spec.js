import 'spec/spec-helper';
import * as _ from 'lodash';

import services from 'app/services';
import referenceDataFixtureRaw from 'spec/fixtures/reference-data.json';

describe('referenceData.service', () => {
  const referenceDataFixture = angular.fromJson(referenceDataFixtureRaw);
  let referenceDataService;
  let dbSpy;
  let RestangularSpy;

  beforeEach(() => {
    angular.mock.module(services, ($provide) => {
      RestangularSpy = jasmine.createSpyObj('Restangular', ['one', 'get']);
      $provide.value('Restangular', RestangularSpy);

      dbSpy = jasmine.createSpyObj('QDatastore',
        ['findOne', 'insert', 'remove']);
      $provide.value('QDatastore', dbSpy);
    });

    inject((_referenceDataService_, _Restangular_, _QDatastore_) => {
      referenceDataService = _referenceDataService_;
      RestangularSpy = _Restangular_;
      dbSpy = _QDatastore_;
      referenceDataService._db = dbSpy;
    });

    RestangularSpy.one.and.returnValue(RestangularSpy);
  });

  describe('get api data', () => {
    beforeEach(() =>
      RestangularSpy.get.and.returnValue(
        Promise.resolve(referenceDataFixture)));

    it('should retrieve data', async () =>
      expect(await referenceDataService._getAPIData()).toEqual(
        referenceDataFixture.referenceData));
  });

  describe('get api version', () => {
    beforeEach(() =>
      RestangularSpy.get.and.returnValue(
        Promise.resolve({ version: 1 })));

    it('should match version with api', async () =>
      expect(await referenceDataService._getAPIVersion()).toEqual(1));
  });

  describe('get db data', () => {
    beforeEach(() =>
      dbSpy.findOne.and.returnValue(
        Promise.resolve({ data: referenceDataFixture.referenceData })));

    it('should retrieve data', async () =>
      expect(await referenceDataService._getDBData()).toEqual(
        referenceDataFixture.referenceData));
  });

  describe('get db version', () => {
    beforeEach(() =>
      dbSpy.findOne.and.returnValue(
        Promise.resolve({ version: 1 })));

    it('should match version with db', async () =>
      expect(await referenceDataService._getDBVersion()).toEqual(1));
  });

  describe('update db', () => {
    beforeEach(async () =>
      await referenceDataService._updateDB(
        1, referenceDataFixture.referenceData));

    it('should remove old data & version', () =>
      expect(dbSpy.remove).toHaveBeenCalled());

    it('should insert new data & version', () =>
      expect(dbSpy.insert).toHaveBeenCalled());
  });

  describe('prepare service', () => {
    describe('when offline', async () => {
      beforeEach(async () => {
        spyOn(referenceDataService, 'build');
        spyOn(referenceDataService, '_updateDB');
        spyOn(referenceDataService, '_getAPIData');
        spyOn(referenceDataService, '_getAPIVersion');
        spyOn(referenceDataService, '_getDBData');
        spyOn(referenceDataService, '_getDBVersion');
        await referenceDataService.prepareDatabase(false);
      });

      it('shouldn\'t check api or db versions', () => {
        expect(referenceDataService._getAPIVersion).not.toHaveBeenCalled();
        expect(referenceDataService._getDBVersion).not.toHaveBeenCalled();
      });

      it('shouldn\'t pull down data from api', () =>
        expect(referenceDataService._getAPIData).not.toHaveBeenCalled());

      it('shouldn\'t update db', () =>
        expect(referenceDataService._getAPIData).not.toHaveBeenCalled());

      it('should build data', () =>
        expect(referenceDataService.build).toHaveBeenCalled());
    });

    describe('when online', () => {
      describe('no new version available', async () => {
        beforeEach(async () => {
          spyOn(referenceDataService, 'build');
          spyOn(referenceDataService, '_updateDB');
          spyOn(referenceDataService, '_getAPIData');
          spyOn(referenceDataService, '_getAPIVersion').and.returnValue(
            Promise.resolve(0));
          spyOn(referenceDataService, '_getDBData');
          spyOn(referenceDataService, '_getDBVersion').and.returnValue(
            Promise.resolve(1));
          await referenceDataService.prepareDatabase(true);
        });

        it('should check api & db versions', () => {
          expect(referenceDataService._getAPIVersion).toHaveBeenCalled();
          expect(referenceDataService._getDBVersion).toHaveBeenCalled();
        });

        it('shouldn\'t pull down data from api', () =>
          expect(referenceDataService._getAPIData).not.toHaveBeenCalled());

        it('shouldn\'t update db', () =>
          expect(referenceDataService._updateDB).not.toHaveBeenCalled());

        it('should build data', () =>
          expect(referenceDataService.build).toHaveBeenCalled());
      });

      describe('new version available', async () => {
        beforeEach(async () => {
          spyOn(referenceDataService, 'build');
          spyOn(referenceDataService, '_updateDB');
          spyOn(referenceDataService, '_getAPIData').and.returnValue(
            Promise.resolve(referenceDataFixture.referenceData));
          spyOn(referenceDataService, '_getAPIVersion').and.returnValue(
            Promise.resolve(1));
          spyOn(referenceDataService, '_getDBData');
          spyOn(referenceDataService, '_getDBVersion').and.returnValue(
            Promise.resolve(0));
          await referenceDataService.prepareDatabase(true);
        });

        it('should check api & db versions', () => {
          expect(referenceDataService._getAPIVersion).toHaveBeenCalled();
          expect(referenceDataService._getDBVersion).toHaveBeenCalled();
        });

        it('should pull down data from api', () =>
          expect(referenceDataService._getAPIData).toHaveBeenCalled());

        it('should update db', () =>
          expect(referenceDataService._updateDB).toHaveBeenCalled());

        it('should build data', () =>
          expect(referenceDataService.build).toHaveBeenCalled());
      });
    });
  });

  describe('should build data', () => {
    beforeEach(async () => {
      spyOn(referenceDataService, '_getDBData').and.returnValue(
        Promise.resolve(referenceDataService.referenceData));
      await referenceDataService.build();
    });

    it('should retrieve db data', () =>
      expect(referenceDataService._getDBData).toHaveBeenCalled());

    it('should define data properties', () => {
      expect(referenceDataService.input).toBeDefined();
      expect(referenceDataService.view).toBeDefined();
    });
  });

  describe('should extract data', () => {
    const data = referenceDataFixture.referenceData;

    beforeEach(async () => {
      spyOn(referenceDataService, 'build').and.returnValue(
        Promise.resolve(true));

      // mocking building
      // doing this way instead of via a call because it's designed to wait
      // on multiple requests & is hard to work with in a test environment
      // because of that
      referenceDataService.view = {};
      referenceDataService.input = {};

      // creates getters for unfiltered view categories
      _.forEach(data, (categoryValue, categoryKey) =>
        Reflect.defineProperty(referenceDataService.view, categoryKey, {
          get: () => categoryValue
        }));

      // creates getters for filtered input categories
      _.forEach(data, (categoryValue, categoryKey) =>
        Reflect.defineProperty(referenceDataService.input, categoryKey, {
          get: () => _.mapValues(categoryValue,
            (tableValue) => _.reject(tableValue, 'deleted'))
        }));
    });

    describe('all data', async () => {
      it('should extract all data/subcategories from category', async () => {
        expect(await referenceDataService.get(
            ['categoryOne'])).toEqual(
          _.pick(data, 'categoryOne'));
        expect(await referenceDataService.get(
            'categoryOne')).toEqual(
          data.categoryOne);
      });

      it('should extract all data from multiple categories', async () => {
        expect(await referenceDataService.get(
            ['categoryOne', 'categoryTwo'])).toEqual(
          data);
        expect(await referenceDataService.get(
            ['categoryOne.subCategoryOne', 'categoryTwo'])).toEqual({
              categoryOne: {
                subCategoryOne: data.categoryOne.subCategoryOne
              },
              categoryTwo: data.categoryTwo
            });
      });

      it('should extract all data from subcategory', async () => {
        expect(await referenceDataService.get(
            ['categoryOne.subCategoryOne'])).toEqual(
          _.set({}, 'categoryOne.subCategoryOne',
            data.categoryOne.subCategoryOne));
        expect(await referenceDataService.get(
            'categoryOne.subCategoryOne')).toEqual(
          data.categoryOne.subCategoryOne);
      });
    });

    describe('non-deleted data', () => {
      it('should extract non-deleted data from category', async () => {
        expect(await referenceDataService.get(
            'categoryOne', true)).toEqual(
          _.mapValues(data.categoryOne, (subCategory) =>
            _.reject(subCategory, 'deleted')));
        expect(await referenceDataService.get(
            'categoryTwo', true)).toEqual(
          _.mapValues(data.categoryTwo, (subCategory) =>
            _.reject(subCategory, 'deleted')));
      });

      it('should extract non-deleted data from multiple categories', async () => {
        expect(await referenceDataService.get(
            ['categoryOne', 'categoryTwo'], true)).toEqual(
          _.mapValues(data, (category) =>
            _.mapValues(category, (subCategory) =>
              _.reject(subCategory, 'deleted'))));
      });

      it('should extract non-deleted data from subcategory', async () => {
        expect(await referenceDataService.get(
            'categoryOne.subCategoryOne', true)).toEqual(
          _.reject(data.categoryOne.subCategoryOne, 'deleted'));
        expect(await referenceDataService.get(
            'categoryOne.subCategoryTwo', true)).toEqual(
          _.reject(data.categoryOne.subCategoryTwo, 'deleted'));
        expect(await referenceDataService.get(
            'categoryTwo.subCategoryOne', true)).toEqual(
          _.reject(data.categoryTwo.subCategoryOne, 'deleted'));
      });
    });
  });
});
