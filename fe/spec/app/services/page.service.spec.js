import services from 'app/services';

describe('Page Service', () => {
  let $rootScope;
  let $state, $stateParams, assetDecisionService, pageService;
  let apiMock, checkedInAsset, checkedOutAsset;

  beforeEach(() => {
    apiMock = [{ name: 'Test', id: 5 }, { name: 'Other test', id: 8 }, { id: 1 }];
    checkedInAsset = { id: 7, name: 'Some ship' };
    checkedOutAsset = { id: 8, name: 'Some other ship' };

    angular.mock.module(services, ($provide) => {
      const stateMock = jasmine.createSpyObj('stateMock', ['go']);
      stateMock.go.and.callFake((name, data) => {
        stateMock.data = data;
        stateMock.name = name;
      });
      stateMock.current = { whenCheckedIn: 'some.state' };

      $provide.value('$state', stateMock);
      $provide.value('$stateParams', { assetId: 1 });
      $provide.value('assetDecisionService', {
        checkedOut: () => apiMock,
        checkOut: () => true,
        checkIn: () => true
      });

      $provide.value('pageService', {});
    });

    inject((_$rootScope_, _$state_, _$stateParams_, _pageService_, _assetDecisionService_) => {
      $state = _$state_;
      $stateParams = _$stateParams_;
      $rootScope = _$rootScope_;
      pageService = _pageService_;

      assetDecisionService = _assetDecisionService_;
    });
  });

  /* eslint no-loop-func: 0 */
  const methods = ['getCheckedOut', 'isCheckedOut', 'checkOut', 'checkIn',
    'setHeader', 'resetData', 'checkPermissions'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  xit('Should use same object for checkedOut list', async () => {
    expect(pageService.checkedOut.length).toBe(0);
    await pageService.getCheckedOut();
    expect(pageService.checkedOut.length).toBe(apiMock.length);
  });

  xit('Should find is asset is checked out by asset', async () => {
    expect(Boolean(pageService.isCheckedOut(checkedOutAsset))).toBe(false);
    expect(Boolean(pageService.isCheckedOut(checkedInAsset))).toBe(false);
    await pageService.getCheckedOut();
    expect(Boolean(pageService.isCheckedOut(checkedOutAsset))).toBe(true);
    expect(Boolean(pageService.isCheckedOut(checkedInAsset))).toBe(false);
  });

  xit('Should find is asset is checked out by $stateParams', async () => {
    expect(Boolean(pageService.isCheckedOut($stateParams))).toBe(false);
    expect(Boolean(pageService.isCheckedOut({ assetId: 2 }))).toBe(false);
    await pageService.getCheckedOut();
    expect(Boolean(pageService.isCheckedOut($stateParams))).toBe(true);
    expect(Boolean(pageService.isCheckedOut({ assetId: 2 }))).toBe(false);
  });

  xit('Should do check out if asset is not checked in', async () => {
    spyOn(assetDecisionService, 'checkOut');
    spyOn(pageService, 'isCheckedOut').and.callThrough();
    await pageService.checkOut(checkedOutAsset);
    expect(pageService.isCheckedOut).toHaveBeenCalled();
    expect(assetDecisionService.checkOut).toHaveBeenCalledTimes(1);

    await pageService.checkOut(checkedInAsset);
    expect(pageService.isCheckedOut).toHaveBeenCalled();
    expect(assetDecisionService.checkOut).toHaveBeenCalledTimes(2);
  });

  xit('Should do check in if asset is not checked out', async () => {
    spyOn(assetDecisionService, 'checkIn');
    await pageService.getCheckedOut();
    await pageService.checkIn(checkedInAsset);
    expect(assetDecisionService.checkIn).not.toHaveBeenCalled();

    await pageService.checkIn(checkedOutAsset);
    expect(assetDecisionService.checkIn).toHaveBeenCalled();
  });

  xit('Should update checkedOut list on check in/out', async () => {
    spyOn(assetDecisionService, 'checkedOut').and.callThrough();
    expect(pageService.checkedOut.length).toBe(0);

    await pageService.checkOut(checkedInAsset);
    expect(assetDecisionService.checkedOut).toHaveBeenCalledTimes(1);
    expect(pageService.checkedOut.length).toBe(apiMock.length);

    await pageService.checkIn(checkedOutAsset);
    expect(assetDecisionService.checkedOut).toHaveBeenCalledTimes(2);
  });

  xit('Should NOT redirect if asset is checked OUT and state have no permission', async () => {
    spyOn(pageService, 'checkPermissions').and.callThrough();
    await pageService.getCheckedOut();
    expect(pageService.checkPermissions).toHaveBeenCalled();
    expect($state.go).not.toHaveBeenCalled();
  });

  xit('Should REDIRECT if asset is NOT checked OUT and state have no permission', async () => {
    spyOn(pageService, 'checkPermissions').and.callThrough();

    // not checked out id
    $stateParams.assetId = 99;
    await pageService.getCheckedOut();
    expect(pageService.checkPermissions).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalled();
  });
});
