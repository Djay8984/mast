import 'spec/spec-helper';

import services from 'app/services';

import dictionaryZipFixtureRaw from 'spec/fixtures/dictionary.zip';

// import dictionaryDicFixtureRaw from 'spec/fixtures/en_GB.dic';
// import dictionaryAffFixtureRaw from 'spec/fixtures/en_GB.aff';

/*
  TODO:
    * figure out binary file parsing & mocking into restangular
    * mock restangular interception for arraybuffer hack because of lib bug
    * mock node libs like buffer, fs & path, & use them for verifying tests
    * mock jszip
    * mock remote to properly provide spellchecker & context menu editor
    * mock html for application of ~directive~
 */
xdescribe('spellCheck.service', () => {
  let Restangular, spellCheckService;

  beforeEach(() => {
    angular.mock.module(services, ($provide) => {
      Restangular = jasmine.createSpyObj('Restangular',
        ['withHttpConfig', 'one', 'get']);
      $provide.value('Restangular', Restangular);
    });

    inject((_spellCheckService_, _Restangular_) => {
      spellCheckService = _spellCheckService_;
      Restangular = _Restangular_;
    });
  });

  describe('prep while offline', () => {
    it('shouldn\'t retrieve zipped dictionary from api', async () => {
      await spellCheckService.prepare(false);

      expect(Restangular.one).not.toHaveBeenCalled();
      expect(Restangular.get).not.toHaveBeenCalled();

      expect(spellCheckService.applySpellChecking).toHaveBeenCalled();
    });
  });

  describe('prep while online', () => {
    it('should retrieve zipped dictionary from api', async () => {
      Restangular.withHttpConfig.and.returnValue(Restangular);
      Restangular.one.and.returnValue(Restangular);
      Restangular.get.and.returnValue(Promise.resolve(dictionaryZipFixtureRaw));

      await spellCheckService.prepare(true);

      expect(spellCheckService._getAPIData).toHaveBeenCalled();
      expect(Restangular.one).toHaveBeenCalled();
      expect(Restangular.get).toHaveBeenCalled();

      expect(spellCheckService._parseBufferToFiles).toHaveBeenCalled();

      expect(spellCheckService.applySpellChecking).toHaveBeenCalled();
    });
  });
});
