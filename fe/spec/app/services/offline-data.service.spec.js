import * as _ from 'lodash';
import 'angular-moment';

import 'spec/spec-helper';

import services from 'app/services';
import jobBundleFixtureRaw from 'spec/fixtures/job-bundle.json';


describe('offlineData.service', () => {
  const jobBundleFixture = angular.fromJson(jobBundleFixtureRaw);
  let dbSpy = null;
  let moment = null;
  let offlineDataService = null;

  beforeEach(() => {
    angular.mock.module('angularMoment');
    angular.mock.module(services, ($provide) => {
      dbSpy = jasmine.createSpyObj('QDatastore',
        ['delete', 'find', 'findOne', 'insert', 'remove', 'update']);
      $provide.value('QDatastore', dbSpy);
      $provide.value('moment', moment);
    });

    inject((_offlineDataService_, _QDatastore_, _moment_) => {
      offlineDataService = _offlineDataService_;
      dbSpy = _QDatastore_;
      offlineDataService._db = dbSpy;
      moment = _moment_;
    });
  });

  describe('insert entity', () => {
    beforeEach(async () => {
      dbSpy.findOne.and.returnValue(
        Promise.resolve({ }));
      await offlineDataService._insertEntity(
        'coc', 1, _.find(jobBundleFixture.coC, { id: 1 }));
    });

    it('should insert one entity', () =>
      expect(dbSpy.insert).toHaveBeenCalledWith(
        { type: 'coc', id: 1, data: _.find(jobBundleFixture.coC, { id: 1 }) }));
  });

  describe('insert entities', () => {
    beforeEach(async () => {
      dbSpy.findOne.and.returnValue(
        Promise.resolve({ }));
      await offlineDataService._insertEntities([
          { type: 'coc', id: 1, data: _.find(jobBundleFixture.coC, { id: 1 }) },
          { type: 'coc', id: 2, data: _.find(jobBundleFixture.coC, { id: 2 }) }
      ]);
    });

    it('should insert multiple entities', () =>
      expect(dbSpy.insert).toHaveBeenCalledWith([
          { type: 'coc', id: 1, data: _.find(jobBundleFixture.coC, { id: 1 }) },
          { type: 'coc', id: 2, data: _.find(jobBundleFixture.coC, { id: 2 }) }
      ]));
  });

  describe('update entity', () => {
    beforeEach(async () => {
      dbSpy.update.and.returnValue(
        Promise.resolve({ }));
      await offlineDataService._updateEntity(
        'coc', 1, _.find(jobBundleFixture.coC, { id: 1 }));
    });

    it('should update one entity', () =>
      expect(dbSpy.update).toHaveBeenCalledWith(
        { type: 'coc', id: 1 },
        { type: 'coc', id: 1, data: _.find(jobBundleFixture.coC, { id: 1 }) }));
  });

  describe('delete entity', () => {
    beforeEach(() =>
      dbSpy.remove.and.returnValue(
        Promise.resolve({ })));

    it('should delete one entity', async () => {
      await offlineDataService._deleteEntity('coc', 1);
      expect(dbSpy.remove).toHaveBeenCalledWith(
        { type: 'coc', id: 1 });
    });

    it('should delete multiple entities', async () => {
      await offlineDataService._deleteEntity('coc');
      expect(dbSpy.remove).toHaveBeenCalledWith(
        { type: 'coc' },
        { multi: true });
    });
  });

  describe('get entity', () => {
    beforeEach(() => {
      dbSpy.findOne.and.returnValue(
        Promise.resolve(
          { data: _.find(jobBundleFixture.coC, { id: 1 }) }));
      dbSpy.find.and.returnValue(
        Promise.resolve(
          _.map(jobBundleFixture.coC, (coc) => ({ data: coc }))));
    });

    it('should retrieve one entity', async () =>
      expect(await offlineDataService._getEntity('coc', 1)).toEqual(
        jobBundleFixture.coC[0]));

    it('should retrieve multiple entities', async () =>
      expect(await offlineDataService._getEntity('coc')).toEqual(
        jobBundleFixture.coC));
  });

  describe('url to object key', () => {
    it('should match plural', () =>
      expect(offlineDataService._urlToObjectKey(
          { tests: true }, 'test')).toEqual(
        'tests'));

    it('should match full string', () =>
      expect(offlineDataService._urlToObjectKey(
          { testAndMore: true }, 'test')).toBeUndefined());

    it('should match ignoring root prefix', () =>
       expect(offlineDataService._urlToObjectKey(
          { test: true }, 'rootTest')).toEqual(
          'test'));

    it('should match kebab to camel case', () =>
       expect(offlineDataService._urlToObjectKey(
          { testCamel: true }, 'test-camel')).toEqual(
        'testCamel'));

    it('should match ignoring case', () =>
       expect(offlineDataService._urlToObjectKey(
          { test: true }, 'TEST')).toEqual(
        'test'));
  });

  describe('filter', () => {
    describe('handle empty inputs', () => {
      it('should return all data with no filter', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { })).toEqual(
          jobBundleFixture.defect);
        expect(offlineDataService._filter(
            jobBundleFixture.defect)).toEqual(
          jobBundleFixture.defect);
      });

      it('should return no data with no data', () => {
        expect(offlineDataService._filter([ ], { })).toEqual([ ]);
        expect(offlineDataService._filter()).toEqual([ ]);
      });
    });

    describe('by id list', () => {
      it('should filter for multiple ids', () =>
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { idList: [1, 2] })).toEqual(
          jobBundleFixture.defect));

      it('should filter for single id ', () =>
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { idList: [1] })).toEqual(
          _.filter(jobBundleFixture.defect, { id: 1 })));

      it('should filter for unfound id', () =>
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { idList: [-1] })).toEqual(
          _.filter(jobBundleFixture.defect, { id: -1 })));
    });

    describe('by search string', () => {
      it('should filter for full string', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { search: 'one' })).toEqual(
          _.filter(jobBundleFixture.defect, { text: 'one' }));
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { searchString: 'one' })).toEqual(
          _.filter(jobBundleFixture.defect, { text: 'one' }));
      });

      it('should filter for unfound string', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { search: 'whatever' })).toEqual(
          _.filter(jobBundleFixture.defect, { text: 'whatever' }));
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { searchString: 'whatever' })).toEqual(
          _.filter(jobBundleFixture.defect, { text: 'whatever' }));
      });
    });

    describe('dynamically matched list & id suffix filters', () => {
      it('should filter for multiple ids', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingList: [1, 2] })).toEqual(
          jobBundleFixture.defect);
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingId: [1, 2] })).toEqual(
          jobBundleFixture.defect);
      });

      it('should filter for single id', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingList: [1] })).toEqual(
          _.filter(jobBundleFixture.defect, { thing: { id: 1 } }));
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingId: [1] })).toEqual(
          _.filter(jobBundleFixture.defect, { thing: { id: 1 } }));
      });

      it('should filter for unfound id', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingList: [-1] })).toEqual(
          _.filter(jobBundleFixture.defect, { thing: { id: -1 } }));
        expect(offlineDataService._filter(
            jobBundleFixture.defect, { thingId: [-1] })).toEqual(
          _.filter(jobBundleFixture.defect, { thing: { id: -1 } }));
      });
    });

    describe('filter conjunction', () => {
      it('should filter for single id with two suffix filters', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect,
            { thingList: [1], anotherThingList: [11] })).toEqual(
          _.filter(jobBundleFixture.defect,
            { thing: { id: 1 }, anotherThing: { id: 11 } }));
      });

      it('should filter for single id with suffix & id filters', () => {
        expect(offlineDataService._filter(
            jobBundleFixture.defect,
            { idList: [1], thingList: [1] })).toEqual(
          _.filter(jobBundleFixture.defect,
            { id: 1, thing: { id: 1 } }));
      });
    });
  });

  describe('pagination', () => {
    describe('handle empty inputs', () => {
      it('should return all data with pagination addition', () => {
        const expectation = angular.copy(jobBundleFixture.defect);
        expectation.pagination = {
          first: true,
          last: true,
          number: _.size(jobBundleFixture.defect),
          numberOfElements: _.size(jobBundleFixture.defect),
          size: _.size(jobBundleFixture.defect),
          sort: '',
          order: 'asc',
          totalElements: _.size(jobBundleFixture.defect),
          totalPages: 1
        };

        expect(offlineDataService._paginate(
            jobBundleFixture.defect, { })).toEqual(
          expectation);
        expect(offlineDataService._paginate(
            jobBundleFixture.defect)).toEqual(
          expectation);
      });
    });

    describe('size', () => {
      const size = 1;
      let result = null;

      beforeEach(() =>
        result = offlineDataService._paginate(
            jobBundleFixture.defect, { size }));

      it('should limit length', () => {
        expect(result.length).toEqual(1);
        expect(result).not.toContain(
          _.find(jobBundleFixture.defect, { id: 2 }));
        expect(result).toContain(
          _.find(jobBundleFixture.defect, { id: 1 }));
      });

      it('should add appropriate pagination information', () =>
        expect(result.pagination).toEqual({
          first: true,
          last: false,
          number: size,
          numberOfElements: _.size(jobBundleFixture.defect),
          size,
          sort: '',
          order: 'asc',
          totalElements: _.size(jobBundleFixture.defect),
          totalPages: _.size(jobBundleFixture.defect) / size
        }));
    });

    describe('page', () => {
      describe('first', () => {
        const page = 1, size = 1;
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { size, page }));

        it('should offset data', () => {
          expect(result).toContain(
            _.find(jobBundleFixture.defect, { id: 1 }));
          expect(result).not.toContain(
            _.find(jobBundleFixture.defect, { id: 2 }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: true,
            last: false,
            number: size,
            numberOfElements: _.size(jobBundleFixture.defect),
            size,
            sort: '',
            order: 'asc',
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: _.size(jobBundleFixture.defect) / size
          }));
      });

      describe('second', () => {
        const page = 2, size = 1;
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { size, page }));

        it('should offset data', () => {
          expect(result).not.toContain(
            _.find(jobBundleFixture.defect, { id: 1 }));
          expect(result).toContain(
            _.find(jobBundleFixture.defect, { id: 2 }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: false,
            last: true,
            number: size,
            numberOfElements: _.size(jobBundleFixture.defect),
            size,
            sort: '',
            order: 'asc',
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: _.size(jobBundleFixture.defect) / size
          }));
      });
    });

    describe('sort', () => {
      describe('by id', () => {
        const sort = 'id';
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { sort }));

        it('should sort data', () => {
          expect(_.first(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 1 }));
          expect(_.last(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 2 }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: true,
            last: true,
            number: _.size(jobBundleFixture.defect),
            numberOfElements: _.size(jobBundleFixture.defect),
            size: _.size(jobBundleFixture.defect),
            sort,
            order: 'asc',
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: 1
          }));
      });

      describe('by another other property', () => {
        const sort = 'oneMoreThing';
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { sort }));

        it('should sort data', () => {
          expect(_.first(result)).toEqual(
            _.find(jobBundleFixture.defect, { oneMoreThing: 'a' }));
          expect(_.last(result)).toEqual(
            _.find(jobBundleFixture.defect, { oneMoreThing: 'b' }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: true,
            last: true,
            number: _.size(jobBundleFixture.defect),
            numberOfElements: _.size(jobBundleFixture.defect),
            size: _.size(jobBundleFixture.defect),
            sort,
            order: 'asc',
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: 1
          }));
      });
    });

    describe('order', () => {
      describe('ascending', () => {
        const order = 'asc', sort = 'id';
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { sort, order }));

        it('should order data by id', () => {
          expect(_.first(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 1 }));
          expect(_.last(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 2 }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: true,
            last: true,
            number: _.size(jobBundleFixture.defect),
            numberOfElements: _.size(jobBundleFixture.defect),
            size: _.size(jobBundleFixture.defect),
            sort,
            order,
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: 1
          }));
      });

      describe('descending', () => {
        const order = 'desc', sort = 'id';
        let result = null;

        beforeEach(() =>
          result = offlineDataService._paginate(
            jobBundleFixture.defect, { sort, order }));

        it('should order data by id', () => {
          expect(_.first(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 2 }));
          expect(_.last(result)).toEqual(
            _.find(jobBundleFixture.defect, { id: 1 }));
        });

        it('should add appropriate pagination information', () =>
          expect(result.pagination).toEqual({
            first: true,
            last: true,
            number: _.size(jobBundleFixture.defect),
            numberOfElements: _.size(jobBundleFixture.defect),
            size: _.size(jobBundleFixture.defect),
            sort,
            order,
            totalElements: _.size(jobBundleFixture.defect),
            totalPages: 1
          }));
      });
    });
  });

  describe('dehyration', () => {
    describe('tidy hydrated properties', () => {
      let expectation = null;
      let result = null;

      beforeEach(() => {
        expectation = _.find(jobBundleFixture.assetNote, { id: 1 });
        result = offlineDataService._dehydrate(
          _.find(jobBundleFixture.assetNote, { id: 1 }), { thing: null });
      });

      it('should remove hydrated properties from specified object recursively',
        () => {
          expect(result.thing).toEqual(
            _.pick(expectation.thing, ['id', '_id']));
          expect(result.yetAnotherThing.deeperThing.thing).toEqual(
            _.pick(expectation.yetAnotherThing.deeperThing.thing,
              ['id', '_id']));
        });

      it('shouldn\'t remove hydrated properties from unspecified object', () =>
        expect(result.anotherThing).toEqual(expectation.anotherThing));
    });

    describe('tidy other properties', () => {
      let expectation = null;
      let result = null;

      beforeEach(() => {
        expectation = angular.copy(_.find(jobBundleFixture.assetNote, { id: 1 }));
        expectation.date = moment();
        result = offlineDataService._dehydrate(expectation);
      });

      it('should remove angular properties from any object', () =>
        expect(_.keys(result.thing)).not.toContain('$angular'));

      it('should set moment properties to their iso strings', () =>
        expect(result.date).toEqual(expectation.date.toISOString()));
    });
  });
});
