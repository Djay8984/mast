import 'spec/spec-helper';
import services from 'app/services';

describe('Job Decision Service', () => {
  let jobDecisionService;
  let $rootScope;
  let Restangular;
  let employeeDecisionService;
  let referenceDataService;
  let jobStatus;
  let officeRole;
  let surveyorRole;
  let buildServices;
  let buildInjections;

  let employeeId;
  let openStatusIds;
  let assetIds;

  beforeEach(() => {
    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        referenceDataService =
          jasmine.createSpyObj('referenceDataService', ['get']);
        $provide.value('referenceDataService', referenceDataService);

        employeeDecisionService =
          jasmine.createSpyObj('employeeDecisionService',
            ['getUserFromName', 'getCurrentUser']);
        employeeDecisionService.getUserFromName.and.returnValue({});
        employeeDecisionService.getCurrentUser.and.returnValue({});
        $provide.value('employeeDecisionService', employeeDecisionService);

        Restangular = jasmine.createSpyObj('Restangular', ['all', 'one']);
        Restangular.all.and.returnValue(Restangular);
        Restangular.one.and.returnValue(Restangular);
        $provide.value('Restangular', Restangular);

        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4,
          awaitingTechnicalReviewerAssignment: 5,
          underTechnicalReview: 6,
          awaitingEndorserAssignment: 7,
          underEndorsement: 8,
          cancelled: 9,
          aborted: 10,
          closed: 11
        };
        jobStatus = {
          get: (field) => jobStatuses[field],
          toObject: () => jobStatuses
        };
        $provide.value('jobStatus', jobStatus);

        const officeRoles = {
          sdo: 1,
          cfo: 2,
          cfo3: 3,
          mmsSdo: 4,
          mmso: 5,
          tso: 6,
          tsoStat: 7,
          classGroup: 8,
          dce: 9,
          eic: 10,
          mds: 11,
          tsdo: 12,
          tssao: 13,
          caseSdo: 14,
          jobSdo: 15
        };
        officeRole = {
          get: (field) => officeRoles[field],
          toObject: () => officeRoles
        };
        $provide.value('officeRole', officeRole);

        const surveyorRoles = {
          businessSupport: 1,
          surveyor: 2,
          management: 3,
          salesCRM: 4,
          specialistLeadership: 5,
          authorisingSurveyor: 6,
          eicCaseSurveyor: 7,
          eicAdmin: 8,
          sdo: 9,
          eicManager: 10,
          leadSurveyor: 11,
          sdoCoordinator: 12
        };
        surveyorRole = {
          get: (field) => surveyorRoles[field],
          toObject: () => surveyorRoles
        };
        $provide.value('surveyorRole', surveyorRole);

        $provide.value('jobDecisionService', jobDecisionService);
      });
    };
    buildInjections = () => {
      inject((_jobDecisionService_, _$rootScope_) => {
        jobDecisionService = _jobDecisionService_;
        $rootScope = _$rootScope_;
      });
    };
    buildServices();
    buildInjections();

    [ employeeId, openStatusIds ] = [1, [2, 3]];
    [ assetIds, openStatusIds ] = [[1], [2, 3]];
  });

  /* eslint no-loop-func: 0 */
  const methods = ['forAssets', 'forEmployee', 'get', 'getCertificatesForJob',
    'getSurveysForJob', 'saveNewSurveyForJob', 'getUser', 'saveJobTeam',
    'openStatusIds', 'processModel', 'processSurveyors', 'updateJob', 'query',
    'referenceDataKeyMap'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  it('requests only active jobs', async () => {
    spyOn(jobDecisionService, 'query').and.callFake(() => true);
    spyOn(jobDecisionService, 'openStatusIds').and.returnValue(openStatusIds);

    await jobDecisionService.forEmployee(employeeId);
    expect(jobDecisionService.query).toHaveBeenCalledWith({
      employeeId: [ employeeId ],
      jobStatusId: openStatusIds
    }, jasmine.any(Object));
  });

  it('requests only active jobs', async () => {
    spyOn(jobDecisionService, 'query').and.callFake(() => true);
    spyOn(jobDecisionService, 'openStatusIds').and.returnValue(openStatusIds);

    await jobDecisionService.forAssets(assetIds);
    expect(jobDecisionService.query).toHaveBeenCalledWith({
      assetId: assetIds,
      jobStatusId: openStatusIds
    }, jasmine.any(Object));
  });

  describe('Helper Functions', () => {
    it('"get" should populate attributes', async () => {
      spyOn(jobDecisionService, 'populateAttributes').and.callFake(() => true);
      await jobDecisionService.get(1);
      expect(jobDecisionService.populateAttributes).toHaveBeenCalled();
    });
  });
});
