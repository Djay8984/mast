import Immutable from 'immutable';

import 'spec/spec-helper';

import services from 'app/services';

describe('rbac.service', () => {
  let rbacService = null;

  const rbacActionsFixture = new Immutable.Map({
    doThingOne: 1,
    doThingTwo: 2,
    doThingThree: 3
  });

  const rbacGroupPermissionsFixture = new Immutable.Map({
    groupOne: [1],
    groupTwo: [1, 2]
  });

  beforeEach(() => {
    angular.mock.module(services, ($provide) => {
      $provide.value('rbacActions', rbacActionsFixture);
      $provide.value('rbacGroupPermissions', rbacGroupPermissionsFixture);
    });

    inject((_rbacService_) => {
      rbacService = _rbacService_;
    });
  });

  describe('can', () => {
    it('should check single group permissions', () => {
      expect(rbacService.can(['groupOne'], 'doThingOne')).toBe(true);
      expect(rbacService.can(['groupOne'], 'doThingTwo')).toBe(false);
      expect(rbacService.can(['groupTwo'], 'doThingTwo')).toBe(true);
      expect(rbacService.can(['groupTwo'], 'doThingThree')).toBe(false);
    });

    it('should check multiple group permissions', () => {
      expect(rbacService.can(['groupOne', 'groupTwo'], 'doThingOne')).toBe(
        true);
      expect(rbacService.can(['groupOne', 'groupTwo'], 'doThingTwo')).toBe(
        true);
      expect(rbacService.can(['groupTwo', 'groupThree'], 'doThingThree')).toBe(
        false);
    });

    it('should throw an error if groups are invalid', () => {
      expect(() => rbacService.can(['whatever'], 'doThingOne')).toThrowError(
        Error, 'invalid groups: whatever');
    });
  });

  describe('available actions', () => {
    it('should give available actions for single group', () =>
      expect(rbacService.availableActions(['groupOne'])).toEqual(
        ['doThingOne']));

    it('should give available actions for multiple groups', () =>
      expect(rbacService.availableActions(['groupOne', 'groupTwo'])).toEqual(
        ['doThingOne', 'doThingTwo']));

    it('should throw an error if groups are invalid', () => {
      expect(() => rbacService.availableActions(['whatever'])).toThrowError(
        Error, 'invalid groups: whatever');
    });
  });
});
