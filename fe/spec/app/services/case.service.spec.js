import 'spec/spec-helper';
import 'angular-moment';

import services from 'app/services';

describe('Case Service', () => {
  let $rootScope;
  let $scope;
  let caseService;
  let referenceDataService;
  let Restangular;
  let caseStatus;
  let employeeDecisionService;
  let buildServices;
  let buildInjections;

  let assetId;
  let employeeId;
  let openStatusIds;
  let model;
  let flag;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        // referenceDataService;
        referenceDataService =
          jasmine.createSpyObj('referenceDataService', ['get']);
        $provide.value('referenceDataService', referenceDataService);

        // Restangular;
        Restangular = jasmine.createSpyObj('Restangular',
          ['one', 'all', 'customPUT', 'customPOST']);
        Restangular.one.and.returnValue(Restangular);
        Restangular.all.and.returnValue(Restangular);
        Restangular.customPUT.and.returnValue(Restangular);
        Restangular.customPOST.and.returnValue(Restangular);
        $provide.value('Restangular', Restangular);

        // employeeDecisionService;
        employeeDecisionService =
          jasmine.createSpyObj('employeeDecisionService', ['getCurrentUser']);
        employeeDecisionService.getCurrentUser.and.returnValue({});
        $provide.value('employeeDecisionService', employeeDecisionService);

        // caseStatus;
        const caseStatusMock = {
          jobPhase: 4,
          onHold: 3,
          populate: 2,
          uncommitted: 1,
          validateAndUpdate: 5,
          closed: 6,
          cancelled: 7,
          deleted: 8,
          takeOffHold: 9
        };
        caseStatus = {
          get: (field) => caseStatusMock[field],
          toObject: () => caseStatusMock
        };
        $provide.value('caseStatus', caseStatus);
      });
    };
    buildInjections = () => {
      inject((_caseService_, _$rootScope_) => {
        caseService = _caseService_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
      });
    };

    buildServices();
    buildInjections();

    [ employeeId, openStatusIds ] = [1, [1, 2, 3, 4, 5, 8, 9]];
    [ assetId, openStatusIds ] = [[1], [1, 2, 3, 4, 5, 8, 9]];

    model = { asset: { 'id': 1 } };
    flag = false;
  });

  /* eslint no-loop-func: 0 */
  const methods = ['updateCase', 'getCase', 'openStatusIds', 'isOpenStatus',
    'customerQuery', 'customerTypeahead', 'getCustomers', 'societyQuery',
    'societyTypeahead', 'momentiseDates', 'processModel', 'checkExistingCases',
    'saveCase', 'query', 'forEmployee', 'forAssets', 'referenceDataKeyMap',
    'validateCase'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  it('requests only active cases for employees', async () => {
    spyOn(caseService, 'openStatusIds').and.returnValue(openStatusIds);
    spyOn(caseService, 'query').and.callFake(() => true);
    await caseService.forEmployee(employeeId);
    expect(caseService.query).toHaveBeenCalledWith({
      employeeId: [ employeeId ],
      caseStatusId: openStatusIds
    }, jasmine.any(Object));
  });

  it('requests only active cases for assets', async () => {
    spyOn(caseService, 'openStatusIds').and.returnValue(openStatusIds);
    spyOn(caseService, 'query').and.callThrough();
    await caseService.forAssets(assetId);
    expect(caseService.query).toHaveBeenCalledWith({
      assetId,
      caseStatusId: openStatusIds
    }, jasmine.any(Object));
  });

  it('should post the new case with new processModel', async () => {
    spyOn(caseService, 'saveCase').and.callFake(() => true);
    spyOn(caseService, 'processModel').and.callFake(() => true);

    // Spy on the checkExistingCases method and throw it;
    const existingCheck = jasmine.createSpy(caseService, 'checkExistingCases');
    existingCheck.and.throwError();

    // Perform the save, and expect things to happen;
    const save = await caseService.saveCase(model, flag);

    // Check the arguments on the call;
    expect(caseService.saveCase).toHaveBeenCalledWith(model, flag);

    // We expect the processModel to have been called;
    caseService.processModel(model);

    // We expect the checkExistingCases to error anyway because we set that;
    expect(existingCheck).toThrow();

    // Now we call the checkExistingCases method but don't throw it;
    existingCheck.and.returnValue(1);

    // The result from saveCase should not be blank;
    expect(save).not.toEqual();
  });

  it('processModel method should work correctly', async () => {
    spyOn(caseService, 'processModel').and.callThrough();

    // Rewrite model slightly;
    model.offices = [
      {
        office: null
      }
    ];
    model.surveyors = [
      {
        surveyor: null
      },
      {
        surveyor: {
          id: 1
        }
      }
    ];

    const processedModel = await caseService.processModel(model);

    // We expect it to have had one argument;
    expect(caseService.processModel).toHaveBeenCalledWith(model);

    // processModel should have reduced model.surveyors to one entry;
    expect(processedModel.surveyors.length).toBe(1);
  });

  it('processModel method should add dates to the model', async () => {
    spyOn(caseService, 'processModel').and.callThrough();

    // Rewrite model slightly;
    model.offices = [
      {
        office: null
      }
    ];
    model.surveyors = [
      {
        surveyor: { }
      }
    ];
    model.caseAcceptanceDate = '2016-09-06';

    const processedModel = await caseService.processModel(model);

    expect(processedModel.caseAcceptanceDate).not.toBe(null);
    expect(processedModel.caseAcceptanceDate).not.toEqual('2016-09-06');
  });
});
