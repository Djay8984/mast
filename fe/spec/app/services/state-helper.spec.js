import 'spec/spec-helper';
import constants from 'app/constants';

const debug = (value, pad = '') => {
  pad += '   ';

  if (_.isObject(value)) {
    _.forEach(value, (val, key) => {
      console.log(pad, key, typeof val);
      debug(val, pad);
    })
  } else {
    console.log(pad, typeof value, '=>', value);
  }
};

describe('stateHelper', () => {
  let $rootScope, stateHelper, routeObj, $stateProviderMock;

  beforeEach(() => {
    angular.mock.module(constants);

    inject((_$rootScope_, _stateHelper_) => {
      $rootScope = _$rootScope_;
      stateHelper = _stateHelper_;
    });

    $stateProviderMock = {
      states: {},
      statesArr: [],
      state(name, state) {
        this.name = name;
        this.stateObj = state;
        this.states[name] = state;
        this.statesArr.push(state);
      }
    }

    routeObj = [
      {
        title: 'Add new Job',
        name: 'asset.jobs.add',
        parent: 'asset',
        url: '/jobs/add',
        content: {
          controller: 'AddJobController',
          templateUrl: 'AddJobTemplateUrl',
          resolve: {
            add: () => 1110,
            add2: () => 2220
          }
        },
        params: {
          caseId: null,
          backTarget: null
        }
      },
      {
        title: 'Wrong state',
        parent: 'asset',
        url: '/some'
      },
      {
        name: 'asset.jobs',
        parent: 'asset',
        url: '/jobs/{jobId:int}',
        abstract: true,
        header: { open: true },
        data: {
          test: 333
        },
        viewsHeader: {
          controller: 'HeaderController',
          templateUrl: 'headerTemplateUrl'
        },
        resolve: {
          asd: () => 333
        },
        children: [
          {
            title: 'View Job {{job.name}}',
            name: 'view',
            url: '/view',
            header: { open: false },
            params: {
              backTarget: null,
              caseId: null
            },
            controller: 'Other',
            content: {
              controller: 'ViewJobController',
              templateUrl: 'ViewJobTemplateUrl',
              resolve: {
                view: () => 111,
                view2: () => 222
              }
            }
          },
          {
            title: 'Edit job',
            name: 'edit',
            url: '/edit',
            controller: 'Other',
            templateUrl: 'OtherTemplate',
            target: '',
            resolve: { test: true },
            content: {
              controller: 'EditJobController',
              templateUrl: 'EditJobTemplateUrl',
              resolve: {
                test: () => 111,
                editTest: () => 444,
                editTest2: () => 555
              }
            }
          },
          {
            name: 'add-scope',
            url: '/add-scope',
            content: {
              controller: 'AddScopeController',
              templateUrl: 'AddScopeTemplateUrl'
            }
          }
        ]
      }
    ];
  });

  describe('Create', () => {
    it('State empty', () => {
      expect($stateProviderMock.name).toBeUndefined();
      expect($stateProviderMock.stateObj).toBeUndefined();
    });

    it('State is created', () => {
      stateHelper.create($stateProviderMock, routeObj);

      expect($stateProviderMock.name).toBeDefined();
      expect($stateProviderMock.stateObj).toBeDefined();
    });

    it('Create all states', () => {
      stateHelper.create($stateProviderMock, routeObj);

      const createdStates = ['asset.jobs.add', 'asset.jobs', 'asset.jobs.view',
      'asset.jobs.edit', 'asset.jobs.add-scope'];

      createdStates.forEach((val) => {
        expect($stateProviderMock.states[val]).toEqual(jasmine.any(Object));
      });

      expect(_.size($stateProviderMock.states)).toEqual(_.size(createdStates));
    });

    it('Should execute once', () => {
      stateHelper.create($stateProviderMock, routeObj);
      expect(_.size($stateProviderMock.states)).toEqual(_.size($stateProviderMock.statesArr));
    });
  });

  describe('Should extend item, when parent is not set', () => {
    beforeEach(() => {
      stateHelper.create($stateProviderMock, [
        {
          name: 'test',
          children: [{ name: 'next.child' }]
        }
      ]);
    });

    it('Should concat name with parent name', () => {
      expect($stateProviderMock.states['test']).toEqual(jasmine.any(Object));
      expect($stateProviderMock.states['test.next.child']).toEqual(jasmine.any(Object));
    });

    it('Should create child parent name', () => {
      expect($stateProviderMock.states['test.next.child'].parent).toEqual('test');
    });
  });

  describe('Extend item, when parent is set', () => {
    beforeEach(() => {
      stateHelper.create($stateProviderMock, [
        {
          name: 'test',
          children: [{ name: 'next.child', parent: 'some' }]
        }
      ]);
    });

    it('Have fixed name', () => {
      expect($stateProviderMock.states['test']).toEqual(jasmine.any(Object));
      expect($stateProviderMock.states['next.child']).toEqual(jasmine.any(Object));
    });

    it('Child parent name stays the same', () => {
      expect($stateProviderMock.states['next.child'].parent).toEqual('some');
    });
  });

  describe('Extend content', () => {
    let parentAdd, parent, childEdit, childView;
    beforeEach(() => {
      stateHelper.create($stateProviderMock, routeObj);
      parentAdd = $stateProviderMock.states['asset.jobs.add'];
      parent = $stateProviderMock.states['asset.jobs'];
      childView = $stateProviderMock.states['asset.jobs.view'];
      childEdit = $stateProviderMock.states['asset.jobs.edit'];
    });

    it('Should create views using default target and content', () => {
      expect(parentAdd.views['@main'].controller).toEqual('AddJobController');
      expect(parentAdd.views['@main'].templateUrl).toEqual('AddJobTemplateUrl');
      expect(parentAdd.views['@main'].resolve).toBeUndefined();
    });

    it('Should create views using target and overwrite content elements', () => {
      expect(childEdit.views[''].controller).toEqual('Other');
      expect(childEdit.views[''].templateUrl).toEqual('OtherTemplate');
      expect(childEdit.views[''].resolve).toBeUndefined();
      expect(childEdit.views['@main']).toBeUndefined();

      expect(childView.views['@main'].controller).toEqual('Other');
      expect(childView.views['@main'].templateUrl).toEqual('ViewJobTemplateUrl');
      expect(childView.views['@main'].resolve).toBeUndefined();
      expect(childView.views['']).toBeUndefined();
    });

    it('Should extend content.resolve with resolve', () => {
      expect(parentAdd.resolve.add).toEqual(jasmine.any(Function));

      expect(childEdit.resolve.test).toBe(true);
      expect(childEdit.resolve.editTest).toEqual(jasmine.any(Function));

      expect(childView.resolve.view).toEqual(jasmine.any(Function));
    });

    it('Creates header views', () => {
      expect(parent.views['header@main'].controller).toEqual('HeaderController');
      expect(parent.views['header@main'].controllerAs).toEqual('vm');
      expect(parent.views['header@main'].templateUrl).toEqual('headerTemplateUrl');
    });

  describe('Resolve', () => {
    it('To have all resolves in resolve', () => {
      expect(childEdit.resolve.resolve.length).toBe(4);
    });
  });
  });
});
