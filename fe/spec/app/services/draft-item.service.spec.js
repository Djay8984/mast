import 'spec/spec-helper';
import services from 'app/services';
import draftItem from 'spec/fixtures/draft-item.json';

xdescribe('draftItemService', () => {
  let draftItemService;
  let Restangular;
  let draftItemFixture;

  beforeEach(() => {
    draftItemFixture = angular.fromJson(draftItem);

    angular.mock.module(services, ($provide) => {
      Restangular = jasmine.createSpyObj('Restangular', ['one', 'customPOST']);
      Restangular.one.and.returnValue(Restangular);
      Restangular.customPOST.and.returnValue(draftItemFixture);
      $provide.value('Restangular', Restangular);

      $provide.value('appConfig',
        jasmine.createSpyObj('appConfig', ['not', 'used']));
    });

    inject((_draftItemService_) => {
      draftItemService = _draftItemService_;
    });
  });

  describe('#get draft item', () => {
    it('calls the restangular service', async () => {
      await draftItemService.createDraftItem(1);
      expect(Restangular.customPOST).toHaveBeenCalled();
    });

    it('brings back a draftItem object', async () => {
      const newItem = await draftItemService.createDraftItem(1);
      expect(newItem.id).toBe(1);
    });

    xit('saves the draftItem object', async () => {
      expect(draftItemService.draftObject).toEqual({});
      await draftItemService.createDraftItem(1);
      expect(draftItemService.draftObject.id).toBe(1);
    });

    describe('can copy an assetModel', () => {
      beforeEach(() => {
        spyOn(draftItemService, '_draftItem').and.returnValue(draftItemFixture);
      });

      it('should have the asset model in memory', async () => {
        expect(draftItemService.draftObject().id).toBe(1);
      });
    });
  });
});
