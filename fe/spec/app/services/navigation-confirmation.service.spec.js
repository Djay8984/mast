import 'spec/spec-helper';
import services from 'app/services';

describe('navigationConfirmationService', () => {
  let $rootScope;
  let $state;
  let controller;
  let MastModal;
  let navigationConfirmationService;
  let Restangular;

  const dialogContent = '<h1>Dialog</h1><div>dialog content</div>';

  beforeEach(() => {
    controller = jasmine.createSpyObj('controller',
      ['needNavConfirmation', 'launchNavigationConfirmation']);
    controller.launchNavigationConfirmation.and.returnValue({
      then: (cb) => cb()
    });

    $state = jasmine.createSpy('$state');
    Restangular = jasmine.createSpy('Restangular');
    MastModal = jasmine.createSpyObj('MastModal', ['activate']);
    MastModal.activate.and.returnValue({
      then: () => Object.create()
    });

    // make spies available for injection by Angular;
    angular.mock.module(services, ($provide) => {
      $provide.value('Restangular', Restangular);
      $provide.value('MastModalFactory', MastModal);
      $provide.value('$state', $state);
    });

    // inject stuff the class needs;
    inject((_navigationConfirmationService_, _$rootScope_) => {
      navigationConfirmationService = _navigationConfirmationService_;
      $rootScope = _$rootScope_;
    });
  });

  /*
   needs params: $state, dialogContent, controller
   on rootscope.$stateChangeStart ...
   if(controller.needNavConfirmation) open dialog (launchNavigationConfirmation)
   else don't
   */
  describe('Navigation dialog is shown', () => {
    beforeEach(() => {
      controller.needNavConfirmation.and.returnValue(true);
    });
    it('checks controller attempts to launch dialog', () => {
      navigationConfirmationService.setConfirmationCase(
        $state, dialogContent, controller);
      $rootScope.$broadcast('$stateChangeStart', null, $state, {});
      expect(controller.needNavConfirmation).toHaveBeenCalled();
      expect(MastModal.activate).toHaveBeenCalled();
    });
  });

  describe('Navigation dialog is not shown', () => {
    beforeEach(() => {
      controller.needNavConfirmation.and.returnValue(false);
    });
    it('checks controller does not cause dialog to launch', () => {
      navigationConfirmationService.setConfirmationCase(
        $state, dialogContent, controller);
      $rootScope.$broadcast('$stateChangeStart', null, $state, {});
      expect(controller.needNavConfirmation).toHaveBeenCalled();
      expect(MastModal.activate).not.toHaveBeenCalled();
    });
  });
});
