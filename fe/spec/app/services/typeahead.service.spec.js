import * as _ from 'lodash';

import 'spec/spec-helper';

import services from 'app/services';
import typeaheadFixtureRaw from 'spec/fixtures/typeahead.json';

const TYPEAHEAD_RESULTS_LENGTH = 8;

describe('typeahead.service', () => {
  const typeaheadFixture = angular.fromJson(typeaheadFixtureRaw);
  let typeaheadService = null;

  beforeEach(() => {
    angular.mock.module(services, ($provide) => {
      $provide.value('typeaheadResultsLength', TYPEAHEAD_RESULTS_LENGTH);
    });

    inject((_typeaheadService_) =>
      typeaheadService = _typeaheadService_);
  });


  describe('query to regex', () => {
    let result = null;

    beforeEach(() =>
      result = typeaheadService._queryToRegex('tes****t'));

    it('should produce case ignorant regex', () =>
      expect(result.ignoreCase).toBe(true));

    it('should remove wildcards', () =>
      expect(result.source).not.toContain('*'));

    it('should match at the beginning of the string', () =>
      expect(_.first(result.source)).toBe('^'));
  });

  describe('query', () => {
    describe('filter using default options', () => {
      const expectation = _.chain(typeaheadFixture)
        .filter((datum) => new RegExp('^thing', 'i').test(datum.name))
        .sortBy('name')
        .slice(0, TYPEAHEAD_RESULTS_LENGTH)
        .value();
      let result = null;

      beforeEach(() =>
        result = typeaheadService.query(typeaheadFixture)('thing'));

      it('should retrieve the right results', () =>
        expect(_.every(_.map(result,
              (datum) => _.includes(expectation, datum)))).toBe(true));

      it('should retrieve the right amount of results', () =>
        expect(_.size(result)).toEqual(_.size(expectation)));

      it('should retrieve the results in the correct order', () =>
        expect(_.every(_.map(result,
              (datum, index) => datum === _.get(expectation, index)))).toBe(true));
    });

    describe('filter using custom options', () => {
      const expectation = _.chain(typeaheadFixture)
        .filter((datum) => new RegExp('^Six', 'i').test(datum.extra))
        .sortBy('name')
        .slice(0, 3)
        .value();
      let result = null;

      beforeEach(() =>
        result = typeaheadService.query(typeaheadFixture, null,
          { field: 'extra', sort: 'name', length: 3 })('Six'));

      it('should retrieve the right results', () =>
        expect(_.every(_.map(result,
              (datum) => _.includes(expectation, datum)))).toBe(true));

      it('should retrieve the right amount of results', () =>
        expect(_.size(result)).toEqual(_.size(expectation)));

      it('should retrieve the results in the correct order', () =>
        expect(_.every(_.map(result,
              (datum, index) => datum === _.get(expectation, index)))).toBe(true));
    });

    describe('filter using custom prefilter', () => {
      const expectation = _.chain(typeaheadFixture)
        .filter((datum) => new RegExp('^thing', 'i').test(datum.name))
        .sortBy('name')
        .slice(0, 8)
        .value();
      let preFilterSpy = null;
      let result = null;

      beforeEach(() => {
        preFilterSpy = jasmine.createSpy('preFilterSpy');
        preFilterSpy.and.returnValue((datum) => _.includes(datum.name, 'thing'));
        result = typeaheadService.query(typeaheadFixture, preFilterSpy)(
            'thing', 'preFilterOption');
      });

      it('should execute prefilter', () =>
        expect(preFilterSpy).toHaveBeenCalledWith('preFilterOption'));

      it('should retrieve the right results', () =>
        expect(_.every(_.map(result,
              (datum) => _.includes(expectation, datum)))).toBe(true));

      it('should retrieve the right amount of results', () =>
        expect(_.size(result)).toEqual(_.size(expectation)));

      it('should retrieve the results in the correct order', () =>
        expect(_.every(_.map(result,
              (datum, index) => datum === _.get(expectation, index)))).toBe(true));
    });
  });
});
