import 'spec/spec-helper';
import services from 'app/services';

xdescribe('MyWorkService', () => {
  let $rootScope;
  let assetOnlineService;
  let caseService;
  let employeeOnlineService;
  let jobOnlineService;
  let MyWorkService;
  let buildServices;
  let buildInjections;

  beforeEach(async () => {
    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        caseService =
          jasmine.createSpyObj('caseService', ['forEmployee']);
        caseService.forEmployee.and.returnValue([]);
        $provide.value('caseService', caseService);

        assetOnlineService =
          jasmine.createSpyObj('assetOnlineService', ['getByIds']);
        assetOnlineService.getByIds.and.returnValue([]);
        $provide.value('assetOnlineService', assetOnlineService);

        employeeOnlineService =
          jasmine.createSpyObj('employeeOnlineService', ['getCurrentUser']);
        employeeOnlineService.getCurrentUser.and.returnValue({});
        $provide.value('employeeOnlineService', employeeOnlineService);

        jobOnlineService =
          jasmine.createSpyObj('jobOnlineService', ['forEmployee']);
        jobOnlineService.forEmployee.and.returnValue([]);
        $provide.value('jobOnlineService', jobOnlineService);

        $provide.value('$stateParams', jasmine.createSpyObj('$stateParams', ['']));

        // no idea
        $provide.value('appConfig', jasmine.createSpyObj('appConfig', ['ugh']));

        $provide.value('MyWorkService', {});
      });
    };
    buildInjections = (type) => {
      inject((_MyWorkService_, _$rootScope_) => {
        $rootScope = _$rootScope_;
        MyWorkService = _MyWorkService_;

        const current = {
          id: 1,
          groups: ['ADMIN'],
          workItemType: 'cases'
        };
        if (type === 'jobs') {
          current.workItemType = 'jobs';
        }
        employeeOnlineService.getCurrentUser.and.returnValue(current);
      });
    };
    buildServices();
  });

  describe('#query', () => {
    describe('when the user can see cases', () => {
      it('calls the case service', async () => {
        buildInjections('cases');
        await MyWorkService.getForUser({ id: 1 }, { });
        expect(caseService.forEmployee).toHaveBeenCalled();
      });

      it('does not call the job service', async () => {
        buildInjections('cases');
        await MyWorkService.getForUser({ id: 2 }, { });
        expect(jobOnlineService.forEmployee).not.toHaveBeenCalled();
      });
    });

    describe('when the user can see jobs', () => {
      it('does not call the case service', async () => {
        buildInjections('jobs');
        await MyWorkService.getForUser({ id: 3 }, { });
        expect(caseService.forEmployee).not.toHaveBeenCalled();
      });

      it('calls the job service', async () => {
        buildInjections('jobs');
        await MyWorkService.getForUser({ id: 4 }, { });
        expect(jobOnlineService.forEmployee).toHaveBeenCalled();
      });
    });
  });
});
