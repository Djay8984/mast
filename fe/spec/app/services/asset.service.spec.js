import 'spec/spec-helper';
import assetServices from 'app/services/asset';

describe('assetService', () => {
  let assetOnlineService;
  let caseService;
  let employeeDecisionService;
  let jobDecisionService;
  let referenceDataService;
  let Restangular;

  beforeEach(() => {
    angular.mock.module(assetServices, ($provide) => {
      caseService =
        jasmine.createSpyObj('caseService', ['forAssets']);
      caseService.forAssets.and.returnValue([]);
      $provide.value('caseService', caseService);

      employeeDecisionService =
        jasmine.createSpyObj('employeeDecisionService', ['getCurrentUser']);
      $provide.value('employeeDecisionService', employeeDecisionService);

      jobDecisionService =
        jasmine.createSpyObj('jobDecisionService', ['forAssets']);
      jobDecisionService.forAssets.and.returnValue([]);
      $provide.value('jobDecisionService', jobDecisionService);

      referenceDataService =
        jasmine.createSpyObj('referenceDataService', ['get']);
      referenceDataService.get.and.returnValue({});
      $provide.value('referenceDataService', referenceDataService);

      Restangular =
        jasmine.createSpyObj('Restangular',
          ['get', 'one', 'all', 'getList', 'customPOST', 'customPUT']);
      Restangular.one.and.returnValue(Restangular);
      Restangular.all.and.returnValue(Restangular);
      Restangular.get.and.returnValue(Restangular);
      Restangular.getList.and.returnValue([]);
      Restangular.customPOST.and.returnValue({});
      Restangular.customPUT.and.returnValue({});
      $provide.value('Restangular', Restangular);

      $provide.value('assetLifecycleStatus',
        jasmine.createSpyObj('assetLifecycleStatus', ['']));
      $provide.value('$stateParams', {});
    });

    inject((_assetOnlineService_) => {
      assetOnlineService = _assetOnlineService_;
    });
  });

  it('"getProducts" should return an object', async () => {
    expect(angular.isObject(await assetOnlineService.getProducts(1))).toBe(true);
  });

  it('"getServiceForJob" should return an object', async () => {
    expect(angular.isObject(await assetOnlineService.getServiceForJob(1))).toBe(true);
  });

  it('"create" should return an object', async () => {
    expect(angular.isObject(await assetOnlineService.create({ id: 1 }))).toBe(true);
  });

  it('"update" should return an object', async () => {
    expect(angular.isObject(await assetOnlineService.update({ id: 1 }))).toBe(true);
  });

  it('"getItem" should return an object', async () => {
    expect(angular.isObject(await assetOnlineService.getItem(1, 2))).toBe(true);
  });

  // TODO:
  // * enable most of these tests and fix how the case and job services are
  //   handled
  // * add new tests for online offline intregration

  describe('#addItemsToAssets', () => {
    let assetList;
    let result = null;

    describe('with no assets', () => {
      beforeEach(async () => {
        assetList = [];

        result = await assetOnlineService.addItemsToAssets(assetList);
      });

      it('does not call the employee service', () => {
        expect(employeeDecisionService.getCurrentUser).not.toHaveBeenCalled();
      });

      it('does not call the case service', () => {
        expect(caseService.forAssets).not.toHaveBeenCalled();
      });

      it('does not call the job service', () => {
        expect(jobDecisionService.forAssets).not.toHaveBeenCalled();
      });

      it('should return an empty array', () => {
        expect(angular.isArray(result)).toBe(true);
      });
    });

    xdescribe('with assets', () => {
      beforeEach(() => {
        assetList = [{ id: 1 }];
      });

      describe('cases users', () => {
        beforeEach(async () => {
          employeeDecisionService.getCurrentUser.and.returnValue({
            workItemType: 'cases'
          });

          await assetOnlineService.addItemsToAssets(assetList);
        });

        it('calls the case service', () => {
          // fails - not sure what actually gets called here
          expect(caseService.forAssets).toHaveBeenCalled();
        });

        it('does not call the job service', () => {
          expect(jobDecisionService.forAssets).not.toHaveBeenCalled();
        });

        it('only requests cases for the provided assets', () => {
          // not sure what actually gets called here
          expect(caseService.forAssets).toHaveBeenCalledWith(
            [1], jasmine.any(Object));
        });

        it('sorts the cases by created date', () => {
          expect(caseService.forAssets).toHaveBeenCalledWith(
            jasmine.any(Object), { sort: 'createdOn' });
        });
      });

      describe('jobs users', () => {
        beforeEach(async () => {
          employeeDecisionService.getCurrentUser.and.returnValue({
            workItemType: 'jobs'
          });

          await assetOnlineService.addItemsToAssets([{ id: 1 }]);
        });

        it('does not call the case service', async () => {
          expect(caseService.forAssets).not.toHaveBeenCalled();
        });

        it('calls the job service', async () => {
          expect(jobDecisionService.forAssets).toHaveBeenCalled();
        });

        it('only requests jobs for the provided assets', async () => {
          expect(jobDecisionService.forAssets).toHaveBeenCalledWith(
            [1], jasmine.any(Object));
        });

        it('sorts the jobs by created date', async () => {
          expect(jobDecisionService.forAssets).toHaveBeenCalledWith(
            jasmine.any(Object), { sort: 'createdOn' });
        });
      });
    });
  });
});
