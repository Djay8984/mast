import 'spec/spec-helper';
import 'angular-moment';

import services from 'app/services';

import AddDefectController
  from 'app/codicil/defect/add/add-defect.controller.js';

import defectCategoryFixture from 'spec/fixtures/defect-categories.json';
import defectDetailFixture from 'spec/fixtures/defect-details.json';

describe('Add Defect Controller', () => {
  let vm;
  let $rootScope;
  let $stateMock;
  let $stateParamsMock;
  let assetModelMock;
  let mastModalMock;
  let confidentialityTypesFixture;
  let defectMock;
  let assetMock;
  let jobMock;
  let surveyTypeMock;
  let buildInjections;
  let buildServices;
  let attachmentDecisionService;
  let defectDecisionService;
  let surveyDecisionService;
  let defectCategories;
  let defectDetails;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        defectDecisionService =
          jasmine.createSpyObj('defectDecisionService', ['save']);
        defectDecisionService.save.and.returnValue({});
        $provide.value('defectDecisionService', defectDecisionService);

        attachmentDecisionService =
          jasmine.createSpyObj('attachmentDecisionService', ['add']);
        attachmentDecisionService.add.and.returnValue({});
        $provide.value('attachmentDecisionService', attachmentDecisionService);

        surveyDecisionService =
          jasmine.createSpyObj('surveyDecisionService', ['generateSurvey']);
        surveyDecisionService.generateSurvey.and.returnValue({});
        $provide.value('surveyDecisionService', surveyDecisionService);

        $stateMock = jasmine.createSpyObj('$state', ['go']);
        $stateMock.go.and.returnValue({});
        $provide.value('$state', $stateMock);
      });
    };
    buildInjections = () => {
      inject((_$controller_, _$rootScope_, _moment_) => {
        $rootScope = _$rootScope_;

        $stateParamsMock = {};

        assetModelMock = {};

        mastModalMock = () => ({
          activate: () => true
        });

        confidentialityTypesFixture = [
          {
            id: 1,
            description: 'test1'
          },
          {
            id: 2,
            description: 'test2'
          }
        ];

        defectMock = {
          defectCategory: {
            id: 2
          }
        };

        assetMock = {
          id: 1,
          ihsAsset: {
            id: 2
          }
        };

        jobMock = {
          id: 1
        };

        const surveyTypes = {
          damage: 1,
          repair: 2
        };
        surveyTypeMock = {
          get: (field) => surveyTypes[field],
          toObject: () => surveyTypes
        };

        defectCategories = angular.fromJson(defectCategoryFixture);

        defectDetails = angular.fromJson(defectDetailFixture);

        vm = _$controller_(AddDefectController,
          {
            $state: $stateMock,
            $stateParams: $stateParamsMock,
            assetModel: assetModelMock,
            MastModalFactory: mastModalMock,
            confidentialityTypes: confidentialityTypesFixture,
            defect: defectMock,
            asset: assetMock,
            job: jobMock,
            surveyType: surveyTypeMock,
            refDefectCategories: defectCategories,
            refDefectDetails: defectDetails
          },
          {
            section: {
              items: {
                assetId: 1,
                type: 'multi'
              }
            }
          });
      });
    };

    buildServices();
    buildInjections();
  });

  /* eslint no-loop-func: 0 */
  const methods = ['selectedSubCategory', 'getDetails', 'filterDefectTypes',
    'filterOnboardLocations', 'filterDefectDescriptors',
    'filterPollutionTypes', 'removeItem', 'shouldHaveShipDetails',
    'changeLocations', 'hasLocation', 'defectTypeChanged',
    'defectDescriptorChanged', 'loadingConditionChanged',
    'locationChanged', 'selectLongitudinal', 'selectTransverse',
    'onBoardLocationChanged', 'pollutionTypeChanged', 'activityChanged',
    'saveOtherValues', 'saveOtherActivity', 'saveOtherLocation',
    'saveOtherPollution', 'saveOtherOnBoard', 'addExplosion', 'addGrounding',
    'removeCurrentDetailValue', 'removeCurrentDetailValues',
    'shouldHaveShipLoadingCondition', 'shouldHaveShipLocation',
    'shouldHaveShipActivity', 'addCodicil', 'rowIndices', 'rowSlice',
    'buildDefect', 'formValid', 'toggleCategory', 'toggleSubCategory',
    'subCategories', 'marshallItems', 'save', 'back'];
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  /* eslint no-loop-func: 0 */
  const getters = ['maxDate', 'getItems', 'items', 'itemRows',
    'refDefectCategories', 'refDefectDetails', 'defectTypes',
    'defectDescriptors', 'onBoardLocations', 'longitudinalLocations',
    'transverseLocations', 'pollutionTypes', 'groundingInvolved',
    'fireExplosion', 'shipLocations', 'job', 'asset',
    'confidentialityTypes', 'shipLoadingConditions',
    'otherActivitySpecified', 'otherLocationSpecified', 'shipActivities',
    'attachmentPath', 'showAllForm'];
  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  it('"setUpData" should be called on start', async () => {
    await vm.setUpData();
    expect(_.size(vm.section)).not.toBe(null);
  });

  xit('can save a basic defect', async () => {
    spyOn(vm, 'saveOtherValues').and.callFake(() => true);
    spyOn(vm, 'marshallItems').and.callFake(() => true);
    await vm.save();

    expect(vm.saveOtherValues).toHaveBeenCalled();
    expect(vm.marshallItems).toHaveBeenCalled();
    expect(surveyDecisionService.generateSurvey).toHaveBeenCalled();
    expect(defectDecisionService.save).toHaveBeenCalled();
    expect(attachmentDecisionService.addAll).toHaveBeenCalled();
  });

  describe('Helper Functions', () => {
    it('getter "maxDate" should return an object', async () => {
      expect(angular.isObject(await vm.maxDate)).toBe(true);
    });

    it('getItems should return an array', async () => {
      expect(angular.isArray(await vm.getItems())).toBe(true);
    });

    it('getter "items" should return an array', async () => {
      expect(angular.isArray(await vm.items)).toBe(true);
    });

    it('getter "itemRows" should return an array', async () => {
      expect(angular.isArray(await vm.itemRows)).toBe(true);
    });

    it('"selectedSubCategory" should return a boolean', async () => {
      vm.currentSubCategory = {
        id: 101
      };
      expect(await vm.selectedSubCategory({ id: 101 })).toBe(true);
      vm.currentSubCategory = null;
      expect(await vm.selectedSubCategory({ id: 101 })).toBe(false);
    });

    it('getter "refDefectCategories" should return an array', async () => {
      expect(angular.isArray(await vm.refDefectCategories)).toBe(true);
    });

    it('getter "refDefectDetails" should return an object', async () => {
      expect(angular.isObject(await vm.refDefectDetails)).toBe(true);
    });

    it('getter "defectTypes" should return an array', async () => {
      expect(angular.isArray(await vm.defectTypes)).toBe(true);
    });

    it('"getDetails" should call 6 methods', async () => {
      /* eslint arrow-body-style: 0 */
      spyOn(vm, 'filterDefectDescriptors').and.callFake(() => { });
      spyOn(vm, 'filterDefectTypes').and.callFake(() => { });
      spyOn(vm, 'filterOnboardLocations').and.callFake(() => { });
      spyOn(vm, 'filterLongitudinalLocations').and.callFake(() => { });
      spyOn(vm, 'filterTransverseLocations').and.callFake(() => { });
      spyOn(vm, 'filterPollutionTypes').and.callFake(() => { });
      await vm.getDetails();
      expect(vm.filterDefectDescriptors).toHaveBeenCalled();
      expect(vm.filterDefectTypes).toHaveBeenCalled();
      expect(vm.filterOnboardLocations).toHaveBeenCalled();
      expect(vm.filterLongitudinalLocations).toHaveBeenCalled();
      expect(vm.filterTransverseLocations).toHaveBeenCalled();
      expect(vm.filterPollutionTypes).toHaveBeenCalled();
    });

    it('getter "defectDescriptors" should return an array', async () => {
      expect(angular.isArray(await vm.defectDescriptors)).toBe(true);
    });

    it('"filterDefectTypes"', async () => {
      vm.defect = {
        defectCategory: {
          id: 64
        }
      };
      await vm.filterDefectTypes();
      expect(angular.isArray(vm._defectTypes)).toBe(true);
    });

    it('"filterOnboardLocations"', async () => {
      vm.defect = {
        defectCategory: {
          id: 193
        }
      };
      await vm.filterOnboardLocations();
      expect(angular.isArray(vm._onBoardLocations)).toBe(true);
    });

    it('"filterDefectDescriptors"', async () => {
      vm.defect = {
        defectCategory: {
          id: 81
        }
      };
      await vm.filterDefectDescriptors();
      expect(angular.isArray(vm._defectDescriptors)).toBe(true);
    });

    it('"filterTransverseLocations"', async () => {
      vm.defect = {
        defectCategory: {
          id: 32
        }
      };
      await vm.filterTransverseLocations();
      expect(angular.isArray(vm._transverseLocations)).toBe(true);
    });

    it('"filterPollutionTypes"', async () => {
      vm.defect = {
        defectCategory: {
          id: 32
        }
      };
      await vm.filterPollutionTypes();
      expect(angular.isArray(vm._pollutionTypes)).toBe(true);
    });

    it('getter "onBoardLocations" should return an array', async () => {
      expect(angular.isArray(await vm.onBoardLocations)).toBe(true);
    });

    it('getter "longitudinalLocations" should return an array', async () => {
      expect(angular.isArray(await vm.longitudinalLocations)).toBe(true);
    });

    it('getter "transverseLocations" should return an array', async () => {
      expect(angular.isArray(await vm.transverseLocations)).toBe(true);
    });

    it('getter "pollutionTypes" should return an array', async () => {
      /* eslint no-undefined: 0 */
      expect(await vm.pollutionTypes).toBe(undefined);
    });

    it('getter "groundingInvolved" should return an object', async () => {
      expect(angular.isObject(await vm.groundingInvolved)).toBe(true);
    });

    it('getter "fireExplosion" should return an object', async () => {
      expect(angular.isObject(await vm.fireExplosion)).toBe(true);
    });

    it('getter "shipLocations" should return an object', async () => {
      expect(angular.isObject(await vm.shipLocations)).toBe(true);
    });

    it('getter "job" should return an object', async () => {
      expect(angular.isObject(await vm.job)).toBe(true);
    });

    it('getter "asset" should return an object', async () => {
      expect(angular.isObject(await vm.asset)).toBe(true);
    });

    it('getter "confidentialityTypes" should return an array', async () => {
      expect(await vm.confidentialityTypes).toBe(undefined);
    });

    it('getter "shipLoadingConditions" should return an object', async () => {
      const result = await vm.shipLoadingConditions;
      expect(angular.isObject(result)).toBe(true);
    });

    it('"removeItem" should remove items', async () => {
      vm._items = [
        {
          id: 1
        },
        {
          id: 2
        }
      ];
      await vm.removeItem({ id: 2 });
      expect(_.size(vm._items)).toBe(1);
    });

    it('"shouldHaveShipDetails" should return a boolean', async () => {
      vm.currentCategory = {
        id: 33,
        name: 'Hull'
      };
      let result = await vm.shouldHaveShipDetails();
      expect(result).toMatch(/true|false/);
      vm.currentCategory = null;
      result = await vm.shouldHaveShipDetails();
      expect(result).toBe(false);
    });

    it('"changeLocations" should manipulate this.locations', async () => {
      vm.locations = [];
      await vm.changeLocations({ id: 1, name: 'peterborough' });
      expect(_.size(vm.locations)).toBe(1);
    });

    it('"hasLocation" should return an object', async () => {
      vm.locations = [
        {
          id: 1,
          name: 'London'
        },
        {
          id: 2,
          name: 'La Rochelle'
        }
      ];
      const result = await vm.hasLocation({ id: 2 });
      expect(result).toEqual({ id: 2, name: 'La Rochelle' });
    });

    it('"defectTypeChanged"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.defectTypeChanged('hull');
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"defectDescriptorChanged"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.defectDescriptorChanged('engine');
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"loadingConditionChanged"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.loadingConditionChanged('flag');
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"locationChanged"', async () => {
      // SHIP_OTHER_LOCATION = 297
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.locationChanged({ id: 297, name: 'london' });
      expect(vm.removeCurrentDetailValue).not.toHaveBeenCalled();
      await vm.locationChanged({ id: 296, name: 'Dover' });
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"selectLongitudinal"', async () => {
      spyOn(vm, 'removeCurrentDetailValues').and.callFake(() => true);
      await vm.selectLongitudinal();
      expect(vm.removeCurrentDetailValues).toHaveBeenCalled();
    });

    it('"selectTransverse"', async () => {
      spyOn(vm, 'removeCurrentDetailValues').and.callFake(() => true);
      await vm.selectLongitudinal();
      expect(vm.removeCurrentDetailValues).toHaveBeenCalled();
    });

    it('"onBoardLocationChanged"', async () => {
      // SHIP_OTHER_ONBOARD: 177
      spyOn(vm, 'removeCurrentDetailValues').and.callFake(() => true);
      await vm.onBoardLocationChanged([ { id: 177 }, { id: 2 } ]);
      expect(vm.removeCurrentDetailValues).toHaveBeenCalled();
      expect(vm.onBoardLocationSpecified).toEqual({ id: 177 });
    });

    it('"pollutionTypeChanged"', async () => {
      // SHIP_OTHER_POLLUTION: 445
      spyOn(vm, 'removeCurrentDetailValues').and.callFake(() => true);
      await vm.pollutionTypeChanged([ { id: 176 }, { id: 445 } ]);
      expect(vm.otherPollutionSpecified).toEqual({ id: 445 });
    });

    it('"activityChanged"', async () => {
      // SHIP_OTHER_ACTIVITY: 306
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.activityChanged({ id: 306 });
      expect(vm.otherActivitySpecified).toBe(true);
      expect(vm.removeCurrentDetailValue).not.toHaveBeenCalled();
      await vm.activityChanged({ id: 305 });
      expect(vm.otherActivitySpecified).toBe(false);
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"saveOtherValues"', async () => {
      let myValue = true;
      spyOn(vm, 'saveOtherLocation').and.callFake(() => myValue);
      spyOn(vm, 'saveOtherActivity').and.callFake(() => myValue);
      spyOn(vm, 'saveOtherOnBoard').and.callFake(() => myValue);
      spyOn(vm, 'saveOtherPollution').and.callFake(() => myValue);
      spyOn(vm, 'otherLocationSpecified').and.callFake(() => myValue);
      spyOn(vm, 'otherActivitySpecified').and.callFake(() => myValue);
      vm.otherLocationSpecified(true);
      vm.otherActivitySpecified(true);
      vm.onBoardLocationSpecified = true;
      vm.otherPollutionSpecified = true;
      await vm.saveOtherValues();
      expect(vm.otherLocationSpecified).toHaveBeenCalled();
      expect(vm.saveOtherLocation).toHaveBeenCalled();
      expect(vm.otherActivitySpecified).toHaveBeenCalled();
      expect(vm.saveOtherActivity).toHaveBeenCalled();
      expect(vm.saveOtherOnBoard).toHaveBeenCalled();
      expect(vm.saveOtherPollution).toHaveBeenCalled();

      myValue = false;
      vm.onBoardLocationSpecified = false;
      vm.otherPollutionSpecified = false;
      await vm.saveOtherValues();
      expect(vm.otherLocationSpecified).toHaveBeenCalled();
      expect(vm.otherActivitySpecified).toHaveBeenCalled();
      expect(vm.saveOtherOnBoard).toHaveBeenCalled();
      expect(vm.saveOtherPollution).toHaveBeenCalled();

      // if (vm.otherLocationSpecified) {
      //   expect(vm.saveOtherLocation).toHaveBeenCalled();
      // }
      // vm.otherLocationSpecified = false;
      // if (!vm.otherLocationSpecified) {
      //   expect(vm.saveOtherLocation).not.toHaveBeenCalled();
      // }
    });

    it('"saveOtherActivity"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.saveOtherActivity();
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"saveOtherLocation"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.saveOtherLocation();
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"addExplosion"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.addExplosion();
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"addGrounding"', async () => {
      spyOn(vm, 'removeCurrentDetailValue').and.callFake(() => true);
      await vm.addGrounding();
      expect(vm.removeCurrentDetailValue).toHaveBeenCalled();
    });

    it('"shouldHaveShipLoadingCondition"', async () => {
      spyOn(vm, 'shouldHaveShipDetails').and.callFake(() => true);
      await vm.shouldHaveShipLoadingCondition();
      expect(vm.shouldHaveShipDetails).toHaveBeenCalled();
    });

    it('"shouldHaveShipLocation"', async () => {
      spyOn(vm, 'shouldHaveShipDetails').and.callFake(() => true);
      await vm.shouldHaveShipLocation();
      expect(vm.shouldHaveShipDetails).toHaveBeenCalled();
    });

    it('"shouldHaveShipActivity"', async () => {
      spyOn(vm, 'shouldHaveShipDetails').and.callFake(() => true);
      await vm.shouldHaveShipActivity();
      expect(vm.shouldHaveShipDetails).toHaveBeenCalled();
    });

    it('"addCodicil"', async () => {
      vm.defect = {};
      await vm.addCodicil('coc');
      expect($stateMock.go).toHaveBeenCalled();
      vm.defect = { id: 1 };
      await vm.addCodicil('coc');
      expect($stateMock.go).toHaveBeenCalled();
    });

    it('getter "shipActivities" should return an object', async () => {
      expect(angular.isObject(await vm.shipActivities)).toBe(true);
    });

    it('"rowIndices" should return an array', async () => {
      let result = await vm.rowIndices([], 0);
      expect(result).toEqual([]);
      result = await vm.rowIndices([{ id: 1 }, { id: 2 }, { id: 3 }], 2);
      expect(result).toEqual([ 0, 1 ]);
    });

    it('"rowSlice" should return an array', async () => {
      const result = await vm.rowSlice({}, 1, 2);
      expect(angular.isArray(result)).toBe(true);
    });

    it('getter "showAllForm" should return a boolean', async () => {
      spyOn(vm, 'subCategories').and.callThrough();
      vm.currentCategory = {
        selected: true,
        parent: {
          id: 2
        }
      };
      vm.currentSubCategory = {
        id: 6,
        deleted: false,
        name: 'Contact',
        description: null,
        categoryLetter: null,
        parent: {
          id: 2,
          _id: '2'
        },
        _id: '6',
        selected: true
      };

      let result = await vm.showAllForm;
      expect(vm.subCategories).toHaveBeenCalled();
      expect(result).toBe(true);
      vm.currentCategory.selected = false;
      result = await vm.showAllForm;
      expect(result).toBe(false);
    });

    it('"formValid" should return a boolean', async () => {
      expect(await vm.formValid()).toMatch(/true|false/);
    });

    it('"toggleCategory"', async () => {
      // Set up our spies;
      spyOn(vm, 'subCategories').and.callFake(() => true);
      spyOn(vm, 'getDetails').and.callFake(() => true);
      let cat = { id: 456, name: 'Hull', selected: false };

      // First time through, we want a blank defect; call the method, and
      // then expect() the defectCategory to be added;
      vm.defect = {};
      await vm.toggleCategory(cat, 2);
      expect(vm.defect.defectCategory).toEqual({ id: 456 });
      expect(vm.getDetails).toHaveBeenCalled();
      expect(cat.selected).toEqual(true);
      expect(vm.currentCategory).toEqual(cat);
      expect(vm.subCategories).toHaveBeenCalledWith(cat);
      expect(vm.subCategories.calls.count()).toEqual(2);

      // Second time through;
      cat = { id: 5, name: 'Hull', selected: true };
      vm.defect = {
        category: {
          id: 456,
          name: 'Hull',
          selected: false
        },
        defectCategory: { }
      };
      await vm.toggleCategory(cat, 2);
      expect(cat.selected).toEqual(false);
      expect(vm.currentCategory).toEqual({});
      expect(vm.currentRow).toEqual(null);
      expect(vm.subCategories).toHaveBeenCalled();
    });

    it('"toggleSubCategory"', async () => {
      spyOn(vm, 'getDetails').and.callFake(() => true);
      spyOn(vm, 'subCategories').and.callFake(() => true);
      vm.defect = {
        defectCategory: { }
      };
      await vm.toggleSubCategory({ id: 2 }, { id: 3 }, 1);
      expect(vm.getDetails).toHaveBeenCalled();
      expect(vm.subCategories).toHaveBeenCalledWith({ id: 3 });
    });

    it('"subCategories" should return an array', async () => {
      spyOn(vm, 'refDefectCategories').and.callFake(() => true);
      expect(angular.isArray(await vm.subCategories({ id: 2 }))).toBe(true);
    });

    it('"marshallItems" should return an array', async () => {
      expect(angular.isArray(await vm.marshallItems([ { id: 21 }, { id: 22 } ]))).toBe(true);
    });

    it('"back" should to to the list page', async () => {
      await vm.back();
      expect($stateMock.go).toHaveBeenCalledWith('^.list',
        $stateParamsMock, jasmine.any(Object));
    });
  });
});
