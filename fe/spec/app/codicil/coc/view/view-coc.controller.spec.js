import 'spec/spec-helper';
import services from 'app/services';
import ViewCocController from 'app/codicil/coc/view/view-coc.controller';
import 'angular-moment';

describe('ViewCocController', () => {
  let $controller;
  let vm;
  let printService;
  let codicilDecisionService;
  let employeeDecisionService;
  let navigationService;
  let userProvider;
  const backStringValue = {
    name: 'thing',
    backTitle: 'thing2'
  };
  const CocInjectionObject = {
    $state: {
      current: {
        backString: ''
      }
    },
    $stateParams: {
      backTarget: 'asset.codicilsAndDefects'
    },
    $scope: {
      $on: (event, callback) => true
    },
    MastModalFactory: () => ({
      activate: () => true
    }),
    asset: {
      id: 1,
      ihsAsset: {
        id: 2
      }
    },
    coc: {
      id: 1
    },
    cocStatus: {
      toObject: () => []
    },
    codicilStatuses: [1, 2, 3, 4],
    job: {
      id: 1
    }
  };

  describe('Back Title', () => {
    beforeEach(async () => {
      angular.mock.module('angularMoment');

      angular.mock.module(services, ($provide) => {
        printService = jasmine.createSpyObj(
          'printService', ['stateName', 'pagesToPrint']);
        printService.stateName.and.returnValue('coc.state');
        printService.pagesToPrint.and.returnValue(1);

        codicilDecisionService = jasmine.createSpyObj(
          'codicilDecisionService', ['get']);
        codicilDecisionService.get.and.returnValue({});

        navigationService = jasmine.createSpyObj(
          'navigationService', ['backState']);
        navigationService.backState.and.returnValue(backStringValue);

        employeeDecisionService = jasmine.createSpyObj(
          'employeeDecisionService', ['get']);
        employeeDecisionService.get.and.returnValue(() => backStringValue);

        userProvider = jasmine.createSpyObj('user', ['get']);

        $provide.value('codicilDecisionService', codicilDecisionService);
        $provide.value('employeeDecisionService', employeeDecisionService);
        $provide.value('printService', printService);
        $provide.value('navigationService', navigationService);
        $provide.value('user', userProvider);
      });

      inject((_$controller_, _moment_) => {
        $controller = _$controller_;
        CocInjectionObject.moment = _moment_;
      });
    });

    it('Returns Codicils and Defects when back target is asset.codicilsAndDefects', async () => {
      backStringValue.backTitle = 'Back to Codicils and defects';
      vm = $controller(ViewCocController, CocInjectionObject);
      expect(vm.header.backString).toBe('Back to Codicils and defects');
    });

    it('Returns Back to Job Cocs when back target is crediting.cocs.list', async () => {
      backStringValue.backTitle = 'Back to job cocs';
      vm = $controller(ViewCocController, CocInjectionObject);
      expect(vm.header.backString).toBe('Back to job cocs');
    });

    it('Returns Back to Job Scoping when back target is asset.jobs.edit-scope', async () => {
      backStringValue.backTitle = 'Back to job scoping';
      vm = $controller(ViewCocController, CocInjectionObject);
      expect(vm.header.backString).toBe('Back to job scoping');
    });

    it('Returns Back to My Defect when back target is crediting.defects.list and defect is named my defect', async () => {
      backStringValue.name = 'crediting.defects.view';
      CocInjectionObject.coc.defect = {
        title: 'My Defect'
      };
      vm = $controller(ViewCocController, CocInjectionObject);
      expect(vm.header.backString).toBe('Back to My Defect');
    });
  });
});
