import * as _ from 'lodash';

/*  date.spec.js */
import { hasFormat } from '../../../src/app/utils/date';
import 'angular-moment';

describe('dateUtils - utility module', () => {
  beforeEach(() =>
    angular.mock.module('angularMoment'));

  describe('hasFormat()', () => {
    const tests = [
      { input: '', format: '', expectedResult: true },
      { input: '(anything @2Z.!)', format: '', expectedResult: true },
      { input: '28 Feb 2017', format: 'DD MMM YYYY', expectedResult: true },
      { input: '3 Feb 2017', format: 'DD MMM YYYY', expectedResult: false },
      { input: '03 Feb 2017', format: 'DD MMM YYYY', expectedResult: true },
      { input: '03 Feb 2017 12:34+01:00', format: 'DD MMM YYYY',
      expectedResult: true },
      { input: '3 Feb 2017', format: 'DD MMM YYYY hh:mm', expectedResult: false }
    ];

    _.forEach(tests, (item) => {
      const expectedOutcome = item.expectedResult ? 'pass' : 'fail';
      it('Should ' + expectedOutcome + ' "' + item.input +
        '" against format "' + item.format + '".', () =>
        expect(hasFormat(item.input, item.format)).toEqual(
          item.expectedResult));
    });
  });
});
