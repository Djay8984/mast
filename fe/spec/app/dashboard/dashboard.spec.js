import 'spec/spec-helper';
import dashboard from 'app/dashboard';

xdescribe('Dashboard', () => {
  let $compile;
  let $injector;
  let $rootScope;
  let $state;
  let $view;
  let apiService;
  let assetService;
  let employeeService;
  let myWorkService;
  let node;

  beforeEach(() => {
    angular.mock.module('restangular', dashboard, ($provide, $stateProvider) => {
      apiService = jasmine.createSpyObj('apiService', ['getAll']);
      apiService.getAll.and.returnValue({});

      assetService = jasmine.createSpyObj('assetService', ['getAll']);
      assetService.getAll.and.returnValue({});

      employeeService =
        jasmine.createSpyObj('employeeService', ['getCurrentUser']);
      employeeService.getCurrentUser.and.returnValue({});

      myWorkService =
        jasmine.createSpyObj('myWorkService', ['getForCurrentUser']);
      myWorkService.getForCurrentUser.and.returnValue({});

      node =
        jasmine.createSpyObj('node',
          ['getUser', 'getGroups', 'getFriendlyUserName']);
      node.getUser.and.returnValue({});
      node.getGroups.and.returnValue({});
      node.getFriendlyUserName.and.returnValue({});

      // dashboard state has a parent of 'main'
      $stateProvider.state('main', { template: '<ui-view />' });

      $provide.value('assetService', assetService);
      $provide.value('employeeService', employeeService);
      $provide.value('myWorkService', myWorkService);
      $provide.value('node', node);
    });

    inject((_$injector_, _$rootScope_, _$state_, _$compile_, _$controller_) => {
      $compile = _$compile_;
      $injector = _$injector_;
      $rootScope = _$rootScope_;
      $state = _$state_;

      $view = $compile('<div><ui-view /></div>')($rootScope);
    });
  });

  ['cases', 'jobs'].forEach((userType) => {
    describe(`${userType} user`, () => {
      beforeEach(() => {
        employeeService.getCurrentUser.and.returnValue({
          then: (fn) => fn({
            id: 1, groups: ['Admin'], workItemType: userType
          })
        });
      });

      describe('with work assigned', () => {
        beforeEach(() => {
          myWorkService.getForCurrentUser.and.returnValue([{ cases: [] }]);
        });

        it('redirects to My work', () => {
          $state.go('dashboard');
          $rootScope.$digest();

          expect($state.current.name).toBe('dashboard.userHub');
        });
      });

      describe('with no work assigned', () => {
        beforeEach(() => {
          myWorkService.getForCurrentUser.and.returnValue([]);

          $state.go('dashboard');
          $rootScope.$digest();
        });

        it('redirects to All Assets', () => {
          expect($state.current.name).toBe('dashboard.assetList');
        });

        it('can still navigate to My work', () => {
          const link = $view.find('a[data-ui-sref="dashboard.userHub"]');

          expect(link.prop('href')).toContain('#/dashboard/userhub');
          expect(link.text()).toBe('My work');
        });

        describe('visiting the My work tab', () => {
          beforeEach(() => {
            $state.go('dashboard.userHub');
            $rootScope.$digest();
          });

          it(`shows the user they are not assigned to any ${userType}`, () => {
            const content = $view.find('div.grid-block p[data-ng-if="!vm.filtered"]');

            expect(content.text().trim()).toBe(`You are not currently assigned to any ${userType}.`);
          });
        });
      });
    });
  });

  Object.entries({ 'My work': 'userHub', 'All assets': 'assetList' }).forEach(([ title, state ]) => {
    describe(`The ${title} tab`, () => {
      beforeEach(() => {
        myWorkService.getForCurrentUser.and.returnValue([{ cases: [] }]);
        employeeService.getCurrentUser.and.returnValue({
          then: (fn) => fn({
            id: 1, groups: ['Admin'], workItemType: 'cases'
          })
        });

        $state.go(`dashboard.${state}`);
        $rootScope.$digest();
      });

      describe('header items', () => {
        it('contain a link to \'My work\'', () => {
          const element = $view.find('a[data-ui-sref="dashboard.userHub"]');

          expect(element.prop('href')).toContain('#/dashboard/userhub');
          expect(element.text()).toBe('My work');
        });

        it('contain a link to \'All assets\'', () => {
          const element = $view.find('a[data-ui-sref="dashboard.assetList"]');

          expect(element.prop('href')).toContain('#/dashboard/assetlist');
          expect(element.text()).toBe('All assets');
        });

        it('contain an option to search for assets', () => {
          const element = $view.find('span[data-ng-click="vm.filterOpen = !vm.filterOpen"]');

          expect(element.text().trim()).toBe('Search / Filter assets');
        });

        describe('for cases users', () => {
          beforeEach(() => {
            employeeService.getCurrentUser.and.returnValue({
              then: (fn) => fn({
                id: 1, groups: ['Admin'], workItemType: 'cases'
              })
            });
            $state.go(`dashboard.${state}`, {}, { reload: true });
            $rootScope.$digest();
          });

          it('contain an option to navigate to batch actions', () => {
            const batch = $view.find('.actions a:first-child');

            expect(batch.text().trim()).toBe('Batch actions');
          });

          it('contain an option to add a new asset', () => {
            const add = $view.find('.actions a:last-child');

            expect(add.text().trim()).toBe('Add new asset');
          });
        });

        describe('for jobs users', () => {
          beforeEach(() => {
            employeeService.getCurrentUser.and.returnValue({
              then: (fn) => fn({
                id: 1, groups: ['Admin'], workItemType: 'jobs'
              })
            });
            $state.go(`dashboard.${state}`, {}, { reload: true });
            $rootScope.$digest();
          });

          it('do not contain an option to navigate to batch actions', () => {
            const batch = $view.find('.actions a:first-child');

            expect(batch.length).toBe(0);
          });

          it('do not contain an option to add a new asset', () => {
            const add = $view.find('.actions a:last-child');

            expect(add.length).toBe(0);
          });
        });
      });
    });
  });
});
