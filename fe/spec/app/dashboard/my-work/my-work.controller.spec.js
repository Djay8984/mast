import 'spec/spec-helper';
import 'angular-moment';

import Asset from 'app/models/asset.model';
import services from 'app/services';

import MyWorkController from 'app/dashboard/my-work/my-work.controller';

describe('"My Work" Tab controller', () => {
  let vm;
  let $rootScope;
  let $controller;
  let $stateMock;
  let caseMock;
  let $stateParamsMock;
  let jobMock;
  let jobStatusMock;
  let offlinular;
  let MastModalFactoryMock;
  let officeRoleMock;
  let surveyorRoleMock;
  let assetCaseMock;
  let myWorkMock;
  let myWorkFiltersDefaultMock;
  let myCasesMock;
  let myJobsMock;
  let teamJobsMock;
  let teamCasesMock;
  let teamStateMock;
  let userMock;
  let assetDecisionService;
  let caseService;
  let buildServices;
  let buildInjections;

  beforeEach(async () => {
    angular.mock.module('angularMoment');

    buildServices = () => {
      angular.mock.module(services, ($provide) => {
        // Asset Decision Service;
        assetDecisionService =
          jasmine.createSpyObj('assetDecisionService', ['get']);
        assetDecisionService.get.and.returnValue({});
        $provide.value('assetDecisionService', assetDecisionService);

        caseService = jasmine.createSpyObj('caseService', ['isOpenStatus']);
        caseService.isOpenStatus.and.returnValue({});
        $provide.value('caseService', caseService);
      });
    };
    buildInjections = () => {
      inject((_$controller_, _$rootScope_) => {
        $controller = _$controller_;
        $rootScope = _$rootScope_;

        $stateMock = jasmine.createSpyObj('$state', ['go', 'reload']);
        $stateMock.go.and.returnValue({});
        $stateMock.reload.and.returnValue({});

        $stateParamsMock = {
          id: 1,
          jobId: 1,
          assetId: 1,
          sortBy: 'A-Z'
        };

        assetCaseMock = {
          id: 2
        };

        jobMock = {
          id: 11,
          asset: {
            id: 12
          },
          jobStatus: {
            id: 1
          },
          employees: [
            {
              id: 1,
              lrEmployee: {
                id: 1,
                name: 'Jing Yu'
              },
              employeeRole: {
                id: 11
              }
            }
          ],
          model: {
            id: 587
          }
        };

        caseMock = {
          id: 16,
          surveyors: [
            {
              id: 17,
              employeeRole: {
                id: 3
              },
              surveyor: {
                id: 1,
                name: 'Jing Yu'
              }
            }
          ],
          offices: [
            {
              id: 18,
              officeRole: {
                id: 14
              },
              office: {
                id: 19,
                name: 'Antwerp'
              }
            }
          ]
        };

        // Mock up jobStatus;
        const jobStatuses = {
          sdoAssigned: 1,
          resourceAssigned: 2,
          underSurvey: 3,
          underReporting: 4
        };
        jobStatusMock = {
          toObject: () => jobStatuses,
          get: (field) => jobStatuses[field]
        };

        MastModalFactoryMock = {
          activate: () => true
        };

        // Mock up user;
        userMock = {
          fullName: 'Jing Yu',
          workItemType: 'jobs'
        };

        // Mock up surveyorRole;
        const surveyorRoles = {
          businessSupport: 1,
          surveyor: 2,
          management: 3,
          salesCRM: 4,
          specialistLeadership: 5,
          authorisingSurveyor: 6,
          eicCaseSurveyor: 7,
          eicAdmin: 8,
          sdo: 9,
          eicManager: 10,
          leadSurveyor: 11,
          sdoCoordinator: 12
        };
        surveyorRoleMock = {
          toObject: () => surveyorRoles,
          get: (field) => surveyorRoles[field]
        };

        // Mock up officeRole;
        const officeRoles = {
          sdo: 1,
          cfo: 2,
          cfo3: 3,
          mmsSdo: 4,
          mmso: 5,
          tso: 6,
          tsoStat: 7,
          classGroup: 8,
          dce: 9,
          eic: 10,
          mds: 11,
          tsdo: 12,
          tssao: 13,
          caseSdo: 14,
          jobSdo: 15
        };
        officeRoleMock = {
          toObject: () => officeRoles,
          get: (field) => officeRoles[field]
        };

        userMock = {
          id: 1
        };

        myJobsMock = [
          {
            id: 1
          }
        ];

        teamJobsMock = [
          {
            id: 2
          }
        ];

        myCasesMock = [
          {
            id: 3
          }
        ];

        teamCasesMock = [
          {
            id: 4
          }
        ];

        teamStateMock = {
          team: false
        };

        myWorkMock = {};
        myWorkFiltersDefaultMock = {};

        vm = $controller(MyWorkController,
          {
            $rootScope,
            $state: $stateMock,
            $stateParams: $stateParamsMock,
            assetDecisionService,
            jobStatus: jobStatusMock,
            offlinular,
            MastModalFactory: MastModalFactoryMock,
            officeRole: officeRoleMock,
            surveyorRole: surveyorRoleMock,
            assetCase: assetCaseMock,
            myWork: myWorkMock,
            myWorkFiltersDefault: myWorkFiltersDefaultMock,
            myCases: myCasesMock,
            myJobs: myJobsMock,
            teamJobs: teamJobsMock,
            teamCases: teamCasesMock,
            user: userMock
          },
          {
            _case: caseMock,
            _job: jobMock,
            _myJobs: [{ id: 1, asset: { id: 2 } }],
            _myCases: [{ id: 3, asset: { id: 3 } }],
            _selectedItems: [],
            _teamState: teamStateMock,
            expandedAsset: null,
            expandedAssetType: null,
            expandedAssetIndex: null
          });
      });
    };

    buildServices();
    buildInjections();
  });

  const methods = ['setUpData', 'jobStatus', 'jobSurveyor', 'jobView',
    'teamState', 'setTeamState', 'displayState', 'updateStateParams',
    'reload', 'addAssetToItem', 'addCountstoAssets', 'showAmounts', 'caseView',
    'caseSurveyor', 'caseOffice', 'isCurrentExpandedCard', 'hideAssetCard',
    'showAssetCard', 'addNameToItem'];

  /* eslint no-loop-func: 0 */
  for (let i = 0; i < methods.length; i += 1) {
    it(`should have a "${methods[i]}" method`, async () => {
      expect(`$rootScope.${methods[i]}`).toBeDefined();
    });
  }

  const getters = ['myJobs', 'teamJobs', 'myCases', 'teamCases',
    'selectedItems', 'assets', 'workItemType', 'filtered', 'casePaddingOffset',
    'sortOrder'];

  for (let i = 0; i < getters.length; i += 1) {
    it(`should have a "${getters[i]}" getter`, async () => {
      expect(`$rootScope.${getters[i]}`).toBeDefined();
    });
  }

  const privates = ['_itemAsset'];

  for (let i = 0; i < privates.length; i += 1) {
    it(`should have a "${privates[i]}" private method`, async () => {
      expect(`$rootScope.${privates[i]}`).toBeDefined();
    });
  }

  describe('on controller creation', () => {
    it('"setUpData" is called', async () => {
      spyOn(vm, 'addAssetToItem').and.callThrough();
      await vm.setUpData();
      expect(vm._teamState).toEqual({ team: false });
      vm._$stateParams.dashboard = { team: true };
      await vm.setUpData();
      expect(vm._teamState).toEqual({ team: true });
      expect(vm.addAssetToItem).toHaveBeenCalled();
    });

    it('"addAssetToItem" calls "addNameToItem"', async () => {
      vm._myWork = [
        {
          id: 1,
          jobs: [
            {
              id: 2
            }
          ]
        },
        {
          id: 2,
          jobs: [
            {
              id: 1
            }
          ]
        }
      ];
      spyOn(vm, 'addNameToItem').and.callThrough();
      await vm.addAssetToItem();
      expect(vm.addNameToItem).toHaveBeenCalled();
      expect(vm.addNameToItem.calls.count()).toEqual(4);
    });
  });

  describe('methods should return correctly', () => {
    it('getter "myJobs" should return an array', async () => {
      const jobs = await vm.myJobs;
      expect(angular.isArray(jobs)).toBe(true);
    });

    it('getter "teamJobs" should return an array', async () => {
      const jobs = await vm.teamJobs;
      expect(angular.isArray(jobs)).toBe(true);
    });

    it('getter "myCases" should return an array', async () => {
      const cases = await vm.myCases;
      expect(angular.isArray(cases)).toBe(true);
    });

    it('getter "teamCases" should return an array', async () => {
      const cases = await vm.teamCases;
      expect(angular.isArray(cases)).toBe(true);
    });

    it('getter "selectedItems" should return an array', async () => {
      const items = await vm.selectedItems;
      expect(angular.isArray(items)).toBe(true);
    });

    it('getter "filtered" should return a boolean', async () => {
      const f = await vm.filtered;
      expect(f).toMatch(/true|false/);
    });

    it('getter "sortOrder" should return a string', async () => {
      const so = await vm.sortOrder;
      expect(angular.isString(so)).toBe(true);
    });

    it('private method "_itemAsset" should get an asset', async () => {
      await vm._itemAsset(1);
      expect(assetDecisionService.get).toHaveBeenCalledWith(1);
    });

    it('"reload" should reload the page', async () => {
      await vm.reload();
      expect($stateMock.reload).toHaveBeenCalled();
    });

    it('"caseSurveyor" should return a string', async () => {
      const surveyor = await vm.caseSurveyor('management', vm._case);
      expect(angular.isString(surveyor)).toBe(true);
    });

    it('"showAmounts" should return a string', async () => {
      const amount = await vm.showAmounts('jobs');
      expect(angular.isString(amount)).toBe(true);
    });
  });

  describe('helper functions should work correctly', () => {
    it('"jobStatus" should return the correct job status', async () => {
      let status = await vm.jobStatus(vm._job);
      expect(status).toMatch(/sdoAssigned|null/);
      jobMock.jobStatus.id = 20;
      status = await vm.jobStatus(vm._job);
      expect(status).toEqual(null);
    });

    it('"jobSurveyor" should return the name of the surveyor', async () => {
      const surveyor = await vm.jobSurveyor(vm._job);
      expect(surveyor).toEqual('Jing Yu');
    });

    it('"updateStateParams" should update the $stateParams', async () => {
      await vm.updateStateParams({ foo: 'bar' });
      expect(_.includes(_.keys(vm._$stateParams), 'foo')).toBe(true);
    });
  });

  describe('"state" (my/team) methods', () => {
    it('"teamState" should return a boolean', async () => {
      const ts = await vm.teamState('team');
      expect(ts).toEqual(false);
    });

    it('"setTeamState" should set the team state correctly', async () => {
      spyOn(vm, 'updateStateParams').and.callFake(() => true);
      spyOn(vm, 'displayState').and.callFake(() => true);
      await vm.setTeamState();
      expect(vm._teamState).toEqual({ team: true });
      expect(vm.updateStateParams).toHaveBeenCalledWith({ dashboard: vm._teamState });
      expect(vm.displayState).toHaveBeenCalled();
    });

    it('getter "currentState" should return the correct state', async () => {
      expect(await vm.currentState).toEqual(vm._teamState);
    });

    it('"displayState" should understand the team', async () => {
      vm._selectedItems = [];
      await vm.displayState();
      expect(_.size(vm._selectedItems)).toBe(2);
    });
  });

  describe('viewing the job from this page', () => {
    it('"jobView" should go to asset.jobs.view', async () => {
      const asset = { id: 123 };
      await vm.jobView(vm._job, null);
      expect($stateMock.go).toHaveBeenCalledWith('asset.jobs.view',
        { jobId: vm._job.model.id }, { reload: true });
      await vm.jobView(vm._job, asset);
      expect($stateMock.go).toHaveBeenCalledWith('asset.jobs.view',
        { jobId: vm._job.model.id, assetId: asset.id }, { reload: true });
    });
  });

  describe('case-specific methods', () => {
    it('"caseSurveyor" should correctly return a name', async () => {
      const surveyor = await vm.caseSurveyor('management', vm._case);
      expect(angular.isString(surveyor)).toBe(true);
    });

    it('"caseOffice" should correctly return a name', async () => {
      const office = await vm.caseOffice('caseSdo', vm._case);
      expect(angular.isString(office)).toBe(true);
    });
  });

  describe('cards', () => {
    it('"isCurrentExpandedCard" should return a boolean', async () => {
      const result = await vm.isCurrentExpandedCard(1, 'job', 1);
      expect(result).toMatch(/true|false/);
    });

    it('"hideAssetCard" should hide the current asset card', async () => {
      await vm.hideAssetCard();
      expect(vm.expandedAsset).toEqual(null);
      expect(vm.expandedAssetType).toEqual(null);
      expect(vm.expandedAssetIndex).toEqual(null);
    });

    it('"showAssetCard" should return a string', async () => {
      await vm.showAssetCard(1, 'job', 1);
      expect(vm.expandedAssetType).toEqual('job');
      expect(vm.expandedAssetIndex).toEqual(1);
    });
  });
});
