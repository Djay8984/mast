import 'spec/spec-helper';
import example from 'app/example';

describe('Example directive', () => {
  let $element, $rootScope;

  beforeEach(() => {
    angular.mock.module(example);

    inject((_$rootScope_, $compile) => {
      $rootScope = _$rootScope_;
      $element = $compile('<example></example>')($rootScope);

      $rootScope.$digest();
    });
  });

  it('says "Example"', () => {
    expect($element.find('a').html()).toContain('Example');
  });
});

