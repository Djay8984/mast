import 'spec/spec-helper';
import ExampleController from 'app/example/directive/example.controller';

describe('ExampleController', () => {
  let $rootScope, controller;

  beforeEach(() => {
    angular.mock.module(($provide) => {
      $provide.value('config', jasmine.createSpyObj('config', ['get']));
    });

    inject((_$rootScope_) => {
      $rootScope = _$rootScope_;
      controller = new ExampleController();
    });
  });

  it('says "Example"', () => {
    $rootScope.$digest();

    expect(controller.example).toEqual('Example');
  });
});
