module.exports = {
  'env': {
    'jasmine': true
  },
  'globals': {
    '_': false,
    'angular': false,
    'inject': false
  },
  'rules': {
    'babel/new-cap': 0, // hotkeys needs strings based on accelerator names
    'init-declarations': 0,
    'max-len': 0,
    'max-nested-callbacks': 0,
    'new-cap': 0,
    'one-var': 0
  }
};
