window.process = window.process || window.top.process;
window.require = window.require || window.top.require;

const testsContext = require.context('.', true, /\.spec\.js$/);
addAll(testsContext);

require('../src/app/services/index.js');

function addAll(context) {
  context.keys().forEach(context);
}
