import defectCategoryFixture from 'spec/fixtures/defect-categories.json';
import defectDetailFixture from 'spec/fixtures/defect-details.json';

export default class DefectHelper {
  constructor() { }

  buildDependencyInjectionModel(moment) {
    let assetModelFixture;
    let mastModalMock;
    let confidentialityTypesFixture;
    let defectFixture;
    let assetFixture;
    let jobFixture;
    let surveyTypeFixture;
    let defectCategories;
    let defectDetails;

    assetModelFixture = {};

    mastModalMock = () => ({
      activate: () => true
    });

    confidentialityTypesFixture = [
      {
        id: 1,
        description: 'test1'
      },
      {
        id: 2,
        description: 'test2'
      }
    ];

    defectFixture = { };

    assetFixture = {
      id: 1,
      ihsAsset: {
        id: 2
      }
    };

    jobFixture = {
      id: 1
    };

    const surveyTypes = {
      damage: 1,
      repair: 2
    };
    surveyTypeFixture = {
      get: (field) => surveyTypes[field],
      toObject: () => surveyTypes
    };

    defectCategories = angular.fromJson(defectCategoryFixture);

    defectDetails = angular.fromJson(defectDetailFixture);

    return {
      moment,
      assetModel: assetModelFixture,
      MastModalFactory: mastModalMock,
      confidentialityTypes: confidentialityTypesFixture,
      defect: defectFixture,
      asset: assetFixture,
      job: jobFixture,
      surveyType: surveyTypeFixture,
      refDefectCategories: defectCategories,
      refDefectDetails: defectDetails
    };
  }
}
