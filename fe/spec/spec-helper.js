window.global = window.top;
window.process = window.process || window.top.process;
window.require = window.require || window.top.require;
window.__dirname = window.top.__dirname;

const path = require('path');
const cwd = require('process').cwd();
const pathTo = (...args) => path.join(...([cwd].concat(args)));

// support Restangular's old version of lodash
require('lodash');
window._.contains = window._.includes;

// This makes require('module_name') work...
require('module').globalPaths.push(pathTo('node_modules'));

// ...and this makes require('./relative/path/required') work.
// NOTE: the file itself isn't important, it's just there to get
//       the correct directory from basename()
window.top.module.filename = pathTo('package.json');

// As main.js isn't included when running the specs, set initial
// globals here instead of having to stub/mock e.g. the global
// config in every spec.
const _req = window.require;
const globals = {
  config: {},
  spellChecker: {
    setDictionary: () => null,
    isMisspelled: () => true,
    getCorrectionsForMisspelling: () => []
  },
  buildEditorContextMenu: () => ({
    popup: () => null
  })
};

// remote mocking
window.require = (mod) => {
  if (mod !== 'remote') {
    return _req(mod);
  }

  const { remote } = _req('electron');
  const getGlobal = remote.getGlobal;

  remote.getGlobal = (arg) =>
    globals.hasOwnProperty(arg) ? globals[arg] : getGlobal(arg);

  return remote;
};

(function installAsyncHooks() {
  if (window.installedAsyncHooks) {
    return;
  }

  const P = Promise;
  P.qed = true;

  const isSpec = (fn) => ['fit', 'it'].includes(fn);

  ['fit', 'it', 'beforeEach', 'beforeAll', 'afterEach', 'afterAll'].forEach((fn) => {
    const origFn = window[fn];

    window[fn] = (...args) => {
      const [ title, block, timeout ] = isSpec(fn) ? args : [null, ...args];
      const maybePromised = (done) => {
        const result = block(done);

        if (result && result.then) {
          return result.then(done, done.fail);
        }

        done();
      };
      const newArgs = isSpec(fn) ? [title, maybePromised] : [maybePromised, timeout];

      return origFn(...newArgs);
    };
  });

  window.installedAsyncHooks = true;
}());

// Because there's no jQuery and jqLite doesn't support finding with selectors
angular.element.prototype.find = function _find(...args) {
  return angular.element(this[0].querySelectorAll(...args));
};
