const webpack = require('webpack');
const baseConfig = require('./webpack.production.settings.js');
const config = Object.create(baseConfig);

config.bail = true;

config.eslint = {
  failOnError: true,
  failOnWarning: true
};

config.output.publicPath = '';


config.plugins = config.plugins || [];
config.plugins.push(
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
      screw_ie8: true,
      keep_fnames: true
    },
    mangle: false
  })
);

module.exports = config;
