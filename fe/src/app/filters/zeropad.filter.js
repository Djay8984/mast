import * as _ from 'lodash';

export default () => (input = '', zeroes = 0) => _.padStart(input, zeroes, '0');
