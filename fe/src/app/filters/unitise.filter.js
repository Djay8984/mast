/* @ngInject */
export default () =>
  (input, singular, plural) => {
    const num = Number(input);
    if (Number.isNaN(num)) {
      return input; // leave unchanged
    }

    let unit = '';
    if (num === 1) {
      unit = singular;
    } else {
      // Append s to singular string if plural not given
      unit = plural ? plural : `${singular}s`;
    }
    return `${input} ${unit}`;
  };
