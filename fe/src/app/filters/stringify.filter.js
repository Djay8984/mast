import * as _ from 'lodash';

/* @ngInject */
export default () =>
   (input) => _.toString(input);

