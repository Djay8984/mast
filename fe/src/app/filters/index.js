import * as angular from 'angular';

import ellipsise from './ellipsise.filter';
import capitalise from './capitalise.filter';
import integerComparator from './integer-comparator.filter';
import kebabise from './kebabise.filter';
import moment from './moment.filter';
import singularise from './singularise.filter';
import stringify from './stringify.filter';
import unitise from './unitise.filter';
import zeropad from './zeropad.filter';

export default angular
  .module('app.filters', [])
  .filter('capitalise', capitalise)
  .filter('ellipsise', ellipsise)
  .filter('integerComparator', integerComparator)
  .filter('kebabise', kebabise)
  .filter('moment', moment)
  .filter('singularise', singularise)
  .filter('stringify', stringify)
  .filter('unitise', unitise)
  .filter('zeropad', zeropad)
  .name;
