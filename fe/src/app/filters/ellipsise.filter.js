import * as _ from 'lodash';

/* @ngInject */
export default () =>
   (input, length) => _.size(input) > length ?
     `${input.slice(0, length)}…` :
     input;
