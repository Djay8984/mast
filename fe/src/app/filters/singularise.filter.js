/* @ngInject */
export default () =>
  (string, length) => length !== 1 ?
    string :
    string.replace(/s$/, '');
