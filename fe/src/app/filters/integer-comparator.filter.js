import * as _ from 'lodash';

export default () =>
  (actual, expected) =>
    _.parseInt(actual) === _.parseInt(expected);
