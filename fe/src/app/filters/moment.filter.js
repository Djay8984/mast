/* @ngInject */
export default (
  moment,
  dateFormat
) =>
  (input, defaultValue, format) =>
    moment(input).isValid() ?
      moment(input).format(format || dateFormat) : defaultValue;
