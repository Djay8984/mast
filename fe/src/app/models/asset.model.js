import * as _ from 'lodash';

import Model from './model.class';
import Case from './case.model';
import Customer from './customer.model';
import Job from './job.model';
import Office from './office.model';

export default class Asset extends Model {
  get cases() {
    if (!this._cases && this.model.cases) {
      this._cases = this.model.cases.map((_case) => new Case(_case));
    }

    return this._cases || [];
  }

  set cases(cases) {
    this._cases = cases;
    this.model.cases = cases;
  }

  get jobs() {
    if (!this._jobs && this.model.jobs) {
      this._jobs = this.model.jobs.map((job) => new Job(job));
    }

    return this._jobs || [];
  }

  set jobs(jobs) {
    this._jobs = jobs;
    this.model.jobs = jobs;
  }

  get type() {
    // What's the real map?
    switch (this.assetType) {
      case 'Tankers':
        return 'tanker';
      case 'Non Ship Structures':

        // ?!
        return 'buoy';
      default:
        return 'bulk-carrier';
    }
  }

  get customers() {
    if (!this._customers && this.model.customers) {
      this._customers = _.map(this.model.customers, (customer) =>
          Reflect.construct(Customer, [customer]));
    }

    return this._customers;
  }

  get buildDate() {
    if (!this._buildDate && this.model.buildDate) {
      this._buildDate = this.model.buildDate;
    }

    return this._buildDate;
  }

  get imo() {
    if (!this._imo) {
      this._imo = _.get(this, 'model.ihsAsset.id') || this.model.id;
    }

    return this._imo;
  }

  get flag() {
    if (!this._flag) {
      this._flag = this.model.proposedFlagState;
    }

    return this._flag;
  }

  get category() {
    if (!this._category) {
      this._category = this.model.assetCategory;
    }

    return this._category;
  }

  get isManagementSystemAsset() {
    if (!this._isManagementSystemAsset) {
      this._isManagementSystemAsset = this.category === 'MSA';
    }

    return this._isManagementSystemAsset;
  }

  get offices() {
    if (!this._offices && this.model.offices) {
      this._offices = _.map(this.model.offices, (office) =>
          Reflect.construct(Office, [office]));
    }

    return this._offices;
  }

  get ruleSet() {
    if (!this._ruleSet) {
      this._ruleSet = this.model.ruleSet;
    }

    return this._ruleSet;
  }

  get isImoRegistered() {
    return !_.isUndefined(_.get(this, 'ihsAsset.id'));
  }

  get sdo() {
    return this.officeByRole('SDO');
  }

  get cfo() {
    return this.officeByRole('CFO');
  }

  get classGroup() {
    return this.officeByRole('Class Group');
  }

  officeByRole(role) {
    const result = _.find(this.offices, ['officeRole.name', role]);
    return _.get(result, 'office.name') || '-';
  }
}
