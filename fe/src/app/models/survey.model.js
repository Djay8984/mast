import * as _ from 'lodash';

import Model from './model.class';
import Task from './task.model';
import Item from './item.model';

export default class Survey extends Model {
  constructor(survey, crediting, taskCreditStatuses) {
    super(survey);
    this._crediting = crediting;
    this._taskCreditStatuses = taskCreditStatuses;

    this.initTree();
  }

  initTree() {
    // Recursive lambda which wraps all items in the tree with an Item object
    // while preserving the original reference to the item
    const findItems = (currentItem) => {
      _.forEach(_.get(currentItem, 'tasks'), (task, index) =>
          _.set(currentItem.tasks, index,
            new Task(task, this._taskCreditStatuses)));

      if (_.get(currentItem, 'items')) {
        _.forEach(currentItem.items, (item, index) =>
          _.set(currentItem.items, index, new Item(item)));

        _.forEach(currentItem.items, (item) => findItems(item));
      }
    };

    findItems(this._crediting);
  }

  get actionTakenDate() {
    return this.model.actionTakenDate;
  }

  set actionTakenDate(actionTakenDate) {
    this.model.actionTakenDate = actionTakenDate;
  }

  get creditedBy() {
    return this.model.creditedBy;
  }

  set creditedBy(employee) {
    this.model.creditedBy = employee;
    this._crediting.creditedBy = employee;
  }

  get crediting() {
    return this._crediting;
  }

  get dateOfCrediting() {
    return this.model.dateOfCrediting;
  }

  set dateOfCrediting(date) {
    this.model.dateOfCrediting = date;
    this._crediting.dateOfCrediting = date;
  }

  get id() {
    return this.model.id || this.model._id;
  }

  set id(id) {
    if (_.startsWith(id, '-')) {
      this.model._id = _.toString(id);
    } else {
      this.model.id = _.parseInt(id);
    }
  }

  /**
   * Generates a list of all Items in the crediting hierarchy and caches the
   * result for subsequent retrievals
   * @returns {object} the item list
   */
  get items() {
    if (!this.itemList) {
      this.itemList = [];
      const findItems = (currentItem) => {
        if (currentItem.items) {
          this.itemList = _.union(this.itemList, currentItem.items);
          _.forEach(currentItem.items, (item) => findItems(item));
        }
      };

      findItems(this._crediting);
    }

    return this.itemList;
  }

  get narrative() {
    return this.model.narrative;
  }

  set narrative(narrative) {
    this.model.narrative = narrative;
  }

  get requiresFollowUp() {
    return this.model.actionRequired;
  }

  set requiresFollowUp(actionRequired) {
    this.model.actionRequired = actionRequired;
  }

  /**
   * Generates a list of all Tasks in the crediting hierarchy and caches the
   * result for subsequent retrievals
   * @returns {object} the task list
   */
  get tasks() {
    if (!this.taskList) {
      this.taskList = [];

      // recursive lambda which goes through the items tree extracting the tasks
      // takes a current path which it appends to find its way deeper into the
      // tree
      const findTasks = (currentPath) => {
        this.taskList = _.union(this.taskList,
          _.get(this._crediting, _.concat(currentPath, 'tasks'), []));
        currentPath.push('items');
        _.forEach(_.get(this._crediting, currentPath),
          (item, index) => findTasks(_.concat(currentPath, index)));
      };

      findTasks([]);
    }

    return this.taskList;
  }

  get surveyStatus() {
    return this.model.surveyStatus || this._crediting.surveyStatus;
  }

  set surveyStatus(status) {
    this.model.surveyStatus = status;
    this._crediting.surveyStatus = status;
  }
}
