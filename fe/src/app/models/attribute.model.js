import Model from './model.class';

export default class Attribute extends Model {

  set value(val) {
    this.model.value = val;
  }

  get value() {
    return this.model.value;
  }

}
