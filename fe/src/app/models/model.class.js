import * as _ from 'lodash';

export default class Model {
  /* eslint no-invalid-this: 0 */
  static from(list) {
    return _.forEach(list, (item, index) => {
      if (item.constructor !== this) {
        list[index] = new this(item);
      }
    });
  }

  constructor(model) {
    this.model = model;

    if (model) {
      Object.entries(model).forEach(([ key, value ]) => {
        if (!Reflect.has(this, key)) {
          Reflect.defineProperty(this,
            key,
            {
              get: () => model[key],
              enumerable: true
            });
        }
      });
    }
  }
}
