import * as _ from 'lodash';

import Model from './model.class';

export default class Customer extends Model {
  constructor(model) {
    super(model);
  }
  get functionNameList() {
    return _.map(this.functions, (func) =>
        _.get(func, 'customerFunction.name'));
  }
}
