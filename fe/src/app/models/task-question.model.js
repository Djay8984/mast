import * as _ from 'lodash';

import Model from './model.class';

export default class TaskQuestion extends Model {
  constructor(question) {
    super(question);
  }

  get attributeValueString() {
    return this.model.attributeValueString;
  }

  set attributeValueString(value) {
    this.model.attributeValueString = value;
  }

  /**
   * @return {boolean} whether or not the current answer is included in the list
   * of followOn trigger values
   */
  get showNextQuestion() {
    const triggerValues = _.map(
      _.filter(this.workItemConditionalAttribute.allowedAttributeValues,
        'isFollowOnTrigger'), 'value');

    return _.includes(triggerValues, this.attributeValueString);
  }
}
