import * as _ from 'lodash';

import Model from './model.class';

export default class ServiceModel extends Model {
  constructor(
    model,
    moment
  ) {
    super(model);
    this.moment = moment;
  }

  /**
   * When the current day is beyond the service's upperRangeDate it is
   * 'Overdue' and the useWarning flag is set.
   *
   * When the current day is between the service's lower and upper range dates
   * it is 'Due'
   *
   * @param {Object} service Service
   * @returns {Object} Status indicates current date in range or beyond it
   */
  get rangeStatus() {
    const now = this.moment();
    const lowerBound = this.moment.utc(this.effectiveLowerRangeDate);
    const upperBound = this.moment.utc(this.effectiveUpperRangeDate);

    const status = { name: null, useWarning: false };

    if (now.isAfter(upperBound)) {
      status.name = 'Overdue';
      status.useWarning = true;
    } else if (now.isBetween(lowerBound, upperBound, 'day', '(]')) {
      status.name = 'Due';
    }

    return status;
  }

  /**
   * @returns {String} Date
   */
  get effectiveAssignedDate() {
    return this.assignedDateManual || this.assignedDate;
  }

  /**
   * @returns {String} Date
   */
  get effectiveDueDate() {
    return this.dueDateManual || this.dueDate;
  }

  /**
   * @returns {String} Date
   */
  get effectiveLowerRangeDate() {
    return this.lowerRangeDateManual || this.lowerRangeDate;
  }

  /**
   * @returns {String} Date
   */
  get effectiveUpperRangeDate() {
    return this.upperRangeDateManual || this.upperRangeDate;
  }

  /**
   * Checks to see if both upper and lower range dates are null
   * @returns {Boolean} true if both are null
   */
  get effectiveRangeIsNull() {
    return this.isRangeDateNull && this.isRangeDateManualNull;
  }

  get isRangeDateNull() {
    return _.isNil(this.upperRangeDate) && _.isNil(this.lowerRangeDate);
  }

  get isRangeDateManualNull() {
    return _.isNil(this.upperRangeDateManual) &&
      _.isNil(this.lowerRangeDateManual);
  }

  /**
   * Provisional (service either has this flag or it doesn't)
   *
   * NB: Not to be confused with provisionalDates which indicates all the date
   *     fields on the service are provisional, not the service as a whole.
   *
   * Clarification:
   *   When the FAR is submitted with this service on it but the FSR is not
   *   yet submitted. The user when amending or adding a service can also
   *   select the provisional tag (as per wireframes or screenshots in my
   *   stories) and that way this will appear here.
   *
   * @returns {String} The provisional flag or an empty string.
   */
  get provisionalStatus() {
    return _.get(this.model, 'provisional') ? 'Provisional' : '';
  }

  get serviceCreditStatus() {
    return _.get(this.model, 'serviceCreditStatus');
  }

  set serviceCreditStatus(value) {
    _.set(this.model, 'serviceCreditStatus', value);
  }

  /**
   * @returns {String} The service type name or the empty string
   */
  get statutoryServiceType() {
    return _.get(this, 'serviceType.name', '');
  }

  /**
   * @todo Determine source of this kind of status
   *
   * @returns {String} unknown
   */
  get classificationServiceStatus() {
    return '';
  }

  /**
   * The model's lastCreditedJob is a job number as a string
   *
   * @returns {String} Job number
   */
  get lastCreditedOn() {
    return _.get(this, 'lastCreditedJob', '');
  }

  /**
   * The model's lastPartheldJob is a job number as a string
   *
   * @returns {String} Job number
   */
  get partHeldOn() {
    return _.get(this, 'lastPartheldJob', '');
  }

  /**
   * @returns {String} ISO format date string
   */
  get dateOnCompletion() {
    return _.get(this, 'completionDate', '');
  }

  set completionDate(value) {
    _.set(this.model, 'completionDate', value.format());
  }

  get completionDate() {
    return _.get(this.model, 'completionDate');
  }

  /**
   * @returns {object} the minimum due date
   */
  get minDueDate() {
    return this.assignedDateManual ?
      this.moment(this.assignedDateManual).add(1, 'day') :
        this.moment();
  }

  set dueDateManual(value) {
    this._dueDateManual = value;
  }

  get dueDateManual() {
    if (!this._dueDateManual && this.model.dueDateManual) {
      this._dueDateManual = this.moment(this.model.dueDateManual);
    }
    return this._dueDateManual;
  }

  get assignedDateManual() {
    if (!this._assignedDateManual && this.model.assignedDateManual) {
      this._assignedDateManual = this.moment(this.model.assignedDateManual);
    }
    return this._assignedDateManual;
  }

  set assignedDateManual(date) {
    if (_.isUndefined(date)) {
      Reflect.deleteProperty(this, '_assignedDateManual');
    } else {
      this._assignedDateManual = date;
    }
  }

  set assetItem(item) {
    this._assetItem = item;
    this.model.assetItem = _.pick(item, ['id', '_id']);
  }

  get assetItem() {
    return this._assetItem;
  }

  get minLowerRangeDate() {
    return this.assignedDateManual ?
      this.moment(this.assignedDateManual).add(1, 'day') :
        this.moment();
  }

  get maxLowerRangeDate() {
    return this.dueDateManual ?
      this.dueDateManual : this.moment().add(3, 'months');
  }

  get minUpperRangeDate() {
    return this.dueDateManual ?
      this.moment(this.dueDateManual) :
        this.moment();
  }

  set lowerRangeDateManual(value) {
    this._lowerRangeDateManual = value;
  }

  get lowerRangeDateManual() {
    if (!this._lowerRangeDateManual && this.model.lowerRangeDateManual) {
      this._lowerRangeDateManual = this.moment(this.model.lowerRangeDateManual);
    }
    return this._lowerRangeDateManual;
  }

  get upperRangeDateManual() {
    if (!this._upperRangeDateManual && this.model.upperRangeDateManual) {
      this._upperRangeDateManual = this.moment(this.model.upperRangeDateManual);
    }
    return this._upperRangeDateManual;
  }

  set upperRangeDateManual(value) {
    this._upperRangeDateManual = value;
  }

  set provisional(value) {
    this._provisional = value;
    this.model.provisional = value === 'true';
  }

  get provisional() {
    return this._provisional;
  }

  get cyclePeriodicity() {
    return this.model.cyclePeriodicity;
  }

  set cyclePeriodicity(value) {
    this.model.cyclePeriodicity = value;
  }

  get serviceStatus() {
    return this.model.serviceStatus;
  }

  set serviceStatus(value) {
    this.model.serviceStatus = Number(value);
  }

  get payload() {
    if (this._assignedDateManual) {
      this.model.assignedDateManual = this._assignedDateManual.toISOString();
    }

    if (this._dueDateManual) {
      this.model.dueDateManual = this._dueDateManual.toISOString();
    }

    if (this._upperRangeDateManual) {
      this.model.upperRangeDateManual = this._upperRangeDateManual
        .toISOString();
    }

    if (this._lowerRangeDateManual) {
      this.model.lowerRangeDateManual = this._lowerRangeDateManual
        .toISOString();
    }

    return this.model;
  }
}
