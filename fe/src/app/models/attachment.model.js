import * as _ from 'lodash';

import Model from './model.class';

export default class Attachment extends Model {

  isAttachment() {
    return !this.isNote();
  }

  isNote() {
    return _.isEmpty(this.model.attachmentUrl) &&
      !_.has(this, 'file');
  }

  get title() {
    return this.model.title || 'Note';
  }

  get creationDate() {
    return this.model.creationDate;
  }

  get creator() {
    return _.get(this, 'model.author.name');
  }

  get type() {
    return this.isNote() ? 'Note' : _.get(this, 'model.attachmentType.name');
  }

  get note() {
    return this.model.note;
  }

  set note(note) {
    this.model.note = note;
  }

  get showDownloadIcon() {
    return !_.isNil(this.model.attachmentUrl);
  }
}
