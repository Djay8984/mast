import Item from 'app/models/item.model';

export default class AssetModel extends Item {
  /* @ngInject */
  constructor(model, items) {
    super(model);

    if (items) {
      this.items = items;
    }
  }
}
