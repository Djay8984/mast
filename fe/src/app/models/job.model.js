import * as _ from 'lodash';

import Model from './model.class';
import Office from './office.model';

export default class Job extends Model {
  get offices() {
    if (!this._offices) {
      this._offices = this.model.offices.map((office) => new Office(office));
    }

    return this._offices || [];
  }

  get employees() {
    return this.model.employees;
  }

  set employees(employees) {
    this.model.employees = employees;
  }

  // TODO: use correct field from service when implemented
  get estimatedArrivalDate() {
    return this.serviceRequiredDate;
  }

  // TODO: use correct field from service when implemented
  get estimatedDepartureDate() {
    return this.serviceRequiredDate;
  }

  get jobStatus() {
    return this.model.jobStatus;
  }

  set jobStatus(status) {
    this.model.jobStatus = status;
  }

  get serviceType() {
    const getTypeName = (s) => _.get(s, 'serviceCatalogue.serviceType.name');
    const typeNames = _.map(this.model.surveys, getTypeName);
    return typeNames.join(', ');
  }

  get sdo() {
    const sdo = this.offices.find((office) => office.role.name === 'SDO');

    return sdo ? sdo.name : '-';
  }
}
