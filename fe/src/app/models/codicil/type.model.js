import * as _ from 'lodash';

import Model from '../model.class';

const CODICIL_SLUGS = {
  '1': 'coc',
  '2': 'actionable-item',
  '3': 'asset-note'
};

export default class CodicilType extends Model {
  /* @ngInject */
  constructor(model, categories) {
    super(model);

    if (_.size(categories)) {
      this.categories = categories;
    }

    this.slug = CODICIL_SLUGS[model._id] || 'defect';
  }

  set categories(categories) {
    this._categories = _.map(categories, (value) =>
      Reflect.construct(Model, [value]));
  }

  /**
   * @return {array} the categories associated with this type
   */
  get categories() {
    return this._categories;
  }

  /**
   * @return {boolean} true if any children are selected
   */
  get selected() {
    return _.some(this._categories, ['selected', true]);
  }

  /**
   * @param {boolean} value set selected property of all chidren to this
   */
  set selected(value) {
    _.forEach(this._categories, (category) =>
      _.set(category, 'selected', value));
  }

  /**
   * @return {array} an array of selected categories
   */
  get selectedCategories() {
    return _.filter(this._categories, 'selected');
  }

  /**
   * @param {array} list an array of category ids to select
   */
  selectCategories(list) {
    _.forEach(this._categories, (category) => {
      _.set(category, 'selected', _.includes(list, category.id));
    });
  }

  /**
   * @return {object} object containing id, categories
   */
  getSelectedCategories() {
    return _.assign({
      categories: _.flatMap(this.selectedCategories, 'id')
    }, _.pick(this, ['id', 'slug']));
  }
}
