import * as _ from 'lodash';

import Model from '../model.class';

export default class CodicilSearchFilter extends Model {
  /* @ngInject */
  constructor(model) {
    super(model);
  }

  getCategoriesBySlug(slug) {
    const type = _.find(this.typeList, ['slug', slug]);
    return _.get(type, 'categories');
  }
}
