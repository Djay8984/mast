import * as _ from 'lodash';

import Model from './model.class';

export default class Item extends Model {
  constructor(model) {
    super(model);

    this.childSelected = false;
    this.expanded = false;
    this.selected = false;
    this.model = this.model || {};
  }

  set displayOrder(value) {
    if (!_.isNumber(this.model.displayOrder)) {
      this.model.displayOrder = 0;
    }

    this.model.displayOrder = value;
  }

  get displayOrder() {
    return this.model.displayOrder;
  }

  getItemIds() {
    return _.uniq(_.map(this.items, 'id'));
  }

  set items(value) {
    this._items = _.map(value, (i) => {
      if (i instanceof Item) {
        return i;
      }

      const flatItem = new Item(i);
      flatItem.parent = this;
      return flatItem;
    });
  }

  get items() {
    return this._items || this.model.items;
  }

  select({ reset = true } = {}) {
    if (reset) {
      this.root.setChildProps({
        selected: false,
        childSelected: false
      });
    }
    this.selected = true;

    return this;
  }

  expand({ parents = true } = {}) {
    const isRootChild = _.find(this.root.items, this);

    this.expanded = !this.expanded;

    /* This only applies to the root items of a hierarchy layout so we can
       identify the highest indexed expanded item, and, if removed, the sets
       the expanded item to the (next) highest remaining expanded item; */
    if (!_.isUndefined(isRootChild)) {
      this.root.setChildProps({
        lastExpandedItem: false
      });

      const expandedItems = _.filter(this.root.items, {
        expanded: true
      });

      if (_.last(expandedItems) === this) {
        this.lastExpandedItem = true;
      } else {
        _.last(expandedItems).lastExpandedItem = true;
      }
    }

    if (parents) {
      this.setParentProps({ expanded: true });
    }

    return this;
  }

  /**
   * @param {object} props the properties to set on parent items
   */
  setParentProps(props) {
    if (!_.get(this, 'parent')) {
      return;
    }

    for (let item = this.parent; item; item = item.parent) {
      _.extend(item, props);
    }
  }

  findChildren(props) {
    const matches = [];
    const recurse = (item) => {
      if (_.isMatch(item, props)) {
        matches.push(item);
      }
      _.forEach(item.items, recurse);
    };
    recurse(this);
    return matches;
  }

  /**
   * @param {object} props the properties to set on child items
   */
  setChildProps(props) {
    const recurse = (item) => {
      _.extend(item, props);
      _.forEach(item.items, recurse);
    };
    recurse(this);
  }

  getChildIdsDeep() {
    const ids = [];
    const recurse = (item) => {
      ids.push(item.id);
      _.forEach(item.items, recurse);
    };
    recurse(this);

    return ids;
  }

  /**
   * @return {object} the top-level ancestor of this item
   */
  get root() {
    /* eslint consistent-this: 0 */
    let item = this;

    while (item.parent) {
      item = item.parent;
    }

    return item;
  }

  /**
   * @return {array} an array of parent objects
   */
  get parents() {
    const parents = [];
    let item = this;

    while (item.parent) {
      parents.push(item);
      item = item.parent;
    }

    parents.push(item);

    return parents;
  }

  /**
   * Given an array, traverses down the tree and hydrates child items
   * @param {array} items array of item objects
   * @return {object} this
   */
  populate(items) {
    let current = this;

    _.forEach(items, (item) => {
      const child = _.find(current.items, { id: item.id });
      child.items = item.items;
      current = child;
    });

    current.select().expand();
    return this;
  }

  /**
   * Calculates the overall task status of all tasks linked to this item and all
   * child items recursively and caches the result for faster retrieval on
   * subsequent calls.
   * @param {boolean} refreshCache whether or not to force a cache refresh
   * @returns {boolean} the overall task status
   */
  getTaskStatus(refreshCache = false) {
    if (_.isNil(this.taskStatus) || refreshCache) {
      this.taskStatus = true;

      if (this.items) {
        _.forEach(this.items, (item) => {
          // Needs to be stored as a variable, else it doesn't update properly
          const childStatus = item.getTaskStatus(refreshCache);
          this.taskStatus = this.taskStatus && childStatus;
          return true; // force the loop to continue through all items
        });
      }

      if (this.tasks) {
        this.taskStatus = _.every(this.tasks, 'isCredited');
      }
    }
    return this.taskStatus;
  }

  /**
   * @returns {Array} an array of all tasks on this item and all its children
   */
  get allChildTasks() {
    return _.reduce(this.items, (result, item) =>
      _.concat(result, item.allChildTasks), this.tasks);
  }
}
