import Model from './model.class';

export default class Office extends Model {
  get role() {
    return this.officeRole;
  }

  // The case and job services return the object from either
  // mast_case_case_office or mast_job_job_office, so the actuall Office
  // model is under this.model.office (so Model gives this.office).
  // This class might be better named as either CaseOffice or JobOffice?
  get name() {
    return this.office.name;
  }
}
