import * as _ from 'lodash';

export default class FormModel {
  constructor(data, parent) {
    _.assignIn(this, parent);

    this.data = parent.data;
    this.parent = parent;
    this.config = parent.config;

    this.tpls = {};
  }

  //  ---------------------------------------------------------
  $get(name, param) {
    const res = this.tpls[name];
    return param ? _.get(res, param) : res;
  }

  //  ---------------------------------------------------------
  getModel() {
    return this;
  }

  //  ---------------------------------------------------------
  isEmpty(value) {
    return _.isNil(value) ||
      typeof value === 'object' && _.isEmpty(value);
  }

  //  ---------------------------------------------------------
  //  Set values to From elements
  setValues(values, not) {
    values = values || this.data;

    this._children = _.filter(this._children, (item) => {
      if (item.name && !_.includes(not, item.name)) {
        item.value = _.get(values, item.name);
      }

      return item.autoHide ? !this.isEmpty(item.value) : true;
    });
  }

  /**
   * Add additional methods to each template object
   * Filter when its not to be visible
   * @param {array} arr - primary list of elements
   * @return {array} - filtered list of elements
   */
  prepare(arr) {
    return _.filter(arr, (item) => {
      item.$getModel = this.getModel.bind(this);
      item.$get = this.$get.bind(this);

      const extendBase = _.get(this.extend, item.extend);

      if (extendBase) {
        _.merge(item, _.merge(extendBase.bind(this)(item), item));
      }

      if (item.name) {
        this.tpls[item.name] = item;
      }

      return item.inactive !== true;
    });
  }

  //  ---------------------------------------------------------
  //  Set disabled to From elements
  setDisabled(disable, not) {
    this.list.forEach((item) => {
      if (!_.includes(not, item.name)) {
        item.disabled = disable;
      }
    });
  }

  //  ---------------------------------------------------------
  //  Get values from Form and set to model data
  get values() {
    this.data = this.data || {};

    _.forEach(this.children, (item) => {
      if (item.name) {
        const oldValue = _.get(this.data, item.name);
        if (oldValue !== item.value) {
          _.set(this.data, item.name, item.value);
        }
      }
    });
    return this.data;
  }

  set children(value) {
    if (!this._children) {
      this._children = this.prepare(value);
    }
  }

  get children() {
    return this._children;
  }

  get list() {
    return this._children;
  }

  set list(value) {
    this.children = value;
  }
}
