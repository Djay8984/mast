import * as _ from 'lodash';

import Model from './model.class';

const REPORT_IDS = [ 1, 3 ];

export default class ReportModel extends Model {
  constructor(model) {
    super(model);
  }

  get cocs() {
    return _.get(this.content, 'wipCocs');
  }

  get actionableItems() {
    return _.get(this.content, 'wipActionableItems');
  }

  get assetNotes() {
    return _.get(this.content, 'wipAssetNotes');
  }

  get certificates() {
    return _.get(this.content, 'wipCertificates');
  }

  get surveys() {
    return _.get(this.content, 'surveys');
  }

  /**
   * @summary reportStatus() short but sweet status message
   * @return {String} status
   */
  get reportStatus() {
    if (!_.includes(REPORT_IDS, this.reportType.id)) {
      return '';
    }
    const status = _.get(this, 'approved');
    if (_.isNull(status)) {
      return 'Pending';
    }
    if (!status) {
      return 'Rejected';
    }
    return 'Approved';
  }

  /**
   * @summary hacky way of determining rejection status
   * @returns {Boolean} is this report rejected
   */
  get rejected() {
    return !_.isNull(this.rejectionComments);
  }
}
