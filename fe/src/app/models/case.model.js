import * as _ from 'lodash';

import Model from './model.class';
import Customer from './customer.model';
import Office from './office.model';

const CASE_SURVEYOR_ID = 1;
const LEAD_SURVEYOR_ID = 11;
const CASE_ADMIN = 8;
const CASE_OWNER = 7;
const CONTRACT_HOLDER = 4;
const CASE_SDO_ID = 14;
const JOB_SDO_ID = 15;

export default class Case extends Model {
  get offices() {
    if (!this._offices) {
      this._offices = this.model.offices.map((office) => new Office(office));
    }

    return this._offices || [];
  }

  // SDO = Service Delivery Office
  get sdo() {
    const sdo = this.offices.find((office) => office.role.name === 'SDO');

    return sdo ? sdo.name : '-';
  }

  // CFO = Client Facing Office
  get cfo() {
    const cfo = this.offices.find((office) => office.role.name === 'CFO');

    return cfo ? cfo.name : '-';
  }


  get leadSurveyor() {
    return this.getSurveyorByRoleId(LEAD_SURVEYOR_ID);
  }

  get caseSurveyor() {
    return this.getSurveyorByRoleId(CASE_SURVEYOR_ID);
  }

  get caseAdmin() {
    return this.getSurveyorByRoleId(CASE_ADMIN);
  }

  get caseOwner() {
    return this.getSurveyorByRoleId(CASE_OWNER);
  }

  get contractHolder() {
    return this.getSurveyorByRoleId(CONTRACT_HOLDER);
  }

  get caseSDO() {
    return this.getOfficeByRoleId(CASE_SDO_ID);
  }

  get jobSDO() {
    return this.getOfficeByRoleId(JOB_SDO_ID);
  }

  getOfficeByRoleId(id) {
    const office = _.find(this.offices, ['officeRole.id', id]);

    return office ? office.name : '-';
  }

  getSurveyorByRoleId(roleId) {
    const surveyor = this.surveyors.find((s) =>
      _.isEqual(_.get(s, 'employeeRole.id'), roleId));

    return surveyor ? surveyor.surveyor.name : '-';
  }

  set caseStatus(value) {
    this.model.caseStatus = value;
  }

  get caseStatus() {
    return this.model.caseStatus;
  }

  set statusReason(value) {
    this.model.statusReason = value;
  }

  get statusReason() {
    return this.model.statusReason;
  }

  get customers() {
    if (!this._customers && this.model.customers) {
      this._customers = _.map(this.model.customers, (customer) =>
          Reflect.construct(Customer, [customer]));
    }

    return this._customers;
  }

  addCustomer(customer) {
    _.defaults(customer, {
      customer: {},
      relationship: {},
      functions: []
    });

    this.model.customers.push(Reflect.construct(Customer, [customer]));
  }
}
