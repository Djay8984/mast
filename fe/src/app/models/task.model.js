import * as _ from 'lodash';

import Model from './model.class';
import TaskQuestion from './task-question.model';

export default class Task extends Model {
  constructor(task, creditStatuses) {
    super(task);
    this._creditStatuses = creditStatuses;

    this._initQuestions();
  }

  /**
   * Wraps all the questions (aka 'attributes') belonging to this Task in a
   * TaskQuestion model
   */
  _initQuestions() {
    _.forEach(this.attributes, (question, index) =>
      _.set(this.attributes, index, new TaskQuestion(question)));
  }

  /**
   * @param {int} questionId the parent question ID
   * @returns {[TaskQuestion]} the list of questions whose followedOnFrom has
   * the provided ID
   */
  getFollowOnQuestions(questionId) {
    return _.filter(this.attributes, { followedOnFrom: { id: questionId } });
  }

  /**
   * Recurses through all the Task's questions, resetting the supplied answer to
   * null
   */
  resetQuestionAnswers() {
    // Use 'attributes' instead of 'questions' so all answers are reset
    _.forEach(this.attributes, (question) =>
      _.set(question, 'attributeValueString', null));
  }

  get actionTakenDate() {
    return this.model.actionTakenDate;
  }

  set actionTakenDate(actionTakenDate) {
    this.model.actionTakenDate = actionTakenDate;
  }

  get creditStatus() {
    return this.model.creditStatus;
  }

  set creditStatus(status) {
    this.model.creditStatus = status;
  }

  get description() {
    return this.model.description;
  }

  set description(description) {
    this.model.description = description;
  }

  get isConditionalTask() {
    return Boolean(_.get(this, 'questions', []).length);
  }

  get isCredited() {
    return this.model.creditStatus.id !==
      this._creditStatuses.get('uncredited');
  }

  get questions() {
    return _.reject(this.model.attributes, 'followedOnFrom');
  }

  get resolvedBy() {
    return this.model.resolvedBy;
  }

  set resolvedBy(employee) {
    this.model.resolvedBy = employee;
  }

  get resolutionDate() {
    return this.model.resolutionDate;
  }

  set resolutionDate(date) {
    this.model.resolutionDate = date;
  }

  get showFirstQuestion() {
    return this.model.creditStatus.id ===
      this._creditStatuses.get('completed');
  }
}
