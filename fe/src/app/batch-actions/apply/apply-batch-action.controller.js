import Base from 'app/base.class';
import * as _ from 'lodash';

import ItemTypesController from './item-types.controller';
import ItemTypesDialog from './item-types.dialog';

import ConfirmationController from './confirmation.controller';
import ConfirmationDialog from './confirmation.dialog';

import SavingDialog from './saving.dialog';

import '../partials/assets.html';
import '../partials/selected.html';

import batchActionBackTmp from
  'app/components/batch-action-banner/modals/batch-action-back-template.html';

export default class ApplyBatchActionController extends Base {
  /* @ngInject */
  constructor(
    $anchorScroll,
    $scope,
    $state,
    $stateParams,
    $timeout,
    $q,
    attachmentDecisionService,
    MastModalFactory,
    assetOnlineService,
    batchActionService,
    batchActionLargeAssetSet,
    assets,
    assetCategories,
    assetFilterDefault
  ) {
    super(arguments);

    this.selected = $stateParams.selectedAssets || {
      perPage: 12,
      page: 1,
      obj: {},
      list: [],
      recently: 0,
      total: 0
    };

    $stateParams.selectedAssets = this.selected;

    this.assetFilter = _.isEmpty($stateParams.assetFilter) ?
      assetFilterDefault : $stateParams.assetFilter;
    this.filterActive = !_.isEqual(this.assetFilter, assetFilterDefault);
    this.filterOpen = false;

    if (_.isNull(this._$stateParams.batchAction)) {
      this._$state.go('^.define');
    }

    /** @type {Object[]} */
    this.batch = this._$stateParams.batchAction;

    this.applyBatchActions();
  }

  updateList(filter) {
    this._$state.go(this._$state.current.name,
      _.set(this._$stateParams, 'assetFilter', filter), {
        reload: true
      });
  }

  get filtered() {
    return !_.isEmpty(this._$stateParams.assetFilter);
  }

  get totalElements() {
    return this._assets.pagination.totalElements;
  }

  get filteredAndZeroAssets() {
    return _.isEmpty(this.allAssets) && this.filtered && !this.filterOpen;
  }

  get assetFilterDefault() {
    return this._assetFilterDefault;
  }

  /**
   * @summary applyBatchActions()
   * @description Sets the radio buttons in the footer depending on what type
   *   of codicil was selected (asset-note or actionable-item)
   */
  applyBatchActions() {
    this.itemType = _.get(this.batch, 'itemType');

    this.applyTo = this.itemType ? 'item-types' : 'asset-item';
  }

  /**
   * @description Uses the injected 'assets' as a getter to return all of the
   *   injected assets
   * @summary All of the assets
   * @return {Object} assets All the assets we want to display, unfiltered
   */
  get allAssets() {
    return this._assets;
  }

  /**
   * @description Uses the injected 'selectedAssets' as a getter to return
   *   all of the injected assets
   * @summary All of the 'selected' assets
   * @return {Object} selectedAssets All the assets we want to display
   */
  get selectedAssets() {
    return this._selectedAssets;
  }

  /**
   * @description Gets the possible asset categories
   * @return {Array} assetCategories Array of objects
   */
  get assetCategories() {
    return _.get(this._assetCategories, 'asset.assetCategories');
  }


  /**
   * @summary Scrolls to the selected assets at the bottom
   */
  viewSelectedAssets() {
    this._$timeout(() => this._$anchorScroll('selected-content'));
  }

  /**
   * @summary Scrolls to the assets at the top
   */
  viewAssets() {
    this._$anchorScroll('assets-content');
  }

  /**
   * @return {String} assetType The type of asset to display
   */
  get assetType() {
    return this._assetType;
  }

  async reload() {
    this._$state.go(
      this._$state.current, this._$stateParams, { reload: true, notify: true }
    );
  }

  /**
   * @summary validForm()
   * @return {Boolean} true or false Whether the form is valid or not
   */
  validForm() {
    // Various conditions that must be met before the form is valid;
    if (this.applyBatchActionsForm.applyTo &&
      _.isUndefined(this.applyBatchActionsForm.applyTo.$viewValue)) {
      return false;
    }

    if (this.withTemplate && this.templateBroken &&
      _.isUndefined(this.applyBatchActionsForm.itemType.$viewValue)) {
      return false;
    }

    return this.applyBatchActionsForm.$valid;
  }

  /**
   * @summary selectItemTypeModal()
   * @description Initiated by a button on the footer this opens a modal up to
   *   allow the user to select an item type, giving the typeahead enough room
   *   to display;
   */
  async selectItemTypeModal() {
    const response = await this._MastModalFactory({
      controller: ItemTypesController,
      controllerAs: 'vm',
      templateUrl: ItemTypesDialog,
      class: 'large'
    }).activate();

    this.itemType = response || this.batch.itemType || {};
  }

  //  -------------------------------------------------------------
  async addAttachment(val, key, type) {
    const assetId = key;
    const codicilId = val[0].id;
    const path = `asset/${assetId}/${type}/${codicilId}`;

    await this._attachmentDecisionService.addAll(path, this.batch.attachments);
  }

  //  -------------------------------------------------------------
  async updateAttachments(res, type) {
    if (this.batch.attachments) {
      await this._$q.all(
        _.map(res, (val, key) => this.addAttachment(val, key, type)));
    }
  }

  //  -------------------------------------------------------------
  savingModal() {
    return new this._MastModalFactory({
      templateUrl: SavingDialog
    });
  }

  //  -------------------------------------------------------------
  confirmationModal() {
    return new this._MastModalFactory({
      controller: ConfirmationController,
      controllerAs: 'vm',
      templateUrl: ConfirmationDialog,
      inject: {
        assets: this.selected,
        response: this.response,
        itemType: this.itemType,
        actionType: this.batch.actionType
      }
    });
  }

  /**
   * @summary finishBatchAction()
   * @description Saving the batch action, calls the service to save everything
   *   and then opens up the modal to inform the user;
   */
  async finishBatchAction() {
    const saving = this.savingModal();
    saving.activate();

    if (!this.batch.template || !this.batch.template.id) {
      this.batch.template = null;
    }

    // Constructing options for saving the codicil;
    const options = {
      ids: _.keys(this.selected.obj),
      note: this.batch,
      type: this.batch.actionType.name
    };

    // Setting the item type id;
    if (this.applyTo === 'item-types') {
      options.itemTypeId = _.get(this.itemType, 'id');
    } else {
      this.itemType = null;
    }

    // Make the save to the service;
    this.response =
      await this._batchActionService.saveBatchActionCodicils(
        options.ids, options.note, options.type, options.itemTypeId);

    //  Upload attachments if asset note
    await this.updateAttachments(
      this.response.savedAssetNoteDtos, 'asset-note');

    //  Upload attachments actionable item
    await this.updateAttachments(
      this.response.savedActionableItemDtos, 'actionable-item');

    // close saving modal and open confirmation
    const confirmation = this.confirmationModal();
    saving.destroy();

    // Display the modal with the save results;
    await confirmation.activate();

    // 7.15 AC9 move to prior page after clicking OK on success modal - the
    // dashboard is in there as a default;
    this._$state.go(this._$stateParams.from || 'dashboard.myWork');
  }

  /**
   * @description Checks the asset-note/actionable-item for a template
   * @return {Boolean} true or false
   */
  get withTemplate() {
    return _.get(this._$stateParams.batchAction, 'template.id');
  }

  /**
   * @description Is the template broken?
   * @return {Boolean} true or false
   */
  get templateBroken() {
    return _.get(this._$stateParams.batchAction, 'linkBroken');
  }

  /**
   * @return {String} sortBy The sort order
   */
  get sortOrder() {
    return this._$stateParams.sortBy || this._assets.sortedBy;
  }

  /**
   * @param {String} value The sort order
   */
  set sortOrder(value) {
    this._$stateParams.sortBy = value;
  }

  /**
   * @description Navigate to "define" tab
   */
  async defineTab() {
    const result = await this._MastModalFactory({
      templateUrl: batchActionBackTmp,
      class: 'dialog'
    }).activate();
    if (result) {
      this._$state.go('batch-actions.define', this._$state.params);
    }
  }

  /**
   * @description Select single asset
   * @param {object} asset - asset
   */
  select(asset) {
    this.selected.obj[asset.id] = asset;
    this.count();

    this.setInfo('add');
  }

  /**
   * @description Deselect single asset
   * @param {object} asset - asset
   */
  deselect(asset) {
    Reflect.deleteProperty(this.selected.obj, asset.id);
    this.count();

    this.setInfo('remove');
  }

  /**
   * @description Select all assets
   */
  async selectAll() {
    this.loading = 'preload';
    const res = await this._assets.getAll();
    _.forEach(res, (asset) => {
      this.selected.obj[asset.id] = asset;
    });

    this.count();
    this.loading = false;

    this.viewSelectedAssets();
    this.setInfo('addAll');
  }

  /** @summary Deselect all assets */
  deselectAll() {
    this.selected.obj = {};
    this.count();

    this.setInfo('removeAll');
  }

  /** @description Count selected assets */
  count() {
    this.selected.list = _.values(this.selected.obj);
    this.selected.total = this.selected.list.length;
  }

  //  -------------------------------------------------------
  isSelected(asset) {
    return this.selected.obj[asset.id];
  }

  /**
   * @description Set timeout info box
   * @param {string} type - type of action
   */
  setInfo(type = 'add') {
    this.showInfo = true;
    if (this.selected.lastAction !== type) {
      this.selected.recently = 0;
    }

    this.selected.lastAction = type;
    this.selected.recently++;

    const s = this.selected.recently > 1 ? 's' : '';

    const text = {
      add: `${this.selected.recently} asset${s} added`,
      remove: `${this.selected.recently} asset${s} removed`,
      addAll: 'All assets added',
      removeAll: 'All assets removed'
    };

    this.selected.actionText = text[type];

    if (this._singleTimeout) {
      this._$scope.$evalAsync();
    }

    this._singleTimeout =
      this._$timeout(() => {
        this.showInfo = false;
        this.selected.recently = 0;
      }, 3000, true);
  }
}
