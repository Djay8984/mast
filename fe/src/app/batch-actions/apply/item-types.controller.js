import Base from 'app/base.class';

export default class ItemTypesController extends Base {
  /* @ngInject */
  constructor(
    assetDecisionService
  ) {
    super(arguments);
  }

  findItemType(queryString) {
    return this._assetDecisionService.itemTypeTypeahead(queryString);
  }
}
