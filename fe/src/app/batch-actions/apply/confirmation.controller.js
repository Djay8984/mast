import Base from 'app/base.class';
import * as _ from 'lodash';

export default class ConfirmationController extends Base {
  /* @ngInject */
  constructor(
    actionType,
    assets,
    itemType,
    response
  ) {
    super(arguments);
  }

  get totalSelectedAssets() {
    return _.size(_.get(this, '_assets.list', 0));
  }

  get totalSaved() {
    return _.size(this.savedItems);
  }

  get itemType() {
    return this._itemType;
  }

  get actionType() {
    return this._actionType;
  }

  get savedItems() {
    return this._response.savedActionableItemDtos ||
      this._response.savedAssetNoteDtos;
  }

  assetName(assetId) {
    return _.get(this._assets.obj[assetId], 'name');
  }

  assetImo(assetId) {
    const asset = this._assets.obj[assetId];
    if (!asset.ihsAsset || !asset.ihsAsset.id) {
      return `(Asset id ${asset.id})`;
    }
    return `(IMO no. ${asset.ihsAsset.id})`;
  }

  //  -------------------------------------------------------------
  get rejectedAssets() {
    return _.map(this._response.rejectedAssetIds, (id) => this._assets.obj[id]);
  }
}
