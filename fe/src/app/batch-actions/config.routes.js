import * as _ from 'lodash';

import HeaderController from 'app/dashboard/header/dashboard-header.controller';
import headerTemplateUrl from 'app/dashboard/header/dashboard-header.html';

import batchActionController from './batch-action.controller';
import batchActionTemplateUrl from './batch-action.html';

import defineBatchActionController from
  './define/define-batch-action.controller';
import defineBatchActionTemplateUrl from './define/define-batch-action.html';

import applyBatchActionController from './apply/apply-batch-action.controller';
import applyBatchActionTemplateUrl from './apply/apply-batch-action.html';

/* @ngInject */
export default function config(
  $stateProvider,
  managementSystemAsset
) {
  $stateProvider
    .state('batch-actions', {
      abstract: true,
      parent: 'main',
      url: '/batch-actions',
      views: {
        '': {
          controller: batchActionController,
          templateUrl: batchActionTemplateUrl,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm'
        }
      }
    })
    .state('batch-actions.define', {
      parent: 'batch-actions',
      url: '/define',
      controller: defineBatchActionController,
      controllerAs: 'vm',
      params: {
        batchAction: null,
        from: null
      },
      templateUrl: defineBatchActionTemplateUrl,
      resolve: {

        /* @ngInject */
        categories: (referenceDataService) =>
          referenceDataService.get(['asset.codicilCategories'], true),

        /* @ngInject */
        confidentialityTypes: (referenceDataService) =>
          referenceDataService.get(['attachment.confidentialityTypes'], true),

        /* @ngInject */
        templateStatuses: (referenceDataService) =>
          referenceDataService.get(['asset.templateStatuses']),

        /* @ngInject */
        templates: (referenceDataService) =>
          referenceDataService.get(['asset.codicilTemplates'], true)
      }
    })
    .state('batch-actions.apply', {
      parent: 'batch-actions',
      params: {
        sortBy: { sort: 'name', order: 'ASC' },
        selectedIds: null,
        selectedAssets: null,
        batchAction: null,
        from: null,
        assetFilter: null
      },
      url: '/apply',
      controller: applyBatchActionController,
      controllerAs: 'vm',
      templateUrl: applyBatchActionTemplateUrl,
      resolve: {

        /* @ngInject */
        assetFilterDefault: (assetDecisionService) =>
          assetDecisionService.getCurrentLifecycleStatusIds().then((ids) => ({
            lifecycleStatusId: ids
          })),

        /* @ngInject */
        assetCategories: (referenceDataService) =>
          referenceDataService.get(['asset.assetCategories']),

        /* @ngInject */
        assets: (
          assetCategories,
          assetDecisionService,
          $stateParams,
          assetLifecycleStatus
        ) => {
          const lifecycleStatusId =
            _.values(_.pickBy(assetLifecycleStatus.toObject(), (als) =>
              !_.includes([assetLifecycleStatus.get('decommissioned'),
                assetLifecycleStatus.get('cancelled')], als)
            ));

          const categoryId = _.reject(
            _.map(_.get(assetCategories, 'asset.assetCategories'), 'id'),
              managementSystemAsset);

          // Sets the assetFilter.<filter> to itself if it exists, or the one
          // defined above as a default
          _.set($stateParams, 'assetFilter.lifecycleStatusId',
            _.get($stateParams, 'assetFilter.lifecycleStatusId',
              lifecycleStatusId));

          _.set($stateParams, 'assetFilter.categoryId',
            _.get($stateParams, 'assetFilter.categoryId',
              categoryId));

          return assetDecisionService.query(
            $stateParams.assetFilter,
            $stateParams.sortBy);
        }
      }
    });
}
