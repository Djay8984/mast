import Base from 'app/base.class';
import * as _ from 'lodash';

import '../partials/define.html';
import AIInput from 'app/codicil/actionable-item/ai.input';
import ANInput from 'app/codicil/asset-note/an.input';

export default class AddBatchActionsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    codicilType
  ) {
    super(arguments);

    this.status = {
      [this._codicilType.get('assetNote')]: 'assetNote',
      [this._codicilType.get('actionableItem')]: 'actionableItem'
    };

    const config = {
      title: 'Batch Actions.',
      batchAction: true
    };

    this.actionTypes = {
      assetNote: {
        id: this._codicilType.get('assetNote'),
        code: 'AN',
        name: 'asset-note',
        title: 'Asset note',
        constant: 'assetNote',
        _moment: this._moment,
        data: {},
        config
      },
      actionableItem: {
        id: this._codicilType.get('actionableItem'),
        code: 'AI',
        name: 'actionable-item',
        title: 'Actionable item',
        constant: 'actionableItem',
        _moment: this._moment,
        data: {},
        config
      }
    };

    this.data = $stateParams.batchAction || {};
    this.type = _.get(this.data, 'actionType.constant');

    if (this.type) {
      this.actionTypes[this.type].data = this.data;
    }

    this.assetNote = new ANInput(this.actionTypes.assetNote);
    this.assetNote.setValues();

    this.actionableItem = new AIInput(this.actionTypes.actionableItem);
    this.actionableItem.setValues();

    if (this.type) {
      this.children = this[this.type];
    }
  }

  change() {
    this.children = this[this.type];

    //  Clear data
    this.children.setValues();

    //  Set all fields enabled
    this.children.setDisabled(false);
  }

  /**
   * next()
   */
  next() {
    this.loading = 'Loading...';
    const batchAction = this.children.values;
    batchAction.actionType = this.actionTypes[this.type];
    this._$stateParams.batchAction = batchAction;

    this._$state.go('^.apply', this._$stateParams);
  }

  cancel() {
    this._$state.go('dashboard.myWork', this._$stateParams);
  }
}
