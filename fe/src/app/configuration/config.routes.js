import controller from './configuration.controller.js';
import templateUrl from './configuration.html';

import HeaderController from 'app/dashboard/header/dashboard-header.controller';
import headerTemplateUrl from 'app/dashboard/header/dashboard-header.html';


/* @ngInject */
export default ($stateProvider) =>
  $stateProvider
    .state('config', {
      parent: 'safeMain',
      url: '/config',
      views: {
        '': {
          templateUrl,
          controller,
          controllerAs: 'vm'
        },
        'header@safeMain': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm'
        }
      }
    });
