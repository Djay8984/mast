import * as angular from 'angular';
import Base from 'app/base.class';
import { remote } from 'electron';

export default class ConfigurationController extends Base {
  /* @ngInject */
  constructor(
    appConfig,
    node
  ) {
    super(arguments);

    this.original = angular.copy(appConfig);
  }

  get appConfig() {
    if (!this._config) {
      this._config = angular.copy(this._appConfig);
    }

    return this._config;
  }

  set appConfig(value) {
    this._config = value;
  }

  get keys() {
    return Object.keys(this.appConfig).sort();
  }

  fieldType(key) {
    if (typeof this.appConfig[key] === 'boolean') {
      return 'boolean';
    }

    if (key === 'MAST_SIGN_PEM_KEY') {
      return 'textarea';
    }

    if (key.toLowerCase().includes('_password')) {
      return 'password';
    }

    return 'text';
  }

  async save() {
    const db = await remote.getGlobal('db');
    const reload = await remote.getGlobal('reload');

    await Promise.all(Object.entries(this.appConfig).map(([ key, value ]) =>
        db.config.update({ key }, { $set: { value } }, {})
    ));

    return reload();
  }

  reset() {
    this.appConfig = this.original;
  }

  async clearSession() {
    await this._node.clearSession();
    this.sessionCleared = true;
  }
}
