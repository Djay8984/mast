import * as angular from 'angular';
import routes from './config.routes';

export default angular
  .module('app.config', [])
  .config(routes)
  .name;
