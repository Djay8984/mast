import * as _ from 'lodash';

export default (...fields) =>
  (target, key, descriptor) => {
    const method = descriptor.value;

    descriptor.value = function sorted(...args) {
      return Reflect.apply(method, this, args).then((data) => {
        data.sortBy = (sort, order) => {
          if (!fields.includes(sort) ||
              !['asc', 'desc'].includes(order.toLowerCase())) {
            throw new Error(`Invalid arguments: ${sort}, ${order}`);
          }

          const lastArg = args.pop();

          // FIXME: this assumes that optional last arg is the only object param
          if (!_.isPlainObject(lastArg)) {
            args.push(lastArg);
          }

          // ??? Should this reset the page on each sort?
          Object.assign(data.reqParams, { page: 0, sort, order });
          args.push(data.reqParams);

          return Reflect.apply(method, this, args);
        };

        data.reqParams = data.reqParams || {};

        const { sort, order } = data.reqParams;

        data.sortable = true;
        data.sortFields = fields;
        data.sortedBy = { sort, order };

        return data;
      });
    };

    descriptor.value.sortable = true;

    return descriptor;
  };
