import modelable from './modelable.decorator';
import pageable from './pageable.decorator';
import sortable from './sortable.decorator';

export {
  modelable,
  pageable,
  sortable
};
