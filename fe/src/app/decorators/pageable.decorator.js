import * as _ from 'lodash';

/*
  Provides a nextPage method to collections returned by decorated
  service methods.

  NOTE: Only use on service methods which have query params
  with { page: ? } as the last argument.
*/
export default (target, key, descriptor) => {
  const method = descriptor.value;

  // NOTE: use function() here instead of lambda as `this` isn't mangled
  descriptor.value = function paged(...args) {
    return Reflect.apply(method, this, args).then((data) => {
      data.nextPage = () => {
        fixArgs();

        data.reqParams.page++;
        args.push(data.reqParams);

        return Reflect.apply(method, this, args);
      };

      data.getAll = () => {
        fixArgs();
        args.push({

          // Don't want to modify the reqParams, just create a similar
          // object with different size and page.
          ...data.reqParams,

          // Total number of elements requested so far.
          size: null,
          page: 0
        });

        return Reflect.apply(method, this, args);
      };

      data.refresh = () => {
        fixArgs();
        args.push({

          // Don't want to modify the reqParams, just create a similar
          // object with different size and page.
          ...data.reqParams,

          // Total number of elements requested so far.
          size: data.reqParams.size * (data.reqParams.page + 1),
          page: 0
        });

        return Reflect.apply(method, this, args).then((resp) => {
          data.pagination = resp.pagination;

          // We want to keep the methods on data, so we just replace
          // all of the array elements with the ones from the new response.
          return data.splice(0, data.length, ...resp);
        });
      };

      data.pageable = true;
      return data;

      function fixArgs() {
        const lastArg = args.pop();

        // FIXME: this assumes that optional last arg is the only object param
        if (!_.isPlainObject(lastArg)) {
          args.push(lastArg);
        }
      }
    });
  };

  descriptor.value.pageable = true;

  return descriptor;
};
