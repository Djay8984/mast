import * as _ from 'lodash';

/**
 * @param {class} model class to construct & turn response into
 * @return {object} function descriptor decorated for async modelling
 * adds some functionality on to the end of a promise chain to convert the
 *   resolved value into whatever class is passed. handles both list responses
 *   as well as single entities
 */
export default (model) =>
  (target, key, descriptor) => {
    const method = descriptor.value;
    descriptor.value = function applyModel(...args) {
      return Reflect
        .apply(method, this, args)
        .then((data) =>
          _.isArray(data) ?
            model.from(data) :
            Reflect.construct(model, data));
    };

    return descriptor;
  };
