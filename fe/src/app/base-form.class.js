import * as _ from 'lodash';

import Base from 'app/base.class';

export default class BaseForm extends Base {
  set form(form) {
    this._form = form;
  }

  get form() {
    return this._form;
  }

  get dirty() {
    return _.get(this, 'form.$dirty', false);
  }

  async save() {
    throw new TypeError('must implement');
  }
}
