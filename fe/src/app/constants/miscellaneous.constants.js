import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.miscellaneous', [])
  .constant('apiPollingFrequency', 0.1) // polls/second
  .constant('confidentialityTypeIdToHide', new Immutable.Map({
    asset: 3,
    coc: 1
  }))
  .constant('repairAction', new Immutable.Map({
    permanent: 1,
    temporary: 2,
    modification: 3
  }))
  .constant('dateFormat', 'DD MMM YYYY')
  .constant('ihsDateFormat', 'YYYYMM')
  .constant('serviceStatusIdMiscellaneous', 7)
  .constant('milestoneDueDateReference', new Immutable.Map({
    caseAcceptenceDate: 1,
    estimatedBuildDate: 2,
    milestoneCompletion: 3,
    manualEntry: 4
  }))
  .constant('modifyingRequests', new Immutable.Set([
    'put',
    'post',
    'patch',
    'delete',
    'remove'
  ]))
  .constant('errorCodes', new Immutable.Map({
    staleness: 1001
  }))
  .constant('typeaheadResultsLength', 8)
  .name;
