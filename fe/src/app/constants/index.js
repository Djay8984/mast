import * as angular from 'angular';

import category from './category.constants';
import miscellaneous from './miscellaneous.constants';
import node from './node.constants';
import rbac from './rbac.constants';
import role from './role.constants';
import status from './status.constants';
import type from './type.constants';
import validation from './validation.constants';
import stateHelper from './state-helper';

export default angular
  .module('app.constants', [
    category,
    miscellaneous,
    node,
    rbac,
    role,
    status,
    type,
    validation,
    stateHelper
  ])
  .name;
