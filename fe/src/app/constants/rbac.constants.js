import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.rbac', [])
  .constant('rbacActions', new Immutable.Map({
    createAssetHeader: 1,
    updateAssetHeader: 2,
    deleteAssetHeader: 3,
    createAssetModel: 4,
    updateAssetModel: 5,
    deleteAssetModel: 6,
    createCase: 7,
    updateCase: 8,
    deleteCase: 9,
    createCodicil: 10,
    updateCodicil: 11,
    deleteCodicil: 12,
    createDefect: 13,
    updateDefect: 14,
    deleteDefect: 15,
    createJob: 16,
    updateJob: 17,
    deleteJob: 18,
    createAssetServiceSchedule: 19,
    updateAssetServiceSchedule: 20,
    deleteAssetServiceSchedule: 21,
    createReferenceData: 22,
    updateReferenceData: 23,
    deleteReferenceData: 24,
    getReferenceData: 25,
    dataMigration: 26
  }))
  .constant('rbacGroupPermissions', new Immutable.Map({
    admin: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
      20, 21, 25, 26],
    surveyor: [8, 16, 17, 18, 25],
    authorisingSurveyor: [16, 17, 18, 25],
    sdo: [8, 16, 17, 18, 25],
    cfo: [1, 2, 3, 7, 8, 16, 17, 18, 25],
    mmso: [1, 2, 3, 7, 8, 9, 10, 11, 12, 16, 17, 18, 25],
    dceTechnical: [10, 11, 16, 20, 25],
    classGroupMms: [1, 7, 8, 9, 10, 11, 12, 16, 17, 25],
    classGroupFleetDatabase: [4, 5, 6, 19, 20, 21, 25],
    classGroupAdmin: [2, 8, 16, 25],
    classGroupSeniorAdmin: [1, 2, 3, 8, 10, 11, 12, 16, 19, 20, 21, 25],
    classGroupTechnical: [1, 2, 3, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
      20, 21, 25],
    eicAdmin: [1, 2, 5, 7, 8, 9, 16, 20, 25],
    eicSeniorAdmin: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 19, 20, 21, 25],
    eicTechnical: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 17, 19, 20, 21,
      25],
    mdsSystemAdmin: [1, 2, 3, 7, 8, 9, 16, 19, 20, 21, 25],
    mdsSuperuser: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
      18, 19, 20, 21, 22, 23, 24, 25]
  }))
  .name;
