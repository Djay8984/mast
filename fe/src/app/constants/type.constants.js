import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.type', [])
  .constant('assetItemRelationshipType', new Immutable.Map({
    isPartOf: 1,
    isRelatedTo: 9
  }))
  .constant('attributeValueType', new Immutable.Map({
    date: 1,
    float: 2,
    lookup: 3,
    string: 4,
    boolean: 5
  }))
  .constant('caseType', new Immutable.Map({
    aic: 1,
    aimc: 2,
    firstEntry: 3,
    toc: 4,
    tomc: 5,
    nonClassed: 6
  }))
  .constant('codicilType', new Immutable.Map({
    conditionOfClass: 1,
    actionableItem: 2,
    assetNote: 3,
    statutoryDeficiency: 4,
    statutoryOutstandingItem: 5,
    nonConformityNote: 6,
    statutoryDefectDeficiency: 7,
    observation: 8
  }))
  .constant('classRecommendationType', new Immutable.Map({
    standardDSR: 1,
    nonStandardDSR: 2,
    reinstatementDSR: 3,
    tocAicCompletedDSR: 4,
    tocAicNotCompletedDSR: 5,
    maintenanceDSR: 6,
    reactivationDSR: 7,
    fitnessToBeTowedDSR: 8,
    standardFAR: 9,
    nonStandardFAR: 10,
    reinstatementFAR: 11,
    tocAicCompletedFAR: 12,
    tocAicNotCompletedFAR: 13,
    maintenanceFAR: 14,
    reactivationFAR: 15,
    fitnessToBeTowedFAR: 16
  }))
  .constant('confidentialityType', new Immutable.Map({
    lrInternal: 1,
    lrCustomer: 2,
    all: 3
  }))
  .constant('productType', new Immutable.Map({
    classification: 1,
    mms: 2,
    statutory: 3
  }))
  .constant('reportType', new Immutable.Map({
    fsr: 1,
    far: 2,
    dsr: 3,
    dar: 4
  }))
  .constant('serviceType', new Immutable.Map({
    alternate: 1,
    annual: 2,
    docking: 3,
    initial: 4,
    interim: 5,
    intermediate: 6,
    miscellaneous: 7,
    periodic: 8,
    renewal: 9,
    standAlone2InSpecialCycle: 10,
    additional: 11
  }))
  .constant('serviceRelationshipType', new Immutable.Map({
    dependent: 1,
    counterpart: 2
  }))
  .constant('surveyType', new Immutable.Map({
    damage: 1,
    repair: 2
  }))
  .name;
