import * as _ from 'lodash';
import * as angular from 'angular';

export default angular
  .module('app.constants.stateHelper', [])
  .constant('stateHelper', {
    initState(item) {
      const state = _.pick(item,
        ['url', 'abstract', 'parent', 'data', 'header', 'checkedInRedirect',
        'params', 'title']);

      if (item.header) {
        _.set(state, 'data.header', item.header);
      }

      state.views = this.getViews(item);
      state.resolve = this.getResolve(item);

      if (item.name) {
        this._$stateProvider.state(item.name, state);
      }
      return state;
    },
    getResolve(item) {
      let itemResolve = _.get(item, 'content.resolve', {});
      itemResolve = _.extend({}, itemResolve, item.resolve);

      const params = _.keys(itemResolve);

      const func = (...arg) => {
        const res = {};
        _.forEach(params, (val, key) => {
          res[val] = arg[key];
        });
        return res;
      };

      return _.extend(itemResolve, {
        page: ['$state', '$stateParams', ($state, $stateParams) => item],
        resolve: [...params, func]
      });
    },
    getViews(item) {
      const views = {};

      if (item.templateUrl || item.controller) {
        const target = item.hasOwnProperty('target') ? item.target : '@main';
        views[target] = {
          templateUrl: item.templateUrl,
          controller: item.controller,
          controllerAs: item.controllerAs || 'vm'
        };
      }

      if (item.viewsHeader && item.viewsHeader.templateUrl) {
        views['header@main'] =
          _.extend({ controllerAs: 'vm' }, item.viewsHeader);
      }
      return _.extend(views, item.views);
    },
    extendItem(item, key, parent) {
      item = _.extend({ _key: key, name: _.isString(key) && key }, item);

      if (parent.name && item.name && !item.parent) {
        item.parent = parent.name;
        item.name = `${parent.name}.${item.name}`;
      }

      _.defaults(item, parent.extend);
      return _.defaults(item, item.content);
    },
    parseChildren(children, parent = {}) {
      _.forEach(children, (val, key) => {
        const item = this.extendItem(val, key, parent);

        this.initState(item);

        this.parseChildren(item.children, item);
      });
    },
    create($stateProvider, obj) {
      this._$stateProvider = $stateProvider;

      this.parseChildren(obj);
    }
  })
  .name;
