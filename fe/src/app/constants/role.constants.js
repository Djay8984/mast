import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.role', [])
  .constant('employeeRole', new Immutable.Map({
    businessSupport: 1,
    surveyorAssessorInspector: 2,
    management: 3,
    salesClientRelationshipManagement: 4,
    specialistLeadership: 5,
    authorisingSurveyor: 6,
    eicCaseSurveyor: 7,
    eicAdmin: 8,
    sdo: 9,
    eicManager: 10,
    leadSurveyor: 11,
    sdoCoordinator: 12
  }))
  .constant('officeRole', new Immutable.Map({
    sdo: 1,
    cfo: 2,
    cfo3: 3,
    mmsSdo: 4,
    mmso: 5,
    tso: 6,
    tsoStat: 7,
    classGroup: 8,
    dce: 9,
    eic: 10,
    mds: 11,
    tsdo: 12,
    tssao: 13,
    caseSdo: 14,
    jobSdo: 15
  }))
  .constant('surveyorRole', new Immutable.Map({
    businessSupport: 1,
    surveyor: 2,
    management: 3,
    salesCRM: 4,
    specialistLeadership: 5,
    authorisingSurveyor: 6,
    eicCaseSurveyor: 7,
    eicAdmin: 8,
    sdo: 9,
    eicManager: 10,
    leadSurveyor: 11,
    sdoCoordinator: 12
  }))
  .name;
