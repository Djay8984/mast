import * as angular from 'angular';

// for some unknown reason if you remove this import then the nightlies break
import packageJson from 'app/../../package.json';
import { remote } from 'electron';

export default angular
  .module('app.constants.node', [])
  .constant('db', remote.getGlobal('db'))
  .constant('appConfig', angular.copy(remote.getGlobal('config')))
  .constant('packageJson', packageJson)
  .name;
