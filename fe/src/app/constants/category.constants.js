import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.category', [])
  .constant('assetNoteCategory', new Immutable.Map({
    class: 1,
    statutory: 2,
    external: 3
  }))
  .constant('actionableItemCategory', new Immutable.Map({
    class: 4,
    statutory: 5,
    external: 6
  }))
  .constant('cocCategory', new Immutable.Map({
    hull: 7,
    machinery: 8,
    refrigeration: 9,
    liftingAppliance: 10,
    electroTechnical: 11,
    asset: 12
  }))
  .constant('defectCategory', new Immutable.Map({
    electroTechnical: 1,
    hull: 2,
    liftingAppliance: 3,
    machinery: 4,
    refrigeration: 5,

    // The real names of these categories don't include the "hull" or
    // "machinery" prefix, but we need something to differentiate between them
    hullContact: 6,
    hullStructural: 7,
    hullMiscellaneous: 8,
    hullEquipment: 9,
    hullMechanical: 10,
    machineryContact: 11,
    machineryStructural: 12,
    machineryMiscellaneous: 13,
    machineryEquipment: 14,
    machineryMechanical: 15
  }))
  .constant('jobCategory', new Immutable.Map({
    survey: 1,
    auditMms: 2
  }))
  .name;
