import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.status', [])
  .constant('actionableItemStatus', new Immutable.Map({
    draft: 1,
    inactive: 2,
    open: 3,
    changeRecommended: 4,
    deleted: 5,
    closedForCurrentJob: 6,
    cancelled: 7
  }))
  .constant('assetLifecycleStatus', new Immutable.Map({
    underConstruction: 1,
    buildSuspended: 2,
    inService: 3,
    laidUp: 4,
    underRepair: 5,
    inCasualty: 6,
    converting: 7,
    toBeBrokenUp: 8,
    decommissioned: 9,
    toBeHandedOver: 10,
    cancelled: 11
  }))
  .constant('assetNoteStatus', new Immutable.Map({
    draft: 8,
    inactive: 9,
    open: 10,
    changeRecommended: 11,
    deleted: 12,
    cancelled: 13
  }))
  .constant('caseStatus', new Immutable.Map({
    uncommitted: 1,
    populate: 2,
    onHold: 3,
    jobPhase: 4,
    validateAndUpdate: 5,
    closed: 6,
    cancelled: 7,
    deleted: 8,
    takeOffHold: 9
  }))
  .constant('cocStatus', new Immutable.Map({
    open: 14,
    deleted: 15,
    cancelled: 16
  }))
  .constant('classMaintenanceStatus', new Immutable.Map({
    single: 1,
    dual: 2
  }))
  .constant('creditStatus', new Immutable.Map({
    confirmed: 1,
    completed: 2,
    waived: 3,
    uncredited: 4
  }))
  .constant('defectStatus', new Immutable.Map({
    open: 1,
    closed: 2,
    cancelled: 3
  }))
  .constant('dueStatus', new Immutable.Map({
    notDue: 1,
    due: 2,
    overdue: 3
  }))
  .constant('jobStatus', new Immutable.Map({
    sdoAssigned: 1,
    resourceAssigned: 2,
    underSurvey: 3,
    underReporting: 4,
    awaitingTechnicalReviewerAssignment: 5,
    underTechnicalReview: 6,
    awaitingEndorserAssignment: 7,
    underEndorsement: 8,
    cancelled: 9,
    aborted: 10,
    closed: 11
  }))
  .constant('milestoneStatus', new Immutable.Map({
    open: 1,
    completed: 2
  }))
  .constant('preInspectionStatus', new Immutable.Map({
    favourable: 1,
    notFavourable: 2,
    notRequired: 3
  }))
  .constant('serviceCreditStatus', new Immutable.Map({
    notStarted: 1,
    partHeld: 2,
    postponed: 3,
    complete: 4,
    finished: 5
  }))
  .constant('serviceStatus', new Immutable.Map({
    notStarted: 1,
    partHeld: 2,
    held: 3, // All tasks completed but an associated service is not complete
    complete: 4
  }))
  .constant('serviceTypeStatus', new Immutable.Map({
    miscellaneous: 7
  }))
  .constant('scheduledServiceStatus', new Immutable.Map({
    notStarted: 1,
    partHeld: 2,
    held: 3,
    complete: 4
  }))
  .constant('templateStatus', new Immutable.Map({
    live: 1,
    draft: 2,
    inactive: 3
  }))
  .name;
