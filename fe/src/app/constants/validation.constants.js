import Immutable from 'immutable';

import * as angular from 'angular';

export default angular
  .module('app.constants.validation', [])
  .constant('allowedSpecialCharactersRegex',
   /^[ a-zA-Z0-9!\"#\$£\%\&'\(\)\*\+\,\-\.\/\:\;\<\=\>\?@\[\\\]\^_\`\{\|\}~]*$/)
  .constant('attachmentMaxChars', 1000)
  .constant('batchActionLargeAssetSet', 24)
  .constant('batchActionTypeAI', new Immutable.Map({
    id: 2,
    name: 'actionable-item',
    title: 'Actionable Item'
  }))
  .constant('batchActionTypeAN', new Immutable.Map({
    id: 3,
    name: 'asset-note',
    title: 'Asset Note'
  }))
  .constant('builderLength', 50)
  .constant('cancelCaseTextLength', 500)
  .constant('codicilDescriptionLength', 2000)
  .constant('codicilSurveyorGuidanceLength', 2000)
  .constant('codicilTitleLength', 50)
  .constant('contractReferenceLength', 30)
  .constant('creditingNarrativeLength', 2000)
  .constant('imoNumberLength', 7)
  .constant('managementSystemAsset', 9)
  .constant('reportNoteMaxChars', 2000)
  .constant('templateDescriptionLength', 150)
  .constant('yardNumberLength', 9)
  .name;
