import * as angular from 'angular';
import directive from './directive';

export default angular
  .module('app.example', [
    directive
  ])
  .name;
