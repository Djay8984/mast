import * as angular from 'angular';
import example from './example.directive.js';

export default angular
  .module('app.example.directive', [])
  .directive('example', example)
  .name;
