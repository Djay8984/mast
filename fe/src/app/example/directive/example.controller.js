import Base from 'app/base.class';

export default class ExampleController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
  }

  get example() {
    return 'Example';
  }
}
