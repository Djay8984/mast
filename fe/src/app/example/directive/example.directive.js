import ExampleController from './example.controller';
import templateUrl from './example.html';

const obj = {
  bindToController: true,
  controllerAs: 'vm',
  restrict: 'AE',
  templateUrl,
  controller: ExampleController,
  scope: {},
  link: (scope, element, attrs) => ({})
};

/* @ngInject */
/* eslint arrow-body-style: 0*/
export default () => {
  return obj;
};
