import * as _ from 'lodash';
import QDatastore from 'lib/nedbq';

const QUERY_REGEX = new RegExp(/\/query/i);

const HASH_DB_FILENAME = './hash-data.db';
const HASH_DB_TABLE_NAME = 'hashCache';
const HASH_ATTR = 'stalenessHash';

/*
  staleness hash interception

  description:
    hashes are used to determine whether or not the data has been changed since
    retrieving it from the api. these interceptors take this hash & cache it
    between the getting the data & sending it back modified.
  how all this works:
    hashes are stripped off single & list entities on get/query responses, these
    are cached locally & then synced with a local database using a constructed
    url as the key. on a modifying request, the hash is retrieved & added onto
    the payload. hashes are asynced to a db to preserve between refreshes for
    dev & offline handling
 */

/* @ngInject */
export default function run(
  Restangular
) {
  const db = new QDatastore({ filename: HASH_DB_FILENAME, autoload: true });
  const hashCache = {};
  load(db).then((loadedHashCache) => _.defaults(hashCache, loadedHashCache));

  Restangular.addResponseInterceptor(saveHash);
  Restangular.addRequestInterceptor(applyHash);

  /**
   * @param {qdatastore} qDataStore db to load hashCache from
   * @return {object} hashCache from the db
   */
  async function load(qDataStore) {
    const name = HASH_DB_TABLE_NAME;
    let hashCacheTable = await qDataStore.findOne({ name });

    if (_.isEmpty(hashCacheTable)) {
      hashCacheTable = { name, data: {} };
      await qDataStore.insert(hashCacheTable);
    }

    return hashCacheTable.data;
  }

  /**
   * syncs the db & the local cache
   */
  async function syncDBAndCache() {
    const name = HASH_DB_TABLE_NAME;
    await db.update({ name }, { name, data: hashCache });
  }

  /**
   * @param {object/array} data response body from api
   * @param {string} operation http/restangular method used
   * @param {string} what model being requested
   * @param {string} url where the response has come from
   * @return {object/array} slightly modified data
   * restangular response intercept method which saves the hash from singular
   *   data & list responses into the hash cache & strips it away from the data
   */
  function saveHash(data, operation, what, url) {
    // single entity
    if (_.has(data, HASH_ATTR)) {
      _.set(hashCache, url, _.get(data, HASH_ATTR));
      data = _.omit(data, HASH_ATTR);

    // list resource
    } else if (_.every(data, HASH_ATTR)) {
      _.forEach(data, (datum) =>
        _.set(hashCache,
          _.join([_.replace(url, QUERY_REGEX, ''), datum.id], '/'),
          _.get(datum, HASH_ATTR)));

      const pagination = data.pagination;
      data = _.map(data, (datum) => _.omit(datum, HASH_ATTR));
      data.pagination = pagination;
    }

    // async but not blocking (fingers crossed)
    syncDBAndCache();

    return data;
  }

  /**
   * @param {object} element what it being sent to the server
   * @param {string} operation http/restangular method used
   * @param {string} what model being requested
   * @param {string} url where the response has come from
   * @return {object/array} slightly modified element
   * restangular request intercept method which applies the saved hash from the
   *   hash cache
   */
  function applyHash(element, operation, what, url) {
    if (operation === 'put') {
      // if there's only one key & its value is an array, assume we're modifying
      // some list shimmed resource type thing
      const elementKeys = _.keys(element);
      if (_.size(elementKeys) === 1 && _.isArray(_.get(element, elementKeys))) {
        const type = _.last(_.split(url, '/'));
        const keyRegex = new RegExp(`^${type}List$`);
        const listKey = _.findKey(element, (value, key) => keyRegex.test(key));

        if (!_.isUndefined(listKey)) {
          _.set(element, listKey, _.map(_.get(element, listKey),
            (datum) => _.set(datum, HASH_ATTR,
              _.get(hashCache, _.join([url, datum.id], '/')))));
        }
      } else {
        _.set(element, HASH_ATTR, _.get(hashCache, url));
      }
    }

    return element;
  }
}
