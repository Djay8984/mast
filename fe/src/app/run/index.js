import * as angular from 'angular';

import main from './main.run';
import hashCache from './hash-cache.run';
import poller from './poller.run';

export default angular
  .module('app.run', [])
  .run(main)
  .run(hashCache)
  .run(poller)
  .name;
