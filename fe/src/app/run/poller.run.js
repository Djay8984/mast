import * as _ from 'lodash';

import templateUrl from 'app/components/modals/templates/confirmation-modal';

const PING_REGEX = new RegExp(/\/ping/i);

/* @ngInject */
export default function run(
  $rootScope,
  $timeout,
  $q,
  poller,
  Restangular,
  appConfig,
  apiPollingFrequency,
  MastModalFactory
) {
  // ping doesn't have a private endpoint, so needs to be addressed via http
  const pingRestangular = $rootScope.DEBUG ?
    Restangular
    .withConfig((customiser) =>
      customiser.setBaseUrl(
        _.replace(appConfig.MAST_API_URL, 'https', 'http'))) :
    Restangular;

  $rootScope.READY = pingRestangular.one('ping')
    .get()
    .then(true)
    .catch(false)
    .then((result) => {
      $rootScope.CONNECTED = result;
    });

  // pinging
  const pingPoller = poller.get(pingRestangular.one('ping'), {
    action: 'get',
    delay: 1000 / apiPollingFrequency,
    catchError: true
  });

  pingPoller.promise.then(null, null, (result) =>
    $rootScope.CONNECTED = !_.isNull(_.get(result, 'data')));

  // polling cacher, if other requests are valid, restarts pinging from last
  // successful request - stops spamming pings when actually using the app
  let pingPollerRestart = $q.defer();
  Restangular.addResponseInterceptor((data, operation, what, url, response) => {
    if (_.inRange(response.status, 100, 400) && !PING_REGEX.test(url)) {
      pingPoller.stop();
      $timeout.cancel(pingPollerRestart);
      pingPollerRestart = $timeout(
        () => pingPoller.restart(),
        1000 / apiPollingFrequency);
    }
    return data;
  });

  // disconnected modal
  const initialiseModal = () => new MastModalFactory({
    templateUrl,
    scope: {
      message: 'Online data is not available without an internet connection',
      actions: [
        { name: 'Return', result: true }
      ]
    }
  });
  let modal = initialiseModal();
  Restangular.addRequestInterceptor((element, operation, what, url) => {
    if ($rootScope.CONNECTED === false &&
      !PING_REGEX.test(url) &&
      !modal.isActive()) {
      modal = initialiseModal();
      modal.activate();
    }
    return element;
  });
}
