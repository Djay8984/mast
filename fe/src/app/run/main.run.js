import * as _ from 'lodash';

/* @ngInject */
export default function run(
  $rootScope,
  $q,
  node,
  Restangular,
  appConfig
) {
  $rootScope.DEBUG = Boolean(appConfig.MAST_DEBUG);
  node.registerRedirectListener();

  // hack: a bug with restangular means that it will try to assign its
  // additional properties onto typed arrays such as a buffer, resulting in an
  // empty buffer, so shim it to maintain buffer
  // https://github.com/mgonto/restangular/issues/1422
  Restangular.addResponseInterceptor((data, operation, what, url, response) =>
    _.get(response, 'config.responseType') === 'arraybuffer' ?
      { buffer: data } : data);


  const defaultHeaders = {};
  const configName = appConfig.MAST_USER_FULL_NAME;
  const configGroups = appConfig.MAST_GROUPS;
  if (configName) {
    defaultHeaders.user = configName;
  }
  if (configGroups) {
    defaultHeaders.groups = configGroups;
  }
  Restangular.setDefaultHeaders(defaultHeaders);

  /*
   * Override ES6 Promise methods to use the $q versions so Promise.
   * (and therefore async/await methods) trigger the $digest cycle
   * when necessary.
   *
   * The alias is necessary to get the correct handle on babel's
   * version of Promise.
   *
   * Note: the specs set Promise.qed to true so they won't rely on
   *       $q's behaviour.
   */
  const P = Promise;

  if (!P.qed) {
    P.qed = true;

    P.resolve = $q.resolve;
    P.reject = $q.reject;
  }
}
