import * as _ from 'lodash';
import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class Confirmation {
  constructor(
    config,
    data,
    MastModalFactory
  ) {
    this.config = config;
    this.data = data;
    this._MastModalFactory = MastModalFactory;
  }

  async addSuccess() {
    this.message = `You successfully added: ${_.get(this.data, 'title', '')}`;

    if (_.get(this.data, 'job.id', '')) {
      this.message += ` to job no. ${_.get(this.data, 'job.id', '')}`;
    }

    const anOrA =
      new RegExp('^[aeiouh]', 'i').test(this.config.item) ? 'an' : 'a';

    return this._MastModalFactory({
      templateUrl: confirmationTemplateUrl,
      scope: {
        title: `Successfully added ${anOrA} ${this.config.item}`,
        message: `${this.message}`,
        actions: [
          { name: 'OK', result: true }
        ]
      },
      class: 'dialog'
    }).activate();
  }
}
