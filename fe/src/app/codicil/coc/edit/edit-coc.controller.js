import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/coc/coc.input';

export default class EditCoCController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    codicilDecisionService,
    referenceDataService,
    MastModalFactory,
    coc,
    cocStatus
  ) {
    super(arguments);

    this.data = this._coc;

    this.config = {
      title: 'Edit Condition of Class',
      context: this._$stateParams.context,
      actionType: 'edit'
    };

    this.header = new Header(this);
    this.footer = new Footer(this);

    this.model = new Input(this);
    this.model.setValues();
  }

  //  -----------------------------------------------------------
  redirect() {
    this.header.redirect(false);
  }

  //  -----------------------------------------------------------
  async save() {
    await this.model.save();
    this.header.redirect(true);
  }

  //  -----------------------------------------------------------
  async back() {
    const result = await this.model.leaveConfirm();
    return this[result] && this[result]();
  }
}
