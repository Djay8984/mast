import Base from 'app/base.class';
import * as _ from 'lodash';
import Header from 'app/codicil/header.class';
import View from 'app/codicil/coc/coc.view';
import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewCoCController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    $rootScope,
    $scope,
    moment,
    printService,
    codicilDecisionService,
    employeeDecisionService,
    navigationService,
    MastModalFactory,
    asset,
    coc,
    cocStatus,
    codicilStatuses,
    job,
    user
  ) {
    super(arguments);

    this._status = this._cocStatus;
    this.data = this._coc;

    this.config = {
      title: 'Condition of Class',
      context: this._$stateParams.context
    };


    this.header = new Header(this);
    this.header.getActions();

    this.view = new View(this);
    this.view.setValues();

    this.printPayload = _.extend(
     {},
     this.data,
     { additionalInfo: ['defect', 'repairs'] }
    );
  }
}
