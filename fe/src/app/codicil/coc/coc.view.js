import * as _ from 'lodash';
import Methods from 'app/codicil/input.methods';

export default class View extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.type = 'CoC';
    this.slug = 'coc';
    this.parent = parent;
    this.config = parent.config;

    this.list = [
      { extend: 'view.id' },
      {
        templateUrl: 'view/yes-no',
        label: 'Inherited',
        name: 'inheritedFlag'
      },
      {
        templateUrl: 'view/text',
        label: 'Description',
        name: 'description'
      },
      {
        templateUrl: 'view/date',
        label: 'Imposed Date',
        name: 'imposedDate'
      },
      {
        templateUrl: 'view/date',
        label: 'Due Date',
        name: 'dueDate'
      },
      {
        templateUrl: 'view/text',
        label: 'Visible for',
        name: 'confidentialityType.description'
      },
      {
        templateUrl: 'view/attachments',
        label: 'Attachments & notes',
        modalTitle: 'Condition of Class',
        path: this.path
      },
      {
        templateUrl: 'view/defect',
        name: 'defect',
        label: 'Linked defect',
        type: 'defect',
        autoHide: true,
        viewState: this.config.context === 'asset' ?
          'defect.view' : 'crediting.defects.view',
        viewParams: {
          backTarget: this.stateName,
          backTargetId: _.get(this.data, 'id'),
          jobId: _.get(this.data, 'job.id'),
          assetId: _.get(this.data, 'asset.id'),
          codicilId: _.get(this.data, 'defect.id'),
          context: this.config.context
        }
      },
      {
        templateUrl: 'view/list-repairs',
        name: 'repairs',
        label: 'Repairs',
        type: 'repair',
        autoHide: true,
        select: (item) =>
          this._$state.go(this.repairState, _.merge(
            this._$stateParams,
            {
              backTarget: this._$state.current.name,
              codicilId: item.id,
              parentId: this.data.id,
              repairType: 'coc'
            }
          ))
      }
    ];
  }

  get repairState() {
    return this.config.context === 'asset' ? 'repair.view' :
      'crediting.repair.view';
  }
}
