import * as _ from 'lodash';
import Base from 'app/base.class';
import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/coc/coc.input';
import Confirmation from 'app/codicil/confirmation';

export default class AddCoCController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    asset,
    attachmentDecisionService,
    cocStatus,
    codicilDecisionService,
    defect,
    MastModalFactory,
    moment,
    navigationService,
    referenceDataService
  ) {
    super(arguments);

    this.config = {
      item: 'condition of class',
      title: 'Add Condition of Class',
      context: this._$stateParams.context,
      actionType: 'add',
      hideInherited: this._$state.current.name ===
        'crediting.defects.view.addCoc'
    };

    const { jobId, assetId } = this._$stateParams;

    this.data = {
      asset: assetId ? { id: assetId } : null,
      category: { id: 12 }, // temporary, see LRC-328
      defect: null,
      description: _.get(defect, 'promptThoroughRepair') ?
        'Prompt & thorough' : null,
      inheritedFlag: false,
      job: jobId ? { id: jobId } : null,
      jobScopeConfirmed: false,
      status: { id: this._cocStatus.get('open') }
    };

    if ($stateParams.defectId) {
      const key = _.startsWith($stateParams.defectId, '-') ? '_id' : 'id';
      _.merge(this.data,
        _.set({}, ['defect', `${key}`], $stateParams.defectId));
    }

    this.header = new Header(this);
    this.model = new Input(this);
    this.model.setValues();
    this.footer = new Footer(this);

    this.confirmation = new Confirmation(
      this.config,
      this.data,
      MastModalFactory
    );
  }

  async save() {
    const res = await this.model.add();
    this._$stateParams.codicilId = res.id;

    if (await this.confirmation.addSuccess()) {
      this.header.redirect();
    }
  }
}
