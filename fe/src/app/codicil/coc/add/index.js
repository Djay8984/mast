import * as angular from 'angular';

export default angular
  .module('app.codicil.coc.add', [])
  .name;
