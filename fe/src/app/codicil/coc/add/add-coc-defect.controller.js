import * as _ from 'lodash';

import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/coc/coc.input';

export default class AddCoCController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    asset,
    job,
    codicilDecisionService,
    cocStatus,
    cocCategory,
    defectCategory,
    defectDecisionService
  ) {
    super(arguments);

    this.config = {
      actionType: 'add',
      title: 'Add Condition of Class',
      context: this._$stateParams.context
    };

    this.header = new Header(this);
    this.model = new Input(this);
    this.footer = new Footer(this);
  }

  async save() {
    const params = {
      assetId: this._$stateParams.assetId,
      jobId: this._$stateParams.jobId,
      defectId: this._$stateParams.defectId,
      context: this._$stateParams.context
    };

    const defect = await this._defectDecisionService.get(params);
    const categoryId = _.isNull(defect.defectCategory.parent) ?
      defect.defectCategory.id : defect.defectCategory.parent.id;

    const finalCategory =
      this._cocCategory.get(
        _.first(Object.keys(
          this._defectCategory.filter((x) => x === categoryId).toObject())));

    const payload = {
      ...this.model.values,
      status: { id: this._cocStatus.get('open') },
      defect: {
        [this._$stateParams.defectId > 0 ? 'id' : '_id']:
          this._$stateParams.defectId
      },
      jobScopeConfirmed: this._job.scopeConfirmed,
      inheritedFlag: !this.config.hideInherited,
      category: finalCategory
    };

    await this._codicilDecisionService.saveWipCocForDefect(
      this._$stateParams.jobId, this._$stateParams.defectId, payload);
    this.header.redirect();
  }
}

