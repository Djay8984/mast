import * as _ from 'lodash';
import Methods from 'app/codicil/input.methods';

export default class Input extends Methods {

  constructor(parent) {
    super(parent.data, parent);

    this.type = 'CoC';
    this.slug = 'coc';
    this.parent = parent;
    this.config = parent.config;
    this.config.hideInherited = !_.isNull(this._$stateParams.defectId);

    // Unless the CoC was created in the job scope we can only
    // edit the due date and description (10.33/10.47)
    const editingCoCFromOutsideJobScope =
      (!(_.get(this, 'data.route') === 'wip-coc') ||
        _.get(this, 'data.parent')) &&
        _.get(this, 'data.id');

    // For editing CoC inside job scope, for inherited or non-defect CoC,
    // only Description and Due Date are editable
    const disabled = editingCoCFromOutsideJobScope ||
      parent.config.actionType === 'edit' &&
        (parent.data.inheritedFlag || parent.data.defect);

    this.children = [
      { extend: 'view.id' },
      { extend: 'input.title', disabled },
      { extend: 'input.description' },
      {
        templateUrl: 'input/checkbox',
        name: 'inheritedFlag',
        label: 'Inherited',
        disabled,
        inactive: this.config.hideInherited
      },
      { extend: 'input.imposedDate', disabled },
      { extend: 'input.dueDate' },
      { extend: 'input.confidentialityType', disabled,
        $set: {
          list: {
            service: 'confidentialityType',
            refData: 'attachment.confidentialityTypes',
            result: (res) => _.map(res.refData,
              (val) => ({
                id: val.id,
                name: val.description,
                disabled: val.id === res.service.get('lrInternal')
              })
            )
          }
        }
      },
      { extend: 'input.attachments' }
    ];
  }
}
