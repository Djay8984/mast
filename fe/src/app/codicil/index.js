import * as angular from 'angular';

import defect from './defect';
import repair from './repair';

export default angular
  .module('app.codicil', [
    defect,
    repair
  ])
  .name;
