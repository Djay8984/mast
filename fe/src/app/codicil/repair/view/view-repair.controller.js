import * as _ from 'lodash';

import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import View from 'app/codicil/repair/repair.view';

export default class ViewRepairController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    codicilDecisionService,
    MastModalFactory,
    asset,
    defectCategory,
    job,
    repair
  ) {
    super(arguments);

    this.data = this._repair;

    this.config = {
      title: 'View Repair',
      context: this._$stateParams.context
    };

    this.header = new Header(this);
    this.header.getActions();

    this.view = new View(this);
    this.view.setValues();
  }

  back() {
    // Required for the parent page to load and navigate properly
    this._$stateParams.codicilId = this._$stateParams.parentId;

    this._$state.go(this._$stateParams.backTarget || '^.^',
      _.omit(this._$stateParams, 'backTarget'), { reload: true });
  }
}
