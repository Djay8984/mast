import * as _ from 'lodash';
import Methods from 'app/codicil/input.methods';

export default class Input extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.type = 'Repair';
    this.slug = 'repair';
    this.parent = parent;
    this.config = parent.config;

    this.list = [
      {
        templateUrl: 'input/select-item',
        name: 'repairs',
        label: 'Items',
        labelProperty: 'item.name',
        $set: {
          list: this.config.itemsNotPermanentlyRepaired },
        validate: { required: true },
        inactive: this.config.type.toLowerCase() !== 'defect'
      }, {
        templateUrl: 'input/select-modal',
        name: 'repairTypes',
        label: 'Repair type',
        multi: true,
        $set: { list: { refData: 'repair.repairTypes' } },
        validate: { required: true }
      }, {
        templateUrl: 'input/select-modal',
        name: 'materialsUsed',
        label: 'Materials used',
        multi: true,
        $set: { list: { refData: 'repair.materials' } },
        validate: { required: true },
        inactive: this.config.type === 'Defect' &&
          !this.defectCategoryIsMachinery(this.parent._parent) || true
      }, {
        templateUrl: 'input/radio-buttons',
        name: 'repairAction.id',
        label: 'Action taken',
        $set: {
          list: {
            refData: 'repair.repairActions',
            result: (res) => _.reject(
              _.forEach(res.refData, (val) => val.name = val.description),
              (val) => _.includes(this.config.omittedRepairActions, val.id))
          }
        },
        validate: { required: true }
      }, {
        templateUrl: 'input/textarea',
        name: 'description',
        label: 'Description',
        validate: {
          required: true,
          type: 'string'
        },
        $set: {
          placeholder: {
            service: 'codicilDescriptionLength',
            result: (res) => `${res.service} character limit`
          },
          ['validate.maxLength']: {
            service: 'codicilDescriptionLength'
          }
        }
      }, {
        templateUrl: 'input/attachments',
        name: 'attachments',
        label: 'Attachments',
        modalTitle: this.config.title,
        path: this.path
      }
    ];
  }

  defectCategoryIsMachinery(defect) {
    // assumes expanded defect category hierarchy
    const recurse = (category) => {
      if (category.id === this.config.defectCategoryMachinery) {
        return true;
      } else if (_.has(category, 'parent.id')) {
        return recurse(category.parent);
      }
    };

    if (defect.defect) {
      return recurse(defect.defect.defectCategory);
    }

    return recurse(defect.defectCategory);
  }
}
