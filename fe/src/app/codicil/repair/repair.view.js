import * as _ from 'lodash';
import Methods from 'app/codicil/view.methods';

export default class View extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.type = 'Repair';
    this.slug = 'repair';
    this.parent = parent;
    this.config = parent.config;

    this.list = [
      {
        templateUrl: 'view/text',
        label: 'ID',
        value: _.padStart(this.data.id, 8, 0)
      },
      {
        templateUrl: 'view/list-items',
        label: 'Items',
        value: this._$stateParams.repairType === 'defect' ?
          _.map(this.data.repairs, 'item.item') : null,
        autoHide: true
      },
      {
        templateUrl: 'view/date',
        label: 'Date added',
        name: 'dateAdded'
      },
      {
        templateUrl: 'view/list-items',
        label: 'Repair types',
        name: this.isMachineryDefect ? 'repairTypes' : '',
        autoHide: true
      },
      {
        templateUrl: 'view/text',
        label: 'Action taken',
        name: 'repairAction.name'
      },
      {
        templateUrl: 'view/attachments',
        label: 'Attachments & notes',
        modalTitle: 'Repair',
        path: this.path
      },
      {
        templateUrl: 'view/text',
        label: 'Description',
        name: 'description'
      },
      {
        templateUrl: 'view/defect',
        name: 'defect',
        label: 'Linked defect',
        type: 'defect',
        autoHide: true,
        viewState: this.config.context === 'asset' ?
          'defect.view' : 'crediting.defects.view',
        viewParams: {
          jobId: _.get(this, '_job.id'),
          assetId: _.get(this, '_asset.id'),
          codicilId: _.get(this.data, 'defect.id') ||
            _.get(this.data, 'defect._id'),
          context: this.config.context
        }
      },
      {
        templateUrl: 'view/defect',
        name: 'codicil',
        label: 'Linked CoC',
        type: 'coc',
        autoHide: true,
        viewState: this.config.context === 'asset' ?
          'coc.view' : 'crediting.cocs.view',
        viewParams: {
          jobId: _.get(this, '_job.id'),
          assetId: _.get(this, '_asset.id'),
          codicilId: _.get(this.data, 'codicil.id') ||
            _.get(this.data, 'codicil._id'),
          context: this.config.context
        }
      }
    ];
  }

  /**
   * @returns {Boolean} whether or not the repair's defect's defectCategory is
   *  'machinery' or one of its sub-categories
   */
  get isMachineryDefect() {
    return _.includes(_.values(_.pick(this._defectCategory.toObject(),
      ['machinery', 'machineryContact',
      'machineryStructural', 'machineryMiscellaneous',
      'machineryEquipment', 'machineryMechanical'])),
    _.get(this.data, 'defect.defectCategory.id'));
  }

  /**
   * Repairs have a more complicated path than codicils, so override the parent
   * function with one specific to repairs.
   * @return {string} the attachment path
   */
  get path() {
    const params = this._$stateParams;
    const path = {
      asset: `asset/${params.assetId}/${params.repairType}/` +
        `${params.parentId}/repair/${params.codicilId}`,
      job: `job/${params.jobId}/wip-${params.repairType}/` +
        `${params.parentId}/wip-repair/${params.codicilId}`
    };

    return path[this.config.context] || path.asset;
  }
}
