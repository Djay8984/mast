import * as angular from 'angular';

import add from './add';
import view from './view';

export default angular
  .module('app.codicil.repair', [
    add,
    view
  ])
  .name;
