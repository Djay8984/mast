import * as _ from 'lodash';

import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/repair/repair.input';

import templateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class AddRepairController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    asset,
    attachmentDecisionService,
    codicilDescriptionLength,
    codicilTitleLength,
    confidentialityTypeIdToHide,
    confidentialityTypes,
    defectCategory,
    job,
    MastModalFactory,
    moment,
    navigationService,
    parent,
    repairAction,
    repairDecisionService,
    refServiceCatalogue,
    siblings,
    surveyDecisionService,
    surveyType
  ) {
    super(arguments);

    const context = this._$stateParams.context;
    this._$stateParams.repairType =
      _.isUndefined(this._parent.defectCategory) ? 'coc' : 'defect';

    const type = this._$stateParams.repairType;
    this._$stateParams.parentId = parent.id;

    this.config = {
      context,
      type,
      title: 'Add Repair',
      omittedRepairActions: [repairAction.get('modification')],
      itemsNotPermanentlyRepaired: _.reject(parent.items,
        (item) => _.find(
          _.filter(siblings,
            { repairAction: { id: this._repairAction.get('permanent') } }),
          (repair) => _.find(repair.repairs,
            { item: { item: { id: item.item.id } } }))),
      defectCategoryMachinery: defectCategory.get('machinery')
    };

    this.data = {
      repairs: [],
      repairTypes: [],
      promptThoroughRepair: false,
      confirmed: context === 'job',
      [type === 'coc' ? 'codicil' : 'defect']:
        _.isNull(parent.id) && _.parseInt(parent._id) < 0 ?
          { id: null, _id: parent._id } :
          { id: parent.id, _id: _.toString(parent.id) },
      parent: null
    };

    this.header = new Header(this);
    this.model = new Input(this);
    this.footer = new Footer(this);
  }

  // check to see if repair route to follow is on a defect or coc
  async save() {
    await (this._$stateParams.repairType === 'defect' ?
      this.saveRepairOnDefect() :
      this.saveRepairOnCoc());
    this.redirect();
  }

  // repair being done on a defect, determine whether to follow route for a
  // temporary or permanent repair
  async saveRepairOnDefect() {
    const values = this.model.values;
    values.repairs = _.map(values.repairs, (repair) =>
      ({ item: { id: repair.id, item: { id: repair.item.id } } }));

    await (_.get(values, 'repairAction.id') ===
      this._repairAction.get('permanent') ?
        this.savePermanentDefectRepair(values) :
        this.saveTemporaryDefectRepair(values));
  }

  // permanent repair to be carried out on a defect
  async savePermanentDefectRepair(values) {
    if (_.get(values, 'repairAction.id') ===
      this._repairAction.get('permanent') &&
      this.allOtherItemsPermanentlyRepaired(
        _.map(values.repairs, 'item.item.id'))) {
      if (!(await new this._MastModalFactory({
        templateUrl,
        scope: {
          title: 'This defect will be closed',
          message: `By applying this permanent repair there will now be a
          permanent repair against all items in the defect, and so this
          defect will be closed.\n
          Are you sure that you want to continue adding this repair?`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Continue', result: true }
          ]
        },
        class: 'dialog'
      }).activate())) {
        return;
      }
      await this.generateDefectSurvey(values);
      await this.saveDefectRepair(values);
    }
  }

  // save the temporary repair and show modal
  async saveTemporaryDefectRepair(values) {
    if (!(await new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Adding temporary repair',
        message: 'Repair saved',
        actions: [ { name: 'Continue' } ]
      },
      class: 'dialog'
    }).activate())) {
      await this.saveDefectRepair(values);
    }
  }

  // generate the survey for a permanent defect repair
  async generateDefectSurvey(values) {
    _.set(
      values,
      'survey',
      await this._surveyDecisionService.generateDefectSurvey(
        this._job.id,
        this._surveyType.get('repair'),
        this._parent.defectCategory.id,
        this._asset.ruleSet.id
      )
    );
  }

  // save the defect repair
  async saveDefectRepair(values) {
    const cleanValues = _.omitBy(values, _.isNil);
    await this._repairDecisionService.save(
      this._$stateParams.repairType, this._$stateParams, cleanValues);
  }

  // repair being done on a coc, determine whether to follow route for a
  // temporary or permanent repair
  async saveRepairOnCoc() {
    await (_.get(this.model.values, 'repairAction.id') ===
      this._repairAction.get('permanent') ?
        this.savePermanentRepairOnCoc() :
        this.saveTemporaryRepairOnCoc());
  }

  // permanent repair on a coc, determines whether to follow route for a
  // defect related coc or a standard coc
  async savePermanentRepairOnCoc() {
    if (this.cocIsNotDefectRelated()) {
      await this.cocPermanentRepairNonDefectRelated();
    } else {
      await this.cocPermanentRepairOnDefect();
    }
  }

  // checks if the coc is linked to a defect
  cocIsNotDefectRelated() {
    return _.isNil(this._parent.defect);
  }

  // defect related coc permanent repair
  // additional logic to be added here to determine if this needs to close the
  // defect
  async cocPermanentRepairOnDefect() {
    await this.generateCocSurvey();
    await this.saveCocRepair();
  }

  // coc permanent repair not linked to a defect
  async cocPermanentRepairNonDefectRelated() {
    await this.generateCocSurvey();
    await this.saveCocRepair();
  }

  // coc temporary repair
  async saveTemporaryRepairOnCoc() {
    await this.saveCocRepair();
  }

  // this determines which route to follow, if the coc is on a survey or not
  // as they will not have access to the same object properties
  async generateCocSurvey() {
    const values = this.model.values;
    await (_.isNil(this._parent.category) ?
      this.generateCocSurveyOnDefect(values) :
      this.generateStandardCocSurvey(values));
  }

  // coc linked to a defect will have access to the category letter of the
  // parent defect
  async generateCocSurveyOnDefect(values) {
    const categoryLetter =
      _.isNil(this._parent.defect.defectCategory.parent) ?
        'null' : this._parent.defect.defectCategory.parent.categoryLetter;
    _.set(
      values,
      'survey',
      await this._surveyDecisionService.generateCocSurvey(
        this._job.id,
        this._surveyType.get('repair'),
        categoryLetter,
        this._asset.ruleSet.id
      )
    );
  }

  // a general coc survey to be created, takes the last letter of the
  // description before appending RPS in the surveyDecisionService
  async generateStandardCocSurvey(values) {
    _.set(
      values,
      'survey',
      await this._surveyDecisionService.generateCocSurvey(
        this._job.id,
        this._surveyType.get('repair'),
        this._parent.category.description.slice(-1),
        this._asset.ruleSet.id
      )
    );
  }

  // saves the coc repair
  async saveCocRepair() {
    const cleanValues = _.omitBy(this.model.values, _.isNil);
    this._repairDecisionService.save(
      this._$stateParams.repairType, this._$stateParams, cleanValues);
  }

  redirect() {
    this._$state.go('^', this._$stateParams, { reload: true });
  }

  /**
   * @param {array} items list of item ids permanently repaired by this repair
   * @return {boolean} whether or not all of the parent defect's items are
   *   permanently repaired
   */
  allOtherItemsPermanentlyRepaired(items) {
    return _.chain(this.config.itemsNotPermanentlyRepaired)
      .map('item.id')
      .difference(items)
      .isEmpty()
      .value();
  }
}
