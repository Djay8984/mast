import * as _ from 'lodash';

export default class ExtendItems {
  constructor() {
    this.view = {
      id() {
        return {
          inactive: !_.get(this.data, 'id'),
          templateUrl: 'view/text',
          label: 'ID',
          value: this.buildId()
        };
      }
    };

    this.input = {
      title() {
        return {
          templateUrl: 'input/input',
          name: 'title',
          label: 'Title',
          validate: {
            required: true,
            autofocus: true,
            type: 'string'
          },
          $set: {
            ['validate.maxLength']: 'codicilTitleLength',
            placeholder: {
              service: 'codicilTitleLength',
              result: (res) => `${res.service} character limit`
            }
          }
        };
      },

      category() {
        return {
          templateUrl: 'input/radio-buttons',
          name: 'category.id',
          label: 'Category',
          validate: {
            required: true
          },
          $set: {
            list: {
              constant: 'codicilType',
              param: this.type || 'actionableItem',
              refData: 'asset.codicilCategories',
              result: ({ refData, constant }) =>
                _.filter(refData, { codicilTypeId: { id: constant } })
            }
          }
        };
      },

      description() {
        return {
          templateUrl: 'input/textarea',
          name: 'description',
          label: 'Description',
          validate: {
            required: true,
            type: 'string'
          },
          $set: {
            placeholder: {
              service: 'codicilDescriptionLength',
              result: (res) => `${res.service} character limit`
            },
            ['validate.max']: 'codicilDescriptionLength'
          }
        };
      },

      surveyorGuidance() {
        return {
          templateUrl: 'input/textarea',
          name: 'surveyorGuidance',
          label: 'Surveyor guidance',
          validate: {
            type: 'string'
          },
          $set: {
            placeholder: {
              service: 'codicilSurveyorGuidanceLength',
              result: (res) => `${res.service} character limit`
            },
            ['validate.maxLength']: {
              service: 'codicilSurveyorGuidanceLength'
            }
          }
        };
      },

      selectTemplate() {
        return {
          templateUrl: 'input/select-template',
          name: 'template',
          header: this.config.title,
          title: 'Find an Actionable Item Template',
          label: 'Select template',
          search: { status: 1 },
          codicilType: {
            name: this.slug
          },
          $set: {
            ['codicilType.id']: {
              constant: 'codicilType',
              param: this.type
            }
          },
          select(values) {
            const model = this.$getModel();
            model.data.itemType = values.itemType;
            values.requireApproval = !values.editableBySurveyor;
            model.setValues(values, model.notDisable);
            model.setDisabled(true, model.notDisable);
          },
          onInit(data) {
            const model = this.$getModel();
            if (model.data.template) {
              model.setDisabled(true, model.notDisable);
            }
          }
        };
      },

      assetItem(item) {
        return {
          inactive: this.config.batchAction,
          templateUrl: 'input/item',
          name: 'assetItem',
          validate: {
            requiredWhen: 'template',
            blur: false
          },
          label: 'Item',
          typeName: 'actionable item',
          assetId: this.params.assetId,
          get itemType() {
            const tpl = item.$get('template') || {};
            return tpl.value && _.get(tpl, 'selected.itemType');
          }
        };
      },

      imposedDate() {
        return {
          templateUrl: 'input/datepicker',
          name: 'imposedDate',
          label: 'Imposed date',
          validate: {
            required: true,
            oldErrorMsg: {
              nonExistentDate: 'This date does not exist.',
              dateFormat:
                'Date is not valid. Must be in the format \'DD MMM YYYY\'',
              min: 'The imposed date cannot be in the past.',
              isDateLength:
                'Date length is not valid. ' +
                'Must be in the format \'DD MMM YYYY\''
            }
          },
          $set: {
            ['validate.min']: {
              service: 'moment',
              result: ({ service }) => service().add(-1, 'm').startOf('m')
            }
          }
        };
      },

      dueDate() {
        return {
          templateUrl: 'input/datepicker',
          name: 'dueDate',
          label: 'Due date',
          validate: {
            required: !this.config.batchAction,
            oldErrorMsg: {
              nonExistentDate: 'This date does not exist.',
              dateFormat:
                'Date is not valid. Must be in the format \'DD MMM YYYY\'',
              min: 'The due date cannot be before the imposed date.',
              isDateLength:
                'Date is not valid. Must be in the format \'DD MMM YYYY\''
            }
          },
          $set: {
            disabled: {
              watch: 'imposedDate',
              result: (res) => !res.value
            },
            ['validate.min']: {
              watch: 'imposedDate',
              service: 'moment',
              result: (res, data) => {
                const dateToNr = (val) => Number(res.service(val));

                if (res.value && data.value &&
                  dateToNr(data.value) < dateToNr(res.value)) {
                  data.value = '';
                }
                return res.value || res.service();
              }
            }
          }
        };
      },

      confidentialityType(item) {
        return {
          templateUrl: 'input/radio-buttons',
          name: 'confidentialityType.id',
          label: 'Visible for',
          modifyList: (val) => ({ id: val.id, name: val.description }),
          $set: {
            list: {
              refData: 'attachment.confidentialityTypes',
              result: (res, self) => _.map(res.refData, self.modifyList)
            }
          },
          validate: {
            required: true
          }
        };
      },

      attachments() {
        return {
          templateUrl: 'input/attachments',
          name: 'attachments',
          label: 'Attachments',
          modalTitle: this.config.title || 'Add Actionable Item',
          path: this.path
        };
      },

      requireApproval() {
        return {
          templateUrl: 'input/checkbox',
          name: 'requireApproval',
          label: 'Requires approval for change',
          value: false,
          validate: {
            required: true
          }
        };
      }
    };
  }
}
