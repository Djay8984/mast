export default class Footer {
  constructor(parent) {
    this.parent = parent;
    this.templateUrl = 'footer/buttons';

    this.buttons = [
      {
        name: 'Cancel',
        action: () => parent.header.back(),
        class: 'transparent-dark-button'
      }, {
        name: 'Save',
        action: () => this.parent.save(),
        class: 'primary-button',
        get disabled() {
          return parent.form && parent.form.$invalid && parent.form.$pristine;
        }
      }
    ];
  }
}
