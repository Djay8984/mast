import * as _ from 'lodash';

import Base from 'app/base.class';
import PrintMixin from 'app/components/print/print.mixin';

const DEFECT_DETAIL_ID_DEFECT_TYPE = 1;
const DEFECT_DETAIL_ID_DEFECT_DESCRIPTOR = 2;
const DEFECT_DETAIL_ID_ASSET_LOADING_CONDITION = 3;
const DEFECT_DETAIL_ID_ASSET_LOCATION = 4;
const DEFECT_DETAIL_ID_ASSET_ACTIVITY = 5;
const DEFECT_DETAIL_ID_TRANSVERSE_LOCATION = 7;
const DEFECT_DETAIL_ID_LONGITUDINAL_LOCATION = 6;
const DEFECT_DETAIL_ID_GROUNDING_INVOLVED = 8;
const DEFECT_DETAIL_ID_ONBOARD_LOCATION = 9;
const DEFECT_DETAIL_ID_POLLUTION_TYPE = 10;
const DEFECT_DETAIL_ID_FIRE_EXPLOSION = 11;


/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewDefectController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $scope,
    $rootScope,
    $log,
    $state,
    $stateParams,
    MastModalFactory,
    navigationService,
    dateFormat,
    defectStatus,
    asset,
    cocs,
    defect,
    refDefectDetails,
    printService,
    repairs
   ) {
    super(arguments);

    this.listRepairTypes();

    this.printPayload = _.extend(
      {},
      this._defect,
      {
        coc: this._cocs,
        repairs: this._repairs,
        additionalInfo: ['coc', 'repairs']
      }
    );
  }

  back() {
    this._navigationService.back();
  }

  listRepairTypes() {
    _.forEach(this._repairs, (repair) => {
      repair.allRepairTypes = _.join(_.map(repair.repairTypes, 'name'), ', ');
    });
  }

  get refDefectDetails() {
    return _.get(this._refDefectDetails, 'defect.defectDetails');
  }

  get defectValues() {
    return _.map(this.defect.values, 'defectValue');
  }

  get defectType() {
    const defectTypes = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_DEFECT_TYPE
    }), 'values');

    // Any of these ids in defect values
    const defectTypeId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
      _.map(defectTypes, 'id')));
    if (defectTypeId) {
      return _.capitalize(_.find(this.defectValues,
        { id: defectTypeId }).name);
    }
  }

  get defectDescriptor() {
    const defectDescriptors = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_DEFECT_DESCRIPTOR
    }), 'values');

    // Any of these ids in defect values
    const defectDescriptorId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
        _.map(defectDescriptors, 'id')));
    if (defectDescriptorId) {
      return _.capitalize(_.find(this.defectValues,
        { id: defectDescriptorId }).name);
    }
  }

  get assetLocation() {
    const assetLocations = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_LOCATION
    }), 'values');

    // Any of these ids in defect values
    const locationId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
      _.map(assetLocations, 'id')));
    if (locationId) {
      const assetLocation = _.find(this.defect.values,
        { 'defectValue': { 'id': locationId } });
      return _.capitalize(assetLocation.otherDetails ||
        assetLocation.defectValue.name);
    }
  }

  get assetActivity() {
    const assetActivities = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_ACTIVITY
    }), 'values');

    // Any of these ids in defect values
    const activityId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
      _.map(assetActivities, 'id')));
    if (activityId) {
      const assetActivity = _.find(this.defect.values,
        { 'defectValue': { id: activityId } });
      return _.capitalize(assetActivity.otherDetails ||
        assetActivity.defectValue.name);
    }
  }

  get longitudinalLocations() {
    const longitudinalLocationValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_LONGITUDINAL_LOCATION
    }), 'values');

    const longitudeLocationIds = _.filter(
        _.intersection(_.map(this.defectValues, 'id'),
          _.map(longitudinalLocationValues, 'id')));

    let longitudinalLocations = [];

    if (longitudeLocationIds) {
      longitudinalLocations = _.filter(this.defect.values, (value) =>
          _.includes(longitudeLocationIds, value.defectValue.id)
        );
    }
    return longitudinalLocations;
  }

  get transverseLocations() {
    const transverseLocationValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_TRANSVERSE_LOCATION
    }), 'values');

    const transverseLocationIds = _.filter(
        _.intersection(_.map(this.defectValues, 'id'),
          _.map(transverseLocationValues, 'id')));

    let transverse = [];

    if (transverseLocationIds) {
      transverse = _.filter(this.defect.values, (value) =>
        _.includes(transverseLocationIds, value.defectValue.id));
    }
    return transverse;
  }

  get loadingCondition() {
    const loadingConditions = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_LOADING_CONDITION
    }), 'values');

    // Any of th sese ids in defect values
    const loadingId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
      _.map(loadingConditions, 'id')));
    if (loadingId) {
      return _.capitalize(_.find(this.defectValues, { id: loadingId }).name);
    }
  }

  get onBoard() {
    let onBoard = [];
    const onBoardValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ONBOARD_LOCATION
    }), 'values');

    const onBoardIds = _.filter(
    _.intersection(_.map(this.defectValues, 'id'),
      _.map(onBoardValues, 'id')));

    if (onBoardIds) {
      onBoard = _.filter(this.defect.values, (value) =>
         _.includes(onBoardIds, value.defectValue.id)
      );
    }
    return onBoard;
  }

  get groundingInvolved() {
    const groundingValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_GROUNDING_INVOLVED
    }), 'values');

    // go through each item in array and pull out the defect values

    // Any of those ID's in defect values
    const groundingId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
        _.map(groundingValues, 'id')));
    if (groundingId) {
      return _.capitalize(_.find(this.defectValues,
        { id: groundingId }).name);
    }
    return 'No';
  }

  get pollutionTypes() {
    let pollution = [];
    const pollutionValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_POLLUTION_TYPE
    }), 'values');

    const pollutionIds = _.filter(
      _.intersection(_.map(this.defectValues, 'id'),
        _.map(pollutionValues, 'id')));

    if (pollutionIds) {
      pollution = _.filter(this.defect.values, (value) =>
        _.includes(pollutionIds, value.defectValue.id)
      );
    }
    return pollution;
  }

  get fireExplosion() {
    const fireExplosionValues = _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_FIRE_EXPLOSION
    }), 'values');

    // go through each item in array and pull out the defect values

    // Any of those ID's in defect values
    const explosionId =
      _.first(_.intersection(_.map(this.defectValues, 'id'),
        _.map(fireExplosionValues, 'id')));
    if (explosionId) {
      return _.capitalize(_.find(this.defectValues,
        { id: explosionId }).name);
    }
    return 'No';
  }

  get enableCourseOfActionCTAs() {
    return _.includes(
      _.values(_.pick(this._defectStatus.toObject(), ['open', 'deleted'])),
      this._defect.defectStatus.id) && this._$stateParams.context !== 'asset';
  }

  get dateFormat() {
    return this._dateFormat;
  }

  get asset() {
    return this._asset;
  }


  /**
   * @return {array} flattened hierarchy of defect categories from the top
   */
  get flatCat() {
    const categories = [];
    const getCategory = (category) => {
      categories.push(category);
      if (_.has(category, 'parent.id')) {
        getCategory(category.parent);
      }
    };

    getCategory(this._defect.defectCategory);
    return _.reverse(categories);
  }

  get totalCoCs() {
    return _.get(this._cocs, 'pagination.totalElements', 0);
  }

  get cocs() {
    return this._cocs;
  }

  get viewCocState() {
    return this._$stateParams.context === 'job' ?
      'crediting.cocs.view' :
      'coc.view';
  }

  get viewRepairState() {
    return this._$stateParams.context === 'job' ?
      'crediting.repairs.view' :
      'repair.view';
  }

  get totalRepairs() {
    return _.get(this._repairs, 'pagination.totalElements', 0);
  }

  get repairs() {
    return this._repairs;
  }

  get attachmentPath() {
    return this._$stateParams.context === 'job' ?
      `job/${this._$stateParams.jobId}` +
        `/wip-defect/${this._$stateParams.codicilId}` :
      `asset/${this._$stateParams.assetId}` +
        `/defect/${this._$stateParams.codicilId}`;
  }

  addCodicil(type) {
    const path = _.get({
      actionableItem: 'actionableItem.add',
      coc: 'crediting.defects.add.addCoc',
      repair: 'crediting.defects.view.addRepair'
    }, _.camelCase(type));
    this._$state.go(path, _.merge(
      _.omit(this._$stateParams, 'codicilId'), {
        defectId: this._defect.id || this._defect._id
      }));
  }

  get defect() {
    return this._defect;
  }

  get actions() {
    // TODO Action map
    return {};
  }

  get jobId() {
    return _.get(this._$stateParams, 'jobId', null);
  }

  /*
   * Leaving this here as an example of the stateParams required to nagivate to
   * an actionableItem page from a defect page.
   * Do not remove until the development of this page has been completed.
   *
   * this._$state.go('crediting.actionableItems.add', {
   *   assetId: _.get(this, '_defect.asset.id'),
   *   defectId: this._$stateParams.codicilId,
   *   jobId: _.get(this, '_defect.job.id'),
   *   context: this._$stateParams.context,
   *   codicilId: null
   * });
   */
}
