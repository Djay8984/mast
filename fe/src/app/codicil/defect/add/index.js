import * as angular from 'angular';

export default angular
  .module('app.codicil.defect.add', [])
  .name;
