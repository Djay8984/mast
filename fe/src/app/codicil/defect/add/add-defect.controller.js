import Base from 'app/base.class';
import * as _ from 'lodash';

const DEFECT_DETAIL_ID_DEFECT_TYPE = 1;
const DEFECT_DETAIL_ID_DEFECT_DESCRIPTOR = 2;
const DEFECT_DETAIL_ID_ASSET_LOADING_CONDITION = 3;
const DEFECT_DETAIL_ID_ASSET_LOCATION = 4;
const DEFECT_DETAIL_ID_ASSET_ACTIVITY = 5;
const DEFECT_DETAIL_ID_LONGITUDINAL_LOCATION = 6;
const DEFECT_DETAIL_ID_TRANSVERSE_LOCATION = 7;
const DEFECT_DETAIL_ID_GROUNDING_INVOLVED = 8;
const DEFECT_DETAIL_ID_ONBOARD_LOCATION = 9;
const DEFECT_DETAIL_ID_POLLUTION_TYPE = 10;
const DEFECT_DETAIL_ID_FIRE_EXPLOSION = 11;
const SHIP_OTHER_LOCATION = 297;
const SHIP_OTHER_ACTIVITY = 306;
const SHIP_OTHER_ONBOARD = 177;
const SHIP_OTHER_POLLUTION = 445;
const DEFAULT_CONFIDENTIALITY_TYPE = 3;
const MAX_TEXT_LENGTH = 50;
const MAX_DESCRIPTION_LENGTH = 2000;

const ROW_LENGTH = 3;

import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class AddDefectController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    assetModel,
    attachmentDecisionService,
    defectDecisionService,
    MastModalFactory,
    confidentialityTypes,
    defect,
    asset,
    job,
    surveyDecisionService,
    surveyType,
    refDefectCategories,
    refDefectDetails
  ) {
    super(arguments);

    this.setUpData();
  }

  setUpData() {
    this.attachments = [];
    this._items = [];

    this.currentCategory = {};
    this.currentSubCategory = {};
    this.currentRow = null;

    // Top level categories have parent === null
    this.topLevelCategories = _.filter(
      this.refDefectCategories,
      _.flow(_.property('parent'), _.isEmpty));

    _.forEach(this.topLevelCategories, (category) => {
      category.selected = false;
    });

    // commented out will need to be looked at again in  R3
    // if (this._$stateParams.defect) {
    // this.buildDefect(this._$stateParams);
    // }

    // adding a defect is an odd one as the get provides a prehydrated thing,
    // thus post a hydrated object (for offline)
    this.defect = this._$stateParams.defect || {
      asset: { id: this._asset.id },
      job: { id: this._job.id },
      items: [],
      cocs: [],
      ais: [],
      deleted: false,
      values: [],
      defectStatus: { id: 1 },
      incidentDate: this._moment().add(-1, 'm').startOf('m'),
      confidentialityType: {
        id: DEFAULT_CONFIDENTIALITY_TYPE
      },
      parent: null
    };

    this.section = {
      title: {
        label: 'Title',
        validate: {
          required: true,
          maxLength: MAX_TEXT_LENGTH
        }
      },
      otherLocation: {
        label: 'Other ship location',
        validate: {
          required: _.isObject(this.otherLocationSpecified),
          maxLength: MAX_TEXT_LENGTH
        }
      },
      otherActivity: {
        label: 'Other ship activity',
        validate: {
          required: _.isObject(this.otherActivitySpecified),
          maxLength: MAX_TEXT_LENGTH
        }
      },
      otherOnBoard: {
        label: 'Other In way of',
        validate: {
          required: _.isObject(this.onBoardLocationSpecified),
          maxLength: MAX_TEXT_LENGTH
        }
      },
      otherPollution: {
        label: 'Other pollution',
        validate: {
          required: _.isObject(this.otherPollutionSpecified),
          maxLength: MAX_TEXT_LENGTH
        }
      },
      description: {
        label: 'Description',
        validate: {
          maxLength: MAX_DESCRIPTION_LENGTH
        }
      },
      items: {
        assetId: this._asset.id,
        type: 'multi',
        validate: {
          required: true
        }
      },
      coc: {
        templateUrl: 'list/coc',
        name: 'cocs',
        label: 'Conditions of Class',
        value: this.defect.cocs,
        select: (item) => {
          this._$stateParams.payload = item;
          this._$state.go('crediting.defects.add.editCoc',
            this._$stateParams);
        }
      },
      actionableItem: {
        templateUrl: 'list/coc',
        name: 'actionableItem',
        label: 'Actionable items',
        value: this.defect.ais,
        select: (item) => {
          this._$stateParams.payload = item;
          this._$state.go('crediting.defects.add.editAI',
            this._$stateParams);
        }
      }
    };
  }

  get maxDate() {
    return this._moment().add(-1, 'm').startOf('m');
  }

  getItems(row) {
    const itemKeys = _.keys(this.defect.items);
    const indexOfStartingItem = row * ROW_LENGTH;
    const indexOfEndingItem = row * ROW_LENGTH + ROW_LENGTH;
    const itemKeysInRow =
      _.slice(itemKeys, indexOfStartingItem, indexOfEndingItem);
    const itemsInRow = _.toArray(_.pick(this.defect.items, itemKeysInRow));
    return itemsInRow;
  }

  get items() {
    return this._items;
  }

  get itemRows() {
    const itemArray = [0];
    let counter = 0;
    for (let i = 0, max = _.size(this.defect.items); i < max; i++) {
      if (i > 0 && i % ROW_LENGTH === 0) {
        itemArray.push(counter);
        counter++;
      }
    }
    return itemArray;
  }

  selectedSubCategory(subCategory) {
    if (this.currentSubCategory) {
      return this.currentSubCategory.id === subCategory.id;
    }
    return false;
  }

  get refDefectCategories() {
    return _.get(this._refDefectCategories, 'defect.defectCategories');
  }

  get refDefectDetails() {
    return _.get(this._refDefectDetails, 'defect.defectDetails');
  }

  get defectTypes() {
    return this._defectTypes || [];
  }

  // stop infinite digest loops
  getDetails() {
    // This needs firing when the categoryId changes
    this.filterDefectDescriptors();
    this.filterDefectTypes();
    this.filterOnboardLocations();
    this.filterLongitudinalLocations();
    this.filterTransverseLocations();
    this.filterPollutionTypes();
  }

  get defectDescriptors() {
    return this._defectDescriptors || [];
  }

  filterDefectTypes() {
    const categoryId = this.defect.defectCategory.id;

    this._defectTypes = _.filter(_.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_DEFECT_TYPE
    }), 'values'), {
      defectCategories: [{ id: categoryId }]
    });
  }

  get onBoardLocations() {
    return this._onBoardLocations || [];
  }

  filterOnboardLocations() {
    const categoryId = this.defect.defectCategory.id;

    this._onBoardLocations =
      _.filter(_.get(_.find(this.refDefectDetails, {
        id: DEFECT_DETAIL_ID_ONBOARD_LOCATION
      }), 'values'), {
        defectCategories: [{ id: categoryId }]
      });
  }

  filterDefectDescriptors() {
    const categoryId = this.defect.defectCategory.id;

    this._defectDescriptors = _.filter(_.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_DEFECT_DESCRIPTOR
    }), 'values'), {
      defectCategories: [{ id: categoryId }]
    });
  }

  filterLongitudinalLocations() {
    const categoryId = this.defect.defectCategory.id;

    this._longitudinalLocations = _.filter(_.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_LONGITUDINAL_LOCATION
    }), 'values'), {
      defectCategories: [{ id: categoryId }]
    });

    // Force deselection for Longitudinal Locations
    _.forEach(this._longitudinalLocations, (longitudinalLocation) => {
      _.set(longitudinalLocation, 'selected', false);
    });
  }

  get longitudinalLocations() {
    return this._longitudinalLocations || [];
  }

  filterTransverseLocations() {
    const categoryId = this.defect.defectCategory.id;

    this._transverseLocations = _.filter(_.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_TRANSVERSE_LOCATION
    }), 'values'), {
      defectCategories: [{ id: categoryId }]
    });

    // Force deselection for Transverse Locations
    _.forEach(this._transverseLocations, (transverseLocation) => {
      _.set(transverseLocation, 'selected', false);
    });
  }

  get transverseLocations() {
    return this._transverseLocations || [];
  }

  filterPollutionTypes() {
    const categoryId = this.defect.defectCategory.id;
    this._pollutionTypes = _.filter(_.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_POLLUTION_TYPE
    }), 'values'), {
      defectCategories: [{ id: categoryId }]
    });
  }

  get pollutionTypes() {
    return this._pollutionTypes;
  }

  get groundingInvolved() {
    return _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_GROUNDING_INVOLVED
    }), 'values');
  }

  get fireExplosion() {
    return _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_FIRE_EXPLOSION
    }), 'values');
  }

  get shipLocations() {
    return _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_LOCATION
    }), 'values');
  }

  get job() {
    return this._job;
  }

  get asset() {
    return this._asset;
  }

  get confidentialityTypes() {
    return _.get(this._confidentialityTypes, 'attachment.confidentialityTypes');
  }

  get shipLoadingConditions() {
    return _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_LOADING_CONDITION
    }), 'values');
  }

  removeItem(item) {
    _.remove(this._items, { id: item.id });
  }

  /* Ship Loading Conditions, Ship Location, and Ship Activity should only be
   * visible on the form and present in the defect data if the following
   * category + sub-category combinations are in effect:
   *
   * CATEGORY      SUB-CATEGORY
   * Hull          Contact
   * Hull          Equipment
   * Machinery     Contact
   * Machinery     Equipment
   */
  shouldHaveShipDetails() {
    const validCombinations = {
      hull: { contact: true, equipment: true },
      machinery: { contact: true, equipment: true }
    };

    let result = false;

    if (_.isObject(this.currentCategory)) {
      const categoryName = _.toLower(this.currentCategory.name);

      if (_.has(validCombinations, categoryName)) {
        const validSubCategories = validCombinations[categoryName];
        const subCategoryName = _.toLower(this.currentSubCategory.name);

        if (_.has(validSubCategories, subCategoryName)) {
          result = true;
        }
      }
    }

    return result;
  }
  changeLocations(addedLocation) {
    // do we have a locations array ?
    if (_.isNil(this.locations)) {
      this.locations = [];
    }

    // is location in the array ?
    const location = _.find(this.locations,
      { id: addedLocation.id });

    if (location) {
      _.pull(this.locations, location);
    } else {
      this.locations.push(addedLocation);
    }
  }

  hasLocation(location) {
    return _.find(this.locations, { 'id': location.id });
  }

  defectTypeChanged(selectedDefectType) {
    const type = _.first(selectedDefectType);

    const defectType = { defectValue: type };
    this.removeCurrentDetailValue(this.defectTypes);
    this.defect.values.push(defectType);
  }

  defectDescriptorChanged(selectedDefectDescriptor) {
    const descriptor = _.first(selectedDefectDescriptor);
    const defectDescriptor = { defectValue: descriptor };
    this.removeCurrentDetailValue(this.defectDescriptors);
    this.defect.values.push(defectDescriptor);
  }

  loadingConditionChanged(loadingCondition) {
    const defectLoading = { defectValue: loadingCondition };
    this.removeCurrentDetailValue(this.shipLoadingConditions);
    this.defect.values.push(defectLoading);
  }

  locationChanged(location) {
    if (location.id === SHIP_OTHER_LOCATION) {
      this.otherLocationSpecified = true;
    } else {
      const defectLocation = { defectValue: location };
      this.otherLocationSpecified = false;
      this.removeCurrentDetailValue(this.shipLocations);
      this.defect.values.push(defectLocation);
    }
  }

  selectLongitudinal() {
    // remove All from values
    this.removeCurrentDetailValues(this.longitudinalLocations);

    const selectedLocations = _.filter(this.longitudinalLocations, 'selected');
    _.forEach(selectedLocations, (defectValue) => {
      const value = _.omit(defectValue, 'selected');
      this.defect.values.push({ defectValue: value });
    });
  }

  selectTransverse() {
    // remove All from values
    this.removeCurrentDetailValues(this.transverseLocations);
    const selectedLocations = _.filter(this.transverseLocations, 'selected');
    _.forEach(selectedLocations, (defectValue) => {
      const value = _.omit(defectValue, 'selected');
      this.defect.values.push({ defectValue: value });
    });
  }


  onBoardLocationChanged(locations) {
    // remove all items from defect.values
    this.removeCurrentDetailValues(this.onBoardLocations);
    this.onBoardLocationSpecified = false;

    // if its empty do nothing
    if (!_.isEmpty(locations)) {
      _.forEach(locations, (location) => {
        if (location.id === SHIP_OTHER_ONBOARD) {
          this.onBoardLocationSpecified = location;
        } else {
          this.defect.values.push({ defectValue: location });
        }
      });
    }
  }

  pollutionTypeChanged(pollutions) {
    // remove all items from defect.values
    this.removeCurrentDetailValues(this.pollutionTypes);
    this.otherPollutionSpecified = false;

    // if its empty do nothing
    if (!_.isEmpty(pollutions)) {
      _.forEach(pollutions, (pollution) => {
        if (pollution.id === SHIP_OTHER_POLLUTION) {
          this.otherPollutionSpecified = pollution;
        } else {
          this.defect.values.push({ defectValue: pollution });
        }
      });
    }
  }

  activityChanged(activity) {
    if (activity.id === SHIP_OTHER_ACTIVITY) {
      this.otherActivitySpecified = true;
    } else {
      const defectActivity = { defectValue: activity };
      this.otherActivitySpecified = false;
      this.removeCurrentDetailValue(this.shipActivities);
      this.defect.values.push(defectActivity);
    }
  }

  saveOtherValues() {
    if (this.otherLocationSpecified) {
      this.saveOtherLocation();
    }
    if (this.otherActivitySpecified) {
      this.saveOtherActivity();
    }
    if (this.onBoardLocationSpecified) {
      this.saveOtherOnBoard();
    }
    if (this.otherPollutionSpecified) {
      this.saveOtherPollution();
    }
  }

  saveOtherActivity() {
    const defectActivity = {
      otherDetails: this.otherActivity,
      defectValue: this.shipActivity
    };
    this.removeCurrentDetailValue(this.shipActivities);
    this.defect.values.push(defectActivity);
  }

  saveOtherLocation() {
    const defectLocation = {
      otherDetails: this.otherLocation,
      defectValue: this.shipLocation
    };
    this.removeCurrentDetailValue(this.shipLocations);
    this.defect.values.push(defectLocation);
  }

  saveOtherPollution() {
    const otherPollution = _.find(this.pollutionTypes, {
      id: SHIP_OTHER_POLLUTION
    });

    const otherPollutionType = {
      defectValue: otherPollution,
      otherDetails: this.otherPollutionText
    };
    this.defect.values.push(otherPollutionType);
  }

  saveOtherOnBoard() {
    const otherLocation = _.find(this.onBoardLocations,
      { id: SHIP_OTHER_ONBOARD });
    this.defect.values.push({
      otherDetails: this.otherOnBoard,
      defectValue: otherLocation
    });
  }

  get otherActivitySpecified() {
    if (_.isUndefined(this._activityChanged)) {
      this._activityChanged = false;
    }
    return this._activityChanged;
  }

  set otherActivitySpecified(value) {
    this._activityChanged = value;
  }

  get otherLocationSpecified() {
    if (_.isUndefined(this._locationChanged)) {
      this._locationChanged = false;
    }
    return this._locationChanged;
  }

  set otherLocationSpecified(value) {
    this._locationChanged = value;
  }

  addExplosion() {
    if (!this.defect.values) {
      this.defect.values = [];
    }

    // do we have fire explosion?
    this.removeCurrentDetailValue(this.fireExplosion);
    this.defect.values.push({ defectValue: this.explosion });
  }

  addGrounding() {
    if (!this.defect.values) {
      this.defect.values = [];
    }

    // do we have a grounding ?
    this.removeCurrentDetailValue(this.groundingInvolved);
    this.defect.values.push({ defectValue: this.grounding });
  }

  removeCurrentDetailValue(detail) {
    const currentItemId =
      _.first(_.intersection(_.map(this.defect.values, 'id'),
        _.map(detail, 'id')));
    if (currentItemId) {
      const itemToRemove = _.find(this.defect.values, { id: currentItemId });
      _.pull(this.defect.values, itemToRemove);
    }
  }

  removeCurrentDetailValues(details) {
    // find everything in values which has an id in details
    const currentValueIds =
      _.intersection(_.map(this.defect.values, 'id'),
        _.map(details, 'id'));

    _.forEach(currentValueIds, (id) => {
      const itemToRemove = _.find(this.defect.values, { id });
      _.pull(this.defect.values, itemToRemove);
    });
  }

  shouldHaveShipLoadingCondition() {
    const result = this.shouldHaveShipDetails();

    /* If the chosen category and sub-category do not offer a ship loading
     * condition, remove any previously selected ship loading condition.
     */
    if (!result) {
      this.defect.shipLoadingCondition = null;
    }

    return result;
  }

  shouldHaveShipLocation() {
    const result = this.shouldHaveShipDetails();

    /* If the chosen category and sub-category do not offer a ship location,
     * remove any previously selected ship location.
     */
    if (!result) {
      this.defect.shipLocation = null;
    }

    return result;
  }

  shouldHaveShipActivity() {
    const result = this.shouldHaveShipDetails();

    /* If the chosen category and sub-category do not offer a ship activity,
     * remove any previously selected ship activity.
     */
    if (!result) {
      this.defect.shipActivity = null;
    }
    return result;
  }

  addCodicil(type) {
    this._$stateParams.payload = null;
    this._$stateParams.defect = this.defect;

    const state = {
      coc: 'crediting.defects.add.addCoc',
      actionableItem: 'crediting.defects.add.addAI',
      repair: 'crediting.defects.add.addRepair'
    };

    if (!this.defect.id) {
      this._$state.go(state[type], this._$stateParams);
    } else {
      this._$state.go('crediting.defects.add.addCoc',
        {
          jobId: this._job.id,
          defectId: this._defect.id,
          context: 'job'
        });
    }
  }
  get shipActivities() {
    return _.get(_.find(this.refDefectDetails, {
      id: DEFECT_DETAIL_ID_ASSET_ACTIVITY
    }), 'values');
  }

  rowIndices(data, rowLength) {
    if (_.isEmpty(data) || rowLength <= 0) {
      return [];
    }

    return _.times(Math.ceil(data.length / rowLength), _.identity);
  }

  rowSlice(data, rowIndex, rowLength) {
    const start = rowIndex * rowLength;
    return _.slice(data, start, start + rowLength);
  }

  get showAllForm() {
    if (this.currentCategory.selected) {
      // we have a category
      // do we need to a sub category
      if (!_.isEmpty(this.subCategories(this.currentCategory))) {
        // we need a sub category
        return this.currentSubCategory.selected;
      }

      // No sub category needed display the rest of the form
      return true;
    }

    // Category is needed to display rest of the form.
    return false;
  }

  // Don't like this but it helps rebuild the categories
  buildDefect(defect) {
    this.defect = this._$stateParams.defect;
    const returnedCategory = _.find(this.refDefectCategories,
      { id: this.defect.defectCategory.id });
    if (_.isNil(returnedCategory.parent)) {
      this.currentCategory = returnedCategory;
      this.currentCategory.selected = true;
      const topCategory =
        _.find(this.topLevelCategories, { id: returnedCategory.id });
      topCategory.selected = true;
    } else {
      this.currentCategory =
        _.find(this.refDefectCategories,
          { id: returnedCategory.parent.id });
      this.currentCategory.selected = true;
      const topCategory = _.find(this.topLevelCategories,
        { id: returnedCategory.parent.id });
      topCategory.selected = true;
      this.currentSubCategory = returnedCategory;
      this.currentSubCategory.selected = false;
    }

    // select the row and make sure its selected
    const top4 = _.take(this.topLevelCategories, 4);
    if (_.find(top4, { id: this.currentCategory.id })) {
      this.currentRow = 0;
    } else {
      this.currentRow = 1;
    }
    if (this.currentSubCategory) {
      this.toggleSubCategory(this.currentSubCategory,
        this.currentCategory, this.currentRow);
    }
  }

  formValid() {
    if (_.isUndefined(this.addDefectForm)) {
      return false;
    }
    return this.showAllForm && this.addDefectForm.$valid;
  }

  toggleCategory(category, row) {
    if (_.isUndefined(this.defect.category)) {
      this.defect.defectCategory = {};
    }

    // require subcategory ?
    if (_.isEmpty(this.subCategories(category))) {
      this.defect.defectCategory.id = category.id;
      this.getDetails();
    }

    // Toggle this category
    _.set(category, 'selected', !category.selected);

    // Deselect everything else
    if (category.selected) {
      this.currentCategory = category;
      this.currentRow = row;

      _.forEach(this.topLevelCategories, (c) => {
        if (c.id !== category.id) {
          c.selected = false;
        }
      });
    } else {
      this.currentCategory = {};
      this.currentRow = null;
    }

    // Regardless of whether a category has been deselected or an alternative
    // category selected, all sub-category selections must be cleared.
    this.currentSubCategory = {};
    _.forEach(this.subCategories(category), (c) => {
      _.set(c, 'selected', false);
    });
  }

  toggleSubCategory(subCategory, category, row) {
    this.defect.defectCategory.id = subCategory.id;

    // need to get details again
    this.getDetails();

    // Toggle this category
    _.set(subCategory, 'selected', !subCategory.selected);

    // Deselect everything else
    if (subCategory.selected) {
      this.currentSubCategory = subCategory;

      _.forEach(this.subCategories(category), (c) => {
        if (c.id !== subCategory.id) {
          c.selected = false;
        }
      });
    } else {
      this.currentSubCategory = {};
    }
  }

  subCategories(category) {
    // Subcategories have a parent.id equal to the
    // id of their parent, the supplied category.
    return _.filter(this.refDefectCategories,
      _.flow(_.property('parent.id'),
        _.partial(_.isEqual,
          _.get(category, 'id'))));
  }

  marshallItems(items) {
    return _.map(items, (item) => ({
      item: {
        id: item.id,
        _id: _.toString(item.id)
      }
    }));
  }

  async save() {
    this.saveOtherValues();

    let payload = _.cloneWith(this.defect, (value) => {
      if (_.isDate(value)) {
        return value.toISOString();
      }
    });

    payload.items = this.marshallItems(this.section.items.value);
    payload.defectCategory = _.pick(payload.defectCategory, 'id');

    payload = _.omit(payload, ['ais', 'cocs']);

    _.set(
      payload,
      'survey',
      await this._surveyDecisionService.generateDefectSurvey(
        payload.job.id,
        this._surveyType.get('damage'),
        payload.defectCategory.id,
        this.asset.ruleSet.id,
        payload.title)
    );

    const cleanedPayload = _.omitBy(payload, _.isNil);
    const defect = await this._defectDecisionService.save({
      jobId: cleanedPayload.job.id
    }, cleanedPayload);

    if (!_.isUndefined(this.attachments)) {
      this._attachmentDecisionService
        .addAll(`job/${this._$stateParams.jobId}/wip-defect/${defect.id}`,
          this.attachments);
    }

    if (defect) {
      if (await this._MastModalFactory({
        templateUrl: confirmationTemplateUrl,
        scope: {
          title: 'Successful added defect',
          message: `You successfully added a defect to asset
           ${_.get(this, 'asset.ihsAsset.id') ?
           `IMO no. ${_.get(this, 'asset.ihsAsset.id')}` :
           `ID ${_.get(this, 'asset.id')}`}`,

          actions: [
            { name: 'OK', result: true }
          ]
        },
        class: 'dialog'
      }).activate()) {
        this._$state.go('^.view', _.merge(this._$stateParams, {
          codicilId: defect.id || defect._id,
          assetId: this.assetId,
          jobId: this.jobId,
          context: this.context
        }));
      }
    }
  }

  back() {
    this._$state.go('^.list', this._$stateParams, { reload: true });
  }
}
