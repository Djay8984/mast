import * as _ from 'lodash';
import FormModel from 'app/models/form-model';

export default class Input extends FormModel {
  buildId() {
    if (_.isUndefined(this.data)) {
      return '';
    }
    const pref = _.get(this.data, 'compositeId', '');
    const id = _.padStart(this.data._id, 5, 0);
    return `${pref}${id}`;
  }

  //  ---------------------------------------------------------
  set params(value) {
    this._params = {
      context: this.config.context || 'asset',
      jobId: _.get(value, 'job.id'),
      assetId: _.get(value, 'asset.id'),
      codicilId: _.get(value, 'id')
    };
  }

  //  ---------------------------------------------------------
  get params() {
    if (!this._params) {
      this.params = this.data;
    }
    return this._params;
  }

  //  ---------------------------------------------------------
  get path() {
    const params = this.params;
    const path = {
      job: `job/${params.jobId}/wip-${this.slug}/${params.codicilId}`,
      asset: `asset/${params.assetId}/${this.slug}/${params.codicilId}`
    };
    const str = path[this.config.context] || path.asset;

    return params.codicilId && str;
  }
}
