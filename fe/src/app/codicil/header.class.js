import * as _ from 'lodash';
import HeaderUrl from './header.html';

export default class Header {
  constructor(parent) {
    this.parent = parent;
    this.config = parent.config;

    this.htmlUrl = HeaderUrl;

    this.actionsByStatus = {
      changeRecommended: ['approve', 'reject'],
      inactive: ['edit', 'cancel'],
      open: ['edit', 'cancel', 'remove']
    };

    this.actionsbyApproval = {
      requireApproval: ['closure', 'amendment']
    };

    this.actionsMap = {
      addRepair: { name: 'Add repair', func: this.addRepair.bind(this) },
      amendment: { name: 'Recommend amendment',
        func: this.amendment.bind(this) },
      approve: { name: 'Approve', func: this.approve.bind(this) },
      cancel: { name: 'Cancel', func: this.cancel.bind(this) },
      close: { name: 'Close', func: this.close.bind(this) },
      closure: { name: 'Recommend closure', func: this.closure.bind(this) },
      edit: { name: 'Edit', func: this.edit.bind(this) },
      reject: { name: 'Reject', func: this.reject.bind(this) },
      remove: { name: 'Close', func: this.remove.bind(this) }
    };

    this.$set = {
      $stateParams: { service: '$stateParams' },
      $state: { service: '$state' },
      navigationService: 'navigationService'
    };
  }

  //  ---------------------------------------------------------
  get jobStatus() {
    return _.get(this.parent, '_job.jobStatus.constant');
  }

  //  ---------------------------------------------------------
  get statusId() {
    return _.get(this.obj, 'status.id');
  }

  get status() {
    this.mapStatus();
    return this.statusById[this.statusId];
  }

  //  ---------------------------------------------------------
  /**
   * @summary disableButtons()
   * @description The buttons are disabled when the job status reaches certain
   *   conditions, such as when a FAR or FSR is submitted;
   * @return {Boolean} true or false depending on the job status
   */
  get disableButtons() {
    const disabledStatus = [
      'awaitingTechnicalReviewerAssignment',
      'underTechnicalReview',
      'awaitingEndorserAssignment',
      'underEndorsement',
      'closed'
    ];
    return _.includes(disabledStatus, this.jobStatus);
  }

  //  ---------------------------------------------------------
  //  Get action buttons what will be displayed
  getActions() {
    const status = this.status;
    const { context, title } = this.parent.config;
    const { requireApproval, inheritedFlag } = this.parent.data;
    let actionSet = [];
    if (context === 'asset') {
      if (_.get(this.parent, '_$stateParams.jobId') ||
        _.get(this.parent.data, 'defect.id')) {
        actionSet = _.get(this.actionsByStatus, status, []);
      }
    }

    if (requireApproval || context === 'job') {
      actionSet = _.get(this.actionsByStatus, status, []);
    }

    if (requireApproval && status !== 'changeRecommended') {
      actionSet = this.actionsbyApproval.requireApproval;
    }

    if (inheritedFlag &&
        status !== 'deleted' &&
        context === 'job' &&
        title === 'Condition of Class') {
      actionSet.unshift('addRepair');
    }

    if (status !== 'deleted' &&
        context === 'asset') {
      actionSet = _.get(this.actionsByStatus, status, []);
    }

    this.actions = _.map(actionSet, (val) =>
      _.set(this.actionsMap[val], 'disabled', this.disableButtons));
  }

  //  ---------------------------------------------------------
  get obj() {
    return this.parent.data;
  }

  get parentTitle() {
    return _.get(this, 'parent.data.defect.title') ||
      _.get(this, 'parent._parent.title');
  }

  //  ---------------------------------------------------------
  get codicilType() {
    return this.parent.codicilType;
  }

  //  ---------------------------------------------------------
  get view() {
    return _.get(this.parent, 'view');
  }

  /**
   *  Returns the backTitle dynamically depending on the back target
   * if the back target is a defect the name of the page we go back
   * to is the title of the defect.
   */
  get backString() {
    if (_.has(this.parent, '_navigationService')) {
      const backState = this.parent._navigationService.backState();
      if (
        _.get(backState, 'name') === 'crediting.defects.view' &&
        _.has(this.parent, '_coc')
      ) {
        return `Back to ${this.parent._coc.defect.title}`;
      }
      return _.get(backState, 'backTitle', '^');
    }
    return 'Back';
  }

  //  ---------------------------------------------------------
  //  Create object from constant, where key is id, value: name
  mapStatus() {
    const status = _.get(this.parent, '_status');
    this.statusById = {};
    if (status) {
      _.forEach(status.toObject(), (val, key) => {
        this.statusById[val] = key;
      });
    }
  }

  //  ---------------------------------------------------------
  //  Check status
  is(str) {
    const constant = _.get(this.parent, '_status');
    return constant && this.statusId === constant.get(str);
  }

  //  ---------------------------------------------------------
  //  Edit button action
  edit() {
    this.$stateParams.codicilId = this.obj.id || this.obj._id;
    this.$state.go('^.edit', this.$stateParams);
  }

  //  ---------------------------------------------------------
  //  Default confirmation modal for actions
  async openModal(data) {
    const result = await this.parent._MastModalFactory({
      templateReady: 'confirmation',
      scope: {
        title: data.title,
        message: data.message,
        actions: [
          { name: 'Go back', result: false },
          { name: _.get(data, 'name'), result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();

    if (result) {
      if (this.view && this.view.updateStatus && data.newStatus) {
        await this.view.updateStatus(data.newStatus);
        return this.back();
      }
    }
  }

  //  ---------------------------------------------------------
  addRepair() {
    this.$state.go(`${this.$state.current.name}.addRepair`,
      _.merge(this.$stateParams, { backTarget: this.$state.current.name }));
  }

  //  ---------------------------------------------------------
  amendment() {
    this.openModal({
      name: 'Request change',
      title: `Amendment ${this.config.title}`,
      message: `This ${this.config.title} requires approval from the back ` +
        `office team to change.
Please add a note to the ${this.config.title} to explain your suggestions ` +
        `and reasons why.`,
      newStatus: 'changeRecommended'
    });
  }

  //  ---------------------------------------------------------
  approve() {
    this.openModal({
      name: 'Approve',
      title: `Approve ${this.config.title}`,
      message: `Are you sure you want to approve this ${this.config.title}?`,
      newStatus: 'open'
    });
  }

  //  ---------------------------------------------------------
  cancel() {
    this.openModal({
      name: 'Cancel',
      title: `Cancel ${this.config.title}`,
      message: `Are you sure you want to cancel this ${this.config.title}?`,
      newStatus: 'cancelled'
    });
  }

  //  ---------------------------------------------------------
  close() {
    this.openModal({
      name: 'Close',
      title: `Close ${this.config.title}`,
      message: `Are you sure you want to close this ${this.config.title}?`,
      newStatus: 'closed'
    });
  }

  //  ---------------------------------------------------------
  closure() {
    this.openModal({
      name: 'Request closure',
      title: `Closure ${this.config.title}`,
      message: `This ${this.config.title} requires approval from the back ` +
        `office team to close.
Please add a note to the ${this.config.title} to explain your reasons why.`,
      newStatus: 'changeRecommended'
    });
  }

  //  ---------------------------------------------------------
  reject() {
    this.openModal({
      name: 'Reject',
      title: `Reject ${this.config.title}`,
      message: `Are you sure you want to reject this ${this.config.title}?`,
      newStatus: 'open'
    });
  }

  //  ---------------------------------------------------------
  remove() {
    this.openModal({
      name: 'Delete',
      title: `Delete ${this.config.title}`,
      message: `Are you sure you want to delete this ${this.config.title}?`,
      newStatus: 'deleted'
    });
  }

  //  ---------------------------------------------------------
  back(reload) {
    if (this.parent.back) {
      return this.parent.back();
    }

    if (this.config.actionType === 'add') {
      reload = false;
    }

    return this.redirect(reload);
  }

  /**
   * Maps the object's codicil's route to a string suitable for the header to
   * use. This needs to be done because the codicil doesn't contain its own type
   * @return {String} the parent type to display in the header
   */
  get relatedType() {
    const typeMap = {
      'coc': 'Condition of Class',
      'asset-note': 'Asset Note',
      'actionable-item': 'Actionable Item'
    };

    return typeMap[_.get(this.obj, 'codicil.route')];
  }

  redirect(reload = true) {
    return this.navigationService.back({ reload });
  }
}
