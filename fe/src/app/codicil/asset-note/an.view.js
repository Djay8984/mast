import Methods from '../input.methods';

export default class View extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.parent = parent;
    this.config = parent.config;

    this.type = 'assetNote';
    this.slug = 'asset-note';

    this.list = [
      { extend: 'view.id' },
      {
        templateUrl: 'view/box',
        label: 'Category',
        name: 'category.name'
      },
      {
        templateUrl: 'view/item',
        label: 'Item',
        name: 'assetItem',
        autoHide: true
      },
      {
        templateUrl: 'view/text',
        label: 'Description',
        name: 'description'
      },
      {
        templateUrl: 'view/text',
        label: 'Surveyor guidance',
        name: 'surveyorGuidance'
      },
      {
        templateUrl: 'view/date',
        label: 'Imposed date',
        name: 'imposedDate'
      },
      {
        templateUrl: 'view/text',
        label: 'Visible for',
        name: 'confidentialityType.description'
      },
      {
        templateUrl: 'view/attachments',
        label: 'Attachments',
        modalTitle: 'Asset note attachments and notes',
        path: this.path,
        disabled: this.data.template
      },
      {
        templateUrl: 'view/yes-no',
        label: 'Template used',
        name: 'template.id'
      },
      {
        templateUrl: 'view/yes-no',
        label: 'Requires approval for change',
        name: 'requireApproval'
      }
    ];
  }
}
