import * as angular from 'angular';

import add from './add';
import edit from './edit';
import view from './view';

export default angular
  .module('app.codicil.asset-note', [
    add,
    edit,
    view
  ])
  .name;
