import * as angular from 'angular';

export default angular
  .module('app.codicil.asset-note.edit', [])
  .name;
