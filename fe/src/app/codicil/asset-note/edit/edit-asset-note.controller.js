import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/asset-note/an.input';

export default class EditAssetNoteController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    codicilDecisionService,
    MastModalFactory,
    assetNote,
    assetNoteStatus
  ) {
    super(arguments);

    this._status = this._assetNoteStatus;
    this.data = this._assetNote;

    this.config = {
      title: 'Edit Asset Note',
      context: this._$stateParams.context
    };

    this.header = new Header(this);
    this.footer = new Footer(this);

    this.model = new Input(this);
    this.model.setValues();
  }

  //  -----------------------------------------------------------
  redirect() {
    this.header.redirect(false);
  }

  //  -----------------------------------------------------------
  async save() {
    await this.model.save();
    this.header.redirect(true);
  }

  //  -----------------------------------------------------------
  async back() {
    const result = await this.model.leaveConfirm();
    return this[result] && this[result]();
  }
}
