import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import View from 'app/codicil/asset-note/an.view';

import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewAssetNoteController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $log,
    $rootScope,
    $scope,
    $state,
    $stateParams,
    moment,
    asset,
    assetNote,
    assetNoteStatus,
    codicilDecisionService,
    job,
    MastModalFactory,
    printService
  ) {
    super(arguments);

    this._status = assetNoteStatus;
    this.data = assetNote;

    this.config = {
      title: 'Asset Note',
      context: this._$stateParams.context
    };

    this.header = new Header(this);
    this.header.getActions();

    this.view = new View(this);
    this.view.setValues();

    this.printPayload = this.data;
  }
}
