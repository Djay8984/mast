import Base from 'app/base.class';
import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/asset-note/an.input';
import Confirmation from 'app/codicil/confirmation';
import * as _ from 'lodash';

export default class AddAssetNoteController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    attachmentDecisionService,
    codicilDecisionService,
    assetNoteStatus,
    MastModalFactory,
    navigationService
  ) {
    super(arguments);

    const { jobId, assetId } = this._$stateParams;

    this.config = {
      actionType: 'add',
      item: 'Asset Note',
      title: 'Add Asset Note',
      context: this._$stateParams.context,
      disableJobScope: _.isEqual(this._$stateParams.context, 'job')
    };

    this.data = {
      job: jobId ? { id: jobId } : null,
      asset: assetId ? { id: assetId } : null,
      status: { id: this._assetNoteStatus.get('open') },
      jobScopeConfirmed: false
    };

    this.header = new Header(this);
    this.footer = new Footer(this);
    this.model = new Input(this);

    this.confirmation = new Confirmation(
      this.config,
      this.data,
      MastModalFactory
    );
  }

  //  -----------------------------------------------------------
  async save() {
    if (_.isEqual(this.config.context, 'job')) {
      this.data.requireApproval = false;
    }

    if (this.model.values.requireApproval) {
      this.model.data.status =
        { id: this._assetNoteStatus.get('changeRecommended') };
    }
    const res = await this.model.add();
    this._$stateParams.codicilId = res.id;

    if (await this.confirmation.addSuccess()) {
      this.header.redirect();
    }
  }
}
