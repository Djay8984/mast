import * as _ from 'lodash';
import Methods from '../input.methods';

export default class AssetNote extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.parent = parent;
    this.config = parent.config;

    this.type = 'assetNote';
    this.slug = 'asset-note';

    this.notDisable = ['template', 'imposedDate', 'assetItem'];

    //  Form items with templates assigned
    this.list = [
      { extend: 'view.id' },
      { extend: 'input.selectTemplate',
        inactive: this.config.disableJobScope },
      { extend: 'input.title' },
      { extend: 'input.category' },
      { extend: 'input.assetItem', typeName: 'asset note' },
      { extend: 'input.description' },
      { extend: 'input.surveyorGuidance' },
      { extend: 'input.imposedDate' },
      { extend: 'input.confidentialityType',
        modifyList: (val) => ({
          id: val.id,
          name: val.description,
          disabled: this.config.context === 'asset' &&
            val.id > _.get(this.data, 'confidentialityType.id')
        })
      },
      { extend: 'input.attachments' },
      { extend: 'input.requireApproval',
        inactive: this.config.disableJobScope }
    ];
  }
}
