//  Extends
//  FromModel -> view.methods -> input.methods
import Methods from '../input.methods';

export default class ActionableItem extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.type = 'actionableItem';
    this.slug = 'actionable-item';

    this.notDisable = ['template', 'imposedDate', 'dueDate', 'assetItem'];

    //  Form items with tempaltes assigned
    this.children = [
      { extend: 'view.id' },
      { extend: 'input.selectTemplate',
        inactive: this.config.disableJobScope },
      { extend: 'input.title' },
      { extend: 'input.category' },
      { extend: 'input.assetItem', typeName: 'actionable item' },
      { extend: 'input.description' },
      { extend: 'input.surveyorGuidance' },
      { extend: 'input.imposedDate' },
      { extend: 'input.dueDate' },
      { extend: 'input.confidentialityType',
        disabled: this.config.disableConfidentiality
      },
      { extend: 'input.attachments' },
      { extend: 'input.requireApproval',
        inactive: this.config.disableJobScope }
    ];
  }
}
