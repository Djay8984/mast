import * as _ from 'lodash';
import Methods from '../input.methods';

export default class View extends Methods {
  constructor(parent) {
    super(parent.data, parent);

    this.parent = parent;
    this.config = parent.config;

    this.type = 'actionableItem';
    this.slug = 'actionable-item';

    this.list = [
      { extend: 'view.id' },
      {
        templateUrl: 'view/box',
        label: 'Category',
        name: 'category.name'
      },
      {
        templateUrl: 'view/item',
        label: 'Item',
        name: 'assetItem',
        autoHide: true
      },
      {
        templateUrl: 'view/text',
        label: 'Description',
        name: 'description'
      },
      {
        templateUrl: 'view/text',
        label: 'Surveyor guidance',
        name: 'surveyorGuidance'
      },
      {
        templateUrl: 'view/date',
        label: 'Imposed date',
        name: 'imposedDate'
      },
      {
        templateUrl: 'view/date',
        label: 'Due date',
        name: 'dueDate'
      },
      {
        templateUrl: 'view/text',
        label: 'Visible for',
        name: 'confidentialityType.description'
      },
      {
        templateUrl: 'view/attachments',
        label: 'Attachments',
        modalTitle: 'Actionable item attachments and notes',
        path: this.path,
        disabled: this.data.template
      },
      {
        templateUrl: 'view/yes-no',
        label: 'Template used',
        name: 'template'
      },
      {
        templateUrl: 'view/yes-no',
        label: 'Requires approval for change',
        name: 'requireApproval'
      },
      {
        templateUrl: 'view/defect',
        name: 'defect',
        label: 'Linked defect',
        type: 'defect',
        autoHide: true,
        viewState: this.config.context === 'asset' ?
          'defect.view' : 'crediting.defects.view',
        viewParams: {
          jobId: _.get(this.data, 'job.id'),
          assetId: _.get(this.data, 'asset.id'),
          codicilId: _.get(this.data, 'defect.id'),
          context: this.config.context
        }
      }
    ];
  }
}
