import Base from 'app/base.class';
import * as _ from 'lodash';

import Header from 'app/codicil/header.class';
import Footer from 'app/codicil/footer.class';
import Input from 'app/codicil/actionable-item/ai.input';

export default class EditActionableItemController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    moment,
    codicilDecisionService,
    MastModalFactory,
    actionableItem,
    actionableItemStatus
  ) {
    super(arguments);

    this._status = this._actionableItemStatus;
    this.data = this._actionableItem;

    this.config = {
      title: 'Edit Actionable Item',
      context: this._$stateParams.context,
      disableConfidentiality: Boolean(_.get(this._actionableItem, 'job.id'))
    };

    this.header = new Header(this);
    this.footer = new Footer(this);

    this.model = new Input(this);
    this.model.setValues();
  }

  //  -----------------------------------------------------------
  async save() {
    await this.model.save();
    this.header.redirect();
  }

  //  -----------------------------------------------------------
  redirect() {
    this.header.redirect(false);
  }

  //  -----------------------------------------------------------
  async back() {
    const result = await this.model.leaveConfirm();
    return this[result] && this[result]();
  }
}
