import * as angular from 'angular';

export default angular
  .module('app.codicil.actionable-item.edit', [])
  .name;

export templateUrl from 'app/codicil/input.html';
export controller from './edit-actionable-item.controller';
