import Base from 'app/base.class';

import Header from 'app/codicil/header.class';
import View from 'app/codicil/actionable-item/ai.view';
import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewActionableItemController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    actionableItem,
    actionableItemStatus,
    asset,
    codicilDecisionService,
    job,
    MastModalFactory,
    moment,
    printService,
    user
   ) {
    super(arguments);

    this._status = this._actionableItemStatus;
    this.data = this._actionableItem;

    this.config = {
      title: 'Actionable Item',
      context: this._$stateParams.context
    };

    this.header = new Header(this);
    this.header.getActions();

    this.view = new View(this);
    this.view.setValues();

    this.printPayload = this._actionableItem;
  }
}
