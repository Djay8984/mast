import angular from 'angular';

export templateUrl from 'app/codicil/view.html';
export controller from './view-actionable-item.controller';

export default angular
  .module('app.codicil.actionable-item.view', [])
  .name;

export const resolve = {

  /* @ngInject */
  job: () => null,

  /* @ngInject */
  actionableItem: (codicilDecisionService, $stateParams) =>
    codicilDecisionService.getActionableItem($stateParams)
};
