import AddController from
  'app/codicil/actionable-item/add/add-actionable-item.controller';

export default class AddActionableItemController extends AddController {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    $stateParams,
    attachmentDecisionService,
    codicilDecisionService,
    MastModalFactory,
    moment,
    asset,
    actionableItemStatus,
    categories,
    dateFormat,
    user
  ) {
    super(...arguments);

    this.data = this._$stateParams.payload;

    if (this.data) {
      this.model.data = this.data;
      this.model.setValues();
    }
  }

  save() {
    if (this.data) {
      this._$stateParams.payload = this.model.values;
    } else {
      this._$stateParams.defect.ais.push(this.model.values);
    }
    this.back();
  }

  back() {
    this._$state.go('crediting.defects.add', this._$stateParams);
  }
}
