import * as angular from 'angular';

export default angular
  .module('app.codicil.actionable-item.add', [])
  .name;

export templateUrl from 'app/codicil/input.html';
export controller from './add-actionable-item.controller';

