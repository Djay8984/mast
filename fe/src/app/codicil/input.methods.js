import * as _ from 'lodash';
import ViewModel from './view.methods';
import ExtendItems from './extend.items';

export default class Input extends ViewModel {
  //  ---------------------------------------------------------
  async addAttachments() {
    const attachments = this.values.attachments;

    if (attachments) {
      return this.parent._attachmentDecisionService
        .addAll(this.path, attachments);
    }
  }

  get extend() {
    return new ExtendItems();
  }

  //  ---------------------------------------------------------
  get omittedValues() {
    return _.omit(this.values, ['attachments', 'templateStatus', 'version',
      'codicilType', 'itemType', 'count']);
  }

  //  ---------------------------------------------------------
  async codicilService(action) {
    const upperFirstType = _.upperFirst(this.type);
    const method = `${action}${upperFirstType}`;

    if (_.isEqual(this._$stateParams.context, 'job') &&
      _.isNil(_.get(this.params, 'parent'))) {
      _.set(this.params, 'actionTakenDate',
        this.parent._moment().toISOString());
    }
    return this.parent._codicilDecisionService[method](
      { ...this.params, ...this._$stateParams }, this.omittedValues);
  }

  //  -----------------------------------------------------------
  set status(value) {
    if (this.parent._status) {
      _.set(this.data, 'status.id', this.parent._status.get(value));
    }
  }

  //  -----------------------------------------------------------
  async updateStatus(value) {
    this.status = value;
    if (_.includes(['deleted', 'changeRecommended'], value) &&
      _.includes(['wip-coc', 'wip-actionable-item'], this.data.route)) {
      const user = this.parent._user;
      _.set(this.data, 'dateOfCrediting', this.parent._moment().toISOString());
      _.set(this.data, 'creditedBy.id', user.id);
    }
    _.set(this.data, 'actionTakenDate', this.parent._moment().toISOString());

    return this.codicilService(
      value === 'cancelled' ? 'remove' : 'update');
  }

  //  ---------------------------------------------------------
  async add() {
    const response = await this.codicilService('save');

    this.params = response;
    await this.addAttachments();

    return response;
  }

  //  ---------------------------------------------------------
  async save() {
    return this.codicilService('update');
  }

  //  ---------------------------------------------------------
  //  Checks is form $dirty.
  //  And if true, displayes confirmation modal
  async leaveConfirm() {
    let leave = 'redirect';
    if (this.parent.form && this.parent.form.$dirty) {
      leave = await this.parent._MastModalFactory({
        templateReady: 'confirmation',
        scope: {
          title: `Leave page`,
          message:
            `Changes have not been saved.
Do you want to keep the changes?`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Save changes', result: 'save', class: 'primary-button' },
            { name: 'Continue without saving',
              result: 'redirect', class: 'secondary-button' }
          ]
        },
        class: 'dialog'
      }).activate();
    }

    return leave;
  }
}
