import * as _ from 'lodash';
import Base from 'app/base.class';

export default class LayoutController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    pageService
  ) {
    super(arguments);
    this.headerDefined = false;

    this._$rootScope.$on('$stateChangeSuccess',
      this.determineHeaderExistence.bind(this));
    this.determineHeaderExistence(null, this._$state.$current);
  }

  determineHeaderExistence(event, newState) {
    if (!_.isUndefined(newState)) {
      if (_.isUndefined(_.get(newState, 'views.header@main'))) {
        this.determineHeaderExistence(newState.parent);
      } else {
        this.headerDefined = true;
      }
    }
  }

  get page() {
    return this._pageService.main;
  }
}
