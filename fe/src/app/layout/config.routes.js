import controller from 'app/layout/layout.controller';
import templateUrl from 'app/layout/layout.html';

/* @ngInject */
export default function config(
  $stateProvider,
  $urlRouterProvider
) {
  $urlRouterProvider.when('', '/startup');
  $urlRouterProvider.when('/', '/startup');

  const mainState = {
    abstract: true,
    controller,
    controllerAs: 'main',
    templateUrl,
    url: '?{offline:bool}&{bundleId:int}',
    params: {
      offline: null,
      bundleId: null,
      backIgnore: null
    },
    resolve: {

      /* @ngInject */
      ready: ($rootScope) => $rootScope.READY,

//      /* @ngInject */
//      spellChecker: (spellCheckService) =>
//        spellCheckService.applySpellChecking(),

      // by injecting ready into this resolution, it makes this resolve wait
      // until the application is ready before making more requests
      /* @ngInject */
      user: (ready, employeeDecisionService) =>
        employeeDecisionService.getCurrentUser(),

      // a way of checking if the user has a bundle downloaded
      /* @ngInject */
      activeBundle: ($stateParams, offlinular) =>
        $stateParams.bundleId ?
          true && offlinular.bundle($stateParams.bundleId) : null,

      /* @ngInject */
      checkedOut: (pageService) => pageService.getCheckedOut()
    }
  };

  $stateProvider
    .state('main', mainState)
    .state('safeMain', {
      ...mainState,
      resolve: {
        ...mainState.resolve,

        // We still want to be able to see some screens if there is an
        // error when getting the user, so we swallow the error and
        // just log a warning.
        /* @ngInject */
        user: (ready, employeeDecisionService, $log) =>
          employeeDecisionService
            .getCurrentUser()
            .catch((e) => $log.warn('Unable to get user:', e))
      }
    });
}
