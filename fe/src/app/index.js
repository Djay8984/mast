// Restangular expects an old version of lodash to be available globally
window._.contains = window._.includes;

import * as angular from 'angular';
import angularAnimate from 'angular-animate';
import angularUIRouter from 'angular-ui-router';
import angularMessages from 'angular-messages';
import 'angular-moment';
import 'angular-poller';
import 'script!foundation-apps/dist/js/foundation-apps';
import 'script!foundation-apps/dist/js/foundation-apps-templates';
import 'restangular';
import 'angular-hotkeys-light';

// Our modules
import asset from './asset';
import batchActions from './batch-actions';
import codicil from './codicil';
import components from './components';
import configuration from './configuration';
import constants from './constants';
import createAsset from './create-asset';
import dashboard from './dashboard';
import error from './error';
import example from './example';
import filters from './filters';
import layout from './layout';
import run from './run';
import services from './services';
import startup from './startup';

import config from './config';

import '../index.html';

export default angular
  .module('app', [
    angularAnimate,
    angularUIRouter,
    angularMessages,
    'angularMoment',
    'emguo.poller',
    'foundation',
    'fps.hotkeys',
    'restangular',
    constants,
    asset,
    codicil,
    batchActions,
    components,
    configuration,
    createAsset,
    dashboard,
    error,
    example,
    filters,
    layout,
    services,
    startup,
    run
  ])
  .config(config)
  .name;
