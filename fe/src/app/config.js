import HotkeyListener from 'lib/hotkeys/listener';

/* @ngInject */
export default function config(
  $httpProvider,
  $logProvider,
  $provide,
  RestangularProvider,
  appConfig
) {
  const listResponseInterceptor = (data, operation) => {
    let extractedData = data;

    if (data.content && data.pagination) {
      extractedData = data.content || {};
      extractedData.pagination = data.pagination;
    }

    return extractedData;
  };

  RestangularProvider.setBaseUrl(appConfig.MAST_API_URL);
  RestangularProvider.addResponseInterceptor(listResponseInterceptor);

  // $httpProvider.interceptors.push('authInterceptor');

  const debug = Boolean(appConfig.MAST_DEBUG);
  $logProvider.debugEnabled(debug);

  const listener = new HotkeyListener();

  listener.listen();
}
