import * as angular from 'angular';
import uirouter from 'angular-ui-router';
import routes from './config.routes';
import services from '../services';

export default angular
  .module('app.startup', [
    uirouter,
    services
  ])
  .config(routes)
  .name;
