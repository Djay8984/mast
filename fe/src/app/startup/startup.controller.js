import Base from 'app/base.class';

export default class StartupController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    navigationService,
    offlineDataService,
    referenceDataService,

//    spellCheckService,
    ready // needed to force any requests to wait until the app is ready
  ) {
    super(arguments);

    this.asyncStartup();
  }

  async asyncStartup() {
    await this._referenceDataService.prepareDatabase(
      this._$rootScope.CONNECTED);

//    await this._spellCheckService.prepare(this._$rootScope.CONNECTED);
    await this._offlineDataService.prepareDatabase();
    this._navigationService.home();
  }
}
