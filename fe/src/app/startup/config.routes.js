import templateUrl from './startup.html';
import StartupController from './startup.controller';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('startup', {
      parent: 'safeMain',
      url: '/startup',
      templateUrl,
      controller: StartupController,
      controllerAs: 'vm'
    });
}
