/*	date.js

	Utility module/helper for validating dates.
	Written for /app/components/date-picker.

*/

import * as _ from 'lodash';

export {
	hasFormat
};

/**	Validates a `value:string` against `formatRequired:string`,
*   failing only the specified format characters.
*   Extra characters in `value` will not trigger a fail.
*   Does not check month strings against actual month names.
*
*	@param {string} value - string to be tested
*	@param {string} formatRequired - format string expected
*	@returns {Boolean} true if (weakly) valid; false if found to be invalid.
*/
function hasFormat(value, formatRequired) {
  let isAllValid = true; // assume true to begin with, until fail found.
  const formatCharTests = {
    'D': /([0-9])/,
    'M': /([a-zA-Z])/,
    'Y': /([0-9])/,
    'h': /([0-9])/,
    'm': /([0-9])/,
    ' ': /([ \-])/,
    ':': /([\-\:])/
  };

  if (!_.isUndefined(value)) {
    let i = value.length;
    let testingChar = '';
    let formatCharTest = '';

    while (i--) {
      testingChar = value[i];
      formatCharTest = formatCharTests[formatRequired[i]];

      if (!_.isUndefined(formatCharTest)) {
        if (!formatCharTest.test(testingChar)) {
          isAllValid = false;
        }
      }
    }

    return isAllValid;
  }
}


