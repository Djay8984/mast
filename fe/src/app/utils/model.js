export {
  construct,
  allProperties
};

/**
 * Curried version of Reflect.construct.
 * @example
 * import Job from 'app/models/job';
 * const job = Restangular.one('job', 1).get();
 * const jobModel = job.then(construct(Job));
 * @param {Function} constructor The target constructor to call
 * @param {...Object} args Arguments to pass to the constructor
 * @returns {Object} The constructed object
 */
function construct(constructor) {
  return (...args) => Reflect.construct(constructor, args);
}

/**
 * @param {object} object thing to parse properties out of
 * @return {array} list of strings detailing all of the properties on this
 *   object from every level of inheritance
 */
function allProperties(object) {
  const hiddenPropertyRegex = new RegExp('/^__/');
  const properties = [];
  const appendProperty = (property) => {
    if (properties.indexOf(property) === -1 &&
      !hiddenPropertyRegex.test(property)) {
      properties.push(property);
    }
  };

  do {
    Reflect
      .ownKeys(object)
      .forEach(appendProperty);
    object = Reflect.getPrototypeOf(object);
  } while (object);
  return properties;
}
