import Base from 'app/base.class';
import * as _ from 'lodash';
import './partials/imo-registered-details.html';
import './partials/non-imo-registered-details.html';
import atpModalTemplate from './asset-type-picker/atp-modal.html';
import AtpModalController from './asset-type-picker/atp-modal.controller';
import templateUrl from './error-modal/creation-error-modal.html';

const ASSET_TYPE_MAX_LEVEL = 5;

export default class CreateAssetController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    moment,
    assetOnlineService,
    ihsService,
    navigationService,
    MastModalFactory,
    categories,
    types,
    builderLength,
    dateFormat,
    ihsDateFormat,
    imoNumberLength,
    yardNumberLength,
    attachmentDecisionService,
    classDepartments
  ) {
    super(arguments);

    this._categories = _.get(categories, 'asset.assetCategories');
    this._filteredCategories = this._categories;
    this._types = _.get(types, 'asset.assetTypes');
    this._filteredTypes = this._types;
    this._registered = true;
    this._classDepartments = _.get(classDepartments, 'asset.classDepartments');
  }

  async verifyImo() {
    try {
      if (_.size(_.toString(this.asset.imo)) === this._imoNumberLength) {
        this._ihsAsset = await this._ihsService.get(this.asset.imo);
      }
    } catch (e) {
      this._ihsAsset = null;
    }
    this.imoValidated = Boolean(this._ihsAsset);
  }

  lookupIhs() {
    if (this.imoValidated) {
      _.merge(this.asset, this._ihsAsset);

      this.asset.buildDate =
        this._moment(this._ihsAsset.dateOfBuild, this._ihsDateFormat)
          .format(this._dateFormat);

      _.set(this.asset, 'type',
        [_.find(this._filteredTypes,
          { code: _.get(this._ihsAsset, 'ihsAssetType.stat5Code') })]);
      this.typeChanged(this.asset.type);

      // this._filteredCategories will be an array containing one value
      _.set(this.asset, 'category', this._filteredCategories);
      this.categoryChanged(this.asset.category);

      this.ihsLookupComplete = true;
    }
  }

  async create() {
    if (this.createAssetForm.$valid) {
      const restAsset = {
        name: this.asset.name,
        buildDate: this._moment(this.asset.buildDate).toISOString(),
        assetType: { id: _.first(this.asset.type).id },
        assetCategory: { id: _.first(this.asset.category).id },
        isLead: true,
        hullIndicator: 1
      };

      if (this.asset.imo) {
        _.set(restAsset, 'ihsAsset.id', this.asset.imo);
        _.set(restAsset, 'leadImo', this.asset.imo);
      }

      if (!this.asset.imo || this.ihsLookupComplete) {
        _.set(restAsset, 'builder', this.asset.builder);
        _.set(restAsset, 'yardNumber', this.asset.yardNumber);
      }

      if (this.asset.classDepartment) {
        _.set(restAsset, 'classDepartment', {
          id: this.asset.classDepartment.id
        });
      }

      try {
        this.creating = true;
        const asset = await this._assetOnlineService.create(restAsset);
        await this._attachmentDecisionService.addAll(`asset/${asset.id}`,
          this.attachments);
        this.creating = false;
        this._$state.go('asset.cases.list', { assetId: asset.id });
      } catch (e) {
        await this._MastModalFactory({
          templateUrl,
          scope: { errorText: e.data.message }
        }).activate();
        this.creating = false;
      }
    }
  }

  back() {
    this._navigationService.back();
  }

  categoryChanged(selection) {
    // Recursively calls itself, building up a final list which contains the
    // original filtered types and all their parents
    const catFilter = (currLevelList, currLevel, finalList) =>
      currLevel === 0 ? finalList :
        catFilter(

        // Finds all unique parents of the items in the current level
        _.uniq(_.reject(_.map(currLevelList,
          (type) => _.find(this._types, { id: type.parentId })), _.isNil)),

        // Decremented counter, and an updated final results list
        --currLevel,
        _.concat(finalList, currLevelList));

    if (_.isEmpty(selection)) {
      this.asset.type = null;
      this._filteredTypes = this._types;
      this.typeChanged(this.asset.type);
    } else {
      this._filteredTypes =
        _.uniq(catFilter(
          _.filter(this._types, ['category.id', _.first(selection).id]),
          ASSET_TYPE_MAX_LEVEL,
          [])
        );
    }
  }

  typeChanged(selectedItems) {
    const selectedType = _.first(selectedItems);
    if (_.isEmpty(selectedType)) {
      this._filteredCategories = this._categories;
      this.asset.classDepartment = null;
    } else {
      this._filteredCategories = _.filter(
        this._categories,
        { id: _.get(selectedType, 'category.id', null) });

      const defaultClassDepartment = _.first(_.filter(this._classDepartments,
        { id: _.get(selectedType, 'defaultClassDepartment.id', null) }
      ));
      if (defaultClassDepartment) {
        this.asset.classDepartment = defaultClassDepartment;
      }
    }
  }

  reset() {
    this.asset = {
      imo: '',
      name: '',
      builder: '',
      yardNumber: '',
      buildDate: '',
      type: [],
      category: []
    };
    this.attachments = [];
    this.createAssetForm.$setPristine();
    this.createAssetForm.$setUntouched();
    this.imoValidated = false;
    this.ihsLookupComplete = false;
    this.categoryChanged(this.asset.category);
    this.typeChanged(this.asset.type);
  }

  validateForm() {
    return this.imoRegistered ?
     this.createAssetForm.$invalid || !this.imoValidated :
     this.createAssetForm.$invalid;
  }

  get categories() {
    return this._filteredCategories;
  }

  get types() {
    return this._filteredTypes;
  }

  get classDepartments() {
    return this._classDepartments;
  }

  get registered() {
    return this._registered;
  }

  set registered(val) {
    this._registered = val;
  }

  get atpModalTemplate() {
    return atpModalTemplate;
  }

  get AtpModalController() {
    return AtpModalController;
  }

  get builderLength() {
    return this._builderLength;
  }

  get imoNumberLength() {
    return this._imoNumberLength;
  }

  get yardNumberLength() {
    return this._yardNumberLength;
  }
}
