import CreateAssetController from './create-asset.controller';
import createAssetTemplateUrl from './create-asset.html';

import HeaderController from './header/create-asset-header.controller';
import headerTemplateUrl from './header/create-asset-header.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider.state('createAsset', {
    parent: 'main',
    url: '/create-asset',
    resolve: {

      /* @ngInject */
      categories: (referenceDataService) =>
        referenceDataService.get(['asset.assetCategories'], true),

      /* @ngInject */
      types: (referenceDataService) =>
        referenceDataService.get(['asset.assetTypes'], true),

      /* @ngInject */
      classDepartments: (referenceDataService) =>
        referenceDataService.get(['asset.classDepartments'], true)
    },
    views: {
      '': {
        templateUrl: createAssetTemplateUrl,
        controller: CreateAssetController,
        controllerAs: 'vm'
      },
      'header@main': {
        templateUrl: headerTemplateUrl,
        controller: HeaderController,
        controllerAs: 'vm',
        resolve: {

          /* @ngInject */
          user: (node) => node.getUser()
        }
      }
    }
  });
}
