import Base from 'app/base.class';
import * as _ from 'lodash';

const STATCODES_LENGTH = 5;
const STATCODE_HIDDEN = -1;
const STATCODE_ALL = 0;
const STATCODE_LEVEL_5_INDEX = 4;

export default class AtpModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    availableOptions,
    selection
  ) {
    super(arguments);

    this.setupStatcodes();
  }

  setupStatcodes() {
    const setStatcode = (id) => {
      const opt = _.find(this._availableOptions, { id });

      if (opt.parentId) {
        setStatcode(opt.parentId);
      }

      const index = opt.levelIndication - 1;
      this.statcodes[index] = `${id}`;
      this.updateStatcodes(index);
    };

    this.statcodes = _.map(_.times(STATCODES_LENGTH, (index) =>
          index === 0 ? STATCODE_ALL : STATCODE_HIDDEN),
      _.toString);

    if (!_.isEmpty(this._selection)) {
      setStatcode(_.first(this._selection).id);
    }
  }

  /**
   * @param {int} statcodeIndex statcode index to start modifying from
   * goes through the statcode list & enforces the correct hierarchy scheme
   */
  updateStatcodes(statcodeIndex) {
    if (_.parseInt(this.statcodes[statcodeIndex]) === STATCODE_ALL) {
      this.statcodes = _.map(this.statcodes, (statcode, index) =>
        index > statcodeIndex ? STATCODE_HIDDEN : statcode);
    } else {
      this.statcodes[statcodeIndex + 1] = STATCODE_ALL;
      this.statcodes = _.map(this.statcodes, (statcode, index) =>
        index > statcodeIndex + 1 ? STATCODE_HIDDEN : statcode);
    }
    this.statcodes = _.map(this.statcodes, _.toString);

    this.levelFiveSelected = statcodeIndex === STATCODE_LEVEL_5_INDEX &&
      this.statcodes[statcodeIndex] > 0;
  }

  confirm() {
    const selected = _.find(this._availableOptions,
      { id: _.parseInt(_.findLast(this.statcodes,
      (sc) => _.parseInt(sc) > STATCODE_ALL)) });

    if (selected) {
      this._$modalInstance.close([selected]);
    }
  }

  get assetTypes() {
    return this._availableOptions;
  }
}
