import * as angular from 'angular';

import routes from './config.routes';

export default angular
  .module('app.create-asset', [])
  .config(routes)
  .name;
