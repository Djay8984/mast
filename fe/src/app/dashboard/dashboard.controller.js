import * as _ from 'lodash';
import Base from 'app/base.class';

export default class DashboardController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    user,
    assetFilterDefault
  ) {
    super(arguments);

    this.assetFilter = _.isEmpty($stateParams.assetFilter) ?
      assetFilterDefault : $stateParams.assetFilter;
    this.filterActive = !_.isEqual(this.assetFilter, assetFilterDefault);
    this.filterOpen = false;
  }

  updateList(filter) {
    this._$state.go(this._$state.current.name,
      _.merge(this._$stateParams, { assetFilter: filter, sortBy: null }),
      { reload: true });
  }

  switchTab(target) {
    this._$state.go(target,
      _.set(this._$stateParams, 'sortBy', null),
      { reload: true });
  }

  get assetFilterDefault() {
    return this._assetFilterDefault;
  }

  get currentTab() {
    return this._$state.current.name;
  }

  get isCaseUser() {
    return this._user.workItemType === 'cases';
  }
}
