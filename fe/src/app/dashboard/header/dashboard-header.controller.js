import Base from 'app/base.class';

export default class DashboardHeaderController extends Base {
  /* @ngInject */
  constructor(user) {
    super(arguments);
  }

  get user() {
    return this._user;
  }
}
