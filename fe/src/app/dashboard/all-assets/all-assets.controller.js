import * as _ from 'lodash';
import Base from 'app/base.class';
import Asset from 'app/models/asset.model';

export default class AllAssetsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    assets,
    user
  ) {
    super(arguments);
  }

  get workItemType() {
    return _.get(this, '_user.workItemType');
  }

  get assets() {
    if (!this._assetList || this._assetList.updated) {
      this._assetList = Asset.from(this._assets);
      this._assetList.updated = false;
    }

    return this._assetList;
  }

  get filtered() {
    return !_.isEmpty(this._$stateParams.assetFilter);
  }

  // Make sure $stateParams are updated properly
  // This is here because the filter requires $stateParams to be set
  async reload() {
    this._$state.go(
      this._$state.current, this._$stateParams, { reload: true, notify: true }
    );
  }

  get sortOrder() {
    return this._$stateParams.sortBy || this._assets.sortedBy;
  }

  set sortOrder(value) {
    this._$stateParams.sortBy = value;
  }
}
