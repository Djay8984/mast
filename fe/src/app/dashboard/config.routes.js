import * as _ from 'lodash';

import DashboardController from './dashboard.controller';
import dashboardTemplateUrl from './dashboard.html';

import HeaderController from './header/dashboard-header.controller';
import headerTemplateUrl from './header/dashboard-header.html';

import AllAssetsController from './all-assets/all-assets.controller';
import allAssetsTemplateUrl from './all-assets/all-assets.html';

import MyWorkController from './my-work/my-work.controller';
import myWorkTemplateUrl from './my-work/my-work.html';


/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('dashboard', {
      parent: 'main',
      abstract: true,
      url: '/dashboard',
      params: {
        assetFilter: null,
        sortBy: { sort: 'name', order: 'ASC' }
      },
      resolve: {

        /* @ngInject */
        myWork: ($stateParams, user, myWorkService) =>
          _.isEmpty($stateParams.assetFilter) ?
            myWorkService.getForUser(user, $stateParams.sortBy) :
            myWorkService.query(user, $stateParams.assetFilter,
              $stateParams.sortBy),

        /* @ngInject */
        assetFilterDefault: (assetDecisionService) =>
          assetDecisionService.getCurrentLifecycleStatusIds().then((ids) => ({
            lifecycleStatusId: ids
          }))
      },
      views: {
        '': {
          templateUrl: dashboardTemplateUrl,
          controller: DashboardController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            user: (node) => node.getUser(),

            /* @ngInject */
            asset: () => null
          }
        }
      }
    })
    .state('dashboard.assetList', {
      url: '/assetlist',
      templateUrl: allAssetsTemplateUrl,
      controller: AllAssetsController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        assets: ($stateParams, assetDecisionService) => {
          if (_.isEmpty($stateParams.assetFilter)) {
            return assetDecisionService.getAllCurrent($stateParams.sortBy);
          }

          return assetDecisionService.query(
            $stateParams.assetFilter, $stateParams.sortBy);
        }
      }
    })
    .state('dashboard.myWork', {
      url: '/my-work',
      templateUrl: myWorkTemplateUrl,
      controller: MyWorkController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        myCases: (caseService, user) => caseService.forEmployee(user.id),

        /* @ngInject */
        myJobs: (jobDecisionService, user) =>
          jobDecisionService.forEmployee(user.id),

        /* @ngInject */
        teamCases: (caseService) => caseService.query({}),

        /* @ngInject */
        teamJobs: (jobDecisionService) =>
          jobDecisionService.query({})
      }
    });
}
