import * as _ from 'lodash';

import Base from 'app/base.class';
import Asset from 'app/models/asset.model';

import './item-card-face-case.html';
import './item-card-face-job.html';

const ASSET_CARD_PADDING_LIMIT = 5;

export default class MyWorkController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    $timeout,
    assetDecisionService,
    caseService,
    jobStatus,
    offlinular,
    MastModalFactory,
    officeRole,
    surveyorRole,
    myWork,
    myCases,
    myJobs,
    teamJobs,
    teamCases,
    user
  ) {
    super(arguments);

    this.setUpData();
  }

  /**
   * @description Basic method to set up some data for the controller;
   */
  setUpData() {
    this.expandedAsset = null;
    this._selectedItems = [ this._myJobs, this._myCases ];

    this._teamState = { team: false };

    if (!_.isNil(this._$stateParams.dashboard)) {
      this._teamState = this._$stateParams.dashboard;
    }

    // If we don't have my cases and my jobs then set the team state to true
    // since we want to then select all team jobs/cases;
    if (_.isEmpty(_.flatten(this._selectedItems))) {
      this._teamState = { team: true };
    }

    this.addAssetToItem();
    this.addCountsToAssets();
  }

  /**
   * @description Private method to loop through the jobs and cases and add
   *   the imo number to them in turn for use in the template;
   */
  addAssetToItem() {
    const items =
      [...this._myJobs, ...this._teamJobs,
        ...this._myCases, ...this._teamCases];
    _.forEach(items, (item) => this.addNameToItem(item));
  }

  /**
   * @param {Object} item The item we're adding to
   * @description Method to add the name and ihsAsset id to each job/case
   */
  async addNameToItem(item) {
    const myAsset =
      await this._itemAsset(_.has(item, 'model') ?
        _.get(item.model.asset, 'id') : _.get(item.asset, 'id'));
    _.set(item, 'imoNumber', myAsset.ihsAsset.id);
    _.set(item, 'assetName', myAsset.name);
  }

  addCountsToAssets() {
    this._assets = Asset.from(this._myWork);

    if (!_.isEmpty(this._assets)) {
      this._assets.forEach((asset) => {
        _.set(asset, 'jobs.length',
          _.filter(_.uniqBy([...this._myJobs, ...this._teamJobs], 'id'),
            { asset: { id: asset.id } }).length);
        _.set(asset, 'cases.length',
          _.filter(_.uniqBy([...this._myCases, ...this._teamCases], 'id'),
            { asset: { id: asset.id } }).length);
      });
    }
  }

  get myJobs() {
    return this._myJobs;
  }

  get teamJobs() {
    return this._teamJobs;
  }

  get myCases() {
    return this._myCases;
  }

  get teamCases() {
    return this._teamCases;
  }

  get selectedItems() {
    return this._selectedItems;
  }

  showAmounts(type) {
    const typeMap = { 'jobs': 0, 'cases': 1 };
    const amount = _.size(this.selectedItems[typeMap[type]]);
    const workItem = amount < 2 ? type.substr(0, type.length - 1) : type;
    return `Showing ${_.min([20, amount])} of ` +
      `${_.size(this.selectedItems[typeMap[type]])} ${workItem}`;
  }

  /**
   * @param {Object} job The job we're getting the status of
   * @return {String} jobStatus The status of the current job
   */
  jobStatus(job) {
    if (!_.isNil(_.get(job, 'jobStatus.id'))) {
      return _.get(_.find(this._jobStatus,
        { id: job.jobStatus.id }), 'name', null);
    }
  }

  /**
   * @param {Object} job The job we're getting the surveyor for
   * @return {String} surveyor The name of the lead surveyor
   */
  jobSurveyor(job) {
    const item = _.find(job.employees,
      { employeeRole: { id: this._surveyorRole.get('leadSurveyor') } });

    if (item && !_.isEmpty(item) && !_.isNil(_.get(item, 'lrEmployee.name'))) {
      return _.startCase(item.lrEmployee.name);
    }
  }

  /**
   * @param {Object} job The job we're going to view
   * @param {Object} asset The asset, if we need the id
   */
  jobView(job, asset = { }) {
    const myParams = !_.isNil(asset) ?
      { jobId: job.model.id, assetId: asset.id } : { jobId: job.model.id };
    this._$state.go('asset.jobs.view', myParams, { reload: true });
  }

  /**
   * @param {Object} data The case we're going to view
   */
  async caseView(data) {
    const statusId = _.get(data, 'caseStatus.id');
    const target = await this._caseService.isOpenStatus(statusId) ?
      'details' : 'milestones';
    this.updateStateParams({ caseId: data.id, assetId: data.asset.id });
    this._$state.go(`asset.cases.view.${target}`,
      this._$stateParams, { reload: true });
  }

  /**
   * @param {String} type The type (title) of surveyor
   * @param {Object} data The case we're looking into for the surveyor
   * @return {String} name The name of the surveyor
   */
  caseSurveyor(type, data) {
    const surveyor = _.find(data.surveyors,
      { employeeRole: { id: this._surveyorRole.get(type) } });
    return _.get(surveyor, 'surveyor.name', null);
  }

  /**
   * @param {String} type The type of office role
   * @param {Object} data The case we're looking into for our office
   * @return {String} name The name of the office
   */
  caseOffice(type, data) {
    const office = _.find(data.offices,
      { officeRole: { id: this._officeRole.get(type) } });
    return _.get(office, 'office.name', null);
  }

  /**
   * @param {String} type The type of data we're looking at, my or team's
   * @return {Boolean} true or false
   */
  teamState(type) {
    return this._teamState[type];
  }

  /**
   * @description Sets the team state (my or teams' for example)
   *   this also triggers another method that selects which we're looking at;
   * @param {Boolean} state The team state, omission sets it to false
   */
  setTeamState(state = true) {
    this._teamState.team = state;

    // Update the state parameters;
    this.updateStateParams({ dashboard: this._teamState });

    // Re-display the jobs/cases as necessary;
    this.displayState();
  }

  /**
   * @description Helper function from teamState that selects which jobs to view
   */
  displayState() {
    this._selectedItems = this._teamState.team ?
      [ this._teamJobs, this._teamCases ] : [ this._myJobs, this._myCases ];
  }

  /**
   * @param {Object} parameters Params to add to the state params;
   * @description Helper function to update the state params
   */
  updateStateParams(parameters) {
    _.merge(this._$stateParams, parameters);
  }

  get casePaddingOffset() {
    // If showIndex is negative, the card is going to be one of the last 4
    // cards in the list, so we need to add padding
    const showIndex = _.size(this.selectedItems[1]) -
      ASSET_CARD_PADDING_LIMIT - _.parseInt(this.expandedAssetIndex);

    return showIndex < 0 ? Math.abs(showIndex) : 0;
  }

  /**
   * @param {Number} assetId the ID of the asset
   * @param {String} type what type of row the asset is on: 'job' or 'case'
   * @param {Number} index the ng-repeat $index of the row
   * @returns {Boolean} 'true' if the given parameters match the equivalent for
   * the currently active asset card, 'false' if otherwise
   */
  isCurrentExpandedCard(assetId, type, index) {
    return _.get(this.expandedAsset, 'id') === assetId &&
      this.expandedAssetType === type &&
      this.expandedAssetIndex === index;
  }

  /**
   * Hides the current asset card, and sets the case grid-block padding to 0
   */
  hideAssetCard() {
    this.expandedAsset = null;
    this.expandedAssetType = null;
    this.expandedAssetIndex = null;
  }

  /**
   * Displays an asset card
   * @param {Number} id the ID of the asset
   * @param {String} type what type of row the asset is on: 'job' or 'case'
   * @param {Number} index the ng-repeat $index of the row
   * @returns {undefined} nothing
   */
  async showAssetCard(id, type, index) {
    // Set variables for use in isCurrentExpandedCard() and hideAssetCard()
    this.expandedAsset = _.find(this.assets, { id });
    this.expandedAssetType = type;
    this.expandedAssetIndex = index;
  }

  /**
   * @param {Number} id The id of the asset
   * @return {Object} asset The single asset we're interested in
   */
  async _itemAsset(id) {
    return this._assetDecisionService.get(id);
  }

  get assets() {
    return this._assets;
  }

  get workItemType() {
    return this._user.workItemType;
  }

  get filtered() {
    return !_.isEmpty(this._$stateParams.assetFilter);
  }

  get sortOrder() {
    return this._$stateParams.sortBy || this._assets.sortedBy;
  }

  set sortOrder(value) {
    this._$stateParams.sortBy = value;
  }

  /**
   * @description Reloads the page with the correct state and params;
   */
  async reload() {
    this._$state.reload();
  }
}
