import * as _ from 'lodash';

import Base from 'app/base.class';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class IhsService extends Base {
  /* @ngInject */
  constructor(
    Restangular,
    referenceDataService
  ) {
    super(arguments);
  }

  async get(imoNumber, populate = true) {
    return this._Restangular
      .one('ihs', imoNumber)
      .get()
      .then(populate ? this.hydrateFlag.bind(this) : _.identity);
  }

  async hydrateFlag(ihsAsset) {
    const flag = _.find(
      await this._referenceDataService.get('flag.flags'),
      { id: _.parseInt(_.get(ihsAsset, 'ihsAsset.flag')) });

    return flag ?
      _.set(ihsAsset, 'ihsAsset.flag', flag) :
      ihsAsset;
  }
}
