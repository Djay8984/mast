import * as _ from 'lodash';

import Base from 'app/base.class';

export default class HarmonisationDateService extends Base {
  /* @ngInject */
  constructor(
    moment,
    serviceStatus
  ) {
    super(arguments);
    this._timeConstants = {
      maxSSPostponementLaker: {
        momentTimeKey: 'M',
        value: 12
      },
      maxSSPostponementNonLaker: {
        momentTimeKey: 'M',
        value: 3
      },
      threshold60MonthCycle: {
        momentTimeKey: 'M',
        value: 33
      },
      threshold72MonthCycle: {
        momentTimeKey: 'M',
        value: 45
      }
    };
    this.serviceGroupConstants = {
      idHullIWW: 14,
      idHullLaker: 16
    };
  }

  _formatDate(dateArg) {
    return this._moment(dateArg).format();
  }

  _addOneToDueDate(dueDate) {
    return this._moment(dueDate).add(1, 'd').format();
  }

  _addOneAndMaxSSPPLakerToDueDate(dueDate) {
    return this._moment(dueDate)
      .add(
        this._timeConstants.maxSSPostponementLaker.value,
        this._timeConstants.maxSSPostponementLaker.momentTimeKey
      )
      .add(1, 'd')
      .format();
  }

  _getRules() {
    // NON-CONVENTIONAL FUNCTION NAMING.
    // Due to complexity of the rule naming,
    // I have used abbreviations of the calculations being used against
    // completionDate as the rules' method names and annotated the rule
    // as a comment above each method name.
    return {

      // DS.completionDate <= SS.completionDate < SS.lowerRangeDate
      DSCDlteSSCDltSSLRD: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDateDS &&
          serviceData.completionDateSS &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDateSS).isBetween(
            serviceData.completionDateDS,
            serviceData.calculatedEffectiveLowerRangeDate,
            'day',
            '[)'
          )
        ) {
          return self._formatDate(serviceData.completionDateSS);
        }
      },

      // DS.completionDate <= SS.completionDate
      // &&
      // SS.lowerRangeDate
      // <= SS.completionDate <= SS.dueDate + threshold60MonthCycle
      DSCDlteSSCDAndSSLRDlteSSCDlteSSDDplus60MC: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDateDS &&
          serviceData.completionDateSS &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDateDS).isSameOrBefore(
            serviceData.completionDateSS, 'day'
          ) &&
          self._moment(serviceData.completionDateSS).isBetween(
            serviceData.calculatedEffectiveLowerRangeDate,
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold60MonthCycle.value,
              self._timeConstants.threshold60MonthCycle.momentTimeKey
            ),
            'day',
            '[]'
          )
        ) {
          return self._addOneToDueDate(serviceData.calculatedEffectiveDueDate);
        }
      },

      // SS.completionDate >= DS.completionDate
      // &&
      // SS.completionDate > SS.dueDate + threshold60MonthCycle
      DSCDlteSSCDAndSSCDgtSSDDplus60MC: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDateDS &&
          serviceData.completionDateSS &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDateDS).isSameOrBefore(
            serviceData.completionDateSS,
            'day'
          ) &&
          self._moment(serviceData.completionDateSS).isAfter(
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold60MonthCycle.value,
              self._timeConstants.threshold60MonthCycle.momentTimeKey
            ),
            'day'
          )
        ) {
          return self._formatDate(serviceData.completionDateSS);
        }
      },

      // SS.completionDate < DS.completionDate <=
      // SS.completionDate + maxSSPostponementLaker
      SSCDltDSCDlteSSCDplusMSSPPL: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDateDS &&
          serviceData.completionDateSS &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDateDS).isBetween(
            self._moment(serviceData.completionDateSS),
            self._moment(serviceData.completionDateSS).add(
              self._timeConstants.maxSSPostponementLaker.value,
              self._timeConstants.maxSSPostponementLaker.momentTimeKey
            ),
            'day',
            '(]'
          )
        ) {
          return this._formatDate(serviceData.completionDateDS);
        }
      },

      // DS.completionDate > SS.completionDate + maxSSPostponementLaker
      DSCDgtSSCDplusMSSPPL: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDateDS &&
          serviceData.completionDateSS &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDateDS).isAfter(
            self._moment(serviceData.completionDateSS).add(
              self._timeConstants.maxSSPostponementLaker.value,
              self._timeConstants.maxSSPostponementLaker.momentTimeKey
            ),
            'day'
          )
        ) {
          return self._addOneAndMaxSSPPLakerToDueDate(
            serviceData.calculatedEffectiveDueDate);
        }
      },

      // completionDate not falsey
      CDExists: (service) =>
        _.get('scheduledService.completionDate'),

      // completionDate < lowerRangeDate
      CDltLRD: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity &&
          self._moment(serviceData.completionDate).isBefore(
            serviceData.calculatedEffectiveLowerRangeDate,
            'day'
          )
        ) {
          return self._formatDate(serviceData.completionDate);
        }
      },

      // lowerRangeDate <= completionDate <= dueDate
      LRDlteCDlteDD: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity &&
          self._moment(serviceData.completionDate).isBetween(
            serviceData.calculatedEffectiveLowerRangeDate,
            serviceData.calculatedEffectiveDueDate,
            'day',
            '[]'
          )
        ) {
          return self._addOneToDueDate(serviceData.calculatedEffectiveDueDate);
        }
      },

      // dueDate < completionDate < dueDate + maxSSPostponementNonLaker
      DDltCDltDDplusSSPPNL: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity &&
          self._moment(serviceData.completionDate).isBetween(
            serviceData.calculatedEffectiveDueDate,
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.maxSSPostponementNonLaker.value,
              self._timeConstants.maxSSPostponementNonLaker.momentTimeKey
            ),
            'day',
            '()'
          )
        ) {
          return self._addOneToDueDate(serviceData.calculatedEffectiveDueDate);
        }
      },

      // dueDate + maxSSPostponementNonLaker <=
      // completionDate <= dueDate + threshold60MonthCycle
      // &&
      // cyclePeriodicity === 60
      DDplusPPNLlteCDlteDDplus60MC: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDate).isBetween(
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.maxSSPostponementNonLaker.value,
              self._timeConstants.maxSSPostponementNonLaker.momentTimeKey
            ),
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold60MonthCycle.value,
              self._timeConstants.threshold60MonthCycle.momentTimeKey
            ),
            'day',
            '[]'
          )
        ) {
          return self._addOneToDueDate(serviceData.calculatedEffectiveDueDate);
        }
      },

      // dueDate + maxSSPostponementNonLaker <= completionDate <=
      // dueDate + threshold72MonthCycle
      // &&
      // cyclePeriodicity === 72
      DDplusPPNLlteCDlteDDplus72MC: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 72 &&
          self._moment(serviceData.completionDate).isBetween(
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.maxSSPostponementNonLaker.value,
              self._timeConstants.maxSSPostponementNonLaker.momentTimeKey
            ),
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold72MonthCycle.value,
              self._timeConstants.threshold72MonthCycle.momentTimeKey
            ),
            'day',
            '[]'
          )
        ) {
          return self._addOneToDueDate(serviceData.calculatedEffectiveDueDate);
        }
      },

      // dueDate + threshold60MonthCycle < completionDate
      // && cyclePeriodicity === 60
      DDplus60MCltCD: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 60 &&
          self._moment(serviceData.completionDate).isAfter(
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold60MonthCycle.value,
              self._timeConstants.threshold60MonthCycle.momentTimeKey
            ),
            'day'
          )
        ) {
          return self._formatDate(serviceData.completionDate);
        }
      },

      // dueDate + threshold72MonthCycle < completionDate
      // && cyclePeriodicity === 72
      DDplus72MCltCD: (
        self,
        serviceData
      ) => {
        if (
          serviceData.completionDate &&
          serviceData.calculatedEffectiveLowerRangeDate &&
          serviceData.calculatedEffectiveDueDate &&
          serviceData.cyclePeriodicity === 72 &&
          self._moment(serviceData.completionDate).isAfter(
            self._moment(serviceData.calculatedEffectiveDueDate).add(
              self._timeConstants.threshold72MonthCycle.value,
              self._timeConstants.threshold72MonthCycle.momentTimeKey
            ),
            'day'
          )
        ) {
          return self._formatDate(serviceData.completionDate);
        }
      }
    };
  }

  /**
   * @description Iterates through all rules returned by _getRules(),
   * optionally filtering against ruleList param, until a match
   * is found, returning the date passed back from the rule.
   * @param {Object} serviceData Object containing date properties to be used
   * in rules calculations.
   * @param {Array} [rulesList] List of strings which match the keys of
   * rules to be run
   * @returns {string} The harmonisation date which was returned by the first
   * rule successfully run against the serviceData
   */
  _testRules(serviceData, rulesList = []) {
    let potentialHarmonisationDate = false;

    // filter data
    const rules = _.filter(
      this._getRules(),
      (rule, key) =>
        _.isEmpty(rulesList) || _.includes(rulesList, key)
    );
    _.forEach(rules, (rule) => {
      potentialHarmonisationDate = rule(this, serviceData);
      return !potentialHarmonisationDate;
    });
    if (potentialHarmonisationDate) {
      return potentialHarmonisationDate;
    }
  }

  /**
   * @description Iterates through all services picking out Hull_IWW services
   * and applying the only Hull_IWW applicable rule, to potentially generate
   * harmonisation date(s).
   * @param {array} serviceData List of all services to be filtered, and have
   * rule applied to.
   * @returns {array} List of harmonisation dates calculated from the
   * serviceData.
   */
  _testHullIWW(serviceData) {
    const executeFlow = _.flow([
      (data) => _.filter(
        data, (service) =>
          service.serviceCatalogue.serviceGroup.id === this
            .serviceGroupConstants.idHullIWW &&
          _.includes(['SS', 'CSH'], service.serviceCatalogue.code) &&

          // status is complete
          _.eq(service.surveyStatus.id, this._serviceStatus.get('complete')) &&
          (
            _.get(service, 'scheduledService.completionDate') ||
              service.dateOfCrediting
          )
      ),
      (data) => _.map(
        data, (service) => this._formatDate(
          _.get(service, 'scheduledService.completionDate') ||
            service.dateOfCrediting
        )
      )
    ]);
    const potentialHarmonisationDates = executeFlow(serviceData);
    if (!_.isEmpty(potentialHarmonisationDates)) {
      return potentialHarmonisationDates;
    }
  }

  /**
   * @description Iterates through all services picking out Hull_Laker services
   * and applying the Hull_Laker applicable rules, to potentially generate
   * harmonisation date(s).
   * @param {array} serviceData List of all services to be filtered, and have
   * rule applied to.
   * @returns {array} List of harmonisation dates calculated from the
   * serviceData.
   */
  _testHullLaker(serviceData) {
    const executeFlow = _.flow([

      // remove data where required proerties don't exist
      (data) => _.filter(
        data, (service) =>
          service.serviceCatalogue.serviceGroup.id === this
            .serviceGroupConstants.idHullLaker &&

          // status is complete
          _.eq(service.surveyStatus.id, this._serviceStatus.get('complete')) &&
          (
            _.get(service, 'scheduledService.completionDate') ||
              service.dateOfCrediting
          ) &&
          _.get(service, 'scheduledService.cyclePeriodicity') &&
          _.get(service, 'scheduledService.calculatedEffectiveDueDate') &&
          _.get(service, 'scheduledService.calculatedEffectiveLowerRangeDate')
      ),

      // group data by serviceCatalogue.code 'SS'|'DS'|other
      (data) => _.groupBy(data, (service) => _.get(
          ['SS', 'DS'],
          _.indexOf(['SS', 'DS'], service.serviceCatalogue.code)
        )
      ),

      // remove falsey values from array
      (data) => _.compact(

        // flatten array (ie destructure all sub arrays into main)
        _.flatMap(
          data.DS,
          (serviceDS) => {
            let someHarmonisationDates = [];

            // forEach DS record, start looping through services where
            // serviceCatalogue.code === 'SS'
            _.forEach(
              data.SS,
              (serviceSS) => {
                const combinedData = {
                  completionDateDS:
                    _.get(serviceDS, 'scheduledService.completionDate') ||
                    serviceDS.dateOfCrediting,
                  completionDateSS:
                    _.get(serviceSS, 'scheduledService.completionDate') ||
                    serviceSS.dateOfCrediting,
                  cyclePeriodicity:
                    _.get(serviceSS, 'scheduledService.cyclePeriodicity'),
                  dueDate:
                    _.get(serviceSS,
                    'scheduledService.calculatedEffectiveDueDate'),
                  lowerRangeDate:
                    _.get(serviceSS,
                    'scheduledService.calculatedEffectiveLowerRangeDate')
                };
                someHarmonisationDates = _.concat(
                  someHarmonisationDates,
                  this._testRules(
                    combinedData,
                    [
                      'DSCDlteSSCDltSSLRD',
                      'DSCDlteSSCDAndSSLRDlteSSCDlteSSDDplus60MC',
                      'DSCDlteSSCDAndSSCDgtSSDDplus60MC',
                      'SSCDltDSCDlteSSCDplusMSSPPL',
                      'DSCDgtSSCDplusMSSPPL'
                    ]
                  )
                );
              }
            );

            // return an array of possible harmonisation dates
            // returned from each SS service, into the _.map of
            // ds services
            return someHarmonisationDates;
          }
        )
      )
    ]);
    const potentialHarmonisationDates = executeFlow(serviceData);
    if (!_.isEmpty(potentialHarmonisationDates)) {
      return potentialHarmonisationDates;
    }
  }


  /**
   * @description Iterates through all services picking out services not in
   * the Hull_Laker or Hull_IWW service groups, then applying applicable rules,
   * to potentially generate harmonisation date(s).
   * @param {array} serviceData List of all services to be filtered, and have
   * rule applied to.
   * @returns {array} List of harmonisation dates calculated from the
   * serviceData.
   */
  _testOther(serviceData) {
    const executeFlow = _.flow([
      (data) => _.filter(
        data, (service) =>
          !_.includes(
            [
              this.serviceGroupConstants.idHullIWW,
              this.serviceGroupConstants.idHullLaker
            ],
            service.serviceCatalogue.serviceGroup.id
          ) &&
            _.includes(['SS', 'CSH'], service.serviceCatalogue.code) &&

          // status is complete
          _.eq(service.surveyStatus.id, this._serviceStatus.get('complete')) &&
          (
            _.get(service, 'scheduledService.completionDate') ||
              service.dateOfCrediting
          ) &&
          _.get(service, 'scheduledService.cyclePeriodicity') &&
          _.get(service, 'scheduledService.calculatedEffectiveDueDate') &&
          _.get(service, 'scheduledService.calculatedEffectiveLowerRangeDate')
      ),
      (data) => _.compact(
        _.map(
          data, (service) => {
            service.scheduledService.completionDate =
              _.get(service, 'scheduledService.completionDate') ||
                service.dateOfCrediting;
            return this._testRules(
              service.scheduledService,
              [
                'CDltLRD',
                'LRDlteCDlteDD',
                'DDltCDltDDplusSSPPNL',
                'DDplusPPNLlteCDlteDDplus60MC',
                'DDplusPPNLlteCDlteDDplus72MC',
                'DDplus60MCltCD',
                'DDplus72MCltCD'
              ]
            );
          }
        )
      )
    ]);
    const potentialHarmonisationDates = executeFlow(serviceData);
    if (!_.isEmpty(potentialHarmonisationDates)) {
      return potentialHarmonisationDates;
    }
  }

  /**
   * @summary calculateEffectiveDates(surveys)
   * @description Set the correct dates on the object by checking the manual
   * and automatically  generated dates, giving priority to the manual date
   * @param {Array} surveys The surveys to copy and massage/mutate
   * @returns {Array} a copy of the array passed in plus the calculated dates
   */
  calculateEffectiveDates(surveys) {
    const newSurveys = _.reject(surveys, { 'scheduledService': null });
    return _.map(newSurveys, (survey) => {
      const newSurvey = _.cloneDeep(survey);
      newSurvey.scheduledService.calculatedEffectiveDueDate =
          newSurvey.scheduledService.dueDateManual ||
            newSurvey.scheduledService.dueDate;
      newSurvey.scheduledService.calculatedEffectiveLowerRangeDate =
          newSurvey.scheduledService.lowerRangeDateManual ||
            newSurvey.scheduledService.lowerRangeDate;
      return newSurvey;
    });
  }

  /**
   * @description Attempts to return a single harmonisation date, by
   * concatenating and filter
   * service data to processing functions, which return harmonisation dates
   * @param {array} serviceData List of all services to be filtered, and have
   * rule applied to.
   * @returns {string} A single harmonisation date calculated from the
   * serviceData.
   */
  getHarmonisationDate(serviceData) {
    const serviceDataWithCalculatedDates =
      this.calculateEffectiveDates(serviceData);
    const harmonisationDate = _.uniq(
      _.compact(
        _.concat(
          [],
          this._testHullIWW(serviceDataWithCalculatedDates),
          this._testHullLaker(serviceDataWithCalculatedDates),
          this._testOther(serviceDataWithCalculatedDates)
        )
      )
    );
    return _.get(
      harmonisationDate,
      _.toSafeInteger(
        _.eq(
          _.size(harmonisationDate), 1
        )
      ) - 1,
      null
    );
  }
}
