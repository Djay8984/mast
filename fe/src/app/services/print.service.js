import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintHelper from 'app/components/print/print.helper';
import PrintScreenTemplate from
'app/components/print/templates/print-screen.template';
import PrintJobDetails from 'app/components/print/templates/print-job-details';

import JobReport from
  'app/components/print/templates/job-report.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class PrintService extends Base {

  /* @ngInject */
  constructor(
    $log,
    $filter,
    $state,
    $stateParams,
    $rootScope,
    $q,
    moment,
    appConfig,
    assetDecisionService,
    assetModelDecisionService,
    codicilDecisionService,
    creditingDecisionService,
    defectDecisionService,
    employeeDecisionService,
    jobDecisionService,
    referenceDataService,
    reportDecisionService,
    actionableItemStatus,
    assetNoteStatus,
    confidentialityType,
    reportType,
    dateFormat,
    node
  ) {
    super(arguments);
    this._printHelper = new PrintHelper(
        this._$log, this._dateFormat, this._moment);
    this.printables = [];
  }

  async getAsset() {
    // Get asset details that will be displayed as PDF header
    const asset = await this._assetDecisionService.get(
      this._$stateParams.assetId);
    asset.IMOnumber = asset.ihsAsset ? asset.ihsAsset.id : '';
    return asset;
  }

  async getAssetItem(itemId) {
    return this._assetModelDecisionService.getItem(itemId);
  }

  get stateName() {
    return this._$state.current.name;
  }

  pagesToPrint(state, option) {
    if (option === 'add') {
      this.printables.push(state);
      this._$rootScope.$broadcast('showPrintButton', this.printables);
    } else if (option === 'remove') {
      if (this.printables.length === 2 &&
        this.printables[0] === this.printables[1] &&
        this.printables[0] === state
      ) {
        this.printables = [state];
      } else {
        this.printables = _.without(this.printables, state);
        this._$rootScope.$broadcast('showPrintButton', this.printables);
      }
    }
  }

  async printJobDetails(stateName) {
    this._$q.all([
      this._jobDecisionService.get(this._$stateParams.jobId),
      this._employeeDecisionService.getCurrentUser(),
      this._referenceDataService.get(['job']),
      this._assetDecisionService.get(this._$stateParams.assetId)
    ]).then((result) => {
      const jobInfo = {
        job: result[0],
        currentUser: result[1],
        jobReferenceData: result[2],
        asset: result[3]
      };

      const printJobDetails = new PrintJobDetails(
        this._printHelper,
        stateName,
        this._$log,
        this._appConfig,
        jobInfo,
        this._$q
      );

      printJobDetails.generateJobDetails();
    });
  }

  async printReport() {
    const report = new JobReport(
      this._$log,
      this._$state,
      this._$stateParams,
      this._moment,
      this._assetDecisionService,
      this._codicilDecisionService,
      this._creditingDecisionService,
      this._jobDecisionService,
      this._referenceDataService,
      this._reportDecisionService,
      this._actionableItemStatus,
      this._appConfig,
      this._assetNoteStatus,
      this._confidentialityType,
      this._dateFormat,
      this._node,
      this._reportType
    );

    report.print();
  }

  async printDynamicPage(stateName) {
    switch (stateName) {
      case 'asset.jobs.view':
        this.printJobDetails(stateName);
        break;
      case 'reporting.view':
        this.printReport();
        break;
      default:
    }
  }

  async print(data, stateName) {
    if (data.dynamicPage) {
      this.printDynamicPage(stateName);
      return false;
    }
    const printToolset = {
      printHelper: this._printHelper,
      stateName,
      $log: this._$log,
      appConfig: this._appConfig,
      refService: this._referenceDataService,
      $q: this._$q,
      asset: await this.getAsset()
    };
    this.template = new PrintScreenTemplate(printToolset);
    const isPrinted = await this.template.print(data, this);
    return isPrinted;
  }
}
