export default ({
  allowedAttributeValues: {
    searchKey: 'allowedWorkItemAttributeValues',
    referenceKey: 'job'
  },
  assetCategory: {
    searchKey: 'assetCategories',
    referenceKey: 'asset'
  },
  assetLifecycleStatus: {
    referenceKey: 'asset',
    searchKey: 'lifecycleStatuses'
  },
  assetType: {
    searchKey: 'assetTypes',
    referenceKey: 'asset'
  },
  assignedSchedulingType: {
    searchKey: 'assignedSchedulingTypes',
    referenceKey: 'service'
  },
  associatedSchedulingType: {
    searchKey: 'associatedSchedulingTypes',
    referenceKey: 'service'
  },
  attachmentType: {
    searchKey: 'attachmentTypes',
    referenceKey: 'attachment'
  },
  attributeDataType: {
    searchKey: 'attributeTypeValues',
    referenceKey: 'asset'
  },
  attributeType: {
    searchKey: 'attributeTypes',
    referenceKey: 'asset'
  },
  businessProcess: {
    searchKey: 'businessProcesses',
    referenceKey: 'case'
  },
  caseStatus: {
    searchKey: 'caseStatuses',
    referenceKey: 'case'
  },
  caseType: {
    searchKey: 'caseTypes',
    referenceKey: 'case'
  },
  classDepartment: {
    searchKey: 'classDepartments',
    referenceKey: 'asset'
  },
  classMaintenanceStatus: {
    searchKey: 'classMaintenanceStatuses',
    referenceKey: 'asset'
  },
  classSociety: {
    searchKey: 'iacsSocieties',
    referenceKey: 'asset'
  },
  classStatus: {
    searchKey: 'classStatuses',
    referenceKey: 'asset'
  },
  coClassificationSociety: {
    searchKey: 'iacsSocieties',
    referenceKey: 'asset'
  },
  confidentialityType: {
    searchKey: 'confidentialityTypes',
    referenceKey: 'attachment'
  },
  creditedBy: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  completedBy: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  creditStatus: {
    searchKey: 'creditStatuses',
    referenceKey: 'job'
  },
  customerFunction: {
    searchKey: 'partyFunctions',
    referenceKey: 'party',
    omit: ['deleted']
  },
  defectCategory: {
    searchKey: 'defectCategories',
    referenceKey: 'defect'
  },
  defectStatus: {
    searchKey: 'defectStatuses',
    referenceKey: 'defect'
  },
  defectValue: {
    searchKey: 'defectDetails',
    referenceKey: 'defect'
  },
  dueStatus: {
    searchKey: 'dueStatuses',
    referenceKey: 'miscellaneous'
  },
  employee: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  flags: {
    searchKey: 'flags',
    referenceKey: 'flag'
  },
  flagState: {
    searchKey: 'flags',
    referenceKey: 'flag'
  },
  jobStatus: {
    searchKey: 'jobStatuses',
    referenceKey: 'job',
    constant: 'jobStatus'
  },
  harmonisationType: {
    searchKey: 'harmonisationTypes',
    referenceKey: 'service'
  },
  losingSociety: {
    searchKey: 'iacsSocieties',
    referenceKey: 'asset'
  },
  lrEmployee: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  milestone: {
    searchKey: 'milestones',
    referenceKey: 'case'
  },
  office: {
    searchKey: 'office',
    referenceKey: 'employee'
  },
  officeRole: {
    searchKey: 'officeRoles',
    referenceKey: 'case'
  },
  preEicInspectionStatus: {
    searchKey: 'preEicInspectionStatuses',
    referenceKey: 'case'
  },
  previousRuleSet: {
    searchKey: 'ruleSets',
    referenceKey: 'asset'
  },
  productType: {
    searchKey: 'productTypes',
    referenceKey: 'product'
  },
  productGroup: {
    searchKey: 'productGroups',
    referenceKey: 'product'
  },
  productRuleSet: {
    searchKey: 'rulesetCategories',
    referenceKey: 'asset'
  },
  proposedFlagState: {
    searchKey: 'flags',
    referenceKey: 'flag'
  },
  raisedBy: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  registeredPort: {
    searchKey: 'portsOfRegistry',
    referenceKey: 'flag'
  },
  relationship: {
    searchKey: 'partyRoles',
    referenceKey: 'party',
    omit: ['deleted']
  },
  repairAction: {
    searchKey: 'repairActions',
    referenceKey: 'repair'
  },
  repairTypes: {
    searchKey: 'repairTypes',
    referenceKey: 'repair'
  },
  riskAssessmentStatus: {
    searchKey: 'riskAssessmentStatuses',
    referenceKey: 'case'
  },
  ruleSet: {
    searchKey: 'ruleSets',
    referenceKey: 'asset'
  },
  schedulingDueType: {
    searchKey: 'schedulingDueTypes',
    referenceKey: 'service'
  },
  schedulingRegime: {
    searchKey: 'schedulingRegimes',
    referenceKey: 'service'
  },
  schedulingType: {
    searchKey: 'schedulingTypes',
    referenceKey: 'service'
  },
  scopeConfirmedBy: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  serviceCatalogue: {
    searchKey: 'serviceCatalogues',
    referenceKey: 'service'
  },
  serviceCreditStatus: {
    searchKey: 'serviceCreditStatuses',
    referenceKey: 'service'
  },
  serviceGroup: {
    searchKey: 'serviceGroups',
    referenceKey: 'service'
  },
  serviceRuleset: {
    searchKey: 'serviceRulesets',
    referenceKey: 'service'
  },
  serviceStatus: {
    searchKey: 'serviceStatuses',
    referenceKey: 'service'
  },
  serviceType: {
    searchKey: 'serviceTypes',
    referenceKey: 'service'
  },
  surveyor: {
    searchKey: 'employee',
    referenceKey: 'employee'
  },
  surveyStatus: {
    searchKey: 'serviceCreditStatuses',
    referenceKey: 'service'
  },
  workItemType: {
    searchKey: 'workItemTypes',
    referenceKey: 'job'
  }
});
