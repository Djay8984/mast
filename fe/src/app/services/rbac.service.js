import * as _ from 'lodash';

import Base from 'app/base.class';

export default class RBACService extends Base {
  /* @ngInject */
  constructor(
    rbacActions,
    rbacGroupPermissions
  ) {
    super(arguments);
  }

  /**
   * @param {string} message for error
   * @return {function} checks if param is empty & throw error if so
   * designed mainly for use in _.chain with a .tap
   */
  _emptyError(message) {
    return (data) => {
      if (_.isEmpty(data)) {
        throw new Error(message);
      } else {
        return data;
      }
    };
  }

  /**
   * @param {string[]} groups single or comma delimited groups
   * @return {string[]} names of things these groups can do
   */
  availableActions(groups = []) {
    const permissions = _.chain(this._rbacGroupPermissions.toObject())
      .pick(groups)
      .tap(this._emptyError(`invalid groups: ${groups}`))
      .values()
      .flatten()
      .uniq()
      .value();

    return _.keys(_.pickBy(this._rbacActions.toObject(),
        (value) => _.includes(permissions, value)));
  }

  /**
   * @param {string[]} groups single or comma delimited groups
   * @param {string} permission what permission to check
   * @return {boolean} whether or not the group(s) is permitted to do the action
   */
  can(groups = [], permission) {
    return _.chain(this._rbacGroupPermissions.toObject())
      .pick(groups)
      .tap(this._emptyError(`invalid groups: ${groups}`))
      .values()
      .flatten()
      .uniq()
      .includes(this._rbacActions.get(permission))
      .value();
  }
}
