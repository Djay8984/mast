import Base from 'app/base.class';
import * as _ from 'lodash';
import fs from 'fs';
import { remote, ipcRenderer } from 'electron';
import uuid from 'node-uuid';


export default class NodeService extends Base {
  /* @ngInject */
  constructor(
    $log,
    $q,
    $state,
    $rootScope,
    Restangular,
    appConfig
  ) {
    super(arguments);
  }

  // This should either be in the in employee service on in its own service
  // but it's currently being used a lot.
  async getUser() {
    await this._$rootScope.READY;
    if (!this._$rootScope.CONNECTED) {
      return {
        fullName: null,
        groups: null,
        id: null,
        deleted: null
      };
    }
    if (!this._user) {
      const { id, userName, groupList, deleted } =
        await this._Restangular.one('current-user').get();

      this._user = {
        fullName: userName,
        groups: _.map(groupList, _.toLower),
        id,
        deleted
      };
    }

    return this._user;
  }

  async stat(file) {
    const promisify = (method, ...args) => new Promise((resolve, reject) => {
      method(...args, (err, result) => err ? reject(err) : resolve(result));
    });
    await promisify(fs.stat, file.path);
  }

  isDev() {
    return remote.process.env.WEBPACK_ENV === 'dev';
  }

  showSaveDialog(fileName) {
    return remote.dialog.showSaveDialog({ defaultPath: fileName });
  }

  saveFile(path, contents, encoding = 'binary') {
    fs.writeFile(path, contents, encoding, (err) => {
      if (err) {
        throw err;
      }
    });
  }

  uuid() {
    return uuid.v4();
  }

  async clearSession() {
    const deferred = this._$q.defer();
    const resolve = deferred.resolve.bind(deferred);
    remote.session.defaultSession.clearStorageData(resolve);
    return deferred.promise;
  }

  registerRedirectListener() {
    const goToStartup = this._$state.go.bind(this, 'startup');
    ipcRenderer.on('redirect', goToStartup);
  }
}
