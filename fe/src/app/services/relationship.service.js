import * as _ from 'lodash';

import Base from 'app/base.class';

export default class AttributeService extends Base {
  /* @ngInject */
  constructor(
    Restangular,
    referenceDataService
  ) {
    super(arguments);
  }

  _core(assetId, itemId, relationshipId) {
    return this._Restangular
      .one('asset', assetId)
      .one('item', itemId)
      .one('relationship', relationshipId);
  }

  async create(assetId, itemId, toItemId, relationshipTypeId) {
    return this
      ._core(assetId, itemId)
      .customPOST({
        toItem: { id: toItemId },
        type: { id: relationshipTypeId }
      });
  }

  async remove(assetId, itemId, relationshipId) {
    return this
      ._core(assetId, itemId, relationshipId)
      .customDELETE();
  }

  validateRelationship(itemFrom, itemTo) {
    const isNotSameItem = itemFrom.id !== itemTo.id;
    const isNotParentItem = _.isUndefined(
        _.find(itemTo.items, { id: itemFrom.id }));
    const isNewRelationship = _.isUndefined(
        _.find(itemFrom.related, { toItem: { id: itemTo.id } }));

    return _.every([isNotSameItem, isNotParentItem, isNewRelationship]);
  }
}
