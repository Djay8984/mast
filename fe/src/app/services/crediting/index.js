import angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import creditingOfflineService from './crediting.offline.service';
import creditingOnlineService from './crediting.online.service';

export default angular
  .module('app.services.crediting', [])
  .service('creditingOfflineService', creditingOfflineService)
  .service('creditingOnlineService', creditingOnlineService)
  .service('creditingDecisionService', [
    'creditingOnlineService',
    'creditingOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
