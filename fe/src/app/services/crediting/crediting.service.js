import * as _ from 'lodash';

import * as angular from 'angular';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { Survey } from 'app/models';

const FIRST_ID_REGEX = /^\.?\d+\.?/;

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CreditingService extends
  ExternalAttributeMixin(BaseService) {

  /**
   * Recurses through the given asset model and strips the unnecessary
   * properties off it in order to reduce the time it takes to copy the model
   * into each survey.
   * Also populates the pathIds array with an entry for each item in the format
   * '1.2.3', where the right-most number is the ID of the current item
   * followed by its parent's ID to the left and so on.
   * Both the asset model and pathId array are modified in situ.
   * @param {AssetModel} assetModel the asset model to clean
   * @param {array} pathIds the array to populate with pathIds
   */
  _cleanAssetModel(assetModel, pathIds) {
    // strips irrelevant properties off each Item, and adds a 'task' property
    // also generates an element in pathIds
    const reduce = (currentItem, parentId) => {
      currentItem =
        _.pick(currentItem, ['id', 'displayOrder', 'name', 'items']);
      currentItem.tasks = null;

      const currentPathId = `${parentId}.${currentItem.id}`;
      pathIds.push(currentPathId);

      _.forEach(currentItem.items, (item, index) =>
        currentItem.items[index] = reduce(item, currentPathId));
      return currentItem;
    };

    reduce(assetModel, '');
  }

  /**
   * Recurses through all items on the survey (if there are any) and removes all
   * items which either have no tasks of their own, or no child items with tasks
   * (to an arbitrary depth). The resulting item hierarchy will therefore only
   * contain items that can be expanded to eventually real tasks.
   * @param {Survey} survey the target survey
   */
  _pruneItems(survey) {
    // Modifies the currentItem in situ
    const prune = (currentItem) => {
      // Prune all child items
      _.forEach(currentItem.items, (item, index) => {
        if (!prune(item)) {
          // Remove item from currentItem which didn't have any tasks/items
          // remaining after pruning
          _.set(currentItem, ['items', index], null);
        }
      });

      // Remove falsey values from the item list
      currentItem.items = _.compact(currentItem.items);

      // Return true if there are tasks or any items remaining on this item
      return currentItem.tasks || !_.isEmpty(currentItem.items);
    };

    prune(survey);
  }

  /**
   * Finds the Item with the ID in the first position of the given path and
   * returns it if it was the only element of the path. Otherwise, remove the
   * first path element and repeat on the found Item's items.
   * @param {array} items the current list of items
   * @param {array} path the path to the item
   * @returns {Item} the desired item
   */
  _retrieveItem(items, path) {
    const currentItem = _.find(items, { id: _.parseInt(_.first(path)) });
    return _.size(path) === 1 ? currentItem :
      this._retrieveItem(currentItem.items, _.tail(path));
  }

  async get(jobId, assetId) {
    let assetModel = {};
    if (assetId) {
      // get asset model for job
      assetModel = await this._assetModelDecisionService.get(assetId);
    }

    // get surveys for job
    const surveys =
      await this._surveyDecisionService.getAllSurveys(jobId, { size: null });

    // get (wip)tasks for surveys
    const tasks = await this._taskDecisionService.query(
      { surveyId: _.map(surveys, 'id') },
      { size: null },
      { endpoint: 'wip', jobId });

    // group all tasks based on survey and item
    const groupedTasks = _.groupBy(tasks, 'survey.id');
    _.forEach(groupedTasks, (task, index) =>
      groupedTasks[index] = _.groupBy(task, 'assetItem.id'));

    if (assetId && !_.isEmpty(surveys)) {
      let pathIds = [];

      // strip out superfluous properties to reduce angular.copy() time and
      // generate the list of pathIds
      assetModel = assetModel.plain();
      this._cleanAssetModel(assetModel, pathIds);

      // clean up pathIds
      pathIds =
        _.map(_.tail(pathIds), (id) => id = _.replace(id, FIRST_ID_REGEX, ''));

      /* eslint max-nested-callbacks: 0 */
      _.forEach(groupedTasks, (taskGroup, surveyId) => {
        // clone asset model into each survey that had tasks
        const survey = _.find(surveys, { id: _.parseInt(surveyId) });
        _.set(survey, 'items', angular.copy(assetModel.items));

        // assign each task group to relevant item
        _.forEach(taskGroup, (task, itemId) => {
          const pathId = _.find(pathIds, (path) => _.endsWith(path, itemId));

          if (pathId) {
            _.set(this._retrieveItem(survey.items, pathId.split('.')),
              'tasks', task);
          }
        });
      });

      // prune all survey hierarchies
      _.forEach(surveys, this._pruneItems);
    } else if (!_.isEmpty(surveys)) {
      _.forEach(groupedTasks, (taskGroup, surveyId) =>
        _.find(surveys, { id: _.parseInt(surveyId) }).tasks =
          _.reduce(taskGroup, (res, t) => _.union(res, t), [])
      );
    }

    return surveys;
  }

  getServiceTypeTaskCompletions(job, crediting, omitAutoGenerated = true) {
    if (omitAutoGenerated) {
      crediting = _.reject(crediting, (survey) =>
        !_.isEmpty(_.find(job.surveys,
            { id: survey.id, autoGenerated: true })));
    }

    const tasks = _.flatMap(job.surveys,
      (survey) => new Survey(survey,
        _.find(crediting, { id: survey.id })).tasks);

    const serviceTypes = [];
    _.forEach(crediting, (survey) => {
      const jobSurvey = _.find(job.surveys, { id: survey.id });
      if (!_.isUndefined(jobSurvey)) {
        const existing = _.find(serviceTypes,
          { code: jobSurvey.serviceCatalogue.code });

        if (!existing) {
          serviceTypes.push({
            code: jobSurvey.serviceCatalogue.code,
            surveys: [jobSurvey],
            tasks: _.filter(tasks, { survey: { id: survey.id } })
          });
        } else {
          existing.surveys.push(jobSurvey);
          existing.tasks.push(_.filter(tasks, { survey: { id: survey.id } }));
        }
      }
    });

    const completedStatuses = _.pick(this._creditStatus.toObject(),
      ['completed', 'confirmed', 'waived']);
    return _.forEach(serviceTypes, (type) => {
      const allTasks = _.size(type.tasks);
      const completedTasks = _.size(_.filter(type.tasks,
        (task) => {
          if (!_.isArray(task)) {
            return _.includes(completedStatuses, task.creditStatus.id);
          }
        }));
      type.tasksCompletion = completedTasks && allTasks ?
         Math.floor(completedTasks / allTasks * 100) : 0;
    });
  }

  async groupCreditingByProductType(job, crediting) {
    const productTypes =
      await this._referenceDataService.get('product.productTypes');

    // get a list of all item IDs which have defects on the current job
    const defects = await this._defectDecisionService.get({
      jobId: job.id,
      context: 'job'
    });
    const defectItems =
      _.map(_.flatMap(defects, 'items'), (item) => _.get(item, 'item.id'));

    _.forEach(productTypes,
      (productType) => _.set(productType, 'surveys', []));

    // finds the survey's product type & then adds the relevant crediting to
    // the group
    _.forEach(job.surveys, (survey) => {
      const creditingSurvey = _.find(crediting, { id: survey.id });
      if (creditingSurvey) {
        const surveyModel =
          new Survey(survey, creditingSurvey, this._creditStatus);

        // mark items which have defects assigned to them
        _.forEach(surveyModel.items, (item) =>
          _.set(item, 'hasDefect', _.includes(defectItems, item.id)));

        _.find(productTypes, { id:
          _.get(survey, 'serviceCatalogue.productCatalogue.productType.id')
        }).surveys.push(surveyModel);
      }
    });

    return productTypes;
  }
}
