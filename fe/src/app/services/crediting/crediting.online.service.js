import * as _ from 'lodash';

import CreditingService from './crediting.service';

export default class CreditingOnlineService extends CreditingService {
  /* @ngInject */
  constructor(
    Restangular,
    assetModelDecisionService,
    defectDecisionService,
    referenceDataService,
    surveyDecisionService,
    taskDecisionService,
    creditStatus
  ) {
    super(arguments);
  }

  /**
   * Processes a list of Survey models and PUTs a list of all provided surveys
   * and their Tasks
   * @param {int} jobId the surveys' job's ID
   * @param {int} assetId the surveys' job's asset's ID
   *  (used for getting the saved survey hierarchies back from the back end)
   * @param {Array} surveys the list of surveys models to update
   * @returns {Array} the updates surveys populated with their tasks
   */
  async update(jobId, assetId, surveys) {
    const tasksModels = _.map(_.flatMap(surveys, 'taskList'), 'model');
    _.forEach(tasksModels, (task) =>
      task.attributes = _.map(task.attributes, 'model'));

    const surveyModels = _.map(surveys, 'model');

    await this._surveyDecisionService.updateSurveys(jobId, surveyModels);
    await this._taskDecisionService.updateTasks(jobId, tasksModels);

    return this.get(jobId, assetId);
  }

  get _dataSource() {
    return this._Restangular;
  }
}
