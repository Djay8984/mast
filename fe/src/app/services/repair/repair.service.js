import * as _ from 'lodash';

import angular from 'angular';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class RepairService extends ExternalAttributeMixin(BaseService) {

  _core({
    assetId,
    jobId,
    codicilId,
    context,
    type
  }) {
    context = context || (jobId ? 'job' : 'asset');
    return this._dataSource
      .one(context === 'asset' ? 'asset' : 'job',
        context === 'asset' ? assetId : jobId)
      .one(context === 'asset' ? type : `wip-${type}`, codicilId);
  }

  _repair({
    assetId,
    jobId,
    codicilId,
    repairId,
    context,
    type
  }) {
    context = context || (jobId ? 'job' : 'asset');
    return this._core({ assetId, jobId, codicilId, type, context })
      .one(context === 'asset' ? 'repair' : 'wip-repair', repairId);
  }

  async _hydrate({ assetId, jobId, context }, dryRepair) {
    dryRepair = await this.populateAttributes(dryRepair);
    const savedPagination = angular.copy(dryRepair.pagination);

    const self = this;
    const hydrate = async (repair) => {
      if (_.get(repair, 'codicil.id')) {
        repair.codicil = await self._core({
          assetId,
          jobId,
          codicilId: _.get(repair, 'codicil.id'),
          type: 'coc',
          context
        }).get();
      }

      if (_.get(repair, 'defect.id')) {
        repair.defect = await self._core({
          assetId,
          jobId,
          codicilId: _.get(repair, 'defect.id'),
          type: 'defect',
          context
        }).get();
      }

      const itemIds = _.map(repair.repairs, 'item.item.id');
      if (!_.isEmpty(itemIds)) {
        const items =
          await this._assetModelDecisionService.getItems(assetId, itemIds);

        _.forEach(repair.repairs, (repairItem) => {
          const itemId = _.get(repairItem, 'item.item.id');
          _.set(repairItem, 'item.item', _.find(items, { id: itemId }));
        });
      }

      return repair;
    };

    if (_.isArray(dryRepair)) {
      const result = await this._$q.all(_.map(dryRepair, hydrate));
      return _.set(result, 'pagination', savedPagination);
    }
    return hydrate(dryRepair);
  }

  @pageable
  async get(type, params = {}) {
    const request = this._repair(_.merge(params, { type }));
    const repair = params.repairId ?
      request.get() :
      request.getList();
    return this._hydrate(_.merge(params, { type }), await repair);
  }

  async saveCoCRepair(params, payload) {
    return this._hydrate(_.merge(params, { type: 'coc' }),
      await this.get('coc', params, payload));
  }
}
