import * as _ from 'lodash';

import RepairService from './repair.service';

export default class RepairOfflineService extends RepairService {
  /* @ngInject */
  constructor(
    $q,
    assetModelDecisionService,
    codicilDecisionService,
    cocStatus,
    defectDecisionService,
    defectStatus,
    offlinular,
    referenceDataService,
    repairAction
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }

  /**
   * @param {object} newRepair newly created repair
   * @param {object} params query parameters with information about job/asset
   * updates a coc's status to deleted if new repair is permanent
   */
  async _updateCoCWithNewRepair(newRepair, params) {
    const parent = await this._codicilDecisionService.getCoC(params, false);
    if (newRepair.repairAction.id === this._repairAction.get('permanent')) {
      await this._codicilDecisionService.updateCoC(params,
        _.set(parent, 'status', {
          id: this._cocStatus.get('deleted'),
          _id: _.toString(this._cocStatus.get('deleted'))
        }));
    }
  }

  /**
   * @param {object} newRepair newly created repair
   * @param {object} params query parameters with information about job/asset
   * updates a defect's status to closed if there are permanent repairs for all
   *   items attached to the defect
   */
  async _updateDefectWithNewRepair(newRepair, params) {
    const parent = await this._defectDecisionService.get(params, false);
    const parentItemIds = _.map(parent.items, 'item.id');

    const children = await this._repair(
      _.merge(_.omit(params, 'repairId'), { type: 'defect' })).get();
    const childItemIds = _.chain(children)
      .filter({ repairAction: { id: this._repairAction.get('permanent') } })
      .flatMap('repairs')
      .flatMap('item.item.id')
      .value();

    // check if all items are permanently repaired, if so, closes defect
    if (_.isEmpty(_.difference(parentItemIds, childItemIds))) {
      await this._defectDecisionService.update(params,
        _.set(parent, 'defectStatus', {
          id: this._defectStatus.get('closed'),
          _id: _.toString(this._defectStatus.get('closed'))
        }));
    }
  }

  // saves the repair & updates the related codicil's status if relevant
  async save(type, params, payload) {
    const newRepair = await this._hydrate(_.merge(params, { type }),
      await this._repair(_.merge(params, { type })).customPOST(payload));

    await _.get({
      defect: this._updateDefectWithNewRepair.bind(this),
      coc: this._updateCoCWithNewRepair.bind(this)
    }, type)(newRepair, params);

    return newRepair;
  }
}
