import * as _ from 'lodash';

import RepairService from './repair.service';

export default class RepairOnlineService extends RepairService {
  /* @ngInject */
  constructor(
    $q,
    Restangular,
    assetModelDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }

  async save(type, params, payload) {
    return this._hydrate(_.merge(params, { type }),
      await this._repair(_.merge(params, { type })).customPOST(payload));
  }
}
