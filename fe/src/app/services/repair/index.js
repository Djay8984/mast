import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import repairOfflineService from './repair.offline.service';
import repairOnlineService from './repair.online.service';

export default angular
  .module('app.services.repair', [])
  .service('repairOfflineService', repairOfflineService)
  .service('repairOnlineService', repairOnlineService)
  .service('repairDecisionService', [
    'repairOnlineService',
    'repairOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
