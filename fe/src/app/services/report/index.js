import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import reportOfflineService from './report.offline.service';
import reportOnlineService from './report.online.service';

export default angular
  .module('app.services.report', [])
  .service('reportOfflineService', reportOfflineService)
  .service('reportOnlineService', reportOnlineService)
  .service('reportDecisionService', [
    'reportOnlineService',
    'reportOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
