import * as _ from 'lodash';

import * as angular from 'angular';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { Task } from 'app/models';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ReportService extends ExternalAttributeMixin(BaseService) {

  _core(jobId, reportId) {
    return this._dataSource
      .one('job', jobId)
      .one('report', reportId);
  }

  /**
   * @summary create(jobId, payload)
   * @param {Number} jobId The id of the job
   * @param {Object} payload The data for the report
   * @return {Object} report The newly created report
   */
  async create(jobId, payload) {
    return this.populateAttributes(await this._core(jobId)
      .customPOST(payload));
  }

  /**
   * @summary update(jobId, payload)
   * @param {Number} jobId The id of the job
   * @param {Number} reportId The id of the report
   * @param {Object} payload The report we're updating
   * @return {Object} report The newly updated reportId
   */
  async update(jobId, reportId, payload) {
    return await this._core(jobId, reportId)
      .customPUT(payload);
  }

  /**
   * @summary get(jobId, reportId)
   * @param {Number} jobId The id of the job
   * @param {Number} reportId The id of the report
   * @return {Object} report
   */
  async get(jobId, reportId) {
    return this.populateAttributes(await this._core(jobId, reportId)
      .get());
  }

  /**
   * @summary reduceCodicils(codicils)
   * @param {Object} codicils The codicils we want to filter for the user
   * @return {Object} codicils Newly filtered codicils
   */
  async reduceCodicils(codicils) {
    const user = await this._employeeDecisionService.getCurrentUser();
    const userId = user.id;
    return _.filter(codicils, (item) =>
      item.creditedBy.id === userId &&
      this._moment(item.actionTakenDate).format(this._dateFormat) ===
        this._moment(this._issueDate).format(this._dateFormat));
  }

  /**
   * @summary filterReport()
   * @description This filters the report objects by current date and user using
   *   promises
   * @param {Object} content The report we want to filter
   * @return {promise}
   */
  async filterReport(content) {
    return this._$q.all(_.mapValues(content, (codicils, index) =>
        this.reduceCodicils(codicils)
          .then((response) =>
            _.set(content, index, response))));
  }

  async generateTasks(surveys) {
    return await this._taskDecisionService.query(
      { surveyId: _.map(surveys, 'id') },
      { size: null },
      { endpoint: 'wip' });
  }

  /**
   * @summary prepareTasks(jobId)
   * @description Creates a list of the tasks for the job
   * @param {Number} jobId The id of the job
   * @param {Array} services The services we want the tasks for;
   * @return {Array} tasks
   */
  async prepareTasks(jobId, services) {
    const job = await this._jobDecisionService.get(jobId);
    const crediting = await this._creditingDecisionService.get(jobId);
    const serviceTasks = this._creditingDecisionService
      .getServiceTypeTaskCompletions(job, crediting);

    _.forEach(serviceTasks, (task) =>
      _.set(
        _.find(services, { id: _.first(task.surveys).id }),
          'tasksCompletion', task.tasksCompletion));

    // ensure that we only return task models
    return _.filter(_.flatMap(serviceTasks, 'tasks'), (task) =>
      task instanceof Task);
  }

  stripTasks(services) {
    return _.map(services, (service) =>
      _.omit(service, ['code', 'tasksCompletion', 'relatedTasks']));
  }

  stripModel(tasks) {
    return _.map(tasks, (task) => _.omit(task, ['model']));
  }

  /**
   * @summary createReport(jobId, type)
   * @param {Number} jobId The id of the job we're saving the report for
   * @param {String} type The type of report (Dar, far, dsr, fsr)
   * @param {String} date The ISO string date for issueDate / firstVisitDate
   * @param {String} submit If we want to submit this report
   * @param {String} daily The true/false filter switch
   * @param {Object} payload Payload for report submission
   * @description This method creates a report using the basic report, then
   *   accumulates all the necessary parts of the report to construct in the
   *   correct format for use in the template;
   * @return {Object} report
   */
  async createReport(jobId, type, date, submit = 'false', daily = 'false',
    payload) {
    // Create the basic report object (report) and also for FSR reports we need
    // a copy of the report to update the DSR if it was successfully approved
    // (dsrReport);
    let report = this.createBasicReport(jobId, type);
    const dsrReport = angular.copy(payload);

    let wipActionableItems = await this._codicilDecisionService
      .getActionableItem({ jobId });

    let wipAssetNotes = await this._codicilDecisionService
      .getAssetNote({ jobId });

    let wipCocs = await this._codicilDecisionService
      .getCoC({ jobId });
    const surveys = await this._surveyDecisionService
    .getAllSurveys(jobId, { size: null });

    // TODO: fix this hack;
    // TODO: write decorator to remove restangular;
    wipActionableItems = _.map(wipActionableItems, (i) => {
      const result = i.restangularized ? i.plain() : i;
      return _.omit(result, 'compositeId');
    });

    wipAssetNotes = _.map(wipAssetNotes, (i) => {
      const result = i.restangularized ? i.plain() : i;
      return _.omit(result, 'compositeId');
    });

    wipCocs = _.map(wipCocs, (i) => {
      const result = i.restangularized ? i.plain() : i;
      return _.omit(result, ['compositeId', 'repairs']);
    });

    _.extend(report.content, {
      wipActionableItems,
      wipAssetNotes,
      wipCocs,
      surveys: surveys.map((x) => x)
    });
    this.report = report;
    this.surveys = surveys;

    // Set this report's date (even if we're not eventually saving it);
    report.issueDate = date;
    this._issueDate = date;

    // If this is a daily report we need to filter it by date & user, but it
    // must also be non-submittable (ie generating a DAR) since FARs are
    // accumulations of all of the DAR reports;
    if (type === 'dar' && daily === 'true') {
      _.forOwn(report.content, (value, key) => {
        _.set(report.content, key, _.filter(value, 'actionTakenDate'));
        _.set(report.content, key, _.filter(value, 'creditedBy'));
      });
      await this.filterReport(report.content);
    }

    // This is placed after the filter because tasks dont have updatedBy or
    // actionTakenDate on them;
    report.content.tasks =
      await this.prepareTasks(jobId, angular.copy(report.content.surveys));

    // Adding harmonisationDate to report
    if (type === 'far') {
      report.harmonisationDate = this._harmonisationDateService
        .getHarmonisationDate(report.content.surveys);
    }

    // If we're not submitting then return the report object immediately;
    if (submit === 'false') {
      return angular.copy(report);
    }

    // Get all the versions of the reports of this type for this job;
    const versions = await this.getVersions(jobId, this._reportType.get(type));

    // Get the job so we can alter the status for dar/far;
    const job = await this._jobDecisionService.get(jobId);

    // Saving the report as a 'submitted' DAR;
    if (type === 'dar') {
      report = angular.copy(payload);

      // At this point we're saving a DAR, we need to check if there are already
      // DAR's existing on this job; if not, we need to set the job status to
      // 'under survey', and the firstVisitDate;
      if (_.isEmpty(versions) && _.isNull(job.firstVisitDate)) {
        job.firstVisitDate = date;
        job.jobStatus = { id: this._jobStatus.get('underSurvey') };
        await this._jobDecisionService.updateJob(job, true);
      }
    }

    // Saving the report as a 'submitted' FAR' - this sets the last visit date
    // on the job and sets the job status to 'under reporting';
    if (type === 'far') {
      job.lastVisitDate = date;
      job.jobStatus = { id: this._jobStatus.get('underReporting') };
      await this._jobDecisionService.updateJob(job, true);
    }

    // DSR needs to check:
    // -) named individual for 'authorising surveyor';
    if (type === 'dsr') {
      const authorising =
        _.find(job.employees,
          { employeeRole:
            { id: this._surveyorRole.get('authorisingSurveyor') }
          });
      if (_.isNil(authorising)) {
        job.jobStatus =
          { id: this._jobStatus.get('awaitingTechnicalReviewerAssignment') };
      } else {
        job.jobStatus =
          { id: this._jobStatus.get('underTechnicalReview') };
      }
      await this._jobDecisionService.updateJob(job, true);
    }

    // For FSR we need to check:
    // -) named individual as 'fleet services specialist' in job team
    if (type === 'fsr') {
      const specialist =
        _.find(job.employees,
          { employeeRole:
            { id: this._surveyorRole.get('eicManager') }
          });
      if (_.isNil(specialist)) {
        job.jobStatus =
          { id: this._jobStatus.get('awaitingEndorserAssignment') };
      } else {
        job.jobStatus =
          { id: this._jobStatus.get('underEndorsement') };
      }
      await this._jobDecisionService.updateJob(job, true);

      const reportUser = await this._employeeDecisionService.getCurrentUser();

      report.authorisedBy = reportUser.id;
      report.approved = true;
      report.authorisationDate = this._moment().toISOString();

      // Here, we also need to update the existing DSR report so we use the
      // dsrreport object to update it's information;
      dsrReport.authorisedBy = reportUser.id;
      dsrReport.approved = true;
      dsrReport.authorisationDate = this._moment().toISOString();

      this.update(jobId, dsrReport.id, dsrReport);
    }

    // This should be true anyway, but we'll check it just in case;
    if (!_.isNull(payload)) {
      _.set(report, 'notes', payload.notes);
      _.set(report, 'confidentialNotes', payload.confidentialNotes);
      _.set(report, 'recommendation', payload.recommendation);
      _.set(report, 'secondClassRecommendation',
        payload.secondClassRecommendation);

      // Remove 'code' from services;
      _.set(report.content, 'surveys',
        this.stripTasks(payload.content.surveys));
      _.set(report.content, 'tasks', this.stripModel(payload.content.model));
    }
    return this.create(jobId, report);
  }

  /**
   * @summary rejectReport(type)
   * @param {Number} jobId The id of the job
   * @param {Number} reportId The id of the report
   * @param {String} type The type we're rejecting
   * @param {String} reason The reason we're rejecting this report
   * @return {Undefined} zilch
   */
  async rejectReport(jobId, reportId, type, reason) {
    if (type === 'dsr') {
      const job = await this._jobDecisionService.get(jobId);
      const report = await this.get(jobId, reportId);

      // Since this report (DSR) is getting rejected we reset the status of
      // the job to 'under reporting';
      job.jobStatus =
        { id: this._jobStatus.get('underReporting') };
      await this._jobDecisionService.updateJob(job, true);

      // We need to set the reasoning for the rejection;
      const reportUser = await this._employeeDecisionService.getCurrentUser();
      report.authorisedBy = reportUser.id;
      report.approved = false;
      report.authorisationDate = this._moment().toISOString();
      report.rejectionComments = reason;

      // Then update this report (we're not creating a FSR but rather updating
      // the current DSR report);
      return this.update(jobId, report.id, report);
    }
  }

  /**
   * @summary createBasicReport(type)
   * @param {Number} jobId The id of the job
   * @param {String} type The type of report (dar, far, dsr, fsr)
   * @return {Object} report The basic report Object
   */
  createBasicReport(jobId, type) {
    return _.defaults({
      reportType: {
        id: this._reportType.get(type),
        name: type,
        description: type.toUpperCase()
      },
      job: {
        id: jobId
      },
      classRecommendation: null,
      secondClassRecommendation: null,
      content: {
        wipActionableItems: [ ],
        wipAssetNotes: [ ],
        wipCocs: [ ],
        surveys: [ ],
        tasks: [ ]
      }
    });
  }

  async checkDSRBeforeSubmission(jobId, report) {
    // Get all the defects for this job; ACs have changed and this section is
    // commented out temporarily - put back in in R3;

    // const allDefects =
    //   _.filter(await this._defectDecisionService.getDefectsForJob(jobId),
    //     { defectStatus: { id: this._defectStatus.get('open') } });
    // const nullDefectValues = [];
    //
    // _.forEach(allDefects, (defect) => {
    //   nullDefectValues
    //     .push(_.values(_.pick(defect, ['title', 'defectCategory'])));
    // });
    //
    // if (!_.isEmpty(allDefects) &&
    //   _.some(_.flatten(nullDefectValues), (val) => _.isNull(val))) {
    //   return 1;
    // }

    if (!_.isEmpty(report.content.surveys)) {
      const tasks =
        _.filter(await this.generateTasks(report.content.surveys),
          (survey) => {
            // attributes on the tasks;
            const attrs = survey.attributes;

            // find null values;
            const incomplete =
              _.reject(survey.attributes, 'attributeValueString');

            // Find followedOnFrom ids on incomplete tasks;
            const parents =
              _.map(incomplete, (v) =>
                _.find(attrs, { id: _.get(v, 'followedOnFrom.id') }));

            // Map the value against possible values, and look for a matching
            // value with isFollowOnTrigger; if true, this causes _.some() to
            // populate, which prevents DSR from being submitted;
            return _.some(_.map(parents, (parent) => {
              const allowedValue = _.find(
                parent.workItemConditionalAttribute.allowedAttributeValues, {
                  value: parent.attributeValueString
                });

              return allowedValue.isFollowOnTrigger;
            }));
          });

      if (!_.isEmpty(tasks)) {
        return 2;
      }
    }

    return 3;
  }

  /**
   * @summary performs a number of checks prior to submitting a report
   * @param {Number} jobId The id of the jobId
   * @param {Object} report The report
   * @return {Number} code Returns a code that the controller uses to
   *   determine which message to give back to the user, if any;
   */
  async checkBeforeSubmission(jobId, report) {
    const allDefects = await this._defectDecisionService
      .getDefectsForJob(jobId);

    const openDefects = _.filter(allDefects, {
      defectStatus: { id: this._defectStatus.get('open') }
    });

    const closedDefects = _.filter(allDefects, {
      defectStatus: { id: this._defectStatus.get('closed') }
    });

    const cocs = await this._codicilDecisionService.getCoC({ jobId });

    const openCocs = _.filter(cocs, {
      status: { id: this._cocStatus.get('open') }
    });

    // Checking for 'open defect without any associated open coc';
    if (!_.isEmpty(openDefects)) {
      // This checks defects that exist on open CoC's
      const cocDefects =
        _.map(openCocs, (coc) => _.get(coc, 'defect.id')).filter(Boolean);

      // filter the openDefects array with the ids we got in cocDefects;
      const outstanding =
        _.filter(openDefects, (defect) => !_.includes(cocDefects, defect.id));

      // Anything remaining is a problem...
      if (!_.isEmpty(outstanding)) {
        return 1;
      }
    }

    // Checking for 'defect is closed but associated coc is still open'
    if (!_.isEmpty(closedDefects)) {
      // The ids of the closed defects;
      const defectIds = _.map(closedDefects, (defect) => _.get(defect, 'id'));

      if (this.hasOpenCoCWithDefectId(openCocs, defectIds)) {
        return 2;
      }
    }

    // Checking if there are one or more open CoCs but no action have been taken
    // for those CoCs;
    if (!_.isEmpty(
      _.map(cocs, (coc) => _.isNull(coc.actionTakenDate)).filter(Boolean))) {
      return 3;
    }

    // Narratives are those uncredited services that have narratives on them;
    const badSurveyStatuses =
      _.values(_.pick(this._serviceCreditStatus.toObject(), ['notStarted']));
    const narratives =
      _.filter(report.content.surveys, (survey) =>
        !_.isNull(survey.narrative) &&
        _.includes(badSurveyStatuses, survey.surveyStatus.id));

    if (!_.isEmpty(narratives)) {
      return 4;
    }

    return 5;
  }

  hasOpenCoCWithDefectId(openCocs, defectIds) {
    const outstanding =
        _.filter(openCocs, (coc) =>
        _.includes(defectIds, _.get(coc, 'defect.id')));

    return !_.isEmpty(outstanding);
  }

  /**
   * @param {number} jobId The Id of the job
   * @return {Array} all reports for a given job
   */
  async getReportsForJob(jobId) {
    const items = await this._dataSource
      .one('job', jobId)
      .all('report')
      .getList();

    return await this.populateAttributes(items);
  }

  async getFARSubmissionState(jobId) {
    const versions = await this.getVersions(jobId, this._reportType.get('far'));

    return Boolean(_.get(versions, 'issueDate'));
  }

  /**
   * @summary getVersions(jobId, typeId)
   * @param {Number} jobId The id of the job
   * @param {Number} typeId The id of the type of report
   * @return {Array} reports A filtered array of reports, by type, all versions
   */
  async getVersions(jobId, typeId) {
    return this.populateAttributes(
      await this._dataSource.one('job', jobId).all('report').getList());
  }
}
