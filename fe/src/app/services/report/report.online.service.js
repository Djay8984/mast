import ReportService from './report.service';

export default class ReportOnlineService extends ReportService {
  /* @ngInject */
  constructor(
    $q,
    dateFormat,
    moment,
    node,
    Restangular,
    cocStatus,
    codicilDecisionService,
    creditingDecisionService,
    defectDecisionService,
    defectStatus,
    employeeDecisionService,
    harmonisationDateService,
    jobDecisionService,
    jobStatus,
    reportType,
    serviceCreditStatus,
    surveyDecisionService,
    surveyorRole,
    taskDecisionService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }
}
