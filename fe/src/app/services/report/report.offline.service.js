import ReportService from './report.service';

export default class ReportOfflineService extends ReportService {
  /* @ngInject */
  constructor(
    $q,
    dateFormat,
    moment,
    node,
    cocStatus,
    codicilDecisionService,
    creditingDecisionService,
    defectDecisionService,
    defectStatus,
    employeeDecisionService,
    harmonisationDateService,
    jobDecisionService,
    jobStatus,
    offlinular,
    reportType,
    serviceCreditStatus,
    surveyDecisionService,
    surveyorRole,
    taskDecisionService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
