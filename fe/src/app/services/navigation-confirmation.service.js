import Base from 'app/base.class';
import * as _ from 'lodash';

export default class NavigationConfirmation extends Base {
  /* @ngInject */
  constructor($rootScope, $q, MastModalFactory) {
    super(arguments);
  }

  /*
   * Displays a modal dialog containing content from dialogContent (html).
   * Buttons in html :
   *  <a class="button" ng-click="$close(true)">Yes</a>
   *    - allow navigation to continue
   *  <a class="button" ng-click="$close(false)">No</a> - cancel navigation
   *  <a class="button" ng-click="$close(someFunction)">No</a> - something else
   *  If the calling controller has a someFunction(), it will be called
   *    when user clicks third button and navigation will continue
   *
   * @param $state
   * @param dialogContent - html to insert into dialog template
   * @param controller - the controller using this function,
   *  if controller implements a needNavConfirmation(), it will be used
   *    to determine if the modal should be shown (return true|false).
   *    If that function does not exist we assume there is no need
   *    for confirmation dialog
   *  eg
   *  this.needNavConfirmation = (params) => {
   *    if (params.aField.$dirty) {
   *      return true;
   *    }
   *    return false;
   *  }
   *
   *  @returns {void}
   */
  setConfirmationCase($state, dialogContent, controller) {
    const launchNavigationConfirmation = (templateUrl, additionalClasses) => {
      const modal = new this._MastModalFactory({
        templateUrl,
        class: additionalClasses
      });
      return modal.activate();
    };

    const onRouteChangeOff =
      this._$rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
        let needNavConfirmation = () => false;
        if (_.isFunction(_.get(controller, 'needNavConfirmation'))) {
          needNavConfirmation = controller.needNavConfirmation.bind(controller);
        }
        if (!needNavConfirmation()) {
          return;
        }
        event.preventDefault();
        launchNavigationConfirmation(dialogContent, '')
          .then((response) => {
            if (!_.isUndefined(response) && !response) {
              return;
            }

            // allow callback to the controller that made use of the nav dialog
            if (_.isFunction(_.get(controller, response))) {
              controller[response]();
            }
            onRouteChangeOff();

            /*
             In theory we can get $state from controller._$state, however $state
             may not be included in controller
             (ESLint may complain about unused var)
             therefore force providing as param
             */
            $state.go(toState, toParams);
          });
      });
  }
}
