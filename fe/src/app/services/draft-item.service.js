import * as _ from 'lodash';

import Base from 'app/base.class';
import ExternalAttributeMixin from './external-attribute.class';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DraftItemService extends ExternalAttributeMixin(Base) {

  /* @ngInject */
  constructor(
    Restangular,
    referenceDataService
  ) {
    super(arguments);
    this._draftItem = {};
  }

  get draftObject() {
    return this._draftItem;
  }

  set draftObject(value) {
    this._draftItem = value;
  }


  // FIXME: clone item nodes correctly instead of relying on item.model.id
  async copyPreviewAssetModel(
      assetId,
      parentId,
      previewModel,
      attributeResponse
      ) {
    const appendItemRelationships = (obj, item) => {
      if (!_.isEmpty(item.items)) {
        obj.itemRelationshipDto = item.items.map((child) => {
          const data = {
            originalItemId: child.model.id
          };

          return appendItemRelationships(data, child);
        });
      }

      return obj;
    };

    const draftItemDtoList = previewModel.map((item) => {
      const data = {
        toParentId: parentId,
        fromId: item.model.id
      };

      return appendItemRelationships(data, item);
    });

    const payload = {
      copyAttributes: attributeResponse,
      draftItemDtoList
    };

    return this._Restangular
      .one('asset', assetId)
      .customPOST(payload, 'draft-item');
  }

  // ------------------------------------------------------
  //  get all list of relationship
  //  group them by assetCategoryId, fromItemType, toItemType
  async getItemTypeRelations() {
    const data =
      await this._referenceDataService.get('asset.itemTypeRelationships');
    const result = {};

    _.forEach(data, (val) =>
      _.setWith(result,
        [ val.assetCategoryId, val.fromItemType, val.toItemType ],
        val, Object));
    this.itemTypeRelationships = result;
    return result;
  }

  // ------------------------------------------------------
  getItemType(asset, parent, item) {
    return _.get(this.itemTypeRelationships,
      [ asset.assetCategory.id, parent.itemType.id, item.itemType.id ]) || {};
  }

  async getByOriginalId(asset, originalIds) {
    /* eslint max-nested-callbacks: 0 */
    return this.query(asset.id, { originalIds }).then((originalItems) => {
      const promises = _.map(originalItems, (item) =>
        item.items.length ? this.getDraftItems(asset, item)
          .then((items) => _.set(item, 'items', items)) : item);

      return Promise.all(promises);
    });
  }

  async copyAssetModel(
          assetId, draftItemIdToCopyTo, assetModelId, copyAttributes) {
    const parentItem = await this._Restangular
      .one('asset', assetId)
      .one('draft-item', draftItemIdToCopyTo)
      .get({ useOriginalId: false });

    const searchPayload = { itemTypeId: [parentItem.itemType.id] };

    const assetsToCopy = await this._Restangular
      .one('asset', assetModelId)
      .all('item')
      .customPOST(searchPayload, 'query');

    const parentPayload = {
      itemId: _.map(parentItem.items, 'id')
    };

    const parentItems = await this._Restangular.one('asset', assetId)
      .all('draft-item')
      .customPOST(parentPayload, 'query');

    const parentItemTypeIds = _.map(parentItems, 'itemType.id');

    const copyAssetsPayload = {
      itemId: _.map(assetsToCopy[0] && assetsToCopy[0].items, 'id')
    };

    const childCopyAssetTypes = await this._Restangular
      .one('asset', assetModelId)
      .all('item')
      .customPOST(copyAssetsPayload, 'query');

    const assetCopyList = [];

    // For each asset
    // get the itemType id
    _.forEach(childCopyAssetTypes, (asset) => {
      // fetch the assetType
      if (_.includes(parentItemTypeIds, asset.itemType.id)) {
        assetCopyList.push(asset);
      }
    });

    const draftItemDtoList = [];

    _.forEach(assetCopyList, (item) => {
      draftItemDtoList.push({
        toParentId: parseInt(draftItemIdToCopyTo, 10),
        fromId: item.id
      });
    });

    //  Save
    if (draftItemDtoList.length) {
      await this._Restangular
        .one('asset', assetId)
        .customPOST({ copyAttributes, draftItemDtoList }, 'draft-item');
    }
  }

  async duplicateAssetItem(assetId, parentId, draftItemId) {
    const originalItem = await this._Restangular
      .one('asset', assetId)
      .one('draft-item', draftItemId)
      .get({ useOriginalId: false });

    const payload = {
      copyAttributes: false,
      draftItemsDtoList: [
        {
          toParentId: parentId,
          fromId: originalItem.originalItem.id
        }
      ]
    };

    return this._Restangular
      .one('asset', assetId)
      .customPOST(payload, 'draft-item');
  }

  async addDuplicateAssetItem(asset, parent, draftItem) {
    const payload = {
      copyAttributes: false,
      draftItemDtoList: [
        {
          toParentId: parent.id,
          fromId: draftItem.originalItem.id
        }
      ]
    };

    return this._Restangular
      .one('asset', asset.id)
      .customPOST(payload, 'draft-item');
  }

  // This might work
  async copyReferenceData(assetId, parentId, referenceItems) {
    const payload = {
      copyAttributes: false,
      draftItemDtoList: this.buildReferenceItems(referenceItems, parentId)
    };

    return this._Restangular
      .one('asset', assetId)
      .customPOST(payload, 'draft-item');
  }

  buildReferenceItems(referenceItems, draftItemId) {
    return _.map(referenceItems, (referenceItem) => ({
      toParentId: draftItemId,
      itemTypeId: referenceItem.id
    }));
  }

  async validateDraftItem(assetId, draftItemId) {
    return this._Restangular.one('asset', assetId)
      .one('draft-item', draftItemId)
      .one('validate')
      .remove();
  }

  async deleteDraftItem(assetId, draftItemId) {
    return this._Restangular.one('asset', assetId)
      .one('draft-item', draftItemId)
      .remove();
  }

  async copyItems(assetItemId, copyAttributes, nodesToBuild) {
    let payload = {};
    if (_.size(nodesToBuild) === 1) {
      // only one assetModel to copy
      payload =
        this.buildSingleItemPayload(
            assetItemId,
            copyAttributes,
            nodesToBuild[0]);
    } else {
      payload = this.buildMultiItemPayload(
          assetItemId,
          copyAttributes,
          nodesToBuild);
    }

    return this._Restangular
      .one('asset', assetItemId)
      .customPOST(payload, 'draft-item');
  }

  async updateDisplayOrder(asset, draftItem, displayOrder) {
    return this._Restangular
      .one('asset', asset.id)
      .one('draft-item', draftItem.id)
      .customPUT({
        id: draftItem.id,
        displayOrder
      });
  }
}
