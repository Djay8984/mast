import Base from 'app/base.class';
import * as _ from 'lodash';

export default class BatchActionService extends Base {
  /* @ngInject */
  constructor(
    moment,
    Restangular,
    actionableItemStatus,
    assetNoteStatus,
    referenceDataService
  ) {
    super(arguments);
  }

  /**
   * @summary Get the codicil types from reference data
   * @return {Object} codicilTypes All of the codicil types
   * @since 27/05/2016
   */
  async codicilTypes() {
    if (!this._codicilTypes) {
      this._codicilTypes =
        await this._referenceDataService.get('service.codicilTypes');
    }

    return this._codicilTypes;
  }

  /**
   * @summary Construct the codicil status from the typeId
   * @param {Number} type The codicil type
   * @param {Object} codicil The dto passed in, so we can get the date
   * @return {Object} status The status object
   * @since 27/05/2016
   */
  async codicilStatus(type, codicil) {
    const map = {
      'asset-note': this._assetNoteStatus,
      'actionable-item': this._actionableItemStatus
    };
    let codicilStatusId = null;

    if (Date.now() > this._moment(codicil.imposedDate).valueOf()) {
      codicilStatusId =
        _.first(_.values(_.pick(map[type].toObject(), ['open'])));
    }
    if (_.isUndefined(codicil.imposedDate)) {
      codicilStatusId =
        _.first(_.values(_.pick(map[type].toObject(), ['draft'])));
    }
    codicilStatusId =
      _.first(_.values(_.pick(map[type].toObject(), ['inactive'])));
    return codicilStatusId;
  }

  /**
   * @description Saving the batch action codicils on an array of assets
   * @summary saveBatchActionCodicils
   * @param {Array} assetIds An array of ids of assets we are saving
   * @param {Object} assetNoteDto An array of objects
   * @param {String} actionType The type of codicil we're saving
   * @param {Number} itemTypeId A number of the item type id
   * @return {Object[]} An array of objects
   */
  async saveBatchActionCodicils(
    assetIds,
    assetNoteDto,
    actionType,
    itemTypeId = null
  ) {
    // Set the status of the codicil according to type and imposed date;
    const codicilStatusId = await this.codicilStatus(actionType, assetNoteDto);
    assetNoteDto.status = { 'id': codicilStatusId };

    assetNoteDto =
      _.omit(assetNoteDto, ['codicilCategory', 'editableBySurveyor',
        'templateStatus', 'version', 'codicilType', 'itemType', 'templateId',
        'linkBroken', 'actionType', 'count', 'id', '_id', 'attachments']);

    /* eslint prefer-template: 0 */
    const options =
      {
        assetIds,
        itemTypeId,
        [_.camelCase(actionType) + 'Dto']: assetNoteDto
      };

    const retval = this._Restangular
      .one('batch-action', actionType)
      .customPOST(options);
    return retval;
  }
}
