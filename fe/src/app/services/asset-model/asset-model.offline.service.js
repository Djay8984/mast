import * as _ from 'lodash';

import AssetModelService from './asset-model.service';

export default class AssetModelOfflineService extends AssetModelService {
  /* @ngInject */
  constructor(
    $q,
    assetDecisionService,
    pageService,
    offlinular,
    referenceDataService
  ) {
    super(arguments);
  }


  /**
   * @param {number} assetId what asset to look for an item on
   * @param {object} query a bunch of item query parameters
   * @param {boolean} populate whether or not to hydrate
   * @return {array} found items
   * noticeably absent is the type name stuff because that requires big,
   *   expensive reference data parsing
   */
  async queryItem(
    assetId,
    {
      itemId = [],
      itemTypeId = [],
      itemName = null,
      attributeId = [],
      attributeTypeId = [],
      attributeName = null
    } = {},
    { page, size, sort = 'displayOrder', order = 'ASC' } = {},
    populate = true
  ) {
    // functions matching against query params, truthiness indicates valid item
    const checks = {
      itemId: (item) => _.includes(itemId, item.id),
      itemTypeId: (item) => _.includes(itemTypeId, _.get(item, 'itemType.id')),
      itemName: (item) => _.includes(item.name, itemName),
      attributeId: (item) => _.includes(item.attributes,
        (attribute) => _.find(attributeId, attribute.id)),
      attributeTypeId: (item) => _.find(item.attributes,
        (attribute) => _.includes(attributeTypeId,
          _.get(attribute, 'attributeType.id'))),
      attributeName: (item) => _.find(item.attributes,
        (attribute) => _.includes(attributeName, attribute.name))
    };

    if (!_.every([ itemId, itemTypeId, itemName,
      attributeId, attributeTypeId, attributeName ], _.isEmpty)) {
      const assetModel = await this._dataSource
        .one('asset-model', assetId)
        .get();

      // recursive searching in asset models
      let found = [];
      const find = (item) => {
        if (_.find(checks, (check) => check(item))) {
          found.push(item);
        }
        _.forEach(item.items, find);
      };

      find(assetModel);

      found = _.sortBy(found, sort);
      if (order !== 'ASC') {
        found = _.reverse(found);
      }

      return populate ? this.populateAttributes(found) : found;
    }
  }

  get _dataSource() {
    return this._offlinular;
  }
}
