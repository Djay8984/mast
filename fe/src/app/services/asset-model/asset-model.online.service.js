import AssetModelService from './asset-model.service';

export default class AssetModelOnlineService extends AssetModelService {
  /* @ngInject */
  constructor(
    $q,
    Restangular,
    assetDecisionService,
    pageService,
    referenceDataService
  ) {
    super(arguments);
  }

  async validateItemDeletable(assetId, ItemId) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', ItemId)
      .one('validate')
      .customDELETE();
  }

  /**
   * @param {number} assetId id of an asset
   * @param {object} query series of attributes/values to match
   * @param {boolean} populate defaulted true, whether to expand
   * @return {array} list of matching items
   */
  async queryItem(
    assetId,
    query = {},
    { page, size, sort = 'displayOrder', order, includeDraft = false } = {},
    populate = true
  ) {
    const result = await this._dataSource
      .one('asset', assetId)
      .all('item')
      .customPOST(query, 'query', { page, size, sort, order, includeDraft });
    return populate ? this.populateAttributes(result) : result;
  }

  get _dataSource() {
    return this._Restangular;
  }
}
