import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import assetModelOfflineService from './asset-model.offline.service';
import assetModelOnlineService from './asset-model.online.service';

export default angular
  .module('app.services.assetModel', [])
  .service('assetModelOfflineService', assetModelOfflineService)
  .service('assetModelOnlineService', assetModelOnlineService)
  .service('assetModelDecisionService', [
    'assetModelOnlineService',
    'assetModelOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
