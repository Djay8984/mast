import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 , babel/no-await-in-loop: 0*/
export default class AssetModelService
  extends ExternalAttributeMixin(BaseService) {

  async get(assetId) {
    return this._dataSource
      .one('asset-model', assetId)
      .get();
  }

  async getAvailableAttributes(assetId, itemId) {
    const item = await this.getItem(assetId, itemId, false);
    const attributeTypes = await this._referenceDataService
      .get(['asset.attributeTypes']);

    return _.filter(_.get(attributeTypes, 'asset.attributeTypes'),
        { itemType: { id: _.get(item, 'itemType.id') } });
  }

  async getAvailableItems(asset, item) {
    const itemTypeId = _.get(item, 'itemType.id');
    const assetCategoryId = _.get(asset, 'assetCategory.id');

    // find available list
    const template = await this.getTemplate();
    let rule = _.get(template, [itemTypeId, 'validItems']);
    rule = _.filter(rule, { assetCategoryId });

    //  map names against available list
    const allItems = await this._referenceDataService.get('asset.itemTypes');
    const itemsById = _.keyBy(allItems, 'id');
    const items = _.map(rule, (val) => _.get(itemsById, val.toItemType));

    return { rule, items };
  }

  async getDraftItem(assetId, itemId, populate = true, parents = false,
    children = false) {
    let result = await this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .get();

    result = await (populate ?
       this.populateAttributes(result) :
       result);

    if (parents) {
      result.parents = [result];
      let item = result;

      while (_.has(item, 'parentItem.id')) {
        item = await this._dataSource
          .one('asset', assetId)
          .one('item', item.parentItem.id)
          .get();

        result.parents.push(item);
      }
    }

    if (children) {
      result.items = await this._$q.all(
        _.map(result.items, (child) => this._dataSource
          .one('asset', assetId)
          .one('item', child.id)
          .get()));
    }

    return result;
  }

  /**
   * @param {number} assetId id of an asset
   * @param {number} itemId id of an item
   * @param {object} params population & url parameters
   * @return {promise} resolving to an item
   */
  async getItem(assetId, itemId, { populate = true, ...params } = {}) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .get(this._fixParams(assetId, params))
      .then(populate ? this.populateAttributes.bind(this) : _.identity);
  }

  /**
   * @param {number} assetId id of an asset
   * @param {array} itemIds list of item ids
   * @param {object} params population & query parameters
   * @return {promise} resolving to a list of items
   */
  async getItems(assetId, itemIds, { populate = true, ...params } = {}) {
    return _.isEmpty(itemIds) ?
      [] :
      this
        .queryItem(assetId, { itemId: itemIds },
          this._fixParams(assetId, params))
        .then(populate ? this.populateAttributes.bind(this) : _.identity);
  }

  async getRootItem(assetId, { populate = true, ...params } = {}) {
    return this._dataSource
      .one('asset', assetId)
      .one('root-item')
      .get(this._fixParams(assetId, params))
      .then(populate ? this.populateAttributes.bind(this) : _.identity);
  }

  async markItemAsComplete(assetId, itemId, params) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .customPUT({}, 'review-complete', this._fixParams(assetId, params));
  }

  async decommissionItem(assetId, itemId, params) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .customPUT({}, 'decommission', this._fixParams(assetId, params));
  }

  async recommissionItem(assetId, itemId, params) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .customPUT({}, 'recommission', this._fixParams(assetId, params));
  }

  async deleteItem(assetId, itemId, params) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .customDELETE(null, this._fixParams(assetId, params));
  }

  @pageable
  async search(
    filters,
    id,
    { page = 0, size = 20, sort = 'name', order = 'asc' } = {}
  ) {
    return this._dataSource
      .one('asset', id)
      .customPOST(filters, 'item/query',
          { page, size, sort, order }, {});
  }

  /**
   * @return {object} asset model template
   */
  async getTemplate() {
    return _.get(
      await this._referenceDataService.get(['asset.assetModelTemplate']),
      'asset.assetModelTemplate.assetModelTemplate');
  }

  /**
   * @param {number} assetId asset id to check whether it's checked out
   * @param {object} params normal url parameters plus otional include draft
   * @return {object} fixed parameters
   * fixes the include draft option if it isn't overridden
   */
  _fixParams(assetId, { includeDraft, ...params } = {}) {
    return _.omitBy({
      includeDraft: includeDraft ||
        this._pageService.isCheckedOut({ id: assetId }) || null,
      ...params
    }, _.isNil);
  }
}
