import AssetService from './asset.service';

import { pageable } from 'app/decorators';

export default class AssetOfflineService extends AssetService {
  /* @ngInject */
  constructor(
    $q,
    caseService,
    employeeDecisionService,
    jobDecisionService,
    offlinular,
    referenceDataService,
    assetLifecycleStatus
  ) {
    super(arguments);
  }

  async get(id) {
    return this.populateAttributes(await this._dataSource
      .one('asset', id)
      .get());
  }

  @pageable
  async getCases(assetId) {
    // Temporary fix until cases are maybe included in the job bundle
    return [];
  }

  get _dataSource() {
    return this._offlinular;
  }
}
