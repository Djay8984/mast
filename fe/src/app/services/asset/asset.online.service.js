import * as _ from 'lodash';

import AssetService from './asset.service';

import { pageable } from 'app/decorators';

export default class AssetOnlineService extends AssetService {
  /* @ngInject */
  constructor(
    $q,
    Restangular,
    caseService,
    employeeDecisionService,
    jobDecisionService,
    referenceDataService,
    assetLifecycleStatus
  ) {
    super(arguments);
  }

  checkedOut({ all = false } = {}) {
    return this._dataSource
      .one('checked-out')
      .get({ all });
  }

  async checkOut(asset) {
    return this._dataSource
      .one('asset', asset.id)
      .one('check-out')
      .customPOST();
  }

  checkIn(asset) {
    return this._dataSource
      .one('asset', asset.id)
      .one('check-in')
      .customPOST();
  }

  discardAsset(asset) {
    // TODO
    return true;
  }

  async createAsset(asset) {
    return this._dataSource
      .all('asset')
      .customPOST(asset);
  }

  async get(data) {
    if (_.isNumber(data)) {
      data = { assetId: data };
    }
    let query = {};
    if (data.checkedOut || data.draft) {
      query = { draft: true };
      data = { assetId: data.id || data.assetId };
    }
    if (data.jobId) {
      query = { jobId: data.jobId };
    }
    if (data.versionId) {
      query = { version: data.versionId };
    }

    return this.populateAttributes(await this._dataSource
      .one('asset', data.assetId)
      .get(query));
  }

  @pageable
  getAllAssets(
    query,
    { size = null, page = 0, sort = 'id', order = 'desc' } = {}
  ) {
    return this._dataSource
      .all('asset')
      .customPOST(query, 'query', { size, page, sort, order });
  }

  @pageable
  async getCases(assetId) {
    const response = await this._dataSource
      .one('asset', assetId)
      .one('case')
      .get();

    return this.populateAttributes(response);
  }

  @pageable
  async getCustomers(assetId) {
    const customers = await this._dataSource
      .one('asset', assetId)
      .all('customer')
      .getList();

    return this.populateAttributes(customers);
  }

  async getProducts(assetId) {
    return this._dataSource
      .one('asset', assetId)
      .all('product')
      .getList();
  }

  get _dataSource() {
    return this._Restangular;
  }
}
