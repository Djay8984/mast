import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable, sortable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class AssetService extends ExternalAttributeMixin(BaseService) {

  async addItemsToAssets(assetList, service, type) {
    if (_.isEmpty(assetList)) {
      return assetList;
    }

    // TODO: almost all of this is shared with myWorkService...
    const assetIds = assetList.map((asset) => Number(asset.id));
    const items = await service.forAssets(assetIds,
      { size: null, sort: 'createdOn' });
    const itemsByAssetId = items.reduce((out, item) => {
      out[item.asset.id] = (out[item.asset.id] || []).concat(item);

      return out;
    }, {});

    _.forEach(assetList, (asset) => {
      asset[type] = itemsByAssetId[asset.id];
    });

    return assetList;
  }

  async getProducts(assetId) {
    return this._dataSource
      .one('asset', assetId)
      .one('product')
      .get();
  }

  @pageable
  async getServicesForAsset(assetId) {
    return this._dataSource
      .one('asset', assetId)
      .all('service')
      .getList();
  }

  async create(asset) {
    return this._dataSource
      .all('asset')
      .customPOST(asset);
  }

  /**
   * update(asset) Update an asset in the db
   * @param {Object} asset The asset to be updated
   * @return {Object} response The return object after customPUT
   */
  async update(asset) {
    asset = await this.processModel(asset);

    /* See addItemsToAssets() in asset.service.js
     *
     * As of 2016-05-31 the server rejects an asset payload with
     * the following properties that addItemsToAssets() has added
     * after an asset has been retrieved from the server.
     *
     * @todo
     * We're cleaning up the asset before sending it back to the
     * server but are they still needed on the asset to begin with?
     */
    asset = _.omit(asset, [ 'cases', 'jobs' ]);

    return this._dataSource
      .one('asset', asset.id)
      .customPUT(asset);
  }

  @pageable
  @sortable('name', 'buildDate', 'ihsAsset.id,id')
  async getAllCurrent(
    { page = 0, size = 12, sort = 'id', order = 'desc' } = {}
  ) {
    const ids = await this.getCurrentLifecycleStatusIds();

    return this.query(
      { 'lifecycleStatusId': ids }, { page, size, sort, order }
    );
  }

  @pageable
  @sortable('name', 'buildDate', 'ihsAsset.id,id')
  async getAllCurrentByCategoryId(
    categoryId,
    { page = 0, size = 12, sort = 'id', order = 'desc' } = {}) {
    const ids = await this.getCurrentLifecycleStatusIds();
    return this.query(
      {
        categoryId: _.isArray(categoryId) ? categoryId : [categoryId],
        lifecycleStatusId: ids
      },
      { page, size, sort, order }
    );
  }

  @pageable
  @sortable('name', 'buildDate', 'ihsAsset.id,id')
  async getAllById(
    itemId,
    { page = 0, size = 12, sort = 'id', order = 'desc' } = {}) {
    return this.query(
      { idList: _.isArray(itemId) ? itemId : [itemId] },
      { page, size, sort, order }
    );
  }

  async getCurrentLifecycleStatusIds() {
    // NB: decommissioned is spelt incorrectly (one 's') in the reference data
    return _.values(_.omit(this._assetLifecycleStatus.toObject(),
      ['decommissioned', 'cancelled']));
  }

  async getItem(assetId, itemId) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', itemId)
      .get();
  }

  async itemTypeTypeahead(queryString) {
    return this.itemTypeQuery(`${queryString}`);
  }

  async itemTypeQuery(search) {
    if (!this._itemTypes) {
      this._itemTypes = _.get(
        await this._referenceDataService.get(['asset.itemTypes'], true),
        'asset.itemTypes');
    }

    return _.filter(this._itemTypes, (item) =>
      _.includes(item.name.toLowerCase(), search));
  }

  async processModel(model) {
    if (!_.isNull(model.offices)) {
      const roles = _.get(
        await this._referenceDataService.get(['case.officeRoles']),
        'case.officeRoles');

      model.offices =
        _.filter(model.offices, (office) => {
          office.officeRole =
            _.first(_.filter(roles, { id: office.officeRole.id }));
          return !_.isUndefined(office.office);
        });
    }

    return model;
  }

  @pageable
  @sortable('name', 'buildDate', 'ihsAsset.id,id')
  async query(
    query,
    { page = 0, size = 12, sort = 'id', order = 'desc' } = {}
  ) {
    let assets = await this._dataSource.all('asset')
          .customPOST(query, 'query', { page, size, sort, order });

    assets = await this.populateAttributes(assets);

    this._$q.all([
      this.addItemsToAssets(assets, this._caseService, 'cases'),
      this.addItemsToAssets(assets, this._jobDecisionService, 'jobs')
    ]);

    return assets;
  }
}
