import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import assetOfflineService from './asset.offline.service';
import assetOnlineService from './asset.online.service';

export default angular
  .module('app.services.asset', [])
  .service('assetOfflineService', assetOfflineService)
  .service('assetOnlineService', assetOnlineService)
  .service('assetDecisionService', [
    'assetOnlineService',
    'assetOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
