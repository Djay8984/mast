import Base from 'app/base.class';

import * as _ from 'lodash';

const ABSTRACT_FUNCTIONS = [

  // none for now as service migration is too hard with these
];

// Really an interface
export default class BaseService extends Base {
  constructor() {
    super(...arguments);
    _.forEach(ABSTRACT_FUNCTIONS, (func) => {
      if (!_.isFunction(this[func])) {
        throw new TypeError(
        `Must implement function '${func}' in class ${this.constructor.name}`);
      }
    });
  }
}
