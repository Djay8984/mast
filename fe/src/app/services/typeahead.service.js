import * as _ from 'lodash';

import Base from 'app/base.class';

export default class TypeaheadService extends Base {

  /* @ngInject */
  constructor(typeaheadResultsLength) {
    super(arguments);
  }

  /**
   * @param {array} data the stuff to build a filter method for
   * @param {function} preFilter optional data prefilter, takes result's
   *   additional params
   * @param {object} options field, sort & length options
   * @return {function} method which can be called to filter data with
   *   consistent rules
   * @description the prefilter is executed before the search query every time
   *   it is used, & is passed all parameters from that call. it defaults to do
   *   nothing, but can be used to filter the data with an additional step
   */
  query(
    data,
    preFilter = null,
    {
      field = 'name',
      sort = 'name',
      length = this._typeaheadResultsLength
    } = {}
  ) {
    const self = this;

    if (_.isNil(preFilter)) {
      preFilter = () => _.identity;
    }

    return (query, ...preFilterParams) => _.chain(data)
      .filter(preFilter(...preFilterParams))
      .filter(self._evaluateRegex(field, self._queryToRegex(query)))
      .sortBy(sort)
      .slice(0, length)
      .value();
  }

  /**
   * @param {string} field name of the field in data to test query against
   * @param {regex} regex test to compare the field in the data against
   * @return {function} test of the regex against the field in the datum
   */
  _evaluateRegex(field, regex) {
    return (datum) => regex.test(_.get(datum, field));
  }

  /**
   * @param {string} query search string to escape
   * @return {regex} query as regex
   */
  _queryToRegex(query) {
    const escapedQuery = _.replace(query, /\*/g, '');
    return new RegExp(`^${escapedQuery}`, 'i');
  }
}
