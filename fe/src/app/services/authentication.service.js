const AuthInterceptor = (
  $httpParamSerializer,
  appConfig,
  KJUR,
  RSAKey
) => {
  const service = {
    request
  };

  return service;

  /* eslint max-statements: 0 */
  function request(config) {
    const privateKey = new RSAKey();
    privateKey.readPrivateKeyFromPEMString(appConfig.MAST_SIGN_PEM_KEY);

    if (config.url.endsWith('.html')) {
      return config;
    }

    let url = config.url;
    const pos = url.indexOf(appConfig.MAST_SIGN_API_PREFIX);

    if (pos >= 0) {
      url = url.substring(pos);
    }

    let data = `${config.method}:${url}`;
    const paramString = $httpParamSerializer(config.params);

    if (paramString > 0) {
      data = `${data}?${paramString}`;
    }

    if (config.data) {
      data += `[${config.transformRequest[0](config.data)}]`;
    }

    const sig = new KJUR.crypto.Signature({
      alg: appConfig.MAST_SIGN_ALGORITHM,
      prov: 'cryptojs/jsrsa'
    });

    sig.initSign(privateKey);

    // calculate signature
    const sigValueHex = `${sig.signString(data)}=`;

    config.headers.IDSClientSign = sigValueHex;
    config.headers.IDSClientVersion = appConfig.MAST_SIGN_CLIENT_VERSION;
    config.headers.IDSClientIdentity = appConfig.MAST_CLIENT_IDENTITY;

    return config;
  }
};

AuthInterceptor.$inject = [
  '$httpParamSerializer',
  'appConfig',
  'KJUR',
  'RSAKey'
];

export default AuthInterceptor;
