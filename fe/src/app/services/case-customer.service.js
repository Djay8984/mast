import * as _ from 'lodash';
import Base from 'app/base.class';

const UNIQUE_FUNCTIONS = [ 'CFO', 'Contract holder' ];

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CaseCustomerService extends Base {
  /* @ngInject */
  constructor(
    Restangular,
    referenceDataService
  ) {
    super(arguments);

    this._caseCustomers = [];
  }

  /**
   * addCustomer(customer)
   * @param {Object} customer The customer object to add
   */
  addCustomer(customer) {
    this._caseCustomers.push(customer);
  }

  /**
   * setCustomers(customers) needed for the editing of a case
   * @param {Array} customers An array of customers
   */
  setCustomers(customers) {
    this._caseCustomers = customers;
  }

  /**
   * @summary Removes the customer from the customer list
   * @param {Object} customer The customer object to remove
   */
  removeCustomer(customer) {
    this._caseCustomers = _.pull(this._caseCustomers, customer);
  }

  /**
   * updateCustomer(customer)
   * @param {Object} customer The customer object to focus on/replace
   */
  updateCustomer(customer) {
    let existing = _.find(this._caseCustomers, { 'id': customer.id });

    if (!_.isUndefined(existing)) {
      existing = customer;
    }
  }

  /**
   * customerList()
   * @return {Array} customers Returns the array of customers
   */
  customerList() {
    return this._caseCustomers;
  }

  /**
   * functionTypes()
   * @param {Object} customer The customer that we're adding the functions to
   * @return {Array} functionTypes An array of possible function types
   * @since 01/06/2016
   */
  async functionTypes(customer) {
    let functionTypes =
      _.get(await this._referenceDataService.get(['party.partyFunctions']),
        'party.partyFunctions');

    // .filter is there to prevent undefined items on array;
    const selectedFunctions =
      _.flattenDeep(_.reduce(this._caseCustomers, (result, item) => {
        result.push(item.functions);
        return result;
      }, [])).filter(Boolean);

    // Find the uniq functions (CFO/contract Holder);
    const uniqSelections =
      _.filter(selectedFunctions, (item) =>
        _.includes(UNIQUE_FUNCTIONS, item.customerFunction.name));

    // Finally only show the available function types;
    if (_.size(uniqSelections)) {
      _.forEach(uniqSelections, (uniq) => {
        functionTypes =
          _.reject(functionTypes, { 'name': uniq.customerFunction.name });
      });
    }

    return functionTypes;
  }

  /**
   * customerFunctionList()
   * @param {Object} customer The customer we want the functions from
   * @return {Array} customers.fucntions Returns the array of functions
   */
  customerFunctionList(customer) {
    return customer.functions;
  }

  /**
   * addCustomerFunction()
   * @param {Object} customer The customer we're dealing with
   * @param {Object} item The item (function) we're adding
   * @since 02/06/2016
   */
  addCustomerFunction(customer, item) {
    const focus = _.find(this._caseCustomers, customer);

    focus.functions = focus.functions || [];

    focus.functions = _.forEach(item, (single) => {
      const newItem = _.omit(single, ['deleted']);
      return _.unionBy(focus.functions, newItem, 'customerFunction.id');
    });
  }

  async staticCustomerList() {
    if (!this._staticCustomerList) {
      const refData = await this._referenceDataService.get(['party']);
      this._staticCustomerList = _.get(refData, 'party.party');
    }
    return this._staticCustomerList;
  }

  /**
   * Helper function to get the Id from the name
   * @param {String} name The customer name
   * @return {Number} id The id of the customer
   */
  async getCustomerIdFromName(name) {
    const customers = await this.staticCustomerList();
    return _.find(customers, { name }).id;
  }

  /**
   * addCustomerName(customer, name)
   * @param {Object} customer The customer we're dealing with
   * @param {String} name The customer's name to set
   */
  async addCustomerName(customer, name) {
    const focus = _.find(this._caseCustomers, customer);
    const id = await this.getCustomerIdFromName(name);
    focus.customer = { name, id };
  }

  /**
   * removeCustomerFunction(customer, item)
   * @param {Object} customer The customer we're removing a function from
   * @param {Object} item The item (function) we're removing
   */
  removeCustomerFunction(customer, item) {
    customer.functions = _.reject(customer.functions,
        { customerFunction: { id: item.customerFunction.id } });
  }
}
