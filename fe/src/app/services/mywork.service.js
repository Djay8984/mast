import * as _ from 'lodash';

import Base from 'app/base.class';

import { modelable, pageable, sortable } from 'app/decorators';
import { Asset } from 'app/models';

export default class MyWorkService extends Base {
  /* @ngInject */
  constructor(
    node,
    caseService,
    jobOnlineService,
    assetOnlineService,
    $stateParams
  ) {
    super(arguments);
  }

  @modelable(Asset)
  @pageable
  @sortable('name', 'buildDate')
  async query(
    currentUser,
    query,
    { page = 0, size = 3, sort = 'id', order = 'desc' } = {}
  ) {
    if (_.isNil(currentUser.id)) {
      return [];
    }

    const cases =
      await this._caseService.forEmployee(currentUser.id, { size: null });
    const jobs =
      await this._jobOnlineService.forEmployee(currentUser.id, { size: null });

    if (_.isEmpty(cases) && _.isEmpty(jobs)) {
      return [];
    }

    const casesByAssetId = cases.reduce((out, item) =>
      _.set(out, item.asset.id, (out[item.asset.id] || []).concat(item)), {});

    const jobsByAssetId = jobs.reduce((out, item) =>
      _.set(out, item.asset.id, (out[item.asset.id] || []).concat(item)), {});

    const assets = await this._assetOnlineService.query(
      {
        ...query,
        idList: [..._.keys(casesByAssetId), ..._.keys(jobsByAssetId)]
      },
      { page, size, sort, order });

    _.forEach(assets, (asset) => {
      asset.cases = casesByAssetId[asset.id];
      asset.jobs = jobsByAssetId[asset.id];
    });

    return assets;
  }

  @pageable
  @sortable('name', 'buildDate')
  async getForUser(
    user,
    { page = 0, size = 3, sort = 'id', order = 'desc' } = {}
  ) {
    return this.query(user, null, { page, size, sort, order });
  }
}
