import * as _ from 'lodash';
import QDatastore from 'lib/nedbq';

import Base from 'app/base.class';

/*
  notes:
    * do not remove $injector, it's used for hydration in the
      externalattributemixin in a really hacky way
 */
export default class ReferenceData extends Base {
  /* @ngInject */
  constructor(
    $injector,
    $log,
    Restangular
  ) {
    super(arguments);

    this._db = new QDatastore({
      filename: './reference-data.db',
      autoload: true
    });
  }

  /**
   * @param {boolean} online whether or to try the backend
   * @param {boolean} force forces an update from online
   * @return {promise} loading the reference data into this service
   */
  async prepareDatabase(online = true, force = false) {
    if (online) {
      const dbVersion = await this._getDBVersion();
      const apiVersion = await this._getAPIVersion();
      if (apiVersion > dbVersion || force) {
        await this._updateDB(apiVersion, await this._getAPIData());
      }
    }
    return this.build();
  }

  /**
   * builds the reference data, attaching it to this service & allowing it to
   *   be accessed in a synchronous way, filters deleted on the fly
   * @return {promise} builder which will resolve to true when complete
   */
  async build() {
    if (!this._building) {
      const self = this;
      const builder = async () => {
        const data = await self._getDBData();
        self.view = {};
        self.input = {};

        // creates getters for unfiltered view categories
        _.forEach(data, (categoryValue, categoryKey) =>
          Reflect.defineProperty(self.view, categoryKey, {
            get: () => categoryValue
          }));

        // creates getters for filtered input categories
        _.forEach(data, (categoryValue, categoryKey) =>
          Reflect.defineProperty(self.input, categoryKey, {
            get: () => _.mapValues(categoryValue,
              (tableValue) => _.reject(tableValue, 'deleted'))
          }));

        return true;
      };

      this._building = builder();
    }
    return this._building;
  }

  /**
   * @param {array|string} categories list of categories or single cateogyr
   * @param {boolean} input whether or not to filter out deleted entities
   * @returns {object} map of each of the categories, or single category
   * retrieves one or many data tables from the reference data, allows for
   *   whole groups of data & singular tables
   */
  async get(categories, input = false) {
    await this.build();
    let param = null;

    if (_.isString(categories)) {
      param = categories;
      categories = [categories];
    }

    // creating a map of the found categories
    const map = _.reduce(categories, (total, category) => {
      const target = _.get(this, [
        input ? 'input' : 'view',
        ..._.split(category, '.')]);

      if (_.isNil(target)) {
        this._$log.error('[RDS]', `${category} not found in reference data`);
        return total;
      }
      return _.set(total, category, target);
    }, {});

    return _.get(map, param, map);
  }

  async _updateDB(version, data) {
    await this._db.remove({ }, { multi: true });
    await this._db.insert([{ key: 'data', data }, { key: 'version', version }]);
    this._$log.debug('[RDS]', `reference data version updated to ${version}`);
  }

  async _getAPIData() {
    return _.get(
      await this._Restangular.one('reference-data').get(),
      'referenceData');
  }

  async _getAPIVersion() {
    return _.get(
      await this._Restangular.one('reference-data-version').get(),
      'version');
  }

  async _getDBData() {
    return _.get(
      await this._db.findOne({ key: 'data' }),
      'data');
  }

  async _getDBVersion() {
    return _.get(
      await this._db.findOne({ key: 'version' }),
      'version',
      0);
  }
}
