import * as _ from 'lodash';

import Base from 'app/base.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class MilestoneService extends ExternalAttributeMixin(Base) {

  /* @ngInject */
  constructor(
    $q,
    moment,
    Restangular,
    attachmentDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  _core(caseId) {
    return this._Restangular
      .one('case', caseId)
      .one('milestone');
  }

  async getMilestoneStatuses() {
    if (!this._milestoneStatuses) {
      this._milestoneStatuses =
        await this._referenceDataService.get('case.milestoneStatuses');
    }

    return this._milestoneStatuses;
  }

  purgeMilestoneList(milestones, markComplete = false) {
    return _.map(milestones, (milestone) => {
      if (milestone.reviewed && markComplete) {
        milestone.completionDate = this._moment().format();
        milestone.milestoneStatus.id =
          _.get(_.first(_.filter(this._milestoneStatuses,
            { name: 'Completed' })), 'id');
      }

      // remove temporary properties
      Reflect.deleteProperty(milestone, 'attachments');
      Reflect.deleteProperty(milestone, 'reviewed');
      return milestone;
    });
  }

  async getMilestones(caseId) {
    const milestones =
      await this.populateAttributes(await this._core(caseId).get());
    return milestones;
  }

  async markAsComplete(caseId, milestones) {
    await this.getMilestoneStatuses();
    const caseMilestone = {
      caseMilestoneList: this.purgeMilestoneList(milestones, true)
    };

    if (caseMilestone) {
      return this._core(caseId).customPUT(caseMilestone);
    }
  }

  async saveScope(caseId, milestones) {
    const caseMilestone = {
      caseMilestoneList: this.purgeMilestoneList(_.reject(milestones,
        'completionDate'))
    };

    if (caseMilestone) {
      return this._core(caseId).customPUT(caseMilestone);
    }
  }

  async saveDueDate(caseId, milestone, dueDate) {
    milestone.dueDate = dueDate;
    const caseMilestone = {
      caseMilestoneList: this.purgeMilestoneList([milestone])
    };

    if (caseMilestone) {
      return this._core(caseId).customPUT(caseMilestone);
    }
  }
}
