import * as angular from 'angular';

import offlineDataService from './offline-data.service';
import offlinular from './offlinular.service';

export default angular
  .module('app.services.offline-data', [
  ])
  .service('offlineDataService', offlineDataService)
  .service('offlinular', offlinular)
  .name;
