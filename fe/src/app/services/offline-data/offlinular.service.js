import * as _ from 'lodash';

import * as angular from 'angular';

import Base from 'app/base.class';

/*
  restangular-esque interface for the offline data service

  notes:
    * copies maintain the bundle's id, which isn't very stateless but makes life
      much much easier

  how this all works:
    generates copies of itself expanding on the _path every time, caching its
    own values & providing the same methods all over again for pretty chaining
 */
export default class Offlinular extends Base {
  /* @ngInject */
  constructor(
    $q,
    Restangular,
    offlineDataService,
    path = [],
    bundleId
  ) {
    super(arguments);
    this._path = path;
    this._bundleId = bundleId;
  }

  /**
   * @param {number} bundleId id of bundle to build call for
   * @return {promise} restangular request chain for a bundle request
   */
  _bundleCore(bundleId) {
    return this._Restangular
      .one('job-bundle', bundleId);
  }

  /**
   * @param {object|string|number} thing path part
   * @return {array} path
   * parses the path part from a restangular-esque call into an array of
   *   objects denoting a path
   */
  _appendPathPart(thing = null) {
    let path = angular.copy(this._path);
    let newPart = null;
    if (_.isObject(thing)) {
      if (_.parseInt(thing.url)) {
        newPart = { id: thing.url };
      } else {
        newPart = { url: thing.url };
      }

      if (_.parseInt(thing.id)) {
        newPart.id = _.toString(thing.id);
      } else if (_.isEmpty(thing.url)) {
        return path;
      }
    } else if (_.parseInt(thing)) {
      newPart = { id: _.toString(thing) };
    } else if (_.isString(thing)) {
      newPart = { url: thing };
    } else if (_.isNull(thing)) {
      return path;
    }

    const lastPart = _.last(path);
    if (_.has(newPart, 'id') && !_.has(newPart, 'url') &&
      !_.has(lastPart, 'id') && _.has(lastPart, 'url')) {
      path.pop();
      path.push({ url: lastPart.url, id: _.toString(newPart.id) });
    } else {
      path = _.concat(path, newPart);
    }
    return _.reject(path, (part) => _.isNil(part) || _.every(part, _.isNil));
  }


  // ///////////////////////////////////////////////////////////////////////////
  // public - data modification, restangular mocks


  /*
   * @param {number} bundleId id of bundle to start working with
   * @return {this} offlinular service for chaining
   */
  bundle(bundleId) {
    this._bundleId = bundleId;
    return new Offlinular(
      ..._.pickBy(this, (value, key) => _.startsWith(key, '_')));
  }

  /**
   * @param {string|number} url path part
   * @param {string|number} id path part
   * @return {this} offlinular service for chaining
   * interprets the parameters flexibly based on types
   */
  one(url, id = null) {
    if (_.isEmpty(id) && _.includes(url, '/')) {
      [url, id] = _.split(url, '/');
    }
    return new Offlinular(
      this._$q,
      this._Restangular,
      this._offlineDataService,
      this._appendPathPart({ url, id }),
      this._bundleId);
  }

  /**
   * @param {string|number} url path part
   * @param {string|number} id path part
   * @return {this} offlinular service for chaining
   * interprets the parameters flexibly based on types
   */
  all(...args) {
    return this.one(...args);
  }

  /**
   * @param {string|number} path path part
   * @param {object} options request parameters
   * @return {promise} requested datum/data
   */
  async get(path, options) {
    return this._offlineDataService
      .request(this._bundleId, this._appendPathPart(path), options, null,
        'GET');
  }

  /**
   * @param {string|number} path path part
   * @param {object} options request parameters
   * @return {promise} requested datum/data
   */
  async getList(...args) {
    return this.get(...args);
  }

  /**
   * @param {string|number} path path part
   * @param {object} options request parameters
   * @return {promise} removed element
   */
  async customDELETE(path, options) {
    return this._offlineDataService
      .request(this._bundleId, this._appendPathPart(path), options,
        null, 'DELETE');
  }

  /**
   * @param {object} payload modified data
   * @param {string|number} path path part
   * @param {object} options request parameters
   * @return {promise} modified element
   */
  async customPUT(payload, path, options) {
    return this._offlineDataService
      .request(this._bundleId, this._appendPathPart(path), options, payload,
        'PUT');
  }

  /**
   * @param {object} payload new data
   * @param {string|number} path path part
   * @param {object} options request parameters
   * @return {promise} created element
   */
  async customPOST(payload, path, options) {
    return this._offlineDataService
      .request(this._bundleId, this._appendPathPart(path), options, payload,
        'POST');
  }


  // ///////////////////////////////////////////////////////////////////////////
  // public - bundle management


  /**
   * @param {number} bundleId id of bundle to save online
   * @return {promise} saving request reponse
   */
  async saveOnline(bundleId) {
    return this._bundleCore(bundleId).customPUT(
      await this._offlineDataService.composeBundle(bundleId));
  }

  /**
   * @param {number} bundleId id of bundle to save offline
   */
  async saveOffline(bundleId) {
    if (!(await this.checkAvailableOffline(bundleId))) {
      await this._offlineDataService.addBundle(
        (await this._bundleCore(bundleId).get()).plain());
    }
  }

  /**
   * @param {number} bundleId id of bundle to check if saved
   * @return {boolean} whether or not the job is saved offline
   */
  async checkAvailableOffline(bundleId) {
    return _.includes(await this.getAvailableOffline(), bundleId);
  }

  /**
   * @param {boolean} expand whether or not to give full bundles
   * @return {array} bundle ids or fully expanded bundles
   */
  async getAvailableOffline(expand = false) {
    const bundleIds = await this._offlineDataService.getAvailableBundles();
    return expand ?
      this._$q.all(_.map(bundleIds, (bundleId) =>
          this._offlineDataService.composeBundle(bundleId))) :
      bundleIds;
  }

  /**
   * @param {number} bundleId id of bundle to remove from offline
   */
  async clearOffline(bundleId) {
    await this._offlineDataService.removeBundle(bundleId);
  }

  /**
   * @return {number} new id
   * shim to expose for the offline data service
   */
  async generateNewId() {
    return this._offlineDataService.generateNewId();
  }
}
