import * as _ from 'lodash';
import QDatastore from 'lib/nedbq';

import * as angular from 'angular';

import Base from 'app/base.class';

import referenceDataKeyMap from 'app/services/reference-data.keymap';

/*
  notes:
    * cannot fill out objects, needs people to post the full object every time
    * bundle ids are assumed to be job ids
    * everything hinges off the string _id rather than the id, this is because
      we need to reserve null ids for new things & new things use negative _ids
    * designed to work with json object apis, will freak out on array apis
    * provides more apis than actually exist, thanks to its dynamism
    * provides simply a crud interface - any side effects need to be handled
      in offline versions of services

  how this all works:
    bundles are injested & taken to pieces, each of their parts is dumped as a
    record in the database, & can be identified using their _ids & their type
    (key in the bundle)

    extracting a bundle requires finding it in the bundle table & compiling real
    lists of its link resources. components are updated individually & can be
    shared across bundles

    extracting data from within components involves tearing apart the path into
    various url/id components which are then manipulated to match the names in
    the bundle api. the last of these components in a path which maps onto a
    table is then used to extract a table & then a search is done within that
    table with the remainder of the path
*/

const DB_FILENAME = './offline-data.db';

// set of regexes for matching on paths for exceptional circumstances
const EXCEPTIONAL_PATHS = {

  // queries are handled differently
  QUERY: new RegExp(/query/i),

  // attachments are handled differently too
  ATTACHMENT: new RegExp(/attachment/i),

  // requests to asset item or root-item actually query the asset model
  ASSET_MODEL: new RegExp(/^(item|root-item)$/i),

  // requests to root item should only return one result
  SINGULAR: new RegExp(/^root-item$/i),

  // valid request urls which are not found any of the data's keys
  UNKEYED: new RegExp(/^(query|root-item)$/i)
};

const ORDER_ASCENDING_REGEX = new RegExp(/asc/i);
const FILTER_REGEX = new RegExp(/^(\w+)(List|Id)$/);

const PLAIN_OMISSIONS = ['plain', 'route', 'restangularized'];

// allowed keys in the bundle/db & wether they're singular or not
const BUNDLE_MAP = {
  actionableItem: false,
  asset: true,
  assetModel: true,
  assetNote: false,
  attachment: false,
  coC: false,
  defect: false,
  job: true,
  repair: false,
  report: false,
  service: false,
  survey: false,
  task: false,
  wipActionableItem: false,
  wipAssetNote: false,
  wipCoC: false,
  wipDefect: false,
  wipRepair: false,
  wipTask: false
};

const PROPERTY_MAP = {
  actionableItem: 'codicil',
  assetNote: 'codicil',
  coC: 'codicil',
  defect: 'defect',
  wipActionableItem: 'codicil',
  wipAssetNote: 'codicil',
  wipCoC: 'codicil',
  wipDefect: 'defect'
};

export default class OfflineDataStore extends Base {
  /* @ngInject */
  constructor(
    $log,
    $q,
    moment
  ) {
    super(arguments);
    this._db = new QDatastore({
      filename: DB_FILENAME,
      autoload: true
    });
  }

  /**
   * @param {boolean} force whether or not to refresh the database
   * creates the database meta if it doesn't exist or is forced
   */
  async prepareDatabase(force = false) {
    if (force || _.isEmpty(await this._getEntity('meta'))) {
      // metadata tables keep track of current id & what bundles are stored
      await this._insertEntities(_.map(['bundleId', 'currentId'],
        (id) => ({ type: 'meta', id, data: [] })));
    }
  }


  // ///////////////////////////////////////////////////////////////////////////
  // public - bundle management


  /**
   * @return {array} list of bundle ids currently stored in the database
   */
  async getAvailableBundles() {
    return this._getEntity('meta', 'bundleId');
  }

  /**
   * @param {object} bundle bundle from the job-bundle api
   * adds a bundle, tearing it apart & adding its own components to the various
   *   tables in the database
   */
  async addBundle(bundle) {
    const bundleId = _.parseInt(bundle._id);

    // converts all bundle entities into a flat list
    const entities = _.flatMap(
      _.omit(bundle, ['id', '_id']), (table, type) =>
        _.get(BUNDLE_MAP, type) ?
          [{ type, id: table._id, data: table }] :
          _.map(table, (datum) =>
            ({ type, id: datum._id, data: datum })));

    // adds bundle into the db
    await this._insertEntities(entities);

    await this._updateEntity('meta', 'bundleId',
      _.concat(await this._getEntity('meta', 'bundle'), bundleId));
  }

  /**
   * @param {number} bundleId id of the bundle to create
   * @return {object} bundle ready for the job-bundle api
   * pulls together all of the parts of a bundle, & creates a clean object ready
   *   for the job-bundle api
   */
  async composeBundle(bundleId) {
    let bundle = await this._$q.all(
      _.mapValues(BUNDLE_MAP, (singular, type) => this._getEntity(type)));

    // singularising relevant entities
    bundle = _.mapValues(bundle, (data, type) =>
      _.get(BUNDLE_MAP, type) ? _.first(data) : data);

    return {
      ...bundle,
      id: bundleId,
      _id: _.toString(bundleId)
    };
  }

  /**
   * @param {number} bundleId id of the bundle to remove
   * removes the bundle from the database, removing all of its components from
   *   the various tables. if the id is unspecified, it removes all bundles
   */
  async removeBundle(bundleId = null) {
    if (!_.isNull(bundleId)) {
      await this._updateEntity('meta', 'bundleId',
        _.pull(await this._getEntity('meta', 'bundleId'), bundleId));
    }
    await this._$q.all(
      _.map(BUNDLE_MAP, (singular, type) => this._deleteEntity(type)));
  }


  // ///////////////////////////////////////////////////////////////////////////
  // public - data interaction


  /**
   * @param {number} bundleId id of the bundle to work on
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters, such as pagination options
   * @param {object} payload what's being sent
   * @param {string} method http request type
   * @return {object|array} requested data, paginated or otherwise
   * @throws {error} occurs if the request is unsuccessful
   * the entry point for the crudiness of the offline data service, acts as a
   *   wrapper which handles request method redirection, errors & logging. tries
   *   to use uniform methods for as many request types as possible but also
   *   handles some edge cases where requests need modifying
   */
  async request(bundleId, path, options = {}, payload = null, method) {
    try {
      let result = null;

      payload = _.omit(payload, PLAIN_OMISSIONS);

      // TODO: make dynamic
      // it's too much to ask to do fancy filtering, so just get everything
      if (EXCEPTIONAL_PATHS.QUERY.test(_.findLast(path, 'url').url)) {
        options = { ...options, ...payload };
        method = 'GET';
      } else if (EXCEPTIONAL_PATHS.ATTACHMENT.test(
        _.findLast(path, 'url').url)) {
        path = [ { url: 'attachment' } ];
      }

      switch (method) {
        case 'DELETE':
          result = await this._delete(bundleId, path, options);
          break;
        case 'GET':
          result = await this._get(bundleId, path, options);
          break;
        case 'POST':
          result = await this._post(bundleId, path, options, payload);
          break;
        case 'PUT':
          result = await this._put(bundleId, path, options, payload);
          break;
        default:
          result = null;
      }

      this._log(path, options, payload, method, 200, angular.copy(result));
      return result;
    } catch (error) {
      this._log(path, options, payload, method, error.status, null, error);

      if (!error.status) {
        throw error;
      } else {
        return {
          data: null,
          status: error.status,
          config: { method, url: this._composePath(path, options) },
          payload
        };
      }
    }
  }

  /**
   * @return {number} new id
   * uses the current id table to generate a new id
   */
  async generateNewId() {
    const assignedIds = await this._getEntity('meta', 'currentId');
    const newId = (_.last(assignedIds) || 0) - 1;
    await this._updateEntity('meta', 'currentId', _.concat(assignedIds, newId));
    return newId;
  }


  // ///////////////////////////////////////////////////////////////////////////
  // private - actual requests


  /**
   * @param {number} bundleId id of the bundle to act upon
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @return {object} deleted object
   * updates the deleted property of the datum in the database, meaning that
   *   it will not be returned in get requests. it will still be present in the
   *   composed bundle for deleting on putting though
   */
  async _delete(bundleId, path, options) {
    const data = (await this._get(bundleId, path, options)).plain();
    const tablePath = this._getTablePath(path);

    // removing any attachments linked to this deleted item
    const id = _.findLast(path, 'id').id;
    const entityKey = this._urlToEntityKey(_.findLast(path, 'url').url);
    const cleanedAttachments = _.reject(await this._getEntity('attachment'),
      (item) => _.get(item, 'entityLink.link._id') === id &&
        this._urlToEntityKey(_.get(item, 'entityLink.type')) === entityKey);

    await this._$q.all(cleanedAttachments, (attachment) =>
      this._removeEntity('attachment', attachment._id));

    await this._updateEntity(
      this._urlToBundleKey(tablePath.url),
      data._id, { ...data, deleted: true });

    return this._offlinise(data, bundleId, path, options, null, 'DELETE');
  }

  /**
   * @param {number} bundleId id of the bundle to act upon
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters, such as pagination
   * @return {object|array} requested data
   * extracts data from the database, first looking at the bundle attributes &
   *   then going deeper & looking inside them as the path determines. will
   *   paginate responses if requesting a list and offlinise
   */
  async _get(bundleId, path, options) {
    const tableData = await this._getTable(
      this._getTablePath(path), path, bundleId);

    // get data path and extract initial data
    const dataPath = this._getDataPath(tableData, path, bundleId);
    let data = _.get(tableData, dataPath, tableData);

    // create a filter path that includes all but the paths that can't be
    // converted into ob properties
    let filterPath = _.reduce(_.cloneDeep(path), (result, part) => {
      if (!EXCEPTIONAL_PATHS.QUERY.test(part.url)) {
        result.push(part);
      }
      return result;
    }, []);

    // filter data if the path had more than 1 level under the bundle
    // e.g. job/{x}/coc/{y}/repair
    if (_.isEmpty(dataPath) && _.size(filterPath) > 2) {
      // convert path urls into property names to multi-path filtering then
      // remove undefined urls
      _.forEach(filterPath, (part) => _.set(part, 'url',
        PROPERTY_MAP[this._urlToObjectKey(PROPERTY_MAP, part.url)]));
      filterPath = _.find(filterPath, 'url');

      const filter = { [filterPath.url]: { _id: _.toString(filterPath.id) } };

      // if we have a final id, should filter differently
      data = _.isArray(data) ?
        _.filter(data, filter) :
        _.find([data], filter);

      if (_.isUndefined(data)) {
        throw this._freakOut('unable to find requested data in bundle', 404);
      }
    }

    return this._offlinise(_.isArray(data) ?
        this._paginate(this._filter(data, options), options) : data,
      bundleId, path, options, null, 'GET');
  }

  /**
   * @param {number} bundleId id of the bundle to act upon
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @param {object} payload new data to add
   * @return {object|array} newly created entity
   * adds a new datum to the relevant table in the database, associated with
   *   the bundle in question & assigns a new _id & null id
   */
  async _post(bundleId, path, options, payload) {
    payload = await this._applyMeta(payload, null, bundleId);

    const tablePath = this._getTablePath(path);
    const tableData = await this._getTable(tablePath, path, bundleId);
    const dataPath = this._getDataPath(tableData, path);
    const data = _.isEmpty(dataPath) ?
      payload : _.set(tableData, dataPath, payload);

    await this._insertEntity(
      this._urlToBundleKey(tablePath.url), data._id, this._dehydrate(data));

    return this._offlinise(data, bundleId, path, options, payload, 'POST');
  }

  /**
   * @param {number} bundleId id of the bundle to act upon
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @param {object} payload updated data to add
   * @return {object|array} newly updated entity
   * updates a datum in the relevant table, associated with the bundle passed
   */
  async _put(bundleId, path, options, payload) {
    // we'll lose reference to this if we don't preserve the _id
    payload = await this._applyMeta(payload, payload._id, bundleId);
    const tablePath = this._getTablePath(path);
    const tableData = await this._getTable(tablePath, path, bundleId);
    const dataPath = this._getDataPath(tableData, path);
    const data = _.isEmpty(dataPath) ?
      payload : _.set(tableData, dataPath, payload);

    await this._updateEntity(
      this._urlToBundleKey(tablePath.url), data._id, this._dehydrate(data));

    return this._offlinise(data, bundleId, path, options, payload, 'PUT');
  }


  // ///////////////////////////////////////////////////////////////////////////
  // private - restangular mocks


  /**
   * @param {array} data things to filter
   * @param {object} options filter options, such as query attributes
   * @return {array} subset of data which satisfy filter options
   * attempts to pull apart the query parameters object & extract a subset of
   *   the data based on those. assumes that query apis are consistent & use
   *   descriptive attribute names (*list) & attempts to do some fuzzy mapping
   *   from filter names to data keys
   */
  _filter(data = [], {
    search = null,
    searchString = null,
    idList = null,
    ...filters
  } = {}) {
    if (!_.isEmpty(data)) {
      // pulls apart filter keys if they match the id regex & filters content
      // & tries to map them against keys in the data
      if (!_.isEmpty(filters)) {
        const datum = _.first(data);

        // filters keys by ones which match the regex, then maps to the group &
        // gets rid of any empty ones & attempts a slightly fuzzy key remap to
        // properly link to datum attributes, then does a final filter
        const parsedFilters = _.chain(filters)
          .pickBy((value, key) => FILTER_REGEX.test(key))
          .mapKeys((value, key) => key.match(FILTER_REGEX)[1])
          .omitBy(_.isEmpty)
          .mapKeys((filterValue, filterKey) =>
            _.findKey(datum, (datumValue, datumKey) =>
              new RegExp(filterKey, 'i').test(datumKey)) || filterKey)
          .pickBy((value, key) => _.has(datum, key))
          .value();

        data = _.filter(data, (item) =>
          _.every(parsedFilters, (value, key) =>
            _.includes(_.map(value, _.toString),
              _.toString(
                _.get(item, [key, 'id'], _.get(item, [key, '_id'], ''))))));
      }

      if (!_.isNull(idList)) {
        idList = _.map(idList, _.toString);
        data = _.filter(data, (item) => _.includes(idList, item._id));
      }

      if (!_.isNull(search) || !_.isNull(searchString)) {
        data = _.filter(data,
          (item) => _.includes(item, search || searchString));
      }
    }

    return data;
  }

  /**
   * @param {array} data things to paginate
   * @param {object} options pagination & sorting options
   * @return {array} subset of data based on page & size, sorted & ordered
   * uses standard pagination options to extract data based on offsets & size
   *   limits, ordered & sorted. decorates the array with a pagination object
   *   to remember options by
   */
  _paginate(data = [], {
    page = 1,
    size = _.size(data),
    sort = '',
    order = 'asc'
  } = {}) {
    const fullDataSize = _.size(data);
    page--;

    data = _.sortBy(data, sort);

    if (!ORDER_ASCENDING_REGEX.test(order)) {
      data = _.reverse(data);
    }

    return _.set(_.slice(data, page * size, (page + 1) * size), 'pagination', {
      first: page === 0,
      last: (page + 1) * size >= fullDataSize,
      number: size < fullDataSize ? size : fullDataSize,
      numberOfElements: fullDataSize,
      size,
      sort,
      order,
      totalElements: fullDataSize,
      totalPages: _.ceil(fullDataSize / size) || 0
    });
  }

  /**
   * @param {object|array} data thing(s) to decorate
   * @param {number} bundleId id of bundle to act upon
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @param {object} payload data to augment
   * @param {string} method http request type
   * @return {object|array} things passed decorated with restangularesque stuff
   * synonymous to restangularise, this adds a bunch of stuff so that we can
   *   treat results from this service as if they were returned from restangular
   */
  _offlinise(data, bundleId, path, options, payload, method) {
    const addLinkResourceIDs = (obj) => {
      _.forEach(obj, (prop) => {
        if (_.get(prop, 'id') && _.size(Reflect.ownKeys(prop)) === 1) {
          _.set(prop, '_id', _.toString(_.get(prop, 'id')));
        }
      });
    };

    if (_.isArray(data)) {
      // as this array also has named properties, we have to do some funky stuff
      _.forEach(data, (item, index) => {
        _.set(data, index, {
          ...item,
          plain: () => _.omit(item, PLAIN_OMISSIONS),
          route: _.findLast(path, 'url').url,
          restangularized: true
        });
        addLinkResourceIDs(item);
      });
    }
    data.plain = () => _.omit(data, PLAIN_OMISSIONS);
    data.route = _.findLast(path, 'url').url;
    data.restangularized = true;
    addLinkResourceIDs(data);
    return data;
  }

  /**
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @param {object} payload new data to add
   * @param {string} method http request type
   * @param {number} status http response code
   * @param {object|array} data response of request
   * @param {error} error what went wrong (if something did)
   * @return {object} logging
   * mocks restangular's error logging for successful requests too, so
   *   offline data interaction isn't completely silent
   */
  _log(path, options, payload, method, status, data, error = null) {
    const url = this._composePath(path, options);
    return _.inRange(status, 100, 400) && _.isNull(error) ?
      this._$log.debug('[ODS]', { method, status, url, payload, data }) :
      this._$log.error('[ODS]', { method, status, url, payload, data, error });
  }


  // ///////////////////////////////////////////////////////////////////////////
  // private - data manipulation

  /**
   * @param {string} type whatever type of thing the entity is
   * @param {number|string} id unique identifier entity is stored under
   * @param {anything} data corresponding value
   * @return {promise} nedb insert reponse
   */
  async _insertEntity(type, id, data) {
    return this._db.insert({ type, id, data });
  }

  /**
   * @param {array} entities list of entities (will be cleaned up)
   * @return {promise} nedb insert reponse
   */
  async _insertEntities(entities) {
    return this._db.insert(entities);
  }

  /**
   * @param {string} type whatever type of thing the entity is
   * @param {number|string} id unique identifier entity is stored under
   * @param {anything} data corresponding value
   * @return {promise} nedb update reponse
   */
  async _updateEntity(type, id, data) {
    return this._db.update({ type, id }, { type, id, data });
  }

  /**
   * @param {string} type whatever type of thing the entity is
   * @param {number|string} id unique identifier entity is stored under
   * @return {promise} nedb remove reponse
   * will remove entire type if id is not provided
   */
  async _deleteEntity(type, id = null) {
    return _.isNull(id) ?
      this._db.remove({ type }, { multi: true }) :
      this._db.remove({ type, id });
  }

  /**
   * @param {string} type whatever type of thing the entity is
   * @param {number|string} id unique identifier entity is stored under
   * @return {promise} nedb find reponse, single or multiple dependent on id
   */
  async _getEntity(type, id = null) {
    return _.isNull(id) ?
      _.map(await this._db.find({ type }), 'data') :
      _.get(await this._db.findOne({ type, id }), 'data');
  }

  /**
   * @param {object} pathComponent part of the path with a table name
   * @param {array} path list denoting the components of the path
   * @param {number} bundleId id of bundle to act upon
   * @return {object|array} thing(s) associated with the path component passed
   * gets data relevant to a bundle from inside a table, extracting a single
   *   entity if an id is provided
   */
  async _getTable({ url, id = null }, path, bundleId) {
    const data = await this._getEntity(this._urlToBundleKey(url), id);

    if (!_.isArray(data) && _.get(data, 'deleted')) {
      throw this._freakOut('data deleted', 404);
    }

    return _.isArray(data) ?
      _.reject(data, { deleted: true }) :
      data;
  }

  /**
   * @param {array} path list denoting the components of the path
   * @returns {array} content of the table found
   * @throws {error} occurs when the table cannot be found
   * looks through the path to find the last component of which matches a
   *   database table, handles some edge cases
   */
  _getTablePath(path) {
    const table = angular.copy(_.findLast(path, (part) =>
      _.includes(_.keys(BUNDLE_MAP), this._urlToBundleKey(part.url))));

    if (_.isUndefined(table)) {
      throw this._freakOut('unable to find requested data in bundle', 404);
    }

    // if the url contains any reference to an item, assume we use the asset
    // model table
    if (!_.isUndefined(_.find(path,
        (part) => EXCEPTIONAL_PATHS.ASSET_MODEL.test(part.url)))) {
      table.url = 'assetModel';
    }

    return table;
  }

  /**
   * @param {array} tableData data from a database table
   * @param {array} path list denoting the components of the path
   * @param {number} bundleId id of bundle to act upon
   * @return {array} list of attribute names/indices to the found data
   * @throws {error} occurs when the requested data cannot be found
   * attempts to generate a list for usage with _.get. makes use of the path,
   *   looking recursively for data using path component urls & related ids,
   *   mapping them onto attributes in the relevant table's data
   */
  _getDataPath(tableData, path, bundleId) {
    // extract from the path anything which isn't a table name - going to have
    //   to look deeper for that data
    const nonTablePath = _.filter(path,
      (part) => _.isUndefined(this._urlToBundleKey(part.url)));

    let innerPath = [];
    let found = false;

    // if we're searching at this level, have an idea & can find it or have no
    // id, consider things found
    if (_.isEmpty(nonTablePath)) {
      const id = _.last(path).id;
      if (id && _.find(this._arrayise(tableData), { _id: id }) || !id) {
        found = true;
      }
    }

    // converts ids into indices, has to do some weird modification of urls to
    //   fit into the bundle's internal data
    _.forEach(nonTablePath, (part, index) => {
      const key = this._urlToObjectKey(
        _.get(tableData, innerPath, tableData),
        part.url);

      if (!EXCEPTIONAL_PATHS.UNKEYED.test(part.url) && _.isUndefined(key)) {
        throw new this._freakOut(`cannot find key ${part.url}`);
      }

      innerPath.push(key);

      if (_.isNaN(_.parseInt(part.id))) {
        found = true;
      } else {
        // only want to search with with id, unless we're at the first level
        // of looking
        const search = { _id: _.toString(part.id) };
        const foundIndex = _.findIndex(
          _.get(tableData, innerPath),
          search);

        if (foundIndex >= 0) {
          innerPath.push(foundIndex);
          found = true;
        } else {
          // recursively looks for the item, assumes a tree structure like:
          // { key: [ { key: [] } }
          // where key is the transmogrified url
          const recurse = (currentPath) => {
            const deepFoundIndex = _.findIndex(_.get(tableData, currentPath),
              { _id: _.toString(part.id) });

            if (deepFoundIndex >= 0) {
              innerPath = _.concat(currentPath, deepFoundIndex);
              found = true;
            } else {
              // if unavailable at this level, look at child levels
              _.find(_.get(tableData, currentPath), (item, deepIndex) => {
                const currentKey = this._urlToObjectKey(
                  _.get(tableData, _.concat(currentPath, deepIndex)),
                  part.url);

                if (_.isUndefined(currentKey)) {
                  found = false;
                }

                return recurse(_.concat(currentPath, deepIndex, currentKey));
              });
            }
            return found;
          };

          recurse(innerPath);
        }
      }
    });

    if (EXCEPTIONAL_PATHS.SINGULAR.test(
      _.get(_.findLast(path, 'url'), 'url', ''))) {
      innerPath.push(0);
    }

    if (!found) {
      throw this._freakOut('unable to find requested data in bundle', 404);
    }

    return _.reject(innerPath, _.isUndefined);
  }

  /**
   * @param {object} payload what's being sent
   * @param {number} newId new id to assign
   * @param {number} bundleId id of bundle to act upon
   * @return {object} payload with applied metadata
   * grabs a new id & _id & applies it if one isn't specified
   */
  async _applyMeta(payload, newId, bundleId) {
    if (_.isNull(newId)) {
      newId = await this.generateNewId();
      payload.id = null;
      payload._id = _.toString(newId);
    } else if (_.isNumber(newId)) {
      payload.id = null;
      payload._id = _.toString(newId);
    }

    return payload;
  }

  /**
   * @param {object} data thing to dehydrate
   * @param {object} extraDehydrateables extra keys to allow dehydration on
   * @returns {object} cleaned datum
   * recursively goes through the object, removing the isHydrated flag if it
   *   finds it & removing any other attributes other than the id & _id if it
   *   is in fact hydrated (looks up in the reference data keymap), also tidies
   *   up any moment objects by parsing their results out to iso strings &
   *   strips any $ prefixed keys
   */
  _dehydrate(data, extraDehydrateables = {}) {
    // extra, manually hydrated entities
    const dehydrateables = {
      ...referenceDataKeyMap,
      item: null,
      ...extraDehydrateables
    };

    /*
     * @param {object|array|string|number} item value to inspect & recurse
     * @param {string} key the name of the current object we're looking in
     * @return {object|array|string|number} dehydrated value
     * recursively looks inside the value passed, preserving type & stripping
     *   away things as noted in the parent method
     */
    const recurse = (item, key = null) => {
      if (_.has(item, 'isHydrated') && _.has(dehydrateables, key)) {
        return _.pick(item, ['id', '_id']);
      } else if (_.isArray(item)) {
        return _.map(item, (newItem) => recurse(newItem));
      } else if (this._moment.isMoment(item)) {
        return item.toISOString();
      } else if (_.isObject(item)) {
        return _.mapValues(
          _.omitBy(item, (childItem, childKey) => _.startsWith(childKey, '$')),
          (childItem, childKey) => recurse(childItem, childKey));
      }
      return item;
    };

    // needs to map differently for arrays to preserve type
    return _.isArray(data) ?
      _.map(data, (item) => recurse(item)) :
      _.mapValues(data, (item, key) => recurse(item, key));
  }

  /**
   * @param {string} message what the error should say
   * @param {number} status http response code
   * @returns {error} new error with attached status
   */
  _freakOut(message, status) {
    return _.set(new Error(message), 'status', status);
  }


  // ///////////////////////////////////////////////////////////////////////////
  // private - helper functions


  /**
   * @param {object|array} data thing to convert into an array
   * @returns {array} data an as an array
   * useful for lodash functions
   */
  _arrayise(data) {
    return _.isArray(data) ? data : [data];
  }

  /**
   * @param {string} url text to convert into an attachment entity key
   * @returns {string} url as matching the bundle entity key
   */
  _urlToEntityKey(url) {
    return _.toUpper(_.replace(this._urlToBundleKey(url), /([A-Z])/g, '_$1'));
  }

  /**
   * @param {string} url attribute name to look for in the bundle map
   * @returns {string} found key in the bundle map
   * @see _urlToObjectKey
   * looks for the passed string as a name in the bundle (table) map in a
   *   slightly fuzzy fashion
   */
  _urlToBundleKey(url) {
    return this._urlToObjectKey(
      _.mapKeys(_.concat(_.keys(BUNDLE_MAP), 'currentId')), url);
  }

  /**
   * @param {object} object thing to look for keys in
   * @param {string} string attribute name to look for in the passed object
   * @returns {string} found key in the object
   * looks for the string as an attribute name in the object in a slightly fuzzy
   *   fashion, ignoring casing, pluralisation & root prefixes
   */
  _urlToObjectKey(object, string) {
    // lets ignore any root prefix
    string = _.camelCase(_.replace(string, 'root', ''));
    const objectKeyRegex = new RegExp(`^${string}[s]{0,1}$`, 'i');
    return _.find(_.keys(object), (key) => objectKeyRegex.test(key));
  }

  /**
   * @param {array} path list denoting the components of the path
   * @param {object} options url parameters
   * @returns {string} path as if it were an url
   */
  _composePath(path = [], options = {}) {
    return _.replace(_.join([
      _.join(_.map(path, (part) => _.join(_.values(part), '/')), '/'),
      _.join(_.map(options,
          (value, key) => _.join([key, encodeURI(value)], '=')), '&')
    ], '?'), '//', '/');
  }
}
