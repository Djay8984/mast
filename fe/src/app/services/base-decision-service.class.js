import * as _ from 'lodash';

import BaseService from './base-service.class';

export default (onlineService, offlineService, $rootScope) => {
  // Only way I could find to get a list of functions from
  // the object's __proto__, including any inherited functions
  const allFunctions = (proto, functions) =>
    proto instanceof BaseService ?
      allFunctions(Reflect.getPrototypeOf(proto),
        _.union(Reflect.ownKeys(proto), functions)) :
      functions;

  // get a list of all common functions between the services
  const functions = _.filter(_.intersection(
    allFunctions(Reflect.getPrototypeOf(onlineService), []),
    allFunctions(Reflect.getPrototypeOf(offlineService), [])
  ), (func) => func !== 'constructor'); // we don't want the constructor

  // wrap the functions in a new function which decides if the online or offline
  // service should be called, then calls it
  const wrappedFunctions = _.map(functions, (func) =>
    function newFunc() {
      return $rootScope.OFFLINE ? offlineService[func](...arguments) :
        onlineService[func](...arguments);
    }
  );

  // return the wrapped functions with names matching the functions they wrap
  // e.g. { getOne: () => `call service.getOne` }
  return _.zipObject(functions, wrappedFunctions);
};
