import * as _ from 'lodash';

import Base from 'app/base.class';
import ExternalAttributeMixin from './external-attribute.class';

import { pageable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CaseService extends ExternalAttributeMixin(Base) {
  /* @ngInject */
  constructor(
    $log,
    moment,
    employeeDecisionService,
    Restangular,
    referenceDataService,
    caseStatus
  ) {
    super(arguments);

    this.OPEN_STATUS_IDS = _.values(_.omit(caseStatus.toObject(),
        ['cancelled', 'closed']));
  }

  async updateCase(payload) {
    return this._Restangular
      .one('case', payload.id)
      .customPUT(payload);
  }

  async getCase(caseId) {
    return await this.populateAttributes(
      await this._Restangular.one('case', caseId).get());
  }

  async openStatusIds() {
    const caseStatuses =
      await this._referenceDataService.get('case.caseStatuses');

    return caseStatuses
      .filter((status) => this.OPEN_STATUS_IDS.includes(status.id));
  }

  async isOpenStatus(statusId) {
    return _.includes(await this.openStatusIds(), statusId);
  }

  async getCustomers() {
    this._customers = await this._referenceDataService.get(['party.party']);

    return this._customers.get(['party.party']);
  }

  /**
   * @param {Object} model The case model passed in
   * @return {Object} model The new object, momentising the dates
   */
  momentiseDates(model) {
    const dates =
      [
        'caseAcceptanceDate',
        'dateOfRegistry',
        'keelLayingDate',
        'tocAcceptanceDate',
        'buildDate',
        'estimatedBuildDate'
      ];

    _.forEach(dates, (index) => {
      // if its not empty and not a moment, momentise it
      if (!_.isNil(model[index]) && !this._moment.isMoment(model[index])) {
        model[index] = this._moment(model[index]).format();
      }
    });

    return model;
  }

  /**
   * @param {Object} model The case model passed in
   * @return {Object} model The new object, processing the offices
   */
  processModel(model) {
    // Get the offices in indexed order; effectively removes preset field names
    if (!_.isNull(model.offices)) {
      model.offices =
        _.filter(model.offices, (office) => !_.isNil(office.office));
    }

    // Get the surveyors in indexed order;
    if (!_.isNull(model.surveyors)) {
      model.surveyors =
        _.filter(model.surveyors, (surv) => !_.isNil(surv.surveyor));
    }

    return this.momentiseDates(model);
  }

  /**
   * checkExistingCases(id) Check existing cases on asset;
   * @param {Object} model The model
   * @return {Number} number The case id, or null
   */
  async checkExistingCases(model) {
    // We're interested in the cases open on this asset, but only the ones
    // that aren't closed in some way; if we get a result and its the same
    // status id as our model, then it's the one we're dealing with so we need
    // to use its id, i.e., to update the case, not save a new one;
    const response =
      await this._Restangular
        .all('case')
        .customPOST({
          assetId: [ model.asset.id ],
          caseTypeId: [ model.caseType.id ],
          caseStatusId: _.sortBy(this.OPEN_STATUS_IDS)
        }, 'query');

    const ids = _.map(response, 'caseStatus.id');

    return _.isEmpty(ids) || !_.includes(ids, model.caseStatus.id) ?
      0 : response.id;
  }

  /**
   * @param {Boolean} model The job model to save as a case
   * @param {Boolean} flag The commit/uncommitted flag
   * @returns {Object} Case The newly saved case Object
   */
  async saveCase(model, flag) {
    model.caseStatus = {
      id: this._caseStatus.get(flag ? 'populate' : 'uncommitted')
    };

    // If we are truly saving we need to set the 'Case Acceptance Date' to
    // the current date - AC 22;
    if (flag) {
      model.caseAcceptanceDate = this._moment();
    }

    model = this.processModel(model);

    try {
      const caseId = await this.checkExistingCases(model);
      const saveMethod = caseId ?
        this._Restangular.one('case', caseId).customPUT(model) :
        this._Restangular.one('case').customPOST(model);

      const response = await saveMethod;

      return response;
    } catch (e) {
      this._$log.error('saveCase Error:', e);
    }
  }

  @pageable
  async query(
    query,
    { size = 10, page = 0, sort = 'id', order = 'desc', view = '' } = {}
  ) {
    const params = { page, size, sort, order };
    if (!_.isEmpty(view)) {
      const user = await this._employeeDecisionService.getCurrentUser();
      _.set(params, view, _.get(user, 'id'));
    }
    const cases = await this._Restangular.all('case')
        .customPOST(query, 'query', params);

    return this.populateAttributes(cases);
  }

  async forEmployee(
    id,
    { page = 0, size = 3, sort = 'id', order = 'desc' } = {}
  ) {
    const query = {
      employeeId: [ id ],
      caseStatusId: _.sortBy(this.OPEN_STATUS_IDS)
    };

    return this.query(query, { size, page, sort, order });
  }

  async forAssets(
    assetIds,
    { page = 0, size = 10, sort = 'id', order = 'desc' } = {}
  ) {
    const query = {
      assetId: assetIds,
      caseStatusId: _.sortBy(this.OPEN_STATUS_IDS)
    };

    return this.query(query, { size, page, sort, order });
  }

  async validateCase(payload) {
    return await this._Restangular
      .one('case', payload.id)
      .one('validate')
      .customPUT(payload);
  }
}
