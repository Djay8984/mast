import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';

export default class AttributeService extends BaseService {

  async saveAttributeList(assetId, item, attributes) {
    return this._dataSource
      .one('asset', assetId)
      .one('item', item.id)
      .one('attribute')
      .customPUT(_.set(
        _.pick(item, ['id', 'reviewed', 'name', 'itemType']),
        'attributes', _.map(_.reject(attributes, 'deleted'), 'model')));
  }
}
