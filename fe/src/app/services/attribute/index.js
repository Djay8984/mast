import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import attributeOfflineService from './attribute.offline.service';
import attributeOnlineService from './attribute.online.service';

export default angular
  .module('app.services.attribute', [])
  .service('attributeOfflineService', attributeOfflineService)
  .service('attributeOnlineService', attributeOnlineService)
  .service('attributeDecisionService', [
    'attributeOnlineService',
    'attributeOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
