import AttributeService from './attribute.service';

export default class AttributeOfflineService extends AttributeService {
  /* @ngInject */
  constructor(
    Restangular
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }
}
