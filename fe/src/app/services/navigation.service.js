import * as _ from 'lodash';

import Base from 'app/base.class';

/**
 * Usage note: this service must be injected into controllers that are to
 * be traversed back to, as well as the controller that has the back method on
 */

export default class NavigationService extends Base {
  /* @ngInject */
  constructor(
    $log,
    $rootScope,
    $state,
    offlinular
  ) {
    super(arguments);
    this._backStack = [];

    this._$rootScope.$on('$stateChangeStart', this.buildBackStack.bind(this));
    this._$rootScope.$on('$stateChangeStart', this.onChangeStart.bind(this));
    this._$rootScope.$on('$stateChangeError', this.onChangeError.bind(this));
    this._$rootScope.$on('$stateChangeSuccess',
      this.onChangeSuccess.bind(this));
  }

  onChangeSuccess(event, toState, toParams, fromState, fromParams) {
    this._buildParentStack(this._$state.current);
    Reflect.deleteProperty(this._$rootScope, 'OFFLINE_CHANGED');
  }

  onChangeStart(event, toState, toParams, fromState, fromParams) {
    if (toParams.offline && toParams.offline !== fromParams.offline) {
      this._$rootScope.OFFLINE = true;
      this._$rootScope.OFFLINE_CHANGED = true;
    } else if (!_.isNil(toParams.offline) && !toParams.offline &&
      toParams.offline !== fromParams.offline) {
      this._$rootScope.OFFLINE = false;
      this._$rootScope.OFFLINE_CHANGED = true;
    }
  }

  onChangeError(event, toState, toParams, fromState, fromParams, error) {
    this._$log.error(error);
    if (this._$rootScope.OFFLINE_CHANGED) {
      this._$rootScope.OFFLINE = !this._$rootScope.OFFLINE;
      Reflect.deleteProperty(this._$rootScope, 'OFFLINE_CHANGED');
    }
  }

  buildBackStack(event, toState, toParams, fromState, fromParams) {
    const index = _.findIndex(this._backStack, { name: toState.name });

    if (index !== -1) {
      this._backStack = _.slice(this._backStack, 0, index);
    }
    this._backStack.push({ name: toState.name, params: toParams });
  }

  back(opts = { reload: true }, inputParams) {
    opts = _.isBoolean(opts) ? { reload: opts } : opts;
    if (!_.isEmpty(this._backStack)) {
      this._backStack.pop();
    }

    const state = _.last(this._backStack);
    if (state) {
      const params = _.extend(state.params, inputParams);
      return this._$state.go(state.name, params, opts);
    }
    this._backStack = [];
    this.home();
  }

  /**
   * @param {object} state the state to _build the stack from
   * _builds a list of navigable (non-abstract) states & their related current
   *   parameters
   */
  _buildParentStack(state) {
    this._parentStack = [];

    let currentState = this._$state.get(state.name);
    while (currentState.parent) {
      if (!currentState.abstract) {
        this._parentStack.push({
          name: currentState.name,
          params: _.pick(this._$state.params, _.keys(currentState.params))
        });
      }
      currentState = this._$state.get(currentState.parent);
    }
  }

  backState() {
    const backStateName =
      _.get(this._backStack[this._backStack.length - 2], 'name');
    if (backStateName) {
      return this._$state.get(backStateName);
    }
    return backStateName;
  }

  /**
   * @param {number} level level of parentage to access, defaults to the last
   *   thing in the state stack
   * navigates to whatever level of parentage is passed or the last parent
   */
  parent(level = 1) {
    const state = this._parentStack[_.size(this._parentStack) - level];
    this._parentStack = _.slice(this._parentStack, 0, -level);
    this._$state.go(state.name, state.params);
  }

  async getBundle() {
    const bundles = await this._offlinular.getAvailableOffline(true);
    return _.first(bundles) || false;
  }

  async home() {
    const activeBundle = await this.getBundle();
    const state = {
      name: 'dashboard.myWork',
      params: { offline: false, bundleId: null }
    };

    if (!this._$rootScope.CONNECTED || activeBundle) {
      state.name = 'asset.jobs.view';
      state.params = {
        offline: true,
        bundleId: activeBundle.id,
        jobId: activeBundle.id,
        assetId: activeBundle.asset.id
      };
    }

    this._$state.go(state.name, state.params, { reload: true });
  }
}
