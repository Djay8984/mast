import * as _ from 'lodash';

import Base from 'app/base.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ServiceScheduleService
  extends ExternalAttributeMixin(Base) {
  /* @ngInject */
  constructor(
    Restangular,
    referenceDataService,
    serviceCreditStatus
  ) {
    super(arguments);

    this.openStatuses = [
      'notStarted',
      'partHeld',
      'postponed'
    ];
  }

  /**
   * Obtain a Restangular promise for an asset
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @returns {Object} Promise
   */
  coreAsset({ assetId }) {
    return this._Restangular.one('asset', assetId);
  }

  /**
   * Obtain a Restangular promise for an asset's product
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Number} ids.productId - ID of a product assigned to the asset
   * @returns {Object} Promise
   */
  coreProduct({ assetId, productId }) {
    return this.coreAsset({ assetId }).one('product', productId);
  }

  /**
   * Obtain a Restangular promise for an asset's service
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Number} ids.serviceId - ID of a service assigned to the asset
   * @returns {Object} Promise
   */
  coreService({ assetId, serviceId }) {
    return this.coreAsset({ assetId }).one('service', serviceId);
  }

  /**
   * Get an array of products assigned to an asset.
   *
   * Endpoints:
   * GET /asset/{assetId}/product
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @returns {Object} Promise for products assigned to the asset
   */
  async getAssignedAssetProducts({ assetId }) {
    const response = await this.coreAsset({ assetId })
      .all('product')
      .getList();

    return this.populateAttributes(response);
  }

  /**
   * Get an array of services assigned to an asset.
   *
   * NB: Routes needing access to individual services (e.g. viewing / editing)
   *     will tend to be children of a route that lists all services so that
   *     they inherit the list.
   *
   * Endpoints:
   * GET /asset/{assetId}/service
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @returns {Object} Promise for services assigned to the asset
   */
  async getAssignedAssetServices({ assetId }) {
    const statusIdList = _.map(this.openStatuses, (v) =>
      this._serviceCreditStatus.get(v)).join(',');

    const response = await this.coreAsset({ assetId })
      .all('service')
      .getList({ statusIdList });

    return this.populateAttributes(response);
  }

  /**
   * Get an array of services assigned to a product associated with an asset.
   *
   * NB: Routes needing access to individual products (e.g. viewing / editing)
   *     will tend to be children of a route that lists all products so that
   *     they inherit the list.
   *
   * Endpoints:
   * GET /asset/{assetId}/product/{productId}/service
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Number} ids.productId - ID of a product associated with the asset
   * @returns {Object} Promise for services assigned to the asset
   */
  getAssignedProductServices({ assetId, productId }) {
    return this.coreProduct({ assetId, productId })
      .all('service')
      .getList();
  }

  /**
   * Assign a service to an asset
   *
   * Endpoints:
   * POST /asset/{assetId}/service
   *
   * @param {Number} assetId - ID of a particular asset
   * @param {Object} scheduledServices - Service object to assign to the asset
   * @returns {Object} Promise (Restangular response)
   */
  assignAssetService(assetId, scheduledServices) {
    return this.coreAsset({ assetId })
      .all('service')
      .customPOST({ scheduledServices });
  }

  /**
   * Update a service assigned to an asset
   *
   * Endpoints:
   * PUT /asset/{assetId}/service/{serviceId}
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Number} ids.serviceId - ID of the service to update
   * @param {Object} service Service object to update the existing service
   * @returns {Object} Promise (Restangular response)
   */
  updateAssignedAssetService({ assetId, serviceId }, service) {
    return this.coreService({ assetId, serviceId })
      .customPUT(service);
  }

  /**
   * Update the list of products assigned to an asset.
   *
   * NB: This is used to remove assigned products from an asset, as well as
   *     to assign them to an asset.
   *
   * Endpoints:
   * PUT /asset/{assetId}/product
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Object} updateSpec - Arrays to build an updated catalogue list
   * @param {Array} updateSpec.base - Base list
   * @param {Array} [updateSpec.toAdd] - Catalogues to add to the base
   * @param {Array} [updateSpec.toRemove] - Catalogues to remove from the base
   * @returns {Object} Promise (Restangular response)
   */
  updateAssignedAssetProducts({ assetId }, updateSpec) {
    const purgedBase = _.isArray(updateSpec.toRemove) ?
      _.reject(updateSpec.base, (product) =>
        _.some(updateSpec.toRemove, { id: product.id })) :
      updateSpec.base;

    const grownBase = _.isArray(updateSpec.toAdd) ?
      _.concat(purgedBase, updateSpec.toAdd) : purgedBase;

    return this.coreAsset({ assetId })
      .all('product')
      .customPUT({
        productList: grownBase
      });
  }

  /**
   * Remove a list of assigned services from an asset.
   *
   * Endpoints:
   * DELETE /asset/{assetId}/service?serviceIdList=1%2C2%2C3
   *
   * An idList processed as [ 1, 2, 3 ].join(',') will become 1%2C2%2C3
   *
   * @param {Object} ids - IDs to address the correct endpoint
   * @param {Number} ids.assetId - ID of a particular asset
   * @param {Number} idList - List of service IDs assigned to the asset
   * @returns {Object} Promise (Restangular response)
   */
  async removeAssignedAssetServices({ assetId }, idList) {
    return this.coreAsset({ assetId })
      .customDELETE('service', { serviceIdList: idList.join(',') });
  }
}
