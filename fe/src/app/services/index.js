import * as angular from 'angular';

import asset from './asset';
import assetModel from './asset-model';
import attachment from './attachment';
import attribute from './attribute';
import codicil from './codicil';
import crediting from './crediting';
import defect from './defect';
import employee from './employee';
import job from './job';
import offlineData from './offline-data';
import repair from './repair';
import report from './report';
import soap from './soap/';
import survey from './survey';
import task from './task';

import batchActionService from './batch-action.service';
import caseCustomerService from './case-customer.service';
import caseFieldMatrix from './case-field-matrix.service';
import caseService from './case.service';
import draftItemService from './draft-item.service';
import harmonisationDateService from './harmonisation-date.service';
import ihsService from './ihs.service';
import milestoneService from './milestone.service';
import myWorkService from './mywork.service';
import navigationConfirmationService from './navigation-confirmation.service';
import navigationService from './navigation.service';
import node from './node.service';
import printService from './print.service';
import pageService from './page.service';
import rbacService from './rbac.service';
import referenceDataService from './reference-data.service';
import relationshipService from './relationship.service';
import serviceScheduleService from './service-schedule.service';

// import spellCheckService from './spell-check.service';

import templateService from './template.service';
import typeaheadService from './typeahead.service';

export default angular
  .module('app.services', [
    asset,
    assetModel,
    attachment,
    attribute,
    codicil,
    crediting,
    defect,
    employee,
    job,
    offlineData,
    repair,
    report,
    soap,
    survey,
    task
  ])
  .service('batchActionService', batchActionService)
  .service('caseCustomerService', caseCustomerService)
  .service('caseFieldMatrix', caseFieldMatrix)
  .service('caseService', caseService)
  .service('draftItemService', draftItemService)
  .service('harmonisationDateService', harmonisationDateService)
  .service('ihsService', ihsService)
  .service('milestoneService', milestoneService)
  .service('myWorkService', myWorkService)
  .service('navigationConfirmationService', navigationConfirmationService)
  .service('navigationService', navigationService)
  .service('node', node)
  .service('pageService', pageService)
  .service('printService', printService)
  .service('rbacService', rbacService)
  .service('referenceDataService', referenceDataService)
  .service('relationshipService', relationshipService)
  .service('serviceScheduleService', serviceScheduleService)

//  .service('spellCheckService', spellCheckService)
  .service('templateService', templateService)
  .service('typeaheadService', typeaheadService)
  .name;
