import * as _ from 'lodash';

import * as angular from 'angular';

import referenceDataKeyMap from './reference-data.keymap';

export default (Base) => class extends Base {
  get referenceDataKeyMap() {
    return referenceDataKeyMap;
  }

  async populateAttributes(list) {
    await Promise.all(this.getPromisedAttributes({ items: list }));

    return list;
  }

  getPromisedAttributes({
    items,
    recursionDepth = 0,
    originalData = null,
    path = []
  }) {
    const MAX_RECURSION_DEPTH = 20;

    if (_.isNull(originalData)) {
      originalData = items;
    }

    const hydrate = (item) => {
      // Replaces 'value' with 'data' on item[key]
      // i.e. replace { id: <Number> } stub with actual data
      //
      // The sourceKey is used to look up the key in reference data
      // The targetKey is used to place the result on the object
      const setExternalAltAttribute = (object, sourceKey, targetKey, value) =>
        new Promise((resolve, reject) => {
          this.getAttribute(sourceKey, value).then((data) => {
            if (_.isEmpty(data)) {
              data = null;
            }

            if (!_.isNil(data)) {
              object[targetKey] = angular.copy(data);
            }

            return resolve(data);
          });
        });

      // Calls setExternalAltAttribute with sourceKey === targetKey
      const setExternalAttribute = (object, key, value) =>
        setExternalAltAttribute(object, key, key, value);

      const promises = [];

      Object.entries(item).forEach(([ key, value ]) => {
        if (_.isArray(value) && Reflect.has(this.referenceDataKeyMap, key)) {
          // The reference data key map offers guidance on how to treat
          // the elements of this array
          promises.push(Promise
            .all(_.map(value, (val, index, array) =>
              setExternalAltAttribute(array, key, index, val))));
        } else if (_.isArray(value)) {
          // No guidance exists on the contents of the array, so treat each
          // item on its own merits
          promises.push(Promise.all(this.getPromisedAttributes({
            items: value,
            recursionDepth: recursionDepth + 1,
            originalData,
            path: _.concat(path, [key])
          })));
        } else if (_.get(value, 'id') && _.get(value, '_id') &&
                   Reflect.ownKeys(value).length === 2) {
          // Base case: the value is an object that needs hydrating
          // Having hydrated the value, it may need further hydration
          promises.push(setExternalAttribute(item, key, value).then(() => {
            // Only recurse if the key does not already exist in the path
            if (!_.includes(path, key)) {
              return Promise.all(this.getPromisedAttributes({
                items: _.get(item, key),
                recursionDepth: recursionDepth + 1,
                originalData,
                path: _.concat(path, [key])
              }));
            }

            if (this._$log) {
              this._$log.warn(`cycle detected: ${key} in ${path.join('.')}`);
            }
          }));
        }
      });

      return promises;
    };

    const promises = [];

    // Assumption is that item is an object
    // If an item bears the non-enumerable isHydrated flag, skip it
    if (recursionDepth < MAX_RECURSION_DEPTH) {
      _.concat(items).forEach((item) => {
        if (!item.isHydrated) {
          promises.push(Promise.all(hydrate(item)).then((value) => {
            // All the hydrations have resolved for this item
            // so mark it as hydrated with a non-enumerable flag
            // that no one else will know of or care to look for.
            Reflect.defineProperty(item, 'isHydrated', {
              enumerable: false,
              configurable: false,
              writable: false,
              value: true
            });
          }));
        }
      });
    } else if (this._$log) {
      this._$log.error('getPromisedAttributes() MAX_RECURSION_DEPTH reached',
        items, originalData);
    }

    return promises;
  }

  async getAttribute(key, value) {
    const reference = await this.fromRefData(key, value) ||
    await this.fromService(key, value) || {};

    return _.merge(this.getConstant(key, value), reference);
  }

  async fromRefData(key, value) {
    try {
      /* eslint prefer-const: 0 */
      let {
        referenceKey,
        searchKey,
        omit
      } = this.referenceDataKeyMap[key] || {};

      if (referenceKey) {
        const data = await this._referenceDataService.get(
          _.join([referenceKey, searchKey], '.'));
        const match = _.find(data, { id: value.id });

        return _.isArray(omit) ? _.omit(match, omit) : match;
      }
    } catch (e) {
      return null;
    }
  }

  getConstant(key, value) {
    const refMap = this.referenceDataKeyMap[key] || {};
    const constant = refMap.constant;
    const result = {};

    if (constant) {
      try {
        const constantService =
          _.get(this, '_referenceDataService._$injector').get(constant);

        const all = constantService.toObject();

        _.forEach(all, (nr, name) => {
          if (nr === value.id) {
            result.constant = name;
          }
        });
      } catch (e) {
        const log = _.get(this, '_referenceDataService._$injector').get('$log');
        log.warn(`Constant Service "${constant}" not found`);
      }
    }
    return result;
  }

  // TODO: map appropriate service calls (if necessary)
  async fromService(key, value) {
    return null;
  }
};
