import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class TaskService extends ExternalAttributeMixin(BaseService) {

  @pageable
  async query(
    query,
    { page = 0, size = 50, sort = 'id', order = 'asc' } = {},
    { endpoint, hydrateItems = false, assetId, jobId } = {}
  ) {
    const tasks = await this._dataSource
      .all(endpoint ? `${endpoint}-task` : 'task')
      .customPOST(query, 'query', { page, size, sort, order });

    if (hydrateItems) {
      const itemIds = _.map(tasks, 'assetItem.id');

      const assetItems =
        await this._assetModelDecisionService.getItems(assetId, itemIds);

      _.forEach(tasks, (task) =>
        task.assetItem = _.find(assetItems, { id: task.assetItem.id }));
    }

    return this.populateAttributes(tasks);
  }
}
