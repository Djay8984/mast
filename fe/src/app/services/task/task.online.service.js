import TaskService from './task.service';

export default class TaskOnlineService extends TaskService {
  /* @ngInject */
  constructor(
    Restangular,
    assetModelDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  async updateTasks(jobId, tasks) {
    return this.populateAttributes(await this._dataSource
      .one('job', jobId)
      .all('wip-task') // the only task PUT is wip
      .customPUT({ tasks }));
  }

  get _dataSource() {
    return this._Restangular;
  }
}
