import TaskService from './task.service';

export default class TaskOfflineService extends TaskService {
  /* @ngInject */
  constructor(
    assetModelDecisionService,
    offlinular,
    referenceDataService
  ) {
    super(arguments);
  }

  async updateTasks(jobId, tasks) {
    // The offlinular can't handle ListDTOs properly, so we need to
    // update each task as if there was a PUT job/{x}/wip-task/{y} API
    const result = [];
    for (const task of tasks) {
      result.push(await this._dataSource
        .one('job', jobId)
        .all('wip-task', task.id) // the only task PUT is wip
        .customPUT(task));
    }

    return this.populateAttributes(result);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
