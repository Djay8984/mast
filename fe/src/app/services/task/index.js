import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import taskOfflineService from './task.offline.service';
import taskOnlineService from './task.online.service';

export default angular
  .module('app.services.task', [])
  .service('taskOfflineService', taskOfflineService)
  .service('taskOnlineService', taskOnlineService)
  .service('taskDecisionService', [
    'taskOnlineService',
    'taskOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
