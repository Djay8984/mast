import * as _ from 'lodash';
import Base from 'app/base.class';

export default class TemplateService extends Base {

  /* @ngInject */
  constructor(Restangular) {
    super(arguments);
    this.LIVE_TEMPLATE_STATUS = 1;
  }

  findTemplates(data, templates) {
    const search = data.search;

    let regex = null;
    if (search.title && search.title.split('*').length > 1) {
      let rule = search.title.split('*').join('.*');
      rule = `^${rule}$`;
      regex = new RegExp(rule, 'i');
    }

    //  Validation rules
    //  Executed if key matches with search
    const filters = {
      codicilType: (val) =>
        search.codicilType.id === _.get(val, 'codicilType.id'),
      title: (val) =>
        regex ? regex.test(val.title) : search.title === val.title,
      itemType: (val) =>
        _.get(search, 'itemType.id') === _.get(val, 'itemType.id'),
      status: (val) =>
        search.status === _.get(val, 'templateStatus.id')
    };

    const filteredList = _.filter(templates, (template) => {
      let valid = true;
      _.forEach(filters, (filter, key) => {
        if (search[key]) {
          valid = valid && filter(template);
        }
      });

      return valid;
    });

    return !_.isEmpty(filteredList) ?
      this.addAppliedToAssetsCount(filteredList, data.codicilType) : [];
  }

  async addAppliedToAssetsCount(templates, codicilType) {
    const appliedCountList =
      await this.getAssetAppliedCount(_.map(templates, 'id'), codicilType);

    return _.map(appliedCountList.counts,
      (count, i) =>
        _.set(_.find(templates,
          { 'id': _.parseInt(i) }), 'count', count));
  }

  async getAssetAppliedCount(assetIds, codicilType) {
    return this._Restangular
      .all('asset')
      .all(codicilType.name)
      .get('count', { template: assetIds.toString() });
  }
}
