import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import jobOfflineService from './job.offline.service';
import jobOnlineService from './job.online.service';

export default angular
  .module('app.services.job', [])
  .service('jobOfflineService', jobOfflineService)
  .service('jobOnlineService', jobOnlineService)
  .service('jobDecisionService', [
    'jobOnlineService',
    'jobOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
