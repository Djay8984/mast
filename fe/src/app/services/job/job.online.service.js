import JobService from './job.service';
import * as _ from 'lodash';

export default class JobOnlineService extends JobService {
  /* @ngInject */
  constructor(
    Restangular,
    employeeDecisionService,
    referenceDataService,
    jobStatus,
    officeRole,
    surveyorRole
  ) {
    super(arguments);
  }

  /**
   * Online only
   * @param {object} model Passes in the model from the controller;
   * @returns {object}
   */
  async addJob(model) {
    // The constructed payload we'll save as the new job;
    const payload = {
      jobStatus: { id: 1 },
      offices: [
        { officeRole: { id: this._officeRole.get('sdo') } }
      ],
      scopeConfirmed: false
    };

    super.processModel(model);

    // Merges the processed model with what we want as extras
    _.merge(model, payload);

    return this._dataSource
      .one('job')
      .customPOST(model);
  }

  get _dataSource() {
    return this._Restangular;
  }

  async getEndorsement(jobId) {
    const res = await this._Restangular
      .one('job', jobId)
      .all('follow-up-action')
      .getList();
    return this.populateAttributes(res);
  }

  async updateEndorsement(jobId, payload) {
    payload = _.omit(payload, ['shortDesc', 'selected']);
    const res = await this._Restangular
      .one('job', jobId)
      .one('follow-up-action', payload.id)
      .customPUT(payload);
    return this.populateAttributes(res);
  }
}
