import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { modelable, pageable } from 'app/decorators';
import { Job } from 'app/models';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class JobService extends ExternalAttributeMixin(BaseService) {

  @pageable
  async forAssets(
    assetIds,
    { page = 0, size = 10, sort = 'id', order = 'desc' } = {}
  ) {
    const query = {
      assetId: assetIds,
      jobStatusId: this.openStatusIds()
    };

    return this.query(query, { size, page, sort, order });
  }

  @pageable
  async forEmployee(
    id,
    { page = 0, size = 3, sort = 'id', order = 'desc' } = {}
  ) {
    // There should probably be a different method for getting only open jobs
    const query = {
      employeeId: [ id ],
      jobStatusId: this.openStatusIds()
    };

    return this.query(query, { size, page, sort, order });
  }

  async get(id) {
    return this._dataSource
      .one('job', id)
      .get()
      .then(this.populateAttributes.bind(this));
  }

  /**
   * @summary getCertificatesForJob(jobId)
   * @param {Number} jobId The id of the job
   * @return {Object} certificates The array of certificates
   */
  async getCertificatesForJob(jobId) {
    return this._dataSource
      .one('job', jobId)
      .all('wip-certificate')
      .getList();
  }

  /**
   * @summary getSurveysForJob(jobId)
   * @param {Number} jobId The id of the job
   * @return {Object} surveys The array of survey objects
   */
  async getSurveysForJob(jobId) {
    return this.populateAttributes(
      await this._dataSource
      .one('job', jobId)
      .all('survey')
      .getList()
    );
  }

  /**
   * @summary saveNewSurveyForJob(job, survey)
   * @param {Object} id The job id
   * @param {Object} survey The survey to save
   * @return {Object} survey
   */
  async saveNewSurveyForJob(id, survey) {
    return this._dataSource
      .one('job', id)
      .customPOST(survey, 'survey');
  }

  async getUser(name) {
    if (name && _.isString(name)) {
      name = await this._employeeDecisionService.getUserFromName(name);
    }

    return name;
  }

  /**
   * @param {Object} job Object that holds the surveyor information
   * @returns {undefined} nothing Returns the newly saved job
   */
  async saveJobTeam(job) {
    const processedJob = await this.processSurveyors(job);
    return this._dataSource
      .one('job', job.id)
      .customPUT(processedJob)
      .then(this.populateAttributes.bind(this));
  }

  /**
   * @returns {Array} array Returns an array of ids
   */
  openStatusIds() {
    return _.values(_.omit(this._jobStatus.toObject(),
      ['aborted', 'cancelled', 'closed']));
  }

  /**
   * @param {Object} model A model passed to process
   * @returns {Object} model The processed model is returned
   */
  processModel(model) {
    /* Fix the offices and give the SDO id to the office; */
    if (!_.isArray(model.offices)) {
      Reflect.deleteProperty(model, 'offices');
      if (_.isObject(model.sdo)) {
        const offices = [
          {
            office: { id: model.sdo.id },
            officeRole: { id: this._officeRole.get('sdo') }
          }
        ];
        model.offices = offices;
      }
    }

    /* Save the case in the correct format; */
    if (!_.isObject(model.aCase) && _.isNumber(model.caseId)) {
      model.aCase = { id: model.caseId };
    }

    // Remove the case if it has been removed by the user after it was
    // previously on the job
    if (!_.get(model, 'aCase.id')) {
      Reflect.deleteProperty(model, 'aCase');
    }

    Reflect.deleteProperty(model, 'sdo');
    Reflect.deleteProperty(model, 'caseId');
    Reflect.deleteProperty(model, 'changeOfFlag');

    return model;
  }

  /**
   * @param {Job} job A Job model passed to process
   * @returns {Object} model The processed job is returned as a plain object,
   * this should be the job with the employees filled in correctly;
   */
  async processSurveyors(job) {
    const newEmployeeLink = (lrEmployee, role) =>
      lrEmployee ? {
        lrEmployee,
        employeeRole: { id: this._surveyorRole.get(role) }
      } : null;

    const employees = [
      newEmployeeLink(job.sdoCoordinator, 'sdoCoordinator'),
      newEmployeeLink(job.eicManager, 'eicManager'),
      newEmployeeLink(job.authorisingSurveyor, 'authorisingSurveyor'),
      ..._.map(job.generalSurveyors, (surveyor, index) =>
        newEmployeeLink(surveyor,
          index === job.leadSurveyorIndex ? 'leadSurveyor' : 'surveyor')
      )
    ];

    job.employees = _.compact(employees);
    return job.model;
  }

  /**
   * @summary updateJob()
   * @param {Object} job The job to save
   * @return {Object} job The restangular Object
   */
  async updateJob(job, process = false) {
    return this._dataSource
      .one('job', job.id)
      .customPUT(process ? this.processModel(job) : job)
      .then(this.populateAttributes.bind(this));
  }

  /**
   * Checks whether or not the given user is linked to an office in reference
   * data which is set as the given job's SDO
   * @param {Object} user the currently logged in user
   * @param {Job} job the job to check the user against
   * @returns {Boolean} true or false
   */
  async userSharesSdoWithJob(user, job) {
    const offices =
      await this._referenceDataService.get('employee.employeeOffices');
    const jobOffice =
      _.find(job.offices, { officeRole: { id: this._officeRole.get('sdo') } });

    return _.chain(offices)
      .filter({ office: { id: jobOffice.office.id } })
      .map('employee.id')
      .includes(user.id)
      .value();
  }

  /**
   * Checks whether or not the given user is linked to a team based in an office
   * in reference data which is set as the given job's SDO
   * @param {Object} user the currently logged in user
   * @param {Job} job the job to check the user against
   * @returns {Boolean} true or false
   */
  async userSharesTeamSdoWithJob(user, job) {
    const refData =
      await this._referenceDataService.get([
        'employee.teamEmployee',
        'employee.teamOffice'
      ]);
    const jobOffice =
      _.find(job.offices, { officeRole: { id: this._officeRole.get('sdo') } });

    const teamIds = _.chain(refData)
      .get('employee.teamEmployee')
      .filter({ employee: { id: user.id } })
      .map('team.id')
      .value();

    return _.chain(refData)
      .get('employee.teamOffice')
      .filter((to) => _.includes(teamIds, to.team.id))
      .find({ office: { id: jobOffice.office.id } })
      .value();
  }

  @modelable(Job)
  @pageable
  async query(
    query,
    { page = 0, size = 10, sort = 'id', order = 'desc', view = '' } = {}
  ) {
    const params = { page, size, sort, order };
    if (!_.isEmpty(view)) {
      const user = await this._employeeDecisionService.getCurrentUser();
      _.set(params, view, _.get(user, 'id'));
    }
    return this.populateAttributes(await this._dataSource.all('job')
        .customPOST(query, 'query', params));
  }
}
