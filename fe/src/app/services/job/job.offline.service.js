import * as _ from 'lodash';

import JobService from './job.service';

export default class JobOfflineService extends JobService {
  /* @ngInject */
  constructor(
    employeeDecisionService,
    offlinular,
    referenceDataService,
    surveyorRole,
    jobStatus
  ) {
    super(arguments);
  }

  /**
   * @summary saveNewSurveyForJob(job, survey)
   * @param {Object} id The job id
   * @param {Object} survey The survey to save
   * @return {Object} survey
   */
  async saveNewSurveyForJob(id, survey) {
    const surv = await this._dataSource
      .one('job', id)
      .customPOST(survey, 'survey');

    const job = await this.get(id);
    job.surveys = _.union(job.surveys, [surv.plain()]);
    this.updateJob(job);
    return surv;
  }

  /**
   * @summary updateJob()
   * @param {Object} job The job to save
   * @return {Object} job The restangular Object
   */
  async updateJob(job, process = false) {
    // Can't use the codicilDecisionService due to circular dependencies
    const codicilCore = (path, id) =>
      this._offlinular
        .one('job', job.id)
        .one(path, id);

    const ais = await codicilCore('wip-actionable-item').getList();
    const cocs = await codicilCore('wip-coc').getList();

    // Iterate through actionable items and cocs,
    // updating the 'jobScopeConfirmed' property where required
    for (const codicil of _.concat(ais, cocs)) {
      if (codicil.jobScopeConfirmed !== job.scopeConfirmed) {
        codicil.jobScopeConfirmed = job.scopeConfirmed;
        await codicilCore(codicil.route, codicil.id).customPUT(codicil);
      }
    }

    return super.updateJob(job, process);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
