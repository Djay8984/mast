import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable } from 'app/decorators';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DefectService extends ExternalAttributeMixin(BaseService) {

  /**
   * @param {Number} defectId The ID of a particular defect
   * @returns {Object} defect The defect required
   */
  async get({ assetId, jobId, defectId, context }, hydrate = true) {
    const request = this._core(
      { assetId, jobId, codicilId: defectId, context }).get();

    return hydrate ?
      this._hydrate({ assetId, jobId, defectId }, await request) :
      request;
  }

  @pageable
  async getCocs({ assetId, jobId, defectId, context }) {
    return this._core({ assetId, jobId, codicilId: defectId, context })
      .getList(context === 'asset' ? 'coc' : 'wip-coc');
  }

  async getDefectsForJob(jobId) {
    return this._dataSource
      .one('job', jobId)
      .all('wip-defect')
      .getList();
  }

  /**
   * Create (but do not invoke) a promise for reaching a defect
   * endpoint in asset or job scope.
   *
   * @returns {Promise} promise
   */
  _core({ assetId, jobId, codicilId, context }) {
    return this._dataSource
      .one(context === 'asset' ? 'asset' : 'job',
        context === 'asset' ? assetId : jobId
      )
      .one(context === 'asset' ? 'defect' : 'wip-defect', codicilId);
  }

  async _hydrate({ assetId, jobId, context }, dryDefect) {
    const defect = await this.populateAttributes(dryDefect);

    // recursively hydrates the categories & parents
    const refCategories = await this._referenceDataService.get(
      'defect.defectCategories');
    const setCategory = (current) => {
      const category = _.find(refCategories, { id: current.parent.id });
      current.parent = category;
      if (_.has(category, 'parent.id')) {
        setCategory(current.parent);
      }
    };

    if (_.has(defect, 'defectCategory.parent.id')) {
      setCategory(defect.defectCategory);
    }

    if (!_.isEmpty(defect.items) && assetId) {
      const items = await this._assetModelDecisionService.queryItem(
        assetId, { itemId: _.map(defect.items, 'item.id') });
      defect.items = _.map(defect.items, (item) =>
        _.set(item, 'item',
          _.merge(
            _.find(items, { id: item.item.id }),
            { isHydrated: true })));
    }

    return defect;
  }

  /**
   * @param {Object} params Contains exactly one of assetId or jobId
   * @returns {Array} defects A list of defects related to an asset or job
   */
  @pageable
  async query({ assetId, jobId, context }, query, endpoint, hydrate, {
    page = 0, size = 20, sort = 'id', order = 'asc' } = {}) {
    const payload = _.omit(query, ['typeList', 'viewType']);
    const slugType = _.find(query.typeList, ['slug', endpoint]);

    // When the starting/default search query is used it contains typelist as
    // an object, but if search terms have been selected then it is an array
    const type = slugType ? slugType : query.typeList.slug;
    let result = [];

    if (type) {
      _.set(payload, 'categoryList', _.get(type, 'categories'));

      result = await this._core({ assetId, jobId, context })
          .customPOST(payload, 'query', { page, size, sort, order }, {});

      if (hydrate) {
        _.forEach(result, (obj) => this.populateAttributes(obj));
      }
    }

    return result;
  }

  filterAvailableItemsForActionableItems(defect, actionableItems) {
    // Reject from defect.items those which already have an actionable item
    // relating to it
    return _.reject(

      // List of all assetItems on the defect
      _.map(defect.items, 'item'),
      (item) =>
        _.includes(

          // List of item IDs on actionable items for the given defect
          _.map(
            _.filter(actionableItems, { defect: { id: defect.id } }),
            'assetItem.id'),
        item.id));
  }
}
