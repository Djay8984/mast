import DefectService from './defect.service';

export default class DefectOfflineService extends DefectService {
  /* @ngInject */
  constructor(
    assetModelDecisionService,
    offlinular,
    referenceDataService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }

  /**
   * Save (create) a defect in asset or job scope.
   *
   * @param {Object} payload Defect definition
   * @returns {Object} Object
   */
  async save({ assetId, jobId, context }, payload) {
    // to link repairs to defect items, we need to generate ids for the links
    for (const item of payload.items) {
      item.id = await this._offlinular.generateNewId();
    }

    return this._core({ assetId, jobId, context })
      .customPOST(payload);
  }

  async update({ assetId, jobId, defectId, codicilId, context }, payload) {
    return this._core({ assetId, jobId, codicilId: defectId || codicilId })
      .customPUT(payload);
  }
}
