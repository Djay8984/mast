import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import defectOfflineService from './defect.offline.service';
import defectOnlineService from './defect.online.service';

export default angular
  .module('app.services.defect', [])
  .service('defectOfflineService', defectOfflineService)
  .service('defectOnlineService', defectOnlineService)
  .service('defectDecisionService', [
    'defectOnlineService',
    'defectOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
