import DefectService from './defect.service';

export default class DefectOnlineService extends DefectService {
  /* @ngInject */
  constructor(
    Restangular,
    assetModelDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }

  /**
   * Save (create) a defect in asset or job scope.
   *
   * @param {Object} payload Defect definition
   * @returns {Object} Object
   */
  async save({ assetId, jobId, context }, payload) {
    return this._core({ assetId, jobId, context })
      .customPOST(payload);
  }

  async update({ assetId, jobId, defectId, codicilId, context }, payload) {
    return this._core({ assetId, jobId, codicilId: defectId || codicilId })
      .customPUT(payload);
  }
}
