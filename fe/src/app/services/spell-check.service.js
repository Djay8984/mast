import path from 'path';
import fs from 'fs';
import JSZip from 'jszip';

import { remote, webFrame } from 'electron';

const spellChecker = remote.getGlobal('spellChecker');
const buildEditorContextMenu = remote.getGlobal('buildEditorContextMenu');

import * as _ from 'lodash';

import Base from 'app/base.class';

const ELEMENT_SELECTOR =
  'textarea[spellcheck="true"], textarea[data-spellcheck="true"],' +
  'input[spellcheck="true"], input[data-spellcheck="true"]';

const MAX_SUGGESTIONS = 5;
const DICTIONARY_PATH =
  ['src', 'lib', 'spellchecker', 'vendor', 'hunspell_dictionaries'];


/*
  how this all works:
    the actual spellchecking is provided by node-spellchecker, this service
    manages that, interacting with that lower-level library & electron to apply
    the spellchecking on startup & request/unpack dictionaries

    on startup the spellchecker polls the backend for a zipped dictionary,
    containing the .aff & .dic file node-spellchecker's underlying lib
    (hunspell) uses for word definitions. the loaded arraybuffer is parsed into
    a zip which is then unpacked on the fly & the two files are written & put
    in a place for hunspell to pick up. node-spellchecker is then initialised
 */
export default class SpellCheckService extends Base {
  /* @ngInject */
  constructor(
    $q,
    $log,
    $window,
    $document,
    $timeout,
    Restangular
  ) {
    super(arguments);
  }

  /**
   * @param {boolean} online whether or to try the backend
   * prepares the database & loads new data
   */
  async prepare(online = true) {
    if (online) {
      await this._parseBufferToFiles(await this._getAPIData());
    }
    this.applySpellChecking();
  }

  /**
   * applies the spellchecking: mispelling warning & context menu after forcing
   *   a refresh of the dictionaries from disk
   */
  applySpellChecking() {
    spellChecker.setDictionary('en-GB', path.join(...DICTIONARY_PATH));

    this._$window.addEventListener('contextmenu', this.contextMenu.bind(this));

    webFrame.setSpellCheckProvider('en-GB', true,
      { spellCheck: this.spellCheck.bind(this) });
  }

  /**
   * @param {string} word text to test if spelled correctly
   * @return {boolen} whether the word is spelled correctly
   * always truthy if the field is one which should not be spell checked
   */
  spellCheck(word) {
    return _.first(this._$document).activeElement.matches(ELEMENT_SELECTOR) ?
      !spellChecker.isMisspelled(word) :
      true;
  }

  /**
   * @param {object} event listener event triggered by context menu event
   * shows the context menu with suggestions for spell checking
   */
  contextMenu(event) {
    if (event.target.closest(ELEMENT_SELECTOR)) {
      const word = this._$window.getSelection().toString();
      if (spellChecker.isMisspelled(word)) {
        const menu = buildEditorContextMenu({
          isMisspelled: true,
          spellingSuggestions: _.slice(
            spellChecker.getCorrectionsForMisspelling(word),
            0, MAX_SUGGESTIONS)
        });
        this._$timeout(() => menu.popup(remote.getCurrentWindow()), 30);
      }
    }
  }

  /**
   * @param {buffer} buffer a zip file as a buffer (uint8array)
   * unpacks a zip & writes its files to disk at the dictionary path,
   *   overwriting any equivalently named files if there
   */
  async _parseBufferToFiles(buffer) {
    const zip = await JSZip.loadAsync(buffer);

    // retrieve files as strings
    const files = await this._$q.all(_.mapValues(zip.files, (file) =>
        zip.file(file.name).async('string')));

    // write strings as files to disk
    await this._$q.all(_.mapValues(files, (content, filename) =>
        fs.writeFile(path.join(...DICTIONARY_PATH, filename), content)));

    this._$log.debug('[SCS]', 'dictionaries updated');
  }

  /**
   * @return {promise} resolving as a buffer
   */
  async _getAPIData() {
    return this._Restangular
      .one('spellcheck')
      .one('dictionary')
      .withHttpConfig({ responseType: 'arraybuffer' })
      .get()
      .then((res) => new Buffer(res.buffer));
  }
}
