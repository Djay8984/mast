import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import codicilOfflineService from './codicil.offline.service';
import codicilOnlineService from './codicil.online.service';

export default angular
  .module('app.services.codicil', [])
  .service('codicilOfflineService', codicilOfflineService)
  .service('codicilOnlineService', codicilOnlineService)
  .service('codicilDecisionService', [
    'codicilOnlineService',
    'codicilOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
