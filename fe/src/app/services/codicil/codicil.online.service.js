import CodicilService from './codicil.service';

export default class CodicilOnlineService extends CodicilService {
  /* @ngInject */
  constructor(
    $q,
    Restangular,
    assetModelDecisionService,
    defectDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }
}
