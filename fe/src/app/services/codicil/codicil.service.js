import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';
import ExternalAttributeMixin from 'app/services/external-attribute.class';

import { pageable } from 'app/decorators';

const CODICIL_TYPE_CODE_MAP = {
  'actionable-item': 'AI',
  'asset-note': 'AS',
  'coc': 'CC'
};

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CodicilService
  extends ExternalAttributeMixin(BaseService) {

  _core({ assetId, jobId, codicilId, type, context }) {
    context = context || (jobId && !assetId ? 'job' : 'asset');
    const obj = {
      asset: {
        target: ['asset', assetId],
        query: [ type, codicilId ]
      },
      job: {
        target: ['job', jobId],
        query: [`wip-${type}`, codicilId]
      }
    };

    const use = obj[context] || obj.asset;

    return this._dataSource
      .one(...use.target)
      .one(...use.query);
  }

  @pageable
  async query({ assetId, jobId, context }, query, endpoint, hydrate, {
    page = 0, size = 20, sort = 'id', order = 'asc' } = {}) {
    const payload = _.omit(query, ['typeList', 'viewType']);
    const type = _.find(query.typeList, ['slug', endpoint]);
    context = context || (jobId && !assetId ? 'job' : 'asset');

    const queryUrl = context === 'asset' ?
    { target: 'asset', id: assetId, endpoint } :
    { target: 'job', id: jobId, endpoint: `wip-${endpoint}` };

    let result = [];

    if (type) {
      if (type.categories) {
        type.categories = await this.getChildCategories(type.categories);
      }
      _.set(payload, 'categoryList', _.get(type, 'categories'));

      result = await this._dataSource
        .one(queryUrl.target, queryUrl.id)
        .all(queryUrl.endpoint)
        .customPOST(payload, 'query', { page, size, sort, order }, {});

      if (hydrate) {
        _.forEach(result, (obj) => {
          this._hydrate({ type: type.slug, assetId, jobId, context },
            obj);
        });
      }
    }

    return result;
  }

  async getChildCategories(categories) {
    const allCategories =
      await this._referenceDataService.get('defect.defectCategories');
    const categoriesSelected = _.map(_.filter(allCategories,
      (cat) => _.includes(categories, cat.id)), 'id');
    _.forEach(allCategories, (cat) => {
      if (cat.parent && _.includes(categoriesSelected, cat.parent.id)) {
        categoriesSelected.push(cat.id);
      }
    });
    return categoriesSelected;
  }

  async _get(type, params, hydrate = true) {
    params.type = type;
    const request = _.parseInt(params.codicilId) ?
      this._core(params).get() :
      this._core(params).getList();
    return hydrate ? this._hydrate(params, await request) : request;
  }

  async _save(type, params, payload, hydrate = false) {
    Reflect.deleteProperty(payload, 'compositeId');
    Reflect.deleteProperty(payload, 'repairs');
    params.type = type;
    const request = this._core(params).customPOST(payload);
    return hydrate ? this._hydrate(params, await request) : request;
  }

  async _remove(type, params) {
    return this._core(_.merge(params, { type })).customDELETE();
  }

  async _update(type, params, payload, hydrate = false) {
    Reflect.deleteProperty(payload, 'compositeId');
    Reflect.deleteProperty(payload, 'repairs');
    const request = this._core(_.merge(params, { type })).customPUT(payload);
    return hydrate ? this._hydrate(request(params, await request)) : request;
  }

  // unhydrated codicil = dry codicil
  async _hydrate({ type, assetId, jobId, context }, dryCodicil) {
    dryCodicil = await this.populateAttributes(dryCodicil);
    const self = this;
    const hydrate = async (codicil) => {
      // hydrates the composite id from various components
      codicil.compositeId = _.join(_.reject([
        _.get(CODICIL_TYPE_CODE_MAP, type),
        _.last(_.get(codicil, 'category.description')),
        _.get(codicil, 'sequenceNumber')
      ], _.isNil), '-');

      // hydrates the defect
      if (_.has(codicil, 'defect.id')) {
        codicil.defect = await self._defectDecisionService.get(
          { assetId, jobId, defectId: codicil.defect.id, context });
      }

      if (!_.isEmpty(codicil.items) &&
        (assetId || _.get(codicil, 'asset.id'))) {
        const items = await this._assetModelDecisionService.queryItem(
          assetId || _.get(codicil, 'asset.id'),
          { itemId: _.map(codicil.items, 'item.id') });
        codicil.items = _.map(codicil.items, (item) =>
          _.set(item, 'item',
            _.merge(
              _.find(items, { id: item.item.id }),
              { isHydrated: true })));
      }

      // hydrates repairs
      if (type === 'coc') {
        codicil.repairs =
          await self.populateAttributes(await self._core({
            assetId,
            jobId,
            codicilId: codicil.id || codicil._id,
            type,
            context
          })
            .all(assetId ? 'repair' : 'wip-repair')
            .getList());
      }

      return codicil;
    };

    return _.isArray(dryCodicil) ?
      this._$q.all(_.map(dryCodicil, hydrate)) :
      hydrate(dryCodicil);
  }

  async getActionableItem(params, hydrate) {
    return this._get('actionable-item', params, hydrate);
  }

  async getAssetNote(params, hydrate) {
    return this._get('asset-note', params, hydrate);
  }

  async getCoC(params, hydrate) {
    return this._get('coc', params, hydrate);
  }

  async removeActionableItem(params, hydrate) {
    return this._remove('actionable-item', params, hydrate);
  }

  async removeAssetNote(params, hydrate) {
    return this._remove('asset-note', params, hydrate);
  }

  async removeCoC(params, hydrate) {
    return this._remove('coc', params, hydrate);
  }

  async saveActionableItem(params, payload, hydrate) {
    return this._save('actionable-item', params, payload, hydrate);
  }

  async saveAssetNote(params, payload, hydrate) {
    return this._save('asset-note', params, payload, hydrate);
  }

  async saveCoC(params, payload, hydrate) {
    return this._save('coc', params, payload, hydrate);
  }

  async saveWipCocForDefect(jobId, defectId, payload) {
    return this._dataSource
      .one('job', jobId)
      .one('wip-defect', defectId)
      .customPOST(payload, 'wip-coc');
  }

  async updateActionableItem(params, payload, hydrate) {
    return this._update('actionable-item', params, payload, hydrate);
  }

  async updateAssetNote(params, payload, hydrate) {
    return this._update('asset-note', params, payload, hydrate);
  }

  async updateCoC(params, payload, hydrate) {
    return this._update('coc', params, payload, hydrate);
  }

  get referenceDataKeyMap() {
    return {
      ...super.referenceDataKeymap,
      category: { referenceKey: 'asset', searchKey: 'codicilCategories' },
      status: { referenceKey: 'service', searchKey: 'codicilStatuses' },
      template: { referenceKey: 'asset', searchKey: 'codicilTemplates' }
    };
  }
}
