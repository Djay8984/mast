import CodicilService from './codicil.service';

export default class CodicilOfflineService extends CodicilService {
  /* @ngInject */
  constructor(
    $q,
    assetModelDecisionService,
    defectDecisionService,
    offlinular,
    referenceDataService
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
