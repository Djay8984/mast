import EmployeeService from './employee.service';

export default class EmployeeOnlineService extends EmployeeService {
  /* @ngInject */
  constructor(
    $log,
    dateFormat,
    moment,
    node,
    referenceDataService,
    Restangular,
    surveyorRole
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._Restangular;
  }
}
