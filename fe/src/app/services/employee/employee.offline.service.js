import EmployeeService from './employee.service';

export default class EmployeeOfflineService extends EmployeeService {
  /* @ngInject */
  constructor(
    $log,
    dateFormat,
    moment,
    node,
    referenceDataService,
    offlinular,
    surveyorRole
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
