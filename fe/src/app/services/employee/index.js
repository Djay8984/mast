import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import employeeOfflineService from './employee.offline.service';
import employeeOnlineService from './employee.online.service';

export default angular
  .module('app.services.employee', [])
  .service('employeeOfflineService', employeeOfflineService)
  .service('employeeOnlineService', employeeOnlineService)
  .service('employeeDecisionService', [
    'employeeOnlineService',
    'employeeOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
