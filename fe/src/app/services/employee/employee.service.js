import * as _ from 'lodash';

import BaseService from 'app/services/base-service.class';

// Groups are defined in lowercase
const CASE_GROUPS = new Set([
  'admin', // This doesn't want to be here forever
  'surveyor',
  'sdo',
  'cfo',
  'mmso',
  'dce-technical',
  'classgroup-mms',
  'classgroup-admin',
  'classgroup-senioradmin',
  'classgroup-technical',
  'eic-admin',
  'eic-senior-admin',
  'eic-technical',
  'mds-systemadmin',
  'mds-superuser'
]);

const JOB_GROUPS = new Set([
  'authorisingsurveyor'
]);

export default class EmployeeService extends BaseService {

  async getCurrentUser() {
    if (!this._currentUser) {
      const user = await this._node.getUser();
      const { groups } = user;
      let id = user.id;
      const userIsDeleted = user.deleted;

      if (_.isNil(id) || userIsDeleted) {
        const employee = await this.getUserFromName(user.fullName);
        if (_.isNil(employee) || userIsDeleted) {
          throw Error(`Invalid user: "${user.fullName}"`);
        }
        id = employee.id;
      }

      // User workItemType is 'cases' if they have both case and job groups
      const userInGroups = (groupSet) => _.some(groups, (g) => groupSet.has(g));
      const caseUser = userInGroups(CASE_GROUPS);
      if (!caseUser && !userInGroups(JOB_GROUPS)) {
        this._$log.warn('Current user has no valid roles');
      }
      const workItemType = caseUser ? 'cases' : 'jobs';
      this._currentUser = _.merge({}, user, { id, workItemType });
    }
    return this._currentUser;
  }

  async get(id) {
    return _.find(
      await this._referenceDataService.get('employee.employee'),
      { id });
  }

  async getUserFromName(name) {
    return _.find(
      await this._referenceDataService.get('employee.employee'),
      { name });
  }

  isUserRBACStatus(user, job, role) {
    const surveyors =
      _.find(job.employees, { lrEmployee: { id: user.id } });
    return surveyors.employeeRole.id === this._surveyorRole.get(role);
  }
}
