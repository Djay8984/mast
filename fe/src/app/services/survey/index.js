import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import surveyOfflineService from './survey.offline.service';
import surveyOnlineService from './survey.online.service';

export default angular
  .module('app.services.survey', [])
  .service('surveyOfflineService', surveyOfflineService)
  .service('surveyOnlineService', surveyOnlineService)
  .service('surveyDecisionService', [
    'surveyOnlineService',
    'surveyOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
