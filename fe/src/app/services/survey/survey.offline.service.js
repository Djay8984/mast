import SurveyService from './survey.service';

export default class SurveyOfflineService extends SurveyService {
  /* @ngInject */
  constructor(
    employeeDecisionService,
    moment,
    offlinular,
    referenceDataService,
    serviceCreditStatus
  ) {
    super(arguments);
  }

  async updateSurveys(jobId, surveys) {
    // The offlinular can't handle ListDTOs properly, so we need to
    // update each survey as if there was a PUT job/{x}/survey/{y} API
    const result = [];
    for (const survey of surveys) {
      result.push(await this._dataSource
        .one('job', jobId)
        .all('survey', survey.id)
        .customPUT(survey));
    }

    return this.populateAttributes(result);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
