import SurveyService from './survey.service';

export default class SurveyOnlineService extends SurveyService {
  /* @ngInject */
  constructor(
    employeeDecisionService,
    moment,
    Restangular,
    referenceDataService,
    serviceCreditStatus
  ) {
    super(arguments);
  }

  async updateSurveys(jobId, surveys) {
    return this.populateAttributes(await this._dataSource
      .one('job', jobId)
      .all('survey')
      .customPUT({ surveys }));
  }

  get _dataSource() {
    return this._Restangular;
  }
}
