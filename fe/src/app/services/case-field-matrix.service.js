import Base from 'app/base.class';
import * as _ from 'lodash';

const CONTRACT_REF = 'contractReferenceNumber';
const PRE_INSPECTION_STATUS = 'preEicInspectionStatus';
const RISK_ASSESSMENT_STATUS = 'riskAssessmentStatus';
const LOSING_SOCIETY = 'losingSociety';
const EST_BUILD_DATE = 'estimatedBuildDate';
const CASE_ACCEPTANCE_DATE = 'caseAcceptanceDate';
const ASSET_NAME = 'assetName';
const IMO_NUMBER = 'imoNumber';
const GROSS_TONNAGE = 'grossTonnage';
const ASSET_TYPE = 'assetType';
const LIFECYCLE_STATUS = 'assetLifecycleStatus';
const CLASS_STATUS = 'classStatus';
const CURRENT_FLAG = 'flagState';
const PROPOSED_FLAG = 'proposedFlagState';
const PORT_OF_REGISTRY = 'registeredPort';
const BUILD_RULE_SET = 'ruleSet';
const CLASS_MAINTENANCE_REGIME = 'maintainenceRegime';
const CO_CLASSIFICATION_SOCIETY = 'coClassificationSociety';
const DATE_OF_BUILD = 'dateOfBuild';
const KEEL_LAYING_DATE = 'keelLayingDate';
const CASE_OFFICE = 'caseOffice';
const CASE_OWNER = 'caseOwner';
const CASE_ADMIN = 'caseAdmin';

export default class CaseFieldMatrix extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    caseType,
  ) {
    super(arguments);

    const defaultFields =
      [
        CONTRACT_REF, CASE_ACCEPTANCE_DATE, ASSET_NAME, IMO_NUMBER,
        GROSS_TONNAGE, ASSET_TYPE, LIFECYCLE_STATUS, PROPOSED_FLAG,
        PORT_OF_REGISTRY, CASE_OFFICE, CASE_OWNER, CASE_ADMIN
      ];

    this.matrix = _.zipObject(
      _.values(
        _.pick(caseType.toObject(),
          ['firstEntry', 'toc', 'aic', 'nonClassed', 'tomc', 'aimc'])),
      [
        _.union(defaultFields, [ EST_BUILD_DATE, CLASS_STATUS, BUILD_RULE_SET,
          CLASS_MAINTENANCE_REGIME, CO_CLASSIFICATION_SOCIETY, DATE_OF_BUILD,
          KEEL_LAYING_DATE ]),
        _.union(defaultFields, [ PRE_INSPECTION_STATUS, RISK_ASSESSMENT_STATUS,
          LOSING_SOCIETY, CLASS_STATUS, CURRENT_FLAG, BUILD_RULE_SET,
          CLASS_MAINTENANCE_REGIME, CO_CLASSIFICATION_SOCIETY, DATE_OF_BUILD,
          KEEL_LAYING_DATE ]),
        _.union(defaultFields, [ CLASS_STATUS, BUILD_RULE_SET,
          CLASS_MAINTENANCE_REGIME, CO_CLASSIFICATION_SOCIETY, DATE_OF_BUILD,
          KEEL_LAYING_DATE ]),
        _.union(defaultFields, [ CURRENT_FLAG, BUILD_RULE_SET, DATE_OF_BUILD,
          KEEL_LAYING_DATE ]),
        _.union(defaultFields, [ LOSING_SOCIETY, CURRENT_FLAG ]),
        defaultFields
      ]);
  }

  /**
   * canDisplayField(fieldName)
   * @param {String} fieldName The field name we want to display or not
   * @param {number} caseType The type of case being viewed
   * @return {Boolean} true or false
   */
  canDisplayField(fieldName, caseType) {
    const type = caseType || this._$stateParams.caseType;
    return _.includes(this.matrix[type], fieldName);
  }
}
