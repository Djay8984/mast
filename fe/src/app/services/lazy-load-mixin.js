import * as _ from 'lodash';

// TODO dry up this and draft items mixin
export default (Base) => class extends Base {
  async lazyLoad(item) {
    if (!item.loaded && !_.isEmpty(item.items)) {
      let ids = item.getItemIds();

      // find all items related to this item
      const related = _.map(_.filter(item.related, {
        type: { id: this._assetItemRelationshipType.get('isRelatedTo') }
      }), 'toItem.id');
      ids = _.concat(ids, related);
      item.items = await this._assetModelDecisionService
        .getItems(this.asset.id, ids);
      item.loaded = true;
    }
  }

  async select(item) {
    await this.lazyLoad(item);

    item.select({ reset: true });
    item.expand();

    item.setParentProps({ childSelected: true });

    if (_.has(this, 'selectedItem')) {
      this.selectedItem = item;
    }
  }

  async expand(item) {
    await this.lazyLoad(item);
    item.expand({ parents: false });
  }
};
