import * as _ from 'lodash';
import Base from 'app/base.class';

export default class PageService extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    $rootScope,
    assetOnlineService
  ) {
    super(arguments);
    this.main = {
      loading: false
    };

    this._$rootScope.$on('$stateChangeSuccess', () => this.resetData());

    this.checkedOut = [];
  }

  setHeader(value) {
    this.header = _.clone(_.get(value, 'data.header'));
    this.data = _.clone(value.data);
  }

  resetData() {
    this.setHeader(this._$state.current);
    this.main.loading = false;
  }

  checkPermissions() {
    const state = this._$state.current.checkedInRedirect;
    if (this._$stateParams.assetId && state && !this.isCheckedOut()) {
      this._$state.go(state);
    }
  }

  /**
   * get list of checked out assets
   * use the same array for new data.
   * Check page permissions
   */
  async getCheckedOut() {
    const res = await this._assetOnlineService.checkedOut();

    this.checkedOut.length = 0;
    if (_.isArray(res)) {
      this.checkedOut.push(...res);
    }

    this.checkPermissions();
  }

  isCheckedOut(asset = this._$stateParams) {
    return !_.isUndefined(asset.checkedOutBy);
  }

  async checkOut(asset) {
    if (!this.isCheckedOut(asset)) {
      await this._assetOnlineService.checkOut(asset);
      return this.getCheckedOut();
    }
  }

  async checkIn(asset) {
    if (this.isCheckedOut(asset)) {
      await this._assetOnlineService.checkIn(asset);
      return this.getCheckedOut();
    }
  }
}
