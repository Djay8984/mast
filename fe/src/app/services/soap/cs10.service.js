import * as _ from 'lodash';
import Base from 'app/base.class';

const AUTHENTICATE_USER =
  'urn:Core.service.livelink.opentext.com/AuthenticateUser';
const CREATE_DOCUMENT =
  'urn:DocMan.service.livelink.opentext.com/CreateDocument';
const GET_VERSION_CONTENTS =
  'urn:DocMan.service.livelink.opentext.com/GetVersionContents';
const DELETE_NODE =
  'urn:DocMan.service.livelink.opentext.com/DeleteNode';

export default class CS10 extends Base {
  /* @ngInject */
  constructor(
    $log,
    $window,
    appConfig,
    node,
    soap
  ) {
    super(arguments);
    this.savedAuthenticationToken = null;
  }

  async authenticate() {
    const args = {
      userName: this._appConfig.MAST_CS10_USERNAME,
      userPassword: this._appConfig.MAST_CS10_PASSWORD
    };

    return this.authenticateUser(args);
  }

  async createDocument(args) {
    return this.standardAction(
      this._appConfig.MAST_CS10_DOC_URL,
      CREATE_DOCUMENT,
      this.createCreateDocumentRequest.bind(this),
      args,
      'data.Envelope.Body.CreateDocumentResponse.CreateDocumentResult.ID'
    );
  }

  async getVersionContents(args) {
    const contents = await this.standardAction(
      this._appConfig.MAST_CS10_DOC_URL,
      GET_VERSION_CONTENTS,
      this.createGetVersionContentsRequest,
      args,
      'data.Envelope.Body.GetVersionContentsResponse.' +
        'GetVersionContentsResult.Contents'
    );

    // atob() throws if it can't decode
    return this._$window.atob(contents);
  }

  async deleteNode(args) {
    return this.standardAction(
      this._appConfig.MAST_CS10_DOC_URL,
      DELETE_NODE,
      this.createDeleteNodeRequest,
      args,
      'data.Envelope.Body.DeleteNodeResponse'
    );
  }

  async standardAction(url, soapAction, createRequest,
             args, responseProperties, retries = 1) {
    const token = await this.authenticate();
    const argsClone = _.clone(args);

    _.defaults(argsClone, { authenticationToken: token });

    const data = createRequest(argsClone);
    let response = null;
    try {
      response = await this._soap.post(url, soapAction, data);
    } catch (e) {
      const faultcode =
        this.getPropertiesAt('data.Envelope.Body.Fault.faultcode')(e);
      if (retries > 0 && _.includes(faultcode, 'LoginFailed')) {
        this.savedAuthenticationToken = null;
        return this.standardAction(url, soapAction, createRequest, args,
            responseProperties, retries - 1);
      }
      this._$log.debug(`Bad response from CS10. Faultcode: ${faultcode}`);
      throw e;
    }

    return this.getPropertiesAt(responseProperties)(response);
  }

  async authenticateUser(args) {
    const url = this._appConfig.MAST_CS10_AUTH_URL;
    const data = this.createAuthenticateUserRequest(args);
    const responseProperties = 'data.Envelope.Body.' +
      'AuthenticateUserResponse.AuthenticateUserResult';

    if (_.isNil(this.savedAuthenticationToken)) {
      const response = await this._soap.post(url, AUTHENTICATE_USER, data);

      this.savedAuthenticationToken =
        this.getPropertiesAt(responseProperties)(response);
    }

    return this.savedAuthenticationToken;
  }

  getPropertiesAt(paths) {
    return _.isUndefined(paths) ? _.constant : doGetProperties;

    function doGetProperties(node) {
      const pathsArray = [].concat(paths);
      const properties = pathsArray.map((path) => {
        const childNode = _.get(node, path);

        return _.has(childNode, 'toString') ?
          childNode.toString() : childNode;
      });

      return _.isArray(paths) ? properties : properties[0];
    }
  }

  createAuthenticateUserRequest(args) {
    const { userName, userPassword } = args;

    return `
      <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:urn="urn:Core.service.livelink.opentext.com">
        <soapenv:Header/>
        <soapenv:Body>
          <urn:AuthenticateUser>
            <!--Optional:-->
            <urn:userName>${userName}</urn:userName>
            <!--Optional:-->
            <urn:userPassword>${userPassword}</urn:userPassword>
          </urn:AuthenticateUser>
        </soapenv:Body>
      </soapenv:Envelope>`;
  }

  createCreateDocumentRequest(args) {
    const { authenticationToken, file, parentId, comment } = args;
    const { name, size, lastModifiedDate } = file;
    const contents = this._$window.btoa(file.contents);

    // file         -> file-{uuid}
    // file.tar     -> file-{uuid}.tar
    // file.tar.gz  -> file-{uuid}.tar.gz
    const makeUniqueName = (filename) =>
      filename.replace(/(?=\.|$)/, `-${this._node.uuid()}`);
    const getCreatedDate = () =>
      (file.birthtime || file.lastModifiedDate).toISOString();

    /* eslint-disable max-len */
    return `
      <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:urn="urn:api.ecm.opentext.com"
        xmlns:urn1="urn:DocMan.service.livelink.opentext.com"
        xmlns:urn2="urn:Core.service.livelink.opentext.com">
        <soapenv:Header>
          <urn:OTAuthentication>
            <!--Optional:-->
            <urn:AuthenticationToken>${authenticationToken}</urn:AuthenticationToken>
          </urn:OTAuthentication>
        </soapenv:Header>
        <soapenv:Body>
           <urn1:CreateDocument>
             <urn1:parentID>${parentId}</urn1:parentID>
             <!--Optional:-->
             <urn1:name>${makeUniqueName(name)}</urn1:name>
             <!--Optional:-->
             <urn1:comment>${comment}</urn1:comment>
             <urn1:advancedVersionControl>
               false
             </urn1:advancedVersionControl>
             <!--Optional:-->
             <urn1:attach>
               <urn2:CreatedDate>
                 ${getCreatedDate(file)}
               </urn2:CreatedDate>
               <!--Optional:-->
               <urn2:FileName>${name}</urn2:FileName>
               <urn2:FileSize>${size}</urn2:FileSize>
               <urn2:ModifiedDate>${lastModifiedDate.toISOString()}</urn2:ModifiedDate>
            <urn2:Contents>${contents}</urn2:Contents>
           </urn1:attach>
          </urn1:CreateDocument>
         </soapenv:Body>
      </soapenv:Envelope>`;

    /* eslint-enable max-len */
  }


  createGetVersionContentsRequest(args) {
    const { authenticationToken, id, version } = args;

    /* eslint-disable max-len */
    return `
      <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:urn="urn:api.ecm.opentext.com"
        xmlns:urn1="urn:DocMan.service.livelink.opentext.com">
        <soapenv:Header>
          <urn:OTAuthentication>
            <!--Optional:-->
            <urn:AuthenticationToken>${authenticationToken}</urn:AuthenticationToken>
          </urn:OTAuthentication>
        </soapenv:Header>
        <soapenv:Body>
          <urn1:GetVersionContents>
            <urn1:ID>${id}</urn1:ID>
            <urn1:versionNum>${version}</urn1:versionNum>
          </urn1:GetVersionContents>
        </soapenv:Body>
      </soapenv:Envelope>`;

    /* eslint-enable max-len */
  }


  createDeleteNodeRequest(args) {
    const { authenticationToken, id } = args;

    /* eslint-disable max-len */
    return `
      <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:urn="urn:api.ecm.opentext.com"
        xmlns:urn1="urn:DocMan.service.livelink.opentext.com">
        <soapenv:Header>
          <urn:OTAuthentication>
            <!--Optional:-->
            <urn:AuthenticationToken>${authenticationToken}</urn:AuthenticationToken>
          </urn:OTAuthentication>
        </soapenv:Header>
        <soapenv:Body>
          <urn1:DeleteNode>
            <urn1:ID>${id}</urn1:ID>
          </urn1:DeleteNode>
        </soapenv:Body>
      </soapenv:Envelope>`;

    /* eslint-enable max-len */
  }
}
