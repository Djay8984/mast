import * as angular from 'angular';
import Soap from './soap.service';
import CS10 from './cs10.service';

export default angular
  .module('app.services.soap', [])
  .service('soap', Soap)
  .service('cs10', CS10)
  .name;
