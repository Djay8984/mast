import * as _ from 'lodash';

import Base from 'app/base.class';
import X2JS from 'x2js';

export default class Soap extends Base {
  /* @ngInject */
  constructor(
    $http
  ) {
    super(arguments);
    const x2js = new X2JS();
    const transformResponse = [].concat($http.defaults.transformResponse,
                                        x2js.xml2js.bind(x2js));

    this._bareRequest = () => ({
      method: 'POST',
      headers: {
        'Content-Type': 'text/xml; charset=utf-8'
      },
      transformResponse
    });
  }

  async post(url, soapAction, data) {
    const request = this._bareRequest();

    _.set(request, 'url', url);
    _.set(request, 'data', data);
    _.set(request.headers, 'SOAPAction', soapAction);

    return this._$http(request);
  }
}
