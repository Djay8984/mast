import * as _ from 'lodash';

export default (Base) => class extends Base {

  async lazyLoad(item) {
    item.loading = true;
    if (!item.loaded && item.items.length) {
      item.items = await this._draftItemService.getDraftItems(this.asset, item);
      item.loaded = true;
    }
    item.loading = false;
  }

  async select(item) {
    await this.lazyLoad(item);

    item.select({ reset: true });
    item.expand({ parents: true });

    item.setParentProps({
      childSelected: true
    });

    if (_.has(this, 'selectedItem')) {
      this.selectedItem = item;
    }
  }

  async expand(item, exp = true) {
    await this.lazyLoad(item);
    if (exp) {
      item.expand({ parents: false });
    }
  }
};
