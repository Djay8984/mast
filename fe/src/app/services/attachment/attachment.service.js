import * as _ from 'lodash';

import * as angular from 'angular';

import ExternalAttributeMixin from 'app/services/external-attribute.class';
import BaseService from 'app/services/base-service.class';

import { pageable } from 'app/decorators';
import Attachment from 'app/models/attachment.model';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class AttachmentService
  extends ExternalAttributeMixin(BaseService) {

  @pageable
  async paginate(
    elements, {
      page = 0,
      size = 12,
      sort = 'creationDate',
      order = 'desc'
    } = {}
  ) {
    const copiedElements = angular.copy(elements);
    const start = page * size;
    const end = start + size;
    const content = _.orderBy(copiedElements, sort, order).slice(start, end);

    // Mimic API
    content.pagination = {
      size,
      page,
      totalElements: copiedElements.length,
      numberOfElements: content.length
    };

    // Mimic this._Restangular
    content.reqParams = content.pagination;
    return this._$q.when(content);
  }

  @pageable
  async getAttachments(
    path, {
      page = 0,
      size = 12,
      sort = 'updatedDate',
      order = 'desc'
    } = {}
  ) {
    const attachments = await this._dataSource
      .one(path)
      .all('attachment')
      .getList({ page, size, sort, order });

    await this.makeAttachmentModels(attachments);

    // TODO: Verify this
    // Currently doesn't populate stuff as desired due to the mixin not making
    // API calls when required yet
    return this.populateAttributes(attachments);
  }

  async makeAttachmentModels(attachments) {
    const extensions = await this._getValidExtensions();
    const confidentialityTypes = await this._getConfidentialityTypes();

    _.forEach(attachments, (a, i) => {
      attachments[i] = new Attachment(a);

      // Set attachment type
      if (!attachments[i].isNote()) {
        _.set(attachments[i],
          'attachmentType.name',
          _.get(
            _.find(extensions,
              { id: _.get(attachments, [i, 'attachmentType', 'id']) }),
            'name'));
      }

      // Set confidentiality type
      _.set(attachments[i],
        'confidentialityType.description',
        _.get(
          _.find(confidentialityTypes, {
            id: _.get(attachments, [i, 'confidentialityType', 'id']) }),
          'description'));
    });
  }

  async getAttachmentCount(path) {
    return _.get(await this.getAttachments(path, { size: 1 }),
      'pagination.totalElements', 0);
  }

  async populateAttachmentMetadata(attachment, file) {
    const author = await this._employeeDecisionService.getCurrentUser();
    attachment.author = {
      name: author.fullName,
      id: author.id,
      firstName: '',
      lastName: ''
    };
    if (!_.isUndefined(file)) {
      attachment.attachmentType = await this._getAttachmentType(file.name);
    }
  }

  async add(path, attachment, file) {
    // The file is added to the attachment when storing in memory
    // and uploaded at a later date e.g. when a form is submitted.
    _.unset(attachment, 'file');
    await this.populateAttachmentMetadata(attachment, file);
    if (!_.isUndefined(file)) {
      attachment.attachmentUrl = await this._uploadFileContents(file);
    }

    return this._uploadMetadata(path, attachment);
  }

  async remove(path, attachmentId, hasFileContent) {
    // TODO: Might want to remove from CS10
    // if (hasFileContent) {
    //   await this._cs10.deleteNode({ id: attachmentId });
    // }
    return this._removeMetadata(path, attachmentId);
  }

  async update(path, attachment) {
    return this._dataSource
      .one(path)
      .one('attachment', attachment.id)
      .customPUT(attachment);
  }

  async addAll(path, attachments) {
    return this._$q.all(
      _.map(attachments, (attachment) =>
        this.add(path, attachment, attachment.file))
    );
  }

  async downloadFileContents(fileMetadata) {
    const path = this._node.showSaveDialog(fileMetadata.title);
    if (_.isUndefined(path)) {
      return;
    }

    const args = {

      // The file ID is stored in the attachmentUrl field of the file metadata.
      // It's weird, I know.
      id: fileMetadata.attachmentUrl,

      // Currently we only use one version of each document.
      version: 1
    };

    const contents = await this._cs10.getVersionContents(args);
    this._node.saveFile(path, contents);
  }


  /* *********************************************** */

  async _getAttachmentType(filename) {
    const validExtensions = await this._getValidExtensions();
    if (_.includes(filename, '.')) {
      const ext = _.toUpper(filename.split('.').pop());
      const attachmentType = _.find(validExtensions, ['name', ext]);
      if (!_.isUndefined(attachmentType)) {
        return attachmentType;
      }
    }

    // This should be the extension '---'. We could hardcode this value.
    return _
        .chain(validExtensions)
        .sortBy('id')
        .last()
        .value();
  }

  async _getValidExtensions() {
    return this._referenceDataService.get(
      'attachment.attachmentTypes', true);
  }

  async _getConfidentialityTypes() {
    return this._referenceDataService.get(
      'attachment.confidentialityTypes', true);
  }

  async _uploadMetadata(path, attachment) {
    return this._dataSource
      .one(path)
      .customPOST(attachment, 'attachment');
  }

  async _readFileContents(file) {
    const deferred = this._$q.defer();
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onloadend = (e) => {
      // TODO: error reading file
      deferred.resolve(reader.result);
    };
    return deferred.promise;
  }

  async _prepareFileForUpload(file) {
    const metadata = await this._node.stat(file);
    _.forEach(metadata, (value, key) => {
      if (key !== 'size') { // already exists as a getter
        Reflect.defineProperty(file, key, {
          get: () => value
        });
      }
    });
    file.contents = await this._readFileContents(file);
  }

  async _uploadFileContents(file) {
    await this._prepareFileForUpload(file);
    const args = {
      parentId: this._appConfig.MAST_CS10_FOLDER_ID,
      comment: '',
      file
    };

    const response = this._cs10.createDocument(args);
    if (_.some(response, { status: -1 })) {
      throw new Error('Bad status response from SOAP request.');
    }
    return response;
  }

  async _removeMetadata(path, attachmentId) {
    return this._dataSource
      .one(path)
      .one('attachment', attachmentId)
      .customDELETE();
  }
}
