import attachmentDecisionService from './attachment.service';

/* eslint new-cap: 0, babel/new-cap: 0 , no-new: 0 */
export default class AttachmentOfflineService
  extends attachmentDecisionService {

  /* @ngInject */
  constructor(
    $log,
    $q,
    appConfig,
    cs10,
    employeeDecisionService,
    node,
    offlinular,
    referenceDataService,
    Restangular
  ) {
    super(arguments);
  }

  get _dataSource() {
    return this._offlinular;
  }
}
