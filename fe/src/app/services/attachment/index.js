import * as angular from 'angular';

import baseDecisionService from 'app/services/base-decision-service.class';

import attachmentOfflineService from './attachment.offline.service';
import attachmentOnlineService from './attachment.online.service';

export default angular
  .module('app.services.attachment', [])
  .service('attachmentOfflineService', attachmentOfflineService)
  .service('attachmentOnlineService', attachmentOnlineService)
  .service('attachmentDecisionService', [
    'attachmentOnlineService',
    'attachmentOfflineService',
    '$rootScope',
    baseDecisionService
  ])
  .name;
