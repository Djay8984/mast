import * as angular from 'angular';
import * as _ from 'lodash';

import assetModel from './asset-model';
import cases from './cases';
import codicilsAndDefects from './codicils-and-defects';
import jobs from './jobs';
import serviceSchedule from './service-schedule';

import routes from './config.routes';

export default angular
  .module('app.asset', [
    assetModel,
    cases,
    codicilsAndDefects,
    jobs,
    serviceSchedule
  ])
  .config(routes)
  .name;

export controller from './asset.controller';
export templateUrl from './asset.html';
export const resolve = {

  /* @ngInject */
  asset: ($stateParams, assetDecisionService) =>
    $stateParams.assetId ?
      assetDecisionService.get($stateParams) : null,

  /* @ngInject */
  ihsAsset: (ihsService, asset) => {
    const imoNumber = _.get(asset, 'ihsAsset.id', null);
    return imoNumber ? ihsService.get(imoNumber) : null;
  }

};
