import HeaderController from './header/asset-header.controller';
import headerTemplateUrl from './header/asset-header.html';

import * as assetPage from './';
import * as assetModel from './asset-model';

// import * as assetModelView from './asset-model/view';
// import * as buildMode from './asset-model/build-mode';
import * as cases from './cases';

// import * as jobs from './jobs';
import * as viewDetails from './asset-details/view';
import * as editDetails from './asset-details/edit';
import * as serviceSchedule from './service-schedule';
import * as codicilsAndDefects from './codicils-and-defects';

/* @ngInject */
export default function config($stateProvider, stateHelper) {
  const routes = [
    {
      name: 'asset',
      abstract: true,
      parent: 'main',
      url: '/asset/{assetId:int}',
      content: assetPage,
      target: '',
      viewsHeader: {
        templateUrl: headerTemplateUrl,
        controller: HeaderController
      },
      header: { open: true, enabled: true, test: 123 },
      params: {
        assetId: null,
        back: null,
        draft: false
      },
      children: [
        {
          name: 'details',
          url: '/details?tab',
          header: { open: false },
          content: viewDetails,
          children: [
            {
              name: 'edit',
              url: '/edit',
              checkedInRedirect: '^',
              content: editDetails
            }
          ]
        },

        // {
        //   name: 'jobs',
        //   url: '/jobs',
        //   abstract: true,
        //   content: jobs,
        //   target: ''
        // },
        {
          name: 'cases',
          url: '/cases',
          abstract: true,
          params: {
            assetId: null,
            caseId: null
          },
          content: cases,
          target: ''
        },
        {
          name: 'assetModel',
          abstract: true,
          url: '/asset-model',
          resolve: assetModel.resolve,
          controller: assetModel.controller,
          templateUrl: assetModel.templateUrl,
          target: '',
          children: [
            {
              name: 'view',
              url: '/view/?view?searchMode?buildMode'
            }
          ]
        },
        {
          name: 'serviceSchedule',
          url: '/service-schedule',
          content: serviceSchedule,
          target: ''
        },
        {
          name: 'codicilsAndDefects',
          url: '/codicils-and-defects',
          header: { open: false, enabled: false },
          params: {
            searchFilter: {},
            defectSearchFilter: {},
            searched: false,
            context: 'asset',
            jobId: null
          },
          content: codicilsAndDefects,
          target: ''
        }
      ]
    }
  ];
  stateHelper.create($stateProvider, routes);
}
