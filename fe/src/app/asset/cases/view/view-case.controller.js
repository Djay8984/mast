import * as _ from 'lodash';
import Base from 'app/base.class';

export default class ViewCasesController extends Base {
  /* @ngInject */
  constructor(
    dateFilterFormat,
    caseStatus,
    asset,
    assetCase,
    assetCustomers
  ) {
    super(arguments);
  }

  get asset() {
    return this._asset;
  }

  get assetCase() {
    return this._assetCase;
  }

  get assetCustomers() {
    return this._assetCustomers;
  }

  get dateFormat() {
    return this._dateFilterFormat;
  }

  get statusId() {
    return _.get(this.assetCase, 'caseStatus.id');
  }

  get isTerminated() {
    return _.isEqual(this.statusId, this._caseStatus.get('closed')) ||
      _.isEqual(this.statusId, this._caseStatus.get('cancelled'));
  }

}
