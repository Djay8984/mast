import Base from 'app/base.class';
import * as _ from 'lodash';

export default class JobsController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $state,
    $stateParams,
    assetCase,
    caseStatus,
    jobs,
    jobStatuses
  ) {
    super(arguments);

    this._jobStatuses = _.get(jobStatuses, 'job.jobStatuses');

    this._validCaseStatuses = _.values(_.pick(this._caseStatus.toObject(),
      ['populate', 'jobPhase', 'validateAndUpdate']));
  }

  viewJob(jobId) {
    this._$state.go('asset.jobs.view',
      _.merge(this._$stateParams,
        { jobId, backTarget: 'asset.cases.view.jobs' }), { reload: true });
  }

  get assetCase() {
    return this._assetCase;
  }

  get assetJobs() {
    const assocJobs =
      _.filter(this._jobs, (job) =>
        job.aCase.id === _.parseInt(this._$stateParams.caseId));

    return assocJobs;
  }

  jobSDO(job) {
    return _.first(job.offices).office.name;
  }

  jobStatusName(job) {
    return _.first(_.filter(this._jobStatuses, { id: job.jobStatus.id })).name;
  }

  canAddJob() {
    return Boolean(_.includes(this._validCaseStatuses,
      this._assetCase.caseStatus.id));
  }
}
