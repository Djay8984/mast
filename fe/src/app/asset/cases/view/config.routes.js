import * as _ from 'lodash';

import CaseDetailsController from './case-details/case-details.controller';
import caseDetailsTemplateUrl from './case-details/case-details.html';

import JobsController from './jobs/jobs.controller';
import jobsTemplateUrl from './jobs/jobs.html';

import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';


/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset.cases.view', {
      url: '/view/{caseId:int}',
      abstract: true,
      params: {
        caseId: null
      },
      resolve: {

        /* @ngInject */
        assetCustomers: (asset, assetOnlineService) =>
          assetOnlineService.getCustomers(asset.id),

        /* @ngInject */
        assetCase: ($stateParams, caseService) =>
          caseService.getCase($stateParams.caseId),

        /* @ngInject */
        assetCaseFlag: (referenceDataService, assetCase) =>
          referenceDataService
            .get('flag.flags')
            .then((flags) => _.find(flags, { id: assetCase.flagState ?
              assetCase.flagState.id : 0 })),

        /* @ngInject */
        attachmentPath: ($stateParams) =>
          `case/${$stateParams.caseId}`,

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          attachmentDecisionService
            .getAttachmentCount(`case/${$stateParams.caseId}`),

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'asset.assetTypes',
            'asset.classStatuses',
            'asset.iacsSocieties',
            'asset.lifecycleStatuses',
            'asset.ruleSets',
            'case.caseTypes',
            'case.riskAssessmentStatuses',
            'party.partyRoles',
            'party.partyFunctions',
            'flag'
          ])
      }
    })
    .state('asset.cases.view.details', {
      url: '/details/{caseId:int}',
      views: {
        '@main': {
          controller: CaseDetailsController,
          controllerAs: 'vm',
          templateUrl: caseDetailsTemplateUrl
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      }
    })
    .state('asset.cases.view.jobs', {
      url: '/jobs/{caseId:int}',
      views: {
        '@main': {
          controller: JobsController,
          controllerAs: 'vm',
          templateUrl: jobsTemplateUrl
        }
      },
      resolve: {

        /* @ngInject */
        jobs: (jobDecisionService, $stateParams) =>
          jobDecisionService.get($stateParams.jobId),

        /* @ngInject */
        jobStatuses: (referenceDataService) =>
          referenceDataService.get(['job.jobStatuses'])
      }
    });
}
