import * as _ from 'lodash';
import Base from 'app/base.class';
import Asset from 'app/models/asset.model';

import './partials/case-details.html';
import './partials/asset-details.html';
import './partials/flag-details.html';
import './partials/rule-set.html';
import './partials/class-maintenance.html';
import './partials/key-dates.html';
import './partials/offices-and-surveyors.html';
import './partials/customers.html';

export default class CaseDetailsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    dateFormat,
    assetCaseFlag,
    asset,
    assetCase,
    assetCustomers,
    attachmentPath,
    attachmentCount,
    caseStatus,
    caseService,
    caseCustomerService,
    officeRole,
    preInspectionStatus,
    referenceData,
    surveyorRole
  ) {
    super(arguments);
    this.assetModel = Reflect.construct(Asset, [asset]);

    if (_.find(_.values(_.pick(this._caseStatus.toObject(),
      ['jobPhase', 'onHold', 'populate', 'validateAndUpdate'])),
      { id: this._assetCase.caseStatus.id })) {
      this._$state.go('^.milestones', this._$stateParams);
    }

    _.forEach(this._assetCase.customers, (customer) =>
      this._caseCustomerService.addCustomer(customer));
  }

  get asset() {
    return this.assetModel;
  }

  get assetCase() {
    return this._assetCase;
  }

  get assetCustomers() {
    return this._assetCustomers;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  get statusId() {
    return _.get(this.assetCase, 'caseStatus.id');
  }

  get attachmentCount() {
    return this._attachmentCount;
  }

  get attachmentPath() {
    return this._attachmentPath;
  }

  get allFlags() {
    return _.get(this._referenceData, 'flag.flags');
  }

  get allPorts() {
    return _.get(this._referenceData, 'flag.portsOfRegistry');
  }

  get currentFlag() {
    return !_.isNull(this._assetCase.flagState) ?
      _.find(this.allFlags, { 'id': this._assetCase.flagState.id }).name : '';
  }

  get customerFunctions() {
    return _.get(this._referenceData, 'party.partyFunctions');
  }

  get proposedFlag() {
    return _.get(_.find(this.allFlags,
        { id: _.get(this, '_assetCase.proposedFlagState.id') }), 'name', '');
  }

  get registeredPort() {
    return _.get(_.find(this.allPorts,
        { id: _.get(this, '_assetCase.registeredPort.id') }), 'name', '');
  }

  get losingSociety() {
    return _.get(_.find(this._referenceData.asset.iacsSocieties,
        { id: _.get(this, '_assetCase.losingSociety.id') }), 'name', '');
  }

  get lifecycleStatus() {
    return _.get(_.find(this._referenceData.asset.lifecycleStatuses,
        { id: _.get(this, '_assetCase.assetLifecycleStatus.id') }), 'name', '');
  }

  get classStatus() {
    return _.get(_.find(this._referenceData.asset.classStatuses,
        { id: _.get(this, '_assetCase.classStatus.id') }), 'name', '');
  }

  get assetType() {
    return _.get(_.find(this._referenceData.asset.assetTypes,
        { id: _.get(this, '_assetCase.assetType.id') }), 'name', '');
  }

  get ruleSet() {
    return _.get(_.find(this._referenceData.asset.ruleSets,
        { id: _.get(this, '_assetCase.ruleSet.id') }), 'name', '');
  }

  get coClassificationSociety() {
    return _.get(_.find(this._referenceData.asset.iacsSocieties,
        { id: _.get(this, '_assetCase.coClassificationSociety.id') }),
        'name', '');
  }

  customerRelationship(id) {
    return _.find(_.get(this._referenceData, 'party.partyRoles'),
      { id }).name;
  }

  customerFunctionListNames(customer) {
    const ourFunctions =
      _.map(this._caseCustomerService.customerFunctionList(customer),
        'customerFunction.id');
    const names =
      _.reduce(this.customerFunctions, (result, item) => {
        if (_.includes(ourFunctions, item.id)) {
          result.push(item.name);
        }
        return result;
      }, []);
    return names.join(', ');
  }

  preInspectionChecks() {
    const checks = this._preInspectionStatus.toObject();
    if (!_.isNull(this.assetCase.preEicInspectionStatus)) {
      const idIndex = _.findIndex(_.values(checks),
        (id) => id === this.assetCase.preEicInspectionStatus.id);
      return _.capitalize(_.keys(checks)[idIndex]);
    }
  }

  get canEditCase() {
    const statuses = _.pick(this._caseStatus.toObject(), ['uncommitted',
      'populate', 'jobPhase', 'validateAndUpdate', 'onHold']);

    return _.includes(_.values(statuses), this.statusId);
  }

  convertWord(text) {
    return text.replace(/([A-Z])/g, ' $1').toLowerCase();
  }

  get caseStatusName() {
    const idIndex =
      _.findIndex(_.values(this._caseStatus.toObject()), (index) =>
        index === this.assetCase.caseStatus.id);
    return this.convertWord(_.keys(this._caseStatus.toObject())[idIndex]);
  }

  caseOffice(office) {
    const item = _.filter(this.assetCase.offices, (location) =>
      location.officeRole.id === this._officeRole.get(office));

    if (item && !_.isEmpty(item)) {
      return _.startCase(_.first(item).office.name);
    }
  }

  caseSurveyor(surveyor) {
    const item = _.filter(this.assetCase.surveyors, (role) =>
      role.employeeRole.id === this._surveyorRole.get(surveyor));

    if (item && !_.isEmpty(item)) {
      return _.startCase(_.first(item).surveyor.name);
    }
  }
}
