import * as _ from 'lodash';
import Base from 'app/base.class';

import markCompleteDialog from './mark-as-complete-dialog.html';

export default class MilestonesController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    caseService,
    MastModalFactory,
    milestoneService,
    assetCase,
    attachmentPath,
    attachmentCount,
    caseStatus,
    milestones
  ) {
    super(arguments);
    this.committedIds =
      [ this._caseStatus.get('populate'),
        this._caseStatus.get('jobPhase'),
        this._caseStatus.get('validateAndUpdate')];
  }

  get assetCase() {
    return this._assetCase;
  }

  /**
   * @return {String} Milestones The milestones;
   */
  get milestones() {
    return this._milestones.caseMilestoneList;
  }

  set milestones(milestones) {
    this._milestones.caseMilestoneList = milestones;
  }

  get attachmentCount() {
    return this._attachmentCount;
  }

  get attachmentPath() {
    return this._attachmentPath;
  }

  get isCommitted() {
    return _.includes(this.committedIds, _.get(this._assetCase,
      'caseStatus.id'));
  }

  get isOnHold() {
    return this._caseStatus.get('onHold') === _.get(this._assetCase,
        'caseStatus.id');
  }

  get hideStickyFooter() {
    return _.includes(
      [this._caseStatus.get('cancelled'),
        this._caseStatus.get('closed')],
      _.get(this._assetCase, 'caseStatus.id'));
  }

  selectedMilestones() {
    return _.filter(this.milestones, 'reviewed');
  }

  async completeMilestones() {
    const reviewed = this.selectedMilestones();

    if (_.size(reviewed)) {
      const dialogResponse = await new this._MastModalFactory({
        templateUrl: markCompleteDialog,
        scope: {
          completeMilestoneText:
            `Complete ${reviewed.length} selected milestones`
        },
        class: 'dialog'
      }).activate();

      if (dialogResponse) {
        await this._milestoneService.markAsComplete(this.assetCase.id,
          reviewed);
        this._$state.reload();
      }
    }
  }

  isCaseUncommitted() {
    return _.find(_.values(_.pick(this._caseStatus.toObject(),
      ['uncommitted'])), { id: this._assetCase.caseStatus.id });
  }
}
