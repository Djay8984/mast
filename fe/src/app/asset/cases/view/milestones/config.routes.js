import MilestonesController from './milestones.controller';
import milestonesTemplateUrl from './milestones.html';

import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

import ManageMilestonesController from
  './manage-milestones/manage-milestones.controller.js';
import manageMilestonesTemplateUrl from
  './manage-milestones/manage-milestones.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset.cases.view.milestones', {
      url: '/milestones/{caseId:int}',
      views: {
        '@main': {
          controller: MilestonesController,
          controllerAs: 'vm',
          templateUrl: milestonesTemplateUrl
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      },
      resolve: {

        /* @ngInject */
        milestones: ($stateParams, milestoneService) =>
          milestoneService.getMilestones($stateParams.caseId)
      }
    })
    .state('asset.cases.view.manage-milestones', {
      url: '/manage-milestones/{caseId:int}',
      views: {
        '@main': {
          controller: ManageMilestonesController,
          controllerAs: 'vm',
          templateUrl: manageMilestonesTemplateUrl
        }
      },
      resolve: {

        /* @ngInject */
        milestones: ($stateParams, milestoneService) =>
          milestoneService.getMilestones($stateParams.caseId)
      }
    });
}
