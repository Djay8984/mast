import * as angular from 'angular';
import routes from './config.routes.js';

export default angular
  .module('app.asset.cases.view.milestones', [])
  .config(routes)
  .name;
