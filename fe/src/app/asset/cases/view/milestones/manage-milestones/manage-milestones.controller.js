import * as _ from 'lodash';
import Base from 'app/base.class';

import inscopeDialog from './partials/inscope-dialog.html';
import outscopeDialog from './partials/outscope-dialog.html';

export default class ManageMilestonesController extends Base {
  /* @ngInject */
  constructor(
    $state,
    MastModalFactory,
    milestoneService,
    assetCase,
    attachmentCount,
    attachmentPath,
    milestones
  ) {
    super(arguments);
    this.inScope = true;
    this.isBannerOpen = true;
  }

  get assetCase() {
    return this._assetCase;
  }

  get milestones() {
    return this._milestones.caseMilestoneList;
  }

  set milestones(milestones) {
    this._milestones.caseMilestoneList = milestones;
  }

  get attachmentCount() {
    return this._attachmentCount;
  }

  get attachmentPath() {
    return this._attachmentPath;
  }

  get inScopeMilestones() {
    return _.filter(this.milestones, 'inScope');
  }

  get outScopeMilestones() {
    return _.filter(this.milestones, ['inScope', false]);
  }

  async changeScope(toInScope, inScope) {
    let successorsList = null;
    let dependentList = null;
    let reviewed =
      _.filter(this.milestones, { 'reviewed': true, inScope });

    if (inScope) {
      // to out of scope
      successorsList = this.getSuccessors(reviewed, []);
      reviewed = _.concat(reviewed, successorsList);
    } else {
      // into scope
      dependentList = this.getDependents(reviewed, []);
      reviewed = _.concat(reviewed, dependentList);
    }

    // build string of milestone scope changed milestone names for modal
    const milestoneList = _.reduce(this.milestones, (result, milestone) => {
      if (milestone.reviewed && milestone.inScope === inScope) {
        result = !_.isEmpty(result) ?
          `${result}, '${milestone.milestone.description}'` :
          `'${milestone.milestone.description}'`;
      }
      return result;
    }, '');

    if (!_.isEmpty(reviewed)) {
      let modalChangeScope = new this._MastModalFactory({
        templateUrl: outscopeDialog,
        scope: {
          milestoneList,
          successorsList
        },
        class: 'dialog'
      });

      if (toInScope) {
        modalChangeScope = new this._MastModalFactory({
          templateUrl: inscopeDialog,
          scope: {
            milestoneList,
            dependentList
          },
          class: 'dialog'
        });
      }

      const dialogResponse =
        !_.isEmpty(successorsList) || !_.isEmpty(dependentList) ?
          await modalChangeScope.activate() : true;

      if (dialogResponse) {
        _.forEach(reviewed, (milestone) => {
          milestone.inScope = toInScope;
          milestone.reviewed = false;
        });
      }
    }
  }

  async save() {
    try {
      await this._milestoneService.saveScope(this.assetCase.id,
        this.milestones);
      this._$state.go('asset.cases.view.milestones');
    } catch (e) {
      // TODO modal error
    }
  }

  onInScope() {
    this.inScope = true;
  }

  onOutScope() {
    this.inScope = false;
  }

  /*
   Recursively looks for and brings back an array of milestone objects that
   are successors of the passed in parent
   @params {array} parents - array of parent obj
   @params {array} successors - array of successor obj
   @params {boolean} head - top of tree
   @return {array} list - list of successors
   */
  getSuccessors(parents, successors, head = true) {
    const list = successors;
    _.forEach(parents, (parent) => {
      // only if parent is not the milestone from the head of the dependency,
      // is not mandatory, is not already completed tree and is not
      // already out of scope and not already in the successor list
      // do you need to add to successor list
      if (!head && !parent.milestone.mandatory && !parent.completionDate &&
        parent.inScope && !_.find(successors, ['id', parent.id])) {
        list.push(parent);
      }
      const children = _.reduce(parent.milestone.childMilestones,
        (result, child) => {
          const childObject = _.find(this.milestones,
            ['milestone.id', child.id]);
          if (!_.isUndefined(childObject)) {
            result.push(childObject);
          }
          return result;
        }, []);

      if (!_.isEmpty(children)) {
        this.getSuccessors(children, list, false);
      }
    });
    return list;
  }

  /*
   Recursively looks for and brings back an array of milestone objects that are
   dependencies of the passed in child
   @params {array} children - array of child obj
   @params {array} dependents - array of dependent obj
   @params {boolean} head - top of tree
   @return {array} list - list of dependents
   */
  getDependents(children, dependents, head = true) {
    const list = dependents;
    _.forEach(children, (child) => {
      // only if parent is not the milestone from
      // the head of the dependency, not already in
      // scope and not already in the successor list
      // do you add to successor list
      if (!head && !child.inScope && !_.find(dependents, ['id', child.id])) {
        list.push(child);
      }

      if (!_.isNull(child.milestone.predecessorMilestones)) {
        const parents =
          _.reduce(child.milestone.predecessorMilestones,
            (result, parent) => {
              const parentObject = _.find(this.milestones,
                ['milestone.id', parent.id]);
              if (!_.isUndefined(parentObject)) {
                result.push(parentObject);
              }
              return result;
            }, []);

        if (parents) {
          this.getDependents(parents, list, false);
        }
      }
    });
    return list;
  }

  showBanner() {
    this.isBannerOpen = !this.isBannerOpen;
  }
}
