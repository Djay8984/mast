import * as angular from 'angular';
import milestones from './milestones';
import routes from './config.routes';

export default angular
  .module('app.asset.cases.view', [milestones])
  .config(routes)
  .name;
