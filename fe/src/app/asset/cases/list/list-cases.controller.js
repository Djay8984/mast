import Base from 'app/base.class';

export default class ListCasesController extends Base {
  /* @ngInject */
  constructor(asset, cases) {
    super(arguments);
  }

  get cases() {
    return this._cases;
  }
}
