import * as _ from 'lodash';

import * as angular from 'angular';

import Base from 'app/base.class';

import './partials/case-details.html';
import './partials/asset-details.html';
import './partials/flag-details.html';
import './partials/rule-set.html';
import './partials/class-maintenance.html';
import './partials/offices-surveyors.html';
import './partials/key-dates.html';
import './partials/customers.html';

import selectAssetTypesDialog from
  '../add/details/select-asset-types-dialog.html';
import SelectAssetTypesController from
  '../add/details/select-asset-types.controller';

import selectRelationshipDialog from
  '../add/details/select-relationship-dialog';

import SelectRelationshipController from
  '../add/details/select-relationship.controller';

import buildRuleSetDialog from '../add/details/build-rule-set-dialog';
import BuildRuleSetController from '../add/details/build-rule-set.controller';

import selectOfficeTypesDialog from
  '../add/details/select-office-types-dialog';
import SelectOfficeTypesController from
  '../add/details/select-office-types.controller';

import selectFunctionsDialog from
  '../add/details/select-functions-dialog';
import SelectFunctionsController from
  '../add/details/select-functions.controller';

const ASSET_LEVEL_INDICATOR = 5;

export default class EditCaseController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $state,
    moment,
    MastModalFactory,
    assetCase,
    attachmentPath,
    attachmentCount,
    caseCustomerService,
    caseFieldMatrix,
    caseService,
    caseStatus,
    classMaintenanceStatus,
    contractReferenceLength,
    dateFormat,
    navigationService,
    officeRole,
    referenceData,
    surveyorRole,
    typeaheadService
  ) {
    super(arguments);
    this._caseCustomerService.setCustomers(this._assetCase.customers);
    this.model = _.assign(this.model, this._assetCase);

    this.offices =
    {
      caseOffice: { officeRole: { id: this._officeRole.get('sdo') } },
      caseCfo: { officeRole: { id: this._officeRole.get('cfo') } },
      cfo3: { officeRole: { id: this._officeRole.get('cfo3') } },
      mmsSdo: { officeRole: { id: this._officeRole.get('mmsSdo') } },
      mmso: { officeRole: { id: this._officeRole.get('mmso') } },
      tso: { officeRole: { id: this._officeRole.get('tso') } },
      tsoStat: { officeRole: { id: this._officeRole.get('tsoStat') } },
      classGroup: { officeRole: { id: this._officeRole.get('classGroup') } },
      dce: { officeRole: { id: this._officeRole.get('dce') } },
      eic: { officeRole: { id: this._officeRole.get('eic') } },
      mds: { officeRole: { id: this._officeRole.get('mds') } },
      tsdo: { officeRole: { id: this._officeRole.get('tsdo') } },
      tssao: { officeRole: { id: this._officeRole.get('tssao') } },
      caseSdo: { officeRole: { id: this._officeRole.get('caseSdo') } },
      jobSdo: { officeRole: { id: this._officeRole.get('jobSdo') } }
    };

    this.surveyors =
    {
      caseOwner:
        { employeeRole: { id: this._surveyorRole.get('management') } },
      caseAdmin:
        { employeeRole: { id: this._surveyorRole.get('eicAdmin') } },
      contractHolder:
        { employeeRole: { id: this._surveyorRole.get('salesCRM') } },
      leadSurveyor:
        { employeeRole: { id: this._surveyorRole.get('leadSurveyor') } }
    };

    this.losingSocietyTypeahead = this._typeaheadService.query(
      this.assetSocieties);
    this.surveyorTypeahead = this._typeaheadService.query(
      this.employeeEmployees);
    this.flagTypeahead = this._typeaheadService.query(this.allFlags);
    this.portTypeahead = this._typeaheadService.query(this.allPorts);
    this.customerTypeahead = this._typeaheadService.query(
      this.staticCustomerList, this.customerPreFilter.bind(this));
    this.officeTypeahead = this._typeaheadService.query(
      this.employeeOffices, this.officePreFilter.bind(this));

    this.setupData();
  }

  setupData() {
    _.forEach(this.offices, (office) => {
      const match = _.find(this._assetCase.offices, ['officeRole.id',
        office.officeRole.id]);

      if (!_.isUndefined(match)) {
        _.assign(office, _.pick(match, ['office']));
      }
    });

    _.forEach(this.surveyors, (surveyor) => {
      _.assign(surveyor, _.find(this._assetCase.surveyors, ['employeeRole.id',
        surveyor.employeeRole.id]));
    });

    if (!_.isNull(this._assetCase.assetType)) {
      this.model.assetType.name = this.assetType;
    }
    this.model.registeredPort = this.registeredPort;
    this.model.proposedFlagState = this.proposedFlag;
    if (!_.isNull(this._assetCase.ruleSet)) {
      this.model.ruleSet.name = this.ruleSet;
    }
    this.model.coClassificationSociety = this.coClassificationSociety;

    // Correctly assign the data for the customers' & functions' names;
    _.forEach(this.model.customers, (customer) => {
      customer.relationship.name = this.customerRelationshipName(customer);
      _.forEach(customer.functions, (func) => {
        _.assign(func.customerFunction,
          _.pick(_.find(this.functionTypes, { id: func.customerFunction.id }),
            ['id', '_id', 'name']));
      });
    });

    this._displayedOfficeTypes =
      _.values(_.pick(this._officeRole.toObject(),
        ['sdo', 'cfo', 'jobSdo', 'tso', 'tsoStat', 'mmso']));

    this.model = this._caseService.momentiseDates(this.model);
  }

  get contractReferenceLength() {
    return this._contractReferenceLength;
  }

  get assetCase() {
    return this._assetCase;
  }

  get allFlags() {
    return _.get(this._referenceData, 'flag.flags');
  }

  get allPorts() {
    return _.get(this._referenceData, 'flag.portsOfRegistry');
  }

  get functionTypes() {
    return _.get(this._referenceData, 'party.partyFunctions');
  }

  get coClassificationSociety() {
    return !_.isNull(this._assetCase.coClassificationSociety) ?
      _.find(_.get(this._referenceData, 'asset.iacsSocieties'),
        { 'id': this._assetCase.coClassificationSociety.id }) : null;
  }

  get ruleSet() {
    return !_.isNull(this._assetCase.ruleSet) ?
      _.find(this.ruleSets, { 'id': this._assetCase.ruleSet.id }).name : '';
  }

  get assetType() {
    return !_.isNull(this._assetCase.assetType) ?
      _.find(this.assetTypes, { 'id': this._assetCase.assetType.id }).name : '';
  }

  get caseType() {
    return _.find(_.get(this._referenceData, 'case.caseTypes'),
      { 'id': this._assetCase.caseType.id }).name;
  }

  get currentFlag() {
    return !_.isNull(this._assetCase.flagState) ?
      _.find(this.allFlags, { 'id': this._assetCase.flagState.id }).name : '';
  }

  get proposedFlag() {
    return !_.isNull(this._assetCase.proposedFlagState) ?
      _.find(this.allFlags,
        { 'id': this._assetCase.proposedFlagState.id }) : '';
  }

  get registeredPort() {
    return !_.isNull(this._assetCase.registeredPort) ?
      _.find(this.allPorts,
        { 'id': this._assetCase.registeredPort.id }) : '';
  }

  customerRelationshipName(customer) {
    return _.find(this.relationshipTypes,
      [ 'id', customer.relationship.id ]).name;
  }

  /**
   * @summary isFieldDisabled()
   * @description Looks for uncommited, populate, job phase or validate and
   *   update statuses, so fields can be disabled in the template;
   * @return {Boolean} true or false
   * @since 01/06/2016
   */
  get isFieldDisabled() {
    return true;
  }

  canDisplayField(field) {
    return this._caseFieldMatrix.canDisplayField(field,
        _.get(this.model, 'caseType.id'));
  }

  /**
   * @return {Array} riskAssessmentTypes The types of risk assessments
   */
  get riskAssessmentTypes() {
    return _.get(this._referenceData, 'case.riskAssessmentStatuses');
  }

  /**
   * @return {Array} preEicInspectionTypes The types of pre EIC Inspections
   */
  get preEicInspectionTypes() {
    return _.get(this._referenceData, 'case.preEicInspectionStatuses');
  }

  /**
   * @return {Array} lifecycleStatuses Returns the resolved lifecycle statuses
   */
  get lifecycleStatuses() {
    return _.get(this._referenceData, 'asset.lifecycleStatuses');
  }

  /**
   * @return {Array} classStatuses Returns the resolved class statuses
   */
  get classStatuses() {
    return _.get(this._referenceData, 'asset.classStatuses');
  }

  /**
   * @return {Array} classMaintenanceStatuses Returns the resolved class
   * maintenance statuses
   */
  get classMaintenanceStatuses() {
    return _.get(this._referenceData, 'asset.classMaintenanceStatuses');
  }

  /**
   * @return {Array} Asset Returns the asset types array
   */
  get assetTypes() {
    return _.get(this._referenceData, 'asset.assetTypes');
  }

  /**
   * @return {Array} societies Returns the societies from reference data
   */
  get assetSocieties() {
    return _.get(this._referenceData, 'asset.iacsSocieties');
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get ruleSets() {
    return _.get(this._referenceData, 'asset.ruleSets');
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get relationshipTypes() {
    return _.get(this._referenceData, 'party.partyRoles');
  }

  /**
   * @return {Array} employees Returns the employees from reference data
   */
  get employeeEmployees() {
    return _.get(this._referenceData, 'employee.employee');
  }

  /**
   * @return {Array} offices Returns the offices roles from reference data
   */
  get employeeOffices() {
    return _.get(this._referenceData, 'employee.office');
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get originalOfficeTypes() {
    return _.get(this._referenceData, 'case.officeRoles');
  }

  /**
   * @return {Boolean} true or false Depends on the form maintainence regime
   */
  get isDualMaintenance() {
    return !_.isUndefined(this.editCaseForm.maintainenceRegime) &&
      !_.isUndefined(this.editCaseForm.maintainenceRegime.$modelValue) &&
      this._classMaintenanceStatus.get('dual') ===
      _.toInteger(this.editCaseForm.maintainenceRegime.$modelValue);
  }

  /**
   * Opens the modal to display the asset types
   */
  async selectAssetTypes() {
    const response = await this._MastModalFactory({
      controller: SelectAssetTypesController,
      controllerAs: 'vm',
      templateUrl: selectAssetTypesDialog,
      inject: {
        selection: this.assetTypes
      },
      class: 'large dialog'
    }).activate();

    this.model.assetType =
      _.find(response, { 'levelIndication': ASSET_LEVEL_INDICATOR });
  }

  /**
   * @summary save(flag)
   * @param {Boolean} flag True or not
   */
  async save(flag = false) {
    const caseStatuses = _.values(_.pick(this._caseStatus.toObject(),
      ['onHold', 'closed', 'cancelled', 'deleted', 'jobPhase',
      'validateAndUpdate', 'takeOffHold']));

    this.model.offices = _.reduce(this.offices, (result, office) => {
      if (_.has(office, 'office') && !_.isNull(office.office)) {
        result.push(office);
      }
      return result;
    }, []);

    this.model.surveyors = _.reduce(this.surveyors, (result, surveyor) => {
      if (_.has(surveyor, 'surveyor') && !_.isNull(surveyor.surveyor)) {
        result.push(surveyor);
      }
      return result;
    }, []);

    _.unset(this, 'model.customers');
    _.set(this, 'model.customers', this.caseCustomers);

    try {
      // The save flag indicates we want to save in the populated state. But we
      // also want to ensure that the case isn't closed, or active;
      if (flag &&
        !_.includes(caseStatuses, this.model.caseStatus.id)) {
        this.model.caseStatus = this._caseStatus.get('populate');
      }
      await this._caseService.updateCase(angular.copy(this.model));
      this._$state.go('asset.cases.view.details',
        _.merge(this._$stateParams, { caseId: this.model.id }),
        { reload: true });
    } catch (e) {
      this._$log.error(e);
    }
  }

  /**
   * @summary staticCustomerList()
   * @return {Object} customers List of customers from reference data
   */
  get staticCustomerList() {
    return _.get(this._referenceData, 'party.party');
  }

  /**
   * @param {String} query what to search for
   * @param {Object} customer The customer we're dealing with
   * @return {Array} Customer name
   */
  findCustomer(query, customer) {
    const pattern = new RegExp(query, 'i');
    let searchList = this.staticCustomerList;

    // We need the customers other than the one we're typing in;
    const otherCustomers =
      _.filter(angular.copy(this.caseCustomers),
        { relationship: { name: customer.customer.relationship.name } });

    if (!_.isEmpty(otherCustomers)) {
      searchList =
        _.reject(searchList, { name: _.first(otherCustomers).customer.name });
    }

    return _.filter(searchList, (thisCustomer) =>
      pattern.test(thisCustomer.name));
  }

  /**
   * @param {object} customer The customer we have the typeahead on
   * @return {function} filtering customers removing those with the same
   *   relationship type to prevent duplicates
   */
  customerPreFilter(customer) {
    // getting customers with the same relationship id as this on the case
    // & filtering out itself
    const sameRelationshipIds =
      _.chain(this.model.customers)
        .filter({ relationship: { id: customer.relationship.id } })
        .map('customer.id')
        .reject(_.isNil)
        .value();

    return _.isNil(customer) ?
      (datum) => true :
      (datum) => !_.includes(sameRelationshipIds, datum.id);
  }

  /*
   * @param {string|number} name optional office role name or id
   * @return {function} filtering based on office role
   */
  officePreFilter(name) {
    const id = _.isString(name) ?
      this._officeRole.get(name) :
      name;
    return _.isNil(id) ?
      (datum) => true :
      (datum) => _.get(datum, 'officeRole.id') === id;
  }

  /**
   * Opens the modal to choose office types to add
   */
  async selectOfficeType() {
    this._officeTypes =
      _.filter(this.originalOfficeTypes,
        (office) => !_.includes(this._displayedOfficeTypes, office.id) &&
        !_.includes(_.map(this.extraOffices, 'id'), office.id));

    const response = await this._MastModalFactory({
      controller: SelectOfficeTypesController,
      controllerAs: 'vm',
      inject: {
        officeTypes: this._officeTypes
      },
      templateUrl: selectOfficeTypesDialog,
      class: 'small dialog'
    }).activate();

    this._extraOffices = this._extraOffices || [];

    this._extraOffices = _.concat(this._extraOffices, response);

    _.forEach(this._extraOffices, (office) => {
      _.set(office, 'nickname', _.lowerFirst(_.camelCase(office.name)));
    });

    this._officeTypes =
      _.filter(this._officeTypes,
        (office) => !_.includes(_.map(this._extraOffices, 'id'), office.id));
  }

  get extraOffices() {
    if (!this._extraOffices) {
      this._extraOffices =
        _.filter(this.originalOfficeTypes,
          { 'id': this._displayedOfficeTypes });
    }
    return this._extraOffices;
  }

  /**
   * @summary selectRelationship()
   * Opens the modal to choose a customer
   */
  async selectRelationship() {
    const response = await this._MastModalFactory({
      controller: SelectRelationshipController,
      controllerAs: 'vm',
      inject: {
        relationshipTypes: this.relationshipTypes,
        currentCustomers: this._caseCustomerService.customerList()
      },
      templateUrl: selectRelationshipDialog,
      class: 'large dialog'
    }).activate();

    this._caseCustomerService.addCustomer({
      customer: { name: null },
      relationship: { name: response.name, id: response.id }
    });
  }

  /**
   * caseCustomerFunctions()
   * @param {Object} customer The customer we're dealing with
   * @return {Array} functions The function list of a given customer
   */
  caseCustomerFunctions(customer) {
    return this._caseCustomerService.customerFunctionList(customer);
  }

  /**
   * @param {Object} customer The customer section on the add case form
   * Removes the customer from the customers
   */
  removeCustomer(customer) {
    this._caseCustomerService.removeCustomer(customer);
  }

  /**
   * @param {Object} customer The customer we're dealing with
   * @param {Object} item The element calling this function
   * Removes this button from the selected functions
   */
  removeCustomerFunction(customer, item) {
    this._caseCustomerService.removeCustomerFunction(customer, item);
  }

  setCustomerName(customer, name) {
    this._caseCustomerService.addCustomerName(customer, name);
  }

  /**
   * caseCustomers()
   * @return {Array} customerList An array of customer objects
   */
  get caseCustomers() {
    return this._caseCustomerService.customerList();
  }

  /**
   * Opens the modal to select a rule set
   */
  async buildRuleSet() {
    this.model.ruleSet = await this._MastModalFactory({
      controller: BuildRuleSetController,
      controllerAs: 'vm',
      inject: {
        ruleSets: this.ruleSets
      },
      templateUrl: buildRuleSetDialog,
      class: 'large dialog'
    }).activate();
  }

  /**
   * @param {Object} customer The object that the click comes from
   * Opens the modal to choose customer functions
   */
  async selectCustomerFunctions(customer) {
    const response = await this._MastModalFactory({
      controller: SelectFunctionsController,
      controllerAs: 'vm',
      inject: {
        functionTypes: await this._caseCustomerService.functionTypes(customer)
      },
      templateUrl: selectFunctionsDialog,
      class: 'large dialog'
    }).activate();

    // Creating the customerFunction object;
    _.forEach(response, (item) => {
      item.customerFunction =
        _.pick(item, ['_id', 'id', 'name', 'description']);
      Reflect.deleteProperty(item, 'description');
      Reflect.deleteProperty(item, 'name');
      Reflect.deleteProperty(item, 'id');
      Reflect.deleteProperty(item, '_id');
      Reflect.deleteProperty(item, 'deleted');
    });

    this._caseCustomerService.addCustomerFunction(customer, response);
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false Depends on the form item status
   */
  validFormItem(item) {
    if (!item || item.$dirty && item.$invalid && !item.$pristine) {
      return true;
    }
    return false;
  }

  validForm() {
    // Mandatory to commit case
    const emptyField = (field) =>
      !_.isUndefined(this.editCaseForm[field]) &&
      (_.isUndefined(this.editCaseForm[field].$modelValue) ||
        _.isEmpty(this.editCaseForm[field].$modelValue));

    if (emptyField.caseSdo || emptyField.caseOwner ||
      emptyField.caseAdmin) {
      return false;
    }

    // If the maintenance regime is dual then the co-classification society is
    // Mandatory
    if (this.isDualMaintenance &&
      (_.isUndefined(this.editCaseForm.coClassificationSociety) ||
      !this.editCaseForm.coClassificationSociety.$modelValue)) {
      return false;
    }

    if (this.editCaseForm.$pristine || this.editCaseForm.$invalid ||
        this.editCaseForm.$error.required) {
      return false;
    }

    return true;
  }

  back() {
    this._navigationService.back();
  }
}
