import Base from 'app/base.class';
import * as _ from 'lodash';

import failedCasesTypesDialog from './failed-cases-types-dialog';

const CASE_TYPES = ['AIC', 'AIMC-In (ship)', 'First entry', 'TOC-In',
  'TOMC-In (ship)', 'Non-classed'];

export default class AddTypesController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    caseService,
    MastModalFactory,
    asset,
    caseTypes,
    caseStatus,
    referenceData
  ) {
    super(arguments);
  }

  /**
   * @return {Boolean} Boolean Returns valid or not depending on a variety of
   * different factors;
   */
  get validForm() {
    if (_.isUndefined(this.addCaseType.caseType.$modelValue)) {
      return false;
    }

    return true;
  }

  /**
   * @return {Object} Types Returns the asset
   */
  get asset() {
    return this._asset;
  }

  openCaseStatuses() {
    return _.values(_.omit(this._caseStatus.toObject(),
      ['closed', 'cancelled', 'deleted']));
  }

  /**
   * Moves the user to the next page only if there
   * is not an open case of selected case type on this asset;
   */
  async nextPage() {
    const caseType = _.parseInt(this.addCaseType.caseType.$modelValue);
    const cases =
      await this._caseService.query({
        caseStatusId: this.openCaseStatuses(),
        assetId: [ this._$stateParams.assetId ]
      });

    const openCases = _.filter(cases, (thiscase) =>
      thiscase.caseType.id === caseType &&
        _.includes(this.openCaseStatuses(), thiscase.caseStatus.id));

    if (_.size(openCases) > 0) {
      const caseName = _.filter(this.caseTypes, { id: caseType });

      const modal = new this._MastModalFactory({
        templateUrl: failedCasesTypesDialog,
        scope: {
          customText: `The case cannot be created because there is already a
case of that type (${_.first(caseName).name}) existing on the asset.`
        },
        class: 'dialog'
      });

      await modal.activate();
    } else {
      this._$state.go('asset.cases.add.addDetails', { caseType });
    }
  }

  /**
   * @return {Object} Types Returns the case types from the reference data
   * @since 02/06/2016
   */
  get caseTypes() {
    return _.filter(_.get(this._caseTypes, 'case.caseTypes'), (item) =>
      _.includes(CASE_TYPES, item.name));
  }
}
