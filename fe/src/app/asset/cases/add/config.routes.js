import AddDetailsController from './details/add-details.controller';
import addDetailsTemplateUrl from './details/add-details.html';

import AddTypesController from './types/add-types.controller';
import addTypesTemplateUrl from './types/add-types.html';

import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset.cases.add', {
      url: '/add',
      abstract: true,
      params: {
        caseId: null,
        assetId: null
      },
      views: {
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      }
    })
    .state('asset.cases.add.addDetails', {
      url: '/add/add-details/{caseType:int}',
      params: {
        caseType: null,
        assetId: null,
        caseId: null
      },
      views: {
        '@main': {
          templateUrl: addDetailsTemplateUrl,
          controller: AddDetailsController,
          controllerAs: 'vm'
        }
      },
      resolve: {

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'asset.assetTypes',
            'asset.classStatuses',
            'asset.classMaintenanceStatuses',
            'asset.iacsSocieties',
            'asset.lifecycleStatuses',
            'asset.ruleSets',
            'case.caseTypes',
            'case.officeRoles',
            'case.preEicInspectionStatuses',
            'case.riskAssessmentStatuses',
            'employee.employee',
            'employee.office',
            'flag.flags',
            'flag.portsOfRegistry',
            'party.party',
            'party.partyRoles'
          ], true),

        /* @ngInject */
        attachmentPath: ($stateParams) =>
          `case/${$stateParams.caseId}`,

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          !$stateParams.caseId ? 0 :
            attachmentDecisionService
              .getAttachmentCount(`case/${$stateParams.caseId}`)
      }
    })
    .state('asset.cases.add.addTypes', {
      url: '/add/add-types',
      params: {
        caseType: null,
        assetId: null
      },
      views: {
        '@main': {
          templateUrl: addTypesTemplateUrl,
          controller: AddTypesController,
          controllerAs: 'vm'
        }
      },
      resolve: {

        /* @ngInject */
        caseTypes: (referenceDataService) =>
          referenceDataService.get(['case.caseTypes'], true),

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'case.caseStatuses'
          ])
      }
    });
}
