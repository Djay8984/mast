import * as _ from 'lodash';

import Base from 'app/base.class';

import './partials/case-details.html';
import './partials/asset-details.html';
import './partials/flag-details.html';
import './partials/rule-set.html';
import './partials/class-maintenance.html';
import './partials/key-dates.html';
import './partials/offices-surveyors.html';
import './partials/customers.html';

import selectAssetTypesDialog from './select-asset-types-dialog';
import SelectAssetTypesController from './select-asset-types.controller';

import selectOfficeTypesDialog from './select-office-types-dialog';
import SelectOfficeTypesController from './select-office-types.controller';

import buildRuleSetDialog from './build-rule-set-dialog';
import BuildRuleSetController from './build-rule-set.controller';

import selectRelationshipDialog from './select-relationship-dialog';
import SelectRelationshipController from './select-relationship.controller';

import selectFunctionsDialog from './select-functions-dialog';
import SelectFunctionsController from './select-functions.controller';

import confirmBackNavDialog from './confirm-back-nav-dialog';
import ConfirmBackNavController from './confirm-back-nav.controller';

const ASSET_LEVEL_INDICATOR = 5;
const STATUS_NOT_FAVOURABLE = 2;

export default class AddDetailsController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $scope,
    $state,
    $stateParams,
    moment,
    MastModalFactory,
    attachmentDecisionService,
    caseService,
    caseCustomerService,
    caseFieldMatrix,
    classMaintenanceStatus,
    contractReferenceLength,
    dateFormat,
    asset,
    officeRole,
    surveyorRole,
    referenceData,
    typeaheadService
  ) {
    super(arguments);

    const date = this._moment(_.get(this._asset, 'buildDate'));
    _.set(date, '_i', this._moment(date._i).valueOf());

    this.case = { asset: { 'id': this._asset.id } };

    this.losingSocietyTypeahead = this._typeaheadService.query(
      this.assetSocieties);
    this.surveyorTypeahead = this._typeaheadService.query(
      this.employeeEmployees);
    this.flagTypeahead = this._typeaheadService.query(this.allFlags);
    this.portTypeahead = this._typeaheadService.query(this.allPorts);
    this.customerTypeahead = this._typeaheadService.query(
      this.staticCustomerList, this.customerPreFilter.bind(this));
    this.officeTypeahead = this._typeaheadService.query(
      this.employeeOffices, this.officePreFilter.bind(this));

    this.setUpData(date);

    // This fixes an error where someone navigating from the edit case to this
    // page where a case is added, inherits the old customers;
    this._caseCustomerService.setCustomers([]);

    this.formValidate = true;
  }

  /**
   * Set up the data as the form loads, taking in asset properties and
   * assigning them to the vm.case model;
   *  @param {Object} date The build date object
   */
  setUpData(date) {
    _.set(this.case, 'assetName', _.get(this._asset, 'name'));
    _.set(this.case, 'grossTonnage', _.get(this._asset, 'grossTonnage'));
    _.set(this.case, 'imoNumber', _.get(this._asset, 'ihsAsset.id'));
    _.set(this.case, 'buildDate', date);
    _.set(this.case, 'proposedFlagState',
      _.get(this._asset, 'proposedFlagState'));
    _.set(this.case, 'currentFlagState',
      _.get(this._asset, 'currentFlagState'));
    _.set(this.case, 'coClassificationSociety',
      _.get(this._asset, 'coClassificationSociety'));

    _.assign(this.case,
      _.pick(this._asset,
        ['assetLifecycleStatus', 'classStatus', 'registeredPort', 'ruleSet',
          'losingSociety', 'assetType']));

    this.case.registeredPort = this.registeredPort;
    if (this.ruleSet) {
      this.case.ruleSet.name = this.ruleSet;
    }
    this.case.assetType.name = this.assetType;

    this.case.offices =
    {
      caseOffice: { officeRole: { id: this._officeRole.get('sdo') } },
      caseCfo: { officeRole: { id: this._officeRole.get('cfo') } },
      cfo3: { officeRole: { id: this._officeRole.get('cfo3') } },
      mmsSdo: { officeRole: { id: this._officeRole.get('mmsSdo') } },
      mmso: { officeRole: { id: this._officeRole.get('mmso') } },
      tso: { officeRole: { id: this._officeRole.get('tso') } },
      tsoStat: { officeRole: { id: this._officeRole.get('tsoStat') } },
      classGroup: { officeRole: { id: this._officeRole.get('classGroup') } },
      dce: { officeRole: { id: this._officeRole.get('dce') } },
      eic: { officeRole: { id: this._officeRole.get('eic') } },
      mds: { officeRole: { id: this._officeRole.get('mds') } },
      tsdo: { officeRole: { id: this._officeRole.get('tsdo') } },
      tssao: { officeRole: { id: this._officeRole.get('tssao') } },
      caseSdo: { officeRole: { id: this._officeRole.get('caseSdo') } },
      jobSdo: { officeRole: { id: this._officeRole.get('jobSdo') } }
    };

    this.case.surveyors =
    {
      caseOwner: { employeeRole: {
        id: this._surveyorRole.get('management') } },
      caseAdmin: { employeeRole: {
        id: this._surveyorRole.get('eicAdmin') } },
      contractHolder: { employeeRole: {
        id: this._surveyorRole.get('salesCRM') } },
      leadSurveyor: { employeeRole: {
        id: this._surveyorRole.get('leadSurveyor') } }
    };

    this._displayedOfficeTypes =
      _.values(_.pick(this._officeRole.toObject(), ['sdo', 'cfo', 'jobSdo']));
  }

  /**
   * References the matrix service
   * @param {String} fieldName The field name we want to display
   * @return {Boolean} true or false Can the field be shown or not?
   */
  canDisplayField(fieldName) {
    return this._caseFieldMatrix.canDisplayField(fieldName);
  }

  get contractReferenceLength() {
    return this._contractReferenceLength;
  }

  /**
   * @return {Boolean} Boolean Returns true;
   */
  validateForm() {
    if (!_.isUndefined(this.addCaseForm.caseAcceptanceDate) &&
      (_.isUndefined(this.addCaseForm.caseAcceptanceDate.$modelValue) ||
      _.isEmpty(this.addCaseForm.caseAcceptanceDate.$modelValue))) {
      return false;
    }

    // AC ID 4110
    if (!_.isUndefined(this.addCaseForm.preEicInspectionStatus) &&
      _.parseInt(this.addCaseForm.preEicInspectionStatus.$modelValue) ===
      STATUS_NOT_FAVOURABLE && this.canDisplayField('preEicInspectionStatus')) {
      return false;
    }

    // AC ID 4112
    if (!_.isUndefined(this.addCaseForm.riskAssessmentStatus) &&
      _.parseInt(this.addCaseForm.riskAssessmentStatus.$modelValue) ===
      STATUS_NOT_FAVOURABLE && this.canDisplayField('riskAssessmentStatus')) {
      return false;
    }

    // Mandatory to commit case
    const emptyField = (field) =>
      !_.isUndefined(this.addCaseForm[field]) &&
      (_.isUndefined(this.addCaseForm[field].$modelValue) ||
        _.isEmpty(this.addCaseForm[field].$modelValue));

    if (emptyField.caseOffice || emptyField.caseOwner ||
      emptyField.caseAdmin) {
      return false;
    }

    // If the maintenance regime is dual then the co-classification society is
    // Mandatory
    if (this.isDualMaintenance &&
      (_.isUndefined(this.addCaseForm.coClassificationSociety) ||
      !this.addCaseForm.coClassificationSociety.$modelValue)) {
      return false;
    }

    if (this.addCaseForm.$pristine || this.addCaseForm.$invalid ||
        this.addCaseForm.$error.required) {
      return false;
    }

    return true;
  }

  /**
   * go back to the first screen;
   */
  async confirmBackNav() {
    if (await this._MastModalFactory({
      controller: ConfirmBackNavController,
      controllerAs: 'vm',
      templateUrl: confirmBackNavDialog,
      class: 'large dialog'
    }).activate()) {
      this._$state.go('asset.cases.add.addTypes');
    }
  }

  /**
   * @return {Object} Asset Returns the asset object
   */
  get asset() {
    return this._asset;
  }

  /**
   * @return {Array} Asset Returns the asset types array
   */
  get assetTypes() {
    return _.get(this._referenceData, 'asset.assetTypes');
  }

  async selectAssetTypes() {
    const response = await this._MastModalFactory({
      controller: SelectAssetTypesController,
      controllerAs: 'vm',
      templateUrl: selectAssetTypesDialog,
      inject: {
        selection: this.assetTypes
      },
      class: 'large dialog'
    }).activate();

    this.case.assetType =
      _.find(response, { 'levelIndication': ASSET_LEVEL_INDICATOR });
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get ruleSets() {
    return _.get(this._referenceData, 'asset.ruleSets');
  }

  /**
   * Opens the modal to select a rule set
   */
  async buildRuleSet() {
    this.case.ruleSet = await this._MastModalFactory({
      controller: BuildRuleSetController,
      controllerAs: 'vm',
      inject: {
        ruleSets: this.ruleSets
      },
      templateUrl: buildRuleSetDialog,
      class: 'large dialog'
    }).activate();
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get relationshipTypes() {
    return _.get(this._referenceData, 'party.partyRoles');
  }

  /**
   * Opens the modal to choose a customer
   */
  async selectRelationship() {
    const response = await this._MastModalFactory({
      controller: SelectRelationshipController,
      controllerAs: 'vm',
      inject: {
        relationshipTypes: this.relationshipTypes,
        currentCustomers: this._caseCustomerService.customerList()
      },
      templateUrl: selectRelationshipDialog,
      class: 'large dialog'
    }).activate();

    this._caseCustomerService.addCustomer({
      customer: { name: null },
      relationship: { name: response.name, id: response.id }
    });
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get originalOfficeTypes() {
    return _.get(this._referenceData, 'case.officeRoles');
  }

  /**
   * Opens the modal to choose office types to add
   */
  async selectOfficeType() {
    this._officeTypes =
      _.filter(this.originalOfficeTypes,
        (office) => !_.includes(this._displayedOfficeTypes, office.id) &&
        !_.includes(_.map(this.extraOffices, 'id'), office.id));

    const response = await this._MastModalFactory({
      controller: SelectOfficeTypesController,
      controllerAs: 'vm',
      inject: {
        officeTypes: this._officeTypes
      },
      templateUrl: selectOfficeTypesDialog,
      class: 'small dialog'
    }).activate();

    this._extraOffices = this._extraOffices || [];

    // Adding a camelCase here to response.name for displaying/saving offices;
    response.camelName = _.camelCase(response.name);

    this._extraOffices = _.concat(this._extraOffices, response);

    this._officeTypes =
      _.filter(this._officeTypes,
        (office) => !_.includes(_.map(this._extraOffices, 'id'), office.id));
  }

  get extraOffices() {
    if (!this._extraOffices) {
      this._extraOffices =
        _.filter(this.originalOfficeTypes,
          { 'id': this._displayedOfficeTypes });
    }
    return this._extraOffices;
  }

  /**
   * @param {Object} customer The object that the click comes from
   * Opens the modal to choose customer functions
   */
  async selectCustomerFunctions(customer) {
    const response = await this._MastModalFactory({
      controller: SelectFunctionsController,
      controllerAs: 'vm',
      inject: {
        functionTypes: await this._caseCustomerService.functionTypes()
      },
      templateUrl: selectFunctionsDialog,
      class: 'large dialog'
    }).activate();

    // Creating the customerFunction object;
    _.forEach(response, (item) => {
      item.customerFunction =
        _.pick(item, ['_id', 'id', 'name', 'description', 'deleted']);
      Reflect.deleteProperty(item, 'description');
      Reflect.deleteProperty(item, 'name');
      Reflect.deleteProperty(item, 'id');
      Reflect.deleteProperty(item, '_id');
      Reflect.deleteProperty(item, 'deleted');
    });

    this._caseCustomerService.addCustomerFunction(customer, response);
  }

  setCustomerName(customer, name) {
    this._caseCustomerService.addCustomerName(customer, name);
  }

  /**
   * @param {Object} customer The customer section on the add case form
   * Removes the customer from the customers
   */
  removeCustomer(customer) {
    this._caseCustomerService.removeCustomer(customer);
  }

  /**
   * @param {Object} customer The customer we're dealing with
   * @param {Object} item The element calling this function
   * Removes this button from the selected functions
   */
  removeCustomerFunction(customer, item) {
    this._caseCustomerService.removeCustomerFunction(customer, item);
  }

  /**
   * caseCustomerFunctions()
   * @param {Object} customer The customer we're dealing with
   * @return {Array} functions The function list of a given customer
   */
  caseCustomerFunctions(customer) {
    return this._caseCustomerService.customerFunctionList(customer);
  }

  /**
   * caseCustomers()
   * @return {Array} customerList An array of customer objects
   */
  get caseCustomers() {
    return this._caseCustomerService.customerList();
  }

  /**
   * Method that sets up the case type object, as well as returning the name
   * @return {String} Name Returns the name of the case type
   */
  get caseType() {
    const type = _.find(this.caseTypes,
        { 'id': _.parseInt(this._$stateParams.caseType) });

    this.case.caseType = this.case.caseType || type;

    return type.name;
  }

  /**
   * @return {Object} caseTypes Returns the case types from the reference data
   */
  get caseTypes() {
    return _.get(this._referenceData, 'case.caseTypes');
  }

  /**
   * @return {Array} riskAssessmentTypes The types of risk assessments
   */
  get riskAssessmentTypes() {
    return _.get(this._referenceData, 'case.riskAssessmentStatuses');
  }

  /**
   * @return {Array} preEicInspectionTypes The types of pre EIC Inspections
   */
  get preEicInspectionTypes() {
    return _.get(this._referenceData, 'case.preEicInspectionStatuses');
  }

  /**
   * @return {Array} classMaintenanceStatuses Returns the resolved class
   * maintenance statuses
   */
  get classMaintenanceStatuses() {
    return _.get(this._referenceData, 'asset.classMaintenanceStatuses');
  }

  /**
   * @return {String} dateFormat The format of dates, from the constants;
   */
  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @return {Array} lifecycleStatuses Returns the resolved lifecycle statuses
   */
  get lifecycleStatuses() {
    return _.get(this._referenceData, 'asset.lifecycleStatuses');
  }

  /**
   * @return {Array} classStatuses Returns the resolved class statuses
   */
  get classStatuses() {
    return _.get(this._referenceData, 'asset.classStatuses');
  }

  /**
   * @return {Array} offices Returns the offices from reference data
   */
  get employeeOffices() {
    return _.get(this._referenceData, 'employee.office');
  }

  /**
   * @return {Array} societies Returns the societies from reference data
   */
   get assetSocieties() {
     return _.get(this._referenceData, 'asset.iacsSocieties');
   }

  /**
   * @summary allFlags()
   * @description An array of flag objects listing all the possible ports;
   * @return {Object} flags
   */
  get allFlags() {
    return _.get(this._referenceData, 'flag.flags');
  }

  /**
   * @summary allPorts()
   * @description An array of port objects listing all the possible ports;
   * @return {Object} ports
   */
  get allPorts() {
    return _.get(this._referenceData, 'flag.portsOfRegistry');
  }

  /**
   * @summary staticCustomerList()
   * @return {Object} customers List of customers from reference data
   */
  get staticCustomerList() {
    return _.get(this._referenceData, 'party.party');
  }

  /**
   * @param {object} customer The customer we have the typeahead on
   * @return {function} filtering customers removing those with the same
   *   relationship type to prevent duplicates
   */
  customerPreFilter(customer) {
    // getting customers with the same relationship id as this on the case
    // & filtering out itself
    const sameRelationshipIds =
      _.chain(this.model.customers)
        .filter({ relationship: { id: customer.relationship.id } })
        .map('customer.id')
        .reject(_.isNil)
        .value();

    return _.isNil(customer) ?
      (datum) => true :
      (datum) => !_.includes(sameRelationshipIds, datum.id);
  }

  /*
   * @param {string|number} name optional office role name or id
   * @return {function} filtering based on office role
   */
  officePreFilter(name) {
    const id = _.isString(name) ?
      this._officeRole.get(name) :
      name;
    return _.isNil(id) ?
      (datum) => true :
      (datum) => _.get(datum, 'officeRole.id') === id;
  }

  /**
   * @return {Array} employees Returns the employees from reference data
   */
  get employeeEmployees() {
    return _.get(this._referenceData, 'employee.employee');
  }

  get assetType() {
    return !_.isNull(this.case.assetType) ?
      _.find(_.get(this._referenceData, 'asset.assetTypes'),
        { 'id': this.case.assetType.id }).name : '';
  }

  /**
   * @summary ruleSet()
   * @return {Object} ruleSet Returns the object rule via the id
   */
  get ruleSet() {
    return !_.isNull(this.case.ruleSet) ?
      _.find(this.ruleSets, { 'id': this.case.ruleSet.id }).name : '';
  }

  /**
   * @summary registeredPort()
   * @return {String} registeredPort The port that the asset for the case is
   *   registered to
   */
  get registeredPort() {
    return !_.isNull(this.case.registeredPort) ?
      _.find(this.allPorts, { 'id': this.case.registeredPort.id }) : '';
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false Depends on the form item status
   */
  validFormItem(item) {
    if (item.$dirty && item.$invalid && !item.$pristine) {
      return true;
    }
    return false;
  }

  /**
   * @return {Boolean} true or false Depends on the form maintainence regime
   */
  get isDualMaintenance() {
    return !_.isUndefined(this.addCaseForm.maintainenceRegime) &&
      !_.isUndefined(this.addCaseForm.maintainenceRegime.$modelValue) &&
      this._classMaintenanceStatus.get('dual') ===
      _.toInteger(this.addCaseForm.maintainenceRegime.$modelValue);
  }

  /**
   * @param {Boolean} flag The true or false flag for commit/uncommitted
   */
  async save(flag) {
    this.formValidate = false;
    let returnCase = null;

    this.case.customers = this._caseCustomerService.customerList();

    this.case.surveyors = _.filter(this.case.surveyors, (surveyor) => {
      if (_.isObject(surveyor.surveyor)) {
        return surveyor;
      }
    });

    // null the coClassificationSociety if the maintainenceRegime is single
    if (!this.isDualMaintenance) {
      this.case.coClassificationSociety = null;
    }

    try {
      returnCase = await this._caseService.saveCase(this.case, flag);
      await this._attachmentDecisionService.addAll(`case/${returnCase.id}`,
        this.attachments);
    } catch (err) {
      this._$log.error('save(): ', err);
    }

    this._$state.go('asset.cases.list',
      _.merge({ caseId: returnCase.id }, this._$stateParams));
  }
}
