import Base from 'app/base.class';

export default class BuildRuleSetController extends Base {
  /* @ngInject */
  constructor(
    ruleSets,
    $modalInstance
  ) {
    super(arguments);
  }

  get ruleSets() {
    return this._ruleSets;
  }

  validForm() {
    return true;
  }

  confirm() {
    this._$modalInstance.close(this.ruleSet);
  }
}
