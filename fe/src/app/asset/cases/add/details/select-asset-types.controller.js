import Base from 'app/base.class';
import * as _ from 'lodash';

export default class AddCasesAssetTypesController extends Base {
  /* @ngInject */
  constructor(
    selection,
    $modalInstance
  ) {
    super(arguments);

    this.levels = _.groupBy(this._selection, 'levelIndication');

    this.assetType = {};
    this.assetType.dropdown1 = {};
    this.assetType.dropdown2 = {};
    this.assetType.dropdown3 = {};
    this.assetType.dropdown4 = {};
    this.assetType.dropdown5 = {};
  }

  get getSelection() {
    return this._selection;
  }

  getLevel(level) {
    if (level > 1) {
      const priorLevel = Number(level - 1);
      const priorName = `dropdown${priorLevel}`;
      const priorId = this.assetType[priorName].id;

      return _.filter(this.levels[level], (item) => item.parentId === priorId);
    }

    return this.levels[level];
  }

  validForm() {
    return _.size(_.filter(this.assetType,
      (item) => !_.isEmpty(item))) === _.size(this.assetType);
  }

  confirm() {
    this._$modalInstance.close(this.assetType);
  }
}
