import Base from 'app/base.class';

export default class ConfirmBackNavController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance
  ) {
    super(arguments);
  }

  confirm() {
    this._$modalInstance.close(true);
  }
}
