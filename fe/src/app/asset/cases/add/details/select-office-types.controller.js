import Base from 'app/base.class';

export default class SelectOfficeTypesController extends Base {
  /* @ngInject */
  constructor(
    officeTypes,
    $modalInstance
  ) {
    super(arguments);
  }

  get officeTypes() {
    return this._officeTypes;
  }

  validForm() {
    return true;
  }

  confirm() {
    this._$modalInstance.close(this.office);
  }
}
