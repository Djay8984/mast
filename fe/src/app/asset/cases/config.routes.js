import * as _ from 'lodash';

import ReviewCasesController from './review/review-cases.controller';
import reviewCasesTemplateUrl from './review/review-cases.html';

import ListCasesController from './list/list-cases.controller';
import listCasesTemplateUrl from './list/list-cases.html';

import AssetHeaderController from '../header/asset-header.controller';
import assetHeaderTemplateUrl from '../header/asset-header.html';

import EditCaseController from './edit/edit-case.controller';
import editCaseTemplateUrl from './edit/edit-case.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset.cases.list', {
      url: '/list',
      parent: 'asset.cases',
      header: { open: true },
      templateUrl: listCasesTemplateUrl,
      controller: ListCasesController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        caseStatuses: (referenceDataService) =>
          referenceDataService.get(['case.caseStatuses']).then((data) =>
            _.get(data, 'case.caseStatuses')),

        /* @ngInject */
        cases: (asset, caseStatuses, caseService) =>
          caseService.query({
            caseStatusId: _.map(caseStatuses, 'id'),
            assetId: [asset.id]
          }, { size: 20, sort: 'createdOn', order: 'desc' })
      }
    })
    .state('asset.cases.edit', {
      url: '/edit/{caseId:int}',
      parent: 'asset.cases',
      resolve: {

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'asset.assetTypes',
            'asset.classStatuses',
            'asset.classMaintenanceStatuses',
            'asset.iacsSocieties',
            'asset.lifecycleStatuses',
            'asset.ruleSets',
            'case.caseTypes',
            'case.officeRoles',
            'case.preEicInspectionStatuses',
            'case.riskAssessmentStatuses',
            'employee.office',
            'employee.employee',
            'flag.flags',
            'flag.portsOfRegistry',
            'party.party',
            'party.partyRoles',
            'party.partyFunctions'
          ], true),

        /* @ngInject */
        assetCase: ($stateParams, caseService) =>
          caseService.getCase($stateParams.caseId),

        /* @ngInject */
        attachmentPath: ($stateParams) =>
          `case/${$stateParams.caseId}`,

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          !$stateParams.caseId ? 0 :
            attachmentDecisionService
              .getAttachmentCount(`case/${$stateParams.caseId}`)
      },
      views: {
        '@main': {
          controller: EditCaseController,
          controllerAs: 'vm',
          templateUrl: editCaseTemplateUrl
        }
      }
    })
    .state('asset.cases.review', {
      url: '/review/:caseId',
      parent: 'asset.cases',
      views: {
        '@main': {
          templateUrl: reviewCasesTemplateUrl,
          controller: ReviewCasesController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: assetHeaderTemplateUrl,
          controller: AssetHeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      }
    });
}
