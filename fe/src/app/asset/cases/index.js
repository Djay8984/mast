import * as angular from 'angular';

import routes from './config.routes';
import view from './view';
import add from './add';

export default angular
  .module('app.asset.cases', [
    view,
    add
  ])
  .config(routes)
  .name;

export templateUrl from './cases.html';
