import * as _ from 'lodash';

import Item from 'app/models/item.model';

import AddDefectController from
  'app/codicil/defect/add/add-defect.controller';
import addDefectTemplateUrl from
  'app/codicil/defect/add/add-defect.html';
import ViewDefectController from
  'app/codicil/defect/view/view-defect.controller';
import viewDefectTemplateUrl from
  'app/codicil/defect/view/view-defect.html';
import EditDefectController from
  'app/codicil/defect/edit/edit-defect.controller';
import editDefectTemplateUrl from
  'app/codicil/defect/edit/edit-defect.html';


import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('defect', {
      abstract: true,
      parent: 'asset.codicilsAndDefects',
      url: '/defect',
      params: {
        assetId: null,
        backTarget: null,
        codicilId: null,
        jobId: null
      }
    })
    .state('defect.add', {
      parent: 'defect',
      url: '/add',
      views: {
        '@main': {
          templateUrl: addDefectTemplateUrl,
          controller: AddDefectController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            assetModel: ($q, assetModelDecisionService, asset) =>
              $q.when(assetModelDecisionService.getRootItem(asset.id))
                .then((rootItem) => $q.all({
                  rootItem,
                  rootItems: assetModelDecisionService.getItems(asset.id,
                    _.map(rootItem.items, 'id'))
                }))
                .then((ri) =>
                  _.set(new Item(ri.rootItem), 'items', ri.rootItems)),

            /* @ngInject */
            refDefectCategories: (referenceDataService) =>
              referenceDataService.get(['defect.defectCategories'], true),

            /* @ngInject */
            refDefectDetails: (referenceDataService) =>
              referenceDataService.get(['defect.defectDetails'], true)
          }
        }
      }
    })
    .state('defect.view', {
      parent: 'defect',
      url: '/{codicilId:int}/view',
      params: {
        backTargetId: null
      },
      resolve: {

        /* @ngInject */
        subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

        /* @ngInject */
        defect: (defectDecisionService, $stateParams) =>
          defectDecisionService.get({
            assetId: $stateParams.assetId,
            defectId: $stateParams.codicilId,
            context: $stateParams.context
          }),

        /* @ngInject */
        cocs: ($stateParams, defectDecisionService, defect) =>
          defectDecisionService.getCocs({
            assetId: $stateParams.assetId,
            defectId: $stateParams.codicilId,
            context: $stateParams.context
          }).then((cocs) => _.forEach(cocs, (coc) => coc.defect = defect)),

        /* @ngInject */
        repairs: ($stateParams, repairDecisionService) =>
          repairDecisionService.get('defect', {
            assetId: $stateParams.assetId,
            codicilId: $stateParams.codicilId,
            context: $stateParams.context
          }),

        /* @ngInject */
        refDefectDetails: (referenceDataService) =>
          referenceDataService.get(['defect.defectDetails'])
      },
      views: {
        '@main': {
          templateUrl: viewDefectTemplateUrl,
          controller: ViewDefectController,
          controllerAs: 'vm'
        }
      }
    })
    .state('defect.edit', {
      parent: 'defect',
      url: '/{codicilId:int}/edit',
      views: {
        '@main': {
          templateUrl: editDefectTemplateUrl,
          controller: EditDefectController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      }
    });
}
