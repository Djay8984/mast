import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

import AddCoCController from 'app/codicil/coc/add/add-coc.controller';
import ViewCocController from 'app/codicil/coc/view/view-coc.controller';
import EditCocController from 'app/codicil/coc/edit/edit-coc.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('coc', {
      abstract: true,
      parent: 'asset.codicilsAndDefects',
      url: '/coc',
      params: {
        assetId: null,
        codicilId: null,
        defectId: null,
        jobId: null,
        backTarget: null,
        context: 'asset'
      }
    })
    .state('coc.add', {
      parent: 'coc',
      url: '/add?backTarget',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddCoCController,
          controllerAs: 'vm',
          resolve: {
            defect: () => null
          }
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false
          }
        }
      },
      resolve: {
        defect: () => null
      }
    })
    .state('coc.edit', {
      parent: 'coc',
      url: '/{codicilId:int}/edit?backTarget',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: EditCocController,
          controllerAs: 'vm'
        }
      },
      resolve: {

        /* @ngInject */
        coc: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getCoC($stateParams)
      }
    })
    .state('coc.view', {
      parent: 'coc',
      url: '/{codicilId:int}/view',
      back: 'crediting.defects.list',
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewCocController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            codicilStatuses: (referenceDataService) =>
              referenceDataService.get(['service.codicilStatuses'])

          }
        }
      },
      resolve: {
        job: () => null,

        /* @ngInject */
        coc: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getCoC($stateParams)
      }
    });
}
