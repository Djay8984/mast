import * as angular from 'angular';

import routes from './config.routes';

export default angular
  .module('app.asset.codicils-and-defects.coc', [])
  .config(routes)
  .name;
