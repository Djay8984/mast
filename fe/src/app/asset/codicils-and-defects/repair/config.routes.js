import ViewRepairController from
  'app/codicil/repair/view/view-repair.controller';
import AddRepairController from
  'app/codicil/repair/add/add-repair.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('repair', {
      abstract: true,
      parent: 'asset.codicilsAndDefects',
      url: '/repair',
      params: {
        assetId: null,
        backTarget: null,
        codicilId: null,
        jobId: null,
        parentId: null,
        repairType: null
      }
    })
    .state('repair.add', {
      parent: 'repair',
      url: '/repair/add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddRepairController,
          controllerAs: 'vm'
        }
      }
    })
    .state('repair.view', {
      parent: 'repair',
      url: '/{codicilId:int}/view',
      resolve: {
        job: () => null,

        /* @ngInject */
        repair: ($stateParams, repairDecisionService) =>
          repairDecisionService.get($stateParams.repairType, {
            assetId: $stateParams.assetId,
            codicilId: $stateParams.parentId,
            repairId: $stateParams.codicilId,
            context: $stateParams.context
          })
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewRepairController,
          controllerAs: 'vm'
        }
      }
    });
}
