import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

import AddAssetNoteController from
  'app/codicil/asset-note/add/add-asset-note.controller.js';
import EditAssetNoteController from
  'app/codicil/asset-note/edit/edit-asset-note.controller';
import ViewAssetNoteController from
  'app/codicil/asset-note/view/view-asset-note.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';


/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('assetNote', {
      abstract: true,
      parent: 'asset.codicilsAndDefects',
      url: '/asset-note',
      params: {
        assetId: null,
        backTarget: null,
        codicilId: null,
        jobId: null
      },
      resolve: {

        /* @ngInject */
        assetNote: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getAssetNote($stateParams)
      }
    })
    .state('assetNote.add', {
      parent: 'assetNote',
      url: '/add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddAssetNoteController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false
          }
        }
      }
    })
    .state('assetNote.edit', {
      parent: 'assetNote',
      url: '/{codicilId:int}/edit',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: EditAssetNoteController,
          controllerAs: 'vm'
        }
      }
    })
    .state('assetNote.view', {
      parent: 'assetNote',
      url: '/{codicilId:int}/view',
      resolve: {

        /* @ngInject */
        job: () => null
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewAssetNoteController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false
          }
        }
      }
    });
}
