import * as add from 'app/codicil/actionable-item/add';
import * as view from 'app/codicil/actionable-item/view';
import * as edit from 'app/codicil/actionable-item/edit';

/* @ngInject */
export default function config($stateProvider, stateHelper) {
  const routes = {
    actionableItem: {
      abstract: true,
      parent: 'asset.codicilsAndDefects',
      url: '/actionable-item',
      header: {
        open: false
      },
      params: {
        assetId: null,
        jobId: null
      },
      children: {
        add: {
          url: '/add',
          title: 'Add actionable item',
          content: add,
          header: { open: true }
        },
        edit: {
          url: '/{codicilId:int}/edit',
          title: 'Edit actionable item',
          content: edit,
          resolve: view.resolve
        },
        view: {
          url: '/{codicilId:int}/view',
          title: 'View actionable item',
          content: view
        }
      }
    }
  };

  stateHelper.create($stateProvider, routes);
}
