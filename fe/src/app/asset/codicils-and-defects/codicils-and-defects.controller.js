import Base from 'app/base.class';
import * as _ from 'lodash';
import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CodicilsAndDefectsController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $log,
    $q,
    $rootScope,
    $scope,
    $stateParams,
    actionableItemsSearchFilter,
    asset,
    assetNoteSearchFilter,
    cocStatusSearchFilter,
    codicilFilter,
    defectDefaultSearchFilter,
    printService,
    searchResult
  ) {
    super(arguments);
  }

  get asset() {
    return this._asset;
  }

  get assetNotes() {
    return _.get(this._searchResult, 'asset-note');
  }

  get cocs() {
    return _.get(this._searchResult, 'coc');
  }

  get defects() {
    return _.get(this._searchResult, 'defect');
  }

  get actionableItems() {
    return _.get(this._searchResult, 'actionable-item');
  }

  countElements(codicilType) {
    return _.get(this._searchResult,
      `${codicilType}.pagination.totalElements`, 0);
  }

  get codicilFilter() {
    return this._codicilFilter;
  }

  get defectDefaultSearchFilter() {
    return this._defectDefaultSearchFilter;
  }

  get searched() {
    return this._$stateParams.searched;
  }

  get totalCodicils() {
    return _.sumBy(
      ['actionable-item', 'asset-note', 'coc'],
      (codicilType) => this.countElements(codicilType));
  }

  get totalResults() {
    return _.sumBy(
      ['actionable-item', 'asset-note', 'coc', 'defect'],
      (codicilType) => this.countElements(codicilType));
  }

  get totalDefects() {
    return _.size(this._searchResult.defect);
  }

  async print(stateName) {
    const printables = await this._searchResult.printables();

    this.printPayload = _.merge(this._asset, {
      actionableItems: printables['actionable-item'],
      assetNotes: printables['asset-note'],
      coc: printables.coc,
      defects: printables.defect,
      additionalInfo: ['coc', 'actionableItems', 'assetNotes', 'defects']
    });

    super.print(stateName);
  }
}
