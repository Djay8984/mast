import * as _ from 'lodash';
import * as angular from 'angular';

import actionableItem from './actionable-item';
import assetNote from './asset-note';
import coc from './coc';
import defect from './defect';
import repair from './repair';

export default angular
  .module('app.asset.codicils-and-defects', [
    actionableItem,
    assetNote,
    coc,
    defect,
    repair
  ])
  .name;

export controller from './codicils-and-defects.controller';
export templateUrl from './codicils-and-defects.html';

export const resolve = {

  /* @ngInject */
  categories: (referenceDataService) =>
    referenceDataService.get(['asset.codicilCategories']),

  /* @ngInject */
  confidentialityTypes: (referenceDataService) =>
    referenceDataService.get(['attachment.confidentialityTypes']),

  /* @ngInject */
  codicilStatuses: (referenceDataService) =>
    referenceDataService.get(['service.codicilStatuses']),

  /* @ngInject */
  actionableItemsSearchFilter: (
    actionableItemStatus,
    actionableItemCategory
  ) => ({
    statusList:
      _.values(_.pick(actionableItemStatus.toObject(),
        ['changeRecommended', 'inactive', 'open'])),
    confidentialityList: [1, 2, 3],
    typeList: {
      slug: 'actionable-item',
      categories: actionableItemCategory.toArray()
    }
  }),

  /* @ngInject */
  assetNoteSearchFilter: (assetNoteStatus, assetNoteCategory) => ({
    statusList:
      _.values(_.pick(assetNoteStatus.toObject(),
        ['changeRecommended', 'inactive', 'open'])),
    confidentialityList: [1, 2, 3],
    typeList: {
      slug: 'asset-note',
      categories: assetNoteCategory.toArray()
    }
  }),

  /* @ngInject */
  cocStatusSearchFilter: (cocStatus, cocCategory) => ({
    statusList: [cocStatus.get('open')],
    confidentialityList: [1, 2, 3],
    typeList: {
      slug: 'coc',
      categories: cocCategory.toArray()
    }
  }),

  /* @ngInject */
  codicilFilter: (actionableItemsSearchFilter,
    assetNoteSearchFilter,
    cocStatusSearchFilter,
    defectDefaultSearchFilter) => {
    const filters = [
      actionableItemsSearchFilter,
      assetNoteSearchFilter,
      cocStatusSearchFilter,
      defectDefaultSearchFilter
    ];

    return {
      statusList: _.uniq(_.flatMap(filters, 'statusList')),
      confidentialityList: _.uniq(
        _.flatMap(filters, 'confidentialityList')),
      typeList: _.map(filters, 'typeList')
    };
  },

  /* @ngInject */
  codicilSearchFilter: (actionableItemsSearchFilter,
    assetNoteSearchFilter,
    cocStatusSearchFilter) => {
    const filters = [
      actionableItemsSearchFilter,
      assetNoteSearchFilter,
      cocStatusSearchFilter
    ];

    return {
      statusList: _.uniq(_.flatMap(filters, 'statusList')),
      confidentialityList: _.uniq(
        _.flatMap(filters, 'confidentialityList')),
      typeList: _.map(filters, 'typeList')
    };
  },

  /* @ngInject */
  defectDefaultSearchFilter: (defectStatus) => ({
    statusList: [defectStatus.get('open')],
    confidentialityList: [1, 2, 3],
    typeList: {
      slug: 'defect',
      categories: [1, 2, 3, 4, 5]
    }
  }),

  /* @ngInject */
  employee: (employeeDecisionService) =>
    employeeDecisionService.getCurrentUser(),

  /* @ngInject */
  searchResult: (
    $q,
    $stateParams,
    codicilOnlineService,
    codicilSearchFilter,
    defectDefaultSearchFilter,
    defectOnlineService
  ) => {
    const searchFilter = _.isEmpty($stateParams.searchFilter) ?
      angular.copy(codicilSearchFilter) :
      angular.copy($stateParams.searchFilter);

    const defectFilter = _.isEmpty($stateParams.defectSearchFilter) ?
      angular.copy(defectDefaultSearchFilter) :
      angular.copy($stateParams.defectSearchFilter);

    const defaultOptions = { size: 3 };

    const actionableItemParams = [
      $stateParams,
      searchFilter,
      'actionable-item',
      true,
      { ...defaultOptions, sort: 'dueDate' }
    ];

    const assetNoteParams = [
      $stateParams,
      searchFilter,
      'asset-note',
      true,
      { ...defaultOptions, sort: 'title' }
    ];

    const cocParams = [
      $stateParams,
      searchFilter,
      'coc',
      true,
      { ...defaultOptions, sort: 'dueDate' }
    ];

    const defectParams = [
      $stateParams,
      defectFilter,
      'defect',
      true,
      defaultOptions
    ];

    return $q.all({
      'actionable-item':
        codicilOnlineService.query(...actionableItemParams),
      'asset-note':
        codicilOnlineService.query(...assetNoteParams),
      'coc':
        codicilOnlineService.query(...cocParams),
      'defect':
        defectOnlineService.query(...defectParams)
    }).then((result) => {
      const nullifyLimit = (params) => {
        params[params.length - 1].size = null;
        return params;
      };

      result.printables = () =>
        $q.all({
          'actionable-item': codicilOnlineService.query(
              ...nullifyLimit(actionableItemParams)),
          'asset-note': codicilOnlineService.query(
              ...nullifyLimit(assetNoteParams)),
          'coc': codicilOnlineService.query(
              ...nullifyLimit(cocParams)),
          'defect': defectOnlineService.query(
            ...nullifyLimit(defectParams))
        });

      return result;
    });
  }
};
