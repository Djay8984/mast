import * as _ from 'lodash';
import Base from 'app/base.class';
import ServiceCatalogueModel from 'app/models/service-catalogue.model';

export default class AddServiceController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    $state,
    asset,
    assignedProducts,
    assignedServices,
    serviceType,
    serviceRelationshipType,
    product,
    productType,
    productServiceCatalogues
  ) {
    super(arguments);
    this.relationshipType = this._serviceRelationshipType.toObject();
    this.productType = this._productType.toObject();
    this.serviceType = this._serviceType.toObject();

    this.models = {};
    this.models.serviceCatalogues = _.map(
      this._productServiceCatalogues, (catalogue) => {
        const model = Reflect.construct(ServiceCatalogueModel, [catalogue]);

        _.set(model, 'relationshipType', this.relationshipType);
        _.set(model, 'serviceType', this.serviceType);

        return model;
      });

    this.availableOptions = this.getAvailableOptions();

    this.sequenceSteps = [
      {
        slug: 'select_services',
        name: 'Select services'
      },
      {
        slug: 'specify_details',
        name: 'Specify details'
      }
    ];

    this.sequenceConfig = {};
    this.sequenceConfig.currentStep = _.first(this.sequenceSteps);
  }

  get asset() {
    return this._asset;
  }

  get assignedProducts() {
    return this._assignedProducts;
  }

  get assignedServices() {
    return this._assignedServices;
  }

  get assignedCatalogueIds() {
    return _.map(this._assignedServices, 'serviceCatalogueId');
  }

  get addedCatalogues() {
    return _.union(this.assignedCatalogueIds, this.selectedCatalogueIds);
  }

  get product() {
    return this._product;
  }

  get productId() {
    return this._$stateParams.productId;
  }

  get productTypeId() {
    return _.get(this.product, 'productType.id');
  }

  get selectedCatalogueIds() {
    return _.map(_.filter(this.models.serviceCatalogues, 'selected'), 'id');
  }

  isAssigned(id) {
    return _.includes(this.assignedCatalogueIds, id);
  }

  isProductType(type) {
    return _.isEqual(this.productTypeId, this._productType.get(type));
  }

  isSameSchedulingRegime(catalogue) {
    if (!this.isProductType('statutory')) {
      return true;
    }

    return catalogue.schedulingRegimeId ===
      _.get(this.product, 'schedulingRegime.id');
  }

  /**
   * @returns {array} array containing a catalogue and optional counterpart.
   */
  getAvailableOptions() {
    // before doing the reduce the list is filtered by the asset ruleset if
    // the product type is classification
    return _.reduce(_.filter(this.models.serviceCatalogues, (catalogue) =>
     !this.isProductType('classification') ||
     catalogue.serviceRuleset.id === this.asset.productRuleSet.id),
     (result, catalogue) => {
       const opt = { catalogue };

      // does this service catalogue have a counterpart
       if (catalogue.hasCounterparts) {
         const counterpart = _.find(this.models.serviceCatalogues, {
           id: _.first(catalogue.counterparts)
         });
         const existing = _.find(result, { catalogue: { id: counterpart.id } });

         // set the catalogue counterpart
         _.set(opt, 'counterpart', counterpart);

         // link the counterpart with the catalogue
         _.set(opt.counterpart, 'counterpart', catalogue);

         if (_.isUndefined(existing)) {
           result.push(opt);
         }
       } else if (this.isSameSchedulingRegime(catalogue)) {
         result.push(opt);
       }
       return result;
     }, []);
  }


  /**
   * Predicate which decides whether a service should be available for
   * selection.
   *
   * @param {object} opt the option we're inspecting
   * @property {object} opt.catalogue the service catalogue
   * @property {object} opt.counterpart (optional) counterpart
   * @returns {boolean} to display or not to display
   */
  showOption(opt) {
    // is this service already assigned?
    if (this.isAssigned(opt.catalogue.id) && !opt.catalogue.multipleIndicator) {
      return false;
    }

    // does this catalogue have a counterpart that is already added?
    if (_.some(opt.catalogue.counterparts, _.bind(this.isAssigned, this))) {
      return false;
    }

    // don't show any misc services
    if (opt.catalogue.isType(this.serviceType.miscellaneous)) {
      return false;
    }

    // statutory products will only show services that share the same
    // scheduling regime
    if (this.isProductType('statutory') &&
      !this.isSameSchedulingRegime(opt.catalogue)) {
      return false;
    }

    // does this catalogue depend on others
    if (!_.isEmpty(opt.catalogue.dependsOn)) {
      const missing = _.difference(opt.catalogue.dependsOn,
          this.addedCatalogues);

      if (!_.isEmpty(missing)) {
        // deselect catalogues with missing dependencies.
        _.set(opt.catalogue, 'selected', false);

        // deselect service checkbox
        _.set(opt, 'selected', false);
        return false;
      }
    }

    // dont show services if they have a cycle periodicity of zero.
    return opt.catalogue.cyclePeriodicity;
  }

  /**
   * @param {object} opt the selected service catalogue
   */
  toggleOption(opt) {
    // if de-selecting a counterpart service, ensure both service
    // catalogues are also de-selected.
    if (!opt.selected && opt.counterpart) {
      _.set(opt, 'catalogue.selected', false);
      _.set(opt, 'counterpart.selected', false);
    }

    // if selecting a statutory renewal opt, auto-select the annual and
    // intermediate opts
    if (opt.selected && this.isProductType('statutory') &&
      opt.catalogue.isType(this.serviceType.renewal)) {
      const types = _.values(
        _.pick(this.serviceType, ['annual', 'intermediate']));

      _.forEach(opt.catalogue.dependents, (id) => {
        const dependent = _.find(this.availableOptions, {
          catalogue: { id }
        });

        if (_.includes(types, dependent.catalogue.serviceTypeId)) {
          _.set(dependent, 'selected', true);
          _.set(dependent, 'catalogue.selected', true);
        }
      });
    }

    _.set(opt, 'catalogue.selected', opt.selected);
  }

  /**
   * Called when used selects a counterpart service, ensures the opposite
   * counterpart is deselected and auto-selects the option checkbox.
   *
   * @param {object} opt the current option
   * @param {object} opposite the opposite counterpart
   */
  toggleCounterpart(opt, opposite) {
    opt.selected = true;
    opposite.selected = false;
  }

  /**
   * @returns {boolean} returns true if any catalogues have been selected.
   */
  hasSelectedCatalogues() {
    return _.some(this.models.serviceCatalogues, 'selected');
  }

  next() {
    this._$state.go('asset.serviceSchedule.addService.specifyDetails', {
      productId: this._$stateParams.productId,
      serviceCatalogues: _.filter(this.models.serviceCatalogues, 'selected')
    });
  }

  back() {
    this._$state.go('asset.serviceSchedule.list');
  }
}
