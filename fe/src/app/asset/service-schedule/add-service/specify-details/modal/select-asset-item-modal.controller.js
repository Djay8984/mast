import Base from 'app/base.class';
import * as _ from 'lodash';

export default class SelectAssetItemModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    assetModelDecisionService,
    asset,
    assetModel,
    selectedItem
  ) {
    super(arguments);
  }

  checkTerm(searchTerm) {
    const oldMode = this.searchMode;
    this.searchMode = !_.isNull(searchTerm) && this.searchMode;

    if (this.searchMode !== oldMode) {
      this.chosenItem = null;
    }
  }

  async search(searchTerm) {
    this.searchMode = !_.isEmpty(searchTerm) && searchTerm !== '*';
    this.chosenItem = null;

    if (this.searchMode) {
      this.searchResults = await this._assetModelDecisionService.search({
        itemName: searchTerm },
          this._asset.id);
    }
  }

  confirm() {
    // de-select the item
    this.chosenItem.selected = false;

    // collapse hierarchy
    this.chosenItem.setParentProps({ expanded: false });

    this._$modalInstance.close(this.chosenItem);
  }

  get asset() {
    return this._asset;
  }

  get assetModel() {
    return this._assetModel;
  }

  get selection() {
    return this._selection;
  }

  get selectedItem() {
    return this._selectedItem;
  }
}
