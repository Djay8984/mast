import * as _ from 'lodash';

import BaseForm from 'app/base-form.class';

import Service from 'app/models/service.model';
import Asset from 'app/models/asset.model';

import selectAssetItemModalTemplate from
  './modal/select-asset-item-modal.html';
import SelectAssetItemModalController from
  './modal/select-asset-item-modal.controller';


export default class AddServiceSpecifyDetailsController extends BaseForm {
  /* @ngInject */
  constructor(
    $log,
    $stateParams,
    $state,
    moment,
    MastModalFactory,
    asset,
    assetModel,
    assignedProducts,
    assignedServices,
    dateFormat,
    serviceType,
    serviceRelationshipType,
    scheduledServiceStatus,
    serviceCreditStatus,
    serviceScheduleService,
    serviceStatus,
    product,
    productType,
    refServicesStatuses
  ) {
    super(arguments);
    this.models = {};
    this.setup();
  }

  setup() {
    this.models.asset = Reflect.construct(Asset, [this._asset]);
    this.models.services = _.map(this.serviceCatalogues, (obj) => {
      const service = Reflect.construct(Service, [{
        provisionalDates: false,
        cyclePeriodicity: _.get(obj, 'cyclePeriodicity'),
        serviceCatalogueId: _.get(obj, 'id'),
        asset: _.pick(this._asset, ['id', '_id']),
        serviceCreditStatus: {
          id: this._serviceCreditStatus.get('notStarted')
        },
        serviceStatus: { id: this._serviceStatus.get('notStarted') }
      }, this._moment]);
      return _.set(service, '_serviceCatalogue', obj);
    });

    this._provisionalStatuses = [
      { id: 1, value: false, name: 'None' },
      { id: 2, value: true, name: 'Provisional' }
    ];
  }

  get asset() {
    return this.models.asset;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  get serviceStatuses() {
    return _.reject(this._refServicesStatuses, {
      id: this._scheduledServiceStatus.get('complete')
    });
  }

  get product() {
    return this._product;
  }

  get services() {
    return this.models.services;
  }

  get serviceCatalogues() {
    return this._$stateParams.serviceCatalogues;
  }

  get provisionalStatuses() {
    return this._provisionalStatuses;
  }

  async save() {
    try {
      await this._serviceScheduleService
        .assignAssetService(this._asset.id,
          _.map(this.models.services, 'payload'));

      this._$state.go('asset.serviceSchedule.list', {}, { reload: true });
    } catch (e) {
      this._$log.error(e);
    }
  }

  validForms() {
    return _.every(this.models.services, 'form.$valid');
  }

  async selectItem(service) {
    service.assetItem = await this._MastModalFactory({
      controller: SelectAssetItemModalController,
      controllerAs: 'vm',
      templateUrl: selectAssetItemModalTemplate,
      inject: {
        asset: this._asset,
        assetModel: this._assetModel,
        selectedItem: this._assetModel
      },
      class: 'full dialog background-sea'
    }).activate();
  }

  /**
   * @param {object} field - the field we're inspecting
   * @returns {boolean} field - the field validity
   */
  isFieldInvalid(field) {
    return field.$dirty && field.$invalid && !field.$pristine;
  }

  /**
   * @returns {object} todays date plus 3 months
   */
  get maxAssignedDate() {
    if (!this._maxAssignedDate) {
      this._maxAssignedDate = this._moment().add(3, 'months');
    }

    return this._maxAssignedDate;
  }

  /**
   * @param {object} service - the service to inspect
   * @returns {boolean} is service harmonised and harmonisation date set
   */
  isHarmonisationInvalid(service) {
    return !_.isNull(service._serviceCatalogue.harmonisationType) &&
      _.isNull(this._asset.harmonisationDate);
  }

  /**
   * @param {object} service - the service we're inspecting
   * @return {boolean} to enable the date range fields or not
   */
  enableDateRanges(service) {
    const serviceCatalogue = _.get(service, '_serviceCatalogue');

    // if the upper/lower range period on the product catalogue is > 0
    // and the due date for the service has been populated
    return _.size(_.get(service.form, 'dueDateManual.$viewValue')) &&
      serviceCatalogue.lowerRangeDateOffsetMonths;
  }

  /**
   * @param {number} serviceCatalogueId service catalogue id to remove
   * @returns {array} array of returned elements
   */
  removeService(serviceCatalogueId) {
    return _.remove(this.models.services, { serviceCatalogueId });
  }

  /**
   * @description return to previous state
   */
  back() {
    this._$state.go('asset.serviceSchedule.addService');
  }
}
