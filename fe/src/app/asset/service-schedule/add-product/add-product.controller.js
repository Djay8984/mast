import * as _ from 'lodash';

import Base from 'app/base.class';

import Model from 'app/models/model.class';

export default class AddProductController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    $state,
    serviceScheduleService,
    asset,
    productCatalogues,
    serviceCatalogues,
    assignedProducts,
    refProductTypes,
    refRulesetCategories,
    productType
  ) {
    super(arguments);

    // Constants - Reference data IDs considered stable.
    this.classificationId = this._productType.get('classification');
    this.statutoryId = this._productType.get('statutory');

    this.productType = _.head(
      _.filter(_.get(this._refProductTypes, 'product.productTypes'),
        { id: $stateParams.productTypeId }));

    this.ruleset = _.isInteger($stateParams.rulesetId) ?
      _.find(this.rulesetCategories, { id: $stateParams.rulesetId }) : null;

    this.flagControls = [
      {
        label: 'Not restricted by flag',
        value: 'ignore_flag'
      },
      {
        label: 'Restricted by flag',
        value: 'restrict_flag'
      }
    ];

    // Default to filter by the asset's flag
    this.formConfig = {
      flag: this.flagControls[1].value
    };

    this.setupRuntime();
  }

  get asset() {
    return this._asset;
  }

  get rulesetCategories() {
    return _.get(this._refRulesetCategories, 'asset.rulesetCategories');
  }

  get serviceCatalogues() {
    return this._serviceCatalogues;
  }

  setupRuntime() {
    /*
     * Product filter: Stage 1 - filter by product type
     *   Removes anything not of the requested product type
     */
    const filterByType = ({ products, type }) =>
      _.filter(products, (product) =>
        _.get(product, 'productType.id') === type.id);

    this.productCataloguesByType = filterByType({
      products: this._productCatalogues,
      type: this.productType
    });

    this.assignedProductsByType = filterByType({
      products: this._assignedProducts,
      type: this.productType
    });

    /*
     * Product filter: Stage 2 - filter by existing service schedule
     *   Removes anything that already exists on the service schedule
     */
    const rejectByAssignment = ({ candidates, rejectList }) =>
      _.reject(candidates, (candidate) =>
        _.some(rejectList, { productCatalogueId: candidate.id }));

    this.productCataloguesUnassigned = rejectByAssignment({
      candidates: this.productCataloguesByType,
      rejectList: this.assignedProductsByType
    });

    /*
     * Sorts <services> into <buckets>, each bucket labeled by the value found
     * in <path> of that service.
     */
    const bucketise = ({ services, path }) => {
      const buckets = {};

      // Sort the services into buckets
      _.forEach(services, (service) => {
        const property = _.get(service, path);
        if (!buckets[property]) {
          buckets[property] = { services: [], schedulingRegimes: [] };
        }
        buckets[property].services.push(service);
      });

      // Extract a unique list of schedulingRegimes for each bucket
      _.forEach(buckets, (bucket) => {
        bucket.schedulingRegimes = _.uniqBy(
          _.compact(_.map(bucket.services, 'schedulingRegime')),
          (regime) => _.get(regime, 'id', null));
      });

      return buckets;
    };

    /*
     * Each bucket is known by product catalogue ID
     * Each bucket looks like { services: [], schedulingRegimes: [] }
     */
    this.servicesByProduct = bucketise({
      services: this.serviceCatalogues,
      path: 'productCatalogue.id'
    });

    /*
     * These options available in principle to the user.
     * Promote filtered products into selectable UI options.
     * These will be the 'underlying' options.
     */
    this.underlyingOptions = _.map(this.productCataloguesUnassigned, (opt) =>
      _.set(new Model(opt), 'schedulingRegimes',
        this.servicesByProduct[opt.id].schedulingRegimes));

    /*
     * These are the options visible to the user.
     * They are a subset of the underlying options.
     */
    this.updateVisibleOptions();
  }

  updateVisibleOptions() {
    let flag = null;

    if (this.formConfig.flag === 'restrict_flag') {
      flag = { id: _.get(this.asset, 'flagState.id') };
    }

    this.visibleOptions = this.limitOptions({
      underlyingOptions: this.underlyingOptions,
      limitRuleset: this.ruleset,
      limitFlag: flag
    });
  }

  limitOptions({ underlyingOptions, limitRuleset, limitFlag }) {
    /*
     * Overview
     *
     * Filter the product catalogues according to the:
     *   service catalogues that:
     *     have a link to the ruleset AND a link to the product
     *
     * Details
     *
     * Instead of search all the services for every product which would result
     * in an O(n^2) search, we can re-use some filtering to get closer to O(n)
     * time.
     *
     * First, go through all the services and group them into product buckets.
     *
     * Then, for each product bucket that exists in options, filter the services
     * by ruleset or flag.
     */

    /*
     * Remove any option (product catalogue) that has no services
     * related to it because further filtering of the product
     * catalogue relies on the data in the services.
     */
    const filterByBucket = ({ options, buckets }) =>
      _.filter(options, (option) =>
        buckets.hasOwnProperty(option.model.id));

    /*
     * options are unassigned product catalogues
     * buckets are services grouped by product catalogue
     * Each service has a ruleset it is valid under.
     */
    const filterByRuleset = ({ options, buckets, ruleset }) =>
      _.filter(options, (option) =>
        _.some(buckets[option.model.id].services, (service) =>
          _.get(service, 'serviceRuleset.id') === ruleset.id));

    /*
     * options are unassigned product catalogues
     * buckets are services grouped by product catalogue
     * Each service has a flag it is valid under.
     */
    const hasFlag = (service, flag) =>
      _.includes(_.map(service.flags, 'id'), flag.id);

    const filterByFlag = ({ options, buckets, flag }) =>
      _.filter(options, (option) =>
        _.some(buckets[option.model.id].services, (service) =>
          hasFlag(service, flag)));

    if (limitRuleset || limitFlag) {
      underlyingOptions = filterByBucket({
        options: underlyingOptions,
        buckets: this.servicesByProduct
      });
    }

    if (this.productType.id === this.classificationId && limitRuleset) {
      underlyingOptions = filterByRuleset({
        options: underlyingOptions,
        buckets: this.servicesByProduct,
        ruleset: limitRuleset
      });
    }

    if (this.productType.id === this.statutoryId && limitFlag) {
      underlyingOptions = filterByFlag({
        options: underlyingOptions,
        buckets: this.servicesByProduct,
        flag: limitFlag
      });
    }

    return underlyingOptions;
  }

  validForm() {
    const selectedOptions = _.filter(this.visibleOptions, 'selected');
    let validRegimes = true;

    if (this.schedulingRegimeControlsRequired()) {
      validRegimes = _.every(selectedOptions, 'schedulingRegime');
    }

    return !_.isEmpty(selectedOptions) && validRegimes;
  }

  back(shouldReload = false) {
    this._$state.go('asset.serviceSchedule.list',
      this._$stateParams,
      { reload: shouldReload });
  }

  async confirm() {
    const selected = _.filter(this.visibleOptions, 'selected');
    const products = _.map(selected, (opt) => {
      const productCatalogue = opt.model;

      const product = {
        id: null,
        schedulingRegime: null,
        productCatalogueId: productCatalogue.id
      };

      if (this.schedulingRegimeControlsRequired()) {
        product.schedulingRegime = {
          id: _.get(opt, 'schedulingRegime.id')
        };
      }

      return product;
    });

    const existingProducts = _.invokeMap(this._assignedProducts, 'plain');

    await this._serviceScheduleService
      .updateAssignedAssetProducts({
        assetId: this.asset.id
      }, {
        base: existingProducts,
        toAdd: products
      });

    this.back(true);
  }

  isStatutoryProduct() {
    return this.productType.id === this.statutoryId;
  }

  flagControlsRequired() {
    return this.isStatutoryProduct();
  }

  schedulingRegimeControlsRequired() {
    return this.isStatutoryProduct();
  }
}
