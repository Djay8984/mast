import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintMixin from 'app/components/print/print.mixin';
import ServiceModel from 'app/models/service.model';
import ServiceCatalogueModel from 'app/models/service-catalogue.model';

import swapServicesTemplateUrl from
  './partials/swap-services.html';
import swapServicesModalController from
  './swap-services-modal.controller';

import 'app/asset/service-schedule/view-service/partials/task-summary.html';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewServiceServiceScheduleController
  extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $log,
    $rootScope,
    $scope,
    $state,
    $stateParams,
    asset,
    assignedProducts,
    assignedServices,
    attachmentCount,
    attachmentPath,
    MastModalFactory,
    moment,
    printService,
    serviceCatalogues,
    serviceRelationshipType,
    serviceScheduleService,
    tasks
  ) {
    super(arguments);

    this.models = {};

    this.models.service =
      new ServiceModel(this.service, moment);

    this.models.serviceCatalogue =
      new ServiceCatalogueModel(this.serviceCatalogue);

    this.printPayload = _.extend({}, this._asset, {
      scheduleTasks: this._tasks,
      serviceSchedule: this.serviceCatalogue,
      confidentialityType: { id: 'All' },
      additionalInfo: ['scheduleTasks']
    });
  }

  get asset() {
    return this._asset;
  }

  get service() {
    return _.find(this._assignedServices, { id: this._$stateParams.serviceId });
  }

  get assignedProducts() {
    return this._assignedProducts;
  }

  get serviceCatalogues() {
    return this._serviceCatalogues;
  }

  // Gets the service catalogue for this service
  get serviceCatalogue() {
    const serviceCatalogueId = this.service.serviceCatalogueId;

    return _.find(this.serviceCatalogues, { id: serviceCatalogueId });
  }

  /**
   * Retrieve the product associated with the service
   *
   * @returns {Object} Product
   */
  get product() {
    const serviceCatalogue = this.serviceCatalogue;
    const serviceProductLinkId = _.get(serviceCatalogue, 'productCatalogue.id');

    return _.find(this.assignedProducts, {
      productCatalogueId: serviceProductLinkId
    });
  }

  get backTarget() {
    return this._$stateParams.backTarget;
  }

  get attachmentCount() {
    return this._attachmentCount;
  }

  get attachmentPath() {
    return this._attachmentPath;
  }

  /**
   * Determine the service's counterpart
   *
   * @returns {Object} Service Catalogue or null if no counterpart exists
   */
  get counterpart() {
    const counterpartId = this._serviceRelationshipType.get('counterpart');

    const serviceCatalogue = this.serviceCatalogue;

    const relationships = _.get(serviceCatalogue, 'relationships');

    const counterpartRelationships = _.filter(relationships, (relationship) =>
      _.get(relationship, 'relationshipType.id') === counterpartId);

    // Take the first relationship because it shouldn't be possible to have
    // more than one counterpart
    const relationship = _.first(counterpartRelationships);

    // Find the opposite service catalogue ID
    let oppositeId = _.get(relationship, 'toServiceCatalogue.id');

    // Whoops, the relationship must be the other way around
    if (oppositeId === serviceCatalogue.id) {
      oppositeId = _.get(relationship, 'fromServiceCatalogue.id');
    }

    return _.isNumber(oppositeId) ?
      _.find(this.serviceCatalogues, { id: oppositeId }) : null;
  }

  /**
   * @returns {Boolean} Existence of counterpart
   */
  hasCounterpart() {
    return Boolean(this.counterpart);
  }

  async swap() {
    try {
      const service = this.service;
      const serviceCatalogue = this.serviceCatalogue;
      const counterpartCatalogue = this.counterpart;

      await new this._MastModalFactory({
        controller: swapServicesModalController,
        controllerAs: 'vm',
        templateUrl: swapServicesTemplateUrl,
        scope: {
          title: 'You have selected to swap'
        },
        class: 'dialog',
        inject: {
          outgoingServiceCatalogue: serviceCatalogue,
          incomingServiceCatalogue: counterpartCatalogue
        }
      }).activate();

      const updatedService = service;

      updatedService.serviceCatalogueId = counterpartCatalogue.id;

      await this._serviceScheduleService.updateAssignedAssetService({
        assetId: this.asset.id,
        serviceId: service.id
      }, updatedService);

      this._$state.reload();
    } catch (e) {
      if (e) {
        this._$log.error(e);
      } else {
        this._$log.debug('swap() Modal dismissed');
      }
    }
  }

  get tasks() {
    return this._tasks;
  }
}
