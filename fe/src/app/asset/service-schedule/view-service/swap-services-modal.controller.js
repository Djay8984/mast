import Base from 'app/base.class';

export default class SwapServicesModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    outgoingServiceCatalogue,
    incomingServiceCatalogue
  ) {
    super(arguments);
  }

  get outgoingServiceCatalogue() {
    return this._outgoingServiceCatalogue;
  }

  get incomingServiceCatalogue() {
    return this._incomingServiceCatalogue;
  }

  confirm() {
    this._$modalInstance.close(true);
  }
}
