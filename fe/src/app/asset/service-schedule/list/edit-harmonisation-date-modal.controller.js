import Base from 'app/base.class';

export default class EditHarmonisationModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    moment,
    harmonisationDate,
    assetBuildDate,
    dateFormat
  ) {
    super(arguments);

    this.dates = {
      harmonisationLowerBound: moment(this.assetBuildDate),
      harmonisationUpperBound: moment().add(3, 'months')
    };
  }

  get assetBuildDate() {
    return this._assetBuildDate;
  }

  get harmonisationDate() {
    return this._harmonisationDate;
  }

  set harmonisationDate(value) {
    this._harmonisationDate = value;
  }

  /**
   * @return {String} dateFormat The format of dates, from the constants;
   */
  get dateFormat() {
    return this._dateFormat;
  }

  validForm() {
    return this.modalForm.$valid;
  }

  confirm() {
    this._$modalInstance.close(this.harmonisationDate);
  }

  get harmonisationLowerBound() {
    return this.dates.harmonisationLowerBound;
  }

  get harmonisationUpperBound() {
    return this.dates.harmonisationUpperBound;
  }
}
