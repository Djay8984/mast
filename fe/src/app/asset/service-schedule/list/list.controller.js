import * as _ from 'lodash';

import Base from 'app/base.class';

import Model from 'app/models/model.class';
import ServiceModel from 'app/models/service.model';
import ServiceCatalogueModel from 'app/models/service-catalogue.model';

import editHarmonisationTemplateUrl from
  './partials/edit-harmonisation-date.html';

import editHarmonisationModalController from
  './edit-harmonisation-date-modal.controller';

import editRuleSetTemplateUrl from
  './partials/edit-rule-set.html';

import editRuleSetModalController from
  './edit-rule-set-modal.controller';

import removeProductTemplateUrl from
  './partials/remove-product.html';

import removeServicesTemplateUrl from
  './partials/remove-services.html';

import removeServicesModalController from
  './remove-services-modal.controller';

export default class ListServiceScheduleController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    $state,
    $log,
    assetDecisionService,
    pageService,
    serviceScheduleService,
    dateFormat,
    moment,
    MastModalFactory,
    asset,
    refRulesetCategories,
    assignedServices,
    assignedProducts,
    serviceCatalogues,
    serviceRelationshipType,
    productCatalogues,
    refProductTypes,
    productType,
    serviceType
  ) {
    super(arguments);

    // Constants - Reference data IDs considered stable.
    this.constants = {
      productType: this._productType.toObject(),
      serviceType: this._serviceType.toObject(),
      relationshipType: this._serviceRelationshipType.toObject()
    };

    // Product Types are filtered by the productTypeWhitelist
    // because these will become the tabs in the UI, which per
    // the spec are fewer than those available in the data.
    this.productTypes = _.filter(
      _.get(this._refProductTypes, 'product.productTypes'),
      (type) => _.includes([
        this.constants.productType.classification,
        this.constants.productType.statutory
      ], type.id));

    this.tabOptions = _.map(this.productTypes, (type) =>
      Reflect.construct(Model, [type]));

    this.viewByOptions = [
      { id: 1, name: 'Product', slug: 'product' },
      { id: 2, name: 'Service Type', slug: 'service_type' }
    ];

    this.listConfig = {};

    const defaultTabPicker = _.isNumber($stateParams.productTypeId) ?
      (tab) => tab.model.id === $stateParams.productTypeId :
      (tab) => tab.model.id === this.constants.productType.classification;

    this.listConfig.currentTab = _.find(this.tabOptions, defaultTabPicker);
    this.listConfig.viewBy = _.first(this.viewByOptions);

    // Shadow of asset.harmonisationDate, which is in ISO 8601 format
    this.listConfig.harmonisationDate =
      _.cloneDeep(this.asset.harmonisationDate);

    // Shadow of asset.productRuleSet
    this.listConfig.ruleSet =
      _.cloneDeep(this.asset.productRuleSet);

    this.models = {};
    this.models.assignedServices = _.map(this.assignedServices, (service) => {
      const model = Reflect.construct(ServiceModel, [service, moment]);
      model._serviceCatalogue = _.find(this.serviceCatalogues, {
        id: _.get(service, 'serviceCatalogueId')
      });
      return model;
    });

    this.models.serviceCatalogues =
      _.map(this.serviceCatalogues, (serviceCatalogue) => {
        const model = Reflect.construct(ServiceCatalogueModel, [
          serviceCatalogue
        ]);

        return _.set(model, 'relationshipType',
          this.constants.relationshipType);
      });

    this.shouldShowAddProductsButton();
    this.shouldShowDeleteServicesButton();
  }

  get asset() {
    return this._asset;
  }

  get isCheckedOut() {
    return this._pageService.isCheckedOut(this._asset);
  }

  get rulesetCategories() {
    return _.get(this._refRulesetCategories, 'asset.rulesetCategories');
  }

  get assignedServices() {
    return this._assignedServices;
  }

  get assignedProducts() {
    return this._assignedProducts;
  }

  get serviceCatalogues() {
    return this._serviceCatalogues;
  }

  /**
   * @param {String} productTypeId - Product Type ID
   * @returns {Array} Products filtered by Product Type ID
   */
  assignedProductsByType(productTypeId) {
    const products = _.filter(this.assignedProducts, (product) =>
      _.get(product, 'productType.id') === productTypeId);

    return products;
  }

  /**
   * @returns {Array} Service Types derived from assigned services
   */
  get assignedServiceTypes() {
    const uniqueAssignedServiceCatalogueIDs =
      _.uniq(_.map(this.assignedServices, 'serviceCatalogueId'));

    const assignedServiceCatalogues =
      _.filter(this.serviceCatalogues, (serviceCatalogue) =>
        _.includes(uniqueAssignedServiceCatalogueIDs, serviceCatalogue.id));

    const serviceTypes = _.map(assignedServiceCatalogues,
      (serviceCatalogue) => _.get(serviceCatalogue, 'serviceType'));

    const uniqueServiceTypes = _.uniqBy(serviceTypes,
      (serviceType) => _.get(serviceType, 'id'));

    return uniqueServiceTypes;
  }

  async editHarmonisationDate() {
    try {
      const response = await new this._MastModalFactory({
        controller: editHarmonisationModalController,
        controllerAs: 'vm',
        templateUrl: editHarmonisationTemplateUrl,
        scope: {
          title: 'Edit Harmonisation Date'
        },
        class: 'dialog',
        inject: {
          harmonisationDate: this.listConfig.harmonisationDate,
          assetBuildDate: this.asset.buildDate,
          dateFormat: this.dateFormat
        }
      }).activate();

      this.listConfig.harmonisationDate = response.toISOString();

      // Update the asset's harmonisation date to reflect this choice.
      this.asset.harmonisationDate =
        _.cloneDeep(this.listConfig.harmonisationDate);

      await this._assetDecisionService.update(this.asset);

      // Having updated the asset, the server should satisfy the criteria
      // below and it should be sufficient for the front end to reload
      // its state.
      this.reloadCurrentTab();
    } catch (e) {
      if (e) {
        this._$log.error(e);
      }
    }
  }

  /*
   * NB: Weirdly a single Ruleset Category is known in the UI
   *     as simply a "Rule set" and this is reflected here:
   *       - We supply many ruleset categories from which to choose
   *       - The chosen ruleset category is known as the "ruleSet"
   */
  async editRuleSet() {
    try {
      const response = await new this._MastModalFactory({
        controller: editRuleSetModalController,
        controllerAs: 'vm',
        templateUrl: editRuleSetTemplateUrl,
        scope: {
          title: 'Edit Rule Set'
        },
        class: 'dialog',
        inject: {
          ruleSet: this.listConfig.ruleSet,
          availableOptions: this.rulesetCategories
        }
      }).activate();

      // The response is an array of chosen rules
      // (of which there should only be one)
      this.listConfig.ruleSet = _.head(response);
      this.shouldShowAddProductsButton();

      this.asset.productRuleSet =
        _.cloneDeep(this.listConfig.ruleSet);

      await this._assetDecisionService.update(this.asset);
    } catch (e) {
      if (e) {
        this._$log.error(e);
      } else {
        this._$log.debug('editRuleSet() Modal dismissed');
      }
    }
  }

  /**
   * A schedule is empty if it has no products and no services.
   *
   * @returns {Boolean} 'Empty Schedule' state
   */
  isEmptySchedule() {
    return _.isEmpty(this.assignedServices) &&
      _.isEmpty(this.assignedProducts);
  }

  /**
   * A 'Products Only' schedule has products assigned to it but no
   * services assigned to any of the products
   *
   * @returns {Boolean} 'Products Only' state
   */
  isProductsOnlySchedule() {
    return _.isEmpty(this.assignedServices) &&
      !_.isEmpty(this.assignedProducts);
  }

  /**
   * List all the services assigned to a product.
   *
   * @param {Number} productId - ID of Product to filter by
   * @returns {Array} Service Models
   */
  productServiceModels(productId) {
    this._productServiceCache = this._productServiceCache || {};

    const productCatalogueId = _.get(
      _.find(this.assignedProducts, { id: productId }),
      'productCatalogueId');

    if (!this._productServiceCache[productId]) {
      this._productServiceCache[productId] = _.filter(this.serviceCatalogues, {
        productCatalogue: { id: productCatalogueId }
      });
    }

    const assignedServiceModels = _.filter(
      this.models.assignedServices, (serviceModel) =>
        _.some(this._productServiceCache[productId], {
          id: serviceModel.serviceCatalogueId
        }));

    return assignedServiceModels;
  }

  /**
   * List all the services of a given Service Type
   *
   * @param {Number} serviceTypeId - ID of Service Type to filter by
   * @returns {Array} Services
   */
  serviceTypeServiceModels(serviceTypeId) {
    const serviceCataloguesOfType =
      _.filter(this.serviceCatalogues, { serviceType: { id: serviceTypeId } });

    const productTypeId = this.listConfig.currentTab.model.id;

    const serviceCataloguesOfProduct = _.filter(serviceCataloguesOfType, {
      productCatalogue: { productType: { id: productTypeId } }
    });

    const serviceCatalogueIds = _.map(serviceCataloguesOfProduct, 'id');

    const assignedServiceModels =
      _.filter(this.models.assignedServices, (serviceModel) =>
        _.includes(serviceCatalogueIds, serviceModel.serviceCatalogueId));

    return assignedServiceModels;
  }

  /**
   * Gets the Service Catalogue model for the given Service model
   *
   * @param {Object} serviceModel - Service  to examine
   * @returns {Object} Service Catalogue Model
   */
  serviceCatalogueModel(serviceModel) {
    return _.find(this.models.serviceCatalogues, (serviceCatalogueModel) =>
      serviceCatalogueModel.id === serviceModel.serviceCatalogueId);
  }

  /**
   * @param {Object} tab - Product type tab
   */
  selectTab(tab) {
    this.listConfig.currentTab = tab;
    this.shouldShowAddProductsButton();
  }

  /**
   * @param {Object} tab - Product type tab
   * @returns {Boolean} Selected state
   */
  isTabSelected(tab) {
    return tab === this.listConfig.currentTab;
  }

  /**
   * @param {Object} ruleSet - Chosen ruleset
   * @returns {String} Name of the chosen ruleset or '-'
   */
  ruleName(ruleSet) {
    return _.get(ruleSet, 'name') || '-';
  }

  /**
   * @param {String} mode - To select (all) or clear (none)
   */
  selectServices(mode) {
    mode = mode === 'all';

    _.forEach(this.models.assignedServices, (serviceModel) =>
      _.set(serviceModel, 'selected', mode));

    this.shouldShowDeleteServicesButton();
  }

  selectServiceModel(serviceModel) {
    this.shouldShowDeleteServicesButton();
  }

  /**
   * Updates this.listConfig.showAddProductsButton
   */
  shouldShowAddProductsButton() {
    let state = false;

    // Show the Add Product button when viewing by Product
    if (this.listConfig.viewBy.id === 1) {
      state = true;
    }

    const tabId = this.listConfig.currentTab.model.id;

    // Further to that, if we're looking at the Classification tab only
    // enable the Add Product button if a ruleset has been chosen.
    this.listConfig.enableAddProductsButton = true;
    if (state && tabId === this.constants.productType.classification) {
      this.listConfig.enableAddProductsButton =
        !_.isEmpty(this.listConfig.ruleSet);
    }

    this.listConfig.showAddProductsButton = state;
  }

  shouldShowDeleteServicesButton() {
    this.listConfig.showDeleteServicesButton =
      _.some(this.models.assignedServices, { selected: true });
  }

  /**
   * Update the display when the view mode changes.
   */
  viewByChanged() {
    this.shouldShowAddProductsButton();
  }

  /**
   * Like $state.reload() but adds the productTypeId into the state
   * so that the reload comes back to the save Product Type tab.
   */
  reloadCurrentTab() {
    const params = _.merge(this._$stateParams, {
      productTypeId: _.get(this.listConfig.currentTab, 'model.id')
    });

    this._$state.transitionTo(this._$state.current, params, {
      reload: true, inherit: false, notify: true
    });
  }

  async deleteSelectedServices() {
    const selected = _.filter(this.models.assignedServices, 'selected');

    // find dependent services.
    const dependentServiceCatalogueIds = _.reduce(selected, (res, s) => {
      const relationships = _.get(s, '_serviceCatalogue.relationships');

      // filter out non-dependent services.
      const matches = _.filter(relationships, (relation) =>
        relation.relationshipType.id === 1 &&
        relation.fromServiceCatalogue.id !== s._serviceCatalogue.id)
        .map((x) => x.fromServiceCatalogue.id);

      if (!_.isEmpty(matches)) {
        res.push(...matches);
      }
      return res;
    }, []);

    // an array of models to remove
    const dependentsToRemove = _.filter(this.models.assignedServices, (x) =>
      dependentServiceCatalogueIds.includes(x.serviceCatalogueId) &&
        !x.selected);

    try {
      await new this._MastModalFactory({
        controller: removeServicesModalController,
        controllerAs: 'vm',
        templateUrl: removeServicesTemplateUrl,
        scope: {
          title: 'You have selected to remove services:'
        },
        class: 'dialog',
        inject: {
          selectedNames: _.map(selected, '_serviceCatalogue.name'),
          relatedNames: _.map(dependentsToRemove, '_serviceCatalogue.name')
        }
      }).activate();

      await this._serviceScheduleService
        .removeAssignedAssetServices({
          assetId: this.asset.id
        }, [...selected, ...dependentsToRemove].map((x) => x.id));

      this.reloadCurrentTab();
    } catch (e) {
      if (e) {
        this._$log.error(e);
      } else {
        this._$log.debug('deleteSelectedServices() Modal dismissed');
      }
    }
  }

  /**
   * Requires that a product be deletable when it has no services associated
   * with it.
   *
   * @param {Object} product - Product to delete
   */
  async deleteProduct(product) {
    try {
      await new this._MastModalFactory({
        templateUrl: removeProductTemplateUrl,
        scope: {
          title: 'You have selected to remove a product'
        },
        class: 'dialog',
        inject: {
          harmonisationDate: this.listConfig.harmonisationDate,
          assetBuildDate: this.asset.buildDate,
          dateFormat: this.dateFormat
        }
      }).activate();

      const stripRestangular = (object) =>
        object.restangularized ? object.plain() : object;

      await this._serviceScheduleService
        .updateAssignedAssetProducts({
          assetId: this.asset.id
        }, {
          base: _.map(this.assignedProducts, stripRestangular),
          toRemove: _.map([ product ], stripRestangular)
        });

      this.reloadCurrentTab();
    } catch (e) {
      if (e) {
        this._$log.error(e);
      } else {
        this._$log.debug('deleteSelectedServices() Modal dismissed');
      }
    }
  }

  /**
   * Configure and navigate to the addProduct state
   */
  addProduct() {
    const params = _.merge(this._$stateParams, {
      productTypeId: _.get(this.listConfig.currentTab, 'model.id'),
      rulesetId: _.get(this.listConfig.ruleSet, 'id')
    });

    this._$state.go('asset.serviceSchedule.addProduct', params);
  }

  /**
   * Configure and navigate to the addService state
   *
   * @param {Object} product Product that will have a service added to it
   */
  addService(product) {
    const params = _.merge(this._$stateParams, {
      productTypeId: _.get(this.listConfig.currentTab, 'model.id'),
      productId: _.get(product, 'id')
    });

    this._$state.go('asset.serviceSchedule.addService', params);
  }

  /**
   * @param {object} product the product to check
   * @returns {boolean} true if the given product is statutory
   */
  isStatutoryProduct(product) {
    return _.get(product, 'productType.id') ===
      this.constants.productType.statutory;
  }

  /**
   * Predicate used to determine if all the available services have been
   * assigned to a product.
   *
   * @param {object} prod the product to check
   * @return {boolean} true if product has available services
   */
  hasUnassignedServices(prod) {
    const assignedIds = _.map(this.productServiceModels(prod.id),
        'serviceCatalogueId');
    const available = _.filter(this.models.serviceCatalogues, (catalogue) => {
      // filter out any service catalogues not available to this product
      if (catalogue.productCatalogue.id !== prod.productCatalogueId) {
        return false;
      }

      // filter out any already assigned services
      if (_.includes(assignedIds, catalogue.id)) {
        return false;
      }

      // filter out statutory product services that dont share the same
      // scheduling regime.
      if (this.isStatutoryProduct(prod) &&
        !catalogue.isSchedulingRegime(_.get(prod, 'schedulingRegime.id'))) {
        return false;
      }

      if (!_.isEmpty(catalogue.counterparts)) {
        // if this service has a counterpart, check whether the counterpart
        // has already been added, if it has - don't add this as they are
        // mutually exclusive.
        if (_.isEmpty(_.difference(catalogue.counterparts, assignedIds))) {
          return false;
        }
      }

      // filter out any misc services
      if (catalogue.isType(this.constants.serviceType.miscellaneous)) {
        return false;
      }

      return catalogue.cyclePeriodicity;
    });

    return !_.isEmpty(available);
  }

  /**
   * @return {String} dateFormat The format of dates, from the constants;
   */
  get dateFormat() {
    return this._dateFormat;
  }
}
