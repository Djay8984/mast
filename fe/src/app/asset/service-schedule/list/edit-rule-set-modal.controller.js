import * as _ from 'lodash';

import Base from 'app/base.class';

import Model from 'app/models/model.class';

export default class EditRuleSetModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    ruleSet,
    availableOptions
  ) {
    super(arguments);

    this.setupRuntime();
  }

  /**
   * Wraps all the available options in a Model so that the 'selected' attribute
   * can be added to each option with polluting it.
   *
   * Each runtimeOption.model is the original available option.
   *
   * When invoked with a currently chosen ruleSet, this will be selected.
   *
   * The selected options will be unwrapped by confirm()
   */
  setupRuntime() {
    const rulesetId = _.get(this._ruleSet, 'id');
    this.runtimeOptions = _.map(this._availableOptions, (opt) =>
      _.set(new Model(opt), 'selected', rulesetId === opt.id));
  }

  /**
   * Deselect all options other than the one selected.
   *
   * @param {Object} option - The currently selected option
   */
  singleSelect(option) {
    _.forEach(_.reject(this.runtimeOptions, option),
      (opt) => _.set(opt, 'selected', false));
  }

  /*
   * Unwrap the selected options and pass these back on close.
   *
   * See setupRuntime() for wrapping details.
   */
  confirm() {
    const selected = _.filter(this.runtimeOptions, 'selected');
    this._$modalInstance.close(_.map(selected, 'model'));
  }
}
