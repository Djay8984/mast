import Base from 'app/base.class';

export default class RemoveServicesModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    selectedNames,
    relatedNames
  ) {
    super(arguments);
  }

  get selectedNames() {
    return this._selectedNames;
  }

  get relatedNames() {
    return this._relatedNames;
  }

  confirm() {
    this._$modalInstance.close(true);
  }
}
