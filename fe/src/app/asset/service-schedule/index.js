import * as angular from 'angular';

import routes from './config.routes';

export default angular
  .module('app.asset.service-schedule', [])
  .config(routes)
  .name;

export controller from './service-schedule.controller';
export templateUrl from './service-schedule.html';

export const resolve = {

  /* @ngInject */
  refRulesetCategories: (referenceDataService) =>
    referenceDataService.get(['asset.rulesetCategories']),

  /* @ngInject */
  assignedServices: (serviceScheduleService, asset) =>
    serviceScheduleService
      .getAssignedAssetServices({ assetId: asset.id }),

  /* @ngInject */
  assignedProducts: (serviceScheduleService, asset) =>
    serviceScheduleService
      .getAssignedAssetProducts({ assetId: asset.id }),

  /* @ngInject */
  productCatalogues: (referenceDataService) =>
    referenceDataService.get(['product.productCatalogues']),

  /* @ngInject */
  refProductTypes: (referenceDataService) =>
    referenceDataService.get(['product.productTypes']),

  /* @ngInject */
  serviceCatalogues: (referenceDataService) =>
    referenceDataService.get('service.serviceCatalogues'),

  /* @ngInject */
  serviceCreditStatuses: (referenceDataService) =>
    referenceDataService.get(['service.serviceCreditStatuses'])
};
