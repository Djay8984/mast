import * as _ from 'lodash';

import Item from 'app/models/item.model';

import ListServiceScheduleController
  from './list/list.controller';
import listServiceScheduleTemplateUrl
  from './list/list.html';

import AddProductController
  from './add-product/add-product.controller';
import addProductTemplateUrl
  from './add-product/add-product.html';

import AddServiceController
  from './add-service/add-service.controller';
import addServiceTemplateUrl
  from './add-service/add-service.html';

import AddServiceSpecifyDetailsController
  from './add-service/specify-details/specify-details.controller';
import addServiceSpecifyDetailsTemplateUrl
  from './add-service/specify-details/specify-details.html';

import ViewServiceController
  from './view-service/view-service.controller';
import viewServiceTemplateUrl
  from './view-service/view-service.html';

import EditServiceController
  from './edit-service/edit-service.controller';
import editServiceTemplateUrl
  from './edit-service/edit-service.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('asset.serviceSchedule.list', {
      parent: 'asset.serviceSchedule',
      url: '/list',
      params: {
        productTypeId: null
      },
      header: { open: true },
      templateUrl: listServiceScheduleTemplateUrl,
      controller: ListServiceScheduleController,
      controllerAs: 'vm'
    })
    .state('asset.serviceSchedule.addProduct', {
      parent: 'asset.serviceSchedule',
      url: '/add-product',
      params: {
        productTypeId: null,
        rulesetId: null
      },
      views: {
        '@main': {
          templateUrl: addProductTemplateUrl,
          controller: AddProductController,
          controllerAs: 'vm'
        }
      }
    })
    .state('asset.serviceSchedule.addService', {
      parent: 'asset.serviceSchedule',
      url: '/add-service/{productId:int}',
      params: {
        productTypeId: null
      },
      resolve: {

        /* @ngInject */
        product: (assignedProducts, $stateParams) =>
          _.find(assignedProducts, { id: $stateParams.productId }),

        productServiceCatalogues: (product, serviceCatalogues) =>
          _.filter(serviceCatalogues, {
            'productCatalogue': { id: product.productCatalogueId }
          })
      },
      views: {
        '@main': {
          templateUrl: addServiceTemplateUrl,
          controller: AddServiceController,
          controllerAs: 'vm'
        }
      }
    })
    .state('asset.serviceSchedule.addService.specifyDetails', {
      url: '/add-service/{productId:int}/specify-details',
      params: {
        productTypeId: null,
        serviceCatalogues: null
      },
      resolve: {

        /* @ngInject */
        rootItem: (asset, assetModelDecisionService) =>
          assetModelDecisionService.getRootItem(asset.id),

        /* @ngInject */
        rootItems: (asset, rootItem, assetModelDecisionService) =>
          assetModelDecisionService.getItems(asset.id,
            _.map(rootItem.items, 'id')),

        /* @ngInject */
        assetModel: (rootItem, rootItems) =>
          _.set(new Item(rootItem), 'items', rootItems),

        /* @ngInject */
        refServicesStatuses: (referenceDataService) =>
          referenceDataService.get('service.serviceStatuses')
      },
      views: {
        '@main': {
          templateUrl: addServiceSpecifyDetailsTemplateUrl,
          controller: AddServiceSpecifyDetailsController,
          controllerAs: 'vm'
        }
      }
    })
    .state('asset.serviceSchedule.viewService', {
      parent: 'asset.serviceSchedule',
      url: '/view-service/{serviceId:int}',
      params: {
        productTypeId: null
      },
      resolve: {

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          attachmentDecisionService.getAttachmentCount(
            `asset/${$stateParams.assetId}/service/${$stateParams.serviceId}`),

        /* @ngInject */
        attachmentPath: ($stateParams) =>
          `asset/${$stateParams.assetId}/service/${$stateParams.serviceId}`,

        /* @ngInject */
        tasks: ($stateParams, taskOnlineService) =>
          taskOnlineService.query(
            { scheduledServiceId: [$stateParams.serviceId] }, {},
            { hydrateItems: true, assetId: $stateParams.assetId })
      },
      views: {
        '@main': {
          templateUrl: viewServiceTemplateUrl,
          controller: ViewServiceController,
          controllerAs: 'vm'
        }
      }
    })
    .state('asset.serviceSchedule.editService', {
      parent: 'asset.serviceSchedule',
      url: '/edit-service/{serviceId:int}',
      params: {
        productTypeId: null
      },
      resolve: {

        /* @ngInject */
        refServicesStatuses: (referenceDataService) =>
          referenceDataService.get(['service.serviceStatuses']),

        /* @ngInject */
        service: (assignedServices, $stateParams) =>
          _.find(assignedServices, { id: $stateParams.serviceId })
      },
      views: {
        '@main': {
          templateUrl: editServiceTemplateUrl,
          controller: EditServiceController,
          controllerAs: 'vm'
        }
      }
    });
}
