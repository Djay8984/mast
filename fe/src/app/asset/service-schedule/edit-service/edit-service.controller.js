import * as _ from 'lodash';

import BaseForm from 'app/base-form.class';

export default class EditServiceController extends BaseForm {
  /* @ngInject */
  constructor(
    $stateParams,
    $state,
    $scope,
    serviceScheduleService,
    moment,
    dateFormat,
    asset,
    assignedServices,
    service,
    serviceCatalogues,
    productCatalogues,
    refServicesStatuses,
    serviceStatus,
    serviceType
  ) {
    super(arguments);

    this.setUpData();

    $scope.$watch('vm.editConfig.dueDate', (newValue) => {
      if (!newValue) {
        this.editConfig.lowerRangeDate = null;
        this.editConfig.upperRangeDate = null;
        this.form.lowerRangeDatePicker.$setDirty();
        this.form.upperRangeDatePicker.$setDirty();
      }
    }, true);

    $scope.$watch('vm.editConfig.assignedDate', (newValue) => {
      const { dueDate } = this.editConfig;

      if (dueDate) {
        this.form.dueDatePicker.$setValidity('min',
          !this._moment(dueDate).isBefore(newValue));
      }
    });
  }

  setUpData() {
    this.constants = {
      serviceStatus: this._serviceStatus.toObject(),
      serviceType: this._serviceType.toObject()
    };

    // There is no reference data for a service's provisional status, a service
    // either has this flag or it doesn't, so for the sake of the UI we create
    // these two choices.
    this._provisionalStatuses = [
      { id: 1, name: 'None', slug: 'none' },
      { id: 2, name: 'Provisional', slug: 'provisional' }
    ];

    this.editConfig = {};

    this.editConfig.assignedDate = !this._service.assignedDateManual ?
      this._service.assignedDate : this._service.assignedDateManual;

    this.editConfig.dueDate = this._service.dueDateManual ||
      this._service.dueDate;

    this.editConfig.lowerRangeDate = this._service.lowerRangeDateManual ||
      this._service.lowerRangeDate;

    this.editConfig.upperRangeDate = this._service.upperRangeDateManual ||
      this._service.upperRangeDate;

    this.editConfig.cyclePeriodicity = this.service.cyclePeriodicity;
    this.editConfig.lastCreditedJob = _.parseInt(this.service.lastCreditedJob);
    this.editConfig.serviceStatus = _.get(this.service, 'serviceStatus.id');

    this.editConfig.provisional = _.get(this.service, 'provisional') ?
      _.get(_.find(this.provisionalStatuses, { slug: 'provisional' }), 'id') :
      _.get(_.find(this.provisionalStatuses, { slug: 'none' }), 'id');

    this.editConfig.lastPartheldJob = _.parseInt(this.service.lastPartheldJob);
    this.editConfig.completionDate = this.service.completionDate;
  }

  get asset() {
    return this._asset;
  }

  get service() {
    return this._service;
  }

  get serviceCatalogues() {
    return this._serviceCatalogues;
  }

  // Gets the service catalogue for this service
  get serviceCatalogue() {
    const serviceCatalogueId = this.service.serviceCatalogueId;

    return _.find(this.serviceCatalogues, { id: serviceCatalogueId });
  }

  get serviceTypeId() {
    return _.get(this.serviceCatalogue, 'serviceType.id');
  }

  get productCatalogue() {
    return _.find(this._productCatalogues, {
      id: this.serviceCatalogue.productCatalogue.id
    });
  }

  get serviceStatuses() {
    const displayable = [
      this.constants.serviceStatus.notStarted,
      this.constants.serviceStatus.partHeld,
      this.constants.serviceStatus.held
    ];

    return _.filter(_.get(this._refServicesStatuses, 'service.serviceStatuses'),
      (status) => _.includes(displayable, status.id));
  }

  get provisionalStatuses() {
    return this._provisionalStatuses;
  }

  get isHarmonised() {
    return !_.isNull(_.get(this.serviceCatalogue, 'harmonisationType'));
  }

  get hasHarmonisationDate() {
    return !_.isNull(_.get(this.asset, 'harmonisationDate'));
  }

  /**
   * @param {object} service - the service we're inspecting
   * @returns {boolean} to disable lower range or not
   */
  disableLowerRange() {
    if (!this.serviceCatalogue.lowerRangeDateOffsetMonths) {
      return true;
    }

    return _.isEmpty(this.editConfig.dueDate);
  }

  /**
   * @param {object} service - the service we're inspecting
   * @returns {boolean} to disable upper range or not
   */
  disableUpperRange() {
    if (!this.serviceCatalogue.upperRangeDateOffsetMonths) {
      return true;
    }

    return _.isEmpty(this.editConfig.dueDate);
  }

  /**
   * Check to see if the assigned date field should be disabled
   *
   * @returns {boolean}
   */
  get isAssignedDateDisabled() {
    if (this.isHarmonised && !this.hasHarmonisationDate) {
      return true;
    }

    if (this.serviceTypeId === this._serviceType.get('intermediate')) {
      return !_.isEmpty(this.editConfig.dueDate);
    }

    return false;
  }

  /**
   * Check to see if the due date field should be disabled.
   *
   * @returns {boolean}
   */
  get isDueDateDisabled() {
    if (this.isHarmonised && !this.hasHarmonisationDate) {
      return true;
    }

    // if intermediate, user can specify either an assigned date OR
    // a due date, not both.
    if (this.serviceTypeId === this._serviceType.get('intermediate')) {
      return !_.isEmpty(this.editConfig.assignedDate);
    }

    // if renewal, user can optionally specify a due date once the
    // assigned date is selected.
    if (this.serviceTypeId === this._serviceType.get('renewal')) {
      return _.isEmpty(this.editConfig.assignedDate);
    }

    return false;
  }

  /**
   * @summary checks if the assigned date should be marked as
   * required. Should only be TRUE for non-harmonised renewals.
   * @returns {boolean}
   */
  get isAssignedDateRequired() {
    return !this.isHarmonised && this.serviceTypeId ===
      this._serviceType.get('renewal');
  }

  // @see LRC-261
  hasAssociatedItem() {
    return false;
  }

  revealHeldStatusElaboration() {
    const serviceStatusId = _.parseInt(this.editConfig.serviceStatus);

    return serviceStatusId === this.constants.serviceStatus.partHeld ||
      serviceStatusId === this.constants.serviceStatus.held;
  }

  get isPeriodicityEditable() {
    return Boolean(_.get(this.serviceCatalogue, 'cyclePeriodicityEditable'));
  }

  displayName() {
    const serviceCatalogue = this.serviceCatalogue;
    const code = _.get(serviceCatalogue, 'code');
    const name = _.get(serviceCatalogue, 'name');
    return _.compact([ code, name ]).join(' - ');
  }

  /**
   * Returns the min selectable value for the due-date datepicker. It
   * must be greater than the assigned date, if set.
   *
   * @returns {object} moment
   */
  get minDueDate() {
    return this.editConfig.assignedDate ?
      this._moment(this.editConfig.assignedDate).add(1, 'day') :
        this._moment();
  }

  /**
   * Returns the max assigned date, which is 3 months in the future.
   *
   * @returns {object} moment
   */
  get maxAssignedDate() {
    if (!this._maxAssignedDate) {
      this._maxAssignedDate = this._moment().add(3, 'months');
    }

    return this._maxAssignedDate;
  }

  /**
   * Determines the min selectable date for the lower range datepicker.
   *
   * @returns {object} moment
   */
  get minLowerRangeDate() {
    const { assignedDate } = this.editConfig;

    return assignedDate ? this._moment(assignedDate).add(1, 'day') :
      this._moment();
  }

  /**
   * Determines the max selectable date for the lower range datepicker.
   *
   * @returns {object} moment
   */
  get maxLowerRangeDate() {
    const { dueDate } = this.editConfig;

    return dueDate ? dueDate : this._moment().add(3, 'months');
  }

  /**
   * Determines the min selectable date for the upper range datepicker.
   *
   * @return {object} moment
   */
  get minUpperRangeDate() {
    return this._moment(this.editConfig.dueDate).add(1, 'day');
  }

  validForm() {
    return this.form.$valid && !this.form.$pristine;
  }

  back(shouldReload = false) {
    const params = _.merge(this._$stateParams, {
      productTypeId: this._$stateParams.productTypeId,
      serviceId: _.get(this.service, 'id')
    });

    this._$state.go('asset.serviceSchedule.viewService',
      params,
      { reload: shouldReload });
  }

  async save() {
    // Modified to use this instead of a const;
    this.finalService = this._service.plain();

    const {
      assignedDate,
      dueDate,
      lowerRangeDate,
      upperRangeDate
    } = this.editConfig;

    if (!this.form.assignedDatePicker.$pristine) {
      _.set(this.finalService, 'assignedDateManual',
        !_.isEmpty(assignedDate) ? assignedDate.toISOString() : null);
    }

    if (!this.form.dueDatePicker.$pristine) {
      _.set(this.finalService, 'dueDateManual',
        !_.isEmpty(dueDate) ? dueDate.toISOString() : null);
    }

    if (!this.form.lowerRangeDatePicker.$pristine) {
      _.set(this.finalService, 'lowerRangeDateManual',
        !_.isEmpty(lowerRangeDate) ? lowerRangeDate.toISOString() : null);
    }

    if (!this.form.upperRangeDatePicker.$pristine) {
      _.set(this.finalService, 'upperRangeDateManual',
        !_.isEmpty(upperRangeDate) ? upperRangeDate.toISOString() : null);
    }

    _.set(this.finalService, 'completionDate', this.getCompletionDate());

    if (!this.form.periodicityNumber.$pristine) {
      _.set(this.finalService, 'cyclePeriodicity',
        this.editConfig.cyclePeriodicity);
    }

    _.set(this.finalService, 'lastCreditedJob',
      this.editConfig.lastCreditedJob ?
        this.editConfig.lastCreditedJob.toString() : null);

    _.set(this.finalService, 'serviceStatus',
      { id: _.parseInt(this.editConfig.serviceStatus) });

    _.set(this.finalService, 'lastPartheldJob',
      this.editConfig.lastPartheldJob && this.revealHeldStatusElaboration() ?
        this.editConfig.lastPartheldJob.toString() : null);

    _.set(this.finalService, 'provisional',
      _.get(
        _.find(this._provisionalStatuses, {
          id: _.parseInt(this.editConfig.provisional)
        }),
        'slug') === 'provisional' || null);

    await this._serviceScheduleService
      .updateAssignedAssetService({
        assetId: this.asset.id,
        serviceId: this.finalService.id
      }, this.finalService);

    this.back(true);
  }

  /**
   * @return {String} dateFormat The format of dates, from the constants;
   */
  get dateFormat() {
    return this._dateFormat;
  }

  getCompletionDate() {
    if (this.editConfig.completionDate &&
      this.revealHeldStatusElaboration()) {
      return _.isString(this.editConfig.completionDate) ?
          this.editConfig.completionDate :
            this.editConfig.completionDate.format('YYYY-MM-DD');
    }
    return null;
  }
}
