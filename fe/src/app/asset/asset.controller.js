import Base from 'app/base.class';

export default class AssetController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state
  ) {
    super(arguments);

    this.tabs = [
      {
        name: 'Asset model',
        route: 'asset.assetModel.view',
        offline: true
      },
      {
        name: 'Service schedule',
        route: 'asset.serviceSchedule.list',
        offline: false
      },
      {
        name: 'Cases',
        route: 'asset.cases.list',
        offline: false
      },
      {
        name: 'Jobs',
        route: 'asset.jobs.list',
        offline: true
      },
      {
        name: 'Codicils and defects',
        route: 'asset.codicilsAndDefects',
        offline: true
      }
    ];
  }

  go(state) {
    this._$state.go(state);
  }

  isActive(tab) {
    return this._$state.includes(tab.route);
  }

  isEnabled(tab) {
    return tab.offline || !this._$rootScope.OFFLINE;
  }
}
