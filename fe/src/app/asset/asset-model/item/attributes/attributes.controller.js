import * as _ from 'lodash';

import BaseForm from 'app/base-form.class';

import Attribute from 'app/models/attribute.model';

import AttributeSelectorDialogController from
  './add-attributes/attribute-selector.controller';
import attributeSelectorDialogTemplate from
  './add-attributes/attribute-selector.html';

import discardChangesDialogTemplate from
  './add-attributes/discard-changes-dialog.html';

import ItemNameUpdatedDialogController from
  './add-attributes/item-name-updated-dialog.controller';
import itemNameUpdatedDialogTemplate from
  './add-attributes/item-name-updated-dialog.html';

import contentForNavigationConfirmationTemplate from
  'app/components/modals/templates/confirm-navigation.html';

export default class AttributesController extends BaseForm {
  /* @ngInject */
  constructor(
    $log,
    $state,
    $timeout,
    attributeDecisionService,
    availableAttributes,
    asset,
    item,
    assetModelDecisionService,
    MastModalFactory,
    navigationConfirmationService,
    user
  ) {
    super(arguments);

    this.setUpCache();
    this._navigationConfirmationService
      .setConfirmationCase(
        $state, contentForNavigationConfirmationTemplate, this);
  }

  setUpCache() {
    this._itemCache = _.cloneDeep(this._item);
    this._item.validAttributes = true;
    this.initialiseAttributes();
  }

  initialiseAttributes() {
    _.forEach(this._itemCache.attributes, this.addAttribute.bind(this));
    this.orderAttributes();
  }

  addAttribute(attr, ind) {
    let attrModel = _.cloneDeep(attr);

    if (attr.constructor !== Attribute) {
      attrModel = new Attribute(attrModel);
    }

    _.set(attrModel, 'deleted', false);

    // Default value to an empty string if it doesn't already have a value
    if (_.isNull(_.get(attrModel, 'value'))) {
      _.set(attrModel, 'value', '');
    }

    if (!_.isNil(ind)) {
      this._item.attributes[ind] = attrModel;
    } else {
      this._item.attributes.push(attrModel);
    }

    this.setDeletable(attrModel);

    // Check if the attribute has an empty values
    if (!attr.value) {
      this._item.validAttributes = false;
    }
  }

  setDeletable(attrModel) {
    // If mandatory and multiple
    if (_.get(attrModel, 'attributeType.minOccurs') &&
      !_.get(attrModel, 'attributeType.maxOccurs')) {
      let deletableVal = true;

      const matchingTypes = _.filter(this._item.attributes,
        {
          deleted: false,
          attributeType: { id: _.get(attrModel, 'attributeType.id') }
        });

      if (matchingTypes.length <=
        _.get(attrModel, 'attributeType.minOccurs')) {
        deletableVal = false;
      }

      matchingTypes.forEach((matchingAttr) => {
        _.set(matchingAttr, 'isDeletable', deletableVal);
      });
    } else {
      _.set(attrModel, 'isDeletable',
        !_.get(attrModel, 'attributeType.minOccurs'));
    }
  }

  needNavConfirmation() {
    return _.get(this, 'form.$dirty');
  }

  async addNewAttributes() {
    const modal = new this._MastModalFactory({
      controller: AttributeSelectorDialogController,
      controllerAs: 'vm',
      templateUrl: attributeSelectorDialogTemplate,
      inject: {
        attributes: _.filter(this._item.attributes, { deleted: false }),
        availableAttributes: this._availableAttributes
      },
      class: 'full dialog background-sea'
    });

    const attrDialogResult = await modal.activate();
    attrDialogResult.forEach((attr) => {
      this.addAttribute({
        attributeType: attr,
        value: null
      });
    });
    this.orderAttributes();
    this._$timeout(() => this.form.$setDirty());
  }

  async save() {
    if (!this.saving) {
      this.saving = true;
      const currentItemName = _.get(this, '_item.name');

      try {
        await this._attributeDecisionService.saveAttributeList(this._asset.id,
          this._item, this._item.attributes);
      } catch (e) {
        this._$log.error(e);
        this.saving = false;
        return;
      }

      const savedItem = await this._assetModelDecisionService
        .getItem(this._asset.id, this._item.id);
      const newName = _.get(savedItem, 'name');

      if (currentItemName !== _.get(savedItem, 'name')) {
        new this._MastModalFactory({
          controller: ItemNameUpdatedDialogController,
          controllerAs: 'vm',
          templateUrl: itemNameUpdatedDialogTemplate,
          inject: {
            oldName: currentItemName,
            newName
          }
        }).activate();
      }
      Object.assign(this._item, savedItem);
      this.setUpCache();
      if (this.form) {
        this.form.$setPristine();
      }
      this.saving = false;
    }
  }

  removeAttribute(attribute) {
    _.set(attribute, 'deleted', true);
    this.setDeletable(attribute);
    this.form.$setDirty();
  }

  doDiscard() {
    this.initialiseAttributes();
    this.form.$setPristine();
  }

  async discardChanges() {
    const modal = new this._MastModalFactory({
      templateUrl: discardChangesDialogTemplate,
      class: ''
    });
    const dialogResponse = await modal.activate();
    if (dialogResponse) {
      this.doDiscard();
    }
  }

  orderAttributes() {
    this._item.attributes =
      _.orderBy(this._item.attributes,
        ['attributeType.displayOrder', 'attributeType.name'],
        ['asc', 'asc']);
  }

  get item() {
    return this._item;
  }

  get numAttributes() {
    return _.size(_.filter(this.item.attributes, { deleted: false }));
  }

  get saveButtonDisabled() {
    // All attributes on the item have been deleted
    if (!this.form && this.item.attributes.length) {
      return false;
    }

    // Default form.$pristine to true for when the page loads
    // and there are no attributes (and thus no form)
    return _.get(this, 'form.$pristine', true) || this.saving;
  }

  /**
   * @return {boolean} whether the asset is checked out by the current user
   * This code is duplicated in item.controller. It would be better if both
   * used the same bit of code.
   */
  get checkedOut() {
    return this._asset.checkedOutBy === this._user.fullName;
  }

}
