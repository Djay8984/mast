import Base from 'app/base.class';
export default class itemNameUpdatedDialogController extends Base {
  /* @ngInject */
  constructor(
    newName,
    oldName
  ) {
    super(arguments);
  }

  get newName() {
    return this._newName;
  }

  get oldName() {
    return this._oldName;
  }
}
