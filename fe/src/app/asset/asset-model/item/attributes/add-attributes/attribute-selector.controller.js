import * as _ from 'lodash';

import Base from 'app/base.class';

import Attribute from 'app/models/attribute.model';

export default class AttributeSelectorController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $modalInstance,
    attributes,
    availableAttributes
  ) {
    super(arguments);

    this.selected = [];
    this.numSelected = 0;
    this.prepareList();

    $scope.$watchGroup(
      _.map(this.attributeList, (item, index) =>
        `vm.attributeList[${index}].instances`),
      _.bind(this.refreshNumSelected, this)
    );
  }

  prepareList() {
    const tempAvailableList = _.map(this._availableAttributes,
      (attr) => new Attribute(attr));

    const attributeList = [];

    tempAvailableList.forEach((attr) => {
      const exists = _.find(this._attributes,
        { attributeType: { id: attr.id } });

      // optional and not present, or multiple allowed
      if (attr.minOccurs === 0 && !exists || attr.maxOccurs === 0) {
        add(attr, exists);
      }
    });

    this.attributeList =
      _.orderBy(attributeList, ['displayOrder', 'name'], ['asc', 'asc']);

    function add(attr, exists) {
      _.set(attr, 'instances', 0);
      if (exists) {
        _.set(attr, 'added', '(Attribute added)');
      }
      attributeList.push(attr);
    }
  }

  selectAttribute(attr) {
    _.set(attr, 'instances', Number(_.get(attr, 'selected', false)));
  }

  refreshNumSelected() {
    this.numSelected = _.sum(_.chain(this.attributeList)
      .filter('selected')
      .map('instances')
      .value());
  }

  cleanAndClose() {
    let selected = _.filter(this.attributeList,
      (attr) => attr.selected && attr.instances);
    const newSelected = [];

    // expand attr.instances so there is an Object for each occurence
    selected.forEach((attr) => {
      if (attr.instances > 1) {
        addNewSelected(attr, attr.instances - 1);
      }
    });
    selected = _.concat(selected, newSelected);

    // resolve just the model in the selected attributes
    this._$modalInstance.close(_.map(selected, 'model'));

    function addNewSelected(attr, instances) {
      if (instances) {
        newSelected.push(attr);
        addNewSelected(attr, instances - 1);
      }
    }
  }
}
