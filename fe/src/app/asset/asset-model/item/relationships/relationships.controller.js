import * as _ from 'lodash';

import Base from 'app/base.class';


import CreateRelationshipController from
  './create-relationship/create-relationship.controller';
import createRelationshipTemplate from
  './create-relationship/create-relationship.html';

export default class RelationshipsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    asset,
    assetModel,
    relationshipService,
    MastModalFactory,
    item,
    referenceData
  ) {
    super(arguments);

    this.currentItem = assetModel;

    this.associatedRelationshipType = _.find(
      referenceData.asset.itemRelationshipTypes,
      { name: 'IS RELATED TO' });

    this.buildFilteredItemRelationships();

    // look through the reference data for item type relationships allowed from
    // the current item
    this.relevantRelationships = _.filter(
      referenceData.asset.itemTypeRelationships,
      {
        fromItemType: item.itemType.id,
        assetCategoryId: asset.assetCategory.id
      });
  }

  buildFilteredItemRelationships() {
    // caches the of relations, needs to apply deletable rules
    this._filteredItemRelationships = _.forEach(_.filter(this._item.related,
      { type: { id: this.associatedRelationshipType.id } }), (relationship) =>
      relationship.deletable = true);
  }

  get filteredItemRelationships() {
    return this._filteredItemRelationships;
  }

  async create() {
    const chosenItem = await this._MastModalFactory({
      controller: CreateRelationshipController,
      controllerAs: 'vm',
      templateUrl: createRelationshipTemplate,
      inject: {
        asset: this._asset,
        assetModel: this._assetModel,
        item: this._item,
        itemTypes: this._referenceData.asset.itemTypes
      },
      class: 'full dialog background-sea'
    }).activate();

    if (chosenItem) {
      // check if chosenItem is an object as search results returns a string
      const newRelationship = await this._relationshipService
        .create(this._asset.id, this._item.id,
          _.get(chosenItem, 'id', chosenItem),
          this.associatedRelationshipType.id);

      this._$state.reload();
      if (newRelationship) {
        this._item.related.push(
          _.set(newRelationship, 'toItem.name', chosenItem.name));
        this.buildFilteredItemRelationships();
      }
    }
  }

  async remove(relation) {
    await this._relationshipService
        .remove(this._asset.id, this._item.id, relation.id);
    _.remove(this._item.related, relation);

    this.buildFilteredItemRelationships();
  }
}
