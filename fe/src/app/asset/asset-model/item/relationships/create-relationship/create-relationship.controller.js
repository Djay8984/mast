import * as _ from 'lodash';

import Base from 'app/base.class';

export default class CreateRelationshipController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $modalInstance,
    assetModelDecisionService,
    relationshipService,
    asset,
    assetModel,
    item,
    itemTypes
  ) {
    super(arguments);

    this.selectedItem = assetModel;
    this.searchResults = [];
  }

  get asset() {
    return this._asset;
  }

  get assetModel() {
    return this._assetModel;
  }

  get item() {
    return this._item;
  }

  get itemSelectionValidator() {
    return this._relationshipService.validateRelationship;
  }

  checkTerm(searchTerm) {
    const oldMode = this.searchMode;
    this.searchMode = !_.isNull(searchTerm) && this.searchMode;

    if (this.searchMode !== oldMode) {
      this.chosenItem = null;
    }
  }

  async search(searchTerm) {
    this.searchMode = !_.isEmpty(searchTerm) && searchTerm !== '*';
    this.chosenItem = null;

    if (this.searchMode) {
      this.searchResults =
        await this._assetModelDecisionService.search({
          itemName: searchTerm,
          itemTypeName: searchTerm
        }, this._asset.id);
    }
  }

  selectAndClose() {
    this._$modalInstance.close(this.chosenItem);
  }

  get validateRelationship() {
    return this._relationshipService.validateRelationship;
  }
}
