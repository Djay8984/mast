import Base from 'app/base.class';

import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class TasksController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $rootScope,
    $scope,
    asset,
    availableTasks,
    item,
    printService
  ) {
    super(arguments);
    this.printPayload = {
      ...this._asset,
      tasks: this._availableTasks,
      confidentialityType: { id: 'All' },
      additionalInfo: ['tasks']
    };
  }

  get item() {
    return this._item;
  }

  get tasks() {
    return this._availableTasks;
  }
}
