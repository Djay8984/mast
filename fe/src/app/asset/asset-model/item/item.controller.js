import * as _ from 'lodash';

import Base from 'app/base.class';

import templateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class ItemController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    asset,
    assetModelOnlineService,
    attachmentCount,
    attachmentPath,
    item,
    MastModalFactory,
    navigationService,
    settings,
    user
  ) {
    super(arguments);
    this.setup();
  }

  setup() {
    _.set(this, 'attachments.pagination.totalElements', this._attachmentCount);
  }

  back() {
    this._navigationService.back({ reload: true }, {
      originalIds: _.map(
        _.get(this._settings, 'selected.parent.parents'),
        'id').reverse(),
      view: this._settings.activeView
    });
  }

  async deleteItem() {
    try {
      // validate item before allowing delete
      await this._assetModelOnlineService.validateItemDeletable(
        this._$stateParams.assetId, this._$stateParams.itemId);

      if (await this._deleteItemConfirmModal(this._item)) {
        await this._assetModelOnlineService.deleteItem(
          this._$stateParams.assetId, this._$stateParams.itemId);
        this._item.deleted = true;
        this.back();
      }

    // cannot delete
    } catch (error) {
      await this._deleteItemFailModal(this._item);
    }
  }

  async markItemAsComplete() {
    try {
      await this._assetModelOnlineService.markItemAsComplete(
        this._$stateParams.assetId, this._$stateParams.itemId);
      this._item.reviewed = true;

    // cannot mark as complete
    } catch (error) {
      await this._markItemAsCompleteFailModal();
    }
  }

  async decommissionItem() {
    try {
      if (await this._decommissionItemConfirmModal(this._item)) {
        await this._assetModelOnlineService.decommissionItem(
          this._$stateParams.assetId, this._$stateParams.itemId);
        this._item.decommissioned = true;
      }

    // cannot delete
    } catch (error) {
      await this._decommissionItemFailModal(this._item);
    }
  }

  async recommissionItem() {
    if (await this._recommissionItemConfirmModal(this._item)) {
      this._assetModelOnlineService.recommissionItem(
        this._asset.id, this._item.id);
      this._item.decommissioned = false;
    }
  }

  async _deleteItemConfirmModal(item) {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Delete item',
        message: `Are you sure you want to delete ${item.name}?

Dependent items or tasks will also be deleted.`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Delete item', result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();
  }

  async _deleteItemFailModal(item) {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Item cannot be deleted',
        message: `${item.name} cannot be deleted because it is associated
with active services, defects or codicils, or its chldren are.`,
        actions: [ { name: 'OK', result: false } ]
      },
      class: 'dialog'
    }).activate();
  }

  async _markItemAsCompleteFailModal() {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Cannot mark as complete',
        message: `This item cannot be marked as complete because one for
more child items are not complete.`,
        actions: [ { name: 'OK', result: false } ]
      },
      class: 'dialog'
    }).activate();
  }

  async _decommissionItemConfirmModal(item) {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Decommission item',
        message: `Are you sure you want to decommission ${item.name}?

Dependent items will also be decommissioned.`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Decommission item', result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();
  }

  async _decommissionItemFailModal(item) {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Item cannot be decommissioned',
        message: `${item.name} cannot be decommissioned because it is
associated with active services, defects or codicils, or its
children are.`,
        actions: [
          { name: 'OK', result: false }
        ]
      },
      class: 'dialog'
    }).activate();
  }

  async _recommissionItemConfirmModal(item) {
    return new this._MastModalFactory({
      templateUrl,
      scope: {
        title: 'Recommission item',
        message: `Are you sure you want to recommission ${item.name}?

Dependent items will also be recommissioned.`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Recommission item', result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();
  }

  get asset() {
    return this._asset;
  }

  get attachmentPath() {
    return this._attachmentPath;
  }

  /**
   * @return {boolean} whether the asset is checked out by the current user
   */
  get checkedOut() {
    return this._asset.checkedOutBy === this._user.fullName;
  }

  get item() {
    return this._item;
  }
}
