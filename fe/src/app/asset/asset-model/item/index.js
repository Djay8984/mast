import * as angular from 'angular';

import attributes from './attributes';
import relationships from './relationships';
import tasks from './tasks';

import routes from './config.routes';

export default angular
  .module('app.asset.asset-model.item', [
    attributes,
    relationships,
    tasks
  ])
  .config(routes)
  .name;
