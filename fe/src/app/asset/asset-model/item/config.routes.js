import * as _ from 'lodash';

import Item from 'app/models/item.model';

import ItemController from './item.controller';
import itemTemplateUrl from './item.html';

import AttributesController from './attributes/attributes.controller';
import attributesTemplateUrl from './attributes/attributes.html';

import HeaderController from 'app/asset/header/asset-header.controller';
import headerTemplateUrl from 'app/asset/header/asset-header.html';

import RelationshipsController from './relationships/relationships.controller';
import relationshipsTemplateUrl from './relationships/relationships.html';

import TasksController from './tasks/tasks.controller';
import tasksTemplateUrl from './tasks/tasks.html';

/* @ngInject */
export default function config(
  $stateProvider
) {
  $stateProvider
    .state('item', {
      abstract: true,
      parent: 'asset.assetModel',
      url: '/item/{itemId:int}',
      params: {
        itemId: null,
        pager: null
      },
      resolve: {

        /* @ngInject */
        item: ($stateParams, assetModelDecisionService) =>
          assetModelDecisionService.getItem($stateParams.assetId,
            $stateParams.itemId),

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          attachmentDecisionService.getAttachmentCount(
            `asset/${$stateParams.assetId}/item/${$stateParams.itemId}`),

        /* @ngInject */
        attachmentPath: ($stateParams) =>
          `asset/${$stateParams.assetId}/item/${$stateParams.itemId}`
      },
      views: {
        '@main': {
          templateUrl: itemTemplateUrl,
          controller: ItemController,
          controllerAs: 'vm'
        }
      }
    })
    .state('item.attributes', {
      parent: 'item',
      url: '/attributes',
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        availableAttributes: ($stateParams, assetModelDecisionService) =>
          assetModelDecisionService.getAvailableAttributes(
            $stateParams.assetId, $stateParams.itemId)
      },
      views: {
        '': {
          templateUrl: attributesTemplateUrl,
          controller: AttributesController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            subHeaderOpen: ($rootScope) => $rootScope.SUBHEADER_OPEN = false,

            /* @ngInject */
            assetAttachmentCount: ($stateParams, attachmentDecisionService) =>
              $stateParams.assetId ?
                attachmentDecisionService.getAttachmentCount(
                  `asset/${$stateParams.assetId}`) : null,

            /* @ngInject */
            assetAttachmentPath: ($stateParams) =>
              `asset/${$stateParams.assetId}`
          }
        }
      }
    })
    .state('item.relationships', {
      parent: 'item',
      url: '/relationships',
      templateUrl: relationshipsTemplateUrl,
      controller: RelationshipsController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        rootItem: (asset, assetModelDecisionService) =>
          assetModelDecisionService.getRootItem(asset.id),

        /* @ngInject */
        rootItems: (asset, rootItem, assetModelDecisionService) =>
          assetModelDecisionService.getItems(asset.id,
            _.map(rootItem.items, 'id')),

        /* @ngInject */
        assetModel: (rootItem, rootItems) =>
          _.set(new Item(rootItem), 'items', rootItems),

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'asset.itemTypes',
            'asset.itemRelationshipTypes',
            'asset.itemTypeRelationships'
          ])
      }
    })
    .state('item.tasks', {
      parent: 'item',
      url: '/tasks',
      templateUrl: tasksTemplateUrl,
      controller: TasksController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        availableTasks: ($stateParams, taskOnlineService) =>
          taskOnlineService.query({ itemId: [$stateParams.itemId] },
            { size: null })
      }
    });
}
