import Base from 'app/base.class';

export default class AssetModelViewController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    assetModelDecisionService,
    asset,
    item,
    assetModel,
    settings
  ) {
    super(arguments);

    this.searchService = assetModelDecisionService;
    this.searchMode = $stateParams.searchMode;

    this._settings.activeView = this._$stateParams.view || 'list';
  }

  get activeView() {
    return this._$stateParams.view || 'list';
  }

  get searchMode() {
    return this._settings.searchMode;
  }

  set searchMode(value) {
    this._settings.searchMode = Number(value);
  }

  get asset() {
    return this._asset;
  }

  set selectedItem(value) {
    this._settings.selected = value;
  }

  get selectedItem() {
    return this._settings.selected;
  }

  get currentLevel() {
    return this._settings.currentLevel;
  }

  set currentLevel(value) {
    this._settings.currentLevel = value;
  }

  get assetModel() {
    return this._assetModel;
  }
}
