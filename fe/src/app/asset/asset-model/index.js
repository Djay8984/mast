import * as _ from 'lodash';
import * as angular from 'angular';
import Item from 'app/models/item.model';

import itemFolder from './item';
import build from './build';
import services from 'app/services';

export default angular
  .module('app.asset.asset-model', [
    build,
    itemFolder,
    services
  ])
  .name;

export controller from './asset-model.controller';
export templateUrl from './asset-model.html';

export const resolve = {

  /* @ngInject */
  rootItem: (asset, assetModelDecisionService) =>
    assetModelDecisionService.getRootItem(asset.id),

  /* @ngInject */
  rootItems: (asset, rootItem, assetModelDecisionService) =>
    assetModelDecisionService.getItems(asset.id,
      _.map(rootItem.items, 'id')),


    /* @ngInject */
  settings: () => ({
    activeView: 'list',
    currentLevel: {},
    selected: {}
  }),

  /* @ngInject */
  assetModel: (
    $stateParams,
    asset,
    rootItem,
    rootItems,
    assetModelDecisionService,
    settings
  ) => {
    const item = _.set(new Item(rootItem), 'items', rootItems);

    settings.selected = item;
    settings.currentLevel = item;

    const expandById = (root, oIds) => {
      if (!root || !root.items) {
        return item;
      }

      const oId = oIds[1];
      oIds = oIds.splice(1);
      const childItem = _.filter(root.items, (val) => val.id === oId)[0];

      if (childItem && childItem.items) {
        const ids = _.map(childItem.items, 'id');
        return assetModelDecisionService.getItems(asset.id, ids)
          .then((response) => {
            childItem.items = response;
            childItem.select({ reset: true });
            childItem.expand({ parents: true });

            settings.selected = childItem;
            settings.currentLevel = childItem.items ?
              childItem : childItem.parent;

            return expandById(childItem, oIds);
          });
      }
      return item;
    };

    const originalIds = angular.copy($stateParams.originalIds || []);
    return expandById(item, originalIds);
  },

  /* @ngInject */
  item: ($stateParams, assetModelDecisionService) => $stateParams.itemId ?
    assetModelDecisionService.getItem($stateParams.assetId,
      $stateParams.itemId) : null
};
