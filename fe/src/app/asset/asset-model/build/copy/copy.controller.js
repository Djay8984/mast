import * as _ from 'lodash';
import * as angular from 'angular';
import Base from 'app/base.class';
import Asset from 'app/models/asset.model';
import Item from 'app/models/item.model';
import templateUrl from './copy-attributes-dialogue.html';
import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class CopyController extends Base {
  /* @ngInject */
  constructor(
    $q,
    $anchorScroll,
    $state,
    $stateParams,
    $timeout,
    $log,
    assetOnlineService,
    assetModelOnlineService,
    draftItemService,
    MastModalFactory,
    navigationService,
    asset,
    assets,
    itemTypeRelationships,
    assetFilterDefault
  ) {
    super(arguments);

    this._selectedItems = [];
    this._relationships = _.filter(
      _.get(this._itemTypeRelationships, 'asset.itemTypeRelationships'),
      {
        assetCategoryId: _.get(this._asset, 'assetCategory.id'),
        fromItemType: _.get(this._$stateParams, 'item.itemType.id')
      });

    this.assetFilter = _.isEmpty(this._$stateParams.assetFilter) ?
      this._assetFilterDefault :
      this._$stateParams.assetFilter;
    this.filterActive = !_.isEqual(this.assetFilter, this._assetFilterDefault);
    this.filterOpen = false;

    this.canDirectCopy = !Boolean(_.get(this._$stateParams, 'item.parentItem'));
  }

  get assetFilterDefault() {
    return this._assetFilterDefault;
  }

  get item() {
    return this._$stateParams.item;
  }


  get asset() {
    return this._asset;
  }

  get assets() {
    if (!this._assetList || this._assetList.updated) {
      this._assetList = Asset.from(this._assets);
      this._assetList.updated = false;
    }

    return this._assetList;
  }

  async initialisePreview(asset, elementId) {
    this.cancel();
    _.set(asset, 'selected', true);
    this._previewAssetElementId = elementId;
    const rootItem = await this._assetModelOnlineService.getRootItem(asset.id);
    const rootItems = await this._assetModelOnlineService
      .getItems(asset.id, _.map(rootItem.items, 'id'));

    this._previewAssetModel = new Item(rootItem.plain());
    this._previewAssetModel.items = rootItems;

    this._previewAsset = asset;

    this._$timeout(() => this._$anchorScroll('asset-model-preview'));
  }

  async confirmCopy() {
    const confirmation = await new this._MastModalFactory({
      templateUrl: confirmationTemplateUrl,
      scope: {
        title: 'Copy across attribute values',
        message: `Do you want to copy across
         attribute values for the selected items?`,
        actions: [
        { name: 'No', result: false },
        { name: 'Yes', result: true }
        ]
      },
      class: 'dialog'
    }).activate();

    // this will copy items selected and if the user chooses, the attributes
    // as well
    this.copy(confirmation);
  }

  /**
   * Builds a hierarchical tree of IDs of the selected items
   * @param {boolean} attributeResponse copy attribute decision
   */
  async copy(attributeResponse) {
    this.copying = true;
    const parentId = this._$stateParams.itemId;
    const assetId = this._asset.id;
    const getSelected = (items) =>
      items.filter((item) => {
        if (item.selected) {
          item.items = getSelected(item.items);
        }

        return item.selected;
      });

    const itemTree = getSelected(angular.copy(this.topLevelSelectedItems));

    try {
      await this._draftItemService
        .copyPreviewAssetModel(assetId, parentId, itemTree, attributeResponse);
    } catch (e) {
      this._$log(e);
    }

    this.copying = false;

    this._$state.go('asset.assetModel.buildMode');
  }

  async directCopy(asset) {
    if (!this.copying) {
      const assetToCopyId = asset.id;
      const assetId = this._asset.id;
      const parentId = this._$stateParams.itemId;

      const modal = new this._MastModalFactory({
        templateUrl,
        class: ''
      });

      const responses = await modal.activate();

      this.copying = true;

      await this._draftItemService
        .copyAssetModel(assetId, parentId, assetToCopyId, responses);

      this.copying = false;

      this._$state.go('asset.assetModel.buildMode');
    }
  }

  cancel() {
    this._selectedItems = [];
    _.set(this._previewAsset, 'selected', false);
    this._previewAsset = null;
    this._previewAssetModel = null;
    this._$timeout(() => this._$anchorScroll(this._previewAssetElementId));
  }

  updateList(filter) {
    this._$state.go(this._$state.current.name,
      _.set(_.omit(this._$stateParams, 'assetFilter'), 'assetFilter', filter), {
        reload: true
      });
  }

  get previewAsset() {
    return this._previewAsset;
  }

  get previewAssetModel() {
    return this._previewAssetModel;
  }

  get selectedItems() {
    return this._selectedItems;
  }

  /**
   * Counts the selected items whose parent is not also in the list
   */
  get topLevelSelectedItems() {
    return _.filter(this._selectedItems,
      (item) => !_.includes(this._selectedItems, item.parent));
  }

  get topLevelSelectedItemCount() {
    return _.size(this.topLevelSelectedItems);
  }

  get relationships() {
    return this._relationships;
  }
}
