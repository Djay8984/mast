import * as angular from 'angular';

export default angular
  .module('app.asset.asset-model.build.add', [])
  .name;
