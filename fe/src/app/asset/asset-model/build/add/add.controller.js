import * as _ from 'lodash';
import Base from 'app/base.class';

export default class AddController extends Base {
  /* @ngInject */
  constructor(
    $stateParams,
    draftItemService,
    navigationService,
    item,
    asset,
    referenceDataService,
    availableTemplateItems
  ) {
    super(arguments);

    const itemsList = _.uniqBy(availableTemplateItems.items, 'id');

    _.forEach(_.get(availableTemplateItems, 'rule.validItems'),
      (childItemRule) => {
        const matchingExistingItems = _.filter(item.items,
          { itemType: { id: childItemRule.toItemType } });

        // cannot select if we're already at the max occurs
        if (childItemRule.maxOccurs &&
          _.size(matchingExistingItems) >= childItemRule.maxOccurs) {
          _.set(_.find(itemsList, { id: childItemRule.toItemType }),
            'disabled', true);
        }

        // cannot disable & unselect if mandatory
        if (_.isEmpty(matchingExistingItems) && childItemRule.minOccurs) {
          _.merge(_.find(itemsList, { id: childItemRule.toItemType }),
            { disabled: true, selected: true });
        }
      });

    this.availableItems = itemsList;
  }

  get item() {
    return this._$stateParams.item || this._item;
  }

  cancel() {
    this.back();
  }

  back() {
    this._navigationService.back();
  }

  async addSelectedItems() {
    const items = _.map(_.filter(this.availableItems, { selected: true }),
      (item) => _.omit(item, 'selected'));

    await this._draftItemService.copyReferenceData(
      this._$stateParams.assetId, this._item.id, items
    );

    this.back();
  }
}
