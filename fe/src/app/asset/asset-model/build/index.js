import * as angular from 'angular';

import add from './add';
import copy from './copy';

import routes from './config.routes';

export default angular
  .module('app.asset.asset-model.build', [
    add,
    copy
  ])
  .config(routes)
  .name;
