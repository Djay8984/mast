import Base from 'app/base.class';

export default class BuildController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    asset,
    item
  ) {
    super(arguments);
  }

  get item() {
    return this._$stateParams.item || this._item;
  }

  get asset() {
    return this._asset;
  }

  back() {
    this._$state.go('asset.assetModel.view', { buildMode: 'true' });
  }
}
