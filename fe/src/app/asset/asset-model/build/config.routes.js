import * as _ from 'lodash';

import BuildController from './build.controller';
import buildTemplateUrl from './build.html';

import CopyController from './copy/copy.controller';
import copyTemplateUrl from './copy/copy.html';

import AddController from './add/add.controller';
import addTemplateUrl from './add/add.html';

import HeaderController from '../../header/asset-header.controller';
import headerTemplateUrl from '../../header/asset-header.html';

/* @ngInject */
export default function config(
  $stateProvider
) {
  $stateProvider
    .state('build', {
      abstract: true,
      parent: 'asset.assetModel',
      url: '/build/{itemId:int}',
      params: {
        item: null,
        pager: null,
        originalIds: null
      },
      resolve: {

        /* @ngInject */
        item: ($stateParams, assetModelDecisionService) =>
          assetModelDecisionService.getDraftItem(
            $stateParams.assetId, $stateParams.itemId, true, true, true)
      },
      views: {
        '@main': {
          templateUrl: buildTemplateUrl,
          controller: BuildController,
          controllerAs: 'vm'
        },
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            user: (node) => node.getUser(),

            /* @ngInject */
            asset: ($stateParams, assetDecisionService) =>
              assetDecisionService.get($stateParams.assetId)
          }
        }
      }
    })
    .state('build.copy', {
      parent: 'build',
      url: '/copy',
      templateUrl: copyTemplateUrl,
      controller: CopyController,
      controllerAs: 'vm',
      params: {
        itemId: null,
        assetFilter: null
      },
      resolve: {

        /* @ngInject */
        assets: ($stateParams, assetDecisionService, asset) =>
          $stateParams.assetFilter ?
            assetDecisionService.query(_.merge($stateParams.assetFilter, {
              categoryId: [asset.assetCategory.id]
            }), { sort: 'name', order: 'asc' }) :
            assetDecisionService.getAllCurrentByCategoryId(
              asset.assetCategory.id, { sort: 'name', order: 'asc' }),

        /* @ngInject */
        itemTypeRelationships: (referenceDataService) =>
         referenceDataService.get(['asset.itemTypeRelationships']),

        /* @ngInject */
        assetFilterDefault: (assetDecisionService, asset) =>
          assetDecisionService.getCurrentLifecycleStatusIds().then((ids) => ({
            categoryId: [asset.assetCategory.id],
            lifecycleStatusId: ids
          }))
      }
    })
    .state('build.add', {
      parent: 'build',
      url: '/add',
      checkedInRedirect: '^.^.view',
      templateUrl: addTemplateUrl,
      controller: AddController,
      controllerAs: 'vm',
      resolve: {

        /* @ngInject */
        availableTemplateItems: (asset, item, assetModelDecisionService) =>
          assetModelDecisionService.getAvailableItems(asset, item)
      }
    });
}
