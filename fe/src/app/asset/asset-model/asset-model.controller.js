import * as _ from 'lodash';

import BaseForm from 'app/base-form.class';

import saveConfirmationDialog from './build-mode/save-confirmation-dialog.html';
import SaveConfirmationController from
 './build-mode/save-confirmation.controller';

import listUrl from './partials/list.html';
import hierarchyUrl from './partials/hierarchy.html';
import searchUrl from './partials/search.html';
import buildModeFooterUrl from './partials/build-mode-footer.html';

export default class AssetModelController extends BaseForm {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    $timeout,
    $log,
    assetModelDecisionService,
    draftItemService,
    MastModalFactory,
    pageService,
    asset,
    settings
  ) {
    super(arguments);

    this.actions = {
      open: (data) => {
        this._$state.go('item.attributes', { itemId: data.item.id });
      },
      move(data, step) {
        const list = data.getList();
        const index = _.findIndex(list, data.item);
        _.remove(list, data.item);
        list.splice(index - step, 0, data.item);
      },
      moveUp(data) {
        this.move(data, 1);
      },
      moveDown(data) {
        this.move(data, -1);
      },
      duplicate: (data) => {
        const list = data.getList();
        const newItem = _.clone(data.item);
        newItem.id += Math.random();
        const index = _.findIndex(list, data.item);
        list.splice(index + 1, 0, newItem);

        // data.getList().push(newItem);
      },
      delete: (data) => {
        data.loading = true;
        this._$timeout(() => {
          _.remove(data.getList(), data.item);
          data.loading = false;
        }, 1000);
      }
    };

    this.view = {
      search: [{ htmlUrl: searchUrl }],
      list: [{ htmlUrl: listUrl }],
      _buildModeFooter: { htmlUrl: buildModeFooterUrl },
      _hierarchy: {
        htmlUrl: hierarchyUrl,
        asset: this._asset,
        actions: this.actions,
        saved: {
          templates: {},
          loaded: {}
        }
      },
      get hierarchy() {
        this._hierarchy.editable = false;
        return [this._hierarchy];
      },
      get buildMode() {
        this._hierarchy.editable = true;
        this._buildModeFooter.total = this._hierarchy.totalSelected;
        return [this._hierarchy, this._buildModeFooter];
      }
    };
  }

  get content() {
    return this.view[this.display];
  }

  get display() {
    let res = this.searchMode ? 'search' : this.activeView;
    res = this.buildMode ? 'buildMode' : res;
    return res;
  }

  get activeView() {
    return this._$state.params.view || 'list';
  }

  get searchMode() {
    return this._$state.params.searchMode === 'true';
  }

  set searchMode(value) {
    this._$state.params.searchMode = value;
  }

  get buildMode() {
    return this._pageService.isCheckedOut(this._asset);
  }

  set buildMode(value) {
    this._$state.params.buildMode = value;
  }

  async save() {
    const modal = new this._MastModalFactory({
      controller: SaveConfirmationController,
      controllerAs: 'vm',
      templateUrl: saveConfirmationDialog,
      inject: {
        asset: this._asset
      }
    });
    modal.activate();
    await this._draftItemService.publishDraftItem(this._asset.id);
    modal.destroy();
  }

  get asset() {
    return this._asset;
  }

  set selectedItem(value) {
    this._settings.selected = value;
  }

  get selectedItem() {
    return this._settings.selected;
  }

  get currentLevel() {
    return this._settings.currentLevel;
  }

  set currentLevel(value) {
    this._settings.currentLevel = value;
  }

  async toggleBuildMode() {
    try {
      await this._pageService.checkOut(this._asset);
      this._$state.go('asset.assetModel.view',
        { ...this._$state.params, draft: true },
        { reload: true });
    } catch (e) {
      this._$log.error('Failed check out', e);
    }
  }

  add() {
    const item = this.view._hierarchy.getSelected();
    this._$state.go('build.add', { itemId: item.id });
  }

  copy() {
    const item = this.view._hierarchy.getSelected();
    this._$state.go('build.copy', { itemId: item.id });
  }

  changeView(view, searchMode) {
    view = view || this.activeView;
    this._$state.go('asset.assetModel.view', { view, searchMode });
  }

  toggleSearch() {
    this.searchMode = !this.searchMode;
    this._$state.go('asset.assetModel.view', this._$state.params);
  }

  // shim for deferred saving changes
  // TODO: use when build mode features with deferred saving implemented
  get form() {
    return { $dirty: false };
  }
}
