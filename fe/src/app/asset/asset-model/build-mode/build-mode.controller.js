import Base from 'app/base.class';
import * as _ from 'lodash';
import saveConfirmationDialog from './save-confirmation-dialog.html';
import SaveConfirmationController from './save-confirmation.controller';

export default class BuildModeController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    asset,
    draftAssetModel,
    draftItemService,
    MastModalFactory,
    settings
  ) {
    super(arguments);

    const item = _.first(draftAssetModel.findChildren({ selected: true }));
    this.selectedItem = item || draftAssetModel;

    //  Used in parent controller asset-model.controller.js
    //  To be able go back to the same view
    this._settings.activeView = this._$stateParams.view;

    const onRouteChangeOff =
      this._$rootScope.$on('$stateChangeStart', (event, toState) => {
        if (!_.includes(['build.copy', 'build.add', 'asset.assetModel.view'],
          toState.name)) {
          this.save();
        }
        onRouteChangeOff();
      });
  }

  set selectedItem(value) {
    this._settings.selected = value;
  }

  get selectedItem() {
    return this._settings.selected;
  }

  get asset() {
    return this._asset;
  }

  get assetModel() {
    return this._draftAssetModel;
  }

  set assetModel(value) {
    let currentModel = this._draftAssetModel;

    _.forEach(currentModel.items, (item) => {
      if (currentModel.items.id === value.id) {
        currentModel = value;

        return currentModel;
      }
    });
  }

  async save() {
    const modal = new this._MastModalFactory({
      controller: SaveConfirmationController,
      controllerAs: 'vm',
      templateUrl: saveConfirmationDialog,
      inject: {
        asset: this._asset
      }
    });
    modal.activate();
    await this._draftItemService.publishDraftItem(this._asset.id);
    modal.destroy();
  }

  async exit() {
    await this.save();
    this._$state.go('asset.assetModel.view', {
      originalIds: _.map(this._settings.selected.parents, 'originalItem.id')
        .reverse(),
      view: this._settings.activeView
    }, { reload: true });
  }

  add(item) {
    this.goTo(item, 'build.add');
  }

  copy(item) {
    this.goTo(item, 'build.copy');
  }

  goTo(item, state) {
    this._$state.go(state, {
      item,
      draftItemId: item.id,
      originalIds: _.map(this.selectedItem.parents, 'originalItem.id').reverse()
    });
  }
}
