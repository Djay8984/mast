import Base from 'app/base.class';

export default class SaveConfirmationController extends Base {
  /* @ngInject */
  constructor(asset) {
    super(arguments);
  }

  get assetName() {
    return this._asset.name;
  }
}
