import * as _ from 'lodash';
import Item from 'app/models/item';

export controller from './build-mode.controller';
export templateUrl from './build-mode.html';

export const resolve = {

  /* @ngInject */
  rootItem: (asset, draftItemService) =>
    draftItemService.createDraftItem(asset.id),

  /* @ngInject */
  rootItems: (asset, rootItem, draftItemService) => draftItemService
    .getDraftItems(asset, rootItem),

  /* @ngInject */
  draftAssetModel: (
    asset,
    rootItem,
    rootItems,
    draftItemService,
    $stateParams
  ) => {
    const model = _.set(new Item(rootItem), 'items', rootItems).expand();
    const originalIds = _.without(_.compact($stateParams.originalIds),
        _.get(model, 'originalItem.id'));

    if (!originalIds.length) {
      return model.select({ reset: false });
    }

    return draftItemService.getByOriginalId(asset, originalIds)
      .then((items) => model.populate(_.map(originalIds, (id) =>
        _.find(items, { originalItem: { id } }))));
  }
};
