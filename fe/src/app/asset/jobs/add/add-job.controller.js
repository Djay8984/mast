import Base from 'app/base.class';
import * as _ from 'lodash';
import * as angular from 'angular';

const RESET_KEYS = [
  'jobCategory',
  'caseId',
  'location',
  'description',
  'changeOfFlag',
  'proposedFlagState',
  'etaDate',
  'etdDate',
  'requestedAttendanceDate'
];

export default class AddJobController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $scope,
    $state,
    $stateParams,
    asset,
    dateFormat,
    moment,
    jobCategory,
    jobOnlineService,
    navigationService,
    typeaheadService,
    referenceData,
    caseIds
  ) {
    super(arguments);

    this.setUpWatches();
    this.setUpData();
  }

  setUpData() {
    /* Set up initial dates for the date pickers; */
    this.dates = {
      etaDate: this._moment().add(-9999, 'Y').hour(12).startOf('h'),
      etdDate: this._moment().add(9999, 'Y').hour(12).startOf('h'),
      svcReqdDate: this._moment().add(9999, 'Y').hour(12).startOf('h')
    };

    /* Set up initial parameters on the add job page; */
    this.jobmodel =
    {
      zeroVisitJob: false,
      sdo: '',
      caseId: this._$stateParams.caseId ?
        [ this._$stateParams.caseId ] : _.map(this._caseIds, 'id'),
      classGroupJob: false
    };

    this._selectDisabled = false;

    if (!_.isNull(this._$stateParams.caseId)) {
      this.jobmodel.caseId = _.first(this.jobmodel.caseId);
      this._selectDisabled = true;
    }

    this.sdoTypeahead = this._typeaheadService.query(this.employeeOffices);
    this.flagTypeahead = this._typeaheadService.query(this.flagStates);

    if (this._$scope.etaDate) {
      this.jobmodel.etaDate = this._$scope.etaDate;
    }

    if (this._$scope.etdDate) {
      this.jobmodel.etdDate = this._$scope.etdDate;
    }
  }

  setUpWatches() {
    this._$scope.$watch('vm.jobmodel.etaDate', () => {
      if (this.addJobForm.requestedAttendanceDate) {
        this.addJobForm.requestedAttendanceDate.$setValidity('min',
          this.jobmodel.requestedAttendanceDate > this.jobmodel.etaDate);
      }
    });

    this._$scope.$watch('vm.jobmodel.etdDate', () => {
      if (this.addJobForm.requestedAttendanceDate) {
        this.addJobForm.requestedAttendanceDate.$setValidity('max',
          this.jobmodel.requestedAttendanceDate < this.jobmodel.etdDate);
      }
    });

    this._$scope.$watch('vm.jobmodel.zeroVisitJob', () => {
      this.resetForm();
    });
  }

  get selectDisabled() {
    return this._selectDisabled;
  }

  get jobETA() {
    return _.get(this, 'jobmodel.etaDate') || this.dates.etaDate;
  }

  get jobETD() {
    return _.get(this, 'jobmodel.etdDate') || this.dates.etdDate;
  }

  /**
   * @returns {Boolean} true or false
   */
  validForm() {
    return this.addJobForm.$valid;
  }

  /**
   * Cancels the job addition and returns to original page state;
   */
  cancel() {
    this._$state.go('asset.jobs.list');
  }

  /**
   * Saves the job just created
   */
  async save() {
    const asset = {
      id: this._asset.id,
      version: this._asset.assetVersionId
    };

    this.jobmodel.asset = asset;

    /*
       For Sprint 10+

       When the job is 'created' for the first time, the system will add a
       Technical Review Checklist, which is a blank document. (Similar to R1
       attachments);

       This will go here, or in the job service at the point of saving;

       For sprint 10+
     */

    const model = angular.copy(this.jobmodel);

    try {
      const newJob = await this._jobOnlineService.addJob(this.jobmodel);

      /* This is to cover the temporary delay of saving as it displays the
         selections being wiped; */
      this.jobmodel = model;

      this._$state.go('asset.jobs.view',
        _.merge(this._$stateParams, { jobId: newJob.id }));
    } catch (err) {
      const officeId = _.first(this.jobmodel.offices).office.id;
      const office = _.find(this.employeeOffices, { id: officeId });

      _.set(this.jobmodel, 'sdo', office.name);
      _.set(this.jobmodel, 'caseId', model.caseId);

      this._$log.debug(err);
    }
  }

  /**
   * @returns {String} DateFormat The date format from the constants file
   */
  get dateFormat() {
    return this._dateFormat;
  }

  get employeeOffices() {
    return _.get(this._referenceData, 'employee.office');
  }

  get flagStates() {
    return _.get(this._referenceData, 'flag.flags');
  }

  resetForm() {
    if (this.addJobForm) {
      _.forEach(RESET_KEYS, (key) => this.jobmodel[key] = null);
      this.jobmodel.sdo = '';

      angular.element(document.getElementById('add-job-sdo'))
        .prop('disabled', false);

      this.addJobForm.$setPristine();
    }
  }

  get caseIds() {
    return _.map(this._caseIds, 'id');
  }

  /**
   * @summary back()
   */
  back() {
    this._navigationService.back();
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false depending upon the form item status
   */
  validFormItem(item) {
    return Boolean(item && item.$dirty && item.$invalid && !item.$pristine);
  }

  get jobCategory() {
    return this._jobCategory.toObject();
  }
}
