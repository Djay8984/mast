export controller from './add-job.controller';
export templateUrl from './add-job.html';

export const resolve = {

  /* @ngInject */
  caseIds: (asset, assetDecisionService) =>
    assetDecisionService.getCases(asset.id),

  /* @ngInject */
  referenceData: (referenceDataService) =>
    referenceDataService.get(['employee.office', 'flag.flags'])
};
