import * as _ from 'lodash';

export controller from './edit-job.controller';
export templateUrl from './edit-job.html';

export const resolve = {

  /* @ngInject */
  job: ($stateParams, jobDecisionService) =>
    jobDecisionService.get($stateParams.jobId),

  /* @ngInject */
  caseIds: (job, assetDecisionService) =>
    assetDecisionService.getCases(job.asset.id),

  /* @ngInject */
  closedStatuses: (jobStatus) => (
    { id: _.pick(jobStatus.toObject(), 'closed') }),

  /* @ngInject */
  referenceData: (referenceDataService) =>
    referenceDataService.get(['employee.office'])
};
