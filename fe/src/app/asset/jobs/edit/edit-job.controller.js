import Base from 'app/base.class';
import * as _ from 'lodash';

export default class EditJobsController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    $stateParams,
    moment,
    jobDecisionService,
    typeaheadService,
    asset,
    closedStatuses,
    caseIds,
    job,
    jobCategory,
    jobStatus,
    officeRole,
    referenceData
  ) {
    super(arguments);

    this.setUpWatches();
    this.setUpData();
    this.setUpJob();
  }

  setUpData() {
    /* Set up initial dates for the date pickers; */
    this.dates = {
      etaDate: this._moment().add(-9999, 'Y').hour(12).startOf('h'),
      etdDate: this._moment().add(9999, 'Y').hour(12).startOf('h'),
      svcReqdDate: this._moment().add(9999, 'Y').hour(12).startOf('h')
    };

    this.sdoTypeahead = this._typeaheadService.query(this.employeeOffices);
  }

  /**
   * Object the modified jobmodel
   */
  async setUpJob() {
    this.jobmodel = _.assign({}, this._job);
    _.set(this, 'jobmodel.zeroVisitJob',
      _.get(this, 'jobmodel.zeroVisitJob', false));

    /* A quirk of job.offices is that it is an array; it will probably never
       be more than a 1 element array, so we're hard-coding in _.first(); */
    _.set(this.jobmodel, 'sdo', _.find(this._job.offices, {
      officeRole: { id: this._officeRole.get('sdo') }
    }).office.name);

    /* The caseIds() and the caseIds resolve give us all the possible case
       ids, but we still have to set it from the saved job; */
    _.set(this.jobmodel, 'caseId', this._job.aCase.id);
  }

  setUpWatches() {
    this._$scope.$watch('vm.jobmodel.etaDate', () => {
      if (this.editJobForm.requestedAttendanceDate) {
        this.editJobForm.requestedAttendanceDate.$setValidity('min',
          this.jobmodel.requestedAttendanceDate > this.jobmodel.etaDate);
        this.editJobForm.requestedAttendanceDate.$setDirty();
      }
    });

    this._$scope.$watch('vm.jobmodel.etdDate', () => {
      if (this.editJobForm.requestedAttendanceDate) {
        this.editJobForm.requestedAttendanceDate.$setValidity('max',
          this.jobmodel.requestedAttendanceDate < this.jobmodel.etdDate);
        this.editJobForm.requestedAttendanceDate.$setDirty();
      }
    });
  }

  get jobETA() {
    return _.get(this, 'jobmodel.etaDate') || this.dates.etaDate;
  }

  get jobETD() {
    return _.get(this, 'jobmodel.etdDate') || this.dates.etdDate;
  }

  /**
   * Instructs the job service to save the current job;
   * @param {Object} job The jobmodel passed as the job;
   */
  async saveJob(job) {
    const findSDO = (targetJob) =>
      _.find(targetJob.offices, {
        officeRole: {
          id: this._officeRole.get('sdo')
        }
      });

    const sdo = findSDO(job);

    /* Need to check if the user has changed the SDO; if they have then we
       need to clear the SDO coordinator and lead surveyor, essentially
       empty the job.employees object; */
    if (job.sdo !== sdo.office.name) {
      sdo.office = job.sdo;

      /* AC8: if the SDO is updated, lead surveyor is blank so the job needs
      to have its status reverted back to 'SDO assigned'; */
      if (job.jobStatus.id !== 1) {
        job.jobStatus.id = 1;
      }
    }

    if (job.caseId !== _.get(job.aCase, 'id')) {
      _.set(job, 'aCase.id', job.caseId);
    }

    await this._jobDecisionService.updateJob(
      _.omit(job, ['sdo', 'caseId', 'employees']), true);

    this._$state.go('asset.jobs.view', this._$stateParams, { reload: true });
  }

  /**
   * @returns {Array} array Returns an array of closed status ids which comes
   *          from the job service, via resolve injection;
   */
  get closedStatusIds() {
    if (!this._closedJobs) {
      this._closedJobs = _.map(this._closedStatuses, 'id');
    }

    return this._closedJobs;
  }

  /**
   * @returns {Boolean} true or false
   */
  validForm() {
    /* If it's valid, and been altered (no point in saving when pristine); */
    return this.editJobForm.$valid && !this.editJobForm.$pristine;
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false depending upon the form item status
   */
  validFormItem(item) {
    return Boolean(item && item.$dirty && item.$invalid && !item.$pristine);
  }

  /**
   * @returns {Boolean} whether or not the job is in
   * status 'sdoAssigned' or 'resourceAssigned'
   */
  editableSDO() {
    return this._job.jobStatus.id === this._jobStatus.get('sdoAssigned') ||
      this._job.jobStatus.id === this._jobStatus.get('resourceAssigned');
  }

  /**
   * @returns {Boolean} true or false
   */
  editableForm() {
    const closedJobIds = this.closedStatusIds;

    /* If the job is either in a 'Closed', 'Cancelled' or 'Aborted' state; */
    return !_.includes(closedJobIds, this._job.jobStatus.id);
  }

  /**
   * @return {Array} offices Returns the offices roles from reference data
   */
  get employeeOffices() {
    return _.get(this._referenceData, 'employee.office');
  }

  /**
   * @returns {Array} array Returns an array of ids
   */
  get caseIds() {
    return _.map(this._caseIds, 'id');
  }

  back() {
    this._$state.go('^.view', this._$stateParams, { reload: true });
  }

  get jobCategory() {
    return this._jobCategory.toObject();
  }

  get jobLocationDisabled() {
    let fieldCondition = !this.editableSDO();

    if (this.jobmodel.jobCategory.id === this._jobCategory.get('auditMms')) {
      fieldCondition =
        this._job.jobStatus.id ===
          this._jobStatus.get('awaitingEndorserAssignment') ||
        this._job.jobStatus.id === this._jobStatus.get('underEndorsement');
    }

    return !this.editableForm() || fieldCondition;
  }
}
