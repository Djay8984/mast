import * as _ from 'lodash';
import Base from 'app/base.class';

import Header from './header.class';
import Section from './section.class';
import Footer from './footer.class';

export default class EndorsementController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    $q,
    $rootScope,
    $state,
    $timeout,
    MastModalFactory,
    dateFormat,
    moment,
    employeeRole,
    jobOnlineService,
    job,
    user
  ) {
    super(arguments);

    this.header = new Header(this);
    this.section = new Section(this);
    this.footer = new Footer(this);
  }

  /**
   * @summary currentUserIsFleetServicesSpecialist()
   * @description Fairly self-explanatory, this checks the job's employees and
   *   sees if this user is the fleet services specialist assigned to the job
   * @return {Boolean} true or false
   */
  get currentUserIsFleetServicesSpecialist() {
    return _.some(this._job.employees,
      {
        lrEmployee: { id: this._user.id },
        employeeRole: { id: this._employeeRole.get('eicManager') }
      }
    );
  }

  //  ----------------------------------------------------------
  get job() {
    return this._job;
  }

  //  ----------------------------------------------------------
  get disabledActions() {
    return !this.currentUserIsFleetServicesSpecialist;
  }

  //  ----------------------------------------------------------
  get closed() {
    return this._job.jobStatus.constant === 'closed';
  }

  //  ----------------------------------------------------------
  back() {
    this._$state.go('asset.jobs.view', { jobId: this.job.id });
  }
}
