import * as _ from 'lodash';

import SectionUrl from './section.html';


const COMPLETE_STATUS = 2;

export default class Section {
  /* @ngInject */
  constructor(parent) {
    _.merge(this, parent);
    this.parent = parent;

    this.htmlUrl = SectionUrl;
    this.header = 'Vetting actions';
    this.closed = this.parent.closed;
    this.disabledActions = this.parent.disabledActions;
  }

  //  ----------------------------------------------------------
  get saveBtn() {
    return _.get(this.parent, 'footer.saveBtn');
  }

  /**
   * @summary get short description and return different its version
   *   depends on expanded status
   * @param {object} item - single endorsement
   * @return {string} short or full description
   */
  getDesc(item) {
    item.shortDesc = this._$filter('ellipsise')(item.description, 185);
    return item.expanded ? item.description : item.shortDesc;
  }

  //  ----------------------------------------------------------
  async reOpen(item) {
    const result = await this._MastModalFactory({
      templateReady: 'confirmation',
      scope: {
        title: 'Re-open Follow Up Action?',
        message: `You are sure to re-open "${item.title}"?`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Re-open', result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();

    if (result) {
      this.list.setLoading(true);
      await this.update(item, 1);
      this.list.refresh();
    }
  }

  //  ----------------------------------------------------------
  async getList() {
    return this._jobOnlineService.getEndorsement(this._job.id);
  }

  //  ----------------------------------------------------------
  async update(val, status) {
    val.followUpActionStatus.id = status;
    return this._jobOnlineService.updateEndorsement(this._job.id, val);
  }

  //  ----------------------------------------------------------
  async bulkUpdate(status = 2) {
    this.list.setLoading(true);
    const selected = _.filter(this.list.result, { selected: true });

    return this._$q.all(
      _.map(selected, (val) => this.update(val, status))
    );
  }

  //  ----------------------------------------------------------
  select(item) {
    const total = _.filter(this.list.result, { selected: true });

    this.saveBtn.disabled = _.isEmpty(total);

    this.saveBtn.name = total.length ?
      `Complete (${total.length})` : 'Complete';
  }

  raisedBy(item) {
    return item.addedManually ?
      `${item.raisedBy.firstName} ${item.raisedBy.lastName}` : 'System';
  }

  completionDate(item) {
    return this._moment(item.completionDate).format(this._dateFormat);
  }


  get completeStatus() {
    return COMPLETE_STATUS;
  }
}
