export default class Footer {
  constructor(parent) {
    this.parent = parent;

    this.templateUrl = 'footer/buttons';

    this.cancelBtn = {
      name: 'Cancel',
      action: parent.back.bind(parent),
      class: 'transparent-dark-button'
    };

    this.saveBtn = {
      name: 'Complete',
      action: this.save.bind(this),
      class: 'primary-button',
      disabled: true
    };

    this.buttons = [
      this.cancelBtn,
      this.saveBtn
    ];

    if (parent.closed || parent.disabledActions) {
      this.cancelBtn.name = 'Back';
      this.buttons = [ this.cancelBtn ];
    }
  }

  //  ----------------------------------------------------------
  get section() {
    return this.parent.section || {};
  }

  //  ----------------------------------------------------------
  async save() {
    this.saveBtn.name = 'Saving...';
    this.saveBtn.disabled = true;

    await this.section.bulkUpdate();
    await this.section.list.refresh();
    this.saveBtn.name = 'Complete';
  }
}
