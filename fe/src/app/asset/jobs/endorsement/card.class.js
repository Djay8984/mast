import * as _ from 'lodash';

import CardUrl from './card.html';

export default class Endorsement {
  constructor(parent) {
    _.merge(this, parent);

    this.htmlUrl = CardUrl;
    this.title = 'Endorsement';
    this.emptyText = 'No endorsements have been initiated yet';
    this.data = {};
  }

  //  ---------------------------------------------------
  get job() {
    return this._job;
  }

  //  ---------------------------------------------------
  viewActions() {
    this._$state.go('asset.jobs.endorsement', { jobId: this.job.id });
  }

  //  ---------------------------------------------------
  //  Used to hide Close Job button
  //  And show Closed status
  get closed() {
    return this._job.jobStatus.constant === 'closed';
  }

  //  ---------------------------------------------------
  //  Disabled Close Job button
  get isDisabled() {
    return !this.data || this.data.total !== this.data.completed ||
      this._$rootScope.OFFLINE;
  }

  get isActionDisabled() {
    return this._job.jobStatus.constant === 'underTechnicalReview' ||
      this._job.jobStatus.constant === 'awaitingTechnicalReviewerAssignment' ||
      this._$rootScope.OFFLINE;
  }

  //  ---------------------------------------------------
  //  Get data from API
  async getEndorsement() {
    const list = await this._jobOnlineService.getEndorsement(this.job.id);
    const completed = _.filter(list, ['followUpActionStatus.id', 2]).length;
    const total = list.length;

    this.data.list = list;
    this.data.completed = completed;
    this.data.incomplete = total - completed;
    this.data.total = total;

    return this.data;
  }

  //  ---------------------------------------------------
  //  Close Job action
  //  Open Confirmation Modal
  async closeJob() {
    const result = await this._MastModalFactory({
      templateReady: 'confirmation',
      scope: {
        title: 'Close job?',
        message: `You have selected to close the job.
This action cannot be undone.`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Close job', result: true, class: 'primary-button' }
        ]
      },
      class: 'dialog'
    }).activate();

    if (result) {
      this.card.setLoading(true);

      this._job.jobStatus.id = this._jobStatus.get('closed');
      await this._jobOnlineService.updateJob(this.job.model, true);

      this._$state.reload();
    }
  }

  completedBy(job) {
    return `${job.completedBy}`;
  }

  completionDate(job) {
    return this._moment(job.completedOn).format(this._dateFormat);
  }
}
