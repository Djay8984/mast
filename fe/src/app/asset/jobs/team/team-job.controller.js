import * as _ from 'lodash';

import Base from 'app/base.class';
import Job from 'app/models/job.model';

export default class TeamJobController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $scope,
    $state,
    $stateParams,
    $q,
    employeeReferenceData,
    job,
    jobDecisionService,
    jobStatus,
    jobStatusReferenceData,
    surveyorRole,
    typeaheadService,
    user
  ) {
    super(arguments);

    // TODO: try and move to setUpJob() by adding @modelable to jobService.get
    this.jobModel = new Job(this._job);

    this.setUpJob();
    this.setUpController();
    this.setUpAccessMap();
  }

  /**
   * Defines rules for whether or not to disable certain surveyor fields in the
   * template based off an ABAC table in story JP.03 AC 3
   */
  async setUpAccessMap() {
    this.accessMap = {
      sdoCoordinator: false,
      surveyor: false,
      authorisingSurveyor: false,
      eicManager: false
    };

    // Leaves all access as false (the default)
    if (!this.isTeamEditable()) {
      return;
    }

    // Various rules that return a truethy/falsey value for use in determining
    // whether or not a field in the form should be disabled

    const userIsInSdoTeam = async (user) =>
      this._jobDecisionService.userSharesTeamSdoWithJob(user, this._job);

    const userIsInSdoTeamOrJobTeam = (user) => userIsInSdoTeam(user) ||
      _.includes([
        this._authorisingSurveyor,
        this._fleetServiceSpecialist,
        this._leadSurveyor,
        this._sdoCoordinator,
        ...this._generalSurveyors
      ], user.fullName);

    const userIsInSdo = async (user) =>
      this._jobDecisionService.userSharesSdoWithJob(user, this._job);

    const userhasRequiredRbac = async (user) => _.size(
      _.intersection(user.groups, ['eic', 'class group', 'mms', 'mmso']));

    const fieldIsNeverEditable = async () => false;

    const accessMapRules = {
      sdoAssigned: {
        sdoCoordinator: userIsInSdoTeam,
        surveyor: userIsInSdoTeam,
        authorisingSurveyor: userIsInSdoTeam,
        eicManager: userIsInSdoTeam
      },
      resourceAssigned: {
        sdoCoordinator: userIsInSdoTeamOrJobTeam,
        surveyor: userIsInSdoTeamOrJobTeam,
        authorisingSurveyor: userIsInSdoTeamOrJobTeam,
        eicManager: userIsInSdoTeamOrJobTeam
      },
      underSurvey: {
        sdoCoordinator: userIsInSdo,
        surveyor: userIsInSdo,
        authorisingSurveyor: userIsInSdo,
        eicManager: userIsInSdo
      },
      underReporting: {
        sdoCoordinator: userIsInSdo,
        surveyor: userIsInSdo,
        authorisingSurveyor: userIsInSdo,
        eicManager: userIsInSdo
      },
      awaitingTechnicalReviewerAssignment: {
        sdoCoordinator: userIsInSdoTeam,
        surveyor: userIsInSdoTeam,
        authorisingSurveyor: userIsInSdoTeam,
        eicManager: userIsInSdoTeam
      },
      underTechnicalReview: {
        sdoCoordinator: fieldIsNeverEditable,
        surveyor: fieldIsNeverEditable,
        authorisingSurveyor: (user) =>
          user.fullName === this._authorisingSurveyor,
        eicManager: fieldIsNeverEditable
      },
      awaitingEndorserAssignment: {
        sdoCoordinator: fieldIsNeverEditable,
        surveyor: fieldIsNeverEditable,
        authorisingSurveyor: fieldIsNeverEditable,
        eicManager: userhasRequiredRbac
      },
      underEndorsement: {
        sdoCoordinator: fieldIsNeverEditable,
        surveyor: fieldIsNeverEditable,
        authorisingSurveyor: fieldIsNeverEditable,
        eicManager: userhasRequiredRbac
      }
    };

    const statusName = _.findKey(this._jobStatus.toObject(),
      (status) => status === this.currentJobStatus);

    this.accessMap = await this._$q.all(
      _.forEach(_.keys(this.accessMap), (field) =>
        _.set(this.accessMap, field,
          accessMapRules[statusName][field](this._user))));
  }

  /**
   * Initialises extra non-job and non-accessMap resources the controller needs
   */
  setUpController() {
    this._editableStatuses = [
      this._jobStatus.get('underSurvey'),
      this._jobStatus.get('sdoAssigned'),
      this._jobStatus.get('resourceAssigned'),
      this._jobStatus.get('underReporting')
    ];

    this._employeeRoles =
      _.values(_.pick(this._surveyorRole.toObject(),
      ['sdoCoordinator', 'leadSurveyor', 'authorisingSurveyor', 'eicManager']));

    this.surveyorTypeahead = this._typeaheadService.query(
      _.get(this._employeeReferenceData, 'employee.employee'),
      this.surveyorPreFilter.bind(this));
  }

  /**
   * Initialises the jobModel employees
   */
  setUpJob() {
    this.jobModel.generalSurveyors = [];

    _.forEach(this._job.employees, ({ lrEmployee, employeeRole }) => {
      switch (employeeRole.id) {
        case this._surveyorRole.get('leadSurveyor'):
          this.jobModel.generalSurveyors.push(lrEmployee);
          this.jobModel.leadSurveyorIndex =
            _.findIndex(this.jobModel.generalSurveyors, lrEmployee);
          this.setOriginalLeadSurveyor(lrEmployee.name);
          break;
        case this._surveyorRole.get('sdoCoordinator'):
          this.jobModel.sdoCoordinator = lrEmployee;
          this.setOriginalSdoCoordinator(lrEmployee.name);
          break;
        case this._surveyorRole.get('authorisingSurveyor'):
          this.jobModel.authorisingSurveyor = lrEmployee;
          this.setOriginalAuthorisingSurveyor(lrEmployee.name);
          break;
        case this._surveyorRole.get('eicManager'):
          this.jobModel.eicManager = lrEmployee;
          this.setOriginalFleetServicesSpecialist(lrEmployee.name);
          break;
        default:
          this.jobModel.generalSurveyors.push(lrEmployee);
          this.addOriginalGeneralSurveyor(lrEmployee.name);
      }
    });

    if (_.isEmpty(this.jobModel.generalSurveyors)) {
      this.jobModel.generalSurveyors.push(null);
      this.jobModel.leadSurveyorIndex = 0;
    }
  }

  /**
   * Cancels the job addition and returns to original page state;
   */
  cancel() {
    /* AC13; states that if nothing was changed or the user selects
       cancel then the view needs to navigate back to the view mode; */
    this._$state.go('asset.jobs.view',
      { jobId: this._job.id }, { reload: true });
  }

  /**
   * @param {Number} index the index to check
   * @returns {Boolean} whether or not the surveyor at the index is the
   *   lead surveyor
   */
  isGeneralSurveyorLead(index) {
    return this.jobModel.leadSurveyorIndex === index;
  }

  /**
   * @param {String} field The name of the model field
   * @return {Boolean} true or false
   * @since 13/06/2016
   */
  isTeamEditable() {
    return !_.includes(
      _.pick(this._jobStatus.toObject(), ['closed', 'aborted', 'cancelled']),
      this.currentJobStatus);
  }

  /**
   * @returns {Boolean} whether or not the job team already exist for this job
   */
  jobTeamExists() {
    return Boolean(this.jobTeamEmployeesAssigned);
  }

  /**
   * Get the currently surveyors on the jobModel and checks for duplicates,
   * setting an appropriate error on the current field if a duplicate is found
   * @param {String} fieldName The name of the field in question
   * @param {Object} field The form field with the typeahead
   * Sets an error on the field
   * @return {boolean} Whether or not to set an error on the fieldName
   * @since 23/06/2016
   */
  onSurveyorSelect(fieldName, field) {
    // Get all fields that have a name that starts with one of the strings in
    // the array _.reduce is called on
    const formFields = _.pickBy(this.teamJobForm, (formField) =>
      _.reduce(
        ['authorisingSurveyor', 'eicManager', 'sdoCoordinator', 'surveyor'],
        (pick, prefix) =>
          pick || _.startsWith(_.get(formField, '$name'), prefix),
        false));

    const replaceRegex = new RegExp('(surveyor)([0-9]+)');

    const groupedFields = _.chain(formFields)

      // We don't care about the value of the form fields, that is wrong at this
      // point in time (for unknown reasons)
      .keys()

      // Change 'surveyor0' type keys to 'generalSurveyors[0]' then get those
      // properties from the jobModel and remove falsey values
      .map((key) =>
        _.replace(key, replaceRegex, 'generalSurveyors[$2]'))
      .map((k) => _.get(this.jobModel, k))
      .compact()

      // Filter out the $pristine fields and group them by the surveyors' names
      // (e.g. 'Allan Hyde') and return the group object
      .map((item) => _.isString(item) ? item : item.name)
      .reject('$pristine')
      .groupBy()
      .value();

    // Get the biggest group
    const count = _.max(_.map(groupedFields, _.size));

    // 'duplicate' gets the field value that is repeated, in case it isn't the
    // one that is being altered (duplicate might not have been altered where
    // it occurred and the user then altered another one)
    const duplicate = _.first(_.orderBy(groupedFields, _.size, 'desc'));

    // If this is less than 3, then two of the values in the form fields we
    // have selected are equal, so this cannot be permitted
    if (count >= 2) {
      this.teamJobForm[fieldName].$setValidity('duplicateName',
        !_.includes(duplicate, field.name));
      return true;
    }

    // Loop the form fields and set the validity to true
    _.forEach(_.keys(formFields), (name) =>
      this.teamJobForm[name].$setValidity('duplicateName', true));

    return false;
  }

  /**
   * Removes the surveyor at the index, updating the leadSurveyorIndex if needed
   * @param {Number} index the index to remove
   */
  removeGeneralSurveyor(index) {
    if (this.jobModel.leadSurveyorIndex > index) {
      this.jobModel.leadSurveyorIndex -= 1;
    }
    this.jobModel.generalSurveyors.splice(index, 1);
  }

  /**
   * Saves the job after editing
   */
  async save() {
    const authoriser = this.teamJobForm.authorisingSurveyor;
    const fleetSpecialist = this.teamJobForm.eicManager;
    const leadSurveyor =
      this.teamJobForm[`surveyor${this.jobModel.leadSurveyorIndex}`];
    const currentJobStatus = this.jobModel.jobStatus.id;

    let correctJobStatus = ['id', currentJobStatus];

    // If there is no authorising surveyor at the start of editing job team
    // and we're adding an authorising surveyor
    if (_.isNil(this._authorisingSurveyor) &&
      !_.isNil(authoriser.$viewValue)) {
      // We still need to check if the job status is the edge case of
      // 'awaiting technical reviewer assignment'; if so, then we can now
      // set the status back to 'under technical review'
      if (currentJobStatus ===
        this._jobStatus.get('awaitingTechnicalReviewerAssignment')) {
        correctJobStatus =
          ['id', this._jobStatus.get('underTechnicalReview')];
      }
    }

    // If there is an authorising surveyor and we're removing them
    if (!_.isNil(this._authorisingSurveyor) &&
      _.isNil(authoriser.$viewValue)) {
      if (currentJobStatus ===
        this._jobStatus.get('underTechnicalReview')) {
        correctJobStatus =
          ['id',
            this._jobStatus.get('awaitingTechnicalReviewerAssignment')];
      }
    }

    // If there is an authorising surveyor and we're replacing them
    if (!_.isNil(this._authorisingSurveyor) &&
      this._authorisingSurveyor !== authoriser.$viewValue) {
      if (currentJobStatus ===
        this._jobStatus.get('awaitingTechnicalReviewerAssignment')) {
        correctJobStatus =
          ['id', this._jobStatus.get('underTechnicalReview')];
      }
    }

    // If there isn't a fleet services specialist and we're setting one
    if (_.isNil(this._fleetServiceSpecialist) &&
      !_.isNil(fleetSpecialist.$viewValue)) {
      if (currentJobStatus ===
        this._jobStatus.get('awaitingEndorserAssignment')) {
        correctJobStatus = ['id', this._jobStatus.get('underEndorsement')];
      }
    }

    // If there is a fleet services specialist and we're setting one
    if (!_.isNil(this._fleetServiceSpecialist) &&
      _.isNil(fleetSpecialist.$viewValue)) {
      if (currentJobStatus === this._jobStatus.get('underEndorsement')) {
        correctJobStatus =
          ['id', this._jobStatus.get('awaitingEndorserAssignment')];
      }
    }

    // If there is a fleet services specialist and we're replacing them
    if (!_.isNil(this._fleetServiceSpecialist) &&
      this._fleetServiceSpecialist !== fleetSpecialist.$viewValue) {
      if (currentJobStatus ===
        this._jobStatus.get('awaitingEndorserAssignment')) {
        correctJobStatus = ['id', this._jobStatus.get('underEndorsement')];
      }
    }

    // Check if there isn't a lead surveyor & that the job is in the
    // resource assigned state - but, only set to under survey if the job
    // scope is confirmed
    if (_.isNil(this._leadSurveyor) && !_.isNil(leadSurveyor.$viewValue)) {
      correctJobStatus = ['id', this._job.scopeConfirmed ?
        this._jobStatus.get('underSurvey') :
        this._jobStatus.get('resourceAssigned') ];
    }

    const status = _.find(
      _.get(this._jobStatusReferenceData, 'job.jobStatuses'),
      correctJobStatus);

    this.jobModel.jobStatus = _.pick(status, 'id');

    this._job =
      await this._jobDecisionService.saveJobTeam(this.jobModel);
    this.setUpJob();

    this.cancel();
  }

  surveyorPreFilter() {
    if (this.teamJobsFormSaved) {
      this._teamJobsFormSaved = false;
    }
    return _.identity;
  }

  // Not alphabetised, but makes more sense here
  addOriginalGeneralSurveyor(surveyor) {
    if (!this._generalSurveyors) {
      this._generalSurveyors = [];
    }

    this._generalSurveyors.push(surveyor);
  }

  setOriginalAuthorisingSurveyor(surveyor) {
    if (!this._authorisingSurveyor) {
      this._authorisingSurveyor = surveyor;
    }
  }

  setOriginalFleetServicesSpecialist(surveyor) {
    if (!this._fleetServiceSpecialist) {
      this._fleetServiceSpecialist = surveyor;
    }
  }

  setOriginalLeadSurveyor(surveyor) {
    if (!this._leadSurveyor) {
      this._leadSurveyor = surveyor;
    }
  }

  setOriginalSdoCoordinator(surveyor) {
    if (!this._sdoCoordinator) {
      this._sdoCoordinator = surveyor;
    }
  }

  /**
   * @param {String} field the field name to check
   * @returns {Boolean}
   */
  userCanEditField(field) {
    return this.accessMap[field];
  }

  /**
   * Called from outside the form, this checks if it's valid or not.
   * @returns {Boolean} is the form valid or not?
   */
  validForm() {
    const leadSet =
      this.jobModel.generalSurveyors[this.jobModel.leadSurveyorIndex];
    return leadSet && !this.teamJobForm.$invalid && !this.teamJobForm.$pristine;
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false depending upon the form item status
   */
  validFormItem(item) {
    return item && item.$dirty && item.$invalid && !item.$pristine;
  }

  /**
   * @summary currentJobStatus()
   * @return {Number} id The job status id
   */
  get currentJobStatus() {
    return this._job.jobStatus.id;
  }

  /**
   * @returns {Object} job Returns the job object;
   */
  get job() {
    return this._job;
  }

  /**
   * @returns {Object} employees The employees object of the job
   */
  get jobTeamEmployees() {
    return _.filter(this._job.employees, (employee) =>
      _.includes(this._employeeRoles, employee.employeeRole.id));
  }

  /**
   * @returns {Number} size Returns the size of the employees object(s);
   */
  get jobTeamEmployeesAssigned() {
    return _.size(this.jobTeamEmployees);
  }
}
