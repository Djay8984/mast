export controller from './team-job.controller';
export templateUrl from './team-job.html';
export const resolve = {

  /* @ngInject */
  jobStatusReferenceData: (referenceDataService) =>
    referenceDataService.get(['job.jobStatuses']),

  /* @ngInject */
  employeeReferenceData: (referenceDataService) =>
    referenceDataService.get(['employee.employee'], true)
};
