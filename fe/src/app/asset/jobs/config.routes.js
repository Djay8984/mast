import HeaderController from '../header/asset-header.controller';
import headerTemplateUrl from '../header/asset-header.html';

import * as jobList from './list';
import * as jobView from './view';
import * as jobEdit from './edit';
import * as jobAdd from './add';

import * as addJobsScope from './add-scope';
import * as editJobsScope from './edit-scope';
import * as team from './team';
import * as endorsement from './endorsement';

/* @ngInject */
export default function config($stateProvider, stateHelper) {
  const routes = [
    {
      title: 'Jobs list',
      name: 'asset.jobs.list',
      parent: 'asset',
      url: '/jobs/list',
      content: jobList,
      target: '',
      header: { open: true },
      params: {
        jobFilter: null
      }
    },
    {
      title: 'Add new Job',
      name: 'asset.jobs.add',
      parent: 'asset',
      url: '/jobs/add',
      content: jobAdd,
      params: {
        caseId: null,
        backTarget: null
      }
    },
    {
      name: 'asset.jobs',
      parent: 'asset',
      url: '/jobs/{jobId:int}',
      abstract: true,
      header: { open: true },
      data: {
        test: 333
      },
      viewsHeader: {
        controller: HeaderController,
        templateUrl: headerTemplateUrl
      },
      resolve: {

        /* @ngInject */
        assetForJob: ($stateParams, assetDecisionService) =>
          assetDecisionService.get($stateParams),

        /* @ngInject */
        job: ($stateParams, jobDecisionService) =>
          jobDecisionService.get($stateParams.jobId),

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'employee.office',
            'employee.employee',
            'employee.employeeOffices'
          ], true)
      },
      children: [
        {
          title: 'View Job {{job.name}}',
          name: 'view',
          url: '/view',
          header: { open: false },
          params: {
            backTarget: null,
            caseId: null
          },
          content: jobView
        },
        {
          title: 'Edit job',
          name: 'edit',
          url: '/edit',
          content: jobEdit
        },
        {
          name: 'add-scope',
          url: '/add-scope',
          content: addJobsScope
        },
        {
          name: 'edit-scope',
          url: '/edit-scope',
          content: editJobsScope,
          params: {
            addition: null
          }
        },
        {
          name: 'team',
          url: '/team',
          content: team
        },
        {
          name: 'endorsement',
          url: '/endorsement',
          content: endorsement
        }
      ]
    }
  ];

  stateHelper.create($stateProvider, routes);
}
