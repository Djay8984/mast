import * as angular from 'angular';

import routes from './config.routes';

import addScope from './add-scope';
import crediting from './view/crediting';
import reporting from './view/reporting';

export default angular
  .module('app.asset.jobs', [
    addScope,
    crediting,
    reporting
  ])
  .config(routes)
  .name;

export controller from './jobs.controller';
export templateUrl from './jobs-index.html';
