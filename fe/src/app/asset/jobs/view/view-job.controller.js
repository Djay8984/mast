import Base from 'app/base.class';
import * as _ from 'lodash';
import Report from 'app/models/report.model';

import EndorsementCardClass from '../endorsement/card.class';

import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

import RejectDSRController from './crediting/manage/reject-dsr.controller';
import rejectDSRTemplateUrl from './crediting/manage/reject-dsr-template.html';

import PrintMixin from 'app/components/print/print.mixin';

import jobDatesTemplate from 'app/asset/jobs/view/dates.html';
import editJobVisitDatesModalController from
  './edit-job-visit-dates-modal.controller';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ViewJobController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $log,
    $state,
    $stateParams,
    $scope,
    $rootScope,
    printService,
    MastModalFactory,
    dateFormat,
    moment,
    jobOnlineService,
    jobDecisionService,
    referenceData,
    reportDecisionService,
    reportType,
    surveyorRole,
    allowedSpecialCharactersRegex,
    activeBundle,
    job,
    jobStatus,
    officeRole,
    serviceTypeTaskCompletions,
    surveys,
    reports,
    user,
    wipActionableItems,
    wipAssetNotes,
    wipCoCs,
    resolve
  ) {
    super(arguments);
    this.setUpData();
  }

  setUpData() {
    this.models = {};
    this.title = 'ViewJobController';
    this._employeeRoles =
      _.values(_.pick(this._surveyorRole.toObject(),
      ['sdoCoordinator', 'leadSurveyor', 'authorisingSurveyor', 'eicManager']));
    this.card = {
      endorsement: new EndorsementCardClass(this)
    };

    this.printPayload = {
      dynamicPage: true
    };

    this.jobmodel = _.assign({}, this._job);
  }

  get canCredit() {
    return this._job.scopeConfirmed;
  }

  /**
   * @summary serviceTypeTaskCompletions()
   * @return {Object} serviceTypeTaskCompletions From the resolve
   */
  get serviceTypeTaskCompletions() {
    return this._serviceTypeTaskCompletions;
  }

  /**
   * @summary backTarget()
   * @return {String} backTarget The state params' back target, if set
   */
  get backTarget() {
    return 'asset.jobs.list';
  }

  /**
   * @summary currentUser()
   * @return {Object} user The current user object
   */
  get currentUser() {
    return this._user;
  }

  /**
   * @summary job()
   * @return {Object} job Getter for the job
   */
  get job() {
    return this._job;
  }

  /**
   * @return {String} date The first visit date on the job
   */
  get firstVisitDate() {
    return _.get(this.job, 'firstVisitDate') ?
      this._moment(_.get(this.job, 'firstVisitDate'))
        .format(this._dateFormat) : '';
  }

  /**
   * @return {String} date The last visit date on the job
   */
  get lastVisitDate() {
    return _.get(this.job, 'lastVisitDate') ?
      this._moment(_.get(this.job, 'lastVisitDate'))
        .format(this._dateFormat) : '';
  }

  get currentWipCount() {
    return _.size(_.flatten([
      this._surveys,
      this._wipCoCs,
      this._wipAssetNotes,
      this._wipActionableItems
    ]));
  }

  get currentJobTeam() {
    return _.filter(this._job.employees, (employee) =>
      _.includes(this._employeeRoles, employee.employeeRole.id));
  }

  /**
   * @returns {array} list of report objects
   */
  get reports() {
    if (!_.has(this.models, 'reports') && _.size(this._reports)) {
      this.models.reports = _.map(this._reports, (report) =>
        Reflect.construct(Report, [report]));
    }

    return this.models.reports;
  }

  /**
   * @return {Number} reportTypeId
   */
  get reportTypeId() {
    return _.get(this.report, 'reportType.id', null);
  }

  /**
   * @returns {Array} reports An array of DSR report objects
   */
  get reportsDSR() {
    if (!this._reportsDSR) {
      this._reportsDSR = _.filter(this.models.reports,
        { reportType: { id: this._reportType.get('dsr') } });
    }
    return this._reportsDSR;
  }

  /**
   * @returns {object} the latest DSR
   */
  get latestDSR() {
    return _.last(_.sortBy(this.reportsDSR, 'reportVersion'));
  }

  /**
   * @returns {Object} reports The latest DSRs that aren't rejected
   */
  get latestDSRPending() {
    const reports = _.reject(this._reportsDSR, { approved: false });
    return _.last(_.sortBy(reports, 'reportVersion'));
  }

  /**
   * @returns {object} the latest FSR
   */
  get latestFSR() {
    const reports = _.filter(this.reports,
      { reportType: { id: this._reportType.get('fsr') } });

    return _.last(_.sortBy(reports, 'reportVersion'));
  }

  /**
   * @summary generateDSR()
   */
  async generateDSR() {
    // Save the report temporarily; use a const so we can pass this through
    // to the template page where we view the generated DSR;
    const report =
      await this._reportDecisionService
        .createReport(this.job.id, 'dsr',
          this._moment().toISOString(), 'false', 'true', null);

    _.merge(this._$stateParams,
      {
        report,
        reportTypeId: this._reportType.get('dsr'),
        jobId: this.job.id,
        backTarget: this._$state.current.name
      });

    // Fixes issue with DSR not showing on carousel;
    this._reportsDSR = null;

    // Then navigate to view the report;
    this._$state.go('reporting.view', this._$stateParams);
  }

  /**
   * @summary actionOnDSR()
   * @param {String} actionTaken The action we're carrying out on the DSR
   * @param {Number} id The id of the report, so we can retrive the DSR version
   * @description This method gets included on the directive inside the
   *   technical review;
   * @since 20/07/16
   */
  async actionOnDSR(actionTaken, id) {
    switch (actionTaken) {
      case 'reject': {
        const confirmation = await new this._MastModalFactory({
          controller: RejectDSRController,
          controllerAs: 'vm',
          templateUrl: rejectDSRTemplateUrl,
          inject: {
            allowedSpecialCharactersRegex: this._allowedSpecialCharactersRegex
          },
          class: 'dialog'
        }).activate();

        // If they 'cancel' out of the modal it sends back reject which only
        // cancels the modal, so we want the confirmation as the reason;
        if (confirmation !== 'reject') {
          await this._reportDecisionService
            .rejectReport(this._job.id, id, 'dsr', confirmation);
          this._$state.go(this._$state.current, this._$stateParams,
            { reload: true });
        }
        break;
      }
      case 'approve':
      default: {
        const confirmation = await new this._MastModalFactory({
          templateUrl: confirmationTemplateUrl,
          scope: {
            title: 'Approve DSR',
            message: 'You have selected to approve the DSR.',
            actions: [
              { name: 'Cancel', result: false },
              { name: 'Approve DSR', result: true }
            ]
          },
          class: 'dialog'
        }).activate();

        if (confirmation === true) {
          // This gets the DSR, which we can then use to post the FSR;
          const dsr = await this._reportDecisionService.get(this._job.id, id);

          // Call the report service to create the fsr report; this will also
          // set all the necessary fields on the new report object for us;
          await this._reportDecisionService
            .createReport(this.job.id, 'fsr',
              this._moment().toISOString(), 'true', 'false', dsr);
          this._$state.go(this._$state.current, this._$stateParams,
            { reload: true });
        }
        break;
      }
    }
  }

  /**
   * @summary reportNotSubmitted()
   * @return {Boolean} true or false
   */
  get reportNotSubmitted() {
    return !_.isNull(this._$stateParams.report);
  }

  /**
   * @return {String} backTarget The back target depending on the report
   */
  get reportBackTarget() {
    return this.reportNotSubmitted ? 'crediting.manage' : 'asset.jobs.view';
  }

  /**
   * @summary getLeadSurveyor()
   * @description Since the template doesn't display the name, we're using the
   *   safer option of getting the id rather than the name
   * @return {Number} id The id of the lead surveyors
   * @since 01/08/2016
   */
  getLeadSurveyor() {
    const surveyors =
      _.find(this._job.employees,
        { employeeRole: { id: this._surveyorRole.get('leadSurveyor') } });
    return _.get(surveyors, 'lrEmployee.id', false);
  }

  /**
   * @summary currentUserIsInTeam()
   * @description Is this user part of the team assigned to this job?
   * @return {Boolean} true or false
   */
  get currentUserIsInTeam() {
    return _.some(this._job.employees,
      {
        lrEmployee: { id: this._user.id }
      }
    );
  }

  /**
   * @summary currentUserIsLeadSurveyor()
   * @description Is this current user the lead surveyor?
   * @return {Boolean} true or false
   */
  get currentUserIsLeadSurveyor() {
    return _.find(this._job.employees,
      { lrEmployee: { id: _.parseInt(this._user.id) },
        employeeRole: { id: this._surveyorRole.get('leadSurveyor') } });
  }

  /**
   * @summary editableSDO()
   * @returns {Boolean} true or false
   * @since 06/06/2016
   */
  editableSDO() {
    return true;
  }

  get limitedToJob() {
    return this._activeBundle;
  }

  /**
   * @summary jobScopeConfirmedNotice()
   * @return {String} status The current status of the scopeConfirmed flag
   *   determines what string gets returned
   */
  get jobScopeConfirmedNotice() {
    if (this.currentWipCount < 1) {
      return 'to be defined';
    }
    if (this._job.scopeConfirmed) {
      return 'confirmed';
    }
    return 'to be confirmed';
  }

  /**
   * @summary jobScopeConfirmed()
   * @description Returns the scopeConfirmed flag on the job
   * @return {Boolean} true or false
   */
  get jobScopeConfirmed() {
    return this._job.scopeConfirmed;
  }

  /**
   * @summary jobUnderReporting()
   * @return {Boolean} true or false
   */
  get jobUnderReporting() {
    return _.get(this._job, 'jobStatus.id') ===
      this._jobStatus.get('underReporting');
  }

  /**
   * @summary shouldShowTeamAndScope()
   * @return {Boolean} true or false
   */
  get shouldShowTeamAndScope() {
    if (_.get(this._job, 'jobStatus.id') ===
      this._jobStatus.get('underEndorsement')) {
      return true;
    }
    return true;
  }

  /**
   * @summary showJobDatesEditor()
   */
  async showJobDatesEditor() {
    await this._MastModalFactory({
      controller: editJobVisitDatesModalController,
      controllerAs: 'vm',
      inject: {
        $state: this._$state,
        job: this._job,
        jobDecisionService: this._jobDecisionService
      },
      templateUrl: jobDatesTemplate,
      class: 'job-updates-modal'
    }).activate();
  }

  /**
   * @summary canEditJobDates()
   * @return {Boolean} true or false
   */
  canEditJobDates() {
    const status = _.get(this._job, 'jobStatus.constant');
    const employeeAssignedToSdo = _.find(_.get(this._referenceData,
      'employee.employeeOffices'), { office: {
        id: _.first(this.job.model.offices).office.id },
          employee: { id: _.get(this, 'currentUser.id') } });
    const employeeAssignedToJob = _.find(this.job.employees,
      { lrEmployee: { id: _.get(this, 'currentUser.id') } });

    // job is sdo assigned (1), resource assigned (2) or
    // awaiting tr assignment (5) and therefore anyone assigned
    // to the same SDO or the job team can edit the dates
    if (_.includes([
      'sdoAssigned',
      'resourceAssigned',
      'awaitingTechnicalReviewerAssignment'
    ], status) && employeeAssignedToSdo || employeeAssignedToJob) {
      return true;

    // job is under survey (3) or under reporting (4) and therefore the employee
    // must be assigned to the job in order to edit the dates
    } else if (_.includes([
      'underSurvey',
      'underReporting'
    ], status) && employeeAssignedToJob) {
      return true;
    }

    return false;
  }
}
