import Base from 'app/base.class';
import * as _ from 'lodash';

export default class EditJobVisitDatesModalController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    $modalInstance,
    moment,
    dateFormat,
    jobDecisionService,
    job
  ) {
    super(arguments);
    this.setUpData();
  }

  setUpData() {
    this.dates = {
      firstVisitDateMin: this._moment().add(-9999, 'Y').hour(12).startOf('h'),
      firstVisitDateMax: this._moment().add(-9999, 'Y').hour(12).startOf('h')
    };

    this.jobmodel = _.assign({}, this._job);

    // if a first date hasn't already been specified, suggest today's date
    if (!this.jobmodel.firstVisitDate) {
      this.jobmodel.firstVisitDate = this._moment();
    }
  }

  /**
   * @return {String} date The first visit date on the job
   */
  get firstVisitDate() {
    return this._moment(_.get(this.job, 'firstVisitDate'));
  }

  /**
   * @return {String} date The last visit date on the job
   */
  get lastVisitDate() {
    return this._moment(_.get(this.job, 'lastVisitDate'));
  }

  /**
   * @return {String} minLastVisitDate The minimum first visit date
   */
  get minLastVisitDate() {
    return this._moment(_.get(this.job, 'firstVisitDate'))
      .format(this._dateFormat) || _.get(this.dates, 'firstVisitDateMin');
  }

  /**
   * @return {String} maxFirstVisitDate The maximum first visit date
   */
  get maxFirstVisitDate() {
    return this._moment(_.get(this.job, 'lastVisitDate'))
      .format(this._dateFormat) || _.get(this.dates, 'firstVisitDateMax');
  }

  /**
   * @return {String} dateFormat The format of dates, from the constants;
   */
  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false Depends on the form item status
   */
  validFormItem(item) {
    if (
      this._moment(this.editJobsDatesForm.lastVisitDate.$viewValue)
      .isBefore(this.editJobsDatesForm.firstVisitDate.$viewValue)
    ) {
      this.editJobsDatesForm.lastVisitDate.$setValidity('min', false);
    } else {
      this.editJobsDatesForm.lastVisitDate.$setValidity('min', true);
    }

    return item && item.$dirty && item.$invalid && !item.$pristine;
  }

  /**
   * Determine whether the form is valid
   * @return {Boolean} true or false Depends on the form item status
   */
  validForm() {
    if (
      this.editJobsDatesForm.lastVisitDate.$modelValue &&
      !this.editJobsDatesForm.firstVisitDate.$modelValue
    ) {
      return false;
    }

    return this.editJobsDatesForm.$valid;
  }

  /**
   * Update the first and last visit dates for a job
   * @param  {Object} dates An object containing first and last visit dates
   */
  async saveVisitDates(dates) {
    // need to make sure we're sending through null if the value of
    // the date picker has been left empty
    dates.firstVisitDate = dates.firstVisitDate === '' ?
      null : dates.firstVisitDate;
    dates.lastVisitDate = dates.lastVisitDate === '' ?
      null : dates.lastVisitDate;

    const oldJob = await this._jobDecisionService.get(this._job.id);
    const updatedJob = _.merge(oldJob, dates);

    // update the job with the new date
    this._job = await this._jobDecisionService.updateJob(
      _.omit(updatedJob, ['model', '_offices']), true);

    // close the modal
    this._$modalInstance.close();

    // reload the state
    this._$state.go(this._$state.current, {}, { reload: true });
  }
}
