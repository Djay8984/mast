import * as angular from 'angular';
import routes from './config.routes';

import actionableItem from './actionable-item';
import assetNote from './asset-note';
import coc from './coc';
import defect from './defect';
import manage from './manage';
import repair from './repair';

export default angular
  .module('app.asset.jobs.crediting', [
    actionableItem,
    assetNote,
    coc,
    defect,
    manage,
    repair
  ])
  .config(routes)
  .name;
