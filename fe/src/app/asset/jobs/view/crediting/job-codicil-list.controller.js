import Base from 'app/base.class';
import * as _ from 'lodash';
import PrintMixin from 'app/components/print/print.mixin';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class JobCodicilListController extends PrintMixin(Base) {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    $scope,
    $rootScope,
    asset,
    jobStatus,
    codicilFilter,
    itemType,
    job,
    jobAttachmentCount,
    jobAttachmentPath,
    jobCodicils,
    listContents,
    navigationService,
    otherCodicils,
    printService
  ) {
    super(arguments);

    // ToDo remove if not needed taken out 20.07.2016
    // Temp removed if this breaks the AC in 10.24
    // put back and we'll fix it another way

    // this._jobCodicils = _.union(
    //   _.filter(jobCodicils, 'jobScopeConfirmed'),
    //   _.filter(jobCodicils, { childWIPs: null })
    // );
  }

  get codicilFilter() {
    return this._codicilFilter;
  }

  addCodicil() {
    this._$state.go('^.add', { jobId: this._job.id, context: 'job' });
  }

  /**
   * @summary ctaVisible()
   * @description This checks the job status and disables the CTA if the job
   *   status is in certain conditions, regardless of membership of the team;
   * @returns {Boolean} true or false
   */
  ctaVisible() {
    // If job scope is not confirmed, user should not be able to add a new
    // codicil
    if (!this._job.scopeConfirmed) {
      return false;
    }

    // After the job has had either DSR or FSR generated, the status of the job
    // is in these, and so disables the CTA buttons;
    const disabledJobStatuses = _.values(_.pick(this._jobStatus.toObject(),
      [
        'aborted',
        'awaitingEndorserAssignment',
        'awaitingTechnicalReviewerAssignment',
        'cancelled',
        'closed',
        'underEndorsement',
        'underTechnicalReview'
      ]));

    return !_.includes(disabledJobStatuses, this._job.jobStatus.id);
  }

  get job() {
    return this._job;
  }

  get jobAttachmentCount() {
    return this._jobAttachmentCount;
  }

  get jobAttachmentPath() {
    return this._jobAttachmentPath;
  }

  get listContents() {
    return this._listContents;
  }

  get itemType() {
    return this._itemType;
  }

  get prettyPluralItemType() {
    return _.get({
      actionableItem: 'Actionable Items',
      assetNote: 'Asset Notes',
      coc: 'CoCs',
      defect: 'Defects'
    }, _.camelCase(this.itemType));
  }

  get viewType() {
    return this._$stateParams.searchFilter.viewType;
  }

  get jobCodicils() {
    return this._jobCodicils;
  }

  get otherCodicils() {
    return this._otherCodicils;
  }

  get viewStateName() {
    return '^.view';
  }

  async print(stateName) {
    const jobCodicilsForPrint =
      await this._jobCodicils.printData();
    const otherCodicilsForPrint =
      await this._otherCodicils.printData();

    this.printPayload = _.extend({}, this._asset, {
      confidentialityType: { id: 'All' },
      job: this._job,
      additionalInfo: []
    });

    if (jobCodicilsForPrint) {
      this.printPayload.jobs = jobCodicilsForPrint;
      this.printPayload.additionalInfo.push(`jobs:${this.itemType}`);
    }

    if (otherCodicilsForPrint) {
      this.printPayload.others = otherCodicilsForPrint;
      this.printPayload.additionalInfo.push(`others:${this.itemType}`);
    }

    super.print(stateName);
  }
}
