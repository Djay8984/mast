import * as _ from 'lodash';

import jobCodicilListTemplateUrl from '../job-codicil-list.html';
import JobCodicilListController from '../job-codicil-list.controller';

import AddAssetNoteController from
  'app/codicil/asset-note/add/add-asset-note.controller.js';
import EditAssetNoteController from
  'app/codicil/asset-note/edit/edit-asset-note.controller';
import ViewAssetNoteController from
  'app/codicil/asset-note/view/view-asset-note.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.assetNotes.list', {
      parent: 'crediting.assetNotes',
      url: '/asset-note/list',
      views: {
        '@main': {
          templateUrl: jobCodicilListTemplateUrl,
          controller: JobCodicilListController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            codicilFilter: (
              assetNoteCategory,
              assetNoteStatus,
              itemType
            ) => ({
              statusList: _.values(_.pick(
                  assetNoteStatus.toObject(),
                  ['open', 'inactive', 'changeRecommended'])),
              confidentialityList: [1, 2, 3],
              typeList: [
                { slug: 'asset-note', categories: assetNoteCategory.toArray() }
              ]
            }),

            /* @ngInject */
            jobCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'other') {
                const searchParameters = [
                  { jobId: $stateParams.jobId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                  $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.jobCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            },

            /* @ngInject */
            otherCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'job') {
                const searchParameters = [
                  { assetId: $stateParams.assetId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                    $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.otherCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            }
          }
        }
      }
    })
    .state('crediting.assetNotes.add', {
      parent: 'crediting.assetNotes',
      url: '/asset-note/add',
      params: {
        context: 'job'
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddAssetNoteController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.assetNotes.view', {
      parent: 'crediting.assetNotes',
      url: '/asset-note/{codicilId:int}/view?context',
      resolve: {

        /* @ngInject */
        assetNote: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getAssetNote(
            _.pick($stateParams, [`${$stateParams.context}Id`, 'codicilId']))
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewAssetNoteController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.assetNotes.edit', {
      parent: 'crediting.assetNotes',
      url: '/asset-note/{codicilId:int}/edit?context',
      resolve: {

        /* @ngInject */
        assetNote: (codicilDecisionService, $stateParams) => {
          const params =
            _.pick($stateParams, [`${$stateParams.context}Id`, 'codicilId']);

          return codicilDecisionService.getAssetNote(params);
        }
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: EditAssetNoteController,
          controllerAs: 'vm'
        }
      }
    });
}
