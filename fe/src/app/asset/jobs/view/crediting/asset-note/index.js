import * as angular from 'angular';

import routes from './config.routes';

export default angular
  .module('app.asset.jobs.crediting.asset-note', [])
  .config(routes)
  .name;
