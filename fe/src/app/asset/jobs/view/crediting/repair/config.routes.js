import ViewRepairController from
  'app/codicil/repair/view/view-repair.controller';

import ViewTemplateUrl from 'app/codicil/view.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.repairs.view', {
      parent: 'crediting.repairs',
      params: {
        repairId: null,
        repairType: null,
        parentId: null
      },
      url: '/repair/{codicilId:int}/view?context',
      resolve: {

        /* @ngInject */
        repair: (repairDecisionService, $stateParams) =>
          repairDecisionService.get($stateParams.repairType, {
            assetId: $stateParams.assetId,
            codicilId: $stateParams.parentId,
            repairId: $stateParams.codicilId,
            jobId: $stateParams.jobId,
            context: 'job'
          })
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewRepairController,
          controllerAs: 'vm'
        }
      }
    });
}
