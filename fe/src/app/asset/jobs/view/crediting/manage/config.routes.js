import * as _ from 'lodash';

import ManageCreditingController from './manage-crediting.controller';
import manageCreditingTemplateUrl from './manage-crediting.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.manage', {
      parent: 'crediting',
      url: '/crediting/manage',
      resolve: {

        /* @ngInject */
        crediting: ($stateParams, creditingDecisionService, job) =>
          creditingDecisionService
            .get($stateParams.jobId, job.asset.id)
            .then((crediting) => creditingDecisionService
              .groupCreditingByProductType(job, crediting)),

        /* @ngInject */
        creditStatuses: (referenceDataService) =>
          referenceDataService.get(['job.creditStatuses']),

        /* @ngInject */
        cocs: ($stateParams, codicilDecisionService, cocStatus) =>
          codicilDecisionService
            .query(_.pick($stateParams, 'jobId'), {
              statusList: _.values(
                  _.omit(cocStatus.toObject(), 'cancelled')),
              typeList: [ { slug: 'coc' } ]
            }, 'coc', true)
            .then((items) => _.remove(items,
                (item) => !(_.isEmpty(item.parent) &&
                  !_.isEqual(item.status.id, cocStatus.get('deleted'))))),

        /* @ngInject */
        actionableItems:
          ($stateParams, codicilDecisionService, actionableItemStatus) =>
          codicilDecisionService
            .query(_.pick($stateParams, 'jobId'), {
              statusList: _.values(
                  _.omit(actionableItemStatus.toObject(), 'cancelled')),
              typeList: [ { slug: 'actionable-item' } ]
            }, 'actionable-item', true)
            .then((items) => _.remove(items,
              (item) => !(_.isEmpty(item.parent) &&
                !_.isEqual(item.status.id,
                  actionableItemStatus.get('deleted'))))),

        /* @ngInject */
        farSubmissionState: ($stateParams, reportDecisionService) =>
          reportDecisionService.getFARSubmissionState($stateParams.jobId),

        /* @ngInject */
        surveyCreditStatuses: (referenceDataService) =>
          referenceDataService.get(['service.serviceCreditStatuses'])
      },
      views: {
        '@main': {
          templateUrl: manageCreditingTemplateUrl,
          controller: ManageCreditingController,
          controllerAs: 'vm'
        }
      }
    });
}
