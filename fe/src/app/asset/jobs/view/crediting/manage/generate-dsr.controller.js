import * as _ from 'lodash';

import Base from 'app/base.class';

export default class GenerateDSRController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    moment,
    reportDecisionService,
    dateFormat,
    job,
    reportType
  ) {
    super(arguments);

    /** @type {Object} */
    this.dar = { };
    this.dates = {
      now: moment().format(this._dateFormat),
      minDate: moment().add(-1000, 'Y').hour(0).startOf('h')
    };

    // Fixes issue where the field needs to be set to current date;
    this.dar.firstVisitDate = this.dates.now;

    this.jobReportVersions();
  }

  get job() {
    return this._job;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @summary generateDARActive()
   * @return {Boolean} true or false
   */
  get generateDARActive() {
    if (this.generateDARForm.$invalid) {
      return false;
    }
    return true;
  }

  /**
   * @summary darFormMinDate()
   */
  darFormMinDate() {
    let reportDate = _.min(_.map(this._reports, 'issueDate'));
    if (_.isNil(reportDate)) {
      reportDate = this.dates.minDate;
    }
    const minimumDate =
      !_.isEmpty(this._reports) ? reportDate : this.dates.minDate;
    this.dates.minDate = this._moment(minimumDate).hour(1);
  }

  /**
   * reports A sorted (by firstVisitDate) array of all the job
   *   reports for this job;
   * @summary jobReportVersions()
   */
  async jobReportVersions() {
    if (!this._reports) {
      this._reports =
        _.orderBy(await this._reportDecisionService.getVersions(this.job.id,
          this._reportType.get('dar')), ['firstVisitDate']);
    }
    this.darFormMinDate();
  }

  /**
   * @summary saveDAR()
   */
  async saveDAR() {
    const firstVisitDate =
      _.get(this.generateDARForm.firstVisitDate.$modelValue,
        '_d').toISOString();
    this._$modalInstance.close(firstVisitDate);
  }
}
