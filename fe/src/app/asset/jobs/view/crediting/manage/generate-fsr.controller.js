import * as _ from 'lodash';

import Base from 'app/base.class';

export default class GenerateFSRController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    dateFormat,
    job,
    moment,
    reportDecisionService,
    reportType
  ) {
    super(arguments);

    /** @type {Object} */
    this.dates = {
      now: moment().format(this._dateFormat),
      maxDate: moment().hour(23).startOf('h'),
      minDate: moment().year(-9999).startOf('h')
    };
    this.far = { lastVisitDate: this._job.lastVisitDate || null };

    this.jobReportVersions();
  }

  get job() {
    return this._job;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @summary generateFARActive()
   * @return {Boolean} true or false
   */
  get generateFARActive() {
    return !this.generateFARForm.$invalid;
  }

  /**
   * @summary farFormMinDate()
   * @return {Object} date
   */
  get farFormMinDate() {
    const minimumDate = _.size(this._reports) ?
      _.min(_.map(this._reports, 'issueDate')) : this.dates.minDate;
    return this._moment(minimumDate).hour(1);
  }

  /**
   * @summary farFormNowDate()
   * @return {Object} date
   */
  get farFormNowDate() {
    return _.get(this._job, 'lastVisitDate', this.dates.now);
  }

  /**
   * reports A sorted (by firstVisitDate) array of all the job
   *   (daily) reports for this job;
   * @summary jobReportVersions()
   */
  async jobReportVersions() {
    if (!this._reports) {
      this._reports = _.orderBy(await this._reportDecisionService
        .getVersions(this.job.id, this._reportType.get('dar')),
        'issueDate');
    }
  }

  /**
   * @summary saveFAR()
   */
  async saveFAR() {
    const lastVisitDate =
      _.get(this.generateFARForm.lastVisitDate.$modelValue,
        '_d').toISOString();

    this._$modalInstance.close(lastVisitDate);
  }
}
