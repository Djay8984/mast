import * as _ from 'lodash';

import Base from 'app/base.class';

export default class GenerateDARController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    moment,
    jobDecisionService,
    reportDecisionService,
    dateFormat,
    job,
    reportType
  ) {
    super(arguments);

    /** @type {Object} */
    this.dates = {
      now: moment(),
      minDate: moment().add(-1000, 'Y').hour(0).startOf('h')
    };
    this.dar = { firstVisitDate: this.dates.now };

    this.jobReportVersions();
  }

  get job() {
    return this._job;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @summary generateDARActive()
   * @return {Boolean} true or false
   */
  get generateDARActive() {
    if (!_.isNil(this.dar.firstVisitDate) && this.generateDARForm.$invalid) {
      return false;
    }
    return true;
  }

  /**
   * @summary darFormMinDate()
   */
  darFormMinDate() {
    let reportDate = _.min(_.map(this._reports, 'issueDate'));
    if (_.isNil(reportDate)) {
      reportDate = this.dates.minDate;
    }
    const minimumDate =
      !_.isEmpty(this._reports) ? reportDate : this.dates.minDate;
    this.dates.minDate = this._moment(minimumDate);
  }

  /**
   * @summary jobReportVersions()
   * reports A sorted (by firstVisitDate) array of all the job
   *   reports for this job;
   */
  async jobReportVersions() {
    if (!this._reports) {
      this._reports = await this._reportDecisionService.getVersions(this.job.id,
          this._reportType.get('dar'));
    }
    this.darFormMinDate();
    this.generateDARForm.$setDirty();
  }

  /**
   * @summary saveDAR()
   */
  async saveDAR() {
    const visitDate = this.generateDARForm.firstVisitDate.$modelValue;
    const firstVisitDate = _.isString(visitDate) ?
      this._moment(visitDate).add(2, 'h').toISOString() :
      _.get(this.generateDARForm.firstVisitDate.$modelValue,
        '_d').toISOString();
    if (_.isEmpty(this._reports)) {
      const job = await this._jobDecisionService.get(this._job.id);

      // Check first visit date again just in case, & set the first visit date;
      if (_.isNull(job.firstVisitDate)) {
        job.firstVisitDate = firstVisitDate;
        this._job = await this._jobDecisionService.updateJob(job, true);
      }
    }
    this._$modalInstance.close(firstVisitDate);
  }
}
