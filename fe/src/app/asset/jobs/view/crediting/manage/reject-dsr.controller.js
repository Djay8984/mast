import Base from 'app/base.class';

import * as _ from 'lodash';

export default class RejectDSRController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    allowedSpecialCharactersRegex
  ) {
    super(arguments);
  }

  get allowedSpecialCharactersRegex() {
    return this._allowedSpecialCharactersRegex;
  }

  validForm() {
    return !_.size(this.rejectDSRForm.rejectDSRReason.$viewValue);
  }

  decline() {
    this._$modalInstance.close('reject');
  }

  confirm() {
    this._$modalInstance.close(this.dsr.reason);
  }

  validateForm(item) {
    return item.$dirty && item.$invalid && !item.$pristine;
  }
}
