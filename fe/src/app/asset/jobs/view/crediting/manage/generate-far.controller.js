import * as _ from 'lodash';

import Base from 'app/base.class';

export default class GenerateFARController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    moment,
    jobDecisionService,
    reportDecisionService,
    dateFormat,
    job,
    reportType
  ) {
    super(arguments);

    /** @type {Object} */
    this.far = { };
    this.dates = {
      now: moment().hour(0).startOf('h'),
      maxDate: moment().add(1000, 'year').startOf('h'),
      minDate: moment().year(-1000).startOf('h')
    };

    this.jobReportVersions();
  }

  get job() {
    return this._job;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @summary generateFARActive()
   * @return {Boolean} true or false
   */
  get generateFARActive() {
    if (!_.isNil(this.far.lastVisitDate) && this.generateFARForm.$invalid) {
      return false;
    }
    return true;
  }

  /**
   * @summary farFormMinDate()
   * @return {Object} date
   */
  get farFormMinDate() {
    return this.firstVisitDate;
  }

  get farFormNowDate() {
    return this.lastVisitDate || this.dates.minDate;
  }

  /**
   * @summary firstVisitDate()
   * @return {String} date Moment formatted date;
   */
  get firstVisitDate() {
    return this._moment(this._job.firstVisitDate).format(this._dateFormat);
  }

  /**
   * @summary lastVisitDate()
   * @return {String} date Moment formatted date;
   */
  get lastVisitDate() {
    return !_.isUndefined(this._job.lastVisitDate) ?
      this._moment(this._job.lastVisitDate).format(this._dateFormat) : '';
  }

  /**
   * @summary jobReportVersions()
   * reports A sorted (by firstVisitDate) array of all the job
   *   (daily) reports for this job;
   */
  async jobReportVersions() {
    if (!this._reports) {
      this._reports = await this._reportDecisionService.getVersions(this.job.id,
        this._reportType.get('dar'));
    }
    this.farFormMinDate();
    this.generateFARForm.$setDirty();
  }

  /**
   * @summary saveFAR()
   */
  async saveFAR() {
    const lastVisitDate =
      _.get(this.generateFARForm.lastVisitDate.$modelValue,
        '_d').toISOString();

    // The FAR generation overwrites the last visit date no matter what;
    const job = await this._jobDecisionService.get(this._job.id);
    job.lastVisitDate = lastVisitDate;
    this._job = await this._jobDecisionService.updateJob(job, true);

    this._$modalInstance.close(lastVisitDate);
  }
}
