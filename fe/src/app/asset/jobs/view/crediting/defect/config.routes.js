import * as _ from 'lodash';

import Item from 'app/models/item.model';

import jobCodicilListTemplateUrl from '../job-codicil-list.html';
import JobCodicilListController from '../job-codicil-list.controller';

import AddDefectController from
  'app/codicil/defect/add/add-defect.controller';
import addDefectTemplateUrl from
  'app/codicil/defect/add/add-defect.html';
import ViewDefectController from
  'app/codicil/defect/view/view-defect.controller';
import viewDefectTemplateUrl from
  'app/codicil/defect/view/view-defect.html';
import EditDefectController from
  'app/codicil/defect/edit/edit-defect.controller';
import editDefectTemplateUrl from
  'app/codicil/defect/edit/edit-defect.html';

import addCocController from 'app/codicil/coc/add/add-coc-defect.controller';
import addAIController from
  'app/codicil/actionable-item/add/add-defect-ai.controller';
import AddRepairController from
  'app/codicil/repair/add/add-repair.controller';

import InputTemplateUrl from 'app/codicil/input.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.defects.list', {
      parent: 'crediting.defects',
      url: '/defect/list',
      views: {
        '@main': {
          templateUrl: jobCodicilListTemplateUrl,
          controller: JobCodicilListController,
          controllerAs: 'vm',
          resolve: {

            /*
             * NB: The defectId is known here as the codicilId even though
             * strictly speaking it's not a codicil.
             */

            /* @ngInject */
            codicilFilter: (
              defectStatus,
              itemType
            ) => ({
              statusList: [defectStatus.get('open')],
              confidentialityList: [1, 2, 3],
              typeList: [
                { slug: 'defect', categories: [1, 2, 3, 4, 5] }
              ]
            }),

            /* @ngInject */
            jobCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'other') {
                const searchParameters = [
                  { jobId: $stateParams.jobId },
                  _.isEmpty($stateParams.searchFilter) ?
                    codicilFilter : $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.jobCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            },

            /* @ngInject */
            otherCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'job') {
                const searchParameters = [
                  { assetId: $stateParams.assetId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                    $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.otherCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            }
          }
        }
      }
    })
    .state('crediting.defects.add', {
      parent: 'crediting.defects',
      url: '/defect/add',
      params: {
        payload: null
      },
      views: {
        '@main': {
          templateUrl: addDefectTemplateUrl,
          controller: AddDefectController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            assetModel: ($q, assetModelDecisionService, asset) =>
              $q.when(assetModelDecisionService.getRootItem(asset.id))
                .then((rootItem) => $q.all({
                  rootItem,
                  rootItems: assetModelDecisionService.getItems(asset.id,
                    _.map(rootItem.items, 'id'))
                }))
                .then((ri) =>
                  _.set(new Item(ri.rootItem), 'items', ri.rootItems)),

            /* @ngInject */
            defect: ($stateParams) => {
              if ($stateParams.defect && !$stateParams.defect.id) {
                return $stateParams.defect;
              }
            },

            /* @ngInject */
            refDefectCategories: (referenceDataService) =>
              referenceDataService.get(['defect.defectCategories'], true),

            /* @ngInject */
            refDefectDetails: (referenceDataService) =>
              referenceDataService.get(['defect.defectDetails'], true)
          }
        }
      }
    })
    .state('crediting.defects.view', {
      parent: 'crediting.defects',
      url: '/defect/{codicilId:int}/view?context',
      resolve: {

        /* @ngInject */
        defect: (defectDecisionService, $stateParams) =>
          defectDecisionService.get({
            jobId: $stateParams.jobId,
            assetId: $stateParams.assetId,
            defectId: $stateParams.codicilId,
            context: $stateParams.context
          }),

        /* @ngInject */
        refDefectDetails: (referenceDataService) =>
          referenceDataService.get(['defect.defectDetails']),

        /* @ngInject */
        cocs: ($stateParams, defectDecisionService, defect) =>
          defectDecisionService.getCocs({
            defectId: $stateParams.codicilId,
            jobId: $stateParams.jobId,
            assetId: $stateParams.assetId,
            context: $stateParams.context
          }).then((cocs) => _.forEach(cocs, (coc) => coc.defect = defect)),

        /* @ngInject */
        repairs: ($stateParams, repairDecisionService) =>
          repairDecisionService.get('defect', {
            codicilId: $stateParams.codicilId,
            jobId: $stateParams.jobId,
            assetId: $stateParams.assetId,
            context: $stateParams.context
          })
      },
      views: {
        '@main': {
          templateUrl: viewDefectTemplateUrl,
          controller: ViewDefectController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.defects.view.addRepair', {
      parent: 'crediting.defects.view',
      url: '/defect/{codicilId:int}/add-repair',
      resolve: {

        /* @ngInject */
        parent: (defect) => defect,

        /* @ngInject */
        siblings: ($stateParams, repairDecisionService) =>
          repairDecisionService.get('defect', {
            codicilId: $stateParams.codicilId,
            jobId: $stateParams.jobId,
            assetId: $stateParams.assetId,
            context: $stateParams.context
          }),

        /* @ngInject */
        refServiceCatalogue: (referenceDataService) =>
          referenceDataService.get(['asset.codicilCategories'])
      },

      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddRepairController,
          controllerAs: 'vm'
        }
      }
    })

    .state('crediting.defects.add.addCoc', {
      parent: 'crediting.defects.add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: addCocController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.defects.add.editCoc', {
      parent: 'crediting.defects.add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: addCocController,
          controllerAs: 'vm'
        }
      }
    })

    .state('crediting.defects.add.addAI', {
      parent: 'crediting.defects.add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: addAIController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.defects.add.editAI', {
      parent: 'crediting.defects.add',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: addAIController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.defects.edit', {
      parent: 'crediting.defects',
      url: '/defect/{codicilId:int}/edit',
      views: {
        '@main': {
          templateUrl: editDefectTemplateUrl,
          controller: EditDefectController,
          controllerAs: 'vm'
        }
      }
    });
}
