import * as _ from 'lodash';

import jobCodicilListTemplateUrl from '../job-codicil-list.html';
import JobCodicilListController from '../job-codicil-list.controller';

import AddActionableItemController from
  'app/codicil/actionable-item/add/add-actionable-item.controller';

import ViewActionableItemController from
  'app/codicil/actionable-item/view/view-actionable-item.controller.js';

import EditActionableItemController from
  'app/codicil/actionable-item/edit/edit-actionable-item.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';


/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.actionableItems.list', {
      parent: 'crediting.actionableItems',
      url: '/actionable-item/list',
      views: {
        '@main': {
          templateUrl: jobCodicilListTemplateUrl,
          controller: JobCodicilListController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            codicilFilter: (
              actionableItemCategory,
              actionableItemStatus,
              itemType
            ) => ({
              statusList: _.values(_.pick(
                  actionableItemStatus.toObject(),
                  ['open', 'inactive', 'changeRecommended'])),
              confidentialityList: [1, 2, 3],
              typeList: [
                {
                  slug: 'actionable-item',
                  categories: actionableItemCategory.toArray()
                }
              ]
            }),

            /* @ngInject */
            jobCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'other') {
                const searchParameters = [
                  { jobId: $stateParams.jobId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                    $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.jobCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            },

            /* @ngInject */
            otherCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'job') {
                const searchParameters = [
                  { assetId: $stateParams.assetId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                    $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.otherCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            }
          }
        }
      }
    })
    .state('crediting.actionableItems.add', {
      parent: 'crediting.actionableItems',
      url: '/actionable-item/add',
      params: {
        context: 'job'
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddActionableItemController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.actionableItems.view', {
      parent: 'crediting.actionableItems',
      url: '/actionable-item/{codicilId:int}/view?context',
      params: {
        forCurrentJob: null
      },
      resolve: {

        /* @ngInject */
        actionableItem: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getActionableItem(
            _.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']))
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewActionableItemController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.actionableItems.edit', {
      parent: 'crediting.actionableItems',
      url: '/actionable-item/{codicilId:int}/edit?context',
      resolve: {

        /* @ngInject */
        actionableItem: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getActionableItem(
            _.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']))
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: EditActionableItemController,
          controllerAs: 'vm'
        }
      }
    });
}
