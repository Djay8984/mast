import * as _ from 'lodash';

import HeaderController from '../../../header/asset-header.controller';
import headerTemplateUrl from '../../../header/asset-header.html';

/* @ngInject */
export default function config($stateProvider) {
  const calcAttachmentPath = ({ assetId, jobId, codicilId }, type) =>
    assetId ? `asset/${assetId}/${type}/${codicilId}` :
      `job/${jobId}/wip-${type}/${codicilId}`;

  $stateProvider
    .state('crediting', {
      abstract: true,
      parent: 'asset.jobs.view',
      params: {
        context: null,
        searchFilter: {},
        defectId: null,
        backTarget: null,
        defect: null
      },
      resolve: {

        /* @ngInject */
        codicilStatuses: (referenceDataService) =>
          referenceDataService.get(['service.codicilStatuses']),

        /* @ngInject */
        categories: (referenceDataService) =>
          referenceDataService.get(['asset.codicilCategories']),

        /* @ngInject */
        confidentialityTypes: (referenceDataService) =>
          referenceDataService.get(['attachment.confidentialityTypes']),

        /* @ngInject */
        jobAttachmentPath: ($stateParams) => `job/${$stateParams.jobId}`,

        /* @ngInject */
        jobAttachmentCount: ($stateParams, attachmentDecisionService) =>
          $stateParams.jobId ? attachmentDecisionService.getAttachmentCount(
            `job/${$stateParams.jobId}`) : null
      },
      views: {
        'header@main': {
          templateUrl: headerTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            user: (node) => node.getUser(),

            /* @ngInject */
            asset: (job, assetDecisionService) =>
              assetDecisionService.get(job.asset.id),

            /* @ngInject */
            assetAttachmentCount: (job, attachmentDecisionService) =>
              attachmentDecisionService.getAttachmentCount(
                `asset/${job.asset.id}`),

            /* @ngInject */
            assetAttachmentPath: (job) => `asset/${job.asset.id}`
          }
        }
      }
    })
    .state('crediting.actionableItems', {
      parent: 'crediting',
      abstract: true,
      resolve: {
        listContents: () => 'Actionable Items',
        itemType: () => 'actionable-item',

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService, itemType) =>
          $stateParams.codicilId ? attachmentDecisionService.getAttachmentCount(
            calcAttachmentPath(_.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']), itemType)) : null,

        /* @ngInject */
        attachmentPath: ($stateParams, itemType) => calcAttachmentPath(
          _.pick($stateParams, [`${$stateParams.context}Id`, 'codicilId']),
          itemType)
      }
    })
    .state('crediting.assetNotes', {
      parent: 'crediting',
      abstract: true,
      resolve: {
        listContents: () => 'Asset Notes',
        itemType: () => 'asset-note',

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService, itemType) =>
          $stateParams.codicilId ? attachmentDecisionService.getAttachmentCount(
            calcAttachmentPath(_.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']), itemType)) : null,

        /* @ngInject */
        attachmentPath: ($stateParams, itemType) => calcAttachmentPath(
          _.pick($stateParams, [`${$stateParams.context}Id`, 'codicilId']),
          itemType)
      }
    })
    .state('crediting.cocs', {
      parent: 'crediting',
      abstract: true,
      resolve: {
        listContents: () => 'Conditions of Class',
        itemType: () => 'coc',

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService, itemType) =>
          $stateParams.codicilId ? attachmentDecisionService.getAttachmentCount(
            calcAttachmentPath(_.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']), itemType)) : null,

        /* @ngInject */
        attachmentPath: ($stateParams, itemType) => calcAttachmentPath(
          _.pick($stateParams, [`${$stateParams.context}Id`, 'codicilId']),
          itemType)
      }
    })
    .state('crediting.defects', {
      parent: 'crediting',
      abstract: true,
      resolve: {
        listContents: () => 'Defects',
        itemType: () => 'defect',

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService, itemType) =>
          $stateParams.codicilId ? attachmentDecisionService.getAttachmentCount(
            calcAttachmentPath(_.pick($stateParams,
              [`${$stateParams.context}Id`, 'codicilId']), itemType)) : null
      }
    })
    .state('crediting.repairs', {
      parent: 'crediting',
      abstract: true,
      url: '/{codicilId:int}{defectId:int}',
      params: {
        codicilId: null,
        defectId: null
      },
      resolve: {
        itemType: () => 'repair'
      }
    });
}
