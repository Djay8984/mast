import * as _ from 'lodash';

import jobCodicilListTemplateUrl from '../job-codicil-list.html';
import JobCodicilListController from '../job-codicil-list.controller';

import AddRepairController from
  'app/codicil/repair/add/add-repair.controller';

import AddCoCController from 'app/codicil/coc/add/add-coc.controller';
import EditCoCController from 'app/codicil/coc/edit/edit-coc.controller';
import ViewCoCController from 'app/codicil/coc/view/view-coc.controller';

import InputTemplateUrl from 'app/codicil/input.html';
import ViewTemplateUrl from 'app/codicil/view.html';


/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('crediting.cocs.list', {
      parent: 'crediting.cocs',
      url: '/coc/list',
      backTitle: 'Back to job cocs',
      views: {
        '@main': {
          templateUrl: jobCodicilListTemplateUrl,
          controller: JobCodicilListController,
          controllerAs: 'vm',
          resolve: {

            /* @ngInject */
            codicilFilter: (
              cocCategory,
              cocStatus,
              itemType
            ) => ({
              statusList: [cocStatus.get('open')],
              confidentialityList: [1, 2, 3],
              typeList: [
                { slug: 'coc', categories: cocCategory.toArray() }
              ]
            }),

            /* @ngInject */
            jobCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'other') {
                const searchParameters = [
                  { jobId: $stateParams.jobId },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                  $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.jobCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            },

            /* @ngInject */
            otherCodicils: (
              $stateParams, codicilDecisionService,
              codicilFilter, itemType
            ) => {
              if ($stateParams.searchFilter.viewType !== 'job') {
                const searchParameters = [
                  { assetId: $stateParams.assetId,
                    context: 'asset'
                  },
                  _.isEmpty($stateParams.searchFilter) ? codicilFilter :
                    $stateParams.searchFilter,
                  itemType,
                  true
                ];
                $stateParams.otherCodicilsForPrint = [
                  codicilDecisionService.query,
                  searchParameters
                ];
                return codicilDecisionService
                  .query(...searchParameters).then((res) => {
                    res.printData = () =>
                      codicilDecisionService
                        .query(...searchParameters, { size: null });
                    return res;
                  });
              }
            }
          }
        }
      }
    })
    .state('crediting.cocs.add', {
      parent: 'crediting.cocs',
      url: '/coc/add',
      params: {
        context: 'job'
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddCoCController,
          controllerAs: 'vm',
          resolve: {
            defect: () => null
          }
        }
      }
    })
    .state('crediting.cocs.view.addRepair', {
      parent: 'crediting.cocs.view',
      url: '/coc/{codicilId:int}/add-repair',
      resolve: {

        /* @ngInject */
        parent: (coc) => coc,

        /* @ngInject */
        siblings: () => [],

        /* @ngInject */
        refServiceCatalogue: (referenceDataService) =>
          referenceDataService.get(['asset.codicilCategories'])
      },
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: AddRepairController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.cocs.view', {
      parent: 'crediting.cocs',
      url: '/coc/{codicilId:int}/view?context',
      resolve: {

        /* @ngInject */
        coc: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getCoC(
            _.pick(
              $stateParams,
              [`${$stateParams.context}Id`, 'codicilId', 'context']
            )
          )
      },
      views: {
        '@main': {
          templateUrl: ViewTemplateUrl,
          controller: ViewCoCController,
          controllerAs: 'vm'
        }
      }
    })
    .state('crediting.cocs.edit', {
      parent: 'crediting.cocs',
      url: '/coc/{codicilId:int}/edit?context',
      views: {
        '@main': {
          templateUrl: InputTemplateUrl,
          controller: EditCoCController,
          controllerAs: 'vm'
        }
      },
      resolve: {

        /* @ngInject */
        coc: (codicilDecisionService, $stateParams) =>
          codicilDecisionService.getCoC(
            _.pick(
              $stateParams,
              [`${$stateParams.context}Id`, 'codicilId', 'context']
            )
          )
      }
    });
}
