import Base from 'app/base.class';
import * as angular from 'angular';
import * as _ from 'lodash';

import Task from 'app/models/task.model';

import './partials/service-details.html';
import './partials/class-recommendation.html';
import './partials/job-scope.html';
import './partials/tasks-credited.html';
import './partials/surveys-credited.html';
import './partials/action-cocs.html';
import './partials/action-actionable-items.html';
import './partials/action-asset-notes.html';
import './partials/notes.html';
import './partials/narrative.html';

import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default class ViewReportController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    MastModalFactory,
    moment,
    assetDecisionService,
    creditingDecisionService,
    jobDecisionService,
    printService,
    reportDecisionService,
    actionableItemStatus,
    assetNoteStatus,
    cocStatus,
    creditStatus,
    dateFormat,
    jobStatus,
    reportNoteMaxChars,
    reportType,
    surveyCreditStatuses,
    job,
    referenceData,
    report,
    services,
    tasks,
    user,
    wipActionableItems,
    wipAssetNotes,
    wipCoCs
  ) {
    super(arguments);

    // If the $stateParams.report exists, this is a generated DAR/FAR;
    if (this._$stateParams.report) {
      this._report = this._$stateParams.report;
    } else {
      // Filters for codicils and services; the generated report has this done
      // in the service already;
      this.filterCodicils(this._report.content);

      // This takes care of the tasks until it's available on the BE;
      _.set(this._report.content, 'tasks', this._tasks);
    }

    this.reportBackTarget();
    this.setUpData();

    if (!_.isEmpty(this.tasks) && !_.isEmpty(this.services)) {
      this.prepareServicesAndTasks();
      this.prepareTasks(this.services);
    }

    this.reportTypes = this._reportType.toObject();
    this.cocStatus = this._cocStatus.toObject();
  }

  async setUpData() {
    /** @type {Object} */
    this._asset = await this._assetDecisionService.get(this.job.asset.id);

    /** @type {Array} */
    this.validFARSubmissionStates = _.values(_.pick(this._jobStatus.toObject(),
      ['underSurvey', 'underReporting']));

    // This is here for ordering by the code; calling the function from the
    // ng-repeat's orderBy doesn't sort it correctly;
    _.forEach(this.services, (service) =>
      _.set(service, 'code', this.serviceCode(service)));

    if (this.reportTypeId !== this._reportType.get('dar')) {
      _.set(this.report, 'classRecommendation', this.classRecommendation);
    }
  }

  /**
   * @summary prepareServicesAndTasks()
   * @description This takes the tasks and if there are any, adds them to the
   *   services so we can display them in the report;
   */
  prepareServicesAndTasks() {
    _.forEach(this._tasks, (task) => {
      const survey = _.find(this._services, { id: task.survey.id });
      const related = _.get(survey, 'relatedTasks', []);
      related.push(task);
      _.set(survey, 'relatedTasks', related);
    });
  }

  async prepareTasks(services) {
    const job = await this._jobDecisionService.get(this._job.id);
    const crediting = await this._creditingDecisionService.get(this._job.id);
    const serviceTasks = this._creditingDecisionService
      .getServiceTypeTaskCompletions(job, crediting);

    _.forEach(serviceTasks, (task) => {
      const service = _.find(services, { id: _.first(task.surveys).id });
      _.set(service, 'tasksCompletion', task.tasksCompletion);
    });

    // ensure that we only return task models
    return _.filter(_.flatMap(serviceTasks, 'tasks'), (task) =>
      task instanceof Task);
  }

  /**
   * @summary serviceCatalogue()
   * @return {Object} serviceCatalogues
   */
  get serviceCatalogue() {
    return _.get(this._referenceData, 'service.serviceCatalogues');
  }

  /**
   * @summary serviceTitle(service)
   * @param {Object} service The service we're querying
   * @return {String} name
   */
  serviceTitle(service) {
    return service.name;
  }

  /**
   * @summary serviceId(service)
   * @param {Object} service The service we're querying
   * @return {String} id
   */
  serviceID(service) {
    return service.id;
  }

  /**
   * @summary serviceCode(service)
   * @param {Object} service The service we want the service code for
   * @return {String} code The service code from the service id
   */
  serviceCode(service) {
    return _.get(_.find(this.serviceCatalogue,
      { 'id': service.serviceCatalogue.id }), 'code');
  }

  /**
   * @summary serviceCodeFromId(id)
   * @param {Number} id The id of the service we're looking for
   * @return {String} code The code of the service
   */
  serviceCodeFromId(id) {
    return _.get(_.find(this.services, { id: _.parseInt(id) }), 'code', '');
  }

  get serviceTasks() {
    return _.filter(this._services, 'relatedTasks');
  }

  /**
   * @summary getActionableItemActionTaken(codicil)
   * @param {Object} codicil The codicil we're querying
   * @return {Boolean} true or false
   */
  getActionableItemActionTaken(codicil) {
    const closed = _.values(_.pick(this._actionableItemStatus.toObject(),
      ['deleted', 'cancelled', 'closedForCurrentJob']));
    return _.includes(closed, codicil.status.id) ? 'Closed' : 'Raised';
  }

  /**
   * @summary getAssetNoteActionTaken(codicil)
   * @param {Object} codicil The codicil we're querying
   * @return {Boolean} true or false
   */
  getAssetNoteActionTaken(codicil) {
    const closed = _.values(_.pick(this._assetNoteStatus.toObject(),
      ['deleted', 'cancelled']));
    return _.includes(closed, codicil.status.id) ? 'Closed' : 'Raised';
  }

  /**
   * @summary firstVisitDate()
   * @description Either hand back the job's true firstVisitDate, or if this is
   *   the very first generated DAR (and thus no firstVisitDate) give back the
   *   issueDate of this DAR
   * @return {String} date
   */
  get firstVisitDate() {
    return this._moment(this._job.firstVisitDate ?
      this._job.firstVisitDate : this._report.issueDate)
      .format(this._dateFormat);
  }

  get report() {
    return this._report;
  }

  get services() {
    return this._services;
  }

  get servicesNarratives() {
    if (!this._servicesNarratives) {
      this._servicesNarratives =
        _.filter(this._services, (service) => !_.isNil(service.narrative));
    }
    return this._servicesNarratives;
  }

  get tasks() {
    return this._report.content.tasks;
  }

  get groupedTasks() {
    if (!this._groupedTasks) {
      this._groupedTasks = _.groupBy(angular.copy(this.tasks), 'survey.id');
    }
    return this._groupedTasks;
  }

  /**
   * @summary reportNotSubmitted()
   * @return {Boolean} true or false
   */
  get reportNotSubmitted() {
    return !_.isNil(this._$stateParams.report);
  }

  /**
   * The back target depending on the report
   */
  reportBackTarget() {
    _.set(this._$stateParams, 'backTarget',
      this.reportNotSubmitted ? 'crediting.manage' : 'asset.jobs.view');
  }

  /**
   * @return {String} reportType
   */
  get reportType() {
    return _.upperCase(this.report.reportType.name);
  }

  /**
   * @return {Number} reportTypeId
   */
  get reportTypeId() {
    return this.report.reportType.id;
  }

  get reportServiceTitle() {
    return this.reportTypeId === this._reportType.get('fsr') ?
      'Surveys confirmed in the job scope' : 'Surveys credited in this report';
  }

  get reportSurveyCode() {
    return this.reportTypeId === this._reportType.get('fsr');
  }

  get job() {
    return this._job;
  }

  get asset() {
    return this._asset;
  }

  /**
   * @summary filterCodicils()
   * @param {Object} filterable Codicils that are filterable
   * codicils Newly filtered codicils by user/date
   */
  filterCodicils(filterable) {
    if (this.reportTypeId === this._reportType.get('dar')) {
      _.forEach(filterable, (codicils, index) => {
        const filtered = _.filter(codicils, (codicil) =>
          _.isEqual(codicil.updatedBy, this._user.name) &&
          _.isEqual(
            this._moment(codicil.actionTakenDate).format(this._dateFormat),
            this._moment(this._report.issueDate).format(this._dateFormat)));
        _.set(this._report.content, index, filtered);
      });
    }
  }

  /**
   * @summary wipCoCs()
   * @return {Array} cocs
   */
  get wipCoCs() {
    return !_.isNil(this._report.content.wipCocs) ?
      this._report.content.wipCocs : this._wipCoCs;
  }

  /**
   * @summary wipActionableItems()
   * @return {Array} actionableItems An array of items
   */
  get wipActionableItems() {
    return !_.isNil(this._report.content.wipActionableItems) ?
      this._report.content.wipActionableItems :
      this._wipActionableItems;
  }

  /**
   * @summary wipAssetNotes()
   * @return {Array} assetNotes An array of items
   */
  get wipAssetNotes() {
    return !_.isNil(this._report.content.wipAssetNotes) ?
      this._report.content.wipAssetNotes : this._wipAssetNotes;
  }

  /**
   * @summary surveyCreditStatuses()
   * @return {Object} statuses The statuses of crediting
   */
  get surveyCreditStatuses() {
    return _.get(this._surveyCreditStatuses, 'service.serviceCreditStatuses');
  }

  /**
   * @summary surveyStatusCode(service)
   * @param {Object} service The service we're querying
   * @return {String} status The single letter 'status'
   */
  surveyStatusCode(service) {
    return _.get(_.find(this.surveyCreditStatuses,
      { id: service.surveyStatus.id }), 'code');
  }

  /**
   * @summary classRecommendations()
   * @description Gets the class recommendations from the reference data
   * @return {String} recommendations
   */
  get classRecommendations() {
    return _.get(this._referenceData, 'job.classRecommendations');
  }

  /**
   * @summary classRecommendation()
   * @description Gets the narrative text for the class recommendations
   * @since 15/07/2016
   * @todo This is a temporary R2 version ONLY: this will different for R3
   */
  get classRecommendation() {
    return _.get(_.find(this.classRecommendations, { id: 9 }), 'narrativeText');
  }

  get showCancelButton() {
    return this.reportNotSubmitted;
  }

  /**
   * @summary activeSubmitButton()
   * @description The conditions for whether the submit button is useable or
   *   not - all except DAR are impossible when OFFLINE is true;
   * @return {Boolean} true or false
   */
  get activeSubmitButton() {
    // If we're offline, no report can be submitted;
    if (this._$rootScope.OFFLINE) {
      return false;
    }

    // For FAR;
    if (this.reportTypeId === this._reportType.get('far')) {
      // Is the job in the correct status, and online?
      if (this.isReportSubmittable('far')) {
        return true;
      }
    }

    // For DSR;
    if (this.reportTypeId === this._reportType.get('dsr')) {
      // Is the job in the 'under reporting' status, and online?
      if (this._jobStatus.get('underReporting') === this._job.jobStatus.id) {
        return true;
      }
    }

    // For DAR;
    if (this.reportTypeId === this._reportType.get('dar')) {
      return true;
    }

    return false;
  }

  /**
   * @summary isReportSubmittable(type)
   * @param {String} type i.e., 'dar' or 'dsr'
   * @return {Boolean} true or false
   */
  isReportSubmittable(type) {
    if (type === 'far' &&
      _.includes(this.validFARSubmissionStates, this._job.jobStatus.id)) {
      return true;
    }
  }

  async submissionError(code, type) {
    let errorMessage = null;
    let errorTitle = null;
    if (type === 'far') {
      switch (code) {
        case 1:
          errorMessage = 'The FAR is unable to be submitted because there ' +
            'exist open defects which have no open CoC. ' +
            'Please update the defect(s) to resolve this issue.';
          break;
        case 2:
          errorMessage = 'The FAR is unable to be submitted because there ' +
            'exist open CoC which have no associated open Defects. Please ' +
            'update the CoC(s) to resolve this issue.';
          break;
        case 3:
          errorMessage = 'The FAR is unable to be submitted because there ' +
            'exist open CoCs in the job scope which have no action taken ' +
            'for them. Please update the CoC(s) or job scope to resolve ' +
            'this issue.';
          break;
        case 4:
        default:
          errorMessage = 'The FAR is unable to be submitted because there ' +
            'exists uncredited services with narrative defined for them. ' +
            'Please update the service(s) or job scope to resolve this issue.';
          break;
      }
    }

    if (type === 'dsr') {
      errorTitle = 'Cannot submit DSR';
      switch (code) {
        case 1:
          errorMessage = 'Defect details are not populated.';
          break;
        case 2:
        default:
          errorMessage = 'Conditional task or data value required for the ' +
            'tasks is not populated.';
          break;
      }
    }

    await new this._MastModalFactory({
      templateUrl: confirmationTemplateUrl,
      scope: {
        title: !_.isNull(errorTitle) ? errorTitle : null,
        message: errorMessage,
        actions: [
          { name: 'Close', result: true }
        ]
      },
      class: 'dialog'
    }).activate();
  }

  /**
   * @summary submit()
   * @description Submit the DAR to create/save it on the system
   * @return {boolean}
   */
  async submit() {
    // For FAR's we need to check a variety of things;
    if (this.reportTypeId === this._reportType.get('far')) {
      const error =
        await this._reportDecisionService.checkBeforeSubmission(this._job.id,
          this.report);

      // Errors 1 - 4 stop the submission;
      if (_.inRange(error, 1, 5)) {
        return this.submissionError(error, 'far');
      }
    }

    // For DSR's;
    if (this.reportTypeId === this._reportType.get('dsr')) {
      const error =
        await this._reportDecisionService
          .checkDSRBeforeSubmission(this._job.id, this._report);

      if (_.inRange(error, 1, 3)) {
        return this.submissionError(error, 'dsr');
      }
    }

    // DSR is the only report that has (two) checks before we create the report
    // at this point; the first is the warning, the second is the confirmation
    // of actual submission (or declining to do so);
    if (this.report.reportType.name === 'dsr') {
      const confirmation = await new this._MastModalFactory({
        templateUrl: confirmationTemplateUrl,
        scope: {
          title: 'Submit DSR',
          message: `You have selected to generate a DSR.
This action cannot be undone.`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Submit DSR', result: true }
          ]
        },
        class: 'dialog'
      }).activate();

      if (confirmation) {
        const savedReport = await this._reportDecisionService
          .createReport(this.job.id, this.report.reportType.name,
            this.report.issueDate, 'true', 'true', null);
        const version = savedReport.reportVersion;

        await new this._MastModalFactory({
          templateUrl: confirmationTemplateUrl,
          scope: {
            message: `V${version} of DSR was submitted successfully.`,
            actions: [
              { name: 'OK', result: true }
            ]
          },
          class: 'dialog'
        }).activate();

        const params =
          {
            report: null,
            reportId: savedReport.id,
            jobId: this._job.id,
            backTarget: 'asset.jobs.view'
          };

        this._$state.go(this._$state.current, params, { reload: true });
        return true;
      }
      return false;
    }

    // Perform the createReport() for DAR/FAR - but this time we're telling it
    // to save, so make a real API call to the report service;
    const savedReport = await this._reportDecisionService
      .createReport(this.job.id, this._report.reportType.name,
        this._report.issueDate, 'true', 'true', this._report);
    const version = savedReport.reportVersion;

    // Confirmation modal when submitting a DAR/FAR;
    if (await new this._MastModalFactory({
      templateUrl: confirmationTemplateUrl,
      scope: {
        message: `V${version} of ${_.toUpper(this._report.reportType.name)} ` +
          `was submitted successfully.`,
        actions: [
          { name: 'OK', result: true }
        ]
      },
      class: 'dialog'
    }).activate()) {
      const params =
        {
          report: null,
          reportId: savedReport.id,
          jobId: this._job.id,
          backTarget: 'asset.jobs.view'
        };
      this._job.jobStatus.id =
        this.report.reportType.name === 'dar' ?
           this._jobStatus.get('underSurvey') :
             this._jobStatus.get('underReporting');
      this._job = await this._jobDecisionService.updateJob(this.job.model);

      // Reload the page, inject the newly saved report id (dar/far);
      this._$state.go(this._$state.current, params, { reload: true });
    }
  }

  get reportNoteMaxChars() {
    return this._reportNoteMaxChars;
  }

  print() {
    return this._printService.print({ dynamicPage: true }, 'reporting.view');
  }
}
