import * as _ from 'lodash';

import HeaderController from '../../../header/asset-header.controller';
import darTemplateUrl from '../../../header/dar-header.html';

import ViewReportController from './view/view-report.controller';
import viewReportTemplateUrl from './view/view-report.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider
    .state('reporting', {
      abstract: true,
      parent: 'asset.jobs.view'
    })
    .state('reporting.view', {
      parent: 'reporting',
      url: '/{reportId:int}/view',
      params: {
        backTarget: null,
        codicilId: null,
        defectId: null,
        jobId: null,
        report: null,
        reportId: null,
        reportType: null,
        reportTypeId: null
      },
      views: {
        'header@main': {
          templateUrl: darTemplateUrl,
          controller: HeaderController,
          controllerAs: 'vm'
        },
        '@main': {
          templateUrl: viewReportTemplateUrl,
          controller: ViewReportController,
          controllerAs: 'vm'
        }
      },
      resolve: {

        /* @ngInject */
        attachmentCount: ($stateParams, attachmentDecisionService) =>
          attachmentDecisionService
            .getAttachmentCount(`job/${$stateParams.jobId}`),

        /* @ngInject */
        attachmentPath: ($stateParams) => `job/${$stateParams.jobId}`,

        /* @ngInject */
        referenceData: (referenceDataService) =>
          referenceDataService.get([
            'job.classRecommendations',
            'service.serviceCatalogues'
          ]),

        /* @ngInject */
        report: (reportDecisionService, $stateParams) =>
          reportDecisionService.get($stateParams.jobId, $stateParams.reportId),

        /* @ngInject */
        services: (dateFormat, jobDecisionService, report, moment, $stateParams,
          user, reportType) => {
          if ($stateParams.report) {
            return $stateParams.report.content.surveys;
          }
          return jobDecisionService
            .getSurveysForJob($stateParams.jobId).then((surveys) => {
              if (report.reportType.id === reportType.get('dar')) {
                return _.filter(surveys, (survey) =>
                  _.isEqual(survey.updatedBy, user.fullName) &&
                  _.isEqual(moment(survey.actionTakenDate).format(dateFormat),
                  moment(report.issueDate).format(dateFormat)));
              }
              return surveys;
            });
        },

        /* @ngInject */
        surveys: (jobDecisionService, $stateParams) =>
          jobDecisionService.getSurveysForJob($stateParams.jobId),

        /* @ngInject */
        surveyCreditStatuses: (referenceDataService) =>
          referenceDataService.get(['service.serviceCreditStatuses']),

        /* @ngInject */
        tasks: (reportDecisionService, services) =>
          !_.isEmpty(services) ?
            reportDecisionService.generateTasks(services) : [],

        /* @ngInject */
        wipAssetNotes: ($stateParams, codicilDecisionService) => {
          if ($stateParams.report) {
            return _.get($stateParams, 'report.content.wipAssetNotes');
          }
          return codicilDecisionService
            .getAssetNote({ jobId: $stateParams.jobId });
        },

        /* @ngInject */
        wipCoCs: ($stateParams, codicilDecisionService) => {
          if ($stateParams.report) {
            return _.get($stateParams, 'report.content.wipCocs');
          }
          return codicilDecisionService
            .getCoC({ jobId: $stateParams.jobId });
        },

        /* @ngInject */
        wipActionableItems: ($stateParams, codicilDecisionService) => {
          if ($stateParams.report) {
            return _.get($stateParams, 'report.content.wipActionableItems');
          }
          return codicilDecisionService
            .getActionableItem({ jobId: $stateParams.jobId });
        }
      }
    });
}
