import * as _ from 'lodash';

import { construct } from 'app/utils/model';
import Job from 'app/models/job.model';

export controller from './view-job.controller';
export templateUrl from './view-job.html';

export const resolve = {

  /* @ngInject */
  page: (pageService) => {
    pageService.data = { title: 'Job asset' };
  },

  /* @ngInject */
  reports: (reportDecisionService, $stateParams) =>
    reportDecisionService.getReportsForJob($stateParams.jobId),

  /* @ngInject */
  job: ($stateParams, jobDecisionService) =>
    jobDecisionService
      .get($stateParams.jobId)
      .then(construct(Job)),

  /* @ngInject */
  crediting: ($stateParams, creditingDecisionService) =>
    creditingDecisionService
      .get($stateParams.jobId),

  /* @ngInject */
  serviceTypeTaskCompletions:
    (creditingDecisionService, job, crediting) =>
      creditingDecisionService
        .getServiceTypeTaskCompletions(job, crediting),

  /* @ngInject */
  surveys: (jobDecisionService, $stateParams) =>
    jobDecisionService.getSurveysForJob($stateParams.jobId),

  /* @ngInject */
  wipAssetNotes: ($stateParams, codicilDecisionService) =>
    codicilDecisionService
      .getAssetNote({ jobId: $stateParams.jobId }, false)
      .then((assetNotes) =>
        _.map(assetNotes, (note) =>
          _.set(note, 'route', 'wip-asset-note'))),

  /* @ngInject */
  wipCoCs: ($stateParams, codicilDecisionService) =>
    codicilDecisionService.getCoC({ jobId: $stateParams.jobId }, false)
      .then((cocs) =>
        _.map(cocs, (coc) =>
          _.set(coc, 'route', 'wip-coc'))),

  /* @ngInject */
  wipActionableItems: ($stateParams, codicilDecisionService) =>
    codicilDecisionService.getActionableItem({
      jobId: $stateParams.jobId }, false)
      .then((actionableItems) =>
        _.map(actionableItems, (item) =>
          _.set(item, 'route', 'wip-actionable-item')))
};
