export controller from './list-jobs.controller';
export templateUrl from './list-jobs.html';

export const resolve = {

  /* @ngInject */
  employee: (employeeDecisionService) =>
    employeeDecisionService.getCurrentUser(),

  /* @ngInject */
  allAssetJobs: ($stateParams, jobDecisionService, asset) =>
    jobDecisionService.query({
      ...$stateParams.jobFilter,
      assetId: [asset.id]
    }, { size: 8, sort: 'createdOn' })
};
