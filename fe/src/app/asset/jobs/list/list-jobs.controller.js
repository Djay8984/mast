import Base from 'app/base.class';
import * as _ from 'lodash';

export default class JobsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    allAssetJobs,
    asset,
    assetLifecycleStatus
  ) {
    super(arguments);
    this.jobFilter = _.isEmpty(this._$stateParams.jobFilter) ?
      this.jobFilterDefault :
      this._$stateParams.jobFilter;

    this.filterActive = !_.isEqual(this.jobFilter, this.jobFilterDefault);

    this.filterOpen = false;

    // when API ready to pass in jobs for other users from config.routes,
    // can remove this
    // can't filter here as we lose pageable properties on collection
    this._otherJobs = allAssetJobs;
  }

  isAssetDecommissioned() {
    return _.get(this, '_asset.assetLifecycleStatus.id') ===
      this._assetLifecycleStatus.get('decommissioned');
  }

  updateList(jobFilter) {
    this._$state.go(this._$state.current.name, {
      jobFilter
    }, { reload: true });
  }

  get otherJobs() {
    return this._otherJobs;
  }

  get jobs() {
    return this._jobs;
  }
}
