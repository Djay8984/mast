import * as _ from 'lodash';

import Base from 'app/base.class';

import removeSelectedDialog from './edit-scope-dialog.html';
import confirmDialog from './confirm-scope-dialog.html';

import 'app/asset/jobs/edit-scope/partials/services.html';

export default class EditJobsScopeController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    moment,
    codicilDecisionService,
    defectDecisionService,
    jobDecisionService,
    MastModalFactory,
    navigationService,
    referenceDataService,
    surveyDecisionService,
    dueStatus,
    employeeRole,
    job,
    jobStatus,
    productType,
    referenceData,
    surveys,
    user,
    wipAssetNotes,
    wipCoCs,
    wipActionableItems
  ) {
    super(arguments);

    this.setUpData();

    /** @type {Array} */
    this._selectedScopeItems = [];
  }

  async setUpData() {
    const referenceDataMap = {
      productCatalogue: 'product.productCatalogues',
      productGroup: 'product.productGroups',
      serviceCatalogue: 'service.serviceCatalogues'
    };

    const data = await this._referenceDataService
      .get(_.values(referenceDataMap));

    _.forEach(referenceDataMap, (value, key) =>
      _.set(this, key, _.get(data, value)));

    const modelProperties = {
      enumerable: true,
      configurable: true,
      value: 0,
      writable: true
    };

    _.forEach(
      _.flattenDeep(
        [this._wipCoCs, this._wipAssetNotes, this._wipActionableItems]
      ), (codicil) => {
      Reflect.defineProperty(codicil, 'checkBoxModel', modelProperties);

      // Attach the defect category name to the codicil; also add the
      // repairCount if it's > 1;
      if (_.isObject(codicil.defect) && !this._$rootScope.OFFLINE) {
        return this._defectDecisionService
          .get({
            jobId: codicil.job.id,
            defectId: codicil.defect.id,
            context: 'job'
          })
          .then((defect) => {
            _.set(codicil.defect, 'category', defect.defectCategory.name);
            if (defect.repairCount > 0) {
              _.set(codicil.defect, 'repairCount', defect.repairCount);
            }
            return defect;
          });
      }
    });

    this.updateActiveJob();
    this.assignProductFamily();
  }

  assignProductFamily() {
    // loops through the maze of ref data that links the product family to
    // the survey;
    _.forEach(this._surveys, (survey) => {
      const surveyProductCat = _.filter(this.serviceCatalogues, (cat) =>
        _.isEqual(cat.id, _.get(survey, 'serviceCatalogue.id')));

      const productFamilyId = _.filter(this.productGroup, (group) =>
        _.isEqual(group.id,
          _.first(surveyProductCat).productCatalogue.productGroup.id));

      // Sets the product family;
      _.set(survey, 'productFamily', _.first(productFamilyId).productType.id);
    });
    return this._surveys;
  }

  async updateActiveJob() {
    // If the user has added any services or codicils, regardless of whether
    // they confirm or not, then the status must be reset to false, so it can
    // then be displayed as 'unconfirmed';
    if (this._$stateParams.addition) {
      this._job.scopeConfirmed = false;
      this._job.scopeConfirmedBy = null;
      this._job.scopeConfirmedDate = null;

      this._job = await this._jobDecisionService.updateJob(
        _.omit(this._job, '_meta'));
    }
  }

  /**
   * @summary jobStatusAllowsButtons()
   * @description The buttons are only visible when job is in the status between
   *   'under survey' and 'awaiting TR'
   * @return {Boolean} true or false
   */
  get jobStatusAllowsButtons() {
    return _.includes(this._jobStatusesAllowButtons, this._job.jobStatus.id);
  }

  /**
   * @summary currentUserIsLeadSurveyor()
   * @return {Boolean} true or false
   */
  get currentUserIsLeadSurveyor() {
    return _.some(this._job.employees,
      {
        lrEmployee: { id: this._user.id },
        employeeRole: { id: this._employeeRole.get('leadSurveyor') }
      }
    );
  }

  /**
   * Moves the user onwards
   * @summary confirmJobScope()
   */
  async confirmJobScope() {
    Reflect.deleteProperty(this._job, '_meta');
    this._job.scopeConfirmed = true;
    this._job.scopeConfirmedDate = this._moment().toISOString();
    this._job.scopeConfirmedBy = { id: this._user.id };

    const modalConfirm = new this._MastModalFactory({
      templateUrl: confirmDialog,
      scope: {
        confirmText: `You have selected to confirm the job scope.`
      },
      class: 'dialog'
    });

    if (await modalConfirm.activate()) {
      if (this._job.jobStatus.id === this._jobStatus.get('sdoAssigned')) {
        this._job.jobStatus.id = this._jobStatus.get('resourceAssigned');
      }
      this._job = await this._jobDecisionService.updateJob(this._job);
      await this._$state.go('asset.jobs.view', this._$stateParams,
        { reload: true });
    }
  }

  get codicilStatuses() {
    return _.get(this._referenceData, 'service.codicilStatuses');
  }

  /**
   * @summary getDisplayStatus(statusId)
   * @param {Number} statusId The status id
   * @return {String} name Name of the status' id
   */
  getDisplayStatus(statusId) {
    return _.find(this.codicilStatuses, { id: statusId }).name;
  }

  /**
   * @summary deleteSelected()
   */
  async deleteSelected() {
    if (!this.editableSDO()) {
      return;
    }

    const items = _.groupBy(this._selectedScopeItems, 'route');
    const itemIds = _.map(_.flatMap(items), 'id');
    const routeMap =
      {
        'survey': {
          service: '_surveyDecisionService',
          func: 'removeSurvey',
          property: 'surveyId'
        },
        'wip-coc': {
          service: '_codicilDecisionService',
          func: 'removeCoC',
          property: 'codicilId'
        },
        'wip-asset-note': {
          service: '_codicilDecisionService',
          func: 'removeAssetNote',
          property: 'codicilId'
        },
        'wip-actionable-item': {
          service: '_codicilDecisionService',
          func: 'removeActionableItem',
          property: 'codicilId'
        }
      };

    // Modal confirm delete;
    const modalConfirmDelete = new this._MastModalFactory({
      templateUrl: removeSelectedDialog,
      scope: {
        customText: `You have selected to remove
          ${_.size(this._selectedScopeItems)} items from the Job scope`
      },
      class: 'dialog'
    });

    if (await modalConfirmDelete.activate()) {
      for (const key of _.keys(items)) {
        for (const { id, _id } of items[key]) {
          await this[routeMap[key].service][routeMap[key].func]({
            jobId: this._$stateParams.jobId,
            [routeMap[key].property]: id || _id
          });
        }
      }

      // Because deleting a service/codicil changes the scope, the
      // scopeConfirmed flag is reset to false (defined but not confirmed)
      this._job.scopeConfirmed = false;

      // Even though the codicils are deleted, we need to manually delete
      // the surveys from the job;
      this._job.surveys =
        _.reject(this._job.surveys, (survey) =>
          _.includes(itemIds, survey.id));

      // Save (update) the job so the scopeConfirmed is saved, just in case
      // the user navigates elsewhere before clicking 'confirm job scope';
      this._job = await this._jobDecisionService.updateJob(
        _.omit(this._job, '_meta'));
      await this._$state.go(this._$state.current, this._$stateParams,
        { reload: true });
    }
  }

  /**
   * @summary singleDefectLink(codicil)
   * @description The individual codicils have possibly have defect links
   * @param {Object} codicil The incoming codicil type
   * @since 27/06/2016
   */
  async singleDefectLink(codicil) {
    await this._$state.go('crediting.defects.view', {
      ...this._$stateParams,
      codicilId: _.parseInt(codicil.defect.id),
      context: 'job',
      jobId: _.parseInt(codicil.job.id)
    });
  }

  /**
   * @summary singleCodicilLink(codicil)
   * @description The individual codicils have links on them linking to the
   *   view page of each - these need to be constructed
   * @param {Object} codicil The incoming codicil type
   * @since 22/06/2016
   */
  singleCodicilLink(codicil) {
    if (codicil.parent) {
      const codicilName = _.camelCase(codicil.route.slice(4));
      this._$state.go(`crediting.${codicilName}s.view`, {
        ...this._$stateParams,
        backTarget: 'asset.jobs.edit-scope',
        jobId: _.parseInt(this._$stateParams.jobId),
        assetId: _.parseInt(this._$stateParams.assetId),
        codicilId: _.parseInt(codicil.id),
        context: 'job'
      });
    }
  }

  /**
   * @summary ctaVisible()
   * @description This checks the job status and disables the CTA if the job
   *   status is in certain conditions, regardless of membership of the team;
   * @returns {Boolean} true or false
   */
  ctaVisible() {
    // After the job has had either DSR or FSR generated, the status of the job
    // is in these, and so disables the CTA buttons;
    const disabledJobStatuses = _.values(_.pick(this._jobStatus.toObject(),
      [
        'awaitingTechnicalReviewerAssignment',
        'underTechnicalReview',
        'awaitingEndorserAssignment',
        'underEndorsement',
        'closed',
        'cancelled',
        'aborted'
      ]));

    return !_.includes(disabledJobStatuses, this._job.jobStatus.id);
  }

  /**
   * @summary setSelectedValueForAll(selectedState)
   * @param {Number} selectedState A 1 or a 0
   */
  setSelectedValueForAll(selectedState) {
    _.forEach(this._wipAssetNotes, (node) => {
      this.addSelectedScopeItems(node);
      node.checkBoxModel = selectedState;
    });
    _.forEach(this._wipCoCs, (node) => {
      this.addSelectedScopeItems(node);
      node.checkBoxModel = selectedState;
    });
    _.forEach(this._wipActionableItems, (node) => {
      this.addSelectedScopeItems(node);
      node.checkBoxModel = selectedState;
    });
    _.forEach(this._surveys, (node) => {
      if (!node.autoGenerated) {
        this.addSelectedScopeItems(node);
        node.checkBoxModel = selectedState;
      }
    });
  }

  /**
   * Method to add objects to the _selectedItems called from the checkbox;
   * @param {Object} obj The (node) object from the template;
   */
  addSelectedScopeItems(obj) {
    if (obj.checkBoxModel === 0) {
      this._selectedScopeItems.push(obj);
    } else {
      this._selectedScopeItems =
        _.reject(this._selectedScopeItems, { route: obj.route, id: obj.id });
    }
  }

  /**
   * @summary selectedScopeItemsSize()
   */
  get selectedScopeItemsSize() {
    return _.size(this._selectedScopeItems);
  }

  /**
   * @summary serviceCatalogues()
   * @return {Array} serviceCatalogues A helper function to get the array of
   *   objects from the resolved reference data
   */
  get serviceCatalogues() {
    return _.get(this._referenceData, 'service.serviceCatalogues');
  }

  /**
   * @summary serviceStatusWarning()
   * @param {Object} service The service we're questioning
   * @return {String} warning Returns a warning message to be used both as a
   *   flag to display the warning, and as the warning message itself;
   */
  serviceStatusWarning(service) {
    if (!_.isNil(service.scheduledService)) {
      if (_.get(service, 'scheduledService.dueStatus.id') ===
        this._dueStatus.get('overdue')) {
        return 'Overdue';
      }
      if (_.get(service, 'scheduledService.dueStatus.id') ===
        this._dueStatus.get('due')) {
        return 'Due';
      }
    }
  }

  /**
   * @summary job()
   * @return {Object} job The job in question
   */
  get job() {
    return this._job;
  }

  get productTypes() {
    return this._productType;
  }

  /**
   * @summary classificationWipSurveys()
   * @return {Object} classification wipSurveys The work-in-progress surveys
   */
  get classificationWipSurveys() {
    return _.filter(this._surveys, (survey) =>
      _.isEqual(survey.productFamily,
        this._productType.get('classification')) &&
          _.isNil(survey.autoGenerated));
  }

  /**
   * @summary statutoryWipSurveys()
   * @return {Object} statutory wipSurveys The work-in-progress surveys
   */
  get statutoryWipSurveys() {
    return _.filter(this._surveys, (survey) =>
      _.isEqual(survey.productFamily,
        this._productType.get('statutory')) &&
          _.isNil(survey.autoGenerated));
  }

  /**
   * @summary mmsWipSurveys()
   * @return {Object} mms wipSurveys The work-in-progress surveys
   */
  get mmsWipSurveys() {
    return _.filter(this._surveys, (survey) =>
      _.isEqual(survey.productFamily,
        this._productType.get('mms')) &&
          _.isNil(survey.autoGenerated));
  }

  /**
   * @summary jobGeneratedSurveys()
   * @return {Object} surveys Filters the surveys looking for autoGenerated
   *   surveys with parents
   */
  get jobGeneratedSurveys() {
    return _.filter(this._surveys, 'autoGenerated');
  }

  /**
   * @summary getSurveyCode()
   * @param {Number} id The survey's service catalogue id
   * @return {String} code The 'code' from the survey's service catalogue
   */
  getSurveyCode(id) {
    return _.get(_.find(this.serviceCatalogues, { id }), 'code', '');
  }

  /**
   * @summary wipAssetNotes()
   * @returns {Object} assetNotes List of all the asset notes for the job
   */
  get wipAssetNotes() {
    return _.filter(this._wipAssetNotes, 'parent');
  }

  /**
   * @summary wipCoCs()
   * @description Returns the resolved list of CoCs, minus those CoCs that were
   *   raised in this job
   * @returns {Object} Cocs List of all the cocs for the job
   * @since 12.08.16
   */
  get wipCoCs() {
    return _.filter(this._wipCoCs, 'parent');
  }

  /**
   * @summary wipActionableItems()
   * @returns {Object} actionableItems List of all the actionable items
   */
  get wipActionableItems() {
    return _.filter(this._wipActionableItems, 'parent');
  }

  /**
   * @summary editableSDO()
   * @returns {Boolean} true or false
   */
  editableSDO() {
    return this._job.jobStatus.id <= this._jobStatus.get('underSurvey');
  }

  /**
   * @summary validForm()
   * @returns {Boolean} boolean True or false
   */
  validForm() {
    return true;
  }

  loadAddScopePage() {
    this._$state.go('asset.jobs.add-scope',
      { jobId: this._job.id },
      { reload: true });
  }

  get isScopeEmpty() {
    return !_.size(this.wipAssetNotes) &&
      !_.size(this.wipActionableItems) &&
      !_.size(this.wipCoCs) &&
      !_.size(this.classificationWipSurveys) &&
      !_.size(this.statutoryWipSurveys) &&
      !_.size(this.mmsWipSurveys) &&
      !_.size(this.jobGeneratedSurveys);
  }
}
