import * as _ from 'lodash';

export controller from './edit-scope-jobs.controller';
export templateUrl from './edit-scope-jobs.html';
export const resolve = {

  /* @ngInject */
  codicilStatuses: (referenceDataService) =>
    referenceDataService.get(['service.codicilStatuses']),

  /* @ngInject */
  job: (jobDecisionService, $stateParams) =>
    jobDecisionService.get($stateParams.jobId),

  /* @ngInject */
  referenceData: (referenceDataService) =>
    referenceDataService.get([
      'employee.employee',
      'service.codicilStatuses',
      'service.serviceCatalogues'
    ]),

  /* @ngInject */
  surveys: (jobDecisionService, $stateParams) =>
    jobDecisionService.getSurveysForJob($stateParams.jobId),

  /* @ngInject */
  wipAssetNotes: ($stateParams, codicilDecisionService) =>
    codicilDecisionService
      .getAssetNote({ jobId: $stateParams.jobId })
      .then((assetNotes) => {
        _.forEach(assetNotes, (note) =>
          _.set(note, 'route', 'wip-asset-note'));
        return assetNotes;
      }),

  /* @ngInject */
  wipCoCs: ($stateParams, codicilDecisionService) =>
    codicilDecisionService.getCoC({ jobId: $stateParams.jobId })
      .then((cocs) => {
        _.forEach(cocs, (coc) =>
          _.set(coc, 'route', 'wip-coc'));
        return cocs;
      }),

  /* @ngInject */
  wipActionableItems: ($stateParams, codicilDecisionService) =>
    codicilDecisionService.getActionableItem({
      jobId: $stateParams.jobId })
      .then((actionableItems) => {
        _.forEach(actionableItems, (item) =>
          _.set(item, 'route', 'wip-actionable-item'));
        return actionableItems;
      })
};
