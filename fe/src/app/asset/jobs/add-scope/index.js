import * as _ from 'lodash';
import * as angular from 'angular';

export default angular
  .module('app.asset.jobs.add-scope', [])
  .name;

export controller from './add-scope-jobs.controller';
export templateUrl from './add-scope-jobs.html';
export const resolve = {

  /* @ngInject */
  dataForServiceList:
  ($q, assetOnlineService, jobDecisionService, referenceDataService,
    $stateParams) => $q.all({
      productModel: referenceDataService.get(['product.productModel'])
      .then((data) => _.get(data, 'product.productModel')),

      products:
        assetOnlineService.getProducts($stateParams.assetId),

      services:
        assetOnlineService.getServicesForAsset($stateParams.assetId),

      wipSurveys:
        jobDecisionService.getSurveysForJob($stateParams.jobId)
    }),

  /* @ngInject */
  actionableItems: (codicilDecisionService, $stateParams,
    actionableItemStatus) =>
    codicilDecisionService.getActionableItem({
      assetId: $stateParams.assetId, context: 'asset' })
      .then((actionableItems) =>
        _.filter(actionableItems,
          { status: { id: actionableItemStatus.get('open') } }))
      .then((actionableItems) =>
        _.forEach(actionableItems, (actionableItem) =>
          _.set(actionableItem, 'route', 'actionable-item'))),

  /* @ngInject */
  assetNotes: (codicilDecisionService, $stateParams,
    assetNoteStatus) =>
    codicilDecisionService.getAssetNote({
      assetId: $stateParams.assetId, context: 'asset' })
      .then((assetNotes) =>
        _.filter(assetNotes,
          { status: { id: assetNoteStatus.get('open') } }))
      .then((assetNotes) =>
        _.forEach(assetNotes, (assetNote) =>
          _.set(assetNote, 'route', 'asset-note'))),

  /* @ngInject */
  cocs: (codicilDecisionService, $stateParams, cocStatus) =>
    codicilDecisionService.getCoC({
      assetId: $stateParams.assetId,
      context: 'asset'
    })
      .then((cocs) =>
        _.filter(cocs, { status: { id: cocStatus.get('open') } }))
      .then((cocs) =>
        _.forEach(cocs, (coc) => _.set(coc, 'route', 'coc'))),

  /* @ngInject */
  job: (jobDecisionService, $stateParams) =>
    jobDecisionService.get($stateParams.jobId),

  /* @ngInject */
  referenceData: (referenceDataService) =>
    referenceDataService.get([
      'product.productCatalogues',
      'product.productGroups',
      'product.productTypes',
      'service.serviceCatalogues'
    ]),

  /* @ngInject */
  user: (node) => node.getUser(),

  /* @ngInject */
  wipActionableItems: (codicilDecisionService, $stateParams) =>
    codicilDecisionService.getActionableItem({
      jobId: $stateParams.jobId, context: 'job' })
      .then((actionableItems) =>
        _.forEach(actionableItems, (actionableItem) =>
          _.set(actionableItem, 'route', 'wip-actionable-item'))),

  /* @ngInject */
  wipAssetNotes: (codicilDecisionService, $stateParams) =>
    codicilDecisionService
      .getAssetNote({ jobId: $stateParams.jobId, context: 'job' })
      .then((actionableItems) =>
        _.forEach(actionableItems, (actionableItem) =>
          _.set(actionableItem, 'route', 'wip-asset-note'))),

  /* @ngInject */
  wipCoCs: (codicilDecisionService, $stateParams) =>
    codicilDecisionService.getCoC({
      jobId: $stateParams.jobId,
      context: 'job'
    })
    .then((cocs) =>
      _.forEach(cocs, (coc) => _.set(coc, 'route', 'wip-coc')))
};
