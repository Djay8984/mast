import Base from 'app/base.class';

export default class ServiceNameDialogController extends Base {
  /* @ngInject */
  constructor(
    services,
    allowedSpecialCharactersRegex
  ) {
    super(arguments);
  }

  get allowedSpecialCharactersRegex() {
    return this._allowedSpecialCharactersRegex;
  }

  get services() {
    return this._services;
  }
}
