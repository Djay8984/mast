import Base from 'app/base.class';
import * as _ from 'lodash';
import * as angular from 'angular';

import ServiceNameChangeDialogController from
  './change-service-name-dialog.controller';
import serviceNameChangeDialogTemplate from
  './change-service-name-dialog.html';

import 'app/asset/jobs/add-scope/partials/asset-notes.html';
import 'app/asset/jobs/add-scope/partials/actionable-items.html';
import 'app/asset/jobs/add-scope/partials/cocs.html';
import 'app/asset/jobs/add-scope/partials/services.html';

export default class AddScopeJobsController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $scope,
    $state,
    $stateParams,
    actionableItems,
    asset,
    assetNotes,
    cocs,
    codicilDecisionService,
    dueStatus,
    dataForServiceList,
    defectOnlineService,
    job,
    jobDecisionService,
    jobStatus,
    MastModalFactory,
    moment,
    navigationService,
    referenceData,
    wipActionableItems,
    wipAssetNotes,
    wipCoCs,
    serviceStatus,
    serviceCreditStatus,
    serviceTypeStatus,
    surveyorRole,
    user
  ) {
    super(arguments);

    this.data = this.groupData();
    this.wipData = this.groupWipData();

    /** @type {Number} */
    this._selectedStateCount = 0;

    /** @type {Array} */
    this._selectedScopeItems = [];
    this._serviceFamilies = [];
    this._scheduledServices = [];
    this._productServices = [];
    this._selectedServices = [];

    // Setting the initial model, so it enters on the overdue button;
    this.model = {};
    this.model.services = 'overdue';

    // Define the service statuses we want to display;
    this._serviceStatuses =
      _.values(_.pick(this._serviceStatus.toObject(),
        ['planned', 'partHeld', 'notStarted']));

    // The job statuses where all users can add services;
    this._jobStatuses =
      _.values(_.pick(this._jobStatus.toObject(),
        ['sdoAssigned', 'resourceAssigned']));

    // Create our two sets of services, schedules and product model;
    this.setUpScheduledServices();
    this.setUpModelServices();

    // Manipulate the services into groups;
    this.setUpServices();

    this.setUpData();
  }

  /**
   * @summary getServiceCatalogueItem(id)
   * @param {Number} id The id of the service catalogue
   * @return {Object} service The service item
   */
  getServiceCatalogueItem(id) {
    return _.find(_.get(this._referenceData,
      'service.serviceCatalogues'), { id });
  }

  /**
   * @summary getServiceCatalogueFamily(id)
   * @param {Number} id The id of the service catalogue
   * @return {Object} service The service item
   */
  getServiceCatalogueFamily(id) {
    return _.find(_.get(this._referenceData,
      'product.productTypes'), { id });
  }

  /**
   * @summary getServiceCatalogueProduct(id)
   * @param {Number} id The id of the service catalogue
   * @return {Object} service The service item
   */
  getServiceCatalogueProduct(id) {
    return _.find(_.get(this._referenceData,
      'product.productGroups'), { id });
  }

  /**
   * @summary getServiceCatalogueFamilyFromName()
   * @param {String} name The name of the product
   * @return {Number} int The id of the product family
   */
  getServiceCatalogueFamilyFromName(name) {
    return _.find(_.get(this._referenceData,
      'product.productCatalogues'), { name }).productType.id;
  }

  /**
   * @summary emptyService(filterType)
   * @param {String} filterType schedule or overdue
   * @description Check is filtered services is empty
   * @return {Boolean}
   * @since 11/08/2016
   */
  emptyService(filterType) {
    if (filterType === 'schedule') {
      return _.isEmpty(this._scheduledServices) &&
        this.model.services === filterType;
    }

    if (filterType === 'overdue') {
      return _.isEmpty(this.getDueOverdueServices()) &&
        this.model.services === filterType;
    }

    return false;
  }

  /**
   * @summary switchService(service)
   * @param {String} service The service we're switching to
   * @description Three buttons on the template allow switching between the
   *   two types of services, this facilitates the change;
   * @since 15/06/2016
   */
  switchService(service) {
    switch (service) {
      case 'all': {
        const temp = this.groupServices(
          _.union(this._scheduledServices, this._productServices));
        this._selectedServices =
          _.omit(_.merge(temp, this._selectedServices), ['MMS']);
        break;
      }
      case 'overdue': {
        this._selectedServices = this.getDueOverdueServices();
        this.expandServices(this._selectedServices);
        break;
      }
      case 'schedule':
      default: {
        this._selectedServices = _.omit(this._groupScheduledServices, ['MMS']);
        this.expandServices(this._selectedServices);
        break;
      }
    }
  }

  /**
   * @summary reOrderServices(services)
   * @param {Object} services that need to be reordered
   * @description Reorder the services as to be displayed in UI.
   *   Not all set of services have all three types of service
   * @return {Object} reordered services
   * @since 14/07/2016
   */
  reOrderServices({ Classification, Statutory, MMS }) {
    return _.pickBy({ Classification, Statutory, MMS });
  }

  /**
   * @summary scopeServices()
   * @description When the user first comes into the page, or when they click
   *   one of the buttons along the top, the services displayed need to be
   *   ready, this methods selects which set of services are available;
   * @return {Object} services The 'selected' services
   * @since 15/06/2016
   */
  scopeServices() {
    return _.isEmpty(this._selectedServices) &&
      this.model.services !== 'overdue' ?
        this.reOrderServices(this._groupScheduledServices) :
        this.reOrderServices(this._selectedServices);
  }

  /**
   * @summary setUpBasicService()
   * @description Creates a default service that the services will use as their
   *   scaffold ready for saving;
   * @param {Object} service The serivce we're acting upon
   * @param {Boolean} scheduled True or false
   * @return {Object} service The basic service structure
   * @since 15/06/2016
   */
  setUpBasicService(service, scheduled) {
    // Create the default assignments in the empty object;
    const defaultService = _.defaults({},
      {
        approved: false,
        autoGenerated: null,
        creditedBy: null,
        checkBoxModel: 0,
        dateOfCrediting: null,
        expanded: false,
        flags: [],
        jobScopeConfirmed: false,
        job: {
          id: this._job.id
        },
        scheduleDatesUpdated: false,
        surveyStatus: {
          id: this._serviceCreditStatus.get('notStarted')
        },
        ruleset: null
      }
    );

    if (scheduled) {
      const catalogue =
        this.getServiceCatalogueItem(service.serviceCatalogueId);
      const productType = catalogue.productCatalogue.productType.id;
      const productGroup = catalogue.productCatalogue.productGroup.id;

      defaultService.scheduledService = service;
      defaultService.serviceCatalogue = { 'id': service.serviceCatalogueId };
      defaultService.name = catalogue.name;
      defaultService.code = catalogue.code;
      defaultService.flags = catalogue.flags;
      defaultService.serviceFamily =
        this.getServiceCatalogueFamily(productType).name;
      defaultService.serviceProduct =
        this.getServiceCatalogueProduct(productGroup).name;
      defaultService.ruleset = catalogue.serviceRuleset;
    } else {
      defaultService.description = service.description;
      defaultService.code = service.code;
      defaultService.flags = this.getServiceCatalogueItem(service.id).flags;
      defaultService.serviceProduct = service.group;
      defaultService.serviceFamily =
        this.getServiceCatalogueFamily(
          this.getServiceCatalogueFamilyFromName(service.group)).name;
      defaultService.scheduledService = null;
      defaultService.serviceCatalogue = { 'id': service.id };
      defaultService.name = service.name;
      defaultService.ruleset = service.serviceRuleset;
    }

    return defaultService;
  }

  /**
   * @summary setUpModelServices()
   * @description Create the services that come from the product model
   * @since 15/06/2016
   */
  setUpModelServices() {
    const surveys = this._dataForServiceList.productModel;
    let modelServices = [];
    let newModel = [];

    // We need to rip the productModel apart and create a new 'service' for use
    // in this controller, and ready for saving;
    modelServices = this.createModelServices(surveys);

    newModel = this.createNewModelServices(modelServices);

    // This is the true list of 'services' we're constructing;
    _.forEach(newModel, (newservice) => {
      this._productServices.push(this.setUpBasicService(newservice, false));
    });

    // Ensure that the product services are unique by removing ALL of those
    // that the scheduled services already appear for;
    this._productServices = _.reject(this._productServices, (item) => {
      if (_.size(_.filter(this._scheduledServices, { name: item.name }))) {
        return item;
      }
    });

    // remove any classification services that do not match the asset's ruleset
    if (this._asset.productRuleSet) {
      this._productServices = _.reject(this._productServices, (service) =>
        service.serviceFamily === 'Classification' &&
        service.ruleset.id !== this._asset.productRuleSet.id);
    } else {
      this._productServices =
        _.reject(this._productServices, { serviceFamily: 'Classification' });
    }

    // remove any statutory services that are not applicable to the asset's flag
    if (this._asset.flagState) {
      this._productServices = _.reject(this._productServices, (service) =>
        service.serviceFamily === 'Statutory' &&
        !_.includes(_.map(service.flags, 'id'), this._asset.flagState.id));
    }

    // remove any duplicates produced by the above processing
    this._productServices = _.uniqBy(this._productServices,
      'serviceCatalogue.id');

    this._productServices = _.sortBy(this._productServices, 'serviceProduct');
  }

  /**
   * @summary createModelServices(surveys)
   * @param {Array} surveys The surveys we're converting
   * @return {Array} modelServices The new services Array
   */
  createModelServices(surveys) {
    let modelServices = [];
    _.forEach(surveys, (survey, i) =>
      modelServices =
        _.union(modelServices,
          _.flatMap(survey.productGroups, 'productCatalogues')));
    return modelServices;
  }

  /**
   * @summary createNewModelServices(modelServices)
   * @param {Array} modelServices The services we're changing
   * @return {Array} newModel The changed services
   */
  createNewModelServices(modelServices) {
    let newModel = [];
    _.forEach(modelServices, (item) => {
      _.forEach(item.serviceCatalogues, (svc) =>
        _.set(svc, 'group', item.name));
      newModel = newModel.concat(item.serviceCatalogues);
    });
    return newModel;
  }

  /**
   * @summary setUpScheduledServices()
   * @description Obtain all the scheduled services and populate them ready to
   *   display for the job scoping page;
   * @since 14/06/2016
   */
  setUpScheduledServices() {
    const surveys = this._dataForServiceList.services.plain();

    // Create our surveys according to the structure in /job/x/survey API;
    this.createScheduledServices(surveys);

    this._scheduledServices =
      _.sortBy(this._scheduledServices, 'serviceProduct');
  }

  createScheduledServices(surveys) {
    _.forEach(surveys, (survey) => {
      if (_.isObject(survey.serviceStatus) &&
        _.includes(this._serviceStatuses, survey.serviceStatus.id)) {
        this._scheduledServices.push(this.setUpBasicService(survey, true));
      }
    });
  }

  /**
   * @summary expandServices(serviceGroup)
   * @param {Object} serviceGroup The grouped services we want to expand
   */
  expandServices(serviceGroup) {
    _.forEach(serviceGroup, (family) => {
      if (!family.expanded) {
        family.expanded = true;
      }
      _.forEach(family, (product) => {
        if (!product.expanded && !_.isBoolean(product)) {
          product.expanded = true;
        }
      });
    });
  }

  /**
   * @summary setUpServices()
   * @since 15/06/2016
   */
  setUpServices() {
    this._groupProductServices = this.groupServices(this._productServices);
    this._groupScheduledServices = this.groupServices(this._scheduledServices);

    // As the user enters the page, they should be viewing the due/overdue
    // services;
    this._selectedServices = this.getDueOverdueServices();

    // First viewable services have to be expanded by default;
    this.expandServices(this._selectedServices);
  }

  /**
   * @summary getDueOverdueServices()
   * @description get due/overdue services
   * @return {Object} services
   * @since 11/08/2016
   */
  getDueOverdueServices() {
    const dueStatuses = _.reject(this._dueStatus.toObject(),
      (id) => id === this._dueStatus.get('notDue'));

    return _.omit(this.groupServices(
      _.filter(this._scheduledServices, (svc) =>
        _.includes(dueStatuses,
          _.get(svc, 'scheduledService.dueStatus.id', 0)))), ['MMS']);
  }

  /**
   * @summary groupServices(service)
   * @param {Object} service The collection of services to act on
   * @return {Object} services A new 'grouped' service object
   * @since 15/06/2016
   */
  groupServices(service) {
    const first = _.groupBy(service, 'serviceFamily');
    _.forEach(first, (family, items) =>
      first[items] = _.groupBy(family, 'serviceProduct'));
    return first;
  }

  unGroupServices(group) {
    return _.reduce(group, (result, item) => {
      const items = _.filter(_.flatten(_.values(item)), (service) =>
        _.isObject(service) ? service : null);
      result.push(items);
      return result;
    }, []);
  }

  /**
   * @summary singleCodicilLink(codicil)
   * @description The individual codicils have links on them linking to the
   *   view page of each - these need to be constructed
   * @param {Object} codicil The incoming codicil type
   * @since 22/06/2016
   */
  singleCodicilLink(codicil) {
    this._$state.go(`${_.camelCase(codicil.route)}.view`,
      _.merge(this._$stateParams, {
        jobId: _.parseInt(this._$stateParams.jobId),
        assetId: _.parseInt(this._$stateParams.assetId),
        codicilId: _.parseInt(codicil.id)
      }));
  }

  /**
   * @summary singleDefectLink(codicil)
   * @description The individual codicils have possibly have defect links
   * @param {Object} codicil The incoming codicil type
   * @since 24/06/2016
   */
  singleDefectLink(codicil) {
    this._$state.go('defect.view',
      _.merge(this._$stateParams,
        { codicilId: codicil.defect.id }));
  }

  /**
   * @summary groupData()
   * @description Groups up the data in arrays by the route
   * @return {Object[]} data The object of arrays
   */
  groupData() {
    return _.groupBy(
      _.flattenDeep(
        [this._cocs, this._assetNotes, this._actionableItems]
      ),
      'route');
  }

  /**
   * @summary groupWipData()
   * @description Groups up the wip data by the route
   * @return {Object[]} data The object of wip arrays
   */
  groupWipData() {
    return _.groupBy(
      _.flattenDeep(
        [this._wipCoCs, this._wipAssetNotes, this._wipActionableItems]
      ),
      'route');
  }

  /**
   * @summary setSelectedStateForAllServices(shouldBeSelected)
   * @description Set all the services as 'selected' via the checkBoxModel
   *   property on each, passing in our value;
   * @param {Number} shouldBeSelected Boolean number
   */
  setSelectedStateForAllServices(shouldBeSelected) {
    /* eslint max-nested-callbacks: 0 */
    _.forEach(this._selectedServices, (family) => {
      _.forEach(family, (product) => {
        _.forEach(product, (service) => {
          if (shouldBeSelected !== _.get(service, 'checkBoxModel')) {
            this.addSelectedScopeItems(service);
          }
          _.set(service, 'checkBoxModel', shouldBeSelected);
        });
      });
    });
  }

  /**
   * @summary visibleServices()
   * @return {Array} services
   */
  visibleServices() {
    return _.flattenDeep(this._selectedServices);
  }

  /**
   * @summary setUpData()
   * @description Prepares data for display on the add-scope page
   * @since 24/08/2016
   */
  async setUpData() {
    let data = _.flattenDeep(_.values(this.data));
    const statusSetOne = _.concat(
      this._jobStatuses,
      _.values(
        _.pick(
          this._jobStatus.toObject(),
          ['underSurvey', 'underReporting']
        )
      )
    );

    data = this.addCheckBoxes(data);

    // Examine WIP data for job, see if any exist with this job,
    // indicating that the edit page has been visited and things selected
    //  (now saved as WIP-type).
    this.disableAlreadySelectedItems();

    // Post-service-setup processing; looking at wipSurveys (if any exist) and
    // greying out the versions on this page;
    this.disableAlreadySelectedSurveys();

    // Attach the defect category name to the codicil; also add the repairCount
    // if it's > 1; _values() on this.data returns all the codicils, then
    // _.flattenDeep() gets them altogether;
    this.addDefectAndRepairCount();

    // According to ACs, anyone can add a service if the job status is
    // SDO assigned or Resource assigned;
    _.forEach(this._scheduledServices, (service) => {
      if (!_.includes(statusSetOne, this._job.jobStatus.id)) {
        service.untickable = true;
      }
    });
  }

  /**
   * @summary addDefectAndRepairCount()
   * @description If it's not offline, adds the defect and repair count to the
   *   individual codicils that are displayed;
   */
  addDefectAndRepairCount() {
    _.forEach(_.flattenDeep(_.values(this.data)), (codicil) => {
      if (_.isObject(codicil.defect) && !this._$rootScope.OFFLINE) {
        this._defectOnlineService
          .get({ assetId: codicil.asset.id,
            defectId: codicil.defect.id,
            context: 'asset' })
          .then((defect) => {
            _.set(codicil.defect, 'category', defect.defectCategory.name);
            if (defect.repairCount > 0) {
              _.set(codicil.defect, 'repairCount', defect.repairCount);
            }
          });
      }
    });
  }

  /**
   * @summary disableAlreadySelectedSurveys()
   * @description This checks the WIP surveys and disables the ones on this
   *   add-scope page if they're already added to the scope;
   */
  disableAlreadySelectedSurveys() {
    if (_.size(this._dataForServiceList.wipSurveys)) {
      // Concatenate all our services, product and scheduled;
      const services = _.concat(this._productServices, this._scheduledServices);
      _.forEach(this._dataForServiceList.wipSurveys, (wipSurvey) => {
        // Look for our wip- version of the service, and if found, then make
        // sure the user cannot select it again;
        const item = _.find(services,
          { serviceCatalogue: { id: wipSurvey.serviceCatalogue.id },
            name: wipSurvey.name });
        if (_.isObject(item)) {
          item.unselectable = true;
          item.untickable = true;
        }
      });
    }
  }

  /**
   * @summary disableAlreadySelectedItems()
   * @description Disables the codicils that have already been added to the
   *   job scope;
   */
  disableAlreadySelectedItems() {
    if (!_.isEmpty(this.wipData)) {
      const addItems = this.data; // Items to 'add';
      const wipItems = this.wipData; // 'wip' versions of items (on edit page)
      // Loop through our items on this side (add-scope);
      _.forEach(addItems, (availableItem, i) => {
        // Now loop the wip- versions of the same items (as if we were coming
        // from the edit-scope page);
        _.forEach(wipItems[`wip-${i}`], (wip) => {
          // If we find a codicil that we have on the edit-scope side, then we
          // need to ensure that the user -cannot- select the non-wip versions
          // on this (add-scope) side;
          const marked =
            _.find(availableItem, { id: _.get(wip, 'parent.id') });
          if (_.isObject(marked)) {
            marked.unselectable = true;
          }
        });
      });
    }
  }

  /**
   * @summary addCheckBoxes(data)
   * @param {Object} data The data we're adding checkboxes to
   * @return {Object} data The newly altered data with checkboxes added
   */
  addCheckBoxes(data) {
    const modelProperties =
      {
        enumerable: true,
        configurable: true,
        value: false,
        writable: true
      };

    for (const i in data) {
      if (data.hasOwnProperty(i)) {
        Reflect.defineProperty(data[i], 'checkBoxModel', modelProperties);
      }
    }

    return data;
  }

  /**
   * @summary requiresWarning()
   * @param {Object} service The service we're questioning
   * @return {String} warning Returns a warning message to be used both as a
   *   flag to display the warning, and as the warning message itself;
   */
  requiresWarning(service) {
    if (_.get(service, 'unselectable')) {
      return 'Added';
    }
    if (!_.isNil(service.scheduledService)) {
      if (_.get(service, 'scheduledService.dueStatus.id') ===
        this._dueStatus.get('overdue')) {
        return 'Overdue';
      }
      if (_.get(service, 'scheduledService.dueStatus.id') ===
        this._dueStatus.get('due')) {
        return 'Due';
      }
    }
  }

  /**
   * @summary job()
   * @description Getter for the job
   * @return {Object} job
   */
  get job() {
    return this._job;
  }

  /**
   * @summary jobAssetNotes()
   * @returns {Object} assetNotes List of all the asset notes for the job
   */
  get jobAssetNotes() {
    return this._assetNotes;
  }

  /**
   * @summary jobActionableItems()
   * @returns {Object} actionableItems List of all the actionable items
   */
  get jobActionableItems() {
    return this._actionableItems;
  }

  /**
   * @summary jobCocs()
   * @returns {Object} Cocs List of all the cocs for the job
   */
  get jobCocs() {
    return this._cocs;
  }

  /**
   * @summary wipAssetNotes()
   * @returns {Object} assetNotes List of all the job wip asset notes
   */
  get wipAssetNotes() {
    return this._wipAssetNotes;
  }

  /**
   * @summary wipActionableItems()
   * @returns {Object} actionableItems List of all the job wip actionable items
   */
  get wipActionableItems() {
    return this._wipActionableItems;
  }

  /**
   * @summary wipCoCs()
   * @returns {Object} Cocs List of all the job wip cocs
   */
  get wipCoCs() {
    return this._wipCoCs;
  }

  /**
   * @summary addSelectedScopeItems(obj)
   * @description Method to add objects to the _selectedScopeItems called from
   *   the checkbox;
   * @param {Object} obj The (node) object from the template;
   */
  addSelectedScopeItems(obj) {
    if (obj.checkBoxModel === 0) {
      this._selectedScopeItems.push(obj);
    } else {
      // Two possibilities here, one is a codicil with an id and a route, the
      // other is a service with neither both can be added;
      //
      // Update - 11/08/2016
      // Added additional condition to check against service catalogue id,
      // otherwise multiple services with same name and code will get rejected
      this._selectedScopeItems =
        _.reject(this._selectedScopeItems, _.get(obj, 'id') ?
          { route: obj.route, id: obj.id } :
          {
            code: obj.code,
            name: obj.name,
            serviceCatalogue: obj.serviceCatalogue
          }
        );
    }
  }

  /**
   * @summary selectedScopeItemsSize()
   * @return {Number} size The size of the selectedScopeItems array
   */
  get selectedScopeItemsSize() {
    return _.size(this._selectedScopeItems);
  }

  get totalScopeItemsSize() {
    return this.selectedScopeItemsSize + _.size(this.selectedServices());
  }

  /**
   * @summary editableService()
   * @returns {Boolean} jobStatus
   */
  editableService() {
    return _.includes(this._jobStatuses, this._job.jobStatus.id);
  }

  /**
   * @summary ctaVisible()
   * @returns {Boolean} boolean True or false
   */
  ctaVisible() {
    // This first checks two statuses where anyone can see the CTA buttons;
    let allowedJobStatuses = _.values(_.pick(this._jobStatus.toObject(),
      ['sdoAssigned', 'resourceAssigned']));

    if (_.includes(allowedJobStatuses, this._job.jobStatus.id)) {
      return true;
    }

    // After the job has had either DSR or FSR generated, the status of the job
    // is in these, and so disables the CTA buttons;
    const disabledJobStatuses = _.values(_.pick(this._jobStatus.toObject(),
      [
        'awaitingTechnicalReviewerAssignment',
        'underTechnicalReview',
        'awaitingEndorserAssignment',
        'underEndorsement'
      ]));

    if (_.includes(disabledJobStatuses, this._job.jobStatus.id)) {
      return false;
    }

    // Now we need to have another set of statuses for the job team only to
    // see the CTA buttons;
    allowedJobStatuses = _.values(_.pick(this._jobStatus.toObject(),
      [
        'underSurvey',
        'underReporting',
        'awaitingTechnicalReviewerAssignment'
      ]));

    const teamRoles = _.values(_.pick(this._surveyorRole.toObject(),
      ['sdoCoordinator', 'leadSurveyor', 'authorisingSurveyor', 'eicManager']));

    // System will only show the CTAs to those who are assigned to the job when
    // the status is between 'under survey' and 'awaiting technical review';
    if (_.includes(allowedJobStatuses, this._job.jobStatus.id)) {
      // It's not good enough that the employee be a member of the team, they
      // have to be one of the assigned roles in order to edit job scope;
      const employee =
        _.find(this._job.employees, { lrEmployee: { id: this._user.id } });
      if (employee && _.includes(teamRoles, employee.employeeRole.id)) {
        return true;
      }
      return false;
    }

    return false;
  }

  /**
   * @summary validForm()
   * @returns {Boolean} boolean True or false
   */
  validForm() {
    return this.selectedScopeItemsSize || !_.isEmpty(this.selectedServices()) &&
      this.editableService();
  }

  /**
   * @summary expand(item)
   * @param {Object} item The item we're expanded
   * @description Expanded items in the hierarchy
   * @return {Object} item The item that was expanded
   */
  expand(item) {
    item.expanded = !item.expanded;
    return item;
  }

  /**
   * @summary serviceNameDialog()
   * @param {Object} controller The controller (this)
   * @param {Object} servicesNeedingNameChange The service in question
   * @return {promise}
   */
  async serviceNameDialog(controller, servicesNeedingNameChange) {
    const modal = new controller._MastModalFactory({
      controller: ServiceNameChangeDialogController,
      controllerAs: 'vm',
      templateUrl: serviceNameChangeDialogTemplate,
      inject: { services: servicesNeedingNameChange }
    });
    return modal.activate();
  }

  /**
   * @summary selectedServices()
   * @return {Object} services The selected services
   */
  selectedServices() {
    return _.union(
      _.filter(_.flattenDeep(this.unGroupServices(this._selectedServices)),
        'checkBoxModel'),
      _.filter(this._selectedScopeItems,
        (service) => _.isNil(service.route)
      )
    );
  }

  /**
   * @summary saveSelectedServices()
   * @description Strip the services of unrequired properties, and create a
   *   list of promises to resolve either one by one (offline) or with $q
   * @return {Object} promises or void depending if you're offline
   */
  async saveSelectedServices() {
    if (this._$rootScope.OFFLINE) {
      for (let service of this.selectedServices()) {
        service = _.omit(service, ['oldName', 'checkBoxModel', 'expanded',
        'code', 'serviceFamily', 'serviceProduct', 'serviceType',
        'description', 'flags', 'ruleset']);

        /* eslint no-loop-func: 0 */
        service = _.omitBy(service, (s) => _.startsWith('$'));
        _.set(service, 'actionTakenDate', this._moment().toISOString());

        /* eslint babel/no-await-in-loop: 0 */
        await this._jobDecisionService.saveNewSurveyForJob(
          this._job.id, angular.copy(service));
      }
      return {};
    }

    if (_.size(this.selectedServices())) {
      const mapOfSaveRequests = _.map(this.selectedServices(), (service) => {
        service = _.omit(service, ['oldName', 'checkBoxModel', 'expanded',
          'code', 'serviceFamily', 'serviceProduct', 'serviceType',
          'description', 'flags', 'ruleset']);
        service = _.omitBy(service, (s) => _.startsWith('$'));
        _.set(service, 'actionTakenDate', this._moment().toISOString());
        return service;
      });

      for (const service of mapOfSaveRequests) {
        await this._jobDecisionService
          .saveNewSurveyForJob(this._job.id, angular.copy(service));
      }
      return {};
    }
  }

  /**
   * @summary getServiceType(service)
   * @param {Object} service The service we're questioning
   * @return {Number} id The service type id
   */
  getServiceType(service) {
    const svc = _.find(_.get(this._referenceData, 'service.serviceCatalogues'),
      { id: service.serviceCatalogue.id });
    return _.get(svc, 'serviceType.id', 0);
  }

  /**
   * @summary shouldContinueWithNewServiceNames()
   * @description When saving if misc services are selected we need to give user
   *   option to change name of service(s). Function provides modal and returns
   *   indication of whether user wants to continue or cancel flow
   * @returns {Boolean} false if user cancels modal, true otherwise
   */
  async shouldContinueWithNewServiceNames() {
    const selectedMiscServices =
      _.filter(this._selectedScopeItems, (svc) =>
        _.isObject(svc.serviceCatalogue) &&
          this.getServiceType(svc) ===
          this._serviceTypeStatus.get('miscellaneous'));

    if (!_.isEmpty(selectedMiscServices)) {
      _.forEach(selectedMiscServices, (service) =>
        _.set(service, 'oldName', service.name));

      if (!(await this.serviceNameDialog(this, selectedMiscServices))) {
        _.forEach(selectedMiscServices, (service) =>
          _.set(service, 'name', service.oldName));
        return false;
      }
    }
    return true;
  }

  /**
   * @summary saveDefect(save)
   * @param {Object} save The codicil we're dealing with
   * @return {Object} save The newly saved codicil
   */
  async saveDefect(save) {
    if (!_.isNil(save.defect)) {
      _.assign(save.defect,
        {
          job: { id: this._job.id },
          parent: { id: save.defect.id },
          id: null
        });

      // Save the new defect as a wip- version;
      const wipDefect = await this._defectOnlineService
        .save({ jobId: this._job.id }, save.defect);

      // And then on the codicil we're saving, remove the defect, then set it
      // with the newly updated id of the wip-defect;
      Reflect.deleteProperty(save, 'defect');
      save.defect = {};
      save.defect.id = wipDefect.id;
    }
    return save;
  }

  /**
   * @summary save()
   */
  async save() {
    if (this.selectedScopeItemsSize) {
      if (!(await this.shouldContinueWithNewServiceNames())) {
        return;
      }

      // Save the services and then group up the remaining items;
      await this.saveSelectedServices();
      const items = _.groupBy(this._selectedScopeItems, 'route');
      const requests = [];
      const routeMap = {
        'coc': 'saveCoC',
        'asset-note': 'saveAssetNote',
        'actionable-item': 'saveActionableItem'
      };

      _.forEach(items, (item, routeName) => {
        if (_.size(item) && !_.isUndefined(routeName)) {
          _.forEach(item, (save) => {
            save = _.omit(save, 'checkBoxModel');

            // Adding a service/codicil immediately puts the scope into the
            // 'defined but not confirmed' status, which sets the confirmed by
            // and on, to null;
            save.jobScopeConfirmed = false;

            // Some codicils don't have templates, or require the
            // 'editableBySurveyor' property;
            if (routeName !== 'asset-note') {
              if (_.isObject(save.template)) {
                save.editableBySurveyor = save.template.editableBySurveyor;
              }
            }

            // Title must exist;
            if (!_.isString(save.title) && !_.isNull(save.title)) {
              save.title = `${_.join(_.map(item, 'title'), ', ')} title`;
            }

            // Id must be null;
            if (_.isNumber(save.id)) {
              save.parent = { id: save.id };
              save.id = null;
              save._id = null;
            }

            // Must be an object;
            if (!_.isObject(save.confidentialityType)) {
              save.confidentialityType = { id: 1, _id: '1' };
            }

            // Date cannot be in the past;
            save.imposedDate = this._moment().toISOString();
            save.dueDate = this._moment().toISOString();

            // Job cannot be null...
            if (!_.isObject(save.job)) {
              save.job = {};
            }

            // Category may not be null...
            if (!_.isObject(save.category)) {
              save.category = {};
            }

            // Strip out category from defect, if it was there;
            if (_.get(save, 'defect.category')) {
              Reflect.deleteProperty(save.defect, 'category');
            }

            // ...and job id must be set;
            _.merge(save.job, {
              id: this._$stateParams.jobId,
              _id: _.toString(this._$stateParams.jobId)
            });

            requests.push({ save, routeName });
          });
        }
      });

      for (const { save, routeName } of requests) {
        await this.saveDefect(save)
          .then((data) => this._codicilDecisionService[routeMap[routeName]](
            { jobId: this._$stateParams.jobId }, data));
      }

      this._$state.go('asset.jobs.edit-scope',
        { jobId: this._$stateParams.jobId, addition: true },
        { reload: true });
    }
  }

  /**
   * @summary cancel()
   * @description Provides a route out of the job scoping back to the jobs view
   */
  cancel() {
    this._$state.go('asset.jobs.view', this._$stateParams);
  }
}
