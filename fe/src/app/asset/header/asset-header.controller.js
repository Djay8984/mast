import * as _ from 'lodash';
import Base from 'app/base.class';

export default class AssetHeaderController extends Base {
  /* @ngInject */
  constructor(
    user,
    asset,
    pageService,
    resolve
  ) {
    super(arguments);

    this.header = this._pageService.header;
  }

  get data() {
    return this._pageService.data;
  }

  get user() {
    return this._user;
  }

  get asset() {
    return _.get(this._resolve, 'assetForJob') || this._asset;
  }
}
