import Base from 'app/base.class';
import * as _ from 'lodash';

export default class AddRelationshipController extends Base {
  /* @ngInject */
  constructor(
    currentCustomers,
    relationshipTypes,
    $modalInstance
  ) {
    super(arguments);

    if (_.isUndefined(this._currentCustomers)) {
      this._currentCustomers = [];
    }
  }

  get relationshipTypes() {
    return _.filter(this._relationshipTypes, (item) =>
        !_.includes(_.map(this._currentCustomers, 'relationship'), item.name));
  }

  validForm() {
    return true;
  }

  confirm() {
    this._$modalInstance.close(this.relationship);
  }
}
