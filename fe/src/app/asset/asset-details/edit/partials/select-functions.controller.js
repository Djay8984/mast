import Base from 'app/base.class';

export default class AddFunctionsController extends Base {
  /* @ngInject */
  constructor(
    functionTypes,
    $modalInstance
  ) {
    super(arguments);
  }

  get functionTypes() {
    return this._functionTypes;
  }

  validForm() {
    return true;
  }

  confirm() {
    this._$modalInstance.close(this.functionSelection);
  }
}
