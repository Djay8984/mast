import * as _ from 'lodash';

import BaseForm from 'app/base-form.class';

import * as angular from 'angular';

import selectAssetTypesDialog from './partials/select-asset-types-dialog';
import SelectAssetTypesController from
  './partials/select-asset-types.controller';

import selectOfficeTypesDialog from './partials/select-office-types-dialog';
import SelectOfficeTypesController from
  './partials/select-office-types.controller';

import buildRuleSetDialog from './partials/build-rule-set-dialog';
import BuildRuleSetController from './partials/build-rule-set.controller';

import selectRelationshipDialog from './partials/select-relationship-dialog';
import SelectRelationshipController from
  './partials/select-relationship.controller';

import selectFunctionsDialog from './partials/select-functions-dialog';
import SelectFunctionsController from './partials/select-functions.controller';

import './partials/asset-details.html';
import './partials/build-details.html';
import './partials/class-maintenance.html';
import './partials/customers.html';
import './partials/flag-details.html';
import './partials/offices.html';
import './partials/rule-set.html';

import Header from '../header';

const ASSET_LEVEL_INDICATOR = 5;
const OFFICE_TYPES = [ 1, 2 ];
const UNIQUE_FUNCTIONS = [ 'CFO', 'Contract Holder' ];

export default class EditAssetDetailsController extends BaseForm {
  /* @ngInject */
  constructor(
    $log,
    $state,
    $stateParams,
    MastModalFactory,
    caseCustomerService,
    caseService,
    dateFormat,
    ihsService,
    pageService,
    imoNumberLength,
    asset,
    ihsAsset,
    referenceData,
    assetOnlineService,
    navigationService,
    typeaheadService,
    moment
  ) {
    super(arguments);

    this._prevVersion = angular.copy(this._asset);

    this._imo = angular.copy(this._asset.ihsAsset);
    this.setUpData();

    // Temporarily convert the hullIndicator to a String for the 'Special survey
    // indicator' field. Convert it back before saving.
    this.asset.hullIndicator = String(this.asset.hullIndicator);

    this.header = new Header(this);

    this.setActiveTab(this._$stateParams.tab || 'assetDetails');

    this.flagTypeahead = this._typeaheadService.query(this.staticFlagList);
    this.portTypeahead = this._typeaheadService.query(this.staticPortList);
    this.officeTypeahead = this._typeaheadService.query(this.staticSDOList);
    this.losingSocietyTypeahead = this._typeaheadService.query(
      this.staticLosingSocietyList);
    this.customerTypeahead = this._typeaheadService.query(
      this.staticCustomerList);
    this.customerTypeahead = this._typeaheadService.query(
      this.staticCustomerList);
  }

  get activeTab() {
    return this.header.activeTab;
  }

  async setUpData() {
    // Remove the cases, as it interferes with saving;
    Reflect.deleteProperty(this._asset, 'cases');

    // Set the customers in the caseCustomerService;
    this._caseCustomerService.setCustomers(this.asset.customers);

    // Available class section
    this._classDepartments = _.get(
      this._referenceData,
      'asset.classDepartments'
    );

    // Create the offices;
    this._officeTypes = _.get(this._referenceData, 'case.officeRoles');
    if (!_.isEmpty(this.asset.offices)) {
      const cfoType = _.find(this._officeTypes, { name: 'CFO' });
      const cfo = {
        office: _.get(_.first(this._asset.offices), 'office', { }),
        officeRole: _.merge(
          this._asset.offices[0].officeRole,
          {
            id: (cfoType) ? cfoType.id : -1
          }
        )
      };
      const grpType = _.find(this._officeTypes, { name: 'GRP' });
      const grp = {
        office: _.get(this, '_asset.offices[1].office', { }),
        officeRole: _.merge(
          this._asset.offices[1].officeRole,
          {
            id: (grpType) ? grpType.id : -1
          }
        )
      };
      this.asset.offices = { 'classCFO': cfo, 'class': grp };
    } else {
      this.asset.offices = { };
    }
    this.imoValidated = true;
  }

  /**
   * @param {Object} item The form object being validated
   * @return {Boolean} true or false Depends on the form item status
   */
  validFormItem(item) {
    try {
      if (item && item.$dirty && item.$invalid && !item.$pristine) {
        return true;
      }
      return false;
    } catch (err) {
      this._$log.error('validFormItem, item:', item, 'Error:', err);
    }
  }

  /**
   * setActiveTab(tab) sets the active tab on the page
   * @param {String} tab The active tab name
   */
  setActiveTab(tab) {
    this._activeTab = tab;
    this.header.activeTab = { [tab]: true };
  }

  /**
   * cancel()
   */
  cancel() {
    // AC 8, cancel shows the last saved header info, so reload the page;
    this._$state.go('^', { }, { reload: true });
  }

  /**
   * save()
   */
  async save() {
    const payload = angular.copy(this.asset);
    if (_.isEmpty(payload.flagState)) {
      payload.flagState = null;
    }
    if (_.isEmpty(payload.registeredPort)) {
      payload.registeredPort = null;
    }

    _.set(payload, 'customers', this.assetCustomers);
    _.set(payload, 'hullIndicator', _.parseInt(this.asset.hullIndicator));

    _.unset(payload, 'checkedOutBy');
    await this._assetOnlineService.update(payload);
    this.cancel();
  }

  dateFormat() {
    return this._dateFormat;
  }

  get imoNumberLength() {
    return this._imoNumberLength;
  }

  /**
   * isIMORegistered(); used for form hide/show
   * @return {Boolean} true or false
   */
  isIMORegistered() {
    return _.isNumber(_.get(this, '_imo.id'));
  }

  /**
   * @description Opens the modal to display the asset types
   */
  async selectAssetTypes() {
    const response = await this._MastModalFactory({
      controller: SelectAssetTypesController,
      controllerAs: 'vm',
      templateUrl: selectAssetTypesDialog,
      inject: {
        selection: this.assetTypes
      },
      class: 'large dialog'
    }).activate();

    this.asset.assetType =
      _.find(response, { 'levelIndication': ASSET_LEVEL_INDICATOR });

    const defaultClassDepartment = _.first(
      _.filter(
        this._classDepartments,
        {
          id: _.get(
            this.asset.assetType, 'defaultClassDepartment.id', null)
        }
      )
    );

    if (defaultClassDepartment) {
      this.asset.classDepartment = defaultClassDepartment;
    }
  }

  /**
   * @rescription Opens the modal to select a rule set
   */
  async buildRuleSet() {
    this.asset.ruleSet = await this._MastModalFactory({
      controller: BuildRuleSetController,
      controllerAs: 'vm',
      inject: {
        ruleSets: this.ruleSets
      },
      templateUrl: buildRuleSetDialog,
      class: 'large dialog'
    }).activate();
  }

  /**
   * @return {Object} Asset Returns IHS object
   */
  get ihsAsset() {
    return !_.isEmpty(this._ihsAsset) ? this._ihsAsset : null;
  }

  /**
   * @return {Array} Asset Returns the asset types array
   */
  get assetTypes() {
    return _.get(this._referenceData, 'asset.assetTypes');
  }

  /**
    * @return {Array} Asset Returns the class section array
    */
  get classDepartments() {
    return this._classDepartments;
  }

  /**
   * @return {Array} Asset Returns the class maintenance status array
   */
  get classMaintenanceStatuses() {
    return _.get(this._referenceData, 'asset.classMaintenanceStatuses');
  }

  /**
   * @return {Array} ruleSets Returns the rule sets
   */
  get ruleSets() {
    return _.get(this._referenceData, 'asset.ruleSets');
  }

  /**
   * @return {Array} officeTypes Returns the office types (roles)
   */
  get officeTypes() {
    return this._officeTypes;
  }

  /**
   * @return {Array} officeTypes Returns the office types (roles)
   */
  get relationshipTypes() {
    return _.get(this._referenceData, 'party.partyRoles');
  }

  /**
   * @return {Object} asset Returns this asset
   */
  get asset() {
    return this._asset;
  }

  /**
   * @description Opens the modal to choose a customer
   */
  async selectRelationship() {
    const response = await this._MastModalFactory({
      controller: SelectRelationshipController,
      controllerAs: 'vm',
      inject: {
        relationshipTypes: this.relationshipTypes,
        currentCustomers: this._caseCustomerService.customerList()
      },
      templateUrl: selectRelationshipDialog,
      class: 'large dialog'
    }).activate();

    this._caseCustomerService.addCustomer({
      customer: { name: null },
      relationship: { name: response.name, id: response.id }
    });
  }

  /**
   * @description Opens the modal to choose office types to add
   */
  async selectOfficeType() {
    if (!this._officeTypes) {
      this._officeTypes = this.officeTypes;
    }

    this._officeTypes =
      _.filter(this._officeTypes,
        (office) => !_.includes(OFFICE_TYPES, office.id));

    const response = await this._MastModalFactory({
      controller: SelectOfficeTypesController,
      controllerAs: 'vm',
      inject: {
        officeTypes: this._officeTypes
      },
      templateUrl: selectOfficeTypesDialog,
      class: 'small dialog'
    }).activate();

    this._extraOffices = this._extraOffices || [];

    this._extraOffices = _.concat(this._extraOffices, response);

    this._officeTypes =
      _.filter(this._officeTypes,
        (office) => !_.includes(_.map(this._extraOffices, 'id'), office.id));
  }

  get extraOffices() {
    return this._extraOffices;
  }

  /**
   * @description Opens the modal to choose customer functions
   * @param {Object} customer The customer that the click comes from
   * @since 18/05/2016
   */
  async selectCustomerFunctions(customer) {
    const response = await this._MastModalFactory({
      controller: SelectFunctionsController,
      controllerAs: 'vm',
      inject: {
        functionTypes: await this._caseCustomerService.functionTypes(customer)
      },
      templateUrl: selectFunctionsDialog,
      class: 'large dialog'
    }).activate();

    // Creating the customerFunction object;
    _.forEach(response, (item) => {
      item.customerFunction =
        _.pick(item, ['_id', 'id', 'name', 'description']);
      Reflect.deleteProperty(item, 'description');
      Reflect.deleteProperty(item, 'name');
      Reflect.deleteProperty(item, 'id');
      Reflect.deleteProperty(item, '_id');
      Reflect.deleteProperty(item, 'deleted');
    });

    this._caseCustomerService.addCustomerFunction(customer, response);

    // Check the CFO functionality;
    this.checkCFOFunctionality(response);
  }

  /**
   * checkCFOFunctionality(item); check functions for 'CFO'
   * @param {Array} items An array of functions
   */
  async checkCFOFunctionality(items) {
    const positions = _.filter(items, (item) =>
      _.includes(UNIQUE_FUNCTIONS, item.customerFunction.name));

    if (_.size(positions)) {
      const selectedOffice =
        _.find(this.staticSDOList, { id: _.first(positions).id });

      this.asset.offices.classCFO.office = selectedOffice.plain();
    }
  }

  setCustomerName(customer, name) {
    this._caseCustomerService.addCustomerName(customer, name);
  }

  /**
   * @description Removes the customer from the customers
   * @param {Object} customer The customer section on the add case form
   */
  removeCustomer(customer) {
    this._caseCustomerService.removeCustomer(customer);
  }

  /**
   * @description Removes this button from the selected functions
   * @param {Object} customer The customer we're dealing with
   * @param {Object} item The element calling this function
   */
  removeCustomerFunction(customer, item) {
    this._caseCustomerService.removeCustomerFunction(customer, item);
  }

  /**
   * assetCustomerFunctions()
   * @param {Object} customer The customer we're dealing with
   * @return {Array} functions The function list of a given customer
   */
  assetCustomerFunctions(customer) {
    return this._caseCustomerService.customerFunctionList(customer);
  }

  /**
   * assetCustomers()
   * @return {Array} customerList An array of customer objects
   */
  get assetCustomers() {
    return this._caseCustomerService.customerList();
  }

  /**
   * @return {Array} classStatuses Returns the resolved class statuses
   */
  get classStatuses() {
    return _.get(this._referenceData, 'asset.classStatuses');
  }

  /**
   * @return {Array} lifecycleStatuses Returns the resolved lifecycle statuses
   */
  get lifecycleStatuses() {
    return _.get(this._referenceData, 'asset.lifecycleStatuses');
  }

  get staticSDOList() {
    return _.get(this._referenceData, 'employee.office');
  }

  get staticLosingSocietyList() {
    return _.get(this._referenceData, 'asset.iacsSocieties');
  }

  get staticCustomerList() {
    return _.get(this._referenceData, 'party.party');
  }

  get staticFlagList() {
    return _.get(this._referenceData, 'flag.flags');
  }

  get staticPortList() {
    return _.get(this._referenceData, 'flag.portsOfRegistry');
  }

  isClassStatusChanged() {
    const previousClassStatus = _.get(this, '_prevVersion.classStatus', null);
    const currentClassStatus = _.get(this, 'asset.classStatus', null);

    if (_.isEmpty(previousClassStatus)) {
      return !_.isEmpty(currentClassStatus);
    }

    const prevId = _.get(previousClassStatus, 'id', null);
    const curId = _.get(currentClassStatus, 'id', null);
    return prevId !== curId;
  }

  classStatusChanged() {
    if (this.isClassStatusChanged()) {
      _.set(this, 'asset.effectiveDate',
        this._moment({
          hour: 0,
          minute: 0,
          seconds: 0,
          milliseconds: 0
        }).toISOString()
      );
    } else {
      _.set(this, 'asset.effectiveDate',
        _.get(this, '_prevVersion.effectiveDate', null));
    }
  }

  /**
   * @description Verify the typed-in IMO Number
   */
  async verifyImo() {
    this.imoValidated = true;
    try {
      if (_.size(_.toString(this.asset.leadImo)) ===
        this._imoNumberLength) {
        this._ihsLeadAsset = await this._ihsService.get(this.asset.leadImo);
      }
    } catch (e) {
      this._ihsLeadAsset = null;
    }
    if (_.size(_.toString(this.asset.leadImo)) ===
        this._imoNumberLength) {
      this.imoValidated = Boolean(this._ihsLeadAsset);
    }
  }

  compareLeadImo() {
    const editForm = this.form;
    if (_.isEqual(this.asset.leadImo, _.get(this.asset, 'ihsAsset.id')) &&
      !editForm.leadImo.$viewValue) {
      _.set(editForm, 'lrLeadAsset.$invalid', true);
      return true;
    }
  }

  validForm() {
    /* eslint no-extra-parens: 0 */
    return !(_.get(this.form, 'lrLeadAsset.$invalid') ||
      !(_.get(this.form, 'lrLeadAsset.$pristine') ||
        this.imoValidated));
  }
}
