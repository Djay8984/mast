export templateUrl from './edit-asset-details.html';
export controller from './edit-asset-details.controller';

export const resolve = {

  /* @ngInject */
  referenceData: (referenceDataService) =>
    referenceDataService.get([
      'asset.assetTypes',
      'asset.classDepartments',
      'asset.classStatuses',
      'asset.classMaintenanceStatuses',
      'asset.iacsSocieties',
      'asset.lifecycleStatuses',
      'asset.ruleSets',
      'case.officeRoles',
      'employee.office',
      'flag.flags',
      'flag.portsOfRegistry',
      'party.party',
      'party.partyRoles'
    ])
};
