import * as _ from 'lodash';

import Base from 'app/base.class';

import Asset from 'app/models/asset.model';

import './partials/basic-asset-details.html';
import './partials/build-details.html';
import './partials/ruleset.html';
import './partials/class-maintenance.html';
import './partials/flag-details.html';
import './partials/customers.html';
import './partials/offices.html';

import Header from '../header';

export default class ViewAssetDetailsController extends Base {
  /* @ngInject */
  constructor(
    $state,
    asset,
    ihsService,
    ihsAsset,
    pageService,
    navigationService
  ) {
    super(arguments);
    this.assetModel = Reflect.construct(Asset, [asset]);
    this.header = new Header(this);
  }

  get activeTab() {
    return this.header.activeTab;
  }

  get asset() {
    return this.assetModel;
  }

  /**
   * @return {Object} Asset Returns IHS object
   */
  get ihsAsset() {
    return !_.isEmpty(this._ihsAsset) ? this._ihsAsset : null;
  }

  get isCheckedOut() {
    return this._pageService.isCheckedOut(this._asset);
  }
}
