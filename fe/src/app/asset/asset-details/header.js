import HeaderUrl from './header.html';

export default class Header {
  constructor(parent) {
    this.parent = parent;
    this.config = parent.config;

    this.htmlUrl = HeaderUrl;

    this.$set = {
      _pageService: 'pageService',
      _navigationService: 'navigationService'
    };

    this.activeTab = { assetDetails: true };
  }

  get asset() {
    return this.parent._asset;
  }

  async checkOut() {
    await this._pageService.checkOut(this.asset);
    this.parent._$state.go(this.parent._$state.current.name,
      { ...this.parent._$state.params, draft: true },
      { reload: true });
  }

  back() {
    this._navigationService.back();
  }
}
