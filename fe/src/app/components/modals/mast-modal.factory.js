import * as angular from 'angular';
import confirmationTemplateUrl from './templates/confirmation-modal.html';

/* @ngInject */
export default (
  $controller,
  $document,
  $http,
  $templateCache,
  $rootScope,
  $compile,
  $timeout,
  $q,
  FoundationApi
) => {
  return ModalFactory;

  function ModalFactory(config) {
    // for prototype functions
    const self = this;
    const id = config.id || FoundationApi.generateUuid();
    const modalResultDeferred = $q.defer();
    const ctrlLocals = {};
    const container = angular.element(config.container || document.body);
    const modalInstance = {
      close: (result) =>
        destroy()
          .then(() => modalResultDeferred.resolve(result))
          .then(() => modalResultDeferred.promise),
      dismiss: (reason) =>
        destroy()
          .then(() => modalResultDeferred.reject(reason))
          .then(() => modalResultDeferred.promise)
    };
    const props = [
      'animationIn',
      'animationOut',
      'overlay',
      'overlayClose',
      'class'
    ];

    const tpls = {
      confirmation: confirmationTemplateUrl
    };

    let attached = false;
    let destroyed = false;
    let html = null;
    let element = null;
    let fetched = null;
    let modalScope = null;
    let ctrlInstance = null;

    config.overlayClose = config.overlayClose || false;


    config.templateUrl = config.templateUrl || tpls[config.templateReady];

    if (config.templateUrl) {
      // get template
      fetched = $http.get(config.templateUrl, {
        cache: $templateCache
      }).then((response) => {
        html = response.data;
        assembleDirective();
      });
    } else if (config.template) {
      // use provided template
      fetched = true;
      html = config.template;
      assembleDirective();
    }

    self.activate = activate;
    self.deactivate = deactivate;
    self.toggle = toggle;
    self.destroy = destroy;

    return {
      isActive,
      activate,
      deactivate,
      toggle,
      destroy
    };

    function checkStatus() {
      if (destroyed) {
        throw new Error('Error: Modal was destroyed.' +
          'Delete the object and create a new ModalFactory instance.');
      }
    }

    function isActive() {
      return !destroyed && modalScope && modalScope.active === true;
    }

    function activate() {
      checkStatus();
      return $timeout(() => {
        init(true);
        FoundationApi.publish(id, 'show');
      }, 0, false)
        .then(() => modalResultDeferred.promise);
    }

    function deactivate() {
      checkStatus();
      return $timeout(() => {
        init(false);
        FoundationApi.publish(id, 'hide');
      }, 0, false);
    }

    function toggle() {
      checkStatus();
      return $timeout(() => {
        init(true);
        FoundationApi.publish(id, 'toggle');
      }, 0, false);
    }

    function init(state) {
      $q.when(fetched).then(() => {
        if (!attached && html.length > 0) {
          container.append(element);
          $compile(element)(modalScope);
          attached = true;
        }

        modalScope.active = state;
      });
    }

    function assembleDirective() {
      // check for duplicate elements to prevent factory from cloning modals
      if (document.getElementById(id)) {
        return;
      }

      html = `<zf-modal id="${id}">${html}</zf-modal>`;
      element = angular.element(html);

      // account for directive attributes and modal classes
      for (let i = 0; i < props.length; i++) {
        const prop = props[i];

        if (config.hasOwnProperty(prop)) {
          switch (prop) {
            case 'animationIn':
              element.attr('animation-in', config[prop]);
              break;
            case 'animationOut':
              element.attr('animation-out', config[prop]);
              break;
            case 'overlayClose':
              element.attr('overlay-close',
                config[prop] === false ? 'false' : 'true');
              break;
            case 'class':
              if (angular.isString(config[prop])) {
                config[prop].split(' ').forEach(addClass);
              } else if (angular.isArray(config[prop])) {
                config[prop].forEach(addClass);
              }
              break;
            default:
              element.attr(prop, config[prop]);
              break;
          }
        }
      }

      modalScope = $rootScope.$new();
      modalScope.$close = modalInstance.close;
      modalScope.$dismiss = modalInstance.dismiss;

      if (config.scope) {
        for (const prop in config.scope) {
          if (config.scope.hasOwnProperty(prop)) {
            modalScope[prop] = config.scope[prop];
          }
        }
      }

      if (config.controller) {
        ctrlLocals.$scope = modalScope;
        ctrlLocals.$modalInstance = modalInstance;
        if (config.inject) {
          for (const prop in config.inject) {
            if (config.inject.hasOwnProperty(prop)) {
              ctrlLocals[prop] = config.inject[prop];
            }
          }
        }
        ctrlInstance = $controller(config.controller, ctrlLocals);
        modalScope[config.controllerAs || config.controller] = ctrlInstance;
      }

      function addClass(klass) {
        element.addClass(klass);
      }
    }

    function destroy() {
      self.deactivate();
      return $timeout(() => {
        modalScope.$destroy();
        element.remove();
        destroyed = true;
      }, 0, false)
        .then(() => FoundationApi.unsubscribe(id));
    }
  }
};
