import templateUrl from './full-page.html';

export default () => ({
  templateUrl,
  restrict: 'E',
  scope: {
    title: '='
  },
  transclude: {
    header: '?modalHeader',
    body: 'modalBody',
    footer: '?modalFooter'
  }
});
