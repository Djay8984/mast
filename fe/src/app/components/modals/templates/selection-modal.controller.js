import * as _ from 'lodash';

import Base from 'app/base.class';

export default class SelectionController extends Base {

  /* @ngInject */
  constructor(
    multi,
    options,
    selected,
    title
  ) {
    super(arguments);

    this.selected = selected || [];
  }

  toggle(option) {
    if (!this.isSelected(option)) {
      if (!this._multi) {
        this.selected = [];
      }
      this.selected.push(option);
    } else {
      _.remove(this.selected, option);
    }
  }

  isSelected(option) {
    return _.includes(this.selected, option);
  }

  get title() {
    return this._title;
  }

  get options() {
    return this._options;
  }
}
