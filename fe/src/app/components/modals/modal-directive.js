export default class ModalDirective {
  constructor(templateUrl) {
    return () => ({
      templateUrl,
      restrict: 'E',
      scope: {
        title: '='
      },
      transclude: {
        header: '?modalHeader',
        body: 'modalBody',
        footer: '?modalFooter'
      }
    });
  }
}
