import * as angular from 'angular';

import templates from './templates';

import MastModalFactory from './mast-modal.factory';

import ModalDirective from './modal-directive.js';
import fullPageModalTemplate from './partials/full-page.html';
import smallModalTemplate from './partials/small.html';

export default angular
  .module('app.components.modals', [
    'foundation.core',
    templates
  ])
  .factory('MastModalFactory', MastModalFactory)
  .directive('fullScreenModal', new ModalDirective(fullPageModalTemplate))
  .directive('smallModal', new ModalDirective(smallModalTemplate))
  .name;
