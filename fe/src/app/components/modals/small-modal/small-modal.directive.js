import templateUrl from './small.html';

export default () => ({
  templateUrl,
  restrict: 'E',
  scope: {
    title: '='
  },
  transclude: {
    header: '?modalHeader',
    body: 'modalBody',
    footer: '?modalFooter'
  }
});
