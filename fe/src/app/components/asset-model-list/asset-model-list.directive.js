import templateUrl from './asset-model-list.html';
import AssetModelListController from './asset-model-list.controller';

/* @ngInject */
export default () => ({
  templateUrl,
  controller: AssetModelListController,
  controllerAs: 'vm',
  bindToController: {
    asset: '=',
    assetModel: '=',
    selectedItem: '=',
    currentLevel: '=',
    onItemSelected: '&'
  },
  restrict: 'AE',
  scope: true
});
