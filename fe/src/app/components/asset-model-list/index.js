import * as angular from 'angular';
import assetModelListDirective from './asset-model-list.directive';

export default angular
  .module('app.components.asset-model-list', [])
  .directive('assetModelList', assetModelListDirective)
  .name;
