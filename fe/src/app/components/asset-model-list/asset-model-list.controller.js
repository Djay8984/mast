import * as _ from 'lodash';
import Base from 'app/base.class';
import lazyLoadMixin from 'app/services/lazy-load-mixin';

export default class AssetModelList extends lazyLoadMixin(Base) {
  /* @ngInject */
  constructor(
    $state,
    assetModelDecisionService,
    assetItemRelationshipType
  ) {
    super(arguments);

    this.defaultNumItemsShown = 50;
    this.numItemsIncrement = 5;
    this.numItemsShown = this.defaultNumItemsShown;

    /* A user may have selected an item from the hierachy view
    * return to this item in list view*/
    this.selectItem(this.selectedItem);
  }

  get items() {
    return _.get(this.currentLevel, 'items');
  }

  get itemId() {
    return _.get(this.selectedItem, 'id');
  }

  async selectItem(item) {
    const ancestors = _.filter(item.parents, { id: item.id });

    if (_.size(ancestors) < 2) {
      await this.lazyLoad(item);
    } else {
      item = _.last(ancestors);
    }

    item.select({ reset: true });

    this.selectedItem = item;

    if (!_.isEmpty(_.get(item, 'items'))) {
      this.currentLevel = item;
      this.numItemsShown = this.defaultNumItemsShown;
    } else {
      this.currentLevel = item.parent;
    }
  }

  get showMorePages() {
    this.numItemsShown = _.toNumber(this.numItemsShown +
      this.numItemsIncrement);
  }

  get hasMorePages() {
    const numItems = _.size(_.get(this, 'selectedItem.items')) ||
      _.size(_.get(this, 'selectedItem.parent.items'));
    return this.numItemsShown < numItems;
  }

  openItem(node) {
    this.selectItem(node);
    this._$state.go('item.attributes', { itemId: node.id });
  }
}
