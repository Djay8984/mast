import Base from 'app/base.class';
import * as _ from 'lodash';
import * as angular from 'angular';

import jobStatusTemplateUrl from './partials/job-status.html';
import serviceDeliveryOffice from './partials/service-delivery-office.html';
import startDate from './partials/start-date.html';
import surveyorName from './partials/surveyor-name.html';

const ROW_LENGTH = 4;

export default class JobSearchController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $q,
    $stateParams,
    dateFormat,
    jobStatus,
    moment,
    referenceDataService,
    typeaheadService
    ) {
    super(arguments);

    this.dates = {
      fromDate: moment().hour(12).startOf('h'),
      toDate: moment().hour(12).startOf('h'),
      startDateMin: moment().add(-9999, 'Y').hour(12).startOf('h'),
      startDateMax: moment().add(9999, 'Y').hour(12).startOf('h')
    };

    this.filters = {
      startDate: {
        name: 'Start date',
        templateUrl: startDate,
        parameters: ['startDateMin', 'startDateMax']
      },
      surveyorName: {
        name: 'Surveyor name',
        templateUrl: surveyorName,
        parameters: ['employeeId']
      },
      jobStatus: {
        name: 'Job status',
        templateUrl: jobStatusTemplateUrl,
        parameters: ['jobStatusId']
      },
      serviceDeliveryOffice: {
        name: 'SDO',
        templateUrl: serviceDeliveryOffice,
        parameters: ['officeId']
      }
    };

    this.parameters = {};

    $scope.$watch('vm.parameters.startDateMin', (newval, oldval) => {
      this.filters.startDate.active = newval || this.parameters.startDateMax;
    });

    $scope.$watch('vm.parameters.startDateMax', (newval, oldval) => {
      this.filters.startDate.active = this.parameters.startDateMin || newval;
    });

    this.setupData();
  }

  /**
   * sets up the reference data & applies it to the controller, then fires off
   * the unmarshalling
   * @return {promise} setting up promise, resolves when set up
   */
  async setupData() {
    if (!this._building) {
      const self = this;
      const builder = async () => {
        const referenceDataMap = {
          jobStatuses: 'job.jobStatuses',
          employeeOffices: 'employee.office',
          surveyors: 'employee.employee'
        };

        const data = await self._referenceDataService
          .get(_.values(referenceDataMap));

        _.forEach(referenceDataMap, (value, key) =>
          _.set(self, key, _.get(data, value)));

        self.sdoTypeahead = self._typeaheadService.query(self.employeeOffices);
        self.surveyorTypeahead = self._typeaheadService.query(self.surveyors);

        self.unmarshall();
      };

      this._building = builder();
    }
    return this._building;
  }

  /**
   * interprets the model passed & transmogrifies it into the filters' content
   */
  async unmarshall() {
    if (this._building) {
      await this._building;
    }

    const model = this._$scope.getModel() || {};
    this.parameters = angular.copy(model);

    if (model.officeId) {
      this.parameters.officeId = await this._serviceDeliveryOfficeService
        .get(_.first(model.officeId));
    }

    if (model.employeeId) {
      this.parameters.employeeId = _.find(this.surveyors,
        { id: _.first(model.employeeId) });
    }

    if (model.startDateMin) {
      this.parameters.startDateMin = model.startDateMin;
    }

    if (model.startDateMax) {
      this.parameters.startDateMax = model.startDateMax;
    }

    _.forEach(this.filters, (filter) => {
      const actualModel = _.pick(model, filter.parameters);
      const defaultModel = _.pick(this.defaultModel, filter.parameters);

      _.set(filter, 'active', _.isEmpty(defaultModel) ?
          !_.isEmpty(actualModel) :
          !_.isEqual(actualModel, defaultModel));
    });


    _.forEach(this.jobStatuses, (status) => {
      if (_.includes(_.values(_.omit(this._jobStatus.toObject(),
        ['aborted', 'cancelled', 'closed'])), status.id)) {
        _.set(status, 'selected', true);
      }
    });
    this.defaultJobStatuses = angular.copy(this.jobStatuses);

    if (model.jobStatusId) {
      _.forEach(this.jobStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(model.jobStatusId, (id) =>
        _.set(_.find(this.jobStatuses, { id }), 'selected', true));
    } else if (_.get(this, 'defaultModel.jobStatusId')) {
      _.forEach(this.jobStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(this.defaultModel.jobStatusId, (id) =>
        _.set(_.find(this.jobStatuses, { id }), 'selected', true));
      this.defaultJobStatuses = angular.copy(this.jobStatuses);
    }
  }

  /**
   * @return {string} date format
   */
  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @param {array} list objects to set the selected attribute on & extract ids
   * @param {boolean} selected value for selected attribute
   * @return {array} modified list
   */
  setListSelected(list, selected) {
    return _.map(list, (item) => _.set(item, 'selected', selected));
  }

  resetFilters() {
    this.parameters = {};

    _.forEach(this.jobStatuses, (status) => {
      if (_.includes(_.values(_.omit(this._jobStatus.toObject(),
        ['aborted', 'cancelled', 'closed'])), status.id)) {
        _.set(status, 'selected', true);
      }
    });

    _.forEach(this.filters, (filter) => _.set(filter, 'active', false));

    this._$scope.setModel({});
  }

  /**
   * @param {object|number|string} thingOne thing to compare
   * @param {object|number|string} thingTwo thing to compare
   * @return {boolean} whether the two things are equal
   */
  getFilterActivation(thingOne, thingTwo) {
    return _.isEqual(thingOne, thingTwo);
  }

  /**
   * @param {object} filter thing to toggle selected attribute on
   * disables the selection on all other filters too
   */
  toggleFilterSelected(filter) {
    _.reject(this.filters, filter).forEach(
      (deselectedFilter) => deselectedFilter.selected = false);
    filter.selected = !filter.selected;
  }

  /**
   * @param {object} filter thing to find parameters to strip
   * strips parameters relevant to a filter from the parameters object
   */
  setFilterInactive(filter) {
    _.omit(this.parameters, filter.parameters);
    filter.active = false;
  }

  /**
   * @param {int} row number of row to extract
   * @return {array} array of filters extracted
   */
  getFilters(row) {
    return _.toArray(_.pick(this.filters, _.slice(_.keys(this.filters),
        row * ROW_LENGTH, row * ROW_LENGTH + ROW_LENGTH)));
  }

  /**
   * @return {boolean} whether or not all of the filters' forms are valid
   */
  getFiltersValidity() {
    return _.every(_.filter(this.filters, 'form'), (filter) =>
        _.get(filter, 'form.$valid'));
  }

  /**
   * builds parameters from inputs, & exposes via the model only if the filter
   * forms are valid
   */
  applyFilters() {
    const parameters = angular.copy(this.parameters);

    parameters.search = _.parseInt(parameters.search);

    if (!_.isEmpty(_.filter(this.jobStatuses, 'selected'))) {
      parameters.jobStatusId =
        _.map(_.filter(this.jobStatuses, 'selected'), 'id');
    }

    if (this.parameters.startDateMin) {
      parameters.startDateMin =
        this._moment(this.parameters.startDateMin).toISOString();
    }

    if (this.parameters.startDateMax) {
      parameters.startDateMax =
        this._moment(this.parameters.startDateMax).toISOString();
    }

    parameters.officeId = _.isEmpty(this.parameters.officeId) ?
      null : [this.parameters.officeId.id];

    parameters.employeeId = _.isEmpty(this.parameters.employeeId) ?
      null : [this.parameters.employeeId.id];

    const defaultParameters = _.intersection(
      _.keys(this.defaultModel),
      _.flatMap(this.filters, 'parameters'));

    _.forEach(
      _.filter(this.filters,
        (filter) => !_.isEmpty(
          _.intersection(filter.parameters, defaultParameters))),
      (filter) => _.set(filter, 'active', true));

    this._$scope.setModel(_.omitBy(
      _.omit(parameters,
        _.flatMap(
          _.reject(this.filters, 'active'),
          'parameters')),
        _.isNil));
  }

  get minSetDate() {
    return _.get(this, 'parameters.startDateMin') || this.dates.startDateMin;
  }

  get maxSetDate() {
    return _.get(this, 'parameters.startDateMax') || this.dates.startDateMax;
  }
}
