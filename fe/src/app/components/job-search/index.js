import * as angular from 'angular';

import jobSearchDirective from './job-search.directive';

export default angular
  .module('app.components.job-search', [])
  .directive('jobSearch', jobSearchDirective)
  .name;
