import * as _ from 'lodash';

import templateUrl from './job-search.html';
import JobSearchController from './job-search.controller';

/* @ngInject */
export default () => ({
  bindToController: true,
  controller: JobSearchController,
  controllerAs: 'vm',
  link: (scope, element, attrs, controller) => _.merge(scope, {
    setModel: controller.$setViewValue,
    getModel: () => controller.$viewValue
  }),
  require: 'ngModel',
  restrict: 'AE',
  scope: {
    defaultModel: '='
  },
  templateUrl
});
