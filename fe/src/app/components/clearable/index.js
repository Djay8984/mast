import * as angular from 'angular';

import clearableDirective from './clearable.directive';

export default angular
  .module('app.components.clearable', [])
  .directive('clearable', clearableDirective)
  .name;
