import * as _ from 'lodash';
import * as angular from 'angular';

const CLEARABLE_CLASS = 'typeahead-clearable';

/* @ngInject */
export default (
  $parse,
  $compile,
  $window
) => ({
  require: 'ngModel',
  restrict: 'EA',
  link(originalScope, element, attrs, ngModel) {
    // create a child scope for the clearable directive
    const scope = originalScope.$new();
    originalScope.$on('$destroy', () => {
      if (!_.isNil(scope)) {
        scope.$destroy();
      }
    });

    /**
     * Add the appropriate `top` and `left` values to the clear button
     * @param  {Element} input The input
     * @param  {Element} clearEl The clear button
     */
    const positionClearEl = (input, clearEl) => {
      // grab the input's label only if it is left aligned
      const leftAlignedLabel = input.parentNode.querySelector('label.left');
      const pLabel = input.parentNode.querySelector('p');

      // calculate the required left offset (width of input + label width)
      const leftOffset = _.get(input, 'offsetWidth') +
        _.get(leftAlignedLabel, 'offsetWidth', 0) -
        _.get(clearEl, 'offsetWidth');

      // add relative positioning to parent to contain the clear button
      input.parentNode.classList.add('pos-rel');

      // apply the `left` value to the clear button
      _.set(clearEl, 'style.left', `${leftOffset}px`);

      // if the input is within a `.grid-content` or `form-control` element
      // we need to offset the button by the padding on the left side
      if (
        input.parentNode.classList.contains('grid-content') ||
        input.parentNode.classList.contains('form-control')
      ) {
        _.set(clearEl, 'style.marginLeft', `0.2rem`);
      }

      // if the label is top aligned, nudge the button down slightly
      // if the label is a <p> tag for whatever reason, we need to nudge
      // it down even further
      if (!leftAlignedLabel) {
        _.set(clearEl, 'style.top', pLabel ? '2.85rem' : '1.35rem');
      }
    };

    // create clear button
    const clearEl = angular.element(
      `<button class="typeahead-clear" ng-hide="hideClear" ng-click="clear()">
        Clear
      </button>`
    );

    // clear function used to clear the input
    scope.clear = () => {
      element.val(null);
      ngModel.$setViewValue(null);
      ngModel.$setPristine();
      element.prop('disabled', false);
      element.removeClass(CLEARABLE_CLASS);
    };

    // add clear button after the input element
    const $clear = $compile(clearEl)(scope);
    element.after($clear);

    // hide clear button when the modelValue is '' or null or undefined
    scope.$watch(
      () => ngModel.$modelValue,
      (newValue) => {
        if (

          // _.size equivalent of not null, undefined, or ''
          _.size(ngModel.$modelValue)
        ) {
          scope.hideClear = false;
          element.addClass(CLEARABLE_CLASS);
          positionClearEl(_.first(element), _.first(clearEl));
        } else {
          scope.hideClear = true;
        }
      }
    );

    // on resize, we should reposition the clear button to account for
    // the new width of the associated input
    angular.element($window).bind('resize', () =>
      positionClearEl(_.first(element), _.first(clearEl)));
  }
});

