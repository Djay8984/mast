import * as _ from 'lodash';

import templateUrl from './select-modal.html';

import selectionTemplateUrl from
  'app/components/modals/templates/selection-modal.html';

/* @ngInject */
export default (
  MastModalFactory
) => ({
  link: (scope, element, attributes, controller) => _.merge(scope, {
    getSelected: () => _.reject(scope.multi ?
      controller.$viewValue : [controller.$viewValue], _.isNil),
    open: async () => {
      const selected = await new MastModalFactory({
        templateUrl: selectionTemplateUrl,
        controller: 'selectionModalController',
        controllerAs: 'vm',
        inject: {
          multi: scope.multi,
          options: scope.values,
          selected: controller.$viewValue,
          title: `Select ${_.lowerCase(scope.label)}`
        },
        scope: { labelProperty: scope.labelProperty },
        class: 'dialog'
      }).activate();

      controller.$setViewValue(scope.multi ? selected : _.first(selected));
    },
    remove: (item) => controller.$setViewValue(scope.multi ?
      _.reject(scope.getSelected(), item) : null)
  }),
  require: 'ngModel',
  scope: {
    label: '=',
    labelProperty: '=',
    multi: '=',
    values: '='
  },
  templateUrl
});
