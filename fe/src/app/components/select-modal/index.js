import * as angular from 'angular';

import SelectModalDirective from './select-modal.directive';

export default angular.module('app.component.select-modal', [])
  .directive('selectModal', SelectModalDirective)
  .name;
