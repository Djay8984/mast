import templateUrl from './autocomplete-list.html';

/* @ngInject */
export default () => ({
  templateUrl,
  replace: true
});
