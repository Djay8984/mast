import autocompleteController from './autocomplete.controller';
import * as _ from 'lodash';

/* @ngInject */
export default ($compile, $parse) => ({
  controller: autocompleteController,
  controllerAs: 'vm',
  require: '?ngModel',
  bindToController: {
    data: '=autocomplete',
    ngModel: '='
  },
  scope: {},
  link: (scope, element, attrs, ngModel) => {
    scope.link = {
      init: function init() {
        this.setParsers();
        this.setElements();
        this.setModel();
        this.setWatch();
      },
      setElements: () => {
        scope.vm.element = element;
        scope.vm.listElement = $compile('<div autocomplete-list></div>')(scope);
        scope.vm.openedElement = element.after(scope.vm.listElement);
      },
      setModel: () => {
        const data = scope.vm.data;
        if (data && ngModel) {
          data.ngModel = ngModel;
        }
      },
      setWatch: () => {
        const once = scope.$watch('vm.data.ngModel.$viewValue', (val) => {
          scope.vm.onLoad(val);
          once();
        });
      },

      // -----------------------------------------------------------
      //  result is object but viewValue is string
      setParsers: () => {
        ngModel.$parsers.push((val) => scope.vm.data.result);
        ngModel.$formatters.push((val) => _.isObject(val) ? val.name : val);
      }
    };
    scope.link.init();
  }
});
