import * as angular from 'angular';
import autocomplete from './autocomplete.directive';
import autocompleteList from './autocomplete-list.directive';

export default angular
  .module('app.components.autocomplete', [])
  .directive('autocomplete', autocomplete)
  .directive('autocompleteList', autocompleteList)
  .name;
