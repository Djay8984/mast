import * as angular from 'angular';
import * as _ from 'lodash';
import Base from 'app/base.class';

export default class AutocompleteController extends Base {
  /* @ngInject */
  constructor($scope, $timeout) {
    super(arguments);
  }

  // -----------------------------------------------------
  get value() {
    return this.data.ngModel.$viewValue;
  }

  // -----------------------------------------------------
  set value(val) {
    this.data.ngModel.$setViewValue(val);
    this.data.ngModel.$render();
  }

  // -----------------------------------------------------
  set disabled(stage) {
    this.data.disabled = stage;
    this.element.attr('disabled', stage);
  }

  // -----------------------------------------------------
  get opened() {
    return this.data.opened && this.value;
  }

  // -----------------------------------------------------
  onLoad() {
    this.updateFromString();
    this.bindKeys();
  }

  // -----------------------------------------------------
  //  convert result array to object by name
  arr2obj(arr, name = 'name') {
    const result = {};
    if (arr) {
      arr.forEach((val) => {
        result[val[name]] = val;
      });
    }
    return result;
  }

  // -----------------------------------------------------
  //  if result is string, get object
  async updateFromString() {
    if (this.value) {
      const tmpValue = this.value;
      this.disabled = true;
      this.value = 'Loading...';

      if (tmpValue) {
        const list = await this.search(tmpValue);
        this.data.result = this.arr2obj(list)[tmpValue];

        if (!this.data.result) {
          this.value = '';
        } else {
          this.data.isSelected = true;
          this.value = tmpValue;
        }
      }
    }
    await this.watchValue();
    this.disabled = false;
  }

  // -----------------------------------------------------
  async update(val) {
    this.data.value = val;
    this.data.selected = null;

    if (val && this.data.ngModel.$dirty) {
      const list = await this.search(val);
      this.data.list = list;
      this.data.selected = list[0];

      this.open();
    } else {
      this.data.selected = null;
      this.close();
    }
  }

  async search(val) {
    const list = await this.data.search(val);
    return _.filter(list, (item) => {
      const results = item.name.toLowerCase().split(val.toLowerCase());
      return results.length > 1;
    });
  }

  // -----------------------------------------------------
  async watchValue() {
    this.watch = await this._$scope.$watch('vm.data.ngModel.$viewValue',
      (val) => {
        this.update(val);
      });
  }

  // -----------------------------------------------------
  open() {
    if (!this.data.isSelected && !this.data.opened && !this.data.disabled) {
      this.data.opened = true;
      this.bindClose();
    }
    this.data.isSelected = false;
  }

  // -----------------------------------------------------
  close() {
    this.data.opened = false;
    if (this.bg) {
      this.bg.unbind('click');
      this.element.unbind('click');
    }
  }

  // -----------------------------------------------------
  select(item) {
    this.close();
    this.element[0].focus();
    return this.value ? this.save(item) : this.validate();
  }

  // -----------------------------------------------------
  save(item) {
    this.data.result = item;
    this.data.isSelected = true;
    this.value = item.name;
  }

  // -----------------------------------------------------
  validate() {
    this.close();

    if (!this.value) {
      this.data.result = null;
      this.ngModel = null;
      return false;
    }
    this.data.result =
      this.arr2obj(this.data.list)[this.value] || this.data.result;
    if (this.data.result) {
      return this.select(this.data.result);
    }
    this.value = '';
  }

  // -----------------------------------------------------
  restore() {
    this.select(this.data.result);
  }

  // -----------------------------------------------------
  //  Close on background click
  bindClose() {
    this.element.bind('click', (event) => {
      event.stopPropagation();
    });

    this.bg = angular.element(document).bind('click', () => {
      this.validate();
      this._$scope.$apply();
    });
  }

  // -----------------------------------------------------
  move(up) {
    const nr = up ? -1 : 1;
    const index = this.data.list.indexOf(this.data.selected);
    const altPos = index ? 0 : this.data.list.length - 1;
    this.data.selected = this.data.list[index + nr] || this.data.list[altPos];
  }

  // -----------------------------------------------------
  bindKeys() {
    this.keyMap = {
      38: { name: 'Arrow up', action: () => this.move(true) },
      40: { name: 'Arrow down', action: () => this.move() },
      27: { name: 'Esc', action: () => this.restore() },
      13: { name: 'Enter', action: () => this.select(this.data.selected) },
      9: {
        name: 'Tab',
        default: true,
        action: () => this.select(this.data.selected)
      }
    };

    this.element.bind('keydown', (event) => {
      if (this.keyMap[event.which]) {
        if (this.data.opened && !this.keyMap[event.which].default) {
          event.preventDefault();
        }
        this.keyMap[event.which].action();
        this._$scope.$apply();
      }
    });
  }
}
