import * as angular from 'angular';
import attachmentType from './attachment-type.directive';

export default angular
  .module('app.components.attachment-type', [])
  .directive('attachmentType', attachmentType)
  .name;
