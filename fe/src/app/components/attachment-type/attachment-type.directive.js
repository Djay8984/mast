import controller from './attachment-type.controller';
import templateUrl from './attachment-type.html';

/* @ngInject */
export default () => ({
  templateUrl,
  controller,
  controllerAs: 'vm',
  scope: true,
  restrict: 'AE',
  bindToController: {
    attachmentType: '=?',
    hideText: '=?'
  }
});
