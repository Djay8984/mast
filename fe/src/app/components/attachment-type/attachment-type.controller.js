import Base from 'app/base.class';
import * as _ from 'lodash';

export default class AttachmenTypeController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
  }

  get attachmentTypeClass() {
    const name = _.get(this, 'attachmentType');
    let type = '';
    switch (name) {
      case 'PNG':
      case 'JPG':
      case 'JPEG':
        type = 'Image';
        break;
      case 'Note':
        type = 'Notes';
        break;
      case 'PDF':
      case 'PPTX':
      case 'XLSX':
      case 'TXT':
      case 'DOCX':
      case 'DOC':
      default:
        type = 'File';
        break;
    }
    return `svg-icon-${type}`;
  }
}
