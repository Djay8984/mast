import * as angular from 'angular';

import assetModelHierarchy from
  './asset-model-hierarchy.directive';

import assetModelHierarchyBuild from
  './build/asset-model-hierarchy-build.directive';

import assetModelHierarchyMetadata from
  './utility/asset-model-hierarchy-metadata.directive';

import assetModelHierarchyPreview from
  './preview/asset-model-hierarchy-preview.directive';

import assetModelHierarchySelector from
  './selector/asset-model-hierarchy-selector.directive';

export default angular
  .module('app.components.asset-model-hierarchy', [])
  .directive('assetModelHierarchy', assetModelHierarchy)
  .directive('assetModelHierarchyBuild', assetModelHierarchyBuild)
  .directive('assetModelHierarchyMetadata', assetModelHierarchyMetadata)
  .directive('assetModelHierarchyPreview', assetModelHierarchyPreview)
  .directive('assetModelHierarchySelector', assetModelHierarchySelector)
  .name;
