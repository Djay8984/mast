import AssetModelHierarchyBuildController
  from './asset-model-hierarchy-build.controller';
import templateUrl from './asset-model-hierarchy-build.html';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  bindToController: true,
  controller: AssetModelHierarchyBuildController,
  controllerAs: 'vm',
  scope: {
    assetModel: '=',
    asset: '=',
    debug: '=',
    loadItems: '&',
    selectedItem: '=?'
  },
  templateUrl
});
