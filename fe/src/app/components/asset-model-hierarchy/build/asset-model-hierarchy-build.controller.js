import Base from 'app/base.class';
import * as _ from 'lodash';
import draftLazyLoadMixin from 'app/services/draft-lazy-load-mixin';
import deleteItemDialog from './delete-item-dialog.html';
import errorItemDialog from './error-item-dialog.html';

export default class HierarchyBuildController extends draftLazyLoadMixin(Base) {
  /* @ngInject */
  constructor(
    draftItemService,
    MastModalFactory
  ) {
    super(arguments);
  }

  //  ------------------------------------------------------------
  //  Modal templates
  get modals() {
    return {
      confirmDelete: (item) => new this._MastModalFactory({
        templateUrl: deleteItemDialog,
        scope: {
          customText: `Are you sure you want to delete ${item.name}?`
        },
        class: 'dialog'
      }),
      error: (item) => new this._MastModalFactory({
        templateUrl: errorItemDialog,
        scope: {
          customText: `${item.name} cannot be deleted because it is
            associated with services defects or codicils`
        },
        class: 'dialog'
      })
    };
  }

  //  ----------------------------------------------------------
  //  Default tempalte
  get templateUrl() {
    return 'nested-2.html';
  }

  //  ----------------------------------------------------------
  //  Item line css class
  itemClass(data) {
    return {
      'is-selected': data.selected,
      'is-child-selected': data.childSelected,
      'has-items': data.items && data.items.length,
      'disabled': data.loading
    };
  }

  //  ----------------------------------------------------------
  //  Delete item
  async deleteItem(item) {
    try {
      // validate item before allowing delete
      await this._draftItemService.validateDraftItem(this.asset.id, item.id);

      const dialogResponse = await this.modals.confirmDelete(item).activate();

      if (dialogResponse) {
        item.loading = true;
        const index = item.parent._items.indexOf(item);
        item.parent._items.splice(index, 1);
        await this._draftItemService
          .deleteDraftItem(this.asset.id, item.id);

        this.select(item.parent);

        item.loading = false;
      }
    } catch (e) {
      // assume 409 Conflict with another item. (Cannot delete)
      await this.modals.error(item).activate();
    }
  }

  //  ----------------------------------------------------------
  //  Show duplicate button
  canDuplicate(item) {
    const max = item.type.maxOccurs;

    if (_.isUndefined(max) || max === 1 || !item.originalItem) {
      return false;
    }
    if (max === 0) {
      return true;
    }
    const items = item.parent.items;
    const same = items.filter((v) => v.name === item.name);
    return same.length < max;
  }

  //  ----------------------------------------------------------
  //  Duplicate item
  async createDuplicate(item) {
    item.loading = true;
    const newItem = await this._draftItemService
        .addDuplicateAssetItem(this.asset, item.parentItem, item);

    item.parent.loaded = false;
    await this.expand(newItem, false);

    item.parent.items = newItem.items;
    item.loading = false;

    if (newItem.items.filter((val) => val.id === this.selectedItem.id)[0]) {
      this.selectedItem = item.parent;
      item.parent.select({ reset: false });
    }
  }

  //  ----------------------------------------------------------
  //  Move item
  async moveItem(item, step) {
    const index = item.parent.items.indexOf(item);
    const target = index + step;
    item.loading = true;

    try {
      await this._draftItemService
        .updateDisplayOrder(this.asset, item, target);

      this.swapItemPosition(item, index, target);
    } catch (e) {
      await this.modals.error(item).activate();
    }
    item.loading = false;
  }

  noChildItemsExpanded() {
    return !_.some(this.assetModel.items, { expanded: true });
  }

  swapItemPosition(item, oldPos, newPos) {
    const removed = item.parent.items.splice(oldPos, 1);
    item.parent.items.splice(newPos, 0, _.first(removed));
    item.displayOrder = newPos;
    item.parent.items[oldPos].displayOrder = oldPos;
  }
}
