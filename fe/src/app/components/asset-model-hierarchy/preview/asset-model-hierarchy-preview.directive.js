import AssetModelHierarchyPreviewController
  from './asset-model-hierarchy-preview.controller';

import templateUrl from './asset-model-hierarchy-preview.html';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  scope: true,
  templateUrl,
  controller: AssetModelHierarchyPreviewController,
  controllerAs: 'vm',
  bindToController: {
    selectedItems: '=',
    debug: '=',
    model: '=',
    asset: '=',
    assetModel: '=',
    relationships: '='
  }
});
