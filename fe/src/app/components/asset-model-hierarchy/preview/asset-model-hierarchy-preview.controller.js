import Base from 'app/base.class';
import * as _ from 'lodash';
import lazyLoadMixin from 'app/services/lazy-load-mixin';

export default class HierarchyViewPreviewController
  extends lazyLoadMixin(Base) {
  /* @ngInject */
  constructor(
    assetItemRelationshipType,
    assetModelDecisionService,
    referenceDataService
  ) {
    super(arguments);
  }

  /**
   * Expand function called by the +/- button. The item is loaded if required
   * and if it is selected prior to loading, its children are selected. If the
   * item has already been loaded, the expanded state is simple toggled
   * @param {Item} item the item to expand
   */
  async expand(item) {
    if (!item.loaded) {
      await this.lazyLoad(item);
      item.loaded = true;

      item.items.forEach((child) => {
        // Using Boolean(...) is cleaner than having an object/undefined as
        // the truthy/falsey values
        child.standaloneValid = Boolean(_.find(this.relationships,
          { toItemType: child.itemType.id }));

        child.valid = child.standaloneValid || item.valid;
      });

      if (item.selected) {
        this.selectChildren(item.items, item.selected);
      }
    }
    item.expand({ parents: false });
  }

  /**
   * Select function called by the checkbox ng-change
   * @param {Item} item the item to select
   */
  checkboxSelect(item) {
    this.selectItem(item, item.selected);
    this.postSelectToggle(item);
  }

  /**
   * Select function called by the <span> ng-click
   * @param {Item} item the item to select
   */
  select(item) {
    // Cleaner to check validity here rather than add an extra non-clickable
    // <span> to the DOM
    if (item.valid) {
      // item.selected is not automatically toggled as it is with the checkbox,
      // therefore must be toggled manually here
      this.selectItem(item, !item.selected);
      this.postSelectToggle(item);
    }
  }

  /**
   * Expands the item if it needs to be expanded, then selects its children
   * and updates its parent
   * @param {Item} item the current item
   */
  postSelectToggle(item) {
    if (item.loaded) {
      this.selectChildren(item.items, item.selected);
    }

    // Also select the parents if the current item would not be valid on its own
    if (item.selected && !item.standaloneValid) {
      this.selectParent(item);
    }
  }

  /**
   * Recursively selects all children as long as it is loaded
   * @param {List} children the list of children
   * @param {boolean} selectedState the state to set
   */
  selectChildren(children, selectedState) {
    children.forEach((child) => {
      this.selectItem(child, selectedState);
      if (child.loaded && child.items) {
        this.selectChildren(child.items, selectedState);
      }
    });
  }

  /**
   * Checks the item's parent to see if all its children have been selected
   * and selects it if required
   * @param {Item} item the current item
   */
  selectParent(item) {
    const parentItem = item.parent;

    if (parentItem.valid) {
      this.selectItem(parentItem, true);
      this.selectParent(parentItem);
    }
  }

  /**
   * Adds or removes an item from the selected item list if it is valid
   * @param {Item} item the item to add or remove
   * @param {boolean} selectedState whether to add or remove
   */
  selectItem(item, selectedState) {
    if (item.valid) {
      item.selected = selectedState;
      item.checked = selectedState;
      if (selectedState) {
        if (!_.includes(this.selectedItems, item)) {
          this.selectedItems.push(item);
        }
      } else {
        _.remove(this.selectedItems, item);
      }
    }
  }

  /**
   * @returns {boolean} whether or not any items have been expanded
   */
  noChildItemsExpanded() {
    return !_.some(this.assetModel.items, { 'expanded': true });
  }
}
