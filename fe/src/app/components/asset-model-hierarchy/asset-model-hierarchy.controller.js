import Base from 'app/base.class';
import * as _ from 'lodash';
import lazyLoadMixin from 'app/services/lazy-load-mixin';

export default class AssetModelHierarchyController
    extends lazyLoadMixin(Base) {
  /* @ngInject */
  constructor(
    $state,
    assetModelDecisionService,
    assetItemRelationshipType
  ) {
    super(arguments);

    // filter was unable to access the correct context.
    this.filterItems = _.bind(this._filterItems, this);

    // auto-expand the selected item
    if (!this.selectedItem.expanded && this.selectedItem.expand) {
      this.selectedItem.expand({
        parents: true
      });
    }
  }

  noChildItemsExpanded() {
    return !_.some(this.assetModel.items, { 'expanded': true });
  }

  openItem(node) {
    this.select(node);
    this._$state.go('item.attributes', { itemId: node.id });
  }

  /**
   * Filter out items that have a "is related to" relationship as they
   * should not be shown on the hierarchy.
   *
   * @param {object} item - item to filter
   * @return {boolean}
   */
  _filterItems(item) {
    return !_.find(item.related, {
      type: {
        id: this._assetItemRelationshipType.get('isRelatedTo')
      }
    });
  }
}
