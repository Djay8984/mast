
export default () => ({
  restrict: 'AE',
  scope: {
    item: '='
  },
  template: `
    <ul class="node-meta">
      <li>Display Order: {{ item.displayOrder }}</li>
      <li>ID:{{ item.id }}</li>
      <li>Parent:{{ item.parent.id || 'No Parent Item' }}</li>
      <li
        data-ng-if="item.expanded"
        data-ng-class="{active:item.expanded}">
        expanded
      </li>
      <li
        data-ng-if="item.selected"
        data-ng-class="{active:item.selected}">
        selected
      </li>
      <li
        data-ng-if="item.childSelected"
        data-ng-class="{active:item.childSelected}">
        child-selected
      </li>
      <li
        data-ng-if="item.valid"
        data-ng-class="{active:item.valid}">
        valid
      </li>
      <li
        data-ng-if="item.standaloneValid"
        data-ng-class="{active:item.standaloneValid}">
        standaloneValid
      </li>
    </ul>`
});
