import Base from 'app/base.class';
import * as _ from 'lodash';
import lazyLoadMixin from 'app/services/lazy-load-mixin';

export default class HierarchyViewSelectorController
  extends lazyLoadMixin(Base) {
  /* @ngInject */
  constructor(
    assetModelDecisionService,
    assetItemRelationshipType
  ) {
    super(arguments);

    if (!this.itemSelectionValidator) {
      this.itemSelectionValidator = () => true;
    }
    this.assetModel.loaded = true;
    this.assetModel.expanded = true;
  }

  /**
   * Expand function called by the +/- button. The item is loaded if required
   * and if it is selected prior to loading, its children are selected. If the
   * item has already been loaded, the expanded state is simple toggled
   * @param {Item} item the item to expand
   */
  async expand(item) {
    if (!item.loaded) {
      await this.lazyLoad(item);
      item.loaded = true;

      _.forEach(item.items, (child) =>
        _.set(child, 'valid', this.itemSelectionValidator(this.item, child)));

      if (item.selected) {
        this.selectChildren(item.items, item.selected);
      }
    }

    item.expand({ parents: false });
  }

  changeChosen(node) {
    _.unset(this.chosenItem, 'selected');
    this.chosenItem = node;
    _.set(node, 'selected', node.model._id);
  }

  /**
   * @returns {boolean} whether or not any items have been expanded
   */
  noChildItemsExpanded() {
    return !_.some(this.assetModel.items, { 'expanded': true });
  }
}
