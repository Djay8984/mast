import AssetModelHierarchySelectorController
  from './asset-model-hierarchy-selector.controller';

import templateUrl from './asset-model-hierarchy-selector.html';

/* @ngInject */
export default () => ({
  controller: AssetModelHierarchySelectorController,
  controllerAs: 'vm',
  bindToController: {
    selectedItem: '=',
    chosenItem: '=',
    debug: '=',
    asset: '=',
    assetModel: '=',
    item: '=',
    itemSelectionValidator: '=?'
  },
  restrict: 'AE',
  scope: true,
  templateUrl
});
