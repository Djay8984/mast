import AssetModelHierarchyController from './asset-model-hierarchy.controller';
import templateUrl from './asset-model-hierarchy.html';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  bindToController: true,
  controller: AssetModelHierarchyController,
  controllerAs: 'vm',
  scope: {
    assetModel: '=',
    asset: '=',
    debug: '=',
    selectedItem: '='
  },
  templateUrl
});
