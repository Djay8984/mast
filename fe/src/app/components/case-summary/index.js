import * as angular from 'angular';

import caseSummaryDirective from './case-summary.directive';

export default angular
  .module('app.components.caseSummary', [])
  .directive('caseSummary', caseSummaryDirective)
  .name;
