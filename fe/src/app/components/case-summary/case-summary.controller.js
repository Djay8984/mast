import * as _ from 'lodash';

import Base from 'app/base.class';
import Case from 'app/models/case.model';

export default class CaseSummaryController extends Base {
  /* @ngInject */
  constructor(
    $state,
    caseService,
    officeRole,
    surveyorRole
  ) {
    super(arguments);
    this.caseModel = Reflect.construct(Case, [this.case]);
  }

  /**
   * @summary viewCase()
   * Moves the user to the next page to view the case
   */
  async viewCase() {
    const statusId = _.get(this.caseModel, 'caseStatus.id');
    const target = await this._caseService.isOpenStatus(statusId) ?
      'details' : 'milestones';
    this._$state.go(`asset.cases.view.${target}`,
      { caseId: this.caseModel.id },
      { reload: true });
  }

  /**
   * @summary caseOfficer()
   * @param {Object} item The case we're dealing with
   * @param {String} office The office type we're searching by
   * @return {String} name The name of the office type assigned to this case
   */
  caseOffice(item, office) {
    const result = _.filter(item.offices,
      { officeRole: { id: this._officeRole.get(office) } });
    if (result && !_.isEmpty(result)) {
      return _.startCase(_.first(result).office.name);
    }
  }

  /**
   * @summary caseSurveyor()
   * @param {Object} item The case we're dealing with
   * @param {String} surveyor The surveyor type we're searching by
   * @return {String} name The name of the surveyor type assigned to this case
   */
  caseSurveyor(item, surveyor) {
    const result = _.filter(item.surveyors,
      { employeeRole: { id: this._surveyorRole.get(surveyor) } });
    if (result && !_.isEmpty(result)) {
      return _.startCase(_.get(_.first(result).surveyor, 'name'));
    }
  }
}
