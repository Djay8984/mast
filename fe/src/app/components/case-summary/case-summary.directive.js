import templateUrl from './case-summary.html';
import CaseSummaryController from './case-summary.controller';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  bindToController: {
    case: '='
  },
  controller: CaseSummaryController,
  controllerAs: 'vm',
  templateUrl
});
