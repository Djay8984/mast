import controller from './checked-out-by.controller';
import templateUrl from './checked-out-by.html';

/* @ngInject */
export default () => ({
  controller,
  controllerAs: 'vm',
  templateUrl,
  replace: true,
  scope: {
    asset: '='
  },
  bindToController: true
});
