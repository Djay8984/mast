import * as angular from 'angular';
import directive from './checked-out-by.directive';

export default angular
	.module('app.components.checkedOutBy', [])
	.directive('checkedOutBy', directive)
	.name;
