import * as _ from 'lodash';
import Base from 'app/base.class';

export default class CheckedOutByController extends Base {
  /* @ngInject */
  constructor(
    $timeout,
    assetDecisionService,
    assetOnlineService,
    employeeDecisionService
  ) {
    super(arguments);
    this.setup();
  }

  async setup() {
    this.currentUser = await this._employeeDecisionService.getCurrentUser();
    this.isMdsSuperUser = _.includes(this.currentUser.groups, 'mds-superuser');
    const checkedOutAssets = await this.getCheckedOutAssets();

    _.forEach(checkedOutAssets, (coAsset) => {
      if (coAsset.id === this.asset.id) {
        this.asset.checkedOutBy = coAsset.checkedOutBy;
        this.show = coAsset.checkedOutBy !== this.currentUser.fullName;
      }
    });
  }

  async getCheckedOutAssets() {
    let checkedOutAssets = {};
    if (!this.checkedOut) {
      this.checkedOut = true;
      checkedOutAssets = await this._assetOnlineService.checkedOut(
      { all: true });
    }
    return checkedOutAssets.plain();
  }


  async discard() {
    this.loading = true;
    const res = await this._assetOnlineService.discardAsset(this.asset);

    if (res) {
      this.isCheckedOut = false;
      this.loading = false;
    }
  }
}
