import * as angular from 'angular';

import assetImageDirective from './asset-image.directive';

export default angular
  .module('app.components.asset-image ', [])
  .directive('assetImage', assetImageDirective)
  .name;
