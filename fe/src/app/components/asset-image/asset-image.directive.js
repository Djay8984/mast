import * as _ from 'lodash';

/* @ngInject */
export default ($parse) => ({
  restrict: 'AE',
  link: (scope, element, attributes) => {
    // require strings read by reflection, so cannot be generated
    const imageMap = {
      'Tanker': require('content/images/assets/tanker.png'),
      'Bulk Carrier': require('content/images/assets/bulk-carrier.png'),
      'Buoy': require('content/images/assets/buoy.png')
    };

    element.attr('src', _.get(imageMap, $parse(attributes.assetImage)(scope)) ||
        _.get(imageMap, 'Tanker'));
  }
});
