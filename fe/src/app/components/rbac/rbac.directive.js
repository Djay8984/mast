import * as _ from 'lodash';

/* @ngInject */
export default (
  rbacService
) => ({

  // uses a scope watch to check changes to the options passed, allowing it to
  //   handle async loaded options. sets the appropriate properties on for
  //   hiding/disabling if groups aren't empty & rbac is valid
  link: (scope, element, attrs) =>
    scope.$watch('rbac', ({ groups, action, disable }) =>
      ((valid) => disable ?
          element.prop('disabled', !valid) :
          element.css('display', valid ? '' : 'none'))(
        !_.isEmpty(groups) && rbacService.can(groups, action)), true),
  restrict: 'EA',
  scope: {
    rbac: '=rbac'
  }
});

