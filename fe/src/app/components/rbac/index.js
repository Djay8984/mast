import * as angular from 'angular';

import rbacDirective from './rbac.directive';

export default angular
  .module('app.components.rbac', [])
  .directive('rbac', rbacDirective)
  .name;
