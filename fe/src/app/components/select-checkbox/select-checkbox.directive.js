import * as _ from 'lodash';

import templateUrl from './select-checkbox.html';

/* @ngInject */
export default () => ({
  link: (scope, element, attributes, controller) => _.merge(scope, {
    getLabel: (datum) => _.get(datum, scope.labelProperty || 'name'),
    setAll: (which) => _.forEach(scope.values,
      (value) => _.set(value, 'selected', which)),
    updateSelected: (list) => controller.$setViewValue(
      _.map(_.filter(list, { selected: true })),
        (value) => _.omit(value, 'selected'))
  }),
  require: 'ngModel',
  scope: {
    values: '=',
    labelProperty: '=',
    columns: '='
  },
  templateUrl
});
