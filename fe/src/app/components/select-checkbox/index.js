import * as angular from 'angular';

import SelectCheckboxDirective from './select-checkbox.directive';

export default angular.module('app.component.select-checkbox', [])
  .directive('selectCheckbox', SelectCheckboxDirective)
  .name;
