import * as _ from 'lodash';
import Base from 'app/base.class';

export default class LogoAssetHeaderController extends Base {
  /* @ngInject */
  constructor(
    navigationService
  ) {
    super(arguments);
    this.subheaderOpen = _.isUndefined(this.subheaderOpen) ?
      true : this.subheaderOpen;
    this.enabled = _.isUndefined(this.enabled) ?
      true : this.enabled;
  }

  home() {
    this._navigationService.home();
  }
}
