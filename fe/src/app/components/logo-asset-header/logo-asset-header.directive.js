import LogoAssetHeaderController from './logo-asset-header.controller';
import templateUrl from './logo-asset-header.html';

/* @ngInject */
export default () => ({
  bindToController: {
    asset: '=',
    subheaderOpen: '=?',
    enabled: '=?'
  },
  controller: LogoAssetHeaderController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
