import * as angular from 'angular';

import logoAssetHeaderDirective from './logo-asset-header.directive';

export default angular
  .module('app.components.logo-asset-header ', [])
  .directive('logoAssetHeader', logoAssetHeaderDirective)
  .name;
