import * as angular from 'angular';

import assetModelSearchDirective from './asset-model-search.directive';

export default angular
    .module('app.components.asset-model-search', [])
    .directive('assetModelSearch', assetModelSearchDirective)
    .name;
