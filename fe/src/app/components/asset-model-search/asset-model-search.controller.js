import Base from 'app/base.class';
import * as _ from 'lodash';

export default class AssetModelSearchController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    $stateParams,
    assetModelDecisionService
  ) {
    super(arguments);
    this.filters = this._assetModelDecisionService.filters();

    if (this.pager) {
      this.searchItems = [];
      const pagerObj = _.cloneDeep(this.pager.searchItems || []);
      pagerObj.pagination = this.pager.pagination;
      this.pageable = pagerObj;
      this.filters = this.pager.filters;
      this.searchCompleted = true;
    } else {
      this.pager = {};
      this.pager.filters = this.filters;
    }

    //  Loads saved search
    this.assetId = this._$stateParams.assetId;
    const data = this._assetModelDecisionService.savedSearch;
    if (data && data.id === this.assetId &&
      Number(this._$stateParams.searchMode)) {
      _.assignIn(this, data);
    }

    //  Removes search results from service after its loaded
    //  Should be delete, but its not allowed by eslint
    this._assetModelDecisionService.savedSearch = null;
  }

  // -------------------------------------------------
  //  Save search results into service
  save() {
    this._assetModelDecisionService.savedSearch = {
      id: this.searchId,
      pager: this.pager,
      inSearchMode: this.inSearchMode,
      searchCompleted: this.searchCompleted,
      searchItems: this.searchItems,
      filters: this.filters
    };
  }

  async search(filters, searchId) {
    // If searchItem =  * remove it from the search filter
    // before calling service
    _.forEach(filters, (filter) => {
      if (filter.searchTerm === '*') {
        Reflect.deleteProperty(filter, 'searchTerm');
      }
    });

    const searchObject = this.buildSearchObject(filters);
    const assetModel = await this._assetModelDecisionService.search(
      searchObject, searchId);
    this.searchItems = assetModel;
    this.pager.searchItems = this.searchItems;
    this.pager.pagination = this.searchItems.pagination;
    this._assetModelDecisionService.id = searchId;
    this.searchCompleted = true;
  }

  clear() {
    _.forEach(this.filters, (filter) => {
      Reflect.deleteProperty(filter, 'searchTerm');
    });

    this.searchCompleted = false;

    this.searchItems = [];
  }

  changeView(view) {
    view = view || this.activeView;
    const searchMode = 0;
    this._$state.go('asset.assetModel.view', { view, searchMode });
  }

  toggleSearch() {
    this.changeView(false);
  }

  /*
   * builds a search object according to what filters the user has input
   */
  buildSearchObject(filters) {
    const filtersToUse = _.filter(filters, 'searchTerm');
    const searchObject = {};

    _.forEach(filtersToUse, (filter) =>
      _.set(searchObject, filter.value, filter.searchTerm));

    return searchObject;
  }

  get filtersHaveValue() {
    let doesAFilterHasAValue = false;

    _.forEach(this.filters, (filter) => {
      let currentValue = filter.searchTerm;

      if (currentValue) {
        currentValue = currentValue.replace('*', '');
        currentValue = currentValue.trim();
        if (currentValue.length > 0) {
          doesAFilterHasAValue = true;
        }
      }
    });
    return doesAFilterHasAValue;
  }

  openItem(item) {
    this.save();
    this._$state.go('item.attributes', { itemId: item.id, pager: this.pager });
  }
}
