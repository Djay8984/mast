import templateUrl from './asset-model-search.html';

import assetModelSearchController from './asset-model-search.controller';

/* @ngInject */
export default () => ({
  templateUrl,
  scope: true,
  controller: assetModelSearchController,
  controllerAs: 'vm',
  bindToController: {
    searchService: '=',
    searchId: '=',
    inSearchMode: '=',
    searchComplete: '=',
    pager: '='
  },
  restrict: 'AE'
});
