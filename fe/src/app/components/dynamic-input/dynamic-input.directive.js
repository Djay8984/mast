import * as _ from 'lodash';

import templateUrl from './dynamic-input.html';

/* @ngInject */
export default (
  attributeValueType
) => ({
  templateUrl,
  restrict: 'E',
  link(scope) {
    scope.vm.category =
      _.get(_.invert(attributeValueType.toObject()), scope.vm.type, 'text');
  },
  scope: true,
  controller: _.constant(),
  bindToController: {
    ngModel: '=',
    type: '=',
    label: '@?',
    inputId: '@',
    dropdownOptions: '=?',
    dropdownDisplayProperty: '=',
    numHardMin: '=?',
    numHardMax: '=?',
    lengthLimit: '=?',
    ngDisabled: '=?'
  },
  controllerAs: 'vm'
});

/*
1     = date         = datepicker
2     = float        = number
3     = lookup       = dropdown
4     = string       = text
5     = boolean      = checkbox
*/
