import * as angular from 'angular';
import dynamicInputDirective from './dynamic-input.directive';

export default angular
  .module('app.components.dynamic-input', [])
  .directive('dynamicInput', dynamicInputDirective)
  .name;
