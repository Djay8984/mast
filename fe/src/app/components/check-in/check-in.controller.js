import * as _ from 'lodash';

import Base from 'app/base.class';
import BaseForm from 'app/base-form.class';

import { allProperties } from 'app/utils/model';

export default class CheckInController extends Base {
  /* @ngInject */
  constructor(
    $q,
    $state,
    pageService,
    MastModalFactory
  ) {
    super(arguments);

    window.page = this;

    this.list = pageService.checkedOut;

    this.confirmation = {
      checkIn: (asset) => this._MastModalFactory({
        templateReady: 'confirmation',
        scope: {
          title: 'Are you sure',
          message: `You have selected to check in ${asset.name}
This will commit your changes made during this check out session.`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Check In', result: true, class: 'primary-button' }
          ]
        },
        class: 'dialog'
      }).activate(),
      save: (asset) => this._MastModalFactory({
        templateReady: 'confirmation',
        scope: {
          title: 'Unsaved changes',
          message: `You have unsaved changes to the draft asset.
Choose to either save or discard your changes to continue with check in,
or cancel check in`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Discard changes and check in', result: 'discard',
              class: 'primary-button' },
            { name: 'Save and check In', result: 'save',
              class: 'secondary-button' }
          ]
        },
        class: ['dialog', 'unsaved']
      }).activate(),

      discard: (asset) => this._MastModalFactory({
        templateReady: 'confirmation',
        scope: {
          title: 'Are you sure',
          message: `You have selected to discard check out.
This will mean your changes made during check out will be lost.`,
          actions: [
            { name: 'Cancel', result: false },
            { name: 'Discard check out', result: true, class: 'primary-button' }
          ]
        },
        class: 'dialog'
      }).activate()
    };
  }

  openAsset(asset) {
    this._$state.go('asset.assetModel.view', { assetId: asset.id });
  }

  async checkIn(asset) {
    // uses ui router to explore the currently defined views through the
    // annoyingly unenumerable views hierarchy attached to the state, extract
    // their controllers through the scope & check whether they are form
    // controllers, if so, check the dirtiness of their form
    const dirtyFormControllers = _.chain(this._$state.$current.locals)
      .pick(_.filter(
          allProperties(this._$state.$current.locals),
          (key) => _.includes(key, '@')))
      .map((local) => _.get(local, ['$scope', _.get(local, '$$controllerAs')]))
      .filter((controller) => controller instanceof BaseForm)
      .filter('dirty')
      .value();

    if (_.isEmpty(dirtyFormControllers)) {
      const checkIn = await this.confirmation.checkIn(asset);
      if (checkIn) {
        this.checkInAndRefresh(asset);
      }
    } else {
      const saveAndCheckIn = await this.confirmation.save(asset);
      if (saveAndCheckIn === 'save') {
        await this._$q.all(_.invokeMap(dirtyFormControllers, 'save'));
      }
      this.checkInAndRefresh(asset);
    }
  }

  checkInAndRefresh(asset) {
    this._pageService.checkIn(asset);
    this._$state.go(this._$state.current.name,
      { ...this._$state.params },
      { reload: true });
  }

  async discard(asset) {
    const discard = await this.confirmation.discard(asset);
    if (discard) {
      this._pageService.checkIn(asset);
    }
  }
}
