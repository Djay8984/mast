import * as angular from 'angular';
import directive from './check-in.directive';

export default angular
	.module('app.components.checkIn', [])
	.directive('checkIn', directive)
	.name;
