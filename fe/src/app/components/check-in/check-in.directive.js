import controller from './check-in.controller';
import templateUrl from './check-in.html';

/* @ngInject */
export default () => ({
  controller,
  controllerAs: 'checkIn',
  templateUrl,
  replace: true
});
