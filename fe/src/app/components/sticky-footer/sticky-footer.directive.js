import templateUrl from './sticky-footer.html';

/* @ngInject */
export default () => ({
  templateUrl,
  restrict: 'AE',
  transclude: {
  }
});
