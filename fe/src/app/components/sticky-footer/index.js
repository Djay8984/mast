import * as angular from 'angular';
import stickyFooterDirective from './sticky-footer.directive.js';

export default angular
  .module('app.components.sticky-footer', [])
  .directive('stickyFooter', stickyFooterDirective)
  .name;
