import preLoadController from './pre-load.controller';

/* @ngInject */
export default () => ({
  controller: preLoadController,
  controllerAs: 'loader',
  transclude: true,
  bindToController: {
    promise: '&preLoad',
    result: '=?',
    preLoad: '=?data'
  },
  link(scope, element) {
    scope.loader._load();
  }
});
