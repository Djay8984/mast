import * as angular from 'angular';
import preLoad from './pre-load.directive';

export default angular
  .module('app.components.preLoad', [])
  .directive('preLoad', preLoad)
  .name;
