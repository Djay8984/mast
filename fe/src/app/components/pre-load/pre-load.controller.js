import Base from 'app/base.class';
import * as angular from 'angular';
import * as _ from 'lodash';

export default class PreLoadController extends Base {
  /* @ngInject */
  constructor($scope, $element, $transclude, $timeout) {
    super(arguments);

    this.$scope = this._$scope.$new();

    this.$scope.spinner =
      angular.element('<div class="spinner"><i></i><i></i></div>');
  }

  //  ----------------------------------------------------------
  async _load() {
    const data = {
      promise: this.promise,
      refresh: this.refresh.bind(this),
      setLoading: (value) => {
        this.loading = value;
      }
    };
    this.$scope.preLoad = data;
    this.preLoad = data;

    if (_.isFunction(data.promise) && !data.result) {
      let res = data.promise();

      if (res.then) {
        this.loading = true;
        res = await res;
      }

      this.results = res;

      this._$transclude(this.$scope, (clone) => {
        this._$element.append(clone);
      });

      this._$timeout(() => {
        this.loading = false;
      });
    }
  }

  //  ----------------------------------------------------------
  async refresh() {
    this.loading = true;
    this.results = await this.promise();
  }

  //  ----------------------------------------------------------
  get loading() {
    return this.$scope.preLoad.loading;
  }

  //  ----------------------------------------------------------
  set loading(value) {
    this.$scope.preLoad.loading = value;
    this.preloadClass = value;
    this.spinner = value;
  }

  //  ----------------------------------------------------------
  set spinner(value) {
    if (value) {
      this._$element.append(this.$scope.spinner);
    } else {
      this.$scope.spinner.remove();
    }
  }

  //  ----------------------------------------------------------
  set preloadClass(value) {
    if (this.$scope.preLoad.result) {
      if (value) {
        this._$element.addClass('preload');
      } else {
        this._$element.removeClass('preload');
      }
    }
  }

  //  ----------------------------------------------------------
  set results(value) {
    _.set(this.$scope, 'preLoad.result', value);
    _.set(this.$scope, 'result', value);

    this.result = value;
  }
}
