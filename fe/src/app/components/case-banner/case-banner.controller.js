import * as _ from 'lodash';
import * as angular from 'angular';
import Base from 'app/base.class';
import statusChangeDialog from './status-change/status-change.html';
import StatusChangeController from './status-change/status-change.controller';
import errorMessageTemplate from './error-message.html';

export default class CaseBannerController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $state,
    moment,
    caseService,
    MastModalFactory,
    referenceDataService,
    caseStatus,
    officeRole,
    surveyorRole
  ) {
    super(arguments);

    this.init({
      caseStatuses: 'case.caseStatuses',
      caseTypes: 'case.caseTypes'
    });

    const buttons = {
      deleteCase: {
        label: 'Delete case',
        fn: () => this.deleteCase()
      },
      cancelCase: {
        label: 'Cancel case',
        fn: () => this.cancelCase()
      },
      putOnHold: {
        label: 'Put on hold',
        fn: () => this.putOnHold()
      },
      takeOffHold: {
        label: 'Take off hold',
        fn: () => this.takeOffHold()
      },
      closeCase: {
        label: 'Close',
        fn: () => this.closeCase()
      },
      moveToJobPhase: {
        label: 'Move To Job Phase',
        fn: () => this.moveToJobPhase()
      },
      moveToValidateAndUpdate: {
        label: 'Move To Validate and Update',
        fn: () => this.moveToValidateAndUpdate()
      }
    };

    this.statusButtonMatrix = _.zipObject(
      _.values(_.pick(caseStatus.toObject(),
          ['uncommitted', 'populate', 'jobPhase', 'validateAndUpdate',
            'onHold'])),
      [
        [buttons.deleteCase],
        [buttons.putOnHold, buttons.cancelCase, buttons.moveToJobPhase],
        [buttons.putOnHold, buttons.cancelCase,
          buttons.moveToValidateAndUpdate],
        [buttons.putOnHold, buttons.cancelCase, buttons.closeCase],
        [buttons.takeOffHold, buttons.cancelCase]
      ]
    );

    _.set(this, 'attachments.pagination.totalElements', this.attachmentCount);
  }

  async init(referenceDataKeyMap) {
    const data = await this._referenceDataService
      .get(_.values(referenceDataKeyMap));

    _.forEach(referenceDataKeyMap, (value, key) => {
      _.set(this, key, _.get(data, value));
    });
  }

  get isCancelled() {
    return _.isEqual(this.statusId, this._caseStatus.get('cancelled'));
  }

  get statusId() {
    return _.get(this.case, 'caseStatus.id');
  }

  get availableActions() {
    return _.get(this.statusButtonMatrix, this.statusId);
  }

  deleteCase() {
    this.statusChangeDialog({
      title: 'Delete case',
      confirmBtnText: 'Delete case'
    }, this._caseStatus.get('deleted'));
  }

  cancelCase() {
    this.statusChangeDialog({
      title: 'Cancel case',
      confirmBtnText: 'Cancel case'
    }, this._caseStatus.get('cancelled'));
  }

  putOnHold() {
    this.statusChangeDialog({
      title: 'Put on hold',
      customText: 'Add a note to the case to explain the reason why.',
      confirmBtnText: 'Put on hold'
    }, this._caseStatus.get('onHold'));
  }

  takeOffHold() {
    this.statusChangeDialog({
      title: 'Take off hold',
      customText: 'Add a note to the case to explain the reason why.',
      confirmBtnText: 'Take off hold'
    }, this.case.previousCaseStatus.id);
  }

  moveToJobPhase() {
    this.statusChangeDialog({
      title: 'Move To Job Phase',
      confirmBtnText: 'Move To Job Phase'
    }, this._caseStatus.get('jobPhase'));
  }

  moveToValidateAndUpdate() {
    this.statusChangeDialog({
      title: 'Move To Validate and Update',
      confirmBtnText: 'Move To Validate and Update'
    }, this._caseStatus.get('validateAndUpdate'));
  }

  async closeCase() {
    // TODO DRY up creating the payload (same logic used in updateStatus)
    const payload = angular.copy(this.case);
    const caseStatus = _.find(this.caseStatuses,
      { id: this._caseStatus.get('closed') });

    _.set(payload, 'caseStatus', caseStatus);

    payload.caseClosedDate = this._moment().toISOString();

    try {
      await this._caseService.validateCase(payload);
      this.statusChangeDialog({
        title: 'Close case',
        confirmBtnText: 'Close case'
      }, this._caseStatus.get('closed'));
    } catch (e) {
      const modal = new this._MastModalFactory({
        templateUrl: errorMessageTemplate,
        class: 'dialog'
      });
      await modal.activate();
    }
  }

  async statusChangeDialog(modalConfig, statusId) {
    const modal = new this._MastModalFactory({
      controller: StatusChangeController,
      controllerAs: 'vm',
      templateUrl: statusChangeDialog,
      scope: _.pick(modalConfig, [
        'title',
        'customText',
        'confirmBtnText'
      ]),
      inject: {
        statusId
      },
      class: 'dialog'
    });

    try {
      const response = await modal.activate();
      if (response.confirmed) {
        this.updateStatus(statusId, _.get(response, 'statusReason'));
      }
    } catch (e) {
      this._$log.log(e);
    }
  }

  async updateStatus(statusId, statusReason) {
    const caseStatus = _.find(this.caseStatuses, { id: statusId });
    const payload = angular.copy(this.case);
    _.set(payload, 'caseStatus', caseStatus);

    if (!_.isUndefined(statusReason)) {
      _.set(payload, 'statusReason', statusReason);
    }

    if (statusId === this._caseStatus.get('closed') ||
        statusId === this._caseStatus.get('cancelled')) {
      payload.caseClosedDate = this._moment().toISOString();
    }

    try {
      await this._caseService.updateCase(payload);
      this._$state.go(this._$state.current.name, {
        caseId: this.case.id
      }, { reload: true });
    } catch (e) {
      this._$log.log(e);
    }
  }

  convertWord(text) {
    return text.replace(/([A-Z])/g, ' $1')
      .replace(/^./, (word) => word.toUpperCase());
  }

  caseStatusName() {
    const idIndex =
      _.findIndex(_.values(this._caseStatus.toObject()), (index) =>
        index === this.case.caseStatus.id);
    return this.convertWord(_.keys(this._caseStatus.toObject())[idIndex]);
  }

  caseStatusType() {
    if (this.caseTypes) {
      const caseType =
        _.find(this.caseTypes, { id: this.case.caseType.id });
      return caseType.name;
    }
  }

  caseOffice(office) {
    const item = _.filter(this.case.offices, (location) =>
      location.officeRole.id === this._officeRole.get(office));

    if (item && !_.isEmpty(item)) {
      return _.startCase(_.first(item).office.name);
    }
  }

  caseSurveyor(surveyor) {
    const item = _.filter(this.case.surveyors,
      { employeeRole: { id: this._surveyorRole.get(surveyor) } });

    if (item && !_.isEmpty(item)) {
      return _.startCase(_.first(item).surveyor.name);
    }
  }

  back() {
    this._$state.go(
      this._$state.previousState.name,
      this._$state.previousState.params
    );
  }
}
