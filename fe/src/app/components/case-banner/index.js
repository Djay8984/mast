import * as angular from 'angular';

import CaseBannerDirective from './case-banner.directive';

export default angular.module('app.components.case-banner', [])
  .directive('caseBanner', CaseBannerDirective)
  .name;
