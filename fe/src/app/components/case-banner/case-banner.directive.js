import templateUrl from './case-banner.html';
import CaseBannerController from './case-banner.controller';

export default () => ({
  controller: CaseBannerController,
  controllerAs: 'vm',
  bindToController: {
    attachmentPath: '=',
    attachmentCount: '=',
    case: '=',
    collapsible: '='
  },
  scope: true,
  templateUrl
});
