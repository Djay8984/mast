import * as _ from 'lodash';
import Base from 'app/base.class';

export default class CancelCaseController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $modalInstance,
    statusId,
    cancelCaseTextLength,
    caseStatus
  ) {
    super(arguments);
  }

  get statusId() {
    return this._statusId;
  }

  get isCancel() {
    return _.isEqual(this._statusId, this._caseStatus.get('cancelled'));
  }

  get cancelTextMaxLength() {
    return this._cancelCaseTextLength;
  }

  close(confirmed) {
    this._$modalInstance.close({
      confirmed,
      statusReason: _.get(this, 'statusReason')
    });
  }
}
