import * as angular from 'angular';
import * as _ from 'lodash';

import Base from 'app/base.class';
import templateUrl from './partials/modal-test.html';

export default class TestHubController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $state,
    $log,
    moment,
    MastModalFactory,
    assetModelDecisionService,
    referenceDataService,
    vpOptions
  ) {
    super(arguments);

    this.ddSelectOptions = [
      {
        text: 'Option1',
        value: 'Some value',
        someprop: 'Property'
      },
      {
        text: 'Option2',
        value: 'Another value',
        someprop: 'somevalue'
      },
      {
        text: 'Option3',
        value: 'Another value'
      },
      {
        text: 'Option4',
        value: 'option4'
      }
    ];
    this.ddSelectOptions2 = [
      {
        text: 'Option 1 - a Really Really Really Long Value',
        value: 'Some value'
      },
      {
        text: 'Honi Soit Qui Mal Y Pense',
        value: 'Another value'
      }
    ];
    this.ddSelectOptions3 = [
      {
        text: 'Option 17 - Long Day',
        value: 'Some value'
      },
      {
        text: 'HMS Pinnafore',
        value: 'Another value',
        someprop: 'HMS Pinnafore'
      }
    ];
    this.ddSelectSelected = {
      text: 'Please select an option:'
    };
    this.ddSelectSelected2 = {
      text: 'Option 1 - a Really Really Really Long Value',
      value: 'Some value'
    };
    this.ddSelectSelected3 = {
      text: 'Disabled!'
    };

    this.dynamicInputData = [
      {
        model: 'text',
        type: 4
      },
      {
        model: true,
        type: 5,
        label: 'boolean value'
      },
      {
        model: 1,
        type: 2,
        label: 'number value'
      },
      {
        model: { text: 'a', value: 'a' },
        type: 3,
        label: 'dropdown select',
        dropdownOptions: [
          {
            text: 'a',
            value: 'a'
          },
          {
            text: 'b',
            value: 'b'
          },
          {
            text: 'c',
            value: 'c'
          },
          {
            text: 'd',
            value: 'd'
          }
        ]
      }
    ];

    this.dates = {
      today: moment().hour(12).startOf('h'),
      minDate: moment().add(-2, 'M').hour(12).startOf('h'),
      maxDate: moment().add(2, 'M').hour(12).startOf('h')
    };

    this.options = {
      view: 'date',
      format: 'lll',
      maxView: false,
      minView: 'hours'
    };

    this.minDate = this.dates.minDate;
    this.maxDate = this.dates.maxDate;

    this.formats = [
      'MMMM YYYY',
      'DD MMM YYYY',
      'ddd MMM DD YYYY',
      'D MMM YYYY HH:mm',
      'lll'
    ];

    this.views = ['year', 'month', 'date', 'hours', 'minutes'];

    this.callbackState = 'Callback: Not fired';

    this.changeDate = (modelName, newDate) => {
      this.callbackState = 'Callback: Fired';
    };

    this.changeMinMax = (modelName, newValue) => {
      const values = {
        minDate: false,
        maxDate: false
      };

      if (modelName === 'dates.minDate') {
        values.minDate = newValue;
        $scope.$broadcast('pickerUpdate', ['pickerMinDate', 'pickerMinDateDiv',
          'pickerMaxSelector'], values);
        values.maxDate = this.dates.maxDate;
      } else if (modelName === 'dates.maxDate') {
        values.maxDate = newValue;
        $scope.$broadcast('pickerUpdate', ['pickerMaxDate', 'pickerMaxDateDiv',
          'pickerMinSelector'], values);
        values.minDate = this.dates.minDate;
      }

      $scope.$broadcast('pickerUpdate', ['pickerBothDates',
        'pickerBothDatesDiv'], values);
    };

    this.changeData = (type) => {
      const values = {};

      const pickersToUpdate = ['pickerMinDate', 'pickerMaxDate',
        'pickerBothDates', 'pickerMinDateDiv', 'pickerMaxDateDiv',
        'pickerBothDatesDiv', 'pickerRange'];

      switch (type) {
        case 'view':
          values.view = this.options.view;
          break;
        case 'minView':
          values.minView = this.options.minView;
          break;
        case 'maxView':
          values.maxView = this.options.maxView;
          break;
        case 'format':
        default:
          values.format = this.options.format;
          break;
      }

      if (values) {
        $scope.$broadcast('pickerUpdate', pickersToUpdate, values);
      }
    };

    this.checkboxModel = [ false, true, false, true, true, true, true, true ];

    this.vpSelection = [];

    this.setUpAttachments();
  }

  async setUpAttachments() {
    this.loading = true;
    const attachmentRefData =
      await this._referenceDataService.get(['attachment']);
    this.cTypes =
      _.get(attachmentRefData, 'attachment.confidentialityTypes');
    this.loading = false;
  }

  get vpOptions() {
    return _.get(this._vpOptions, 'asset.assetCategories');
  }

  get assetModel() {
    return this._assetModel;
  }

  async loadItems(item) {
    item.items = await this._assetModelDecisionService
      .getItems(5, item.getItemIds());
  }

  modalTest() {
    const test = new this._MastModalFactory({
      templateUrl,
      inject: {
        attributes: this.attributes,
        availableAttributes: this.availableAttributes
      },
      class: 'dialog'
    });

    test
      .activate()
      .then((result) => {
        /* eslint no-console: 0 */

        // console.log(result);
      });
  }

  reset() {
    const original = this.lreg;

    this.lreg = angular.copy(original);
  }
}
