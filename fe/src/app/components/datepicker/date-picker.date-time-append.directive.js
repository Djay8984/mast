export default () => ({
  link: (scope, element) => {
    element.bind('click', () => {
      element.find('input')[0].focus();
    });
  }
});
