import * as _ from 'lodash';
import Base from 'app/base.class';

let firstDay = 0;

export default class datePickerUtils extends Base {
  /* @ngInject */
  constructor(
    moment
  ) {
    super(arguments);
  }

  createNewDate(year, month, day, hour, minute) {
    /* eslint no-bitwise: 0 */
    const utc = Date.UTC(year | 0, month | 0, day | 0, hour | 0, minute | 0);

    return this._moment(utc);
  }

  getVisibleMinutes(m, step) {
    const year = m.year();
    const month = m.month();
    const day = m.date();
    const hour = m.hours();
    const offset = m.utcOffset() / 60;
    const minutes = [];

    let pushedDate = null;

    for (let minute = 0; minute < 60; minute += step) {
      pushedDate = this.createNewDate(year, month, day, hour - offset, minute);
      minutes.push(pushedDate);
    }

    return minutes;
  }

  getVisibleWeeks(m) {
    m = this._moment(m);
    const startYear = m.year();
    const startMonth = m.month();

    // Set date to the first day of the month
    m.date(1);

    const day = m.day();

    // Go back the required number of days to arrive at the previous week start
    m.date(firstDay - (day + (firstDay >= day ? 6 : -1)));

    const weeks = [];

    while (weeks.length < 6) {
      if (m.year() === startYear && m.month() > startMonth) {
        break;
      }
      weeks.push(this.getDaysOfWeek(m));
      m.add(7, 'day');
    }

    return weeks;
  }

  getVisibleYears(d) {
    const m = this._moment(d);

    let year = m.year();

    /* eslint no-extra-parens: 0 */
    m.year(year - (year % 10));
    year = m.year();

    const years = [];

    let offset = m.utcOffset() / 60;
    let pushedDate = null;
    let actualOffset = null;

    for (let i = 0; i < 12; i += 1) {
      pushedDate = this.createNewDate(year, 0, 1, 0 - offset);
      actualOffset = pushedDate.utcOffset() / 60;
      if (actualOffset !== offset) {
        pushedDate = this.createNewDate(year, 0, 1, 0 - actualOffset);
        offset = actualOffset;
      }
      years.push(pushedDate);
      year++;
    }

    return years;
  }

  getDaysOfWeek(m) {
    m = m ? m : this._moment().day(firstDay);

    const year = m.year();
    const month = m.month();
    const offset = m.utcOffset() / 60;
    const days = [];

    let day = m.date();
    let pushedDate = null;
    let actualOffset = null;

    for (let i = 0; i < 7; i += 1) {
      pushedDate = this.createNewDate(year, month, day, 0 - offset, 0, false);
      actualOffset = pushedDate.utcOffset() / 60;
      if (actualOffset !== offset) {
        pushedDate = this.createNewDate(year, month, day, 0 - actualOffset, 0,
          false);
      }
      days.push(pushedDate);
      day++;
    }

    return days;
  }

  getVisibleMonths(m) {
    const year = m.year();
    const offset = m.utcOffset() / 60;
    const months = [];

    let pushedDate = null;
    let actualOffset = null;

    for (let month = 0; month < 12; month += 1) {
      pushedDate = this.createNewDate(year, month, 1, 0 - offset, 0, false);
      actualOffset = pushedDate.utcOffset() / 60;
      if (actualOffset !== offset) {
        pushedDate = this.createNewDate(year, month, 1, 0 - actualOffset, 0,
          false);
      }
      months.push(pushedDate);
    }

    return months;
  }

  getVisibleHours(m) {
    const year = m.year();
    const month = m.month();
    const day = m.date();
    const offset = m.utcOffset() / 60;
    const hours = [];

    let pushedDate = null;
    let actualOffset = null;

    for (let hour = 0; hour < 24; hour += 1) {
      pushedDate = this.createNewDate(year, month, day, hour - offset, 0,
        false);
      actualOffset = pushedDate.utcOffset() / 60;
      if (actualOffset !== offset) {
        pushedDate = this.createNewDate(year, month, day,
          hour - actualOffset, 0, false);
      }
      hours.push(pushedDate);
    }

    return hours;
  }

  isAfter(model, date) {
    return model && model.unix() >= date.unix();
  }

  isBefore(model, date) {
    return model.unix() <= date.unix();
  }

  isSameYear(model, date) {
    return model && model.year() === date.year();
  }

  isSameMonth(model, date) {
    return this.isSameYear(model, date) && model.month() === date.month();
  }

  isSameDay(model, date) {
    return this.isSameMonth(model, date) && model.date() === date.date();
  }

  isSameHour(model, date) {
    return this.isSameDay(model, date) && model.hours() === date.hours();
  }

  isSameMinutes(model, date) {
    return this.isSameHour(model, date) && model.minutes() === date.minutes();
  }

  setParams(fd) {
    firstDay = fd;
  }

  scopeSearch(scope, name, comparisonFn) {
    const nameArray = name.split('.');
    const j = nameArray.length;

    let parentScope = scope;
    let target = null;

    do {
      target = parentScope = parentScope.$parent;

      // Loop through provided names;
      for (let i = 0; i < j; i++) {
        target = target[nameArray[i]];
        if (!target) {
          /* eslint no-continue: 0 */
          continue;
        }
      }

      /* If we reached the end of the list for this scope, and something
       * was found, trigger the comparison function. If the comparison
       * function is happy, return found result. Otherwise, continue to
       * the next parent scope;
       */
      if (target && comparisonFn(target)) {
        return target;
      }
    } while (parentScope.$parent);

    return false;
  }

  findFunction(scope, name) {
    return this.scopeSearch(scope, name, (target) => _.isFunction(target));
  }

  findParam(scope, name) {
    return this.scopeSearch(scope, name, () => true);
  }

  createMoment(m) {
    /* If input is a moment, and we have no TZ info, we need to remove TZ
     * info from the moment, otherwise the newly created moment will take
     * the timezone of the input moment. The easiest way to do that is to
     * take the unix timestamp, and use that to create a new moment.
     * The new moment will use the local timezone of the user machine.
     */
    return this._moment.isMoment(m) ?
      this._moment.unix(m.unix()) :
      this._moment(m);
  }

  getDate(scope, attrs, name) {
    let result = false;

    if (attrs[name]) {
      result = this.createMoment(new Date(attrs[name]));
      if (!result.isValid()) {
        result = this.findParam(scope, attrs[name]);
        if (result) {
          result = this.createMoment(result);
        }
      }
    }

    return result;
  }

  eventIsForPicker(targetIDs, pickerID) {
    /* Checks if an event targeted at a specific picker, via either a string
     * name, or an array of strings.
     */
    return _.includes(targetIDs, pickerID) || targetIDs === pickerID;
  }
}
