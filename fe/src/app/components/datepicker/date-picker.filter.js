/* eslint arrow-body-style: 0 */
/* @ngInject */
export default (moment) => {
  return (m, format) => {
    if (!moment.isMoment(m)) {
      return m ? moment(m).format(format) : '';
    }

    return m.format(format);
  };
};
