import * as angular from 'angular';
import * as _ from 'lodash';

/* @ngInject */
export default (datePickerUtils, datePickerTime, moment, $compile) => ({
  scope: {
    start: '=',
    end: '='
  },
  link: (scope, element, attrs) => {
    let dateChange = null;
    const pickerRangeID = element[0].id;
    const pickerIDs = [randomName(), randomName()];
    const createMoment = datePickerUtils.createMoment;
    const eventIsForPicker = datePickerUtils.eventIsForPicker;

    function getTemplate(templateAttrs, id, model, min, max) {
      return datePickerTime.template(angular.extend(templateAttrs, {
        ngModel: model,
        minDate: min && moment.isMoment(min) ? min.format() : false,
        maxDate: max && moment.isMoment(max) ? max.format() : false
      }), id);
    }

    function randomName() {
      return `picker ${_.random(0, 99)}`;
    }

    scope.dateChange = (modelName, newDate) => {
      // Notify user if callback exists.
      if (dateChange) {
        dateChange(modelName, newDate);
      }
    };

    function setMax(date) {
      scope.$broadcast('pickerUpdate', pickerIDs[0], {
        maxDate: date
      });
    }

    function setMin(date) {
      scope.$broadcast('pickerUpdate', pickerIDs[1], {
        minDate: date
      });
    }

    if (pickerRangeID) {
      scope.$on('pickerUpdate', (event, targetIDs, data) => {
        if (eventIsForPicker(targetIDs, pickerRangeID)) {
          scope.$broadcast('pickerUpdate', pickerIDs, data);
        }
      });
    }

    scope.start = createMoment(scope.start);
    scope.end = createMoment(scope.end);

    scope.$watchGroup(['start', 'end'], (dates) => {
      setMin(dates[0]);
      setMax(dates[1]);
    });

    if (!_.isUndefined(attrs.dateChange)) {
      dateChange = datePickerUtils.findFunction(scope, attrs.dateChange);
    }

    attrs.onSetDate = 'dateChange';

    const template = `<div><table class="date-range"><tr><td valign="top">
      ${getTemplate(attrs, pickerIDs[0], 'start', false, scope.end)}
      </td><td valign="top">
      ${getTemplate(attrs, pickerIDs[1], 'end', scope.start, false)}
      </td></tr></table></div>`;

    const picker = $compile(template)(scope);
    element.append(picker);
  }
});
