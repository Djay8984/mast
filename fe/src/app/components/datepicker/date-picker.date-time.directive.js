import * as angular from 'angular';
import * as _ from 'lodash';
import * as dateUtils from '../../utils/date';

const PRISTINE_CLASS = 'ng-pristine';
const DIRTY_CLASS = 'ng-dirty';

/* @ngInject */
export default (
  datePickerTime,
  $document,
  $filter,
  datePickerUtils,
  $parse,
  $compile,
  moment,
  dateFormat
) => ({
  require: 'ngModel',
  scope: true,

  link: (scope, element, attrs, ngModel) => {
    let picker = null;
    let { dateChange, container } = { };
    let format = attrs.format || dateFormat;
    let shownOnce = false;
    let template = '';

    const body = $document.find('body');
    const dateFilter = $filter('mFormat');
    const parentForm = element.inheritedData('$formController');
    const views = $parse(attrs.views)(scope) || datePickerTime.views.concat();
    const view = attrs.view || views[0];
    const index = views.indexOf(view);
    const dismiss = attrs.autoClose ? $parse(attrs.autoClose)(scope) :
      datePickerTime.autoClose;
    const pickerID = element[0].id;
    const position = attrs.position || datePickerTime.position;
    const eventIsForPicker = datePickerUtils.eventIsForPicker;

    element.attr('placeholder', dateFormat);

    if (index === -1) {
      views.splice(index, 1);
    }

    views.unshift(view);

    function formatter(value) {
      return dateFilter(value, format);
    }

    function parser(viewValue) {
      if (Date.parse(viewValue)) {
        return moment(viewValue);
      }

      /* eslint no-undefined: 0 */
      return viewValue;
    }

    ngModel.$formatters.push(formatter);
    ngModel.$parsers.unshift(parser);

    /* Validators
    *
    *   Note that three separate $validator.* functions are called
    *   independently, and one returned false causes validation to fail.
    *
    *   Each validator has its one error mmessage, so it's important we generate
    *   the correct error, to satisfy UAC.
    *
    */

    function emptyAndNotRequired(value) {
      return _.isEmpty(value) && !attrs.required;
    }


    if (!_.isNil(attrs.minDate)) {
      // If we don't have a min / max value, then any value is valid.
      ngModel.$validators.min = (value) => {
        const minDate = datePickerUtils.findParam(scope, attrs.minDate);
        if (!_.isUndefined(minDate._d)) {
          // force 00:00 UT; works in different local time zones,
          // and resolves as midnight UT.
          minDate.utc().startOf('day');
        }
        return !moment.isMoment(value) || !moment.isMoment(minDate) ||
          (minDate.isSame(value) || minDate.isBefore(value));
      };
    }

    if (!_.isNil(attrs.maxDate)) {
      ngModel.$validators.max = (value) => {
        const maxDate = datePickerUtils.findParam(scope, attrs.maxDate);

        return !moment.isMoment(value) || !moment.isMoment(maxDate) ||
          (maxDate.isSame(value) || maxDate.isAfter(value));
      };
    }

    ngModel.$validators.dateFormat = (value) => {
      if (emptyAndNotRequired(value)) {
        return true;
      }

      if (_.isEmpty(value)) {
        return false;
      }

      // if value is long date string, convert inline to short format
      if (_.isString(value) && _.size(value) !== _.size(dateFormat)) {
        value = moment(value).format(dateFormat);
      }

      // Changed this to be a simple format validator, because the previous
      // moment.format() method was not fit-for-purpose for format validation.
      return dateUtils.hasFormat(value, format);
    };

    ngModel.$validators.isDateLength = (value) => {
      if (emptyAndNotRequired(value) ||
        !_.isObject(value)) { // is a (clean) string
        return true;
      }

      // is a Moment object
      const dateFormatLength = _.size(dateFormat);
      const formattedDate = value.format(dateFormat);

      const returns = _.isEqual(
        _.get(formattedDate, '.length', 0),
        dateFormatLength
      );
      return returns;
    };

    ngModel.$validators.nonExistentDate = (value) => {
      if (emptyAndNotRequired(value)) {
        return true;
      }

      const str = value && _.isObject(value) ? value._i : value;

      if (_.isString(str) && str.split(' ')[0] > 31) {
        /*  Necessary because moment.format() accidentally transforms DD>31
            (in 'DD MMM YYYY') by rolling the days into the next month, which
            would fail the "does the date match a formatted date?" validation
            test in $validators.dateFormat(), which we don't want.

            We want these errors to be caught HERE instead, to give the
            appropriate error message in the UI.
        */
        return false;
      }

      // this method checks if the date entered is
      // actually a date that exists. I.e. 31 April
      // which moment seems to not fire an invalid date on,
      // but moment fires an error on something like 32 April
      const formattedDate = moment(value).format(format);
      const valueAsString = _.toUpper(formattedDate.toString());

      const returns = _.isEmpty(formattedDate) ||
        moment(formattedDate, format, true).isValidDate(value) &&
        valueAsString !== 'INVALID DATE';

      return returns;
    };

    moment.fn.isValidDate = function isValidTest(value) {
      // _d is the JS date and _i is value the moment was constructed with
      // the JS date will rollover to the next month if the original date
      // is invalid - so comparing the momentised date with the original date
      // tells us if it's invalid in the datepicker or not
      return value && !_.isUndefined(value._i) && isNaN(value._i) &&
        !moment(value._i, 'YYYY-MM-DD', true).isValid() ?
        _.isEqual(
          _.toUpper(moment(this._d).format(format)),
          _.toUpper(value._i)
        ) :
        true;
    };

    if (!_.isNil(attrs.dateChange)) {
      dateChange = datePickerUtils.findFunction(scope, attrs.dateChange);
    }

    function getTemplate() {
      template = datePickerTime.template(attrs);
    }

    function updateInput(event) {
      event.stopPropagation();
      if (ngModel.$pristine) {
        ngModel.$dirty = true;
        ngModel.$pristine = false;
        element.removeClass(PRISTINE_CLASS).addClass(DIRTY_CLASS);
        if (parentForm) {
          parentForm.$setDirty();
        }
        ngModel.$render();
      }
    }

    function clear() {
      if (picker) {
        picker.remove();
        picker = null;
      }

      if (container) {
        container.remove();
        container = null;
      }
    }

    if (pickerID) {
      scope.$on('pickerUpdate', (event, pickerIDs, data) => {
        if (eventIsForPicker(pickerIDs, pickerID)) {
          if (picker) {
            /* Need to handle situation where the data changed but the
             * picker is currently open. To handle this, we can create the
             * inner picker with a random ID, then forward any events
             * received to it.
             */
          } else {
            let validateRequired = false;

            if (!_.isNil(data.minDate)) {
              attrs.minDate = data.minDate;
              validateRequired = true;
            }

            if (!_.isNil(data.maxDate)) {
              attrs.maxDate = data.maxDate;
              validateRequired = true;
            }

            if (!_.isNil(data.minView)) {
              attrs.minView = data.minView;
            }

            if (!_.isNil(data.maxView)) {
              attrs.maxView = data.maxView;
            }

            attrs.view = data.view || attrs.view;

            if (validateRequired) {
              ngModel.$validate();
            }

            if (!_.isNil(data.format)) {
              format = attrs.format = data.format || dateFormat;
              ngModel.$modelValue = -1;
            }

            getTemplate();
          }
        }
      });
    }

    function showPicker() {
      if (picker) {
        return;
      }
      picker = $compile(template)(scope);
      scope.$digest();

      if (!shownOnce) {
        scope.$on('setDate', (event, date, thisView) => {
          updateInput(event);
          if (dateChange) {
            dateChange(attrs.ngModel, date);
          }
          if (dismiss && views[views.length - 1] === thisView) {
            clear();
          }
        });

        scope.$on('hidePicker', () => {
          element.triggerHandler('blur');
        });

        scope.$on('$destroy', clear);

        shownOnce = true;
      }

      if (position === 'absolute') {
        const pos = element[0].getBoundingClientRect();
        const height = pos.height || element[0].offsetHeight;
        const postop = pos.top + document.body.scrollTop + height;
        picker.css({ top: `${postop}px`,
          left: `${pos.left}px`,
          display: 'block',
          position });
        body.append(picker);
      } else {
        container = angular.element('<div date-picker-wrapper></div>');
        element[0].parentElement.insertBefore(container[0], element[0]);
        container.append(picker);
        picker.css({ top: `{element[0].offsetHeight}px`, display: 'block' });
      }
      picker.bind('mousedown', (evt) => {
        evt.preventDefault();
      });
    }

    element.bind('focus', showPicker);
    element.bind('blur', clear);
    getTemplate();
  }
});
