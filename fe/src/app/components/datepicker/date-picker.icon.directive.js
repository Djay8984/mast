/* @ngInject */
export default ($document) => ({
  restrict: 'A',
  scope: {
    siblingId: '@'
  },
  link: (scope, element, attrs) => {
    let focusedElement = null;
    element.bind('mousedown', (e) => {
      focusedElement = $document[0].activeElement.id;
    });
    element.bind('click', (e) => {
      if (focusedElement !== scope.siblingId) {
        $document[0].getElementById(scope.siblingId).focus();
      }
    });
  }
});
