/* datePickerTime */
import * as _ from 'lodash';

export default {
  template(attrs, id) {
    const expected = [
      'ngModel', 'minDate', 'maxDate',
      'view', 'minView', 'format', 'autoClose', 'onSetDate', 'firstDay',
      'autoClose', 'partial', 'template', 'step'
    ];
    const attributes = expected.reduce((all, attr) =>
      all += _.has(attrs, attr) ? `${_.kebabCase(attr)}="${attrs[attr]}"\n` : ''
    , id ? `id="${id}"` : '');

    return `<div
      date-picker="${attrs.ngModel}"
      class="date-picker-date-time"
      ${attributes}></div>`;
  },
  views: ['date', 'year', 'month', 'hours', 'minutes'],
  autoClose: true,
  position: 'absolute'
};
