import * as _ from 'lodash';
import templateUrl from './partials/datepicker-template.html';

/* @ngInject */
export default (
  datePickerConfig,
  datePickerUtils,
  moment
) => ({
  require: '?ngModel',
  templateUrl,
  scope: {
    model: '=datePicker',
    after: '=?',
    before: '=?'
  },
  link: (scope, element, attrs, ngModel) => {
    function prepareViews() {
      scope.views = [].concat(datePickerConfig.views);
      scope.view = attrs.view || datePickerConfig.view;

      scope.views = scope.views.slice(
        scope.views.indexOf(attrs.maxView || 'year'),
        scope.views.indexOf(attrs.minView || 'minutes') + 1
      );

      if (scope.views.length === 1 || scope.views.indexOf(scope.view) === -1) {
        scope.view = scope.views[0];
      }
    }

    function getDate(name) {
      return datePickerUtils.getDate(scope, attrs, name);
    }

    let arrowClick = false;
    let minDate = getDate('minDate');
    let maxDate = getDate('maxDate');

    const createMoment = datePickerUtils.createMoment.bind(datePickerUtils);
    const eventIsForPicker =
      datePickerUtils.eventIsForPicker.bind(datePickerUtils);
    const step = parseInt(attrs.step || datePickerConfig.step, 10);
    const partial = Boolean(attrs.partial);
    const pickerID = element[0].id;
    const now = scope.now = createMoment();
    const selected = scope.date = createMoment(scope.model || now);
    const autoclose = attrs.autoClose === 'true';
    const firstDay = attrs.firstDay && attrs.firstDay >= 0 &&
      attrs.firstDay <= 6 ? parseInt(attrs.firstDay, 10) :
      moment().weekday(0).day();

    datePickerUtils.setParams(firstDay);

    if (!scope.model) {
      selected.minute(Math.ceil(selected.minute() / step) * step).second(0);
    }

    scope.template = attrs.template || datePickerConfig.template;

    /* eslint no-undefined: 0 */
    scope.watchDirectChanges = !_.isUndefined(attrs.watchDirectChanges);
    scope.callbackOnSetDate = attrs.dateChange ?
      datePickerUtils.findFunction(scope, attrs.dateChange) :
      null;

    prepareViews();

    scope.setView = (nextView) => {
      if (scope.views.indexOf(nextView) !== -1) {
        scope.view = nextView;
      }
    };

    scope.selectDate = (date) => {
      if (attrs.disabled) {
        return false;
      }

      if (isSame(scope.date, date)) {
        date = scope.date;
      }

      date = clipDate(date);
      if (!date) {
        return false;
      }

      scope.date = date;

      const nextView = scope.views[scope.views.indexOf(scope.view) + 1];

      /* eslint no-extra-parens: 0 */
      if ((!nextView || partial) || scope.model) {
        setDate(date);
      }

      if (nextView) {
        scope.setView(nextView);
      } else if (autoclose) {
        element.addClass('hidden');
        scope.$emit('hidePicker');
        document.activeElement.blur();
      } else {
        prepareViewData();
      }
    };

    function setDate(date) {
      if (date) {
        // If the user only want to pick the date (and not hours/minutes), force
        // the hour to noon to account for Moment's BST -> GMT conversion and
        // any other random differences in time
        if (attrs.minView === 'date') {
          date.hour(12);
        }

        scope.model = date;
        if (ngModel) {
          ngModel.$setViewValue(date);
        }
      }

      scope.$emit('setDate', scope.model, scope.view);

      if (scope.callbackOnSetDate) {
        scope.callbackOnSetDate(attrs.datePicker, scope.date);
      }
    }

    function update() {
      const view = scope.view;

      datePickerUtils.setParams(firstDay);

      if (scope.model && !arrowClick) {
        scope.date = createMoment(scope.model);
        arrowClick = false;
      }

      const date = scope.date;

      switch (view) {
        case 'year':
          scope.years = datePickerUtils.getVisibleYears(date);
          break;
        case 'month':
          scope.months = datePickerUtils.getVisibleMonths(date);
          break;
        case 'date':
          scope.weekdays = scope.weekdays || datePickerUtils.getDaysOfWeek();
          scope.weeks = datePickerUtils.getVisibleWeeks(date);
          break;
        case 'hours':
          scope.hours = datePickerUtils.getVisibleHours(date);
          break;
        case 'minutes':
        default:
          scope.minutes = datePickerUtils.getVisibleMinutes(date, step);
          break;
      }

      prepareViewData();
    }

    function watch() {
      if (scope.view !== 'date') {
        return scope.view;
      }

      return scope.date ? scope.date.month() : null;
    }

    scope.$watch(watch, update);

    if (scope.watchDirectChanges) {
      scope.$watch('model', () => {
        arrowClick = false;
        update();
      });
    }

    /* eslint complexity: 0 */
    function prepareViewData() {
      const view = scope.view;
      const date = scope.date;
      const classes = [];

      let classList = null;

      datePickerUtils.setParams(firstDay);

      if (view === 'date') {
        const weeks = scope.weeks;

        let week = null;

        for (let i = 0; i < weeks.length; i += 1) {
          week = weeks[i];
          classes.push([]);
          for (let j = 0; j < week.length; j += 1) {
            classList = '';
            if (datePickerUtils.isSameDay(date, week[j])) {
              classList += 'active';
            }
            if (isNow(week[j], view)) {
              classList += ' now';
            }
            if (week[j].month() !== date.month() || !inValidRange(week[j])) {
              classList += ' disabled';
            }
            classes[i].push(classList);
          }
        }
      } else {
        const params = datePickerConfig.viewConfig[view];
        const dates = scope[params[0]];
        const compareFunc = params[1];

        for (let i = 0; i < dates.length; i += 1) {
          classList = '';
          if (datePickerUtils[compareFunc](date, dates[i])) {
            classList += 'active';
          }
          if (isNow(dates[i], view)) {
            classList += ' now';
          }
          if (!inValidRange(dates[i])) {
            classList += ' disabled';
          }
          classes.push(classList);
        }
      }
      scope.classes = classes;
    }

    scope.next = (delta) => {
      let date = moment(scope.date);

      delta = delta || 1;
      switch (scope.view) {
        case 'year':
        case 'month':
          date.year(date.year() + delta);
          break;
        case 'date':
          date.month(date.month() + delta);
          break;
        case 'hours':
        case 'minutes':
        default:
          date.hours(date.hours() + delta);
          break;
      }
      date = clipDate(date);
      if (date) {
        scope.date = date;
        setDate(date);
        arrowClick = true;
        update();
      }
    };

    function inValidRange(date) {
      let valid = true;

      if (moment.isMoment(minDate) && minDate.isAfter(date)) {
        valid = isSame(minDate, date);
      }

      if (moment.isMoment(maxDate) && maxDate.isBefore(date)) {
        valid &= isSame(maxDate, date);
      }

      return valid;
    }

    function isSame(date1, date2) {
      return date1.isSame(date2, datePickerConfig.momentNames[scope.view]);
    }

    function clipDate(date) {
      if (moment.isMoment(minDate) && minDate.isAfter(date)) {
        return minDate;
      }

      if (moment.isMoment(maxDate) && maxDate.isBefore(date)) {
        return maxDate;
      }

      return date;
    }

    function isNow(date, view) {
      let is = true;

      /* eslint no-bitwise: 0 no-fallthrough: 0 */
      switch (view) {
        case 'minutes':
          is &= ~~(now.minutes() / step) === ~~(date.minutes() / step);
        case 'hours':
          is &= now.hours() === date.hours();
        case 'date':
          is &= now.date() === date.date();
        case 'month':
          is &= now.month() === date.month();
        case 'year':
        default:
          is &= now.year() === date.year();
      }
      return is;
    }

    scope.prev = (delta) => scope.next(-delta || -1);

    if (pickerID) {
      scope.$on('pickerUpdate', (event, pickerIDs, data) => {
        if (eventIsForPicker(pickerIDs, pickerID)) {
          let updateViews = false;
          let updateViewData = false;

          if (!_.isNull(data.minDate) && !_.isUndefined(data.minDate)) {
            minDate = data.minDate ? data.minDate : false;
            updateViewData = true;
          }

          if (!_.isNull(data.maxDate) && !_.isUndefined(data.maxDate)) {
            maxDate = data.maxDate ? data.maxDate : false;
            updateViewData = true;
          }

          if (!_.isNull(data.minView) && !_.isUndefined(data.minView)) {
            attrs.minView = data.minView;
            updateViews = true;
          }

          if (!_.isNull(data.maxview) && !_.isUndefined(data.maxView)) {
            attrs.maxView = data.maxView;
            updateViews = true;
          }

          attrs.view = data.view || attrs.view;

          if (updateViews) {
            prepareViews();
          }

          if (updateViewData) {
            update();
          }
        }
      });
    }
  }
});
