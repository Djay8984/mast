import * as angular from 'angular';

import datepicker from './date-picker.directive';
import dateRange from './date-picker.range.directive';
import dateTime from './date-picker.date-time.directive';
import dateTimeAppend from './date-picker.date-time-append.directive';
import datePickerIcon from './date-picker.icon.directive';
import datePickerUtils from './date-picker.utils.service';
import datePickerConfig from './date-picker-config.constant';
import datePickerTime from './date-picker.date-time.constant';
import datePickerFilter from './date-picker.filter';

export default angular
  .module('app.components.datepicker', [])
  .directive('datePicker', datepicker)
  .directive('dateRange', dateRange)
  .directive('dateTime', dateTime)
  .directive('dateTimeAppend', dateTimeAppend)
  .directive('datePickerIcon', datePickerIcon)
  .service('datePickerUtils', datePickerUtils)
  .constant('datePickerConfig', datePickerConfig)
  .constant('datePickerTime', datePickerTime)
  .filter('mFormat', datePickerFilter)
  .name;
