import * as _ from 'lodash';
import Base from 'app/base.class';

/*
 * Number of items to display
 * @type {number}
 */
const NUM_ITEMS = 3;

export default class JobReportCarouselController extends Base {
  /* @ngInject */
  constructor(
    $state,
    employeeDecisionService,
    jobDecisionService,
    reportType,
    surveyorRole
  ) {
    super(arguments);
    this.setAdjustedIndex();
    this.init();
  }

  async init() {
    await this.currentJob();
    await this.currentUser();
  }

  async currentJob() {
    if (!this._currentJob && _.get(this.job, 'id')) {
      this._currentJob = await this._jobDecisionService.get(this.job.id);
    }
    return this._currentJob;
  }

  async currentUser() {
    if (!this._currentUser) {
      this._currentUser = await this._employeeDecisionService.getCurrentUser();
    }

    return this._currentUser;
  }

  /**
   * @returns {Number} cells The minimum number of cells to user for carousel
   */
  get minimumCells() {
    return `${_.size(this.visibleItems)}`;
  }

  /**
   * @description Checks the jobs employees to ensure that the user viewing the
   *   page is the lead surveyor, so they click the approve/reject buttons;
   * @returns {Boolean} true or false
   */
  get isAuthorisingSurveyor() {
    // Get the surveyors for the job, and find the authorising surveyor;
    if (this._currentJob && this._currentUser) {
      const surveyors =
        _.find(this._currentJob.employees,
          { employeeRole:
            { id: this._surveyorRole.get('authorisingSurveyor') } });

      // Are we, the user, assigned as authorising surveyor for the job?
      return _.isNil(surveyors) ? false : _.isEqual(this._currentUser.id,
        _.get(surveyors, 'lrEmployee.id', null));
    }
  }

  /**
   * @summary isReportPending()
   * @param {Object} item The item on the carousel
   * @description For the Technical Review carousel, this needs to check if
   *   the report the buttons are 'attached' to is a DSR and isn't rejected;
   * @return {Boolean} true or false
   */
  isReportPending(item) {
    // If 'approved' is false, or true, we don't want the buttons;
    if (this.review && _.isNil(_.get(item, 'approved'))) {
      return true;
    }
    return false;
  }

  /**
   * @summary setAdjustedIndex()
   */
  setAdjustedIndex() {
    const defaultIndex = 0;
    const index = _.size(this.items) - this.numberOfItems;
    this._index = _.size(this.items) < this.numberOfItems ?
      defaultIndex : index;
  }

  /**
   * @returns {number} the current index + the total items to display
   */
  get adjustedIndex() {
    return _.parseInt(this._index) + _.parseInt(this.numberOfItems);
  }

  /**
   * @returns {Number} items The number of items in the carousel
   */
  get numberOfItems() {
    return this.numItems ? this.numItems : NUM_ITEMS;
  }

  /**
   * @param {number} dir - the direction which the user is navigating
   */
  navigate(dir) {
    // do nothing if we're at the end of the item list
    if (dir === 1 && _.size(this.items) <= this.adjustedIndex) {
      return;
    }

    // do nothing if we're already at zero index
    if (dir === -1 && !this._index) {
      return;
    }

    this._index += dir;
  }

  /**
   * @returns {array} a subset of the available items
   */
  get visibleItems() {
    return _.slice(this.items, this._index, this.adjustedIndex);
  }
}
