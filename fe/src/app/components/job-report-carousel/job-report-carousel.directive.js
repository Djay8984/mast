import templateUrl from './job-report-carousel.html';
import JobReportCarouselController from './job-report-carousel.controller';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  scope: true,
  bindToController: {
    items: '=',
    itemsClickable: '@?',
    job: '=',
    numItems: '@?',
    review: '@?',
    action: '&?'
  },
  controller: JobReportCarouselController,
  controllerAs: 'fm',
  templateUrl
});
