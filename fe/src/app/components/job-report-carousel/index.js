import * as angular from 'angular';

import jobReportCarousel from './job-report-carousel.directive';

export default angular
  .module('app.components.job-report-carousel', [])
  .directive('jobReportCarousel', jobReportCarousel)
  .name;
