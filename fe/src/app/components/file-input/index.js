import * as angular from 'angular';
import fileInputDirective from './file-input.directive';

export default angular
  .module('app.components.file-input', [])
  .directive('fileInput', fileInputDirective)
  .name;
