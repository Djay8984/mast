import * as _ from 'lodash';

export default () => ({
  restrict: 'AE',
  require: 'ngModel',
  controllerAs: 'vm',
  link(scope, element, attrs, ngModel) {
    element.bind('change', () => {
      const files = Array.from(_.first(element).files);
      const current = ngModel.$isEmpty(ngModel.$viewValue) ?
        [] : ngModel.$viewValue;
      files.map(scope.vm.attachNewDocument, scope.vm);
      ngModel.$setViewValue(current.concat(files));
    });
  }
});
