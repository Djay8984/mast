import InfoHeaderController from './info-header.controller';
import templateUrl from './info-header.html';

/* @ngInject */

export default () => ({
  bindToController: {
    user: '='
  },
  controller: InfoHeaderController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
