import * as angular from 'angular';

import infoHeaderDirective from './info-header.directive';

export default angular
  .module('app.components.info-header ', [])
  .directive('infoHeader', infoHeaderDirective)
  .name;
