import * as angular from 'angular';
import sortByDirective from './sort-by.directive';

export default angular
  .module('app.components.sort-by', [])
  .directive('sortBy', sortByDirective)
  .name;
