import * as _ from 'lodash';
import Base from 'app/base.class';

export default class SortByController extends Base {
  /* @ngInject */
  constructor($stateParams, $timeout) {
    super(arguments);

    if (!this.sortable.sortable) {
      throw new Error('Collection is not sortable');
    }

    if (!this.sortOrder) {
      this.sortOrder = _.cloneDeep(this.sortable.sortedBy);
    }
  }

  async sortItems() {
    // This is only here to cope with states which store the sort order
    // on the $stateParams, i.e. the dashboard so it can interop with
    // the filter. The timeout is there as ngChange fires before ngModel
    // is updated.
    if (this.hasUpdate) {
      return this._$timeout(this.onUpdate);
    }

    this.loading = true;

    const { sort, order } = this.sortOrder;
    const resp = await this.sortable.sortBy(sort, order);

    this.sortable.splice(0, this.sortable.length, ...resp);
    this.sortable.pagination = resp.pagination;
    this.sortable.updated = true;
    this.loading = false;
  }

  trackBy(item) {
    const obj = item.value || item;

    return `${obj.sort}-${obj.order}`.toLowerCase();
  }

  get sortOptions() {
    if (!this._sortOptions) {
      this._sortOptions = this.sortable.sortFields.reduce((opts, field) => {
        const { title = field, asc = 'Ascending', desc = 'Descending' } =
          this.options[field] || {};

        return opts.concat([
          {
            value: { sort: field, order: 'asc' },
            text: `${title} ${asc}`
          },
          {
            value: { sort: field, order: 'desc' },
            text: `${title} ${desc}`
          }
        ]);
      }, []);
    }

    return this._sortOptions;
  }
}
