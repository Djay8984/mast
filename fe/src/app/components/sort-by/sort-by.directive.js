import controller from './sort-by.controller';
import templateUrl from './sort-by.html';

export default () => ({
  restrict: 'AE',
  templateUrl,
  controller,
  controllerAs: 'vm',
  scope: true,
  bindToController: {
    sortable: '=',
    sortOrder: '=?',
    options: '@?sortOptions',
    onUpdate: '&?'
  },
  link: {
    pre(scope, element, attrs) {
      scope.vm.options = scope.$eval(attrs.sortOptions) || {};
      scope.vm.hasUpdate = Boolean(attrs.onUpdate);
    }
  }
});
