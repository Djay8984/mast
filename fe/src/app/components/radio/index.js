import * as angular from 'angular';
import radio from './radio.directive';
import radioConfig from './radio.constant';

export default angular
    .module('app.components.radio', [])
    .directive('btnRadio', radio)
    .constant('radioButtonConfig', radioConfig)
    .name;
