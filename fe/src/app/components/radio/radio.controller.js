import Base from 'app/base.class';

export default class RadioButtonGroupController extends Base {
  /* @ngInject */
  constructor($scope, radioButtonConfig) {
    super(arguments);
    this.activeClass = radioButtonConfig.activeClass;
    this.toggleEvent = radioButtonConfig.toggleEvent;
  }
}
