import radioController from './radio.controller';
import * as angular from 'angular';

/* @ngInject */
export default () => ({
  require: ['btnRadio', 'ngModel'],
  controller: radioController,
  controllerAs: 'vm',
  link: (scope, element, attrs, ctrls) => {
    const buttonsCtrl = ctrls[0];
    const ngModelCtrl = ctrls[1];

    // model -> UI
    ngModelCtrl.$render = () => {
      if (ngModelCtrl.$modelValue) {
        element.toggleClass(
            buttonsCtrl.activeClass,
            angular.equals(
              ngModelCtrl.$modelValue,
              scope.$eval(attrs.btnRadio)));
      }
    };

    // UI -> model
    element.bind(buttonsCtrl.toggleEvent, () => {
      if (!element.hasClass(buttonsCtrl.activeClass)) {
        scope.$apply(() => {
          ngModelCtrl.$setViewValue(scope.$eval(attrs.btnRadio));
          ngModelCtrl.$render();
        });
      }
    });
  }

});
