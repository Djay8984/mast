import * as angular from 'angular';
import stateLoader from './state-loader.directive';

export default angular
  .module('app.components.state-loader', [])
  .directive('stateLoader', stateLoader)
  .name;
