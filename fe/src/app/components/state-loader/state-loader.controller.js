import Base from 'app/base.class';
import * as _ from 'lodash';

export default class StateLoaderController extends Base {
  /* @ngInject */
  constructor($scope, $state) {
    super(arguments);

    this.loaded = true;
    this.viewsToLoad = {};

    $scope.$on('$stateChangeStart', this.onChange.bind(this));
    $scope.$on('$stateChangeError', this.onError.bind(this));
    $scope.$on('$viewContentLoaded', this.onSuccess.bind(this));
  }
  onChange() {
    this.viewsToLoad = _.mapValues(this._$state.$current.views, () => false);
    this.loaded = false;
  }
  onSuccess(event, viewName) {
    this.viewsToLoad[viewName] = true;

    if (this.isLoadComplete()) {
      this.loaded = true;
    }
  }
  onError() {
    this.loaded = true;
  }
  isLoadComplete() {
    return _.values(this.viewsToLoad).every(() => true);
  }
}
