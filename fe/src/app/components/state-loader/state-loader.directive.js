import StateLoaderController from './state-loader.controller.js';
import templateUrl from './state-loader.html';

/* @ngInject */
export default ($rootScope) => ({
  restrict: 'AE',
  transclude: true,
  scope: true,
  bindToController: {
    offsetTopPixels: '='
  },
  controller: StateLoaderController,
  controllerAs: 'vm',
  templateUrl
});
