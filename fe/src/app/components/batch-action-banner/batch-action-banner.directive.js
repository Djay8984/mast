import templateUrl from './batch-action-banner.html';
import batchActionBannerController from './batch-action-banner.controller';

/* @ngInject */
export default () => ({
  bindToController: {

  },
  controller: batchActionBannerController,
  controllerAs: 'vm',
  restrict: 'AE',
  templateUrl
});
