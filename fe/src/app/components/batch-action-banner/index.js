import * as angular from 'angular';

import batchActionBannerDirective from './batch-action-banner.directive';

export default angular
  .module('app.components.batchActionBanner', [])
  .directive('batchActionBanner', batchActionBannerDirective)
  .name;
