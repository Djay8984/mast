import * as _ from 'lodash';

import Base from 'app/base.class';

export default class BatchActionBannerController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    MastModalFactory
  ) {
    super(arguments);
  }

  async back() {
    this._$state.go(_.get(this._$stateParams, 'from', 'dashboard.myWork'));
  }

  applyCheck() {
    return this._$state.current.name === 'batch-actions.apply';
  }
}
