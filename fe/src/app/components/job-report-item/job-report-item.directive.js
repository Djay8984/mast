import templateUrl from './job-report-item.html';
import JobReportItemController from './job-report-item.controller';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  scope: true,
  bindToController: {
    item: '=',
    itemClickable: '@?',
    review: '@?'
  },
  controller: JobReportItemController,
  controllerAs: 'vm',
  templateUrl
});
