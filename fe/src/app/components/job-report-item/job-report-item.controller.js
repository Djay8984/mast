import * as _ from 'lodash';
import Base from 'app/base.class';

import rejectionReasonModalTemplate from
  '../../asset/jobs/view/reporting/rejection-reason-modal.html';

export default class JobReportItemController extends Base {
  /* @ngInject */
  constructor(
    $state,
    MastModalFactory,
    moment,
    dateFormat,
    employeeDecisionService,
    reportType
  ) {
    super(arguments);
    this._reportTypes = this._reportType.toObject();

    // Get the user information for this particular report;
    this.setupData();
  }

  setupData() {
    this.obtainUser(this.item.model);
  }

  /**
   * @returns {String} the report type ie. 'dar'
   */
  get reportType() {
    return _.findKey(this._reportTypes, (id) =>
      id === _.get(this.item.reportType, 'id'));
  }

  /**
   * @summary obtainUser(item)
   * @param {Object} item The report
   * @description If there is a report that is rejected (or approved) in the
   *   carousel then we need to extract the user's info from the report
   */
  async obtainUser(item) {
    if (!this._reportUser) {
      this._reportUser =
        await this._employeeDecisionService.get(item.authorisedBy.id);
    }
  }

  get reportUser() {
    return this._reportUser ? _.get(this._reportUser, 'name') : '';
  }

  /**
   * @summary reportDate()
   * @returns {String} date Either the submission date (for non-approved or
   *   non-rejected reports) or the approval/rejection date;
   */
  get reportDate() {
    const authDate = this.item.model.authorisationDate;
    const issueDate = this.item.model.issueDate;
    return (!_.isNil(authDate) ? this._moment(authDate) :
      this._moment(issueDate)).format(this._dateFormat);
  }

  /**
   * @summary reportUserInfo()
   * @return {String} user The user's name
   */
  get reportUserInfo() {
    return `${this.reportUser}`;
  }

  /**
   * @summary rejectionReason()
   * @return {String} reason The reason the report was rejected
   */
  get rejectionReason() {
    return this.item.model.rejectionComments;
  }

  async generateRejectionModal() {
    await this._MastModalFactory({
      scope: {
        title: `Rejected by ${this.reportUser}`,
        message: this.rejectionReason,
        actions: [
          { name: 'Ok', result: true }
        ]
      },
      templateUrl: rejectionReasonModalTemplate
    }).activate();
  }

  /**
   * @param {number} item - id of the report to view
   */
  viewReport(item) {
    if (this.itemClickable !== 'false') {
      this._$state.go('reporting.view', {
        jobId: _.get(item, 'job.id'),
        reportId: _.get(item, 'id'),
        reportTypeId: _.get(item, 'reportType.id')
      });
    }
  }
}
