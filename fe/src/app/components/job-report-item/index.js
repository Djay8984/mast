import * as angular from 'angular';

import jobReportItem from './job-report-item.directive';

export default angular
  .module('app.components.job-report-item', [])
  .directive('jobReportItem', jobReportItem)
  .name;
