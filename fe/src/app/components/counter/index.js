import * as angular from 'angular';

import counterDirective from './counter.directive';

export default angular
  .module('app.components.counter', [])
  .directive('counter', counterDirective)
  .name;
