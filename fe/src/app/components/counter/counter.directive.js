import templateUrl from './counter.html';

export default () => ({
  templateUrl,
  restrict: 'E',
  scope: {
    ngDisabled: '=',
    value: '=',
    min: '=?',
    max: '=?',
    inputId: '@'
  },
  link(scope) {
    scope.min = scope.min || 0;
  }
});
