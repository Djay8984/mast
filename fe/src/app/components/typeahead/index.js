import * as angular from 'angular';

import bindHtmlUnsafe from './bind-html-unsafe';
import position from './position';
import typeaheadParser from './typeahead-parser';
import typeaheadPopup from './typeahead-popup';
import typeaheadMatch from './typeahead-match';
import typeaheadHighlight from './typeahead-highlight';

import typeaheadDirective from './typeahead.directive';

export default angular
  .module('app.components.typeahead', [
    bindHtmlUnsafe,
    position,
    typeaheadHighlight,
    typeaheadParser,
    typeaheadPopup,
    typeaheadMatch
  ])
  .directive('typeahead', typeaheadDirective)
  .name;
