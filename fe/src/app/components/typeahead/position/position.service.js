import * as angular from 'angular';

import Base from 'app/base.class';

import * as _ from 'lodash';

export default class Position extends Base {
  /* @ngInject */
  constructor(
    $document,
    $window
  ) {
    super(arguments);
  }

  getStyle(element, cssprop) {
    return this._$window.getComputedStyle ?
      this._$window.getComputedStyle(element)[cssprop] :
      element.style[cssprop];
  }

  /**
   * Checks if a given element is statically positioned
   * @param {object} element raw DOM element
   * @return {boolean} whether it's statically positioned or not
   */
  isStaticPositioned(element) {
    return (this.getStyle(element, 'position') || 'static') === 'static';
  }

  /**
   * @param {object} element thing to find offset for
   * @return {number} closest, non-statically positioned parentOffset
   */
  parentOffsetEl(element) {
    const docDomEl = _.first(this._$document);
    let offsetParent = element.offsetParent || docDomEl;
    while (offsetParent && offsetParent !== docDomEl &&
        this.isStaticPositioned(offsetParent)) {
      offsetParent = offsetParent.offsetParent;
    }
    return offsetParent || docDomEl;
  }

  /**
   * Provides read-only equivalent of jQuery's position function:
   * http://api.jquery.com/position/
   * @param {object} element thing to work on
   * @return {object} position
   */
  position(element) {
    const elBCR = this.offset(element);
    let offsetParentBCR = {
      top: 0,
      left: 0
    };
    const offsetParentEl = this.parentOffsetEl(_.first(element));
    if (offsetParentEl !== _.first(this._$document)) {
      offsetParentBCR = this.offset(angular.element(offsetParentEl));
      offsetParentBCR.top += offsetParentEl.clientTop -
        offsetParentEl.scrollTop;
      offsetParentBCR.left += offsetParentEl.clientLeft -
        offsetParentEl.scrollLeft;
    }

    const boundingClientRect = _.first(element).getBoundingClientRect();
    return {
      width: boundingClientRect.width || element.prop('offsetWidth'),
      height: boundingClientRect.height || element.prop('offsetHeight'),
      top: elBCR.top - offsetParentBCR.top,
      left: elBCR.left - offsetParentBCR.left
    };
  }

  /**
   * Provides read-only equivalent of jQuery's offset function:
   * http://api.jquery.com/offset/
   * @param {object} element thing to work on
   * @return {object} position
   */
  offset(element) {
    const boundingClientRect = _.first(element).getBoundingClientRect();
    return {
      width: boundingClientRect.width || element.prop('offsetWidth'),
      height: boundingClientRect.height || element.prop('offsetHeight'),
      top: boundingClientRect.top + (this._$window.pageYOffset ||
          _.first(this._$document).body.scrollTop ||
          _.first(this._$document).documentElement.scrollTop),
      left: boundingClientRect.left + (this._$window.pageXOffset ||
           _.first(this._$document).body.scrollLeft ||
           _.first(this._$document).documentElement.scrollLeft)
    };
  }
}
