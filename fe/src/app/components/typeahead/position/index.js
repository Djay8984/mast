import * as angular from 'angular';

import positionService from './position.service';

export default angular
  .module('app.components.typeahead.position', [])
  .service('position', positionService)
  .name;
