import templateUrl from './typeahead-match.html';

/* @ngInject */
export default () => ({
  restrict: 'EA',
  scope: {
    index: '=',
    match: '=',
    query: '='
  },
  templateUrl
});
