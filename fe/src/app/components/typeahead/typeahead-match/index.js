import * as angular from 'angular';

import typeaheadMatchDirective from './typeahead-match.directive';

export default angular
  .module('app.components.typeahead.typeahead-match', [])
  .directive('typeaheadMatch', typeaheadMatchDirective)
  .name;
