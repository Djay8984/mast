import * as angular from 'angular';

import * as _ from 'lodash';

const HOT_KEYS = [9, 13, 27, 38, 40];

/* @ngInject */
export default (
  $compile,
  $parse,
  $q,
  $timeout,
  $document,
  position,
  typeaheadParser
) => ({
  require: 'ngModel',
  restrict: 'EA',
  link(originalScope, element, attrs, modelCtrl) {
    // SUPPORTED ATTRIBUTES (OPTIONS)

    // minimal no of characters that needs to be entered before typeahead starts
    const minSearch = originalScope.$eval(attrs.typeaheadMinLength) || 1;

    // minimal wait time after last character typed before typehead starts
    const waitTime = originalScope.$eval(attrs.typeaheadWaitMs) || 0;

    // it restrict model values to the ones selected from the popup only?
    const isEditable = originalScope.$eval(attrs.typeaheadEditable) !== false;

    // binding to a variable that indicates if matches are being retrieved async
    const isLoadingSetter = $parse(attrs.typeaheadLoading).assign ||
      angular.noop;

    // a callback executed when a match is selected
    const onSelectCallback = $parse(attrs.typeaheadOnSelect);

    const inputFormatter = attrs.typeaheadInputFormatter ?
      $parse(attrs.typeaheadInputFormatter) :
      null;

    const appendToBody = attrs.typeaheadAppendToBody ?
      $parse(attrs.typeaheadAppendToBody) :
      false;

    // INTERNAL VARIABLES

    // model setter executed upon match selection
    const $setModelValue = $parse(attrs.ngModel).assign;

    // expressions used by typeahead
    const parserResult = typeaheadParser.parse(attrs.typeahead);

    let hasFocus = null;

    // pop-up element used to display matches
    const popUpEl = angular.element('<div typeahead-popup></div>');
    popUpEl.attr({
      matches: 'matches',
      open: 'open',
      active: 'activeIdx',
      select: 'select(activeIdx)',
      query: 'query',
      position: 'position'
    });

    // custom item template
    if (angular.isDefined(attrs.typeaheadTemplateUrl)) {
      popUpEl.attr('template-url', attrs.typeaheadTemplateUrl);
    }

    // create a child scope for the typeahead directive so we are not polluting
    // original scope with typeahead-specific data (matches, query etc.)
    const scope = originalScope.$new();
    originalScope.$on('$destroy', () => {
      if (!_.isNil(scope)) {
        scope.$destroy();
      }
    });

    function resetMatches() {
      scope.matches = [];
      scope.activeIdx = -1;
      scope.open = false;
    }

    /**
     * Updates the size and overflow of the popup element.
     *
     * @param {array} matches - array containing matches
     */
    function resizePopup(matches) {
      if (_.size(matches) > 8) {
        const listItems = _.slice(popUpEl.children(), 0, 8);
        const height = _.reduce(listItems, (h, li) =>
            h += li.clientHeight, 0);

        popUpEl.css('overflow-y', 'scroll');
        popUpEl.css('height', `${height}px`);
      } else {
        popUpEl.css('overflow-y', 'visible');
        popUpEl.css('height', 'inherit');
      }
    }

    const getMatchesAsync = (inputValue) => {
      const locals = {
        $viewValue: inputValue
      };

      isLoadingSetter(originalScope, true);
      $q.when(parserResult.source(originalScope, locals))
        .then((matches) => {
        // it might happen that several async queries were in progress if a
        // user were typing fast but we are interested only in responses that
        // correspond to the current view value
          if (inputValue === modelCtrl.$viewValue && hasFocus) {
            if (_.isEmpty(matches)) {
              resetMatches();
            } else {
              scope.activeIdx = 0;
              scope.matches.length = 0;

              // transform labels
              _.forEach(matches, (match, index) => {
                locals[parserResult.itemName] = match;
                scope.matches.push({
                  label: parserResult.viewMapper(scope, locals),
                  model: match
                });
              });

              scope.query = inputValue;
            }

            scope.open = true;

            // position pop-up with matches - we need to re-calculate its
            // position each time we are opening a window with matches as a
            // pop-up might be absolute-positioned and position of an input
            // might have changed on a page due to other elements being
            // rendered
            scope.position = appendToBody ?
              position.offset(element) :
              position.position(element);
            scope.position.top += element.prop('offsetHeight');

            if (_.size(matches) === 1 &&
              parserResult.viewMapper(scope, locals) === inputValue &&
              originalScope.$eval(attrs.autoselect)) {
              scope.select(scope.activeIdx);
            }

            // zero $timeout ensures this is executed when everything else in
            // the call stack is finished. see - http://bit.ly/2bw9XEq
            $timeout(() => {
              resizePopup(matches);
            }, 0);
          }
        })
        .catch(resetMatches)
        .finally(() => isLoadingSetter(originalScope, false));
    };

    resetMatches();

    // we need to propagate user's query so we can higlight matches
    scope.query = null;

    // Declare the timeout promise var outside the function scope so that
    // stacked calls can be cancelled later
    let timeoutPromise = null;

    // plug into $parsers pipeline to open a typeahead on view changes
    // initiated from DOM $parsers kick-in on all the changes coming from the
    // view as well as manually triggered by $setViewValue
    modelCtrl.$parsers.unshift((inputValue) => {
      if (inputValue && inputValue.length >= minSearch) {
        if (waitTime > 0) {
          if (timeoutPromise) {
            // cancel previous timeout
            $timeout.cancel(timeoutPromise);
          }
          timeoutPromise = $timeout(() =>
            getMatchesAsync(inputValue), waitTime);
        } else {
          getMatchesAsync(inputValue);
        }
      } else {
        isLoadingSetter(originalScope, false);
        resetMatches();
      }

      let result = inputValue;
      if (!isEditable) {
        if (inputValue) {
          modelCtrl.$setValidity('editable', false);
          result = null;
        } else {
          //  Reset in case user had typed something previously.
          modelCtrl.$setValidity('editable', true);
          result = inputValue;
        }
      }

      return result;
    });

    modelCtrl.$formatters.push((modelValue) => {
      let candidateViewValue = null;
      let emptyViewValue = null;
      let result = null;
      const locals = {};

      if (inputFormatter) {
        locals.$model = modelValue;
        result = inputFormatter(originalScope, locals);
      } else {
        // it might happen that we don't have enough info to properly render
        // input value we need to check for this situation and simply return
        // model value if we can't apply custom formatting
        locals[parserResult.itemName] = modelValue;
        candidateViewValue = parserResult.viewMapper(originalScope, locals);
        locals[parserResult.itemName] = null;
        emptyViewValue = parserResult.viewMapper(originalScope, locals);
        result = candidateViewValue === emptyViewValue ?
          modelValue :
          candidateViewValue;
      }

      return result;
    });

    scope.select = (activeIdx) => {
      // called from within the $digest() cycle
      const locals = {};
      let model = null;
      let item = null;

      locals[parserResult.itemName] = item = scope.matches[activeIdx].model;
      model = parserResult.modelMapper(originalScope, locals);
      $setModelValue(originalScope, model);
      modelCtrl.$setValidity('editable', true);

      onSelectCallback(originalScope, {
        $item: item,
        $model: model,
        $label: parserResult.viewMapper(originalScope, locals)
      });

      _.set(element[0], 'disabled', true);

      resetMatches();

      // return focus to the input element if a mach was selected via a mouse
      // click event
      _.first(element).focus();
    };

    // bind keyboard events: arrows up(38) / down(40), enter(13) and tab(9),
    // esc(27)
    element.bind('keydown', (event) => {
      // typeahead is open and an "interesting" key was pressed
      if (!_.isEmpty(scope.matches) && _.includes(HOT_KEYS, event.which)) {
        event.preventDefault();

        switch (event.which) {
          case 40:
            scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length;
            scope.$digest();
            break;
          case 38:
            scope.activeIdx =
              (scope.activeIdx ? scope.activeIdx : scope.matches.length) - 1;
            scope.$digest();
            break;
          case 13:
          case 9:
            scope.$apply(() => scope.select(scope.activeIdx));
            break;
          case 27:
            event.stopPropagation();
            resetMatches();
            scope.$digest();
            break;
          default:
            break;
        }
      }
    });

    element.bind('blur', () => hasFocus = false);
    element.bind('focus', () => hasFocus = true);

    // Keep reference to click handler to unbind it.
    function dismissClickHandler(event) {
      if (_.first(element) !== event.target) {
        resetMatches();
        scope.$digest();
      }
    }

    $document.bind('click', dismissClickHandler);

    originalScope.$on('$destroy', () =>
      $document.unbind('click', dismissClickHandler));

    const $popup = $compile(popUpEl)(scope);

    if (appendToBody) {
      $document.find('body').append($popup);
    } else {
      element.after($popup);
    }
  }
});

