import templateUrl from './typeahead-popup.html';

/* @ngInject */
export default () => ({
  restrict: 'EA',
  scope: {
    matches: '=',
    query: '=',
    active: '=',
    position: '=',
    select: '&',
    open: '='
  },
  replace: true,
  templateUrl,
  link(scope, element, attrs) {
    scope.templateUrl = attrs.templateUrl;

    scope.isOpen = () => scope.open;
    scope.isActive = (matchIdx) => scope.active === matchIdx;
    scope.selectActive = (matchIdx) => scope.active = matchIdx;
    scope.selectMatch = (activeIdx) => scope.select({ activeIdx });
  }
});
