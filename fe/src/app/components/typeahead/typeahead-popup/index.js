import * as angular from 'angular';

import typeaheadPopupDirective from './typeahead-popup.directive';

export default angular
  .module('app.components.typeahead.typeahead-popup', [])
  .directive('typeaheadPopup', typeaheadPopupDirective)
  .name;
