/* @ngInject */
export default () =>
  (scope, element, attr) => {
    element
      .addClass('ng-binding')
      .data('$binding', attr.bindHtmlUnsafe);
    scope.$watch(attr.bindHtmlUnsafe, (value) => element.html(value || ''));
  };
