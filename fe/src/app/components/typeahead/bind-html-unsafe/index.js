import * as angular from 'angular';

import bindHtmlUnsafeDirective from './bind-html-unsafe.directive';

export default angular
  .module('app.components.typeahead.bindHtmlUnsafe', [])
  .directive('bindHtmlUnsafe', bindHtmlUnsafeDirective)
  .name;
