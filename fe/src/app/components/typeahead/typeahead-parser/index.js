import * as angular from 'angular';

import typeaheadParserService from './typeahead-parser.service';

export default angular
  .module('app.components.typeahead.typeahead-parser', [])
  .service('typeaheadParser', typeaheadParserService)
  .name;
