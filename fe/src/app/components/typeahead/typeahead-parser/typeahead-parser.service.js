import Base from 'app/base.class';

const TYPEAHEAD_REGEXP =
  /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;

export default class TypeaheadParser extends Base {
  /* @ngInject */
  constructor($parse) {
    super(arguments);
  }

  parse(input) {
    const match = input.match(TYPEAHEAD_REGEXP);
    if (!match) {
      throw new Error(`
Expected typeahead specification in form of '_modelValue_ (as _label_)? for
_item_ in _collection_' but got '${input}'.`);
    }

    return {
      itemName: match[3],
      source: this._$parse(match[4]),
      viewMapper: this._$parse(match[2] || match[1]),
      modelMapper: this._$parse(match[1])
    };
  }
}
