import * as angular from 'angular';

import typeaheadHighlightFilter from './typeahead-highlight.filter';

export default angular
  .module('app.components.typeahead.typeahead-highlight', [])
  .filter('typeaheadHighlight', typeaheadHighlightFilter)
  .name;
