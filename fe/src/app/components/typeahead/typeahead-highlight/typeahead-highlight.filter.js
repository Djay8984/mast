/* @ngInject */
export default () =>
   (matchItem, query) => query ?
      matchItem.replace(new RegExp(
          query.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'), 'gi'),
        '<strong>$&</strong>') :
      matchItem;
