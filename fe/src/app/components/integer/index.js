import * as angular from 'angular';
import integerDirective from './integer.directive';

export default angular
  .module('app.components.integer', [])
  .directive('integer', integerDirective)
  .name;
