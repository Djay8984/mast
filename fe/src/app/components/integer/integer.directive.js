import * as _ from 'lodash';

/* @ngInject */
export default ($parse) => ({
  require: 'ngModel',
  restrict: 'EA',
  link(scope, element, attrs, ngModel) {
    ngModel.$parsers.push((value) => value ?
        _.parseInt(_.toString(value).replace(/[^\d]/g, '')) :
        null);

    ngModel.$validators.integer = (value) =>
      _.isNil(value) || _.isEmpty(value) ?
        true :
        _.isInteger(value);

    if (attrs.hardMin) {
      ngModel.$parsers.push(hard(attrs.hardMin, _.max));
    }

    if (attrs.hardMax) {
      ngModel.$parsers.push(hard(attrs.hardMax, _.min));
    }

    if (attrs.lengthLimit) {
      ngModel.$parsers.push((value) => {
        const newValue = _.isNil(value) ?
          value :
          _.parseInt(
            _.toString(value).slice(0, $parse(attrs.lengthLimit)(scope)));

        ngModel.$setViewValue(newValue);
        ngModel.$render();

        return newValue;
      });
    }

    if (attrs.lengthMinLimit) {
      ngModel.$validators.lengthMinLimit = (value) =>
        _.size(_.toString(value)) >= $parse(attrs.lengthMinLimit)(scope);
    }

    if (attrs.softMin) {
      ngModel.$validators.softMin = soft(attrs.softMin, _.gte);
    }

    if (attrs.softMax) {
      ngModel.$validators.softMax = soft(attrs.softMax, _.lte);
    }

    function soft(attribute, comparator) {
      return (value) => {
        const limit = $parse(attribute)(scope);
        return _.isNil(limit) || _.isNil(value) ?
            true :
            comparator(value, limit);
      };
    }

    function hard(attribute, clamper) {
      return (value) => {
        const limit = $parse(attribute)(scope);
        let newValue = value;

        if (_.isInteger(value)) {
          newValue = clamper([value, limit]);
        } else if (_.isNaN(value)) {
          newValue = limit;
        }

        ngModel.$setViewValue(newValue);
        ngModel.$render();

        return newValue;
      };
    }
  }
});
