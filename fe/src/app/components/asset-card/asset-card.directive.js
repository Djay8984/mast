import AssetCardController from './asset-card.controller';
import templateUrl from './asset-card.html';
import './job-detail.html';
import './case-detail.html';

/* @ngInject */
export default ($parse) => ({
  controllerAs: 'vm',
  restrict: 'AE',
  templateUrl,
  controller: AssetCardController,
  scope: true,
  bindToController: {
    asset: '=',
    expanded: '=?'
  },
  transclude: {
    cta: 'assetCardCta'
  }
});
