import * as angular from 'angular';
import assetCardDirective from './asset-card.directive';

export default angular
  .module('app.components.asset-card', [])
  .directive('assetCard', assetCardDirective)
  .name;
