import Base from 'app/base.class';
import * as _ from 'lodash';

export default class AssetCardController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $element,
    caseService,
    caseStatus
  ) {
    super(arguments);

    this._activeCaseStatuses = _.values(_.pick(this._caseStatus.toObject(), [
      'uncommitted', 'populate', 'onHold', 'jobPhase', 'validateAndUpdate'
    ]));
  }

  get cases() {
    return _.chain(this)
      .get('asset.cases')
      .filter(
        (item) => _.includes(this._activeCaseStatuses, item.caseStatus.id))
      .sortBy('createdOn')
      .reverse()
      .value();
  }

  async viewCase(assetCase) {
    const statusId = _.get(assetCase, 'caseStatus.id');
    const target = await this._caseService.isOpenStatus(statusId) ?
      'details' : 'milestones';

    this._$state.go(`asset.cases.view.${target}`,
      {
        assetId: this.asset.id,
        caseId: assetCase.id
      },
      { reload: true });
  }
}
