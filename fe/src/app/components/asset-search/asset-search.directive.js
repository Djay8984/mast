import * as _ from 'lodash';

import templateUrl from './asset-search.html';
import AssetSearchController from './asset-search.controller';

/* @ngInject */
export default () => ({
  bindToController: true,
  controller: AssetSearchController,
  controllerAs: 'vm',
  link: (scope, element, attrs, controller) => _.merge(scope, {
    setModel: controller.$setViewValue,
    getModel: () => controller.$viewValue
  }),
  require: 'ngModel',
  restrict: 'AE',
  scope: {
    defaultModel: '=',
    asset: '='
  },
  templateUrl
});
