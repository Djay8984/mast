import * as angular from 'angular';

import assetSearchDirective from './asset-search.directive';

export default angular
  .module('app.components.asset-search', [])
  .directive('assetSearch', assetSearchDirective)
  .name;
