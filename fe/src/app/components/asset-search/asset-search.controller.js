import * as _ from 'lodash';

import * as angular from 'angular';

import Base from 'app/base.class';

import assetType from './partials/asset-type.html';
import lifecycleStatus from './partials/lifecycle-status.html';
import classStatus from './partials/class-status.html';
import buildLocation from './partials/build-location.html';
import buildDate from './partials/build-date.html';
import flag from './partials/flag.html';
import grossTonnage from './partials/gross-tonnage.html';
import customer from './partials/customer.html';
import itemType from './partials/item-type.html';

const ROW_LENGTH = 4;
const STATCODES_LENGTH = 5;
const STATCODE_HIDDEN = -1;
const STATCODE_ALL = 0;

export default class AssetSearchController extends Base {
  /* @ngInject */
  constructor(
    $q,
    $scope,
    $state,
    $stateParams,
    assetLifecycleStatus,
    attributeValueType,
    dateFormat,
    moment,
    referenceDataService,
    typeaheadService
  ) {
    super(arguments);

    this.dates = {
      fromDate: moment().hour(12).startOf('h'),
      toDate: moment().hour(12).startOf('h'),
      buildDateMin: moment().add(-9999, 'Y').hour(12).startOf('h'),
      buildDateMax: moment().add(9999, 'Y').hour(12).startOf('h')
    };

    const refData = this._referenceDataService;
    this.filters = {
      assetType: {
        name: 'Asset type',
        templateUrl: assetType,
        parameters: ['assetTypeId']
      },
      lifecycleStatus: {
        name: 'Lifecycle status',
        templateUrl: lifecycleStatus,
        parameters: ['lifecycleStatusId']
      },
      classStatus: {
        name: 'Class status',
        templateUrl: classStatus,
        parameters: ['classStatusId']
      },
      buildLocation: {
        name: 'Build location',
        templateUrl: buildLocation,
        parameters: ['builder', 'yardNumber'],
        builder: {
          async search(val) {
            const param = 'asset.builder';
            return _.get(await refData.get([param], true), param);
          }
        }
      },
      buildDate: {
        name: 'Build date',
        templateUrl: buildDate,
        parameters: ['buildDateMin', 'buildDateMax']
      },
      flag: {
        name: 'Flag',
        templateUrl: flag,
        parameters: ['flagStateId']
      },
      grossTonnage: {
        name: 'Gross tonnage',
        templateUrl: grossTonnage,
        parameters: ['grossTonnageMin', 'grossTonnageMax']
      },
      customer: {
        name: 'Customer',
        templateUrl: customer,
        parameters: ['partyId', 'partyRoleId', 'partyFunctionTypeId']
      },
      itemType: {
        name: 'Item type',
        templateUrl: itemType,
        parameters: ['itemType', 'attribute', 'attributeValue', 'operator']
      }
    };
    this.parameters = {};

    $scope.$watch('vm.parameters.buildDateMin', (newval, oldval) => {
      this.filters.buildDate.active = newval || this.parameters.buildDateMax;
    });

    $scope.$watch('vm.parameters.buildDateMax', (newval, oldval) => {
      this.filters.buildDate.active = this.parameters.buildDateMin || newval;
    });

    this.setupData();
  }

  /**
   * sets up the reference data & applies it to the controller, then fires off
   * the unmarshalling
   * @return {promise} setting up promise, resolves when set up
   */
  async setupData() {
    if (!this._building) {
      const self = this;
      const builder = async () => {
        // IDs are only for tracking, the BE just needs the strings
        self.attributeValueOperators = [
          { id: 1, name: '=', param: 'Equal' },
          { id: 2, name: '>', param: 'Greater' },
          { id: 3, name: '<', param: 'Less' }
        ];

        const referenceDataMap = {
          assetTypes: 'asset.assetTypes',
          itemTypes: 'asset.itemTypes',
          attributeTypes: 'asset.attributeTypes',
          lifecycleStatuses: 'asset.lifecycleStatuses',
          classStatuses: 'asset.classStatuses',
          partyFunctionTypes: 'party.partyFunctions',
          partyRoles: 'party.partyRoles',
          flags: 'flag.flags',
          customers: 'party.party',
          builders: 'asset.builder'
        };

        const data = await self._referenceDataService
          .get(_.values(referenceDataMap), true);

        _.forEach(referenceDataMap, (value, key) =>
          _.set(self, key, _.get(data, value)));

        self.flagTypeahead = self._typeaheadService.query(self.flags);
        self.builderTypeahead = self._typeaheadService.query(self.builders);
        self.customerTypeahead = self._typeaheadService.query(self.customers);
        self.attributeTypeTypeahead = self._typeaheadService.query(
          self.attributeTypes);
        self.itemTypeTypeahead = self._typeaheadService.query(self.itemTypes);

        self.setPreferences();
        self.unmarshall();
      };
      this._building = builder();
    }
    return this._building;
  }

  setPreferences() {
    _.forEach(this.lifecycleStatuses, (status) => {
      if (status.id === this._assetLifecycleStatus.get('decommissioned') ||
      status.id === this._assetLifecycleStatus.get('cancelled')) {
        _.set(status, 'disabled', Boolean(this._$stateParams.batchAction));
      }
    });
  }

  /**
   * interprets the model passed & transmogrifies it into the filters' content
   */
  async unmarshall() {
    if (this._building) {
      await this._building;
    }

    const model = this._$scope.getModel() || {};
    this.parameters = angular.copy(model);

    this.statcodes = _.map(_.isEmpty(model.assetTypeId) ?
        _.times(STATCODES_LENGTH, (index) =>
          index === 0 ? STATCODE_ALL : STATCODE_HIDDEN) :
        this.unmarshallStatcodes(model.assetTypeId),
      _.toString);

    _.forEach(this.lifecycleStatuses, (status) =>
      _.set(status, 'selected', true));
    this.defaultLifecycleStatuses = angular.copy(this.lifecycleStatuses);

    if (model.lifecycleStatusId) {
      _.forEach(this.lifecycleStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(model.lifecycleStatusId, (id) =>
        _.set(_.find(this.lifecycleStatuses, { id }), 'selected', true));
    } else if (_.get(this, 'defaultModel.lifecycleStatusId')) {
      _.forEach(this.lifecycleStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(this.defaultModel.lifecycleStatusId, (id) =>
        _.set(_.find(this.lifecycleStatuses, { id }), 'selected', true));
      this.defaultLifecycleStatuses = angular.copy(this.lifecycleStatuses);
    }

    _.forEach(this.classStatuses, (status) =>
      _.set(status, 'selected', true));
    this.defaultClassStatuses = angular.copy(this.classStatuses);

    if (model.classStatusId) {
      _.forEach(this.classStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(model.classStatusId, (id) =>
        _.set(_.find(this.classStatuses, { id }), 'selected', true));
    } else if (_.get(this, 'defaultModel.classStatusId')) {
      _.forEach(this.classStatuses, (status) =>
        _.set(status, 'selected', false));
      _.forEach(this.defaultModel.classStatusId, (id) =>
        _.set(_.find(this.classStatuses, { id }), 'selected', true));
      this.defaultClassStatuses = angular.copy(this.classStatuses);
    }

    if (model.buildDateMin) {
      this.parameters.buildDateMin = model.buildDateMin;
    }

    if (model.buildDateMax) {
      this.parameters.buildDateMax = model.buildDateMax;
    }

    if (model.flagStateId) {
      this.parameters.flagStateId = _.find(this.flags, {
        id: _.first(model.flagStateId)
      });
    }

    if (this.parameters.partyId) {
      this.parameters.partyId = _.find(this.customers, {
        id: _.first(model.partyId)
      });
    }

    if (!_.isEmpty(model.partyRoleId)) {
      this.parameters.partyRoleId =
        _.toString(_.first(model.partyRoleId));
    }

    if (!_.isEmpty(model.partyFunctionTypeId)) {
      this.parameters.partyFunctionTypeId =
        _.toString(_.first(model.partyFunctionTypeId));
    }

    if (model.itemTypeId) {
      this.parameters.itemTypeId = _.find(this.itemTypes, {
        id: _.first(model.itemTypeId)
      });
    }

    if (model.attributeTypeId) {
      this.parameters.attributeTypeId = _.find(this.attributeTypes, {
        id: _.first(model.attributeTypeId)
      });
    }

    _.forEach(this.filters, (filter) => {
      const actualModel = _.pick(model, filter.parameters);
      const defaultModel = _.pick(this.defaultModel, filter.parameters);

      _.set(filter, 'active', _.isEmpty(defaultModel) ?
          !_.isEmpty(actualModel) :
          !_.isEqual(actualModel, defaultModel));
    });
  }

  /**
   * @return {string} date format
   */
  get dateFormat() {
    return this._dateFormat;
  }

  /**
   * @param {array} typeIds hierarchy of asset types mapped onto an array
   * @return {array} parsed statcodes (asset types)
   */
  unmarshallStatcodes(typeIds) {
    const actualAssetTypes = _.map(typeIds, (id) =>
      _.find(this.assetTypes, { id }));
    const levels = _.times(STATCODES_LENGTH, (index) =>
      _.filter(actualAssetTypes, {
        levelIndication: index + 1
      }));
    const statcodes = _.times(STATCODES_LENGTH, () => STATCODE_HIDDEN);

    const findStatcodes = (statcodeIndex, parentId) => {
      if (statcodeIndex < 0) {
        return;
      }

      if (parentId) {
        statcodes[statcodeIndex] = parentId;
        findStatcodes(statcodeIndex - 1, _.find(this.assetTypes, {
          id: parentId
        }).parentId);
      }

      if (_.size(levels[statcodeIndex]) === 1) {
        const mostSpecific = _.first(levels[statcodeIndex]);

        statcodes[statcodeIndex] = _.toString(mostSpecific.id);
        findStatcodes(statcodeIndex - 1, mostSpecific.parentId);
      } else {
        const parents = _.union(_.map(levels[statcodeIndex], 'parentId'));

        if (_.size(parents) === 1) {
          statcodes[statcodeIndex] = STATCODE_ALL;
          findStatcodes(statcodeIndex - 1, _.first(parents));
        } else if (_.size(parents) > 1) {
          statcodes[statcodeIndex] = STATCODE_HIDDEN;
          findStatcodes(statcodeIndex - 1, null);
        }
      }
    };

    findStatcodes(STATCODES_LENGTH - 1, null);
    return statcodes;
  }

  /**
   * @param {int} parentId parent id to look for, can be null to not use
   * @param {int} levelIndication level to look for
   * @return {array} list of statcodes
   * filters the statcodes in accordance with the params & asset category
   */
  filterStatcodes(parentId, levelIndication) {
    return _.filter(this.assetTypes, _.omitBy({
      parentId:
        _.includes([STATCODE_HIDDEN, STATCODE_ALL, NaN],
          _.parseInt(parentId)) ? null : _.parseInt(parentId),
      levelIndication,
      category: this.asset ? { id: this.asset.assetCategory.id } : null
    }, _.isNil));
  }

  /**
   * @param {int} statcodeIndex statcode index to start modifying from
   * goes through the statcode list & enforces the correct hierarchy scheme
   */
  updateStatcodes(statcodeIndex) {
    if (_.parseInt(this.statcodes[statcodeIndex]) === STATCODE_ALL) {
      this.statcodes = _.map(this.statcodes, (statcode, index) =>
        index > statcodeIndex ? STATCODE_HIDDEN : statcode);
    } else {
      this.statcodes[statcodeIndex + 1] = STATCODE_ALL;
      this.statcodes = _.map(this.statcodes, (statcode, index) =>
        index > statcodeIndex + 1 ? STATCODE_HIDDEN : statcode);
    }
    this.statcodes = _.map(this.statcodes, _.toString);
  }

  /**
   * @param {array} statcodes list of statcodes (asset types)
   * @return {array} hierarchy of asset types mapped onto an array
   */
  marshallStatcodes(statcodes) {
    const actualAssetTypes = _.map(_.map(statcodes, _.parseInt), (id, index) =>
        _.find(this.assetTypes, { id }));
    const mostSpecific = _.first(_.reject(_.reverse(actualAssetTypes),
          _.isUndefined));
    let assetList = [];

    if (_.isUndefined(mostSpecific)) {
      assetList = this.assetTypes;
    } else {
      const addChildren = (parentAssetType) => {
        const currentChildren = _.filter(this.assetTypes, {
          parentId: parentAssetType.id,
          levelIndication: parentAssetType.levelIndication + 1
        });

        assetList = _.union(assetList, currentChildren);
        _.forEach(currentChildren, addChildren);
      };

      assetList = [mostSpecific];
      if (mostSpecific.levelIndication !== STATCODES_LENGTH) {
        addChildren(mostSpecific);
      }
    }

    return assetList;
  }

  /**
   * @param {array} list objects to set the selected attribute on & extract ids
   * @param {boolean} selected value for selected attribute
   * @return {array} modified list
   */

  selectAll(list) {
    return _.map(_.reject(list, 'disabled'),
      (item) => _.set(item, 'selected', true));
  }

  clearAll(list) {
    return _.map(list, (item) => _.set(item, 'selected', false));
  }

  /*
   * @param {object|number|string} thingOne thing to compare
   * @param {object|number|string} thingTwo thing to compare
   * @return {boolean} whether the two things are equal
   */
  getFilterActivation(thingOne, thingTwo) {
    return _.isEqual(thingOne, thingTwo);
  }

  /**
   * @param {object} filter thing to toggle selected attribute on
   * disables the selection on all other filters too
   */
  toggleFilterSelected(filter) {
    _.reject(this.filters, filter).forEach(
      (deselectedFilter) => deselectedFilter.selected = false);
    filter.selected = !filter.selected;
  }

  /**
   * @param {object} filter thing to find parameters to strip
   * strips parameters relevant to a filter from the parameters object
   */
  setFilterInactive(filter) {
    _.omit(this.parameters, filter.parameters);
    filter.active = false;
  }

  /**
   * @param {int} row number of row to extract
   * @return {array} array of filters extracted
   */
  getFilters(row) {
    return _.toArray(_.pick(this.filters, _.slice(_.keys(this.filters),
        row * ROW_LENGTH, row * ROW_LENGTH + ROW_LENGTH)));
  }

  /**
   * @return {boolean} whether or not all of the filters' forms are valid
   */
  getFiltersValidity() {
    return _.every(_.filter(this.filters, 'form'), (filter) =>
        _.get(filter, 'form.$valid'));
  }

  /**
   * sets all filters to inactive & clears parameters. Utilises the ng-change
   * that exists on the $scope.model which triggers a state reload.
   */
  resetFilters() {
    this._$scope.setModel(null);
  }

  /**
   * builds parameters from inputs, & exposes via the model only if the filter
   * forms are valid
   */
  applyFilters() {
    if (!this.getFiltersValidity()) {
      return;
    }

    const parameters = angular.copy(this.parameters);

    parameters.lifecycleStatusId =
      _.map(_.filter(this.lifecycleStatuses, 'selected'), 'id');

    parameters.classStatusId =
      _.map(_.filter(this.classStatuses, 'selected'), 'id');

    parameters.assetTypeId =
      _.map(this.marshallStatcodes(this.statcodes), 'id');

    if (this.parameters.buildDateMin) {
      parameters.buildDateMin =
        this._moment(this.parameters.buildDateMin).toISOString();
    }

    if (this.parameters.buildDateMax) {
      parameters.buildDateMax =
        this._moment(this.parameters.buildDateMax).toISOString();
    }

    if (_.size(parameters.assetTypeId) === _.size(this.assetType)) {
      parameters.assetTypeId = null;
    }

    parameters.builder = _.get(this.parameters.builder, 'name',
      this.parameters.builder);

    parameters.flagStateId = _.isEmpty(this.parameters.flagStateId) ?
      null : [this.parameters.flagStateId.id];

    parameters.partyId = _.isEmpty(this.parameters.partyId) ?
      null : [this.parameters.partyId.id];

    parameters.partyRoleId = _.parseInt(this.parameters.partyRoleId) ?
      [_.parseInt(this.parameters.partyRoleId)] :
      null;

    parameters.partyFunctionTypeId =
      _.parseInt(this.parameters.partyFunctionTypeId) ?
      [_.parseInt(this.parameters.partyFunctionTypeId)] :
      null;

    parameters.itemTypeId = _.get(this.parameters.itemTypeId, 'id');

    parameters.attributeTypeId = _.get(this.parameters.attributeTypeId, 'id');

    parameters.operator = this.showAttributeOperator ?
      _.find(this.attributeValueOperators,
        { id: _.parseInt(this.parameters.operator) }).param :
      null;

    // okay this one is going to need some explanation:
    // we go through the default parameters, finding the filters which have
    // default parameters specified & set those filters to true, then we go
    // through the filters removing any active ones, the remainder of which
    // are the inactive filters which are then stripped from the parameters.
    // finally any nil parameters are removed from the model to tidy up any
    // pollution
    const defaultParameters = _.intersection(
      _.keys(this.defaultModel),
      _.flatMap(this.filters, 'parameters'));

    const filters = angular.copy(this.filters);
    _.forEach(
      _.filter(filters,
        (filter) => !_.isEmpty(
          _.intersection(filter.parameters, defaultParameters))),
      (filter) => _.set(filter, 'active', true));

    this._$scope.setModel(_.omitBy(
      _.omit(parameters,
        _.flatMap(
          _.reject(filters, 'active'),
          'parameters')),
        _.isNil));
  }

  validateChange(otherField) {
    this.filters.grossTonnage.active = this.parameters.grossTonnageMin ||
      this.parameters.grossTonnageMax;
    otherField.$validate();
  }

  get itemTypeFilterActive() {
    if (this.parameters.attributeValue) {
      return Boolean(this.parameters.attributeTypeId);
    }
    return this.parameters.itemTypeId || this.parameters.attributeTypeId;
  }

  get showAttributeOperator() {
    if (this.parameters.attributeTypeId) {
      return this.parameters.attributeTypeId.valueType.id ===
        this._attributeValueType.get('float');
    }
  }

  get minSetDate() {
    return _.get(this, 'parameters.buildDateMin') || this.dates.buildDateMin;
  }

  get maxSetDate() {
    return _.get(this, 'parameters.buildDateMax') || this.dates.buildDateMax;
  }
}
