import templateUrl from './case-milestone-list.html';
import CaseMilestoneListController from './case-milestone-list.controller.js';

/* @ngInject */
export default () => ({
  templateUrl,
  controller: CaseMilestoneListController,
  controllerAs: 'vm',
  bindToController: {
    case: '=',
    milestones: '=',
    scope: '=',
    manage: '=?',
    isOnHold: '='
  },
  restrict: 'AE',
  scope: true
});
