import * as angular from 'angular';
import caseMilestoneListDirective from './case-milestone-list.directive.js';

export default angular
  .module('app.components.case-milestone-list', [])
  .directive('caseMilestoneList', caseMilestoneListDirective)
  .name;
