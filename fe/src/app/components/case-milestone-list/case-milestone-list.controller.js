import * as _ from 'lodash';
import Base from 'app/base.class';

export default class CaseMilestoneList extends Base {
  /* @ngInject */
  constructor(
    $log,
    $state,
    dateFormat,
    dueStatus,
    milestoneService,
    moment,
    milestoneDueDateReference
  ) {
    super(arguments);
    this.editDueDate = null;

    this.milestones =
      _.orderBy(this.milestones,
        ['milestone.id'],
        ['asc']);
  }

  getOwner(milestone) {
    // check to see if the employee name is in the case object
    const employee = _.findLast(this.case.surveyors,
      { employeeRole: { id: milestone.milestone.id } });
    if (!_.isEmpty(employee)) {
      return employee.surveyor.name;
    }

    return milestone.milestone.owner.name;
  }

  getPredecessorMilestones(parent) {
    return _.sortBy(_.uniqBy(_.reduce(parent.milestone.predecessorMilestones,
      (result, predecessor) => {
      // Only add to predecessor id to list if predecessors is in
      // this.predecessors and not already completed
        if (!Boolean(_.get(
            _.find(_.filter(this.milestones,
              ['milestone.id', predecessor.id]),
              'inScope'), 'completionDate', true))) {
          result.push(predecessor.id);
        }
        return result;
      }, []))).toString().replace(/,/g, ', ');
  }

  isOverdue(milestone) {
    return milestone.dueStatus.id === this._dueStatus.get('overdue');
  }

  async saveDueDate(milestone, dueDate) {
    try {
      await this._milestoneService.saveDueDate(this.case.id,
        milestone, dueDate.format());
      this._$state.reload();
    } catch (e) {
      this._$log.error('Milestone save error');
    } finally {
      this.editDueDate = null;
    }
  }

  getAttachmentPath(milestoneId) {
    // Each attachment counts come from the milestone.service
    return `case/${this.case.id}/milestone/${milestoneId}`;
  }

  get dateFormat() {
    return this._dateFormat;
  }

  setDueDate(id) {
    if (!this.editDueDate) {
      this.editDueDate = id;
    }
  }

  getDueDate() {
    return this.editDueDate;
  }

  enableCheckbox(milestone) {
    // the milestone's mandatory property only matters if we're currently
    // managing milestones otherwise we care about if there is a due date
    const notMandatory = this.manage ?
      !this.hasMandatoryChildren(milestone, milestone.milestone.mandatory) :
      milestone.dueDate;
    return !milestone.completionDate && notMandatory;
  }

  close() {
    this.editDueDate = null;
  }

  /**
   *  Check to see if milestone has children, if so then check that the
   *  children are in scope and that they all have completion dates
   *
   *  @param {obj} parent - parent milestone
   *  @return {bool} complete - true if all children have a completion date
   */
  hasCompletePredecessors(parent) {
    return _.every(parent.milestone.predecessorMilestones, (child) =>
      Boolean(_.get(
        _.find(_.filter(this.milestones, ['milestone.id', child.id]),
          'inScope'), 'completionDate', true)));
  }

  /**
   *  Recursively looks for children and checks if they are mandatory
   *  If there is a child milestone which is found to be mandatory
   *  then exit method when convenient and return true otherwise false.
   *
   *  @param {obj} parents - parents milestone
   *  @param {bool} mandatory - is the dependency tree mandatory so far
   *  @return {bool} mandatory - true if any child is mandatory
   */
  hasMandatoryChildren(parents, mandatory) {
    if (!mandatory) {
      mandatory = _.reduce(parents.milestone.childMilestones,
        (result, child) => {
          const ms = _.find(_.filter(this.milestones,
            ['milestone.id', child.id]),
            'inScope');
          if (_.get(ms, 'milestone.mandatory')) {
            return true;
          } else if (!_.isUndefined(ms)) {
            this.hasMandatoryChildren(ms, result);
          }
        }, false);
    }
    return mandatory;
  }

  get dueDateManual() {
    return this._milestoneDueDateReference.get('manualEntry');
  }
}
