import templateUrl from './breadcrumbs.html';
import BreadcrumbsController from './breadcrumbs.controller';

export default () => ({
  restrict: 'E',
  scope: true,
  templateUrl,
  controller: BreadcrumbsController,
  controllerAs: 'vm',
  bindToController: {
    selectedItem: '=',
    readOnly: '=?',
    preAmble: '@?'
  }
});
