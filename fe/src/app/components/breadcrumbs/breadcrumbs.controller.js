import * as _ from 'lodash';
import Base from 'app/base.class';

export default class BreadcrumbsController extends Base {
  get items() {
    const items = _.get(this.selectedItem, 'items');
    return _.isEmpty(items) ? _.get(this.selectedItem, '.parent.items') : items;
  }

  get breadcrumb() {
    return _.sortBy(_.get(this.selectedItem, 'parents'), 'id') || [];
  }

  selectItem(item) {
    item.select();
    this.selectedItem = item;
  }
}
