import * as angular from 'angular';
import breadcrumbs from './breadcrumbs.directive';

export default angular
  .module('app.components.breadcrumbs', [])
  .directive('breadcrumbs', breadcrumbs)
  .name;
