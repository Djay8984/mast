import Base from 'app/base.class';
import * as _ from 'lodash';

import selectTemplateUrl from './select-template.modal.html';
import breakLinkTemplateUrl from './break-link.modal.html';

export default class SelectTemplateController extends Base {
  /* @ngInject */
  constructor(
    assetModelDecisionService,
    referenceDataService,
    templateService,
    MastModalFactory,
    codicilType,
    templateStatus
  ) {
    super(arguments);

    this.findSelected();
  }

  //  ---------------------------------------------------------
  async findSelected() {
    const id = _.get(this.data, 'value.id');

    if (this.data.value &&
      !this.data.selected || this.data.selected.id !== id) {
      await this.getTemplates();
      this.data.selected = _.find(this.templates, { id });
    }
  }

  //  ---------------------------------------------------------
  async selectTemplate() {
    //  do not open if modal is active
    if (!this.modal || !this.modal.isActive()) {
      this.modal = await this._MastModalFactory({
        templateUrl: selectTemplateUrl,
        scope: {
          vm: this,
          data: this.data
        },
        class: 'full dialog background-sea'
      });
      await this.getTemplates();

      //  Reset search criteria and result list
      Reflect.deleteProperty(this.data.search, 'title');
      Reflect.deleteProperty(this.data.search, 'itemType');
      Reflect.deleteProperty(this.data, 'list');

      this.modal.activate();
    }
  }

  //  ---------------------------------------------------------
  getItemTypes(value) {
    const regex = new RegExp(value, 'i');
    return _.filter(this.itemTypes, (item) => regex.test(item.name));
  }

  //  ---------------------------------------------------------
  async filterTemplates() {
    this.itemTypes = {};
    const codicilTypeId = _.get(this.data, 'codicilType.id');
    const templates =
      await this._referenceDataService.get('asset.codicilTemplates', true);

    this.templates = _.filter(templates, (val) => {
      const itemType = val.itemType;
      const valid = _.get(val, 'codicilType.id') === codicilTypeId;

      if (itemType && valid) {
        this.itemTypes[itemType.id] = this.itemTypes[itemType.id] ||
          { id: itemType.id, name: itemType.name, total: 0 };

        this.itemTypes[itemType.id].total++;
      }
      return valid;
    });

    this.checkItemTypes();
    return this.templates;
  }

  async checkItemTypes() {
    const itemTypeId = _.map(this.itemTypes, 'id');
    const model = this.data.$getModel && this.data.$getModel();
    this.assetId = _.get(model, 'data.asset.id');

    if (this.assetId) {
      const res = await this._assetModelDecisionService
      .queryItem(this.assetId, { itemTypeId }, { order: 'displayOrder' },
        false);

      _.forEach(res, (item) => {
        if (this.itemTypes[_.get(item, 'itemType.id')]) {
          this.itemTypes[_.get(item, 'itemType.id')].available = true;
        }
      });
    }
  }

  //  ---------------------------------------------------------
  async getTemplates() {
    return this.templates || await this.filterTemplates();
  }

  //  ---------------------------------------------------------
  async findTemplate() {
    this.data.loading = 'Loading ...';

    this.data.list =
      await this._templateService.findTemplates(this.data, this.templates);

    if (!_.includes(this.data.list, this.data.selected)) {
      this.data.selected = null;
    }

    this.data.loading = false;
  }

  isDisabled(template) {
    const itemTypeId = _.get(template, 'itemType.id');
    return this.assetId && template.itemType &&
      (template.templateStatus.id !== this._templateStatus.get('live') ||
      !_.get(this.itemTypes[itemTypeId], 'available'));
  }

  //  ---------------------------------------------------------
  select() {
    this.data.value = { id: this.data.selected.id };

    if (_.isFunction(this.data.select)) {
      this.data.select(this.data.selected);
    }
    this.destroy();
  }

  //  ---------------------------------------------------------
  async breakLink() {
    const result = await this._MastModalFactory({
      templateUrl: breakLinkTemplateUrl,
      class: 'dialog'
    }).activate();

    if (result) {
      const model = this.data.$getModel();
      model.setDisabled(false);

      this.data.value = null;
    }
  }
}
