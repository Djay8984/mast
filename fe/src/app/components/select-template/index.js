import * as angular from 'angular';
import selectTemplate from './select-template.directive';

export default angular
    .module('app.components.selectTemplate', [])
    .directive('selectTemplate', selectTemplate)
    .name;
