import selectTemplateController from './select-template.controller';
import selectTemplateUrl from './select-template';

/*
Use:
  @data: {object}
    header: Modal header - {string} 'Batch actions',
    title: Modal title - {string} 'Find an asset Note Template',
    label: Button name - {string} 'Select template',
    search: {
      status: 1, templateStatus.id - {integer}
    },
    codicilType: {
      id: 2, Filters templates by this id - {integer}
    },
    select(values): Hook on select template button click - {function}
    onInit(data): Hook on load - {function}
    selected: {}, Result of selected template - {object},
    value: { Result of selected template,
            by its templateId auto selects template - {object},
      title: 'AAA', Selected template title, also displayed after modal closes
    },
    list: [], Result of tempaltes found - {array}
*/

/* @ngInject */
export default () => ({
  controller: selectTemplateController,
  templateUrl: selectTemplateUrl,
  controllerAs: 'vm',
  bindToController: {
    data: '=selectTemplate'
  }
});
