import templateUrl from './back-button.html';

/* @ngInject */
export default () => ({
  templateUrl,
  restrict: 'AE',
  scope: {
    title: '@'
  }
});
