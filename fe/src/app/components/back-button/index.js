import * as angular from 'angular';
import backButtonDirective from './back-button.directive.js';

export default angular
  .module('app.components.back-button', [])
  .directive('backButton', backButtonDirective)
  .name;
