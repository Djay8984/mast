import * as angular from 'angular';

import attachmentOrNoteDirective from './attachment-or-note.directive';

export default angular
  .module('app.components.attachment-or-note', [])
  .directive('attachmentOrNote', attachmentOrNoteDirective)
  .name;
