import AttachmentOrNoteController from './attachment-or-note.controller.js';
import templateUrl from './attachment-or-note.html';

export default () => ({
  bindToController: {
    attachment: '='
  },
  controller: AttachmentOrNoteController,
  controllerAs: 'vm',
  restrict: 'E',
  scope: true,
  templateUrl
});
