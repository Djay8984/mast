import Base from 'app/base.class';
import * as _ from 'lodash';

import singleLineUrl from './single-line';

export default class HierarchyController extends Base {
  /* @ngInject */
  constructor(
    $q,
    $state,
    $timeout,
    assetModelDecisionService
  ) {
    super(arguments);

    this.selected = {};

    this.loaded = _.get(this.data, 'saved.loaded', {});
    this.templates = _.get(this.data, 'saved.templates', {});

    this.buttonType = this.data.type === 'multi' ? 'check-box' : 'radio-box';
    this.assetId = _.get(this.data, 'asset.id') || _.get(this.data, 'assetId');

    this.data.getSelected = this.getSelected.bind(this);
    this.data.totalSelected = this.totalSelected.bind(this);
  }

  async selectFromValue() {
    if (this.data.value) {
      const values = _.isArray(this.data.value) ?
        this.data.value : [this.data.value];

      if (values.length > 2) {
        await this.queryItem();
      }

      _.forEach(values, (val) => {
        this.selected[val.id] = val;
      });
      return this._$q.all(_.map(values, (val) => this.loadParent(val.id)));
    }
  }

  async filterItemType() {
    const itemTypeId = _.get(this.data, 'itemType.id');

    if (itemTypeId) {
      this.existingOnly = true;
      const items = await this.queryItem({ itemTypeId: [itemTypeId] });

      return this._$q.all(_.map(items, (val) => this.loadParent(val)));
    }
  }

  async loadParent(item) {
    if (!_.isObject(item)) {
      item = this.loaded[item] || await this.getItem(item);
    }

    const parentId = _.get(item, 'parentItem.id');

    if (parentId) {
      const parent = this.loaded[parentId] || await this.getItem(parentId);
      const parentTpl = this.getTemplate(parent);

      if (this.existingOnly) {
        this.getTemplate(item);

        parentTpl.result = parentTpl.result || [];
        parentTpl.ids = parentTpl.ids || {};

        if (!parentTpl.ids[item.id]) {
          parentTpl.result.push(item);
          parentTpl.ids[item.id] = item;
        }
      } else {
        await parentTpl.getItems();
      }

      parentTpl.expand = true;

      if (_.get(parent, 'parentItem.id')) {
        await this.loadParent(parent);
      }
    }
  }

  async getRootItem() {
    await this.filterItemType();
    await this.selectFromValue();
    const res = await this.rootItem();

    if (!this.existingOnly && !this.cache(res)) {
      await this.getItems(res);
    }
    return this.getTemplate(res, !this.existingOnly, true, true);
  }

  async getItem(id) {
    const item = this._assetModelDecisionService
        .getItem(this.assetId, _.get(id, 'id', id), false);
    this.loaded[item.id] = item;
    return item;
  }

  async getItems(item) {
    const ids = _.map(item.items, 'id');
    item.items = await this.queryItem({ itemId: ids });
    return item.items;
  }

  async rootItem() {
    const rootItem = this.loaded.root ||
      await this._assetModelDecisionService.getRootItem(this.assetId,
       { includeDraft: true });
    this.loaded.root = rootItem;
    this.loaded[rootItem.id] = rootItem;
    return rootItem;
  }

  async queryItem(query) {
    const buildMode = _.includes(this._$state.current.url, 'buildMode');
    const res = await this._assetModelDecisionService
      .queryItem(this.assetId, query, { includeDraft: buildMode });

    _.forEach(res, (item) => {
      this.loaded[item.id] = item;
    });
    return res;
  }

  select(item) {
    const byType = {
      single: () => {
        this.selected = { [item.id]: item };
      },
      multi: () => {
        if (_.get(this.selected, item.id)) {
          Reflect.deleteProperty(this.selected, item.id);
        } else {
          this.selected[item.id] = item;
        }
      }
    };

    const selectType = byType[this.data.type] || byType.single;
    return selectType(item);
  }

  isSelected(item, className = 'checked') {
    return _.get(this.selected, item.id) ? className : '';
  }

  totalSelected() {
    return _.size(this.selected);
  }

  getSelected() {
    const items = _.map(this.selected, (val, key) => this.loaded[key]);
    return this.data.type === 'multi' ? items : _.first(items);
  }

  cache(item) {
    let all = true;
    _.forEach(item.items, (val, key) => {
      if (!this.loaded[val.id]) {
        all = false;
      } else {
        item.items[key] = this.loaded[val.id];
      }
    });
    return all && item.items;
  }

  getTemplate(item, expand, first, last) {
    const self = this;
    const itemTypeId = _.get(this.data, 'itemType.id');
    const res = this.templates[item.id] ||
      {
        htmlUrl: singleLineUrl,
        config: this.data,
        item,
        expand,
        expandable: !itemTypeId || itemTypeId !== _.get(item, 'itemType.id'),
        disabled: itemTypeId && itemTypeId !== _.get(item, 'itemType.id'),
        type: this.data.type,
        buttonType: this.buttonType,
        getTemplate: this.getTemplate.bind(this),
        getList() {
          return _.get(self.loaded[item.parentItem.id], 'items');
        },
        getItems() {
          return this.result ||
            self.cache(this.item) || self.getItems(this.item);
        },
        isSelected(name) {
          return self.isSelected(this.item, name);
        },
        select() {
          if (!this.disabled) {
            return self.select(this.item);
          }
        },
        actions: this.data.actions
      };

    res.last = last;
    res.first = first;
    this.templates[item.id] = res;
    return res;
  }
}
