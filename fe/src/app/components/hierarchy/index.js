import * as angular from 'angular';
import hierarchy from './hierarchy.directive';

export default angular
    .module('app.components.hierarchy', [])
    .directive('hierarchy', hierarchy)
    .name;
