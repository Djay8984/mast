import hierarchyController from './hierarchy.controller';
import hierarchyUrl from './hierarchy';

/* Use:
	@data: {object}
		asset: {object}
			id: {number} or
		assetId: {number},
		type: {undefined, string: 'multi'} - single or multiple items to select
		value: {number, array} - result
		itemType: {undefined, object}
			id: {number} - filter items by itemType if set
*/

/* @ngInject */
export default () => ({
  controller: hierarchyController,
  controllerAs: 'vm',
  templateUrl: hierarchyUrl,
  scope: true,
  bindToController: {
    data: '=hierarchy'
  }
});
