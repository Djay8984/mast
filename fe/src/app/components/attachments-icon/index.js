import * as angular from 'angular';
import attachmentsIcon from './attachments-icon.directive';

export default angular
  .module('app.components.attachments-icon', [])
  .directive('attachmentsIcon', attachmentsIcon)
  .name;
