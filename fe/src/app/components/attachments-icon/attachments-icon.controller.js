import * as _ from 'lodash';
import Base from 'app/base.class';

export default class AttachmentsIconController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    attachmentDecisionService
  ) {
    super(arguments);

    this.path = this._$scope.vm.path;
    this.vm = this._$scope.vm;

    this.attachmentCount();
  }

  async attachmentCount() {
    if (this.path) {
      this.loading = '...';
      this.count =
        await this._attachmentDecisionService.getAttachmentCount(this.path);
      _.set(this.vm, 'attachments.pagination.totalElements', this.count);
      this.loading = false;
    }
  }

  get class() {
    let name = this.dark ? 'dark' : 'light';
    name = this.vm.disabled ? 'disabled' : name;
    const preload = this.loading ? 'preload' : '';
    return [`attachment-directive-icon-${name}`, preload];
  }
}
