import controller from './attachments-icon.controller';
import templateUrl from './attachments-icon.html';

/* @ngInject */
export default () => ({
  templateUrl,
  controller,
  controllerAs: 'icon',
  restrict: 'AE',
  bindToController: {
    dark: '=',
    hideCount: '=',
    disabled: '='
  }
});
