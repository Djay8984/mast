import * as _ from 'lodash';

import * as angular from 'angular';

import Base from 'app/base.class';

import CodicilType from 'app/models/codicil/type.model';
import CodicilSearchFilter from 'app/models/codicil/search-filter.model';


import typeAndCategoryTpl from './partials/type-and-category.html';
import dueDateTpl from './partials/due-date.html';
import itemTypeTpl from './partials/item-type.html';
import sdoTpl from './partials/sdo.html';
import statusTpl from './partials/status.html';
import confidentialityTpl from './partials/confidentiality.html';

const ROW_LENGTH = 3;
const CODICIL_TYPE_IDS = [3, 2, 1];
const DEFECT_CATEGORY_IDS = [1, 2, 3, 4, 5];
const FILTER_NO_RESULTS = [-1];

export default class CodicilSearchController extends Base {
  /* @ngInject */
  constructor(
    $filter,
    $parse,
    $scope,
    $state,
    $stateParams,
    moment,
    referenceDataService,
    dateFormat,
    actionableItemStatus,
    assetNoteStatus,
    cocStatus,
    defectStatus
  ) {
    super(arguments);

    if (!_.isEmpty($stateParams.searchFilter)) {
      // Merging the codicil and defect status parameters
      // which were split on search upon calling applyFilters()
      if (!this.codicilType) {
        $stateParams.searchFilter.statusList =
          _.union($stateParams.searchFilter.statusList,
            $stateParams.defectSearchFilter.statusList);
      }

      this.searchFilter = $stateParams.searchFilter;
    } else {
      this.searchFilter = this.codicilFilter;
    }
    this.searchFilter =
      Reflect.construct(CodicilSearchFilter, [this.searchFilter]);

    this.parameters = {};

    this.dates = {
      fromDate: moment().hour(12).startOf('h'),
      toDate: moment().hour(12).startOf('h'),
      dueDateMin: moment().add(-9999, 'Y').hour(12).startOf('h'),
      dueDateMax: moment().add(9999, 'Y').hour(12).startOf('h')
    };

    this.allFilters = {
      categoryList: {
        label: this.codicilType ? 'Category' : 'Type and category',
        templateUrl: typeAndCategoryTpl,
        parameters: ['typeList'],
        model: 'typeFilters'
      },
      dueDate: {
        label: 'Due date',
        templateUrl: dueDateTpl
      },
      status: {
        label: 'Status',
        templateUrl: statusTpl,
        parameters: ['statusList'],
        model: 'statusList'
      },
      itemType: {
        label: 'Item type',
        templateUrl: itemTypeTpl,
        model: 'itemType'
      },
      sdo: {
        label: 'SDO',
        templateUrl: sdoTpl,
        model: 'sdo'
      },
      confidentiality: {
        label: 'Confidentiality',
        templateUrl: confidentialityTpl,
        parameters: ['confidentialityList'],
        model: 'confidentialityList'
      }
    };

    this.filterMap = {
      'asset': _.pick(this.allFilters, [
        'categoryList', 'dueDate', 'status',
        'itemType', 'confidentiality'
      ]),
      'actionable-item': _.pick(this.allFilters, [
        'categoryList', 'dueDate', 'status',
        'itemType', 'sdo', 'confidentiality'
      ]),
      'asset-note': _.pick(this.allFilters, [
        'categoryList', 'status',
        'itemType', 'sdo', 'confidentiality'
      ]),
      'coc': _.pick(this.allFilters, [
        'categoryList', 'dueDate', 'status',
        'sdo', 'confidentiality'
      ]),
      'defect': _.pick(this.allFilters, [
        'categoryList', 'status',
        'itemType', 'sdo', 'confidentiality'
      ])
    };

    this.statusMap = {
      'asset': [
        {
          id: 1,
          name: 'Open',
          statuses: this._getStatuses(['open'])
        }, {
          id: 2,
          name: 'Inactive',
          statuses: this._getStatuses(['inactive'])
        }, {
          id: 3,
          name: 'Recommended change',
          statuses: this._getStatuses(['changeRecommended'])
        }, {
          id: 4,
          name: 'Deleted',
          statuses: _.union(
            this._getStatuses(['deleted']),
            [this._defectStatus.get('cancelled')])
        }
      ],
      'actionable-item': this._getStatuses(
        ['open', 'changeRecommended', 'inactive', 'deleted'],
        [this._actionableItemStatus]),
      'asset-note': this._getStatuses(
        ['open', 'changeRecommended', 'inactive', 'deleted'],
        [this._assetNoteStatus]),
      'coc': this._getStatuses(
        ['open', 'deleted'],
        [this._cocStatus]),
      'defect': this._getStatuses(
        ['open', 'cancelled'],
        [this._defectStatus])
    };

    this._viewTypes = {
      all: {
        label: 'All'
      },
      job: {
        label: 'Job related only'
      },
      other: {
        label: 'Other only'
      }
    };

    this.setupData();
  }

  get viewTypes() {
    return this._viewTypes;
  }

  _getStatuses(statusNames, statusMaps = [
    this._actionableItemStatus,
    this._assetNoteStatus,
    this._cocStatus,
    this._defectStatus
  ]) {
    return _.flatMap(statusMaps,
        (statusMap) => _.values(_.pick(statusMap.toObject(), statusNames)));
  }

  flattenParams(arr) {
    if (typeof arr[0] !== 'object') {
      return arr;
    }

    const result = [];
    arr.forEach((val) => {
      val.categories.forEach((cat) => {
        result.push(val.slug + cat);
      });
    });
    return result;
  }

  compareArr(arr1, arr2) {
    if (typeof arr1 !== 'object' || typeof arr2 !== 'object') {
      return false;
    }

    const flat1 = this.flattenParams(arr1);
    const flat2 = this.flattenParams(arr2);

    const diff = _.difference(flat1, flat2).length;

    return diff || flat1.length !== flat2.length;
  }

  getFiltersToRows() {
    const rows = [];
    let filtersArr = Object.keys(this.filters).map((val) => this.filters[val]);
    while (filtersArr.length) {
      const tmp = filtersArr.splice(ROW_LENGTH);
      rows.push(filtersArr);
      filtersArr = tmp;
    }

    return rows;
  }

  async setupData() {
    this.filters = this.filterMap[this.codicilType || 'asset'];
    this.filterRows = this.getFiltersToRows();

    const statuses = this.codicilType === 'defect' ? 'defect.defectStatuses' :
      'service.codicilStatuses';

    const referenceDataMap = {
      statusList: statuses,
      confidentialityList: 'attachment.confidentialityTypes',
      codicilTypes: 'service.codicilTypes',
      codicilCategories: 'asset.codicilCategories',
      defectCategories: 'defect.defectCategories',
      assetItemTypes: 'asset.itemTypes',
      employeeOffices: 'employee.office'
    };

    const data = await this._referenceDataService
      .get(_.values(referenceDataMap));

    // Filter statuses
    if (this.codicilType) {
      _.set(data, 'service.codicilStatuses', _.filter(
        _.get(data, 'service.codicilStatuses'), (status) =>
        _.includes(this.statusMap[this.codicilType], status.id)));
    } else {
      _.set(data, 'service.codicilStatuses', this.statusMap.asset);
    }

    // Filter defect categories
    _.set(data, 'defect.defectCategories', _.filter(
      _.get(data, 'defect.defectCategories'),
      (category) => _.includes(DEFECT_CATEGORY_IDS, category.id)));

    // Filter codicil types
    _.set(data, 'service.codicilTypes', _.filter(
      _.get(data, 'service.codicilTypes'), (type) =>
        _.includes(CODICIL_TYPE_IDS, type.id)));

    // Sort codicil types
    _.set(data, 'service.codicilTypes', _.sortBy(
      _.get(data, 'service.codicilTypes'), (type) =>
        _.indexOf(CODICIL_TYPE_IDS, type.id)));

    _.forEach(referenceDataMap, (value, key) =>
      _.set(this, key, _.get(data, value)));

    // Watch for due date changes
    if (this.filters.dueDate) {
      this._$scope.$watch('vm.parameters.dueDateMin', (newval) =>
        this.filters.dueDate.active = newval || this.parameters.dueDateMax
      );

      this._$scope.$watch('vm.parameters.dueDateMax', (newval) =>
        this.filters.dueDate.active = this.parameters.dueDateMin || newval
      );
    }

    this.setTypeFilters();
    this.setDefaultValues();
    this.setActiveFilters();
  }

  setTypeFilters() {
    this.typeFilters = _.map(this.codicilTypes, (type) => {
      const categories = _.filter(this.codicilCategories, (category) =>
        _.isEqual(type.id, _.get(category, 'codicilTypeId.id')));

      return Reflect.construct(CodicilType, [type, categories]);
    });

    if (!this.codicilType || this.codicilType === 'defect') {
      const defectCategories = _.forEach(this.defectCategories,
        (category) => {
          Reflect.deleteProperty(category, 'selected');
        });
      const defect = Reflect.construct(CodicilType, [{
        name: 'Defect'
      }, defectCategories]);

      this.typeFilters.push(defect);
    }

    if (this.codicilType) {
      this.typeFilters = _.filter(this.typeFilters, { slug: this.codicilType });
    }
  }

  setActiveFilters() {
    _.forEach(this.filters, this.toggleFilterIcon.bind(this));
  }

  setDefaultValues(cocStatus) {
    if (_.has(this.searchFilter, 'searchString')) {
      this.parameters.searchString =
        _.get(this.searchFilter, 'searchString');
    }

    if (this.codicilType) {
      _.forEach(this.statusList, (option) => {
        option.selected = _.includes(
          _.get(this.searchFilter, 'statusList'), option.id);
      });

      // Set view type on job related codicil and defect search
      if (_.has(this.searchFilter, 'viewType')) {
        this.viewType =
          _.get(this.searchFilter, 'viewType');
      } else {
        this.viewType = 'all';
      }
    } else {
      // Apply default status list, also placing the open id for defects into
      // the status field
      _.forEach(this.statusList, (superset) => {
        const statuses = _.get(this.searchFilter, 'statusList');

        let activeCodicilStatuses = _.includes(statuses,
          this._cocStatus.get('open')) &&
          !_.includes(statuses, this._defectStatus.get('open')) ?
            _.concat(statuses, this._defectStatus.get('open')) : statuses;

        activeCodicilStatuses = _.includes(statuses,
          this._cocStatus.get('deleted')) &&
          !_.includes(statuses, this._defectStatus.get('cancelled')) ?
            _.concat(statuses, [this._defectStatus.get('closed'),
              this._defectStatus.get('cancelled')]) : activeCodicilStatuses;

        _.set(superset, 'selected', _.isEqual(
          superset.statuses,
          _.intersection(superset.statuses, activeCodicilStatuses))
            );
      });
    }

    _.forEach(this.confidentialityList, (option) => {
      option.selected = _.includes(
        _.get(this.searchFilter, 'confidentialityList'), option.id);
    });

    _.forEach(this.typeList, (option) => {
      option.selected = _.includes(
        _.get(this.searchFilter, 'typeList'), option.id);
    });

    _.forEach(this.typeFilters, (type) => {
      const categories = this.searchFilter.getCategoriesBySlug(type.slug);

      type.selectCategories(categories);
    });

    if (_.has(this.searchFilter, 'itemTypeList')) {
      this.itemType = _.find(this.assetItemTypes, ['id', _.first(
        _.get(this.searchFilter, 'itemTypeList'))]);
    }

    if (_.has(this.searchFilter, 'sdo')) {
      this.sdo = _.find(this.employeeOffices, ['id', _.first(
        _.get(this.searchFilter, 'sdo'))]);
    }

    // Due date
    if (_.has(this.searchFilter, 'dueDateMin')) {
      this.parameters.dueDateMin = this.searchFilter.dueDateMin;
    }
    if (_.has(this.searchFilter, 'dueDateMax')) {
      this.parameters.dueDateMax = this.searchFilter.dueDateMax;
    }
  }

  toggleFilterIcon(filter) {
    if (filter.model === 'itemType' && !_.isEmpty(this.itemType)) {
      // Item Type filter
      this.filters.itemType.active = true;
    } else if (filter.model === 'sdo' && !_.isEmpty(this.sdo)) {
      // SDO filter
      this.filters.sdo.active = true;
    } else if (_.has(filter, 'model')) {
      const parsedInputs = this._$parse(filter.model)(this);
      const param = filter.parameters;

      let actualSearch = [];
      if (_.first(param) === 'statusList') {
        // Status filter
        if (this.codicilType) {
          actualSearch = _.map(_.filter(this.statusList, 'selected'), 'id');
        } else {
          actualSearch = _.uniq(_.flatMap(
            _.filter(this.statusList, 'selected'), 'statuses'));
        }
      } else if (_.first(param) === 'typeList') {
        // Type and category filter
        actualSearch = _.invokeMap(
          _.filter(this.typeFilters, 'selected'),
          'getSelectedCategories');
      } else {
        // Confidentiality filter
        actualSearch = _.map(_.filter(parsedInputs, 'selected'), 'id');
      }
      const defaultSearch = _.pick(this.codicilFilter, param)[_.first(param)];
      filter.active = this.compareArr(actualSearch, defaultSearch);
    }
  }

  /**
   * @param {object} filter to activate, deactivates other filters
   */
  toggleSelectedFilter(filter) {
    _.reject(this.filters, filter).forEach(
      (otherFilter) => otherFilter.selected = false);
    filter.selected = !filter.selected;
  }

  setListProperty(list, filterContext, property, value) {
    _.forEach(list, (item) => {
      _.set(item, property, value);

      // Change the property of categories
      if (item.categories) {
        _.forEach(item.categories, (category) =>
          _.set(category, property, value));
      }
    });

    this.toggleFilterIcon(filterContext);
  }

  getFiltersValidity() {
    return _.every(_.filter(this.filters, 'form'), (filter) =>
        _.get(filter, 'form.$valid'));
  }

  applyFilters() {
    if (!this.getFiltersValidity()) {
      return;
    }

    // If the search string is empty we should pass up null otherwise the
    // search is for codicils/defects with an empty title, which is meaningless
    if (_.isEmpty(this.parameters.searchString)) {
      _.unset(this.parameters, 'searchString');
    }

    this.parameters.confidentialityList = _.map(
      _.filter(this.confidentialityList, 'selected'), 'id');
    if (_.isEmpty(this.parameters.confidentialityList)) {
      this.parameters.confidentialityList = FILTER_NO_RESULTS;
    }

    if (this.codicilType) {
      this.parameters.statusList = _.map(
        _.filter(this.statusList, 'selected'), 'id');
    } else {
      this.parameters.statusList =
        _.uniq(_.flatMap(
          _.filter(this.statusList, 'selected'), 'statuses'));
    }

    this.parameters.typeList = _.invokeMap(
      _.filter(this.typeFilters, 'selected'), (codicilType) =>
        'getSelectedCategories');

    this.parameters.itemTypeList = _.values(
      _.pick(this.itemType, 'id'));

    // Temporary disabled as SDO parameter is not available in BE
    // this.parameters.sdo = _.values(
    //   _.pick(this.sdo, 'id'));

    // This was added as part of LRD-3026 however there's nothing in the BE
    // to support viewType and this has been taken out of the scope of R2.
    if (this.viewType) {
      this.parameters.viewType = this.viewType;
    }

    // Split the parameters into codicil parameters and defect parameters
    // to avoid id's conflicting
    const cocStatuses = this._cocStatus.toObject();
    const defectStatuses = this._defectStatus.toObject();
    const actionableStatuses = this._actionableItemStatus.toObject();
    const assetNoteStatuses = this._assetNoteStatus.toObject();

    const statusCheck = () =>
      _.intersection(this.parameters.statusList, [
        defectStatuses.open,
        cocStatuses.deleted,
        assetNoteStatuses.inactive]);

    if (!_.isEmpty(statusCheck())) {
      const defectParameters = angular.copy(this.parameters);
      if (_.includes(this.parameters.statusList, defectStatuses.open) &&
        this.codicilType !== 'defect') {
         // Remove open defect from parameters statusList and place it in
         // defectParameters
        this.parameters.statusList =
          _.pull(this.parameters.statusList, defectStatuses.open);
        defectParameters.statusList = [defectStatuses.open];
      }

      // The 3 here represents two separate id's (open actionable items
      // and cancelled defects).

      // result checks whether parameters.statusList includes the actionable
      // item open id or the defect closed id
      const result = _.difference([
        actionableStatuses.open
      ], this.parameters.statusList);

      if (_.isEmpty(result)) {
        if (_.includes(this.parameters.statusList, cocStatuses.open) &&
          _.includes(this.parameters.statusList, cocStatuses.deleted)) {
          // This means both open and closed are ticked
          // places the deleted id's for defects into the defectParameters
          defectParameters.statusList =
            _.concat(defectParameters.statusList,
              [defectStatuses.closed]);
        } else if (
          _.includes(this.parameters.statusList, cocStatuses.deleted) &&
          !_.includes(this.parameters.statusList, cocStatuses.open)) {
              // This means that deleted is checked but open isn't
              // Removes the defect cancelled id from parameters statusList
              // to prevent it from conflicting with actionable items id's
              // and places it in defectParameters
          this.parameters.statusList =
            _.pull(this.parameters.statusList);
          defectParameters.statusList = [defectStatuses.closed];
        }
      }

      if (_.includes(this.parameters.statusList, assetNoteStatuses.inactive)) {
        if (!_.includes(this.parameters.statusList, cocStatuses.deleted)) {
          // Removes the actionable item inactive id from the defectParameters
          // and prevents open defects coming through if only inactive is
          // checked
          defectParameters.statusList =
            _.pull(defectParameters.statusList, actionableStatuses.inactive);
        }
      }
      this._$state.go(
        this._$state.current.name, {
          searchFilter: this.parameters,
          defectSearchFilter: defectParameters,
          searched: true
        }, { reload: true }
      );
    } else {
      if (_.isEmpty(this.parameters.statusList)) {
        this.parameters.statusList = FILTER_NO_RESULTS;
      }
      this._$state.go(
        this._$state.current.name, {
          searchFilter: this.parameters,
          defectSearchFilter: this.parameters,
          searched: true
        }, { reload: true }
      );
    }
  }

  /**
   * @return {boolean} returns true if all forms are valid
   */
  checkFilterValidity() {
    return _.every(_.filter(this.filters, 'form'), (filter) =>
      _.get(filter, 'form.$valid'));
  }

   /**
   * @param {string} queryString user typeahead input
   * @return {array} list of matching item types
   */
  findItemTypes(queryString) {
    const regex = this.regEsc(queryString);
    return _.filter(this.assetItemTypes, (item) => regex.test(item.name));
  }

   /**
   * @param {string} queryString user typeahead input
   * @return {array} list of matching item types
   */
  findSdos(queryString) {
    const regex = this.regEsc(queryString);
    return _.filter(this.employeeOffices, (item) => regex.test(item.name));
  }

  /**
   * @param {string} queryString string to escape regex characters
   * @return {string} modified string
   */
  regEsc(queryString) {
    return new RegExp(`^${_.escapeRegExp(queryString)
      .replace(/\\\*/g, '.*')}.*`, 'i');
  }

  resetFilters() {
    this.searchFilter = Reflect.construct(CodicilSearchFilter, [
      this.codicilFilter
    ]);

    // Reset the search string
    _.unset(this.parameters, 'searchString');

    this._$state.go(
      this._$state.current.name, {
        searchFilter: this.setDefaultValues(),
        searched: false
      }, {
        reload: true
      }
    );
  }

  get dateFormat() {
    return this._dateFormat;
  }

  validateDueDateChange(otherField) {
    this.filters.dueDate.active = this.parameters.dueDateMin ||
      this.parameters.dueDateMax;
    otherField.$validate();
  }

  get minSetDate() {
    return _.get(this, 'parameters.dueDateMin') || this.dates.dueDateMin;
  }

  get maxSetDate() {
    return _.get(this, 'parameters.dueDateMax') || this.dates.dueDateMax;
  }
}
