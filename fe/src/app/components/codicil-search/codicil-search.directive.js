import templateUrl from './codicil-search.html';
import CodicilSearchController from
    './codicil-search.controller';

export default () => ({
  restrict: 'AE',
  bindToController: true,
  controllerAs: 'vm',
  controller: CodicilSearchController,
  scope: {
    defaultSearchFilter: '=',
    codicilType: '=',
    codicilFilter: '=',
    defectSearchFilter: '='
  },
  templateUrl
});
