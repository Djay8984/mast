import * as angular from 'angular';

import CodicilSearchDirective from './codicil-search.directive';

export default angular.module('app.component.codicil-search', [])
  .directive('codicilSearch', CodicilSearchDirective)
  .name;
