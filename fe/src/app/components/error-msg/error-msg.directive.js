import errorMsgController from './error-msg.controller';
import errorMsgUrl from './error-msg';

/* @ngInject */
export default () => ({
  controller: errorMsgController,
  controllerAs: 'errorCtrl',
  templateUrl: errorMsgUrl,
  replace: true,
  bindToController: true,
  scope: {
    data: '=errorMsg'
  }
});
