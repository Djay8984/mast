import * as angular from 'angular';
import errorMsg from './error-msg.directive';

export default angular
  .module('app.components.errorMsg', [])
  .directive('errorMsg', errorMsg)
  .name;
