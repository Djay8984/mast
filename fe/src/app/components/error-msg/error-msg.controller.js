import Base from 'app/base.class';
import * as _ from 'lodash';

export default class ErrorMsgController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);
  }

  //  ---------------------------------------------------------
  get showMessage() {
    const ngModel = this.data.ngModel;
    const validate = this.data.validate;

    if (ngModel.$error && validate && validate.oldErrorMsg) {
      _.forEach(validate.oldErrorMsg, (val, key) => {
        if (ngModel.$error[key]) {
          validate.message = val;
          return;
        }
      });
    }
    return ngModel.$invalid && ngModel.$dirty &&
      (ngModel.blur && ngModel.blur.$invalid || validate.blur !== true);
  }
}
