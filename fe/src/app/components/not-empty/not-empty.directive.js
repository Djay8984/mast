import * as _ from 'lodash';

/* @ngInject */
export default () => ({
  require: 'ngModel',
  restrict: 'A',
  link(scope, element, attrs, ngModel) {
    ngModel.$validators.notEmpty = (value) =>
      !(_.isNil(value) || _.isEmpty(value));
  }
});
