import * as angular from 'angular';
import notEmptyDirective from './not-empty.directive';

export default angular
  .module('app.components.not-empty', [])
  .directive('notEmpty', notEmptyDirective)
  .name;
