import * as angular from 'angular';

import assetSubheaderDirective from './asset-subheader.directive';

export default angular
  .module('app.components.asset-subheader ', [])
  .directive('assetSubheader', assetSubheaderDirective)
  .name;
