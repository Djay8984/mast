import AssetSubheaderController from './asset-subheader.controller';
import templateUrl from './asset-subheader.html';

/* @ngInject */
export default () => ({
  bindToController: true,
  controller: AssetSubheaderController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: {
    asset: '='
  },
  templateUrl
});
