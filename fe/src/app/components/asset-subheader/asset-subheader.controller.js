import Base from 'app/base.class';

export default class AssetSubheaderController extends Base {
  /* @ngInject */
  constructor($stateParams, pageService) {
    super(arguments);

    this.attachmentPath = `asset/${$stateParams.assetId}`;
  }

  get data() {
    return this._pageService.data;
  }
}
