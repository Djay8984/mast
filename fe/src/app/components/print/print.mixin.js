import * as _ from 'lodash';
import confirmationTemplateUrl from
  'app/components/modals/templates/confirmation-modal.html';

export default (Base) => class extends Base {
  constructor(args) {
    super(args);

    if (!_.hasIn(this, ('_$rootScope', '_$scope', '_printService'))) {
      this._$log.error(
        `You will need to inject $rootScope, $scope and printService',
        ' for the print mixin to work`
       );
    }

    const printListener = this._$rootScope.$on('print', (event, stateName) => {
      this.print(stateName);
    });

    this.stateName = this._printService.stateName;
    this._printService.pagesToPrint(this.stateName, 'add');

    this._$scope.$on('$destroy', (event, payload) => {
      this._printService.pagesToPrint(this.stateName, 'remove');
      printListener();
    });
  }

  isPrintable(data) {
    return _.get(data, 'confidentialityType.id') !==
      this._printService._confidentialityType.get('lrInternal');
  }

  async print(stateName) {
    if (!this.isPrintable(this.printPayload)) {
      await this._MastModalFactory({
        templateUrl: confirmationTemplateUrl,
        scope: {
          title: 'Confidential',
          message: `Not printing this page as it is LR only`,
          actions: [
            { name: 'OK', result: true }
          ]
        },
        class: 'dialog'
      }).activate();
    } else {
      return this._printService.print(this.printPayload, stateName);
    }
    return false;
  }
};
