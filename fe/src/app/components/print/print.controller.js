import * as _ from 'lodash';
import Base from 'app/base.class';

export default class PrintController extends Base {
  /* @ngInject */
  constructor(
    $log,
    Hotkeys,
    printService,
    $scope,
    $rootScope,
    $state
  ) {
    super(arguments);

    this.toggleButton(this._printService.printables);

    this._$rootScope.$on('showPrintButton', (event, printables) => {
      this.toggleButton(printables);
    });

    this.setupHotkeys();
  }

  toggleButton(printables) {
    this.showButton = _.includes(printables, this._$state.current.name);
  }

  async setupHotkeys() {
    const hotkey = this._Hotkeys.createHotkey({
      key: 'ctrl+p',
      callback: () => {
        this.print();
      }
    });

    this._Hotkeys.registerHotkey(hotkey);

    this._$scope.$on('$destroy', () => {
      this._Hotkeys.deregisterHotkey(hotkey);
    });
  }

  print() {
    if (this.showButton) {
      this._$rootScope.$broadcast('print', this._$state.current.name);
    }
  }
}
