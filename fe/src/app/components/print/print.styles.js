
const header =
  {
    fontSize: 18,
    bold: true,
    padding: [15, 10, 15, 10],
    color: 'white',
    background: 'black'
  };

const footer = {
  fontSize: 10,
  bold: false,
  alignment: 'center',
  margin: [15, 15, 15, 15]
};

const heading = {
  fontSize: 18,
  bold: true,
  margin: [0, 0, 0, 20]
};

const heading2 = {
  fontSize: 16,
  bold: true,
  margin: [0, 20, 0, 10],
  color: '#23517e'
};

const small = {
  fontSize: 11
};

const bold = {
  bold: true
};

const table = {
  margin: [0, 20]
};

const tableHeader = {
  bold: true,
  color: '#23517e',
  fillColor: '#EDF0F2'
};

const assetHeader = {
  margin: [0, 2, 0, 0],
  fontSize: 10
};

const assetHeaderLabel = {
  fontSize: 8,
  italics: true,
  color: '#23517e'
};

const assetHeaderValue = {
  margin: [0, 0, 0, 10],
  bold: true
};

function setStyles(definition) {
  definition.styles = {
    header,
    footer,
    heading,
    heading2,
    small,
    bold,
    table,
    tableHeader,
    assetHeader,
    assetHeaderLabel,
    assetHeaderValue
  };
}

export default setStyles;
