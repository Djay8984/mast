import * as _ from 'lodash';
import * as images from '../print.images';

export default (Base) => class PrintTemplatesLayout extends Base {

  constructor(toolset) {
    super(toolset);
    _.forEach(toolset, (v, k) => {
      this[k] = v;
    });
  }

  parseField(field, keyPath, defaultValue) {
    return _.get(field, keyPath, defaultValue);
  }

  getBold(text) {
    return { text, fontSize: 15, bold: true };
  }

  /** Sets the header image for the first page of the PDF
   * @returns {void}
  */
  setHeader() {
    this.printHelper.addParagraph({
      image: images.logo,
      width: 100,
      height: 100,
      alignment: 'center',
      margin: [0, 0, 0, 20]
    });
  }

  /** Sets the footer that appears at the bottom of every page,
   * includes page number and date
   * @returns {void}
  */
  setFooter() {
    const date = this.printHelper.formatDate(new Date());
    this.printHelper.definition.footer = (currentPage, pageCount) => ({
      style: 'footer',
      columns: [
        {
          text: `Page ${currentPage} of ${pageCount}`,
          alignment: 'left'
        },
        {
          text: date,
          alignment: 'right'
        }
      ]
    });
  }
};
