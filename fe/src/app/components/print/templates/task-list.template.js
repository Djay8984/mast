import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class TaskListTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'Service code', width: '*' },
      { label: 'Task number', width: '40%' },
      { label: 'Action', width: '40%' }
    ];
  }

  getTemplate(item) {
    return [
      `${item.serviceCode}`,
      `${item.taskNumber}`,
      `${item.workItemAction.name}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
