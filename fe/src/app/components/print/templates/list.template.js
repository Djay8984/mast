import * as _ from 'lodash';
import AIListItemTemplate from './list-item.ai.template';
import ANListItemTemplate from './list-item.an.template';
import CocListItemTemplate from './list-item.coc.template';
import DefectListItemTemplate from './list-item.defect.template';
import RepairListItemTemplate from './list-item.repair.template';
import ScheduleTaskTemplate from './schedule-tasks.template';
import SurveyTemplate from './survey.template';
import TaskListTemplate from './task-list.template';

export default class ListTemplate {
  constructor(toolset) {
    this._$q = toolset.$q;
    this.templates = {
      'actionableItems': new AIListItemTemplate(toolset),
      'assetNotes': new ANListItemTemplate(toolset),
      'classification': new SurveyTemplate(toolset),
      'coc.view': new CocListItemTemplate(toolset),
      'coc': new CocListItemTemplate(toolset),
      'cocs': new CocListItemTemplate(toolset),
      'crediting.cocs.view': new CocListItemTemplate(toolset),
      'defect': new DefectListItemTemplate(toolset),
      'defects': new DefectListItemTemplate(toolset),
      'jobs:actionable-item': new AIListItemTemplate(toolset),
      'jobs:asset-note': new ANListItemTemplate(toolset),
      'jobs:coc': new CocListItemTemplate(toolset),
      'jobs:defect': new DefectListItemTemplate(toolset),
      'mms': new SurveyTemplate(toolset),
      'others:actionable-item': new AIListItemTemplate(toolset),
      'others:asset-note': new ANListItemTemplate(toolset),
      'others:coc': new CocListItemTemplate(toolset),
      'others:defect': new DefectListItemTemplate(toolset),
      'repair': new RepairListItemTemplate(toolset),
      'repairs': new RepairListItemTemplate(toolset),
      'scheduleTasks': new ScheduleTaskTemplate(toolset),
      'statutory': new SurveyTemplate(toolset),
      'tasks': new TaskListTemplate(toolset)
    };
  }

  async getLine(item, type, body) {
    body.push(await this.templates[type].getTemplate(item));
    return body;
  }

  async getList(items, type) {
    const body = [];

    // get table header
    const tableHeader = this.templates[type].getTableHeader();
    if (_.compact(tableHeader).length) {
      body.push(_.map(tableHeader, (text) => ({
        text,
        style: 'tableHeader'
      })));
    }

    // get table body
    await this._$q.all(
      _.map(items, (item) => this.getLine(item, type, body))
    );

    return {
      style: 'table',
      table: {
        headerRows: 1,
        widths: this.templates[type].getTableHeaderWidths(),
        body
      },
      layout: {
        hLineWidth: (i, node) => 1,
        vLineWidth: (i, node) => 1,
        hLineColor: (i, node) => '#e7ebee',
        vLineColor: (i, node) => '#e7ebee',
        paddingTop: (i, node) => 5,
        paddingBottom: (i, node) => 5
      }
    };
  }
}
