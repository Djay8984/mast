export default class DefectListItemTemplate {
  constructor(args) {}

  getTemplate(item) {
    const statusName = item.defectStatus.name ?
      item.defectStatus.name : 'Unavailable';
    return [ `${item.title}\n${item.id}`,
      `${item.defectCategory.name}`,
      `Prompt & Thorough`,
      `${item.repairCount} repair`,
      `${statusName}`];
  }
}
