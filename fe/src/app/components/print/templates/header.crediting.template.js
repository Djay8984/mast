import * as images from '../print.images';

export default class TemplateBase {
  constructor(args) {}

  getTemplate(item) {
    return {
      columns: [
        {
          stack: [
            {
              image: images.logo,
              width: 85,
              height: 85
            }
          ],
          width: 120
        },
        {
          stack: [
            { text: 'Asset Name: ', style: 'assetHeaderLabel' },
            { text: `${item.asset.name}`, style: 'assetHeaderValue' },
            { text: 'Asset ID: ', style: 'assetHeaderLabel' },
            { text: `${item.asset.id.toString()}`, style: 'assetHeaderValue' },
            { text: 'IMO Number: ', style: 'assetHeaderLabel' },
            { text: `${item.asset.ihsAsset.id}`, style: 'assetHeaderValue' },
            { text: 'ETA: ', style: 'assetHeaderLabel' },
            { text: `${item.etaDate}`, style: 'assetHeaderValue' },
            { text: 'ETD: ', style: 'assetHeaderLabel' },
            { text: `${item.etdDate}`, style: 'assetHeaderValue' }

          ],
          style: 'assetHeader'
        },
        {
          stack: [
            { text: 'Job ID: ', style: 'assetHeaderLabel' },
            { text: `${item.id.toString()}`, style: 'assetHeaderValue' },
            { text: 'Description: ', style: 'assetHeaderLabel' },
            { text: `${item.description}`, style: 'assetHeaderValue' },
            { text: 'Location: ', style: 'assetHeaderLabel' },
            { text: `${item.location}`, style: 'assetHeaderValue' },
            { text: 'Print Date: ', style: 'assetHeaderLabel' },
            { text: `${item.date}`, style: 'assetHeaderValue' },
            { text: 'Document Name: ', style: 'assetHeaderLabel' },
            { text: `Job Crediting`, style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        }
      ]
    };
  }
}
