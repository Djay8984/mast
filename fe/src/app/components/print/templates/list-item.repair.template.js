import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class RepairListItemTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'ID', width: 35 },
      { label: 'Description', width: '*' },
      { label: 'Total repairs', width: 40 },
      { label: 'Date updated', width: 45 }
    ];
  }

  getTemplate(item) {
    return [
      `${_.padStart(item.id, 5, 0)}`,
      `${item.repairTypeDescription}`,
      `${item.repairs.length}`,
      `${this.printHelper.formatDate(item.updatedDate)}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
