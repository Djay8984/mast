import * as images from '../print.images';

export default class TemplateBase {
  constructor(args) {}

  getTemplate(item) {
    return {
      columns: [
        {
          stack: [
            {
              image: images.logo,
              width: 85,
              height: 85
            }
          ],
          width: 120
        },
        {
          stack: [
            { text: 'Job ID: ', style: 'assetHeaderLabel' },
            { text: `${item.job.id}`, style: 'assetHeaderValue' },
            { text: 'Asset Name (Asset ID): ', style: 'assetHeaderLabel' },
            { text: `${item.name} (${item.id.toString()})`,
              style: 'assetHeaderValue' },
            { text: 'IMO Number: ', style: 'assetHeaderLabel' },
            { text: `${item.IMOnumber}`, style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        },
        {
          stack: [
            { text: 'Print Date: ', style: 'assetHeaderLabel' },
            { text: `${item.date}`, style: 'assetHeaderValue' },
            { text: 'User Name: ', style: 'assetHeaderLabel' },
            { text: `${item.userName}`, style: 'assetHeaderValue' },
            { text: 'Location: ', style: 'assetHeaderLabel' },
            { text: `${item.job.location}`, style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        }
      ]
    };
  }
}
