import AssetInfoTemplate from './header.asset-info.template';
import CodicilsDefectTemplate from './header.codicils-defect.template';
import CreditingTemplate from './header.crediting.template';
import JobCodicilsTemplate from './header.job-codicils.template';
import ScheduleTaskTemplate from './header.schedule-task.template';

export default class HeaderTemplate {
  constructor(toolset) {
    this.templates = {
      'asset-info': new AssetInfoTemplate(toolset),
      'codicils-defect': new CodicilsDefectTemplate(toolset),
      'job-codicils': new JobCodicilsTemplate(toolset),
      'crediting': new CreditingTemplate(toolset),
      'scheduleTasks': new ScheduleTaskTemplate(toolset)
    };
  }

  getDetails(item, type) {
    return this.templates[type].getTemplate(item);
  }
}
