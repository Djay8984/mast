import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DefectListItemTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'ID', width: 35 },
      { label: 'Category', width: 50 },
      { label: 'Title', width: '*' },
      { label: 'Status', width: 40 },
      { label: 'Total repairs', width: 40 }
    ];
  }

  getTemplate(item) {
    return [
      `${_.padStart(item.id, 5, 0)}`,
      `${_.get(item, 'defectCategory.name', '') || ''}`,
      `${item.title}`,
      `${_.get(item, 'defectStatus.name', 'Unavailable') || 'Unavailable'}`,
      `${item.repairCount}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
