import DetailsCOCTemplate from './details.coc.template';
import DetailsAITemplate from './details.ai.template';
import DetailsANTemplate from './details.an.template';
import DetailsDefectTemplate from './details.defect.template';


export default class DetailsTemplate {
  constructor(toolset) {
    this.templates = {
      'ai': new DetailsAITemplate(toolset),
      'an': new DetailsANTemplate(toolset),
      'coc': new DetailsCOCTemplate(toolset),
      'defect': new DetailsDefectTemplate(toolset)
    };
  }

  getDetails(item, type) {
    const body = this.templates[type].getTemplate(item);

    return {
      style: 'table',
      table: {
        headerRows: 0,
        widths: ['15%', '*'],
        margin: [20, 40, 0, 0],
        body
      },
      layout: {
        hLineWidth: (i, node) => 1,
        vLineWidth: (i, node) => 1,
        hLineColor: (i, node) => '#e7ebee',
        vLineColor: (i, node) => '#e7ebee',
        paddingTop: (i, node) => 5,
        paddingBottom: (i, node) => 5
      }
    };
  }
}
