import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DetailsDefectTemplate extends PrintTemplatesLayout(Base) {

  getTemplate(item) {
    return [
      [{ text: 'ID', style: 'tableHeader' }, _.padStart(item.id, 5, 0)],
      [{ text: 'Title', style: 'tableHeader' }, item.title],
      [
        { text: 'Defect category', style: 'tableHeader' },
        _.get(item, 'defectCategory.name', '')
      ],
      [
        { text: 'Defect status', style: 'tableHeader' },
        _.get(item, 'defectStatus.name', '')
      ],
      [
        { text: 'Courses of action', style: 'tableHeader' },
        item.courseOfActionCount.toString()
      ],
      [
        { text: 'Repair count', style: 'tableHeader' },
        item.repairCount.toString()
      ]
    ];
  }
}
