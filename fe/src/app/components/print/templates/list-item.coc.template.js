import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class CocListItemTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'ID', width: 35 },
      { label: 'Category', width: 50 },
      { label: 'Title', width: '*' },
      { label: 'Status', width: 40 },
      { label: 'Imposed date', width: 45 },
      { label: 'Due date', width: 45 },
      { label: 'Narrative', width: '*' },
      { label: 'Amended date', width: 45 },
      { label: 'Amended by', width: 45 }
    ];
  }

  async getCodicilStatus(item) {
    this.codicilStatuses = _.get(
      await this.refService.get(['service.codicilStatuses']),
      'service.codicilStatuses');

    return _.get(_.find(this.codicilStatuses, ['id', item.status.id]), 'name');
  }

  async getTemplate(item) {
    const statusName = await this.getCodicilStatus(item);

    return [
      `${_.padStart(item.id, 5, 0)}`,
      `${_.get(item, 'category.name', '') || ''}`,
      `${item.title}`,
      statusName,
      `${this.printHelper.formatDate(item.imposedDate)}`,
      `${this.printHelper.formatDate(item.dueDate)}`,
      `${_.get(item, 'description', '') || ''}`,
      `${this.printHelper.formatDate(item.updatedDate)}`,
      `${_.get(item, 'updatedBy', '') || ''}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
