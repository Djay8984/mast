import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DetailsANTemplate extends PrintTemplatesLayout(Base) {

  getTemplate(item) {
    return [
      [{ text: 'ID', style: 'tableHeader' }, item.id.toString()],
      [{ text: 'Title', style: 'tableHeader' }, item.title],
      [
        { text: 'Category', style: 'tableHeader' },
        _.get(item, 'category.name', '')],
      [
        { text: 'Item', style: 'tableHeader' },
        _.get(item, 'assetItem.name', '')],
      [
        { text: 'Description', style: 'tableHeader' },
        item.description
      ],
      [
        { text: 'Surveyour guidance', style: 'tableHeader' },
        item.surveyorGuidance
      ],
      [
        { text: 'Imposed date', style: 'tableHeader' },
        this.printHelper.formatDate(item.imposedDate)
      ],
      [
        { text: 'Visible for', style: 'tableHeader' },
        _.get(item, 'confidentialityType.name', '')
      ],
      [
        { text: 'Requires approval for change', style: 'tableHeader' },
        `${item.requireApproval ? 'Yes' : 'No'}`
      ]
    ];
  }
}
