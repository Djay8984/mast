import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ANListItemTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'ID', width: 35 },
      { label: 'Category', width: 50 },
      { label: 'Title', width: '*' },
      { label: 'Status', width: 40 },
      { label: 'Imposed date', width: 45 },
      { label: 'Narrative', width: '*' },
      { label: 'Item name', width: '*' },
      { label: 'Amended date', width: 45 },
      { label: 'Amended by', width: 45 }
    ];
  }

  getTemplate(item) {
    return [
      `${_.padStart(item.id, 5, 0)}`,
      `${_.get(item, 'category.name', '') || ''}`,
      `${item.title}`,
      `${_.get(item, 'status.name', 'Unavailable') || 'Unavailable'}`,
      `${this.printHelper.formatDate(item.imposedDate)}`,
      `${_.get(item, 'description', '') || ''}`,
      `${_.get(item, 'assetItem.name', '') || 'None'}`,
      `${this.printHelper.formatDate(item.updatedDate)}`,
      `${_.get(item, 'updatedBy', '') || ''}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
