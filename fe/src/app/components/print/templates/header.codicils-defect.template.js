import * as images from '../print.images';

export default class TemplateBase {
  constructor(args) {}

  getTemplate(item) {
    const stack = [];
    stack.push({ text: 'Asset Name: ', style: 'assetHeaderLabel' });
    stack.push({ text: `${item.name}`, style: 'assetHeaderValue' });
    stack.push({ text: 'Asset ID: ', style: 'assetHeaderLabel' });
    stack.push({ text: `${item.id.toString()}`, style: 'assetHeaderValue' });
    stack.push({ text: 'IMO Number: ', style: 'assetHeaderLabel' });
    stack.push({ text: `${item.IMOnumber}`, style: 'assetHeaderValue' });
    return {
      columns: [
        {
          stack: [
            {
              image: images.logo,
              width: 85,
              height: 85
            }
          ],
          width: 120
        },
        {
          stack,
          style: 'assetHeader'
        },
        {
          stack: [
            { text: 'Print Date: ', style: 'assetHeaderLabel' },
            { text: `${item.date}`, style: 'assetHeaderValue' },
            { text: 'User Name: ', style: 'assetHeaderLabel' },
            { text: `${item.userName}`, style: 'assetHeaderValue' },
            { text: 'Document Name: ', style: 'assetHeaderLabel' },
            { text: 'Job Codicils List', style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        }
      ]
    };
  }
}
