import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintHelper from '../print.helper';
import defineStyles from '../print.styles';

import { lrLogo } from '../print.images';
import { reportDisclaimer } from '../print.text';

import Asset from 'app/models/asset.model';
import Report from 'app/models/report.model';
import Task from 'app/models/task.model';


export default class JobReportTemplate extends Base {

  /* @ngInject */
  constructor(
    $log,
    $state,
    $stateParams,
    moment,
    assetDecisionService,
    codicilDecisionService,
    creditingDecisionService,
    jobDecisionService,
    referenceDataService,
    reportDecisionService,
    actionableItemStatus,
    appConfig,
    assetNoteStatus,
    confidentialityType,
    dateFormat,
    node,
    reportType
  ) {
    super(arguments);
    this.helper = new PrintHelper($log, dateFormat, moment);
    this.matchState = 'reporting.view';

    this.types = {
      report: this._reportType.toObject()
    };

    this.helper.definition.pageOrientation = 'portrait';
    defineStyles(this.helper.definition);
  }

  getActionableItemActionTaken(codicil) {
    const closed = _.values(_.pick(this._actionableItemStatus.toObject(),
      ['deleted', 'cancelled', 'closedForCurrentJob']));

    return _.includes(closed, codicil.status.id) ? 'Closed' : 'Raised';
  }

  async getReport() {
    if (!this._report) {
      let report = {};

      if (this._$stateParams.reportId && this._$stateParams.jobId) {
        report = this._report = await this._reportDecisionService
          .get(this.jobId, this.reportId);
      } else if (_.isObject(this._$stateParams.report)) {
        report = this._$stateParams.report;
      }
      this._report = Reflect.construct(Report, [report]);
    }

    return this._report;
  }

  prepareServicesAndTasks() {
    _.forEach(this.tasks, (task) => {
      const survey = _.find(this.services, { id: task.survey.id });
      const related = _.get(survey, 'relatedTasks', []);
      related.push(task);
      _.set(survey, 'relatedTasks', related);
    });
  }

  async prepareTasks(services) {
    const job = await this._jobDecisionService.get(this.job.id);
    const crediting = await this._creditingDecisionService.get(this.job.id);
    const serviceTasks = this._creditingDecisionService
      .getServiceTypeTaskCompletions(job, crediting);

    _.forEach(serviceTasks, (task) => {
      const service = _.find(services, { id: _.first(task.surveys).id });
      _.set(service, 'tasksCompletion', task.tasksCompletion);
    });

    // ensure that we only return task models
    return _.filter(_.flatMap(serviceTasks, 'tasks'), (task) =>
      task instanceof Task);
  }

  async print(name = 'JobReport') {
    try {
      this.report = await this.getReport();

      this.job = await this._jobDecisionService
        .get(this.jobId).then(this._jobDecisionService.hydrateSurveys);

      this.crediting = await this._creditingDecisionService.get(this.jobId);

      this.services = await this._jobDecisionService
        .getSurveysForJob(this.jobId);

      this.tasks = !_.isEmpty(this.services) ?
        await this._reportDecisionService.generateTasks(this.services) : [];

      if (!_.isEmpty(this.tasks) && !_.isEmpty(this.services)) {
        this.prepareServicesAndTasks();
        await this.prepareTasks(this.services);
      }

      const asset = await this._assetDecisionService.get(this.assetId);
      this.asset = Reflect.construct(Asset, [asset]);

      this._referenceData = await this._referenceDataService.get([
        'job.classRecommendations',
        'service.serviceCatalogues',
        'service.serviceCreditStatuses'
      ]);

      this.user = await this._node.getUser();

      this.filterAssetNotes = _.flow([
        this.filterChangeRecommended
      ]);

      this.filterActionableItems = this.filterAssetNotes;
      this.filterConditionsOfClass = null;
      this.filterServices = null;

      switch (this.reportTypeId) {
        case this.types.report.dar:
          this.printDAR();
          break;
        case this.types.report.far:
          this.printFAR();
          break;
        case this.types.report.dsr:
          this.printDSR();
          break;
        case this.types.report.fsr:
          this.printFSR();
          break;
        default:
          this._$log.error('no matching report type');
          break;
      }
      this.helper.download(this.filename);
    } catch (e) {
      this._$log.error(`Unable to generate PDF for ${name}`, e);
    }
  }

  printFAR() {
    this.setHeader();
    this.setPageTitle('FINAL ATTENDANCE REPORT');
    this.setPageSubTitle('Provisional Issue');

    this.filterAssetNotes = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.filterActionableItems = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.generateServiceDetails();
    this.generateClassRecommendation();
    this.generateSurveysCredited();
    this.generateJobScope();
    this.generateConditionsOfClass();
    this.generateActionableItems();
    this.generateAssetNotes();
    this.generateSurveyors();
  }

  printDAR() {
    this.setHeader();
    this.setPageTitle('DAILY ATTENDANCE REPORT');
    this.setPageSubTitle(this._moment(this.report.issueDate)
      .format('dddd, Do MMM YYYY'));

    this.filterAssetNotes = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.filterActionableItems = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.filterConditionsOfClass = _.flow([
      this.filterByCurrentUser,
      this.filterUpdatedToday
    ]);

    this.filterJobScope = _.flow([
      this.filterByCurrentUser,
      this.filterUpdatedToday
    ]);

    this.filterServices = _.flow([
      this.filterByCurrentUser,
      this.filterUpdatedToday
    ]);

    this.generateServiceDetails();
    this.generateJobScope();
    this.generateTasksCredited();
    this.generateConditionsOfClass();
    this.generateActionableItems();
    this.generateAssetNotes();
    this.generateNotes();
    this.generateSurveyors();
  }

  printDSR() {
    this.setHeader();
    this.setPageTitle('DAILY SURVEY REPORT');

    this.filterAssetNotes = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.filterActionableItems = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.generateServiceDetails();
    this.generateClassRecommendation();
    this.generateSurveysCredited();
    this.generateConditionsOfClass();
    this.generateActionableItems();
    this.generateAssetNotes();
    this.generateNarrative();
    this.generateSurveyors();
  }

  printFSR() {
    this.setHeader();
    this.setPageTitle('FINAL SURVEY REPORT');

    this.filterAssetNotes = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.filterActionableItems = _.flow([
      this.filterByCurrentUser,
      this.filterConfidential,
      this.filterChangeRecommended,
      this.filterUpdatedToday
    ]);

    this.generateServiceDetails();
    this.generateClassRecommendation();
    this.generateSurveysCredited();
    this.generateConditionsOfClass();
    this.generateActionableItems();
    this.generateAssetNotes();
    this.generateNarrative();
    this.generateSurveyors();
  }

  /**
   * @returns {string} the report version for the header
   */
  get reportVersion() {
    const version = _.get(this.report, 'reportVersion');
    return version ? `Version ${version}` : '';
  }

  /**
   * @param {string} reportType - the type of report we're printing
   * @returns {string} the filename
   */
  get filename() {
    const date = this._moment().format('DDMMMYYYY');
    return `${date}-${this.job.id}-${this.reportTypeName.toUpperCase()}`;
  }

  get reportTypeName() {
    return _.findKey(this._reportType.toObject(), (id) =>
      id === this._$stateParams.reportTypeId);
  }

  get reportTypeId() {
    return this._$stateParams.reportTypeId;
  }

  get creditedTasks() {
    return _.filter(this.services, (service) =>
      _.size(service.relatedTasks));
  }

  get reportId() {
    return this._$stateParams.reportId;
  }

  get jobId() {
    return this._$stateParams.jobId || _.get(this.report, 'job.id');
  }

  get assetId() {
    return this._$stateParams.assetId;
  }

  isReportType(type) {
    return this.reportTypeId === this._reportType.get(type);
  }

  formatDate(date) {
    return this._moment(date).format(this._dateFormat);
  }

  get serviceCatalogue() {
    return _.get(this._referenceData, 'service.serviceCatalogues');
  }

  serviceCode(service) {
    return _.get(_.find(this.serviceCatalogue,
      { 'id': service.serviceCatalogue.id }), 'code');
  }

  get surveyCreditStatuses() {
    return _.get(this._referenceData,
      'service.serviceCreditStatuses');
  }

  surveyStatusCode(service) {
    return _.get(_.find(this.surveyCreditStatuses,
      { id: service.surveyStatus.id }), 'code');
  }

  /**
   * @param {string} text - the title text
   */
  setPageTitle(text) {
    this.helper.addParagraph({
      image: lrLogo,
      width: 70,
      margin: [0, 20]
    });

    this.helper.addParagraph({
      style: {
        fontSize: 18,
        bold: true,
        margin: [0, 0, 0, 20]
      },
      text,
      alignment: 'center'
    });
  }

  /**
   * @summary sets the text beneath the main header
   * @param {string} text - the subtitle text
   */
  setPageSubTitle(text) {
    this.helper.addParagraph({
      style: {
        fontSize: 14,
        bold: true,
        margin: [0, 0, 0, 20]
      },
      text,
      alignment: 'center'
    });
  }

  setHeader() {
    const self = this;
    this.helper.definition.header = function Header(currentPage, pageCount) {
      return [
        {
          style: 'small',
          columns: [
            {
              text: `${self.asset.name} - ${self.asset.imo}`,
              alignment: 'left'
            },
            {
              text: `${self.reportVersion}`,
              alignment: 'right'
            }
          ]
        },
        {
          style: 'small',
          columns: [
            {
              text: `Page ${currentPage} of ${pageCount}`,
              alignment: 'left'
            },
            {
              text: self._moment().format('DD/MMM/YYYY'),
              alignment: 'right'
            }
          ]
        }
      ];
    };
  }

  generateNotes() {
    if (_.isNil(this.report.notes)) {
      return;
    }
    this.helper.addParagraph({
      margin: [0, 10, 0, 0],
      table: {
        widths: ['100%'],
        body: [
          [{
            layout: 'noBorders',
            table: {
              widths: ['100%'],
              body: [
                [{ text: 'Notes', style: 'bold' }],
                [{ text: this.report.notes, style: 'small' }]
              ]
            }
          }]
        ]
      }
    });
  }

  get classRecommendations() {
    return _.get(this._referenceData, 'job.classRecommendations');
  }

  get classRecommendation() {
    return _.find(this.classRecommendations, { id: 9 }).narrativeText;
  }

  generateClassRecommendation() {
    this.helper.addSection('Class Recommendation:');
    this.helper.addParagraph({
      text: this.classRecommendation,
      style: 'small',
      margin: [10, 0]
    });
  }

  generateServiceDetails() {
    this.helper.addSection('Service Details');
    const tableBody = [
      [
        'Ship Name', _.get(this.asset, 'name', ''),
        'Location', this.job.location
      ]
    ];

    const line2 = ['IMO Number', _.toString(this.asset.imo)];

    if (this.job.firstVisitDate) {
      line2.push('First Visit Date', this.formatDate(this.job.firstVisitDate));
    } else {
      line2.push('', '');
    }

    tableBody.push(line2);

    const line3 = [
      'Port of Registry',
      _.get(this.asset, 'registeredPort.name', '')
    ];

    if (this.job.lastVisitDate && !this.isReportType('dar')) {
      line3.push('Last Visit Date', this.formatDate(this.job.lastVisitDate));
    } else {
      line3.push('', '');
    }

    tableBody.push(line3);

    this.helper.addParagraph({
      table: {
        widths: [100, '*', 100, '*'],
        margin: [0, 0, 0, 30],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  generateNarrative() {
    const surveys = _.filter(this.report.surveys, 'narrative');

    if (_.isEmpty(surveys)) {
      return;
    }

    this.helper.addSection('Narrative:');

    _.forEach(surveys, (survey) => {
      const sc = _.get(survey, 'serviceCatalogue');
      this.helper.addParagraph({
        columns: [
          {
            width: 100,
            text: 'Survey Code',
            style: 'bold'
          },
          {
            width: 50,
            text: sc.code
          },
          {
            width: '*',
            text: sc.name
          }
        ]
      });

      this.helper.addParagraph({
        margin: [0, 0, 0, 15],
        columns: [
          {
            width: 100,
            text: `Narrative:`,
            style: 'bold'
          },
          {
            text: survey.narrative
          }
        ]
      });
    });
  }

  generateTasksCredited() {
    if (!_.size(this.creditedTasks)) {
      return;
    }

    this.helper.addSection('Tasks credited:');

    const headers = [
      { text: 'ID', style: 'bold' },
      { text: 'Description', style: 'bold' },
      { text: 'Action taken', style: 'bold', alignment: 'center' }
    ];

    _.forEach(this.creditedTasks, (service) => {
      const taskList = [headers];

      this.helper.addParagraph({
        text: `Survey Code: ${ service.serviceCatalogue.code }`
      });

      _.forEach(service.relatedTasks, (task) => taskList.push([
        _.toString(task.id),
        _.toString(task.longDescription),
        {
          text: _.toString(_.get(task, 'creditStatus.name')),
          alignment: 'center'
        }
      ]));

      this.helper.addParagraph({
        style: 'small',
        table: {
          margin: [0, 0, 0, 30],
          widths: [50, '*', 100],
          body: taskList
        },
        layout: 'noBorders'
      });
    });
  }

  generateSurveysCredited() {
    this.helper.addSection('Surveys credited in this report:');

    const tableBody = [[
      { text: 'Survey Code', style: 'bold' },
      { text: 'Survey Title', style: 'bold' },
      { text: 'Status', style: 'bold', alignment: 'center' },
      { text: 'Credited Date', style: 'bold', alignment: 'center' }
    ]];

    _.forEach(this.report.surveys, (survey) => tableBody.push([
      this.serviceCode(survey),
      survey.name,
      { text: this.surveyStatusCode(survey), alignment: 'center' },
      {
        text: !_.isNull(survey.dateOfCrediting) ?
          this.formatDate(survey.dateOfCrediting) : '',
        alignment: 'center' }
    ]));

    this.helper.addParagraph({
      style: 'small',
      table: {
        margin: [0, 0, 0, 30],
        widths: [100, '*', 100, 100],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  generateSurveyors() {
    this.helper.addSection('Attending Surveyors');

    this.helper.addParagraph({
      text: 'Attending Surveyor Name',
      style: 'bold',
      margin: [0, 0, 0, 50]
    });

    this.helper.addParagraph({
      margin: [0, 0, 0, 15],
      columns: [
        {
          width: '*',
          text: 'Issuing Surveyor Signature',
          style: 'bold'
        },
        {
          width: 200,
          text: 'Signed date',
          style: 'bold'
        }
      ]
    });

    this.helper.addParagraph({
      margin: [0, 0, 0, 15],
      text: 'Issuing Surveyor Name',
      style: 'bold'
    });

    this.helper.addParagraph({
      margin: [0, 0, 0, 20],
      text: 'LR Legal Entity',
      style: 'bold'
    });

    if (this.reportTypeId === this._reportType.get('dar')) {
      this.timeLoggedTable();
      this.helper.horizontalRule();
      this.clientSignature();
    }

    this.helper.addParagraph({
      style: 'bold small',
      text: reportDisclaimer
    });
  }

  clientSignature() {
    this.helper.addParagraph({
      margin: [0, 0, 0, 15],
      columns: [
        {
          width: '*',
          text: 'Client Signature',
          style: 'bold'
        },
        {
          width: 200,
          text: 'Signed date',
          style: 'bold'
        }
      ]
    });

    this.helper.addParagraph({
      margin: [0, 0, 0, 15],
      columns: [
        {
          width: '*',
          text: 'Client Name',
          style: 'bold'
        },
        {
          width: 200,
          text: 'Rank/Position',
          style: 'bold'
        }
      ]
    });
  }

  /**
   * @summary generates a table where surveyors can log time
   */
  timeLoggedTable() {
    const headers = [[
      { text: 'Surveyor', style: 'bold' },
      { text: 'Start Time', style: 'bold' },
      { text: 'End Time', style: 'bold', alignment: 'center' },
      { text: 'Total Hours', style: 'bold', alignment: 'center' },
      { text: 'Overtime Hours', style: 'bold', alignment: 'center' },
      { text: 'Waiting Hours', style: 'bold', alignment: 'center' },
      { text: 'Comments', style: 'bold' }
    ]];

    this.helper.addParagraph({
      style: 'small',
      table: {
        margin: [0, 0, 0, 20],
        widths: [100, 35, 35, 35, 40, 40, '*'],
        body: headers
      },
      layout: 'noBorders'
    });
  }

  generateAssetNotes() {
    const items = this.filterAssetNotes(this.report.assetNotes);
    if (_.isEmpty(items)) {
      return;
    }

    this.helper.addSection('Action taken with Asset Notes:');

    const assetNoteActionTaken = (codicil) => {
      const closed = _.values(_.pick(this._assetNoteStatus.toObject(),
        ['deleted', 'cancelled']));
      return _.includes(closed, codicil.status.id) ? 'Closed' : 'Raised';
    };

    const tableBody = [[
      { text: 'ID', style: 'bold' },
      { text: 'Description', style: 'bold' },
      { text: 'Action taken', style: 'bold', alignment: 'center' }
    ]];

    _.forEach(items, (row) => tableBody.push([
      _.toString(row.id),
      _.toString(row.description),
      { text: assetNoteActionTaken(row), alignment: 'center' }
    ]));

    this.helper.addParagraph({
      style: 'small',
      table: {
        margin: [0, 0, 0, 30],
        widths: [50, '*', 120],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  generateJobScope() {
    const headers = [
      { text: 'Survey Code', style: 'bold' },
      { text: 'Survey Title', style: 'bold' },
      { text: 'Current Progress %', style: 'bold', alignment: 'center' }
    ];

    const tableBody = [headers];

    const items = _.isNull(this.filterServices) ? this.services :
      this.filterServices(this.services);

    if (_.isEmpty(items)) {
      return;
    }

    this.helper.addSection('Agreed scope of work');
    _.forEach(items, (service) => {
      tableBody.push([
        _.toString(_.get(service, 'serviceCatalogue.code')),
        _.toString(service.name),
        {
          text: _.toString(_.get(service, 'tasksCompletion', '0')),
          alignment: 'center'
        }
      ]);
    });

    this.helper.addParagraph({
      style: 'table',
      table: {
        widths: [100, '*', 100],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  generateConditionsOfClass() {
    const items = _.isNull(this.filterConditionsOfClass) ? this.report.cocs :
      this.filterConditionsOfClass(this.report.cocs);

    if (_.isEmpty(items)) {
      return;
    }

    const actionTaken = (item) => {
      if (item.deleted) {
        return 'Closed';
      }

      if (item.actionTaken) {
        return 'Amended';
      }

      if (!item.deleted && !item.actionTaken) {
        return 'Raised';
      }
    };

    const headers = [
      { text: 'Task ID', style: 'bold' },
      { text: 'Task Description', style: 'bold' },
      { text: 'Action Taken', style: 'bold', alignment: 'center' },
      { text: 'Due Date', style: 'bold', alignment: 'center' }
    ];

    const tableBody = [headers];

    _.forEach(items, (item) => tableBody.push([
      _.toString(item.id),
      _.toString(item.description),
      {
        text: actionTaken(item),
        alignment: 'center'
      },
      {
        text: this.formatDate(item.dueDate),
        alignment: 'center'
      }
    ]));

    this.helper.addSection('Action taken with Conditions of Class:');
    this.helper.addParagraph({
      style: 'small',
      table: {
        margin: [0, 0, 0, 30],
        widths: [50, '*', 100, 100],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  generateActionableItems() {
    const headers = [
      { text: 'ID', style: 'bold' },
      { text: 'Description', style: 'bold' },
      { text: 'Action taken', style: 'bold', alignment: 'center' },
      { text: 'Due date', style: 'bold', alignment: 'center' }
    ];

    const tableBody = [headers];
    const items = this.filterActionableItems(this.report.actionableItems);

    const actionTaken = (codicil) => {
      const closed = _.values(_.pick(this._actionableItemStatus.toObject(),
        ['deleted', 'cancelled', 'closedForCurrentJob']));
      return _.includes(closed, codicil.status.id) ? 'Closed' : 'Raised';
    };

    if (_.isEmpty(items)) {
      return;
    }

    _.forEach(items, (item) => tableBody.push([
      _.toString(item.id),
      _.toString(item.description),
      {
        text: actionTaken(item),
        alignment: 'center'
      },
      {
        text: this.formatDate(item.dueDate),
        alignment: 'center'
      }
    ]));

    this.helper.addSection('Action taken with Actionable Items:');
    this.helper.addParagraph({
      style: 'small',
      table: {
        margin: [0, 0, 0, 30],
        widths: [50, '*', 100, 100],
        body: tableBody
      },
      layout: 'noBorders'
    });
  }

  filterConfidential(data) {
    return _.reject(data, (item) =>
      _.get(item, 'confidentialityType.id') ===
        this._confidentialityType.get('lrInternal'));
  }

  filterChangeRecommended(data) {
    return _.reject(data, {
      status: {
        id: this._actionableItemStatus.get('changeRecommended')
      }
    });
  }

  filterByCurrentUser(data) {
    return _.filter(data, { updatedBy: this.user.name });
  }

  filterUpdatedToday(collection) {
    const today = this._moment(this.report.issueDate);

    return _.filter(collection, (item) =>
      today.isSame(item.actionTakenDate, 'day'));
  }
}
