import * as _ from 'lodash';

export default class SurveyTemplate {
  constructor(args) {
    this.columns = [
      { width: 100 },
      { width: 100 },
      { width: '*' },
      { width: 80 }
    ];
  }

  getTemplate(item) {
    const unit = item._crediting;
    const serviceCode = _.get(unit, 'serviceCatalogue.code', '');
    const serviceName = _.get(unit, 'serviceCatalogue.name', '');

    const result = [
      `${unit.name}\n\n`,
      `${serviceName} (${serviceCode})`
    ];

    let temp = '';
    if (unit.items && unit.items.length) {
      temp = this.itemFactory(
        unit.items,
        temp,
        this.itemFactory,
        this.getQuestions
      );
    }
    result.push(temp);
    result.push(`${_.get(unit, 'surveyStatus.name', '')}`);
    return result;
  }

  itemFactory(items, result = ``, itemFactory, getQuestions) {
    items.forEach((subitem, key) => {
      result += `${subitem.name} (ID: ${subitem.id.toString()})\n\n`;

      if ((_.get(subitem, 'model.tasks') || []).length) {
        subitem.model.tasks.forEach((task) => {
          result += `-- Task ID: ${task.model.id}\n`;
          result += `Action: ${task.model.workItemAction.name}\n\n`;
          if (task.attributes && task.attributes.length) {
            result += getQuestions(task.attributes);
          }
        });
      } else if ((_.get(subitem, 'model.items') || []).length) {
        result = itemFactory(
          subitem.model.items,
          result,
          itemFactory,
          getQuestions
        );
      }
    });
    return result;
  }

  getQuestions(questions) {
    let result = ``;
    questions.forEach((question) => {
      result += `---- ${question.model.description}\n\n`;
    });
    return result;
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
