import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class ScheduleTaskTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);

    this.columns = [
      { label: 'Task Number', width: '33%' },
      { label: 'Task Due Date', width: '33%' },
      { label: 'Action', width: '34%' }
    ];
  }

  getTemplate(item) {
    const dueDate = this.printHelper.formatDate(item.dueDate);

    return [
      `${item.taskNumber}`,
      dueDate,
      `${item.workItemAction.name}`
    ];
  }

  getTableHeader() {
    return _.map(this.columns, 'label');
  }

  getTableHeaderWidths() {
    return _.map(this.columns, 'width');
  }
}
