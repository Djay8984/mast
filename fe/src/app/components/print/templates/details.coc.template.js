import * as _ from 'lodash';
import Base from 'app/base.class';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class DetailsCOCTemplate extends PrintTemplatesLayout(Base) {
  constructor(args) {
    super(args);
  }

  getTemplate(item) {
    return [
      [{ text: 'ID', style: 'tableHeader' }, this._buildId(item)],
      [{ text: 'Title', style: 'tableHeader' }, item.title],
      [{ text: 'Description', style: 'tableHeader' }, item.description],
      [
        { text: 'Due date', style: 'tableHeader' },
        this.printHelper.formatDate(item.dueDate)
      ],
      [
        { text: 'Imposed date', style: 'tableHeader' },
        this.printHelper.formatDate(item.imposedDate)],
      [
        { text: 'Visible for', style: 'tableHeader' },
        _.get(item, 'confidentialityType.name', '')
      ]
    ];
  }

  _buildId(item) {
    const pref = _.get(item, 'compositeId', '');
    const id = _.padStart(item._id, 5, 0);
    return `${pref}${id}`;
  }
}
