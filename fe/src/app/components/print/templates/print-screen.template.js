import * as _ from 'lodash';
import Base from 'app/base.class';
import defineStyles from '../print.styles';
import DetailsTemplate from './details.template';
import HeaderTemplate from './header.template';
import ListTemplate from './list.template';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class PrintScreenTemplate extends PrintTemplatesLayout(Base) {

  constructor(toolset) {
    super(toolset);

    const { printHelper, stateName, $log, appConfig, $q } = toolset;
    this.stateName = stateName;
    this._$log = $log;
    this._appConfig = appConfig;
    this.printHelper = printHelper;
    this.printHelper.definition.pageOrientation = 'landscape';
    this._$q = $q;

    this.detailsTemplate = new DetailsTemplate(toolset);
    this.listTemplate = new ListTemplate(toolset);
    this.headerTemplate = new HeaderTemplate(toolset);

    defineStyles(this.printHelper.definition);
  }

  async print(data, _printService) {
    this.setFooter();
    data.date = this.printHelper.formatDate(new Date());
    data.userName = this._appConfig.MAST_USER_FULL_NAME;
    data.IMOnumber = _.get(data, 'ihsAsset.id', '');

    const headerInfo = {
      'asset': this.asset,
      'print': {
        'date': this.printHelper.formatDate(new Date()),
        'userName': this._appConfig.MAST_USER_FULL_NAME
      }
    };

    let filename = `${data.date}`;

    switch (this.stateName) {
      case 'coc.view':
      case 'crediting.cocs.view':
        headerInfo.print.fileName = 'Conditions of Class';
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(headerInfo, 'asset-info'));

        this.printHelper.addTitle(headerInfo.print.fileName);
        this.printHelper.addParagraph(
          this.detailsTemplate.getDetails(data, 'coc'));

        this.printHelper.definition.pageOrientation = 'portrait';

        filename += `-${_.trim(
          _.replace(headerInfo.print.fileName, /\s/g, '-'))}`;
        break;
      case 'assetNote.view':
      case 'crediting.assetNotes.view':
        headerInfo.print.fileName = 'Asset Note';
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(headerInfo, 'asset-info'));

        this.printHelper.addTitle(headerInfo.print.fileName);
        this.printHelper.addParagraph(
          this.detailsTemplate.getDetails(data, 'an'));

        this.printHelper.definition.pageOrientation = 'portrait';

        filename += `-${_.trim(
          _.replace(headerInfo.print.fileName, /\s/g, '-'))}`;
        break;
      case 'actionableItem.view':
      case 'crediting.actionableItems.view':
        headerInfo.print.fileName = 'Actionable Item';
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(headerInfo, 'asset-info'));

        this.printHelper.addTitle(headerInfo.print.fileName);
        this.printHelper.addParagraph(
          this.detailsTemplate.getDetails(data, 'ai'));

        this.printHelper.definition.pageOrientation = 'portrait';

        filename += `-${_.trim(
          _.replace(headerInfo.print.fileName, /\s/g, '-'))}`;
        break;
      case 'defect.view':
      case 'crediting.defects.view':
        headerInfo.print.fileName = 'Defect';
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(headerInfo, 'asset-info'));

        this.printHelper.addTitle(headerInfo.print.fileName);

        this.printHelper.addParagraph(
          this.detailsTemplate.getDetails(data, 'defect'));

        this.printHelper.definition.pageOrientation = 'portrait';

        filename += `-${_.trim(
          _.replace(headerInfo.print.fileName, /\s/g, '-'))}`;
        break;
      case 'crediting.assetNotes.list':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'job-codicils'));
        filename += `-Job-${data.job.id}-Asset-Notes`;
        break;
      case 'crediting.cocs.list':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'job-codicils'));
        filename += `-Job-${data.job.id}-Conditions-of-Classes`;
        break;
      case 'crediting.actionableItems.list':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'job-codicils'));
        filename += `-Job-${data.job.id}-Actionable-Items`;
        break;
      case 'crediting.defects.list':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'job-codicils'));
        filename += `-Job-${data.job.id}-Defects`;
        break;
      case 'item.tasks':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'codicils-defect'));
        filename += '-Task-List';
        break;
      case 'asset.codicilsAndDefects':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'codicils-defect')
        );
        filename += '-Codcils-and-Defects';
        break;
      case 'crediting.manage':
        this.printHelper.addParagraph(
          this.headerTemplate.getDetails(data, 'crediting'));
        filename += '-Job-Crediting';
        break;
      case 'asset.serviceSchedule.viewService':
        this.printHelper.addParagraph(
         this.headerTemplate.getDetails(data, 'scheduleTasks'));
        filename += '-Schedule-TaskList';
        break;
      default:
        throw Error('Unknown state to print');
    }
    filename += '.pdf';
    const generatedContents = {};

    const getAdditionalInfo = async (info) => {
      const type = info.split(':')[0];
      let itemsToPrint =
        _.isArray(data[type]) ? data[type] : _.concat([], data[type]);
      if (data[type] && !_.isNull(data[type][0]) ||
        !_.isNull(data[type]) && data[type].length) {
        itemsToPrint = _.filter(itemsToPrint, (o) =>
          _.get(o, 'confidentialityType.id') !== 1);
        if (itemsToPrint.length) {
          const theList =
            await this.listTemplate.getList(itemsToPrint, info);
          generatedContents[info] = theList;
          return true;
        }
      }
      return false;
    };

    if (data.additionalInfo && data.additionalInfo.length) {
      const withoutEmptyData = _.filter(data.additionalInfo, (info) => {
        const type = info.split(':')[0];
        return data[type] && data[type].length;
      });

      this._$q.all(
        _.map(withoutEmptyData, (info) => getAdditionalInfo(info))
      ).then(() => {
        _.forEach(data.additionalInfo, (info) => {
          if (generatedContents[info]) {
            this.printHelper.addTitle(info.split(':')[0])
              .addParagraph(generatedContents[info]);
          }
        });
        return this.printHelper.download(filename);
      });
    } else {
      return this.printHelper.download(filename);
    }
  }
}
