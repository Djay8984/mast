import * as images from '../print.images';
import * as _ from 'lodash';

export default class TemplateBase {
  constructor(args) {}

  getTemplate(headerInfo) {
    return {
      columns: [
        {
          stack: [
            {
              image: images.logo,
              width: 85,
              height: 85
            }
          ],
          width: 120
        },
        {
          stack: [
            { text: 'Asset Name: ', style: 'assetHeaderLabel' },
            { text: headerInfo.asset.name, style: 'assetHeaderValue' },
            { text: 'Asset ID: ', style: 'assetHeaderLabel' },
            {
              text: headerInfo.asset.id.toString(),
              style: 'assetHeaderValue'
            },
            { text: 'IMO Number: ', style: 'assetHeaderLabel' },
            {
              text: _.get(headerInfo, 'asset.ihsAsset.id', ''),
              style: 'assetHeaderValue'
            }
          ],
          style: 'assetHeader'
        },
        {
          stack: [
            { text: 'Print Date: ', style: 'assetHeaderLabel' },
            {
              text: headerInfo.print.date,
              style: 'assetHeaderValue'
            },
            { text: 'User Name: ', style: 'assetHeaderLabel' },
            { text: headerInfo.print.userName, style: 'assetHeaderValue' },
            { text: 'Document Name: ', style: 'assetHeaderLabel' },
            { text: headerInfo.print.fileName, style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        }
      ]
    };
  }
}
