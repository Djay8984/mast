import * as _ from 'lodash';
import Base from 'app/base.class';
import defineStyles from '../print.styles';
import HeaderTemplate from './header.template';
import PrintTemplatesLayout from './print-templates-layout.template';

/* eslint new-cap: 0, babel/new-cap: 0 */
export default class PrintJobDetails extends PrintTemplatesLayout(Base) {

  constructor(printHelper, stateName, $log, appConfig, jobInfo, $q) {
    super({
      $log,
      $q,
      appConfig,
      printHelper,
      refService: jobInfo.jobReferenceData,
      stateName
    });

    this.stateName = stateName;
    this._$log = $log;
    this._appConfig = appConfig;
    this.printHelper = printHelper;
    this.printHelper.definition.pageOrientation = 'portrait';
    this.jobInfo = jobInfo;
    this.headerTemplate = new HeaderTemplate(arguments);

    defineStyles(this.printHelper.definition);
  }

  /** Lookup the text of the given job status ID
   * @returns {string} the status
   * @param {integer} id - the job status ID
   * @param {object} data - An object containing refData for jobs
  */
  async getJobStatus(id, data) {
    const statuses = data.job.jobStatuses;
    const result = _.find(statuses, { id });
    return result.name;
  }

  /** Generate summary of job
   * details for specific job
   * @returns {void}
  */
  async generateJobDetails() {
    this._$log.debug(`Define 'Print Job Details'`);
    this.setFooter();

    const {
      asset,
      job,
      currentUser,
      jobReferenceData
    } = this.jobInfo;

    // Set document properties
    this.printHelper.definition.pageOrientation = 'landscape';
    const filename = `Job-Details-Asset-${asset.id}-Job-${job.id}`;

    // Create the summary info
    const headerInfo = {
      asset,
      'print': {
        'date': this.printHelper.formatDate(new Date()),
        'userName': currentUser.fullName
      }
    };

    headerInfo.print.fileName = 'Job Details';
    this.printHelper.addParagraph(
      this.headerTemplate.getDetails(headerInfo, 'asset-info'));

    this.printHelper.addTitle(headerInfo.print.fileName);

    const jobStatus =
      await this.getJobStatus(job.jobStatus.id, jobReferenceData);
    const reqDate = this.printHelper.formatDate(job.requestedAttendanceDate);
    const eta = this.printHelper.formatDate(job.etaDate);
    const etd = this.printHelper.formatDate(job.etdDate);
    const summaryTable = {
      style: 'table',
      table: {
        headerRows: 1,
        widths: ['*', '30%'],
        body: [
          [`Job Number: ${_.padStart(job.id, 9, '0')}`, `Job Notes`],
          [
            `Estimated Arrival Date: ${eta}`,
            { text: job.note, rowSpan: 9 }
          ],
          [`Estimated Departure Date: ${etd}`],
          [`Job Status: ${jobStatus}`],
          [`Job Requested Date: ${reqDate}`],
          [`Job Description: ${job.description}`],
          [`Service Delivery Office: ${job.offices[0].office.name}`],
          [`Lead Surveyor: ${job.employees[0].lrEmployee.name}`],
          [`Case Identifier: ${job.aCase.id}`],
          [`Job Location: ${job.location}`]
        ]
      },
      layout: 'lightVerticalLines'
    };

    this.printHelper.addParagraph(summaryTable);

    this.printHelper.download(filename);
  }
}
