import * as images from '../print.images';

export default class TemplateBase {
  constructor(args) {}

  getTemplate(item) {
    return {
      columns: [
        {
          stack: [
            {
              image: images.logo,
              width: 85,
              height: 85
            }
          ],
          width: 120
        },
        {
          stack: [
            { text: 'Asset Name: ', style: 'assetHeaderLabel' },
            { text: `${item.name}`, style: 'assetHeaderValue' },
            { text: 'Asset ID: ', style: 'assetHeaderLabel' },
            { text: `${item.id.toString()}`, style: 'assetHeaderValue' },
            { text: 'IMO Number: ', style: 'assetHeaderLabel' },
            { text: `${item.IMOnumber}`, style: 'assetHeaderValue' },
            { text: 'Document Name: ', style: 'assetHeaderLabel' },
            { text: `Schedule Tasks`, style: 'assetHeaderValue' }

          ],
          style: 'assetHeader'
        },
        {
          stack: [
            { text: 'Service code: ', style: 'assetHeaderLabel' },
            { text: `${item.serviceSchedule.code}`, style: 'assetHeaderValue' },
            { text: 'Service name: ', style: 'assetHeaderLabel' },
            { text: `${item.serviceSchedule.name}`, style: 'assetHeaderValue' },
            { text: 'User Name: ', style: 'assetHeaderLabel' },
            { text: `${item.userName}`, style: 'assetHeaderValue' },
            { text: 'Print Date: ', style: 'assetHeaderLabel' },
            { text: `${item.date}`, style: 'assetHeaderValue' }
          ],
          style: 'assetHeader'
        }
      ]
    };
  }
}
