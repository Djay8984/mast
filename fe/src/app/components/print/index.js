import * as angular from 'angular';
import print from './print.directive';

export default angular
  .module('app.components.print ', [])
  .directive('print', print)
  .name;
