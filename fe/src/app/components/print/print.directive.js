import PrintController from './print.controller';
import templateUrl from './print.html';

/* @ngInject */
export default () => ({
  controller: PrintController,
  controllerAs: 'vm',
  replace: true,
  restrict: 'AE',
  scope: true,
  templateUrl,
  transclude: true
});
