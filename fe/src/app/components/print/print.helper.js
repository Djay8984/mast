import 'pdfmake-client/pdfmake/build/pdfmake';
import 'pdfmake-client/pdfmake/build/vfs_fonts';
import * as _ from 'lodash';

export default class PrintHelper {
  constructor(
    $log,
    $window,
    dateFormat,
    moment
  ) {
    this.$log = $log;
    this.dateFormat = dateFormat;
    this.moment = moment;
    this.pdf = $window.pdfMake;
    this.reset();
  }

  /** Helper function to format the supplied date,
   * return '' on not value
   * @returns {string} the formatted date
   * @param {string} date - the ISO date to format
  */
  formatDate(date) {
    return date ?
      this.moment(date).format(this.dateFormat) : '';
  }

  toString(value) {
    return value || '';
  }

  /** Clears the definition
   * @returns {void}
  */
  reset() {
    this.definition = {
      content: [],
      styles: []
    };
  }

  /** Creates the PDF from the definition and print
   * @returns {void}
  */
  print() {
    this.pdf.createPdf(this.definition).print();
    this.reset();
  }

  /** Creates the PDF from the definition and open in a new
   * browser window
     * @returns {void}
  */
  open() {
    this.pdf.createPdf(this.definition).open();
    this.reset();
  }

  /** Creates the PDF from the definition and download in the
   * browser using the given filename
   * @param {string} filename - The filename of the downloaded file
   * @returns {void}
  */
  download(filename) {
    const locateMissingData = (x, path = []) => {
      if (_.isNil(x)) {
        return path;
      }

      if (!_.isObject(x) || _.isFunction(x)) {
        return null;
      }

      return _.compact(
        _.map(Reflect.ownKeys(x), (key) => {
          const paths = _.compact(locateMissingData(x[key],
            _.concat(path, key)));
          return _.isEmpty(paths) ? null : paths.join('.');
        }));
    };

    const pathsToMissingData = locateMissingData(this.definition);

    if (!_.isEmpty(pathsToMissingData)) {
      this.$log.warn('pathsToMissingData', pathsToMissingData);
      this.$log.warn('Fixing up data to allow generation to proceed');
      _.forEach(pathsToMissingData, (path) =>
        _.set(this.definition, path, {
          text: 'ERROR',
          color: 'red',
          bold: true,
          decoration: 'underline',
          decorationColor: 'red'
        }));
    }

    this.pdf.createPdf(this.definition).download(filename);
    this.reset();
    return true;
  }

  addStyle(name, styleDefinition) {
    this.definition.styles[name] = styleDefinition;
  }

  addTitle(title) {
    this.addParagraph({
      text: `\n${_.upperCase(title)}`,
      style: 'heading2'
    });

    return this;
  }

  addParagraph(content) {
    return this.definition.content.push(content);
  }

  addTable(table) {
    this.definition.content.push({ table });
  }

  /**
   * @param {string} title - the subsection title
   */
  addSection(title) {
    this.definition.content.push({
      style: 'table',
      table: {
        headerRows: 1,
        widths: ['100%'],
        margin: [0, 20, 0, 0],
        body: [
          [{ text: title, style: 'bold' }]
        ]
      }
    });
  }

  horizontalRule() {
    this.definition.content.push({
      table: {
        widths: ['*'],
        body: [[' '], [' ']]
      },
      layout: {
        hLineWidth: (i, node) =>
          i === 0 || i === _.size(node.table.body) ? 0 : 1,
        vLineWidth: (i, node) => 0
      }
    });
  }
}
