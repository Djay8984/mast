import Base from 'app/base.class';
import * as _ from 'lodash';

export default class ReportBannerController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    dateFormat,
    moment,
    referenceDataService,
    reportType
  ) {
    super(arguments);

    this.setUpData();
  }

  async setUpData() {
    this.reportTypes = _.get(
      await this._referenceDataService.get(['job.reportTypes']),
      'job.reportTypes');
  }

  back() {
    if (this.reportNotSubmitted) {
      this._$state.go('crediting.manage');
    } else {
      this._$state.go('asset.jobs.view',
        _.omit(this._$stateParams, 'report'), { reload: true });
    }
  }

  get reportNotSubmitted() {
    return !_.isNil(this._$stateParams.report);
  }

  get type() {
    const type =
      _.find(this.reportTypes, { 'id': this.report.reportType.id });

    return _.get(type, 'name', 'DAR');
  }

  /**
   * @summary reportDate()
   * @description Doubles us as a date reporter for DAR, and a provisional
   *   indicator when it's a FAR
   * @return {String} date or provisional
   */
  get reportDate() {
    const issueDate =
      this._moment(this.report.issueDate).format(this._dateFormat);
    if (_.get(this.report, 'reportType.id') === this._reportType.get('far') ||
      _.get(this.report, 'reportType.id') === this._reportType.get('fsr')) {
      return !_.isNil(this.report.reportVersion) ?
        `V${this.report.reportVersion} (Provisional)   ${issueDate}` :
        `(Provisional)   ${issueDate}`;
    }
    if (_.get(this.report, 'reportType.id') === this._reportType.get('dar') ||
      _.get(this.report, 'reportType.id') === this._reportType.get('dsr')) {
      return !_.isNil(this.report.reportVersion) ?
        `V${this.report.reportVersion}   ${issueDate}` :
        `${issueDate}`;
    }
    return issueDate;
  }
}
