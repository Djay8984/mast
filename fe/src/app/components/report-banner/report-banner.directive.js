import templateUrl from './report-banner.html';
import reportBannerController from './report-banner.controller';

/* @ngInject */
export default () => ({
  bindToController: {
    report: '=',
    backTarget: '@',
    backDisabled: '='
  },
  controller: reportBannerController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
