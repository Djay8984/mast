import * as angular from 'angular';

import reportBannerDirective from './report-banner.directive';

export default angular
  .module('app.components.report-banner', [])
  .directive('reportBanner', reportBannerDirective)
  .name;
