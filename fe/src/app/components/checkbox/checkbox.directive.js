import * as _ from 'lodash';

import templateUrl from './partials/checkbox.template.html';

/* @ngInject */
export default ($parse) => ({
  require: 'ngModel',
  restrict: 'EA',
  scope: {},
  replace: true,
  templateUrl,
  link: (scope, element, attrs, model) => {
    if (!_.isUndefined(attrs.ngClass)) {
      attrs.ngClass = attrs.ngClass.replace(/}\s*{/g, ', ');
    }

    if (!_.isUndefined(attrs.large)) {
      scope.size = 'large';
    } else if (!_.isUndefined(attrs.largest)) {
      scope.size = 'largest';
    } else {
      scope.size = 'default';
    }

    const trueValue = _.isUndefined(attrs.ngTrueValue) ?
      true : $parse(attrs.ngTrueValue)(scope);

    const falseValue = _.isUndefined(attrs.ngFalseValue) ?
      false : $parse(attrs.ngFalseValue)(scope);

    if (!_.isUndefined(scope.name)) {
      element.name = scope.name;
    }

    // Update element when model changes;
    scope.$watch(() => {
      model.$setViewValue(
        model.$modelValue === trueValue || model.$modelValue === true ?
        trueValue : falseValue);
      return model.$modelValue;
    }, (newVal, oldVal) => {
      scope.checked = model.$modelValue === trueValue;
    }, true);

    element.bind('click', () =>
      scope.$apply(() =>
        model.$setViewValue(model.$modelValue === falseValue ?
          trueValue : falseValue)));
  }
});
