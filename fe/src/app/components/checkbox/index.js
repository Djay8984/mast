import * as angular from 'angular';
import checkbox from './checkbox.directive';

export default angular
  .module('app.components.checkbox', [])
  .directive('checkbox', checkbox)
  .name;
