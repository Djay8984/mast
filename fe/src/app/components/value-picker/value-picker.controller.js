import * as _ from 'lodash';

import Base from 'app/base.class';

import VpModalController from './modal/vp-modal.controller';
import vpModalTemplate from './modal/vp-modal.html';

export default class ValuePickerController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    MastModalFactory
  ) {
    super(arguments);
  }

  async openModal() {
    const modal = new this._MastModalFactory({
      controller: this.customController || VpModalController,
      controllerAs: 'vm',
      templateUrl: this.customTemplate || vpModalTemplate,
      inject: {
        selection: this._$scope.getModel(),
        availableOptions: this.availableOptions,
        multiSelect: this.multiSelect,
        modalTitle: this.modalTitle,
        inputType: this.inputType
      }
    });

    this._$scope.setModel(await modal.activate());
  }

  removeItem(item) {
    this._$scope.setModel(_.reject(this._$scope.getModel(), item));
  }

  get selection() {
    return this._$scope.getModel();
  }
}
