import * as angular from 'angular';

import valuePickerDirective from './value-picker.directive';

export default angular
  .module('app.components.value-picker', [])
  .directive('valuePicker', valuePickerDirective)
  .name;
