import * as _ from 'lodash';

import Base from 'app/base.class';

import Model from 'app/models/model.class';

export default class VpModalController extends Base {
  /* @ngInject */
  constructor(
    $modalInstance,
    availableOptions,
    modalTitle,
    selection,
    multiSelect,
    inputType,
  ) {
    super(arguments);

    if (!multiSelect) {
      this.selectOption = this.singleSelect;
    }

    this._inputType = this._inputType || 'checkbox';
    this.setupRuntime();
  }

  setupRuntime() {
    this.runtimeOptions = [];

    _.forEach(this._availableOptions, (opt) => {
      const optM = new Model(opt);
      _.set(optM, 'selected', Boolean(_.find(this._selection, opt)));
      this.runtimeOptions.push(optM);

      if (optM.selected) {
        this.radioModel = optM.id;
      }
    });
  }

  singleSelect(option) {
    _.forEach(_.reject(this.runtimeOptions, option),
      (opt) => _.set(opt, 'selected', false));

    // Make sure the option is selected here as well just in case the inputType
    // is 'radio'
    _.set(option, 'selected', true);
  }

  confirm() {
    const selected = _.filter(this.runtimeOptions, 'selected');
    this._$modalInstance.close(_.map(selected, 'model'));
  }

  get availableOptions() {
    return this.runtimeOptions;
  }

  get modalTitle() {
    return this._modalTitle;
  }

  get typeRadio() {
    return this._inputType === 'radio';
  }
}
