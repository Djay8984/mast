import * as _ from 'lodash';
import templateUrl from './value-picker.html';
import ValuePickerController from './value-picker.controller';

/* @ngInject */
export default () => ({
  bindToController: true,
  controller: ValuePickerController,
  controllerAs: 'vm',
  link: (scope, element, attrs, controller) => _.merge(scope, {
    setModel: controller.$setViewValue,
    getModel: () => controller.$viewValue
  }),
  require: 'ngModel',
  restrict: 'E',
  scope: {
    multiSelect: '@?',
    inputType: '@?',
    availableOptions: '=',
    buttonText: '@',
    customTemplate: '@?',
    customController: '=?',
    modalTitle: '@'
  },
  templateUrl
});
