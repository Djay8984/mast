import Base from 'app/base.class';
import * as _ from 'lodash';

export default class JobSummaryController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    jobStatus,
    offlinular,
    referenceDataService,
    officeRole
  ) {
    super(arguments);
    this.setUpData();
  }

  async setUpData() {
    const referenceDataMap = {
      jobStatuses: 'job.jobStatuses'
    };

    const data = await this._referenceDataService
      .get(_.values(referenceDataMap));

    _.forEach(referenceDataMap, (value, key) =>
      _.set(this, key, _.get(data, value)));
  }

  get jobSDO() {
    return _.get(_.find(this.job.offices, {
      officeRole: {
        id: this._officeRole.get('sdo')
      }
    }), 'office.name');
  }

  jobStatus() {
    return _.get(_.find(this.jobStatuses,
      { id: this.job.jobStatus.id }), 'name', null);
  }

  visit() {
    this._$state.go('asset.jobs.view',
      _.merge(this._$stateParams, { jobId: this.job.id }), { reload: true });
  }
}
