import * as angular from 'angular';

import jobSummaryDirective from './job-summary.directive';

export default angular
  .module('app.components.jobSummary', [])
  .directive('jobSummary', jobSummaryDirective)
  .name;
