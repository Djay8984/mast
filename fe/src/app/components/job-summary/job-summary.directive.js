import templateUrl from './job-summary.html';
import JobSummaryController from './job-summary.controller';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  bindToController: true,
  controller: JobSummaryController,
  controllerAs: 'vm',
  scope: {
    job: '='
  },
  templateUrl
});
