import * as angular from 'angular';
import tpl from './tpl.directive';

export default angular
    .module('app.components.tpl', [])
    .directive('tpl', tpl)
    .name;
