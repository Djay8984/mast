import Base from 'app/base.class';
import * as _ from 'lodash';

import debugTemplateUrl from './partials/debug/modal.html';
import preUrl from './partials/debug/pre.html';

import './partials/footer/cancel-save.html';
import './partials/footer/buttons.html';

import './partials/input/attachments.html';
import './partials/input/checkbox.html';
import './partials/input/datepicker.html';
import './partials/input/input.html';
import './partials/input/item.html';
import './partials/input/radio-buttons.html';
import './partials/input/select-item.html';
import './partials/input/select-template.html';
import './partials/input/select-modal.html';
import './partials/input/textarea.html';

import './partials/view/attachments.html';
import './partials/view/box.html';
import './partials/view/date.html';
import './partials/view/defect.html';
import './partials/view/yes-no.html';
import './partials/view/list-items.html';
import './partials/view/list-repairs.html';
import './partials/view/item.html';
import './partials/view/text.html';

import './partials/list/coc.html';

export default class TplController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    $element,
    $log,
    $window,
    MastModalFactory,
    referenceDataService
  ) {
    super(arguments);

    this.template = this.getTemplate();
    this.parseParams(this._$scope.data);
  }

  //  -----------------------------------------------------------
  getTemplate() {
    const vm = this._$scope.vm || {};
    const data = this._$scope.data || {};

    if (data.inactive) {
      return false;
    }

    this._$scope.data = data;
    if (_.isObject(data)) {
      const templateUrl = data.htmlUrl ||
        data.templateUrl &&
        `app/components/tpl/partials/${data.templateUrl}.html`;

      const vmTemplateUrl = _.isFunction(vm.getTemplateUrl) ?
        vm.getTemplateUrl(data) : preUrl;

      return templateUrl || vm.templateUrl || vmTemplateUrl || templateUrl;
    }
  }

  //  -----------------------------------------------------------
  setData(data) {
    _.forEach(data.$set, (value, key) => {
      this.setResult(data, value, key);
    });
  }

  //  -----------------------------------------------------------
  async setResult(data, conf, key) {
    if (_.isString(conf)) {
      conf = { service: conf };
    }

    const allResults = { param: conf.param };
    let finalResult = null;

    if (conf.service) {
      allResults.service =
        this._referenceDataService._$injector.get(conf.service);
      finalResult = allResults.service;
    }

    if (conf.constant) {
      allResults.constant = this.getConstant(conf);
      finalResult = allResults.constant;
    }

    if (conf.refData) {
      allResults.refData = await this.getRefData(conf);
      finalResult = allResults.refData;
    }

    if (_.isFunction(conf.result) && !allResults.done) {
      allResults.result = await conf.result(allResults, data);
      allResults.done = true;
      finalResult = allResults.result;
    }

    if (conf.watch) {
      this._$scope.$watch(() => data.$get(conf.watch, 'value'), (val) => {
        allResults.value = val;
        if (_.isFunction(conf.result)) {
          val = conf.result(allResults, data);
        }
        allResults.result = val;

        _.set(data, `$${key}`, allResults);
        _.set(data, key, val);
      });
    }

    if (_.isArray(conf)) {
      finalResult = conf;
    }

    _.set(data, `$${key}`, allResults);
    _.set(data, key, finalResult);
  }

  async getRefData(conf) {
    // restricts reference data hydration based on whether this in an input/view
    return this._referenceDataService.get(conf.refData,
      _.includes(this.template, 'input'));
  }

  //  -----------------------------------------------------------
  getConstant(conf) {
    const constant = this._referenceDataService._$injector.get(conf.constant);
    return constant.get(conf.param);
  }

  //  -----------------------------------------------------------
  //  Modal with data and functions used for this element
  debug() {
    const vm = this._$scope.vm || {};
    vm.bindToWindow = () => {
      this._$window.tpl = {
        data: this._$scope.data,
        tpl: this,
        vm,
        element: this._$element
      };
      this._$log.debug('Type "tpl" to debug element', this._$window.tpl);
    };

    this._MastModalFactory({
      templateUrl: debugTemplateUrl,
      scope: {
        data: this._$scope.data,
        vm,
        tpl: this.template,
        vmParams: Object.keys(vm).map((val) => `${val}: ${typeof vm[val]}`)
      },
      class: 'dialog'
    }).activate();
  }

  //  -----------------------------------------------------------
  parseParams(data = {}) {
    if (!data.list && data.$set && data.$set.list) {
      data.list = [{ name: 'Loading...', disabled: true }];
    }

    if (_.isFunction(data.onInit)) {
      data.onInit(data);
    }

    if (_.isFunction(data.onDestroy)) {
      this._$scope.$on('$destroy', (val) => {
        data.onDestroy(data, val);
      });
    }

    if (_.isObject(data.$set)) {
      this.setData(data);
    }
  }
}
