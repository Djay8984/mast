import tplController from './tpl.controller';

//  Creates isolated scope element
//  Uses selected template
//  can bind data {object} and vm {object}

/* @ngInject */
export default () => ({
  controller: tplController,
  controllerAs: 'tpl',
  template: () => {
    const template = [
      localStorage.debug ?
        `<div
          class="debug-btn"
          ng-click="tpl.debug()">
          {{data.templateUrl || 'debug'}}
        </div>` : '',
      '<div data-ng-include="::tpl.template" id="tpl-{{data.name}}">...</div>'
    ];
    return template.join('');
  },
  scope: {
    data: '=tpl',
    vm: '='
  }
});
