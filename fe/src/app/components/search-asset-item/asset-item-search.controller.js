import * as _ from 'lodash';
import Base from 'app/base.class';
import templateUrl from './modal/search-asset-item.html';

export default class SelectAssetItemController extends Base {
  /* @ngInject */
  constructor(
    $scope,
    MastModalFactory
  ) {
    super(arguments);

    this.mapValues();
  }

  mapValues() {
    const byType = {
      single: () => {
        if (this.data.value && this.data.value.name) {
          this.data.selected = [this.data.value];
          this.data.ids = [this.data.value.id];
          this.setValues();
        }
      },
      multi: () => {
        if (_.isArray(this.data.value) && !this.data.selected) {
          this.data.selected = _.map(this.data.value, 'item');
          this.data.ids = _.map(this.data.value, 'id');
          this.setValues();
        }
      }
    };

    const func = byType[this.data.type] || byType.single;
    func();

    this._$scope.$watch('vm.data.itemType', (val) => {
      if (val) {
        _.forEach(this.data.selected, (item) => {
          if (val.id !== _.get(item, 'itemType.id')) {
            this.removeItem(item);
          }
        });
      }
    });
  }

  async open() {
    const modal = await this._MastModalFactory({
      templateUrl,
      scope: { data: this.data },
      class: 'full dialog background-sea'
    }).activate();

    if (modal) {
      const selected = this.data.getSelected();
      this.data.selected = selected.id ? [selected] : selected;
      this.data.ids = _.map(this.data.selected, 'id');
      this.setValues();
    }
  }

  setValues(ids) {
    ids = ids || this.data.ids;
    this.data.values = _.map(ids, (id) => ({ id }));
    this.data.value = this.data.type === 'multi' ?
      this.data.values : this.data.values[0];
  }

  removeItem(item) {
    const filter = (val) => val.id !== item.id;
    if (_.isArray(this.data.value)) {
      this.data.selected = _.filter(this.data.selected, filter);
      this.data.value = _.filter(this.data.value, filter);
      this.data.values = this.data.value;
    } else {
      this.data.selected = null;
      this.data.values = null;
      this.data.value = null;
    }
  }
}
