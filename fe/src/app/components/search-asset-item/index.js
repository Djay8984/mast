import * as angular from 'angular';
import searchAssetItem from './asset-item-search.directive';

export default angular
  .module('app.components.selectAssetItem', [])
  .directive('searchAssetItem', searchAssetItem)
  .name;
