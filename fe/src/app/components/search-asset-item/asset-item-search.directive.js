import AssetItemSearchController
  from './asset-item-search.controller';
import templateUrl from './asset-item-search.html';

/* @ngInject */
export default () => ({
  controller: AssetItemSearchController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl,
  bindToController: {
    data: '=searchData'
  }
});
