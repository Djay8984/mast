const ENTER_KEY = 13;

/* @ngInject */
export default (
  $parse
) => ({
  link: (scope, element, attrs) =>
    element.bind('keydown keypress', ($event) => {
      if ($event.which === ENTER_KEY) {
        scope.$apply(() => scope.$eval(attrs.enter, { $event }));
        event.preventDefault();
      }
    }),
  restrict: 'A'
});

