import * as angular from 'angular';

import enterDirective from './enter.directive';

export default angular
  .module('app.components.enter', [])
  .directive('enter', enterDirective)
  .name;
