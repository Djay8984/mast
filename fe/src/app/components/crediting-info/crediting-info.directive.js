import CreditingInfoController from './crediting-info.controller';
import templateUrl from './crediting-info.html';

export default () => ({
  bindToController: {
    codicil: '=',
    codicilType: '=',
    survey: '=',
    job: '=',
    viewStateName: '='
  },
  controller: CreditingInfoController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
