import * as _ from 'lodash';

import Base from 'app/base.class';

export default class CreditingInfoController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    referenceDataService
  ) {
    super(arguments);
    this._setupData();
  }

  get creditable() {
    return this.codicil || this.survey;
  }

  async _setupData() {
    _.merge(this, _.get(await this._referenceDataService.get(
        ['service.codicilStatuses', 'service.serviceCreditStatuses']),
      'service'));
  }

  get creditingStatus() {
    return this.codicil ?
      _.find(this.codicilStatuses, { id: this.creditable.status.id }) :
      _.find(this.serviceCreditStatuses, { id: _.get(this.creditable,
          'scheduledService.serviceCreditStatus.id') });
  }

  viewCodicil(codicilId) {
    this._$state.go(
      this.viewStateName,
      _.merge(this._$stateParams, {
        codicilId,
        jobId: this.job.id,
        forCurrentJob: true,
        context: 'job',
        backTarget: this._$state.current.name
      }));
  }
}
