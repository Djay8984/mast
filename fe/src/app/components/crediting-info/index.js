import * as angular from 'angular';

import CreditingInfoDirective from './crediting-info.directive';

export default angular.module('app.component.crediting-info', [])
  .directive('creditingInfo', CreditingInfoDirective)
  .name;
