import * as _ from 'lodash';

import Base from 'app/base.class';

export default class ServiceBannerController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    employeeDecisionService
  ) {
    super(arguments);

    this._setupData();
    _.set(this, 'attachments.pagination.totalElements', this.attachmentCount);
  }

  async _setupData() {
    this.user = await this._employeeDecisionService.getCurrentUser();
  }

  back() {
    this._$state.go(this.backTarget || '^.^', this._$stateParams);
  }

  async editService() {
    return this._$state.go('asset.serviceSchedule.editService', {
      productTypeId: this._$stateParams.productTypeId,
      serviceId: _.get(this.serviceModel, 'model.id'),
      ...this._$stateParams
    });
  }

  get displayName() {
    // @todo This is supposed be "item.name service.code service.name" but this
    //       requirement has been moved to R3
    return _.chain(this.serviceCatalogueModel)
      .get('model')
      .pick(['code', 'name'])
      .values()
      .join(' - ')
      .value();
  }

  /**
   * @returns {String} Overdue | Crediting | Provisonal (where present)
   */
  get displayStatuses() {
    const rangeStatus = _.get(this.serviceModel, 'rangeStatus.name');
    const serviceStatus = _.get(this.serviceModel, 'serviceStatus.name');
    const provisionalStatus = _.get(this.serviceModel, 'provisionalStatus');
    const serviceCreditStatus =
      _.get(this.serviceModel, 'serviceCreditStatus.name');

    const statuses = [];

    if (rangeStatus) {
      statuses.push(rangeStatus);
    }

    if (serviceCreditStatus) {
      statuses.push(serviceCreditStatus);
    }

    if (!serviceCreditStatus && serviceStatus) {
      statuses.push(serviceStatus);
    }

    if (provisionalStatus) {
      statuses.push(provisionalStatus);
    }

    return statuses.join(' | ');
  }
}
