import * as angular from 'angular';

import serviceBannerDirective from './service-banner.directive';

export default angular
  .module('app.components.serviceBanner', [])
  .directive('serviceBanner', serviceBannerDirective)
  .name;
