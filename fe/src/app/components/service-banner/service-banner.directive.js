import templateUrl from './service-banner.html';
import serviceBannerController from './service-banner.controller';

/* @ngInject */
export default () => ({
  bindToController: {
    serviceModel: '=',
    serviceCatalogueModel: '=',
    product: '=',
    attachmentPath: '=',
    attachmentCount: '=',
    backTarget: '=',
    swapService: '&',
    hasCounterHalf: '&'
  },
  controller: serviceBannerController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
