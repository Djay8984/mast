import * as _ from 'lodash';
import Base from 'app/base.class';

export default class ServiceSummaryController extends Base {
  /* @ngInject */
  constructor(
    $state,
    moment,
    dueStatus,
    productType
  ) {
    super(arguments);

    // Constants - Reference data IDs considered stable.
    this.classificationId = this._productType.get('classification');
    this.statutoryId = this._productType.get('statutory');
  }

  /**
   * Resolves the Product Type ID into a name to give ng-switch
   * something readable to work with.
   *
   * @returns {String} Product Type name, or the empty string if unknown type.
   */
  get productTypeName() {
    let name = '';

    if (this.productTypeId === this.classificationId) {
      name = 'classification';
    } else if (this.productTypeId === this.statutoryId) {
      name = 'statutory';
    }

    return name;
  }

  /**
   * View the service in full on its own screen, with associated tasks below
   */
  viewService() {
    const params = {
      productTypeId: this.productTypeId,
      serviceId: _.get(this.serviceModel, 'model.id')
    };

    this._$state.go('asset.serviceSchedule.viewService', params);
  }

  /**
   * The service status in brackets, or the empty string if not available.
   *
   * For example (Approved) or (Planned)
   *
   * @returns {String} The status name or an empty string.
   */
  displayClassificationServiceStatus() {
    const statusName = this.serviceModel.classificationServiceStatus;
    return statusName.length ? `(${statusName})` : statusName;
  }

  /**
   * The service type in brackets, or the empty string if not available.
   *
   * For example (Short) or (Extension)
   *
   * @returns {String} The type name or an empty string.
   */
  displayStatutoryServiceType() {
    const typeName = this.serviceModel.statutoryServiceType;
    return typeName.length ? `(${typeName})` : typeName;
  }

  /**
   * @returns {String} Crediting status
   */
  displayCreditingStatus() {
    return this.serviceModel.serviceCreditStatus.name;
  }

  /**
   * @returns {String} The provisional flag or an empty string.
   */
  displayProvisionalStatus() {
    return this.serviceModel.provisionalStatus;
  }

  /**
   * @returns {Boolean} If the service's serviceCatalogueModel code is SS / CSH
   */
  displaySpecialServiceStatus() {
    return _.includes(['SS', 'CSH'], this.serviceCatalogueModel.model.code);
  }

  /**
   * As Service.rangeStatus but a null name is displayed as '-'.
   *
   * @returns {Object} Status indicates current date in range or beyond it
   */
  displayRangeStatus() {
    const status = this.serviceModel.rangeStatus;

    if (_.isNull(status.name)) {
      status.name = '-';
    }

    return status;
  }

  get isOverdue() {
    return this._dueStatus.get('overdue') ===
      _.get(this.serviceModel, 'dueStatus.id');
  }
}
