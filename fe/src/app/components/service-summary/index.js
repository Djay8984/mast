import * as angular from 'angular';

import serviceSummaryDirective from './service-summary.directive';

export default angular
  .module('app.components.serviceSummary', [])
  .directive('serviceSummary', serviceSummaryDirective)
  .name;
