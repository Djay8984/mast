import templateUrl from './service-summary.html';
import ServiceSummaryController from './service-summary.controller';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  bindToController: {
    asset: '=',
    serviceModel: '=',
    serviceCatalogueModel: '=',
    productTypeId: '='
  },
  controller: ServiceSummaryController,
  controllerAs: 'vm',
  scope: true,
  templateUrl
});
