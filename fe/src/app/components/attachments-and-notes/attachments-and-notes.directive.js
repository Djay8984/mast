import * as _ from 'lodash';
import AttachmentsAndNotesController from './attachments-and-notes.controller';

/* @ngInject */
export default (attachmentDecisionService, $q) => ({
  controller: AttachmentsAndNotesController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  bindToController: {
    modalTitle: '=',
    path: '=',
    attachments: '=?',
    disabled: '=?'
  },
  link(scope, element) {
    element.bind('click', _.bind(scope.vm.open, scope.vm));
  }
});
