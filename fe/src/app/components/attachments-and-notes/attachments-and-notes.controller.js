import Base from 'app/base.class';
import templateUrl from './attachment-modal/attachment-modal.html';

export default class AttachmentsAndNotesController extends Base {
  /* @ngInject */
  constructor(MastModalFactory) {
    super(arguments);
  }

  async open() {
    if (this.disabled) {
      return false;
    }

    this.attachments = await new this._MastModalFactory({
      controller: 'AttachmentModalController',
      controllerAs: 'vm',
      templateUrl,
      inject: {
        title: this.modalTitle,
        path: this.path,
        allAttachments: this.attachments
      },
      class: 'full background-sea'
    }).activate();
  }
}
