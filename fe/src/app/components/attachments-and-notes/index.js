import * as angular from 'angular';
import attachmentsAndNotes from './attachments-and-notes.directive';
import AttachmentModalController from
  './attachment-modal/attachment-modal.controller';

export default angular
  .module('app.components.attachments-and-notes', [])
  .directive('attachmentsAndNotes', attachmentsAndNotes)
  .controller('AttachmentModalController', AttachmentModalController)
  .name;
