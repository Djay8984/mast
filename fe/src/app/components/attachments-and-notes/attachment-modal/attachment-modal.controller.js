import * as angular from 'angular';
import * as _ from 'lodash';
import './add-attachment-or-note.html';
import './partials/expanded-notes-card.html';
import Base from 'app/base.class';

import templateUrl from
  'app/components/modals/templates/confirmation-modal.html';

const MEDIA_QUERY_STRING = 'large';
const LARGE_WIDTH = 4;
const NON_LARGE_WIDTH = 3;

/* eslint no-new: 0*/
export default class AttachmentModalController extends Base {
  /* @ngInject */
  constructor(
    $log,
    $modalInstance,
    $q,
    $scope,
    $timeout,
    $window,
    allAttachments,
    allowedSpecialCharactersRegex,
    attachmentDecisionService,
    attachmentMaxChars,
    employeeDecisionService,
    FoundationMQ,
    MastModalFactory,
    path,
    referenceDataService,
    title
  ) {
    super(arguments);
    this._allAttachments = this._allAttachments || [];
    this.attachments = this.attachments || [];
    this.init();

    // Work on server or in memory
    if (path) {
      // Updated _doSaveNote to work with both notes & attachments
      // note: fileToUpload is necessary if this is an attachment.
      // and it's a Javascript File object
      this._doSaveNote = async (note, fileToUpload) =>
        await this._attachmentDecisionService.add(
          this._path, note, fileToUpload);

      this._doDelete = async () =>
        await this._attachmentDecisionService.remove(this._path,
          this.selectedAttachment.id, this.isAttachment());

      this._doEdit = async () =>
        await this._attachmentDecisionService.update(this._path,
          this.selectedAttachment.model.model);
    } else {
      // NOTE: this is the mode when we're in an add {anything} form.
      // The path is then resolved when that form saves, and
      // the respective form's controller calls the service's addAll
      // function directly
      this._doSaveNote = async (note, fileToUpload) => {
        const payload = {
          ...note,
          _id: _.uniqueId()
        };
        if (!_.isNil(fileToUpload)) {
          payload.file = fileToUpload;
        }
        await this._attachmentDecisionService
          .populateAttachmentMetadata(payload, fileToUpload);
        this._allAttachments.push(payload);
      };

      this._doDelete = async () =>
        _.remove(this._allAttachments, (noteToDelete) =>
            noteToDelete._id === this.selectedAttachment._id);

      this._doEdit = async () =>
        _.merge(
          _.find(this._allAttachments, { _id: this.selectedAttachment._id }),
          this.selectedAttachment.model
        );
    }

    angular.element($window).bind('resize', () => this.calcScreenCategory());
  }

  /*
   * General Modal functions
   */

  async init() {
    this.loading = true;
    this.attachments = this._path ?
        await this._attachmentDecisionService.getAttachments(this._path) :
        await this._attachmentDecisionService.paginate(this._allAttachments);
    await this._attachmentDecisionService
      .makeAttachmentModels(this.attachments);

    const attachmentRefData =
      await this._referenceDataService.get(['attachment'], true);

    this.newAttachments = [];

    this.confidentialityTypes =
        _.get(attachmentRefData, 'attachment.confidentialityTypes');

    this._$scope.$watch(
      'vm.attachments.length',
      () => this.calcScreenCategory(true)
    );

    this._currentUser = await this._employeeDecisionService.getCurrentUser();

    this.loading = false;
  }

  calcScreenCategory(force = false) {
    const newLarge = this._FoundationMQ.matchesMedia(MEDIA_QUERY_STRING);
    if (force || this._isLarge !== newLarge) {
      this._isLarge = newLarge;

      this._rowWidth = this._isLarge ? LARGE_WIDTH : NON_LARGE_WIDTH;
      this.rowIndexes = _.times(
        Math.ceil(this.attachments.length / this._rowWidth), _.identity);

      this.rows = _.map(this.rowIndexes, (row) => this.getAttachmentRow(row));

      // This currently works and I don't like it. It disturbs me.
      this._$timeout(() => this._$scope.$apply());
    }
  }

  /**
   * @param {int} row number of row to extract
   * @return {array} array of filters extracted
   */
  getAttachmentRow(row) {
    return _.slice(
      this.attachments,
      row * this._rowWidth,
      row * this._rowWidth + this._rowWidth
    );
  }

  /*
   * Attachment creation functions
   */

  toggleNewAttachment() {
    return this.addingAttachment ?
      this.cancelAttachment() :
      this.createNewAttachment();
  }

  createNewAttachment() {
    this.addingNote = false;
    this.addingAttachment = true;
  }

  async cancelAttachment() {
    const response = await new this._MastModalFactory({
      templateUrl,
      scope: {
        message:
          'Are you sure that you want to leave without uploading the file?',
        actions: [
          { name: 'No', result: false },
          { name: 'Yes', result: true }
        ]
      },
      class: 'inner'
    }).activate();

    if (response) {
      this.addingAttachment = false;
      this.newAttachments = [];
    }

    return response;
  }

  /*
   * Note creation functions
   */

  toggleNewNote() {
    return this.addingNote ?
      this.cancelNote() :
      this.createNewNote();
  }

  createNewNote() {
    // Properties not bound to form models.
    // These can go when the backend is updated
    this.resetNote();
    this.addingAttachment = false;
    this.addingNote = true;
  }

  resetNote() {
    const path = _.split(this._path, '/');
    const id = _.parseInt(path.pop());
    const type = _.toUpper(_.replace(path.pop(), /([A-Z])/g, '_$1'));

    this.newNote = {
      title: '',
      creationDate: this.today,
      attachmentUrl: '',
      entityLink: { type, link: { id } }
    };
  }

  cancelNote() {
    this.addingNote = false;
  }

  async saveNote(newNote) {
    this.savingNote = true;

    try {
      await this._doSaveNote(newNote);
      await this._refresh();
      this.resetNote();
      this.addingNote = false;
    } catch (e) {
      this._$log.error('Failed saving note', e);
    }

    this.savingNote = false;
  }

  /*
   * View Attachment/Note functions
   */

  toggleDetails(attachment) {
    const matches = (key) => _.isEqual(_.get(this, ['selectedAttachment', key]),
      _.get(attachment, key));

    if (this.editNote) {
      this.cancelEdit();
    }
    if (!matches('id') || !matches('_id')) {
      _.set(this, 'selectedAttachment.isExpanded', false);
    }

    this.selectedAttachment = attachment.isExpanded ? attachment : null;
  }

  isAttachment() {
    // Claims that this.selectedAttachment is undefined
    // when opening something without another attachment/note already expanded
    // In theory it is safer to assume an item is not an attachment, thus
    // return false by default
    return this.selectedAttachment ?
      this.selectedAttachment.isAttachment() : false;
  }

  get selectedAttachmentTitle() {
    return _.get(this, 'selectedAttachment.title');
  }

  /*
   * Edit Note functions
   */

  edit() {
    this.editNote = true;

    _.set(this, '_noteCache', _.clone(_.get(this, 'selectedAttachment.note')));
    _.set(this, '_confidentialityTypeCache',
      _.cloneDeep(_.get(this, 'selectedAttachment.confidentialityType')));
  }

  cancelEdit() {
    this.editNote = false;

    _.set(this, 'selectedAttachment.note', this._noteCache);
    _.set(this, 'selectedAttachment.confidentialityType',
      this._confidentialityTypeCache);
  }

  async saveEdit() {
    try {
      await this._doEdit();
    } catch (e) {
      this._$log.error('Failed saving note', e);
    }

    this.editNote = false;
  }

  /*
   * Delete Attachment/Note functions
   */

  async delete() {
    if (this.editNote) {
      return;
    }

    const response = await new this._MastModalFactory({
      templateUrl,
      scope: {
        message: `${this.selectedAttachment.title || 'Note'} will be removed.
Do you wish to continue?`,
        actions: [
          { name: 'Cancel', result: false },
          { name: 'Delete', result: true }
        ]
      },
      class: 'inner'
    }).activate();

    if (response) {
      await this._doDelete();
      await this._refresh();
    }
  }

  /*
   * When we're adding notes/attachments locally to something that hansn't been
   * saved yet, they are just stored in memory, so we need some custom
   * pagination to handle this here.
   */
  async loadMoreLocal() {
    const resp = await this.attachments.nextPage();
    this.attachments.pagination = resp.pagination;
    this.attachments.splice(this.attachments.length, 0, ...resp);
    await this._refresh();
  }

  async close() {
    if (this.addingAttachment) {
      const resp = await this.cancelAttachment();
      if (!resp) {
        return false;
      }
    }

    this._allAttachments.pagination = this.attachments.pagination;
    this._$modalInstance.close(this._allAttachments);
  }

  /*
   * Download Attachment/Note functions
   */

  async download(file) {
    await this._attachmentDecisionService.downloadFileContents(file);
  }

  /*
    Attachment logic
  */

  attachNewDocument(attachment) {
    const newAttachment = {
      meta: {
        title: attachment.name,
        note: attachment.description,
        creationDate: this.today
      },
      file: attachment
    };

    this.newAttachments.push(newAttachment);
    return true;
  }

  removeNewDocument(attachment) {
    this.newAttachments = _.reject(this.newAttachments, attachment);
  }

  // Calls the saveAttachment() function for individual attachments
  async saveAttachments() {
    this.savingAttachment = true;

    return this._$q.all(
      _.map(this.newAttachments, this.saveAttachment.bind(this))
    ).catch((e) => {
      this._$log.error('Failed saving attachments', e);
      new Notification('Upload Failed', {
        body: e.fileName
      });
    }).finally(() => {
      this.addingAttachment = this.savingAttachment = false;
      this.newAttachments = [];
      return this._refresh();
    });
  }

  async saveAttachment(attachment) {
    return this._doSaveNote(attachment.meta, attachment.file);
  }

  async _refresh() {
    await this.attachments.refresh();
    await this._attachmentDecisionService
      .makeAttachmentModels(this.attachments);
    this.calcScreenCategory(true);
  }

  /*
   * General Getters
   */

  get title() {
    return this._title;
  }

  get today() {
    return new Date();
  }

  get currentUser() {
    return this._currentUser.fullName;
  }

  get maxChars() {
    return this._attachmentMaxChars;
  }

  get allowedSpecialCharactersRegex() {
    return this._allowedSpecialCharactersRegex;
  }

  get showDownloadButton() {
    return !_.isNil(this._path);
  }

  get totalNewAttachments() {
    return _.size(this.newAttachments);
  }
}
