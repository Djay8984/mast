import * as angular from 'angular';

import assetCard from './asset-card';
import assetImage from './asset-image';
import assetModelHierarchy from './asset-model-hierarchy';
import assetModelList from './asset-model-list';
import assetSearch from './asset-search';
import assetSearchModel from './asset-model-search';
import assetSubheader from './asset-subheader';
import attachmentsAndNotes from './attachments-and-notes';
import attachmentType from './attachment-type';
import attachmentOrNote from './attachment-or-note';
import attachmentsIcon from './attachments-icon';
import autocomplete from './autocomplete';
import backButton from './back-button';
import batchActionBanner from './batch-action-banner';
import breadcrumbs from './breadcrumbs';
import caseSummary from './case-summary';
import caseBanner from './case-banner';
import caseMilestoneList from './case-milestone-list';
import checkbox from './checkbox';
import checkIn from './check-in';
import checkedOutBy from './checked-out-by';
import clearable from './clearable';
import codicilInfo from './codicil-info';
import codicilSearch from './codicil-search';
import counter from './counter';
import creditingHierarchy from './crediting-hierarchy';
import creditingInfo from './crediting-info';
import datepicker from './datepicker';
import dropdown from './dropdown';
import dynamicInput from './dynamic-input';
import enter from './enter';
import errorMsg from './error-msg';
import fileInput from './file-input';
import formLogger from './form-logger';
import hierarchy from './hierarchy';
import infoHeader from './info-header';
import integer from './integer';
import jobBanner from './job-banner';
import jobReportCarousel from './job-report-carousel';
import jobReportItem from './job-report-item';
import jobSearch from './job-search';
import jobSummary from './job-summary';
import logoAssetHeader from './logo-asset-header';
import modals from './modals';
import notEmpty from './not-empty';
import pagination from './pagination';
import preLoad from './pre-load';
import print from './print';
import radioButton from './radio';
import rbac from './rbac';
import reportBanner from './report-banner';
import searchAssetItem from './search-asset-item';
import selectCheckbox from './select-checkbox';
import selectModal from './select-modal';
import selectTemplate from './select-template';
import serviceBanner from './service-banner';
import serviceSummary from './service-summary';
import sortBy from './sort-by';
import spinner from './spinner';
import stateLoader from './state-loader';
import stickyFooter from './sticky-footer';
import typeahead from './typeahead';
import tpl from './tpl';
import valuePicker from './value-picker';
import validate from './validate';

export default angular
  .module('app.components', [
    assetCard,
    assetImage,
    assetModelHierarchy,
    assetModelList,
    assetSearch,
    assetSearchModel,
    assetSubheader,
    attachmentsAndNotes,
    attachmentType,
    attachmentOrNote,
    attachmentsIcon,
    autocomplete,
    backButton,
    batchActionBanner,
    breadcrumbs,
    caseBanner,
    caseMilestoneList,
    caseSummary,
    checkbox,
    checkIn,
    checkedOutBy,
    clearable,
    codicilInfo,
    codicilSearch,
    counter,
    creditingHierarchy,
    creditingInfo,
    datepicker,
    dropdown,
    dynamicInput,
    enter,
    errorMsg,
    fileInput,
    formLogger,
    hierarchy,
    infoHeader,
    integer,
    jobBanner,
    jobReportCarousel,
    jobReportItem,
    jobSearch,
    jobSummary,
    logoAssetHeader,
    modals,
    notEmpty,
    pagination,
    preLoad,
    print,
    radioButton,
    rbac,
    reportBanner,
    searchAssetItem,
    selectCheckbox,
    selectModal,
    selectTemplate,
    serviceBanner,
    serviceSummary,
    sortBy,
    spinner,
    stateLoader,
    stickyFooter,
    tpl,
    typeahead,
    valuePicker,
    validate
  ]).name;
