import * as _ from 'lodash';

const ANGULAR_KEYS = /^\$/;

/* @ngInject */
export default ($log) => ({
  require: 'form',
  restrict: 'EA',
  link: (scope, element, attrs, form) =>
    scope.$watchCollection(attrs.name, (newValue) => {
      if (newValue.$invalid) {
        _.forEach(
          _.filter(
            _.omitBy(newValue, (value, key) => ANGULAR_KEYS.test(key)),
            '$error'),
          (control, name) => $log.warn(
            _.pick(control, ['$name', '$viewValue', '$error'])));
      }
    })
});
