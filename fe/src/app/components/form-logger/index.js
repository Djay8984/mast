import * as angular from 'angular';
import formLoggerDirective from './form-logger.directive';

export default angular
  .module('app.components.form-logger', [])
  .directive('formLogger', formLoggerDirective)
  .name;
