import CodicilInfoController from './codicil-info.controller';
import templateUrl from './codicil-info.html';

export default () => ({
  bindToController: {
    additionalParameters: '=?',
    assetId: '=',
    context: '=',
    item: '=',
    itemType: '=',
    jobId: '=',
    viewStateName: '='
  },
  controller: CodicilInfoController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
