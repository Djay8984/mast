import * as _ from 'lodash';

import Base from 'app/base.class';

export default class CodicilInfoController extends Base {
  /* @ngInject */
  constructor(
    $state,
    $stateParams,
    dateFormat,
    moment,
    referenceDataService
  ) {
    super(arguments);

    this._setupData();
  }

  async _setupData() {
    this.codicilStatuses = _.get(
      await this._referenceDataService.get(['service.codicilStatuses']),
      'service.codicilStatuses');
  }

  viewCodicil(item) {
    this._$state.go(
      this.viewStateName, {
        ...this._$stateParams,
        codicilId: item.id || item._id,
        assetId: this.assetId,
        jobId: this.jobId,
        context: this.context,
        backTarget: this._$state.current.name,
        backTargetId: this._$stateParams.codicilId,
        ...this.additionalParameters
      });
  }

  get isPromptThorough() {
    return _.get(this, 'item.defect.promptThoroughRepair');
  }

  get itemSubset() {
    return _.slice(_.get(this, 'item.items'), 0, 2);
  }

  get imposedDate() {
    return !_.isNil(this.item.imposedDate) ?
      this._moment(this.item.imposedDate._d ?
        this.item.imposedDate._d : this.item.imposedDate)
        .format(this._dateFormat) : '';
  }

  get dueDate() {
    return !_.isNil(this.item.dueDate) ?
      this._moment(this.item.dueDate._d ? this.item.dueDate._d :
        this.item.dueDate).format(this._dateFormat) : '';
  }

  getCodicilStatus(statusId) {
    return _.get(_.find(this.codicilStatuses, ['id', statusId]), 'name');
  }
}
