import * as angular from 'angular';

import CodicilInfoDirective from './codicil-info.directive';

export default angular.module('app.component.codicil-info', [])
  .directive('codicilInfo', CodicilInfoDirective)
  .name;
