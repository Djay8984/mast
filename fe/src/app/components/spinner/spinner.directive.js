import templateUrl from './spinner.html';

/* @ngInject */
export default () => ({
  restrict: 'AE',
  scope: {
    theme: '@'
  },
  templateUrl
});
