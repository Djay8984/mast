import * as angular from 'angular';
import spinner from './spinner.directive';

export default angular
  .module('app.components.spinner ', [])
  .directive('spinner', spinner)
  .name;
