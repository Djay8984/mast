import * as _ from 'lodash';

import PaginationController from './pagination.controller';
import templateUrl from './pagination.html';

/* @ngInject */
export default () => ({
  bindToController: {
    pageable: '=',
    type: '=',
    hideCount: '=?'
  },
  templateUrl,
  controller: PaginationController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  transclude: {
    'headerLeft': '?paginationHeaderLeft',
    'headerMid': '?paginationHeaderMid',
    'headerRight': '?paginationHeaderRight'
  },
  link: (scope, element, attributes, controller, transclude) => _.merge(scope, {
    headerLeftDefined: transclude.isSlotFilled('headerLeft'),
    headerMidDefined: transclude.isSlotFilled('headerMid'),
    headerRightDefined: transclude.isSlotFilled('headerRight')
  })
});
