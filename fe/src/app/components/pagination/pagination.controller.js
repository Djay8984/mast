import Base from 'app/base.class';
import * as _ from 'lodash';

export default class PaginationController extends Base {
  /* @ngInject */
  constructor() {
    super(arguments);

    if (!_.get(this, 'pageable.pageable')) {
      throw new Error('Collection is not pageable');
    }
  }

  get pagination() {
    return this.pageable.pagination || {};
  }

  get numberOfElements() {
    return this.pageable.length;
  }

  get totalElements() {
    return this.pagination.totalElements;
  }

  get isLastPage() {
    return this.pagination.last;
  }

  get hasMorePages() {
    // Cope with isLastPage === undefined
    return this.isLastPage === false;
  }

  get allItems() {
    return this.loadAll();
  }

  async loadMore() {
    this.loading = true;

    const resp = await this.pageable.nextPage();

    this.pageable.pagination = resp.pagination;
    this.pageable.splice(this.pageable.length, 0, ...resp);
    this.pageable.updated = true;
    this.loading = false;
  }

  async loadAll() {
    this.loading = true;

    const resp = await this.pageable.getAll();

    this.pageable.pagination = resp.pagination;
    this.pageable.splice(this.pageable.length, 0, ...resp);
    this.pageable.updated = true;
    this.loading = false;
  }
}
