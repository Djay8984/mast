import * as angular from 'angular';
import paginationDirective from './pagination.directive';

export default angular
  .module('app.components.pagination', [])
  .directive('pagination', paginationDirective)
  .name;
