import * as angular from 'angular';
import validate from './validate.directive';

export default angular
  .module('app.components.validate', [])
  .directive('validate', validate)
  .name;
