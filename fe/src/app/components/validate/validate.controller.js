import * as _ from 'lodash';
import Base from 'app/base.class';
import ValidationRules from './validation.rules';

export default class ValidateController extends Base {
  /* @ngInject */
  constructor($scope) {
    super(arguments);
  }

  //  ---------------------------------------------------------
  validate(element, attr) {
    const data = this.data;
    const rules = data.validate || {};

    rules.$getElement = () => element;

    const validators = new ValidationRules(rules, data, element, attr);

    //  Set maxlength
    if (rules.maxLength) {
      attr.$set('maxlength', rules.maxLength);
    }

    //  Validate by type (empty to be true)
    if (validators[rules.type]) {
      data.ngModel.$validators[rules.type] =
        validators[rules.type].bind(validators);
    }

    //  Validate for required
    if (rules.required) {
      data.ngModel.$validators.required = validators.required.bind(validators);
    }

    /**
     *  Validation for required when
     */
    if (rules.requiredWhen) {
      data.ngModel.$validators.requiredWhen =
        validators.requiredWhen.bind(validators);

      this.watch = this._$scope.$watch(() => {
        const res = rules.requiredWhen && data.$get &&
          data.$get(rules.requiredWhen);

        return _.get(res, 'value');
      }, (val) => {
        rules.$requiredWhen = val;
        rules.required = Boolean(val);
        data.ngModel.$validate();
      });

      this._$scope.$on('$destroy', () => this.watch && this.watch());
    }

    if (rules.autofocus) {
      _.first(element).focus();
    }
  }
}
