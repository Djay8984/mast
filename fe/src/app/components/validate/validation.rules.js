import * as _ from 'lodash';

class ValidationRulesBase {
  constructor(rules, data, element, attr) {
    this.rules = rules;
    this.data = data;
  }

  setMessage(valid, name, text) {
    const errorMsg = this.data.validate.errorMsg || {};
    if (!valid) {
      this.data.validate.message = errorMsg[name] || text;
    }
  }

  empty(value) {
    return _.isUndefined(value) || value === null || value === '';
  }
}

export default class ValidationRules extends ValidationRulesBase {
  //  Is value set
  required(value) {
    const valid = !this.empty(value);
    this.setMessage(valid, 'required',
      `${this.data.label} is a required field`);
    return valid;
  }

  requiredWhen(value) {
    let valid = true;
    if (value) {
      this.data.ngModel.$setDirty(true);
    }

    if (this.rules.required) {
      valid = this.required(value);
    }

    return valid;
  }

  //  string validation: type, min, max
  string(value) {
    if (this.empty(value)) {
      return true;
    }

    const validString = typeof value === 'string';
    this.setMessage(validString, 'isString', 'Value is in wrong format');

    const min = this.rules.minLength || this.rules.min;
    const validMin = validString && (!min || value.length >= min);
    this.setMessage(validMin, 'minLength',
      `A minimum of ${min} characters is permitted
      for the ${_.lowerCase(this.data.label)}.`);

    const max = this.rules.maxLength || this.rules.max;
    const validMax = validString && (!max || value.length <= max);
    this.setMessage(validMax, 'maxLength',
      `A maximum of ${max} characters is permitted
      for the ${_.lowerCase(this.data.label)}.`);

    return validMax && validMin;
  }
}
