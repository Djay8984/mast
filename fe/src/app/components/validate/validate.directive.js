import ValidateController from './validate.controller';
import * as _ from 'lodash';

/*
Use:
  @data: {object}
    ngModel: { Returns ngModel values what angular have about model {object}
      blur: {} Returns Copy of previous ngModel values on blur action {object}
    },
    validate: { All validation rules - {object}
      required: true/false, {boolean}
      type: 'string/date/number/etc.', validation function name - {string}
      autofocus: true/false, Does autofocus on element load - {boolean}
      maxLength: 20, Adds maxlength attribute to element prevent
                      from adding more characters - {integer}
      errorMsg: { Overwrite messages - {object}
        minLength: 'Too short', Overwrite message for
                                minLength validation - {string}
        {any}: {string}, Other overwrite messages - {string}
      }
      {any}: {any} Can be used any other params for different validations

      message: 'No valid', Returns Text messsage by error - {string}
      $getElement: (), Returns jQlite element of itself - {function}
    }
*/

/* @ngInject */
export default (
  $timeout
) => ({
  controller: ValidateController,
  controllerAs: 'ctrl',
  require: 'ngModel',
  bindToController: {
    data: '=validate'
  },
  link: (scope, element, attr, ngModel) => {
    if (_.isObject(scope.ctrl.data)) {
      ngModel.name = scope.ctrl.data.name;
      scope.ctrl.data.ngModel = ngModel;

      if (_.isObject(scope.ctrl.data.validate)) {
        scope.ctrl.validate(element, attr);
      }

      // save validation results what was on blur
      element.bind('blur', () =>
        $timeout(() =>
          ngModel.blur = _.pick(ngModel, [
            '$valid',
            '$invalid',
            '$pristine',
            '$dirty',
            '$viewValue',
            '$modelValue'])));
    }
  }
});
