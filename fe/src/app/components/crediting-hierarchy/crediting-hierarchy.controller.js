import * as _ from 'lodash';
import * as angular from 'angular';
import Base from 'app/base.class';

import './partials/edit/confirm-uncredit.html';
import './partials/edit/item-cascade.html';
import './partials/edit/survey-cascade.html';
import './partials/edit/survey-details.html';
import './partials/edit/task-details.html';

import './partials/view/conditional-task-details.html';
import './partials/view/item-main.html';
import './partials/view/survey-details.html';
import './partials/view/survey-main.html';
import './partials/view/task-details.html';
import './partials/view/task-main.html';

const CASCADE_COMPLETE_ID = '-cascade-complete';
const CASCADE_CONFIRM_ID = '-cascade-confirm';
const CASCADE_WAIVE_ID = '-cascade-waive';
const UNDO_TEXT = 'Undo';

export default class CreditingHierarchyController extends Base {
  /* @ngInject */
  constructor(
    $document,
    $q,
    moment,
    attachmentDecisionService,
    employeeDecisionService,
    allowedSpecialCharactersRegex,
    creditingNarrativeLength,
    creditStatus,
    serviceCreditStatus
  ) {
    super(arguments);

    // Ordering for the acceptance criteria
    this.taskCreditStatuses = [
      _.find(this.creditStatuses, { id: creditStatus.get('waived') }),
      _.find(this.creditStatuses, { id: creditStatus.get('confirmed') }),
      _.find(this.creditStatuses, { id: creditStatus.get('completed') }),
      _.find(this.creditStatuses, { id: creditStatus.get('uncredited') })
    ];
    this.surveyCreditStatuses = [
      _.find(this.surveyStatuses, { id: serviceCreditStatus.get('partHeld') }),
      _.find(this.surveyStatuses, { id: serviceCreditStatus.get('complete') }),
      _.find(this.surveyStatuses, { id: serviceCreditStatus.get('notStarted') })
    ];
  }

  /*
   * Cascade functions
   */

  /**
   * Based upon the truthiness of the supplied flag, the supplied status ID is
   * applied to all tasks within the current given survey or item.
   * If the flag is truthy, the previous cascade action is undone.
   * @param {object} target the survey or item to interact with
   * @param {string} flag the name of the flag to check
   * @param {string} elementId the button ID to interact with
   * @param {object} statusId the status to cascade
   * @param {string} type the type of target
   */
  _cascade(target, flag, elementId, statusId, type) {
    // If the button clicked said 'Undo'
    if (_.get(this, flag)) {
      _.set(this, flag, false);
      this._applyCascadeCache(target, type);

      // Refresh the item statuses manually
      const survey = type === 'item' ? this.getSurveyFromItem(target) : target;
      _.first(survey.items).getTaskStatus(true);

      _.first(angular.element(
        this._$document.querySelector(`[id$='${elementId}']`))).blur();
      return;
    }

    // Else, start the cascade
    _.set(this, flag, true);
    this._prepareCascadeCache(target, type);

    // Cascade only to uncredited tasks
    const taskList = type === 'survey' ? target.tasks : target.allChildTasks;
    let uncreditedTasks = _.filter(taskList,
      { creditStatus: { id: this._creditStatus.get('uncredited') } });

    // Ignore conditional tasks
    uncreditedTasks = _.reject(uncreditedTasks, 'isConditionalTask');

    _.forEach(uncreditedTasks, (task) =>
      this.credit(task, statusId, 'task', _.get(task, 'creditStatus.id')));
  }

  /**
   * Applies the cached cascade objects to the target target
   * @param {object} target the target to reset
   * @param {string} type the type of target
   */
  _applyCascadeCache(target, type) {
    const survey = type === 'item' ? this.getSurveyFromItem(target) : target;

    const items = survey.crediting.items;
    angular.copy(this._cascadeSurveyCreditingCache, survey.crediting);
    angular.copy(this._cascadeSurveyModelCache, survey.model);
    survey.crediting.items = items;

    const taskList = type === 'survey' ? target.tasks : target.allChildTasks;

    _.forEach(this._cascadeTaskCache, (taskCache) =>
      angular.copy(taskCache.model,
        _.find(taskList, { id: taskCache.id }).model)
    );
  }

  cascadeComplete(target, type) {
    this._cascade(target, 'undoComplete',
      CASCADE_COMPLETE_ID, this._creditStatus.get('completed'), type);
  }

  cascadeConfirm(target, type) {
    this._cascade(target, 'undoConfirm',
      CASCADE_CONFIRM_ID, this._creditStatus.get('confirmed'), type);
  }

  cascadeWaive(target, type) {
    this._cascade(target, 'undoWaive',
      CASCADE_WAIVE_ID, this._creditStatus.get('waived'), type);
  }

  /**
   * Caches the relevant survey's crediting object under
   * this._cascadeSurveyCreditingCache while omitting the items, and the
   * target's task models under this._cascadeTaskCache
   * @param {object} target the target to cache
   * @param {string} type the type of target
   */
  _prepareCascadeCache(target, type) {
    const survey = type === 'item' ? this.getSurveyFromItem(target) : target;

    this._cascadeSurveyCreditingCache =
      angular.copy(_.omit(survey.crediting, 'items'));
    this._cascadeSurveyModelCache = angular.copy(survey.model);
    this._cascadeTaskCache = [];

    const taskList = type === 'survey' ? target.tasks : target.allChildTasks;

    _.forEach(taskList, (task) =>
      this._cascadeTaskCache.push({
        id: task.id,
        model: angular.copy(task.model)
      })
    );
  }

  /*
   * Crediting functions, likely to change
   */

  /**
   * Removes all attachments from the target Task or Survey
   * @param {object} target the target to remove attachments from
   * @param {string} type the target's type
   */
  async _removeAllAttachments(target, type) {
    const path = this.generateAttachmentPath(target, type);

    // The target's attachment & notes modal may not have been opened (and thus
    // there isn't a list of attachments on it yet), so retrieve them now.
    const attachments =
      await this._attachmentDecisionService.getAttachments(path);

    // Loop through all attachments and manually remove them
    await this._$q.all(_.map(attachments, (attachment) =>
      this._attachmentDecisionService.remove(path, attachment.id)));

    // Update the target with the new list of attachments (now empty) in order
    // to update the attachment-icon
    target.attachments =
      await this._attachmentDecisionService.getAttachments(path);
  }

  /**
   * Applies the new credit status to the target Task or Survey.
   * If the new status is the one for uncrediting the uncredit process is begun,
   * else the target's credit information is updated and if the target is a
   * task, its Item and Survey statuses are also updated.
   * @param {object} target the Task or Survey to credit
   * @param {int} statusId the new status ID
   * @param {string} type the target's type
   * @param {int} oldStatusId the old status ID
   */
  credit(target, statusId, type, oldStatusId) {
    target.actionTakenDate = this._moment().toISOString();

    // Initialise various variables for use here and in other crediting related
    // functions
    this._creditStatusPath =
      type === 'task' ? 'creditStatus' : 'surveyStatus';
    this._availableCreditStatuses =
      type === 'task' ? this.taskCreditStatuses : this.surveyCreditStatuses;

    const uncreditId = type === 'task' ? this._creditStatus.get('uncredited') :
      this._serviceCreditStatus.get('notStarted');

    // Temporarily reset the target's credit status ID back to the old one after
    // it was automatically changed by the radio button. This preserves the
    // visual state of the view in the event of an uncredit action
    _.set(target, `${this._creditStatusPath}.id`, oldStatusId);

    if (statusId === uncreditId) {
      // Begin the uncredit logic if needed
      this.uncreditStart(target, type, oldStatusId);
    } else {
      // Actually set the target's credit status (full object this time) to the
      // new one and update various pieces of information
      _.set(target, this._creditStatusPath,
        _.find(this._availableCreditStatuses, { id: statusId }));
      this.updateCreditInformation(target, type);

      // Update the target's survey and item status if the target is a task
      if (type === 'task') {
        this.updateItemStatus(target);
        this.updateSurveyStatus(target);
      }
    }
  }

  /**
   * Begins the uncrediting process on a task or service if one isn't currently
   * being uncredited.
   * @param {object} target the task or service to uncredit
   * @param {string} type 'task' or 'service', depending upon what the target is
   * @param {object} oldStatusId the target's credit status prior to uncrediting
   */
  uncreditStart(target, type, oldStatusId) {
    // Stop if another thing is being uncredited
    if (_.get(this, '_uncreditingTarget.uncrediting')) {
      return;
    }

    // Shows the uncrediting warning,
    // blocks other uncredit actions from being started
    target.uncrediting = true;

    // Used by the view to show the specific word in the uncrediting warning
    // and in this.uncreditConfirm()
    this.uncreditTargetType = type;

    // Used by uncreditCancel() and uncreditConfirm()
    this._uncreditingTarget = target;
    this._uncreditingPreviousId = oldStatusId;

    // Skip warning if no narrative/description or attachments
    if (!(target.narrative || target.description) &&
      !_.get(target, 'attachments.pagination.totalElements')) {
      this.uncreditConfirm();
    }
  }

  /**
   * Cancels the uncrediting action and sets the uncreditingTarget's status to
   * the status it had before uncrediting began
   */
  uncreditCancel() {
    // Removes the uncredit warning, unblocks other uncredit actions
    this._uncreditingTarget.uncrediting = false;

    // Resets the target's credit status back to the initial value
    _.set(this._uncreditingTarget, this._creditStatusPath,
      _.find(this._availableCreditStatuses,
        { id: _.parseInt(this._uncreditingPreviousId) }));
  }

  /**
   * Confirms the uncrediting action by setting the uncreditingTarget's status
   * to 'uncredited'
   */
  async uncreditConfirm() {
    // Removes the uncredit warning, unblocks other uncredit actions
    this._uncreditingTarget.uncrediting = false;

    // Retrieve appropriate ID
    const uncreditId = this.uncreditTargetType === 'task' ?
      this._creditStatus.get('uncredited') :
      this._serviceCreditStatus.get('notStarted');

    // Set target's new credit status
    _.set(this._uncreditingTarget, this._creditStatusPath,
      _.find(this._availableCreditStatuses, { id: uncreditId }));
    this.updateCreditInformation(this._uncreditingTarget,
      this.uncreditTargetType);

    if (this.uncreditTargetType === 'task') {
      // Remove Task's description and question answers
      this._uncreditingTarget.description = null;
      this._uncreditingTarget.resetQuestionAnswers();

      // Update the Task's Item and Survey statuses
      this.updateItemStatus(this._uncreditingTarget);
      await this.updateSurveyStatus(this._uncreditingTarget);

      // Remove Task's attachments
      await this._removeAllAttachments(this._uncreditingTarget,
        this.uncreditTargetType);
    } else if (!_.size(this._uncreditingTarget.items)) {
      // Remove Non-Tasklist Service's (aka Survey) narrative and follow-up flag
      this._uncreditingTarget.narrative = null;
      this._uncreditingTarget.requiresFollowUp = false;

      // If the Job's FAR has not been submitted, also remove attachments
      if (!this.farSubmissionState) {
        await this._removeAllAttachments(this._uncreditingTarget,
          this.uncreditTargetType);
      }
    }
  }

  /**
   * Updates the relevant credited 'on' and 'by' fields for a task or survey
   * depending upon the taret's credited status
   * @param {object} target the task or survey to update the information on
   * @param {string} type the target type
   */
  async updateCreditInformation(target, type) {
    target.actionTakenDate = this._moment().toISOString();

    // Logic structure as follows:
    // if task
    //   if uncredited
    //   else credited
    // else if survey not 'complete'
    // else survey is 'complete'
    if (type === 'task') {
      if (target.creditStatus.id === this._creditStatus.get('uncredited')) {
        target.resolutionDate = null;
        target.resolvedBy = null;
      } else {
        target.resolutionDate = this._moment().toISOString();
        target.resolvedBy =
          _.omit(await this._employeeDecisionService.getCurrentUser(),
            ['groups', 'workItemType']);
      }
    } else if (target.surveyStatus.id !==
      this._serviceCreditStatus.get('complete')) {
      target.dateOfCrediting = null;
      target.creditedBy = null;
    } else {
      target.dateOfCrediting = this._moment().toISOString();
      target.creditedBy =
        _.omit(await this._employeeDecisionService.getCurrentUser(),
            ['groups', 'workItemType']);
    }
  }

  /**
   * Updates the display status for the Item belonging to the given Task.
   * If the status has changed from its previous status, the top level item in
   * the survey is also updated. That call recurses down through all other items
   * in the hierarchy.
   * @param {Task} task the target task
   */
  updateItemStatus(task) {
    const survey = _.find(this.surveys, { id: task.survey.id });
    const item = _.find(survey.items, { id: task.assetItem.id });

    const initialStatus = item.getTaskStatus();

    // Must be obtained after the initial status because getTaskStatus(true)
    // refreshes the item's status cache.
    const updatedStatus = item.getTaskStatus(true);

    if (initialStatus !== updatedStatus) {
      // First item in the list will always be the root item.
      _.first(survey.items).getTaskStatus(true);
    }
  }

  /**
   * Updates the status of the survey that the given task is part of to
   * 'partHeld'/'complete'/'notStarted' depending upon the ratio of credited to
   * uncredited tasks
   * @param {Task} task the target task
   */
  async updateSurveyStatus(task) {
    const survey = _.find(this.surveys, { id: task.survey.id });

    // The getter for survey.tasks does some processing,
    // so store the result for efficiency's sake.
    const tasks = survey.tasks;

    // Gets a list the same length as the the tasks which are uncredited
    const uncreditedTasks = _.filter(_.map(tasks, 'creditStatus'),
      { id: this._creditStatus.get('uncredited') });

    let surveyStatus = 'partHeld';
    if (!_.size(uncreditedTasks)) {
      surveyStatus = 'complete';
    } else if (_.size(uncreditedTasks) === _.size(tasks)) {
      surveyStatus = 'notStarted';

      // Remove narrative and maybe the attachments depending upon the
      // FAR submission state.
      survey.narrative = null;
      survey.requiresFollowUp = false;
      if (!this.farSubmissionState) {
        await this._removeAllAttachments(survey, 'survey');
      }
    }

    // Set the status to the calculated value and update the credit information.
    _.set(survey, 'surveyStatus', _.find(this.surveyStatuses,
      { id: this._serviceCreditStatus.get(surveyStatus) }));
    this.updateCreditInformation(survey, 'survey');
  }

  /*
   * Misc
   */

  canEditTask(task) {
    return task.creditStatus.id !== this._creditStatus.get('uncredited');
  }

  /**
   * Calculates the overall percentage of credited tasks for the given survey.
   * If some of its tasks are credited, this function also calculates the
   * breakdown of specific credited statuses and saves it on the survey
   * @param {Survey} survey the target survey
   * @returns {number} the overall percentage of credited tasks
   */
  creditedPercentage(survey) {
    const percentage = (subsetTasks, totalTasks) => _.size(subsetTasks) ?
      Math.floor(_.size(subsetTasks) / _.size(totalTasks) * 100) : 0;

    // The getter for survey.tasks does some processing,
    // so store the result for efficiency's sake.
    const tasks = survey.tasks;

    // Gets a list the same length as the the tasks which are credited
    const creditedTasks = _.reject(_.map(tasks, 'creditStatus'),
      { id: this._creditStatus.get('uncredited') });

    // Calculate task breakdown if some are complete
    if (_.size(creditedTasks)) {
      const calcPercentage = (status) => {
        const statusTasks =
          _.filter(_.map(tasks, 'creditStatus'), { id: status });
        _.set(survey,
          [
            'creditedTaskBreakdown',
            _.find(this.taskCreditStatuses, { id: status }).name
          ],
          percentage(statusTasks, creditedTasks));
      };

      _.forEach(_.filter(this.taskCreditStatuses, 'id'),
        (status) => calcPercentage(status.id));
    }

    return percentage(creditedTasks, tasks);
  }

  /**
   * @param {object} entity what to build path to
   * @param {string} type what type of entity the passed node is
   * @return {string} path to the attachments api for this entity
   */
  generateAttachmentPath(entity, type) {
    switch (type) {
      case 'item':
        return `/asset/${this.job.asset.id}/item/${entity.id}`;
      case 'service':
      case 'survey':
        return `/job/${this.job.id}/survey/${entity.id}`;
      case 'task':
        return `/job/${this.job.id}/wip-task/${entity.id}`;
      default:
        return `/job/${this.job.id}`;
    }
  }

  /**
   * @param {object} employee description of employee
   * @returns {string} whatever we can grasp of the employee's name
   */
  getEmployeeName(employee) {
    return !_.isEmpty(employee.fullName) ?
      employee.fullName :
      `${employee.firstName || ''} ${employee.lastName || ''}`;
  }

  /**
   * @param {Item} item the target item
   * @returns {Survey} the survey relating to the given item
   */
  getSurveyFromItem(item) {
    return _.find(this.surveys, (survey) =>
      _.find(survey.items, { id: item.id }));
  }

  /**
   * @param {Item} item the target item
   * @returns {boolean} whether or not the item is the top level item for the
   * survey
   */
  isTopLevelItem(item) {
    const survey = this.getSurveyFromItem(item);
    return _.first(survey.items).id === item.id;
  }

  noChildItemsExpanded(survey) {
    return !_.some(survey.items, { expanded: true });
  }

  validNarrative(narrative) {
    return narrative &&
      narrative.$dirty &&
      narrative.$invalid &&
      !narrative.$pristine;
  }

  /*
   * Getters
   */

  get allowedSpecialCharactersRegex() {
    return this._allowedSpecialCharactersRegex;
  }

  get complete() {
    return this.undoComplete ? UNDO_TEXT : '(X) Complete';
  }

  get confirm() {
    return this.undoConfirm ? UNDO_TEXT : '(C) Confirm';
  }

  get creditingNarrativeLength() {
    return this._creditingNarrativeLength;
  }

  get waive() {
    return this.undoWaive ? UNDO_TEXT : '(W) Waive';
  }
}
