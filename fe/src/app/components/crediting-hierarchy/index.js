import angular from 'angular';

import creditingHierarchyDirective from './crediting-hierarchy.directive';

export default angular
  .module('app.components.crediting-hierarchy', [])
  .directive('creditingHierarchy', creditingHierarchyDirective)
  .name;
