import templateUrl from './crediting-hierarchy.html';
import CreditingHierarchyController from './crediting-hierarchy.controller';

export default () => ({
  bindToController: true,
  controller: CreditingHierarchyController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: {
    editCancel: '=',
    editSave: '=',
    editStart: '=',
    creditStatuses: '=',
    farSubmissionState: '=',
    job: '=',
    jobStatusValidity: '=',
    surveys: '=',
    surveyStatuses: '='
  },
  templateUrl
});
