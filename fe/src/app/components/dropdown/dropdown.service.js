import Base from 'app/base.class';
import * as _ from 'lodash';

const _dropdowns = [];

/* @ngInject */
export default class dropdownService extends Base {
  constructor($document) {
    super(arguments);

    const body = this._$document.find('body');

    body.bind('click', () => {
      _.forEach(_dropdowns, (el) => {
        el.removeClass('active');
      });
    });

    const service = {
      register: (ddEl) => {
        _dropdowns.push(ddEl);
      },
      unregister: (ddEl) => {
        const index = _dropdowns.indexOf(ddEl);
        if (index > -1) {
          _dropdowns.splice(index, 1);
        }
      },
      toggleActive: (ddEl) => {
        _.forEach(_dropdowns, (el) => {
          if (el !== ddEl) {
            el.removeClass('active');
          }
        });
        ddEl.toggleClass('active');
      },
      clearActive: () => {
        _.forEach(_dropdowns, (el) => {
          el.removeClass('active');
        });
      },
      isActive: (ddEl) => ddEl.hasClass('active')
    };

    return service;
  }
}
