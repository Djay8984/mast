import templateUrl from './partials/dropdown-select.html';
import DropdownSelectController from './controllers/dropdown-select.controller';

/* @ngInject */
export default () => ({
  controller: DropdownSelectController,
  controllerAs: 'vm',
  restrict: 'A',
  transclude: true,
  bindToController: {
    dropdownSelect: '=',
    dropdownModel: '=',
    dropdownItemLabel: '@',
    dropdownOnchange: '&',
    dropdownDisabled: '=',
    dropdownAlign: '@'
  },
  scope: true,
  templateUrl
});
