import Base from 'app/base.class';
import * as _ from 'lodash';

export default class DropdownSelectController extends Base {
  /* @ngInject */
  constructor($scope, $element, dropdownService) {
    super(arguments);

    this.labelField = this.dropdownItemLabel || 'text';

    dropdownService.register($element);

    $element.bind('click', (event) => {
      event.stopPropagation();
      if (!this.dropdownDisabled) {
        dropdownService.toggleActive($element);
      }
    });

    $scope.$on('$destroy', () => dropdownService.unregister($element));
  }

  select(selected) {
    if (!_.isEqual(selected, this.dropdownModel)) {
      this.dropdownModel = selected;
    }
    this.dropdownOnchange({ selected });
    this._$element[0].blur();
  }
}
