import * as angular from 'angular';
import dropdownService from 'app/components/dropdown/dropdown.service';
import dropdownSelect from './dropdown-select.directive';
import dropdownSelectItem from './dropdown-select-item.directive';

export default angular
  .module('app.components.dropdown', [])
  .service('dropdownService', dropdownService)
  .directive('dropdownSelect', dropdownSelect)
  .directive('dropdownSelectItem', dropdownSelectItem)
  .name;
