import templateUrl from './partials/dropdown-select-item.html';

/* @ngInject */
export default () => ({
  require: '^dropdownSelect',
  transclude: true,
  scope: {
    dropdownItemLabel: '=',
    dropdownSelectItem: '='
  },
  link: (scope, element, attrs, DropdownSelectController) => {
    scope.selectItem = () => {
      if (!scope.dropdownSelectItem.href) {
        DropdownSelectController.select(scope.dropdownSelectItem);
      }
    };
  },
  templateUrl
});
