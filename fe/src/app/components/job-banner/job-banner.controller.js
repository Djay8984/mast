import * as _ from 'lodash';

import Base from 'app/base.class';

import templateUrl from 'app/components/modals/templates/confirmation-modal';

export default class JobBannerController extends Base {
  /* @ngInject */
  constructor(
    $rootScope,
    $state,
    $stateParams,
    MastModalFactory,
    offlinular,
    employeeDecisionService,
    jobStatus,
    referenceDataService,
    employeeRole,
    officeRole
  ) {
    super(arguments);

    this._setupData();
  }

  async _setupData() {
    this.jobStatusIds = _.values(_.pick(this._jobStatus.toObject(),
      ['sdoAssigned', 'resourceAssigned', 'underSurvey', 'underReporting',
      'awaitingEndorserAssignment', 'underEndorsement']));

    this.currentUser = await this._employeeDecisionService.getCurrentUser();
    this.updateOfflineAvailability();

    const referenceDataMap = {
      jobStatuses: 'job.jobStatuses'
    };

    const data = await this._referenceDataService
      .get(_.values(referenceDataMap));

    _.forEach(referenceDataMap, (value, key) =>
      _.set(this, key, _.get(data, value)));
  }

  /**
   * @return {String} SDO The name of the SDO for this job;
   */
  get jobSDO() {
    return _.get(_.find(this.job.offices, {
      officeRole: {
        id: this._officeRole.get('sdo')
      }
    }), 'office.name');
  }

  /**
   * @returns {Boolean} true or false
   */
  editableSDO() {
    return _.includes(this.jobStatusIds, this.job.jobStatus.id);
  }

  async updateOfflineAvailability() {
    this.availableOffline =
      await this._offlinular.getAvailableOffline();
  }

  async saveOffline() {
    this.syncing = true;
    await this._offlinular.saveOffline(this.job.id);
    await this.updateOfflineAvailability();
    this.syncing = false;

    this._$state.go(
      this._$state.current.name,
      _.merge(this._$stateParams, { offline: true, bundleId: this.job.id }),
      { reload: true });
  }

  async saveOnline() {
    const actions = [ { name: 'Return', result: true } ];
    let message = 'Job bundle sync successful';
    try {
      this.syncing = true;
      if (this._$rootScope.CONNECTED) {
        await this._offlinular.saveOnline(this.job.id);
        await this._offlinular.clearOffline(this.job.id);
      } else {
        throw new Error();
      }
    } catch (error) {
      message = 'Job bundle sync unsuccessful';
      actions.push({ name: 'Retry', result: false });
    } finally {
      this.syncing = false;
      if (!(await new this._MastModalFactory({
        templateUrl,
        scope: { message, actions }
      }).activate())) {
        this.saveOnline();
      } else {
        this._$state.go(this._$state.current.name,
          _.merge(this._$stateParams, { offline: false, bundleId: null }),
          { reload: true });
      }
    }
  }

  jobStatus() {
    return _.get(_.find(this.jobStatuses,
      { id: this.job.jobStatus.id }), 'name', null);
  }

  /**
   * @summary isJobClosed()
   * @return {Boolean} true or false depending on the job status;
   */
  get isJobClosed() {
    return this.job.jobStatus.id === this._jobStatus.get('closed');
  }

  async discardOffline() {
    await this._offlinular.clearOffline(this.job.id);
    this._$state.go(this._$state.current,
      _.merge(this._$stateParams, { offline: false, bundleId: null }),
      { reload: true });
  }

  back() {
    this._$state.go(this.backTarget || '^.^',
      this._$stateParams,
      { reload: true });
  }

  // app must be in online mode, the job not already offline & no other jobs
  // available offline, the job must have the current user attached & must be
  // in a status Resource Assigned, Under Survey or Under Reporting
  get canSaveOffline() {
    const jobEmployee = _.find(this.job.employees,
      { lrEmployee: { id: _.get(this, 'currentUser.id') } });

    return !this._$rootScope.OFFLINE &&
      !_.includes(this.availableOffline, this.job.id) &&
        _.isEmpty(this.availableOffline) &&
          jobEmployee && _.includes(_.values(_.pick(this._jobStatus.toObject(),
           ['resourceAssigned', 'underSurvey', 'underReporting'])),
              this.job.jobStatus.id);
  }

  // app must be in online mode, the job not already offline, job must have the
  // current user attached
  get canSaveOnline() {
    const jobEmployee = _.find(this.job.employees,
      { lrEmployee: { id: _.get(this, 'currentUser.id') } });

    return this._$rootScope.CONNECTED &&
      _.includes(this.availableOffline, this.job.id) &&
      jobEmployee;
  }

  get caseId() {
    return _.get(this.job, 'aCase.id') ?
      _.padStart(_.get(this.job, 'aCase.id'), 9, '0') : '-';
  }
}
