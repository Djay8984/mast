import * as angular from 'angular';

import jobBannerDirective from './job-banner.directive';

export default angular
  .module('app.components.jobBanner', [])
  .directive('jobBanner', jobBannerDirective)
  .name;
