import templateUrl from './job-banner.html';
import jobBannerController from './job-banner.controller';

/* @ngInject */
export default () => ({
  bindToController: {
    job: '=',
    attachmentPath: '=',
    attachmentCount: '=',
    backTarget: '=',
    backDisabled: '='
  },
  controller: jobBannerController,
  controllerAs: 'vm',
  restrict: 'AE',
  scope: true,
  templateUrl
});
