import ErrorController from './error.controller';
import errorTemplateUrl from './error.html';

import HeaderController from './header/error-header.controller';
import headerTemplateUrl from './header/error-header.html';

/* @ngInject */
export default function config($stateProvider) {
  $stateProvider.state('error', {
    parent: 'safeMain',
    params: {
      error: null
    },
    views: {
      '': {
        templateUrl: errorTemplateUrl,
        controller: ErrorController,
        controllerAs: 'vm',
        resolve: {

          /* @ngInject */
          error: ($stateParams) => $stateParams.error
        }
      },
      'header@safeMain': {
        templateUrl: headerTemplateUrl,
        controller: HeaderController,
        controllerAs: 'vm'
      }
    }
  });
}
