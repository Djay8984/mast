/* eslint-disable angular/on-watch */
import * as _ from 'lodash';

import templateUrl from 'app/components/modals/templates/confirmation-modal';

const UNAUTHORISED_STATUSES = [401, 403];
const QUERY_REGEX = new RegExp(/\/query/i);

/* @ngInject */
export default function run(
  $rootScope,
  $state,
  $log,
  errorCodes,
  modifyingRequests,
  MastModalFactory,
  navigationService,
  Restangular
) {
  $rootScope.$on('$stateChangeError', onStateChangeError);
  Restangular.setErrorInterceptor(onRestangularError);

  function onStateChangeError(event, toState, toParams, fromState, fromParams,
    error) {
    if (!_.includes(UNAUTHORISED_STATUSES, error.status)) {
      event.preventDefault();
      $log.error(error);
      $state.go('error', { error });
    }
  }

  /**
   * @param {object} response failed request
   * @param {promise} deferred current promise chain for request
   * @param {function} handler any defined catch handling for the promise chain
   * @return {boolean} whether to continue the promise chain (false if handled)
   */
  function onRestangularError(response, deferred, handler) {
    const PAGES_TO_RELOAD = ['case milestone'];

    // handle unauthorised requests
    if (_.includes(UNAUTHORISED_STATUSES, response.status)) {
      const action = modifyingRequests.has(_.toLower(response.config.method)) &&
        !QUERY_REGEX.test(response.config.url) ?
        'submit this form' : 'view this page';
      confirmationModal(
        'Unauthorised',
        `You are not authorised to ${action}`);
      return false;
    } else if (response.status === 409 &&
      response.data.exceptionCode === errorCodes.get('staleness')) {
      const entity = _.toLower(
        _.last(response.data.message.match(/"([\w ]*)"/)));

      confirmationModal(
        'Unable to save changes due to conflict',
        `There is a conflict with this ${entity} and your changes have not \
been saved. You will be sent back to the view mode of this ${entity} so you \
can read the last saved version`,
        'Reload').then(() =>
          _.some(PAGES_TO_RELOAD, (page) => page === entity) ?
            $state.reload() : navigationService.back()
        );

      return false;
    }

    // attempt any higher-level handling first, & then error out
    // deferred.promise.then(handler).catch(() => {
    //   $log.error(response);
    //   confirmationModal('Error', 'Failed resolution');
    // });
    return true;
  }

  function confirmationModal(title, message, name = 'Return') {
    return new MastModalFactory({
      templateUrl,
      scope: { title, message, actions: [ { name } ] }
    }).activate();
  }
}
