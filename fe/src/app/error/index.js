import * as angular from 'angular';

import routes from './config.routes';
import run from './error.run';

export default angular
  .module('app.error', [])
  .config(routes)
  .run(run)
  .name;
