import Base from 'app/base.class';

export default class ErrorController extends Base {
  /* @ngInject */
  constructor(
    $state,
    navigationService,
    appConfig,
    error
  ) {
    super(arguments);
  }

  get debug() {
    return this._appConfig.MAST_DEBUG;
  }

  get name() {
    return this._error.name;
  }

  get message() {
    return this._error.toString();
  }

  get stack() {
    return this._error.stack;
  }

  back() {
    this._navigationService.back();
  }
}
