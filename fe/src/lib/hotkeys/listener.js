import HotkeyMap from './map';
import { ipcRenderer as ipc, webFrame } from 'electron';

export default class HotkeyListener {
  listen() {
    if (!window.hasHotkeyListener) {
      const map = HotkeyMap.get(webFrame);

      ipc.on('hotkey', (event, hotkey) => {
        if (map.hasOwnProperty(hotkey)) {
          map[hotkey]();
        }
      });

      window.hasHotkeyListener = true;
    }
  }
}
