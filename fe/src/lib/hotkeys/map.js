export default class Map {
  static get(webFrame) {
    return {
      'CmdOrCtrl+Plus': () =>
        webFrame.setZoomLevel(webFrame.getZoomLevel() + 1),
      'CmdOrCtrl+-': () =>
        webFrame.setZoomLevel(webFrame.getZoomLevel() - 1),
      'CmdOrCtrl+0': () =>
        webFrame.setZoomLevel(0)
    };
  }
}
