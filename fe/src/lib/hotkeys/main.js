import HotkeyMap from './map';

import app from 'app';
import GlobalShortcut from 'global-shortcut';
import BrowserWindow from 'browser-window';

export default class Main {
  constructor(mainWindow) {
    this.mainWindow = mainWindow;
    this.hotkeys = Object.keys(HotkeyMap.get());
  }

  setup() {
    this.register(null, this.mainWindow);

    app.on('browser-window-focus', () => this.register());
    app.on('browser-window-blur', () => this.unregister());
    app.on('will-quit', () => this.unregister());
  }

  register(event, win) {
    if (!win) {
      win = BrowserWindow.getFocusedWindow() || this.mainWindow;
    }

    const bindHotkeys = () => {
      this.hotkeys.forEach((key) => {
        GlobalShortcut.register(key, () => win.webContents.send('hotkey', key));
      });
    };


    if (win.webContents.isLoading()) {
      win.webContents.on('did-finish-load', bindHotkeys);
    } else {
      bindHotkeys();
    }
  }

  unregister() {
    this.hotkeys.forEach((key) => {
      if (GlobalShortcut.isRegistered(key)) {
        GlobalShortcut.unregister(key);
      }
    });
  }
}
