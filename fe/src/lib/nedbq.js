import Datastore from 'nedb';

export default class QDatastore extends Datastore {
  constructor() {
    super(arguments);
    Reflect.apply(Datastore, this, arguments);
  }

  _promisify(method, ...args) {
    return new Promise((resolve, reject) =>
      method(...args, (err, result) => err ? reject(err) : resolve(result)));
  }

  async find(...args) {
    return this._promisify(super.find.bind(this), ...args);
  }

  async findOne(...args) {
    return this._promisify(super.findOne.bind(this), ...args);
  }

  async count(...args) {
    return this._promisify(super.count.bind(this), ...args);
  }

  async update(...args) {
    return this._promisify(super.update.bind(this), ...args);
  }

  async insert(...args) {
    return this._promisify(super.insert.bind(this), ...args);
  }

  async remove(...args) {
    return this._promisify(super.remove.bind(this), ...args);
  }
}
