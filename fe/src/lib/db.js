/* eslint-disable no-process-env */
import QDatastore from './nedbq';
import _ from 'lodash';

export default class neDb {
  static async initialize(stores) {
    return _.zipObject(_.keys(stores), _.map(stores, createDatastore));

    function createDatastore(options, name) {
      let folder = '.';
      if (process.env.WEBPACK_ENV !== 'dev') {
        folder = __dirname;
      }
      const store = new QDatastore({
        filename: `${folder}/${name}.db`,
        autoload: true
      });
      _.forEach(options.indexes, _.bind(store.ensureIndex, store));
      return store;
    }
  }
}
