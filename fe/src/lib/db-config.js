import * as _ from 'lodash';

export default class dbConfig {
  static async load(db, defaults) {
    const data = await db.find({});
    const newData = _.map(defaults, (value, key) => {
      const existing = _.find(data, { key });

      if (!_.isNil(existing)) {
        value = _.get(existing, 'value');
      }

      return { key, value };
    });

    await db.remove({}, { multi: true });

    return db.insert(newData);
  }

  static async getAll(db) {
    const docs = await db.find({});

    return _.chain(docs)
      .keyBy('key')
      .mapValues('value')
      .value();
  }
}
