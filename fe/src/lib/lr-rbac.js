import _ from 'lodash';

const childProc = require('child_process');
const whoami = 'whoami /all /fo list';
const eol = '\r\n';

export default class Rbac {
  static getGroups() {
    const result = childProc.execSync(whoami);

    const userObj = {
      userInfo: {},
      groups: [],
      privileges: []
    };

    parseRaw(userObj, result.toString());

    function parseRaw(userOne, raw) {
      parseUser(userOne, raw.match(/^USER[\s\S]*?^GROUP/gm));
      parseGroups(userOne, raw.match(/^GROUP[\s\S]*?^PRIVILEGES/gm));
      parsePrivileges(userOne, raw.match(/^PRIVILEGES[\s\S]*/gm));
    }

    function parseUser(userTwo, userLines) {
      const lines = userLines[0].split(eol + eol);

      parse(lines[1]);

      function parse(line) {
        let row = [];

        Array.from(line.split(eol)).forEach((section) => {
          row = section.split(':');

          if (row[0] === 'User Name') {
            const nameParts = row[1].split('\\');

            userTwo.userInfo.domain = nameParts[0].trim();
            userTwo.userInfo.name = nameParts[1].trim();
          } else if (row[0] === 'SID') {
            userTwo.userInfo.sid = row[1].trim();
          }
        });
      }
    }

    function parseGroups(userThree, groupLines) {
      const lines = groupLines[0].split(eol + eol);

      lines.shift();
      lines.pop();

      Array.from(lines).forEach((line) => {
        parse(line.split(eol));
      });

      function parse(group) {
        let row = [];

        const groupData = {};

        /* eslint max-statements: 0 */
        _.forEach(group, (section) => {
          row = section.split(':');

          if (row[0] === 'Group Name') {
            groupNameParts(row);
          } else if (row[0] === 'Type') {
            groupData.type = row[1].trim();
          } else if (row[0] === 'SID') {
            groupData.sid = row[1].trim();
          } else if (row[0] === 'Attributes') {
            groupData.attributes = [];
            Array.from(row[1].split(',')).forEach((attr) => {
              groupData.attributes.push(attr.trim());
            });
          }
        });

        function groupNameParts(input) {
          const nameParts = input[1].split('\\');

          if (nameParts[1]) {
            groupData.domain = nameParts[0].trim();
            groupData.name = nameParts[1].trim();
          } else {
            groupData.domain = '';
            groupData.name = nameParts[0].trim();
          }
        }

        userThree.groups.push(groupData);
      }
    }

    function parsePrivileges(userFour, privilegesLines) {
      const lines = privilegesLines[0].split(eol + eol);

      lines.shift();
      lines.pop();

      Array.from(lines).forEach((line) => {
        parse(line.split(eol));
      });

      function parse(privilege) {
        let row = [];

        const privilegeData = {};

        Array.from(privilege).forEach((section) => {
          row = section.split(':');

          if (row[0] === 'Privilege Name') {
            privilegeData.name = row[1].trim();
          } else if (row[0] === 'Description') {
            privilegeData.description = row[1].trim();
          } else if (row[0] === 'State') {
            privilegeData.state = row[1].trim();
          }
        });
        userFour.privileges.push(privilegeData);
      }
    }

    function getRbacHeader(userFive) {
      let groups = '';

      Array.from(_.map(userFive.groups, 'name')).forEach((group) => {
        groups = groups.concat(group).concat(',');
      });

      return groups;
    }

    return getRbacHeader(userObj);
  }
}
