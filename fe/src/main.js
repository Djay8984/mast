import app from 'app';
import BrowserWindow from 'browser-window';

import devHelper from './lib/dev-helper';
import db from './lib/db';
import dbConfig from './lib/db-config';


import Hotkey from './lib/hotkeys/main';

// .js makes webpack resolve it first and do some magic!
import defaultConfig from './config.js';

// spell checking
// import spellChecker from './lib/spellchecker/lib/spellchecker';
// global.spellChecker = spellChecker;

// import buildEditorContextMenu from 'electron-editor-context-menu';
// global.buildEditorContextMenu = buildEditorContextMenu;

process.env.LANG = 'en_GB';

const path = require('path');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow = null;

class Application {
  constructor(config) {
    this.defaultConfig = config;
  }

  run() {
    this.setCommandLine();

    app.on('window-all-closed', () => {
      if (process.platform !== 'darwin') {
        app.quit();
      }
    });

    app.on('ready', () => this.activate());

    global.reload = () => this.reloadApp();
  }

  setCommandLine() {
    app.commandLine.appendSwitch('ignore-certificate-errors');

    // debug devtools
    // app.commandLine.appendSwitch('remote-debugging-port', '8315');
    // app.commandLine.appendSwitch('host-rules', 'MAP * 127.0.0.1');
  }

  async resetConfig() {
    const dbInstanceConfig = (await this.getDbInstance()).config;

    await dbConfig.load(dbInstanceConfig, this.defaultConfig);

    const configData = await this.getConfigData();

    return this.setConfigGlobal(configData);
  }

   async activate() {
     const conf = await this.resetConfig();
     this.setConfigGlobal(conf);

     mainWindow = this.createMainWindow(conf);
     const hotkey = new Hotkey(mainWindow);
     hotkey.setup();

     return mainWindow;
   }

  async getConfigData() {
    const dbInstanceConfig = (await this.getDbInstance()).config;

    return dbConfig.getAll(dbInstanceConfig);
  }

  setConfigGlobal(data) {
    global.config = data;
    return data;
  }

  getIndexAndOptions() {
    const width = 1024;
    const height = 768;
    const browserWindowOptions = {
      title: app.getName(),
      width,
      height,
      minWidth: width,
      minHeight: height
    };
    let index = path.join('file://', __dirname, '/index.html');

    /* eslint no-process-env: 0 */
    if (process.env.WEBPACK_ENV === 'dev') {
      index = 'http://localhost:9001/index.html';
      browserWindowOptions['web-preferences'] = {
        'web-security': false
      };

      app.commandLine.appendSwitch('disable-web-security');
    }

    return [ index, browserWindowOptions ];
  }

  createMainWindow(appConfig) {
    const [ index, browserWindowOptions ] = this.getIndexAndOptions();

    // Create the browser window.
    mainWindow = new BrowserWindow(browserWindowOptions);

    this.setWindowProperties(appConfig);

    mainWindow.loadURL(index);
    mainWindow.maximize();

    let redirecting = false;
    mainWindow.webContents.on('did-get-redirect-request',
      (event, oldUrl, newUrl) => {
        if (!redirecting) {
          startRedirect(event.sender, newUrl);
        }
      });

    function finishRedirect() {
      redirecting = false;
      mainWindow.webContents.removeListener('did-navigate', finishRedirect);
    }

    function startRedirect(webContents, newUrl) {
      const url = newUrl.indexOf('/mast') === -1 ? newUrl : index;
      redirecting = true;
      mainWindow.webContents.send('redirect');
      mainWindow.webContents.on('did-navigate', finishRedirect);
      webContents.stop();
      webContents.loadURL(url);
    }


    if (Boolean(appConfig.MAST_DEBUG)) {
      try {
        BrowserWindow.addDevToolsExtension(path.join(__dirname,
          'extensions/accessibility-developer-tools'));
      } catch (error) {
        // uh oh
      }
    }

    return mainWindow;
  }

  setWindowProperties(appConfig) {
    const value = Boolean(appConfig.MAST_DEVTOOLS);

    if (value) {
      mainWindow.openDevTools();
      devHelper.setDevMenu();
    } else {
      mainWindow.closeDevTools();
    }

    mainWindow.setMenuBarVisibility(false);
    mainWindow.setAutoHideMenuBar(value);
  }

  async getDbInstance() {
    const dataStores = {
      config: {
        indexes: [
          {
            fieldName: 'key',
            unique: true
          }
        ]
      }
    };

    if (!this._dbInstance) {
      this._dbInstance = db.initialize(dataStores);
      global.db = this._dbInstance;
    }

    return this._dbInstance;
  }

  async reloadApp() {
    const config = await this.resetConfig();

    mainWindow.reloadIgnoringCache();

    return this.setWindowProperties(config);
  }
}

new Application(defaultConfig).run();
