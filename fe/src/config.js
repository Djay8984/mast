/* eslint-disable */
import dotenv from 'dotenv';
import path from 'path';
import fs from 'fs';

const keywords = {
  'true': true,
  'false': false,
  'undefined': undefined,
  'null': null
};

dotenv.load({silent: true});

let env = process.env;
let filePath = path.join(__dirname, '..', '.env.example');
let defaults = dotenv.parse(fs.readFileSync(filePath));
let config = Object.keys(defaults).reduce((obj, key) => {
  let val = env.hasOwnProperty(key) ? env[key] : defaults[key];
  obj[key] = keywords.hasOwnProperty(val) ? keywords[val] : val;
  return obj
}, {});

module.exports = config;
