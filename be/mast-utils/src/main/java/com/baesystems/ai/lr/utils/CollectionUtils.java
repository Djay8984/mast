package com.baesystems.ai.lr.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.baesystems.ai.lr.dto.base.IdDto;

public final class CollectionUtils
{

    private CollectionUtils()
    {
    }

    public static List<Long> nullIfEmpty(final List<Long> list)
    {
        List<Long> result = null;
        if (list != null && !list.isEmpty())
        {
            result = list;
        }
        return result;
    }

    public static <T> Stream<T> nullSafeStream(final Collection<T> collection)
    {
        return nullSafeCollection(collection).stream();
    }

    public static <T> Collection<T> nullSafeCollection(final Collection<T> collection)
    {
        return collection != null ? collection : Collections.<T>emptyList();
    }

    public static <T extends IdDto> List<Long> toIDList(final Collection<T> dtoList)
    {
        return nullSafeStream(dtoList).map(IdDto::getId).collect(Collectors.toList());
    }

    /**
     * @param obj the object to check
     * @return {@code true} if the object is a {@link Collection} or {@link Map}
     */
    public static boolean isCollection(final Object obj)
    {
        return obj instanceof Collection || obj instanceof Map;
    }
}
