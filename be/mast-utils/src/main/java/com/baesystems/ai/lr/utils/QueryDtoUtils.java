package com.baesystems.ai.lr.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;
import com.baesystems.ai.lr.dto.query.QueryDto;
import com.baesystems.ai.lr.exception.MastSystemException;

public final class QueryDtoUtils
{

    private QueryDtoUtils()
    {
    }

    /**
     * General method to handle various manipulations required on the QueryDto
     *
     * @param queryDto
     * @param queryClass
     * @return
     * @throws MastSystemException
     */
    public static <T extends QueryDto> T sanitiseQueryDto(final T queryDto, final Class<T> queryClass) throws MastSystemException
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(queryClass, queryClass).customize(new CustomMapper<T, T>()
        {
            @Override
            public void mapAtoB(final T original, final T result, final MappingContext context)
            {
                Stream<Field> fields = Arrays.stream(FieldUtils.getAllFields(result.getClass()));

                // null out empty lists
                fields.filter(field -> field.getType().isAssignableFrom(List.class))
                        .filter(field -> fieldIsAnEmptyList(field, result))
                        .forEach(field -> nullifyProperty(field, result));

                fields = Arrays.stream(FieldUtils.getAllFields(result.getClass()));
                // deal with wildcards in strings which are annotated as using the like comparator (i.e. supporting
                // wildcards)
                fields.filter(field -> field.getType().isAssignableFrom(String.class))
                        .filter(field -> field.getAnnotation(UsesLikeComparator.class) != null)
                        .forEach(field -> convertStringsForWildcardCharacters(field, result));
            }
        }).byDefault().register();
        return factory.getMapperFacade().map(queryDto, queryClass);
    }

    /**
     * The field is assumed to be a List at this point from the previous filter
     */
    private static boolean fieldIsAnEmptyList(final Field field, final QueryDto resultDto)
    {
        boolean include;
        try
        {
            final List instance = (List) PropertyUtils.getProperty(resultDto, field.getName());
            include = instance != null && instance.isEmpty();
        }
        catch (final IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            throw new MastSystemException("Error reading QueryDto", exception);
        }
        return include;
    }

    private static void nullifyProperty(final Field field, final QueryDto resultDto)
    {
        try
        {
            PropertyUtils.setProperty(resultDto, field.getName(), null);
        }
        catch (final IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            throw new MastSystemException("Error removing empty lists on QueryDto", exception);
        }
    }

    /**
     * Each field that's a String needs the % escaping (as this isn't a valid wildcard entry from the UI), and the *
     * converting to a % to work as required in the SQL LIKE query.
     *
     * @param field
     * @param resultDto
     */
    private static void convertStringsForWildcardCharacters(final Field field, final QueryDto resultDto)
    {
        try
        {
            final String searchString = (String) PropertyUtils.getProperty(resultDto, field.getName());
            PropertyUtils.setProperty(resultDto, field.getName(), escapeSpecialCharsAndConvertWildcards(searchString));
        }
        catch (final IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            throw new MastSystemException("Error handling Strings on QueryDto", exception);
        }
    }

    public static String escapeSpecialCharsAndConvertWildcards(final String searchString)
    {
        String search = null;
        if (searchString != null && !searchString.isEmpty())
        {
            // SQL uses % as wildcard for any number of characters, so escape %s
            search = searchString.replace("%", "\\%");

            // _ is a wildcard for a single character in SQL (which we don't support)
            search = search.replace("_", "\\_");

            // finally, replace * with % as the FE requires the user to enter * as a wildcard.
            search = search.replace("*", "%");
        }

        return search;
    }
}
