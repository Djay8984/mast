package com.baesystems.ai.lr.utils;

import static java.text.MessageFormat.format;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.base.IdDto;
import com.baesystems.ai.lr.utils.exception.ComparisonException;

/**
 * This class has been taken from the mast-it project and provides the same methods for comparing DTOs. This class is
 * currently outside PDM because CyclicComplexity errors result which are difficult to resolve. This is production code
 * so therefore should be within PDM.
 *
 */
public final class DtoUtils
{
    public static final String FIELD_MISMATCH_ERROR = "Response property \"{0}\": {1} did not match expected value: {2}";
    private static final String UNEQUAL_NUMBER_OF_FIELDS = "There are unequal numbers of fields in the response and expected object.\nResponse:\n\t{0}\nExpected:\t\n{1}";
    private static final String EXPECTED_OBJECT_NOT_FOUND = "The expected object with ID {0} was not found.";
    private static final String UNEXPECTED_NON_EMPTY_COLLECTION = "Expected empty \"{0}\" however got {1}";
    private static final String[] IGNORE_FIELDS = new String[]{"serialVersionUID", // Will almost always fail
                                                               "internalId", // Will almost always fail
                                                               "before", // Collections internal field
                                                               "after", // Collections internal field
                                                               "hash", // Collections internal field
                                                               "next", // Collections internal field
                                                               "updatedDate", // Audit information
                                                               "updatedBy"}; // Audit information

    private static final MapperFactory MAPPER_FACTORY = new DefaultMapperFactory.Builder().build();

    private DtoUtils()
    {

    }

    /**
     * Leverage Orika reflective mapping to get a deep copy of an object, without the source type having to adhere to any specific interface
     * eg cloneable
     * //todo Move to an ObjectUtils, not DTO specific
     */
    public static <T> T simpleCopy(final T sourceObject)
    {
        return simpleCopy(sourceObject, (Class<T>) sourceObject.getClass());
    }

    public static <T> T simpleCopy(final T sourceObject, final Class<T> sourceClass)
    {
        return MAPPER_FACTORY.getMapperFacade().map(sourceObject, sourceClass);
    }


    public static void compareFields(final Object responseObj, final Object expectedDto, final boolean allowNull, final String[] ignored)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ComparisonException
    {
        final List<Field> responseFields = Arrays.asList(FieldUtils.getAllFields(responseObj.getClass()))
                .stream()
                .filter(field -> !field.isSynthetic())
                .collect(Collectors.toList());

        final List<Field> expectedFields = Arrays.asList(FieldUtils.getAllFields(expectedDto.getClass()))
                .stream()
                .filter(field -> !field.isSynthetic())
                .collect(Collectors.toList());

        compareFieldTotals(responseFields, expectedFields);

        final ArrayList<String> errors = new ArrayList<String>();

        for (final Field field : expectedFields)
        {
            final String fieldName = field.getName();
            if (Arrays.asList(IGNORE_FIELDS).contains(fieldName) || ignored != null && Arrays.asList(ignored).contains(fieldName))
            {
                continue;
            }

            final String compareResult = compareField(responseObj, expectedDto, fieldName, allowNull, ignored);
            if (compareResult != null)
            {
                errors.add(compareResult);
            }
        }

        if (!errors.isEmpty())
        {
            final StringBuilder builder = new StringBuilder();
            for (final String err : errors)
            {
                builder.append(err).append('\n');
            }
            throw new ComparisonException(builder.toString());
        }
    }

    /**
     * Compares the total number of {@link Field}s in the response against the expected number
     *
     * @param responseFields the list of response Fields
     * @param expectedFields the list of expected Fields
     * @throws ComparisonException if there are unequal numbers of Fields
     */
    private static void compareFieldTotals(final List<Field> responseFields, final List<Field> expectedFields)
            throws ComparisonException
    {
        if (responseFields.size() != expectedFields.size())
        {
            throw new ComparisonException(
                    format(UNEQUAL_NUMBER_OF_FIELDS,
                            listFieldNames(responseFields),
                            listFieldNames(expectedFields)));
        }
    }

    /**
     * Compares the field in both objects with the given name.<br>
     * If the field is a {LinkResource}, its ID is compared via calling {@link #compareFields(Object, Object, boolean)}.
     * <br>
     * If the field is a Collection or Map, its values are compared via calling
     * {@link #compareCollection(Object, Object, boolean)}.<br>
     * Otherwise, the comparison uses the response field's {@code .equals()} method.
     *
     * @param responseObj the JSON object in the server response
     * @param expectedDto the expected DTO containing the values to compare against
     * @param fieldName the name of the field to compare
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @return {@code null} if there are no errors, otherwise a String containing the field name, response and expected
     *         values
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ComparisonException
     */
    private static String compareField(final Object responseObj, final Object expectedDto,
            final String fieldName, final boolean allowNull, final String[] ignored)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ComparisonException
    {
        String retVal = null;
        final Object expectedProp = PropertyUtils.getProperty(expectedDto, fieldName);
        final Object responseProp = PropertyUtils.getProperty(responseObj, fieldName);

        if (responseProp instanceof BaseDto && expectedProp instanceof BaseDto)
        {
            compareFields(responseProp, expectedProp, allowNull, processNestedIgnoredFields(fieldName, "\\.", ignored));
        }
        // Else if they are both collections, compare the inner-fields
        else if (CollectionUtils.isCollection(responseProp) && CollectionUtils.isCollection(expectedProp))
        {
            compareCollection(responseProp, expectedProp, allowNull, fieldName, ignored);
        }
        else
        {
            retVal = compareValue(responseProp, expectedProp, fieldName, allowNull);
        }
        return retVal;
    }

    private static String compareValue(final Object responseProp, final Object expectedProp,
            final String fieldName, final boolean allowNull)
    {
        String retVal = null;
        // Else if responseProp isn't null, compare and return an error if needed
        if (responseProp != null)
        {
            if (!responseProp.equals(expectedProp) && expectedProp != null || expectedProp == null && !allowNull)
            {
                retVal = format(FIELD_MISMATCH_ERROR, fieldName, responseProp, expectedProp);
            }
        }
        // Else if responseProp is null, but the expectedProp isn't null, return an error
        else if (expectedProp != null)
        {
            retVal = format(FIELD_MISMATCH_ERROR, fieldName, null, expectedProp);
        }
        return retVal;
    }

    /**
     * Iterates through both Collections using their {@link Iterator}s and calls
     * {@link #compareFields(Object, Object, boolean)} on each element.<br>
     * Note that if the elements in both Collections are not in the same order, this method is likely to fail. It is
     * probably wise to avoid using this method for Maps at the moment.
     *
     * @param responseColl the Collection in the response
     * @param expectedColl the expected Collection
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @param fieldName the name of the field containing this collection
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ComparisonException
     */
    private static void compareCollection(final Object responseColl, final Object expectedColl, final boolean allowNull, final String fieldName,
            final String[] ignored)
                    throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ComparisonException
    {
        final Iterator<?> responseIt = getIterator(responseColl);
        final Iterator<?> expectedIt = getIterator(expectedColl);

        if (responseIt.hasNext() && !expectedIt.hasNext())
        {
            throw new ComparisonException(format(UNEXPECTED_NON_EMPTY_COLLECTION, fieldName, responseColl));
        }

        Object responseObj;
        Object expectedObj;
        while (expectedIt.hasNext())
        {
            expectedObj = expectedIt.next();
            responseObj = getResponseItem(getIterator(responseColl), getCollectionItemId(expectedObj));

            compareFields(responseObj, expectedObj, allowNull, processNestedIgnoredFields(fieldName, "\\[\\]", ignored));
        }
    }

    /**
     * @param obj the {@link Collection} or {@link Map} object
     * @return the object's {@link Iterator}
     */
    public static Iterator<?> getIterator(final Object obj)
    {
        Iterator<?> retVal;
        if (obj instanceof Collection)
        {
            retVal = ((Collection<?>) obj).iterator();
        }
        else
        {
            retVal = ((Map<?, ?>) obj).entrySet().iterator();
        }
        return retVal;
    }

    /**
     * Converts the list of {@link Field}s into a single String containing a description of each Field separated by a
     * {@code \n} character
     *
     * @param fields the list of fields
     * @return the listed field names
     */
    private static String listFieldNames(final List<Field> fields)
    {
        final StringBuilder builder = new StringBuilder();
        for (final Field field : fields)
        {
            builder.append(field).append('\n');
        }
        return builder.toString();
    }

    private static Object getResponseItem(final Iterator<?> responseIt, final Long id) throws ComparisonException
    {
        Object responseObj = null;
        boolean found = false;
        while (responseIt.hasNext())
        {
            responseObj = responseIt.next();
            if (getCollectionItemId(responseObj).equals(id))
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            throw new ComparisonException(format(EXPECTED_OBJECT_NOT_FOUND, id));
        }

        return responseObj;
    }

    private static Long getCollectionItemId(final Object item)
    {
        Long retVal = null;
        try
        {
            // map
            retVal = (Long) ((Map.Entry<?, ?>) item).getKey();
        }
        catch (final ClassCastException cce)
        {
            // list
            retVal = ((IdDto) item).getId();
        }
        return retVal;
    }

    /**
     * This method extracts all of the nested properties that are to be ignored by searching for any values in the list
     * of ignored fields matching this field name and then splitting at the dot to get the sub fields or [] for array
     * elements. I can be called recursively to reach any level
     */
    private static String[] processNestedIgnoredFields(final String fieldName, final String splitter, final String[] ignoredList)
    {
        return ignoredList == null ? null : Arrays.stream(ignoredList)
                .map(ignoredField -> ignoredField.split(splitter, 2))
                .filter(ignoredFields -> fieldName.equals(ignoredFields[0]) && ignoredFields.length > 1)
                .map(ignoredFields -> ignoredFields[1])
                .toArray(size -> new String[size]);
    }
}
