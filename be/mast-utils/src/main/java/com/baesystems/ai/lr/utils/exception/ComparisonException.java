package com.baesystems.ai.lr.utils.exception;

import org.springframework.http.HttpStatus;

public class ComparisonException extends Exception
{
    private static final long serialVersionUID = -3117847427810816351L;

    public ComparisonException()
    {
        super();
    }

    public ComparisonException(final String message)
    {
        super(message);
    }

    public ComparisonException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public ComparisonException(final Throwable cause)
    {
        super(cause);
    }

    public int getErrorCode()
    {
        return HttpStatus.UNPROCESSABLE_ENTITY.value();
    }
}
