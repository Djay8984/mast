package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.enums.StaleCheckType;
import org.apache.commons.lang3.StringUtils;

import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.exception.StaleObjectException;

public final class StaleCheckUtils
{
    private StaleCheckUtils()
    {
        // Left deliberately blank to prevent instantiation.
    }

    /**
     * This method throws an exception if the staleness hash on the database object differs from the one passed in the
     * updated object. The passed entity enum is used in the UI to display a human readable name of the entity type in
     * question.
     *
     * @param originalObject
     * @param updatedObject
     * @param entityName
     * @throws StaleObjectException
     */
    public static void checkNotStale(final StaleObject originalObject, final StaleObject updatedObject, final StaleCheckType entityType)
            throws StaleObjectException
    {
        if (originalObject != null
                && updatedObject != null
                && !StringUtils.equalsIgnoreCase(originalObject.getStalenessHash(), updatedObject.getStalenessHash()))
        {
            throw new StaleObjectException(entityType.getTypeString());
        }
    }
}
