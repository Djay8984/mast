package com.baesystems.ai.lr.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;

public final class ComparisonUtils
{
    private ComparisonUtils()
    {

    }

    /**
     * Method to test if two objects match by comparing their to level fields (ie the fields on that object with equals
     * not descending through nested objects). The method check all fields except those passes in as ignored and the
     * ones that are only for internal use. I also checks that the objects are off the same class and have the same
     * number of fields
     */
    public static boolean objectsMatch(final Object obj1, final Object obj2, final String[] ignored)
    {
        // test if they are the same class
        boolean match = obj1.getClass().isInstance(obj2);

        if (match)
        { // skip this if the comparison has already failed
            // add internal fields to the list to be ignored
            final List<String> ignoredList = new ArrayList<String>();
            ignoredList.add("internalId");
            ignoredList.add("serialVersionUID");
            ignoredList.add("actionTakenDate");
            ignoredList.add("updatedBy");
            ignoredList.add("updatedDate");

            if (ignored != null)
            { // add the list of ignored fields passed to the function
                ignoredList.addAll(Arrays.asList(ignored));
            }

            // get the fields to be checked
            final List<String> fields1 = getDeclaredFields(obj1.getClass()).stream()
                    .map(field -> field.getName())
                    .filter(fieldName -> !ignoredList.contains(fieldName))
                    .collect(Collectors.toList());
            final List<String> fields2 = getDeclaredFields(obj2.getClass()).stream()
                    .map(field -> field.getName())
                    .filter(fieldName -> !ignoredList.contains(fieldName))
                    .collect(Collectors.toList());

            match = fields1.containsAll(fields2) && fields2.containsAll(fields1) && compareObjectFields(obj1, obj2, fields1);
        }
        return match;
    }

    public static boolean listObjectsMatch(final List obj1, final List obj2, final String[] ignored)
    {
        boolean match = true;
        if (obj1 != null && obj2 != null)
        {
            if (obj1.size() != obj2.size())
            {
                match = false;
            }
            else
            {
                match = iterateAndCompareLists(obj1, obj2, ignored);
            }
        }
        else if (obj1 == null && obj2 != null || obj1 != null && obj2 == null)
        {
            // One of the objects is null
            match = false;
        }
        return match;
    }

    private static boolean iterateAndCompareLists(final List listObject1, final List listObject2, final String[] ignored)
    {
        boolean match = true;
        final Iterator itObj1 = listObject1.iterator();
        while (itObj1.hasNext() && match)
        {
            final Object element1 = itObj1.next();
            final Iterator itObj2 = listObject2.iterator();
            boolean elementInList = false;
            while (itObj2.hasNext() && !elementInList)
            {
                elementInList = objectsMatch(element1, itObj2.next(), ignored);
            }
            match = elementInList;
        }
        return match;
    }

    /**
     * Given two objects by comparing a list of named fields. If the field is missing from either object this results in
     * false.
     */
    private static boolean compareObjectFields(final Object obj1, final Object obj2, final List<String> fieldNames)
    {
        boolean match = true;
        for (final String fieldName : fieldNames)
        { // for all the fields in the list
            try
            {
                final Object field1 = PropertyUtils.getProperty(obj1, fieldName);
                final Object field2 = PropertyUtils.getProperty(obj2, fieldName);

                if (!compareObjects(field1, field2))
                {
                    match = false;
                    break;
                }
            }
            catch (SecurityException | IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
            { // if the field is not found on one of the objects then they are not the same so return false
                match = false;
                break;
            }
        }
        return match;
    }

    /**
     * Compare the two give objects and return true if they are the same. If one object is a collection then the method
     * test that they both are and compares the elements of each (calling recursively) and lengths. If not the null safe
     * equals is used on the objects.
     */
    private static boolean compareObjects(final Object obj1, final Object obj2)
    {
        boolean match = true;
        if (CollectionUtils.isCollection(obj1) || CollectionUtils.isCollection(obj2))
        { // check if either are collections
            // if one is then they both should be if the objects are the same
            match = CollectionUtils.isCollection(obj1) && CollectionUtils.isCollection(obj2);
            if (match)
            {
                match = compareIterators(DtoUtils.getIterator(obj1), DtoUtils.getIterator(obj2));
            }
        }
        else if (!Objects.equals(obj1, obj2))
        { // if they are not both lists then compare them with a normal equals (null safe as they can both be null)
            match = false;
        }
        return match;
    }

    /**
     * Method for comparing two iterators. I checks their lengths and every element.
     */
    private static boolean compareIterators(final Iterator<?> iterator1, final Iterator<?> iterator2)
    {
        boolean match = true;

        while (iterator1.hasNext())
        { // for all the elements in 1
            if (!iterator2.hasNext())
            { // if one has more fields than 2 then they are not the same
                match = false;
                break;
            }
            else if (!compareObjects(iterator1.next(), iterator2.next()))
            { // recursive call to do the comparison of the element
                match = false;
                break;
            }
        }
        if (match && iterator2.hasNext())
        { // if the loop ended check that this is the end of 2 as well. If not they are not the same.
            match = false;
        }

        return match;
    }

    private static List<Field> getDeclaredFields(final Class<?> clazz)
    {
        final List<Field> fields = new ArrayList<Field>();

        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        if (clazz.getSuperclass() != null)
        {
            fields.addAll(getDeclaredFields(clazz.getSuperclass()));
        }

        return fields;
    }
}
