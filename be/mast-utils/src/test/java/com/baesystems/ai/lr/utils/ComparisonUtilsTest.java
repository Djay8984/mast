package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.services.ProductDto;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DtoUtils.class, CollectionUtils.class})
public class ComparisonUtilsTest
{
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;

    @Test
    public void objectsMatchTestSimpleSame()
    {
        final LinkResource link1 = new LinkResource(ONE);
        final LinkResource link2 = new LinkResource(ONE);

        assertTrue(ComparisonUtils.objectsMatch(link1, link2, null));
    }

    @Test
    public void objectsMatchTestSimpleDifferent()
    {
        final LinkResource link1 = new LinkResource(ONE);
        final LinkResource link2 = new LinkResource(TWO);

        assertFalse(ComparisonUtils.objectsMatch(link1, link2, null));
    }

    @Test
    public void objectsMatchTestDifferntValueIgnored()
    {
        final LinkResource link1 = new LinkResource(ONE);
        final LinkResource link2 = new LinkResource(TWO);

        assertTrue(ComparisonUtils.objectsMatch(link1, link2, new String[]{"id"}));
    }

    @Test
    public void objectsMatchTestSimpleListSame()
    {
        final ProductListDto list1 = new ProductListDto();
        final ProductListDto list2 = new ProductListDto();

        list1.setProductList(new ArrayList<ProductDto>());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().get(0).setId(ONE);
        list1.getProductList().get(1).setId(TWO);

        list2.setProductList(new ArrayList<ProductDto>());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().get(0).setId(ONE);
        list2.getProductList().get(1).setId(TWO);

        assertTrue(ComparisonUtils.objectsMatch(list1, list2, null));
    }

    @Test
    public void objectsMatchTestSimpleListDifferent()
    {
        final ProductListDto list1 = new ProductListDto();
        final ProductListDto list2 = new ProductListDto();

        list1.setProductList(new ArrayList<ProductDto>());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().get(0).setId(ONE);
        list1.getProductList().get(1).setId(TWO);

        list2.setProductList(new ArrayList<ProductDto>());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().get(0).setId(ONE);
        list2.getProductList().get(1).setId(THREE);

        assertFalse(ComparisonUtils.objectsMatch(list1, list2, null));
    }

    @Test
    public void objectsMatchTestSimpleListDifferentLengths1()
    {
        final ProductListDto list1 = new ProductListDto();
        final ProductListDto list2 = new ProductListDto();

        list1.setProductList(new ArrayList<ProductDto>());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().get(0).setId(ONE);

        list2.setProductList(new ArrayList<ProductDto>());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().get(0).setId(ONE);
        list2.getProductList().get(1).setId(TWO);

        assertFalse(ComparisonUtils.objectsMatch(list1, list2, null));
    }

    @Test
    public void objectsMatchTestSimpleListDifferentLengths2()
    {
        final ProductListDto list1 = new ProductListDto();
        final ProductListDto list2 = new ProductListDto();

        list1.setProductList(new ArrayList<ProductDto>());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().add(new ProductDto());
        list1.getProductList().get(0).setId(ONE);
        list1.getProductList().get(1).setId(TWO);

        list2.setProductList(new ArrayList<ProductDto>());
        list2.getProductList().add(new ProductDto());
        list2.getProductList().get(0).setId(ONE);

        assertFalse(ComparisonUtils.objectsMatch(list1, list2, null));
    }

    @Test
    public void objectsMatchTestDifferentObjects()
    {
        final ProductListDto obj1 = new ProductListDto();
        final ProductDto obj2 = new ProductDto();

        assertFalse(ComparisonUtils.objectsMatch(obj1, obj2, null));
    }
}
