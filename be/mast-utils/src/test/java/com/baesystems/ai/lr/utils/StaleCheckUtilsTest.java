package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.enums.StaleCheckType;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.StaleObjectException;

@RunWith(MockitoJUnitRunner.class)
public class StaleCheckUtilsTest
{
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private class DummyStaleCheck implements StaleObject
    {
        private String stalenessHash;

        @Override
        public String getStalenessHash()
        {
            return stalenessHash;
        }

        @Override
        public void setStalenessHash(final String newStalenessHash)
        {
            this.stalenessHash = newStalenessHash;
        }

        @Override
        public String getStringRepresentationForHash()
        {
            // Don't care about this for this implementation
            return null;
        }
    }

    @Test
    public void bothHashesAreNull() throws MastBusinessException
    {
        final DummyStaleCheck originalObject = new DummyStaleCheck();
        final DummyStaleCheck updatedObject = new DummyStaleCheck();

        StaleCheckUtils.checkNotStale(originalObject, updatedObject, StaleCheckType.ASSET);
    }

    @Test(expected = StaleObjectException.class)
    public void newObjectHasHashButExistingHasNoHash() throws MastBusinessException
    {
        final DummyStaleCheck originalObject = new DummyStaleCheck();
        final DummyStaleCheck updatedObject = new DummyStaleCheck();
        updatedObject.setStalenessHash("Test");

        StaleCheckUtils.checkNotStale(originalObject, updatedObject, StaleCheckType.ASSET);
    }

    @Test(expected = StaleObjectException.class)
    public void newObjectHashNoHashButExistingHas() throws MastBusinessException
    {
        final DummyStaleCheck originalObject = new DummyStaleCheck();
        originalObject.setStalenessHash("Test");
        final DummyStaleCheck updatedObject = new DummyStaleCheck();

        StaleCheckUtils.checkNotStale(originalObject, updatedObject, StaleCheckType.ASSET);
    }

    @Test(expected = StaleObjectException.class)
    public void bothHaveHashesButTheyDiffer() throws MastBusinessException
    {

        final DummyStaleCheck originalObject = new DummyStaleCheck();
        originalObject.setStalenessHash("Test1");
        final DummyStaleCheck updatedObject = new DummyStaleCheck();
        updatedObject.setStalenessHash("Test2");

        StaleCheckUtils.checkNotStale(originalObject, updatedObject, StaleCheckType.ASSET);
    }

    @Test
    public void bothHaveTheSameHash() throws MastBusinessException
    {

        final DummyStaleCheck originalObject = new DummyStaleCheck();
        originalObject.setStalenessHash("Test");
        final DummyStaleCheck updatedObject = new DummyStaleCheck();
        updatedObject.setStalenessHash("Test");

        StaleCheckUtils.checkNotStale(originalObject, updatedObject, StaleCheckType.ASSET);
    }
}
