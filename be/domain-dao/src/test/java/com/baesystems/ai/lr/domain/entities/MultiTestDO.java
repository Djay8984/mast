package com.baesystems.ai.lr.domain.entities;

public class MultiTestDO implements MultiDO
{
    private Long id;

    private TestDO test1;

    private TestDO test2;

    public MultiTestDO(final TestDO test1, final TestDO test2)
    {
        super();
        this.id = test1 == null ? test2 == null ? null : test2.getId() : test1.getId();
        this.test1 = test1;
        this.test2 = test2;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public TestDO getTest1()
    {
        return this.test1;
    }

    public void setTest1(final TestDO test1)
    {
        this.test1 = test1;
    }

    public TestDO getTest2()
    {
        return this.test2;
    }

    public void setTest2(final TestDO test2)
    {
        this.test2 = test2;
    }

    @Override
    public boolean equals(final Object obj2)
    {
        boolean returnValue = false;

        if (MultiTestDO.class.isInstance(obj2))
        {
            final MultiTestDO multiTest2 = MultiTestDO.class.cast(obj2);
            returnValue = multiTest2.getId() != null && this.id != null && this.id.equals(multiTest2.getId());
        }

        return returnValue;
    }

    @Override
    public int hashCode()
    {
        int hashCode = super.hashCode();

        if (this.id != null)
        {
            hashCode = this.id.hashCode();
        }

        return hashCode;
    }

    @Override
    public Object getContent()
    {
        return this.test1 == null ? this.test2 : this.test1;
    }

    @Override
    public void setContent(final Object content)
    {
        if (this.test1 == null)
        {
            this.test1 = TestDO.class.cast(content);
        }
        else if (this.test2 == null)
        {
            this.test2 = TestDO.class.cast(content);
        }
    }
}
