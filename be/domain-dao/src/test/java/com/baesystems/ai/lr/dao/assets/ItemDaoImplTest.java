package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class ItemDaoImplTest
{
    private static final Long TOP_LEVEL_ITEM_ID = 1L;
    private static final Long SECOND_LEVEL_ITEM_ID_1 = 21L;
    private static final Long SECOND_LEVEL_ITEM_ID_2 = 22L;
    private static final Long THIRD_LEVEL_ITEM_ID_1 = 31L;
    private static final Long THIRD_LEVEL_ITEM_ID_2 = 32L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long VERSION_1 = 1L;

    @Mock
    private VersionedItemRepository mockItemRepo;

    @Captor
    private ArgumentCaptor<Set<Long>> processedIdsCaptor;

    @InjectMocks
    private final ItemPersistDaoImpl dao = new ItemPersistDaoImpl();

    @Test
    public void doNoHarmIfNoItemCanBeFound()
    {
        dao.setItemAndAllDescendantsToSuccessfullyReviewed(TOP_LEVEL_ITEM_ID);
    }

    /*@Test
    public void updateObjectHierarchyToReviewed()
    {

        final VersionedItemPK versionedItemPK = new VersionedItemPK(ASSET_1_ID, SECOND_LEVEL_ITEM_ID_2, VERSION_1);
        final VersionedItemDO itemDO = createDOHierarchy(false);

        final VersionedItemDO itemToUpdate = itemDO.getItems().get(1);

        when(this.mockItemRepo.findOne(versionedItemPK)).thenReturn(itemToUpdate);

        dao.setItemAndAllDescendantsToSuccessfullyReviewed(SECOND_LEVEL_ITEM_ID_2);

        // Make sure only the item and its descendants are updated, and nothing more.
        verify(this.mockItemRepo).findOne(versionedItemPK);
        verify(this.mockItemRepo).updateReviewedStateOfItems(eq(true), processedIdsCaptor.capture());
        Mockito.verifyNoMoreInteractions(this.mockItemRepo);

        final Set<Long> processedIds = processedIdsCaptor.getValue();

        // Ensure the right items are updated to be reviewed.
        assertTrue(processedIds.contains(SECOND_LEVEL_ITEM_ID_2));
        assertTrue(processedIds.contains(THIRD_LEVEL_ITEM_ID_1));
        assertTrue(processedIds.contains(THIRD_LEVEL_ITEM_ID_2));

        // Ensure the rest of the hierarchy is unaffected.
        assertFalse(processedIds.contains(TOP_LEVEL_ITEM_ID));
        assertFalse(processedIds.contains(SECOND_LEVEL_ITEM_ID_1));
    }

    @Test
    public void updateObjectHierarchyToNotReviewed()
    {
        final VersionedItemDO itemDO = createDOHierarchy(true);
        final VersionedItemDO itemToUpdate = itemDO.getItems().get(1);
        final VersionedItemPK versionedItemPK = new VersionedItemPK(ASSET_1_ID, SECOND_LEVEL_ITEM_ID_2, VERSION_1);

        when(this.mockItemRepo.findOne(versionedItemPK)).thenReturn(itemToUpdate);

        dao.setItemAndAllAncestorsToRequireAReview(SECOND_LEVEL_ITEM_ID_2);

        // Make sure only the item and its parent are updated, and nothing more.
        verify(this.mockItemRepo).findOne(versionedItemPK);
        verify(this.mockItemRepo).updateReviewedStateOfItems(eq(false), processedIdsCaptor.capture());
        Mockito.verifyNoMoreInteractions(this.mockItemRepo);

        final Set<Long> processedIds = processedIdsCaptor.getValue();

        // Ensure the item and its parent have had their review status set to false.
        assertTrue(processedIds.contains(TOP_LEVEL_ITEM_ID));
        assertTrue(processedIds.contains(SECOND_LEVEL_ITEM_ID_2));

        // and the rest is unaffected
        assertFalse(processedIds.contains(SECOND_LEVEL_ITEM_ID_1));
        assertFalse(processedIds.contains(THIRD_LEVEL_ITEM_ID_1));
        assertFalse(processedIds.contains(THIRD_LEVEL_ITEM_ID_2));
    }

    @Test
    public void checkHierarchyWithALoopDoesNotFail()
    {
        final VersionedItemDO itemDO = createBadDOHierarchy(true);
        final VersionedItemDO itemToUpdate = itemDO.getItems().get(1);
        final VersionedItemPK versionedItemPK = new VersionedItemPK(ASSET_1_ID, SECOND_LEVEL_ITEM_ID_2, VERSION_1);



        when(this.mockItemRepo.findOne(versionedItemPK)).thenReturn(itemToUpdate);

        dao.setItemAndAllAncestorsToRequireAReview(SECOND_LEVEL_ITEM_ID_2);

        // Make sure only the item and its parent are updated, and nothing more.
        verify(this.mockItemRepo).findOne(versionedItemPK);
        verify(this.mockItemRepo).updateReviewedStateOfItems(eq(false), processedIdsCaptor.capture());
        Mockito.verifyNoMoreInteractions(this.mockItemRepo);

        final Set<Long> processedIds = processedIdsCaptor.getValue();

        // Ensure the item and its parent have had their review status set to false.
        assertTrue(processedIds.contains(TOP_LEVEL_ITEM_ID));
        assertTrue(processedIds.contains(SECOND_LEVEL_ITEM_ID_2));

        // and the rest is unaffected
        assertFalse(processedIds.contains(SECOND_LEVEL_ITEM_ID_1));
        assertFalse(processedIds.contains(THIRD_LEVEL_ITEM_ID_1));
        assertFalse(processedIds.contains(THIRD_LEVEL_ITEM_ID_2));

    }

//    *//**
//     * Creates an invalid DO hierarchy where one of the parents is a child of one of their descendants.
//     *
//     * @param reviewed
//     * @return
//     */
//    private VersionedItemDO createBadDOHierarchy(final boolean reviewed)
//    {
//        final VersionedItemDO topLevelItem = createTestItem(TOP_LEVEL_ITEM_ID, reviewed);
//
//        final VersionedItemDO childItemOne = createTestItem(SECOND_LEVEL_ITEM_ID_1, reviewed);
//        childItemOne.setParentItem(topLevelItem);
//        final VersionedItemDO childItemTwo = createTestItem(SECOND_LEVEL_ITEM_ID_2, reviewed);
//        childItemTwo.setParentItem(topLevelItem);
//
//        final VersionedItemDO thirdLevelchildItemOne = createTestItem(THIRD_LEVEL_ITEM_ID_1, reviewed);
//        thirdLevelchildItemOne.setParentItem(childItemTwo);
//        final VersionedItemDO thirdLevelchildItemTwo = createTestItem(THIRD_LEVEL_ITEM_ID_2, reviewed);
//        thirdLevelchildItemTwo.setParentItem(childItemTwo);
//
////        topLevelItem.setItems(Arrays.asList(childItemOne, childItemTwo));
////        childItemTwo.setItems(Arrays.asList(thirdLevelchildItemOne, thirdLevelchildItemTwo));
//
//        return topLevelItem;
//
//    }

    /**
     * Creates a DO hierarchy with one top level item, two children, where the second child has two children of their
     * own.
     * 
     * @param reviewed
     * @return
     */
//    private VersionedItemDO createDOHierarchy(final boolean reviewed)
//    {
//        final VersionedItemDO topLevelItem = createTestItem(TOP_LEVEL_ITEM_ID, reviewed);
//
//        final VersionedItemDO childItemOne = createTestItem(SECOND_LEVEL_ITEM_ID_1, reviewed);
//        childItemOne.setParentItem(topLevelItem);
//        final VersionedItemDO childItemTwo = createTestItem(SECOND_LEVEL_ITEM_ID_2, reviewed);
//        childItemTwo.setParentItem(topLevelItem);
//
//        final VersionedItemDO thirdLevelchildItemOne = createTestItem(THIRD_LEVEL_ITEM_ID_1, reviewed);
//        thirdLevelchildItemOne.setParentItem(childItemTwo);
//        final VersionedItemDO thirdLevelchildItemTwo = createTestItem(THIRD_LEVEL_ITEM_ID_2, reviewed);
//        thirdLevelchildItemTwo.setParentItem(childItemTwo);
////
////        topLevelItem.setItems(Arrays.asList(childItemOne, childItemTwo));
////        childItemTwo.setItems(Arrays.asList(thirdLevelchildItemOne, thirdLevelchildItemTwo));
//
//        return topLevelItem;
//
//    }

    private VersionedItemDO createTestItem(final Long id, final boolean reviewed)
    {
        final VersionedItemDO item = new VersionedItemDO();
        item.setReviewed(reviewed);
        item.setId(id);
        return item;
    }

}
