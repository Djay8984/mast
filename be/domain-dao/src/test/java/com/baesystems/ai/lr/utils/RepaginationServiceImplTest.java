package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import com.baesystems.ai.lr.domain.entities.MultiTestDO;
import com.baesystems.ai.lr.domain.entities.TestDO;
import com.baesystems.ai.lr.exception.BadPageRequestException;

@SuppressWarnings("PMD.TooManyMethods")
public class RepaginationServiceImplTest
{
    private static final Direction ASC = Direction.fromString("ASC");
    private static final Direction DESC = Direction.fromString("DESC");

    private static final String ID_STRING = "id";
    private static final String NAME_STRING = "name";

    private static final Long ZERO = 0L;
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final Long FOUR = 4L;
    private static final Long FIVE = 5L;

    private RepaginationServiceImpl<MultiTestDO> repaginationService;

    private MultiTestDO test1;
    private MultiTestDO test2;
    private MultiTestDO test3;
    private MultiTestDO test4;
    private MultiTestDO test5;
    private List<MultiTestDO> list1;
    private List<MultiTestDO> list2;
    private List<MultiTestDO> resultingList;
    private Iterator<MultiTestDO> resultingListTester;
    private Pageable pageInfo;
    private long enteredTotal;
    private String list1PropertyName;
    private String list2PropertyName;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup()
    {
        this.test1 = new MultiTestDO(new TestDO(ONE, "A"), null);
        this.test2 = new MultiTestDO(null, new TestDO(TWO, "B"));
        this.test3 = new MultiTestDO(new TestDO(THREE, "C"), null);
        this.test4 = new MultiTestDO(null, new TestDO(FOUR, "D"));
        this.test5 = new MultiTestDO(new TestDO(FIVE, "E"), null);

        this.resultingList = new ArrayList<MultiTestDO>();

        this.repaginationService = new RepaginationServiceImpl<MultiTestDO>();

        this.list1 = new ArrayList<MultiTestDO>();
        this.list2 = new ArrayList<MultiTestDO>();

        this.list1.add(this.test1);
        this.list1.add(this.test3);
        this.list1.add(this.test5);

        this.list2.add(this.test2);
        this.list2.add(this.test4);

        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue(), ASC, ID_STRING);

        this.enteredTotal = FIVE;
        this.list1PropertyName = "test1";
        this.list2PropertyName = "test2";

        this.resultingList.add(this.test1);
        this.resultingList.add(this.test2);
        this.resultingList.add(this.test3);
        this.resultingList.add(this.test4);
        this.resultingList.add(this.test5);

        this.resultingListTester = this.resultingList.iterator();
    }

    @Test
    public void repaginateSimpleAscTest() throws BadPageRequestException
    {
        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FIVE.intValue(), FIVE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateSimpleDescTest() throws BadPageRequestException
    {
        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue(), DESC, ID_STRING);

        Collections.reverse(this.list1);
        Collections.reverse(this.list2);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        final List<MultiTestDO> resultReversed = new ArrayList<MultiTestDO>(result.getContent());
        Collections.reverse(resultReversed);
        resultReversed.stream().forEachOrdered(element -> assertEquals(this.resultingListTester.next().getId(), element.getId()));
    }

    @Test
    public void repaginateSimpleFullListAtStartTest() throws BadPageRequestException
    {
        this.rearrangeLists();

        this.list1.add(this.test4);
        this.list1.add(this.test5);

        this.list2.add(this.test1);
        this.list2.add(this.test2);
        this.list2.add(this.test3);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FIVE.intValue(), FIVE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateSimpleFullListAtEndTest() throws BadPageRequestException
    {
        this.rearrangeLists();

        this.list1.add(this.test1);
        this.list1.add(this.test2);

        this.list2.add(this.test3);
        this.list2.add(this.test4);
        this.list2.add(this.test5);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FIVE.intValue(), FIVE.intValue(), ONE.intValue());

    }

    @Test
    public void repaginateTwoOrdersTest() throws BadPageRequestException
    {
        this.list2.get(1).getTest2().setName("E");

        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue(), ASC, NAME_STRING, ID_STRING);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FIVE.intValue(), FIVE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginatePairsFoundTest() throws BadPageRequestException
    {
        this.list2.get(1).getTest2().setId(ONE);
        this.list2.get(1).setId(ONE);

        this.resultingList.remove(THREE.intValue());
        this.resultingListTester = this.resultingList.iterator();

        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue(), ASC, NAME_STRING);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FOUR.intValue(), FOUR.intValue(), ONE.intValue());
    }

    @Test
    public void repaginatePageOutOfRangeTest() throws BadPageRequestException
    {
        this.pageInfo = new PageRequest(TWO.intValue(), FIVE.intValue(), ASC, NAME_STRING, ID_STRING);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, TWO.intValue(), ZERO.intValue(), FIVE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginatePairsFoundPageOutOfRangeTest() throws BadPageRequestException
    {
        this.list2.get(1).getTest2().setId(ONE);
        this.list2.get(1).setId(ONE);

        this.resultingList.remove(THREE.intValue());
        this.resultingListTester = this.resultingList.iterator();

        this.pageInfo = new PageRequest(TWO.intValue(), FIVE.intValue(), ASC, NAME_STRING);

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, TWO.intValue(), ZERO.intValue(), FOUR.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateNoSorthingTest() throws BadPageRequestException
    {
        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue());

        this.resultingList.clear();
        this.resultingList.addAll(this.list2);
        this.resultingList.addAll(this.list1);
        this.resultingListTester = this.resultingList.iterator();

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), FIVE.intValue(), FIVE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateEmplyList1Test() throws BadPageRequestException
    {
        this.list1 = new ArrayList<MultiTestDO>();

        this.resultingList.clear();
        this.resultingList.addAll(this.list2);
        this.resultingListTester = this.resultingList.iterator();

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), TWO.intValue(), TWO.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateEmplyList2Test() throws BadPageRequestException
    {
        this.list2 = new ArrayList<MultiTestDO>();

        this.resultingList.clear();
        this.resultingList.addAll(this.list1);
        this.resultingListTester = this.resultingList.iterator();

        final Page<MultiTestDO> result = this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName,
                this.list2, this.list2PropertyName);

        this.test(result, ZERO.intValue(), THREE.intValue(), THREE.intValue(), ONE.intValue());
    }

    @Test
    public void repaginateEmplyNullList1Test() throws BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage("Input lists must not be null.");

        this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, null, this.list1PropertyName, this.list2, this.list2PropertyName);
    }

    @Test
    public void repaginateEmplyNullList2Test() throws BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage("Input lists must not be null.");

        this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName, null, this.list2PropertyName);
    }

    @Test
    public void repaginateBadFieldTest() throws BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage("Fields badFieldName are not of a compareable type.");

        this.pageInfo = new PageRequest(ZERO.intValue(), FIVE.intValue(), ASC, "badFieldName");

        this.repaginationService.repaginate(this.pageInfo, this.enteredTotal, this.list1, this.list1PropertyName, this.list2, this.list2PropertyName);
    }

    private void rearrangeLists()
    {
        this.test1 = new MultiTestDO(new TestDO(ONE, "A"), null);
        this.test2 = new MultiTestDO(new TestDO(TWO, "B"), null);
        this.test3 = new MultiTestDO(new TestDO(THREE, "C"), null);
        this.test4 = new MultiTestDO(new TestDO(FOUR, "D"), null);
        this.test5 = new MultiTestDO(new TestDO(FIVE, "E"), null);

        this.list1PropertyName = "test1";
        this.list2PropertyName = "test1";

        this.list1.clear();
        this.list2.clear();
    }

    private void test(final Page<MultiTestDO> result, final int pageNumber, final int numberOfElements, final int total,
            final int totalPages)
    {
        assertNotNull(result);
        this.testPagination(result, pageNumber, numberOfElements, total, totalPages);
        this.testContent(result);
    }

    private void testPagination(final Page<MultiTestDO> result, final int pageNumber, final int numberOfElements, final int total,
            final int totalPages)
    {
        assertEquals(pageNumber, result.getNumber());
        assertEquals(numberOfElements, result.getNumberOfElements());
        assertEquals(total, result.getTotalElements());
        assertEquals(totalPages, result.getTotalPages());
        assertEquals(numberOfElements, result.getContent().size());
    }

    private void testContent(final Page<MultiTestDO> result)
    {
        assertNotNull(result.getContent());
        result.getContent().stream().forEachOrdered(element -> assertEquals(this.resultingListTester.next().getId(), element.getId()));
    }
}
