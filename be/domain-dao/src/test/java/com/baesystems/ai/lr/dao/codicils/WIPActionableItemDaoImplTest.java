package com.baesystems.ai.lr.dao.codicils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.ActionableItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SequenceRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPActionableItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPSequenceRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.utils.SequenceDao;

@RunWith(MockitoJUnitRunner.class)
public class WIPActionableItemDaoImplTest
{
    @Mock
    private WIPActionableItemRepository wipActionableItemRepository;

    @Mock
    private ActionableItemRepository actionableItemRepository;

    @Mock
    private SequenceDao sequenceDao;

    @InjectMocks
    private final WIPActionableItemDaoImpl dao = new WIPActionableItemDaoImpl();

    @Captor
    private ArgumentCaptor<WIPActionableItemDO> doCaptor;

    @Test
    public void createNewWIPActionableItem()
    {
        final ActionableItemDto passedActionableItemDto = new ActionableItemDto();
        passedActionableItemDto.setJob(new LinkResource(1L));

        final WIPActionableItemDO doFromTheDatabase = new WIPActionableItemDO();
        final long newItemId = 50L;
        doFromTheDatabase.setId(newItemId);



        when(this.sequenceDao
                .getNextAssetSpecificSequenceValue(anyLong(), anyBoolean(), any(SequenceRepository.class), any(WIPSequenceRepository.class)))
                .thenReturn(1);

        when(this.wipActionableItemRepository.save(doCaptor.capture())).thenReturn(doFromTheDatabase);

        final ActionableItemDto createdActionableItem = dao.createActionableItem(passedActionableItemDto);

        final WIPActionableItemDO doBeingSaved = doCaptor.getValue();
        assertNull(doBeingSaved.getId());

        assertEquals(newItemId, createdActionableItem.getId().longValue());
    }

    @Test
    public void updateWIPActionableItem()
    {
        final ActionableItemDto passedActionableItemDto = new ActionableItemDto();
        final Long itemId = 50L;
        passedActionableItemDto.setId(itemId);

        final WIPActionableItemDO doFromTheDatabase = new WIPActionableItemDO();
        doFromTheDatabase.setId(itemId);

        when(this.wipActionableItemRepository.merge(doCaptor.capture())).thenReturn(doFromTheDatabase);

        dao.updateActionableItem(passedActionableItemDto);

        final WIPActionableItemDO doBeingSaved = doCaptor.getValue();
        assertEquals(itemId.longValue(), doBeingSaved.getId().longValue());
    }

    @Test
    public void actionableItemExistsForJob()
    {
        final Long jobId = 20L;
        final Long itemId = 50L;
        when(this.wipActionableItemRepository.actionableItemExistsForJob(jobId, itemId)).thenReturn(1L);

        assertTrue(dao.actionableItemExistsForJob(jobId, itemId));
    }
}
