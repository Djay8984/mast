package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;

@RunWith(MockitoJUnitRunner.class)
public class CodicilStatusCriteriaTest
{

    @Test
    public void nullListOfStatuses()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(null, CodicilType.AI);

        assertNull(criteria.getOpen());
        assertNull(criteria.getStatusList());
    }

    @Test
    public void passingAIInactiveAndOpenRemovesInactiveFromQueryStatuses()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AI_CANCELLED.getValue(),
                        CodicilStatusType.AI_INACTIVE.getValue(),
                        CodicilStatusType.AI_OPEN.getValue()),
                CodicilType.AI);

        assertNull(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AI_CANCELLED.getValue(),
                           CodicilStatusType.AI_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }

    @Test
    public void passingAIOpenWithoutInactiveSetsStateQueryAsOpen()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AI_CANCELLED.getValue(),
                        CodicilStatusType.AI_OPEN.getValue()),
                CodicilType.AI);

        Assert.assertTrue(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AI_CANCELLED.getValue(),
                           CodicilStatusType.AI_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }

    @Test
    public void passingAIInactiveWithoutOpenRemovesInactiveAddsOpenAndSetsOpenToFalse()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AI_CANCELLED.getValue(),
                        CodicilStatusType.AI_INACTIVE.getValue()),
                CodicilType.AI);

        Assert.assertFalse(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AI_CANCELLED.getValue(),
                           CodicilStatusType.AI_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }

    @Test
    public void passingANInactiveAndOpenRemovesInactiveFromQueryStatuses()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AN_CANCELLED.getValue(),
                        CodicilStatusType.AN_INACTIVE.getValue(),
                        CodicilStatusType.AN_OPEN.getValue()),
                CodicilType.AN);

        assertNull(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AN_CANCELLED.getValue(),
                           CodicilStatusType.AN_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }

    @Test
    public void passingANOpenWithoutInactiveSetsStateQueryAsOpen()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AN_CANCELLED.getValue(),
                        CodicilStatusType.AN_OPEN.getValue()),
                CodicilType.AN);

        Assert.assertTrue(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AN_CANCELLED.getValue(),
                           CodicilStatusType.AN_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }

    @Test
    public void passingANInactiveWithoutOpenRemovesInactiveAddsOpenAndSetsOpenToFalse()
    {
        final CodicilStatusCriteria criteria = new CodicilStatusCriteria(
                Arrays.asList(
                        CodicilStatusType.AN_CANCELLED.getValue(),
                        CodicilStatusType.AN_INACTIVE.getValue()),
                CodicilType.AN);

        Assert.assertFalse(criteria.getOpen());
        Assert.assertArrayEquals(
                new Long[]{
                           CodicilStatusType.AN_CANCELLED.getValue(),
                           CodicilStatusType.AN_OPEN.getValue()},
                criteria.getStatusList().toArray());
    }
}
