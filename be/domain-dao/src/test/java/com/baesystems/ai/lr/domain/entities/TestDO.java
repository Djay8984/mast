package com.baesystems.ai.lr.domain.entities;

import java.util.Date;

public class TestDO
{
    private Long id;

    private String name;

    private Date date;

    private Double aDouble;

    private Float aFloat;

    private Integer anInt;

    private Object object;

    public TestDO()
    {
        super();
    }

    public TestDO(final Long id, final String name)
    {
        super();
        this.id = id;
        this.name = name;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Date getDate()
    {
        return this.date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    public Double getaDouble()
    {
        return this.aDouble;
    }

    public void setaDouble(final Double aDouble)
    {
        this.aDouble = aDouble;
    }

    public Float getaFloat()
    {
        return this.aFloat;
    }

    public void setaFloat(final Float aFloat)
    {
        this.aFloat = aFloat;
    }

    public Integer getAnInt()
    {
        return this.anInt;
    }

    public void setAnInt(final Integer anInt)
    {
        this.anInt = anInt;
    }

    public Object getObject()
    {
        return this.object;
    }

    public void setObject(final Object object)
    {
        this.object = object;
    }
}
