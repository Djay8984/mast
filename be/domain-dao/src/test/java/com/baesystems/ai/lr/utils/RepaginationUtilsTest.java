package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.baesystems.ai.lr.domain.entities.MultiTestDO;
import com.baesystems.ai.lr.domain.entities.TestDO;
import com.baesystems.ai.lr.exception.BadPageRequestException;

@SuppressWarnings("PMD.TooManyMethods")
public class RepaginationUtilsTest
{
    private static final Double ZERO_POINT_ONE = 0.1D;
    private static final Double ZERO_POINT_TWO = 0.2D;
    private static final String NAME = "name";

    private RepaginationUtils<MultiTestDO> repaginationUtils;
    private MultiTestDO test1;
    private MultiTestDO test2;
    private MultiTestDO test3;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup()
    {
        this.repaginationUtils = new RepaginationUtils<MultiTestDO>();
        this.repaginationUtils.setClassLevelVariables("test1", "test1");
        this.test1 = new MultiTestDO(new TestDO(), null);
        this.test2 = new MultiTestDO(new TestDO(), null);
        this.test3 = new MultiTestDO(new TestDO(), null);
    }

    @Test
    public void testGetRelationshipDates()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException, ParseException
    {
        this.test1.getTest1().setDate(DateHelper.mastFormatter().parse("2016-10-27"));
        this.test2.getTest1().setDate(DateHelper.mastFormatter().parse("2016-10-28"));

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship("date", null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship("date", null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship("date", null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipIntegers() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setAnInt(1);
        this.test2.getTest1().setAnInt(2);

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship("anInt", null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship("anInt", null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship("anInt", null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipLongs() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setId(1L);
        this.test2.getTest1().setId(2L);

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship("id", null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship("id", null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship("id", null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipFloats() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setaFloat(ZERO_POINT_ONE.floatValue());
        this.test2.getTest1().setaFloat(ZERO_POINT_TWO.floatValue());

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship("aFloat", null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship("aFloat", null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship("aFloat", null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipDoubles() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setaDouble(ZERO_POINT_ONE);
        this.test2.getTest1().setaDouble(ZERO_POINT_TWO);

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship("aDouble", null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship("aDouble", null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship("aDouble", null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipStrings() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setName("NameA");
        this.test2.getTest1().setName("NameB");

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship(NAME, null, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship(NAME, null, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship(NAME, null, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipCaseInsensitive()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.test1.getTest1().setName("NameA");
        this.test2.getTest1().setName("nameB");
        this.test3.getTest1().setName("nAMEa");

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship(NAME, true, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship(NAME, true, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship(NAME, true, this.test1, this.test3));
    }

    @Test
    public void testGetRelationshipCaseSensitive()
            throws ParseException, IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            BadPageRequestException
    {
        this.test1.getTest1().setName("NameA");
        this.test2.getTest1().setName("nAMEa");

        assertEquals(RepaginationUtils.Relationship.LT, this.repaginationUtils.getRelationship(NAME, false, this.test1, this.test2));
        assertEquals(RepaginationUtils.Relationship.GT, this.repaginationUtils.getRelationship(NAME, false, this.test2, this.test1));
        assertEquals(RepaginationUtils.Relationship.EQ, this.repaginationUtils.getRelationship(NAME, false, this.test1, this.test1));
    }

    @Test
    public void testGetRelationshipObjects() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage(String.format("Fields object are not of a compareable type."));

        this.test1.getTest1().setObject(new Object());
        this.test2.getTest1().setObject(new Object());

        this.repaginationUtils.getRelationship("object", null, this.test1, this.test2);
    }

    @Test
    public void testGetRelationshipNulls1() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage(String.format("Fields object cannot be compared because at least one of the parent objects is null."));

        this.repaginationUtils.getRelationship("object", null, null, this.test2);
    }

    @Test
    public void testGetRelationshipNulls2() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        this.thrown.expect(BadPageRequestException.class);
        this.thrown.expectMessage(String.format("Fields object cannot be compared because at least one of the parent objects is null."));

        this.repaginationUtils.getRelationship("object", null, this.test1, null);
    }

    @Test
    public void testsetClassLevelVariables() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
    {
        this.repaginationUtils.setClassLevelVariables("f1", "f2");

        final Field parentField1 = RepaginationUtils.class.getDeclaredField("parentField1");
        final Field parentField2 = RepaginationUtils.class.getDeclaredField("parentField2");

        parentField1.setAccessible(true);
        parentField2.setAccessible(true);

        assertEquals("f1.", parentField1.get(this.repaginationUtils));
        assertEquals("f2.", parentField2.get(this.repaginationUtils));
    }

    @Test
    public void testsetClassLevelVariablesNulls() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
    {
        this.repaginationUtils.setClassLevelVariables(null, null);

        final Field parentField1 = RepaginationUtils.class.getDeclaredField("parentField1");
        final Field parentField2 = RepaginationUtils.class.getDeclaredField("parentField2");

        parentField1.setAccessible(true);
        parentField2.setAccessible(true);

        assertEquals("", parentField1.get(this.repaginationUtils));
        assertEquals("", parentField2.get(this.repaginationUtils));
    }

    @Test
    public void testsetClassLevelVariablesEmptyString()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
    {
        this.repaginationUtils.setClassLevelVariables("", "");

        final Field parentField1 = RepaginationUtils.class.getDeclaredField("parentField1");
        final Field parentField2 = RepaginationUtils.class.getDeclaredField("parentField2");

        parentField1.setAccessible(true);
        parentField2.setAccessible(true);

        assertEquals("", parentField1.get(this.repaginationUtils));
        assertEquals("", parentField2.get(this.repaginationUtils));
    }
}
