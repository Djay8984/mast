package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRelationshipRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemPageResourceDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbstractItemDaoImplTest
{
    private static final Long ZERO = 0L;
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final String NAME = "NAME";

    private Pageable pageSpecification;

    @Mock
    private AttributeTypeRepository attributeTypeRepository;

    @Mock
    private ItemTypeRepository itemTypeRepository;

    @Mock
    private VersionedItemRepository versionedItemRepository;

    @Mock
    private ItemTypeRelationshipRepository itemTypeRelationshipRepository;

    @Mock
    private PageResourceFactory pageFactory;

    @InjectMocks
    // needs to be something that can be instantiated, even though the test is for the methods in the abstract class.
    private final ItemDao abstractItemDao = new ItemDaoImpl();

    @Before
    public void setUp() throws BadPageRequestException
    {
        pageSpecification = new PageRequest(ZERO.intValue(), ONE.intValue());
    }

    @Test
    public void isProposedRelationshipValidForTypes()
    {
        when(this.itemTypeRelationshipRepository.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), ONE)).thenReturn(true);
        assertTrue(abstractItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), ONE));
    }

//    @Test
//    public void getItemChildIdList()
//    {
////        when(this.versionedItemRepository.findChildIds(Collections.singletonList(ONE), RelationshipType.IS_PART_OF.getValue()))
////                .thenReturn(Arrays.asList(TWO, THREE));
//
//        final List<Long> returnedList = this.abstractItemDao.getItemChildIdList(ONE);
//
//        assertEquals(THREE.intValue(), returnedList.size());
//        assertTrue(returnedList.containsAll(Arrays.asList(ONE, TWO, THREE)));
//    }

    @Test
    public void findItemsByAssetNoQuery() throws InstantiationException, IllegalAccessException
    {
        final List<VersionedItemDO> list = new ArrayList<>();
        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, ZERO, ZERO)).thenReturn(list);
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, null));

        verify(this.attributeTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository, never())
                .findItemsByAssetId(any(Long.class), any(Long.class), anyListOf(Long.class), anyListOf(Long.class), anyListOf(Long.class),
                        anyString());
    }

    @Test
    public void findItemsByAssetWithQuerySimple() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, Collections.emptyList(), Collections.emptyList(), null))
                .thenReturn(new ArrayList<>());
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));

        verify(this.attributeTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository)
                .findItemsByAssetId(any(Long.class), any(Long.class), anyListOf(Long.class), anyListOf(Long.class), anyListOf(Long.class),
                        anyString());
    }

    @Test
    public void findItemsByAssetWithQueryAttributeTypeName() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        query.setAttributeName(NAME);

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, Collections.emptyList(), Collections.emptyList(), null))
                .thenReturn(new ArrayList<>());
        when(this.attributeTypeRepository.findIdsWithNameLike(NAME)).thenReturn(Arrays.asList(TWO, THREE));
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));
        assertNotNull(query.getAttributeTypeId());
        assertEquals(TWO.intValue(), query.getAttributeTypeId().size());
        assertTrue(query.getAttributeTypeId().containsAll(Arrays.asList(TWO, THREE)));

        verify(this.attributeTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository)
                .findItemsByAssetId(any(Long.class), any(Long.class), anyListOf(Long.class), anyListOf(Long.class), anyListOf(Long.class),
                        anyString());
    }

    @Test
    public void findItemsByAssetWithQueryAttributeTypeNameMergeList() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        query.setAttributeName(NAME);
        query.setAttributeTypeId(new ArrayList<>(Arrays.asList(new Long[]{ONE, TWO})));

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, Collections.emptyList(), Collections.emptyList(), null))
                .thenReturn(new ArrayList<>());
        when(this.attributeTypeRepository.findIdsWithNameLike(NAME)).thenReturn(new ArrayList<>(Arrays.asList(new Long[]{TWO, THREE})));
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));
        assertNotNull(query.getAttributeTypeId());
        assertEquals(ONE.intValue(), query.getAttributeTypeId().size());
        assertTrue(query.getAttributeTypeId().containsAll(Arrays.asList(TWO)));

        verify(this.attributeTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository)
                .findItemsByAssetId(any(Long.class), any(Long.class), anyListOf(Long.class), anyListOf(Long.class), anyListOf(Long.class),
                        anyString());
    }

    @Test
    public void findItemsByAssetWithQueryAttributeTypeNameEmptyList() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        query.setAttributeName(NAME);
        query.setAttributeTypeId(new ArrayList<>(Arrays.asList(new Long[]{ONE})));

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, Collections.emptyList(), Collections.emptyList(), null))
                .thenReturn(new ArrayList<>());
        when(this.attributeTypeRepository.findIdsWithNameLike(NAME)).thenReturn(new ArrayList<>(Arrays.asList(new Long[]{TWO, THREE})));
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));
        assertNotNull(query.getAttributeTypeId());
        assertTrue(query.getAttributeTypeId().isEmpty());

        verify(this.attributeTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository, never())
                .findItemsByAssetId(anyLong(), anyLong(), anyListOf(Long.class), anyListOf(Long.class), anyListOf(Long.class), anyString());
    }

    @Test
    public void findItemsByAssetWithQueryItemTypeName() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        final List<Long> itemTypeIdsMatchingName = Arrays.asList(TWO, THREE);

        query.setItemTypeName(NAME);

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, query.getItemId(), query.getItemTypeId(), query.getItemName()))
                .thenReturn(new ArrayList<>());
        when(this.itemTypeRepository.findIdsWithNameLike(NAME)).thenReturn(itemTypeIdsMatchingName);
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));

        verify(this.attributeTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository)
                .findItemsByAssetId(anyLong(), anyLong(), eq(itemTypeIdsMatchingName), anyListOf(Long.class), anyListOf(Long.class), anyString());
    }

    @Test
    public void findItemsByAssetWithQueryItemTypeNameEmptyList() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        final List<Long> itemTypeIdsMatchingName = new ArrayList<>();

        query.setItemTypeName(NAME);

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, query.getItemId(), query.getItemTypeId(), query.getItemName()))
                .thenReturn(new ArrayList<>());
        when(this.itemTypeRepository.findIdsWithNameLike(NAME)).thenReturn(itemTypeIdsMatchingName);
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));

        verify(this.attributeTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository, never())
                .findItemsByAssetId(anyLong(), anyLong(), eq(itemTypeIdsMatchingName), anyListOf(Long.class), anyListOf(Long.class), anyString());
    }

    @Test
    public void findItemsByAssetWithQueryItemTypeNameEmptyListUseOrOnItemName() throws InstantiationException, IllegalAccessException
    {
        final ItemQueryDto query = new ItemQueryDto();
        final List<Long> itemTypeIdsMatchingName = new ArrayList<>();

        query.setItemName(NAME);
        query.setItemTypeName(NAME);

        when(this.versionedItemRepository.findItemsByAssetId(ONE, ONE, null, query.getItemId(), query.getItemTypeId(), query.getItemName()))
                .thenReturn(new ArrayList<>());
        when(this.itemTypeRepository.findIdsWithNameLike(NAME)).thenReturn(itemTypeIdsMatchingName);
        when(this.pageFactory.createPageResource(any((Class<Page<LazyItemDto>>) (Class<?>) Page.class), eq(LazyItemPageResourceDto.class)))
                .thenReturn(LazyItemPageResourceDto.class.newInstance());

        assertNotNull(this.abstractItemDao.findItemsByAsset(ONE, ONE, false, pageSpecification, query));

        verify(this.attributeTypeRepository, never()).findIdsWithNameLike(any(String.class));
        verify(this.itemTypeRepository).findIdsWithNameLike(any(String.class));
        verify(this.versionedItemRepository)
                .findItemsByAssetId(anyLong(), anyLong(), eq(null), anyListOf(Long.class), anyListOf(Long.class), anyString());
    }
}
