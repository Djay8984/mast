package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.LockingDao;

/*
 * Marker interface to avoid the need to qualify when auto-wiring
 */
public interface WIPCoCDao extends BaseCoCDao, WIPCodicilDefectQueryDao<CoCDto>, LockingDao<WIPCoCDO>
{
    CoCDto saveCoC(Long jobId, CoCDto coCDto);

    PageResource<CoCDto> getCoCsForJob(Pageable pageable, Long jobId, Long defectId);

    List<CoCDto> getCoCsForJob(Long jobId);

    List<CoCDto> getCoCsForJob(Long jobId, String employeeName, Date lastUpdatedDate);

    Boolean coCExistsForJob(Long jobId, Long cocId);

    void deleteCoC(Long coCId);

    Boolean exists(Long coCId);

    List<WIPCoCDO> getCoCsForDefect(Long defectId);

    List<CoCDto> getCoCsForJob(Long jobId, List<Long> statusList);

    Integer updateScopeConfirmed(Long jobId, Boolean scopeConfirmed, List<Long> statusList);

    Boolean defectHasCoCInStatus(Long defectId, List<Long> statusIds);

    List<CoCDto> getCoCsForReportContent(Long jobId);

    Boolean closeCoc(Long cocId);
}
