package com.baesystems.ai.lr.dao.references.cases;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.CaseStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CaseTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeRoleDO;
import com.baesystems.ai.lr.domain.mast.entities.references.PreEicInspectionStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RiskAssessmentStatusDO;
import com.baesystems.ai.lr.domain.mast.repositories.CaseStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CaseStatusTransitionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CaseTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.MilestoneDueDateReferenceRepository;
import com.baesystems.ai.lr.domain.mast.repositories.MilestoneRepository;
import com.baesystems.ai.lr.domain.mast.repositories.MilestoneStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.OfficeRoleRepository;
import com.baesystems.ai.lr.domain.mast.repositories.PreEicInspectionStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.RiskAssessmentStatusRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CaseReferenceDataDao")
public class CaseReferenceDataDaoImpl implements CaseReferenceDataDao
{

    private static final Logger LOG = LoggerFactory.getLogger(CaseReferenceDataDaoImpl.class);

    @Autowired
    private OfficeRoleRepository officeRoleRepository;

    @Autowired
    private PreEicInspectionStatusRepository preEicInspectionStatusRepository;

    @Autowired
    private RiskAssessmentStatusRepository riskAssessmentStatusRepository;

    @Autowired
    private CaseTypeRepository caseTypeRepository;

    @Autowired
    private MilestoneStatusRepository milestoneStatusRepository;

    @Autowired
    private CaseStatusRepository caseStatusRepository;

    @Autowired
    private MilestoneRepository milestoneRepository;

    @Autowired
    private CaseStatusTransitionRepository caseStatusTransitionRepository;

    @Autowired
    private MilestoneDueDateReferenceRepository milestoneDueDateReferenceRepository;

    private final MapperFacade mapper;

    public CaseReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(OfficeRoleDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(PreEicInspectionStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(RiskAssessmentStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CaseTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CaseStatusDO.class, CaseStatusDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(MilestoneDO.class, MilestoneDto.class)
                .customize(new MilestoneRelationshipMapper())
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public ReferenceDataDto getOfficeRole(final Long id)
    {
        final OfficeRoleDO output = this.officeRoleRepository.findOne(id);
        return output == null ? null : this.mapper.map(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getOfficeRoles()
    {
        final List<OfficeRoleDO> output = this.officeRoleRepository.findAll();
        return output == null ? null : this.mapper.mapAsList(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getPreEicInspectionStatuses()
    {
        LOG.debug("Retrieving all Pre EIC Inspection Statuses");

        final List<PreEicInspectionStatusDO> statuses = this.preEicInspectionStatusRepository.findAll();

        return this.mapper.mapAsList(statuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getPreEicInspectionStatus(final Long preEicInspectionStatusId)
    {
        LOG.debug("Retrieving Pre Eic Inspection Status with ID {}", preEicInspectionStatusId);

        final PreEicInspectionStatusDO statusData = this.preEicInspectionStatusRepository.findOne(preEicInspectionStatusId);

        return statusData == null ? null : this.mapper.map(statusData, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getRiskAssessmentStatuses()
    {
        LOG.debug("Retrieving all Risk Assessment Statuses");

        final List<RiskAssessmentStatusDO> statuses = this.riskAssessmentStatusRepository.findAll();

        return this.mapper.mapAsList(statuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getRiskAssessmentStatus(final Long riskAssessmentStatusId)
    {
        LOG.debug("Retrieving Risk Assessment Status with ID {}", riskAssessmentStatusId);

        final RiskAssessmentStatusDO statusData = this.riskAssessmentStatusRepository.findOne(riskAssessmentStatusId);

        return statusData == null ? null : this.mapper.map(statusData, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCaseTypes()
    {
        LOG.debug("Retrieving all Case Types");

        final List<CaseTypeDO> caseTypes = caseTypeRepository.findAll();

        return this.mapper.mapAsList(caseTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCaseType(final Long caseTypeId)
    {
        LOG.debug("Retrieving Case Type with ID {}", caseTypeId);

        final CaseTypeDO caseTypeData = caseTypeRepository.findOne(caseTypeId);

        return caseTypeData == null ? null : this.mapper.map(caseTypeData, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<CaseStatusDto> getCaseStatuses()
    {
        LOG.debug("Retrieving all Case Statuses");

        final List<CaseStatusDO> caseStatuses = caseStatusRepository.findAll();

        return this.mapper.mapAsList(caseStatuses, CaseStatusDto.class);
    }

    @Override
    @Transactional
    public CaseStatusDto getCaseStatus(final Long caseStatusId)
    {
        LOG.debug("Retrieving Case Status with ID {}", caseStatusId);

        final CaseStatusDO caseStatusData = caseStatusRepository.findOne(caseStatusId);

        return caseStatusData == null ? null : this.mapper.map(caseStatusData, CaseStatusDto.class);
    }

    @Override
    @Transactional
    public String getMilestoneStatusName(final Long milestoneId)
    {
        return this.milestoneStatusRepository.findName(milestoneId);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getMilestoneStatuses()
    {
        final List<MilestoneStatusDO> milestoneStatuses = this.milestoneStatusRepository.findAll();
        return milestoneStatuses == null ? null : this.mapper.mapAsList(milestoneStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<MilestoneDto> getMilestonesForCaseType(final Long caseTypeId)
    {
        final List<MilestoneDO> output = this.milestoneRepository.findMilestoneByCaseType(caseTypeId);
        return output == null ? null : this.mapper.mapAsList(output, MilestoneDto.class);
    }

    @Override
    @Transactional
    public MilestoneDto getMilestone(final Long milestoneId)
    {
        final MilestoneDO output = this.milestoneRepository.findOne(milestoneId);
        return output == null ? null : this.mapper.map(output, MilestoneDto.class);
    }

    @Override
    @Transactional
    public Long getDateDependentParentMilestoneId(final Long milestoneId)
    {
        return this.milestoneRepository.findDateDependentParentMilestone(milestoneId);
    }

    @Override
    @Transactional
    public List<Long> getParentMilestoneIds(final Long childMilestoneId)
    {
        return this.milestoneRepository.findParentMilestones(childMilestoneId);
    }

    @Override
    @Transactional
    public Integer getMilestonesWorkingDaysOffset(final Long milestoneId)
    {
        return this.milestoneRepository.findMilestoneWorkingDaysOffset(milestoneId);
    }

    @Override
    @Transactional
    public List<Long> getChildMilestoneIds(final Long milestoneId)
    {
        return this.milestoneRepository.findChildMilestones(milestoneId);
    }

    @Override
    @Transactional
    public Boolean isMilestoneMandatory(final Long milestoneId)
    {
        final Boolean returnValue = this.milestoneRepository.isMilestoneMandatory(milestoneId);

        return returnValue == null ? Boolean.FALSE : returnValue;
    }

    @Override
    @Transactional
    public Long getMilestoneDueDateReferenceId(final Long milestoneId)
    {
        return this.milestoneRepository.findMilestoneDueDateReferenceId(milestoneId);
    }

    @Override
    @Transactional
    public List<MilestoneDto> getMilestones()
    {
        LOG.debug("Retrieving all Milestones");

        final List<MilestoneDO> milestones = this.milestoneRepository.findAll();

        return this.mapper.mapAsList(milestones, MilestoneDto.class);
    }

    @Override
    @Transactional
    public List<Long> getValidNewCaseStatuses(final Long initialStatusId)
    {
        return this.caseStatusTransitionRepository.findValidNewCaseStatuses(initialStatusId);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getMilestoneDueDataReferences()
    {
        return this.mapper.mapAsList(this.milestoneDueDateReferenceRepository.findAll(), ReferenceDataDto.class);
    }

    /**
     * Custom mapper to get the correct ids from the milestone relationships for the predecessor and child milestone
     * lists
     */
    public static class MilestoneRelationshipMapper extends CustomMapper<MilestoneDO, MilestoneDto>
    {
        @Override
        public void mapAtoB(final MilestoneDO milestoneDO, final MilestoneDto milestoneDto, final MappingContext context)
        {
            if (milestoneDO.getPredecessorMilestones() != null)
            { // copy the predecessor ids
                milestoneDto.setPredecessorMilestones(new ArrayList<LinkResource>());
                for (final MilestoneRelationshipDO relationship : milestoneDO.getPredecessorMilestones())
                {
                    milestoneDto.getPredecessorMilestones().add(new LinkResource(relationship.getPredecessorMilestone().getId()));
                }
            }

            if (milestoneDO.getChildMilestones() != null)
            { // copy the child ids
                milestoneDto.setChildMilestones(new ArrayList<LinkResource>());
                for (final MilestoneRelationshipDO relationship : milestoneDO.getChildMilestones())
                {
                    milestoneDto.getChildMilestones().add(new LinkResource(relationship.getMilestone().getId()));
                }
            }
        }
    }
}
