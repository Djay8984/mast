package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPersistDO;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRepository;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.validation.DraftItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import ma.glasnost.orika.MapperFacade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class performs the item mapping from item to draft item where the child items are to be included selectively by
 * posting relationships.
 *
 * For each item in the tree:- If the relationship is null all children are copied. If the relationship is an empty list
 * no children are copied. If there is a list of child items (and their relationships) only the listed children are
 * copied. The method then descends to look at the relationships of the children and apply the same rules.
 *
 */
public class SelectiveRecursionMapper
{
    private final LinkVersionedResource asset; // The asset to link the items to

    private final MapperFacade mapper; // The mapper to map the field of the items and attributes not controlled by this
                                       // class (ie all but items asset and reviewed)

    private final Map<Long, List<Long>> relationships; // A hash map of relationships where the key is the parent id and
                                                       // the list is a list of its children.

    private final Map<Long, VersionedItemPersistDO> idMap;

    private final List<ItemRelationshipPersistDO> otherRelationships;
    private final ItemRepository itemRepository;

    /**
     * Constructor to set the fields and calculate the hash map given the top level item with its relationships
     *
     * @param asset
     * @param mapper
     * @param draftItem
     */
    public SelectiveRecursionMapper(final LinkVersionedResource asset, final MapperFacade mapper, final DraftItemDto draftItem, final ItemRepository itemRepository)
    {
        this.asset = asset;
        this.mapper = mapper;
        this.relationships = new HashMap<>();
        this.idMap = new HashMap<>();
        this.otherRelationships = new ArrayList<>();
        this.itemRepository = itemRepository;

        if (draftItem.getItemRelationshipDto() != null)
        {
            this.processRelationship(draftItem.getFromId(), draftItem.getItemRelationshipDto());
        }
    }

    /**
     * Method is called recursively to map the item at each level. Every time filling in the fields with the default
     * mapper and looking in the relationships to get any child items and them mapping them in the same way.
     *
     * @param enteredDO
     * @return
     */
    public VersionedItemPersistDO map(final VersionedItemPersistDO enteredDO)
    {
        final VersionedItemPersistDO newItem = this.mapper.map(enteredDO, VersionedItemPersistDO.class);

        final List<VersionedItemPersistDO> childItems = getItemsWithRelationship(enteredDO.getRelated(), RelationshipType.IS_PART_OF.getValue());

        ItemDO itemDO = new ItemDO();
        itemDO.setAsset(new AssetDO());
        itemDO.getAsset().setId(asset.getId());
        itemDO = this.itemRepository.merge(itemDO);

        newItem.setReviewed(false);
        newItem.setId(itemDO.getId());
        newItem.setAssetId(asset.getId());
        newItem.setAssetVersionId(asset.getVersion());

        if (!this.idMap.keySet().contains(enteredDO.getId()))
        { // add the newly created draft to the list so that it can be found by the id of the item it came from
            this.idMap.put(enteredDO.getId(), newItem);
        }

        for (final VersionedItemPersistDO childItem : childItems)
        { // for all child items in the database
            if (relationships.get(enteredDO.getId()) == null || relationships.get(enteredDO.getId()).contains(childItem.getId()))
            { // if this relationship is included in the relationship map or is null in which chase the child will be
              // copied anyway.
                final VersionedItemPersistDO draftChild = map(childItem); // map the child in the same way

                if (newItem.getRelated() == null)
                { // create a new list if no items have been added yet
                    newItem.setRelated(new ArrayList<ItemRelationshipPersistDO>());
                }
                final ItemRelationshipPersistDO relationship = new ItemRelationshipPersistDO();

                //I changed this - TOM
                relationship.setFromAssetId(newItem.getAssetId());
                relationship.setFromItemId(newItem.getId());
                relationship.setFromVersionId(newItem.getAssetVersionId());
                relationship.setFromItem(newItem);

                relationship.setAssetId(draftChild.getAssetId());
                relationship.setToItemId(draftChild.getId());
                relationship.setToVersionId(draftChild.getAssetVersionId());
                relationship.setToItem(draftChild);

                relationship.setType(new LinkDO());
                relationship.getType().setId(RelationshipType.IS_PART_OF.getValue()); // add the child to the parents
                newItem.getRelated().add(relationship);                                                                               // list of items
            }
        }

        return newItem;
    }

    /**
     * Method recurses down the tree of relationships creating a map, by adding parent ids as keys (if they are not
     * there already) and adding the ids of their children that are to be copied to a list of child ids.
     *
     * If the relationship is null at any point no key is added hence this information is preserved.
     *
     * @param parentId
     * @param draftItemRelationships
     */
    private void processRelationship(final Long parentId, final List<DraftItemRelationshipDto> draftItemRelationships)
    {
        if (!this.relationships.keySet().contains(parentId))
        {
            this.relationships.put(parentId, new ArrayList<Long>());
        }

        for (final DraftItemRelationshipDto child : draftItemRelationships)
        {
            if (child.getRelationshipTypeId() == null || RelationshipType.IS_PART_OF.getValue().equals(child.getRelationshipTypeId()))
            {
                addRelationshipToMap(parentId, child);
            }
        }
    }

    /**
     * Adds the child id to the parents list of children and then calls the above method to deal with the child's
     * children if the relationships are specified.
     *
     * @param parentId
     * @param child
     */
    private void addRelationshipToMap(final Long parentId, final DraftItemRelationshipDto child)
    {
        this.relationships.get(parentId).add(child.getOriginalItemId());

        if (child.getItemRelationshipDto() != null)
        {
            processRelationship(child.getOriginalItemId(), child.getItemRelationshipDto());
        }
    }

    /**
     * Loop over the list of item relationships of the parent selecting child items with a give relationship type.
     *
     * @param itemRelationships
     * @param relationshipTypeId
     * @return
     */
    private List<VersionedItemPersistDO> getItemsWithRelationship(final List<ItemRelationshipPersistDO> itemRelationships, final Long relationshipTypeId)
    {
        final List<VersionedItemPersistDO> items = new ArrayList<VersionedItemPersistDO>();

        for (final ItemRelationshipPersistDO relationship : itemRelationships)
        {
            if (relationship.getType().getId().equals(relationshipTypeId))
            {
                items.add(relationship.getToItem());
            }
            else // save other types for processing later
            {
                this.otherRelationships.add(relationship);
            }
        }

        return items;
    }

    public Map<Long, VersionedItemPersistDO> getIdMap()
    {
        return this.idMap;
    }

    public List<ItemRelationshipPersistDO> getOtherRelationships()
    {
        return this.otherRelationships;
    }
}
