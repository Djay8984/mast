package com.baesystems.ai.lr.dao.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.DueStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ReferenceDataVersionDO;
import com.baesystems.ai.lr.domain.mast.repositories.DueStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ReferenceDataVersionRepository;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository("ReferenceDataMetaDao")
public class ReferenceDataMetaDaoImpl implements ReferenceDataMetaDao
{
    @Autowired
    private ReferenceDataVersionRepository referenceDataVersionRepository;

    @Autowired
    private DueStatusRepository dueStatusRepository;

    private final MapperFacade mapperFacade;

    public ReferenceDataMetaDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(ReferenceDataVersionDO.class, ReferenceDataVersionDto.class).byDefault().toClassMap());
        this.mapperFacade = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public ReferenceDataVersionDto getTopLevelReferenceDataVersion()
    {
        final ReferenceDataVersionDO versionDO = this.referenceDataVersionRepository.findOne(ReferenceDataVersionRepository.TOP_LEVEL_ID);
        return versionDO != null ? this.mapperFacade.map(versionDO, ReferenceDataVersionDto.class) : null;
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getDueStatuses()
    {
        final List<DueStatusDO> dueStatuses = this.dueStatusRepository.findAll();
        return this.mapperFacade.mapAsList(dueStatuses, ReferenceDataDto.class);
    }
}
