package com.baesystems.ai.lr.dao.defects;

import com.baesystems.ai.lr.dao.codicils.WIPCodicilDefectQueryDao;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDeficiencyDO;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPDeficiencyDao extends BaseDeficiencyDao, WIPCodicilDefectQueryDao<DeficiencyDto>, LockingDao<WIPDeficiencyDO>
{

}
