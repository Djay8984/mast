package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.DummyPartyDO;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;

public class IhsPartyMapper
{
    public boolean mapBuilder(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getBuilderInfo() != null && !assetDO.getBuilderInfo().isEmpty())
        {
            final DummyPartyDO party = assetDO.getBuilderInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsAsset.setBuilder(party.getName());
            }
            if (party.getCode() != null)
            {
                allNull = false;
                ihsAsset.setBuilderCode(party.getCode());
            }
        }

        return allNull;
    }

    public boolean mapShipManager(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getShipManagerInfo() != null && !assetDO.getShipManagerInfo().isEmpty())
        {
            final DummyPartyDO party = assetDO.getShipManagerInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsAsset.setShipManager(party.getName());
            }
            if (party.getCode() != null)
            {
                allNull = false;
                ihsAsset.setShipManagerCode(party.getCode());
            }
        }

        return allNull;
    }

    public boolean mapDocCompany(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getDocCompanyInfo() != null && !assetDO.getDocCompanyInfo().isEmpty())
        {
            final DummyPartyDO party = assetDO.getDocCompanyInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsAsset.setDocCompany(party.getName());
            }
            if (party.getCode() != null)
            {
                allNull = false;
                ihsAsset.setDocCode(party.getCode());
            }
        }

        return allNull;
    }

    public boolean mapOperator(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getOperatorInfo() != null && !assetDO.getOperatorInfo().isEmpty())
        {
            final DummyPartyDO party = assetDO.getOperatorInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsAsset.setOperator(party.getName());
            }
            if (party.getCode() != null)
            {
                allNull = false;
                ihsAsset.setOperatorCode(party.getCode());
            }
        }

        return allNull;
    }

    public boolean mapTechManager(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getTechManagerInfo() != null && !assetDO.getTechManagerInfo().isEmpty())
        {
            final DummyPartyDO party = assetDO.getTechManagerInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsAsset.setTechManager(party.getName());
            }
            if (party.getCode() != null)
            {
                allNull = false;
                ihsAsset.setTechManagerCode(party.getCode());
            }
        }

        return allNull;
    }
}
