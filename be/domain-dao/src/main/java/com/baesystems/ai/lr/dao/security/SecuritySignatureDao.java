package com.baesystems.ai.lr.dao.security;

import java.util.List;

import com.baesystems.ai.lr.dto.security.SecureSignatureDto;

public interface SecuritySignatureDao
{
    List<SecureSignatureDto> getSecuritySignatures();
}
