package com.baesystems.ai.lr.dao.cases;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.domain.mast.repositories.CaseMilestoneRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.enums.CaseMilestoneStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CaseMilestoneDao")
public class CaseMilestoneDaoImpl extends AbstractLockingDaoImpl<CaseMilestoneDO> implements CaseMilestoneDao
{

    @Autowired
    private CaseMilestoneRepository caseMilestoneRepository;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();

        FACTORY.registerClassMap(FACTORY.classMap(CaseMilestoneDO.class, CaseMilestoneDto.class)
                .customize(new StaleObjectMapper<CaseMilestoneDO, CaseMilestoneDto>())
                .byDefault().toClassMap());
    }

    public CaseMilestoneDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public CaseMilestoneListDto getMilestonesForCase(final Long caseId)
    {
        final List<CaseMilestoneDO> output = this.caseMilestoneRepository.findMilestonesForCase(caseId);
        final CaseMilestoneListDto result = new CaseMilestoneListDto();
        result.setCaseMilestoneList(getMapper().mapAsList(output, CaseMilestoneDto.class));
        return output == null ? null : result;
    }

    @Override
    @Transactional
    public CaseMilestoneDto updateMilestone(final Long caseId, final CaseMilestoneDto caseMilestone)
    {
        CaseMilestoneDO updatedMilestone = getMapper().map(caseMilestone, CaseMilestoneDO.class);
        updatedMilestone = this.caseMilestoneRepository.merge(updatedMilestone);
        return updatedMilestone == null ? null : getMapper().map(updatedMilestone, CaseMilestoneDto.class);
    }

    @Override
    @Transactional
    public CaseMilestoneDto getCaseMilestone(final Long caseMilestoneId)
    {
        final CaseMilestoneDO result = this.caseMilestoneRepository.findOne(caseMilestoneId);
        return result == null ? null : getMapper().map(result, CaseMilestoneDto.class);
    }

    @Override
    @Transactional
    public CaseMilestoneDto getParentMilestone(final Long caseMilestoneId, final Long caseId)
    {
        final CaseMilestoneDO result = this.caseMilestoneRepository.findParentMilestone(caseMilestoneId, caseId);
        return result == null ? null : getMapper().map(result, CaseMilestoneDto.class);
    }

    @Override
    @Transactional
    public List<CaseMilestoneDto> getChildMilestones(final List<Long> milestoneId, final Long caseId)
    {
        List<CaseMilestoneDO> output = new ArrayList<CaseMilestoneDO>();

        if (milestoneId != null && !milestoneId.isEmpty())
        {
            output = this.caseMilestoneRepository.findChildMilestones(milestoneId, caseId);
        }

        return getMapper().mapAsList(output, CaseMilestoneDto.class);
    }

    @Override
    @Transactional
    public List<Long> getCaseMilestoneIdsForCase(final Long caseId)
    {
        return this.caseMilestoneRepository.findMilestoneIdsForCase(caseId);
    }

    @Override
    @Transactional
    public Boolean isCaseMilestoneInScopeByCaseAndMilestoneId(final Long milestoneId, final Long caseId)
    {
        return this.caseMilestoneRepository.isCaseMilestoneInScopeByCaseAndMilestoneId(milestoneId, caseId);
    }

    @Override
    @Transactional
    public Long getStatusIdMilestoneIdAndCaseId(final Long milestoneId, final Long caseId)
    {
        return this.caseMilestoneRepository.findStatusIdMilestoneIdAndCaseId(milestoneId, caseId);
    }

    @Override
    @Transactional
    public Boolean areAllInScopeMilestonesCompleteForCase(final Long caseId)
    {
        final Integer inScopeMilestones = this.caseMilestoneRepository.countCaseMilestonesInStatusAndScope(caseId, true, null);
        final Integer completeMilestones = this.caseMilestoneRepository.countCaseMilestonesInStatusAndScope(caseId, true,
                CaseMilestoneStatusType.COMPLETE.getValue());

        return inScopeMilestones == null ? Boolean.FALSE : inScopeMilestones.equals(completeMilestones);
    }

    @Override
    public LockingRepository<CaseMilestoneDO> getLockingRepository()
    {
        return this.caseMilestoneRepository;
    }
}
