package com.baesystems.ai.lr.dao.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsEquipmentDetailsDao
{
    IhsEquipmentDetailsDto getEquipmentDetails(Long imoNumber)  throws RecordNotFoundException;
}
