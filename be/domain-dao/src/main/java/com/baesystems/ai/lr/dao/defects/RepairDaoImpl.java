package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.defects.DefectItemDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.RepairDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.RepairItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.RepairRepository;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.defects.RepairPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DomainObjectFactory;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

@Repository(value = "RepairDao")
public class RepairDaoImpl extends AbstractLockingDaoImpl<RepairDO> implements RepairDao
{
    private static final Logger LOG = LoggerFactory.getLogger(RepairDaoImpl.class);

    @Autowired
    private RepairRepository repairRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();

        FACTORY.registerObjectFactory(new DomainObjectFactory<RepairDO>(RepairDO.class), RepairDO.class);

        FACTORY.registerClassMap(FACTORY.classMap(RepairDO.class, RepairDto.class)
                .customize(new StaleObjectMapper<RepairDO, RepairDto>())
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(DefectItemDO.class, DefectItemDto.class).byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(RepairItemDO.class, RepairItemDto.class).byDefault().toClassMap());
    }

    public RepairDaoImpl()
    {
        super(FACTORY);

    }

    @Override
    @Transactional
    public Boolean repairExists(final Long repairId)
    {
        return this.repairRepository.exists(repairId);
    }

    @Override
    @Transactional
    public RepairDto getRepair(final Long repairId)
    {
        LOG.debug("getRepair with ID {}", repairId);

        final RepairDO repairDo = this.repairRepository.findOne(repairId);

        RepairDto repairDto = null;
        if (repairDo != null)
        {
            repairDto = getMapper().map(repairDo, RepairDto.class);
        }
        return repairDto;
    }

    @Override
    @Transactional
    public RepairDto saveRepair(final RepairDto defectDto)
    {
        RepairDO repair = getMapper().map(defectDto, RepairDO.class);
        if (repair.getRepairs() != null)
        {
            for (final RepairItemDO value : repair.getRepairs())
            {
                value.setRepair(repair);

                if (value.getItem() != null)
                {
                    value.getItem().setDefect(repair.getDefect());
                }
            }
        }
        repair = this.repairRepository.merge(repair);
        return getMapper().map(repair, RepairDto.class);
    }

    @Override
    @Transactional
    public Boolean getAreAllItemsPermanentlyRepaired(final Long defectId)
    {
        final Integer itemCount = repairRepository.countItemsRequiringRepair(defectId, RepairActionType.PERMANENT.value());
        return itemCount == null || itemCount == 0;
    }

    /**
     * @param defectId, the id of the defect.
     * @return true if the defect has at least one confirmed permanent repair.
     */
    @Override
    @Transactional
    public Boolean getDefectHasPermanentRepair(final Long defectId)
    {
        final Integer permamentRepairCount = repairRepository.countPermanentRepairs(defectId, RepairActionType.PERMANENT.value());
        return permamentRepairCount != null && permamentRepairCount > 0;
    }

    @Override
    @Transactional
    public PageResource<RepairDto> getRepairsForDefect(final Pageable pageable, final Long defectId)
    {
        final Page<RepairDO> repairData = this.repairRepository.findAllByDefectId(pageable, defectId);

        final Page<RepairDto> result = repairData.map(asset -> getMapper().map(asset, RepairDto.class));
        return pageFactory.createPageResource(result, RepairPageResourceDto.class);
    }

    /**
     * Counts the total number of temporary and permanent repairs associated with a defect.
     */
    @Override
    @Transactional
    public Integer countRepairsForDefect(final Long defectId)
    {
        return this.repairRepository.countRepairsForDefect(defectId, RepairActionType.getRepairsForDefect());
    }

    @Override
    @Transactional
    public List<RepairDto> findRepairsForAsset(final Long assetId)
    {
        final List<RepairDO> repairDOs = this.repairRepository.findRepairsForAsset(assetId);
        return getMapper().mapAsList(repairDOs, RepairDto.class);
    }

    @Override
    @Transactional
    public PageResource<RepairDto> getRepairsForCoC(final Long cocId, final Pageable pageable)
    {
        final Page<RepairDO> repairData = this.repairRepository.findAllByCoC(pageable, cocId);

        final Page<RepairDto> result = repairData.map(asset -> getMapper().map(asset, RepairDto.class));
        return pageFactory.createPageResource(result, RepairPageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean repairExistsForDefect(final Long repairId, final Long defectId)
    {
        LOG.debug(String.format("Verifying that repair %d exists for defect %d", repairId, defectId));
        return this.repairRepository.repairExistsForDefect(repairId, defectId);
    }

    @Override
    @Transactional
    public Boolean repairExistsForCoC(final Long repairId, final Long cocId)
    {
        LOG.debug(String.format("Verifying that repair %d exists for coc %d", repairId, cocId));
        return this.repairRepository.repairExistsForCoC(repairId, cocId);
    }

    @Override
    @Transactional
    public Boolean getCocHasPermanentRepair(final Long cocId)
    {
        final Integer permamentRepairCount = repairRepository.countCocPermanentRepairs(cocId, RepairActionType.PERMANENT.value());
        return permamentRepairCount != null && permamentRepairCount > 0;
    }

    @Override
    public LockingRepository<RepairDO> getLockingRepository()
    {
        return this.repairRepository;
    }
}
