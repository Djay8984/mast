package com.baesystems.ai.lr.dao.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.IhsGroupFleetDO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsGroupFleetRepository;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "IhsGroupFleetDao")
public class IhsGroupFleetDaoImpl implements IhsGroupFleetDao
{
    @Autowired
    private IhsGroupFleetRepository ihsGroupFleetRepository;

    private final MapperFacade mapper;

    public IhsGroupFleetDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(IhsGroupFleetDao.class, IhsGroupFleetDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public final IhsGroupFleetDto getIhsGroupFleet(final Long imoNumber)
    {
        final IhsGroupFleetDO output = this.ihsGroupFleetRepository.findOne(imoNumber);
        return output == null ? null : this.mapper.map(output, IhsGroupFleetDto.class);
    }
}
