package com.baesystems.ai.lr.dao.attachments;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.SupplementaryInformationDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.ActionableItemDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.AssetNoteDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPAssetNoteDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SupplementaryInformationRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "AttachmentDao")
@SuppressWarnings("PMD.GodClass")
public class AttachmentDaoImpl implements AttachmentDao
{
    private static final Logger LOG = LoggerFactory.getLogger(AttachmentDaoImpl.class);

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private SupplementaryInformationRepository supplementaryInformationRepository;

    @Autowired
    private LrEmployeeRepository employeeRepository;

    private final MapperFacade mapper;

    public AttachmentDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(SupplementaryInformationDO.class, SupplementaryInformationDto.class).byDefault().toClassMap());

        factory.classMap(AttachmentLinkDO.class, SupplementaryInformationDto.class)
                .byDefault()
                .customize(new AttachmentLinkMapper())
                .register();
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity"})
    public SupplementaryInformationDto saveAttachment(final AttachmentTargetType target, final Long objectId,
            final SupplementaryInformationDto supplementaryInformationDto)
    {
        AttachmentLinkDO attachmentLinkDO = mapper.map(supplementaryInformationDto, AttachmentLinkDO.class);
        attachmentLinkDO.setDeleted(false);

        // set up for quick additions of other attachment locations
        switch (target)
        {
            case CASE:
                attachmentLinkDO.setaCase(new CaseDO());
                attachmentLinkDO.getaCase().setId(objectId);
                break;
            case MILESTONE:
                attachmentLinkDO.setMilestone(new CaseMilestoneDO());
                attachmentLinkDO.getMilestone().setId(objectId);
                break;
            case SERVICE:
                attachmentLinkDO.setService(new ScheduledServiceDO());
                attachmentLinkDO.getService().setId(objectId);
                break;
            case COC:
                attachmentLinkDO.setCoC(new CoCDO());
                attachmentLinkDO.getCoC().setId(objectId);
                break;
            case WIP_COC:
                attachmentLinkDO.setWipCoC(new WIPCoCDO());
                attachmentLinkDO.getWipCoC().setId(objectId);
                break;
            case ASSET_NOTE:
                attachmentLinkDO.setAssetNote(new AssetNoteDO());
                attachmentLinkDO.getAssetNote().setId(objectId);
                break;
            case WIP_ASSET_NOTE:
                attachmentLinkDO.setWipAssetNote(new WIPAssetNoteDO());
                attachmentLinkDO.getWipAssetNote().setId(objectId);
                break;
            case ASSET:
                attachmentLinkDO.setAsset(new AssetDO());
                attachmentLinkDO.getAsset().setId(objectId);
                break;
            case JOB:
                attachmentLinkDO.setJob(new JobDO());
                attachmentLinkDO.getJob().setId(objectId);
                break;
            case ITEM:
                attachmentLinkDO.setItem(new ItemDO());
                attachmentLinkDO.getItem().setId(objectId);
                break;
            case DEFECT:
                attachmentLinkDO.setDefect(new DefectDO());
                attachmentLinkDO.getDefect().setId(objectId);
                break;
            case WIP_DEFECT:
                attachmentLinkDO.setWipDefect(new WIPDefectDO());
                attachmentLinkDO.getWipDefect().setId(objectId);
                break;
            case ACTIONABLE_ITEM:
                attachmentLinkDO.setActionableItem(new ActionableItemDO());
                attachmentLinkDO.getActionableItem().setId(objectId);
                break;
            case WIP_ACTIONABLE_ITEM:
                attachmentLinkDO.setWipActionableItem(new WIPActionableItemDO());
                attachmentLinkDO.getWipActionableItem().setId(objectId);
                break;
            case REPAIR:
                attachmentLinkDO.setRepair(new LinkDO());
                attachmentLinkDO.getRepair().setId(objectId);
                break;
            case WIP_REPAIR:
                attachmentLinkDO.setWipRepair(new LinkDO());
                attachmentLinkDO.getWipRepair().setId(objectId);
                break;
            case TASK:
                attachmentLinkDO.setWorkItem(new WorkItemDO());
                attachmentLinkDO.getWorkItem().setId(objectId);
                break;
            case WIP_TASK:
                attachmentLinkDO.setWipWorkItem(new WIPWorkItemDO());
                attachmentLinkDO.getWipWorkItem().setId(objectId);
                break;
            case SURVEY:
                attachmentLinkDO.setSurvey(new SurveyDO());
                attachmentLinkDO.getSurvey().setId(objectId);
                break;
            default:
                // how did it get here?
        }

        attachmentLinkDO = this.supplementaryInformationRepository.merge(attachmentLinkDO);
        attachmentLinkDO = addAuthor(attachmentLinkDO);
        return mapper.map(attachmentLinkDO, SupplementaryInformationDto.class);
    }

    @Override
    @Transactional
    public void deleteAttachment(final Long supplementaryInformationId)
    {
        this.supplementaryInformationRepository.delete(supplementaryInformationId);
    }

    @Override
    @Transactional
    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity"})
    public PageResource<SupplementaryInformationDto> findAttachments(final Pageable pageable, final AttachmentTargetType target,
            final Long targetId)
    {
        LOG.debug(String.format("Finding all Supplementary Information for %s %d", target.toString(), targetId));

        Page<AttachmentLinkDO> output = new PageImpl<AttachmentLinkDO>(new ArrayList<AttachmentLinkDO>());

        switch (target)
        {
            case CASE:
                output = this.supplementaryInformationRepository.findCaseAttachments(pageable, targetId);
                break;
            case MILESTONE:
                output = this.supplementaryInformationRepository.findMilestoneAttachments(pageable, targetId);
                break;
            case SERVICE:
                output = this.supplementaryInformationRepository.findServiceAttachments(pageable, targetId);
                break;
            case COC:
                output = this.supplementaryInformationRepository.findCoCAttachments(pageable, targetId);
                break;
            case WIP_COC:
                output = this.supplementaryInformationRepository.findWIPCoCAttachments(pageable, targetId);
                break;
            case ASSET_NOTE:
                output = this.supplementaryInformationRepository.findAssetNoteAttachments(pageable, targetId);
                break;
            case WIP_ASSET_NOTE:
                output = this.supplementaryInformationRepository.findWIPAssetNoteAttachments(pageable, targetId);
                break;
            case ASSET:
                output = this.supplementaryInformationRepository.findAssetAttachments(pageable, targetId);
                break;
            case JOB:
                output = this.supplementaryInformationRepository.findJobAttachments(pageable, targetId);
                break;
            case ITEM:
                output = this.supplementaryInformationRepository.findItemAttachments(pageable, targetId);
                break;
            case DEFECT:
                output = this.supplementaryInformationRepository.findDefectAttachments(pageable, targetId);
                break;
            case WIP_DEFECT:
                output = this.supplementaryInformationRepository.findWIPDefectAttachments(pageable, targetId);
                break;
            case ACTIONABLE_ITEM:
                output = this.supplementaryInformationRepository.findActionableItemsAttachments(pageable, targetId);
                break;
            case WIP_ACTIONABLE_ITEM:
                output = this.supplementaryInformationRepository.findWIPActionableItemsAttachments(pageable, targetId);
                break;
            case REPAIR:
                output = this.supplementaryInformationRepository.findRepairAttachments(pageable, targetId);
                break;
            case WIP_REPAIR:
                output = this.supplementaryInformationRepository.findWIPRepairAttachments(pageable, targetId);
                break;
            case TASK:
                output = this.supplementaryInformationRepository.findWorkItemAttachments(pageable, targetId);
                break;
            case WIP_TASK:
                output = this.supplementaryInformationRepository.findWIPWorkItemAttachments(pageable, targetId);
                break;
            case SURVEY:
                output = this.supplementaryInformationRepository.findSurveyAttachments(pageable, targetId);
                break;
            default:
                // how did it get here?
        }

        return pageFactory.createPageResource(output.map(new AttachmentConverter()), SupplementaryInformationPageResourceDto.class);
    }

    @Override
    @Transactional
    public SupplementaryInformationDto getAttachment(final Long supplementaryInformationId)
    {
        LOG.debug("Retrieving Supplementary Information with ID {}", supplementaryInformationId);
        final SupplementaryInformationDO supplementaryInformationData = this.supplementaryInformationRepository.findOne(supplementaryInformationId);
        return supplementaryInformationData == null ? null : this.mapper.map(supplementaryInformationData, SupplementaryInformationDto.class);
    }

    class AttachmentConverter implements Converter<AttachmentLinkDO, SupplementaryInformationDto>
    {
        @Override
        public SupplementaryInformationDto convert(final AttachmentLinkDO supplementaryInformation)
        {
            return mapper.map(supplementaryInformation, SupplementaryInformationDto.class);
        }
    }

    @Override
    @Transactional
    public SupplementaryInformationDto updateAttachment(final SupplementaryInformationDto attachmentDto)
    {
        LOG.debug("Updating Attachment with ID: {}", attachmentDto.getId());

        SupplementaryInformationDO attachmentDO = mapper.map(attachmentDto, AttachmentLinkDO.class);
        attachmentDO = supplementaryInformationRepository.merge(attachmentDO);
        return mapper.map(attachmentDO, SupplementaryInformationDto.class);
    }

    @Override
    @Transactional
    public AttachmentLinkDO addAuthor(final AttachmentLinkDO attachmentLinkDO)
    {
        if (attachmentLinkDO.getUpdatedBy() != null)
        {
            final LrEmployeeDO authorName = this.employeeRepository.findByFullName(attachmentLinkDO.getUpdatedBy());
            if (authorName != null)
            {
                attachmentLinkDO.setAuthor(authorName);
            }
        }
        return this.supplementaryInformationRepository.merge(attachmentLinkDO);
    }

    @Override
    public List<SupplementaryInformationDto> findAttachmentsByQuery(final AttachmentLinkQueryDto attachmentLinkQueryDto)
    {
        final List<AttachmentLinkDO> attachmentLinkDOs = this.supplementaryInformationRepository.findAttachmentsByQuery(attachmentLinkQueryDto);
        return this.mapper.mapAsList(attachmentLinkDOs, SupplementaryInformationDto.class);
    }

    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity", "PMD.ModifiedCyclomaticComplexity", "PMD.ExcessiveMethodLength"})
    public static class AttachmentLinkMapper extends CustomMapper<AttachmentLinkDO, SupplementaryInformationDto>
    {
        @Override
        public void mapAtoB(final AttachmentLinkDO attachmentLinkDO, final SupplementaryInformationDto supplementaryInformationDto,
                final MappingContext context)
        {
            supplementaryInformationDto.setEntityLink(new SupplementaryInformationDto.AttachmentEntityLink());
            String entityType = null;
            Long entityId = null;

            // Ugly, will try to impl better when have time.
            // Data assumption is that an attachment link record can only exist against a single entity.
            if (attachmentLinkDO.getaCase() != null)
            {
                entityId = attachmentLinkDO.getaCase().getId();
                entityType = AttachmentTargetType.CASE.name();
            }
            else if (attachmentLinkDO.getActionableItem() != null)
            {
                entityId = attachmentLinkDO.getActionableItem().getId();
                entityType = AttachmentTargetType.ACTIONABLE_ITEM.name();
            }
            else if (attachmentLinkDO.getAsset() != null)
            {
                entityId = attachmentLinkDO.getAsset().getId();
                entityType = AttachmentTargetType.ASSET.name();
            }
            else if (attachmentLinkDO.getAssetNote() != null)
            {
                entityId = attachmentLinkDO.getAssetNote().getId();
                entityType = AttachmentTargetType.ASSET_NOTE.name();
            }
            else if (attachmentLinkDO.getCertificate() != null)
            {
                entityId = attachmentLinkDO.getCertificate().getId();
                entityType = AttachmentTargetType.CERTIFICATE.name();
            }
            else if (attachmentLinkDO.getCoC() != null)
            {
                entityId = attachmentLinkDO.getCoC().getId();
                entityType = AttachmentTargetType.COC.name();
            }
            else if (attachmentLinkDO.getDefect() != null)
            {
                entityId = attachmentLinkDO.getDefect().getId();
                entityType = AttachmentTargetType.DEFECT.name();
            }
            else if (attachmentLinkDO.getItem() != null)
            {
                entityId = attachmentLinkDO.getItem().getId();
                entityType = AttachmentTargetType.ITEM.name();
            }
            else if (attachmentLinkDO.getJob() != null)
            {
                entityId = attachmentLinkDO.getJob().getId();
                entityType = AttachmentTargetType.JOB.name();
            }
            else if (attachmentLinkDO.getMilestone() != null)
            {
                entityId = attachmentLinkDO.getMilestone().getId();
                entityType = AttachmentTargetType.MILESTONE.name();
            }
            else if (attachmentLinkDO.getRepair() != null)
            {
                entityId = attachmentLinkDO.getRepair().getId();
                entityType = AttachmentTargetType.REPAIR.name();
            }
            else if (attachmentLinkDO.getReport() != null)
            {
                entityId = attachmentLinkDO.getReport().getId();
                entityType = AttachmentTargetType.REPORT.name();
            }
            else if (attachmentLinkDO.getService() != null)
            {
                entityId = attachmentLinkDO.getService().getId();
                entityType = AttachmentTargetType.SERVICE.name();
            }
            else if (attachmentLinkDO.getWipActionableItem() != null)
            {
                entityId = attachmentLinkDO.getWipActionableItem().getId();
                entityType = AttachmentTargetType.WIP_ACTIONABLE_ITEM.name();
            }
            else if (attachmentLinkDO.getWipAssetNote() != null)
            {
                entityId = attachmentLinkDO.getWipAssetNote().getId();
                entityType = AttachmentTargetType.WIP_ASSET_NOTE.name();
            }
            else if (attachmentLinkDO.getWipCoC() != null)
            {
                entityId = attachmentLinkDO.getWipCoC().getId();
                entityType = AttachmentTargetType.REPORT.name();
            }
            else if (attachmentLinkDO.getWipDefect() != null)
            {
                entityId = attachmentLinkDO.getWipDefect().getId();
                entityType = AttachmentTargetType.WIP_DEFECT.name();
            }
            else if (attachmentLinkDO.getRepair() != null)
            {
                entityId = attachmentLinkDO.getRepair().getId();
                entityType = AttachmentTargetType.REPAIR.name();
            }
            else if (attachmentLinkDO.getWipRepair() != null)
            {
                entityId = attachmentLinkDO.getWipRepair().getId();
                entityType = AttachmentTargetType.WIP_REPAIR.name();
            }
            else if (attachmentLinkDO.getWorkItem() != null)
            {
                entityId = attachmentLinkDO.getWorkItem().getId();
                entityType = AttachmentTargetType.TASK.name();
            }
            else if (attachmentLinkDO.getWipWorkItem() != null)
            {
                entityId = attachmentLinkDO.getWipWorkItem().getId();
                entityType = AttachmentTargetType.WIP_TASK.name();
            }
            else if (attachmentLinkDO.getSurvey() != null)
            {
                entityId = attachmentLinkDO.getSurvey().getId();
                entityType = AttachmentTargetType.SURVEY.name();
            }
            supplementaryInformationDto.getEntityLink().setLink(new LinkResource(entityId));
            supplementaryInformationDto.getEntityLink().setType(entityType);
        }
    }

    @Override
    @Transactional
    public Boolean exists(final Long supplementaryInformationId)
    {
        return this.supplementaryInformationRepository.exists(supplementaryInformationId);
    }
}
