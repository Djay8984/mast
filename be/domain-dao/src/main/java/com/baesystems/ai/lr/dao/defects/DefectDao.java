package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.CodicilDefectQueryDao;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;

public interface DefectDao extends BaseDefectDao, CodicilDefectQueryDao<DefectDto>
{
    DefectLightDto getDefectLight(Long defectId);

    PageResource<DefectDto> getDefectsForAsset(Pageable pageable, Long assetId, Long statusId);

    Boolean defectExists(Long defectId);

    Boolean doAnyItemsHaveOpenDefects(List<Long> itemIds);

    Boolean defectExistsForAsset(Long assetId, Long defectId);
}
