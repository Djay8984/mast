package com.baesystems.ai.lr.dao.references.attachments;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.AttachmentTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ConfidentialityTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AttachmentTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ConfidentialityTypeRepository;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "AttachmentReferenceDataDao")
public class AttachmentReferenceDataDaoImpl implements AttachmentReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(AttachmentReferenceDataDaoImpl.class);

    @Autowired
    private AttachmentTypeRepository attachmentTypeRepository;

    @Autowired
    private ConfidentialityTypeRepository confidentialityTypeRepository;

    private final MapperFacade mapper;

    public AttachmentReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(AttachmentTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ConfidentialityTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getAttachmentTypes()
    {
        LOG.debug("Retrieving all Attachment Types");
        final List<AttachmentTypeDO> types = this.attachmentTypeRepository.findAll();
        return types == null ? null : this.mapper.mapAsList(types, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getAttachmentType(final Long attachmentTypeId)
    {
        LOG.debug("Retrieving Attachment Type with ID {}", attachmentTypeId);
        final AttachmentTypeDO type = this.attachmentTypeRepository.findOne(attachmentTypeId);
        return type == null ? null : this.mapper.map(type, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getConfidentialityTypes()
    {
        LOG.debug("Retrieving all Confidentiality Types");
        final List<ConfidentialityTypeDO> types = this.confidentialityTypeRepository.findAll();
        return types == null ? null : this.mapper.mapAsList(types, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getConfidentialityType(final Long confidentialityTypeId)
    {
        LOG.debug("Retrieving Confidentiality Type with ID {}", confidentialityTypeId);
        final ConfidentialityTypeDO type = this.confidentialityTypeRepository.findOne(confidentialityTypeId);
        return type == null ? null : this.mapper.map(type, ReferenceDataDto.class);
    }
}
