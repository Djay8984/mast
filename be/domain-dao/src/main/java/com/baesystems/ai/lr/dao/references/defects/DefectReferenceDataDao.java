package com.baesystems.ai.lr.dao.references.defects;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectCategoryDO;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectDetailDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface DefectReferenceDataDao
{
    List<DefectCategoryDto> getDefectCategories();

    DefectCategoryDto getDefectCategory(Long defectCategoryId);

    DefectCategoryDO getDefectCategoryDO(Long defectCategoryId);

    List<DefectDetailDto> getDefectDetails();

    List<ReferenceDataDto> getDefectStatuses();

    ReferenceDataDto getDefectStatus(Long defectStatusId);

    Integer countMatchingDefectValueIds(List<Long> valueIds);
}
