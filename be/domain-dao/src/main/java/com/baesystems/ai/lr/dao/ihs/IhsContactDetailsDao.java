package com.baesystems.ai.lr.dao.ihs;

import java.util.List;

import com.baesystems.ai.lr.dto.ihs.CompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;

public interface IhsContactDetailsDao
{
    List<CompanyContactDto> getAddress(final Long imoNumber);
    
    List<IhsPersonnelContactDto> getPersonnelAddress(final Long imoNumber);
}
