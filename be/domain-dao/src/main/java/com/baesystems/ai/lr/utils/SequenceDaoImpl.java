package com.baesystems.ai.lr.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SequenceRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPSequenceRepository;

@Repository
public class SequenceDaoImpl implements SequenceDao
{
    @Autowired
    private JobRepository jobRepository;

    /**
     * Returns the next available sequence number for an entity which has an asset specific sequence
     * <p>
     * The repository methods calls use pessimistic locking to lock the entity until the transaction has been committed,
     * this prevents repeated reads when the route is flooded with simultaneous creates, e.g. migration see
     * AssetSequenceRestTest.
     *
     * @param id AssetId or JobId depending on WIP
     * @param wip
     * @param sequenceRepository entity repository
     * @param wipSequenceRepository wip entiy repository
     * @return next value in the sequence
     */
    @Override
    @Transactional
    public Integer getNextAssetSpecificSequenceValue(final Long id, final Boolean wip, final SequenceRepository sequenceRepository,
            final WIPSequenceRepository wipSequenceRepository)
    {
        final Long assetId = wip ? this.jobRepository.getAssetIdForJob(id) : id;
        final Integer maxSequenceNumberResult = sequenceRepository.maxSequenceNumberForAssetIdWithLock(assetId);
        final Integer maxSequenceNumberWipResult = wipSequenceRepository.maxSequenceNumberForAssetIdWithLock(assetId);
        final int maxSequenceNumber = maxSequenceNumberResult == null ? 0 : maxSequenceNumberResult.intValue();
        final int maxSequenceNumberWip = maxSequenceNumberWipResult == null ? 0 : maxSequenceNumberWipResult.intValue();
        return Math.max(maxSequenceNumber, maxSequenceNumberWip) + 1;
    }
}
