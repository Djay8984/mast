package com.baesystems.ai.lr.dao.assets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.customers.OrganisationDO;
import com.baesystems.ai.lr.domain.mast.repositories.OrganisationRepository;
import com.baesystems.ai.lr.dto.assets.BuilderDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "BuilderDao")
public class BuilderDaoImpl implements BuilderDao
{
    @Autowired
    private OrganisationRepository organisationRepository;

    private final MapperFacade mapper;

    public BuilderDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(OrganisationDO.class, BuilderDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<BuilderDto> findAll()
    {
        final List<OrganisationDO> output = this.organisationRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, BuilderDto.class);
    }

}
