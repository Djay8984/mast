package com.baesystems.ai.lr.dao.references.employees;

import java.util.List;

import com.baesystems.ai.lr.dto.references.EmployeeOfficeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface EmployeeReferenceDataDao
{
    List<ReferenceDataDto> getEmployeeRoles();

    ReferenceDataDto getEmployeeRole(Long roleId);

    List<EmployeeOfficeDto> getEmployeeOffices();
}
