package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.dto.references.TeamDto;

import java.util.List;

public interface TeamDao
{
    List<TeamDto> findAll();
}
