package com.baesystems.ai.lr.utils;

import java.util.ArrayList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.baesystems.ai.lr.dto.paging.OrderDto;
import com.baesystems.ai.lr.dto.paging.PageLinkDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.dto.paging.SortDto;
import com.baesystems.ai.lr.exception.MastSystemException;

@Component
public class PageResourceFactoryImpl implements PageResourceFactory
{
    private static final String PAGE_PARAM = "page";

    private static final String SIZE_PARAM = "size";

    @Override
    public <U extends PageResource<T>, T> U createPageResource(final Page<T> page, final Class<U> wrapperClazz)

    {
        U result = null;
        try
        {
            result = wrapperClazz.newInstance();
        }
        catch (final InstantiationException exception)
        {
            throw new MastSystemException("createPageResource", exception);
        }
        catch (final IllegalAccessException exception)
        {
            throw new MastSystemException("createPageResource", exception);
        }

        final PaginationDto pagination = this.createPagination(page);
        result.setPagination(pagination);
        result.setContent(page.getContent());

        return result;
    }

    private PaginationDto createPagination(final Page<?> page)
    {
        final PaginationDto pagination = new PaginationDto();
        pagination.setSize(page.getSize());
        pagination.setNumber(page.getNumber());
        pagination.setFirst(page.isFirst());
        pagination.setLast(page.isLast());
        pagination.setTotalPages(page.getTotalPages());
        pagination.setTotalElements(page.getTotalElements());
        pagination.setNumberOfElements(page.getNumberOfElements());
        pagination.setLink(new ArrayList<PageLinkDto>());

        if (page.getSort() != null)
        {
            pagination.setSort(this.createSort(page.getSort()));
        }

        // Create Links
        if (page.hasPrevious())
        {
            final String path = this.createBuilder()
                    .queryParam(PAGE_PARAM, page.getNumber() - 1)
                    .queryParam(SIZE_PARAM, page.getSize())
                    .build()
                    .toUriString();
            final Link link = new Link(path, Link.REL_PREVIOUS);
            pagination.getLink().add(this.createPageLink(link));
        }

        if (page.hasNext())
        {
            final String path = this.createBuilder()
                    .queryParam(PAGE_PARAM, page.getNumber() + 1)
                    .queryParam(SIZE_PARAM, page.getSize())
                    .build()
                    .toUriString();
            final Link link = new Link(path, Link.REL_NEXT);
            pagination.getLink().add(this.createPageLink(link));
        }

        Link link = this.buildPageLink(PAGE_PARAM, 0, SIZE_PARAM, page.getSize(), Link.REL_FIRST);
        pagination.getLink().add(this.createPageLink(link));

        final int indexOfLastPage = page.getTotalPages() - 1;
        link = this.buildPageLink(PAGE_PARAM, indexOfLastPage, SIZE_PARAM, page.getSize(), Link.REL_LAST);
        pagination.getLink().add(this.createPageLink(link));

        link = this.buildPageLink(PAGE_PARAM, page.getNumber(), SIZE_PARAM, page.getSize(), Link.REL_SELF);
        pagination.getLink().add(this.createPageLink(link));
        return pagination;
    }

    private SortDto createSort(final Sort sort)
    {

        final SortDto result = new SortDto();
        result.setOrders(new ArrayList<OrderDto>());

        for (final Order order : sort)
        {
            final OrderDto orderDto = new OrderDto();
            orderDto.setDirection(order.getDirection().name());
            orderDto.setIgnoreCase(order.isIgnoreCase());
            orderDto.setProperty(order.getProperty());

            result.getOrders().add(orderDto);
        }
        return result;
    }

    private UriComponentsBuilder createBuilder()
    {
        final String fullPath = ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString();
        final String basePath = ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString();
        final String relativePath = fullPath.replace(basePath, "");
        return ServletUriComponentsBuilder.fromPath(relativePath);
    }

    private Link buildPageLink(final String pageParam, final int pageNo, final String sizeParam, final int size,
            final String rel)
    {
        final String path = this.createBuilder()
                .queryParam(pageParam, pageNo)
                .queryParam(sizeParam, size)
                .build()
                .toUriString();
        return new Link(path, rel);
    }

    private PageLinkDto createPageLink(final Link link)
    {
        final PageLinkDto pageLink = new PageLinkDto();
        pageLink.setRel(link.getRel());
        pageLink.setHref(link.getHref());

        return pageLink;
    }
}
