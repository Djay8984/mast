package com.baesystems.ai.lr.dao.employees;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.OfficeRepository;
import com.baesystems.ai.lr.dto.references.OfficeDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "OfficeDao")
public class OfficeDaoImpl implements OfficeDao
{
    @Autowired
    private OfficeRepository officeRepository;

    private final MapperFacade mapper;

    public OfficeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(OfficeDO.class, OfficeDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public OfficeDto getOffice(final Long officeId)
    {
        final OfficeDO output = this.officeRepository.findOne(officeId);
        return output == null ? null : mapper.map(output, OfficeDto.class);
    }

    @Override
    @Transactional
    public List<OfficeDto> findAll()
    {
        final List<OfficeDO> output = this.officeRepository.findAll();
        return this.mapper.mapAsList(output, OfficeDto.class);
    }

    @Override
    @Transactional
    public OfficeDto getOfficeByCode(final String code)
    {
        final OfficeDO output = this.officeRepository.findByCode(code);
        return output == null ? null : mapper.map(output, OfficeDto.class);
    }

    @Override
    @Transactional
    public String getOfficeName(final Long officeId)
    {
        return this.officeRepository.findNameById(officeId);
    }
}
