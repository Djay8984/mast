package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.mast.repositories.SequenceRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPSequenceRepository;

public interface SequenceDao
{
    Integer getNextAssetSpecificSequenceValue(Long id, Boolean wip, SequenceRepository sequenceRepository,
            WIPSequenceRepository wipSequenceRepository);
}
