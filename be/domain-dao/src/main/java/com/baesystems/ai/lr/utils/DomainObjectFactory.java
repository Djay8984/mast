package com.baesystems.ai.lr.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.dto.base.IdDto;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.ObjectFactory;

public class DomainObjectFactory<T> implements ObjectFactory<T>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DomainObjectFactory.class);
    private final Class<?> newTypeClass;

    public DomainObjectFactory(final Class<?> newTypeClass)
    {
        this.newTypeClass = newTypeClass;
    }

    private Map<String, Object> fetchScope(final String scopeName, final MappingContext mappingContext)
    {
        @SuppressWarnings("unchecked")
        Map<String, Object> scope = (Map<String, Object>) mappingContext.getProperty(scopeName);
        if (scope == null)
        {
            scope = new HashMap<String, Object>();
            mappingContext.setProperty(this.getClass().getName(), scope);
        }
        return scope;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T create(final Object source, final MappingContext mappingContext)
    {
        Object instance = null;
        if (source instanceof IdDto)
        {
            final String internalId = ((IdDto) source).getInternalId();
            if (internalId != null && internalId.length() > 0)
            {
                final Map<String, Object> scope = fetchScope(getScopeName(source), mappingContext);
                final String key = newTypeClass.getName() + "-" + internalId;
                instance = scope.get(key);
                if (instance == null)
                {
                    try
                    {
                        instance = newTypeClass.newInstance();
                    }
                    catch (final InstantiationException exception)
                    {
                        LOGGER.error("create - instantiationException", exception);
                    }
                    catch (final IllegalAccessException exception)
                    {
                        LOGGER.error("create - illegalAccessException", exception);
                    }

                    scope.put(key, instance);
                }
            }
        }
        else
        {
            try
            {
                instance = newTypeClass.newInstance();
            }
            catch (final InstantiationException exception)
            {
                LOGGER.error("create - instantiationException", exception);
            }
            catch (final IllegalAccessException exception)
            {
                LOGGER.error("create - illegalAccessException", exception);
            }
        }
        return (T) instance;
    }

    protected String getScopeName(final Object source)
    {
        return source.getClass().getName();
    }
}
