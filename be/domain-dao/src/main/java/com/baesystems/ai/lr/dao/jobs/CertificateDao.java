package com.baesystems.ai.lr.dao.jobs;


import com.baesystems.ai.lr.dto.jobs.CertificateDto;


public interface CertificateDao
{
    Integer getCyclePeriodicityOfCertificateType(Long certificateTypeId);
    
    CertificateDto saveCertificate(CertificateDto cert);
}
