package com.baesystems.ai.lr.dao.codicils;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;

public interface BaseActionableItemDao
{
    ActionableItemDto getActionableItem(Long id);

    Boolean actionableItemExists(Long actionableItemId);

    void deleteActionableItem(Long id);

    Integer countActionableItemsForDefect(Long defectId);


}
