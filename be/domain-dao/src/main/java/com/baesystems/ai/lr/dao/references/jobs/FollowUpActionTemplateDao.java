package com.baesystems.ai.lr.dao.references.jobs;

import com.baesystems.ai.lr.dto.references.FollowUpActionTemplateDto;

public interface FollowUpActionTemplateDao
{

    FollowUpActionTemplateDto getTemplate(Long templateId);

}
