package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.mast.repositories.AssetCustomerFunctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "AssetCustomerFunctionDao")
public class AssetCustomerFunctionDaoImpl implements AssetCustomerFunctionDao
{
    @Autowired
    private AssetCustomerFunctionRepository assetCustomerFunctionRepository;

    @Transactional
    public void hardDelete(final Long doID)
    {
        this.assetCustomerFunctionRepository.hardDelete(doID);
    }
}
