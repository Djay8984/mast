package com.baesystems.ai.lr.dao.codicils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.repositories.WIPCodicilRepository;

@Repository("WIPCodicilDao")
public class WIPCodicilDaoImpl implements WIPCodicilDao
{
    @Autowired
    private WIPCodicilRepository wipCodicilRepository;

    @Override
    @Transactional
    public Long getParentId(final Long wipId)
    {
        return this.wipCodicilRepository.findParentId(wipId);
    }
}
