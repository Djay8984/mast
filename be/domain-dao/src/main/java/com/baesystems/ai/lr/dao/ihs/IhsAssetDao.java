package com.baesystems.ai.lr.dao.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;

public interface IhsAssetDao
{
    IhsAssetDto getIhsAsset(Long imoNumber);
}
