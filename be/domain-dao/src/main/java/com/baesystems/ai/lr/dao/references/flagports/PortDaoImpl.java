package com.baesystems.ai.lr.dao.references.flagports;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.PortOfRegistryDO;
import com.baesystems.ai.lr.domain.mast.repositories.PortOfRegistryRepository;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryPageResourceDto;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "PortDao")
public class PortDaoImpl implements PortDao
{
    private static final Logger LOG = LoggerFactory.getLogger(PortDaoImpl.class);

    @Autowired
    private PortOfRegistryRepository portOfRegistryRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private final MapperFacade mapper;

    public PortDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(PortOfRegistryDO.class, PortOfRegistryDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public PortOfRegistryDto getPortOfRegistry(final Long portOfRegistryId)
    {
        LOG.debug("Retrieving Port of Registry with ID {}", portOfRegistryId);

        final PortOfRegistryDO portOfRegistry = portOfRegistryRepository.findOne(portOfRegistryId);

        return portOfRegistry == null ? null : mapper.map(portOfRegistry, PortOfRegistryDto.class);
    }

    @Override
    public PageResource<PortOfRegistryDto> findAll(final Pageable pageSpecification)
    {
        final Page<PortOfRegistryDO> flagData = portOfRegistryRepository.findAll(pageSpecification);
        return mapPageDoToPageDto(flagData);
    }

    @Override
    public PageResource<PortOfRegistryDto> findAll(final Pageable pageSpecification, final Long flagStateId)
    {
        final Page<PortOfRegistryDO> flagData = portOfRegistryRepository.findAll(pageSpecification, flagStateId);
        return mapPageDoToPageDto(flagData);
    }

    private PageResource<PortOfRegistryDto> mapPageDoToPageDto(final Page<PortOfRegistryDO> pageData)
    {
        final Page<PortOfRegistryDto> result = pageData.map(new Converter<PortOfRegistryDO, PortOfRegistryDto>()
        {
            @Override
            public PortOfRegistryDto convert(final PortOfRegistryDO port)
            {
                return mapper.map(port, PortOfRegistryDto.class);
            }
        });

        return this.pageFactory.createPageResource(result, PortOfRegistryPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<PortOfRegistryDto> getPorts()
    {
        LOG.debug("Retrieving all Ports");

        final List<PortOfRegistryDO> ports = portOfRegistryRepository.findAll();
        return mapper.mapAsList(ports, PortOfRegistryDto.class);
    }
}
