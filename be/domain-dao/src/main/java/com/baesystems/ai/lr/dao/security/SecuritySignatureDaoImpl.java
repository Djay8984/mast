package com.baesystems.ai.lr.dao.security;

import java.util.List;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.security.entities.SecuritySignatureDO;
import com.baesystems.ai.lr.domain.security.repositories.SecuritySignatureRepository;
import com.baesystems.ai.lr.dto.security.SecureSignatureDto;

@Repository
@CacheConfig(cacheNames = "SecuritySignatureDto")
public class SecuritySignatureDaoImpl implements SecuritySignatureDao
{
    @Autowired
    private SecuritySignatureRepository signatureRepository;

    private final MapperFacade mapper;

    public SecuritySignatureDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(SecuritySignatureDO.class, SecureSignatureDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<SecureSignatureDto> getSecuritySignatures()
    {
        final List<SecuritySignatureDO> signatures = signatureRepository.findByEnabled(Boolean.TRUE);
        return mapper.mapAsList(signatures, SecureSignatureDto.class);
    }
}
