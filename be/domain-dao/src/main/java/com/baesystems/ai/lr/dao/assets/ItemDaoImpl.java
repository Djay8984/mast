package com.baesystems.ai.lr.dao.assets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.assets.BaseItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRelationshipPersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRelationshipRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRelationshipRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedAssetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.ItemBaseDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.VersioningHelper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "ItemDao")
@SuppressWarnings({"PMD.GodClass", "PMD.TooManyMethods"})
public class ItemDaoImpl implements ItemDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ItemDaoImpl.class);

    private static final List<Long> NULL_LIST = null;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemRelationshipPersistRepository itemRelationshipPersistRepository;

    @Autowired
    private VersionedAssetRepository versionedAssetRepository;

    @Autowired
    private VersionedItemRepository versionedItemRepository;

    // @Autowired
    // private VersionedItemPersistRepository versionedItemPersistRepository;

    @Autowired
    private ItemRelationshipRepository itemRelationshipRepository;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ItemTypeRepository itemTypeRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private AttributeTypeRepository attributeTypeRepository;

    @Autowired
    private ItemTypeRelationshipRepository itemTypeRelationshipRepository;

    @Autowired
    private VersioningHelper versioningHelper;

    private final MapperFacade mapper;

    private static final long THE_NUMBER_OF_ALLOWED_PARENTAL_RELATIONSHIPS = 1L;

    public ItemDaoImpl()
    {
        super();
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    @Transactional
    @Override
    public Boolean itemExistsForAsset(final Long assetId, final Long itemId)
    {
        LOG.debug(String.format("Verifying that Item %d exists for asset %d", itemId, assetId));
        return this.versionedItemRepository.itemExistsForAsset(itemId, assetId);
    }

    @Transactional
    @Override
    public Boolean itemExists(final VersionedItemPK itemPK)
    {
        return this.versionedItemRepository.exists(itemPK);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean itemExists(final Long itemId)
    {
        return this.versionedItemRepository.exists(new VersionedItemPK(itemId, 1L, 1L));
    }

    @Override
    @Transactional
    public LazyItemDto getItemWithoutRelationships(final VersionedItemPK versionedItemPK)
    {
        final Long itemId = versionedItemPK.getId();
        final Long assetId = versionedItemPK.getAssetId();
        final Long versionId = versionedItemPK.getAssetVersionId();

        LOG.debug(String.format("Retrieving Item (without relationships) for ID %d asset version %d", itemId, versionId));
        final VersionedItemDO itemDO = this.versionedItemRepository.findOne(itemId, assetId, versionId);

        return this.mapper.map(itemDO, LazyItemDto.class);
    }

    @Override
    @Transactional
    public LazyItemDto getItem(final VersionedItemPK versionedItemPK)
    {
        final Long itemId = versionedItemPK.getId();
        final Long assetId = versionedItemPK.getAssetId();
        final Long versionId = versionedItemPK.getAssetVersionId();

        LOG.debug(String.format("Retrieving Item for ID %d asset version %d", itemId, versionId));

        final VersionedItemDO itemDO = this.versionedItemRepository.findOne(itemId, assetId, versionId);
        final LazyItemDto lazyItemDto = this.mapper.map(itemDO, LazyItemDto.class);

        decorateWithParentInformation(assetId, versionId, lazyItemDto);
        decorateRelationships(lazyItemDto, assetId, versionId, false, false);
        return lazyItemDto;
    }

    /**
     * Decorates the provided DTO with the version specific parent relationship
     *
     * @param assetId
     * @param versionId
     * @param itemToPopulate
     */
    private void decorateWithParentInformation(final Long assetId, final Long versionId, final ItemBaseDto itemToPopulate)
    {
        final Long itemId = itemToPopulate.getId();
        // Get the parent item.
        final List<ItemRelationshipPersistDO> parentRelationships = this.itemRelationshipPersistRepository
                .getInverseRelationships(itemId, assetId, versionId, RelationshipType.IS_PART_OF.getValue());

        if (parentRelationships.size() > THE_NUMBER_OF_ALLOWED_PARENTAL_RELATIONSHIPS)
        {
            throw new MastSystemException(String.format(ExceptionMessagesUtils.ITEM_DEFINED_WITH_MULTIPLE_PARENTS, itemId, assetId, versionId));
        }
        else if (!parentRelationships.isEmpty())
        {
            // Set the parent information on the item
            final ItemRelationshipPersistDO parentalRelationship = parentRelationships.get(0);
            itemToPopulate.setParentItem(new LinkVersionedResource(parentalRelationship.getFromItemId(), parentalRelationship.getFromVersionId()));
        }
    }

    @Override
    @Transactional
    public ItemLightDto getItemLight(final VersionedItemPK versionedItemPK)
    {
        final Long itemId = versionedItemPK.getId();
        final Long assetId = versionedItemPK.getAssetId();
        final Long versionId = versionedItemPK.getAssetVersionId();
        LOG.debug(String.format("Retrieving Item for ID %d asset version %d", itemId, versionId));
        final VersionedItemDO itemDO = this.versionedItemRepository.findOne(itemId, assetId, versionId);

        return this.mapper.map(itemDO, ItemLightDto.class);
    }

    @Override
    @Transactional
    public LazyItemDto getRootItem(final Long assetId, final Long aVersionId, final Boolean allowDraft)
    {
        final Long versionId = this.versioningHelper.getVersion(assetId, aVersionId, allowDraft);
        LOG.debug(String.format("Retrieving Item for Asset ID %d", assetId));
        final ItemDO itemDO = this.itemRepository.getRootItem(assetId, versionId);
        final VersionedItemDO versionedItemDO = this.versionedItemRepository.findOne(itemDO.getId(), assetId, versionId);
        final LazyItemDto lazyItemDto = this.mapper.map(versionedItemDO, LazyItemDto.class);
        decorateRelationships(lazyItemDto, assetId, versionId, false, allowDraft);

        return lazyItemDto;
    }

    @Override
    @Transactional
    public ItemDto getItemWithFullHierarchy(final Long itemId)
    {
        LOG.debug(String.format("Retrieving Item for ID %d", itemId));
        final VersionedItemDO itemDO = this.versionedItemRepository.findOneUndeleted(itemId);
        return itemDO != null ? this.mapper.map(itemDO, ItemDto.class) : null;
    }

    @Transactional
    @Override
    public Long getItemType(final Long itemId)
    {
        LOG.debug(String.format("Getting itemType for item %d", itemId));
        return this.versionedItemRepository.getItemType(itemId);
    }

    @Transactional
    @Override
    public Boolean relationshipExistsForItem(final Long itemId, final Long relationshipId, final RelationshipType relationshipType)
    {
        LOG.debug("verifying item relationship with ID {} exists for Item {}", relationshipId, itemId);
        return this.itemRelationshipRepository.itemRelationshipExistsForItem(relationshipId, itemId, relationshipType.getValue());
    }

    @Transactional
    @Override
    public Boolean relationshipExists(final Long toItemId, final Long fromItemId, final Long relationshipTypeId)
    {
        return this.itemRelationshipRepository.relationshipExists(toItemId, fromItemId, relationshipTypeId);
    }

    @Transactional
    @Override
    public Boolean itemIsOfType(final Long itemId, final Long typeId)
    {
        LOG.debug("verifying item with ID {} is of type {}", itemId, typeId);
        return this.versionedItemRepository.isItemOfType(itemId, typeId);
    }

    @Transactional
    @Override
    public boolean isItemNameUnique(final String itemName, final Long parentId)
    {
        final Long assetId = getItemMeta(parentId).getAsset().getId();
        final VersionedItemPK parentPK = this.versioningHelper.getVersionedItemPK(parentId, assetId);
        final ItemDto parentItem = getFullItem(parentPK, false);

        return CollectionUtils.nullSafeStream(parentItem.getItems())
                .filter(child -> itemName.equals(child.getName()))
                .count() < 2;
    }

    /**
     * Get a list of the IDs of all the children (of all generations) of an Item.
     *
     * @return ID list
     */
    @Override
    @Transactional
    public List<Long> getItemChildIdList(final Long draftItemId)
    {
        final ItemMetaDto itemMeta = getItemMeta(draftItemId);
        final Long assetId = itemMeta.getAsset().getId();

        final Long version = this.assetDao.getDraftVersion(assetId);

        final VersionedItemPK versionedItemPK = new VersionedItemPK(draftItemId, assetId, version);
        final ItemDto fullItem = getFullItem(versionedItemPK, true);

        return getChildren(new ArrayList<>(), fullItem.getItems())
                .stream()
                .map(ItemDto::getId)
                .collect(Collectors.toList());
    }

    /**
     * Get a list of all the children (of all generations) of an Item.
     *
     * @return {@link ItemDto} list
     */
    @Override
    @Transactional
    public List<ItemDto> getItemChildList(final VersionedItemPK versionedItemPK)
    {
        final ItemDto fullItem = getFullItem(versionedItemPK, true);

        return getChildren(new ArrayList<>(), fullItem.getItems());
    }

    private List<ItemDto> getChildren(final List<ItemDto> childrenGathered, final List<ItemDto> subChildren)
    {
        if (subChildren != null)
        {
            subChildren
                    .stream()
                    .forEach(subChild ->
                    {
                        childrenGathered.add(subChild);
                        getChildren(childrenGathered, subChild.getItems());
                    });
        }

        return childrenGathered;
    }

    @Override
    @Transactional
    public List<Long> getItemIdsFromAssetAndType(final Long assetId, final List<Long> itemTypeIdList)
    {
        List<Long> resultList = null;
        if (itemTypeIdList != null)
        {
            resultList = this.versionedItemRepository.getItemIdsFromAssetAndType(assetId, itemTypeIdList);
        }
        return resultList;
    }

    @Transactional
    @Override
    public ItemMetaDto getItemMeta(final Long itemId)
    {
        return this.mapper.map(this.itemRepository.findOne(itemId), ItemMetaDto.class);
    }

    @Override
    @Transactional
    public Integer countChildItemsOfType(final Long parentId, final Long typeId)
    {
        final Long assetId = getItemMeta(parentId).getAsset().getId();
        final VersionedItemPK parentPK = this.versioningHelper.getVersionedItemPK(parentId, assetId);
        final ItemDto parentItem = getFullItem(parentPK, true);

        return (int) CollectionUtils.nullSafeStream(parentItem.getItems())
                .filter(child -> child.getItemType().getId().equals(typeId))
                .count();
    }

    /**
     * todo reimpl this with display order regarding versioning
     */
    @Transactional
    @Override
    public Integer getNextChildDisplayOrder(final Long parentItemId)
    {
        // Integer currentMaxDisplayOrder = this.versionedItemRepository.getMaximumChildDisplayOrder(parentItemId);
        // if (currentMaxDisplayOrder == null)
        // {
        // currentMaxDisplayOrder = -1;
        // }
        // return Integer.valueOf(currentMaxDisplayOrder.intValue() + 1);
        return parentItemId.intValue() + 1;
    }

    @Transactional
    @Override
    public Integer countItemsOfSpecificVersion(final Long assetId, final Long versionId)
    {
        return this.versionedItemRepository.countItemsOfSpecificVersion(assetId, versionId);
    }

    @Transactional
    @Override
    public ItemDto getFullItem(final VersionedItemPK versionedItemPK, final Boolean allowDraft)
    {
        final Long itemId = versionedItemPK.getId();
        final Long assetId = versionedItemPK.getAssetId();
        final Long versionId = this.versioningHelper.getVersion(versionedItemPK.getAssetId(), versionedItemPK.getAssetVersionId(), allowDraft);
        LOG.debug(String.format("Retrieving Item for ID %d asset version %d", itemId, versionId));

        final VersionedItemDO itemDO = this.versionedItemRepository.findOne(itemId, assetId, versionId);
        final ItemDto itemDto = this.mapper.map(itemDO, ItemDto.class);

        decorateWithParentInformation(assetId, versionId, itemDto);
        decorateRelationships(itemDto, assetId, versionId, true, allowDraft);

        return itemDto;
    }

    /**
     * Decorates an ItemDTO with its associated versioned relationships, this can eiether be a singleshot lazy Item or be part of implicit recursion
     * when combined with getFullItem()
     *
     * @param itemDto    the Item DTO to be decorated
     * @param assetId    the Asset ID
     * @param versionId  the Specific Version ID (can be draft)
     * @param recursive  whether or not to recursively call getFullItem() on the child Item to build a tree downwards
     * @param allowDraft If (and only if) recursive flag is set, propagate the include draft condition downwards though children
     */
    private void decorateRelationships(final ItemBaseDto itemDto, final Long assetId, final Long versionId, final Boolean recursive,
            final Boolean allowDraft)
    {
        final Long itemId = itemDto.getId();

        itemDto.setItems(new ArrayList<>());
        itemDto.setRelated(new ArrayList<>());

        final List<ItemRelationshipDO> childRelationships = this.itemRelationshipRepository
                .getItemRelationships(itemId, assetId, versionId,
                        RelationshipType.IS_PART_OF.getValue());

        final List<ItemRelationshipDO> lateralRelationships = this.itemRelationshipRepository
                .getItemRelationships(itemId, assetId, versionId,
                        RelationshipType.IS_RELATED_TO.getValue());

        CollectionUtils.nullSafeCollection(childRelationships)
                .forEach(childRelationship -> itemDto.getItems().add(
                        recursive
                                ? this.getFullItem(new VersionedItemPK(childRelationship.getToItem().getId(), assetId, versionId), allowDraft)
                                : new LinkVersionedResource(childRelationship.getToItem().getId(),
                                childRelationship.getToItem().getAssetVersionId())));

        CollectionUtils.nullSafeCollection(lateralRelationships)
                .forEach(lateralRelationship -> itemDto.getRelated().add(
                        new LinkVersionedResource(lateralRelationship.getToItem().getId(),
                                lateralRelationship.getToItem().getAssetVersionId())));

        itemDto.setReviewedItems(childRelationships.stream()
                .map(ItemRelationshipDO::getToItem)
                .filter(BaseItemDO::getReviewed)
                .count());
    }

    @Transactional
    @Override
    public PageResource<LazyItemDto> findItemsByAsset(final Long assetId, final Long aVersionId, final Boolean allowDraft, final Pageable pageable,
            final ItemQueryDto itemQueryDto)
    {
        LOG.debug(String.format("Finding Items for Asset %d with query", assetId));

        final Long versionId = Boolean.TRUE.equals(allowDraft)
                ? this.assetDao.getDraftVersion(assetId)
                : aVersionId != null
                ? aVersionId
                : this.assetDao.getPublishedVersion(assetId);

        final Long offset = (long) (pageable.getPageSize() * pageable.getPageNumber());
        final Long limit = (long) pageable.getPageSize();

        Page<VersionedItemDO> output = null;
        if (itemQueryDto == null)
        {
            final List<VersionedItemDO> list = this.versionedItemRepository.findItemsByAssetId(assetId, versionId, limit, offset);
            output = list != null ? new PageImpl<>(list) : null;
        }
        else
        {
            final List<VersionedItemDO> list = this.findItemsByAssetIdAndQueryParameters(assetId, versionId, itemQueryDto);
            output = list != null ? new PageImpl<>(list) : null;
        }

        final Page<LazyItemDto> result = this.mapItemDOPageToLazyItemDtoPage(output);
        result.forEach(lazyItemDto ->
        {
            decorateWithParentInformation(assetId, versionId, lazyItemDto);
            decorateRelationships(lazyItemDto, assetId, versionId, false, false);
        });
        return this.pageFactory.createPageResource(result, LazyItemPageResourceDto.class);
    }

    private List<VersionedItemDO> findItemsByAssetIdAndQueryParameters(final Long assetId, final Long versionId, final ItemQueryDto itemQueryDto)
    {
        List<VersionedItemDO> returnValue = null;
        List<Long> itemTypeIdsMatchingName = null;

        if (itemQueryDto.getAttributeName() != null)
        {
            final List<Long> attributeTypeIdsMatchingName = this.attributeTypeRepository.findIdsWithNameLike(itemQueryDto.getAttributeName());

            if (itemQueryDto.getAttributeTypeId() == null)
            {
                itemQueryDto.setAttributeTypeId(attributeTypeIdsMatchingName);
            }
            else
            {
                itemQueryDto.getAttributeTypeId().retainAll(attributeTypeIdsMatchingName);
            }
        }
        if (itemQueryDto.getItemTypeName() != null)
        {
            itemTypeIdsMatchingName = this.itemTypeRepository.findIdsWithNameLike(itemQueryDto.getItemTypeName());
            // if an item type name has been entered and the list come s back empty the query might still return results
            // if the item name is entered since the desired result is a match on item name OR item type name. However
            // passing null for both would not limit return everything (subject to other constrains) so this should only
            // be set to null when empty if there is an item name if not then the next if will stop the query and return
            // nothing and required.
            itemTypeIdsMatchingName = itemTypeIdsMatchingName.isEmpty() && itemQueryDto.getItemName() != null ? NULL_LIST : itemTypeIdsMatchingName;
        }
        if (!(itemTypeIdsMatchingName != null && itemTypeIdsMatchingName.isEmpty()
                || itemQueryDto.getAttributeTypeId() != null && itemQueryDto.getAttributeTypeId().isEmpty()))
        { // skip the query if the list were empty as there a no matches on the ands so there are no results.
            returnValue = this.versionedItemRepository
                    .findItemsByAssetId(assetId, versionId, itemTypeIdsMatchingName, itemQueryDto.getItemId(), itemQueryDto.getItemTypeId(),
                            itemQueryDto.getItemName());
        }

        return returnValue;
    }

    /**
     * This can take a null value if we want to construct an empty {@link Page<LazyItemDto>}, otherwise map the
     * {@link Page<T>} to a corresponding {@link Page<LazyItemDto>}
     *
     * @param {@link Page<T>} or null to represent an empty page
     * @return {@link Page<LazyItemDto>} the mapped itemDOPage
     */
    private <T extends BaseItemDO> Page<LazyItemDto> mapItemDOPageToLazyItemDtoPage(final Page<?> inputItemDOPage)
    {
        Page<LazyItemDto> result = null;
        if (inputItemDOPage == null)
        {
            result = new PageImpl<>(new ArrayList<>());
        }
        else
        {
            @SuppressWarnings("unchecked")
            final Page<T> itemDOPage = (Page<T>) inputItemDOPage;
            result = itemDOPage.map(itemDO -> this.mapper.map(itemDO, LazyItemDto.class));
        }
        return result;
    }

    @Override
    @Transactional
    public Boolean isProposedRelationshipValidForTypes(final Long proposedToItemTypeId, final Long proposedFromItemTypeId,
            final Integer assetCategoryId, final Long relationshipTypeId)
    {
        return this.itemTypeRelationshipRepository.isProposedRelationshipValidForTypes(proposedToItemTypeId, proposedFromItemTypeId, assetCategoryId,
                relationshipTypeId);
    }

    @Override
    @Transactional
    public ItemRelationshipDO getItemRelationship(final Long relationshipId)
    {
        return this.itemRelationshipRepository.findOne(relationshipId);
    }

    @Override
    @Transactional
    public Optional<Long> getItemRelationshipId(final Long assetId, final Long fromItemId, final Long fromVersion, final Long toItemId,
            final Long toVersion)
    {
        return Optional.ofNullable(this.itemRelationshipRepository.findRelationshipId(assetId, fromItemId, fromVersion, toItemId, toVersion));
    }

    @Override
    @Transactional
    public final AssetModelDto getAssetModel(final Long assetId, final Long versionId)
    {
        final Long verifiedVersionId = versionId != null ? versionId : this.assetDao.getPublishedVersion(assetId);

        final VersionedAssetDO assetDO = this.versionedAssetRepository.findOne(new VersionedAssetPK(assetId, verifiedVersionId));
        final VersionedItemPK itemPK = new VersionedItemPK(assetDO.getAssetItem().getId(), assetId, verifiedVersionId);
        final ItemDto item = getFullItem(itemPK, false);

        final AssetModelDto assetModelDto = this.mapper.map(assetDO, AssetModelDto.class);
        assetModelDto.setItems(Collections.singletonList(item));

        return assetModelDto;
    }
}
