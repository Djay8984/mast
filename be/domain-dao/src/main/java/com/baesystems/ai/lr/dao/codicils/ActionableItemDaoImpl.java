package com.baesystems.ai.lr.dao.codicils;

import static com.baesystems.ai.lr.domain.mast.repositories.CodicilRepositoryHelper.ASSET_COUNT_COLUMN_NAME;
import static com.baesystems.ai.lr.domain.mast.repositories.CodicilRepositoryHelper.TEMPLATE_COLUMN_NAME;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.ActionableItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.ActionableItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPActionableItemRepository;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.CodicilStatusCriteria;
import com.baesystems.ai.lr.utils.CodicilUtils;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository("ActionableItemDao")
public class ActionableItemDaoImpl extends AbstractLockingDaoImpl<ActionableItemDO> implements ActionableItemDao
{
    @Autowired
    private ActionableItemRepository actionableItemRepository;

    @Autowired
    private WIPActionableItemRepository wipActionableItemRepository;

    @Autowired
    private PageResourceFactory pageResourceFactory;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private SequenceDao sequenceDao;

    public static final Logger LOG = LoggerFactory.getLogger(ActionableItemDaoImpl.class);

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(ActionableItemDO.class, ActionableItemDto.class)
                .customize(new StaleObjectMapper<ActionableItemDO, ActionableItemDto>())
                .byDefault().toClassMap());
    }

    public ActionableItemDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public ActionableItemDto getActionableItem(final Long id)
    {
        LOG.debug("Receiving actionable item with id {}", id);
        final ActionableItemDO actionableItemDO = this.actionableItemRepository.findActionableItem(id);
        ActionableItemDto actionableItemDto = null;
        if (actionableItemDO != null)
        {
            actionableItemDto = getMapper().map(actionableItemDO, ActionableItemDto.class);
        }
        return actionableItemDto;
    }

    @Override
    @Transactional
    public Boolean actionableItemExistsForAsset(final Long assetId, final Long actionableItemId)
    {
        LOG.debug("Verifying if Actionable Item {} exists for Asset {}", actionableItemId, assetId);
        return this.actionableItemRepository.actionableItemExistsForAsset(assetId, actionableItemId) != null;
    }

    @Override
    @Transactional
    public PageResource<ActionableItemDto> getActionableItemsForAssets(final Pageable pageSpecification, final Long assetId,
            final Long inputStatusId)
    {
        LOG.debug("Retrieving Actionable Items for Asset {}", assetId);
        Boolean isOpen = null;
        Long statusId = inputStatusId;

        if (CodicilStatusType.AI_OPEN.getValue().equals(statusId))
        {
            isOpen = true;
        }
        else if (CodicilStatusType.AI_INACTIVE.getValue().equals(statusId))
        {
            statusId = CodicilStatusType.AI_OPEN.getValue();
            isOpen = false;
        }

        final Page<ActionableItemDO> output = this.actionableItemRepository.findActionableItemsForAsset(pageSpecification, assetId, statusId,
                new Date(), isOpen);
        final Page<ActionableItemDto> result = output.map(new Converter<ActionableItemDO, ActionableItemDto>()
        {
            @Override
            public ActionableItemDto convert(final ActionableItemDO actionableItemDO)
            {
                return getMapper().map(actionableItemDO, ActionableItemDto.class);
            }
        });

        return this.pageResourceFactory.createPageResource(result, ActionableItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public Integer countActionableItemsForDefect(final Long defectId)
    {
        return this.actionableItemRepository.countCoursesOfActionForDefect(defectId, CodicilStatusType.getActionableItemStatusesForCourseOfAction());
    }

    @Override
    @Transactional
    public ActionableItemDto createActionableItem(final Long assetId, final ActionableItemDto actionableItemDto)
    {
        LOG.debug("Creating actionable item for asset {}", assetId);
        ActionableItemDO actionableItemDO = getMapper().map(actionableItemDto, ActionableItemDO.class);
        actionableItemDO.setAsset(new AssetDO());
        actionableItemDO.getAsset().setId(assetId);
        actionableItemDO.setSequenceNumber(
                this.sequenceDao.getNextAssetSpecificSequenceValue(assetId, false, this.actionableItemRepository, this.wipActionableItemRepository));
        actionableItemDO = this.actionableItemRepository.save(actionableItemDO);

        ActionableItemDto newActionableItemDto = null;
        if (actionableItemDO != null)
        {
            newActionableItemDto = getMapper().map(actionableItemDO, ActionableItemDto.class);
        }
        return newActionableItemDto;

    }

    @Override
    @Transactional
    public ActionableItemDto updateActionableItem(final Long assetId, final ActionableItemDto actionableItemDto)
    {
        LOG.debug("Updating actionable item with ID {}", actionableItemDto.getId());
        ActionableItemDO actionableItemDO = getMapper().map(actionableItemDto, ActionableItemDO.class);
        actionableItemDO.setAsset(new AssetDO());
        actionableItemDO.getAsset().setId(assetId);
        actionableItemDO = this.actionableItemRepository.merge(actionableItemDO);
        return actionableItemDO != null ? getMapper().map(actionableItemDO, ActionableItemDto.class) : null;
    }

    @Override
    @Transactional
    public Boolean actionableItemExists(final Long actionableItemId)
    {
        return this.actionableItemRepository.exists(actionableItemId);
    }

    /**
     * Count the number of assets that have actionable items created using the given template IDs. Only assets with the
     * given lifecycle statuses will be counted.
     *
     * @param templateIds
     * @param assetLifecycleStatuses
     * @return {@link TemplateAssetCountDto} containing a HashMap which maps Template IDs to their associated Asset
     *         Counts.
     */
    @Override
    @Transactional
    public TemplateAssetCountDto countAssetsWithActionableItemsForTemplate(final List<Long> templateIds, final List<Long> assetLifecycleStatuses)
    {
        LOG.debug("Counting the number of Assets with Actionable Items for templates: {} and Asset Lifecycle Statuses: {}",
                templateIds, assetLifecycleStatuses);

        // Key = Column Name, Value = Data
        final List<Map<String, Long>> rowData = this.actionableItemRepository.countAssetsWithActionableItemsForTemplate(templateIds, assetLifecycleStatuses);

        // Key = Template ID, Value = Asset Count
        final Map<Long, Integer> templateCountMap = CodicilUtils.populateTemplateCountMap(templateIds, rowData,
                TEMPLATE_COLUMN_NAME, ASSET_COUNT_COLUMN_NAME);

        final TemplateAssetCountDto assetCounts = new TemplateAssetCountDto();
        assetCounts.setCounts(templateCountMap);

        return assetCounts;
    }

    @Override
    @Transactional
    public void deleteActionableItem(final Long id)
    {
        this.actionableItemRepository.delete(id);
    }

    /**
     * Gets all Actionable Items for an asset with a query
     *
     * @param pageable, page specification
     * @param assetId, Asset ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All Actionable Items for an asset in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<ActionableItemDto> findAllForAsset(final Pageable pageable, final Long assetId,
            final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<ActionableItemDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = this.actionableItemRepository.findAll(pageable, assetId);
        }
        else
        {
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);

            final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(codicilDefectQueryDto.getStatusList(),
                    CodicilType.AI);

            resultPage = this.actionableItemRepository.findAll(pageable, assetId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilStatusCriteria.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(), codicilDefectQueryDto.getDueDateMax(), new Date(),
                    codicilStatusCriteria.getOpen(),
                    CodicilStatusType.AI_OPEN.getValue());
        }
        final Page<ActionableItemDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), ActionableItemDto.class);
        return this.pageResourceFactory.createPageResource(result, ActionableItemPageResourceDto.class);
    }

    @Override
    public LockingRepository<ActionableItemDO> getLockingRepository()
    {
        return this.actionableItemRepository;
    }

    @Override
    @Transactional
    public List<ActionableItemDto> getActionableItemsForAService(final Long serviceId)
    {
        final List<ActionableItemDO> actionableItemDos = this.actionableItemRepository.findActionableItemsForAService(serviceId);
        return getMapper().mapAsList(actionableItemDos, ActionableItemDto.class);
    }
}
