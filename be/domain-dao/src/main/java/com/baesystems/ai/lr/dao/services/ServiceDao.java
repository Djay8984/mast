package com.baesystems.ai.lr.dao.services;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface ServiceDao extends LockingDao<ScheduledServiceDO>
{
    List<ReferenceDataDto> getSchedulingTypesForServiceCatalogue(Long serviceCatalogueId);

    List<ScheduledServiceDto> getServicesForAsset(Long assetId, List<Long> statusIds, List<Long> productFamilyIds, Date dueDateMin, Date dueDateMax);

    List<ScheduledServiceDto> getServicesForProduct(Long productId);

    List<ScheduledServiceDto> getServicesForAsset(Long assetId);

    List<ScheduledServiceDto> getServicesForAssetByStatus(Long assetId, List<Long> statusIds);

    ScheduledServiceDto getService(Long serviceId);

    ScheduledServiceDto getService(Long serviceId, Long assetId);

    ServiceCatalogueDto getServiceCatalogue(Long serviceCatalogueId);

    ServiceCatalogueDO getServiceCatalogueDO(Long serviceCatalogueId);

    ServiceCatalogueDto getServiceCatalogueByCategoryAndSurveyType(String categoryLetter, String surveyType);

    ScheduledServiceDto updateService(ScheduledServiceDto serviceDto);

    ScheduledServiceDto saveService(ScheduledServiceDto newService);

    void deleteService(Long serviceId);

    ServiceCatalogueDto getServiceCatalogueByCodeAndSurveyType(String categoryCode, String surveyType);

    ServiceCatalogueDto getUniqueServiceCatalogueByCode(String serviceCatalogueCode);

    void saveOrUpdateServices(List<ScheduledServiceDto> services);

    Boolean serviceExistsForAsset(Long assetId, Long serviceId);

    String getServiceCodeFromScheduledService(Long scheduledServiceId);

    boolean doAnyItemsHaveOpenServices(List<Long> itemIdsToCheck);
}
