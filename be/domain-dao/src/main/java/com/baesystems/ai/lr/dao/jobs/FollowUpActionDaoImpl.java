package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.FollowUpActionDO;
import com.baesystems.ai.lr.domain.mast.repositories.FollowUpActionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "FollowUpActionDao")
public class FollowUpActionDaoImpl extends AbstractLockingDaoImpl<FollowUpActionDO> implements FollowUpActionDao
{
    public static final Logger LOG = LoggerFactory.getLogger(FollowUpActionDaoImpl.class);

    private static final DefaultMapperFactory FACTORY;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private FollowUpActionRepository followUpActionRepository;

    @Autowired
    private LrEmployeeRepository employeeRepository;

    @Autowired
    private ServiceUtils serviceUtils;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.classMap(FollowUpActionDO.class, FollowUpActionDto.class)
        .customize(new StaleObjectMapper<FollowUpActionDO, FollowUpActionDto>())
        .byDefault().register();
    }

    public FollowUpActionDaoImpl()
    {
        super(FACTORY);
    }

    class FollowUpActionConverter implements Converter<FollowUpActionDO, FollowUpActionDto>
    {
        @Override
        public FollowUpActionDto convert(final FollowUpActionDO followUpActionDO)
        {
            return getMapper().map(followUpActionDO, FollowUpActionDto.class);
        }
    }

    @Override
    @Transactional
    public PageResource<FollowUpActionDto> getFollowUpActionsForJob(final Pageable pageable, final Long jobId)
            throws BadPageRequestException
    {
        LOG.debug("Retrieving Follow Up Actions for Job: {}", jobId);
        final Page<FollowUpActionDO> output = this.followUpActionRepository.findFollowUpActionByJob(pageable, jobId);
        final Page<FollowUpActionDto> result = output == null ? null : output.map(new FollowUpActionConverter());

        return this.pageFactory.createPageResource(result, FollowUpActionPageResourceDto.class);
    }

    @Override
    @Transactional
    public FollowUpActionDto saveFollowUpAction(final FollowUpActionDto followUpAction, final ReportDto report)
    {
        final FollowUpActionDO newFUA = getMapper().map(followUpAction, FollowUpActionDO.class);
        return getMapper().map(this.followUpActionRepository.save(newFUA), FollowUpActionDto.class);
    }

    @Override
    @Transactional
    public Boolean followUpActionExistsForJob(final Long jobId, final Long followUpActionId)
    {
        final Integer existsCount = this.followUpActionRepository.followUpActionExistsForJob(jobId, followUpActionId);
        return existsCount > 0;
    }

    @Override
    @Transactional
    public FollowUpActionDto updateFollowUpActionStatus(final Long followUpActionId, final LinkResource status)
    {
        FollowUpActionDO fuaDO = this.followUpActionRepository.findOne(followUpActionId);
        fuaDO.getFollowUpActionStatus().setId(status.getId());
        fuaDO.setCompletedBy(new LinkDO());
        fuaDO.getCompletedBy().setId(this.employeeRepository.findByFullName(serviceUtils.getSecurityContextPrincipal()).getId());
        fuaDO.setCompletionDate(new Date());
        fuaDO = this.followUpActionRepository.merge(fuaDO);
        return fuaDO == null ? null : getMapper().map(fuaDO, FollowUpActionDto.class);
    }

    @Override
    public LockingRepository<FollowUpActionDO> getLockingRepository()
    {
        return this.followUpActionRepository;
    }
}
