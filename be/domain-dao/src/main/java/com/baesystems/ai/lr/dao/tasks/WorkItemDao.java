package com.baesystems.ai.lr.dao.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

public interface WorkItemDao extends BaseWorkItemDao
{
    List<WorkItemLightDto> getLightTasksForAsset(Long assetId);

    List<WorkItemLightDto> getTasksForService(Long scheduledServiceId);

    WorkItemConditionalAttributeDto getWorkItemConditionalAttribute(String serviceCode, Long itemId, Long workItemActionId);
}
