package com.baesystems.ai.lr.utils;

import java.util.Date;

import com.baesystems.ai.lr.utils.RepaginationUtils.Relationship;

public final class ComparatorUtils
{
    private ComparatorUtils()
    {
        // no constructor
    }

    public static <S> Relationship compareDates(final S obj1, final S obj2)
    {
        Relationship returnValue = null;

        final Date date1 = Date.class.cast(obj1);
        final Date date2 = Date.class.cast(obj2);

        if (date1.before(date2))
        {
            returnValue = Relationship.LT;
        }
        else if (date1.after(date2))
        {
            returnValue = Relationship.GT;
        }
        else
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    public static <S> Relationship compareIntegers(final S obj1, final S obj2)
    {
        Relationship returnValue = null;

        final Integer value1 = Integer.class.cast(obj1);
        final Integer value2 = Integer.class.cast(obj2);

        if (value1 < value2)
        {
            returnValue = Relationship.LT;
        }
        else if (value1 > value2)
        {
            returnValue = Relationship.GT;
        }
        else
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    public static <S> Relationship compareLongs(final S obj1, final S obj2)
    {
        Relationship returnValue = null;

        final Long value1 = Long.class.cast(obj1);
        final Long value2 = Long.class.cast(obj2);

        if (value1 < value2)
        {
            returnValue = Relationship.LT;
        }
        else if (value1 > value2)
        {
            returnValue = Relationship.GT;
        }
        else
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    public static <S> Relationship compareFloats(final S obj1, final S obj2)
    {
        Relationship returnValue = null;

        final Float value1 = Float.class.cast(obj1);
        final Float value2 = Float.class.cast(obj2);

        if (value1 < value2)
        {
            returnValue = Relationship.LT;
        }
        else if (value1 > value2)
        {
            returnValue = Relationship.GT;
        }
        else
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    public static <S> Relationship compareDoubles(final S obj1, final S obj2)
    {
        Relationship returnValue = null;

        final Double value1 = Double.class.cast(obj1);
        final Double value2 = Double.class.cast(obj2);

        if (value1 < value2)
        {
            returnValue = Relationship.LT;
        }
        else if (value1 > value2)
        {
            returnValue = Relationship.GT;
        }
        else
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    public static <S> Relationship compareStrings(final S obj1, final S obj2, final Boolean ignoreCase)
    {
        Relationship returnValue = null;

        final String string1 = String.class.cast(obj1);
        final String string2 = String.class.cast(obj2);

        if (ignoreCase == null || ignoreCase)
        {
            if (string1.compareToIgnoreCase(string2) < 0)
            {
                returnValue = Relationship.LT;
            }
            else if (string1.compareToIgnoreCase(string2) > 0)
            {
                returnValue = Relationship.GT;
            }
            else
            {
                returnValue = Relationship.EQ;
            }
        }
        else
        {
            if (string1.compareTo(string2) < 0)
            {
                returnValue = Relationship.LT;
            }
            else if (string1.compareTo(string2) > 0)
            {
                returnValue = Relationship.GT;
            }
            else
            {
                returnValue = Relationship.EQ;
            }
        }

        return returnValue;
    }
}
