package com.baesystems.ai.lr.dao.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsHistDto;

public interface IhsHistDao
{
    IhsHistDto getIhsHist(Long imoNumber);
}
