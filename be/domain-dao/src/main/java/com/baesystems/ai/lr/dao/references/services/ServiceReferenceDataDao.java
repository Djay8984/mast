package com.baesystems.ai.lr.dao.references.services;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ProductTypeDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SchedulingTypeDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import com.baesystems.ai.lr.dto.references.ServiceGroupDto;
import com.baesystems.ai.lr.enums.ProductType;

public interface ServiceReferenceDataDao
{
    List<ProductCatalogueDto> getProductCatalogues();

    List<ProductCatalogueDto> getProductCataloguesForAsset(Long assetId, List<ProductType> productTypes);

    List<ReferenceDataDto> getServiceStatuses();

    ReferenceDataDto getServiceStatus(Long serviceStatusId);

    List<ReferenceDataDto> getServiceTypes();

    List<ProductCatalogueDO> getProductCatalogueDOsForAsset(Long assetId, List<ProductType> productTypes);

    List<ProductTypeDto> getProductTypes();

    List<ProductGroupDto> getProductGroups();

    List<ServiceCatalogueDto> getServiceCatalogues();

    List<ProductTypeExtendedDto> getProductModel();

    List<ReferenceDataDto> getSchedulingRegimes();

    List<SchedulingTypeDto> getSchedulingTypes();

    List<ServiceGroupDto> getServiceGroups();

    List<ReferenceDataDto> getServiceRulesets();

    List<ReferenceDataDto> getAssignedSchedulingTypes();

    List<ReferenceDataDto> getAssociatedSchedulingTypes();

    List<ReferenceDataDto> getHarmonisationTypes();

    List<ReferenceDataDto> getSchedulingDueTypes();

    ServiceCreditStatusDto getServiceCreditStatus(Long serviceCreditStatusId);

    List<ServiceCreditStatusDto> getServiceCreditStatuses();

    ServiceCatalogueDto getServiceCatalogue(Long catalogueId);
}
