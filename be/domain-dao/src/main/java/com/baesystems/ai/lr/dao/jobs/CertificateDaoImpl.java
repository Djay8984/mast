package com.baesystems.ai.lr.dao.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.jobs.CertificateDO;
import com.baesystems.ai.lr.domain.mast.repositories.CertificateRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CertificateTypeRepository;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CertificateDao")
public class CertificateDaoImpl implements CertificateDao
{
    private static final Logger LOG = LoggerFactory.getLogger(CertificateDaoImpl.class);

    @Autowired
    private CertificateTypeRepository certificateTypeRepository;

    @Autowired
    private CertificateRepository certificateRepository;

    private final MapperFacade mapper;

    public CertificateDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(CertificateDO.class, CertificateDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public Integer getCyclePeriodicityOfCertificateType(final Long certificateTypeId)
    {
        LOG.debug("Retrieving cycle periodicity for certificate type with ID {}.", certificateTypeId);

        return this.certificateTypeRepository.findCyclePeriodicityOfCertificateType(certificateTypeId);
    }

    @Override
    @Transactional
    public CertificateDto saveCertificate(final CertificateDto certificateDto)
    {
        CertificateDO certificate = this.mapper.map(certificateDto, CertificateDO.class);

        certificate = this.certificateRepository.merge(certificate);

        return this.mapper.map(certificate, CertificateDto.class);
    }
}
