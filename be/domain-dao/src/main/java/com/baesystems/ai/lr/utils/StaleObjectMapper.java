package com.baesystems.ai.lr.utils;

import org.apache.commons.codec.digest.DigestUtils;

import com.baesystems.ai.lr.domain.entities.BaseDO;
import com.baesystems.ai.lr.dto.StaleObject;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

public class StaleObjectMapper<A extends BaseDO, B extends StaleObject> extends CustomMapper<A, B>
{
  @Override
  public void mapAtoB(final A unusedDO, final B resultingDto, final MappingContext context)
  {
      if (resultingDto != null)
      {
          final String newStalenessHash = DigestUtils.sha1Hex(resultingDto.getStringRepresentationForHash());
          resultingDto.setStalenessHash(newStalenessHash);
      }
  }
}
