package com.baesystems.ai.lr.dao.defects;

import com.baesystems.ai.lr.dto.defects.DefectDto;

public interface BaseDefectDao
{
    DefectDto createDefect(DefectDto defectDto);

    DefectDto getDefect(Long defectId);

    DefectDto updateDefect(DefectDto defectDto);

    Boolean closeDefect(Long defectId);
}
