package com.baesystems.ai.lr.dao.assets;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface AttributeDao
{
    Boolean attributeExistsForItemAndAsset(Long attributeId, Long itemId, Long assetId);

    AttributeDto updateAttribute(Long itemId, AttributeDto attributeDto);

    AttributeDto getAttribute(Long id);

    void deleteAttribute(Long attributeId) throws MastBusinessException;

    Integer countAttributesByItemAndAttributeType(Long itemId, Long attributeTypeId, Long versionId);

    AttributeDto createAttribute(AttributeDto attributeDto, VersionedItemPK itemPK);

    PageResource<AttributeDto> findAttributesByItem(Long itemId, Pageable pageSpecification);
    
    List<AttributeDto> findAttributesByItem(Long itemId) throws BadPageRequestException;

    List<Long> getMandatoryAttributeTypeIdsForAnItemTypeId(Long itemTypeId);

    List<AttributeDto> findAttributesWithNamingOrderForItem(Long itemId);
}
