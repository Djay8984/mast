package com.baesystems.ai.lr.dao.assets;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.HardDeletingDao;
import com.baesystems.ai.lr.utils.LockingDao;

@SuppressWarnings("PMD.TooManyMethods")
public interface AssetDao extends LockingDao<VersionedAssetDO>, HardDeletingDao
{
    AssetMetaDto createAssetMeta();

    AssetMetaDto updateAssetMeta(AssetMetaDto assetMetaDto);

    Boolean assetExists(Long assetId);

    <T extends AssetLightDto> T getAsset(Long assetId, Long versionId, Class<T> returnClass);

    AssetLightDto getAssetByImoNumber(Long imoNumber);

    PageResource<AssetLightDto> findAll(Pageable pageSpecification);

    void deleteAsset(VersionedAssetPK assetPK);

    void updateHullIndicator(Long assetId, Integer hullIndicator);

    Integer countMatchingItemIds(List<Long> itemIds);

    Long findAssetIdByImoOrBuilderAndYard(Long ihsAssetId, String builder, String yardNumber);

    PageResource<AssetLightDto> findAll(Pageable pageable, AssetQueryDto query);

    PageResource<AssetLightDto> findAll(Pageable pageable, AbstractQueryDto query);

    Long getAssetCategory(Long assetId);

    AssetLightDto createAsset(AssetLightDto assetLightDto);

    AssetLightDto updateAsset(AssetLightDto assetLightDto);

    void setRootItemOnAsset(AssetLightDto assetLightDto, Long rootItemId);

    Date getAssetHarmonisationDate(Long assetId);

    AssetMetaDto getAssetMeta(Long assetId);

    Long getPublishedVersion(Long assetId);

    Long getDraftVersion(Long assetId);

    List<AssetMetaDto> getCheckedOutAssets(String userId);

    PageResource<MultiAssetDto> findAllFromBothDatabases(Pageable pageable, AbstractQueryDto query) throws BadPageRequestException;
}
