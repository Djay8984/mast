package com.baesystems.ai.lr.dao.reports;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.reports.ReportDO;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface ReportDao extends LockingDao<ReportDO>
{
    ReportDto getReport(Long reportId);

    List<ReportLightDto> getReports(Long jobId);

    Long getNextReportVersion(Long jobId, Long reportTypeId);

    ReportDto saveReport(ReportDto reportDto);

    ReportDto getReport(Long jobId, Long typeId, Long version);

    Boolean reportExistsForJob(Long reportId, Long jobId);
}
