package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.jobs.WipCertificateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CertificateActionDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WipCertificateRepository;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.jobs.CertificatePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPCertificateDao")
public class WIPCertificateDaoImpl extends AbstractLockingDaoImpl<WipCertificateDO> implements WIPCertificateDao
{
    private static final Logger LOG = LoggerFactory.getLogger(WIPCertificateDao.class);

    @Autowired
    private WipCertificateRepository wipCertificateRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();

        FACTORY.registerClassMap(FACTORY.classMap(WipCertificateDO.class, CertificateDto.class)
                .customize(new StaleObjectMapper<WipCertificateDO, CertificateDto>())
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(CertificateActionDO.class, CertificateActionDto.class).byDefault().toClassMap());
    }

    public WIPCertificateDaoImpl()
    {
       super(FACTORY);
    }

    @Override
    public LockingRepository<WipCertificateDO> getLockingRepository()
    {
        return this.wipCertificateRepository;
    }

    @Override
    @Transactional
    public CertificateDto getCertificate(final Long certificateId)
    {
        LOG.debug("Getting certificate with id {}.", certificateId);

        final WipCertificateDO certificate = this.wipCertificateRepository.findOne(certificateId);

        return certificate == null ? null : getMapper().map(certificate, CertificateDto.class);
    }

    @Override
    @Transactional
    public PageResource<CertificateDto> getCertificatesForJob(final Pageable pageable, final Long jobId)
    {
        LOG.debug("Retrieving certificates for job with ID {}.", jobId);

        final Page<WipCertificateDO> certificates = this.wipCertificateRepository.findAllCertificatesForJob(pageable, jobId);

        final Page<CertificateDto> results = certificates.map(new Converter<WipCertificateDO, CertificateDto>()
        {
            @Override
            public CertificateDto convert(final WipCertificateDO certificate)
            {
                return WIPCertificateDaoImpl.this.getMapper().map(certificate, CertificateDto.class);
            }
        });
        return this.pageFactory.createPageResource(results, CertificatePageResourceDto.class);
    }

    @Override
    @Transactional
    public List<CertificateDto> getCertificatesForJob(final Long jobId)
    {
        LOG.debug("Retrieving certificates for job with ID {}.", jobId);

        final List<WipCertificateDO> certificates = this.wipCertificateRepository.findAllCertificatesForJob(jobId);

        return getMapper().mapAsList(certificates, CertificateDto.class);
    }

    @Override
    @Transactional
    public CertificateDto saveCertificate(final CertificateDto certificateDto)
    {
        LOG.debug("Saving certificate.");

        WipCertificateDO certificate = getMapper().map(certificateDto, WipCertificateDO.class);

        certificate = this.wipCertificateRepository.merge(certificate);

        return getMapper().map(certificate, CertificateDto.class);
    }

    @Override
    @Transactional
    public Date getCertificateIssueDate(final Long certificateId)
    {
        LOG.debug("Retrieving issue date for certificate type with ID {}.", certificateId);

        return this.wipCertificateRepository.findCertificateIssueDate(certificateId);
    }
}
