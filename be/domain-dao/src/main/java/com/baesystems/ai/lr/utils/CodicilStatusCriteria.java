package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;

public class CodicilStatusCriteria
{
    private List<Long> statusList;
    private Boolean open;

    public CodicilStatusCriteria(final List<Long> statusList, final CodicilType codicilType)
    {
        if (statusList != null)
        {
            final CodicilStatusType inactiveStatusType = CodicilStatusType.getInactive(codicilType);
            final CodicilStatusType openStatusType = CodicilStatusType.getOpen(codicilType);

            this.statusList = new ArrayList<>(statusList);
            if (statusList.containsAll(Arrays.asList(openStatusType.getValue(), inactiveStatusType.getValue())))
            {
                this.statusList.removeAll(Arrays.asList(inactiveStatusType.getValue()));
            }
            else if (statusList.contains(openStatusType.getValue()))
            {
                this.open = true;
            }
            else if (statusList.contains(inactiveStatusType.getValue()))
            {
                this.statusList.removeAll(Arrays.asList(inactiveStatusType.getValue()));
                this.statusList.add(openStatusType.getValue());
                this.open = false;
            }

        }
    }

    public List<Long> getStatusList()
    {
        return this.statusList;
    }

    public Boolean getOpen()
    {
        return this.open;
    }

}
