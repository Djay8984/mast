package com.baesystems.ai.lr.dao.attachments;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;

public interface AttachmentDao
{
    PageResource<SupplementaryInformationDto> findAttachments(Pageable pageable, AttachmentTargetType target, Long targetId);

    SupplementaryInformationDto getAttachment(Long supplementaryInformationId);

    SupplementaryInformationDto saveAttachment(AttachmentTargetType target, Long targetId,
            SupplementaryInformationDto supplementaryInformationDto);

    void deleteAttachment(Long supplementaryInformationId);

    Boolean exists(Long supplementaryInformationId);

    SupplementaryInformationDto updateAttachment(SupplementaryInformationDto attachmentDto);

    AttachmentLinkDO addAuthor(AttachmentLinkDO attachmentDo);

    List<SupplementaryInformationDto> findAttachmentsByQuery(AttachmentLinkQueryDto attachmentLinkQueryDto);

}
