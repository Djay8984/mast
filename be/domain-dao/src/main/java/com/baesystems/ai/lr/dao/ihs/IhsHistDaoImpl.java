package com.baesystems.ai.lr.dao.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHistDO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHistRepository;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "IhsHistDao")
public class IhsHistDaoImpl implements IhsHistDao
{
    @Autowired
    private IhsHistRepository ihsHistRepository;

    private final MapperFacade mapper;

    public IhsHistDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(IhsHistDO.class, IhsHistDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public IhsHistDto getIhsHist(final Long imoNumber)
    {
        final IhsHistDO output = this.ihsHistRepository.findOne(imoNumber);
        return output == null ? null : this.mapper.map(output, IhsHistDto.class);
    }
}
