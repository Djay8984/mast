package com.baesystems.ai.lr.dao.codicils;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;

/**
 * Interface for returning Pageable entity DTOs for an Asset ID, e.g Codicils and Defects
 */
public interface CodicilDefectQueryDao<T>
{
    PageResource<T> findAllForAsset(Pageable pageable, Long assetId, CodicilDefectQueryDto codicilDefectQueryDto);
}
