package com.baesystems.ai.lr.utils;

import org.springframework.data.domain.Page;

import com.baesystems.ai.lr.dto.paging.PageResource;

public interface PageResourceFactory
{
    <U extends PageResource<T>, T> U createPageResource(final Page<T> page, Class<U> wrapperClazz);
}
