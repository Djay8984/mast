package com.baesystems.ai.lr.dao.tasks;

import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;

public interface BaseWorkItemAttributeDao
{
    WorkItemAttributeDto createWorkItemAttribute(WorkItemAttributeDto workItemAttribute);
}
