package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface JobDao extends LockingDao<JobDO>
{
    Boolean jobExists(final Long jobId);

    JobDto getJob(Long jobId);

    PageResource<JobDto> getJobsForAsset(Pageable pageable, Long assetId);

    JobDto createJob(JobDto jobDto);

    JobDto updateJob(JobDto jobDto);

    void updateJobStatus(Long jobId, Long statusId);

    ReferenceDataDto getJobStatusForJob(Long jobId);

    PageResource<JobDto> getJobs(Pageable pageable, JobQueryDto query);

    PageResource<JobDto> getJobsForEmployee(Pageable pageable, Long employeeId, JobQueryDto queryDto);

    PageResource<JobDto> getJobsForTeam(Pageable pageable, Long teamId, JobQueryDto queryDto);

    Integer updateCompletedFields(Long jobId, String completedBy, Date completedOn);

    Long getAssetIdForJob(Long jobId);

    PageResource<JobDto> getJobs(Pageable pageable, AbstractQueryDto query);

    Long getVersionForAssetAndJob(Long assetId, Long jobId);
}
