package com.baesystems.ai.lr.dao.references.flagports;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;

public interface PortDao
{
    PortOfRegistryDto getPortOfRegistry(Long portOfRegistryId);

    PageResource<PortOfRegistryDto> findAll(Pageable pageSpecification);

    PageResource<PortOfRegistryDto> findAll(Pageable pageSpecification, Long flagStateId);

    List<PortOfRegistryDto> getPorts();
}
