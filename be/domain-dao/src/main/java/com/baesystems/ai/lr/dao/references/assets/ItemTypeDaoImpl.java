package com.baesystems.ai.lr.dao.references.assets;

import java.util.List;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeRelationshipDO;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRelationshipRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemTypeRepository;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;
import com.baesystems.ai.lr.enums.RelationshipType;

@Repository(value = "ItemTypeDao")
public class ItemTypeDaoImpl implements ItemTypeDao
{
    private static final Logger LOG = LoggerFactory.getLogger(AssetReferenceDataDaoImpl.class);

    private final MapperFacade mapper;

    public ItemTypeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(ItemTypeRelationshipDO.class, ItemRuleDto.class)
                .field("toItemType.id", "toItemType")
                .field("fromItemType.id", "fromItemType")
                .field("itemRelationshipType.id", "relationshipTypeId")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ItemTypeDO.class, ItemTypeDto.class).byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    @Autowired
    private ItemTypeRelationshipRepository itemTypeRelationshipRepository;

    @Autowired
    private ItemTypeRepository itemTypeRepository;

    @Override
    @Transactional
    public List<ItemRuleDto> getItemTypeRelationships()
    {
        LOG.debug("Retrieving all Item Type Relationships");

        final List<ItemTypeRelationshipDO> itemTypeRelationships = this.itemTypeRelationshipRepository.findAll();

        return this.mapper.mapAsList(itemTypeRelationships, ItemRuleDto.class);
    }

    @Override
    @Transactional
    public Integer getMaxOccursForItemType(final Long parentTypeId, final Long childTypeId, final Integer assetCategoryId)
    {
        return this.itemTypeRelationshipRepository.getMaxOccurs(parentTypeId, childTypeId, assetCategoryId, RelationshipType.IS_PART_OF.getValue());
    }

    @Override
    @Transactional
    public Boolean itemTypeExists(final Long itemTypeId)
    {
        return itemTypeRepository.exists(itemTypeId);
    }

    @Override
    @Transactional
    public final List<ItemTypeDto> getItemTypes()
    {
        final List<ItemTypeDO> output = this.itemTypeRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, ItemTypeDto.class);
    }

    @Override
    @Transactional
    public final ItemTypeDto getItemType(final Long itemTypeId)
    {
        final ItemTypeDO output = this.itemTypeRepository.findOne(itemTypeId);
        return output == null ? null : mapper.map(output, ItemTypeDto.class);
    }

    @Override
    @Transactional
    public final String getItemTypeName(final Long itemTypeId)
    {
        return this.itemTypeRepository.findName(itemTypeId);
    }

    @Override
    @Transactional
    public ItemTypeDto getRootItemTypeForAssetCategory(final Integer assetCategoryId)
    {
        final ItemTypeDO output = this.itemTypeRepository.getRootItemForAssetCategory(assetCategoryId);
        return output == null ? null : mapper.map(output, ItemTypeDto.class);
    }
}
