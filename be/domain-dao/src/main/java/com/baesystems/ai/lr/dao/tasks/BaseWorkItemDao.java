package com.baesystems.ai.lr.dao.tasks;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

public interface BaseWorkItemDao
{
    PageResource<WorkItemLightDto> getTasks(Pageable pageable, WorkItemQueryDto query);

    WorkItemLightDto createOrUpdateTask(WorkItemLightDto task);

    Boolean taskExists(Long taskId);

    WorkItemLightDto getTask(Long taskId);
}
