package com.baesystems.ai.lr.dao.references.defects;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.MaterialDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MaterialTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.MaterialRepository;
import com.baesystems.ai.lr.domain.mast.repositories.MaterialTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.RepairActionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.RepairTypeRepository;
import com.baesystems.ai.lr.dto.references.RepairReferenceDataDto;
import com.baesystems.ai.lr.dto.references.MaterialDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "RepairReferenceDataDao")
public class RepairReferenceDataDaoImpl implements RepairReferenceDataDao
{
    @Autowired
    private MaterialRepository materialRepository;

    @Autowired
    private MaterialTypeRepository materialTypeRepository;

    @Autowired
    private RepairTypeRepository repairTypeRepository;

    @Autowired
    private RepairActionRepository repairActionRepository;

    private final MapperFacade mapper;

    public RepairReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(MaterialDO.class, MaterialDto.class)
                .byDefault().toClassMap());

        factory.registerClassMap(factory.classMap(MaterialTypeDO.class, ReferenceDataDto.class)
                .byDefault().toClassMap());

        factory.registerClassMap(factory.classMap(RepairTypeDO.class, ReferenceDataDto.class)
                .byDefault().toClassMap());

        factory.registerClassMap(factory.classMap(RepairActionDO.class, ReferenceDataDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<MaterialDto> getMaterials()
    {
        final List<MaterialDO> results = this.materialRepository.findAll();
        return mapper.mapAsList(results, MaterialDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getMaterialTypes()
    {
        final List<MaterialTypeDO> results = this.materialTypeRepository.findAll();
        return mapper.mapAsList(results, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getRepairTypes()
    {
        final List<RepairTypeDO> results = this.repairTypeRepository.findAll();
        return mapper.mapAsList(results, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getRepairActions()
    {
        final List<RepairActionDO> results = this.repairActionRepository.findAll();
        return mapper.mapAsList(results, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getRepairAction(final Long repairActionId)
    {
        final RepairActionDO result = this.repairActionRepository.findOne(repairActionId);
        return result == null ? null : mapper.map(result, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public RepairReferenceDataDto getReferenceData()
    {
        final RepairReferenceDataDto result = new RepairReferenceDataDto();
        result.setMaterials(this.getMaterials());
        result.setRepairActions(this.getRepairActions());
        result.setRepairTypes(this.getRepairTypes());
        result.setMaterialTypes(this.getMaterialTypes());
        return result;
    }

}
