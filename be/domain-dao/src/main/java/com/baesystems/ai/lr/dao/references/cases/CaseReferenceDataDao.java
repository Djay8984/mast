package com.baesystems.ai.lr.dao.references.cases;

import java.util.List;

import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface CaseReferenceDataDao
{
    ReferenceDataDto getOfficeRole(Long id);

    List<ReferenceDataDto> getOfficeRoles();

    List<ReferenceDataDto> getPreEicInspectionStatuses();

    ReferenceDataDto getPreEicInspectionStatus(Long preEicInspectionStatusId);

    List<ReferenceDataDto> getRiskAssessmentStatuses();

    ReferenceDataDto getRiskAssessmentStatus(Long riskAssessmentStatusId);

    List<ReferenceDataDto> getCaseTypes();

    ReferenceDataDto getCaseType(Long caseTypeId);

    List<CaseStatusDto> getCaseStatuses();

    CaseStatusDto getCaseStatus(Long caseStatusId);

    String getMilestoneStatusName(Long milestoneId);

    List<MilestoneDto> getMilestonesForCaseType(Long caseTypeId);

    MilestoneDto getMilestone(Long milestoneId);

    Integer getMilestonesWorkingDaysOffset(Long milestoneId);

    List<Long> getChildMilestoneIds(Long milestoneId);

    List<Long> getParentMilestoneIds(Long childMilestoneId);

    Boolean isMilestoneMandatory(Long milestoneId);

    Long getMilestoneDueDateReferenceId(Long milestoneId);

    List<MilestoneDto> getMilestones();

    List<Long> getValidNewCaseStatuses(Long initialStatusId);

    Long getDateDependentParentMilestoneId(Long milestoneId);

    List<ReferenceDataDto> getMilestoneDueDataReferences();

    List<ReferenceDataDto> getMilestoneStatuses();
}
