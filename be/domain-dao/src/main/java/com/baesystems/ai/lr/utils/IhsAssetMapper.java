package com.baesystems.ai.lr.utils;

import java.text.SimpleDateFormat;

import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsOwnaDO;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetTypeDto;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

public class IhsAssetMapper extends CustomMapper<DummyIhsAssetDO, IhsAssetDetailsDto>
{
    private boolean mapType(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset)
    {
        boolean allNull = true;
        final IhsAssetTypeDto ihsAssetTypeDto = new IhsAssetTypeDto();

        if (assetDO.getAssetType() != null)
        {
            if (assetDO.getAssetType().getCode() != null)
            {
                allNull = false;
                ihsAssetTypeDto.setStat5Code(assetDO.getAssetType().getCode());
            }
            if (assetDO.getAssetType().getStatDeCode() != null)
            {
                allNull = false;
                ihsAssetTypeDto.setStatDeCode(assetDO.getAssetType().getStatDeCode());
            }
        }

        if (!allNull)
        {
            ihsAsset.setIhsAssetType(ihsAssetTypeDto);
        }

        return allNull;
    }

    private boolean mapNestedFields(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;

        if (assetDO.getIhsAsset() != null && assetDO.getIhsAsset().getId() != null)
        {
            allNull = false;
            ihsAsset.setId(assetDO.getIhsAsset().getId());
        }
        if (assetDO.getClassStatus() != null && assetDO.getClassStatus().getName() != null)
        {
            allNull = false;
            ihsAsset.setStatus(assetDO.getClassStatus().getName());
        }
        if (assetDO.getFlagState() != null && assetDO.getFlagState().getFlagCode() != null)
        {
            allNull = false;
            ihsAsset.setFlag(assetDO.getFlagState().getFlagCode());
        }

        return allNull;
    }

    private boolean mapSimpleFields(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = this.mapNestedFields(assetDO, ihsAsset, inputAllNull);
        final SimpleDateFormat formatter = DateHelper.mastFormatter();

        if (assetDO.getYardNumber() != null)
        {
            allNull = false;
            ihsAsset.setYardNumber(assetDO.getYardNumber());
        }
        if (assetDO.getName() != null)
        {
            allNull = false;
            ihsAsset.setName(assetDO.getName());
        }
        if (assetDO.getBuildDate() != null)
        {
            allNull = false;
            ihsAsset.setDateOfBuild(formatter.format(assetDO.getBuildDate()));
        }
        if (assetDO.getPortName() != null)
        {
            allNull = false;
            ihsAsset.setPortName(assetDO.getPortName());
        }
        if (assetDO.getCallSign() != null)
        {
            allNull = false;
            ihsAsset.setCallSign(assetDO.getCallSign());
        }

        if (assetDO.getOfficialNo() != null)
        {
            allNull = false;
            ihsAsset.setOfficialNo(assetDO.getOfficialNo());
        }
        if (assetDO.getCountryOfBuild() != null)
        {
            allNull = false;
            ihsAsset.setCountryOfBuild(assetDO.getCountryOfBuild());
        }

        return allNull;
    }

    private boolean mapCustomers(final DummyIhsAssetDO assetDO, final IhsAssetDto ihsAsset, final boolean inputAllNull)
    {
        boolean allNull = inputAllNull;
        final IhsPartyMapper ihsPartyMapper = new IhsPartyMapper();

        allNull = ihsPartyMapper.mapBuilder(assetDO, ihsAsset, allNull);
        allNull = ihsPartyMapper.mapShipManager(assetDO, ihsAsset, allNull);
        allNull = ihsPartyMapper.mapDocCompany(assetDO, ihsAsset, allNull);
        allNull = ihsPartyMapper.mapOperator(assetDO, ihsAsset, allNull);
        allNull = ihsPartyMapper.mapTechManager(assetDO, ihsAsset, allNull);

        return allNull;
    }

    private boolean mapOwners(final DummyIhsAssetDO assetDO, final IhsGroupFleetDto ihsGroupFleetDto)
    {
        boolean allNull = true;

        if (assetDO.getRegisteredOwnerInfo() != null && !assetDO.getRegisteredOwnerInfo().isEmpty())
        {
            final DummyIhsOwnaDO party = assetDO.getRegisteredOwnerInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsGroupFleetDto.setRegdOwner(party.getName());
            }
        }
        if (assetDO.getGroupOwnerInfo() != null && !assetDO.getGroupOwnerInfo().isEmpty())
        {
            final DummyIhsOwnaDO party = assetDO.getGroupOwnerInfo().get(0);

            if (party.getName() != null)
            {
                allNull = false;
                ihsGroupFleetDto.setGroupOwner(party.getName());
            }
        }
        if (!allNull)
        {
            ihsGroupFleetDto.setId(assetDO.getIhsAsset() == null ? null : assetDO.getIhsAsset().getId());
        }
        return allNull;
    }

    @Override
    public void mapAtoB(final DummyIhsAssetDO assetDO, final IhsAssetDetailsDto assetDto, final MappingContext context)
    {
        final IhsAssetDto ihsAsset = new IhsAssetDto();
        final IhsGroupFleetDto ihsGroupFleetDto = new IhsGroupFleetDto();
        boolean allNullIhs = this.mapType(assetDO, ihsAsset);
        allNullIhs = this.mapSimpleFields(assetDO, ihsAsset, allNullIhs);
        allNullIhs = this.mapCustomers(assetDO, ihsAsset, allNullIhs);
        final boolean allNullOwners = this.mapOwners(assetDO, ihsGroupFleetDto);

        if (!allNullIhs)
        {
            assetDto.setIhsAsset(ihsAsset);
        }
        if (!allNullOwners)
        {
            assetDto.setIhsGroupFleet(ihsGroupFleetDto);
        }

        assetDto.setId(assetDO.getIhsAsset() == null ? null : assetDO.getIhsAsset().getId());
    }
}
