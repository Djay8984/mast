package com.baesystems.ai.lr.dao.references.codicils;

import java.util.List;

import com.baesystems.ai.lr.dto.references.CodicilStatusDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.CodicilTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface CodicilReferenceDataDao
{
    List<CodicilTemplateDto> getCodicilTemplates();

    CodicilTemplateDto getCodicilTemplate(Long codicilTemplateId);

    List<ReferenceDataDto> getCodicilCategories();

    ReferenceDataDto getCodicilCategory(Long codicilCategoryId);

    List<ReferenceDataDto> getTemplateStatuses();

    List<CodicilTypeDto> getCodicilTypes();

    CodicilTypeDto getCodicilType(Long codicilTypeId);

    List<CodicilStatusDto> getCodicilStatuses();

    ReferenceDataDto getCodicilStatus(Long codicilStatusId);
}
