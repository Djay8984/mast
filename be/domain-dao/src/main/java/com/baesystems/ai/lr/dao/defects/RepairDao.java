package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.defects.RepairDO;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface RepairDao extends BaseRepairDao, LockingDao<RepairDO>
{
    List<RepairDto> findRepairsForAsset(Long assetId);
}
