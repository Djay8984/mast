package com.baesystems.ai.lr.dao.codicils;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;

/**
 * Interface for returning Pageable entity DTOs for a Job, e.g wip versions of Codicils and Defects
 */
public interface WIPCodicilDefectQueryDao<T>
{
    PageResource<T> findAllForJob(Pageable pageable, Long jobId, CodicilDefectQueryDto codicilDefectQueryDto);
}
