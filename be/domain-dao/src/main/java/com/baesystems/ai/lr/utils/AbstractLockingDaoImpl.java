package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.enums.StaleCheckType;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.entities.BaseDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.exception.StaleObjectException;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "LockingDao")
public abstract class AbstractLockingDaoImpl<T extends BaseDO> implements LockingDao<T>
{
    private final MapperFacade mapper;

    public AbstractLockingDaoImpl(final DefaultMapperFactory factory)
    {
        this.mapper = factory.getMapperFacade();
    }

    public abstract LockingRepository<T> getLockingRepository();

    /**
     * Attempt to find an entity matching the given ID and acquire an exclusive lock (using SELECT ... FOR UPDATE). The
     * lock timeout is set deliberately low (see schema for innodb_lock_wait_timeout = 1) so that any transaction is
     * notified immediately if another transaction already holds a lock on that row. The purpose of the exception
     * handling here is to PREVENT lost updates in the following edge case:
     * </p>
     * - User A opens a transaction, reads the record and obtains an exclusive lock with the intention of then updating
     * the record. </br>
     * - User B wants to make an update as well and opens a transaction in the same time frame, reads the record but is
     * blocked from acquiring a lock, and so waits. </br>
     * - User A completes their update, the data is persisted and their row lock is released. </br>
     * - User B is then unblocked, acquires a new lock on the row, but does not have the updated version of the record
     * (containing User A's changes). </br>
     * - User B completes their update, the data is persisted, and their row lock is released. </br>
     * - User A's update has been overwritten by User B, and neither User is aware.
     * </p>
     */
    @Override
    @Transactional
    public <S extends BaseDto> S findOneWithLock(final Long id, final Class<S> returnDtoType, final StaleCheckType entityType)
            throws StaleObjectException
    {
        T returnedDO = null;

        try
        {
            returnedDO = getLockingRepository().findOneWithLock(id);
        }
        catch (final CannotAcquireLockException lockException)
        {
            throw new StaleObjectException(entityType.getTypeString(), lockException);
        }

        return returnedDO == null ? null : getMapper().map(returnedDO, returnDtoType);
    }

    /**
     * For use on an asset versioned entity with composite pk (ID, VERSION)
     */
    @Override
    @Transactional
    public <S extends AssetLightDto> S findOneWithLock(final Long id, final Long assetVersionId, final Class<S> returnDtoType, final StaleCheckType entityType)
            throws StaleObjectException
    {
        VersionedAssetDO returnedDO = null;

        try
        {
            returnedDO = getLockingRepository().findOneWithLock(id, assetVersionId);
        }
        catch (final CannotAcquireLockException lockException)
        {
            throw new StaleObjectException(entityType.getTypeString(), lockException);
        }

        return returnedDO == null ? null : getMapper().map(returnedDO, returnDtoType);
    }

    protected MapperFacade getMapper()
    {
        return this.mapper;
    }
}
