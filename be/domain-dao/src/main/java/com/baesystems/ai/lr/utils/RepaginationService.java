package com.baesystems.ai.lr.utils;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.entities.MultiDO;
import com.baesystems.ai.lr.exception.BadPageRequestException;

public interface RepaginationService<T extends MultiDO>
{
    Page<T> repaginate(final Pageable pageInfo, final long enteredTotal,
            final List<T> list1, final String list1PropertyName,
            final List<T> list2, final String list2PropertyName) throws BadPageRequestException;
}
