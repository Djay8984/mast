package com.baesystems.ai.lr.dao.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ScheduledServiceRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SchedulingTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceCatalogueRepository;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "ServiceDao")
@SuppressWarnings("PMD.GodClass")
public class ServiceDaoImpl extends AbstractLockingDaoImpl<ScheduledServiceDO> implements ServiceDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ServiceDaoImpl.class);

    @Autowired
    private ScheduledServiceRepository serviceRepository;

    @Autowired
    private ServiceCatalogueRepository serviceCatalogueRepository;

    @Autowired
    private SchedulingTypeRepository schedulingTypeRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DaoUtils daoUtils;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(ServiceCatalogueDO.class, ServiceCatalogueLightDto.class)
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ServiceCatalogueDO.class, ServiceCatalogueDto.class)
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ServiceCatalogueDO.class, ScheduledServiceDto.class)
                .fieldAToB("id", "serviceCatalogueId")
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ScheduledServiceDO.class, ScheduledServiceDto.class)
                .field("serviceCatalogue.id", "serviceCatalogueId")
                .customize(new StaleObjectMapper<ScheduledServiceDO, ScheduledServiceDto>())
                .byDefault().toClassMap());

        FACTORY.registerClassMap(FACTORY.classMap(ScheduledServiceDto.class, ScheduledServiceDO.class)
                .field("serviceCatalogueId", "serviceCatalogue.id")
                .customize(new ActiveMapper())
                .byDefault().toClassMap());

        FACTORY.registerClassMap(FACTORY.classMap(SchedulingTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
    }

    private static class ActiveMapper extends CustomMapper<ScheduledServiceDto, ScheduledServiceDO>
    {
        @Override
        public void mapAtoB(final ScheduledServiceDto scheduledServiceDto, final ScheduledServiceDO scheduledServiceDo, final MappingContext context)
        {
            Boolean active;
            if (scheduledServiceDto.getActive() == null)
            {
                active = Boolean.TRUE;
            }
            else
            {
                active = scheduledServiceDto.getActive();
            }
            scheduledServiceDo.setActive(active);

        }
    }

    public ServiceDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public List<ScheduledServiceDto> getServicesForAsset(final Long assetId, final List<Long> statusIds, final List<Long> productFamilyIds,
            final Date dueDateMin, final Date dueDateMax)
    {
        LOG.debug(
                String.format(
                        "Retrieving all Services for Asset with ID %d, Service Status ID in %s, Product Family ID in %s, %s <= dueDate <= %s.",
                        assetId, this.daoUtils.safeToString(statusIds), this.daoUtils.safeToString(productFamilyIds),
                        this.daoUtils.safeToString(dueDateMin), this.daoUtils.safeToString(dueDateMax)));

        final List<ScheduledServiceDO> services = this.serviceRepository.findServicesForAsset(assetId,
                this.daoUtils.getSafeIdList(statusIds), this.daoUtils.getSafeIdList(productFamilyIds), dueDateMin, dueDateMax);

        final List<ScheduledServiceDto> result = this.getMapper().mapAsList(services, ScheduledServiceDto.class);

        return result;
    }

    /**
     * This method finds the services linked to a given product. There is no link between services and products so the
     * services for this product have to be found using the service catalogues for the given product catalogue id. Each
     * product catalogue can only be assigned to the asset once so the list of services returned must be for the product
     * in question.
     *
     * First the product catalogue id is found for the product this is then used to search for a list of service
     * catalogues and then these are used to find service associated with the asset.
     */
    @Override
    @Transactional
    public List<ScheduledServiceDto> getServicesForProduct(final Long productId)
    {
        LOG.debug("Retrieving all Services for Product with ID {}", productId);

        List<ScheduledServiceDO> services = new ArrayList<ScheduledServiceDO>(); // create an empty list for the
                                                                                 // services
        final Long productCatalogueId = this.productRepository.findProductCatalogueId(productId);

        if (productCatalogueId != null)
        { // if a product is found
          // look in the refdata to find the service catalogues associated with the product catalogue of the given
          // product
            final List<Long> serviceCatalogueIds = this.serviceCatalogueRepository.findServiceCatalogueIdsForProductCatalogue(productCatalogueId);

            if (serviceCatalogueIds != null && !serviceCatalogueIds.isEmpty())
            { // if there are service catalogues associated to this product then find any services on this asset
              // with
              // the those service catalogue ids.
                final Long assetId = this.productRepository.findAssetIdForProduct(productId);
                services = this.serviceRepository.findServicesByServiceCatalogueId(assetId, serviceCatalogueIds);
            }
        }

        return this.mapServices(services);
    }

    @Override
    @Transactional
    public List<ScheduledServiceDto> getServicesForAsset(final Long assetId)
    {
        LOG.debug("Retrieving all Services for Asset with ID {}", assetId);

        final List<ScheduledServiceDO> services = this.serviceRepository.findServicesForAsset(assetId);

        return this.mapServices(services);
    }

    @Override
    @Transactional
    public List<ScheduledServiceDto> getServicesForAssetByStatus(final Long assetId, final List<Long> statusIds)
    {
        LOG.debug("Retrieving all Services for Asset with ID {} and Service Credit Status in {}", assetId, statusIds);

        final List<ScheduledServiceDO> services = this.serviceRepository.findServicesForAssetByStatus(assetId, statusIds);

        return this.mapServices(services);
    }

    @Override
    @Transactional
    public ScheduledServiceDto saveService(final ScheduledServiceDto newService)
    {
        LOG.debug("Adding Service to Asset with ID {}", newService.getAsset().getId());

        ScheduledServiceDO service = this.getMapper().map(newService, ScheduledServiceDO.class);
        service = this.serviceRepository.merge(service);

        return service == null ? null : this.mapService(service);
    }

    /**
     * Merge the given list of services, meaning that if the service already exists, it will be updated, and if it does
     * not exist, a new one will be created.
     *
     * @param services
     */
    @Override
    @Transactional
    public void saveOrUpdateServices(final List<ScheduledServiceDto> services)
    {
        for (final ScheduledServiceDto service : services)
        {
            LOG.debug("Saving or updating Service ID [{}] for Asset ID [{}]", service.getId(), service.getAsset().getId());
            final ScheduledServiceDO serviceDo = this.getMapper().map(service, ScheduledServiceDO.class);
            this.serviceRepository.merge(serviceDo);
        }
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getSchedulingTypesForServiceCatalogue(final Long serviceCatalogueId)
    {
        LOG.debug("Retrieving all Scheduling Types for Service Catalogues with ID {}", serviceCatalogueId);

        final List<SchedulingTypeDO> schedulingType = this.schedulingTypeRepository
                .findAllSchedulingTypesForServiceCatalogue(serviceCatalogueId);

        return schedulingType == null ? null : this.getMapper().mapAsList(schedulingType, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ScheduledServiceDto getService(final Long serviceId)
    {
        LOG.debug("Retrieving Service with ID {}", serviceId);

        final ScheduledServiceDO service = this.serviceRepository.findOne(serviceId);

        return service == null ? null : this.mapService(service);
    }

    @Override
    @Transactional
    public ScheduledServiceDto getService(final Long serviceId, final Long assetId)
    {
        LOG.debug(String.format("Retrieving Service with ID %d, related to Asset with ID %d", serviceId, assetId));

        final ScheduledServiceDO service = this.serviceRepository.findServiceForAsset(serviceId, assetId);

        return service == null ? null : this.mapService(service);
    }

    @Override
    @Transactional
    public ServiceCatalogueDto getServiceCatalogue(final Long serviceCatalogueId)
    {
        LOG.debug("Retrieving Service Catalogue with ID {}", serviceCatalogueId);

        final ServiceCatalogueDO serviceCatalogue = this.serviceCatalogueRepository.findOne(serviceCatalogueId);

        return serviceCatalogue == null ? null : this.getMapper().map(serviceCatalogue, ServiceCatalogueDto.class);
    }

    @Override
    @Transactional
    public ScheduledServiceDto updateService(final ScheduledServiceDto serviceDto)
    {
        LOG.debug("Updating Service with ID {}", serviceDto.getId());

        ScheduledServiceDO service = this.getMapper().map(serviceDto, ScheduledServiceDO.class);

        service = this.serviceRepository.merge(service);

        return this.mapService(service);
    }

    @Override
    @Transactional
    public void deleteService(final Long serviceId)
    {
        LOG.debug("Deleting Service with ID {}", serviceId);
        this.serviceRepository.delete(serviceId);
    }

    @Override
    @Transactional
    public ServiceCatalogueDO getServiceCatalogueDO(final Long serviceCatalogueId)
    {
        LOG.debug("Retrieving Service Catalogue with ID {}", serviceCatalogueId);

        return this.serviceCatalogueRepository.findOne(serviceCatalogueId);
    }

    @Override
    @Transactional
    public ServiceCatalogueDto getServiceCatalogueByCategoryAndSurveyType(final String categoryLetter, final String surveyType)
    {
        LOG.debug("Retrieving Service Catalogue with Category letter {}", categoryLetter);

        final List<ServiceCatalogueDO> serviceCatalogues = this.serviceCatalogueRepository.findByCategoryAndSurveyType(categoryLetter,
                surveyType);
        ServiceCatalogueDto serviceCatalogueDto = null;

        if (serviceCatalogues != null && !serviceCatalogues.isEmpty())
        {
            final ServiceCatalogueDO firstMatchingService = serviceCatalogues.get(0);
            serviceCatalogueDto = firstMatchingService == null ? null : this.getMapper().map(firstMatchingService, ServiceCatalogueDto.class);
        }

        return serviceCatalogues == null ? null : serviceCatalogueDto;
    }

    @Override
    @Transactional
    public ServiceCatalogueDto getServiceCatalogueByCodeAndSurveyType(final String categoryCode, final String surveyType)
    {
        LOG.debug("Retrieving Service Catalogue with Category code {}", categoryCode);

        final List<ServiceCatalogueDO> serviceCatalogues = this.serviceCatalogueRepository.findByCodeAndSurveyType(categoryCode, surveyType);
        ServiceCatalogueDto serviceCatalogueDto = null;

        if (serviceCatalogues != null && !serviceCatalogues.isEmpty())
        {
            final ServiceCatalogueDO firstMatchingService = serviceCatalogues.get(0);
            serviceCatalogueDto = firstMatchingService == null ? null : this.getMapper().map(firstMatchingService, ServiceCatalogueDto.class);
        }

        return serviceCatalogues == null ? null : serviceCatalogueDto;
    }

    private List<ScheduledServiceDto> mapServices(final List<ScheduledServiceDO> serviceDOs)
    {
        final List<ScheduledServiceDto> serviceDtos = new ArrayList<ScheduledServiceDto>();

        for (final ScheduledServiceDO serviceDO : serviceDOs)
        {
            serviceDtos.add(this.mapService(serviceDO));
        }

        return serviceDtos;
    }

    private ScheduledServiceDto mapService(final ScheduledServiceDO serviceDO)
    {
        return this.getMapper().map(serviceDO, ScheduledServiceDto.class);
    }

    @Override
    @Transactional
    public ServiceCatalogueDto getUniqueServiceCatalogueByCode(final String serviceCatalogueCode)
    {
        LOG.debug("Retrieving Service Catalogue with Catalogue Code {}", serviceCatalogueCode);

        final List<ServiceCatalogueDO> serviceCatalogues = this.serviceCatalogueRepository.findByCode(serviceCatalogueCode);
        ServiceCatalogueDto serviceCatalogueDto = null;

        if (serviceCatalogues != null && !serviceCatalogues.isEmpty())
        {
            final ServiceCatalogueDO firstMatchingService = serviceCatalogues.get(0);
            serviceCatalogueDto = firstMatchingService == null ? null : this.getMapper().map(firstMatchingService, ServiceCatalogueDto.class);
        }

        return serviceCatalogues == null ? null : serviceCatalogueDto;
    }

    @Override
    @Transactional
    public Boolean serviceExistsForAsset(final Long assetId, final Long serviceId)
    {
        return this.serviceRepository.existsForAsset(assetId, serviceId);
    }

    @Override
    @Transactional
    public String getServiceCodeFromScheduledService(final Long scheduledServiceId)
    {
        LOG.debug("Retrieving Service Code for Service with ID {}", scheduledServiceId);

        return this.serviceRepository.findServiceCodeFromScheduledService(scheduledServiceId);
    }

    @Override
    public LockingRepository<ScheduledServiceDO> getLockingRepository()
    {
        return this.serviceRepository;
    }

    @Override
    @Transactional
    public boolean doAnyItemsHaveOpenServices(final List<Long> itemIdsToCheck)
    {
        return this.serviceRepository.doAnyItemsHaveOpenServices(itemIdsToCheck, ServiceCreditStatusType.getOpen());
    }
}
