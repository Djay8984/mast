package com.baesystems.ai.lr.dao.tasks;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemConditionalAttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemAttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.WorkItemConditionalAttributeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WorkItemRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPageResourceDto;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.WorkItemDaoUtils;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WorkItemDao")
public class WorkItemDaoImpl implements WorkItemDao
{
    private static final Logger LOG = LoggerFactory.getLogger(WorkItemDaoImpl.class);

    private final MapperFacade mapper;

    @Autowired
    private WorkItemRepository workItemRepository;

    @Autowired
    private WorkItemConditionalAttributeRepository workItemConditionalAttributeRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    public WorkItemDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(WorkItemDO.class, WorkItemLightDto.class)
                .byDefault().register();
        factory.registerClassMap(factory.classMap(WorkItemAttributeDO.class, WorkItemAttributeDto.class)
                .field("workItemConditionalAttribute.allowedAttributeValues", "allowedAttributeValues")
                .byDefault().toClassMap());
        factory.classMap(WorkItemActionDO.class, WorkItemActionDto.class).byDefault().register();
        factory.classMap(LrEmployeeDO.class, LrEmployeeDto.class).byDefault().register();
        factory.classMap(SurveyDO.class, SurveyDto.class).byDefault().register();
        factory.registerClassMap(factory.classMap(ScheduledServiceDO.class, ScheduledServiceDto.class)
                .fieldAToB("serviceCatalogue.id", "serviceCatalogueId")
                .byDefault().toClassMap());
        factory.classMap(WorkItemConditionalAttributeDO.class, LinkResource.class).byDefault().register();
        factory.classMap(ItemDO.class, ItemDto.class).byDefault().register();

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public PageResource<WorkItemLightDto> getTasks(final Pageable pageable, final WorkItemQueryDto query)
    {
        LOG.debug("Retrieving all Tasks matching query");

        Page<WorkItemDO> output = null;

        if (query == null)
        {
            output = this.workItemRepository.findAll(pageable);
        }
        else
        {
            output = this.workItemRepository.findAll(pageable, query);
        }
        final Page<WorkItemLightDto> result = output.map(new WorkItemConverter());
        return this.pageFactory.createPageResource(result, WorkItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public WorkItemLightDto createOrUpdateTask(final WorkItemLightDto task)
    {
        WorkItemDO workItem = this.mapper.map(task, WorkItemDO.class);
        WorkItemDaoUtils.updateTaskFields(workItem);
        workItem = this.workItemRepository.merge(workItem);
        LOG.debug(String.format("The Task with ID %d has been created", workItem.getId()));

        return this.mapper.map(workItem, WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public Boolean taskExists(final Long taskId)
    {
        return this.workItemRepository.exists(taskId);
    }

    @Override
    @Transactional(readOnly = true)
    public WorkItemLightDto getTask(final Long taskId)
    {
        final WorkItemDO workItemDO = this.workItemRepository.findOne(taskId);
        return workItemDO != null ? this.mapper.map(workItemDO, WorkItemLightDto.class) : null;
    }

    @Override
    @Transactional
    public List<WorkItemLightDto> getLightTasksForAsset(final Long assetId)
    {
        LOG.debug("Retrieving all Tasks for Asset", assetId);
        return this.mapper.mapAsList(this.workItemRepository.findAllForAsset(assetId), WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemLightDto> getTasksForService(final Long scheduledServiceId)
    {
        return this.mapper.mapAsList(this.workItemRepository.getTasksForService(scheduledServiceId), WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public WorkItemConditionalAttributeDto getWorkItemConditionalAttribute(final String serviceCode, final Long itemId, final Long workItemActionId)
    {
        final WorkItemConditionalAttributeDO workItemConditionalAttribute = this.workItemConditionalAttributeRepository.findConditionalAttribute(
                serviceCode, itemId, workItemActionId);
        return workItemConditionalAttribute != null ? this.mapper.map(workItemConditionalAttribute, WorkItemConditionalAttributeDto.class) : null;
    }

    private class WorkItemConverter implements Converter<WorkItemDO, WorkItemLightDto>
    {
        @Override
        public WorkItemLightDto convert(final WorkItemDO workItem)
        {
            return WorkItemDaoImpl.this.mapper.map(workItem, WorkItemLightDto.class);
        }
    }

}
