package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPActionableItemDao extends BaseActionableItemDao, WIPCodicilDefectQueryDao<ActionableItemDto>, LockingDao<WIPActionableItemDO>
{
    PageResource<ActionableItemDto> getActionableItemsForJob(Pageable pageSpecification, Long jobId, Long statusId);

    ActionableItemDto createActionableItem(ActionableItemDto actionableItemDto);

    ActionableItemDto updateActionableItem(ActionableItemDto actionableItemDto);

    Boolean actionableItemExistsForJob(Long jobId, Long actionableItemId);

    PageResource<ActionableItemDto> getActionableItemForJob(Pageable pageable, Long jobId, Long defectId);

    List<ActionableItemDto> getActionableItemsForJob(Long jobId, List<Long> statusList);

    List<ActionableItemDto> getActionableItemsForJob(Long jobId, String employeeName, Date lastUpdatedDate);

    List<ActionableItemDto> getActionableItemsForJob(Long jobId);

    Integer updateScopeConfirmed(Long jobId, Boolean scopeConfirmed, List<Long> statusList);

    List<ActionableItemDto> getActionableItemsForReportContent(Long jobId);
}
