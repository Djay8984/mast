package com.baesystems.ai.lr.dao.assets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributePersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AttributePersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRelationshipPersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemPersistRepository;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.AttributeCopyRuleType;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.InverseRelationshipException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MissingItemException;
import com.baesystems.ai.lr.service.assets.AssetVersionUpdateService;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.SelectiveRecursionMapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "DraftItemPersistDao")
@SuppressWarnings("PMD.TooManyMethods")
public class DraftItemPersistDaoImpl implements DraftItemPersistDao
{
    private static final String ITEMS = "items";
    private static final String ATTACHMENTS = "attachments";
    private static final int EXPECTED_INVERSE_RELATIONSHIPS_1 = 1;

    @Autowired
    private VersionedItemPersistRepository itemPersistRepository;

    @Autowired
    private AssetVersionUpdateService assetVersionUpdateService;

    @Autowired
    private AttributeTypeRepository attributeTypeRepository;

    @Autowired
    private AttributePersistRepository attributePersistRepository;

    @Autowired
    private ItemRelationshipPersistRepository itemRelationshipPersistRepository;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ItemRepository itemRepository;

    // Mappers

    // Dedicated Attribute mapper
    private final DraftAttributeMapper attributeMapper;

    private final DraftItemMapper customRecursiveDraftItemMapper;

    // Dedicated DTO <-> DO
    private final MapperFacade dtoMapper;

    // Dedicated Mapper for Draft DOs <-> DO for recursive appending.
    private final MapperFacade recursiveAppendDraftDOMapper;

    // Dedicated Mapper for Draft DOs <-> DO for appending when only certain items are to be copied.
    // The difference here is that the child items are not automatically copied, this is done elsewhere.
    private final MapperFacade specificAppendDraftDOMapper;

    public DraftItemPersistDaoImpl()
    {
        super();

        // DTO Mapper
        DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(VersionedItemPersistDO.class, DraftItemDto.class)
                .field("itemType.id", "itemTypeId")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ItemRelationshipDto.class, ItemRelationshipDO.class).byDefault().toClassMap());
        this.dtoMapper = factory.getMapperFacade();

        this.customRecursiveDraftItemMapper = new DraftItemMapper();

        this.attributeMapper = new DraftAttributeMapper();

        // Recursive Append Mapper
        factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(VersionedItemPersistDO.class, VersionedItemPersistDO.class)
                .exclude("id")
                .exclude(ATTACHMENTS)
                .exclude(ITEMS)
                .exclude("parentItem")
                .customize(this.customRecursiveDraftItemMapper)
                .byDefault()
                .toClassMap());

        factory.registerClassMap(factory.classMap(AttributePersistDO.class, AttributePersistDO.class)
                .exclude("id")
                .customize(this.attributeMapper)
                .byDefault().toClassMap());

        factory.registerClassMap(factory.classMap(ItemRelationshipPersistDO.class, ItemRelationshipPersistDO.class)
                .exclude("id")
                .byDefault().toClassMap());
        this.recursiveAppendDraftDOMapper = factory.getMapperFacade();

        // Explicit Append Mapper
        factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(VersionedItemPersistDO.class, VersionedItemPersistDO.class)
                .exclude("id")
                .exclude(ATTACHMENTS)
                .exclude(ITEMS)
                .exclude("related")
                .exclude("reviewed")
                .byDefault().toClassMap());

        factory.registerClassMap(factory.classMap(AttributePersistDO.class, AttributePersistDO.class)
                .exclude("id")
                .customize(this.attributeMapper)
                .byDefault().toClassMap());

        this.specificAppendDraftDOMapper = factory.getMapperFacade();
    }

    /**
     * Method to create an new draft item from reference data and add the relationship to its parent.
     * <p>
     * This method returns the id of the new item so it can be looked up later once the transition is complete.
     */
    @Override
    @Transactional
    public ItemLightDto appendDraftItemFromReferenceData(final Long assetId, final DraftItemDto draftItemDto, final String draftItemName,
            final Integer nextDisplayOrder)
    {
        ItemDO itemDO = new ItemDO();
        itemDO.setAsset(new AssetDO());
        itemDO.getAsset().setId(assetId);
        itemDO = this.itemRepository.merge(itemDO);

        // Create the item first with no relationships.
        VersionedItemPersistDO draftItem = this.dtoMapper.map(draftItemDto, VersionedItemPersistDO.class);

        final Long draftAssetVersionId = this.assetDao.getDraftVersion(assetId);

        draftItem.setId(itemDO.getId());
        draftItem.setAssetId(assetId);
        draftItem.setAssetVersionId(draftAssetVersionId);
        draftItem.setName(draftItemName);
        draftItem.setReviewed(false);
        draftItem.setDisplayOrder(nextDisplayOrder);

        draftItem = this.itemPersistRepository.save(draftItem);
        linkParent(draftItem, draftItemDto.getToParentId(), assetId);

        return this.dtoMapper.map(draftItem, ItemLightDto.class);
    }

    @Override
    @Transactional
    public void createDraftAttribute(final Long attributeTypeId, final Long itemId, final Long assetId, final Long assetVersionId)
    {
        final AttributePersistDO attribute = new AttributePersistDO();
        attribute.setAssetId(assetId);
        attribute.setAssetVersionId(assetVersionId);
        attribute.setItemId(itemId);
        attribute.setAttributeType(new LinkDO());
        attribute.getAttributeType().setId(attributeTypeId);
        attribute.setValue("");

        this.attributePersistRepository.save(attribute);
    }

    /*
     * Updates the display order of an item, and updates any clashing item, we assume that the item being provided has been checked for a draft
     * version and upversioned appropriately in the service layer. If any clashing item needs updating, we need to manually
     * check for draft upverisoning here.
     */
    @Override
    @Transactional
    public void updateDisplayOrders(final VersionedItemPK itemPK, final Integer newDisplayOrder) throws MastBusinessException
    {

        final VersionedItemPersistDO oldItem = getDecoratedDraftItemDO(itemPK.getId(), itemPK.getAssetId(), itemPK.getAssetVersionId());

        if (newDisplayOrder != null && !newDisplayOrder.equals(oldItem.getDisplayOrder()))
        {
            final List<ItemRelationshipPersistDO> itemRelationshipDOs = this.itemRelationshipPersistRepository.getInverseRelationships(itemPK.getId(),
                    itemPK.getAssetId(),
                    itemPK.getAssetVersionId(),
                    RelationshipType.IS_PART_OF.getValue());

            // Check for one and must be one relationship - an item must have one parent.
            if (itemRelationshipDOs.size() > EXPECTED_INVERSE_RELATIONSHIPS_1 || itemRelationshipDOs.isEmpty())
            {
                throw new InverseRelationshipException(String.format(ExceptionMessagesUtils.TOO_MANY_INVERSE_RELATIONSHIPS, itemPK.getId(), 1, itemRelationshipDOs.size()));
            }

            final Long parent = itemRelationshipDOs.get(0).getFromItemId();
            final Long parentAssetId = itemRelationshipDOs.get(0).getAssetId();
            final Long parentAssetVersionId = itemRelationshipDOs.get(0).getFromVersionId();
            final ItemDto fullParent = this.itemDao.getFullItem(new VersionedItemPK(parent, parentAssetId, parentAssetVersionId), true);

            // Get the clashed item - protect against 'bad data' by returning only the first item and checking if no item found.
            final ItemDto clashedItem = fullParent.getItems().stream()
                    .filter(dto -> dto.getDisplayOrder().equals(newDisplayOrder)).findFirst().get();
            if (clashedItem == null)
            {
                throw new MissingItemException(String.format(ExceptionMessagesUtils.CANNOT_FIND_CLASHING_DISPLAY_ORDER_ITEM, itemPK.getId(), newDisplayOrder));
            }

            if (clashedItem.getId() != null)
            {
                final Long draftVersion = this.assetDao.getDraftVersion(itemPK.getAssetId());
                final VersionedItemPK draftClashedItemPK = new VersionedItemPK(clashedItem.getId(), parentAssetId, draftVersion);
                upVersionItem(draftClashedItemPK, parentAssetId, clashedItem.getId());
                this.itemPersistRepository.updateDisplayOrder(clashedItem.getId(), itemPK.getAssetId(), draftVersion, oldItem.getDisplayOrder());
            }

            this.itemPersistRepository.updateDisplayOrder(itemPK.getId(), itemPK.getAssetId(), itemPK.getAssetVersionId(), newDisplayOrder);

        }
    }

    private void upVersionItem(final VersionedItemPK versionedItemPK, final Long assetId, final Long itemId)
    {

        if (!this.itemDao.itemExists(versionedItemPK))
        {
            this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
        }

    }

    @Override
    @Transactional
    public void appendCopiedAssetModel(final Long assetId, final DraftItemDto draftItemDto, final Boolean copyAttributeValues,
            final Integer nextDisplayOrder)
    {
        this.attributeMapper.setCopyAttributeValues(copyAttributeValues);
        this.customRecursiveDraftItemMapper.setAssetId(assetId);
        this.customRecursiveDraftItemMapper.setAssetDraftVersionId(this.assetDao.getDraftVersion(assetId));

        final Long fromAssetPublishedVersion = this.assetDao.getPublishedVersion(draftItemDto.getFromAssetId());
        final VersionedItemPersistDO originalItemDO = getDecoratedDraftItemDO(draftItemDto.getFromId(), draftItemDto.getFromAssetId(),
                fromAssetPublishedVersion);

        final VersionedItemPersistDO newItemDO = this.recursiveAppendDraftDOMapper.map(originalItemDO, VersionedItemPersistDO.class);
        newItemDO.setDisplayOrder(nextDisplayOrder);
        this.itemPersistRepository.save(newItemDO);
        linkParent(newItemDO, draftItemDto.getToParentId(), assetId);
    }

    /**
     * Returns a VersionItemPersistDO that has been decorated with it's relationships.
     * The VersionItem Model is now to complex to handle without manually decorating.
     */
    private VersionedItemPersistDO getDecoratedDraftItemDO(final Long itemId, final Long assetId, final Long assetVersionId)
    {
        final VersionedItemPersistDO item = this.itemPersistRepository.findOne(itemId, assetId, assetVersionId);
        item.setRelated(new ArrayList<>());

        final List<ItemRelationshipPersistDO> relationships = this.itemRelationshipPersistRepository
                .getItemRelationships(itemId, assetId, assetVersionId, null);

        // GET the relationships
        CollectionUtils.nullSafeCollection(relationships)
                .forEach(relationship -> relationship.setToItem(getDecoratedDraftItemDO(
                        relationship.getToItem().getId(), relationship.getToItem().getAssetId(),
                        assetVersionId)));

        CollectionUtils.nullSafeCollection(relationships)
                .forEach(relationship -> item.getRelated().add(relationship));
        // Update the ToItemOn each relationship by calling this method again.

        return item;
    }

    @Override
    @Transactional
    public void selectiveAppendCopiedAssetModel(final Long assetId, final DraftItemDto draftItemDto, final Boolean copyAttributeValues,
            final Integer nextDisplayOrder)
    {
        this.attributeMapper.setCopyAttributeValues(copyAttributeValues);

        final LinkVersionedResource asset = new LinkVersionedResource();
        asset.setId(assetId);
        asset.setVersion(this.assetDao.getDraftVersion(assetId));

        final Long fromAssetPublishedVersion = this.assetDao.getPublishedVersion(draftItemDto.getFromAssetId());
        final VersionedItemPersistDO originalItemDO = getDecoratedDraftItemDO(draftItemDto.getFromId(), draftItemDto.getFromAssetId(),
                fromAssetPublishedVersion);

        final SelectiveRecursionMapper selectiveRecursionMapper = new SelectiveRecursionMapper(asset, this.specificAppendDraftDOMapper, draftItemDto,
                this.itemRepository);
        final VersionedItemPersistDO newDraftItemDO = selectiveRecursionMapper.map(originalItemDO);

        newDraftItemDO.setReviewed(false);
        newDraftItemDO.setDisplayOrder(nextDisplayOrder);

        this.itemPersistRepository.save(newDraftItemDO);

        final Map<Long, VersionedItemPersistDO> idMap = selectiveRecursionMapper.getIdMap();

        for (final ItemRelationshipPersistDO relationship : selectiveRecursionMapper.getOtherRelationships())
        {
            if (idMap.keySet().contains(relationship.getFromItem().getId()) && idMap.keySet().contains(relationship.getToItem().getId()))
            {
                final ItemRelationshipPersistDO newRelationship = new ItemRelationshipPersistDO();
                final VersionedItemPersistDO fromItem = idMap.get(relationship.getFromItem().getId());

                newRelationship.setFromAssetId(fromItem.getAssetId());
                newRelationship.setFromItemId(fromItem.getId());
                newRelationship.setFromVersionId(fromItem.getAssetVersionId());

                final VersionedItemPersistDO toItem = idMap.get(relationship.getToItem().getId());

                newRelationship.setAssetId(toItem.getAssetId());
                newRelationship.setToItemId(toItem.getId());
                newRelationship.setToVersionId(toItem.getAssetVersionId());

                newRelationship.setType(new LinkDO());
                newRelationship.getType().setId(relationship.getType().getId());
                this.itemRelationshipPersistRepository.save(newRelationship);
            }
        }

        linkParent(newDraftItemDO, draftItemDto.getToParentId(), assetId);
    }

    /**
     * Marks a list of draft items as deleted and then goes on to delete all relationships they are involved in and all
     * of their attributes. Reduce its siblings' display orders by one if they're greater too to close the gap.
     */
    @Override
    @Transactional
    public void deleteDraftItem(final Long draftItemId)
    {
        // final LazyItemDto mainItemForDeletion = this.draftItemDao.getItem(draftItemId);
        final LazyItemDto mainItemForDeletion = new LazyItemDto();
        final Integer deletedItemDisplayOrder = mainItemForDeletion.getDisplayOrder();
        final LinkVersionedResource lrDeletedItemParent = mainItemForDeletion.getParentItem();

        final List<Long> draftItemsToDelete = this.itemDao.getItemChildIdList(draftItemId);

        this.itemPersistRepository.bulkLogicalDelete(draftItemsToDelete);
        // this.draftItemRelationshipPublishRepository.bulkLogicalDeleteByItemId(draftItemsToDelete);
        this.attributePersistRepository.bulkLogicalDeleteByItemId(draftItemsToDelete);
        if (deletedItemDisplayOrder != null && lrDeletedItemParent != null)
        {
            final ItemMetaDto itemMetaDto = this.itemDao.getItemMeta(lrDeletedItemParent.getId());
            final ItemDto itemDto = this.itemDao.getFullItem(new VersionedItemPK(lrDeletedItemParent.getId(), itemMetaDto.getAsset().getId(), null), true);
            final List siblings = itemDto.getItems();
            if (siblings != null && !siblings.isEmpty())
            {
                this.itemPersistRepository.reorganiseDisplayOrdersAfterDelete(deletedItemDisplayOrder, siblings);
            }
        }
    }

    private void linkParent(final VersionedItemPersistDO draftItem, final Long parentId, final Long assetId)
    {
        // Once the item exists find the parent and create a new is part of relationship.

        final Long draftVersionId = this.assetDao.getDraftVersion(assetId);
        final VersionedItemPersistDO parentItem = this.itemPersistRepository.findOne(parentId, assetId, draftVersionId);

        final ItemRelationshipPersistDO relationship = new ItemRelationshipPersistDO();

        relationship.setFromAssetId(parentItem.getAssetId());
        relationship.setFromItemId(parentItem.getId());
        relationship.setFromVersionId(parentItem.getAssetVersionId());

        relationship.setAssetId(draftItem.getAssetId());
        relationship.setToItemId(draftItem.getId());
        relationship.setToVersionId(draftItem.getAssetVersionId());

        relationship.setType(new LinkDO());
        relationship.getType().setId(RelationshipType.IS_PART_OF.getValue());

        this.itemRelationshipPersistRepository.save(relationship);
    }

    /*
     * This Mapper is used during the recursive copy process to set the asset_id on the new draft items.
     */
    public class DraftItemMapper extends CustomMapper<VersionedItemPersistDO, VersionedItemPersistDO>
    {
        private Long assetId;
        private Long assetDraftVersionId;

        public void setAssetId(final Long assetId)
        {
            this.assetId = assetId;
        }

        public void setAssetDraftVersionId(final Long assetDraftVersionId)
        {
            this.assetDraftVersionId = assetDraftVersionId;
        }

        @Override
        public void mapAtoB(final VersionedItemPersistDO originalItem, final VersionedItemPersistDO newItem, final MappingContext context)
        {
            ItemDO itemDO = new ItemDO();
            itemDO.setAsset(new AssetDO());
            itemDO.getAsset().setId(this.assetId);
            itemDO = DraftItemPersistDaoImpl.this.itemRepository.merge(itemDO);

            newItem.setId(itemDO.getId());
            newItem.setAssetId(this.assetId);
            newItem.setAssetVersionId(this.assetDraftVersionId);

            CollectionUtils.nullSafeCollection(newItem.getRelated()).forEach(this::linkRelationship);
        }

        /**
         * Copy the read only assocs to the mutable properties, ready to be persisted
         */
        private void linkRelationship(final ItemRelationshipPersistDO rel)
        {
            rel.setFromItemId(rel.getFromItem().getId());
            rel.setFromVersionId(rel.getFromItem().getAssetVersionId());
            rel.setFromAssetId(rel.getFromItem().getAssetId());

            rel.setToItemId(rel.getToItem().getId());
            rel.setToVersionId(rel.getToItem().getAssetVersionId());
            rel.setAssetId(rel.getToItem().getAssetId());
        }
    }

    public class DraftAttributeMapper extends CustomMapper<AttributePersistDO, AttributePersistDO>
    {

        private Boolean copyAttributeValues = false;

        public void setCopyAttributeValues(final Boolean copyAttributeValues)
        {
            this.copyAttributeValues = copyAttributeValues;
        }

        @Override
        public void mapAtoB(final AttributePersistDO attributeDO, final AttributePersistDO draftAttributeDO, final MappingContext context)
        {
            final AttributeTypeDO attributeTypeDO = DraftItemPersistDaoImpl.this.attributeTypeRepository.findOne(attributeDO.getAttributeType().getId());

            if (AttributeCopyRuleType.MUST_NOT.getValue().equals(attributeTypeDO.getAttributeCopyRule().getId())
                    || !this.copyAttributeValues && !AttributeCopyRuleType.MUST.getValue().equals(attributeTypeDO.getAttributeCopyRule().getId()))
            {
                draftAttributeDO.setValue("");
            }
        }
    }
}
