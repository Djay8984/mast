package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamEmployeeDO;
import com.baesystems.ai.lr.domain.mast.repositories.TeamEmployeeRepository;
import com.baesystems.ai.lr.dto.references.TeamEmployeeDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository(value = "TeamEmployeeDao")
public class TeamEmployeeDaoImpl implements TeamEmployeeDao
{
    private final MapperFacade mapper;

    @Autowired
    private TeamEmployeeRepository teamEmployeeRepository;

    public TeamEmployeeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(TeamEmployeeDO.class, TeamEmployeeDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<TeamEmployeeDto> findAll()
    {
        final List<TeamEmployeeDO> output = this.teamEmployeeRepository.findAll();
        return this.mapper.mapAsList(output, TeamEmployeeDto.class);
    }
}
