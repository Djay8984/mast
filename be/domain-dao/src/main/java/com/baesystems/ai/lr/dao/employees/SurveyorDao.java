package com.baesystems.ai.lr.dao.employees;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.paging.PageResource;

public interface SurveyorDao
{
    PageResource<SurveyorDto> getSurveyors(Pageable pageable);

    PageResource<SurveyorDto> getSurveyors(Pageable pageSpecification, String searchString, Long officeId);

    SurveyorDto getSurveyor(Long surveyorId);
}
