package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedAssetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersionedAssetPKGenerator
{
    @Autowired
    private VersionedAssetRepository versionedAssetRepository;

    public VersionedAssetPK getNextPK(final Long assetId)
    {
        final Long nextVersionId = versionedAssetRepository.getNextVersion(assetId);
        return new VersionedAssetPK(assetId, nextVersionId);
    }
}
