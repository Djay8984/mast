package com.baesystems.ai.lr.dao.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsPrincipalDimensionsDao
{
    IhsPrincipalDimensionsDto getPrincipalDimensions(final Long imoNumber) throws RecordNotFoundException;
}
