package com.baesystems.ai.lr.dao.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.BuilderDto;

public interface BuilderDao
{
    List<BuilderDto> findAll();
}
