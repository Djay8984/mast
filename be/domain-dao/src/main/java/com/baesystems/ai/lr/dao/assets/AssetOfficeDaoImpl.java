package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetOfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetOfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository(value = "AssetOfficeDao")
public class AssetOfficeDaoImpl implements AssetOfficeDao
{
    @Autowired
    private AssetOfficeRepository assetOfficeRepository;

    @Override
    @Transactional
    public void hardDeleteAssetVersionedEntities(final Long assetId, final Long versionId)
    {
        final List<AssetOfficeDO> assetOffices = this.assetOfficeRepository.getAssetOfficeForVersionedAsset(assetId, versionId);
        if (assetOffices != null)
        {
            assetOffices.stream().forEach(office -> this.assetOfficeRepository.hardDelete(office.getId()));
        }
    }
}
