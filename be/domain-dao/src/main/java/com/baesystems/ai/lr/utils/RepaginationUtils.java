package com.baesystems.ai.lr.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.apache.commons.beanutils.PropertyUtils;

import com.baesystems.ai.lr.domain.entities.MultiDO;
import com.baesystems.ai.lr.exception.BadPageRequestException;

public class RepaginationUtils<T extends MultiDO>
{
    protected static final String LISTS_CANNOT_BE_NULL = "Input lists must not be null.";
    protected static final String CANNOT_COMPARE = "Fields %s are not of a compareable type.";
    protected static final String CANNOT_COMPARE_NULL = "Fields %s cannot be compared because at least one of the parent objects is null.";
    protected static final String ASC = "ASC";

    private String parentField1 = "";
    private String parentField2 = "";

    protected enum Relationship
    { // enum for internal use
        EQ,
        GT,
        LT;
    }

    /**
     * Set class level field used in multiple methods
     */
    protected void setClassLevelVariables(final String enteredParentField1, final String enteredEarentField2)
    {
        this.parentField1 = enteredParentField1 == null || "".equals(enteredParentField1) ? "" : enteredParentField1 + ".";
        this.parentField2 = enteredEarentField2 == null || "".equals(enteredEarentField2) ? "" : enteredEarentField2 + ".";
    }

    /**
     * Get the relationship between two objects based on the field with the given name. Other methods are called to
     * determine the relationship between the field values based on the type.
     *
     * @param fieldName name of the field, can use dot notation
     * @param ignoreCase whether or not to ignore the case in string comparisons
     * @param obj1
     * @param obj2
     */
    protected Relationship getRelationship(final String fieldName, final Boolean ignoreCase, final Object obj1, final Object obj2)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, BadPageRequestException
    {
        if (obj1 == null || obj2 == null)
        {
            throw new BadPageRequestException(String.format(CANNOT_COMPARE_NULL, fieldName));
        }

        final Object field1 = this.getProperty(obj1, this.parentField1 + fieldName);
        final Object field2 = this.getProperty(obj2, this.parentField2 + fieldName);

        Relationship returnValue = this.nullComparison(field1, field2);

        if (returnValue == null)
        {
            returnValue = this.compareFields(field1, field2, ignoreCase, fieldName);
        }

        return returnValue;
    }

    private Object getProperty(final Object obj, final String field) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        final String[] segments = field.split("\\.");

        Object requestedObject = obj;

        for (final String segment : segments)
        {
            requestedObject = PropertyUtils.getProperty(requestedObject, segment);

            if (requestedObject == null)
            {
                break;
            }
        }

        return requestedObject;
    }

    private Relationship nullComparison(final Object obj1, final Object obj2)
    {
        Relationship returnValue = null;

        if (obj1 == null && obj2 != null)
        {
            returnValue = Relationship.GT;
        }
        else if (obj1 != null && obj2 == null)
        {
            returnValue = Relationship.LT;
        }
        else if (obj1 == null && obj2 == null)
        {
            returnValue = Relationship.EQ;
        }

        return returnValue;
    }

    private <S> Relationship compareFields(final S field1, final S field2, final Boolean ignoreCase, final String fieldName)
            throws BadPageRequestException
    {
        Relationship returnValue = null;

        if (Date.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareDates(field1, field2);
        }
        else if (Integer.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareIntegers(field1, field2);
        }
        else if (Long.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareLongs(field1, field2);
        }
        else if (Float.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareFloats(field1, field2);
        }
        else if (Double.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareDoubles(field1, field2);
        }
        else if (String.class.isInstance(field1))
        {
            returnValue = ComparatorUtils.compareStrings(field1, field2, ignoreCase);
        }
        else
        {
            throw new BadPageRequestException(String.format(CANNOT_COMPARE, fieldName));
        }

        return returnValue;
    }
}
