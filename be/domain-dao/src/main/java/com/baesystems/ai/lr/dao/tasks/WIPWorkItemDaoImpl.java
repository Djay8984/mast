package com.baesystems.ai.lr.dao.tasks;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemConditionalAttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemAttributeDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPWorkItemRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPageResourceDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;
import com.baesystems.ai.lr.utils.WorkItemDaoUtils;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPWorkItemDao")
public class WIPWorkItemDaoImpl extends AbstractLockingDaoImpl<WIPWorkItemDO> implements WIPWorkItemDao
{
    private static final Logger LOG = LoggerFactory.getLogger(WIPWorkItemDaoImpl.class);

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private WIPWorkItemRepository wipWorkItemRepository;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.classMap(WIPWorkItemDO.class, WorkItemDto.class)
                .byDefault().register();
        FACTORY.classMap(WIPWorkItemDO.class, WorkItemLightDto.class)
                .customize(new StaleObjectMapper<WIPWorkItemDO, WorkItemLightDto>())
                .byDefault().register();
        FACTORY.classMap(WorkItemActionDO.class, WorkItemActionDto.class).byDefault().register();
        FACTORY.classMap(ItemDO.class, ItemDto.class).byDefault().register();
        FACTORY.classMap(SurveyDO.class, SurveyDto.class).byDefault().register();
        FACTORY.classMap(LrEmployeeDO.class, LrEmployeeDto.class).byDefault().register();
        FACTORY.registerClassMap(FACTORY.classMap(ScheduledServiceDO.class, ScheduledServiceDto.class)
                .fieldAToB("serviceCatalogue.id", "serviceCatalogueId")
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(WorkItemAttributeDO.class, WorkItemAttributeDto.class)
                .field("workItemConditionalAttribute.allowedAttributeValues", "allowedAttributeValues")
                .byDefault().toClassMap());
        FACTORY.classMap(WorkItemConditionalAttributeDO.class, LinkResource.class).byDefault().register();
    }

    public WIPWorkItemDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPWorkItemDO> getLockingRepository()
    {
        return this.wipWorkItemRepository;
    }

    @Override
    @Transactional
    public PageResource<WorkItemLightDto> getTasks(final Pageable pageable, final WorkItemQueryDto query)
    {
        LOG.debug("Retrieving all WIP Tasks matching query");

        Page<WIPWorkItemDO> output = null;

        if (query == null)
        {
            output = this.wipWorkItemRepository.findAll(pageable);
        }
        else
        {
            output = this.wipWorkItemRepository.findAll(pageable, query);
        }
        final Page<WorkItemLightDto> result = output.map(new WorkItemConverter());
        return this.pageFactory.createPageResource(result, WorkItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemDto> getTasksForJob(final Long jobId)
    {
        LOG.debug("Retrieving all WIP Tasks for Job", jobId);
        return getMapper().mapAsList(this.wipWorkItemRepository.findAllForJob(jobId), WorkItemDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemDto> getTasksForReportContent(final Long jobId)
    {
        LOG.debug("Retrieving all WIP Tasks where action has been taken in Job", jobId);
        return getMapper().mapAsList(this.wipWorkItemRepository.findForReportContent(jobId), WorkItemDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemLightDto> getLightTasksForJob(final Long jobId)
    {
        LOG.debug("Retrieving all WIP Tasks for Job", jobId);
        return getMapper().mapAsList(this.wipWorkItemRepository.findAllForJob(jobId), WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public WorkItemLightDto createOrUpdateTask(final WorkItemLightDto task)
    {
        WIPWorkItemDO workItemDO = getMapper().map(task, WIPWorkItemDO.class);
        WorkItemDaoUtils.updateWIPTaskFields(workItemDO);
        workItemDO = this.wipWorkItemRepository.merge(workItemDO);
        LOG.debug(String.format("The WIP Task with ID %d has been created", workItemDO.getId()));

        return getMapper().map(workItemDO, WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemDto> getTasksForJob(final Long jobId, final String employeeName, final Date lastUpdatedDate)
    {
        LOG.debug("Finding all Tasks for Job {} restricted by user and report issue date", jobId);

        final List<WIPWorkItemDO> output = this.wipWorkItemRepository.findTasksByJob(jobId, employeeName, lastUpdatedDate);
        return getMapper().mapAsList(output, WorkItemDto.class);
    }

    @Override
    @Transactional
    public WorkItemLightDto updateTask(final WorkItemLightDto task)
    {
        LOG.debug(String.format("Updating WIP Task: %d", task.getId()));
        WIPWorkItemDO workItemDO = getMapper().map(task, WIPWorkItemDO.class);
        WorkItemDaoUtils.updateWIPTaskFields(workItemDO);

        workItemDO = this.wipWorkItemRepository.merge(workItemDO);

        return getMapper().map(workItemDO, WorkItemLightDto.class);
    }

    @Override
    @Transactional
    public Boolean taskExists(final Long taskId)
    {
        return this.wipWorkItemRepository.exists(taskId);
    }

    @Override
    @Transactional(readOnly = true)
    public WorkItemLightDto getTask(final Long taskId)
    {
        final WIPWorkItemDO workItemDO = this.wipWorkItemRepository.findOne(taskId);
        return workItemDO != null ? getMapper().map(workItemDO, WorkItemLightDto.class) : null;
    }

    private class WorkItemConverter implements Converter<WIPWorkItemDO, WorkItemLightDto>
    {
        @Override
        public WorkItemLightDto convert(final WIPWorkItemDO workItem)
        {
            return WIPWorkItemDaoImpl.this.getMapper().map(workItem, WorkItemLightDto.class);
        }
    }
}
