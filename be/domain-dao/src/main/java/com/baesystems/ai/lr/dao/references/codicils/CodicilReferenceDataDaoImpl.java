package com.baesystems.ai.lr.dao.references.codicils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.CodicilCategoryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CodicilStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CodicilTemplateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CodicilTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.TemplateStatusDO;
import com.baesystems.ai.lr.domain.mast.repositories.CodicilCategoryRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CodicilStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CodicilTemplateRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CodicilTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.TemplateStatusRepository;
import com.baesystems.ai.lr.dto.references.CodicilCategoryDto;
import com.baesystems.ai.lr.dto.references.CodicilStatusDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.CodicilTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CodicilReferenceDataDao")
public class CodicilReferenceDataDaoImpl implements CodicilReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(CodicilReferenceDataDaoImpl.class);

    @Autowired
    private CodicilTemplateRepository codicilTemplateRepository;

    @Autowired
    private CodicilCategoryRepository codicilCategoryRepository;

    @Autowired
    private TemplateStatusRepository templateStatusRepository;

    @Autowired
    private CodicilTypeRepository codicilTypeRepository;

    @Autowired
    private CodicilStatusRepository codicilStatusRepository;

    private final MapperFacade mapper;

    public CodicilReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(CodicilTemplateDO.class, CodicilTemplateDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CodicilCategoryDO.class, CodicilCategoryDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(TemplateStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<CodicilTemplateDto> getCodicilTemplates()
    {
        LOG.debug("Retrieving all Codicil Templates");

        final List<CodicilTemplateDO> codicilTemplates = this.codicilTemplateRepository.findAll();

        return this.mapper.mapAsList(codicilTemplates, CodicilTemplateDto.class);
    }

    @Override
    @Transactional
    public CodicilTemplateDto getCodicilTemplate(final Long codicilTemplateId)
    {
        LOG.debug("Retrieving Codicil Template with ID {}", codicilTemplateId);

        final CodicilTemplateDO codicilTemplate = this.codicilTemplateRepository.findOne(codicilTemplateId);

        return this.mapper.map(codicilTemplate, CodicilTemplateDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCodicilCategories()
    {
        LOG.debug("Retrieving all Codicil Categories");

        final List<CodicilCategoryDO> codicilCategories = this.codicilCategoryRepository.findAll();

        return this.mapper.mapAsList(codicilCategories, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCodicilCategory(final Long codicilCategoryId)
    {
        final CodicilCategoryDO output = this.codicilCategoryRepository.findOne(codicilCategoryId);
        return output == null ? null : mapper.map(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getTemplateStatuses()
    {
        LOG.debug("Retrieving all Template Statuses");

        final List<TemplateStatusDO> templateStatuses = this.templateStatusRepository.findAll();

        return this.mapper.mapAsList(templateStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<CodicilTypeDto> getCodicilTypes()
    {
        final List<CodicilTypeDO> output = this.codicilTypeRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, CodicilTypeDto.class);
    }

    @Override
    @Transactional
    public CodicilTypeDto getCodicilType(final Long codicilTypeId)
    {
        final CodicilTypeDO output = this.codicilTypeRepository.findOne(codicilTypeId);
        return output == null ? null : mapper.map(output, CodicilTypeDto.class);
    }

    @Override
    @Transactional
    public List<CodicilStatusDto> getCodicilStatuses()
    {
        final List<CodicilStatusDO> output = this.codicilStatusRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, CodicilStatusDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCodicilStatus(final Long codicilStatusId)
    {
        final CodicilStatusDO output = this.codicilStatusRepository.findOne(codicilStatusId);
        return output == null ? null : mapper.map(output, ReferenceDataDto.class);
    }
}
