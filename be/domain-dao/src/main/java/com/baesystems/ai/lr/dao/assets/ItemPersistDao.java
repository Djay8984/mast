package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;

import java.util.List;

@SuppressWarnings("PMD.TooManyMethods")
public interface ItemPersistDao
{
    ItemMetaDto createItemMeta(Long assetId);

    ItemRelationshipDto createItemRelationship(Long assetId, Long itemId, ItemRelationshipDto itemRelationshipDto);

    void deleteItemRelationship(Long relationshipId);

    void updateItemName(Long itemId, String itemName);

    ItemLightDto setItemAndAllDescendantsToSuccessfullyReviewed(Long id);

    ItemLightDto setItemAndAllAncestorsToRequireAReview(Long id);

    void deleteItem(Long draftItemId, Long assetId);

    ItemLightDto createAssetRootItem(Long assetId, ItemLightDto newItem);

    ItemLightDto upVersionItem(Long itemId, Long assetId);

    ItemLightDto createVersionedItemForDeletion(Long itemId, Long assetId);

    void upversionInverseRelationships(Long assetId, Long itemId, Long draftVersionId, Boolean linkFromDraftVersion);

    void updateDecommissionedFlag(Boolean flagState, Long assetId, List<Long> itemIds, Long versionId);

    void publishItems(Long assetId, Long draftVersionId);
}
