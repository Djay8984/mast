package com.baesystems.ai.lr.dao.assets;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.enums.RelationshipType;

public interface ItemDao
{
    Boolean relationshipExistsForItem(Long itemId, Long relationshipId, RelationshipType relationshipTypeId);

    Boolean itemIsOfType(Long itemId, Long typeId);

    PageResource<LazyItemDto> findItemsByAsset(Long assetId, Long versionId, Boolean allowDrafts, Pageable pageable, ItemQueryDto itemQueryDto);

    boolean isItemNameUnique(String itemName, Long parentId);

    ItemDto getItemWithFullHierarchy(Long itemId);

    List<Long> getItemIdsFromAssetAndType(Long assetId, List<Long> itemTypeId);

    ItemMetaDto getItemMeta(Long itemId);

    LazyItemDto getRootItem(Long assetId, Long aVersionId, Boolean allowDraft);

    LazyItemDto getItem(VersionedItemPK versionedItemPK);

    LazyItemDto getItemWithoutRelationships(VersionedItemPK versionedItemPK);

    ItemLightDto getItemLight(VersionedItemPK versionedItemPK);

    Integer getNextChildDisplayOrder(Long parentItemId);

    Integer countItemsOfSpecificVersion(Long assetId, Long versionId);

    ItemDto getFullItem(VersionedItemPK versionedItemPK, Boolean allowDraft);

    Boolean isProposedRelationshipValidForTypes(Long proposedToItemTypeId, Long proposedFromItemTypeId, Integer assetCategoryId,
            Long relationshipTypeId);

    List<Long> getItemChildIdList(Long itemId);

    List<ItemDto> getItemChildList(VersionedItemPK versionedItemPK);

    Boolean itemExistsForAsset(Long assetId, Long itemId);

    Boolean itemExists(VersionedItemPK itemPK);

    Boolean itemExists(Long itemId);

    Long getItemType(Long itemId);

    Boolean relationshipExists(Long toItemId, Long fromItemId, Long relationshipTypeId);

    Integer countChildItemsOfType(Long parentId, Long typeId);

    ItemRelationshipDO getItemRelationship(Long relationshipId);

    Optional<Long> getItemRelationshipId(Long assetId, Long fromItemId, Long fromVersion, Long toItemId, Long toVersion);

    AssetModelDto getAssetModel(Long assetId, Long versionId);
}
