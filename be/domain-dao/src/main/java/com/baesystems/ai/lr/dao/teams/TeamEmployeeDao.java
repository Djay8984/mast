package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.dto.references.TeamEmployeeDto;

import java.util.List;

public interface TeamEmployeeDao
{
    List<TeamEmployeeDto> findAll();
}
