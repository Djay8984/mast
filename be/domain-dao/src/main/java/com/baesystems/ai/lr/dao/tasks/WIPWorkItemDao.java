package com.baesystems.ai.lr.dao.tasks;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPWorkItemDao extends BaseWorkItemDao, LockingDao<WIPWorkItemDO>
{
    List<WorkItemLightDto> getLightTasksForJob(Long jobId);

    List<WorkItemDto> getTasksForJob(Long jobId);

    List<WorkItemDto> getTasksForJob(Long jobId, String employeeName, Date lastUpdatedDate);

    WorkItemLightDto updateTask(WorkItemLightDto task);

    List<WorkItemDto> getTasksForReportContent(Long jobId);
}
