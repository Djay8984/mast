package com.baesystems.ai.lr.dao.references.jobs;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.AllowedWorkItemAttributeValueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeDataTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassRecommendationDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CreditStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.EndorsementTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.JobCategoryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.JobStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ReportTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ResolutionStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AllowedWorkItemAttributeValueRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ClassRecommendationRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CreditStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.EndorsementTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.JobCategoryRepository;
import com.baesystems.ai.lr.domain.mast.repositories.JobStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ReportTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ResolutionStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WorkItemActionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WorkItemTypeRepository;
import com.baesystems.ai.lr.dto.references.AllowedWorkItemAttributeValueDto;
import com.baesystems.ai.lr.dto.references.ClassRecommendationDto;
import com.baesystems.ai.lr.dto.references.CreditingStatusDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "JobReferenceDataDao")
public class JobReferenceDataDaoImpl implements JobReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(JobReferenceDataDaoImpl.class);

    @Autowired
    private JobStatusRepository jobStatusRepository;

    @Autowired
    private JobCategoryRepository jobCategoryRepository;

    @Autowired
    private CreditStatusRepository creditStatusRepository;

    @Autowired
    private EndorsementTypeRepository endorsementTypeRepository;

    @Autowired
    private WorkItemTypeRepository workItemTypeRepository;

    @Autowired
    private WorkItemActionRepository workItemActionRepository;

    @Autowired
    private ResolutionStatusRepository resolutionStatusRepository;

    @Autowired
    private ReportTypeRepository reportTypeRepository;

    @Autowired
    private AllowedWorkItemAttributeValueRepository allowedWorkItemAttributeValueRepository;

    @Autowired
    private ClassRecommendationRepository classRecommendationRepository;

    private final MapperFacade mapper;

    public JobReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(CreditStatusDO.class, CreditingStatusDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(WorkItemTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(WorkItemActionDO.class, WorkItemActionDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AttributeDataTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ReportTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(EndorsementTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(
                factory.classMap(AllowedWorkItemAttributeValueDO.class, AllowedWorkItemAttributeValueDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ClassRecommendationDO.class, ClassRecommendationDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getJobStatuses()
    {
        LOG.debug("Retrieving all Job Statuses");

        final List<JobStatusDO> jobStatuses = this.jobStatusRepository.findAll();
        return mapper.mapAsList(jobStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCreditStatuses()
    {
        LOG.debug("Retrieving all Credit Statuses");

        final List<CreditStatusDO> creditStatusDOs = this.creditStatusRepository.findAll();
        return this.mapper.mapAsList(creditStatusDOs, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getEndorsementTypes()
    {
        LOG.debug("Retrieving all Endorsement Types");

        final List<EndorsementTypeDO> endorsementTypeDOs = this.endorsementTypeRepository.findAll();
        return this.mapper.mapAsList(endorsementTypeDOs, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getWorkItemTypes()
    {
        LOG.debug("Retrieving all Work Item Types");

        final List<WorkItemTypeDO> typeDOs = this.workItemTypeRepository.findAll();
        return this.mapper.mapAsList(typeDOs, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<WorkItemActionDto> getWorkItemActions()
    {
        LOG.debug("Retrieving all Work Item Actions");

        final List<WorkItemActionDO> typeDOs = this.workItemActionRepository.findAll();
        return this.mapper.mapAsList(typeDOs, WorkItemActionDto.class);
    }

    @Override
    public List<ReferenceDataDto> getResolutionStatuses()
    {
        LOG.debug("Retrieving all Resolution Statuses");

        final List<ResolutionStatusDO> resolutionStatusDOs = this.resolutionStatusRepository.findAll();
        return this.mapper.mapAsList(resolutionStatusDOs, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getReportTypes()
    {
        LOG.debug("Retrieving all Report Types");

        final List<ReportTypeDO> typeDOs = this.reportTypeRepository.findAll();
        return this.mapper.mapAsList(typeDOs, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ClassRecommendationDto> getClassRecommendations()
    {
        LOG.debug("Retrieving all Class Recommendations");

        final List<ClassRecommendationDO> classRecommendationDOs = this.classRecommendationRepository.findAll();
        return this.mapper.mapAsList(classRecommendationDOs, ClassRecommendationDto.class);
    }

    @Override
    @Transactional
    public List<AllowedWorkItemAttributeValueDto> getAllowedWorkItemAttributeValues()
    {
        LOG.debug("Retrieving all Allowed Work Item Attribute Values");

        final List<AllowedWorkItemAttributeValueDO> valueDOs = this.allowedWorkItemAttributeValueRepository.findAll();
        return this.mapper.mapAsList(valueDOs, AllowedWorkItemAttributeValueDto.class);
    }

    @Override
    @Transactional
    public WorkItemActionDto findWorkItemActionFromReferenceCode(final String referenceCode)
    {
        LOG.debug("Finding Work Item Action matching the Reference Code %s", referenceCode);

        final WorkItemActionDO workItemAction = this.workItemActionRepository.findByReferenceCode(referenceCode.toLowerCase(Locale.getDefault()));
        return this.mapper.map(workItemAction, WorkItemActionDto.class);
    }

    @Override
    public List<ReferenceDataDto> getJobCategories()
    {
        LOG.debug("Retrieving all Job Categories");

        final List<JobCategoryDO> jobCategoryDOs = this.jobCategoryRepository.findAll();
        return this.mapper.mapAsList(jobCategoryDOs, ReferenceDataDto.class);
    }

    @Override
    public ReferenceDataDto getJobCategory(final Long categoryId)
    {
        LOG.debug("Finding all Job Categorie with ID %s", categoryId);

        final JobCategoryDO jobCategoryDO = this.jobCategoryRepository.findOne(categoryId);
        return this.mapper.map(jobCategoryDO, ReferenceDataDto.class);
    }
}
