package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.utils.VersioningHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDefectValueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectValueDO;
import com.baesystems.ai.lr.domain.mast.repositories.DefectRepository;
import com.baesystems.ai.lr.domain.mast.repositories.DefectStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPDefectRepository;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.enums.DefectStatusType;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.JobDaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "DefectDao")
public class DefectDaoImpl implements DefectDao
{
    private static final Logger LOG = LoggerFactory.getLogger(DefectDaoImpl.class);

    @Autowired
    private DefectRepository defectRepository;

    @Autowired
    private WIPDefectRepository wipDefectRepository;

    @Autowired
    private DefectStatusRepository defectStatusRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private SequenceDao sequenceDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private VersioningHelper versioningHelper;

    private final MapperFacade mapper;

    public DefectDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(DefectDO.class, DefectDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectDO.class, DefectLightDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectDefectValueDO.class, DefectDefectValueDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectValueDO.class, DefectValueDto.class).byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public DefectDto getDefect(final Long defectId)
    {
        LOG.debug("Retrieving Defect with ID {}", defectId);

        final DefectDO defect = this.defectRepository.findOne(defectId);
        return defect == null ? null : mapper.map(defect, DefectDto.class);
    }

    @Override
    @Transactional
    public DefectLightDto getDefectLight(final Long defectId)
    {
        LOG.debug("Retrieving Defect with ID {}", defectId);

        final DefectDO defect = this.defectRepository.findOne(defectId);
        final DefectLightDto defectDto = mapper.map(defect, DefectLightDto.class);

        if (defectDto != null)
        {
            for (final DefectItemDto defectItemDto : defectDto.getItems())
            {
                final VersionedItemPK itemPK = this.versioningHelper.getVersionedItemPK(defectItemDto.getItem().getId(), defectDto.getAsset().getId());
                final ItemLightDto itemLightDto = this.itemDao
                        .getItemLight(itemPK);
                defectItemDto.setItem(itemLightDto);
            }
        }

        return defectDto;
    }

    @Override
    @Transactional
    public DefectDto createDefect(final DefectDto defectDto)
    {
        DefectDO defect = this.mapper.map(defectDto, DefectDO.class);
        JobDaoUtils.updateDefectFields(defect);
        defect.setSequenceNumber(
                this.sequenceDao.getNextAssetSpecificSequenceValue(defectDto.getAsset().getId(), false, defectRepository, wipDefectRepository));
        defect = this.defectRepository.merge(defect);
        return this.mapper.map(defect, DefectDto.class);
    }

    @Override
    @Transactional
    public DefectDto updateDefect(final DefectDto defectDto)
    {
        LOG.debug("Updating Defect with ID {}", defectDto.getId());

        DefectDO defect = this.mapper.map(defectDto, DefectDO.class);

        JobDaoUtils.updateDefectFields(defect);

        defect = this.defectRepository.merge(defect);
        return this.mapper.map(defect, DefectDto.class);
    }

    @Override
    @Transactional
    public Boolean closeDefect(final Long defectId)
    {
        final DefectStatusDO status = defectStatusRepository.findByName("Closed");
        final int updateCount = this.defectRepository.updateStatus(defectId, status);
        return updateCount == 1;
    }

    @Override
    @Transactional
    public PageResource<DefectDto> getDefectsForAsset(final Pageable pageable, final Long assetId, final Long statusId)
    {
        LOG.debug("Retrieving Defects for Asset ID: {} with status ID: {}", assetId, statusId);
        final Page<DefectDO> output = defectRepository.findDefectsByAsset(pageable, assetId, statusId);
        final Page<DefectDto> result = output.map(defect -> mapper.map(defect, DefectDto.class));
        return pageFactory.createPageResource(result, DefectPageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean defectExists(final Long defectId)
    {
        LOG.debug("Testing if with ID {} exist", defectId);

        return this.defectRepository.exists(defectId);
    }

    @Override
    @Transactional
    public Boolean doAnyItemsHaveOpenDefects(final List<Long> itemIds)
    {
        return this.defectRepository.doAnyItemsHaveOpenDefects(itemIds, DefectStatusType.getDefectStatusesForOpen());
    }

    /**
     * Gets all Defects for an asset with a query
     *
     * @param pageable, page specification
     * @param assetId, Asset ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All Defects for an asset in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<DefectDto> findAllForAsset(final Pageable pageable, final Long assetId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<DefectDO> resultPage = null;
        if (codicilDefectQueryDto == null)
        {
            resultPage = defectRepository.findAll(pageable, assetId);
        }
        else
        {
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);
            resultPage = defectRepository.findAll(pageable, assetId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilDefectQueryDto.getStatusList(), itemIdList);
        }
        final Page<DefectDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, this.mapper, DefectDto.class);
        return pageFactory.createPageResource(result, DefectPageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean defectExistsForAsset(final Long assetId, final Long defectId)
    {
        final Integer existsCount = defectRepository.defectExistsForAsset(assetId, defectId);
        return existsCount > 0;
    }
}
