package com.baesystems.ai.lr.dao.reports;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.reports.ReportDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ReportRepository;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "ReportDao")
public class ReportDaoImpl extends AbstractLockingDaoImpl<ReportDO> implements ReportDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ReportDao.class);

    @Autowired
    private ReportRepository reportRepository;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(ReportDO.class, ReportDto.class)
                .customize(new StaleObjectMapper<ReportDO, ReportDto>())
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ReportDO.class, ReportLightDto.class)
                .customize(new StaleObjectMapper<ReportDO, ReportLightDto>())
                .byDefault().toClassMap());
    }

    public ReportDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public ReportDto getReport(final Long reportId)
    {
        LOG.debug("getReport {}", reportId);

        final ReportDO report = this.reportRepository.findOne(reportId);
        return report == null ? null : getMapper().map(report, ReportDto.class);
    }

    @Override
    @Transactional
    public List<ReportLightDto> getReports(final Long jobId)
    {
        final List<ReportDO> output = this.reportRepository.findReportsForJob(jobId);
        return getMapper().mapAsList(output, ReportLightDto.class);
    }

    @Override
    @Transactional
    public Long getNextReportVersion(final Long jobId, final Long reportTypeId)
    {
        return this.reportRepository.findNextReportVersion(jobId, reportTypeId);
    }

    @Override
    @Transactional
    public ReportDto saveReport(final ReportDto reportDto)
    {
        LOG.debug("saveReport {}", reportDto.getId());

        ReportDO report = getMapper().map(reportDto, ReportDO.class);
        report = this.reportRepository.merge(report);
        return getMapper().map(report, ReportDto.class);
    }

    @Override
    @Transactional
    public ReportDto getReport(final Long jobId, final Long typeId, final Long reportVersion)
    {
        final ReportDO report = this.reportRepository.getReport(jobId, typeId, reportVersion);
        return getMapper().map(report, ReportDto.class);
    }

    @Override
    @Transactional
    public Boolean reportExistsForJob(final Long reportId, final Long jobId)
    {
        LOG.debug(String.format("Verifying that report %d exists for job %d", reportId, jobId));
        return this.reportRepository.reportExistsForJob(reportId, jobId);
    }

    @Override
    public LockingRepository<ReportDO> getLockingRepository()
    {
        return this.reportRepository;
    }
}
