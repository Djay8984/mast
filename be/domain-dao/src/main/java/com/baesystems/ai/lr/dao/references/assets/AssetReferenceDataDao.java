package com.baesystems.ai.lr.dao.references.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.references.AllowedAssetAttributeValueDto;
import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ClassDepartmentDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import com.baesystems.ai.lr.dto.validation.AttributeRuleDto;

public interface AssetReferenceDataDao
{
    List<AttributeRuleDto> getAttributeRules();

    ReferenceDataDto getIacsSociety(Long id);

    List<ReferenceDataDto> getIacsSocieties();

    SocietyRulesetDto getRuleSet(Long id);

    ReferenceDataDto getProductRuleSet(Long id);

    List<SocietyRulesetDto> getRuleSets(String category, Boolean isLrRuleset);

    List<AssetCategoryDto> getAssetCategories();

    AssetCategoryDto getAssetCategory(Long assetCategoryId);

    List<AssetTypeDto> getAssetTypes();

    AssetTypeDto getAssetType(Long assetTypeId);

    List<ReferenceDataDto> getCustomerFunctions();

    ReferenceDataDto getCustomerFunction(Long customerFunctionId);

    List<ReferenceDataDto> getCustomerRelationships();

    ReferenceDataDto getCustomerRelationship(Long customerRelationshipId);

    List<AttributeTypeDto> getAttributeTypes();

    AttributeTypeDto getAttributeType(Long attributeTypeId);

    List<AttributeTypeDto> getDefaultAndMandatoryAttributeTypesForItemType(Long itemTypeId);

    ReferenceDataDto getAttributeTypeValue(Long typeId);

    List<ReferenceDataDto> getAttributeTypeValues();

    List<ReferenceDataDto> getAssetLifecycleStatuses();

    List<ReferenceDataDto> getClassStatuses();

    List<AllowedAssetAttributeValueDto> getAllowedAssetAttributeValues();

    List<ReferenceDataDto> getItemRelationshipTypes();

    ReferenceDataDto getClassStatus(Long classStatusId);

    ClassDepartmentDto getClassDepartment(Long classDepartmentId);

    ReferenceDataDto getAssetLifecycleStatus(Long lifecycleStatusId);

    List<ReferenceDataDto> getProductRuleSets();

    List<ReferenceDataDto> getCountries();

    List<ReferenceDataDto> getClassMaintenanceStatuses();

    List<ClassDepartmentDto> getClassDepartments();

    List<ReferenceDataDto> getActionsTaken();

    List<ReferenceDataDto> getClassNotations();

    List<ReferenceDataDto> getMigrationStatuses();

}
