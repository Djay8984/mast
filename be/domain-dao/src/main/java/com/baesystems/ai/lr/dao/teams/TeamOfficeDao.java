package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.dto.references.TeamOfficeDto;

import java.util.List;

public interface TeamOfficeDao
{
    List<TeamOfficeDto> findAll();
}
