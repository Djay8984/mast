package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPersistDO;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRelationshipPersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemPersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.utils.CollectionUtils;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.TooManyMethods")
@Repository(value = "ItemPersistDao")
public class ItemPersistDaoImpl implements ItemPersistDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ItemPersistDaoImpl.class);

    @Autowired
    private VersionedItemRepository versionedItemRepository;

    @Autowired
    private VersionedItemPersistRepository versionedItemPersistRepository;

    @Autowired
    private ItemRelationshipPersistRepository itemRelationshipPersistRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ItemDao itemDao;

    private final MapperFacade mapper;

    public ItemPersistDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    @Transactional
    @Override
    public ItemMetaDto createItemMeta(final Long assetId)
    {
        ItemDO itemDO = new ItemDO();
        itemDO.setAsset(new AssetDO());
        itemDO.getAsset().setId(assetId);
        itemDO = this.itemRepository.save(itemDO);
        return this.mapper.map(itemDO, ItemMetaDto.class);
    }

    @Transactional
    @Override
    public ItemRelationshipDto createItemRelationship(final Long assetId, final Long itemId, final ItemRelationshipDto itemRelationshipDto)
    {
        final Long draftAssetVersion = this.assetDao.getDraftVersion(assetId);

        ItemRelationshipPersistDO itemRelationshipDO = this.mapper.map(itemRelationshipDto, ItemRelationshipPersistDO.class);
        itemRelationshipDO.setAssetId(assetId);
        itemRelationshipDO.setFromAssetId(assetId);
        itemRelationshipDO.setFromItemId(itemId);
        itemRelationshipDO.setFromVersionId(draftAssetVersion);
        itemRelationshipDO.setToItemId(itemRelationshipDto.getToItem().getId());
        itemRelationshipDO.setToVersionId(draftAssetVersion);
        itemRelationshipDO.setToItem(null);

        itemRelationshipDO = this.itemRelationshipPersistRepository.save(itemRelationshipDO);
        return itemRelationshipDO == null ? null : this.mapper.map(itemRelationshipDO, ItemRelationshipDto.class);
    }

    @Transactional
    @Override
    public void deleteItemRelationship(final Long relationshipId)
    {
        LOG.debug("Deleting item relationship with ID {}", relationshipId);
        this.itemRelationshipPersistRepository.delete(relationshipId);
    }

    @Transactional
    @Override
    public void updateItemName(final Long itemId, final String itemName)
    {
        LOG.debug("Updating Item {} with name {}", itemId, itemName);
        this.versionedItemRepository.updateItemName(itemId, itemName);
    }

    /**
     * Sets an item to be reviewed along with all of its descendants.
     *
     * @param id the Item ID
     */
    @Transactional
    @Override
    public ItemLightDto setItemAndAllDescendantsToSuccessfullyReviewed(final Long id)
    {
        return this.setItemHierarchyReviewState(id, true);
    }

    /**
     * Sets an item's state to requiring a review, along with all its ancestors.
     *
     * @param id the Item ID
     */
    @Transactional
    @Override
    public ItemLightDto setItemAndAllAncestorsToRequireAReview(final Long id)
    {
        return this.setItemHierarchyReviewState(id, false);
    }

    /**
     * Marks a top-level item as deleted and then goes on to mark child items as deleted and relationships.
     * All the items are up-versioned and these up-versioned items are marked as deleted.
     * All CHILD relationships are up-versioned on both the 'to_item' and 'from_item'
     * The relationship of the PARENT of the top-level item is up-version on the 'to_item' only, as the parent remains as-is.
     * @param itemId the tip-level item ID
     * @param assetId the asset ID to which the item belongs.
     */
    @Override
    @Transactional
    public void deleteItem(final Long itemId, final Long assetId)
    {
        final Long draftVersionId = assetDao.getDraftVersion(assetId);
        final VersionedItemPK draftItemPK = new VersionedItemPK(itemId, assetId, draftVersionId);
        final List<Long> itemIdsToUpdate = new ArrayList<>();

        if (!this.itemDao.itemExists(draftItemPK))
        {
            this.createVersionedItemForDeletion(itemId, assetId);
            this.upversionInverseRelationships(assetId, itemId, draftVersionId, false);
        }

        itemIdsToUpdate.add(itemId);

        final LazyItemDto mainItemForDeletion = this.itemDao.getItem(draftItemPK);

        final Integer deletedItemDisplayOrder = mainItemForDeletion.getDisplayOrder();
        final LinkVersionedResource lrDeletedItemParent = mainItemForDeletion.getParentItem();

        for (final ItemDto childItem : this.itemDao.getItemChildList(draftItemPK))
        {
            if (!draftVersionId.equals(childItem.getAssetVersionId()))
            {
                this.createVersionedItemForDeletion(childItem.getId(), assetId);
                this.upversionInverseRelationships(assetId, childItem.getId(), draftVersionId, true);
            }
            itemIdsToUpdate.add(childItem.getId());
        }

        // Delete top-level item, its children and relationships
        this.versionedItemPersistRepository.bulkLogicalDelete(itemIdsToUpdate, draftVersionId);
        this.itemRelationshipPersistRepository.bulkLogicalDeleteByItemId(itemIdsToUpdate, itemId, draftVersionId, draftVersionId);

        // Get the parent of the top-level item, up-version the relationship on the 'toItem' end and delete.
        if (lrDeletedItemParent != null)
        {
            this.itemRelationshipPersistRepository
                    .bulkLogicalDeleteByItemId(Arrays.asList(itemId), lrDeletedItemParent.getId(), mainItemForDeletion.getParentItem().getVersion(), draftVersionId);
        }

        if (deletedItemDisplayOrder != null && lrDeletedItemParent != null)
        {
            final ItemMetaDto itemMetaDto = this.itemDao.getItemMeta(lrDeletedItemParent.getId());
            final ItemDto itemDto = this.itemDao
                    .getFullItem(new VersionedItemPK(lrDeletedItemParent.getId(), itemMetaDto.getAsset().getId(), null),
                            true);
            final List<Long> siblings = CollectionUtils.nullSafeStream(itemDto.getItems())
                    .map(dto -> dto.getId())
                    .collect(Collectors.toList());
            if (!siblings.isEmpty())
            {
                this.versionedItemPersistRepository.reorganiseDisplayOrdersAfterDelete(deletedItemDisplayOrder, siblings);
            }
        }
    }

    private ItemLightDto setItemHierarchyReviewState(final Long id, final boolean reviewed)
    {
        final VersionedItemDO itemDO = this.versionedItemRepository.findOne(new VersionedItemPK(id, 1L, 1L));

        // Create the set of item ids that require their reviewed state to be altered.
        final Set<Long> setOfItemIdsToBeUpdated = new HashSet<Long>();
        this.getSetOfItemIdsToBeSet(itemDO, reviewed, setOfItemIdsToBeUpdated);
        if (!setOfItemIdsToBeUpdated.isEmpty())
        {
            LOG.debug("Setting item ids {} to the review state: {}.", setOfItemIdsToBeUpdated, reviewed);
            this.updateItemIdSetReviewedCase(reviewed, setOfItemIdsToBeUpdated);
        }

        return itemDO == null ? null : this.mapper.map(itemDO, ItemLightDto.class);
    }

    private void updateItemIdSetReviewedCase(final boolean reviewed, final Set<Long> processedIds)
    {
        this.versionedItemRepository.updateReviewedStateOfItems(reviewed, processedIds);
    }

    @SuppressWarnings("PMD.EmptyBlock")
    private void getSetOfItemIdsToBeSet(final VersionedItemDO itemDO, final boolean reviewed, final Set<Long> processedIds)
    {
        if (itemDO != null)
        {
            // In the unlikely event the hierarchy has a loop, prevent further processing of this branch.
            if (!processedIds.contains(itemDO.getId()))
            {
                this.addToSetIfReviewedStateDifferent(itemDO, reviewed, processedIds);
                if (reviewed)
                {
                    // Pointless statement to pass PMD - while versioning branch is still unstable.
                    itemDO.setId(itemDO.getId());

                    // if (itemDO.getItems() != null)
                    // {
                    // for (final VersionedItemDO child : itemDO.getItems())
                    // {
                    // getSetOfItemIdsToBeSet(child, reviewed, processedIds);
                    // }
                    // }
                }
                else
                {

                    // todo getParent dummy code for pmd
                    itemDO.setId(itemDO.getId());
                    // getSetOfItemIdsToBeSet(itemDO.getParentItem(), reviewed, processedIds);
                }
            }
            else
            {
                LOG.warn("Item id {} has already been found. Please check the object hierarchy for loops.", itemDO.getId(),
                        reviewed);
            }
        }
    }

    /**
     * Add the item id to the set of reviewed states to be updated if the passed reviewed flag is different from what is
     * in the database.
     *
     * @param itemDO
     * @param reviewed
     * @param processedIds
     */
    private void addToSetIfReviewedStateDifferent(final VersionedItemDO itemDO, final boolean reviewed, final Set<Long> processedIds)
    {
        if (itemDO.getReviewed() == null || itemDO.getReviewed() != reviewed)
        {
            processedIds.add(itemDO.getId());
        }
    }

    @Override
    @Transactional
    public ItemLightDto createAssetRootItem(final Long assetId, final ItemLightDto newItem)
    {
        VersionedItemPersistDO newItemDO = this.mapper.map(newItem, VersionedItemPersistDO.class);
        newItemDO = this.versionedItemPersistRepository.merge(newItemDO);
        return this.mapper.map(newItemDO, ItemLightDto.class);
    }

    @Transactional
    @Override
    public ItemLightDto upVersionItem(final Long itemId, final Long assetId)
    {
        final Long draftVersion = this.assetDao.getDraftVersion(assetId);
        final VersionedItemPersistDO newVersionDO = getUpVersionedItem(itemId, assetId, draftVersion);

        CollectionUtils.nullSafeStream(newVersionDO.getAttributes()).forEach(attributePersistDO ->
        {
            attributePersistDO.setAssetVersionId(draftVersion);
            attributePersistDO.setId(null);
        });
        CollectionUtils.nullSafeStream(newVersionDO.getRelated()).forEach(relationshipPersistDO ->
        {
            relationshipPersistDO.setFromVersionId(draftVersion);
            relationshipPersistDO.setId(null);
        });

        this.versionedItemPersistRepository.merge(newVersionDO);

        return this.mapper.map(newVersionDO, ItemLightDto.class);
    }

    /**
     * This method is intended for deleting items and the handling of relationships is done manually
     * because when deleting an item, all its children are changing too i.e. they are deleted. When
     * using the createVersionedItem() method, it is not expected the children of the item being
     * upversioned will change.
     *
     * @param itemId  the item to be up-versioned
     * @param assetId the asset id to which the item belongs.
     * @return an upversioned ItemLightDto
     */
    @Override
    @Transactional
    public ItemLightDto createVersionedItemForDeletion(final Long itemId, final Long assetId)
    {
        final Long draftVersion = this.assetDao.getDraftVersion(assetId);
        final VersionedItemPersistDO newVersionDO = getUpVersionedItem(itemId, assetId, draftVersion);

        // Empty the item relationship as they point to the published versions and will be 'touched'
        // during the merge below and last_update_date will change which is NOT desired
        newVersionDO.setRelated(new ArrayList<>());

        // I can upversion the attributes and mark as 'delete' here as they are far simpler to handle
        // compared to ItemRelationships. This saves having to do an explicit up-version and bulk delete.
        CollectionUtils.nullSafeStream(newVersionDO.getAttributes())
                .forEach(attribute -> {
                    attribute.setAssetVersionId(draftVersion);
                    attribute.setId(null);
                    attribute.setDeleted(true);
                });

        this.versionedItemPersistRepository.merge(newVersionDO);

        return this.mapper.map(newVersionDO, ItemLightDto.class);
    }

    private VersionedItemPersistDO getUpVersionedItem(final Long itemId, final Long assetId, final Long draftVersion)
    {
        final Long publishedVersion = this.assetDao.getPublishedVersion(assetId);

        final VersionedItemPersistDO previousVersionDO = this.versionedItemPersistRepository.findOne(itemId, assetId, publishedVersion);

        final VersionedItemPersistDO newVersionDO = this.mapper.map(previousVersionDO, VersionedItemPersistDO.class);
        newVersionDO.setAssetVersionId(draftVersion);

        return newVersionDO;
    }

    @Transactional
    @Override
    public void upversionInverseRelationships(final Long assetId, final Long itemId, final Long draftVersionId, final Boolean linkFromDraftVersion)
    {
        final Long publishedVersionId = this.assetDao.getPublishedVersion(assetId);
        final List<ItemRelationshipPersistDO> relationships = this.itemRelationshipPersistRepository
                .getInverseRelationships(itemId, assetId, publishedVersionId, null);

        final List<ItemRelationshipPersistDO> newRelationships = this.mapper.mapAsList(relationships, ItemRelationshipPersistDO.class);

        CollectionUtils.nullSafeStream(newRelationships).forEach(relationship ->
        {
            relationship.setId(null);

            if (linkFromDraftVersion)
            {
                relationship.setFromVersionId(draftVersionId);
            }

            relationship.setToVersionId(draftVersionId);
            relationship.setFromItem(null);
            relationship.setToItem(null);
            this.itemRelationshipPersistRepository.save(relationship);
        });
    }

    @Transactional
    @Override
    public void updateDecommissionedFlag(final Boolean flagState, final Long assetId, final List<Long> itemIds, final Long versionId)
    {
        this.versionedItemRepository.updateDecommissionedFlag(flagState, assetId, itemIds, versionId);
    }

    @Override
    @Transactional
    public void publishItems(final Long assetId, final Long draftVersionId)
    {
        final List<Long> updatedIds = this.versionedItemRepository.findUpdatedIds(assetId, draftVersionId);

        if (updatedIds != null && !updatedIds.isEmpty())
        {
            this.versionedItemPersistRepository.unpublishUpdated(updatedIds, draftVersionId);
            this.versionedItemPersistRepository.publishUpdates(updatedIds, draftVersionId);
        }
    }
}
