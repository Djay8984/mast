package com.baesystems.ai.lr.dao.references.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;

public interface ItemTypeDao
{
    List<ItemRuleDto> getItemTypeRelationships();

    List<ItemTypeDto> getItemTypes();

    ItemTypeDto getItemType(Long itemTypeId);

    String getItemTypeName(Long itemTypeId);

    ItemTypeDto getRootItemTypeForAssetCategory(Integer assetCategoryId);

    Integer getMaxOccursForItemType(Long parentTypeId, Long childTypeId, Integer assetCategoryId);

    Boolean itemTypeExists(Long itemTypeId);
}
