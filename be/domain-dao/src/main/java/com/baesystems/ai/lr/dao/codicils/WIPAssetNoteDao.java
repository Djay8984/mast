package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPAssetNoteDO;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPAssetNoteDao extends BaseAssetNoteDao, WIPCodicilDefectQueryDao<AssetNoteDto>, LockingDao<WIPAssetNoteDO>
{
    Boolean assetNoteExistsForJob(Long jobId, Long assetNoteId);

    void deleteAssetNote(Long assetNoteId);

    Boolean exists(Long assetNoteId);

    List<AssetNoteDto> getAssetNotesForJob(Long jobId);

    List<AssetNoteDto> getAssetNotesForJob(Long jobId, String employeeName, Date lastUpdatedDate);

    PageResource<AssetNoteDto> getAssetNotesForJob(Pageable pagable, Long jobId) throws BadPageRequestException;

    List<AssetNoteDto> getAssetNotesForJob(Long jobId, List<Long> statusList);

    Integer updateScopeConfirmed(Long jobId, Boolean scopeConfirmed, List<Long> statusList);

    List<AssetNoteDto> getAssetNotesForReportContent(Long jobId);
}
