package com.baesystems.ai.lr.dao.customers;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerDO;
import com.baesystems.ai.lr.utils.HardDeletingDao;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;

public interface PartyDao extends HardDeletingDao
{
    /**
     * Get a {@link PartyDto} for its identifier
     *
     * @param customerId
     * @return
     */
    PartyDto getCustomer(Long customerId);

    List<PartyLightDto> findAll();

    PartyDto saveCustomer(PartyDto customer);

    PageResource<CustomerLinkDto> getAssetCustomers(Pageable pageable, Long assetId);

    List<AssetCustomerDO> getAssetCustomersForVersionedAsset(Long assetId, Long versionedAssetId);

}
