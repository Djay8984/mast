package com.baesystems.ai.lr.dao.cases;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface CaseMilestoneDao extends LockingDao<CaseMilestoneDO>
{
    CaseMilestoneListDto getMilestonesForCase(Long caseId);

    CaseMilestoneDto updateMilestone(Long caseId, CaseMilestoneDto caseMilestone);

    CaseMilestoneDto getCaseMilestone(Long caseMilestoneId);

    CaseMilestoneDto getParentMilestone(Long caseMilestoneId, Long caseId);

    List<CaseMilestoneDto> getChildMilestones(List<Long> milestoneId, Long caseId);

    List<Long> getCaseMilestoneIdsForCase(Long caseId);

    Boolean isCaseMilestoneInScopeByCaseAndMilestoneId(Long milestoneId, Long caseId);

    Long getStatusIdMilestoneIdAndCaseId(Long milestoneId, Long caseId);

    Boolean areAllInScopeMilestonesCompleteForCase(Long caseId);
}
