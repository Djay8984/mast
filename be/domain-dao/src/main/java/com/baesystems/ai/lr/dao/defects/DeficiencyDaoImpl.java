package com.baesystems.ai.lr.dao.defects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.defects.DeficiencyDO;
import com.baesystems.ai.lr.domain.mast.repositories.DeficiencyRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "DeficiencyDao")
public class DeficiencyDaoImpl extends AbstractLockingDaoImpl<DeficiencyDO> implements DeficiencyDao
{
    public static final Logger LOG = LoggerFactory.getLogger(DeficiencyDaoImpl.class);
    private static final DefaultMapperFactory FACTORY;

    @Autowired
    private DeficiencyRepository deficiencyRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(DeficiencyDO.class, DeficiencyDto.class)
                .customize(new StaleObjectMapper<DeficiencyDO, DeficiencyDto>())
                .byDefault().toClassMap());
    }

    public DeficiencyDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<DeficiencyDO> getLockingRepository()
    {
        return this.deficiencyRepository;
    }

    /**
     * Gets all Deficiencies for an asset with a query
     *
     * @param pageable, page specification
     * @param assetId, Asset ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All Deficiencies for an asset in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<DeficiencyDto> findAllForAsset(final Pageable pageable, final Long assetId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        //LRD-4524 - currently search filters are not implemented so we are not using the query DTO.
        //This will be implemented when the story requiring the filters is picked up - see the Defect
        //equivalent for how to implement this.
        final Page<DeficiencyDO> resultPage = deficiencyRepository.findAll(pageable, assetId);
        final Page<DeficiencyDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), DeficiencyDto.class);
        return pageFactory.createPageResource(result, DeficiencyPageResourceDto.class);
    }
}
