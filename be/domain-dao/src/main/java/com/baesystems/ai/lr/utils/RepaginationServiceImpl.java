package com.baesystems.ai.lr.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Repository;

import com.baesystems.ai.lr.domain.entities.MultiDO;
import com.baesystems.ai.lr.exception.BadPageRequestException;

@Repository(value = "RepaginationService")
public class RepaginationServiceImpl<T extends MultiDO> extends RepaginationUtils<T> implements RepaginationService<T>
{

    private List<Order> orders = new ArrayList<Order>();
    private long total = -1L;

    /**
     * This method takes two ordered lists merges them to together into a single ordered list and then performs the
     * calculations to extract the required chuck and returns a page.
     */
    @Override
    public Page<T> repaginate(final Pageable pageInfo, final long enteredTotal,
            final List<T> list1, final String list1PropertyName,
            final List<T> list2, final String list2PropertyName) throws BadPageRequestException
    {
        if (list1 == null || list2 == null)
        {
            throw new BadPageRequestException(LISTS_CANNOT_BE_NULL);
        }

        // set the class level variables used throughout.
        this.setClassLevelVariables(pageInfo, list1PropertyName, list2PropertyName, enteredTotal);
        Page<T> page = null;

        if (pageInfo.getOffset() < enteredTotal)
        {
            this.mergeLists(list1, list2);
            this.pairUp(list1);

            final int lowerLimit = pageInfo.getOffset();
            int upperLimit = pageInfo.getOffset() + pageInfo.getPageSize();
            upperLimit = upperLimit > (int) this.total ? (int) this.total : upperLimit;

            page = new PageImpl<T>(list1.subList(lowerLimit, upperLimit), pageInfo, this.total);
        }
        else
        { // if the page requested is beyond the end of the list then don't bother sorting, just get the length
            while (!list2.isEmpty())
            {
                if (!list1.contains(list2.get(0)))
                { // need to remove any duplicates to get the right length
                    list1.add(list2.get(0));
                }
                list2.remove(0);
            }
            this.total = list1.size();
            page = new PageImpl<T>(new ArrayList<T>(), pageInfo, this.total);
        }

        return page;
    }

    /**
     * Method to set the variables that are used throughout the class and the super class.
     */
    @SuppressWarnings("unchecked")
    private void setClassLevelVariables(final Pageable pageInfo, final String list1PropertyName, final String list2PropertyName,
            final long enteredTotal)
    {
        super.setClassLevelVariables(list1PropertyName, list2PropertyName);
        this.orders = pageInfo.getSort() == null || pageInfo.getSort().iterator() == null ? this.orders
                : IteratorUtils.toList(pageInfo.getSort().iterator());
        this.total = enteredTotal;
    }

    /**
     * Take two ordered lists and merge them into a single oredered list
     */
    private void mergeLists(final List<T> list1, final List<T> list2) throws BadPageRequestException
    {
        if (!list2.isEmpty())
        {
            for (int i = 0; i < list1.size(); ++i)
            { // go through the first list and find out where the head of the second list should go
                if (!this.comesBefore(list1.get(i), list2.get(0)))
                { // if the head of the second list belongs here add it and then move the head of the second list on
                    list1.add(i, list2.get(0));
                    list2.remove(0);

                    if (list2.isEmpty())
                    { // stop once all elements have been added
                        break;
                    }
                }
            }
            list1.addAll(list2); // if the loop ends before all of the second list has been move then anything that is
                                 // left must go at the end.
        }
        this.total = list1.size(); // just in case the total is wrong (e.g. if a list has been changed).
    }

    /**
     * Method to search a list of wrapper objects testing if their contents are equal and can be paired up.
     */
    private void pairUp(final List<T> list)
    {
        for (int i = 0; i < list.size(); ++i)
        {
            for (int j = i + 1; j < list.size(); ++j)
            {
                if (list.get(i).equals(list.get(j)))
                { // if the elements are equal according to the wrapper class equals operator then use the getContent
                  // and set content of the wrapper to get the correct field and put it in the right place.
                    list.get(i).setContent(list.get(j).getContent());
                    list.remove(j);
                    --this.total;
                }
            }
        }
    }

    /**
     * Method to test whether one object comes before another using the same criteria as that used for sorting the
     * original list.
     */
    private boolean comesBefore(final Object obj1, final Object obj2) throws BadPageRequestException
    {
        boolean returnValue = false;

        for (final Order order : this.orders)
        { // keep going through all the sort fields in priority order until it can determine that one object should be
          // before the other.
            try
            {
                final Relationship relationship = this.getRelationship(order.getProperty(), order.isIgnoreCase(), obj1, obj2);

                if (!Relationship.EQ.equals(relationship))
                { // stop once we know that one is before the other
                  // if ascending order is required then a relationship of less that should give true, if not then
                  // greater than results in true.
                    returnValue = ASC.equals(order.getDirection().toString()) ? Relationship.LT.equals(relationship)
                            : Relationship.GT.equals(relationship);
                    break;
                }
            }
            catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
            {
                throw new BadPageRequestException(String.format(CANNOT_COMPARE, order.getProperty()), exception);
            }
        }

        return returnValue;
    }
}
