package com.baesystems.ai.lr.dao.assets;

import static com.baesystems.ai.lr.utils.CollectionUtils.nullSafeStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetWrapperDO;
import com.baesystems.ai.lr.domain.ihs.repositories.DummyIhsAssetRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsAssetWrapperRepository;
import com.baesystems.ai.lr.domain.mast.composite.entities.MultiAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.domain.mast.repositories.AssetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedAssetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetLightPageResourceDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetPageResourceDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.AssetDaoUtils;
import com.baesystems.ai.lr.utils.IhsAssetMapper;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.RepaginationService;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "AssetDao")
@SuppressWarnings("PMD.TooManyMethods")
public class AssetDaoImpl extends AbstractLockingDaoImpl<VersionedAssetDO> implements AssetDao
{
    private static final Logger LOG = LoggerFactory.getLogger(AssetDaoImpl.class);

    @Autowired
    private VersionedAssetRepository versionedAssetRepository;

    @Autowired
    private VersionedItemRepository versionedItemRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private IhsAssetWrapperRepository ihsAssetRepositoryMissingFieldsAllowed;

    @Autowired
    private RepaginationService<MultiAssetDO> repaginationService;

    @Autowired
    private DummyIhsAssetRepository dummyIhsAssetRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();

        FACTORY.registerClassMap(FACTORY.classMap(VersionedAssetDO.class, AssetDto.class)
                .field("assetItem", "assetModel.items[0]")
                .byDefault().toClassMap());

        FACTORY.registerClassMap(FACTORY.classMap(VersionedAssetDO.class, AssetLightDto.class)
                .customize(new StaleObjectMapper<VersionedAssetDO, AssetLightDto>())
                .byDefault().toClassMap());

        FACTORY.registerClassMap(FACTORY.classMap(AssetDO.class, AssetMetaDto.class)
                .customize(new AssetMetaMapper())
                .byDefault().toClassMap());

        FACTORY.registerClassMap(FACTORY.classMap(DummyIhsAssetDO.class, IhsAssetDetailsDto.class)
                .customize(new IhsAssetMapper())
                .byDefault().toClassMap());
    }

    public AssetDaoImpl()
    {
        super(FACTORY);
    }

    @PostConstruct
    public void allowMissingFields()
    {
        this.ihsAssetRepositoryMissingFieldsAllowed.setStrictMissingFields(false);
    }

    @Override
    public LockingRepository<VersionedAssetDO> getLockingRepository()
    {
        return this.versionedAssetRepository;
    }

    @Override
    @Transactional
    public AssetMetaDto createAssetMeta()
    {
        final AssetDO assetDO = this.assetRepository.merge(new AssetDO());
        return this.getMapper().map(assetDO, AssetMetaDto.class);
    }

    @Override
    @Transactional
    public final Boolean assetExists(final Long assetId)
    {
        return this.assetRepository.exists(assetId);
    }

    /**
     * Gets all assets with no query
     *
     * @param pageable, page specification.
     * @return All assets in database in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<AssetLightDto> findAll(final Pageable pageable)
    {
        final Page<VersionedAssetDO> output = this.versionedAssetRepository.findAll(pageable);
        final Page<AssetLightDto> result = output.map(asset -> this.getMapper().map(asset, AssetLightDto.class));
        return this.pageFactory.createPageResource(result, AssetLightPageResourceDto.class);
    }

    /**
     * Gets all assets that match the query parameters specified in {@link AssetQueryDto} query. Nulls are equivalent to
     * sql *
     *
     * @param pageable, page specification.
     * @param {@link AssetQueryDto} query, object containing query parameters.
     * @return All assets in database matching the query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<AssetLightDto> findAll(final Pageable pageable, final AssetQueryDto query)
    {
        if (query.getItemTypeId() != null)
        {
            List<AttributeDO> attributes = new ArrayList<AttributeDO>();
            if (query.getAttributeTypeId() != null)
            {
                attributes = this.attributeRepository.findAttributesByType(query);
                attributes = nullSafeStream(attributes)
                        .filter(attribute -> query.getOperator().include(query.getAttributeValue(), attribute.getValue()))
                        .collect(Collectors.toList());
            }
            if (!attributes.isEmpty() || null == query.getAttributeTypeId())
            {
                query.setIdList(this.versionedItemRepository.findPublishedAssetByItemType(query));
            }
        }

        final Page<VersionedAssetDO> output = this.versionedAssetRepository.findAll(pageable, query);
        final Page<AssetLightDto> result = output.map(asset -> this.getMapper().map(asset, AssetLightDto.class));
        return this.pageFactory.createPageResource(result, AssetLightPageResourceDto.class);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PageResource<MultiAssetDto> findAllFromBothDatabases(final Pageable pageable, final AbstractQueryDto query) throws BadPageRequestException
    {
        // query on the top level objects so that only the published version of the mast asset is queried against
        final List<AssetDO> output1 = this.assetRepository.findAll(pageable.getSort(), query);
        final List<IhsAssetWrapperDO> output2 = this.ihsAssetRepositoryMissingFieldsAllowed.findAll(pageable.getSort(), query);

        // Effectively unwrap the page information to remove the top level field name for the sort parameter
        Pageable newPagable = null;

        if (pageable.getSort() != null)
        {
            final List<Order> newOrders = new ArrayList<Order>();
            pageable.getSort()
                    .forEach(order -> newOrders.add(new Order(order.getDirection(), order.getProperty().replaceAll("^publishedVersion\\.", ""))));
            newPagable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(newOrders));
        }
        else
        {
            newPagable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize());
        }

        // unpack the results from the top level wrapper class and re package them into the multi-asset object, which
        // has a field for each
        final List<MultiAssetDO> list1 = new ArrayList<MultiAssetDO>();
        final List<MultiAssetDO> list2 = new ArrayList<MultiAssetDO>();
        output1.stream().forEach(element -> list1.add(new MultiAssetDO(null, element.getPublishedVersion())));
        output2.stream().forEach(element -> list2.add(new MultiAssetDO(element.getPublishedVersion(), null)));

        // merge the two list and paginate them
        final Page<MultiAssetDO> output = this.repaginationService.repaginate(newPagable,
                output1.size() + output2.size(), list1, "mastAsset", list2, "ihsAsset");

        // insert any asset that exist in both tables where only the asset in one table matches the query.
        nullSafeStream(output.getContent())
                .filter(asset -> asset.getImoNumber() != null && asset.getIhsAsset() == null)
                .forEach(asset -> asset.setIhsAsset(this.dummyIhsAssetRepository.findByImoNumber(asset.getImoNumber())));
        nullSafeStream(output.getContent())
                .filter(asset -> asset.getImoNumber() != null && asset.getMastAsset() == null)
                .forEach(asset -> asset.setMastAsset(this.versionedAssetRepository.findByImoNumber(asset.getImoNumber())));

        // map to dto
        final Page<MultiAssetDto> result = output.map(new Converter<MultiAssetDO, MultiAssetDto>()
        {
            @Override
            public MultiAssetDto convert(final MultiAssetDO asset)
            {
                return AssetDaoImpl.this.getMapper().map(asset, MultiAssetDto.class);
            }
        });
        return this.pageFactory.createPageResource(result, MultiAssetPageResourceDto.class);
    }

    /**
     * Gets all assets that match the query parameters specified in {@link AbstractQueryDto} query. Nulls are equivalent
     * to sql *
     *
     * @param pageable, page specification.
     * @param {@link AbstractQueryDto} query object containing query parameters.
     * @return All assets in database matching the query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<AssetLightDto> findAll(final Pageable pageable, final AbstractQueryDto query)
    {
        final Page<AssetDO> output = this.assetRepository.findAll(pageable, query);
        final Page<AssetLightDto> result = output.map(asset -> this.getMapper().map(asset.getPublishedVersion(), AssetLightDto.class));
        return this.pageFactory.createPageResource(result, AssetLightPageResourceDto.class);
    }

    /**
     * Get an {@link AssetLightDto} by its identifier.
     *
     * @param aVersionId version ID
     * @param assetId asset ID
     * @param returnClass the class for which and instance is to be returned
     * @return an instance of AssetLightDto or subclass
     */
    @Override
    @Transactional
    public <T extends AssetLightDto> T getAsset(final Long assetId, final Long aVersionId, final Class<T> returnClass)
    {
        final Long versionId = aVersionId != null ? aVersionId : this.getPublishedVersion(assetId);
        final VersionedAssetDO output = this.versionedAssetRepository.findOne(new VersionedAssetPK(assetId, versionId));
        return returnClass.cast(output == null ? null : this.getMapper().map(output, returnClass));
    }

    @Override
    @Transactional
    public AssetLightDto createAsset(final AssetLightDto assetLightDto)
    {
        VersionedAssetDO asset = this.getMapper().map(assetLightDto, VersionedAssetDO.class);
        AssetDaoUtils.updateAssetFields(asset);
        asset = this.versionedAssetRepository.merge(asset);
        return this.getMapper().map(asset, AssetLightDto.class);
    }

    /**
     * Get an {@link AssetLightDto} by IMO number, which is the IHS Asset identifier.
     *
     * @param imoNumber
     * @return {@link AssetLightDto}
     */
    @Override
    @Transactional
    public AssetLightDto getAssetByImoNumber(final Long imoNumber)
    {
        final VersionedAssetDO output = this.versionedAssetRepository.findByImoNumber(imoNumber);
        return output == null ? null : this.getMapper().map(output, AssetLightDto.class);
    }

    @Override
    @Transactional
    public void deleteAsset(final VersionedAssetPK assetPK)
    {
        this.versionedAssetRepository.delete(assetPK);
    }

    @Override
    @Transactional
    public void updateHullIndicator(final Long assetId, final Integer hullIndicator)
    {
        LOG.debug("Updating the Hull Indicator to {} for Asset with ID {}", hullIndicator, assetId);

        this.versionedAssetRepository.updateHullIndicator(assetId, hullIndicator);
    }

    @Override
    @Transactional
    public Integer countMatchingItemIds(final List<Long> itemIds)
    {
        return this.versionedItemRepository.countMatchingIds(itemIds);
    }

    @Override
    @Transactional
    public Long findAssetIdByImoOrBuilderAndYard(final Long ihsAssetId, final String builder, final String yardNumber)
    {
        return this.versionedAssetRepository.findAssetIdByImoOrBuilderAndYard(ihsAssetId, builder, yardNumber);
    }

    @Override
    @Transactional
    public final Long getAssetCategory(final Long assetId)
    {
        return this.versionedAssetRepository.findCategory(assetId);
    }

    @Override
    @Transactional
    public AssetLightDto updateAsset(final AssetLightDto assetLightDto)
    {
        VersionedAssetDO asset = this.getMapper().map(assetLightDto, VersionedAssetDO.class);
        AssetDaoUtils.updateAssetFields(asset);
        // set the root item AS it is not exposed on Dto
        asset.setAssetItem(this.itemRepository.getRootItem(asset.getId(), this.getPublishedVersion(asset.getId())));
        asset = this.versionedAssetRepository.merge(asset);
        return this.getMapper().map(asset, AssetLightDto.class);
    }

    @Override
    @Transactional
    public void setRootItemOnAsset(final AssetLightDto assetLightDto, final Long rootItemId)
    {
        final VersionedAssetPK versionedAssetPK = new VersionedAssetPK(assetLightDto.getId(), assetLightDto.getAssetVersionId());
        final VersionedAssetDO versionedAssetDO = this.versionedAssetRepository.findOne(versionedAssetPK);
        versionedAssetDO.setAssetItem(new ItemDO());
        versionedAssetDO.getAssetItem().setId(rootItemId);
        this.versionedAssetRepository.merge(versionedAssetDO);
    }

    @Override
    @Transactional
    public Date getAssetHarmonisationDate(final Long assetId)
    {
        return this.versionedAssetRepository.findAssetHarmonisationDate(assetId);
    }

    @Override
    @Transactional
    public AssetMetaDto updateAssetMeta(final AssetMetaDto assetMetaDto)
    {
        // Get latest and manually map (dual reference to publish / draft versionDO and Id causing issues)
        AssetDO assetDO = this.assetRepository.findOne(assetMetaDto.getId());
        assetDO.setPublishedVersionId(assetMetaDto.getPublishedVersionId());
        assetDO.setDraftVersionId(assetMetaDto.getDraftVersionId());
        assetDO.setCheckedOutBy(assetMetaDto.getCheckedOutBy());

        assetDO = this.assetRepository.merge(assetDO);
        return this.getMapper().map(assetDO, AssetMetaDto.class);
    }

    @Override
    @Transactional
    public AssetMetaDto getAssetMeta(final Long assetId)
    {
        final AssetDO assetDO = this.assetRepository.findOne(assetId);
        return this.getMapper().map(assetDO, AssetMetaDto.class);
    }

    @Override
    @Transactional
    public Long getPublishedVersion(final Long assetId)
    {
        return this.assetRepository.findOne(assetId).getPublishedVersionId();
    }

    @Override
    @Transactional
    public Long getDraftVersion(final Long assetId)
    {
        final AssetDO assetDO = this.assetRepository.findOne(assetId);

        return assetDO.getDraftVersionId() != null ? assetDO.getDraftVersionId() : assetDO.getPublishedVersionId();
    }

    @Override
    @Transactional
    public List<AssetMetaDto> getCheckedOutAssets(final String userId)
    {
        final List<AssetDO> assetDOs = this.assetRepository.getCheckedOutAssetsForUser(userId);
        return this.getMapper().mapAsList(assetDOs, AssetMetaDto.class);

    }

    @Override
    @Transactional
    public void hardDeleteAssetVersionedEntities(final Long assetId, final Long versionId)
    {
        this.versionedAssetRepository.hardDelete(assetId, versionId);
    }

    public static class AssetMetaMapper extends CustomMapper<AssetDO, AssetMetaDto>
    {
        @Override
        public void mapAtoB(final AssetDO assetDO, final AssetMetaDto assetMetaDto, final MappingContext context)
        {
            if (assetDO.getPublishedVersion() != null)
            {
                assetMetaDto.setName(assetDO.getPublishedVersion().getName());
            }
        }
    }
}
