package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.jobs.WipCertificateDO;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPCertificateDao extends LockingDao<WipCertificateDO>
{
    CertificateDto getCertificate(Long certificateId);

    PageResource<CertificateDto> getCertificatesForJob(Pageable pageable, Long jobId);

    List<CertificateDto> getCertificatesForJob(Long jobId);

    CertificateDto saveCertificate(CertificateDto certificateDto);

    Date getCertificateIssueDate(Long certificateId);
}
