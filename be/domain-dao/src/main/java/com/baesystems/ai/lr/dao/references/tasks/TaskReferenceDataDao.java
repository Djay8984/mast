package com.baesystems.ai.lr.dao.references.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface TaskReferenceDataDao
{
    List<ReferenceDataDto> getPostponementTypes();
}
