package com.baesystems.ai.lr.dao.ihs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.CompanyContactDO;
import com.baesystems.ai.lr.domain.ihs.entities.PersonnelContactDO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsOwgeRepository;
import com.baesystems.ai.lr.dto.ihs.CompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.enums.EmailConfigEnum;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "IhsContactDetailsDao")
public class IhsContactDetailsDaoImpl implements IhsContactDetailsDao
{
    @Autowired
    private IhsOwgeRepository ihsOwgeRepository;

    private final MapperFacade mapper;

    public IhsContactDetailsDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(CompanyContactDO.class, CompanyContactDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(PersonnelContactDO.class, IhsPersonnelContactDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<CompanyContactDto> getAddress(final Long imoNumber)
    {
        final List<CompanyContactDO> output = this.ihsOwgeRepository.getAddress(imoNumber, EmailConfigEnum.DEFAULT.getSequence(), EmailConfigEnum.DEFAULT.getEmailType());
        return output == null ? null : this.mapper.mapAsList(output, CompanyContactDto.class);
    }

    public List<IhsPersonnelContactDto> getPersonnelAddress(final Long imoNumber)
    {
        final List<PersonnelContactDO> output = this.ihsOwgeRepository.getPersonnelAddress(imoNumber);
        return  output == null ? null : this.mapper.mapAsList(output, IhsPersonnelContactDto.class);
    }

}
