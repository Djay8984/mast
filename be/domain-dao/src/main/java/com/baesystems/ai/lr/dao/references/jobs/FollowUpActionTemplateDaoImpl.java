package com.baesystems.ai.lr.dao.references.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.jobs.FollowUpActionDaoImpl;
import com.baesystems.ai.lr.domain.mast.entities.references.FollowUpActionTemplateDO;
import com.baesystems.ai.lr.domain.mast.repositories.FollowUpActionTemplateRepository;
import com.baesystems.ai.lr.dto.references.FollowUpActionTemplateDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "FollowUpActionTemplateDao")
public class FollowUpActionTemplateDaoImpl implements FollowUpActionTemplateDao
{
    public static final Logger LOG = LoggerFactory.getLogger(FollowUpActionDaoImpl.class);

    private final MapperFacade mapper;

    @Autowired
    private FollowUpActionTemplateRepository followUpActionTemplateRepository;

    public FollowUpActionTemplateDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(FollowUpActionTemplateDO.class, FollowUpActionTemplateDto.class)
        .byDefault().register();
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public FollowUpActionTemplateDto getTemplate(final Long templateId)
    {
        return this.mapper.map(this.followUpActionTemplateRepository.findOne(templateId), FollowUpActionTemplateDto.class);
    }
}
