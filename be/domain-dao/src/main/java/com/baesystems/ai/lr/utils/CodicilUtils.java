package com.baesystems.ai.lr.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

public final class CodicilUtils
{
    private CodicilUtils()
    {
    }

    /**
     * A Hibernate query using 'select new map()...' will return a list of maps, where each map represents a row and
     * contains an entry per column. This method restructures this into a single Map where each entry represents a
     * single template ID and its associated asset count.
     *
     * @param templateIds
     * @param rowData
     * @param templateColumnName
     * @param assetCountColumnName
     * @return Map of Template IDs and associated Asset Counts
     */
    public static Map<Long, Integer> populateTemplateCountMap(final List<Long> templateIds, final List<Map<String, Long>> rowData,
            final String templateColumnName, final String assetCountColumnName)
    {
        // Key = Template ID, Value = Asset Count
        final Map<Long, Integer> templateCountMap = new HashMap<Long, Integer>();

        if (rowData != null && !StringUtils.isEmpty(templateColumnName) && !StringUtils.isEmpty(assetCountColumnName))
        {
            mapRowData(templateCountMap, rowData, templateColumnName, assetCountColumnName);
        }

        if (templateIds != null)
        {
            // Some templates may not be returned by the query because no associated assets were found.
            // In that case, add an entry to the map with a zero count.
            addZeroEntries(templateCountMap, templateIds);
        }

        return templateCountMap;
    }

    private static void mapRowData(final Map<Long, Integer> templateCountMap, final List<Map<String, Long>> rowData, final String templateColumnName,
            final String assetCountColumnName)
    {
        for (final Map<String, Long> row : rowData)
        {
            Long templateId = null;
            Integer assetCount = null;

            for (final Entry<String, Long> column : row.entrySet())
            {
                if (templateColumnName.equals(column.getKey()))
                {
                    templateId = column.getValue();
                }
                else if (assetCountColumnName.equals(column.getKey()))
                {
                    assetCount = column.getValue().intValue();
                }
            }

            templateCountMap.put(templateId, assetCount);
        }
    }

    private static void addZeroEntries(final Map<Long, Integer> templateCountMap, final List<Long> templateIds)
    {
        for (final Long templateId : templateIds)
        {
            if (!templateCountMap.containsKey(templateId))
            {
                templateCountMap.put(templateId, 0);
            }
        }
    }
}
