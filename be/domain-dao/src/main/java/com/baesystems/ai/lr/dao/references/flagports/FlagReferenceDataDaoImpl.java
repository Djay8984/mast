package com.baesystems.ai.lr.dao.references.flagports;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.FlagStateDO;
import com.baesystems.ai.lr.domain.mast.repositories.FlagStateRepository;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.dto.references.FlagStatePageResourceDto;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "FlagDao")
public class FlagReferenceDataDaoImpl implements FlagReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(FlagReferenceDataDaoImpl.class);

    @Autowired
    private FlagStateRepository flagStateRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private final MapperFacade mapper;

    public FlagReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(FlagStateDO.class, FlagStateDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public FlagStateDto getFlagState(final Long flagStateId)
    {
        LOG.debug("Retrieving Flag State with ID {}", flagStateId);

        final FlagStateDO flagState = flagStateRepository.findOne(flagStateId);

        return flagState == null ? null : mapper.map(flagState, FlagStateDto.class);
    }

    @Override
    @Transactional
    public PageResource<FlagStateDto> findAll(final Pageable pageSpecification)
    {
        final Page<FlagStateDO> flagData = flagStateRepository.findAll(pageSpecification);
        return mapPageDoToPageDto(flagData);
    }

    @Override
    @Transactional
    public PageResource<FlagStateDto> findAll(final Pageable pageSpecification, final String search)
    {
        final Page<FlagStateDO> flagData = flagStateRepository.findAll(pageSpecification, search);
        return mapPageDoToPageDto(flagData);
    }

    private PageResource<FlagStateDto> mapPageDoToPageDto(final Page<FlagStateDO> pageData)
    {
        final Page<FlagStateDto> results = pageData.map(new Converter<FlagStateDO, FlagStateDto>()
        {
            @Override
            public FlagStateDto convert(final FlagStateDO flagState)
            {
                return mapper.map(flagState, FlagStateDto.class);
            }
        });

        return this.pageFactory.createPageResource(results, FlagStatePageResourceDto.class);
    }

    @Override
    @Transactional
    public List<FlagStateDto> getFlags()
    {
        LOG.debug("Retrieving all Flags");

        final List<FlagStateDO> flags = flagStateRepository.findAll();
        return mapper.mapAsList(flags, FlagStateDto.class);
    }
}
