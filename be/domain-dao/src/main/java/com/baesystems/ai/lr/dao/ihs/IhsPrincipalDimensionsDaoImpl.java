package com.baesystems.ai.lr.dao.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHibrDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHideDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHidrDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHileDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHiprDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsHitlDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsSsch2DO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsStdeDO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsAssetRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHibrRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHideRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHidrRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHileRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHiprRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsHitlRepository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsSsch2Repository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsStdeRepository;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;

@Repository(value = "IhsPrincipalDimensionsDao")
public class IhsPrincipalDimensionsDaoImpl implements IhsPrincipalDimensionsDao
{
    @Autowired
    private IhsAssetRepository ihsAssetRepository;

    @Autowired
    private IhsHileRepository ihsHileRepository;

    @Autowired
    private IhsHibrRepository ihsHibrRepository;

    @Autowired
    private IhsHidrRepository ihsHidrRepository;

    @Autowired
    private IhsHideRepository ihsHideRepository;

    @Autowired
    private IhsSsch2Repository ihsSsch2Repository;

    @Autowired
    private IhsHitlRepository ihsHitlRepository;

    @Autowired
    private IhsStdeRepository ihsStdeRepository;

    @Autowired
    private IhsHiprRepository ihsHiprRepository;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    @Transactional
    public IhsPrincipalDimensionsDto getPrincipalDimensions(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsPrincipalDimensionsDto dimensionDto = new IhsPrincipalDimensionsDto();
        final IhsAssetDO ihsAsset = this.ihsAssetRepository.findOne(imoNumber);
        final IhsHileDO ihsHile = this.ihsHileRepository.findOne(imoNumber);
        final IhsHibrDO ihsHibr = this.ihsHibrRepository.findOne(imoNumber);
        final IhsHidrDO ihsHidr = this.ihsHidrRepository.findOne(imoNumber);
        final IhsHideDO ihsHide = this.ihsHideRepository.findOne(imoNumber);
        final IhsSsch2DO ihsSsch2 = this.ihsSsch2Repository.findOne(imoNumber);
        final IhsHitlDO ihsHitl = this.ihsHitlRepository.findOne(imoNumber);
        final IhsStdeDO ihsStde = this.ihsStdeRepository.findOne(imoNumber);
        final IhsHiprDO ihsHipr = this.ihsHiprRepository.findOne(imoNumber);

        serviceHelper.verifyModel(ihsAsset, imoNumber);
        serviceHelper.verifyModel(ihsHibr, imoNumber);
        serviceHelper.verifyModel(ihsHide, imoNumber);
        serviceHelper.verifyModel(ihsHidr, imoNumber);
        serviceHelper.verifyModel(ihsHile, imoNumber);
        serviceHelper.verifyModel(ihsHipr, imoNumber);
        serviceHelper.verifyModel(ihsHitl, imoNumber);
        serviceHelper.verifyModel(ihsSsch2, imoNumber);
        serviceHelper.verifyModel(ihsStde, imoNumber);

        dimensionDto.setLengthOverall(ihsAsset.getLengthOverall());
        dimensionDto.setLengthBtwPerpendiculars(ihsHile.getLengthBtwPerpendiculars());
        dimensionDto.setBreadthMoulded(ihsHibr.getBreadthMoulded());
        dimensionDto.setBreadthExtreme(ihsHibr.getBreadthExtreme());
        dimensionDto.setDraughtMax(ihsHidr.getDraughtMax());
        dimensionDto.setDeadWeight(ihsHidr.getDeadWeight());
        dimensionDto.setDepthMoulded(ihsHide.getDepthMoulded());
        dimensionDto.setGrossTonnage(ihsSsch2.getGrossTonnage());
        dimensionDto.setNetTonnage(ihsHitl.getNetTonnage());
        dimensionDto.setDecks(ihsStde.getDecks());
        dimensionDto.setPropulsion(ihsHipr.getPropulsion());

        return dimensionDto;
    }
}
