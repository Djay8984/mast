package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;
import com.baesystems.ai.lr.domain.mast.repositories.CoCRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CoCDao")
public class CoCDaoImpl extends AbstractLockingDaoImpl<CoCDO> implements CoCDao
{
    public static final Logger LOG = LoggerFactory.getLogger(CoCDaoImpl.class);
    private static final DefaultMapperFactory FACTORY;

    @Autowired
    private CoCRepository coCRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private ItemDao itemDao;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(CoCDO.class, CoCDto.class)
                .customize(new StaleObjectMapper<CoCDO, CoCDto>())
                .byDefault().toClassMap());
    }

    public CoCDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public CoCDto getCoC(final Long coCId)
    {
        LOG.debug("Retrieving CoC for ID: {}", coCId);
        final CoCDO coCDO = this.coCRepository.findCoc(coCId);

        CoCDto coCDto = null;
        if (coCDO != null)
        {
            coCDto = this.getMapper().map(coCDO, CoCDto.class);
        }
        return coCDto;
    }

    @Override
    @Transactional
    public PageResource<CoCDto> getCoCsForAsset(final Pageable pageable, final Long assetId, final Long statusId)
    {
        LOG.debug("Retrieving CoCs for Asset ID: {}", assetId);
        final Page<CoCDO> output = this.coCRepository.findCoCByAsset(pageable, assetId, statusId);
        final Page<CoCDto> result = output.map(new CoCConverter());
        return pageFactory.createPageResource(result, CoCPageResourceDto.class);
    }

    @Override
    @Transactional
    public PageResource<CoCDto> getCoCsForDefect(final Pageable pageable, final Long assetId, final Long defectId, final Long statusId)
    {
        final Page<CoCDO> output = this.coCRepository.findCoCByDefect(pageable, assetId, defectId, statusId);

        final Page<CoCDto> result = output.map(new Converter<CoCDO, CoCDto>()
        {
            @Override
            public CoCDto convert(final CoCDO cocDO)
            {
                return getMapper().map(cocDO, CoCDto.class);
            }
        });

        return pageFactory.createPageResource(result, CoCPageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean coCExistsForAsset(final Long assetId, final Long cocId)
    {
        final Integer existsCount = this.coCRepository.coCExistsForAsset(assetId, cocId);
        return existsCount > 0;
    }

    @Override
    @Transactional
    public CoCDto saveCoC(final CoCDto coCDto, final Long assetId)
    {
        LOG.debug("Saving CoC for Asset ID: {}", assetId);
        CoCDO coCDO = getMapper().map(coCDto, CoCDO.class);
        coCDO.setAsset(new AssetDO());
        coCDO.getAsset().setId(assetId);
        if (coCDto.getTemplate() != null && coCDto.getTemplate().getId() != null)
        {
            final LinkDO template = new LinkDO();
            template.setId(coCDto.getTemplate().getId());
            coCDO.setTemplate(template);
        }

        coCDO = this.coCRepository.save(coCDO);
        return getMapper().map(coCDO, CoCDto.class);
    }

    @Override
    @Transactional
    public CoCDto updateCoC(final CoCDto coCDto)
    {
        LOG.debug("Updating CoC  with ID: {}", coCDto.getId());
        final CoCDO coCDO = coCRepository.merge(getMapper().map(coCDto, CoCDO.class));
        return getMapper().map(coCDO, CoCDto.class);
    }

    @Override
    @Transactional
    public void deleteCoC(final Long cocId)
    {
        this.coCRepository.delete(cocId);
    }

    @Override
    @Transactional
    public Integer countCoCsForDefect(final Long defectId)
    {
        return this.coCRepository.countCoursesOfActionForDefect(defectId, CodicilStatusType.getCoCStatusesForCourseOfAction());
    }

    /**
     * Gets all cocs for an asset with a query
     *
     * @param pageable, page specification
     * @param assetId, Asset ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All cocs for an asset in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<CoCDto> findAllForAsset(final Pageable pageable, final Long assetId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<CoCDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = coCRepository.findAll(pageable, assetId);
        }
        else
        {
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);

            resultPage = coCRepository.findAll(pageable, assetId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilDefectQueryDto.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(), codicilDefectQueryDto.getDueDateMax(), new Date(), null,
                    CodicilStatusType.COC_OPEN.getValue());
        }

        final Page<CoCDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, this.getMapper(), CoCDto.class);
        return pageFactory.createPageResource(result, CoCPageResourceDto.class);
    }

    class CoCConverter implements Converter<CoCDO, CoCDto>
    {
        @Override
        public CoCDto convert(final CoCDO coCDO)
        {
            return getMapper().map(coCDO, CoCDto.class);
        }
    }

    @Override
    @Transactional
    public Boolean exists(final Long coCId)
    {
        return this.coCRepository.exists(coCId);
    }

    @Override
    public LockingRepository<CoCDO> getLockingRepository()
    {
        return this.coCRepository;
    }
}
