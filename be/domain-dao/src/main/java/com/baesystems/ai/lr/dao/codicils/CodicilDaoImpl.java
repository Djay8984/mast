package com.baesystems.ai.lr.dao.codicils;

import java.util.List;

import com.baesystems.ai.lr.enums.CodicilStatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.repositories.CodicilRepository;

@Repository("CodicilDao")
public class CodicilDaoImpl implements CodicilDao
{
    @Autowired
    private CodicilRepository codicilRepository;

    @Override
    @Transactional
    public Boolean doAnyItemsHaveOpenCodicils(final List<Long> itemIds)
    {
        return this.codicilRepository.doAnyItemsHaveCodicils(itemIds, CodicilStatusType.getCodicilStatusesForOpen());
    }
}
