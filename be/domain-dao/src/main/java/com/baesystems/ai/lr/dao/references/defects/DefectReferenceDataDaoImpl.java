package com.baesystems.ai.lr.dao.references.defects;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectCategoryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectDescriptorDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;
import com.baesystems.ai.lr.domain.mast.repositories.DefectCategoryRepository;
import com.baesystems.ai.lr.domain.mast.repositories.DefectDescriptorRepository;
import com.baesystems.ai.lr.domain.mast.repositories.DefectStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.DefectValueRepository;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectDetailDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "DefectReferenceDataDao")
public class DefectReferenceDataDaoImpl implements DefectReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(DefectReferenceDataDaoImpl.class);

    @Autowired
    private DefectCategoryRepository defectCategoryRepository;

    @Autowired
    private DefectStatusRepository defectStatusRepository;

    @Autowired
    private DefectDescriptorRepository defectDescriptorRepository;

    @Autowired
    private DefectValueRepository defectValueRepository;

    private final MapperFacade mapper;

    public DefectReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(DefectCategoryDO.class, DefectCategoryDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectDescriptorDO.class, DefectDetailDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<DefectCategoryDto> getDefectCategories()
    {
        LOG.debug("Retrieving all Defect Categories");

        final List<DefectCategoryDO> defectCategories = this.defectCategoryRepository.findAll();
        return mapper.mapAsList(defectCategories, DefectCategoryDto.class);
    }

    @Override
    @Transactional
    public DefectCategoryDto getDefectCategory(final Long defectCategoryId)
    {
        LOG.debug("Retrieving Defect Category with ID {}", defectCategoryId);

        final DefectCategoryDO defectCategory = this.defectCategoryRepository.findOne(defectCategoryId);
        return defectCategory == null ? null : mapper.map(defectCategory, DefectCategoryDto.class);
    }

    @Override
    @Transactional
    public DefectCategoryDO getDefectCategoryDO(final Long defectCategoryId)
    {
        LOG.debug("Retrieving Defect Category DO with ID {}", defectCategoryId);

        return this.defectCategoryRepository.findOne(defectCategoryId);
    }

    @Override
    @Transactional
    public List<DefectDetailDto> getDefectDetails()
    {
        LOG.debug("Retrieving all Defect Details.");

        final List<DefectDescriptorDO> output = this.defectDescriptorRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, DefectDetailDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getDefectStatuses()
    {
        LOG.debug("Retrieving all Defect Statuses");

        final List<DefectStatusDO> defectStatuses = this.defectStatusRepository.findAll();
        return mapper.mapAsList(defectStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getDefectStatus(final Long defectStatusId)
    {
        LOG.debug("Retrieving Defect Statuses with ID {}", defectStatusId);

        final DefectStatusDO defectStatus = this.defectStatusRepository.findOne(defectStatusId);
        return defectStatus == null ? null : mapper.map(defectStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public Integer countMatchingDefectValueIds(final List<Long> valueIds)
    {
        return this.defectValueRepository.countMatchingIds(valueIds);
    }
}
