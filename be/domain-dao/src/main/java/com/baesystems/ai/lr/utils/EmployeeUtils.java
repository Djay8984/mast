package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;

public final class EmployeeUtils
{
    private EmployeeUtils()
    {
    }

    public static String concatEmployeeName(final LrEmployeeDO employee)
    {
        final String firstName = employee.getFirstName();
        final String lastName = employee.getLastName();
        final StringBuilder name = new StringBuilder(255);
        if (firstName != null)
        {
            name.append(firstName);
        }

        if (lastName != null)
        {
            if (name.length() > 0)
            {
                name.append(' ');
            }
            name.append(lastName);
        }

        return name.toString();
    }
}
