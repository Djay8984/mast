package com.baesystems.ai.lr.dao.references.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface CertificateReferenceDataDao
{
    CertificateTypeDto getCertificateType(Long certificateTypeId);

    List<CertificateTypeDto> getCertificateTypes();

    ReferenceDataDto getCertificateStatus(Long certificateStatusId);

    List<ReferenceDataDto> getCertificateStatuses();

    CertificateActionDto getCertificateAction(Long certificateActionId);

    List<ReferenceDataDto> getCertificateActions();
}
