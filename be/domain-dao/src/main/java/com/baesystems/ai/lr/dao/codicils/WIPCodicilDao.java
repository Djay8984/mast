package com.baesystems.ai.lr.dao.codicils;

public interface WIPCodicilDao
{
    Long getParentId(Long wipId);
}
