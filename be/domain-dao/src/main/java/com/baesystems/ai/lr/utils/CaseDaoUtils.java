package com.baesystems.ai.lr.utils;

import java.util.ArrayList;

import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseCustomerDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseSurveyorDO;

public final class CaseDaoUtils
{
    private CaseDaoUtils()
    {
    }

    /**
     * Updates backward references for the given case entity: Case notes, offices and surveyors.
     *
     * @param caseEntity
     */
    public static void updateCaseFields(final CaseDO caseEntity)
    {
        // Offices
        if (caseEntity.getOffices() == null)
        {
            caseEntity.setOffices(new ArrayList<CaseOfficeDO>());
        }
        else
        {
            for (final CaseOfficeDO office : caseEntity.getOffices())
            {
                office.setaCase(caseEntity);
            }
        }

        // Customers
        if (caseEntity.getCustomers() == null)
        {
            caseEntity.setCustomers(new ArrayList<CaseCustomerDO>());
        }
        else
        {
            for (final CaseCustomerDO customer : caseEntity.getCustomers())
            {
                customer.setaCase(caseEntity);
                updateCustomerFunctions(customer);
            }
        }

        // Surveyors
        if (caseEntity.getSurveyors() == null)
        {
            caseEntity.setSurveyors(new ArrayList<CaseSurveyorDO>());
        }
        else
        {
            for (final CaseSurveyorDO surveyor : caseEntity.getSurveyors())
            {
                surveyor.setaCase(caseEntity);
            }
        }

        updateCaseMilestones(caseEntity);

        updateAttachments(caseEntity);
    }

    private static void updateCustomerFunctions(final CaseCustomerDO customer)
    {
        if (customer.getFunctions() != null)
        {
            for (final CaseCustomerFunctionCustomerFunctionTypeDO function : customer.getFunctions())
            {
                function.setCustomer(customer);
            }
        }
    }

    private static void updateAttachments(final CaseDO caseEntity)
    {
        // Attachments
        if (caseEntity.getAttachments() == null)
        {
            caseEntity.setAttachments(new ArrayList<AttachmentLinkDO>());
        }
        else
        {
            for (final AttachmentLinkDO attachment : caseEntity.getAttachments())
            {
                attachment.setaCase(caseEntity);
            }
        }
    }

    private static void updateCaseMilestones(final CaseDO caseEntity)
    {
        // Milestones
        if (caseEntity.getMilestones() == null)
        {
            caseEntity.setMilestones(new ArrayList<CaseMilestoneDO>());
        }
        else
        {
            for (final CaseMilestoneDO milestone : caseEntity.getMilestones())
            {
                milestone.setaCase(caseEntity);
            }
        }
    }
}
