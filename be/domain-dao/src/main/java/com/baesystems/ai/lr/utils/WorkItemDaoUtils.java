package com.baesystems.ai.lr.utils;

import java.util.ArrayList;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemAttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemAttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;

public final class WorkItemDaoUtils
{
    private WorkItemDaoUtils()
    {
    }

    /**
     * Updates backward references for the given Task entity: Attributes
     *
     * @param taskEntity
     */
    public static void updateTaskFields(final WorkItemDO taskEntity)
    {
        // Attributes
        if (taskEntity.getAttributes() == null)
        {
            taskEntity.setAttributes(new ArrayList<WorkItemAttributeDO>());
        }
        else
        {
            for (final WorkItemAttributeDO attribute : taskEntity.getAttributes())
            {
                attribute.setWorkItem(taskEntity);
            }
        }
    }

    /**
     * Updates backward references for the given WIP Task entity: WIP Attributes
     *
     * @param taskEntity
     */
    public static void updateWIPTaskFields(final WIPWorkItemDO taskEntity)
    {
        // Attributes
        if (taskEntity.getAttributes() == null)
        {
            taskEntity.setAttributes(new ArrayList<WIPWorkItemAttributeDO>());
        }
        else
        {
            for (final WIPWorkItemAttributeDO attribute : taskEntity.getAttributes())
            {
                attribute.setWorkItem(taskEntity);
            }
        }
    }
}
