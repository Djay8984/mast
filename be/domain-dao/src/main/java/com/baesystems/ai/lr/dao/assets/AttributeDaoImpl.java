package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.AttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributePersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AttributePersistRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeTypeRepository;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.AttributePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.ServiceHelper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.format;

@Repository(value = "AttributeDao")
public class AttributeDaoImpl implements AttributeDao
{
    private static final Logger LOG = LoggerFactory.getLogger(AttributeDaoImpl.class);

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private AttributePersistRepository attributePublishRepository;

    @Autowired
    private AttributeTypeRepository attributeTypeRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private ServiceHelper serviceHelper;


    private final MapperFacade mapper;

    public AttributeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(AttributeDO.class, AttributeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AttributePersistDO.class, AttributeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AttributeTypeDO.class, AttributeTypeDto.class).byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public Boolean attributeExistsForItemAndAsset(final Long attributeId, final Long itemId, final Long assetId)
    {
        LOG.debug(format("Checking existence of attribute id %d for item id %d and asset id %d",
                attributeId, itemId, assetId));

        final Integer existsCount = this.attributeRepository.attributeExistsForItemAndAsset(attributeId, itemId, assetId);
        return existsCount > 0;
    }

    //todo inject Asset and Version
    @Override
    @Transactional
    public AttributeDto updateAttribute(final Long itemId, final AttributeDto attributeDto)
    {
        AttributePersistDO attribute = this.mapper.map(attributeDto, AttributePersistDO.class);

        // This is fine because the combination of IDs has gone through
        // validation prior to this point
        attribute.setItemId(itemId);

        attribute = this.attributePublishRepository.merge(attribute);
        return this.mapper.map(attribute, AttributeDto.class);
    }

    @Override
    @Transactional
    public AttributeDto getAttribute(final Long id)
    {
        LOG.debug("Finding Attribute for id {}", id);
        final AttributeDO attributeDO = attributeRepository.findOne(id);
        return attributeDO != null ? mapper.map(attributeDO, AttributeDto.class) : null;
    }

    @Override
    @Transactional
    public void deleteAttribute(final Long attributeId) throws MastBusinessException
    {
        LOG.debug("Soft deleting Attribute with id {}", attributeId);
        this.attributePublishRepository.setAsDeleted(attributeId);
    }

    @Override
    @Transactional
    public Integer countAttributesByItemAndAttributeType(final Long itemId, final Long attributeTypeId, final Long versionId)
    {
        final Integer count = attributeRepository
                .countAttributesByItemAndAttributeType(itemId, attributeTypeId, versionId);
        LOG.debug(format("Counting Attributes with item %d and attributeType %d. %d", itemId, attributeTypeId, count));
        return count;
    }

    @Override
    @Transactional
    public AttributeDto createAttribute(final AttributeDto attributeDto, final VersionedItemPK itemPK)
    {
        AttributePersistDO attributeDO = this.mapper.map(attributeDto, AttributePersistDO.class);
        attributeDO.setItemId(itemPK.getId());
        attributeDO.setAssetId(itemPK.getAssetId());
        attributeDO.setAssetVersionId(itemPK.getAssetVersionId());
        attributeDO = this.attributePublishRepository.save(attributeDO);
        LOG.debug("Created Attribute with id {}", attributeDO.getId());
        return mapper.map(attributeDO, AttributeDto.class);
    }

    @Override
    @Transactional
    public PageResource<AttributeDto> findAttributesByItem(final Long itemId, final Pageable pageSpecification)
    {
        LOG.debug("Finding attributes for item {}", itemId);
        final Page<AttributeDO> output = this.attributeRepository.findAttributesByItemId(itemId, pageSpecification);
        final Page<AttributeDto> result = output.map(new Converter<AttributeDO, AttributeDto>()
        {
            @Override
            public AttributeDto convert(final AttributeDO attributeDO)
            {
                return mapper.map(attributeDO, AttributeDto.class);
            }
        });
        return pageFactory.createPageResource(result, AttributePageResourceDto.class);

    }

    @Override
    @Transactional
    public List<AttributeDto> findAttributesByItem(final Long itemId) throws BadPageRequestException
    {
        LOG.debug("Finding attributes for item {}", itemId);
        final Pageable pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        return mapper.mapAsList(this.attributeRepository.findAttributesByItemId(itemId, pageSpecification).getContent(), AttributeDto.class);
    }

    @Override
    @Transactional
    public List<Long> getMandatoryAttributeTypeIdsForAnItemTypeId(final Long itemTypeId)
    {
        return this.attributeTypeRepository.getMandatoryAttributeTypeIdsForAnItemTypeId(itemTypeId);
    }

    @Override
    @Transactional
    public List<AttributeDto> findAttributesWithNamingOrderForItem(final Long itemId)
    {
        LOG.debug("Finding attributes with naming order for item {}", itemId);

        // The reason why this is using the persisting repository rather than the read-only repository is that this one
        // sees the changes to attributes that may have occurred within this transaction, where the read-only one does
        // not.

        final List<AttributeDO> result = this.attributePublishRepository.findAttributesWithNamingOrderForItem(itemId);

        return result != null ? mapper.mapAsList(result, AttributeDto.class) : null;
    }
}
