package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamDO;
import com.baesystems.ai.lr.domain.mast.repositories.TeamRepository;
import com.baesystems.ai.lr.dto.references.TeamDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository(value = "TeamDao")
public class TeamDaoImpl implements TeamDao
{
    @Autowired
    private TeamRepository teamRepository;

    private final MapperFacade mapper;

    public TeamDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(TeamDO.class, TeamDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<TeamDto> findAll()
    {
        final List<TeamDO> output = this.teamRepository.findAll();
        return this.mapper.mapAsList(output, TeamDto.class);
    }
}
