package com.baesystems.ai.lr.dao.references.attachments;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface AttachmentReferenceDataDao
{
    List<ReferenceDataDto> getAttachmentTypes();

    ReferenceDataDto getAttachmentType(Long attachmentTypeId);

    List<ReferenceDataDto> getConfidentialityTypes();

    ReferenceDataDto getConfidentialityType(Long confidentialityTypeId);
}
