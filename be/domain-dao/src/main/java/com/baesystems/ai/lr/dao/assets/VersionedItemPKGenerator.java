package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VersionedItemPKGenerator
{
    @Autowired
    private VersionedItemRepository versionedItemRepository;

    public VersionedItemPK getNextPK(final Long assetId, final Long itemId)
    {
        final Long nextVersionId = this.versionedItemRepository.getNextVersion(assetId, itemId);
        return new VersionedItemPK(itemId, assetId, nextVersionId);
    }
}
