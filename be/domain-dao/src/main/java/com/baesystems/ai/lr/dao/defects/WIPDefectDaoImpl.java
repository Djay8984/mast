package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDefectValueDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectItemDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectValueDO;
import com.baesystems.ai.lr.domain.mast.repositories.DefectRepository;
import com.baesystems.ai.lr.domain.mast.repositories.DefectStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPDefectRepository;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.JobDaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPDefectDao")
public class WIPDefectDaoImpl extends AbstractLockingDaoImpl<WIPDefectDO> implements WIPDefectDao
{
    private static final Logger LOG = LoggerFactory.getLogger(WIPDefectDaoImpl.class);

    @Autowired
    private WIPDefectRepository wipDefectRepository;

    @Autowired
    private DefectRepository defectRepository;

    @Autowired
    private SequenceDao sequenceDao;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private DefectStatusRepository defectStatusRepository;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(WIPDefectDO.class, DefectLightDto.class).byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(WIPDefectDO.class, DefectDto.class)
                .customize(new StaleObjectMapper<WIPDefectDO, DefectDto>())
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(WIPDefectDefectValueDO.class, DefectDefectValueDto.class).byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(DefectValueDO.class, DefectValueDto.class).byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(WIPDefectItemDO.class, DefectItemDto.class).byDefault().toClassMap());
    }

    public WIPDefectDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPDefectDO> getLockingRepository()
    {
        return this.wipDefectRepository;
    }

    /**
     * Gets all WIP Defects for a job with a query
     *
     * @param pageable, page specification
     * @param jobId, Job ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All Defects for a job in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<DefectDto> findAllForJob(final Pageable pageable, final Long jobId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<WIPDefectDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            LOG.debug("Searching for WIP defects with job id : {}", jobId);
            resultPage = this.wipDefectRepository.findAllForJob(pageable, jobId);
        }
        else
        {
            LOG.debug("Searching for WIP defects with job id : {}, and query data.", jobId);
            final Long assetId = this.jobRepository.getAssetIdForJob(jobId);
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);
            resultPage = this.wipDefectRepository.findAllForJob(pageable, jobId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilDefectQueryDto.getStatusList(), itemIdList);
        }

        final Page<DefectDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), DefectDto.class);
        return this.pageFactory.createPageResource(result, DefectPageResourceDto.class);
    }

    @Override
    @Transactional
    public DefectDto createDefect(final DefectDto defectDto)
    {
        WIPDefectDO defect = getMapper().map(defectDto, WIPDefectDO.class);
        JobDaoUtils.updateWIPDefectFields(defect);
        defect.setSequenceNumber(
                this.sequenceDao.getNextAssetSpecificSequenceValue(defectDto.getJob().getId(), true, this.defectRepository, this.wipDefectRepository));
        defect = this.wipDefectRepository.merge(defect);
        return getMapper().map(defect, DefectDto.class);
    }

    @Override
    @Transactional
    public DefectDto getDefect(final Long defectId)
    {
        LOG.debug("Retrieving WIP Defect with ID {}", defectId);

        final WIPDefectDO defect = this.wipDefectRepository.findOne(defectId);
        return defect == null ? null : getMapper().map(defect, DefectDto.class);
    }

    @Override
    @Transactional
    public Boolean defectExistsForJob(final Long jobId, final Long defectId)
    {
        final Long existsCount = this.wipDefectRepository.findDefectForJob(jobId, defectId);
        return existsCount != null && existsCount > 0;
    }

    @Override
    @Transactional
    public DefectDto updateDefect(final DefectDto defectDto)
    {
        LOG.debug("Updating WIP Defect with ID: {}", defectDto.getId());
        final WIPDefectDO defect = getMapper().map(defectDto, WIPDefectDO.class);
        JobDaoUtils.updateWIPDefectFields(defect);
        final WIPDefectDO defectDO = this.wipDefectRepository.merge(defect);
        return getMapper().map(defectDO, DefectDto.class);
    }

    @Override
    @Transactional
    public void deleteDefect(final Long defectId)
    {
        LOG.debug("Deleting WIP Defect for ID: {}", defectId);
        this.wipDefectRepository.delete(defectId);
    }

    @Override
    @Transactional
    public List<WIPRepairDO> getRepairs(final Long defectId)
    {
        List<WIPRepairDO> repairs = null;
        final WIPDefectDO defectDO = this.wipDefectRepository.findOne(defectId);
        if (defectDO != null)
        {
            repairs = defectDO.getRepairs();
        }
        return repairs;
    }

    @Override
    @Transactional
    public List<AttachmentLinkDO> getAttachments(final Long defectId)
    {
        List<AttachmentLinkDO> attachments = null;
        final WIPDefectDO defectDO = this.wipDefectRepository.findOne(defectId);
        if (defectDO != null)
        {
            attachments = defectDO.getAttachments();
        }
        return attachments;
    }

    @Override
    @Transactional
    public Boolean exists(final Long defectId)
    {
        return this.wipDefectRepository.exists(defectId);
    }

    @Override
    @Transactional
    public Boolean closeDefect(final Long defectId)
    {
        final DefectStatusDO status = this.defectStatusRepository.findByName("Closed");
        final int updateCount = this.wipDefectRepository.updateStatus(defectId, status);
        return updateCount == 1;
    }

    @Override
    @Transactional
    public List<DefectDto> getDefectsForJob(final Long jobId)
    {
        final List<WIPDefectDO> defects = this.wipDefectRepository.findDefectByJobId(jobId);
        return getMapper().mapAsList(defects, DefectDto.class);
    }

    @Override
    @Transactional
    public Long getParentDefectId(final Long defectId)
    {
        return this.wipDefectRepository.findParentId(defectId);
    }

    @Override
    @Transactional
    public List<DefectDto> getDefectsForJobWithStatus(final Long jobId, final List<Long> statusIds)
    {
        final List<WIPDefectDO> defects = this.wipDefectRepository.findDefectByJobId(jobId, statusIds);
        return getMapper().mapAsList(defects, DefectDto.class);
    }
}
