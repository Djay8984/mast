package com.baesystems.ai.lr.dao.services;

import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.utils.LockingDao;

import java.util.List;

public interface ProductDao extends LockingDao<ProductDO>
{
    List<ProductDto> getProducts(Long assetId);

    ProductDO getProductDO(Long productId);

    ProductDto getProductDto(Long productId);

    void createProduct(Long assetId, Long productCatalogueId, Long schedulingRegimeId);

    void deleteProduct(Long productId);

    void updateProductSchedulingRegime(Long productId, Long schedulingRegimeId);

    Boolean isProductCatalogueSelectedForAsset(Long assetId, Long productCatalogueId, Long schedulingRegimeId);

    Boolean isProductCatalogueOfType(Long productCatalogueId, Long productTypeId);
}
