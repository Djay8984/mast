package com.baesystems.ai.lr.utils;

import java.util.Date;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.domain.entities.BaseDO;
import com.baesystems.ai.lr.dto.base.Dto;

import ma.glasnost.orika.MapperFacade;

@Component
public final class DaoUtils
{
    public static final Long NON_EXISTENT_ID = -1L;
    private static final List<Long> NULL = null;

    private DaoUtils()
    {
    }

    /**
     * This can take a null value if we want to construct an empty {@link Page<? extends Dto>}, otherwise map the
     * {@link Page <? extends BaseDo>} to a corresponding {@link Page<? extends Dto>}
     *
     * @param dOPage a {@link Page<? extends BaseDo>}
     * @param mapper a {@link MapperFacade} to map the DO onto the Dto
     * @param dtoClass The {@link Dto} class we want to map onto.
     * @return {@link Page<? extends Dto>} the mapped ? extends BaseDo
     */
    public static <T extends Dto, S extends BaseDO> Page<T> mapDOPageToDtoPage(final Page<S> dOPage, final MapperFacade mapper,
            final Class<T> dtoClass)
    {
        Page<T> result = null;

        result = dOPage.map(new Converter<S, T>()
        {
            @Override
            public T convert(final S baseDo)
            {
                return mapper.map(baseDo, dtoClass);
            }
        });

        return result;
    }

    public static List<Long> addNonExistentIdToEmptyList(final List<Long> list)
    {
        if (list != null && list.isEmpty())
        {
            list.add(NON_EXISTENT_ID);
        }
        return list;
    }

    public List<Long> getSafeIdList(final List<Long> list)
    {
        return list != null && list.isEmpty() ? NULL : list;
    }

    public String safeToString(final Object object)
    {
        String string = null;

        if (object != null)
        {
            if (Date.class.isInstance(object))
            {
                string = DateHelper.mastFormatter().format(object);
            }
            else
            {
                string = object.toString();
            }
        }

        return string;
    }
}
