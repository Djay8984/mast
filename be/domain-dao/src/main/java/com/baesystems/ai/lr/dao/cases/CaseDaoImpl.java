package com.baesystems.ai.lr.dao.cases;

import static com.baesystems.ai.lr.enums.CaseStatusType.getDuplicateCaseStatusSearch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.employees.SurveyorDaoImpl.EmployeeNameMapper;
import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.OrganisationDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CaseStatusDO;
import com.baesystems.ai.lr.domain.mast.repositories.CaseRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CaseStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CasePageResourceDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.CaseDaoUtils;
import com.baesystems.ai.lr.utils.DomainObjectFactory;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "CaseDao")
public class CaseDaoImpl extends AbstractLockingDaoImpl<CaseDO> implements CaseDao
{
    private static final Logger LOG = LoggerFactory.getLogger(CaseDaoImpl.class);

    @Autowired
    private CaseStatusRepository caseStatusRepository;

    @Autowired
    private CaseRepository caseRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerObjectFactory(new DomainObjectFactory<IhsAssetDO>(IhsAssetDO.class), IhsAssetDO.class);
        FACTORY.registerClassMap(FACTORY.classMap(IhsAssetDO.class, IhsAssetDto.class).byDefault().toClassMap());
        FACTORY.classMap(LrEmployeeDO.class, SurveyorDto.class)
                .byDefault()
                .customize(
                        new EmployeeNameMapper())
                .register();
        FACTORY.registerObjectFactory(new DomainObjectFactory<OrganisationDO>(OrganisationDO.class), OrganisationDO.class);
        FACTORY.registerClassMap(FACTORY.classMap(CaseDO.class, CaseDto.class).byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(CaseDO.class, CaseWithAssetDetailsDto.class)
                .customize(new StaleObjectMapper<CaseDO, CaseWithAssetDetailsDto>())
                .byDefault().toClassMap());
    }

    public CaseDaoImpl()
    {
        super(FACTORY);

    }

    @Override
    @Transactional
    public final Boolean caseExists(final Long caseId)
    {
        return this.caseRepository.exists(caseId);
    }

    @Override
    @Transactional
    public final PageResource<CaseDto> getCases(final Pageable pageable, final CaseQueryDto query)
    {
        LOG.debug("Retrieving all Cases");
        Page<CaseDO> caseData = null;

        if (query == null)
        {
            caseData = this.caseRepository.findAll(pageable);
        }
        else
        {
            caseData = this.caseRepository.findAll(pageable, query);
        }

        return this.mapCasePage(caseData);
    }

    @Override
    @Transactional
    public final PageResource<CaseDto> getCases(final Pageable pageable, final AbstractQueryDto query)
    {
        LOG.debug("Retrieving all Cases matching query");

        return this.mapCasePage(this.caseRepository.findAll(pageable, query));
    }

    @Override
    @Transactional
    public final CaseWithAssetDetailsDto getCase(final Long caseId)
    {
        LOG.debug("Retrieving Case with ID {}", caseId);

        final CaseDO caseData = this.caseRepository.findOne(caseId);

        CaseWithAssetDetailsDto result = null;

        if (caseData != null)
        {
            result = this.getMapper().map(caseData, CaseWithAssetDetailsDto.class);
        }

        return result;
    }

    /**
     * check if a give case status id means that the case can be deleted.
     */
    @Override
    @Transactional
    public final Boolean getCanDeleteCase(final Long caseStatusId)
    {
        boolean result = false;
        final CaseStatusDO status = this.caseStatusRepository.findOne(caseStatusId);
        if (status == null)
        {
            result = false;
        }
        else if (status.getCanDelete())
        {
            result = true;
        }
        return result;
    }

    /**
     * delete a case.
     */
    @Override
    @Transactional
    public void deleteCase(final Long caseId)
    {
        this.caseRepository.delete(caseId);
    }

    @Override
    @Transactional
    public CaseWithAssetDetailsDto createCase(final CaseWithAssetDetailsDto caseDto)
    {
        CaseDO caseEntity = this.getMapper().map(caseDto, CaseDO.class);
        CaseDaoUtils.updateCaseFields(caseEntity);

        caseEntity = this.caseRepository.merge(caseEntity);

        return this.getMapper().map(caseEntity, CaseWithAssetDetailsDto.class);
    }

    @Override
    @Transactional
    public CaseWithAssetDetailsDto updateCase(final CaseWithAssetDetailsDto caseDto)
    {
        CaseDO caseEntity = this.getMapper().map(caseDto, CaseDO.class);
        CaseDaoUtils.updateCaseFields(caseEntity);

        final Long orignalCaseStatusId = this.getCaseStatusId(caseEntity.getId());

        if (CaseStatusType.ONHOLD.getValue().equals(caseEntity.getCaseStatus().getId()))
        {
            if (!CaseStatusType.ONHOLD.getValue().equals(orignalCaseStatusId))
            {
                caseEntity.setPreviousCaseStatus(new LinkDO());
                caseEntity.getPreviousCaseStatus().setId(orignalCaseStatusId);
            }
            else
            {
                caseEntity.setPreviousCaseStatus(new LinkDO());
                caseEntity.getPreviousCaseStatus().setId(this.getPreviousCaseStatusId(caseEntity.getId()));
            }
        }

        caseEntity = this.caseRepository.merge(caseEntity);

        return this.getMapper().map(caseEntity, CaseWithAssetDetailsDto.class);
    }

    /**
     * Returns all Cases which are a duplicate of the given Case ID according to the following matching criteria:
     *
     * - Active Case Statuses (Uncommitted, Committed, On-Hold) - Business Process - assetId
     */
    @Override
    @Transactional
    public List<Long> findDuplicateCasesByAssetId(final Long assetId, final Long caseTypeId)
    {
        LOG.debug(String.format("Searching for any Cases where Business Process ID = %d, Asset ID = %d ", caseTypeId, assetId));

        final List<Long> cases = this.caseRepository.findDuplicateCasesByAssetId(assetId, caseTypeId, getDuplicateCaseStatusSearch());

        LOG.debug(String.format("Found %d Case(s) with the same Asset ID.", cases.size()));

        return cases;
    }

    /**
     * Get the number of cases linked to an asset.
     *
     * @param assetId The id of the asset.
     */
    @Override
    @Transactional
    public Long countCasesForAsset(final Long assetId)
    {
        return this.caseRepository.countCasesForAssetId(assetId);
    }

    @Override
    @Transactional
    public List<CaseDto> getCasesForAsset(final Long assetId, final List<Long> caseStatuses)
    {
        LOG.debug(String.format("Finding Cases associated with Asset ID = %d and Case Status in [%s]", assetId, caseStatuses));
        final List<CaseDO> output = this.caseRepository.findCasesByAssetId(assetId, caseStatuses);
        return this.getMapper().mapAsList(output, CaseDto.class);
    }

    @Override
    @Transactional
    public final Long getCaseStatusId(final Long caseId)
    {
        return this.caseRepository.findCaseStatusId(caseId);
    }

    @Override
    @Transactional
    public final Long getPreviousCaseStatusId(final Long caseId)
    {
        return this.caseRepository.findPreviousCaseStatusId(caseId);
    }

    @Override
    @Transactional
    public void updateCaseStatus(final Long caseId, final Long statusId)
    {
        this.caseRepository.updateCaseStatus(caseId, statusId);
    }

    @Override
    public LockingRepository<CaseDO> getLockingRepository()
    {
        return this.caseRepository;
    }

    private PageResource<CaseDto> mapCasePage(final Page<CaseDO> caseData)
    {
        final Page<CaseDto> result = caseData.map(new Converter<CaseDO, CaseDto>()
        {
            @Override
            public CaseDto convert(final CaseDO asset)
            {
                return CaseDaoImpl.this.getMapper().map(asset, CaseDto.class);
            }
        });

        return pageFactory.createPageResource(result, CasePageResourceDto.class);
    }
}
