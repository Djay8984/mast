package com.baesystems.ai.lr.dao.codicils;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface BaseAssetNoteDao
{
    AssetNoteDto getAssetNote(Long assetNoteId) throws RecordNotFoundException;

    PageResource<AssetNoteDto> getAssetNotesByAsset(Pageable pageable, Long assetId, Long statusId)
            throws BadPageRequestException;

    AssetNoteDto saveAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException;

    AssetNoteDto updateAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException;
}
