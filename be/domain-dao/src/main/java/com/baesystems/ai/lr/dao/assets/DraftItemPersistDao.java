package com.baesystems.ai.lr.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface DraftItemPersistDao
{
    ItemLightDto appendDraftItemFromReferenceData(Long assetId, DraftItemDto draftItemDto, String draftItemName, Integer nextDisplayOrder);

    void appendCopiedAssetModel(Long assetId, DraftItemDto draftItemDto, Boolean copyAttributes, Integer nextDisplayOrder);

    void selectiveAppendCopiedAssetModel(Long assetId, DraftItemDto draftItemDto, Boolean copyAttributeValues, Integer nextDisplayOrder);

    void createDraftAttribute(Long attributeTypeId, Long draftItemId, Long assetId, Long assetVersionId);

    void deleteDraftItem(Long draftItemId);

    void updateDisplayOrders(VersionedItemPK itemPK, Integer newDisplayOrder) throws MastBusinessException;
}
