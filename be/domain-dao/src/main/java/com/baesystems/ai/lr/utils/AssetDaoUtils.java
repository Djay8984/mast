package com.baesystems.ai.lr.utils;

import java.util.ArrayList;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetOfficeDO;

public final class AssetDaoUtils
{
    private AssetDaoUtils()
    {
    }

    /**
     * Updates backward references for customers in an asset.
     *
     * @param assetEntity
     */
    public static void updateAssetFields(final VersionedAssetDO assetEntity)
    {
        // Customers
        if (assetEntity.getCustomers() == null)
        {
            assetEntity.setCustomers(new ArrayList<AssetCustomerDO>());
        }
        else
        {
            for (final AssetCustomerDO customer : assetEntity.getCustomers())
            {
                customer.setAsset(assetEntity);

                if (customer.getFunctions() != null)
                {
                    for (final AssetCustomerFunctionCustomerFunctionTypeDO function : customer.getFunctions())
                    {
                        function.setCustomer(customer);
                    }
                }
            }
        }

        // Offices
        if (assetEntity.getOffices() == null)
        {
            assetEntity.setOffices(new ArrayList<AssetOfficeDO>());
        }
        else
        {
            for (final AssetOfficeDO office : assetEntity.getOffices())
            {
                office.setAsset(assetEntity);
            }
        }
    }
}
