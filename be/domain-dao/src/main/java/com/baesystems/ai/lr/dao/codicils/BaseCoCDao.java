package com.baesystems.ai.lr.dao.codicils;

import com.baesystems.ai.lr.dto.codicils.CoCDto;

public interface BaseCoCDao
{
    CoCDto getCoC(Long coCId);

    CoCDto updateCoC(CoCDto coCDto);

    Integer countCoCsForDefect(Long defectId);
}
