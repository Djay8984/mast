package com.baesystems.ai.lr.dao.codicils;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.codicils.ActionableItemDO;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.LockingDao;

public interface ActionableItemDao extends BaseActionableItemDao, CodicilDefectQueryDao<ActionableItemDto>, LockingDao<ActionableItemDO>
{
    Boolean actionableItemExistsForAsset(Long assetId, Long actionableItemId);

    PageResource<ActionableItemDto> getActionableItemsForAssets(Pageable pageSpecification, Long assetId, Long statusId);

    ActionableItemDto createActionableItem(Long assetId, ActionableItemDto actionableItemDto);

    TemplateAssetCountDto countAssetsWithActionableItemsForTemplate(List<Long> templateIds, List<Long> assetLifecycleStatuses);

    ActionableItemDto updateActionableItem(Long assetId, ActionableItemDto actionableItemDto);

    List<ActionableItemDto> getActionableItemsForAService(Long serviceId);
}
