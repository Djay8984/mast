package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import com.baesystems.ai.lr.dao.codicils.WIPCodicilDefectQueryDao;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairDO;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPDefectDao extends BaseDefectDao, WIPCodicilDefectQueryDao<DefectDto>, LockingDao<WIPDefectDO>
{
    Boolean defectExistsForJob(Long jobId, Long defectId);

    void deleteDefect(Long defectId);

    Boolean exists(Long defectId);

    List<WIPRepairDO> getRepairs(Long defectId);

    List<AttachmentLinkDO> getAttachments(Long defectId);

    List<DefectDto> getDefectsForJob(Long jobId);

    Long getParentDefectId(Long defectId);

    List<DefectDto> getDefectsForJobWithStatus(Long jobId, List<Long> statusIds);
}
