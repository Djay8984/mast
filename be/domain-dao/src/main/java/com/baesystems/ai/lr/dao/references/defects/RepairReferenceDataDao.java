package com.baesystems.ai.lr.dao.references.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.references.RepairReferenceDataDto;
import com.baesystems.ai.lr.dto.references.MaterialDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface RepairReferenceDataDao
{
    List<MaterialDto> getMaterials();

    List<ReferenceDataDto> getRepairTypes();

    List<ReferenceDataDto> getRepairActions();

    ReferenceDataDto getRepairAction(Long repairActionId);

    RepairReferenceDataDto getReferenceData();

    List<ReferenceDataDto> getMaterialTypes();
}
