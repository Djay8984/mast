package com.baesystems.ai.lr.dao.codicils;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.codicils.AssetNoteDO;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.utils.LockingDao;

/*
 * Marker interface to avoid the need to qualify when auto-wiring
 */
public interface AssetNoteDao extends BaseAssetNoteDao, CodicilDefectQueryDao<AssetNoteDto>, LockingDao<AssetNoteDO>
{
    Boolean assetNoteExistsForAsset(Long assetId, Long assetNoteId);

    TemplateAssetCountDto countAssetsWithAssetNotesForTemplate(List<Long> templateIds, List<Long> assetLifecycleStatuses);

    void deleteAssetNote(Long assetNoteId);

    Boolean exists(Long assetNoteId);
}
