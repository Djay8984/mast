package com.baesystems.ai.lr.dao.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.baesystems.ai.lr.domain.ihs.entities.IhsEquipment2DO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsEquipment4DO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsEquipment2Repository;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsEquipment4Repository;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment2Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment4Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "IhsEquipmentDetailsDao")
public class IhsEquipmentDetailsDaoImpl implements IhsEquipmentDetailsDao
{
    @Autowired
    private IhsEquipment2Repository ihsEquipment2Repository;

    @Autowired
    private IhsEquipment4Repository ihsEquipment4Repository;

    private final MapperFacade mapper;

    public IhsEquipmentDetailsDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(IhsEquipment2DO.class, IhsEquipment2Dto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(IhsEquipment4DO.class, IhsEquipment4Dto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    public IhsEquipmentDetailsDto getEquipmentDetails(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsEquipmentDetailsDto detailsDto = new IhsEquipmentDetailsDto();
        final IhsEquipment2DO eq2 = this.ihsEquipment2Repository.findOne(imoNumber);
        final IhsEquipment4DO eq4 = this.ihsEquipment4Repository.findOne(imoNumber);
        
        detailsDto.setEquipment2Dto(this.mapper.map(eq2, IhsEquipment2Dto.class));
        detailsDto.setEquipment4Dto(this.mapper.map(eq4, IhsEquipment4Dto.class));
        return detailsDto;
    }

}
