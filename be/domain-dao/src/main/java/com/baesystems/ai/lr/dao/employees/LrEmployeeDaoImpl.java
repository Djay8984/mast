package com.baesystems.ai.lr.dao.employees;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.dto.employees.EmployeePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.EmployeeUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "LrEmployeeDao")
public class LrEmployeeDaoImpl implements LrEmployeeDao
{
    private static final Logger LOG = LoggerFactory.getLogger(LrEmployeeDaoImpl.class);

    @Autowired
    private LrEmployeeRepository lrEmployeeRepository;

    private final MapperFacade mapper;

    @Autowired
    private PageResourceFactory pageResourceFactory;

    public LrEmployeeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(LrEmployeeDO.class, LrEmployeeDto.class)
                .byDefault()
                .customize(new NameMapper())
                .register();
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public final LrEmployeeDto getLrEmployee(final Long employeeId)
    {
        LOG.debug("Retrieving Employee for Employee ID: {}", employeeId);

        final LrEmployeeDO employeeData = this.lrEmployeeRepository.findOne(employeeId);

        return employeeData == null ? null : mapper.map(employeeData, LrEmployeeDto.class);
    }

    @Override
    @Transactional
    public final LrEmployeeDto getLrEmployee(final String userFullName)
    {
        LOG.debug("Retrieving Employee with name: {}", userFullName);

        final LrEmployeeDO employee = this.lrEmployeeRepository.findByFullName(userFullName);

        return employee == null ? null : mapper.map(employee, LrEmployeeDto.class);
    }

    @Override
    @Transactional
    public PageResource<LrEmployeeDto> searchEmployees(final Pageable pageable,
            final String search, final Long officeId) throws BadPageRequestException
    {
        LOG.debug("Retrieving all Employees matching search string " + search);

        final Page<LrEmployeeDO> employees = lrEmployeeRepository.findAll(pageable, search, officeId, null);
        final Page<LrEmployeeDto> result = employees.map(new Converter<LrEmployeeDO, LrEmployeeDto>()
        {
            @Override
            public LrEmployeeDto convert(final LrEmployeeDO employeeDO)
            {
                return mapper.map(employeeDO, LrEmployeeDto.class);
            }
        });
        return pageResourceFactory.createPageResource(result, EmployeePageResourceDto.class);
    }

    @Override
    @Transactional
    public String getEmployeeFullName(final Long employeeId)
    {
        return this.lrEmployeeRepository.findFullNameById(employeeId);
    }

    @Override
    @Transactional
    public final Boolean lrEmployeeExists(final Long employeeId)
    {
        LOG.debug("Testing if Employee ID: {} exists", employeeId);

        return this.lrEmployeeRepository.exists(employeeId);
    }

    @Override
    @Transactional
    public List<LrEmployeeDto> getEmployees()
    {
        final List<LrEmployeeDO> output = this.lrEmployeeRepository.findAll();
        return this.mapper.mapAsList(output, LrEmployeeDto.class);
    }

    public static class NameMapper extends CustomMapper<LrEmployeeDO, LrEmployeeDto>
    {
        @Override
        // Custom mapper to concatenate first and last names from LR Employee data table.
        public void mapAtoB(final LrEmployeeDO enteredDO, final LrEmployeeDto resultingDto, final MappingContext context)
        {
            resultingDto.setName(EmployeeUtils.concatEmployeeName(enteredDO));
        }
    }
}
