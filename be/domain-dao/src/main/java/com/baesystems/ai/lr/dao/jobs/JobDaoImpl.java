package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobResourceDO;
import com.baesystems.ai.lr.domain.mast.entities.references.JobStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.jobs.JobPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DomainObjectFactory;
import com.baesystems.ai.lr.utils.JobDaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "JobDao")
public class JobDaoImpl extends AbstractLockingDaoImpl<JobDO> implements JobDao
{
    private static final Logger LOG = LoggerFactory.getLogger(JobDao.class);

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.classMap(JobDO.class, JobDto.class)
                .customize(new StaleObjectMapper<JobDO, JobDto>())
                .byDefault().register();

        FACTORY.classMap(VersionedAssetDO.class, LinkVersionedResource.class)
                .field("assetVersionId", "version")
                .byDefault().register();

        FACTORY.registerClassMap(FACTORY.classMap(ScheduledServiceDO.class, ScheduledServiceDto.class)
                .fieldAToB("serviceCatalogue.id", "serviceCatalogueId")
                .byDefault().toClassMap());

        FACTORY.registerObjectFactory(new DomainObjectFactory<JobOfficeDO>(JobOfficeDO.class), JobOfficeDO.class);
        FACTORY.registerClassMap(FACTORY.classMap(JobOfficeDO.class, OfficeLinkDto.class).byDefault().toClassMap());

        FACTORY.registerObjectFactory(new DomainObjectFactory<JobResourceDO>(JobResourceDO.class), JobResourceDO.class);
        FACTORY.registerClassMap(FACTORY.classMap(JobResourceDO.class, EmployeeLinkDto.class).byDefault().toClassMap());

        FACTORY.registerObjectFactory(new DomainObjectFactory<LrEmployeeDO>(LrEmployeeDO.class), LrEmployeeDO.class);
    }

    public JobDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<JobDO> getLockingRepository()
    {
        return this.jobRepository;
    }

    @Override
    @Transactional
    public final Boolean jobExists(final Long jobId)
    {
        return this.jobRepository.exists(jobId);
    }

    @Override
    @Transactional
    public JobDto getJob(final Long jobId)
    {
        LOG.debug("Retrieving Job with ID {}", jobId);

        final JobDO job = this.jobRepository.findOne(jobId);

        return this.getMapper().map(job, JobDto.class);
    }

    @Override
    @Transactional
    public PageResource<JobDto> getJobs(final Pageable pageable, final JobQueryDto query)
    {
        LOG.debug("Retrieving all Jobs matching query");

        Page<JobDO> output = null;

        if (query == null)
        {
            output = this.jobRepository.findAll(pageable);
        }
        else
        {
            output = this.jobRepository.findAll(pageable, query);
        }

        final Page<JobDto> result = output.map(new JobConverter());

        return this.pageFactory.createPageResource(result, JobPageResourceDto.class);
    }

    @Override
    @Transactional
    public PageResource<JobDto> getJobsForEmployee(final Pageable pageable, final Long employeeId, final JobQueryDto queryDto)
    {
        LOG.debug("Retrieving all Employee jobs for matching query.");

        final Page<JobDO> output = this.jobRepository.findAllJobsForEmployee(pageable, employeeId, queryDto);

        final Page<JobDto> result = output.map(new JobConverter());

        return this.pageFactory.createPageResource(result, JobPageResourceDto.class);
    }

    @Override
    @Transactional
    public PageResource<JobDto> getJobsForTeam(final Pageable pageable, final Long teamId, final JobQueryDto queryDto)
    {
        // To be done under Team Jobs story.
        return null;
    }

    @Override
    @Transactional
    public Integer updateCompletedFields(final Long jobId, final String completedBy, final Date completedOn)
    {
        return this.jobRepository.updateCompletedFields(jobId, completedBy, completedOn);
    }

    @Override
    @Transactional
    public PageResource<JobDto> getJobsForAsset(final Pageable pageable, final Long assetId)
    {
        LOG.debug("Retrieving all Jobs for Asset {}", assetId);

        final Page<JobDO> output = this.jobRepository.findJobsForAsset(pageable, assetId);
        final Page<JobDto> result = output.map(new JobConverter());

        return this.pageFactory.createPageResource(result, JobPageResourceDto.class);
    }

    @Override
    @Transactional
    public JobDto createJob(final JobDto jobDto)
    {
        JobDO job = this.getMapper().map(jobDto, JobDO.class);

        JobDaoUtils.updateJobFields(job);
        job = this.jobRepository.merge(job);

        return this.getMapper().map(job, JobDto.class);
    }

    @Override
    @Transactional
    public JobDto updateJob(final JobDto jobDto)
    {
        LOG.debug("Updating Job with ID {}", jobDto.getId());
        JobDO job = this.getMapper().map(jobDto, JobDO.class);
        JobDaoUtils.updateJobFields(job);
        job = this.jobRepository.merge(job);
        return this.getMapper().map(job, JobDto.class);
    }

    class JobConverter implements Converter<JobDO, JobDto>
    {
        @Override
        public JobDto convert(final JobDO job)
        {
            return JobDaoImpl.this.getMapper().map(job, JobDto.class);
        }
    }

    @Override
    @Transactional
    public void updateJobStatus(final Long jobId, final Long statusId)
    {
        this.jobRepository.updateStatus(jobId, statusId);
    }

    @Override
    @Transactional
    public ReferenceDataDto getJobStatusForJob(final Long jobId)
    {
        LOG.debug("Retrieving Job Status for Job with ID {}", jobId);

        final JobStatusDO jobStatus = this.jobRepository.getJobStatusForJob(jobId);
        return jobStatus == null ? null : this.getMapper().map(jobStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public Long getAssetIdForJob(final Long jobId)
    {
        LOG.debug("Retrieving Asset ID for job with ID {}", jobId);

        return this.jobRepository.findAssetIdForJob(jobId);
    }

    @Override
    public PageResource<JobDto> getJobs(final Pageable pageable, final AbstractQueryDto query)
    {
        final Page<JobDO> output = this.jobRepository.findAll(pageable, query);
        final Page<JobDto> result = output.map(new JobConverter());

        return this.pageFactory.createPageResource(result, JobPageResourceDto.class);
    }

    @Override
    @Transactional
    public Long getVersionForAssetAndJob(final Long assetId, final Long jobId)
    {
        return this.jobRepository.getVersionForAssetAndJob(assetId, jobId);
    }
}
