package com.baesystems.ai.lr.dao.codicils;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.LockingDao;

/*
 * Marker interface to avoid the need to qualify when auto-wiring
 */
public interface CoCDao extends BaseCoCDao, CodicilDefectQueryDao<CoCDto>, LockingDao<CoCDO>
{
    CoCDto saveCoC(CoCDto coCDto, Long assetId);

    PageResource<CoCDto> getCoCsForAsset(Pageable pageable, Long assetId, Long statusId);

    Boolean coCExistsForAsset(Long assetId, Long cocId);

    void deleteCoC(Long coCId);

    Boolean exists(Long coCId);

    PageResource<CoCDto> getCoCsForDefect(Pageable pageable, Long assetId, Long defectId, Long statusId);
}
