package com.baesystems.ai.lr.dao.codicils;

import java.util.List;

public interface CodicilDao
{
    Boolean doAnyItemsHaveOpenCodicils(List<Long> itemIds);
}
