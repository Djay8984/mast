package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDefectValueDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectItemDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDefectValueDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectItemDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobResourceDO;

public final class JobDaoUtils
{
    private JobDaoUtils()
    {
    }

    /**
     * Updates backward references for the given Job entity: Offices, Employees.
     *
     * @param jobEntity
     */
    public static void updateJobFields(final JobDO jobEntity)
    {
        // Offices
        if (jobEntity.getOffices() == null)
        {
            jobEntity.setOffices(new ArrayList<JobOfficeDO>());
        }
        else
        {
            for (final JobOfficeDO office : jobEntity.getOffices())
            {
                office.setJob(jobEntity);
            }
        }

        // Employees
        if (jobEntity.getEmployees() == null)
        {
            jobEntity.setEmployees(new ArrayList<JobResourceDO>());
        }
        else
        {
            for (final JobResourceDO employee : jobEntity.getEmployees())
            {
                employee.setJob(jobEntity);
            }
        }
    }

    /**
     * Updates backward references for the given Defect entity: Values, Items, Jobs.
     *
     * @param defect
     */
    public static void updateDefectFields(final DefectDO defect)
    {
        final List<DefectDefectValueDO> defectDefectValues = defect.getValues();

        // Defect Values
        if (defectDefectValues != null)
        {
            for (final DefectDefectValueDO defectDefectValue : defectDefectValues)
            {
                defectDefectValue.setDefect(defect);
            }
        }

        // Defect Items
        if (defect.getItems() != null)
        {
            for (final DefectItemDO item : defect.getItems())
            {
                item.setDefect(defect);
            }
        }

        // Job Defects
        if (defect.getJobs() != null)
        {
            for (final JobDefectDO jobDefect : defect.getJobs())
            {
                jobDefect.setDefect(defect);
            }
        }
    }

    /**
     * Updates backward references for the given Defect entity: Values, Items, Jobs.
     *
     * @param defect
     */
    public static void updateWIPDefectFields(final WIPDefectDO defect)
    {
        final List<WIPDefectDefectValueDO> defectDefectValues = defect.getValues();

        // Defect Values
        if (defectDefectValues != null)
        {
            for (final WIPDefectDefectValueDO defectDefectValue : defectDefectValues)
            {
                defectDefectValue.setDefect(defect);
            }
        }

        // Defect Items
        if (defect.getItems() != null)
        {
            for (final WIPDefectItemDO item : defect.getItems())
            {
                item.setDefect(defect);
            }
        }
    }
}
