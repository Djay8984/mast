package com.baesystems.ai.lr.dao.employees;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.employees.SurveyorPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.enums.EmployeeRoleType;
import com.baesystems.ai.lr.utils.EmployeeUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "SurveyorDao")
public class SurveyorDaoImpl implements SurveyorDao
{
    private static final Logger LOG = LoggerFactory.getLogger(SurveyorDaoImpl.class);

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private LrEmployeeRepository lrEmployeeRepository;

    private final MapperFacade mapper;

    public SurveyorDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.classMap(LrEmployeeDO.class, SurveyorDto.class)
                .byDefault()
                .customize(
                        new EmployeeNameMapper())
                .register();

        factory.registerClassMap(factory.classMap(LrEmployeeRoleDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public final PageResource<SurveyorDto> getSurveyors(final Pageable pageable)
    {
        LOG.debug("getSurveyors - Retrieving all Surveyors");

        final Page<LrEmployeeDO> surveyorData = this.lrEmployeeRepository.findAllEmployeeByRole(pageable,
                EmployeeRoleType.SURVEYOR_ASSESSOR_INSPECTOR.getValue());

        final Page<SurveyorDto> result = surveyorData.map(new Converter<LrEmployeeDO, SurveyorDto>()
        {
            @Override
            public SurveyorDto convert(final LrEmployeeDO surveyor)
            {
                return mapper.map(surveyor, SurveyorDto.class);
            }
        });

        return pageFactory.createPageResource(result, SurveyorPageResourceDto.class);
    }

    @Override
    @Transactional
    public final PageResource<SurveyorDto> getSurveyors(final Pageable pageable, final String search, final Long officeId)
    {
        LOG.debug("getSurveyors - Retrieving all Surveyors matching query name = " + search + ", officeId = " + officeId + ".");

        final Page<LrEmployeeDO> surveyorData = this.lrEmployeeRepository.findAll(pageable, search, officeId,
                EmployeeRoleType.SURVEYOR_ASSESSOR_INSPECTOR.getValue());

        final Page<SurveyorDto> result = surveyorData.map(new Converter<LrEmployeeDO, SurveyorDto>()
        {
            @Override
            public SurveyorDto convert(final LrEmployeeDO surveyor)
            {
                return mapper.map(surveyor, SurveyorDto.class);
            }
        });

        return pageFactory.createPageResource(result, SurveyorPageResourceDto.class);
    }

    @Override
    public SurveyorDto getSurveyor(final Long surveyorId)
    {
        final LrEmployeeDO output = this.lrEmployeeRepository.findOne(surveyorId);
        return output == null ? null : mapper.map(output, SurveyorDto.class);
    }

    public static class EmployeeNameMapper extends CustomMapper<LrEmployeeDO, SurveyorDto>
    {
        @Override
        // Custom mapper to concatenate first and last names from LR Employee data table.
        public void mapAtoB(final LrEmployeeDO enteredDO, final SurveyorDto resultingDto, final MappingContext context)
        {
            resultingDto.setName(EmployeeUtils.concatEmployeeName(enteredDO));
        }
    }
}
