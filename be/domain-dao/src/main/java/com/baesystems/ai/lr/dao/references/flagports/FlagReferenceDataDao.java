package com.baesystems.ai.lr.dao.references.flagports;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FlagStateDto;

public interface FlagReferenceDataDao
{
    FlagStateDto getFlagState(Long flagStateId);

    PageResource<FlagStateDto> findAll(Pageable pageSpecification);

    PageResource<FlagStateDto> findAll(Pageable pageSpecification, String search);

    List<FlagStateDto> getFlags();
}
