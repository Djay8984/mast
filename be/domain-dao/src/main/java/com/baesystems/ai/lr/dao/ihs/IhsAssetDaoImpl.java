package com.baesystems.ai.lr.dao.ihs;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.references.IhsAssetTypeDO;
import com.baesystems.ai.lr.domain.ihs.repositories.IhsAssetRepository;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetTypeDto;

@Repository(value = "IhsAssetDao")
public class IhsAssetDaoImpl implements IhsAssetDao
{
    @Autowired
    private IhsAssetRepository ihsAssetRepository;

    private final MapperFacade mapper;

    public IhsAssetDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(IhsAssetDO.class, IhsAssetDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(IhsAssetTypeDO.class, IhsAssetTypeDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public final IhsAssetDto getIhsAsset(final Long imoNumber)
    {
        final IhsAssetDO output = this.ihsAssetRepository.findOne(imoNumber);
        return output == null ? null : this.mapper.map(output, IhsAssetDto.class);
    }
}
