package com.baesystems.ai.lr.dao.employees;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;

public interface LrEmployeeDao
{
    LrEmployeeDto getLrEmployee(Long employeeId);

    LrEmployeeDto getLrEmployee(String userFullName);

    PageResource<LrEmployeeDto> searchEmployees(Pageable pageable, String search, Long officeId) throws BadPageRequestException;

    String getEmployeeFullName(Long employeeId);

    Boolean lrEmployeeExists(Long employeeId);

    List<LrEmployeeDto> getEmployees();
}
