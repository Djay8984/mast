package com.baesystems.ai.lr.dao.references.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.references.AllowedWorkItemAttributeValueDto;
import com.baesystems.ai.lr.dto.references.ClassRecommendationDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;

public interface JobReferenceDataDao
{
    List<ReferenceDataDto> getJobStatuses();

    List<ReferenceDataDto> getCreditStatuses();

    List<ReferenceDataDto> getEndorsementTypes();

    List<ReferenceDataDto> getWorkItemTypes();

    List<WorkItemActionDto> getWorkItemActions();

    List<ReferenceDataDto> getResolutionStatuses();

    List<ReferenceDataDto> getReportTypes();

    List<ClassRecommendationDto> getClassRecommendations();

    List<AllowedWorkItemAttributeValueDto> getAllowedWorkItemAttributeValues();

    WorkItemActionDto findWorkItemActionFromReferenceCode(String referenceCode);

    List<ReferenceDataDto> getJobCategories();

    ReferenceDataDto getJobCategory(Long categoryId);
}
