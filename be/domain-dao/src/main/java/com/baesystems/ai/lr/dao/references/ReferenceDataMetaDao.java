package com.baesystems.ai.lr.dao.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;

public interface ReferenceDataMetaDao
{
    ReferenceDataVersionDto getTopLevelReferenceDataVersion();

    List<ReferenceDataDto> getDueStatuses();
}
