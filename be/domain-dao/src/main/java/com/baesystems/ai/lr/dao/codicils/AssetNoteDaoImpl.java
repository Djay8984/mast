package com.baesystems.ai.lr.dao.codicils;

import static com.baesystems.ai.lr.domain.mast.repositories.CodicilRepositoryHelper.ASSET_COUNT_COLUMN_NAME;
import static com.baesystems.ai.lr.domain.mast.repositories.CodicilRepositoryHelper.TEMPLATE_COLUMN_NAME;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.codicils.AssetNoteDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetNoteRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPAssetNoteRepository;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.AssetNotePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.CodicilStatusCriteria;
import com.baesystems.ai.lr.utils.CodicilUtils;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "AssetNoteDao")
public class AssetNoteDaoImpl extends AbstractLockingDaoImpl<AssetNoteDO> implements AssetNoteDao
{
    public static final Logger LOG = LoggerFactory.getLogger(AssetNoteDaoImpl.class);

    @Autowired
    private AssetNoteRepository assetNoteRepository;

    @Autowired
    private WIPAssetNoteRepository wipAssetNoteRepository;

    @Autowired
    private ItemDao itemDao;

    private static final DefaultMapperFactory FACTORY;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private SequenceDao sequenceDao;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.classMap(AssetNoteDO.class, AssetNoteDto.class)
                .customize(new StaleObjectMapper<AssetNoteDO, AssetNoteDto>())
                .byDefault().register();
    }

    public AssetNoteDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public AssetNoteDto getAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        LOG.debug("Retrieving Asset Note for ID: {}", assetNoteId);
        final AssetNoteDO assetNoteDO = this.assetNoteRepository.findAssetNote(assetNoteId);

        AssetNoteDto newAssetNoteDto = null;
        if (assetNoteDO != null)
        {
            newAssetNoteDto = getMapper().map(assetNoteDO, AssetNoteDto.class);
        }
        return newAssetNoteDto;
    }

    @Override
    @Transactional
    public PageResource<AssetNoteDto> getAssetNotesByAsset(final Pageable pageable, final Long assetId, final Long inputStatusId)
            throws BadPageRequestException
    {
        LOG.debug("Retrieving Asset Notes for Asset ID: {}", assetId);

        Boolean isOpen = null;
        Long statusId = inputStatusId;

        if (CodicilStatusType.AN_OPEN.getValue().equals(statusId))
        {
            isOpen = true;
        }
        else if (CodicilStatusType.AN_INACTIVE.getValue().equals(statusId))
        {
            statusId = CodicilStatusType.AN_OPEN.getValue();
            isOpen = false;
        }

        final Page<AssetNoteDO> output = this.assetNoteRepository.findAssetNotesByAsset(pageable, assetId, statusId, new Date(), isOpen);
        final Page<AssetNoteDto> result = output.map(new AssetNoteConverter());

        return pageFactory.createPageResource(result, AssetNotePageResourceDto.class);
    }

    @Override
    @Transactional
    public AssetNoteDto saveAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException
    {
        final Long assetId = assetNoteDto.getAsset().getId();
        LOG.debug("Saving Asset Note for Asset ID: {}", assetId);
        final AssetNoteDO assetNoteDO = getMapper().map(assetNoteDto, AssetNoteDO.class);
        assetNoteDO.setSequenceNumber(
                this.sequenceDao.getNextAssetSpecificSequenceValue(assetId, false, this.assetNoteRepository, wipAssetNoteRepository));
        final AssetNoteDO newAssetNoteDO = this.assetNoteRepository.save(assetNoteDO);
        return getMapper().map(newAssetNoteDO, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public AssetNoteDto updateAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException
    {
        LOG.debug("Updating Asset Note with ID: {}", assetNoteDto.getId());
        final AssetNoteDO assetNoteDO = this.assetNoteRepository.merge(getMapper().map(assetNoteDto, AssetNoteDO.class));
        return getMapper().map(assetNoteDO, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public Boolean assetNoteExistsForAsset(final Long assetId, final Long assetNoteId)
    {
        LOG.debug("Checking existence of Asset Note ID: {} for Asset ID: {}", assetNoteId, assetId);
        final Integer existsCount = this.assetNoteRepository.assetNoteExistsForAsset(assetId, assetNoteId);
        return existsCount > 0;
    }

    @Override
    @Transactional
    public void deleteAssetNote(final Long assetNoteId)
    {
        LOG.debug("Deleting Asset Note ID: {}", assetNoteId);
        this.assetNoteRepository.delete(assetNoteId);
    }

    /**
     * Count the number of assets that have asset notes created using the given template IDs. Only assets with the given
     * lifecycle statuses will be counted.
     *
     * @param templateIds
     * @param assetLifecycleStatuses
     * @return {@link TemplateAssetCountDto} containing a HashMap which maps Template IDs to their associated Asset
     *         Counts.
     */
    @Override
    @Transactional
    public TemplateAssetCountDto countAssetsWithAssetNotesForTemplate(final List<Long> templateIds, final List<Long> assetLifecycleStatuses)
    {
        LOG.debug("Counting the number of Assets with Asset Notes for templates: {} and asset lifecycle statuses: {}",
                templateIds, assetLifecycleStatuses);

        // Key = Column Name, Value = Data
        final List<Map<String, Long>> rowData = this.assetNoteRepository.countAssetsWithAssetNotesForTemplate(templateIds, assetLifecycleStatuses);

        // Key = Template ID, Value = Asset Count
        final Map<Long, Integer> templateCountMap = CodicilUtils.populateTemplateCountMap(templateIds, rowData,
                TEMPLATE_COLUMN_NAME, ASSET_COUNT_COLUMN_NAME);

        final TemplateAssetCountDto assetCounts = new TemplateAssetCountDto();
        assetCounts.setCounts(templateCountMap);

        return assetCounts;
    }

    class AssetNoteConverter implements Converter<AssetNoteDO, AssetNoteDto>
    {
        @Override
        public AssetNoteDto convert(final AssetNoteDO assetNoteDO)
        {
            return getMapper().map(assetNoteDO, AssetNoteDto.class);
        }
    }

    /**
     * Gets all asset notes for an asset with a query
     *
     * @param pageable, page specification
     * @param assetId, Asset ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All asset notes for an asset in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<AssetNoteDto> findAllForAsset(final Pageable pageable, final Long assetId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<AssetNoteDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = assetNoteRepository.findAll(pageable, assetId);
        }
        else
        {
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);

            final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(codicilDefectQueryDto.getStatusList(),
                    CodicilType.AN);

            resultPage = assetNoteRepository.findAll(pageable, assetId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilStatusCriteria.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(), codicilDefectQueryDto.getDueDateMax(), new Date(),
                    codicilStatusCriteria.getOpen(),
                    CodicilStatusType.AN_OPEN.getValue());
        }
        final Page<AssetNoteDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), AssetNoteDto.class);
        return pageFactory.createPageResource(result, AssetNotePageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean exists(final Long assetNoteId)
    {
        return this.assetNoteRepository.exists(assetNoteId);
    }

    @Override
    public LockingRepository<AssetNoteDO> getLockingRepository()
    {
        return assetNoteRepository;
    }
}
