package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPCoCRepository;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

@SuppressWarnings("PMD.TooManyMethods")
@Repository(value = "WIPCoCDao")
public class WIPCoCDaoImpl extends AbstractLockingDaoImpl<WIPCoCDO> implements WIPCoCDao
{
    public static final Logger LOG = LoggerFactory.getLogger(WIPCoCDaoImpl.class);

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private WIPCoCRepository wipCoCRepository;

    @Autowired
    private WIPCoCRepository wipCoCWithLinksRepository;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(WIPCoCDO.class, CoCDto.class)
                .customize(new StaleObjectMapper<WIPCoCDO, CoCDto>())
                .byDefault().toClassMap());
    }

    public WIPCoCDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPCoCDO> getLockingRepository()
    {
        return this.wipCoCRepository;
    }

    @Override
    @Transactional
    public CoCDto getCoC(final Long coCId)
    {
        LOG.debug("Retrieving WIP CoC for ID: {}", coCId);
        final WIPCoCDO coCDO = this.wipCoCRepository.findOne(coCId);
        return coCDO == null ? null : getMapper().map(coCDO, CoCDto.class);
    }

    @Override
    @Transactional
    public CoCDto updateCoC(final CoCDto coCDto)
    {
        LOG.debug("Updating WIP CoC with ID: {}", coCDto.getId());

        final WIPCoCDO cocDO = getMapper().map(coCDto, WIPCoCDO.class);

        final WIPCoCDO coCDO = this.wipCoCRepository.merge(cocDO);
        return getMapper().map(coCDO, CoCDto.class);
    }

    @Override
    @Transactional
    public Integer countCoCsForDefect(final Long defectId)
    {
        return this.wipCoCRepository.countCoursesOfActionForDefect(defectId, CodicilStatusType.getCoCStatusesForCourseOfAction());
    }

    @Override
    @Transactional
    public PageResource<CoCDto> getCoCsForJob(final Pageable pageable, final Long jobId, final Long defectId)
    {
        LOG.debug("Retrieving all WIP CoCs for Job {} with defect {}", jobId, defectId);
        final Page<WIPCoCDO> output = this.wipCoCRepository.findCoCsForJob(pageable, jobId, defectId);
        final Page<CoCDto> result = output.map(new WIPCoCConverter());
        return this.pageFactory.createPageResource(result, CoCPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<CoCDto> getCoCsForJob(final Long jobId)
    {
        LOG.debug("Retrieving all WIP CoCs for Job {}.", jobId);
        final List<WIPCoCDO> output = this.wipCoCRepository.findCoCsForJob(jobId);

        return getMapper().mapAsList(output, CoCDto.class);
    }

    @Override
    @Transactional
    public List<CoCDto> getCoCsForReportContent(final Long jobId)
    {
        LOG.debug("Retrieving all WIP CoCs where action has been taken in Job {}.", jobId);
        final List<WIPCoCDO> output = this.wipCoCRepository.findCoCsForReportContent(jobId);

        return getMapper().mapAsList(output, CoCDto.class);
    }

    @Override
    @Transactional
    public List<CoCDto> getCoCsForJob(final Long jobId, final String employeeName, final Date lastUpdatedDate)
    {
        LOG.debug("Finding all WIP CoCs for Job {} restricted by user and report issue date", jobId);
        final List<WIPCoCDO> output = this.wipCoCRepository.findCoCsByJob(jobId, employeeName, lastUpdatedDate);
        return getMapper().mapAsList(output, CoCDto.class);
    }

    @Override
    @Transactional
    public Boolean coCExistsForJob(final Long jobId, final Long cocId)
    {
        final Long existsCount = this.wipCoCRepository.findCoCForJob(jobId, cocId);
        return existsCount != null && existsCount > 0;
    }

    @Override
    @Transactional
    public CoCDto saveCoC(final Long jobId, final CoCDto coCDto)
    {
        WIPCoCDO wipCoCDO = getMapper().map(coCDto, WIPCoCDO.class);
        wipCoCDO.setAsset(new LinkDO());
        wipCoCDO.getAsset().setId(this.jobRepository.getAssetIdForJob(jobId));
        final JobDO job = new JobDO();
        job.setId(jobId);
        wipCoCDO.setJob(job);
        wipCoCDO = this.wipCoCWithLinksRepository.save(wipCoCDO);

        return getMapper().map(wipCoCDO, CoCDto.class);
    }

    class WIPCoCConverter implements Converter<WIPCoCDO, CoCDto>
    {
        @Override
        public CoCDto convert(final WIPCoCDO coCDO)
        {
            return WIPCoCDaoImpl.this.getMapper().map(coCDO, CoCDto.class);
        }
    }

    @Override
    @Transactional
    public void deleteCoC(final Long coCId)
    {
        this.wipCoCRepository.delete(coCId);
    }

    @Override
    @Transactional
    public List<WIPCoCDO> getCoCsForDefect(final Long defectId)
    {
        return this.wipCoCRepository.findCoCsForDefect(defectId);
    }

    @Override
    @Transactional
    public List<CoCDto> getCoCsForJob(final Long jobId, final List<Long> statusList)
    {
        final List<WIPCoCDO> output = this.wipCoCRepository.findCoCsForJob(jobId, statusList);
        return getMapper().mapAsList(output, CoCDto.class);
    }

    @Override
    @Transactional
    public Integer updateScopeConfirmed(final Long jobId, final Boolean scopeConfirmed, final List<Long> statusList)
    {
        return this.wipCoCRepository.updateScopeConfirmed(jobId, scopeConfirmed, statusList);
    }

    @Override
    @Transactional
    public PageResource<CoCDto> findAllForJob(final Pageable pageable, final Long jobId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<WIPCoCDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = this.wipCoCRepository.findAllForWIP(pageable, jobId);
        }
        else
        {
            final Long assetId = this.jobRepository.getAssetIdForJob(jobId);
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);
            resultPage = this.wipCoCRepository.findAllForWIP(pageable, jobId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilDefectQueryDto.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(), codicilDefectQueryDto.getDueDateMax(), new Date(), null,
                    CodicilStatusType.COC_OPEN.getValue());
        }

        final Page<CoCDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), CoCDto.class);
        return this.pageFactory.createPageResource(result, CoCPageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean exists(final Long coCId)
    {
        return this.wipCoCRepository.exists(coCId);
    }

    @Override
    @Transactional
    public Boolean defectHasCoCInStatus(final Long defectId, final List<Long> statusIds)
    {
        return this.wipCoCRepository.defectHasCoCInStatus(defectId, statusIds);
    }

    @Override
    @Transactional
    public Boolean closeCoc(final Long cocId)
    {
        final LinkDO status = new LinkDO();
        status.setId(CodicilStatusType.COC_DELETED.getValue());
        final int updateCount = this.wipCoCRepository.updateStatus(cocId, status);
        return updateCount == 1;
    }
}
