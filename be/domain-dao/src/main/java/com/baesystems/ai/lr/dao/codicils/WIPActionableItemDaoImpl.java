package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.ActionableItemRepository;
import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPActionableItemRepository;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.CodicilStatusCriteria;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPActionableItemDao")
public class WIPActionableItemDaoImpl extends AbstractLockingDaoImpl<WIPActionableItemDO> implements WIPActionableItemDao
{
    public static final Logger LOG = LoggerFactory.getLogger(WIPActionableItemDaoImpl.class);

    @Autowired
    private WIPActionableItemRepository wipActionableItemRepository;

    @Autowired
    private ActionableItemRepository actionableItemRepository;

    @Autowired
    private SequenceDao sequenceDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(WIPActionableItemDO.class, ActionableItemDto.class)
                .customize(new StaleObjectMapper<WIPActionableItemDO, ActionableItemDto>())
                .byDefault().toClassMap());
    }

    public WIPActionableItemDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPActionableItemDO> getLockingRepository()
    {
        return this.wipActionableItemRepository;
    }

    @Override
    @Transactional
    public ActionableItemDto getActionableItem(final Long id)
    {
        LOG.debug("Getting wip actionable item with id {}", id);
        final WIPActionableItemDO wipActionableItemDO = this.wipActionableItemRepository.findActionableItem(id);
        return wipActionableItemDO != null ? getMapper().map(wipActionableItemDO, ActionableItemDto.class) : null;
    }

    @Override
    @Transactional
    public Boolean actionableItemExists(final Long actionableItemId)
    {
        return this.wipActionableItemRepository.exists(actionableItemId);
    }

    @Override
    @Transactional
    public void deleteActionableItem(final Long id)
    {
        LOG.debug("Deleting WIP Actionable Item with id ", id);
        this.wipActionableItemRepository.delete(id);
    }

    @Override
    public Integer countActionableItemsForDefect(final Long defectId)
    {
        return this.wipActionableItemRepository.countCoursesOfActionForDefect(defectId, null);
    }

    @Override
    @Transactional
    public PageResource<ActionableItemDto> getActionableItemsForJob(final Pageable pageSpecification, final Long jobId,
            final Long statusId)
    {
        LOG.debug("Retrieving WIP Actionable Items for Job {}", jobId);
        final Page<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsForJob(pageSpecification, jobId, statusId);
        final Page<ActionableItemDto> result = output.map(new Converter<WIPActionableItemDO, ActionableItemDto>()
        {
            @Override
            public ActionableItemDto convert(final WIPActionableItemDO actionableItemDO)
            {
                return WIPActionableItemDaoImpl.this.getMapper().map(actionableItemDO, ActionableItemDto.class);
            }
        });

        return this.pageFactory.createPageResource(result, ActionableItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public ActionableItemDto createActionableItem(final ActionableItemDto actionableItemDto)
    {
        WIPActionableItemDO actionableItemDO = getMapper().map(actionableItemDto, WIPActionableItemDO.class);
        actionableItemDO.setSequenceNumber(this.sequenceDao
                .getNextAssetSpecificSequenceValue(actionableItemDO.getJob().getId(), true, this.actionableItemRepository,
                        this.wipActionableItemRepository));
        actionableItemDO = this.wipActionableItemRepository.save(actionableItemDO);
        return actionableItemDO != null ? getMapper().map(actionableItemDO, ActionableItemDto.class) : null;
    }

    @Override
    @Transactional
    public ActionableItemDto updateActionableItem(final ActionableItemDto actionableItemDto)
    {
        LOG.debug("Update WIP actionable item id {}", actionableItemDto.getId());
        WIPActionableItemDO actionableItemDO = getMapper().map(actionableItemDto, WIPActionableItemDO.class);
        actionableItemDO = this.wipActionableItemRepository.merge(actionableItemDO);
        return actionableItemDO != null ? getMapper().map(actionableItemDO, ActionableItemDto.class) : null;
    }

    @Override
    @Transactional
    public Boolean actionableItemExistsForJob(final Long jobId, final Long actionableItemId)
    {
        LOG.debug("Verifying if Actionable Item {} exists for Job {}", actionableItemId, jobId);
        return this.wipActionableItemRepository.actionableItemExistsForJob(jobId, actionableItemId) != null;
    }

    @Override
    @Transactional
    public PageResource<ActionableItemDto> findAllForJob(final Pageable pageable, final Long jobId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<WIPActionableItemDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = this.wipActionableItemRepository.findAllForWIP(pageable, jobId);
        }
        else
        {
            final Long assetId = this.jobRepository.getAssetIdForJob(jobId);
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);

            final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(codicilDefectQueryDto.getStatusList(),
                    CodicilType.AI);

            resultPage = this.wipActionableItemRepository.findAllForWIP(pageable, jobId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilStatusCriteria.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(),
                    codicilDefectQueryDto.getDueDateMax(), new Date(), codicilStatusCriteria.getOpen(), CodicilStatusType.AI_OPEN.getValue());
        }

        final Page<ActionableItemDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), ActionableItemDto.class);
        return this.pageFactory.createPageResource(result, ActionableItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public PageResource<ActionableItemDto> getActionableItemForJob(final Pageable pageable, final Long jobId, final Long defectId)
    {
        final Page<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsForJobDefect(pageable, jobId, defectId);
        final Page<ActionableItemDto> result = output.map(new Converter<WIPActionableItemDO, ActionableItemDto>()
        {
            @Override
            public ActionableItemDto convert(final WIPActionableItemDO wipActionableItemDO)
            {
                return WIPActionableItemDaoImpl.this.getMapper().map(wipActionableItemDO, ActionableItemDto.class);
            }
        });
        return this.pageFactory.createPageResource(result, ActionableItemPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<ActionableItemDto> getActionableItemsForJob(final Long jobId, final List<Long> statusList)
    {
        final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(statusList,
                CodicilType.AI);

        final List<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsForJob(jobId,
                codicilStatusCriteria.getStatusList(), new Date(), codicilStatusCriteria.getOpen(),
                CodicilStatusType.AI_OPEN.getValue());
        return getMapper().mapAsList(output, ActionableItemDto.class);
    }

    @Override
    @Transactional
    public List<ActionableItemDto> getActionableItemsForReportContent(final Long jobId)
    {
        final List<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsForReportContent(jobId);
        return getMapper().mapAsList(output, ActionableItemDto.class);
    }

    @Override
    @Transactional
    public List<ActionableItemDto> getActionableItemsForJob(final Long jobId, final String employeeName, final Date lastUpdatedDate)
    {
        LOG.debug("Finding all WIP Actionable Items for Job {} restricted by user and report issue date", jobId);
        final List<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsByJob(jobId, employeeName, lastUpdatedDate);
        return getMapper().mapAsList(output, ActionableItemDto.class);
    }

    @Override
    @Transactional
    public List<ActionableItemDto> getActionableItemsForJob(final Long jobId)
    {
        final List<WIPActionableItemDO> output = this.wipActionableItemRepository.findActionableItemsForJob(jobId);
        return getMapper().mapAsList(output, ActionableItemDto.class);
    }

    @Override
    @Transactional
    public Integer updateScopeConfirmed(final Long jobId, final Boolean scopeConfirmed, final List<Long> statusList)
    {
        return this.wipActionableItemRepository.updateScopeConfirmed(jobId, scopeConfirmed, statusList);
    }
}
