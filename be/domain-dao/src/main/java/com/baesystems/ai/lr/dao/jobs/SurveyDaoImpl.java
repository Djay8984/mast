package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SurveyRepository;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyPageResourceDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "SurveyDao")
public class SurveyDaoImpl extends AbstractLockingDaoImpl<SurveyDO> implements SurveyDao
{
    private static final Logger LOG = LoggerFactory.getLogger(SurveyDaoImpl.class);

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(SurveyDO.class, SurveyDto.class)
                .customize(new StaleObjectMapper<SurveyDO, SurveyDto>())
                .byDefault()
                .toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ScheduledServiceDO.class, ScheduledServiceDto.class)
                .field("serviceCatalogue.id", "serviceCatalogueId")
                .byDefault().toClassMap());
    }

    public SurveyDaoImpl()
    {

        super(FACTORY);
    }

    private class SurveyConverter implements Converter<SurveyDO, SurveyDto>
    {
        @Override
        public SurveyDto convert(final SurveyDO survey)
        {
            return getMapper().map(survey, SurveyDto.class);
        }
    }

    @Override
    @Transactional
    public SurveyDto createSurvey(final SurveyDto surveyDto)
    {
        SurveyDO survey = getMapper().map(surveyDto, SurveyDO.class);
        survey = this.surveyRepository.merge(survey);
        return getMapper().map(survey, SurveyDto.class);
    }

    @Override
    @Transactional
    public SurveyDto getSurvey(final Long surveyId)
    {
        LOG.debug("Retrieving Survey with ID {}", surveyId);

        final SurveyDO survey = this.surveyRepository.findOne(surveyId);
        return survey == null ? null : getMapper().map(survey, SurveyDto.class);
    }

    @Override
    @Transactional
    public SurveyDto updateSurvey(final SurveyDto surveyDto)
    {
        LOG.debug("Updating Survey with ID {}", surveyDto.getId());

        SurveyDO survey = getMapper().map(surveyDto, SurveyDO.class);
        survey = this.surveyRepository.merge(survey);
        return getMapper().map(survey, SurveyDto.class);
    }

    @Override
    @Transactional
    public void updateScheduleDatesFlag(final Long surveyId, final Boolean scheduleDatesUpdated)
    {
        LOG.debug("Updating the Schedule Dates Updated flag to {} for Survey with ID {}", scheduleDatesUpdated, surveyId);

        this.surveyRepository.updateScheduleDatesFlag(surveyId, scheduleDatesUpdated);
    }

    @Override
    @Transactional
    public PageResource<SurveyDto> getSurveys(final Pageable pageable, final Long jobId)
    {
        LOG.debug("Retrieving all Surveys for Job with ID {}", jobId);

        final Page<SurveyDO> surveyDOs = this.surveyRepository.findByJob(pageable, jobId);
        final Page<SurveyDto> surveyDtos = surveyDOs.map(new SurveyConverter());
        return pageFactory.createPageResource(surveyDtos, SurveyPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<SurveyDto> getSurveys(final Long jobId)
    {
        LOG.debug("Retrieving all Surveys for Job with ID {}", jobId);

        final List<SurveyDO> surveys = this.surveyRepository.findByJob(jobId);
        return getMapper().mapAsList(surveys, SurveyDto.class);
    }

    @Override
    @Transactional
    public List<SurveyDto> getSurveysForReportContent(final Long jobId)
    {
        LOG.debug("Retrieving all Surveys where action has been taken in Job with ID {}", jobId);

        final List<SurveyDO> surveys = this.surveyRepository.findForReportContent(jobId);
        return getMapper().mapAsList(surveys, SurveyDto.class);
    }

    @Override
    @Transactional
    public List<SurveyDto> getSurveys(final Long jobId, final String employeeName, final Date lastUpdatedDate)
    {
        LOG.debug("Finding all Surveys for Job {} restricted by user and report issue date", jobId);
        final List<SurveyDO> output = surveyRepository.findSurveysByJob(jobId, employeeName, lastUpdatedDate);
        return getMapper().mapAsList(output, SurveyDto.class);
    }

    @Override
    @Transactional
    public List<SurveyDto> getSurveys(final List<Long> surveyIds)
    {
        LOG.debug("Retrieving Surveys with IDs in {} ", surveyIds);

        final List<SurveyDO> surveys = this.surveyRepository.findAll(surveyIds);

        return getMapper().mapAsList(surveys, SurveyDto.class);
    }

    @Override
    @Transactional
    public void deleteSurvey(final Long surveyId)
    {
        this.surveyRepository.delete(surveyId);
    }

    @Override
    @Transactional
    public Boolean surveyExists(final Long surveyId)
    {
        return this.surveyRepository.exists(surveyId);
    }

    @Override
    @Transactional
    public Boolean surveyExistsForJob(final Long jobId, final Long surveyId)
    {
        LOG.debug("Checking existence of Survey ID: {} for Job ID: {}", surveyId, jobId);
        final Integer existsCount = surveyRepository.surveyExistsForJob(jobId, surveyId);
        return existsCount > 0;
    }

    @Override
    @Transactional
    public Integer updateScopeConfirmed(final Long jobId, final Boolean scopeConfirmed, final List<Long> statusList)
    {
        return this.surveyRepository.updateScopeConfirmed(jobId, scopeConfirmed, statusList);
    }

    @Override
    @Transactional
    public List<Long> getUncreditedSurveysWithNarrativeForJob(final Long jobId)
    {
        return this.surveyRepository.findSurveysWithNarrativeInStatus(jobId, ServiceCreditStatusType.getUncredited());
    }

    @Override
    public LockingRepository<SurveyDO> getLockingRepository()
    {
        return this.surveyRepository;
    }
}
