package com.baesystems.ai.lr.dao.services;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductCatalogueRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "ProductDao")
public class ProductDaoImpl extends AbstractLockingDaoImpl<ProductDO> implements ProductDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ProductDao.class);

    private static final Long NULL = null;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductCatalogueRepository productCatalogueRepository;

    @Autowired
    private AssetRepository assetRepository;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(ProductDO.class, ProductDto.class)
                .customize(new StaleObjectMapper<ProductDO, ProductDto>())
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(ProductCatalogueDO.class, ProductDto.class)
                .field("id", "productCatalogueId")
                .byDefault().toClassMap());
        FACTORY.registerClassMap(FACTORY.classMap(MastDO.class, LinkResource.class)
                .field("id", "id")
                .byDefault().toClassMap());
    }

    public ProductDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public void deleteProduct(final Long productId)
    {
        LOG.debug("Deleting product with Product ID {}", productId);

        this.productRepository.delete(productId);

    }

    @Override
    @Transactional
    /**
     * Create a new entry in the link table (product) to link a given product catalogue to an asset. This table only
     * contains the id's and this does not update the asset or product catalogue tables.
     */
    public void createProduct(final Long assetId, final Long productCatalogueId, final Long schedulingRegimeId)
    {
        LOG.debug("Creating product with Product Catalogue ID {} and Asset ID {} and Scheduling Regime Id {}",
                new Long[]{productCatalogueId, assetId, schedulingRegimeId});

        final ProductDO currentProduct = new ProductDO();
        final AssetDO asset = this.assetRepository.findOne(assetId);
        final LinkDO productCatalogue = new LinkDO();
        productCatalogue.setId(productCatalogueId);
        LinkDO schedulingRegime = null;

        if (schedulingRegimeId != null
                && !this.isProductCatalogueOfType(productCatalogueId, ProductType.CLASSIFICATION.getValue()))
        {
            schedulingRegime = new LinkDO();
            schedulingRegime.setId(schedulingRegimeId);
        }

        currentProduct.setAsset(asset);
        currentProduct.setProductCatalogue(productCatalogue);
        currentProduct.setSchedulingRegime(schedulingRegime);
        currentProduct.setDeleted(false);
        this.productRepository.merge(currentProduct);
    }

    @Override
    @Transactional
    public List<ProductDto> getProducts(final Long assetId)
    {
        LOG.debug("Retrieving all selected Product Catalogues for asset with ID {}", assetId);

        final List<ProductDO> products = this.productRepository.findSelectedProductsForAsset(assetId);

        return products == null ? null : this.mapProducts(products);
    }

    @Override
    @Transactional
    public ProductDO getProductDO(final Long productId)
    {
        LOG.debug("Retrieving Product with ID {}", productId);
        return this.productRepository.findOne(productId);
    }

    @Override
    @Transactional
    public ProductDto getProductDto(final Long productId)
    {
        final ProductDO productDo = this.getProductDO(productId);
        return productDo == null ? null : getMapper().map(productDo, ProductDto.class);
    }

    private List<ProductDto> mapProducts(final List<ProductDO> productDOs)
    {
        final List<ProductDto> productDtos = new ArrayList<ProductDto>();

        for (final ProductDO productDO : productDOs)
        {
            productDtos.add(mapProduct(productDO));
        }

        return productDtos;
    }

    private ProductDto mapProduct(final ProductDO productDO)
    {
        final ProductDto productDto = getMapper().map(productDO, ProductDto.class);

        final ProductCatalogueDO productCatalogueDO = this.productCatalogueRepository.findOne(productDO.getProductCatalogue().getId());

        productDto.setDescription(productCatalogueDO.getDescription());
        productDto.setDisplayOrder(productCatalogueDO.getDisplayOrder());
        productDto.setName(productCatalogueDO.getName());
        productDto.setProductCatalogueId(productCatalogueDO.getId());
        productDto.setProductGroup(linkDOToLinkResource(productCatalogueDO.getProductGroup()));
        productDto.setProductType(
                linkDOToLinkResource(productCatalogueDO.getProductGroup() == null ? null : productCatalogueDO.getProductGroup().getProductType()));

        return productDto;
    }

    private LinkResource linkDOToLinkResource(final MastDO generalDO)
    {
        return generalDO == null ? null : getMapper().map(generalDO, LinkResource.class);
    }

    @Override
    @Transactional
    public void updateProductSchedulingRegime(final Long productId, final Long schedulingRegimeId)
    {
        final ProductDO product = this.productRepository.findOne(productId);

        LinkDO schedulingRegime = null;

        if (schedulingRegimeId != null)
        {
            schedulingRegime = new LinkDO();
            schedulingRegime.setId(schedulingRegimeId);
        }

        product.setSchedulingRegime(schedulingRegime);

        this.productRepository.merge(product);
    }

    @Override
    @Transactional
    public Boolean isProductCatalogueSelectedForAsset(final Long assetId, final Long productCatalogueId, final Long schedulingRegimeId)
    {
        final Boolean isClassification = this.isProductCatalogueOfType(productCatalogueId, ProductType.CLASSIFICATION.getValue());
        return this.productRepository.isProductCatalogueSelectedForAsset(assetId, productCatalogueId, isClassification ? NULL : schedulingRegimeId);
    }

    @Override
    @Transactional
    public Boolean isProductCatalogueOfType(final Long productCatalogueId, final Long productTypeId)
    {
        return this.productCatalogueRepository.productCatalogueIsOfType(productCatalogueId, productTypeId);
    }

    @Override
    public LockingRepository<ProductDO> getLockingRepository()
    {
        return this.productRepository;
    }
}
