package com.baesystems.ai.lr.utils;

/**
 * Provides a contact for hard deleting entities, (done at the checking of an asset if no versionable changes
 * have been made)
 */
public interface HardDeletingDao
{
    void hardDeleteAssetVersionedEntities(Long assetId, Long versionId);
}
