package com.baesystems.ai.lr.dao.references.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.domain.mast.entities.references.AssignedSchedulingTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AssociatedSchedulingTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.HarmonisationTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductGroupDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductRuleSetDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingDueTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingRegimeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCreditStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceGroupDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceRulesetDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AssignedSchedulingTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AssociatedSchedulingTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.HarmonisationTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductCatalogueRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductGroupRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SchedulingDueTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SchedulingRegimeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SchedulingTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceCatalogueRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceCreditStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceGroupRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceRulesetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedAssetRepository;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ProductGroupExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductTypeDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SchedulingTypeDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import com.baesystems.ai.lr.dto.references.ServiceGroupDto;
import com.baesystems.ai.lr.enums.ProductType;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "ServiceReferenceDataDao")
public class ServiceReferenceDataDaoImpl implements ServiceReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(ServiceReferenceDataDaoImpl.class);

    @Autowired
    private ProductCatalogueRepository productCatalogueRepository;

    @Autowired
    private VersionedAssetRepository versionedAssetRepository;

    @Autowired
    private SchedulingTypeRepository schedulingTypeRepository;

    @Autowired
    private ServiceStatusRepository serviceStatusRepository;

    @Autowired
    private ServiceTypeRepository serviceTypeRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Autowired
    private ProductGroupRepository productGroupRepository;

    @Autowired
    private ServiceCatalogueRepository serviceCatalogueRepository;

    @Autowired
    private SchedulingRegimeRepository schedulingRegimeRepository;

    @Autowired
    private ServiceGroupRepository serviceGroupRepository;

    @Autowired
    private ServiceRulesetRepository serviceRulesetRepository;

    @Autowired
    private AssignedSchedulingTypeRepository assignedSchedulingTypeRepository;

    @Autowired
    private AssociatedSchedulingTypeRepository associatedSchedulingTypeRepository;

    @Autowired
    private HarmonisationTypeRepository harmonisationTypeRepository;

    @Autowired
    private SchedulingDueTypeRepository schedulingDueTypeRepository;

    @Autowired
    private ServiceCreditStatusRepository serviceCreditStatusRepository;

    @Autowired
    private AssetRepository assetRepository;

    private final MapperFacade mapper;
    private final MapperFacade productModelMapper;

    public ServiceReferenceDataDaoImpl()
    {
        this.mapper = initialiseStandardMapper();
        this.productModelMapper = initialiseProductModelMapper();
    }

    private MapperFacade initialiseStandardMapper()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(ProductDO.class, ProductCatalogueDto.class)
                .field("productCatalogue.id", "id")// display the id of the product catalogue not the product.
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductCatalogueDO.class, ProductCatalogueDto.class)
                .field("productGroup.productType", "productType").byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductGroupDO.class, ProductGroupDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ServiceStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ServiceTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        return factory.getMapperFacade();
    }

    private MapperFacade initialiseProductModelMapper()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(ProductTypeDO.class, ProductTypeExtendedDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductTypeDO.class, ProductTypeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductGroupDO.class, ProductGroupExtendedDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductGroupDO.class, ProductGroupDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductCatalogueDO.class, ProductCatalogueExtendedDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ProductRuleSetDO.class, ProductCatalogueExtendedDto.class).byDefault().toClassMap());

        return factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<ProductCatalogueDto> getProductCatalogues()
    {
        LOG.debug("Retrieving all Product Catalogues");

        final List<ProductCatalogueDO> products = this.productCatalogueRepository.findAll();

        return products == null ? null : this.mapper.mapAsList(products, ProductCatalogueDto.class);
    }

    @Override
    @Transactional
    public List<ProductTypeDto> getProductTypes()
    {
        LOG.debug("Retrieving All Product Types");
        return this.mapper.mapAsList(this.productTypeRepository.findAll(), ProductTypeDto.class);
    }

    @Override
    @Transactional
    public List<ProductGroupDto> getProductGroups()
    {
        LOG.debug("Retrieving All Product Groups");
        return this.mapper.mapAsList(this.productGroupRepository.findAll(), ProductGroupDto.class);
    }

    /**
     * Returns the list of product catalogues in the rule set for a give asset.
     *
     * @param assetId
     * @param productType
     * @return product catalogue list.
     */
    @Override
    @Transactional
    public List<ProductCatalogueDto> getProductCataloguesForAsset(final Long assetId, final List<ProductType> productTypes)
    {
        LOG.debug("Retrieving all selected Product Catalogues for asset with ID {}", assetId);
        List<ProductCatalogueDO> products = null;
        if (productTypes != null)
        {
            products = getProductCatalogueDOsForAsset(assetId, productTypes);
        }

        return products == null ? null : this.mapper.mapAsList(products, ProductCatalogueDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getServiceStatuses()
    {
        LOG.debug("Retrieving all service statuses");

        final List<ServiceStatusDO> serviceStatuses = this.serviceStatusRepository.findAll();
        return serviceStatuses == null ? null : this.mapper.mapAsList(serviceStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getServiceStatus(final Long serviceStatusId)
    {
        LOG.debug("Retrieving Service Status with ID {}", serviceStatusId);

        final ServiceStatusDO serviceStatus = this.serviceStatusRepository.findOne(serviceStatusId);
        return serviceStatus == null ? null : this.mapper.map(serviceStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getServiceTypes()
    {
        LOG.debug("Retrieving all Service Types");

        final List<ServiceTypeDO> serviceTypes = this.serviceTypeRepository.findAll();
        return serviceTypes == null ? null : this.mapper.mapAsList(serviceTypes, ReferenceDataDto.class);
    }

    /**
     * Returns the list of product catalogues in the rule set for a give asset.
     *
     * @param assetId
     * @return product catalogue list.
     */
    @Override
    @Transactional
    public List<ProductCatalogueDO> getProductCatalogueDOsForAsset(final Long assetId, final List<ProductType> productTypes)
    {
        final Long publishedId = this.assetRepository.findOne(assetId).getPublishedVersionId();

        final VersionedAssetDO asset = this.versionedAssetRepository.findOne(new VersionedAssetPK(assetId, publishedId));

        List<ProductCatalogueDO> products = null;
        final LinkDO productRuleset = asset != null ? asset.getProductRuleSet() : null;
        if (productRuleset != null)
        {
            final List<Long> ids = new ArrayList<Long>();
            for (final ProductType productTypeSingle : productTypes)
            {
                ids.add(productTypeSingle.getValue());
            }
            products = this.productCatalogueRepository.findByRulesetIdAndProductType(productRuleset.getId(), ids);
        }

        return products;
    }

    @Override
    @Transactional
    public List<ServiceCatalogueDto> getServiceCatalogues()
    {
        LOG.debug("Retrieving all Service Catalogues");

        final List<ServiceCatalogueDO> serviceCatalogues = this.serviceCatalogueRepository.findAll();
        return serviceCatalogues == null ? null : this.mapper.mapAsList(serviceCatalogues, ServiceCatalogueDto.class);
    }

    @Override
    @Transactional
    public ServiceCatalogueDto getServiceCatalogue(final Long catalogueId)
    {
        LOG.debug("Retrieving Service Catalogue with ID: {}", catalogueId);

        final ServiceCatalogueDO serviceCatalogue = this.serviceCatalogueRepository.findOne(catalogueId);
        return serviceCatalogue == null ? null : this.mapper.map(serviceCatalogue, ServiceCatalogueDto.class);
    }

    @Override
    @Transactional
    public List<ProductTypeExtendedDto> getProductModel()
    {
        LOG.debug("Retrieving the Product Model");

        final List<ProductTypeDO> productTypes = this.productTypeRepository.findAll();

        return productTypes == null ? null : this.productModelMapper.mapAsList(productTypes, ProductTypeExtendedDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getSchedulingRegimes()
    {
        LOG.debug("Retrieving all Scheduling Regimes");

        final List<SchedulingRegimeDO> schedulingRegimes = this.schedulingRegimeRepository.findAll();
        return schedulingRegimes == null ? null : this.mapper.mapAsList(schedulingRegimes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getAssignedSchedulingTypes()
    {
        LOG.debug("Retrieving all Assigned Scheduling Types");

        final List<AssignedSchedulingTypeDO> assignedSchedulingTypes = this.assignedSchedulingTypeRepository.findAll();
        return assignedSchedulingTypes == null ? null : this.mapper.mapAsList(assignedSchedulingTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<SchedulingTypeDto> getSchedulingTypes()
    {
        LOG.debug("Retrieving all Scheduling Types");

        final List<SchedulingTypeDO> schedulingTypes = this.schedulingTypeRepository.findAll();
        return schedulingTypes == null ? null : this.mapper.mapAsList(schedulingTypes, SchedulingTypeDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getAssociatedSchedulingTypes()
    {
        LOG.debug("Retrieving all Associated Scheduling Types");

        final List<AssociatedSchedulingTypeDO> associatedSchedulingTypes = this.associatedSchedulingTypeRepository.findAll();
        return associatedSchedulingTypes == null ? null : this.mapper.mapAsList(associatedSchedulingTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ServiceGroupDto> getServiceGroups()
    {
        LOG.debug("Retrieving all Service Groups");

        final List<ServiceGroupDO> serviceGroups = this.serviceGroupRepository.findAll();
        return serviceGroups == null ? null : this.mapper.mapAsList(serviceGroups, ServiceGroupDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getHarmonisationTypes()
    {
        LOG.debug("Retrieving all Harmonisation Types");

        final List<HarmonisationTypeDO> harmonisationTypes = this.harmonisationTypeRepository.findAll();
        return harmonisationTypes == null ? null : this.mapper.mapAsList(harmonisationTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getServiceRulesets()
    {
        LOG.debug("Retrieving all Service Rulesets");

        final List<ServiceRulesetDO> serviceRulesets = this.serviceRulesetRepository.findAll();
        return serviceRulesets == null ? null : this.mapper.mapAsList(serviceRulesets, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getSchedulingDueTypes()
    {
        LOG.debug("Retrieving all Scheduling Due Types");

        final List<SchedulingDueTypeDO> schedulingDueTypes = this.schedulingDueTypeRepository.findAll();
        return schedulingDueTypes == null ? null : this.mapper.mapAsList(schedulingDueTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ServiceCreditStatusDto getServiceCreditStatus(final Long serviceCreditStatusId)
    {
        LOG.debug("Retrieving Service Credid Status with ID {}", serviceCreditStatusId);

        final ServiceCreditStatusDO serviceCreditStatus = this.serviceCreditStatusRepository.findOne(serviceCreditStatusId);
        return serviceCreditStatus == null ? null : this.mapper.map(serviceCreditStatus, ServiceCreditStatusDto.class);
    }

    @Override
    @Transactional
    public List<ServiceCreditStatusDto> getServiceCreditStatuses()
    {
        LOG.debug("Retrieving all Service Credit Statuses");

        final List<ServiceCreditStatusDO> serviceCreditStatuses = this.serviceCreditStatusRepository.findAll();
        return serviceCreditStatuses == null ? null : this.mapper.mapAsList(serviceCreditStatuses, ServiceCreditStatusDto.class);
    }
}
