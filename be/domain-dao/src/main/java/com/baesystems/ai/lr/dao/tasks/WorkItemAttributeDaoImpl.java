package com.baesystems.ai.lr.dao.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemAttributeDO;
import com.baesystems.ai.lr.domain.mast.repositories.WorkItemAttributeRepository;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WorkItemAttributeDao")
public class WorkItemAttributeDaoImpl implements WorkItemAttributeDao
{
    private final MapperFacade mapper;

    @Autowired
    private WorkItemAttributeRepository workItemAttributeRepository;

    public WorkItemAttributeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public WorkItemAttributeDto createWorkItemAttribute(final WorkItemAttributeDto workItemAttribute)
    {
        WorkItemAttributeDO attributeDO = this.mapper.map(workItemAttribute, WorkItemAttributeDO.class);
        attributeDO = this.workItemAttributeRepository.merge(attributeDO);

        return this.mapper.map(attributeDO, WorkItemAttributeDto.class);
    }

}
