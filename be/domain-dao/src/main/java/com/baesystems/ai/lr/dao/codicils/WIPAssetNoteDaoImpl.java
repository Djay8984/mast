package com.baesystems.ai.lr.dao.codicils;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPAssetNoteDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetNoteRepository;
import com.baesystems.ai.lr.domain.mast.repositories.JobRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPAssetNoteRepository;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.AssetNotePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.CodicilStatusCriteria;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.SequenceDao;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPAssetNoteDao")
public class WIPAssetNoteDaoImpl extends AbstractLockingDaoImpl<WIPAssetNoteDO> implements WIPAssetNoteDao
{
    @Autowired
    private WIPAssetNoteRepository wipAssetNoteRepository;

    @Autowired
    private AssetNoteRepository assetNoteRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private SequenceDao sequenceDao;

    public static final Logger LOG = LoggerFactory.getLogger(WIPAssetNoteDaoImpl.class);

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(WIPAssetNoteDO.class, AssetNoteDto.class)
                .customize(new StaleObjectMapper<WIPAssetNoteDO, AssetNoteDto>())
                .byDefault().toClassMap());
    }

    public WIPAssetNoteDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    @Transactional
    public AssetNoteDto getAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        LOG.debug("Retrieving WIP Asset Note for ID: {}", assetNoteId);

        final WIPAssetNoteDO assetNoteDO = this.wipAssetNoteRepository.findOne(assetNoteId);
        return assetNoteDO == null ? null : getMapper().map(assetNoteDO, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public PageResource<AssetNoteDto> getAssetNotesByAsset(final Pageable pageable, final Long assetId, final Long inputStatusId)
            throws BadPageRequestException
    {
        LOG.debug("Retrieving WIP Asset Notes for Asset ID: {}", assetId);

        Boolean isOpen = null;
        Long statusId = inputStatusId;

        if (CodicilStatusType.AN_OPEN.getValue().equals(statusId))
        {
            isOpen = true;
        }
        else if (CodicilStatusType.AN_INACTIVE.getValue().equals(statusId))
        {
            statusId = CodicilStatusType.AN_OPEN.getValue();
            isOpen = false;
        }

        final Page<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesByAsset(pageable, assetId, statusId, new Date(), isOpen);
        final Page<AssetNoteDto> result = output.map(new WIPAssetNoteConverter());
        return this.pageFactory.createPageResource(result, AssetNotePageResourceDto.class);
    }

    @Override
    @Transactional
    public AssetNoteDto saveAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException
    {
        final Long jobId = assetNoteDto.getJob().getId();
        LOG.debug("Saving WIP Asset Note for Job ID: {}", jobId);
        WIPAssetNoteDO assetNoteDO = getMapper().map(assetNoteDto, WIPAssetNoteDO.class);
        assetNoteDO.setSequenceNumber(
                this.sequenceDao.getNextAssetSpecificSequenceValue(jobId, true, this.assetNoteRepository, this.wipAssetNoteRepository));
        assetNoteDO = this.wipAssetNoteRepository.save(assetNoteDO);
        return getMapper().map(assetNoteDO, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public Boolean assetNoteExistsForJob(final Long jobId, final Long assetNoteId)
    {
        LOG.debug("Checking existence of Asset Note ID: {} for Job ID: {}", assetNoteId, jobId);
        final Integer existsCount = this.wipAssetNoteRepository.assetNoteExistsForJob(jobId, assetNoteId);
        return existsCount > 0;
    }

    @Override
    @Transactional
    public PageResource<AssetNoteDto> getAssetNotesForJob(final Pageable pageable, final Long jobId)
            throws BadPageRequestException
    {
        LOG.debug("Finding all WIP Asset Notes for Job {}", jobId);
        final Page<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesByJob(pageable, jobId);
        final Page<AssetNoteDto> result = output.map(new WIPAssetNoteConverter());
        return this.pageFactory.createPageResource(result, AssetNotePageResourceDto.class);
    }

    @Override
    @Transactional
    public List<AssetNoteDto> getAssetNotesForJob(final Long jobId, final List<Long> statusList)
    {
        final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(statusList,
                CodicilType.AN);

        final List<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesByJob(jobId, codicilStatusCriteria.getStatusList(), new Date(),
                codicilStatusCriteria.getOpen(),
                CodicilStatusType.AN_OPEN.getValue());

        return getMapper().mapAsList(output, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public Integer updateScopeConfirmed(final Long jobId, final Boolean scopeConfirmed, final List<Long> statusList)
    {
        return this.wipAssetNoteRepository.updateScopeConfirmed(jobId, scopeConfirmed, statusList);
    }

    @Override
    @Transactional
    public List<AssetNoteDto> getAssetNotesForJob(final Long jobId)
    {
        LOG.debug("Finding all WIP Asset Notes for Job {}", jobId);
        final List<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesByJob(jobId);
        return getMapper().mapAsList(output, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public List<AssetNoteDto> getAssetNotesForReportContent(final Long jobId)
    {
        LOG.debug("Finding all WIP Asset Notes where action has been taken in Job {}", jobId);
        final List<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesForReportContent(jobId);
        return getMapper().mapAsList(output, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public List<AssetNoteDto> getAssetNotesForJob(final Long jobId, final String employeeName, final Date lastUpdatedDate)
    {
        LOG.debug("Finding all WIP Asset Notes for Job {} restricted by user and report issue date", jobId);
        final List<WIPAssetNoteDO> output = this.wipAssetNoteRepository.findAssetNotesByJob(jobId, employeeName, lastUpdatedDate);
        return getMapper().mapAsList(output, AssetNoteDto.class);
    }

    @Override
    @Transactional
    public AssetNoteDto updateAssetNote(final AssetNoteDto assetNoteDto)
    {
        LOG.debug("Updating WIP Asset Note for Asset Note ID: {}", assetNoteDto.getId());

        WIPAssetNoteDO assetNoteDO = getMapper().map(assetNoteDto, WIPAssetNoteDO.class);
        assetNoteDO = this.wipAssetNoteRepository.merge(assetNoteDO);
        return getMapper().map(assetNoteDO, AssetNoteDto.class);
    }

    class WIPAssetNoteConverter implements Converter<WIPAssetNoteDO, AssetNoteDto>
    {
        @Override
        public AssetNoteDto convert(final WIPAssetNoteDO wipAssetNoteDO)
        {
            return WIPAssetNoteDaoImpl.this.getMapper().map(wipAssetNoteDO, AssetNoteDto.class);
        }
    }

    @Override
    @Transactional
    public void deleteAssetNote(final Long assetNoteId)
    {
        LOG.debug("Deleting WIP Asset Note for ID: {}", assetNoteId);
        this.wipAssetNoteRepository.delete(assetNoteId);
    }

    @Override
    public PageResource<AssetNoteDto> findAllForJob(final Pageable pageable, final Long jobId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        Page<WIPAssetNoteDO> resultPage = null;

        if (codicilDefectQueryDto == null)
        {
            resultPage = this.wipAssetNoteRepository.findAllForWIP(pageable, jobId);
        }
        else
        {

            final Long assetId = this.jobRepository.getAssetIdForJob(jobId);
            List<Long> itemIdList = this.itemDao.getItemIdsFromAssetAndType(assetId, codicilDefectQueryDto.getItemTypeList());
            // a null list means we're not filtering by item ID. An empty list isn't handled by HQL so need to add an
            // empty ID.
            itemIdList = DaoUtils.addNonExistentIdToEmptyList(itemIdList);

            final CodicilStatusCriteria codicilStatusCriteria = new CodicilStatusCriteria(codicilDefectQueryDto.getStatusList(),
                    CodicilType.AN);

            resultPage = this.wipAssetNoteRepository.findAllForWIP(pageable, jobId, codicilDefectQueryDto.getSearchString(),
                    codicilDefectQueryDto.getCategoryList(), codicilStatusCriteria.getStatusList(), codicilDefectQueryDto.getConfidentialityList(),
                    itemIdList, codicilDefectQueryDto.getDueDateMin(), codicilDefectQueryDto.getDueDateMax(), new Date(),
                    codicilStatusCriteria.getOpen(),
                    CodicilStatusType.AN_OPEN.getValue());
        }

        final Page<AssetNoteDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), AssetNoteDto.class);
        return this.pageFactory.createPageResource(result, AssetNotePageResourceDto.class);
    }

    @Override
    @Transactional
    public Boolean exists(final Long assetNoteId)
    {
        return this.wipAssetNoteRepository.exists(assetNoteId);
    }

    @Override
    public LockingRepository<WIPAssetNoteDO> getLockingRepository()
    {
        return this.wipAssetNoteRepository;
    }
}
