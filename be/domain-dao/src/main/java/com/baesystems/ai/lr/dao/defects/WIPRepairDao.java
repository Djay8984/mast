package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairDO;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface WIPRepairDao extends BaseRepairDao, LockingDao<WIPRepairDO>
{
    List<RepairDto> getWIPRepairsForWIPDefectsAndWipCoCs(List<Long> wipDefectIds, List<Long> wipCoCIds);
}
