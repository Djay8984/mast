package com.baesystems.ai.lr.dao.references.employees;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeOfficeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRoleRepository;
import com.baesystems.ai.lr.dto.references.EmployeeOfficeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository("EmployeeReferenceDataDao")
public class EmployeeReferenceDataDaoImpl implements EmployeeReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeReferenceDataDaoImpl.class);

    @Autowired
    private LrEmployeeRoleRepository lrEmployeeRoleRepository;

    @Autowired
    private LrEmployeeOfficeRepository lrEmployeeOfficeRepository;

    private final MapperFacade mapper;

    public EmployeeReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(LrEmployeeRoleDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getEmployeeRoles()
    {
        LOG.debug("Retrieving all Employee Roles");

        final List<LrEmployeeRoleDO> roles = this.lrEmployeeRoleRepository.findAll();
        return this.mapper.mapAsList(roles, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getEmployeeRole(final Long roleId)
    {
        LOG.debug("Retrieving all Employee Role with id {}", roleId);

        final LrEmployeeRoleDO role = this.lrEmployeeRoleRepository.findOne(roleId);
        return role == null ? null : this.mapper.map(role, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<EmployeeOfficeDto> getEmployeeOffices()
    {
        LOG.debug("Retrieving all Employee Offices");

        final List<LrEmployeeOfficeDO> roles = this.lrEmployeeOfficeRepository.findAll();
        return this.mapper.mapAsList(roles, EmployeeOfficeDto.class);
    }
}
