package com.baesystems.ai.lr.dao.customers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.OrganisationDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyAddressDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyContactDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyDO;
import com.baesystems.ai.lr.domain.mast.repositories.AssetCustomerRepository;
import com.baesystems.ai.lr.domain.mast.repositories.PartyRepository;
import com.baesystems.ai.lr.dto.assets.AssetCustomerPageResourceDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.OrganisationDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.utils.PageResourceFactory;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "PartyDao")
public class PartyDaoImpl implements PartyDao
{
    @Autowired
    private PartyRepository customerRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    @Autowired
    private AssetCustomerRepository assetCustomerRepository;

    private final MapperFacade mapper;

    public PartyDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(PartyDO.class, PartyDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(OrganisationDO.class, PartyDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(OrganisationDO.class, OrganisationDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AssetCustomerDO.class, CustomerLinkDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public PartyDto getCustomer(final Long customerId)
    {
        final PartyDO output = this.customerRepository.findOne(customerId);
        PartyDto result = null;
        if (output != null)
        {
            if (output instanceof OrganisationDO)
            {
                result = this.mapper.map(output, OrganisationDto.class);
            }
            else
            {
                result = this.mapper.map(output, PartyDto.class);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public List<PartyLightDto> findAll()
    {
        final List<PartyDO> output = this.customerRepository.findAll();
        return this.mapper.mapAsList(output, PartyLightDto.class);
    }

    @Override
    @Transactional
    public PartyDto saveCustomer(final PartyDto customer)
    {
        PartyDO party = this.mapper.map(customer, PartyDO.class);

        if (party.getContacts() != null)
        {
            for (final PartyContactDO contact : party.getContacts())
            {
                contact.setParty(party);
            }
        }

        if (party.getAddresses() != null)
        {
            for (final PartyAddressDO address : party.getAddresses())
            {
                address.setParty(party);
            }
        }

        party = this.customerRepository.merge(party);

        return this.mapper.map(party, PartyDto.class);
    }

    @Override
    @Transactional
    public PageResource<CustomerLinkDto> getAssetCustomers(final Pageable pageable, final Long assetId)
    {
        final Page<AssetCustomerDO> output = assetCustomerRepository.findAll(pageable, assetId);
        final Page<CustomerLinkDto> result = output.map(new Converter<AssetCustomerDO, CustomerLinkDto>()
        {

            @Override
            public CustomerLinkDto convert(final AssetCustomerDO assetCustomer)
            {
                return mapper.map(assetCustomer, CustomerLinkDto.class);
            }
        });
        return pageFactory.createPageResource(result, AssetCustomerPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<AssetCustomerDO> getAssetCustomersForVersionedAsset(final Long assetId, final Long versionedAssetId)
    {
        return this.assetCustomerRepository.getAssetCustomersForVersionedAsset(assetId, versionedAssetId);
    }

    @Override
    @Transactional
    public void hardDeleteAssetVersionedEntities(final Long assetId, final Long versionId)
    {
        final List<AssetCustomerDO> assetCustomers = this.getAssetCustomersForVersionedAsset(assetId, versionId);
        if (assetCustomers != null)
        {
            assetCustomers.stream().forEach(customer -> this.assetCustomerRepository.hardDelete(customer.getId()));
        }
    }
}
