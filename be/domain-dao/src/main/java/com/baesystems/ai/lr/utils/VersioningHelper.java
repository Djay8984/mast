package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Provides nicities for handling asset and item PKs with regard to versioning
 */

@Service
public class VersioningHelper
{
    @Autowired
    private AssetDao assetDao;

    /*
     * Returns the published VersionedItem PK
     */
    public VersionedItemPK getVersionedItemPK(final Long itemId, final Long assetId)
    {
        return getVersionedItemPK(itemId, assetId, null, false);
    }

    /*
     * Constructs a specific ItemPK, defaulting versioning appropriately
     */
    public VersionedItemPK getVersionedItemPK(final Long itemId, final Long assetId, final Long aVersionId, final Boolean allowDraft)
    {
        final Long versionId = Boolean.TRUE.equals(allowDraft)
                ? this.assetDao.getDraftVersion(assetId)
                : aVersionId != null
                ? aVersionId
                : this.assetDao.getPublishedVersion(assetId);

        return new VersionedItemPK(itemId, assetId, versionId);
    }

    public Long getVersion(final Long assetId, final Long aVersionId, final Boolean allowDraft)
    {
        return Boolean.TRUE.equals(allowDraft) ? this.assetDao.getDraftVersion(assetId)
                : aVersionId != null ? aVersionId : this.assetDao.getPublishedVersion(assetId);

    }

}
