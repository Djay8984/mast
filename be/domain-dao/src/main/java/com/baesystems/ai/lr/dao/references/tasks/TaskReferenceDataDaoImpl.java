package com.baesystems.ai.lr.dao.references.tasks;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.PostponementTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.PostponementTypeRepository;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "TaskReferenceDataDao")
public class TaskReferenceDataDaoImpl implements TaskReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(TaskReferenceDataDaoImpl.class);

    @Autowired
    private PostponementTypeRepository postponementTypeRepository;

    private final MapperFacade mapper;

    public TaskReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(PostponementTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();

    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getPostponementTypes()
    {
        LOG.debug("Retrieving all Postponement Types");

        final List<PostponementTypeDO> postponementTypes = this.postponementTypeRepository.findAll();
        return postponementTypes == null ? null : mapper.mapAsList(postponementTypes, ReferenceDataDto.class);
    }
}
