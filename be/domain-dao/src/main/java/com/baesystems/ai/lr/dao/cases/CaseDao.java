package com.baesystems.ai.lr.dao.cases;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface CaseDao extends LockingDao<CaseDO>
{
    Boolean caseExists(Long caseId);

    CaseWithAssetDetailsDto getCase(Long caseId);

    void deleteCase(Long caseId);

    CaseWithAssetDetailsDto createCase(CaseWithAssetDetailsDto caseDto);

    CaseWithAssetDetailsDto updateCase(CaseWithAssetDetailsDto caseDto);

    PageResource<CaseDto> getCases(Pageable pageable, CaseQueryDto query);

    Long countCasesForAsset(Long assetId);

    Boolean getCanDeleteCase(Long caseId);

    List<Long> findDuplicateCasesByAssetId(Long businessProcessId, Long assetId);

    List<CaseDto> getCasesForAsset(Long assetId, List<Long> caseStatuses);

    Long getCaseStatusId(Long caseId);

    void updateCaseStatus(Long caseId, Long statusId);

    Long getPreviousCaseStatusId(Long caseId);

    PageResource<CaseDto> getCases(Pageable pageable, AbstractQueryDto query);
}
