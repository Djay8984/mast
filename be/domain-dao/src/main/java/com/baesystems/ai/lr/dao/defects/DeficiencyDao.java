package com.baesystems.ai.lr.dao.defects;

import com.baesystems.ai.lr.dao.codicils.CodicilDefectQueryDao;
import com.baesystems.ai.lr.domain.mast.entities.defects.DeficiencyDO;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface DeficiencyDao extends BaseDeficiencyDao, LockingDao<DeficiencyDO>, CodicilDefectQueryDao<DeficiencyDto>
{

}
