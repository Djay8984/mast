package com.baesystems.ai.lr.dao.defects;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.paging.PageResource;

public interface BaseRepairDao
{
    Boolean repairExists(Long repairId);

    RepairDto getRepair(Long repairId);

    PageResource<RepairDto> getRepairsForCoC(Long cocId, Pageable pageable);

    PageResource<RepairDto> getRepairsForDefect(Pageable pageable, Long defectId);

    RepairDto saveRepair(RepairDto repairDto);

    Boolean repairExistsForDefect(Long repairId, Long defectId);

    Boolean repairExistsForCoC(Long repairId, Long cocId);

    Boolean getAreAllItemsPermanentlyRepaired(Long defectId);

    Boolean getDefectHasPermanentRepair(Long defectId);

    Integer countRepairsForDefect(Long defectId);

    Boolean getCocHasPermanentRepair(Long wipCocId);
}
