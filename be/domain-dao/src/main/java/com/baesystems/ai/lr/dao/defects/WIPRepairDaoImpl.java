package com.baesystems.ai.lr.dao.defects;

import java.util.List;

import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.defects.RepairDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairItemDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPRepairRepository;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DomainObjectFactory;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

@Repository(value = "WIPRepairDao")
public class WIPRepairDaoImpl extends AbstractLockingDaoImpl<WIPRepairDO> implements WIPRepairDao
{
    private static final Logger LOG = LoggerFactory.getLogger(WIPRepairDaoImpl.class);

    @Autowired
    private WIPRepairRepository wipRepairRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    private static final DefaultMapperFactory FACTORY;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();

        FACTORY.registerObjectFactory(new DomainObjectFactory<RepairDO>(WIPRepairDO.class), RepairDO.class);
        FACTORY.registerClassMap(FACTORY.classMap(WIPRepairDO.class, RepairDto.class)
                .customize(new StaleObjectMapper<WIPRepairDO, RepairDto>())
                .byDefault().toClassMap());
    }

    public WIPRepairDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPRepairDO> getLockingRepository()
    {
        return this.wipRepairRepository;
    }

    @Override
    @Transactional
    public Boolean repairExists(final Long repairId)
    {
        return this.wipRepairRepository.exists(repairId);
    }

    @Override
    @Transactional
    public RepairDto saveRepair(final RepairDto repairDto)
    {
        WIPRepairDO repair = getMapper().map(repairDto, WIPRepairDO.class);

        // Set all repair items' parents to be this DO.
        if (repair.getRepairs() != null)
        {
            for (final WIPRepairItemDO value : repair.getRepairs())
            {
                value.setRepair(repair);
            }
        }
        repair = this.wipRepairRepository.merge(repair);
        return getMapper().map(repair, RepairDto.class);
    }

    @Override
    @Transactional
    public PageResource<RepairDto> getRepairsForDefect(final Pageable pageable, final Long defectId)
    {
        final Page<WIPRepairDO> output = this.wipRepairRepository.findRepairsForDefect(pageable, defectId);
        final Page<RepairDto> result = output.map(wipRepairDO -> getMapper().map(wipRepairDO, RepairDto.class));
        return this.pageFactory.createPageResource(result, RepairPageResourceDto.class);
    }

    @Override
    @Transactional
    public PageResource<RepairDto> getRepairsForCoC(final Long cocId, final Pageable pageable)
    {
        final Page<WIPRepairDO> repairData = this.wipRepairRepository.findAllByCoC(pageable, cocId);

        final Page<RepairDto> result = repairData.map(repair -> getMapper().map(repair, RepairDto.class));
        return this.pageFactory.createPageResource(result, RepairPageResourceDto.class);
    }

    @Override
    @Transactional
    public List<RepairDto> getWIPRepairsForWIPDefectsAndWipCoCs(final List<Long> wipDefectIds, final List<Long> wipCoCIds)
    {
        final List<WIPRepairDO> repairData = this.wipRepairRepository.findWIPRepairsForWIPDefectsAndWipCoCs(wipDefectIds, wipCoCIds);
        return getMapper().mapAsList(repairData, RepairDto.class);
    }

    @Override
    @Transactional
    public Boolean repairExistsForDefect(final Long repairId, final Long defectId)
    {
        LOG.debug(String.format("Verifying that WIP Repair %d exists for WIP Defect %d", repairId, defectId));
        return this.wipRepairRepository.repairExistsForDefect(repairId, defectId);
    }

    @Override
    @Transactional
    public Boolean repairExistsForCoC(final Long repairId, final Long cocId)
    {
        LOG.debug(String.format("Verifying that WIP Repair %d exists for WIP CoC %d", repairId, cocId));
        return this.wipRepairRepository.repairExistsForCoC(repairId, cocId);
    }

    @Override
    @Transactional
    public RepairDto getRepair(final Long repairId)
    {
        LOG.debug("Get WIP Repair with ID {}", repairId);

        final WIPRepairDO defect = this.wipRepairRepository.findOne(repairId);
        return defect == null ? null : getMapper().map(defect, RepairDto.class);
    }

    @Override
    @Transactional
    public Boolean getAreAllItemsPermanentlyRepaired(final Long wipDefectId)
    {
        final Integer itemCount = this.wipRepairRepository.countItemsRequiringRepair(wipDefectId, RepairActionType.PERMANENT.value());
        return itemCount == null || itemCount == 0;
    }

    @Override
    @Transactional
    public Boolean getDefectHasPermanentRepair(final Long wipDefectId)
    {
        final Integer permamentRepairCount = this.wipRepairRepository.countPermanentRepairs(wipDefectId, RepairActionType.PERMANENT.value());
        return permamentRepairCount != null && permamentRepairCount > 0;
    }

    /**
     * Counts the total number of temporary and permanent repairs associated with a defect.
     */
    @Override
    @Transactional
    public Integer countRepairsForDefect(final Long defectId)
    {
        return this.wipRepairRepository.countRepairsForDefect(defectId, RepairActionType.getRepairsForDefect());
    }

    @Override
    @Transactional
    public Boolean getCocHasPermanentRepair(final Long wipCocId)
    {
        final Integer permamentRepairCount = wipRepairRepository.countCocPermanentRepairs(wipCocId, RepairActionType.PERMANENT.value());
        return permamentRepairCount != null && permamentRepairCount > 0;
    }
}
