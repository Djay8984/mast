package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.domain.entities.BaseDO;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface LockingDao<T extends BaseDO>
{
    <S extends BaseDto> S findOneWithLock(Long id, Class<S> returnDtoType, final StaleCheckType entityType) throws StaleObjectException;

    /* Should this be moved to asset Dao? */
    <S extends AssetLightDto> S findOneWithLock(Long id, Long assetVersionId, Class<S> returnDtoType, final StaleCheckType entityType)
            throws StaleObjectException;

}
