package com.baesystems.ai.lr.dao.jobs;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.jobs.FollowUpActionDO;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.LockingDao;

public interface FollowUpActionDao extends LockingDao<FollowUpActionDO>
{
    PageResource<FollowUpActionDto> getFollowUpActionsForJob(Pageable pageable, Long jobId) throws BadPageRequestException;

    FollowUpActionDto saveFollowUpAction(FollowUpActionDto followUpAction, ReportDto report);

    Boolean followUpActionExistsForJob(Long jobId, Long followUpActionId);

    FollowUpActionDto updateFollowUpActionStatus(Long followUpActionId, LinkResource linkResource);

}
