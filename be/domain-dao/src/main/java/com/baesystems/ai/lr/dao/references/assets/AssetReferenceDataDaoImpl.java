package com.baesystems.ai.lr.dao.references.assets;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.ActionTakenDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AllowedAssetAttributeValueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AssetCategoryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AssetLifecycleStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AssetTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeDataTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassDepartmentDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassMaintenanceStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassNotationDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CountryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CustomerFunctionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CustomerRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.references.IacsSocietyDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ItemRelationshipTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MigrationStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductRuleSetDO;
import com.baesystems.ai.lr.domain.mast.entities.references.SocietyRuleSetDO;
import com.baesystems.ai.lr.domain.mast.repositories.ActionTakenRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AllowedAssetAttributeValueRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AssetCategoryRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AssetLifecycleStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AssetTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeDataTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ClassDepartmentRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ClassMaintenanceStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ClassNotationRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ClassStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CountryRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CustomerFunctionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CustomerRelationshipRepository;
import com.baesystems.ai.lr.domain.mast.repositories.IacsSocietyRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ItemRelationshipTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.MigrationStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ProductRuleSetRepository;
import com.baesystems.ai.lr.domain.mast.repositories.SocietyRuleSetRepository;
import com.baesystems.ai.lr.dto.references.AllowedAssetAttributeValueDto;
import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ClassDepartmentDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import com.baesystems.ai.lr.dto.validation.AttributeRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "AssetReferenceDataDao")
@SuppressWarnings("PMD.GodClass")
public class AssetReferenceDataDaoImpl implements AssetReferenceDataDao
{

    private static final Logger LOG = LoggerFactory.getLogger(AssetReferenceDataDaoImpl.class);

    @Autowired
    private IacsSocietyRepository iacsSocietyRepository;

    @Autowired
    private SocietyRuleSetRepository societyRuleSetRepository;

    @Autowired
    private ProductRuleSetRepository productRuleSetRepository;

    @Autowired
    private AssetCategoryRepository assetCategoryRepository;

    @Autowired
    private AssetTypeRepository assetTypeRepository;

    @Autowired
    private CustomerFunctionRepository customerFunctionRepository;

    @Autowired
    private CustomerRelationshipRepository customerRelationshipRepository;

    @Autowired
    private AttributeTypeRepository attributeTypeRepository;

    @Autowired
    private AllowedAssetAttributeValueRepository allowedAssetAttributeValueRepository;

    @Autowired
    private AttributeDataTypeRepository attributeDataTypeRepository;

    @Autowired
    private AssetLifecycleStatusRepository assetLifecycleStatusRepository;

    @Autowired
    private ClassStatusRepository classStatusRepository;

    @Autowired
    private ItemRelationshipTypeRepository itemRelationshipTypeRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ClassMaintenanceStatusRepository classMaintenanceStatusRepository;

    @Autowired
    private ClassDepartmentRepository classDepartmentRepository;

    @Autowired
    private ActionTakenRepository actionTakenRepository;

    @Autowired
    private ClassNotationRepository classNotationRepository;

    @Autowired
    private MigrationStatusRepository migrationStatusRepository;

    private final MapperFacade mapper;

    public AssetReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(AttributeTypeDO.class, AttributeRuleDto.class)
                .field("itemType.id", "itemTypeId")
                .field("id", "attributeTypeId")
                .field("valueType.description", "valueType")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ItemTypeRelationshipDO.class, ItemRuleDto.class)
                .field("toItemType.id", "toItemType")
                .field("fromItemType.id", "fromItemType")
                .field("itemRelationshipType.id", "relationshipTypeId")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(IacsSocietyDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(SocietyRuleSetDO.class, SocietyRulesetDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AssetCategoryDO.class, AssetCategoryDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CustomerFunctionDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CustomerRelationshipDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AttributeTypeDO.class, AttributeTypeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AllowedAssetAttributeValueDO.class, AllowedAssetAttributeValueDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ItemTypeDO.class, ItemTypeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AttributeDataTypeDO.class, ReferenceDataDto.class)
                .field("id", "id")
                .field("description", "name")
                .field("description", "description")
                .field("deleted", "deleted")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ItemRelationshipTypeDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(AssetTypeDO.class, AssetTypeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CountryDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ClassMaintenanceStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ClassDepartmentDO.class, ClassDepartmentDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(ClassNotationDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(MigrationStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public ReferenceDataDto getIacsSociety(final Long id)
    {
        final IacsSocietyDO output = this.iacsSocietyRepository.findOne(id);
        return output == null ? null : mapper.map(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getIacsSocieties()
    {
        final List<IacsSocietyDO> output = this.iacsSocietyRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public SocietyRulesetDto getRuleSet(final Long id)
    {
        final SocietyRuleSetDO output = societyRuleSetRepository.findOne(id);
        return output == null ? null : mapper.map(output, SocietyRulesetDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getProductRuleSet(final Long id)
    {
        final ProductRuleSetDO output = productRuleSetRepository.findOne(id);
        return output == null ? null : mapper.map(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getProductRuleSets()
    {
        final List<ProductRuleSetDO> output = productRuleSetRepository.findAll();
        return output == null ? null : mapper.mapAsList(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<SocietyRulesetDto> getRuleSets(final String category, final Boolean isLrRuleset)
    {
        final List<SocietyRuleSetDO> output = societyRuleSetRepository.findAll(category, isLrRuleset);
        return output == null ? null : mapper.mapAsList(output, SocietyRulesetDto.class);
    }

    @Override
    @Transactional
    public List<AssetCategoryDto> getAssetCategories()
    {
        LOG.debug("Retrieving all Asset Categories");

        final List<AssetCategoryDO> categories = this.assetCategoryRepository.findAll();

        return mapper.mapAsList(categories, AssetCategoryDto.class);
    }

    @Override
    @Transactional
    public AssetCategoryDto getAssetCategory(final Long assetCategoryId)
    {
        LOG.debug("Retrieving Asset Category with ID {}", assetCategoryId);

        final AssetCategoryDO assetCategoryData = this.assetCategoryRepository.findOne(assetCategoryId);

        return assetCategoryData == null ? null : mapper.map(assetCategoryData, AssetCategoryDto.class);
    }

    @Override
    @Transactional
    public List<AssetTypeDto> getAssetTypes()
    {
        LOG.debug("Retrieving all Asset Types");

        final List<AssetTypeDO> types = this.assetTypeRepository.findAll();

        return mapper.mapAsList(types, AssetTypeDto.class);
    }

    @Override
    @Transactional
    public AssetTypeDto getAssetType(final Long assetTypeId)
    {
        LOG.debug("Retrieving Asset Type with ID {}", assetTypeId);

        final AssetTypeDO assetTypeData = this.assetTypeRepository.findOne(assetTypeId);

        return assetTypeData == null ? null : mapper.map(assetTypeData, AssetTypeDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCustomerFunctions()
    {
        LOG.debug("Retrieving all Customer Functions");

        final List<CustomerFunctionDO> functions = this.customerFunctionRepository.findAll();

        return mapper.mapAsList(functions, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCustomerFunction(final Long customerFunctionId)
    {
        LOG.debug("Retrieving Customer Function with ID {}", customerFunctionId);

        final CustomerFunctionDO customerFunctionData = this.customerFunctionRepository.findOne(customerFunctionId);

        return customerFunctionData == null ? null : mapper.map(customerFunctionData, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCustomerRelationships()
    {
        LOG.debug("Retrieving all Customer Relationships");

        final List<CustomerRelationshipDO> relationships = this.customerRelationshipRepository.findAll();

        return mapper.mapAsList(relationships, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCustomerRelationship(final Long customerRelationshipId)
    {
        LOG.debug("Retrieving Customer Relationship with ID {}", customerRelationshipId);

        final CustomerRelationshipDO customerRelationshipData = this.customerRelationshipRepository.findOne(customerRelationshipId);

        return customerRelationshipData == null ? null : mapper.map(customerRelationshipData, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<AttributeTypeDto> getAttributeTypes()
    {
        LOG.debug("Retrieving all Attribute Types");

        final List<AttributeTypeDO> attributeTypeList = this.attributeTypeRepository.findAll();

        return mapper.mapAsList(attributeTypeList, AttributeTypeDto.class);
    }

    @Override
    @Transactional
    public AttributeTypeDto getAttributeType(final Long attributeTypeId)
    {
        LOG.debug("Retrieving Attribute Type with ID {}", attributeTypeId);

        final AttributeTypeDO attributeTypeData = this.attributeTypeRepository.findOne(attributeTypeId);

        return attributeTypeData == null ? null : mapper.map(attributeTypeData, AttributeTypeDto.class);
    }

    @Override
    @Transactional
    public List<AttributeTypeDto> getDefaultAndMandatoryAttributeTypesForItemType(final Long itemTypeId)
    {
        LOG.debug("Retrieving mandatory and default Attribute Types for item type with ID {}", itemTypeId);

        final List<AttributeTypeDO> attributeTypes = this.attributeTypeRepository.getMandatoryAttributeTypesForItemType(itemTypeId);

        return mapper.mapAsList(attributeTypes, AttributeTypeDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getAttributeTypeValue(final Long typeId)
    {
        final AttributeDataTypeDO output = this.attributeDataTypeRepository.findOne(typeId);
        return output == null ? null : mapper.map(output, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getAttributeTypeValues()
    {
        LOG.debug("Retrieving all Attribute Type Values");

        final List<AttributeDataTypeDO> attributeTypeValueList = this.attributeDataTypeRepository.findAll();

        return mapper.mapAsList(attributeTypeValueList, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getAssetLifecycleStatuses()
    {
        LOG.debug("Retrieving all Asset Lifecycle Statuses");

        final List<AssetLifecycleStatusDO> assetLifecycleStatusList = this.assetLifecycleStatusRepository.findAll();

        return mapper.mapAsList(assetLifecycleStatusList, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getAssetLifecycleStatus(final Long lifecycleStatusId)
    {
        LOG.debug("Retrieving all Asset Lifecycle Status with id {}", lifecycleStatusId);

        final AssetLifecycleStatusDO assetLifecycleStatus = this.assetLifecycleStatusRepository.findOne(lifecycleStatusId);

        return assetLifecycleStatus == null ? null : mapper.map(assetLifecycleStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getClassStatuses()
    {
        LOG.debug("Retrieving all Class Statuses");

        final List<ClassStatusDO> classStatusList = this.classStatusRepository.findAll();

        return mapper.mapAsList(classStatusList, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getClassStatus(final Long classStatusId)
    {
        LOG.debug("Retrieving all Class Status with id : {}", classStatusId);

        final ClassStatusDO classStatus = this.classStatusRepository.findOne(classStatusId);

        return classStatus == null ? null : mapper.map(classStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public ClassDepartmentDto getClassDepartment(final Long classDepartmentId)
    {
        LOG.debug("Retrieving all Class Department with id : {}", classDepartmentId);

        final ClassDepartmentDO classDepartment = this.classDepartmentRepository.findOne(classDepartmentId);

        return classDepartment == null ? null : mapper.map(classDepartment, ClassDepartmentDto.class);
    }

    @Override
    @Transactional
    public List<AllowedAssetAttributeValueDto> getAllowedAssetAttributeValues()
    {
        LOG.debug("Retrieving all Allowed Asset Attribute Values");
        final List<AllowedAssetAttributeValueDO> allowedAssetAttributeValues = this.allowedAssetAttributeValueRepository.findAll();
        return mapper.mapAsList(allowedAssetAttributeValues, AllowedAssetAttributeValueDto.class);
    }

    @Override
    @Transactional
    public List<AttributeRuleDto> getAttributeRules()
    {
        LOG.debug("Retrieving all Attribute Types");

        final List<AttributeTypeDO> attributeTypeList = this.attributeTypeRepository.findAll();

        return mapper.mapAsList(attributeTypeList, AttributeRuleDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getItemRelationshipTypes()
    {
        LOG.debug("Retrieving all Item Relationship Types");

        final List<ItemRelationshipTypeDO> itemRelationshipTypes = this.itemRelationshipTypeRepository.findAll();

        return this.mapper.mapAsList(itemRelationshipTypes, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCountries()
    {
        LOG.debug("Retrieving all Countries");

        final List<CountryDO> countries = this.countryRepository.findAll();

        return this.mapper.mapAsList(countries, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getClassMaintenanceStatuses()
    {
        LOG.debug("Retrieving all Class Maintenance Statuses");

        final List<ClassMaintenanceStatusDO> statuses = this.classMaintenanceStatusRepository.findAll();

        return this.mapper.mapAsList(statuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ClassDepartmentDto> getClassDepartments()
    {
        LOG.debug("Retrieving all Class Departments");

        final List<ClassDepartmentDO> departments = this.classDepartmentRepository.findAll();

        return this.mapper.mapAsList(departments, ClassDepartmentDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getActionsTaken()
    {
        LOG.debug("Retrieving all Actions Taken");

        final List<ActionTakenDO> actionsTaken = this.actionTakenRepository.findAll();

        return this.mapper.mapAsList(actionsTaken, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getClassNotations()
    {
        LOG.debug("Retrieving all Class Notations");

        final List<ClassNotationDO> classNotations = this.classNotationRepository.findAll();

        return this.mapper.mapAsList(classNotations, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getMigrationStatuses()
    {
        LOG.debug("Retrieving all Migration Statuses");

        final List<MigrationStatusDO> migrationStatuses = this.migrationStatusRepository.findAll();

        return this.mapper.mapAsList(migrationStatuses, ReferenceDataDto.class);
    }
}
