package com.baesystems.ai.lr.dao.references.jobs;

import java.util.List;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.references.CertificateActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CertificateStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CertificateTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.CertificateActionRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CertificateStatusRepository;
import com.baesystems.ai.lr.domain.mast.repositories.CertificateTypeRepository;
import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

@Repository(value = "CertificateReferenceDataDao")
public class CertificateReferenceDataDaoImpl implements CertificateReferenceDataDao
{
    private static final Logger LOG = LoggerFactory.getLogger(CertificateReferenceDataDaoImpl.class);

    @Autowired
    private CertificateTypeRepository certificateTypeRepository;

    @Autowired
    private CertificateStatusRepository certificateStatusRepository;

    @Autowired
    private CertificateActionRepository certificateActionRepository;

    private final MapperFacade mapper;

    public CertificateReferenceDataDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.registerClassMap(factory.classMap(CertificateTypeDO.class, CertificateTypeDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CertificateStatusDO.class, ReferenceDataDto.class).byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(CertificateActionDO.class, CertificateActionDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public CertificateTypeDto getCertificateType(final Long certificateTypeId)
    {
        LOG.debug("Getting certificate type with id {}.", certificateTypeId);

        final CertificateTypeDO certificateType = this.certificateTypeRepository.findOne(certificateTypeId);

        return certificateType == null ? null : this.mapper.map(certificateType, CertificateTypeDto.class);
    }

    @Override
    @Transactional
    public ReferenceDataDto getCertificateStatus(final Long certificateStatusId)
    {
        LOG.debug("Getting certificate status with id {}.", certificateStatusId);

        final CertificateStatusDO certificateStatus = this.certificateStatusRepository.findOne(certificateStatusId);

        return certificateStatus == null ? null : this.mapper.map(certificateStatus, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public CertificateActionDto getCertificateAction(final Long certificateActionId)
    {
        LOG.debug("Getting certificate action with id {}.", certificateActionId);

        final CertificateActionDO certificateAction = this.certificateActionRepository.findOne(certificateActionId);

        return certificateAction == null ? null : this.mapper.map(certificateAction, CertificateActionDto.class);
    }

    @Override
    @Transactional
    public List<CertificateTypeDto> getCertificateTypes()
    {
        LOG.debug("Retrieving all Certificate Types");

        final List<CertificateTypeDO> certificateTypes = this.certificateTypeRepository.findAll();
        return mapper.mapAsList(certificateTypes, CertificateTypeDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCertificateStatuses()
    {
        LOG.debug("Retrieving all Certificate Statuses");

        final List<CertificateStatusDO> certificateStatuses = this.certificateStatusRepository.findAll();
        return mapper.mapAsList(certificateStatuses, ReferenceDataDto.class);
    }

    @Override
    @Transactional
    public List<ReferenceDataDto> getCertificateActions()
    {
        LOG.debug("Retrieving all Certificate Actions");

        final List<CertificateActionDO> certificateActions = this.certificateActionRepository.findAll();
        return mapper.mapAsList(certificateActions, ReferenceDataDto.class);
    }
}
