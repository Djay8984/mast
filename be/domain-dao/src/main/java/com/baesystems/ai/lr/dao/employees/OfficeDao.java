package com.baesystems.ai.lr.dao.employees;

import java.util.List;

import com.baesystems.ai.lr.dto.references.OfficeDto;

public interface OfficeDao
{
    OfficeDto getOffice(Long officeId);

    List<OfficeDto> findAll();

    OfficeDto getOfficeByCode(String code);

    String getOfficeName(Long officeId);
}
