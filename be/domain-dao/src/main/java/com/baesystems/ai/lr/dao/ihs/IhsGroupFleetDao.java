package com.baesystems.ai.lr.dao.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;

public interface IhsGroupFleetDao
{
    IhsGroupFleetDto getIhsGroupFleet(Long imoNumber);
}
