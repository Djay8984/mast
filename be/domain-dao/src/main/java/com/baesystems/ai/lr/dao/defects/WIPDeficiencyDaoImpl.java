package com.baesystems.ai.lr.dao.defects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDeficiencyDO;
import com.baesystems.ai.lr.domain.mast.repositories.LockingRepository;
import com.baesystems.ai.lr.domain.mast.repositories.WIPDeficiencyRepository;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.utils.AbstractLockingDaoImpl;
import com.baesystems.ai.lr.utils.DaoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.StaleObjectMapper;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@Repository(value = "WIPDeficiencyDao")
public class WIPDeficiencyDaoImpl extends AbstractLockingDaoImpl<WIPDeficiencyDO> implements WIPDeficiencyDao
{
    public static final Logger LOG = LoggerFactory.getLogger(WIPDefectDaoImpl.class);
    private static final DefaultMapperFactory FACTORY;

    @Autowired
    private WIPDeficiencyRepository wipDeficiencyRepository;

    @Autowired
    private PageResourceFactory pageFactory;

    static
    {
        FACTORY = new DefaultMapperFactory.Builder().build();
        FACTORY.registerClassMap(FACTORY.classMap(WIPDeficiencyDO.class, DeficiencyDto.class)
                .customize(new StaleObjectMapper<WIPDeficiencyDO, DeficiencyDto>())
                .byDefault().toClassMap());
    }

    public WIPDeficiencyDaoImpl()
    {
        super(FACTORY);
    }

    @Override
    public LockingRepository<WIPDeficiencyDO> getLockingRepository()
    {
        return this.wipDeficiencyRepository;
    }

    /**
     * Gets all WIP Deficiencies for a job with a query
     *
     * @param pageable, page specification
     * @param jobId, Job ID
     * @param codicilDefectQueryDto, query parameter object
     * @return All Deficiencies for a job in database with a query in a paginated list.
     */
    @Override
    @Transactional
    public PageResource<DeficiencyDto> findAllForJob(final Pageable pageable, final Long jobId, final CodicilDefectQueryDto codicilDefectQueryDto)
    {
        final Page<WIPDeficiencyDO> resultPage = this.wipDeficiencyRepository.findAllForJob(pageable, jobId);
        final Page<DeficiencyDto> result = DaoUtils.mapDOPageToDtoPage(resultPage, getMapper(), DeficiencyDto.class);
        return this.pageFactory.createPageResource(result, DeficiencyPageResourceDto.class);
    }

}
