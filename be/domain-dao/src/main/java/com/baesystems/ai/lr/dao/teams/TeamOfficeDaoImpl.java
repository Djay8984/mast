package com.baesystems.ai.lr.dao.teams;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamOfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.TeamOfficeRepository;
import com.baesystems.ai.lr.dto.references.TeamOfficeDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository(value = "TeamOfficeDao")
public class TeamOfficeDaoImpl implements TeamOfficeDao
{
    private final MapperFacade mapper;

    @Autowired
    private TeamOfficeRepository teamOfficeRepository;

    public TeamOfficeDaoImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(TeamOfficeDO.class, TeamOfficeDto.class)
                .byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    @Override
    @Transactional
    public List<TeamOfficeDto> findAll()
    {
        final List<TeamOfficeDO> output = this.teamOfficeRepository.findAll();
        return mapper.mapAsList(output, TeamOfficeDto.class);
    }
}
