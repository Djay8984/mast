package com.baesystems.ai.lr.dao.jobs;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.utils.LockingDao;

public interface SurveyDao extends LockingDao<SurveyDO>
{
    SurveyDto createSurvey(SurveyDto surveyDto);

    SurveyDto getSurvey(Long surveyId);

    SurveyDto updateSurvey(SurveyDto surveyDto);

    void updateScheduleDatesFlag(Long surveyId, Boolean scheduleDatesUpdated);

    PageResource<SurveyDto> getSurveys(Pageable pageable, Long jobId);

    List<SurveyDto> getSurveys(Long jobId);

    List<SurveyDto> getSurveys(Long jobId, String employeeName, Date lastUpdatedDate);

    List<SurveyDto> getSurveys(List<Long> surveyIds);

    void deleteSurvey(Long surveyId);

    Boolean surveyExists(Long surveyId);

    Boolean surveyExistsForJob(Long jobId, Long surveyId);

    Integer updateScopeConfirmed(Long jobId, Boolean scopeConfirmed, List<Long> statusList);

    List<Long> getUncreditedSurveysWithNarrativeForJob(Long jobId);

    List<SurveyDto> getSurveysForReportContent(Long jobId);
}
