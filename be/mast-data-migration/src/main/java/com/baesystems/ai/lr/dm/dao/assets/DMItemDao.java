package com.baesystems.ai.lr.dm.dao.assets;

import com.baesystems.ai.lr.dto.assets.ItemDto;

public interface DMItemDao
{
    ItemDto createItem(Long assetId, ItemDto item);
}
