package com.baesystems.ai.lr.dm.validation.service;

import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.validation.service.ServiceValidationHelper;

@Primary
@Component(value = "DMServiceValidationHelper")
@SuppressWarnings("PMD.UselessOverridingMethod")
public class DMServiceValidationHelper extends ServiceValidationHelper
{

    @Override
    protected ProductDO validateProduct(final String inputProductId) throws BadRequestException, RecordNotFoundException
    {
        return super.validateProduct(inputProductId);
    }

    @Override
    protected void validateServiceRelationships(final List<ServiceCatalogueDto> proposedServiceCatalogueList)
            throws MastBusinessException, BadRequestException
    {
        super.validateServiceRelationships(proposedServiceCatalogueList);
    }

    @Override
    protected void validateServiceDuplication(final List<ServiceCatalogueDto> proposedServiceCatalogueList, final List<ScheduledServiceDto> serviceList)
            throws MastBusinessException, BadRequestException
    {
        super.validateServiceDuplication(proposedServiceCatalogueList, serviceList);
    }

    @Override
    protected void validateCyclePeriodicty(final List<ScheduledServiceDto> fullServiceList, final List<ServiceCatalogueDto> serviceCatalogues)
            throws BadRequestException
    {
        super.validateCyclePeriodicty(fullServiceList, serviceCatalogues);
    }

}
