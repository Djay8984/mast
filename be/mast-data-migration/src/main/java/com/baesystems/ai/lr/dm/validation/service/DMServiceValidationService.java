package com.baesystems.ai.lr.dm.validation.service;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ServiceDateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Service(value = "DMServiceValidationService")
public class DMServiceValidationService
{
    @Autowired
    private ValidationService validationService;

    @Resource(name = "DMServiceValidationHelper")
    private DMServiceValidationHelper serviceValidationHelper;

    @Resource(name = "DMServiceReferenceDataValidation")
    private DMServiceReferenceDataValidation serviceReferenceDataValidation;

    @Autowired
    private ServiceDateValidationService dateValidator;

    @Autowired
    private ServiceDao serviceDao;

    public void validateDMService(final String inputAssetId, final ScheduledServiceListDto newServices)
            throws BadRequestException, RecordNotFoundException, ParseException, MastBusinessException
    {
        this.validationService.verifyId(inputAssetId);
        this.validationService.verifyBody(newServices);

        final Long assetId = Long.parseLong(inputAssetId);

        for (final ScheduledServiceDto newService : newServices.getScheduledServices())
        { // validate that the refdata attached to the service exists and that the ids are valid.
            this.validationService.verifyField(newService.getAsset().getId().toString(), inputAssetId,
                    String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", newService.getAsset().getId().toString(), inputAssetId));
            this.serviceReferenceDataValidation.validateServiceRefData(newService);
            this.dateValidator.validateAssignedDate(newService);
            this.dateValidator.validateDateRanges(newService);
        }

        final List<ScheduledServiceDto> fullServiceList = this.serviceDao.getServicesForAsset(assetId);
        fullServiceList.addAll(newServices.getScheduledServices());

        final List<ServiceCatalogueDto> serviceCatalogues = fullServiceList.stream()
                .map(service -> this.serviceDao.getServiceCatalogue(service.getServiceCatalogueId()))
                .collect(Collectors.toList());

        // test
        this.serviceValidationHelper.validateCyclePeriodicty(fullServiceList, serviceCatalogues);
        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogues);
        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogues, fullServiceList);
    }

}
