package com.baesystems.ai.lr.dm.service.assets;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface DMAssetService
{
    AssetDto createAsset(AssetLightDto assetLightDto) throws MastBusinessException;
}
