package com.baesystems.ai.lr.dm.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Service(value = "DMReportValidationService")
public class DMReportValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private JobValidationService jobValidationService;

    public void validateReportSave(final String jobId, final ReportDto reportDto)
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        this.jobValidationService.verifyJobIdExists(jobId);

        this.validationService.verifyBody(reportDto);
        this.validationService.verifyIdNull(reportDto.getId());
        this.validationService.verifyNumberFormat(jobId);
        final String jobIdInBody = reportDto.getJob().getId().toString();
        this.validationService.verifyField(jobIdInBody, jobId,
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", jobIdInBody, jobId));
    }
}
