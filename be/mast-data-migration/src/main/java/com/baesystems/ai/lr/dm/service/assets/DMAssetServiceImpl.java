package com.baesystems.ai.lr.dm.service.assets;

import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.jcabi.aspects.Loggable;

@Service(value = "DMAssetService")
@Loggable(Loggable.DEBUG)
public class DMAssetServiceImpl implements DMAssetService
{
    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Override
    @Transactional
    public AssetDto createAsset(final AssetLightDto assetLightDto) throws MastBusinessException
    {
        final ItemTypeDto rootItemType = this.itemTypeDao.getRootItemTypeForAssetCategory(assetLightDto.getAssetCategory().getId().intValue());
        if (rootItemType == null)
        {
            throw new MastBusinessException(
                    String.format("Cannot create a root item for Asset Category: %d", assetLightDto.getAssetCategory().getId()));
        }

        if (assetLightDto.getHarmonisationDate() == null)
        {
            assetLightDto.setHarmonisationDate(assetLightDto.getBuildDate());
        }

        if (assetLightDto.getHullIndicator() == null)
        {
            assetLightDto.setHullIndicator(1);
        }

        // Create Asset
        final AssetMetaDto assetMetaDto = this.assetDao.createAssetMeta();

        //Save the new ID on the inbound Asset DTO
        assetLightDto.setId(assetMetaDto.getId());
        assetLightDto.setAssetVersionId(1L);

        //Create the Versioned Asset
        final AssetLightDto newLightAssetDto = this.assetDao.createAsset(assetLightDto);

        //Updated the Assets Published version to the new Versioned Asset
        assetMetaDto.setPublishedVersionId(1L);
        this.assetDao.updateAssetMeta(assetMetaDto);

        //Create Root Item and Versioned Item
        final Long itemId = createRootItem(assetLightDto.getId(), rootItemType);

        //Set the Root item on the Asset
        this.assetDao.setRootItemOnAsset(newLightAssetDto, itemId);

        // Retrieve and return a fresh full DTO rather than juggle the existing light dtos.
        return this.assetDao.getAsset(newLightAssetDto.getId(), newLightAssetDto.getAssetVersionId(), AssetDto.class);
    }

    public Long createRootItem(final Long assetId, final ItemTypeDto itemType)
    {
        //Create Root Item
        final ItemMetaDto itemMetaDto = this.itemPersistDao.createItemMeta(assetId);

        // Create the root item separately
        final ItemLightDto rootItemForAsset = new ItemLightDto();
        rootItemForAsset.setId(itemMetaDto.getId());
        rootItemForAsset.setAssetId(assetId);
        rootItemForAsset.setAssetVersionId(1L);

        rootItemForAsset.setItemType(makeLinkResource(itemType.getId()));
        rootItemForAsset.setName(itemType.getName());
        rootItemForAsset.setReviewed(Boolean.FALSE);
        rootItemForAsset.setDisplayOrder(1);
        this.itemPersistDao.createAssetRootItem(assetId, rootItemForAsset);

        return itemMetaDto.getId();

    }

    private LinkResource makeLinkResource(final Long id)
    {
        final LinkResource linkResource = new LinkResource();
        linkResource.setId(id);
        return linkResource;
    }

}
