package com.baesystems.ai.lr.dm.service.jobs;

import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

public interface DMWorkItemService
{
    WorkItemLightDto createWorkItem(final WorkItemLightDto workItem);

    WorkItemLightDto createWipWorkItem(final WorkItemLightDto workItem);
}
