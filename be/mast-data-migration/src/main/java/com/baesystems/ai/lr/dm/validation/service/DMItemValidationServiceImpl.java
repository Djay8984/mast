package com.baesystems.ai.lr.dm.validation.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.service.BaseItemValidationService;
import com.baesystems.ai.lr.validation.utils.ValidationStringUtils;

@Service(value = "DMItemValidationService")
public class DMItemValidationServiceImpl extends BaseItemValidationService
{

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private AttributeDao attributeDao;

    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private ValidationStringUtils validationStringUtils;

    private static final String INCONSISTENT_ATTRIBUTE_VALUE = "This is inconsistent with attribute type %d.";

    /**
     * Method to validate a new item.
     * <p>
     * This method validates that:<br>
     * - The item is of a valid type.<br>
     * - The item relationships have valid to items that already exist for the given asset.<br>
     * - The relationships are valid according to the reference data.<br>
     * - The parent id is not null.<br>
     * - The parent item exists for the asset and is able to accept this item given the max occurs and item types.<br>
     */
    public void validatePostItem(final Long assetId, final ItemDto item) throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        // Validate if id is null
        if (item.getId() != null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.ID_NOT_NULL, item.getId()));
        }

        // This API is only aimed to create item by item using the parent id.
        if (item.getRelated() != null && !item.getRelated().isEmpty())
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "Related items is not empty"));
        }

        // Validation if item type is valid is done in the relationship validation
        final Long itemTypeId = item.getItemType().getId();
        final Integer assetCategoryId = this.assetDao.getAssetCategory(assetId).intValue();

        verifyParentItem(assetId, assetCategoryId, item);
        verifyAttributes(itemTypeId, item.getAttributes());
    }

    /**
     * Method to validate the parent of a posted item.
     * <p>
     * This method validates that:<br>
     * - The parent id is not null.<br>
     * - The parent item exists for the asset and is able to accept this item given the max occurs and item types.<br>
     */
    private void verifyParentItem(final Long assetId, final Integer assetCategoryId, final ItemDto item)
            throws MastBusinessException, RecordNotFoundException, BadRequestException
    {
        if (item.getParentItem() == null || item.getParentItem().getId() == null)
        { // the parent id should not be null, because the root item should already exist.
            throw new MastBusinessException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "parent item"));
        }
        else
        {
            // validate that the parent item exists.
            final boolean idAssetItemPathValid = this.itemDao.itemExistsForAsset(assetId, item.getParentItem().getId());
            final Long draftVersionId = this.assetDao.getDraftVersion(assetId);
            final LazyItemDto parentItem = this.itemDao.getItem(new VersionedItemPK(item.getParentItem().getId(), assetId, draftVersionId));

            if (parentItem.getId() == null || !idAssetItemPathValid)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.INVALID_ITEM_PATH, item.getParentItem().getId(), assetId));
            }

            // Validate that this item is of the correct type for the given parent.
            validateChildItemType(item.getItemType().getId(), parentItem.getItemType().getId(), assetCategoryId, parentItem.getId());

            // validate that the parent can accept this item as a child.
            validateWithinMaxOccurs(parentItem, item.getItemType().getId(), 1, assetCategoryId);
        }
    }

    /**
     * This method will iterate through all the attributes to validate each attribute value. At the same time it is
     * populating a attribute type map <attribute type id, attribute type dto> so it doesn't need to go to the database
     * to get them again when doing the rest of the attribute validation.
     *
     * @param attributes list
     * @return attribute type map <attribute type id, attribute type dto>
     * @throws BadRequestException
     */
    private Map<Long, AttributeTypeDto> validateAttributeValue(final List<AttributeDto> attributes) throws BadRequestException
    {
        // Validate attribute value
        final Map<Long, AttributeTypeDto> attributeTypeMap = new HashMap<>();
        final List<String> attributeValueResults = new ArrayList<>();

        for (final AttributeDto attribute : attributes)
        {
            final Long attributeTypeId = attribute.getAttributeType().getId();
            if (!attributeTypeMap.containsKey(attributeTypeId))
            {
                attributeTypeMap.put(attributeTypeId,
                        this.assetReferenceDataDao.getAttributeType(attributeTypeId));
            }

            // validate the attribute
            this.validationStringUtils.validateString(attribute.getValue(), attributeTypeMap.get(attributeTypeId).getDescription(), "value",
                    attributeValueResults,
                    String.format(INCONSISTENT_ATTRIBUTE_VALUE, attributeTypeId));

            if (!attributeValueResults.isEmpty())
            {
                final String[] resultsArray = attributeValueResults.toArray(new String[attributeValueResults.size()]);
                final String errorMessage = String.format(ExceptionMessagesUtils.VALIDATION_FAILED, Arrays.toString(resultsArray));
                throw new BadRequestException(errorMessage);
            }
        }

        return attributeTypeMap;

    }

    /**
     * Method to validate the attribute list for a given item type.
     * <p>
     * This method validates that:<br>
     * - Each attribute is valid according to the attribute validation rules.<br>
     * - All of the mandatory attributes are present at least once.<br>
     * - The number of occurrences of each attribute type falls between min occurs and max occurs (inclusive of both)
     * for that attribute type.<br>
     */

    private void verifyAttributes(final Long itemTypeId, final List<AttributeDto> attributes) throws BadRequestException
    {
        if (attributes != null)
        {
            final Map<Long, Integer> typeCount = processAttribributeList(attributes);

            // make sure all of the mandatory attributes for this item type are present in the list
            final List<Long> mandatoryAttributes = this.attributeDao.getMandatoryAttributeTypeIdsForAnItemTypeId(itemTypeId);

            if (!typeCount.keySet().containsAll(mandatoryAttributes))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.DEPENDENCY_ERROR_GENERAL, "item type", itemTypeId, "all",
                        "mandatory attributes", mandatoryAttributes.toString()));
            }

            final Map<Long, AttributeTypeDto> attributeTypeMap = validateAttributeValue(attributes);

            // loop through the item types that are present in the list and validate the number of occurrences against
            // min and max occurs for that attribute type. This does not repeat the above because if an attribute was
            // not present at all it would not be detected here.
            final StringBuilder errorMessage = new StringBuilder();

            for (final Entry<Long, Integer> type : typeCount.entrySet())
            {
                final AttributeTypeDto attributeType = attributeTypeMap.get(type.getKey());

                if (!itemTypeId.equals(attributeType.getItemType().getId()))
                {
                    errorMessage.append(String.format(ExceptionMessagesUtils.MISMATCH_ERROR_MSG, "Attribute type", attributeType.getId(),
                            "item type",
                            itemTypeId));
                }
                if (attributeType.getMinOccurs() > type.getValue())
                {
                    errorMessage.append(String.format(ExceptionMessagesUtils.MIN_MAX_OCCURS_VIOLATION, "attribute type", attributeType.getId(),
                            attributeType.getName(), "least", attributeType.getMinOccurs()));
                }
                if (attributeType.getMaxOccurs() != 0 && attributeType.getMaxOccurs() < type.getValue())
                {
                    errorMessage.append(String.format(ExceptionMessagesUtils.MIN_MAX_OCCURS_VIOLATION, "attribute type", attributeType.getId(),
                            attributeType.getName(), "most", attributeType.getMaxOccurs()));
                }
            }

            if (errorMessage.length() > 0)
            {
                throw new BadRequestException(errorMessage.toString());
            }
        }
    }

    private Map<Long, Integer> processAttribributeList(final List<AttributeDto> attributes) throws BadRequestException
    {
        final Map<Long, Integer> typeCount = new HashMap<>();

        for (final AttributeDto attribute : attributes)
        {
            // add to the count of this type or create a new counter if this is the first one.
            final Long attributeTypeId = attribute.getAttributeType().getId();
            if (typeCount.keySet().contains(attributeTypeId))
            {
                typeCount.put(attributeTypeId, typeCount.get(attributeTypeId) + 1);
            }
            else
            {
                typeCount.put(attributeTypeId, 1);
            }
        }
        return typeCount;
    }
}
