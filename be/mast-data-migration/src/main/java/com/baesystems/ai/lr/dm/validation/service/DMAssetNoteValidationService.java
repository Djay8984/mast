package com.baesystems.ai.lr.dm.validation.service;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.service.AssetNoteValidationServiceImpl;

@Service(value = "DMAssetNoteValidationService")
public class DMAssetNoteValidationService extends AssetNoteValidationServiceImpl
{
    @Autowired
    private DateHelper dateHelper;

    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();

    @Override
    public void verifyImposedDateNotHistorical(final AssetNoteDto assetNoteDto) throws BadRequestException
    {
        super.verifyImposedDateNotHistorical(assetNoteDto);

        // Implement extra validation for DM
        if (!dateHelper.isTodayOrAfter(assetNoteDto.getImposedDate()))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.HISTORICAL_IMPOSED_DATE, dateFormat.format(assetNoteDto.getImposedDate())));
        }

    }
}
