package com.baesystems.ai.lr.dm.validation.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.validation.service.ServiceReferenceDataValidation;

@Primary
@Component(value = "DMServiceReferenceDataValidation")
@SuppressWarnings("PMD.UselessOverridingMethod")
public class DMServiceReferenceDataValidation extends ServiceReferenceDataValidation
{

    @Override
    protected void validateServiceRefData(final ScheduledServiceDto newService) throws BadRequestException, RecordNotFoundException
    {
        super.validateServiceRefData(newService);
    }




}
