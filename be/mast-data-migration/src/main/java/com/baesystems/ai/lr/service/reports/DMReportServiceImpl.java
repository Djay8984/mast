package com.baesystems.ai.lr.service.reports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.jcabi.aspects.Loggable;

@Service(value = "DMReportService")
@Loggable(Loggable.DEBUG)
public class DMReportServiceImpl
{
    @Autowired
    private ReportDao reportDao;

    public ReportDto saveReport(final ReportDto reportDto) throws BadRequestException
    {
        return this.reportDao.saveReport(reportDto);
    }
}
