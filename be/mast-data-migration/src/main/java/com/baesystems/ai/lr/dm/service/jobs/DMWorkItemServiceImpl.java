package com.baesystems.ai.lr.dm.service.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

@Service(value = "DMWorkItemService")
public class DMWorkItemServiceImpl implements DMWorkItemService
{
    @Autowired
    private WorkItemDao workItemDao;

    @Autowired
    private WIPWorkItemDao wipWorkItemDao;

    @Override
    public WorkItemLightDto createWorkItem(final WorkItemLightDto workItem)
    {
        return this.workItemDao.createOrUpdateTask(workItem);
    }

    @Override
    public WorkItemLightDto createWipWorkItem(final WorkItemLightDto workItem)
    {
        return this.wipWorkItemDao.createOrUpdateTask(workItem);
    }
}
