package com.baesystems.ai.lr.dm.service.assets;

import com.baesystems.ai.lr.dto.assets.ItemDto;

public interface DMItemService
{
    ItemDto createItem(Long assetId, ItemDto item);
}
