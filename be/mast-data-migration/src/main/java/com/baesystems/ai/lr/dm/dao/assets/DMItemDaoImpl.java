package com.baesystems.ai.lr.dm.dao.assets;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributePersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPersistDO;
import com.baesystems.ai.lr.domain.mast.repositories.VersionedItemPersistRepository;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Repository(value = "DMItemDao")
public class DMItemDaoImpl implements DMItemDao
{
    @Autowired
    private VersionedItemPersistRepository itemPersistRepository;

    private final MapperFacade mapper;

    public DMItemDaoImpl()
    {
        super();
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    /**
     * Single create for DM, assumed parent already exists if specified and is of the same draft version.
     * Assumed no "related" relationships are specified
     *
     * @param assetId
     * @param itemDto
     * @return
     */
    @Override
    @Transactional
    public ItemDto createItem(final Long assetId, final ItemDto itemDto)
    {
        VersionedItemPersistDO itemDo = this.mapper.map(itemDto, VersionedItemPersistDO.class);
        itemDo.setAssetId(assetId);

        if (itemDo.getAttributes() != null)
        {
            for (final AttributePersistDO attribute : itemDo.getAttributes())
            {
                attribute.setItemId(itemDo.getId());
                attribute.setAssetId(itemDo.getAssetId());
                attribute.setAssetVersionId(itemDo.getAssetVersionId());
            }
        }

        if (itemDto.getParentItem() != null)
        {
            final ItemRelationshipPersistDO relationship = new ItemRelationshipPersistDO();
            relationship.setFromItemId(itemDto.getParentItem().getId());
            relationship.setFromAssetId(assetId);
            relationship.setFromVersionId(itemDo.getAssetVersionId());
            relationship.setToItemId(itemDo.getId());
            relationship.setToVersionId(itemDo.getAssetVersionId());
            relationship.setAssetId(assetId);
            relationship.setType(new LinkDO(RelationshipType.IS_PART_OF.getValue()));

            itemDo.setRelated(new ArrayList<>());
            itemDo.getRelated().add(relationship);
        }

        itemDo = this.itemPersistRepository.save(itemDo);
        return this.mapper.map(itemDo, ItemDto.class);
    }
}
