package com.baesystems.ai.lr.dm.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.service.ActionableItemValidationServiceImpl;

@Service(value = "DMActionableItemValidationService")
public class DMActionableItemValidationService extends ActionableItemValidationServiceImpl
{
    @Autowired
    private ValidationService validationService;

    public void verifyBody(final ActionableItemDto actionableItemDto) throws BadRequestException
    {
        // Who is the greatest hack of all?
        try
        {
            this.validationService.verifyBody(actionableItemDto);
        }
        catch (final BadRequestException badRequestException)
        {
            String errorMessage = badRequestException.getMessage();
            errorMessage = errorMessage.replace("The due date cannot be before the imposed date", "").trim();

            if (!errorMessage.equals(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, " ")))
            {
                throw badRequestException;
            }
        }
    }
}
