package com.baesystems.ai.lr.dm.service.assets;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dm.dao.assets.DMItemDao;
import com.baesystems.ai.lr.dto.assets.ItemDto;

@Service(value = "DMItemService")
public class DMItemServiceImpl implements DMItemService
{
    @Autowired
    private DMItemDao dmItemDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    @Autowired
    private AssetDao assetDao;

    @Override
    public ItemDto createItem(final Long assetId, final ItemDto itemDto)
    {
        final Long draftVersionId = this.assetDao.getDraftVersion(assetId);
        final ItemMetaDto itemMeta = this.itemPersistDao.createItemMeta(assetId);

        itemDto.setId(itemMeta.getId());
        itemDto.setAssetVersionId(draftVersionId);

        if (itemDto.getParentItem() != null)
        {
            itemDto.getParentItem().setVersion(draftVersionId);
        }
        return this.dmItemDao.createItem(assetId, itemDto);
    }
}
