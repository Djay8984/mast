package com.baesystems.ai.lr.security.saml;

import org.opensaml.Configuration;
import org.opensaml.xml.security.BasicSecurityConfiguration;
import org.opensaml.xml.signature.SignatureConstants;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.security.saml.SAMLBootstrap;

public class CustomSamlBootstrap extends SAMLBootstrap
{

    private String signingAlgorithm;

    @Override
    public void postProcessBeanFactory(final ConfigurableListableBeanFactory beanFactory) throws BeansException
    {
        super.postProcessBeanFactory(beanFactory);
        final BasicSecurityConfiguration config = (BasicSecurityConfiguration) Configuration.getGlobalSecurityConfiguration();

        if (SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1.equals(signingAlgorithm))
        {
            config.setSignatureReferenceDigestMethod(SignatureConstants.ALGO_ID_DIGEST_SHA1);
            config.registerSignatureAlgorithmURI("RSA", SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1);
        }
        else if (SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256.equals(signingAlgorithm))
        {
            config.setSignatureReferenceDigestMethod(SignatureConstants.ALGO_ID_DIGEST_SHA256);
            config.registerSignatureAlgorithmURI("RSA", SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);
        }
    }

    public String getSigningAlgorithm()
    {
        return signingAlgorithm;
    }

    public void setSigningAlgorithm(final String signingAlgorithm)
    {
        this.signingAlgorithm = signingAlgorithm;
    }

}
