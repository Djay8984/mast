package com.baesystems.ai.lr.security.interceptors;

import org.apache.camel.Exchange;

/**
 * Generic Security intercepter called before / after route or on error
 */
public interface RouteSecurityInterceptor
{
    boolean before(Exchange exchange);

    void after(Exchange exchange);

    void error(Exchange exchange, boolean intercepted);

    boolean accepts();
}
