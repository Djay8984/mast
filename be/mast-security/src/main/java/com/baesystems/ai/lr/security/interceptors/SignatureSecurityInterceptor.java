package com.baesystems.ai.lr.security.interceptors;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.baesystems.ai.lr.constants.MastHeaders;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.http.common.HttpMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.security.SecurityStatusManager;
import com.baesystems.ai.lr.security.sign.SecureSignatureVerifier;

@Component
public class SignatureSecurityInterceptor implements RequestSecurityInterceptor
{
    private static final Logger LOG = LoggerFactory.getLogger(SignatureSecurityInterceptor.class);

    private static final String URI_PREFIX = "/api";

    private final Set<String> publicUrls;

    @Autowired
    private SecureSignatureVerifier signer;

    @Autowired
    private SecurityStatusManager manager;

    public SignatureSecurityInterceptor()
    {
        final Set<String> publicUrlList = new HashSet<String>();
        publicUrlList.add("GET:/api/v2/ping");

        this.publicUrls = Collections.unmodifiableSet(publicUrlList);
    }

    /**
     * Only Accept HTTP Message Type
     */
    @Override
    public final boolean accepts(final Class<? extends Message> clazz)
    {
        final boolean accepts = clazz != null && HttpMessage.class.isAssignableFrom(clazz);
        return accepts && (manager == null || manager.isSecurityEnabled());
    }

    /**
     * Verify signature, if not matched return false to prevent user continuing
     */
    @Override
    public final boolean before(final Exchange exchange)
    {
        final HttpMessage message = exchange.getIn().getBody(HttpMessage.class);

        // Message Body is a StreamCache convert to a String so we can read body
        // i.e. "{name:'hello'}"
        final String bodyValue = message.getBody(String.class);

        // Skip Public URL's
        final String uriKey = this.getUriKey(message, bodyValue);

        boolean result;
        if (publicUrls.contains(uriKey))
        {
            result = true;
        }
        else
        {
            final String checkSum = message.getHeader(MastHeaders.IDS_CLIENT_SIGN, String.class);
            final String clientId = message.getHeader(MastHeaders.IDS_CLIENT_IDENTITY, String.class);

            // verify supplied header vs payload
            result = signer.verify(clientId, uriKey, checkSum);
        }
        return result;
    }

    /**
     * Not required
     */
    @Override
    public final void after(final Exchange exchange)
    {
        // not required
    }

    /**
     * Not required
     */
    @Override
    public final void error(final Exchange exchange, final boolean intercepted)
    {
        // not required
    }

    private String getUriKey(final HttpMessage message, final String bodyValue)
    {
        final String method = message.getHeader(Exchange.HTTP_METHOD, String.class);
        final String query = message.getHeader(Exchange.HTTP_QUERY, String.class);

        String uriAndContext = "";

        //This is already encoded, so decode, ready for re-encoding after concatenating
        try
        {
            uriAndContext = URLDecoder.decode(message.getHeader(Exchange.HTTP_URI, String.class), "utf-8");
        }
        catch (final UnsupportedEncodingException neverThrownException)
        {
            LOG.error("Invalid encoding type specified", neverThrownException); //This will never happen
        }

        final String uri = uriAndContext.substring(uriAndContext.indexOf(URI_PREFIX));

        final StringBuilder key = new StringBuilder(255);
        key.append(method).append(':').append(uri);

        if (query != null && query.length() > 0)
        {
            key.append('?').append(query);
        }

        // PPWNS
        if (bodyValue != null && bodyValue.length() > 0)
        {
            key.append('[').append(bodyValue).append(']');
        }
        return key.toString();
    }
}
