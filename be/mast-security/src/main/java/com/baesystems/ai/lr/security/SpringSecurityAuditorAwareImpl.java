package com.baesystems.ai.lr.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.baesystems.ai.lr.utils.ServiceUtils;

public class SpringSecurityAuditorAwareImpl implements AuditorAware<String>
{
    @Autowired
    private ServiceUtils serviceUtils;

    @Override
    public String getCurrentAuditor()
    {
        return serviceUtils.getSecurityContextPrincipal();
    }
}
