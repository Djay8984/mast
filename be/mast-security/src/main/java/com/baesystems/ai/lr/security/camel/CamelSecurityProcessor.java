package com.baesystems.ai.lr.security.camel;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.spi.Synchronization;
import org.apache.camel.util.MessageHelper;

import com.baesystems.ai.lr.exception.UnauthorizedException;
import com.baesystems.ai.lr.security.interceptors.RequestSecurityInterceptor;
import com.baesystems.ai.lr.security.interceptors.RouteSecurityInterceptor;

/**
 * note: Processor is created per Route, not a COMPONENT
 */
public class CamelSecurityProcessor implements Processor, Synchronization
{
    private static final String INTERCEPTED = "CamelSecurityProcessor#intercepted";

    private final Processor target;
    private final List<RequestSecurityInterceptor> interceptors;

    private final CamelSecurityRouteProcessor routeProcessor;

    public CamelSecurityProcessor(final Processor target, final List<RequestSecurityInterceptor> interceptors,
            final List<RouteSecurityInterceptor> routeInterceptors)
    {
        this.target = target;
        this.interceptors = interceptors;
        this.routeProcessor = new CamelSecurityRouteProcessor(routeInterceptors);
    }

    /**
     * Intercepter workflow: before, after, error or before error (if intercepted)
     */
    @Override
    public void process(final Exchange exchange)
    {
        // wrap route with Process Monitoring for statistics
        routeProcessor.process(exchange);

        final Boolean intercepted = exchange.getProperty(INTERCEPTED, Boolean.class);
        // if "intercepted" before, allow to continue
        // so we don't intercept the error route, validation routes etc
        if (intercepted != null && intercepted.equals(Boolean.TRUE))
        {
            coreProcess(exchange);
        }
        else
        {
            // intercept route
            exchange.setProperty(INTERCEPTED, Boolean.TRUE);
            processExchange(exchange);
        }
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private void coreProcess(final Exchange exchange)
    {
        try
        {
            exchange.setOut(exchange.getIn());
            target.process(exchange);
        }
        catch (final Exception exception)
        {
            exchange.setException(exception);
        }
    }

    @Override
    public void onComplete(final Exchange exchange)
    {
        interceptAfter(exchange);
    }

    @Override
    public void onFailure(final Exchange exchange)
    {
        interceptError(exchange, true);
    }

    /**
     * Intercept Route with Security Processor
     *
     * @param exchange
     */
    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private void processExchange(final Exchange exchange)
    {
        // run before intercepter, to see if we are allowed to continue
        // this will run security rules, audit rules etc and they can stop the processing here
        final boolean invokeTarget = interceptBefore(exchange);

        // Security Passed, allow to continue
        if (invokeTarget)
        {
            try
            {
                // invoke existing route
                exchange.setOut(exchange.getIn());

                // listen to exchange, to see when we complete
                // will will fire back our onError etc
                exchange.addOnCompletion(this);

                target.process(exchange);
            }
            catch (final Exception exception)
            {
                // invoked if the route fires an error
                interceptError(exchange, !invokeTarget);

                // set exception on the route, for dead letter handler
                exchange.setException(exception);
            }
        }
        else
        {
            interceptError(exchange, !invokeTarget);

            // security failed, push generic error to dead letter handler
            exchange.getOut().setBody(null);
            exchange.setException(new UnauthorizedException());
        }
    }

    private boolean interceptBefore(final Exchange exchange)
    {
        boolean invokeTarget = true;
        final Message message = exchange.getIn().getBody(Message.class);
        if (message != null)
        {

            // calculate accumulative result for invokeTarget
            // to see if we are allowed to proceed
            for (final RequestSecurityInterceptor interceptor : interceptors)
            {
                if (interceptor.accepts(message.getClass()))
                {
                    try
                    {
                        invokeTarget = invokeTarget & interceptor.before(exchange);
                    }
                    finally
                    {
                        MessageHelper.resetStreamCache(message);
                    }
                }
            }

        }
        return invokeTarget;
    }

    private void interceptAfter(final Exchange exchange)
    {
        final Message message = exchange.getOut().getBody(Message.class);
        if (message != null)
        {
            for (final RequestSecurityInterceptor interceptor : interceptors)
            {
                if (interceptor.accepts(message.getClass()))
                {
                    try
                    {
                        interceptor.after(exchange);
                    }
                    finally
                    {
                        MessageHelper.resetStreamCache(message);
                    }
                }
            }
        }
    }

    private void interceptError(final Exchange exchange, final boolean intercepted)
    {
        final Message message = exchange.getOut().getBody(Message.class);
        if (message != null)
        {
            for (final RequestSecurityInterceptor interceptor : interceptors)
            {
                if (interceptor.accepts(message.getClass()))
                {
                    try
                    {
                        interceptor.error(exchange, intercepted);
                    }
                    finally
                    {
                        MessageHelper.resetStreamCache(message);
                    }
                }
            }
        }
    }
}
