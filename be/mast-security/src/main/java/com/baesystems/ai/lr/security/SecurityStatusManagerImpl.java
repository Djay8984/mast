package com.baesystems.ai.lr.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value = "SecurityStatusManager")
public class SecurityStatusManagerImpl implements SecurityStatusManager
{
    @Value("#{systemProperties['mast.security.enabled'] ?: false}")
    private boolean securityEnabled;

    @Override
    public boolean isSecurityEnabled()
    {
        return this.securityEnabled;
    }
}
