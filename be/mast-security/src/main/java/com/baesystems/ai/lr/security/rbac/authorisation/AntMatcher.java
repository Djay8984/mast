package com.baesystems.ai.lr.security.rbac.authorisation;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.stereotype.Component;

@Component
public class AntMatcher
{
    private static final String ROOT_PATH = "/";
    private static final char TRAILING_PATH_CHAR = '/';
    private static final char METHOD_SPLIT_CHAR = ':';
    private static final String DIR_WILDCARD = "**";

    public String toMatcherFormat(final String path)
    {
        final char[] values = path.toCharArray();
        final int valuesLength = values.length > 0 && values[values.length - 1] == TRAILING_PATH_CHAR ? values.length - 1 : values.length;
        final int countChar = countChar(values, valuesLength);

        return fillChar(values, valuesLength, countChar);
    }

    public boolean wildcardMatch(final String matcherString, final String path)
    {
        boolean result = false;
        if (matcherString.equals(path))
        {
            result = true;
        }
        else
        {
            final int endIndex = matcherString.lastIndexOf(DIR_WILDCARD);
            if (endIndex >= 0)
            {
                result = path.regionMatches(0, matcherString, 0, endIndex);
            }
        }
        return result;
    }

    public String extractPath(final HttpServletRequest request)
    {
        final String servletPath = request.getServletPath() != null ? request.getServletPath() : ROOT_PATH;
        final String pathInfo = request.getPathInfo();
        final String method = request.getMethod();

        final boolean hasPathInfo = pathInfo != null;
        int pathLength = servletPath.length();
        pathLength += hasPathInfo ? pathInfo.length() : 0;
        pathLength += method.length() + 1;

        StringBuffer pathBuffer = new StringBuffer(pathLength);
        pathBuffer = pathBuffer.append(method);
        pathBuffer = pathBuffer.append(METHOD_SPLIT_CHAR);
        pathBuffer = pathBuffer.append(servletPath);

        if (hasPathInfo)
        {
            pathBuffer = pathBuffer.append(pathInfo);
        }
        return toMatcherFormat(pathBuffer.toString());
    }

    public String extractPath(final Exchange exchange)
    {
        final Message inMessage = exchange.getIn();

        final String method = inMessage.getHeader(Exchange.HTTP_METHOD, String.class);
        final String uri = inMessage.getHeader(Exchange.HTTP_URI, String.class);

        StringBuffer uriBuffer = new StringBuffer(method.length() + uri.length() + 1);
        uriBuffer = uriBuffer.append(method);
        uriBuffer = uriBuffer.append(METHOD_SPLIT_CHAR);
        uriBuffer = uriBuffer.append(uri);

        return toMatcherFormat(uriBuffer.toString());
    }

    private String fillChar(final char[] values, final int valuesLength, final int removedCharCount)
    {
        final char[] result = new char[valuesLength - removedCharCount];

        int countChar = 0;
        boolean dontCount = true;
        for (int index = 0; index < valuesLength; index++)
        {
            switch (values[index])
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                {
                    if (dontCount)
                    {
                        dontCount = false;
                        result[index - countChar] = '*';
                    }
                    else
                    {
                        countChar++;
                    }
                    break;
                }
                default:
                {
                    result[index - countChar] = values[index];
                    dontCount = true;
                    break;
                }
            }
        }
        return new String(result);
    }

    private int countChar(final char[] values, final int valuesLength)
    {
        int countChar = 0;
        boolean dontCount = true;
        for (int index = 0; index < valuesLength; index++)
        {
            switch (values[index])
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                {
                    if (dontCount)
                    {
                        dontCount = false;
                    }
                    else
                    {
                        countChar++;
                    }

                    break;
                }
                default:
                {
                    dontCount = true;
                    break;
                }
            }
        }
        return countChar;
    }
}
