package com.baesystems.ai.lr.security.sign;

public interface SecureSignatureVerifier
{
    boolean verify(String clientVersion, String checkBody, String checkSum);
}
