package com.baesystems.ai.lr.security.rbac.authentication;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.saml.SAMLCredential;

import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public class SamlHeaderEntryPoint extends AbstractEntryPoint
{
    private static final String ATTRIBUTE_ROLE = System.getProperty("mast-saml-attributeName",
            "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");

    public static final String ROLE_PREFIX = System.getProperty("mast-saml-roleprefix", "IDS-TEST-");

    private static final String ATTRIBUTE_NAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
    private static final String GROUP_CN = "CN";

    private static final Logger LOG = LoggerFactory.getLogger(SamlHeaderEntryPoint.class);

    @Override
    protected UserPrincipal getUser(final Authentication authentication, final HttpServletRequest request)
    {
        final UserPrincipal userPrincipal = new UserPrincipal();
        Object credentials = null;
        if (authentication != null)
        {
            userPrincipal.setUserName((String) authentication.getPrincipal());
            credentials = authentication.getCredentials();
        }

        if (credentials instanceof SAMLCredential)
        {
            final SAMLCredential saml = (SAMLCredential) credentials;

            // may not be correct for attributes for LR
            userPrincipal.setDisplayName(saml.getAttributeAsString(ATTRIBUTE_NAME));
        }
        return userPrincipal;
    }

    @Override
    public String[] getGroups(final Authentication authentication, final HttpServletRequest request)
    {
        Object credentials = null;
        if (authentication != null)
        {
            credentials = authentication.getCredentials();
        }

        final Set<String> groups = new HashSet<String>();

        if (credentials instanceof SAMLCredential)
        {
            final SAMLCredential saml = (SAMLCredential) credentials;

            // may not be correct for attributes for LR
            final String[] attributeValues = saml.getAttributeAsStringArray(ATTRIBUTE_ROLE);

            for (final String attributeValue : attributeValues == null ? Collections.<String>emptyList().toArray(new String[0]) : attributeValues)
            {
                try
                {
                    final LdapName ldapName = new LdapName(attributeValue);

                    for (final Rdn rdn : ldapName.getRdns())
                    {
                        if (rdn.getType().equalsIgnoreCase(GROUP_CN))
                        {
                            groups.add(((String) rdn.getValue()).replaceAll(ROLE_PREFIX, ""));
                        }
                    }
                }
                catch (final InvalidNameException invalidNameException)
                {
                    LOG.debug(String.format(ExceptionMessagesUtils.CREDENTIAL_ERROR, invalidNameException.getMessage()));
                    continue;
                }
            }
        }
        return groups.toArray(new String[groups.size()]);
    }
}
