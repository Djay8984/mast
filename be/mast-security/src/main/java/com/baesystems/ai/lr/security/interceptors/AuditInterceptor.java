package com.baesystems.ai.lr.security.interceptors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Component
public class AuditInterceptor implements RequestSecurityInterceptor
{
    private static final Logger LOG = LoggerFactory.getLogger(AuditInterceptor.class);

    private static final String URL = "URL";
    private static final String GET = "GET";
    private static final String AUDIT_LOG = "AuditLog";
    private static final String USER_HEADER = "user";

    @Override
    public boolean before(final Exchange exchange)
    {
        MDC.put(URL, exchange.getIn().getHeader(Exchange.HTTP_URI, String.class));
        if (!GET.equals(exchange.getIn().getHeader(Exchange.HTTP_METHOD, String.class)))
        {
            final String message = exchange.getIn().getBody(String.class);
            final String log = "{"
                    + this.formatLogEntry("ExchangeId", exchange.getExchangeId()) + ","
                    + this.formatLogEntry("TimeStamp", exchange.getProperty(Exchange.CREATED_TIMESTAMP, String.class)) + ","
                    + this.formatLogEntry("HttpPath", exchange.getIn().getHeader(Exchange.HTTP_PATH, String.class)) + ","
                    + this.formatLogEntry("UserName", exchange.getIn().getHeader(USER_HEADER, String.class)) + ","
                    + this.formatLogEntry("HttpMethod", exchange.getIn().getHeader(Exchange.HTTP_METHOD, String.class)) + ","
                    + this.formatLogEntry("HttpQuery", exchange.getIn().getHeader(Exchange.HTTP_QUERY, String.class)) + ","
                    + this.formatLogEntry("InputBody", message != null ? message : "") + ",";
            exchange.setProperty(AUDIT_LOG, log);
        }
        return true;
    }

    @Override
    public void after(final Exchange exchange)
    {
        MDC.remove(URL);
        this.exit(exchange);
    }

    @Override
    public void error(final Exchange exchange, final boolean intercepted)
    {
        this.exit(exchange);
    }

    private void exit(final Exchange exchange)
    {
        if (!GET.equals(exchange.getIn().getHeader(Exchange.HTTP_METHOD, String.class)))
        {
            final String responseCode = exchange.getOut().getHeader(Exchange.HTTP_RESPONSE_CODE, String.class);
            final String log = exchange.getProperty(AUDIT_LOG, String.class)
                    + this.formatLogEntry("ResponseBody", exchange.getOut().getBody(String.class)) + ","
                    + this.formatLogEntry("ResponseCode", responseCode == null ? "2**" : responseCode) + "}";
            LOG.info(log);
        }
    }

    private String formatLogEntry(final String newEntryName, final String newEntry)
    {
        String logEntry;

        if (newEntry == null)
        {
            logEntry = "\"" + newEntryName + "\":" + "null";
        }
        else
        {
            logEntry = "\"" + newEntryName + "\":" + "\"" + newEntry.replace("\n", "") + "\"";
        }
        return logEntry;
    }

    @Override
    public boolean accepts(final Class<? extends Message> clazz)
    {
        return true;
    }

}
