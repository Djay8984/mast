package com.baesystems.ai.lr.security.rbac;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.domain.security.repositories.RoleRepository;

@Component("RoleService")
public class RoleServiceImpl implements RoleService
{
    private static final DirectMatcherRule DIRECT_RULE = new DirectMatcherRule();

    private static final IndirectMatcherRule INDIRECT_RULE = new IndirectMatcherRule();

    private static final String WILDCARD_MATCH = "**";
    private static final int NAME = 0;
    private static final int URI = 1;
    private static final int METHOD = 2;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public CachedRoleDto getRoles()
    {
        final List<Object[]> roles = roleRepository.getGroupRoles();

        final CachedRoleDto roleDto = new CachedRoleDto();
        roleDto.setDirectMatches(getMatches(roles, DIRECT_RULE));
        roleDto.setIndirectMatches(getMatches(roles, INDIRECT_RULE));

        return roleDto;
    }

    private Map<String, Set<String>> getMatches(final List<Object[]> roles, final MatchesRule matchRule)
    {
        final HashMap<String, Set<String>> roleMap = new HashMap<String, Set<String>>();

        StringBuilder uriBuilder = new StringBuilder();
        for (final Object[] values : roles)
        {
            // role name to upper-case for now, so people using headers don't have
            // to worry about it
            final String key = values[NAME].toString().toUpperCase(Locale.ENGLISH);
            Set<String> functions = roleMap.get(key);

            if (functions == null)
            {
                functions = new HashSet<String>();
                roleMap.put(key, functions);
            }

            uriBuilder = uriBuilder.append(values[METHOD]);
            uriBuilder = uriBuilder.append(':');
            uriBuilder = uriBuilder.append(values[URI]);

            final String matchUrl = uriBuilder.toString();

            if (matchRule.matches(matchUrl))
            {
                functions.add(matchUrl);
            }
            // reset sb
            uriBuilder.setLength(0);
        }

        // mark map / contents as read only, to avoid concurrency issues
        for (final Map.Entry<String, Set<String>> entry : roleMap.entrySet())
        {
            entry.setValue(Collections.unmodifiableSet(entry.getValue()));
        }
        return Collections.unmodifiableMap(roleMap);
    }

    private static class DirectMatcherRule implements MatchesRule
    {
        @Override
        public boolean matches(final String matchValue)
        {
            return matchValue.indexOf(WILDCARD_MATCH) < 0;
        }
    }

    private static class IndirectMatcherRule implements MatchesRule
    {
        @Override
        public boolean matches(final String matchValue)
        {
            return matchValue.indexOf(WILDCARD_MATCH) >= 0;
        }
    }

    private interface MatchesRule
    {
        boolean matches(String matchValue);
    }
}
