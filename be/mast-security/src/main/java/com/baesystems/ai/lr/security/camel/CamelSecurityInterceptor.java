package com.baesystems.ai.lr.security.camel;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Processor;
import org.apache.camel.model.ProcessorDefinition;
import org.apache.camel.spi.InterceptStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.security.interceptors.RequestSecurityInterceptor;
import com.baesystems.ai.lr.security.interceptors.RouteSecurityInterceptor;

/**
 * <code>CamelSecurityInterceptor</code> Intercepts all HTTP based routes to determine if they have the correct message
 * payload
 */
@Component
public class CamelSecurityInterceptor implements InterceptStrategy
{
    @Autowired
    private List<RequestSecurityInterceptor> interceptors;

    @Autowired
    private List<RouteSecurityInterceptor> routeInterceptors;

    @Override
    public final Processor wrapProcessorInInterceptors(final CamelContext context, final ProcessorDefinition<?> definition,
            final Processor target, final Processor nextTarget)
    {
        return new CamelSecurityProcessor(target, interceptors, routeInterceptors);
    }
}
