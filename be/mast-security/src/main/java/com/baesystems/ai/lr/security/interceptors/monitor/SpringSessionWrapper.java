package com.baesystems.ai.lr.security.interceptors.monitor;

import java.util.Collections;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.springframework.session.ExpiringSession;

/**
 * Translation adapter class from Spring Session (Session) to Servlet Session this is used to enable us to pass this
 * information to monitoring
 */
public class SpringSessionWrapper<S extends ExpiringSession> implements HttpSession
{
    private S session;
    private final ServletContext servletContext;
    private boolean invalidated;
    private boolean old;

    public SpringSessionWrapper(final S session, final ServletContext servletContext)
    {
        this.session = session;
        this.servletContext = servletContext;
    }

    public void setSession(final S session)
    {
        this.session = session;
    }

    public S getSession()
    {
        return session;
    }

    @Override
    public long getCreationTime()
    {
        checkState();
        return session.getCreationTime();
    }

    @Override
    public String getId()
    {
        return session.getId();
    }

    @Override
    public long getLastAccessedTime()
    {
        checkState();
        return session.getLastAccessedTime();
    }

    @Override
    public ServletContext getServletContext()
    {
        return servletContext;
    }

    @Override
    public void setMaxInactiveInterval(final int interval)
    {
        session.setMaxInactiveIntervalInSeconds(interval);
    }

    @Override
    public int getMaxInactiveInterval()
    {
        return session.getMaxInactiveIntervalInSeconds();
    }

    @SuppressWarnings("deprecation")
    @Override
    public HttpSessionContext getSessionContext()
    {
        return NOOP_SESSION_CONTEXT;
    }

    @Override
    public Object getAttribute(final String name)
    {
        checkState();
        return session.getAttribute(name);
    }

    @Override
    public Object getValue(final String name)
    {
        return getAttribute(name);
    }

    @Override
    public Enumeration<String> getAttributeNames()
    {
        checkState();
        return Collections.enumeration(session.getAttributeNames());
    }

    @Override
    public String[] getValueNames()
    {
        checkState();
        final Set<String> attrs = session.getAttributeNames();
        return attrs.toArray(new String[attrs.size()]);
    }

    @Override
    public void setAttribute(final String name, final Object value)
    {
        checkState();
        session.setAttribute(name, value);
    }

    @Override
    public void putValue(final String name, final Object value)
    {
        setAttribute(name, value);
    }

    @Override
    public void removeAttribute(final String name)
    {
        checkState();
        session.removeAttribute(name);
    }

    @Override
    public void removeValue(final String name)
    {
        removeAttribute(name);
    }

    @Override
    public void invalidate()
    {
        checkState();
        this.invalidated = true;
    }

    public void setNew(final boolean isNew)
    {
        this.old ^= isNew;
    }

    @Override
    public boolean isNew()
    {
        checkState();
        return !old;
    }

    private void checkState()
    {
        if (invalidated)
        {
            throw new IllegalStateException("The HttpSession has already be invalidated.");
        }
    }

    @SuppressWarnings("deprecation")
    private static final HttpSessionContext NOOP_SESSION_CONTEXT = new HttpSessionContext()
    {
        @Override
        public HttpSession getSession(final String sessionId)
        {
            return null;
        }

        @Override
        public Enumeration<String> getIds()
        {
            return EMPTY_ENUMERATION;
        }
    };

    private static final Enumeration<String> EMPTY_ENUMERATION = new Enumeration<String>()
    {
        @Override
        public boolean hasMoreElements()
        {
            return false;
        }

        @Override
        public String nextElement()
        {
            throw new NoSuchElementException("a");
        }
    };
}
