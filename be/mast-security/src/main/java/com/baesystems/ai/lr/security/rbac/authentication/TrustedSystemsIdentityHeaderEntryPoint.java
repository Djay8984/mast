package com.baesystems.ai.lr.security.rbac.authentication;

import com.baesystems.ai.lr.constants.MastHeaders;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * Stub filter for mapping between identified trusted systems against RBAC roles for API access
 */
public class TrustedSystemsIdentityHeaderEntryPoint extends AbstractEntryPoint
{

    private static final String CLASS_DIRECT_FUTURE = "cdf";
    private static final String[] ADMIN_ROLE = {"admin"};

    @Override
    protected UserPrincipal getUser(final Authentication authentication, final HttpServletRequest request)
    {
        UserPrincipal principal = null;
        if (authentication != null)
        {
            principal = (UserPrincipal) authentication.getPrincipal();
        }

        if (principal == null)
        {
            principal = new UserPrincipal();
            principal.setUserName(request.getHeader(MastHeaders.IDS_CLIENT_IDENTITY));
        }

        return principal;
    }

    @Override
    protected String[] getGroups(final Authentication authentication, final HttpServletRequest request)
    {
        String[] groups = null;
        // Stub logic - if User is identified as class direct, and hitting the trusted-systems url that owns this filter
        // given them admin access to all APIs
        if (CLASS_DIRECT_FUTURE.equals(request.getHeader(MastHeaders.IDS_CLIENT_IDENTITY)))
        {
            groups = ADMIN_ROLE;
        }
        return groups;
    }
}
