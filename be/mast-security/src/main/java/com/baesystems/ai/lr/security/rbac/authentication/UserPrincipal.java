package com.baesystems.ai.lr.security.rbac.authentication;

import java.io.Serializable;

public class UserPrincipal implements Serializable
{
    private static final long serialVersionUID = -1517813465958472578L;
    private String userName;
    private String displayName;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(final String userName)
    {
        this.userName = userName;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(final String displayName)
    {
        this.displayName = displayName;
    }

}
