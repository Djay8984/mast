package com.baesystems.ai.lr.security.rbac.authentication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.intercept.RunAsUserToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.filter.GenericFilterBean;

public abstract class AbstractEntryPoint extends GenericFilterBean implements AuthenticationEntryPoint
{
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException
    {
        if (request instanceof HttpServletRequest)
        {
            this.commence((HttpServletRequest) request, (HttpServletResponse) response, null);
        }

        if (chain != null)
        {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException)
            throws IOException, ServletException
    {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication auth = securityContext.getAuthentication();
        if (!(auth instanceof RunAsUserToken))
        {
            final String[] groups = getGroups(auth, request);
            if (authException == null && groups != null && groups.length > 0)
            {
                updateSecurityContext(request, securityContext, auth, groups);
            }
        }
    }

    private void updateSecurityContext(final HttpServletRequest request, final SecurityContext securityContext, final Authentication auth,
            final String[] groups)
    {
        final List<GrantedAuthority> tokens = createTokens(groups);

        final UserPrincipal user = getUser(auth, request);
        if (user != null)
        {
            final Authentication authResult = new RunAsUserToken(user.getUserName(), user, null, tokens, null);
            securityContext.setAuthentication(authResult);
        }
    }

    protected abstract UserPrincipal getUser(Authentication authentication, HttpServletRequest request);

    protected abstract String[] getGroups(Authentication authentication, HttpServletRequest request);


    private List<GrantedAuthority> createTokens(final String[] groups)
    {
        final List<GrantedAuthority> groupResult = new ArrayList<GrantedAuthority>();
        if (groups != null)
        {
            for (final String group : groups)
            {
                // convert to upper-case so headers can be specified as either lower-case / upper case
                groupResult.add(new SimpleGrantedAuthority(group.toUpperCase(Locale.ENGLISH)));
            }
        }
        return groupResult;
    }

}
