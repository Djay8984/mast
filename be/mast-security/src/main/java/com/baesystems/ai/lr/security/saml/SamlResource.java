package com.baesystems.ai.lr.security.saml;

import java.io.IOException;
import java.io.InputStream;

import org.joda.time.DateTime;
import org.opensaml.util.resource.AbstractFilteredResource;
import org.opensaml.util.resource.ResourceException;
import org.springframework.core.io.Resource;

public class SamlResource extends AbstractFilteredResource
{
    private final Resource resource;

    /** Last modification time, set to when resources is created. */
    private final DateTime lastModTime;

    public SamlResource(final Resource resource) throws ResourceException
    {
        super();

        if (resource == null)
        {
            throw new ResourceException("Classpath resource does not exist ");
        }

        this.resource = resource;
        lastModTime = new DateTime();
    }

    @Override
    public boolean exists() throws ResourceException
    {
        return resource != null;
    }

    @Override
    public InputStream getInputStream() throws ResourceException
    {
        try
        {
            final InputStream ins = resource.getInputStream();
            return applyFilter(ins);
        }
        catch (final IOException exception)
        {
            throw new ResourceException("Unable to open resource: " + resource, exception);
        }
    }

    @Override
    public DateTime getLastModifiedTime() throws ResourceException
    {
        return lastModTime;
    }

    @Override
    public String getLocation()
    {
        return resource.toString();
    }

    @Override
    public String toString()
    {
        return getLocation();
    }

    @Override
    public int hashCode()
    {
        return getLocation().hashCode();
    }

    @Override
    public boolean equals(final Object object)
    {
        boolean result = false;
        if (object == this)
        {
            result = true;
        }
        else if (object instanceof SamlResource)
        {
            result = getLocation().equals(((SamlResource) object).getLocation());
        }
        return result;
    }
}
