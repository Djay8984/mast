package com.baesystems.ai.lr.security.rbac.authentication;

import javax.servlet.http.HttpServletRequest;

import com.baesystems.ai.lr.constants.MastHeaders;
import org.springframework.security.core.Authentication;

public class HeaderEntryPoint extends AbstractEntryPoint
{
    @Override
    public UserPrincipal getUser(final Authentication authentication, final HttpServletRequest request)
    {
        UserPrincipal principal = null;
        if (authentication != null)
        {
            principal = (UserPrincipal) authentication.getPrincipal();
        }

        if (principal == null)
        {
            principal = new UserPrincipal();
            principal.setUserName(request.getHeader(MastHeaders.USER));
        }

        return principal;
    }

    @Override
    public String[] getGroups(final Authentication authentication, final HttpServletRequest request)
    {
        final String groupList = request.getHeader(MastHeaders.GROUPS);
        String[] groups = null;
        if (groupList != null && groupList.length() > 0)
        {
            groups = groupList.split(",");
        }
        return groups;
    }
}
