package com.baesystems.ai.lr.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Access Denied Handler: will return unauthorised response if access is denied.
 */
public class IDSAccessDeniedHandler implements AccessDeniedHandler
{
    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response,
            final AccessDeniedException arg2) throws IOException, ServletException
    {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
