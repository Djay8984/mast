package com.baesystems.ai.lr.security.sign;

import java.security.PublicKey;

/**
 * Holds Security Algorithm for Verifying Signatures this is not intended to be a DTO and should not be serialised
 */
public class PublicKeyHolder
{
    private final PublicKey publicKey;
    private final String algorithm;

    public PublicKeyHolder(final PublicKey publicKey, final String algorithm)
    {
        this.publicKey = publicKey;
        this.algorithm = algorithm;
    }

    public final PublicKey getPublicKey()
    {
        return publicKey;
    }

    public final String getAlgorithm()
    {
        return algorithm;
    }
}
