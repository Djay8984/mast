package com.baesystems.ai.lr.security.rbac;

import java.util.Map;
import java.util.Set;

public class CachedRoleDto
{
    private Map<String, Set<String>> directMatches;

    private Map<String, Set<String>> indirectMatches;

    public Map<String, Set<String>> getDirectMatches()
    {
        return directMatches;
    }

    public void setDirectMatches(final Map<String, Set<String>> directMatches)
    {
        this.directMatches = directMatches;
    }

    public Map<String, Set<String>> getIndirectMatches()
    {
        return indirectMatches;
    }

    public void setIndirectMatches(final Map<String, Set<String>> indirectMatches)
    {
        this.indirectMatches = indirectMatches;
    }
}
