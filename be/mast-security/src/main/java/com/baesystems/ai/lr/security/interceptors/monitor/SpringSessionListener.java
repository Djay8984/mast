package com.baesystems.ai.lr.security.interceptors.monitor;

import javax.servlet.http.HttpSessionEvent;

import org.springframework.context.event.EventListener;
import org.springframework.session.ExpiringSession;
import org.springframework.session.events.SessionCreatedEvent;
import org.springframework.session.events.SessionDeletedEvent;
import org.springframework.session.events.SessionDestroyedEvent;
import org.springframework.session.events.SessionExpiredEvent;

import net.bull.javamelody.SessionListener;

/**
 * Listen to Spring Session events and notify Java Melody when Session Created / Session Destroyed
 */
public class SpringSessionListener
{
    /**
     * Java Melody Session Listener - boot class for the Melody Monitoring
     */
    private SessionListener sessionListener;

    public void setSessionListener(final SessionListener sessionListener)
    {
        this.sessionListener = sessionListener;
    }

    @EventListener
    public void handleSessionCreated(final SessionCreatedEvent event)
    {
        final SpringSessionWrapper<ExpiringSession> session = new SpringSessionWrapper<ExpiringSession>((ExpiringSession) event.getSession(), null);
        sessionListener.sessionCreated(new HttpSessionEvent(session));
    }

    @EventListener
    public void handleSessionDeleted(final SessionDeletedEvent event)
    {
        final SpringSessionWrapper<ExpiringSession> session = new SpringSessionWrapper<ExpiringSession>((ExpiringSession) event.getSession(), null);
        sessionListener.sessionDestroyed(new HttpSessionEvent(session));
    }

    @EventListener
    public void handleSessionDestroyed(final SessionDestroyedEvent event)
    {
        final SpringSessionWrapper<ExpiringSession> session = new SpringSessionWrapper<ExpiringSession>((ExpiringSession) event.getSession(), null);
        sessionListener.sessionDestroyed(new HttpSessionEvent(session));
    }

    @EventListener
    public void handleSessionExpired(final SessionExpiredEvent event)
    {
        final SpringSessionWrapper<ExpiringSession> session = new SpringSessionWrapper<ExpiringSession>((ExpiringSession) event.getSession(), null);
        sessionListener.sessionDestroyed(new HttpSessionEvent(session));
    }
}
