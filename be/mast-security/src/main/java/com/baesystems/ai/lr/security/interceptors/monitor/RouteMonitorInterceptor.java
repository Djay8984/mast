package com.baesystems.ai.lr.security.interceptors.monitor;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.security.interceptors.RouteSecurityInterceptor;
import com.baesystems.ai.lr.security.rbac.authorisation.AntMatcher;

import net.bull.javamelody.CounterAccessHelper;

@Component
public class RouteMonitorInterceptor implements RouteSecurityInterceptor
{
    /**
     * To disable this interceptor don't autowire the CounterObject
     */
    @Autowired(required = false)
    private CounterAccessHelper counter;

    @Autowired
    private AntMatcher antMatcher;

    /**
     * Fetches the Route ID from the UoW and notifies the monitoring context this will log this call, once the "after"
     * has been hit
     */
    @Override
    public boolean before(final Exchange exchange)
    {
        final String routeId = exchange.getUnitOfWork().getRouteContext().getRoute().getId();
        final boolean monitor = routeId != null;
        if (monitor)
        {
            // temporary solution until we get RouteId's for all camel routes
            final String requestName = antMatcher.extractPath(exchange);
            StringBuilder routeNameBuilder = new StringBuilder(requestName.length() + 1 + routeId.length());
            routeNameBuilder = routeNameBuilder.append(routeId);
            routeNameBuilder = routeNameBuilder.append(' ');
            routeNameBuilder = routeNameBuilder.append(requestName);

            counter.startRoute(routeNameBuilder.toString());
        }
        return monitor;
    }

    @Override
    public void after(final Exchange exchange)
    {
        counter.endRoute(false);
    }

    @Override
    public void error(final Exchange exchange, final boolean intercepted)
    {
        counter.endRoute(true);
    }

    @Override
    public boolean accepts()
    {
        return counter != null;
    }
}
