package com.baesystems.ai.lr.security.sign;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Map;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SecureSignatureVerifierImpl implements SecureSignatureVerifier
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureSignatureVerifierImpl.class);

    @Autowired
    private SecureSignatureRepository signatureRepository;

    @Override
    public boolean verify(final String clientId, final String checkBody, final String checkSumSource)
    {
        // fetch Digital Signatures from the DB
        final Map<String, PublicKeyHolder> signatures = signatureRepository.getSignatures();

        boolean validationResult = false;

        // check to see if parameters are valid before continuing
        if (validateParameters(clientId, checkSumSource, signatures))
        {
            // extract the checksum
            // IMPORTANT: we are storing the Checksum in B16 not B64
            final String checkSum = checkSumSource.substring(0, checkSumSource.indexOf('='));

            // fetch out Public Key for Client Version
            final PublicKeyHolder securityHolder = signatures.get(clientId);
            try
            {
                // get signature instance for algorithm type i.e. RSA/Sha
                final Signature verifier = Signature.getInstance(securityHolder.getAlgorithm());
                verifier.initVerify(securityHolder.getPublicKey());
                verifier.update(checkBody.getBytes("utf-8"));

                final byte[] checksum = Hex.decodeHex(checkSum.toCharArray());
                validationResult = verifier.verify(checksum);
            }
            catch (final SignatureException exception)
            {
                LOGGER.error("verify-signature", exception);
            }
            catch (final InvalidKeyException exception)
            {
                LOGGER.error("verify-invalidkey", exception);
            }
            catch (final NoSuchAlgorithmException exception)
            {
                LOGGER.error("verify-noalgo", exception);
            }
            catch (final DecoderException exception)
            {
                LOGGER.error("verify-decoder", exception);
            }
            catch (UnsupportedEncodingException exception)
            {
                LOGGER.error("verify-badencoding", exception);
            }
        }
        return validationResult;
    }

    private Boolean validateParameters(final String clientVersion, final String checkSumSource, final Map<String, PublicKeyHolder> signatures)
    {
        boolean result = true;
        if (clientVersion == null || clientVersion.length() == 0)
        {
            LOGGER.warn("validateParameters-version - missing client version");
            result = false;
        }

        else if (!signatures.containsKey(clientVersion))
        {
            LOGGER.warn("validateParameters-version - unsupported client version [" + clientVersion + "]");
            result = false;
        }

        else if (checkSumSource == null || checkSumSource.length() == 0)
        {
            LOGGER.warn("validateParameters-checksum - missing checksum");
            result = false;
        }

        else if (checkSumSource.indexOf('=') < 0)
        {
            LOGGER.warn("validateParameters-checksum - invalidChecksum [" + checkSumSource + "]");
            result = false;
        }
        return result;
    }
}
