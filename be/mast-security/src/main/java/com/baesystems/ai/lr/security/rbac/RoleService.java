package com.baesystems.ai.lr.security.rbac;

public interface RoleService
{
    CachedRoleDto getRoles();
}
