package com.baesystems.ai.lr.security;

public interface SecurityStatusManager
{
    boolean isSecurityEnabled();
}
