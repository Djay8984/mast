package com.baesystems.ai.lr.security.sign;

import java.io.IOException;
import java.io.StringReader;
import java.security.PublicKey;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.security.SecuritySignatureDao;
import com.baesystems.ai.lr.dto.security.SecureSignatureDto;

@Component
public class SecureSignatureRepository
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureSignatureRepository.class);

    private static final Integer REFRESH_DELAY = 60000;

    @SuppressWarnings("PMD.AvoidUsingVolatile")
    private volatile Map<String, PublicKeyHolder> signatures;

    @Autowired
    private SecuritySignatureDao signatureDao;

    /**
     * Keep reference for GC usage
     */
    private final Timer timer = new Timer();

    @PostConstruct
    public void initialise()
    {
        loadSignatures();

        // Auto Refresh Signatures from the DB
        timer.scheduleAtFixedRate(new SecurityRefreshTask(), REFRESH_DELAY, REFRESH_DELAY);
    }

    /**
     * Return signature set, this is volatile and should always be called to fetch latest
     */
    public Map<String, PublicKeyHolder> getSignatures()
    {
        return signatures;
    }

    private void loadSignatures()
    {
        final ConcurrentHashMap<String, PublicKeyHolder> newSignatures = new ConcurrentHashMap<String, PublicKeyHolder>();
        final List<SecureSignatureDto> results = this.signatureDao.getSecuritySignatures();
        for (final SecureSignatureDto result : results)
        {

            // "SHA256WithRSA"
            final PublicKey publicKey = readPublicKey(result.getPublicKey());
            final PublicKeyHolder holder = new PublicKeyHolder(publicKey, result.getAlgorithm());
            newSignatures.put(result.getName(), holder);
        }

        this.signatures = Collections.unmodifiableMap(newSignatures);
    }

    private PublicKey readPublicKey(final String keyFile)
    {
        PublicKey publicKey = null;
        try (final StringReader reader = new StringReader(keyFile))
        {
            try (final PEMParser pemParser = new PEMParser(reader))
            {
                final SubjectPublicKeyInfo publicKeyInfo = (SubjectPublicKeyInfo) pemParser.readObject();

                // Convert to Java (JCA) format
                final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
                publicKey = converter.getPublicKey(publicKeyInfo);

            }
            catch (final IOException exception)
            {
                LOGGER.error("SecurityRefreshTask-run", exception);
            }
        }
        return publicKey;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try
        {
            this.timer.cancel();
        }
        finally
        {
            super.finalize();
        }
    }

    private class SecurityRefreshTask extends TimerTask
    {
        @Override
        public void run()
        {
            loadSignatures();
        }
    }
}
