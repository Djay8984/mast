package com.baesystems.ai.lr.security.interceptors.monitor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.security.interceptors.RequestSecurityInterceptor;
import com.baesystems.ai.lr.security.rbac.authorisation.AntMatcher;

import net.bull.javamelody.CounterAccessHelper;

/**
 * Monitors the overall HTTP Request and tracks the time-used called via the Camel Security Interceptor
 */
@Component
public class MonitorInterceptor implements RequestSecurityInterceptor
{
    @Autowired(required = false)
    private CounterAccessHelper counter;

    @Autowired
    private AntMatcher antMatcher;

    @Override
    public boolean before(final Exchange exchange)
    {
        counter.startMethod(antMatcher.extractPath(exchange));
        return true;
    }

    @Override
    public void after(final Exchange exchange)
    {
        counter.endMethod(false);
    }

    @Override
    public void error(final Exchange exchange, final boolean intercepted)
    {
        counter.endMethod(true);
    }

    @Override
    public boolean accepts(final Class<? extends Message> classType)
    {
        return counter != null;
    }
}
