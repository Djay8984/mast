package com.baesystems.ai.lr.security.camel;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spi.Synchronization;

import com.baesystems.ai.lr.security.interceptors.RouteSecurityInterceptor;

/**
 * Provides "ROUTE" level monitoring of Camel
 */
public class CamelSecurityRouteProcessor implements Processor, Synchronization
{
    private final List<RouteSecurityInterceptor> inteceptors;

    public CamelSecurityRouteProcessor(final List<RouteSecurityInterceptor> interceptors)
    {
        this.inteceptors = interceptors;

    }

    @Override
    public void process(final Exchange exchange)
    {
        // determine if any route-inteceptors are required
        // don't create the callback if not required
        boolean monitor = false;
        for (final RouteSecurityInterceptor routeInterceptor : inteceptors)
        {
            monitor = (routeInterceptor.accepts() && routeInterceptor.before(exchange)) | monitor;
        }

        if (monitor)
        {
            // UoW is the Route marker
            // notify us back when the route is complete
            exchange.getUnitOfWork().addSynchronization(this);
        }
    }

    @Override
    public void onComplete(final Exchange exchange)
    {
        for (final RouteSecurityInterceptor routeInterceptor : inteceptors)
        {
            if (routeInterceptor.accepts())
            {
                routeInterceptor.after(exchange);
            }
        }
    }

    @Override
    public void onFailure(final Exchange exchange)
    {
        for (final RouteSecurityInterceptor routeInterceptor : inteceptors)
        {
            if (routeInterceptor.accepts())
            {
                routeInterceptor.error(exchange, false);
            }
        }
    }
}
