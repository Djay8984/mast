package com.baesystems.ai.lr.security.rbac.authorisation;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.baesystems.ai.lr.constants.MastConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;

import com.baesystems.ai.lr.security.rbac.CachedRoleDto;

/**
 * <code>RoleVoter</code> Checks a ROLE against an ANT Matching pattern examples /asset/1 -> /asset/* /asset/100 ->
 * /asset/* /webjars/swagger/index.html -> /webjars/**
 *
 * return ACCESS_DENIED or ACCESS_GRANTED
 */
public class RoleVoter implements AccessDecisionVoter<FilterInvocation>
{

    @Autowired
    private AntMatcher antMatcher;

    private final CachedRoleDto roleReferenceData;

    public RoleVoter(final CachedRoleDto roleReferenceData)
    {
        this.roleReferenceData = roleReferenceData;
    }

    @Override
    public boolean supports(final ConfigAttribute attribute)
    {
        return true;
    }

    @Override
    public boolean supports(final Class<?> clazz)
    {
        return clazz.isAssignableFrom(FilterInvocation.class);
    }

    @Override
    public int vote(final Authentication authentication, final FilterInvocation object, final Collection<ConfigAttribute> attributes)
    {
        final HttpServletRequest request = object.getHttpRequest();

        // GET requests are only granted if the user is authenticated
        int result = authentication instanceof AnonymousAuthenticationToken ? ACCESS_DENIED : ACCESS_GRANTED;

        // Get API's are allowed - no need for verification
        if (request.getMethod() == null || !request.getMethod().equals(HttpMethod.GET.toString()))
        {
            // returns servletPath + pathInfo, formatted in ANT Matcher Style
            final String fullPath = antMatcher.extractPath(request);

            //Temporarily Sanitise paths from external-system for cdf to use existing admin permissions (rather than
            // altering existing permissions
            //Todo We can remove this when a decision has been made on rbac group roles for trusted systems
            final String path = fullPath.replace("/" + MastConstants.TRUSTED_SYSTEM_PREFIX, "");

            @SuppressWarnings("unchecked")
            final Collection<GrantedAuthority> roles = (Collection<GrantedAuthority>) authentication.getAuthorities();
            result = directMatch(path, roles);

            // only if direct has failed, attempt a wildcard search, this a slower pass
            // than a direct search
            if (result == ACCESS_DENIED)
            {
                result = indirectMatch(path, roles);
            }
        }
        return result;
    }

    /**
     * Performs a direct match only /asset/* to /asset/1
     */
    private int directMatch(final String path, final Collection<GrantedAuthority> roles)
    {
        final Map<String, Set<String>> roleToFunctions = roleReferenceData.getDirectMatches();
        int result = ACCESS_DENIED;
        for (final GrantedAuthority role : roles)
        {
            final Set<String> functions = roleToFunctions.get(role.getAuthority());
            if (functions != null && functions.contains(path))
            {
                result = ACCESS_GRANTED;
                break;
            }
        }
        return result;
    }

    /**
     * Performs a wildcard match /asset/** to /asset/1 or /asset/1/type this is less efficient than a Direct Match
     */
    private int indirectMatch(final String requestedPath, final Collection<GrantedAuthority> roles)
    {
        final Map<String, Set<String>> roleToFunctions = roleReferenceData.getIndirectMatches();
        int result = ACCESS_DENIED;
        for (final GrantedAuthority role : roles)
        {
            final Set<String> functions = roleToFunctions.get(role.getAuthority());
            if (functions == null)
            {
                continue;
            }

            for (final String functionPath : functions)
            {
                if (antMatcher.wildcardMatch(functionPath, requestedPath))
                {
                    result = ACCESS_GRANTED;
                    break;
                }
            }
        }
        return result;
    }

}
