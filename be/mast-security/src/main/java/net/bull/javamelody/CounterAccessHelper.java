package net.bull.javamelody;

import java.util.Date;

/**
 * Updates performance counters in Java Melody we use package acess here as the developer has his whole library in 1
 * package
 */
public class CounterAccessHelper
{
    /**
     * URL intercepts are being counted in Spring Counter
     */
    private final Counter methodCounter;

    /**
     * Camel Route intercepts are being counted in GUICE counter, monitor doesn't support custom counters
     */
    private final Counter routeCounter;

    public CounterAccessHelper()
    {
        // indicate to reporting we are only interested in from now, logs are kept between instance runs and stored in
        // java.io.tmp
        final Date startDate = new Date();
        methodCounter = MonitoringProxy.getSpringCounter();
        methodCounter.setDisplayed(true);
        methodCounter.setStartDate(startDate);
        methodCounter.setUsed(true);

        // guice isn't displayed via default
        routeCounter = MonitoringProxy.getGuiceCounter();
        routeCounter.setDisplayed(true);
        routeCounter.setStartDate(startDate);
        routeCounter.setUsed(true);

    }

    public void startMethod(final String requestName)
    {
        methodCounter.bindContextIncludingCpu(requestName);
    }

    public void endMethod(final boolean error)
    {
        methodCounter.addRequestForCurrentContext(error);
    }

    public void startRoute(final String requestName)
    {
        routeCounter.bindContextIncludingCpu(requestName);
    }

    public void endRoute(final boolean error)
    {
        routeCounter.addRequestForCurrentContext(error);
    }
}
