package com.baesystems.ai.lr.security.interceptors;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.security.rbac.authorisation.AntMatcher;

public class AntMatcherTest
{

    @Test
    public void testMatch()
    {
        final String sourcePath = "GET:/v2/asset/233/asset-note/1";
        final String path = filterRequestedPath(sourcePath);

        final String expectedPath = "GET:/v*/asset/*/asset-note/*";
        Assert.assertEquals("PATH", path, expectedPath);
    }

    @Test
    public void testMatch2()
    {
        final String sourcePath = "PUT:/asset/233/asset-note";
        final String path = filterRequestedPath(sourcePath);

        final String expectedPath = "PUT:/asset/*/asset-note";
        Assert.assertEquals("PATH", path, expectedPath);
    }

    @Test
    public void testMatch3()
    {
        final String sourcePath = "POST:/asset/1/asset-note/133";
        final String path = filterRequestedPath(sourcePath);

        final String expectedPath = "POST:/asset/*/asset-note/*";
        Assert.assertEquals("PATH", path, expectedPath);
    }

    @Test
    public void testWild()
    {
        final String matcher = "GET:/webjars/**";
        final String shouldMatch = "GET:/webjars/swagger/index.html";
        final String shouldMatch2 = "GET:/webjars/";
        final String wontMatch = "GET:/webjar2s/swagger/index.html";
        final String wontMatch2 = "GET:/webjars";

        Assert.assertTrue("should match", wildcardMatch(matcher, shouldMatch));
        Assert.assertTrue("should match", wildcardMatch(matcher, shouldMatch2));
        Assert.assertFalse("shouldn't match", wildcardMatch(matcher, wontMatch));
        Assert.assertFalse("shouldn't match", wildcardMatch(matcher, wontMatch2));
    }

    private boolean wildcardMatch(final String matcherString, final String path)
    {
        final AntMatcher matcher = new AntMatcher();
        return matcher.wildcardMatch(matcherString, path);
    }

    private String filterRequestedPath(final String path)
    {
        final AntMatcher matcher = new AntMatcher();
        return matcher.toMatcherFormat(path);
    }

}
