package com.baesystems.ai.lr.security.interceptors;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.junit.Before;
import org.junit.Test;

public class AuditInterceptorTest
{

    private AuditInterceptor interceptor;

    @Before
    public void setUp()
    {
        this.interceptor = new AuditInterceptor();
    }

    @Test
    public void doTest()
    {
        final DefaultMessage message = new DefaultMessage();
        message.setBody("hello");

        final DefaultExchange exchange = new DefaultExchange(new DefaultCamelContext());
        exchange.setIn(message);
        message.setExchange(exchange);

        message.setHeader(Exchange.HTTP_METHOD, "GET");
        interceptor.before(exchange);
        interceptor.after(exchange);
        interceptor.accepts(null);

        message.setHeader(Exchange.HTTP_METHOD, "PUT");
        interceptor.before(exchange);
        interceptor.after(exchange);
        interceptor.accepts(null);

        interceptor.error(exchange, true);
    }
}
