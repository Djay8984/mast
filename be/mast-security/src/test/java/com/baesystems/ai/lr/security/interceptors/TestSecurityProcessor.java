package com.baesystems.ai.lr.security.interceptors;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.apache.camel.impl.DefaultUnitOfWork;
import org.apache.camel.spi.UnitOfWork;
import org.junit.Test;

import com.baesystems.ai.lr.security.camel.CamelSecurityProcessor;

public class TestSecurityProcessor
{

    @Test
    public void securityOn()
    {
        final AtomicInteger executionCount = new AtomicInteger();
        final Processor target = new Processor()
        {

            @Override
            public void process(final Exchange exchange) throws Exception
            {
                executionCount.incrementAndGet();
            }

        };

        final CamelSecurityProcessor processor = new CamelSecurityProcessor(target, new ArrayList<RequestSecurityInterceptor>(),
                new ArrayList<RouteSecurityInterceptor>());

        final DefaultMessage message = new DefaultMessage();
        message.setBody("hello");

        final DefaultExchange exchange = new DefaultExchange(new DefaultCamelContext());
        final UnitOfWork work = new DefaultUnitOfWork(exchange);
        exchange.setUnitOfWork(work);
        exchange.setIn(message);
        message.setExchange(exchange);
        processor.process(exchange);

        assertEquals("expect allowed", 1, executionCount.get());

        work.done(exchange);
    }

    @Test
    public void securityOnNotAllowed()
    {
        final AtomicInteger executionCount = new AtomicInteger();
        final Processor target = new Processor()
        {

            @Override
            public void process(final Exchange exchange) throws Exception
            {
                executionCount.incrementAndGet();
            }

        };

        final ArrayList<RequestSecurityInterceptor> interceptors = new ArrayList<RequestSecurityInterceptor>();
        interceptors.add(new RequestSecurityInterceptor()
        {

            @Override
            public void error(final Exchange exchange, final boolean intercepted)
            {
                // not required

            }

            @Override
            public boolean before(final Exchange exchange)
            {
                // not required
                return false;
            }

            @Override
            public void after(final Exchange exchange)
            {
                // not required
            }

            @Override
            public boolean accepts(final Class<? extends Message> classType)
            {
                // not required
                return true;
            }
        });

        final CamelSecurityProcessor processor = new CamelSecurityProcessor(target, interceptors, new ArrayList<RouteSecurityInterceptor>());

        final DefaultMessage message = new DefaultMessage();
        message.setBody("hello");
        final DefaultExchange exchange = new DefaultExchange(new DefaultCamelContext());
        exchange.setIn(message);
        message.setExchange(exchange);
        processor.process(exchange);
        assertEquals("expect not allowed", 0, executionCount.get());
    }

    @Test
    public void securityOnProcessorError()
    {
        final AtomicInteger executionCount = new AtomicInteger();
        final Processor target = new Processor()
        {

            @Override
            public void process(final Exchange exchange) throws Exception
            {
                final int a = 1;
                final int b = a - 1;
                final int c = a / b;
                exchange.setProperty("never gets here", c);
            }

        };

        final ArrayList<RequestSecurityInterceptor> interceptors = new ArrayList<RequestSecurityInterceptor>();
        interceptors.add(new RequestSecurityInterceptor()
        {

            @Override
            public void error(final Exchange exchange, final boolean intercepted)
            {
                // check to see if we are hit
                executionCount.incrementAndGet();

            }

            @Override
            public boolean before(final Exchange exchange)
            {
                // not required
                return true;
            }

            @Override
            public void after(final Exchange exchange)
            {
                // not required
            }

            @Override
            public boolean accepts(final Class<? extends Message> classType)
            {
                // not required
                return true;
            }
        });

        final CamelSecurityProcessor processor = new CamelSecurityProcessor(target, interceptors, new ArrayList<RouteSecurityInterceptor>());

        final DefaultMessage message = new DefaultMessage();
        message.setBody("hello");
        final DefaultExchange exchange = new DefaultExchange(new DefaultCamelContext());
        exchange.setIn(message);
        message.setExchange(exchange);
        processor.process(exchange);
        assertEquals("expect 1 error count", 1, executionCount.get());
    }
}
