package com.baesystems.ai.lr.enums;

public enum CaseMilestoneStatusType
{
    OPEN(1L),
    COMPLETE(2L);

    private final Long value;

    CaseMilestoneStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
