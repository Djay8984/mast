package com.baesystems.ai.lr.enums;

public enum AttachmentTargetType
{
    CASE,
    MILESTONE,
    SERVICE,
    ASSET_NOTE,
    WIP_ASSET_NOTE,
    COC,
    WIP_COC,
    ASSET,
    JOB,
    ITEM,
    DEFECT,
    WIP_DEFECT,
    ACTIONABLE_ITEM,
    WIP_ACTIONABLE_ITEM,
    CERTIFICATE,
    REPAIR,
    WIP_REPAIR,
    REPORT,
    TASK,
    WIP_TASK,
    SURVEY
}
