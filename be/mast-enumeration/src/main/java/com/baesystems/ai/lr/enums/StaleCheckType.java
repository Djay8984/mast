package com.baesystems.ai.lr.enums;

public enum StaleCheckType
{
    ACTIONABLE_ITEM("Actionable Item"),
    ASSET("Asset"),
    ASSET_NOTE("Asset Note"),
    CASE("Case"),
    CASE_MILESTONE("Case Milestone"),
    CERTIFICATE("Certificate"),
    COC("Condition of Class"),
    DEFECT("Defect"),
    FOLLOW_UP_ACTION("Follow Up Action"),
    JOB("Job"),
    PRODUCT("Product"),
    REPAIR("Repair"),
    REPORT("Report"),
    SERVICE("Service"),
    SURVEY("Survey"),
    TASK("Task");

    private final String typeString;

    StaleCheckType(final String typeString)
    {
        this.typeString = typeString;
    }

    public String getTypeString()
    {
        return this.typeString;
    }
}
