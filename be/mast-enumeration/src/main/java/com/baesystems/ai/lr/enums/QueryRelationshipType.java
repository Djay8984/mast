package com.baesystems.ai.lr.enums;

public enum QueryRelationshipType
{
    // Less than
    LT,

    // Less or equal
    LE,

    // Equal
    EQ,

    // Not equal
    NE,

    // Greater than
    GT,

    // Greater than or equal
    GE,

    // In values
    IN,

    // for wild card search
    LIKE
}
