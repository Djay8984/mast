package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * CodicilStatusType enum matching MAST_REF_codicilstatus DB table. 31 May 2016
 */
public enum CodicilStatusType
{

    // From table MAST_REF_codicilstatus 31-May-2016
    // UNKNOWN currently has no corresponding DB value however was deemed necessary to prevent
    // the return of NULL when requesting an invalid status type when using one of
    // the getters below, e.g. get INACTIVE for a CoC
    UNKNOWN(0L),
    AI_DRAFT(1L),
    AI_INACTIVE(2L),
    AI_OPEN(3L),
    AI_CHANGE_RECOMMENDED(4L),
    AI_DELETED(5L),
    AI_DELETED_FOR_CURRENT_JOB(6L),
    AI_CANCELLED(7L),
    AN_DRAFT(8L),
    AN_INACTIVE(9L),
    AN_OPEN(10L),
    AN_CHANGE_RECOMMENDED(11L),
    AN_DELETED(12L),
    AN_CANCELLED(13L),
    COC_OPEN(14L),
    COC_DELETED(15L),
    COC_CANCELLED(16L);

    private final Long value;

    private static final Map<Long, CodicilStatusType> CODICIL_STATUS_TYPE_MAP = Arrays
            .stream(CodicilStatusType.values())
            .collect(Collectors.toMap(CodicilStatusType::getValue, Function.identity()));

    CodicilStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }

    /**
     * @return a list of CodicilStatusType values that count as open
     */
    public static final List<Long> getCodicilStatusesForOpen()
    {
        return Arrays.asList(
                AI_OPEN.getValue(),
                AN_OPEN.getValue(),
                COC_OPEN.getValue(),
                AN_INACTIVE.getValue(),
                AI_INACTIVE.getValue(),
                AN_CHANGE_RECOMMENDED.getValue(),
                AI_CHANGE_RECOMMENDED.getValue());
    }

    /**
     * @return a list of CodicilStatusType values that are valid for courses of action for a CoC
     */
    public static final List<Long> getCoCStatusesForCourseOfAction()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(COC_OPEN.getValue());

        return statuses;
    }

    /**
     * @return a list of CodicilStatusType values that are valid for courses of action for an Actionable Item
     */
    public static final List<Long> getActionableItemStatusesForCourseOfAction()
    {
        final ArrayList<Long> statuses = new ArrayList<Long>();

        statuses.add(AI_OPEN.getValue());
        statuses.add(AI_INACTIVE.getValue());

        return statuses;
    }

    /**
     * Returns the OPEN enum for the specified Codicil type or UNKNOWN
     *
     * @param codicilType
     * @return OPEN or UNKNOWN if OPEN is an invalid status for the specified CodicilType.
     */
    public static CodicilStatusType getOpen(final CodicilType codicilType)
    {
        CodicilStatusType open;
        switch (codicilType)
        {
            case AI:
                open = CodicilStatusType.AI_OPEN;
                break;
            case AN:
                open = CodicilStatusType.AN_OPEN;
                break;
            case COC:
                open = CodicilStatusType.COC_OPEN;
                break;
            default:
                open = CodicilStatusType.UNKNOWN;
                break;
        }
        return open;
    }

    /**
     * Returns the DRAFT Enum for the specified Codicil or UNKNOWN.
     *
     * @param codicilType
     * @return DRAFT or UNKNOWN if DRAFT is invalid for the specified CodicilType
     */
    public static CodicilStatusType getDraft(final CodicilType codicilType)
    {
        CodicilStatusType draft;
        switch (codicilType)
        {
            case AI:
                draft = CodicilStatusType.AI_DRAFT;
                break;
            case AN:
                draft = CodicilStatusType.AN_DRAFT;
                break;
            case COC:
            default:
                draft = CodicilStatusType.UNKNOWN;
                break;
        }
        return draft;

    }

    /**
     * Returns the INACTIVE Enum for the specified CodicilType or UNKNOWN
     *
     * @param codicilType
     * @return INACTIVE or UNKNOWN if INACTIVE is an invalid status for the specified CodicilType.
     */
    public static CodicilStatusType getInactive(final CodicilType codicilType)
    {
        CodicilStatusType inactive;
        switch (codicilType)
        {
            case AI:
                inactive = CodicilStatusType.AI_INACTIVE;
                break;
            case AN:
                inactive = CodicilStatusType.AN_INACTIVE;
                break;
            case COC:
            default:
                inactive = CodicilStatusType.UNKNOWN;
                break;
        }
        return inactive;
    }

    /**
     * Returns the enum for the specified value or UNKNOWN no mapping exists.
     *
     * @param value
     * @return the Enum mapped to value or UNKNOWN if no such mapping exists.
     */
    public static CodicilStatusType getEnum(final Long value)
    {
        CodicilStatusType codicilStatusType = UNKNOWN;
        if (value != null)
        {
            codicilStatusType = CODICIL_STATUS_TYPE_MAP.getOrDefault(value, UNKNOWN);
        }
        return codicilStatusType;
    }

    public static List<Long> getOpenAndInactiveForAssetNote()
    {
        return Arrays.asList(
                AN_OPEN.getValue(),
                AN_INACTIVE.getValue());
    }

    public static List<Long> getOpenAndInactiveForActionableItem()
    {
        return Arrays.asList(
                AI_OPEN.getValue(),
                AI_INACTIVE.getValue());
    }
}
