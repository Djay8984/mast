package com.baesystems.ai.lr.enums;

public enum FollowUpActionStatus
{
    INCOMPLETE(1L),
    COMPLETE(2L);

    private final Long value;

    FollowUpActionStatus(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
