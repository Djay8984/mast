package com.baesystems.ai.lr.enums;

public enum SREServiceQueryParameter
{
    CREDITED_SERVICE_ID("creditedServiceId"),
    ASSET_HARMONISATION_DATE("assetHarmonisationDate"),
    COMPLETION_DATE("completionDate");

    private final String parameterName;

    private SREServiceQueryParameter(final String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getParameterName()
    {
        return parameterName;
    }
}
