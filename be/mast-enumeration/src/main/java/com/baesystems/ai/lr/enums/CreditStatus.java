package com.baesystems.ai.lr.enums;

/**
 * CreditStatus enum matching MAST_REF_CreditStatus DB table.
 * 23 June 2016
 */
public enum CreditStatus
{
    CONFIRMED(1L),
    COMPLETED(2L),
    WAIVED(3L),
    UNCREDITED(4L);

    private final Long value;

    CreditStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
