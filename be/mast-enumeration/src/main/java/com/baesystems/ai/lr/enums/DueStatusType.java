package com.baesystems.ai.lr.enums;
public enum DueStatusType
{
    NOT_DUE(1L),
    DUE(2L),
    OVERDUE(3L);

    private final Long value;

    DueStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
