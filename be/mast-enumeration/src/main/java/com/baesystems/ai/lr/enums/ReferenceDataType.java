package com.baesystems.ai.lr.enums;

/**
 * Enum for database tables which follow the Reference Data convention, i.e. only have "id", "name" and "description"
 * columns.
 */
public enum ReferenceDataType
{
    ATTACHMENT_TYPE("MAST_REF_AttachmentType"),
    ASSET_TYPE("MAST_REF_AssetType"),
    CONFIDENTIALITY_TYPE("MAST_REF_ConfidentialityType"),
    CASE_TYPE("MAST_REF_CaseType"),
    CERTIFICATE_STATUS("MAST_REF_CertificateStatus"),
    CODICIL_STATUS("MAST_REF_CodicilStatus"),
    CUSTOMER_FUNCTION("MAST_REF_CustomerFunctionType"),
    CUSTOMER_RELATIONSHIP("MAST_REF_PartyRole"),
    DEFECT_STATUS("MAST_REF_DefectStatus"),
    SURVEYOR_TYPE("MAST_REF_CaseResourceType"),
    IACS_SOCIETY("MAST_REF_ClassSociety"),
    ITEM_CLASS("MAST_REF_ItemClass"),
    OFFICE_ROLE("MAST_REF_OfficeRole"),
    PRE_EIC_INSPECTION_STATUS("MAST_REF_PreEICInspectionStatus"),
    REPORT_TYPE("MAST_REF_ReportType"),
    RISK_ASSESSMENT_STATUS("MAST_REF_RiskAssessmentStatus"),
    RULE_SET("MAST_REF_RulesetCategory"),
    RULE_SET2("MAST_REF_Ruleset2"),
    SERVICE_STATUS("MAST_REF_ServiceStatus"),
    SERVICE_TYPE("MAST_REF_ServiceType"),
    SCHEDULING_RULE_TYPE("MAST_REF_SchedulingType");

    private final String table;

    ReferenceDataType(final String table)
    {
        this.table = table;
    }

    public final String getTable()
    {
        return table;
    }
}
