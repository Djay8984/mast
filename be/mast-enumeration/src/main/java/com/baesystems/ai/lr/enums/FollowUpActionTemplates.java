package com.baesystems.ai.lr.enums;

public enum FollowUpActionTemplates
{
    PR17(1L),
    EIC(2L);

    private final Long value;

    FollowUpActionTemplates(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
