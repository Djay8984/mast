package com.baesystems.ai.lr.enums;

public enum ExceptionType
{
    NON_SPECIFIC_EXCEPTION(1000),
    STALE_ENTITY_UPDATED(1001),
    DUPLICATE_CASE(1002),
    MISSING_DICTIONARY(1003),
    INVERSE_RELATIONSHIP(1004),
    MISSING_ITEM(1005);

    private final int exceptionCode;

    private ExceptionType(final int exceptionCode)
    {
        this.exceptionCode = exceptionCode;

    }

    public int getExceptionCode()
    {
        return this.exceptionCode;
    }
}
