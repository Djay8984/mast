package com.baesystems.ai.lr.enums;

public enum ServiceStatus
{
    NOT_STARTED(1L),
    PART_HELD(2L),
    HELD(3L),
    COMPLETE(4L);

    private final Long value;

    private ServiceStatus(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return this.value;
    }
}
