package com.baesystems.ai.lr.enums;

import java.util.HashMap;
import java.util.Map;

public enum JobStatusType
{
    UNKNOWN(0L),
    SDO_ASSIGNED(1L),
    RESOURCE_ASSIGNED(2L),
    UNDER_SURVEY(3L),
    UNDER_REPORTING(4L),
    AWAITING_TECHNICAL_REVIEWER_ASSIGNMENT(5L),
    UNDER_TECHNICAL_REVIEW(6L),
    AWAITING_ENDORSER_ASSIGNMENT(7L),
    UNDER_ENDORSEMENT(8L),
    CANCELLED(9L),
    ABORTED(10L),
    CLOSED(11L);

    private static final Map<Long, JobStatusType> JOB_STATUS_TYPES = new HashMap<>();

    static
    {
        for (final JobStatusType statusType : JobStatusType.values())
        {
            JOB_STATUS_TYPES.put(statusType.getValue(), statusType);
        }
    }

    private final Long value;

    JobStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    /**
     * Returns a JobStatusType for a given Long value or UNKNOWN if the specified Long is null or not mapped.
     *
     * @param statusTypeValue
     * @return the JobStatusType for the specified Long or UNKNOWN if there is no mapping.
     */
    public static JobStatusType getJobStatusType(final Long statusTypeValue)
    {
        JobStatusType returnValue = UNKNOWN;
        if (statusTypeValue != null)
        {
            returnValue = JOB_STATUS_TYPES.get(statusTypeValue);
            if (returnValue == null)
            {
                returnValue = UNKNOWN;
            }
        }
        return returnValue;
    }
}
