package com.baesystems.ai.lr.enums;

public enum MilestoneDueDateReferenceType
{
    CASE_ACCEPTANCE_DATE(1L),
    ESTIMATED_BUILD_DATE(2L),
    MILESTONE_COMPLETION(3L),
    MANUAL(4L);

    private final Long value;

    MilestoneDueDateReferenceType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
