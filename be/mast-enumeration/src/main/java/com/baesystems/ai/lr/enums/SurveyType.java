package com.baesystems.ai.lr.enums;

public enum SurveyType
{
    DAMAGE,
    REPAIR
}
