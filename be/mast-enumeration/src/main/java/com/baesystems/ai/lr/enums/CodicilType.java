package com.baesystems.ai.lr.enums;

public enum CodicilType
{
    // From table MAST_REF_codiciltype 31-May2016
    COC(1L),
    AI(2L),
    AN(3L),
    SD(4L),
    SOI(5L),
    NCN(6L),
    SDD(7L),
    OBS(8L);

    private final Long value;

    CodicilType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }
}
