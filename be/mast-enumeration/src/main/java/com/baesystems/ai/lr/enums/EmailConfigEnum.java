package com.baesystems.ai.lr.enums;

public enum EmailConfigEnum
{
    DEFAULT("E", "01");
    private String emailType;
    private String sequence;

    public String getEmailType()
    {
        return emailType;
    }

    public String getSequence()
    {
        return sequence;
    }

    private EmailConfigEnum(String emailType, String sequence)
    {
        this.emailType = emailType;
        this.sequence = sequence;
    }
}
