package com.baesystems.ai.lr.enums;

import java.util.ArrayList;
import java.util.List;

public enum ServiceCreditStatusType
{
    NOT_STARTED(1L),
    PART_HELD(2L),
    POSTPONED(3L),
    COMPLETE(4L),
    FINISHED(5L);

    private final Long value;

    ServiceCreditStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return this.value;
    }

    public static List<Long> getUncredited()
    {
        final List<Long> uncreditedIds = new ArrayList<Long>();

        uncreditedIds.add(NOT_STARTED.value());

        return uncreditedIds;
    }

    public static List<Long> getOpen()
    {
        final List<Long> openIds = new ArrayList<Long>();

        openIds.add(NOT_STARTED.value());
        openIds.add(PART_HELD.value());
        openIds.add(POSTPONED.value());

        return openIds;
    }
}
