package com.baesystems.ai.lr.enums;

public enum RuleEngineResponseStatus
{
    SUCCESS,
    FAILURE
}
