package com.baesystems.ai.lr.enums;

public enum CustomerRoleType
{
    CONTRACT_HOLDER(5L),
    MAIN_BUILDER(11L),
    CFO(61L);

    private final Long value;

    CustomerRoleType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
