package com.baesystems.ai.lr.enums;

public enum Operator {

    Greater(">") {
        @Override public boolean include(final String aValue, final String attValue)
        {
            return (Integer.parseInt(attValue) > Integer.parseInt(aValue));
        }
    },

    Less("<")
    {
        @Override public boolean include(final String aValue, final String attValue)
        {
            return (Integer.parseInt(attValue) < Integer.parseInt(aValue));
        }
    },

    Equal("=")
    {
        @Override public boolean include(final String aValue, final String attValue)
        {
            return (Integer.parseInt(attValue) == Integer.parseInt(aValue));
        }
    };


    private final String value;

    Operator(final String value)
    {
        this.value = value;
    }

    public final String value() {
        return this.value;
    }

    public abstract boolean include(final String aValue, final String attValue);

}
