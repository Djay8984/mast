package com.baesystems.ai.lr.enums;

public enum FaultType
{
    // From table MAST_REF_FaultType
    DEFECT(1L),
    DEFICIENCY(2L);

    private final Long value;

    FaultType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }
}
