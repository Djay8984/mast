package com.baesystems.ai.lr.enums;

import java.util.Arrays;
import java.util.List;

public enum OfficeRoleType
{
    SDO(1L),
    CFO(2L),
    CFO_3(3L),
    MMS_SDO(4L),
    MMSO(5L),
    TSO(6L),
    TSO_STAT(7L),
    CLASS_GROUP(8L),
    DCE(9L),
    EIC(10L),
    MDS(11L),
    CASE_SDO(14L),
    JOB_SDO(15L);

    private final Long value;

    OfficeRoleType(final Long value)
    {
        this.value = value;
    }

    public final Long getValue()
    {
        return value;
    }

    public static List<Long> getMandatoryRolesForCase()
    {
        return Arrays.asList(CASE_SDO.getValue());
    }
}
