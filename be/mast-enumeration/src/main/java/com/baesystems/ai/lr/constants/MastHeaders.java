package com.baesystems.ai.lr.constants;

/**
 * Constants class for MAST centric HTTP headers for REST API consumers
 */
public interface MastHeaders
{
    String IDS_CLIENT_IDENTITY = "IDSClientIdentity"; // Client identification, serves as the "key" to the client's stored public key
    String IDS_CLIENT_VERSION = "IDSClientVersion";     // Client version
    String IDS_CLIENT_SIGN = "IDSClientSign";     // Checksum ([METHOD]:[CORE-API_URL)] and payload, signed with the client's private key)
    String USER = "user";  // Header used for RBAC
    String GROUPS = "groups";  // Header used for RBAC
}
