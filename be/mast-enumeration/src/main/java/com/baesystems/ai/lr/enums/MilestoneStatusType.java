package com.baesystems.ai.lr.enums;

public enum MilestoneStatusType
{
    OPEN(1L),
    COMPLETE(2L);

    private final Long value;

    MilestoneStatusType(final Long value)
    {
        this.value = value;
    }

    public final Long value()
    {
        return value;
    }
}
