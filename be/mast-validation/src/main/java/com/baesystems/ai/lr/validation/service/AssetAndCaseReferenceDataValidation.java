package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.dao.references.flagports.FlagReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.AssetReferenceDataService;
import com.baesystems.ai.lr.service.references.CaseReferenceDataService;
import com.baesystems.ai.lr.service.references.EmployeeReferenceDataService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@SuppressWarnings("PMD.TooManyMethods")
public class AssetAndCaseReferenceDataValidation
{
    @Autowired
    private CaseReferenceDataService caseReferenceDataService;

    @Autowired
    private AssetReferenceDataService assetReferenceDataService;

    @Autowired
    private FlagReferenceDataDao flagReferenceDataDao;

    @Autowired
    private EmployeeReferenceDataService employeeReferenceDataService;

    protected void validateCaseType(final LinkResource caseType) throws RecordNotFoundException
    {
        final Long caseTypeId = caseType.getId();
        if (this.caseReferenceDataService.getCaseType(caseTypeId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, caseTypeId.intValue()));
        }
    }

    protected void validatePreEicInspectionStatus(final LinkResource preEicInspectionStatus) throws RecordNotFoundException
    {
        if (preEicInspectionStatus != null)
        {
            final Long preEicInspectionStatusId = preEicInspectionStatus.getId();
            if (this.caseReferenceDataService.getCaseType(preEicInspectionStatusId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, preEicInspectionStatusId.intValue()));
            }
        }
    }

    protected void validateRiskAssessmentStatus(final LinkResource riskAssessmentStatus) throws RecordNotFoundException
    {
        if (riskAssessmentStatus != null)
        {
            final Long riskAssessmentStatusId = riskAssessmentStatus.getId();
            if (this.caseReferenceDataService.getRiskAssessmentStatus(riskAssessmentStatusId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, riskAssessmentStatusId.intValue()));
            }
        }
    }

    protected void validateAssetLifecycleStatus(final LinkResource assetLifecycleStatus) throws RecordNotFoundException
    {
        if (assetLifecycleStatus != null)
        {
            final Long assetLifecycleStatusId = assetLifecycleStatus.getId();
            if (this.assetReferenceDataService.getAssetLifecycleStatus(assetLifecycleStatusId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG,
                        assetLifecycleStatusId.intValue()));
            }
        }
    }

    protected void validateAssetType(final LinkResource assetType) throws RecordNotFoundException
    {
        if (assetType != null)
        {
            final Long assetTypeId = assetType.getId();
            if (this.assetReferenceDataService.getAssetType(assetTypeId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, assetTypeId.intValue()));
            }
        }
    }

    protected void validateClassStatus(final LinkResource classStatus) throws RecordNotFoundException
    {
        if (classStatus != null)
        {
            final Long classStatusId = classStatus.getId();
            if (this.assetReferenceDataService.getClassStatus(classStatusId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, classStatusId.intValue()));
            }
        }
    }

    protected void validateClassDepartment(final LinkResource classDepartment) throws RecordNotFoundException
    {
        if (classDepartment != null)
        {
            final Long classDepartmentId = classDepartment.getId();
            if (this.assetReferenceDataService.getClassDepartment(classDepartmentId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, classDepartmentId.intValue()));
            }
        }

    }

    protected void validateFlagState(final LinkResource flagState) throws RecordNotFoundException
    {
        if (flagState != null)
        {
            final Long flagStateId = flagState.getId();
            if (this.flagReferenceDataDao.getFlagState(flagStateId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, flagStateId.intValue()));
            }
        }
    }

    protected void validateAssetCategory(final LinkResource assetCategory) throws RecordNotFoundException
    {
        if (assetCategory != null)
        {
            final Long assetCategoryId = assetCategory.getId();
            if (this.assetReferenceDataService.getAssetCategory(assetCategoryId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, assetCategoryId.intValue()));
            }
        }
    }

    protected void validateRuleSet(final LinkResource ruleSet) throws RecordNotFoundException
    {
        if (ruleSet != null)
        {
            final Long ruleSetId = ruleSet.getId();
            if (this.assetReferenceDataService.getRuleSet(ruleSetId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, ruleSetId.intValue()));
            }
        }
    }

    protected void validateProductRuleSet(final LinkResource ruleSet) throws RecordNotFoundException
    {
        if (ruleSet != null)
        {
            final Long ruleSetId = ruleSet.getId();
            if (this.assetReferenceDataService.getProductRuleSet(ruleSetId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, ruleSetId.intValue()));
            }
        }
    }

    protected void validateLosingSociety(final LinkResource losingSociety) throws RecordNotFoundException
    {
        if (losingSociety != null)
        {
            final Long losingSocietyId = losingSociety.getId();
            if (this.assetReferenceDataService.getIacsSociety(losingSocietyId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, losingSocietyId.intValue()));
            }
        }
    }

    protected void validateOfficeRole(final LinkResource officeRole) throws RecordNotFoundException
    {
        final Long officeRoleId = officeRole.getId();
        if (this.caseReferenceDataService.getOfficeRole(officeRoleId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, officeRoleId.intValue()));
        }
    }

    protected void validateSurveyorType(final LinkResource surveyorType) throws RecordNotFoundException
    {
        final Long surveyorTypeId = surveyorType.getId();
        if (this.employeeReferenceDataService.getEmployeeRole(surveyorTypeId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, surveyorTypeId.intValue()));
        }
    }

    protected void validateCustomerRelationship(final LinkResource customerRelationship) throws RecordNotFoundException
    {
        final Long customerRelationshipId = customerRelationship.getId();
        if (this.assetReferenceDataService.getCustomerRelationship(customerRelationshipId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, customerRelationshipId.intValue()));
        }
    }

    protected void validateCustomerFunction(final LinkResource customerFunction) throws RecordNotFoundException
    {
        final Long customerFunctionId = customerFunction.getId();
        if (this.assetReferenceDataService.getCustomerFunction(customerFunctionId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, customerFunctionId.intValue()));
        }
    }
}
