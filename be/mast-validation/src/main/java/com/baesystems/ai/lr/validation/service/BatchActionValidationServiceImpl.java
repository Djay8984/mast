package com.baesystems.ai.lr.validation.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.CodicilType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.BatchActionValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "BatchActionValidationService")
@Loggable(Loggable.DEBUG)
public class BatchActionValidationServiceImpl implements BatchActionValidationService
{
    @Autowired
    private AssetValidationService validationService;

    @Autowired
    private DateHelper dateHelper;

    @Override
    public void verifyAssetIdListNotNull(final List<Long> assetIdList) throws BadRequestException
    {
        if (assetIdList == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Asset ID List"));
        }
    }

    /**
     * Verify a list of AssetIds i.e. that each one exists.
     * The specified list of IDs may contain NULL as these are stripped out however duplicate IDs are allowed.
     *
     * @param assetIds List of AssetIds to verify
     * @throws BadRequestException     if specfied assetIds list is NULL
     * @throws RecordNotFoundException an id within the List does not exist.
     */
    @Override
    public void verifyAssetIds(final List<Long> assetIds) throws BadRequestException, RecordNotFoundException
    {
        verifyAssetIdListNotNull(assetIds);
        final List<Long> cleanedAssetIds = assetIds.stream()
                .filter(id -> id != null)
                .collect(Collectors.toList());
        for (final Long id : cleanedAssetIds)
        {
            validationService.verifyAssetIdExists(id.toString());
        }
    }

    @Override
    public void verifyAssetNoteStatus(final AssetNoteDto assetNoteDto) throws BadRequestException
    {
        verifyCodicilStatus(assetNoteDto.getImposedDate(), assetNoteDto.getStatus().getId(), CodicilType.AN);
    }

    @Override
    public void verifyActionableItemStatus(final ActionableItemDto actionableItemDto) throws BadRequestException
    {
        verifyCodicilStatus(actionableItemDto.getImposedDate(), actionableItemDto.getStatus().getId(), CodicilType.AI);
    }

    /**
     * Verifies the specified Codicil status is correct for the specified Imposed date.
     *
     * @param imposedDate
     * @param codicilStatus
     * @param codicilType
     * @throws BadRequestException
     */
    private void verifyCodicilStatus(final Date imposedDate, final Long codicilStatus, final CodicilType codicilType)
            throws BadRequestException
    {
        if (CodicilStatusType.getDraft(codicilType).getValue().equals(codicilStatus))
        {
            if (imposedDate != null)
            {
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.INVALID_VALUE, "The specified status: "
                                + codicilStatus
                                + " for an unspecified Imposed date"));
            }
        }
        else if (CodicilStatusType.getOpen(codicilType).getValue().equals(codicilStatus))
        {
            if (!dateHelper.isTodayOrAfter(imposedDate))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.INVALID_VALUE, "The specified status: "
                        + codicilStatus
                        + " for future Imposed date"));
            }
        }
        else if (!CodicilStatusType.getInactive(codicilType).getValue().equals(codicilStatus))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.INVALID_VALUE, "The specified status: "
                    + codicilStatus
                    + " for historical Imposed date"));
        }
    }
}
