package com.baesystems.ai.lr.validation.service;

import static java.lang.String.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AttachmentValidationService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "AttachmentValidationService")
@Loggable(Loggable.DEBUG)
public class AttachmentValidationServiceImpl implements AttachmentValidationService
{
    @Autowired
    private ValidationDataService validationDataService;

    public void verifyHasAttachment(final Long entityId, final String attachmentTargetTypeString, final Long attachmentId)
            throws RecordNotFoundException, MastBusinessException
    {
        try
        {
            final AttachmentTargetType attachmentTargetType = AttachmentTargetType.valueOf(attachmentTargetTypeString);
            verifyObjectHasAttachment(entityId, attachmentTargetType, attachmentId);
        }
        catch (IllegalArgumentException exception)
        {
            throw new MastBusinessException("Illegal Target Type for Attachment", exception);
        }
    }

    private void verifyObjectHasAttachment(final Long objectId, final AttachmentTargetType target, final Long attachmentId)
            throws RecordNotFoundException
    {
        final Long attId = this.validationDataService.findAttachmentByObject(target, objectId, attachmentId);

        if (attId == null)
        {
            throw new RecordNotFoundException(format(ExceptionMessagesUtils.INVALID_ATTACHMENT_PATH,
                    attachmentId, target.toString(), objectId));
        }
    }
}
