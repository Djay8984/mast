package com.baesystems.ai.lr.validation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.customers.PartyDao;
import com.baesystems.ai.lr.dao.employees.LrEmployeeDao;
import com.baesystems.ai.lr.dao.employees.OfficeDao;
import com.baesystems.ai.lr.dao.references.flagports.PortDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.enums.CustomerRoleType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Service(value = "AssetAndCaseValidationUtils")
public class AssetAndCaseValidationUtils extends AssetAndCaseReferenceDataValidation
{
    @Autowired
    private OfficeDao officeDao;

    @Autowired
    private LrEmployeeDao lrEmployeeDao;

    @Autowired
    private PartyDao partyDao;

    @Autowired
    private PortDao portDao;

    public void validateLinks(final CaseWithAssetDetailsDto caseDto, final Boolean canSaveWithErrors)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        validateCaseType(caseDto.getCaseType());
        validatePreEicInspectionStatus(caseDto.getPreEicInspectionStatus());
        validateRiskAssessmentStatus(caseDto.getRiskAssessmentStatus());
        validateAssetLifecycleStatus(caseDto.getAssetLifecycleStatus());
        validateAssetType(caseDto.getAssetType());
        validateClassStatus(caseDto.getClassStatus());
        validateClassStatus(caseDto.getPreviousCaseStatus());
        validateFlagState(caseDto.getFlagState());
        validateRegisteredPort(caseDto.getRegisteredPort());
        validateRuleSet(caseDto.getRuleSet());
        validateLosingSociety(caseDto.getLosingSociety());
        verifyOffices(caseDto.getOffices(), canSaveWithErrors);
        verifySurveyors(caseDto.getSurveyors(), canSaveWithErrors);
        verifyCustomers(caseDto.getCustomers());
    }

    public void validateLinks(final AssetLightDto asset)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        validateAssetLifecycleStatus(asset.getAssetLifecycleStatus());
        validateAssetType(asset.getAssetType());
        validateClassStatus(asset.getClassStatus());
        validateClassDepartment(asset.getClassDepartment());
        validateAssetCategory(asset.getAssetCategory());
        validateFlagState(asset.getFlagState());
        validateRegisteredPort(asset.getRegisteredPort());
        validateRuleSet(asset.getRuleSet());
        validateRuleSet(asset.getPreviousRuleSet());
        validateProductRuleSet(asset.getProductRuleSet());
        validateLosingSociety(asset.getCoClassificationSociety());

        verifyOffices(asset.getOffices(), true);
        verifyCustomers(asset.getCustomers());
    }

    private void verifySurveyors(final List<CaseSurveyorWithLinksDto> surveyorLinks, final Boolean canSaveWithErrors)
            throws RecordNotFoundException, BadRequestException
    {
        if (!canSaveWithErrors && (surveyorLinks == null || surveyorLinks.isEmpty()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "surveyors"));
        }
        if (surveyorLinks != null)
        {
            for (final CaseSurveyorWithLinksDto surveyorLink : surveyorLinks)
            {
                validateSurveyor(surveyorLink.getSurveyor());
                validateSurveyorType(surveyorLink.getEmployeeRole());
            }
        }
    }

    private void verifyOffices(final List<OfficeLinkDto> officeLinks, final Boolean canSaveWithErrors)
            throws BadRequestException, RecordNotFoundException
    {
        if (!canSaveWithErrors && (officeLinks == null || officeLinks.isEmpty()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "offices"));
        }
        if (officeLinks != null)
        {
            for (final OfficeLinkDto officeLink : officeLinks)
            {
                validateOfficeRole(officeLink.getOfficeRole());
                validateOffice(officeLink.getOffice());
            }
        }
    }

    /**
     * This method validates that all of the customers being linked to the case exist and that all their functions and
     * relationships exist and that a customer has not been added twice with the same relationship.
     */
    private void verifyCustomers(final List<CustomerLinkDto> cutsomerLinks) throws RecordNotFoundException, BadRequestException,
            MastBusinessException
    {
        final Map<Long, List<Long>> customerIdToCunstomerRelationships = new HashMap<Long, List<Long>>();

        if (cutsomerLinks != null)
        {
            Boolean cfoFound = false;
            Boolean contractHolderFound = false;

            for (final CustomerLinkDto customerLink : cutsomerLinks)
            {
                validateCustomer(customerLink.getCustomer());
                final Long customerId = customerLink.getCustomer().getId();

                if (!customerIdToCunstomerRelationships.keySet().contains(customerId))
                { // Add the customer to the hashmap if there are not already there.
                    customerIdToCunstomerRelationships.put(customerId, new ArrayList<Long>());
                }

                validateCustomerRelationship(customerLink.getRelationship());

                final Long relationshipId = customerLink.getRelationship().getId();

                if (customerIdToCunstomerRelationships.get(customerId).contains(relationshipId))
                { // if this relationship already exists for the customer throw an erro if not add it to the customers
                  // relationships.
                    throw new MastBusinessException(String.format(ExceptionMessagesUtils.MORE_THAN_ONE_OF_TYPE, "customer", "relationship"));
                }
                else
                {
                    customerIdToCunstomerRelationships.get(customerId).add(relationshipId);
                }

                if (customerLink.getFunctions() == null)
                {
                    continue;
                }
                if (new HashSet<CustomerHasFunctionDto>(customerLink.getFunctions()).size() != customerLink.getFunctions().size())
                {
                    throw new MastBusinessException(
                            String.format(ExceptionMessagesUtils.CANNOT_ADD_MORE_THAN_ONCE, "function", "customer and relationship"));
                }

                for (final CustomerHasFunctionDto function : customerLink.getFunctions())
                {
                    validateCustomerFunction(function.getCustomerFunction());
                    cfoFound = testRestrictedCustomerFunctions(function.getCustomerFunction(), cfoFound, CustomerRoleType.CFO);
                    contractHolderFound = testRestrictedCustomerFunctions(function.getCustomerFunction(), contractHolderFound,
                            CustomerRoleType.CONTRACT_HOLDER);
                }

            }
        }
    }

    private Boolean testRestrictedCustomerFunctions(final LinkResource function, final Boolean customerFunctionFound,
            final CustomerRoleType functionType) throws MastBusinessException
    {
        Boolean returnValue = false;
        if (functionType.getValue().equals(function.getId()))
        {
            if (customerFunctionFound)
            {
                throw new MastBusinessException(
                        String.format(ExceptionMessagesUtils.CANNOT_ADD_MORE_THAN_ONCE, "Customer with function CFO or contract holder",
                                "asset or case"));
            }

            returnValue = true;
        }
        return returnValue;
    }

    private void validateRegisteredPort(final LinkResource registeredPort) throws RecordNotFoundException
    {
        if (registeredPort != null)
        {
            final Long registeredPortId = registeredPort.getId();
            if (this.portDao.getPortOfRegistry(registeredPortId) == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, registeredPortId.intValue()));
            }
        }
    }

    private void validateOffice(final LinkResource officeLinkResource) throws RecordNotFoundException, BadRequestException
    {
        final Long officeId = officeLinkResource.getId();
        if (officeId == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "office.id"));
        }
        if (this.officeDao.getOffice(officeId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, officeId.intValue()));
        }

    }

    private void validateSurveyor(final LinkResource surveyorLinkResource) throws RecordNotFoundException
    {
        final Long surveyorId = surveyorLinkResource.getId();
        if (this.lrEmployeeDao.getLrEmployee(surveyorId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, surveyorId.intValue()));
        }
    }

    private void validateCustomer(final PartyDto customer) throws RecordNotFoundException, BadRequestException
    {
        final Long customerId = customer.getId();
        if (customerId == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "customer.id"));
        }
        if (this.partyDao.getCustomer(customerId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, customerId.intValue()));
        }
    }
}
