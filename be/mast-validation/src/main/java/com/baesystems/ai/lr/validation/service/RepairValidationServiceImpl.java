package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.RepairValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "RepairValidationService")
@Loggable(Loggable.DEBUG)
public class RepairValidationServiceImpl implements RepairValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private RepairDao repairDao;

    @Autowired
    private WIPRepairDao wipRepairDao;

    private static final String REPAIR = "Repair";

    @Override
    public void verifyRepairIdExists(final String inputRepairId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputRepairId);

        final Long repairId = this.validationService.verifyNumberFormat(inputRepairId);

        if (!this.repairDao.repairExists(repairId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, repairId));
        }
    }

    @Override
    public void verifyWIPRepairIdExists(final String inputRepairId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputRepairId);

        final Long repairId = this.validationService.verifyNumberFormat(inputRepairId);

        if (!this.wipRepairDao.repairExists(repairId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, repairId));
        }
    }

    /**
     * Validation to check that the repair we're retrieving belongs to the coc ID specified.
     *
     * @param inputCocId the coc ID as a {@link String}
     * @param inputRepairId the repair ID as a {@link String}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateCoCHasRepair(final String inputCoCId, final String inputRepairId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputCoCId);
        this.validationService.verifyId(inputRepairId);

        final Long cocId = Long.parseLong(inputCoCId);
        final Long repairId = Long.parseLong(inputRepairId);

        if (!this.repairDao.repairExistsForCoC(repairId, cocId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, REPAIR, repairId,
                    "CoC", cocId));
        }
    }

    /**
     * Validation to check that the repair we're retrieving belongs to the defect ID specified.
     *
     * @param inputDefectId the defect ID as a {@link String}
     * @param inputRepairId the repair ID as a {@link String}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateDefectHasRepair(final String inputDefectId, final String inputRepairId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputDefectId);
        this.validationService.verifyId(inputRepairId);

        final Long defectId = Long.parseLong(inputDefectId);
        final Long repairId = Long.parseLong(inputRepairId);

        if (!this.repairDao.repairExistsForDefect(repairId, defectId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, REPAIR, repairId,
                    "Defect", defectId));
        }
    }

    /**
     * Validation to check that the WIP Repair we're retrieving belongs to the WIP Defect ID specified.
     *
     * @param inputDefectId the WIP Defect ID as a {@link String}
     * @param inputRepairId the WIP Repair ID as a {@link String}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateWIPDefectHasWIPRepair(final String inputDefectId, final String inputRepairId) throws RecordNotFoundException,
            BadRequestException
    {
        this.validationService.verifyId(inputDefectId);
        this.validationService.verifyId(inputRepairId);

        final Long defectId = Long.parseLong(inputDefectId);
        final Long repairId = Long.parseLong(inputRepairId);

        if (!this.wipRepairDao.repairExistsForDefect(repairId, defectId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, REPAIR, repairId,
                    "Defect", defectId));
        }
    }

    /**
     * Validation to check that the WIP Repair we're retrieving belongs to the WIP Defect ID specified.
     *
     * @param inputCoCId the WIP Coc ID as a {@link String}
     * @param inputRepairId the WIP Repair ID as a {@link String}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateWIPCoCHasWIPRepair(final String inputCoCId, final String inputRepairId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputCoCId);
        this.validationService.verifyId(inputRepairId);

        final Long cocId = Long.parseLong(inputCoCId);
        final Long repairId = Long.parseLong(inputRepairId);

        if (!this.wipRepairDao.repairExistsForCoC(repairId, cocId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, REPAIR, repairId,
                    "WIPCoC", cocId));
        }
    }

    /**
     * Verifies if the given repair permanent.
     */
    @Override
    public Boolean isRepairPermanent(final RepairDto repair)
    {
        return repair != null && repair.getRepairAction() != null && repair.getRepairAction().getId().equals(RepairActionType.PERMANENT.value())
                && repair.getConfirmed() != null && repair.getConfirmed();
    }
}
