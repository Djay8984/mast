package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.UpdateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service("UpdateValidationService")
@Loggable(Loggable.DEBUG)
public class UpdateValidationServiceImpl implements UpdateValidationService
{
    @Autowired
    private ValidationService validationService;

    @Override
    public void verifyIds(final String bodyId, final String headerId) throws BadRequestException
    {
        this.validationService
                .verifyField(bodyId, headerId, String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", bodyId, headerId));
    }

}
