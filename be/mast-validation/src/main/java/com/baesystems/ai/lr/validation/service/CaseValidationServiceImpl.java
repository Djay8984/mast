package com.baesystems.ai.lr.validation.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.EmployeeRoleType;
import com.baesystems.ai.lr.enums.OfficeRoleType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.DuplicateCaseException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.CaseReferenceDataService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.CaseValidationService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.utils.ValidationUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CaseValidationService")
@Loggable(Loggable.DEBUG)
public class CaseValidationServiceImpl implements CaseValidationService
{
    private static final Logger LOG = LoggerFactory.getLogger(CaseValidationServiceImpl.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ValidationDataService validationDataService;

    @Autowired
    private CaseDao caseDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private CaseReferenceDataService caseReferenceDataService;

    @Autowired
    private AssetValidationService assetValidationService;

    @Autowired
    private AssetAndCaseValidationUtils assetAndCaseValidationUtils;

    @Autowired
    private CaseMilestoneDao caseMilestoneDao;

    @Autowired
    private ValidationUtils validationUtils;

    @Override
    public void validateCaseUpdate(final Long caseId, final CaseWithAssetDetailsDto caseDto)
            throws BadRequestException, ParseException, RecordNotFoundException, MastBusinessException
    {
        validateCaseAndAssetNotNull(caseDto);

        this.validationService.verifyId(caseId);

        if (caseUpdateRequiresUniquenessValidation(caseDto))
        {
            verifyCaseIsUnique(caseDto);
        }
        this.verifyCase(caseDto);

        final CaseWithAssetDetailsDto originalCaseDto = this.caseDao.getCase(caseDto.getId());

        if (CaseStatusType.CLOSED.getValue().equals(originalCaseDto.getCaseStatus().getId()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, originalCaseDto.getId(), "closed"));
        }

        validateStatusChange(caseDto, originalCaseDto);
        validateNonEditableFields(caseDto, originalCaseDto);
    }

    @Override
    public void validateCaseSave(final CaseWithAssetDetailsDto caseDto)
            throws BadRequestException, ParseException, RecordNotFoundException, MastBusinessException
    {
        validateCaseAndAssetNotNull(caseDto);

        this.validationService.verifyIdNull(caseDto.getId());
        this.verifyCase(caseDto);

        verifyCaseIsUnique(caseDto);
    }

    @Override
    public void verifyCaseIdExists(final String inputCaseId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputCaseId);
        final Long caseId = this.validationService.verifyNumberFormat(inputCaseId);

        if (!this.caseDao.caseExists(caseId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, caseId));
        }
    }

    @Override
    public void verifyCaseIsUnique(final CaseWithAssetDetailsDto caseDto) throws DuplicateCaseException
    {
        List<Long> duplicateCases = null;

        if (caseDto != null && caseDto.getAsset() != null && caseDto.getCaseType() != null)
        {
            final Long assetId = caseDto.getAsset().getId();
            final Long caseTypeId = caseDto.getCaseType().getId();

            duplicateCases = this.validationDataService.findDuplicateCases(caseTypeId, assetId);
        }

        if (duplicateCases != null)
        {
            duplicateCases.remove(caseDto.getId());
            if (!duplicateCases.isEmpty())
            {
                // Any cases found are classified as duplicates
                throw new DuplicateCaseException(duplicateCases);
            }
        }
    }

    @Override
    public void verifyCase(final CaseWithAssetDetailsDto caseDto)
            throws ParseException, RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final Long assetId = caseDto.getAsset().getId();
        final Long publishedId = this.assetDao.getPublishedVersion(assetId);
        final AssetLightDto asset = this.assetDao.getAsset(assetId, publishedId, AssetLightDto.class);

        if (asset == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, assetId));
        }

        verifyCaseStatus(caseDto.getCaseStatus());

        final CaseStatusDto status = this.caseReferenceDataService.getCaseStatus(caseDto.getCaseStatus().getId());
        final boolean canSaveWithErrors = status != null && status.getCanSaveWithErrors() != null ? status.getCanSaveWithErrors() : false;

        this.assetAndCaseValidationUtils.validateLinks(caseDto, canSaveWithErrors);

        if (!canSaveWithErrors)
        {
            this.validationService.verifyBody(caseDto);
            validateMandatorySurveyors(caseDto.getSurveyors());
            validateMandatoryOffices(caseDto.getOffices());

            caseDto.getSurveyors();

            if (asset.getIsLead() == null)
            {
                final String errorMessage = String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Is lead");
                LOG.debug(errorMessage);
                throw new MastBusinessException(errorMessage);
            }
            else if (!asset.getIsLead())
            {
                validateLeadAsset(asset.getLeadImo());
            }
        }
    }

    private void validateLeadAsset(final Long leadImo) throws RecordNotFoundException, MastBusinessException
    {
        if (leadImo != null)
        {
            this.assetValidationService.verifyImoNumberExists(leadImo);
        }
        else
        {
            final String errorMessage = String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Lead imo number");
            LOG.debug(errorMessage);
            throw new MastBusinessException(errorMessage);
        }
    }

    private void validateCaseAndAssetNotNull(final CaseWithAssetDetailsDto caseDto) throws BadRequestException
    {
        if (caseDto == null || caseDto.getAsset() == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.BODY_NOT_NULL);
        }
    }

    private void verifyCaseStatus(final LinkResource caseStatus) throws RecordNotFoundException, BadRequestException
    {
        if (caseStatus == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "caseStatus may not be null"));
        }

        final Long caseStatusId = caseStatus.getId();

        if (caseStatusId == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "caseStatus.id may not be null"));
        }

        if (this.caseReferenceDataService.getCaseStatus(caseStatusId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, caseStatusId.intValue()));
        }
    }

    private void validateMandatorySurveyors(final List<CaseSurveyorWithLinksDto> surveyors) throws BadRequestException
    {
        if (surveyors != null)
        {
            final List<Long> enteredSurveyorRoles = new ArrayList<Long>();

            for (final CaseSurveyorWithLinksDto surveyor : surveyors)
            {
                enteredSurveyorRoles.add(surveyor.getEmployeeRole().getId());
            }

            if (!enteredSurveyorRoles.containsAll(EmployeeRoleType.getMandatoryRolesForCase()))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.MANDATORY_TYPE_MISSING, "surveyor type", "surveyors"));
            }
        }
    }

    private void validateMandatoryOffices(final List<OfficeLinkDto> offices) throws BadRequestException
    {
        if (offices != null)
        {
            final List<Long> officeRoles = new ArrayList<Long>();

            for (final OfficeLinkDto office : offices)
            {
                officeRoles.add(office.getOfficeRole().getId());
            }

            if (!officeRoles.containsAll(OfficeRoleType.getMandatoryRolesForCase()))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.MANDATORY_TYPE_MISSING, "office type", "offices"));
            }
        }
    }

    /**
     * Method to make sure that the imoNumber and build date are not edited when updating the case and the case
     * acceptance date is not edited after the case is committed. The imoNumber and the buildDate are checked against
     * the asset not the case so that the asset information is not updated when it is merged back. The method also
     * checks that the asset has not been changed.
     */
    private void validateNonEditableFields(final CaseWithAssetDetailsDto caseDto, final CaseWithAssetDetailsDto originalCaseDto)
            throws BadRequestException
    {
        validationUtils.verifyObjectIsUnchanged(caseDto.getAsset(), originalCaseDto.getAsset(), "asset");

        final Long publishedId = this.assetDao.getPublishedVersion(caseDto.getAsset().getId());
        final AssetLightDto asset = this.assetDao.getAsset(caseDto.getAsset().getId(), publishedId, AssetLightDto.class);
        validationUtils.verifyObjectIsUnchanged(caseDto.getImoNumber(), asset.getIhsAsset().getId(), "imoNumber");
        validationUtils.verifyObjectIsUnchanged(caseDto.getBuildDate(), asset.getBuildDate(), "buildDate");

        if (!CaseStatusType.UNCOMMITTED.getValue().equals(originalCaseDto.getCaseStatus().getId()))
        {
            validationUtils.verifyObjectIsUnchanged(caseDto.getCaseAcceptanceDate(), originalCaseDto.getCaseAcceptanceDate(), "caseAcceptanceDate");
        }
    }

    /**
     * Method to validate case status changes against the valid transitions in the reference data and check that all
     * milestones are complete if the case is being closed.
     */
    private void validateStatusChange(final CaseWithAssetDetailsDto caseDto, final CaseWithAssetDetailsDto originalCaseDto)
            throws BadRequestException
    {
        final Long newStatus = caseDto.getCaseStatus().getId();
        final Long oldStatus = originalCaseDto.getCaseStatus().getId();

        if (!oldStatus.equals(newStatus) && !this.caseReferenceDataService.getValidNewCaseStatuses(oldStatus).contains(newStatus))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.INVALID_STATUS_UPDATE, newStatus, oldStatus));
        }

        if (CaseStatusType.CLOSED.getValue().equals(newStatus) && !this.caseMilestoneDao.areAllInScopeMilestonesCompleteForCase(caseDto.getId()))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.INCORRECT_STATUS_SPECIFIC, newStatus, "until all in scope milestones are complete"));
        }
    }

    /**
     * If the case is going from a state where other cases of the same type on the asset are permitted to a state where
     * they are not, then we need to validate that there are no duplicates. Otherwise, there's no point in doing the
     * validation as if the data is already in a bad state then this validation may well actually prevent you from
     * fixing it by closing/deleting duplicate cases.
     *
     * @param CaseWithAssetDetailsDto caseDto
     * @return boolean - do we need to do the validation?
     */
    @Override
    public boolean caseUpdateRequiresUniquenessValidation(final CaseWithAssetDetailsDto caseDto)
    {
        boolean validationRequired = false;
        final List<Long> statusesNotPermittingDuplicates = CaseStatusType.getDuplicateCaseStatusSearch();
        final Long newStatus = caseDto.getCaseStatus().getId();
        if (statusesNotPermittingDuplicates.contains(newStatus))
        {
            final Long currentStatus = this.validationDataService.getCurrentStatusForCase(caseDto.getId());
            if (!statusesNotPermittingDuplicates.contains(currentStatus))
            {
                validationRequired = true;
            }
        }

        return validationRequired;

    }
}
