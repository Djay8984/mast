package com.baesystems.ai.lr.validation.utils;

import static com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.FALSE;
import static com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.TRUE;
import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.STRING_FIELD_DOES_NOT_MATCH;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.STRINGREGEX;
import com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.STRINGTYPE;

@Component(value = "ValidationStringUtils")
public class ValidationStringUtils extends ValidationUtils
{
    /**
     * Overloaded method to set the message to an empty sting if no message is given.
     */
    public void validateStringEqual(final String value, final String reference, final String fieldName, final List<String> results)
    {
        validateStringEqual(value, reference, fieldName, results, "");
    }

    /**
     * Overloaded method to set the message to an empty sting if no message is given.
     */
    public void validateStringCustom(final String value, final String pattern, final String fieldName, final List<String> results)
    {
        validateStringCustom(value, pattern, fieldName, results, "");
    }

    /**
     * Overloaded method to set the message to an empty sting if no message is given.
     */
    public void validateString(final String value, final String type, final String fieldName, final List<String> results)
    {
        validateString(value, type, fieldName, results, "");
    }

    /**
     * Tests the presence of an object and adds to the list of errors if it is not found. Tests if the value is with a
     * given range (passing null removes the limit)
     *
     * @param value, the value whose presence and properties are to be tested.
     * @param reference, the string that value should be equal to.
     * @param fieldName, the name of the field to which the object belongs, to be used it the error message.
     * @param results, list of errors to be appended if need be.
     * @param message, an optional string to append to the default message.
     */
    public void validateStringEqual(final String value, final String reference, final String fieldName, final List<String> results,
            final String message)
    {
        validatePresence(value, fieldName, results);

        if (value != null && reference != null && !value.equals(reference))
        {
            results.add(String.format(STRING_FIELD_DOES_NOT_MATCH, fieldName, reference) + message);
        }
    }

    /**
     * Tests the presence of an object and adds to the list of errors if it is not found. Tests if the value is with a
     * given range (passing null removes the limit)
     *
     * @param value, the value whose presence and properties are to be tested.
     * @param pattern, the regex pattern the string should match.
     * @param fieldName, the name of the field to which the object belongs, to be used it the error message.
     * @param results, list of errors to be appended if need be.
     * @param message, an optional string to append to the default message.
     */
    public void validateStringCustom(final String value, final String pattern, final String fieldName, final List<String> results,
            final String message)
    {
        validatePresence(value, fieldName, results);

        if (value != null && pattern != null && !value.matches(pattern))
        {
            results.add(String.format(STRING_FIELD_DOES_NOT_MATCH, fieldName, pattern) + message);
        }
    }

    private boolean matchString(final String value, final String type)
    {
        Boolean isMatch = true;

        switch (type)
        {
            case STRINGTYPE.ALPHANUMERIC:
                isMatch = value.matches(STRINGREGEX.ALPHANUMERIC);
                break;
            case STRINGTYPE.NUMERIC:
                isMatch = value.matches(STRINGREGEX.NUMERIC);
                break;
            case STRINGTYPE.STRICTALPHA:
                isMatch = value.matches(STRINGREGEX.STRICTALPHA);
                break;
            case STRINGTYPE.ALPHA:
                isMatch = value.matches(STRINGREGEX.ALPHA);
                break;
            case STRINGTYPE.BOOLEAN:
                isMatch = value.toLowerCase(Locale.ENGLISH).equals(TRUE) || value.toLowerCase(Locale.ENGLISH).equals(FALSE);
                break;
            case STRINGTYPE.DATE:
                try
                {
                    DateHelper.mastFormatter().parse(value);
                }
                catch (final ParseException exception)
                {
                    isMatch = false;
                }
                break;
            default:
                isMatch = true;
                break;
        }

        return isMatch;
    }

    /**
     * Tests the presence of an object and adds to the list of errors if it is not found. Tests if the value is with a
     * given range (passing null removes the limit)
     *
     * @param value, the value whose presence and properties are to be tested.
     * @param type, the name of the standard regex pattern the string should match.
     * @param fieldName, the name of the field to which the object belongs, to be used it the error message.
     * @param results, list of errors to be appended if need be.
     * @param message, an optional string to append to the default message.
     */
    public void validateString(final String value, final String type, final String fieldName, final List<String> results, final String message)
    {
        validatePresence(value, fieldName, results);

        if (value != null && type != null)
        {
            final Boolean isMatch = matchString(value, type);

            if (!isMatch)
            {
                results.add(String.format(STRING_FIELD_DOES_NOT_MATCH, fieldName, type) + message);
            }
        }
    }

}
