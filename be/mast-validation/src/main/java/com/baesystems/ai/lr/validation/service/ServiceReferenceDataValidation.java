package com.baesystems.ai.lr.validation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Component(value = "ServiceReferenceDataValidation")
public class ServiceReferenceDataValidation
{
    private static final Logger LOG = LoggerFactory.getLogger(ServiceReferenceDataValidation.class);

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Autowired
    private ValidationService validationService;

    /**
     * Verifies if: - status and credit status are valid (exists on the database) if they are given
     */
    protected void validateServiceRefData(final ScheduledServiceDto newService) throws BadRequestException, RecordNotFoundException
    {
        validateServiceStatus(newService.getServiceStatus());
        validateServiceCreditStatus(newService.getServiceCreditStatus());
        validateServiceCatalogue(newService.getServiceCatalogueId());
    }

    private void validateServiceStatus(final LinkResource serviceStatus) throws RecordNotFoundException, BadRequestException
    {
        if (serviceStatus != null)
        {
            final Long serviceStatusId = serviceStatus.getId();

            this.validationService.verifyId(serviceStatusId.toString());

            if (this.serviceReferenceDataDao.getServiceStatus(serviceStatusId) == null)
            {
                final String message = String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service status", serviceStatusId);
                LOG.debug(message);
                throw new RecordNotFoundException(message);
            }
        }
    }

    private void validateServiceCreditStatus(final LinkResource serviceCreditStatus) throws RecordNotFoundException, BadRequestException
    {
        if (serviceCreditStatus != null)
        {
            final Long serviceCreditStatusId = serviceCreditStatus.getId();

            this.validationService.verifyId(serviceCreditStatusId.toString());

            if (this.serviceReferenceDataDao.getServiceCreditStatus(serviceCreditStatusId) == null)
            {
                final String message = String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service credit status",
                        serviceCreditStatusId);
                LOG.debug(message);
                throw new RecordNotFoundException(message);
            }
        }
    }

    /**
     * Verifies if: - If given service catalogue id is valid. - If there is a service catalogue in the database for the
     * given id.
     */
    private void validateServiceCatalogue(final Long serviceCatalogueId) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(serviceCatalogueId.toString());

        if (this.serviceDao.getServiceCatalogueDO(serviceCatalogueId) == null)
        {
            final String message = String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service catalogue", serviceCatalogueId);
            LOG.debug(message);
            throw new RecordNotFoundException(message);
        }
    }
}
