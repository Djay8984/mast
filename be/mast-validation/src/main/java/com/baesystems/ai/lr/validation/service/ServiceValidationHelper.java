package com.baesystems.ai.lr.validation.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.ServiceCatalogueRelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Component(value = "ServiceValidationHelper")
public class ServiceValidationHelper
{
    private static final Logger LOG = LoggerFactory.getLogger(ServiceValidationHelper.class);

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ValidationService validationService;

    /**
     * Verifies if: - inputProductId is a valid id. - There is a valid product on the database for the given
     * inputProductId.
     */
    protected ProductDO validateProduct(final String inputProductId) throws BadRequestException, RecordNotFoundException
    {
        final Long productId = this.validationService.verifyNumberFormat(inputProductId);

        final ProductDO product = this.productDao.getProductDO(productId);

        if (product == null)
        {
            final String message = String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "product", productId);
            LOG.debug(message);
            throw new RecordNotFoundException(message);
        }

        return product;
    }

    /**
     * Method to validate that the proposed new state of the database after a post or delete is valid given the
     * relationships between the service catalogues in the reference data.
     *
     * The method validates that: - There are not going to be any dependent services without the service they depend on
     * (ie orphan services). - There are no services where a mutually exclusive service (counterpart) is selected at the
     * same time.
     *
     * @param proposedServiceCatalogueList This list represents the proposed state of the database after the change not
     *        the current state or the pay load. It must include all services currently active on the asset plus any new
     *        ones less any that are to be deleted.
     */
    protected void validateServiceRelationships(final List<ServiceCatalogueDto> proposedServiceCatalogueList)
            throws MastBusinessException, BadRequestException
    {
        if (proposedServiceCatalogueList == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "The list of services"));
        }

        final List<Long> proposedServiceCatalogueIds = proposedServiceCatalogueList.stream().map(serviceCatalogue -> serviceCatalogue.getId())
                .collect(Collectors.toList());

        final StringBuilder orphanServices = new StringBuilder();
        final StringBuilder conflictingServices = new StringBuilder();

        for (final ServiceCatalogueDto serviceCatalogue : proposedServiceCatalogueList)
        {
            final List<Long> prerequisiteServiceCatalogueIds = getPrerequisiteServceCatalogueIds(serviceCatalogue);
            final List<Long> exclusiveServiceCatalogueIds = getCounterpartServceCatalogueIds(serviceCatalogue);

            if (!proposedServiceCatalogueIds.containsAll(prerequisiteServiceCatalogueIds))
            {
                orphanServices.append(String.format(ExceptionMessagesUtils.DEPENDENCY_ERROR, "service catalogue", serviceCatalogue.getId(),
                        "all", prerequisiteServiceCatalogueIds.toString()));
            }
            if (exclusiveServiceCatalogueIds.stream().anyMatch(serviceCatalogueId -> proposedServiceCatalogueIds.contains(serviceCatalogueId)))
            { // This message will appear both ways around, but this is the only way to deal with the possibility of
              // lists.
                conflictingServices
                        .append(String.format(ExceptionMessagesUtils.MUTUALLY_EXCLUSIVE_ERROR, "service catalogue", serviceCatalogue.getId(),
                                "any", exclusiveServiceCatalogueIds.toString()));
            }
        }

        if (orphanServices.length() > 0 || conflictingServices.length() > 0)
        {
            LOG.debug(orphanServices.toString() + conflictingServices.toString());
            throw new MastBusinessException(orphanServices.toString() + conflictingServices.toString());
        }
    }

    /**
     * This method verifies that there are no duplicate service being added (either in the pay load or duplicates with
     * services already in the database). This is done separately for multiple and non-multiple services. For
     * non-multiple services the service has to be unique on the service catalogue id. For multiple services the service
     * has to be unique for the combination of service catalogue id and asset item id.
     *
     * @param proposedServiceCatalogueList
     * @param serviceList These lists represent the proposed state of the database after the change not the current
     *        state or the pay load. They must include all services currently active on the asset plus any new ones less
     *        any that are to be deleted.
     */
    protected void validateServiceDuplication(final List<ServiceCatalogueDto> proposedServiceCatalogueList,
            final List<ScheduledServiceDto> serviceList) throws MastBusinessException, BadRequestException
    {
        if (proposedServiceCatalogueList == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "The list of services"));
        }

        // list of all non-multiple services that appear more than once
        Set<Long> repeatedServiceCatalogues = proposedServiceCatalogueList.stream()
                .filter(serviceCatalogue -> serviceCatalogue.getMultipleIndicator() == null || !serviceCatalogue.getMultipleIndicator())
                .filter(serviceCatalogue -> Collections.frequency(proposedServiceCatalogueList, serviceCatalogue) > 1)
                .map(serviceCatalogue -> serviceCatalogue.getId())
                .filter(filterCompleteServices(serviceList))
                .collect(Collectors.toSet());

        if (!repeatedServiceCatalogues.isEmpty())
        {
            final String message = String.format(ExceptionMessagesUtils.DUPLICATE_ERROR, "non-multiple service", "service catalogue id",
                    repeatedServiceCatalogues.toString());
            LOG.debug(message);
            throw new MastBusinessException(message);
        }

        //
        // Multiple services are no longer in R2, but the validation for duplicates has been left for use later.
        //
        // list of all multiple services that appear more than once and have the same item associate to the same service
        // catalogue
        repeatedServiceCatalogues = proposedServiceCatalogueList.stream()
                .filter(serviceCatalogue -> serviceCatalogue.getMultipleIndicator())
                .filter(serviceCatalogue -> Collections.frequency(proposedServiceCatalogueList, serviceCatalogue) > 1)
                .map(serviceCatalogue -> serviceCatalogue.getId())
                .filter(hasDuplicateItems(serviceList))
                .collect(Collectors.toSet());

        if (!repeatedServiceCatalogues.isEmpty())
        {
            final String message = String.format(ExceptionMessagesUtils.DUPLICATE_ERROR, "multiple service", "service catalogue id and item id",
                    repeatedServiceCatalogues.toString());
            LOG.debug(message);
            throw new MastBusinessException(message);
        }
    }

    /**
     * This filters out complete services and returns whether there is more than one
     * non-closed service as part of the duplication validation.
     *
     * Method naming is admittedly confusing but this is due to being tied into the existing validation/filtering process
     */
    private Predicate<Long> filterCompleteServices(final List<ScheduledServiceDto> serviceList)
    {
        return serviceCatalogueId ->
        {
            // use the list of services to get all of the item ids to which a give service catalogue has been
            // associated.
            final List<Long> itemIds = serviceList == null ? new ArrayList<Long>() : serviceList.stream()
                    .filter(service -> service.getServiceCatalogueId().equals(serviceCatalogueId))
                    .filter(service -> !serviceIsComplete(service))
                    .map(ScheduledServiceDto::getId)
                    .collect(Collectors.toList());

            return itemIds.size() > 1;
        };
    }

    private boolean serviceIsComplete(final ScheduledServiceDto service)
    {
        final Long creditStatus = service.getServiceCreditStatus().getId();
        return ServiceCreditStatusType.COMPLETE.value().equals(creditStatus)
                || ServiceCreditStatusType.FINISHED.value().equals(creditStatus);
    }

    /**
     * Method to determine whether a given service catalogue has been applied to the same item more than once in a give
     * list of services.
     *
     * @return true if at least one item appears more than once for the same service catalogue id, false if each item
     *         appears only once.
     */
    private Predicate<Long> hasDuplicateItems(final List<ScheduledServiceDto> serviceList)
    {
        return serviceCatalogueId ->
        {
            // use the list of services to get all of the item ids to which a give service catalogue has been
            // associated.
            final List<Long> itemIds = serviceList == null ? new ArrayList<Long>() : serviceList.stream()
                    .filter(service -> service.getServiceCatalogueId().equals(serviceCatalogueId))
                    .filter(service -> !serviceIsComplete(service))
                    .map(service -> service.getAssetItem().getId())
                    .collect(Collectors.toList());

            final Set<Long> itemIdset = new HashSet<Long>(itemIds);

            // if the list and the set are not the same size then the list must contain duplicate ids
            return itemIds.size() != itemIdset.size();
        };
    }

    /**
     * Method to search the relationships for a service catalogue and return a list of prerequisites' ids. If the
     * relationships are null then there are no prerequisites so an empty list is returned (this is also ued to cover
     * the situation where the argument is null, though this does not really make sense)
     *
     * @param serviceCatalogue
     * @return List of ids of service catalogues that must be selected for this service catalogue to be selected.
     */
    private List<Long> getPrerequisiteServceCatalogueIds(final ServiceCatalogueDto serviceCatalogue)
    {
        return serviceCatalogue == null || serviceCatalogue.getRelationships() == null ? new ArrayList<Long>()
                : serviceCatalogue.getRelationships().stream()
                        .filter(relationship -> ServiceCatalogueRelationshipType.IS_DEPENDENT_ON.getValue()
                                .equals(relationship.getRelationshipType().getId()))
                        .filter(relationship -> !serviceCatalogue.getId().equals(relationship.getToServiceCatalogue().getId()))
                        .map(relationship -> relationship.getToServiceCatalogue().getId())
                        .collect(Collectors.toList());
    }

    /**
     * Method to get a list of counterpart ids for a give service catalogues by examining its relationships (this is
     * only expected to contain one value currently, but the implementation is easier for a list and it it for
     * flexible). The relationship is a peer-to-peer relationship so could occur either way around so the method checks
     * which id is the id of the current service and then chooses the other once it has the list of relationships of the
     * correct type. If there are no relationships or the service catalogue is null then an empty list is returned.
     */
    private List<Long> getCounterpartServceCatalogueIds(final ServiceCatalogueDto serviceCatalogue)
    {
        return serviceCatalogue == null || serviceCatalogue.getRelationships() == null ? new ArrayList<Long>()
                : serviceCatalogue.getRelationships().stream()
                        .filter(relationship -> ServiceCatalogueRelationshipType.IS_COUNTERPART_OF.getValue()
                                .equals(relationship.getRelationshipType().getId()))
                        .map(relationship ->
                        { // get the id that does not belong to the current service catalogue whichever way around the
                          // relationship may be.
                            final Long toId = relationship.getToServiceCatalogue().getId();
                            final Long fromId = relationship.getFromServiceCatalogue().getId();
                            return serviceCatalogue.getId().equals(toId) ? fromId : toId;
                        })
                        .collect(Collectors.toList());
    }

    /**
     * Method to validate the cycle periodicity for a list of services against the service catalogues. If the cycle
     * periodicty is not set as editable in the service catalogue then the value on the service must match that on the
     * service catalogue.
     */
    protected void validateCyclePeriodicty(final List<ScheduledServiceDto> fullServiceList, final List<ServiceCatalogueDto> serviceCatalogues)
            throws BadRequestException
    {
        final Set<ServiceCatalogueDto> serviceCatalogueSet = new HashSet<ServiceCatalogueDto>(serviceCatalogues);
        final Map<Long, ServiceCatalogueDto> serviceCatalogueMap = serviceCatalogueSet.stream()
                .collect(Collectors.toMap(ServiceCatalogueDto::getId, Function.identity()));

        final Set<Long> failedServiceCatalogueIds = fullServiceList.stream()
                .filter(cyclePeriodictyInvalid(serviceCatalogueMap))
                .map(service -> service.getServiceCatalogueId())
                .collect(Collectors.toSet());

        if (!failedServiceCatalogueIds.isEmpty())
        {
            final String errorMessage = String.format(ExceptionMessagesUtils.FIELD_IS_NOT_EDITABLE, "cycle periodicity", "services",
                    "service catalogue id(s)", failedServiceCatalogueIds.toString());
            LOG.debug(errorMessage);
            throw new BadRequestException(errorMessage);
        }
    }

    private Predicate<ScheduledServiceDto> cyclePeriodictyInvalid(final Map<Long, ServiceCatalogueDto> serviceCatalogueMap)
    {
        return service ->
        {
            final ServiceCatalogueDto serviceCatalogue = serviceCatalogueMap.get(service.getServiceCatalogueId());

            return !serviceCatalogue.getCyclePeriodicityEditable() && !serviceCatalogue.getCyclePeriodicity().equals(service.getCyclePeriodicity());
        };
    }
}
