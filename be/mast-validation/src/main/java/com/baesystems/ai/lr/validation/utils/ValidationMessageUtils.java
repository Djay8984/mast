package com.baesystems.ai.lr.validation.utils;

public final class ValidationMessageUtils
{
    private ValidationMessageUtils()
    {
        // hide constructor
    }

    public static final String FIELD_CANNOT_BE_NULL = "Field %s cannot be null.";
    public static final String INT_FIELD_TOO_SMALL = "Field %s cannot be less than %d. ";
    public static final String INT_FIELD_TOO_LARGE = "Field %s cannot be greater than %d. ";
    public static final String INT_FIELD_EQUAL = "Field %s must equal %d. ";
    public static final String LIST_FIELD_TOO_SMALL = "Field %s size cannot be less than %d. ";
    public static final String LIST_FIELD_TOO_LARGE = "Field %s size cannot be greater than %d. ";
    public static final String FLOAT_FIELD_TOO_SMALL = "Field %s cannot be less than %f. ";
    public static final String FLOAT_FIELD_TOO_LARGE = "Field %s cannot be greater than %f. ";
    public static final String DATE_FIELD_TOO_EARLY = "Field %s cannot be before %s. ";
    public static final String DATE_FIELD_TOO_LATE = "Field %s cannot be after %s. ";

    public static final String STRING_FIELD_EQUAL = "Field %s must equal %s. ";
    public static final String STRING_FIELD_DOES_NOT_MATCH = "Field %s does not match pattern %s. ";

    public static final String INCORRECT_VERSION = "Field %s does not match pattern %s. ";

    public static final String DUPLICATE_EXISTS = "A duplicate %s exists with %s %d. ";

    public static final String INCONSISTENT_ATTRIBUTE_VALUE = "This is inconsistent with attribute type %d.";

    public static final String FAR_WITH_OPEN_DEFECTS = "Open defects exist on the job with no action recorded. "
            + "Please ensure a repair is reported or that an actionable item or condition of class is raised before submitting the FAR.";
}
