package com.baesystems.ai.lr.validation.service;

import static com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.MAX_IMO_NUMBER_LENGTH;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.utils.ValidationMessageUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "AssetValidationService")
@Loggable(Loggable.DEBUG)
public class AssetValidationServiceImpl implements AssetValidationService
{
    private static final Logger LOG = LoggerFactory.getLogger(AssetValidationServiceImpl.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private IhsAssetDao ihsAssetDao;

    @Autowired
    private AssetAndCaseValidationUtils assetAndCaseValidationUtils;

    /**
     * Validates that a new Asset adheres to the following rules:<br>
     * <br>
     * 1. Asset must not be null and must be valid (according to @Valid annotations on the DTO).<br>
     * 2. Asset ID must be null.<br>
     * 3. Asset must have an IMO Number, or both a Builder and Yard Number.<br>
     * 4. If the Asset has an IMO Number, it must exist in IHS.<br>
     * 5. Asset must not already exist, meaning that there must not be another Asset with the same IMO Number or Builder
     * and Yard Number.<br>
     *
     * @param asset
     * @throws RecordNotFoundException
     * @throws MastBusinessException
     * @throws BadRequestException
     */
    public void validateCreateAsset(final AssetDto asset) throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        this.validationService.verifyBody(asset);
        this.validationService.verifyIdNull(asset.getId());

        Long imoNumber = null;
        String builder = null;
        String yardNumber = null;

        if (asset.getIhsAsset() != null)
        {
            imoNumber = asset.getIhsAsset().getId();
        }

        if (asset.getBuilder() != null)
        {
            builder = asset.getBuilder();
        }

        if (asset.getYardNumber() != null && !asset.getYardNumber().isEmpty())
        {
            yardNumber = asset.getYardNumber();
        }

        verifyImoBuilderAndYardCombination(imoNumber, builder, yardNumber);

        if (asset.getIhsAsset() != null)
        {
            verifyImoNumberExists(asset.getIhsAsset().getId());
        }

        verifyAssetDoesNotExist(imoNumber, builder, yardNumber);
    }

    public void verifyImoNumber(final String imoNumberValue) throws BadRequestException
    {
        final Long imoNumber = this.validationService.verifyNumberFormat(imoNumberValue);

        if (imoNumber == null || imoNumber.intValue() <= 0 || imoNumber.toString().length() > MAX_IMO_NUMBER_LENGTH)
        {
            final String message = String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, imoNumber, "IMO Number");
            LOG.debug(message);
            throw new BadRequestException(message);
        }
    }

    public void verifyImoNumberExists(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsAssetDto ihsAsset = this.ihsAssetDao.getIhsAsset(imoNumber);
        if (ihsAsset == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, imoNumber));
        }
    }

    /**
     * An Asset must have an IMO Number or both a Builder and Yard Number. Verifies that the given combination is valid.
     *
     * @param imoNumber
     * @param builder
     * @param yardNumber
     * @throws MastBusinessException
     */
    private void verifyImoBuilderAndYardCombination(final Long imoNumber, final String builder, final String yardNumber) throws MastBusinessException
    {
        if (imoNumber == null && (builder == null || yardNumber == null))
        {
            final String message = String.format(ExceptionMessagesUtils.VALIDATION_FAILED,
                    "An Asset must have an IMO Number or both a Builder and Yard Number");
            LOG.debug(message);
            throw new MastBusinessException(message);
        }
    }

    public void verifyAssetIdExists(final String inputAssetId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputAssetId);

        final Long assetId = this.validationService.verifyNumberFormat(inputAssetId);

        if (!this.assetDao.assetExists(assetId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, assetId));
        }
    }

    /**
     * Verifies than an Asset does not already exist, meaning that there must not be another Asset with the same IMO
     * Number or Builder and Yard Number.
     *
     * @param imoNumber
     * @param builder
     * @param yardNumber
     * @throws MastBusinessException
     */
    public void verifyAssetDoesNotExist(final Long imoNumber, final String builder, final String yardNumber) throws MastBusinessException
    {
        final Long existingAssetId = this.assetDao.findAssetIdByImoOrBuilderAndYard(imoNumber, builder, yardNumber);

        if (existingAssetId != null)
        {
            final String message = String.format(ValidationMessageUtils.DUPLICATE_EXISTS, "Asset", "ID", existingAssetId);
            LOG.debug(message);
            throw new MastBusinessException(message);
        }
    }

    public void validateAssetUpdate(final AssetLightDto asset) throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        this.assetAndCaseValidationUtils.validateLinks(asset);
    }
}
