package com.baesystems.ai.lr.validation.service;

import static java.lang.String.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.SurveyValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "SurveyValidationService")
@Loggable(Loggable.DEBUG)
public class SurveyValidationServiceImpl implements SurveyValidationService
{
    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private JobValidationService jobValidationService;

    @Override
    public void validateSurveyCreate(final Long jobId, final SurveyDto surveyDto)
            throws BadRequestException, RecordNotFoundException
    {
        this.commonCreateUpdateValidation(jobId, surveyDto);
        this.validationService.verifyIdNull(surveyDto.getId());
    }

    @Override
    public void validateSurveyUpdate(final Long jobId, final Long surveyId, final SurveyDto surveyDto)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(surveyId);
        this.commonCreateUpdateValidation(jobId, surveyDto);
        this.verifySurveyIdExists(surveyId);
        this.verifyJobHasSurvey(jobId, surveyId);

        final String message = format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Survey", surveyDto.getId(), surveyId);
        this.validationService.verifyField(surveyDto.getId(), surveyId, message);
    }

    private void commonCreateUpdateValidation(final Long jobId, final SurveyDto surveyDto)
            throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(jobId);
        this.validationService.verifyBody(surveyDto);
        this.jobValidationService.verifyJobIdExists(jobId.toString());

        Long bodyJobId = null;
        if (surveyDto.getJob() != null)
        {
            bodyJobId = surveyDto.getJob().getId();
        }

        final String message = format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", bodyJobId, jobId);
        this.validationService.verifyField(bodyJobId, jobId, message);
    }

    @Override
    public void verifySurveyIdExists(final Long surveyId) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(surveyId);

        if (!this.surveyDao.surveyExists(surveyId))
        {
            throw new RecordNotFoundException(format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, surveyId));
        }
    }

    @Override
    public void verifyJobHasSurvey(final Long jobId, final Long surveyId) throws RecordNotFoundException
    {
        if (!this.surveyDao.surveyExistsForJob(jobId, surveyId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    /**
     * Validates:</br>
     * - Job ID and Body are in a valid format.</br>
     * - Job ID exists (taken from the header).</br>
     * - The Job ID given in the header matches the Job ID from the Surveys in the list.</br>
     * - Each Survey ID, if not new, in the given list exists.</br>
     * - Each Survey ID, if not new, in the given list is linked to the given Job.</br>
     *
     * @param jobId
     * @param {@link SurveyListDto}
     * @throws BadRequestException
     * @throws RecordNotFoundException
     */
    @Override
    public void validateSurveyListForUpdate(final Long jobId, final SurveyListDto surveys) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(jobId);
        this.validationService.verifyBody(surveys);
        this.jobValidationService.verifyJobIdExists(jobId.toString());

        for (final SurveyDto survey : surveys.getSurveys())
        {
            final Long bodyJobId = survey.getJob().getId();
            this.validationService.verifyField(bodyJobId, jobId,
                    format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", bodyJobId, jobId));

            if (survey.getId() != null)
            {
                this.verifySurveyIdExists(survey.getId());
                this.verifyJobHasSurvey(jobId, survey.getId());
            }

            this.validateCompletion(survey);
        }
    }

    @Override
    public void validateCompletion(final SurveyDto survey) throws BadRequestException
    {
        if (survey.getSurveyStatus() != null && ServiceCreditStatusType.COMPLETE.value().equals(survey.getSurveyStatus().getId())
                && survey.getDateOfCrediting() == null)
        {
            throw new BadRequestException(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "dateOfCrediting"));
        }
    }
}
