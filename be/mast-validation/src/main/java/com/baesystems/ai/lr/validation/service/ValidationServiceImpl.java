package com.baesystems.ai.lr.validation.service;

import static com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.MAX_ID_LENGTH;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.dto.base.IdDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ValidationService")
@Loggable(Loggable.DEBUG)
public class ValidationServiceImpl implements ValidationService
{
    @Autowired
    private Validator validator;

    @Override
    public void verifyId(final Object inputId) throws BadRequestException
    {
        if (inputId == null)
        {
            verifyId((Long) null);
        }
        else if (inputId instanceof Number)
        {
            verifyId(((Number) inputId).longValue());
        }
        else
        {
            final Long id = verifyNumberFormat(inputId.toString());
            verifyId(id);
        }
    }

    @Override
    public void verifyNullableId(final Object inputId) throws BadRequestException
    {
        if (inputId != null)
        {
            if (inputId instanceof Number)
            {
                verifyNullableId(((Number) inputId).longValue());
            }
            else
            {
                final Long id = verifyNumberFormat(inputId.toString());
                verifyNullableId(id);
            }
        }

    }

    private void verifyNullableId(final Long id) throws BadRequestException
    {
        if (id.intValue() <= 0 || id.toString().length() > MAX_ID_LENGTH)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, id, "ID"));
        }
    }

    private void verifyId(final Long id) throws BadRequestException
    {
        if (id == null || id.intValue() <= 0 || id.toString().length() > MAX_ID_LENGTH)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, id, "ID"));
        }
    }

    @Override
    public void verifyIdNull(final Long id) throws BadRequestException
    {
        if (id != null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.ID_NOT_NULL, id));
        }
    }

    @Override
    public void verifyBody(final Object entity) throws BadRequestException
    {
        if (entity == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.BODY_NOT_NULL);
        }

        if (entity instanceof Dto)
        {
            validate(entity);
        }
    }

    @Override
    public void verifyQueryParamNotNull(final Object entity, final String queryParameterName) throws BadRequestException
    {
        if (entity == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.QUERY_PARAM_NOT_NULL, queryParameterName));
        }
    }

    /**
     * Verifies if an input value is a number.
     *
     * @param value String representation of value.
     * @return Long value.
     * @throws BadRequestException
     */
    @Override
    public Long verifyNumberFormat(final String value) throws BadRequestException
    {
        Long numberValue = null;
        try
        {
            numberValue = Long.parseLong(value);
        }
        catch (final NumberFormatException exception)
        {
            final String message = String.format(ExceptionMessagesUtils.DETAILED_BINDING_ERROR_MSG, value, "number");
            throw new BadRequestException(message, exception);
        }
        return numberValue;
    }

    @Override
    public void validate(final Object dto) throws BadRequestException
    {
        final Set<ConstraintViolation<Object>> results = this.validator.validate(dto);

        if (results != null && !results.isEmpty())
        {
            final StringBuilder message = new StringBuilder();
            for (final ConstraintViolation<?> validation : results)
            {
                if (message.length() > 0)
                {
                    message.append(", ");
                }

                message.append(validation.getPropertyPath());
                message.append(' ');
                message.append(validation.getMessage());
            }

            throw new BadRequestException(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, message.toString()));
        }
    }

    @Override
    public void addIdToIdList(final IdDto value, final List<Long> idList) throws BadRequestException
    {
        if (value != null)
        {
            final Long valueId = value.getId();

            verifyId(valueId);

            idList.add(valueId);
        }
    }

    @Override
    public void verifyField(final Object field, final Object headerfield, final String message) throws BadRequestException
    {
        if (field != null && !field.equals(headerfield) || field == null && headerfield != null)
        {
            throw new BadRequestException(message);
        }
    }
}
