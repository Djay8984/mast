package com.baesystems.ai.lr.validation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.enums.CaseMilestoneStatusType;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.MilestoneDueDateReferenceType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.service.validation.CaseMilestoneValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CaseMilestoneValidationService")
@Loggable(Loggable.DEBUG)
public class CaseMilestoneValidationServiceImpl implements CaseMilestoneValidationService
{
    private static final String CASE_MILESTONE = "case milestone";

    @Autowired
    private ValidationService validationService;

    @Autowired
    private CaseDao caseDao;

    @Autowired
    private CaseReferenceDataDao caseReferenceDataDao;

    @Autowired
    private CaseMilestoneDao caseMilestoneDao;

    /**
     * Validate a list of case milestones to update.
     *
     * Verifies that the case is in a status that allows the milestones to be updated and then validates the milestones
     * one by one and checks that each one only appears once in the list.
     */
    public void validateCaseMilestoneListUpdate(final Long caseId, final CaseMilestoneListDto caseMilestoneList)
            throws BadRequestException, MastBusinessException
    {
        this.validationService.verifyBody(caseMilestoneList);

        if (!CaseStatusType.getCaseStatusForUpdateCaseMilestone().contains(this.caseDao.getCaseStatusId(caseId)))
        { // if the case is in the wrong status
            throw new BadRequestException(String.format(ExceptionMessagesUtils.INCORRECT_STATUS, "case milestones", "the case"));
        }

        final List<Long> processed = new ArrayList<Long>();
        final List<Long> validCaseMilestonesForCase = this.caseMilestoneDao.getCaseMilestoneIdsForCase(caseId);
        final Map<Long, Boolean> milestoneScopeMap = new HashMap<Long, Boolean>();
        final Map<Long, Long> milestoneStatusMap = new HashMap<Long, Long>();

        for (final CaseMilestoneDto caseMilestone : caseMilestoneList.getCaseMilestoneList())
        { // creates a map, by milestone (not case milestone) Id of what the scope and status will be if the update is
          // successful. This allows for parent and child to be updated in one transaction.
            milestoneScopeMap.put(caseMilestone.getMilestone().getId(), caseMilestone.getInScope());
            milestoneStatusMap.put(caseMilestone.getMilestone().getId(), caseMilestone.getMilestoneStatus().getId());
        }

        for (final CaseMilestoneDto caseMilestone : caseMilestoneList.getCaseMilestoneList())
        { // loop through and validate the whole list
            final Long id = this.validateCaseMilestoneUpdate(caseId, caseMilestone, validCaseMilestonesForCase, milestoneScopeMap,
                    milestoneStatusMap);

            if (processed.contains(id))
            { // If this milestone has already appeared in the list throw and exception
                throw new MastBusinessException(ExceptionMessagesUtils.MULTIPLE_UPDATES);
            }
            else
            {
                processed.add(id);
            }
        }
    }

    /**
     * Method to validate the update of a single milestone from the list of milestones.
     *
     * The method validates that: the milestone is nod already complete and that it is valid for the case in question
     * and then compares what has been entered with the values held in the database to determine what updates have been
     * done and then calls the appropriate validation. If the case or milestone are update the method throws an
     * exception.
     */
    private Long validateCaseMilestoneUpdate(final Long caseId, final CaseMilestoneDto caseMilestone, final List<Long> validCaseMilestonesForCase,
            final Map<Long, Boolean> milestoneScopeMap, final Map<Long, Long> milestoneStatusMap)
                    throws BadRequestException
    {
        final Long caseMilestoneId = caseMilestone.getId();

        if (!validCaseMilestonesForCase.contains(caseMilestoneId))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Case milestone",
                    caseMilestoneId, "case", caseId));
        }

        final CaseMilestoneDto originalCaseMilestone = this.caseMilestoneDao.getCaseMilestone(caseMilestoneId);

        if (CaseMilestoneStatusType.COMPLETE.getValue().equals(originalCaseMilestone.getMilestoneStatus().getId()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, caseMilestoneId, "complete"));
        }

        if (!caseMilestone.getMilestoneStatus().getId().equals(originalCaseMilestone.getMilestoneStatus().getId()))
        {
            this.validateMarkCaseMilestoneAsComplete(caseMilestone, originalCaseMilestone, milestoneStatusMap);
        }

        if (!caseMilestone.getInScope().equals(originalCaseMilestone.getInScope()))
        {
            this.validateChangeScopeOfCaseMilestone(caseMilestone, originalCaseMilestone, milestoneScopeMap);
        }

        if (caseMilestone.getDueDate() != null && !caseMilestone.getDueDate().equals(originalCaseMilestone.getDueDate()))
        {
            validateChangeDueDate(originalCaseMilestone);
        }

        if (!caseMilestone.getaCase().equals(originalCaseMilestone.getaCase())
                || !caseMilestone.getMilestone().equals(originalCaseMilestone.getMilestone()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_REF_DATA, originalCaseMilestone.getId()));
        }

        return caseMilestoneId;
    }

    /**
     * Method to validate the change of scope of a case milestone.
     *
     * The method verifies that the milestone is not being marked as complete in the same transaction and that the
     * milestone is not mandatory. I then goes on to validate the scope against the scope of its parent or child
     * milestones depending on the type of update. If the milestone is being moved into scope then the parent must also
     * be in scope by the end of the transaction. If the milestone is being move out of scope then all of it children
     * must be out of scope by the end of the transaction.
     */
    private void validateChangeScopeOfCaseMilestone(final CaseMilestoneDto caseMilestone, final CaseMilestoneDto originalCaseMilestone,
            final Map<Long, Boolean> milestoneScopeMap) throws BadRequestException
    {
        if (CaseMilestoneStatusType.COMPLETE.getValue().equals(caseMilestone.getMilestoneStatus().getId()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, caseMilestone.getId(), "complete"));
        }

        if (this.caseReferenceDataDao.isMilestoneMandatory(originalCaseMilestone.getMilestone().getId()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, caseMilestone.getId(), "mandatory"));
        }

        if (caseMilestone.getInScope())
        {
            final List<Long> parentMilestoneIds = this.caseReferenceDataDao.getParentMilestoneIds(originalCaseMilestone.getMilestone().getId());

            if (parentMilestoneIds != null && !parentMilestoneIds.isEmpty())
            {
                validateAgainstParentScope(caseMilestone, originalCaseMilestone.getaCase().getId(), milestoneScopeMap, parentMilestoneIds);
            }
        }
        else
        {
            final List<Long> childMilestoneIds = this.caseReferenceDataDao.getChildMilestoneIds(originalCaseMilestone.getMilestone().getId());

            if (childMilestoneIds != null)
            {
                validateAgainstChildScopes(caseMilestone, originalCaseMilestone.getaCase().getId(), milestoneScopeMap, childMilestoneIds);
            }
        }
    }

    /**
     * Loops through all child items and makes sure they are going to be in the same status as the milestone currently
     * being verified. If the child is in the list of milestones to be updated then this value is taken, if not then the
     * value is taken form the database.
     */
    private void validateAgainstChildScopes(final CaseMilestoneDto caseMilestone, final Long caseId,
            final Map<Long, Boolean> milestoneScopeMap, final List<Long> childMilestoneIds) throws BadRequestException
    {
        for (final Long childMilestoneId : childMilestoneIds)
        {
            final Boolean childScope = milestoneScopeMap.get(childMilestoneId) == null
                    ? this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(childMilestoneId, caseId)
                    : milestoneScopeMap.get(childMilestoneId);

            if (!childScope.equals(caseMilestone.getInScope()))
            {
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, caseMilestone.getId(), "children"));
            }
        }
    }

    /**
     * Makes sure the parent milestone is going to be in the same status as the milestone currently being verified. If
     * the parent is in the list of milestones to be updated then this value is taken, if not then the value is taken
     * form the database.
     */
    private void validateAgainstParentScope(final CaseMilestoneDto caseMilestone, final Long caseId,
            final Map<Long, Boolean> milestoneScopeMap, final List<Long> parentMilestoneIds) throws BadRequestException
    {
        for (final Long parentMilestoneId : parentMilestoneIds)
        {
            final Boolean parentScope = milestoneScopeMap.get(parentMilestoneId) == null
                    ? this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(parentMilestoneId, caseId)
                    : milestoneScopeMap.get(parentMilestoneId);

            if (!parentScope.equals(caseMilestone.getInScope()))
            {
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, caseMilestone.getId(), "parent"));
            }
        }
    }

    /**
     * Method to validate marking a milestone as complete.
     *
     * This method verifies that the due and completed dates are not null and that the parent milestone will be complete
     * by the end of the transaction. If the parent is in the list of milestones to be updated then this value is taken,
     * if not then the value is taken form the database.
     */
    private void validateMarkCaseMilestoneAsComplete(final CaseMilestoneDto caseMilestone, final CaseMilestoneDto originalCaseMilestone,
            final Map<Long, Long> milestoneStatusMap)
                    throws BadRequestException
    {
        if (caseMilestone.getDueDate() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_MARKED_COMPLETE, CASE_MILESTONE,
                    originalCaseMilestone.getId(), "dueDate", "null"));
        }

        if (caseMilestone.getCompletionDate() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_MARKED_COMPLETE, CASE_MILESTONE,
                    originalCaseMilestone.getId(), "completionDate", "null"));
        }

        final List<Long> parentMilestoneIds = this.caseReferenceDataDao.getParentMilestoneIds(originalCaseMilestone.getMilestone().getId());

        if (parentMilestoneIds != null)
        {
            for (final Long parentMilestoneId : parentMilestoneIds)
            {
                final Long parentStatus = milestoneStatusMap.get(parentMilestoneId) == null
                        ? this.caseMilestoneDao.getStatusIdMilestoneIdAndCaseId(parentMilestoneId, originalCaseMilestone.getaCase().getId())
                        : milestoneStatusMap.get(parentMilestoneId);

                if (!parentStatus.equals(caseMilestone.getMilestoneStatus().getId()))
                {
                    throw new BadRequestException(
                            String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, caseMilestone.getId(), "parent"));
                }
            }
        }
    }

    /**
     * Method to validate the updating the due date. This method makes sure that the date is marked as manual entry in
     * the reference data.
     */
    private void validateChangeDueDate(final CaseMilestoneDto originalCaseMilestone) throws BadRequestException
    {
        if (!MilestoneDueDateReferenceType.MANUAL.getValue()
                .equals(this.caseReferenceDataDao.getMilestoneDueDateReferenceId(originalCaseMilestone.getMilestone().getId())))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_REF_DATA, originalCaseMilestone.getId()));
        }
    }
}
