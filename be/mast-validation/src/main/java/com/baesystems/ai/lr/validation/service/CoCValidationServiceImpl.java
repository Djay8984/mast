package com.baesystems.ai.lr.validation.service;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.CoCValidationService;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DtoUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.exception.ComparisonException;
import com.jcabi.aspects.Loggable;

@Service("CoCValidationService")
@SuppressWarnings("PMD.GodClass")
@Loggable(Loggable.DEBUG)
public class CoCValidationServiceImpl implements CoCValidationService
{
    private static final String NULL_DTO = "CocDto is null";
    private static final String NULL_IMPOSED_DATE = "Imposed date is null";
    private static final String NULL_DUE_DATE = "Due date is null";
    private static final String HISTORICAL_IMPOSED_DATE = "Imposed date is in the past: %s.";
    private static final String DUE_BEFORE_IMPOSED_DATE = "Due date is before imposed date: %s";
    private static final String MODIFYING_IMPOSED_DATE = "Attempting to modify imposed date. Original: %s Modified: %s";
    private static final String HISTORICAL_EDITED_DUE_DATE = "Edited due date is in the past: %s";

    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();
    private static final Long EXPECTED_COC_COUNT = 1L;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ItemValidationService itemValidationService;

    @Autowired
    private CoCDao coCDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private DateHelper dateHelper;

    @Override
    public void verifyAssetHasCoC(final Long assetId, final Long cocId)
            throws RecordNotFoundException
    {
        if (!this.coCDao.coCExistsForAsset(assetId, cocId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyJobHasWIPCoC(final Long jobId, final Long cocId)
            throws RecordNotFoundException
    {
        if (!this.wipCoCDao.coCExistsForJob(jobId, cocId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyOptionalParentIdExistsAndIsOfCorrectType(final Long parentCocId) throws RecordNotFoundException
    {
        if (parentCocId != null)
        {
            final CoCDto parentCoc = coCDao.getCoC(parentCocId);
            if (parentCoc == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.PARENT_CODICIL_INVALID, parentCocId));
            }
        }
    }

    @Override
    public void verifyImposedDueDate(final CoCDto cocDto) throws BadRequestException
    {
        if (cocDto == null)
        {
            throw new BadRequestException(NULL_DTO);
        }

        if (cocDto.getImposedDate() == null)
        {
            throw new BadRequestException(NULL_IMPOSED_DATE);
        }

        if (cocDto.getDueDate() == null)
        {
            throw new BadRequestException(NULL_DUE_DATE);
        }

        if (!dateHelper.isTodayOrAfter(cocDto.getImposedDate()))
        {
            throw new BadRequestException(String.format(HISTORICAL_IMPOSED_DATE, dateFormat.format(cocDto.getImposedDate())));
        }

        if (DateUtils.truncatedCompareTo(cocDto.getDueDate(), cocDto.getImposedDate(), Calendar.DATE) < 0)
        {
            throw new BadRequestException(String.format(DUE_BEFORE_IMPOSED_DATE, dateFormat.format(cocDto.getDueDate())));
        }

    }

    /**
     * When a CoC has been edited verify that: 1. Imposed date has not been modified 2. Due date is greater than imposed
     * 3. Due is greater than the present See 9.51 & 9.53
     *
     * @param editedCocDto
     * @throws BadRequestException
     */
    @Override
    public void verifyEditedImposedDueDate(final CoCDto editedCocDto) throws BadRequestException
    {

        final CoCDto originalCoCDto = coCDao.getCoC(editedCocDto.getId());
        final Date originalImposed = originalCoCDto.getImposedDate();
        final Date editedImposed = editedCocDto.getImposedDate();

        if (editedImposed == null || DateUtils.truncatedCompareTo(originalImposed, editedImposed, Calendar.DATE) != 0)
        {
            throw new BadRequestException(String.format(MODIFYING_IMPOSED_DATE, dateFormat.format(originalImposed),
                    dateFormat.format(editedCocDto.getImposedDate())));
        }

        final Date editedDue = editedCocDto.getDueDate();
        if (DateUtils.truncatedCompareTo(editedDue, editedImposed, Calendar.DATE) < 0)
        {
            throw new BadRequestException(String.format(DUE_BEFORE_IMPOSED_DATE, dateFormat.format(editedDue)));
        }

        final Date originalDue = originalCoCDto.getDueDate();
        if (DateUtils.truncatedCompareTo(editedDue, originalDue, Calendar.DATE) != 0
                && !dateHelper.isTodayOrAfter(editedDue))
        {
            throw new BadRequestException(String.format(HISTORICAL_EDITED_DUE_DATE, dateFormat.format(editedCocDto.getDueDate())));
        }
    }

    // TODO This is a refactoring candidate wrt the ActionableItem validation, using a new common super Codicil DTO.
    @Override
    public void verifyTemplatedFieldsCorrect(final CoCDto cocDto) throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        if (cocDto == null)
        {
            throw new BadRequestException(NULL_DTO);
        }

        if (cocDto.getTemplate() != null && cocDto.getTemplate().getId() != null)
        {
            final CodicilTemplateDto template = this.codicilReferenceDataDao.getCodicilTemplate(cocDto.getTemplate().getId());

            if (template == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, cocDto.getTemplate().getId()));
            }

            final boolean valid = cocDto.getTitle().equals(template.getTitle())
                    && cocDto.getDescription().equals(template.getDescription())
                    && (cocDto.getSurveyorGuidance() == null && template.getSurveyorGuidance() == null
                            || cocDto.getSurveyorGuidance() != null
                                    && cocDto.getSurveyorGuidance().equals(template.getSurveyorGuidance()))
                    && cocDto.getConfidentialityType().equals(template.getConfidentialityType())
                    && (cocDto.getEditableBySurveyor() == null && template.getEditableBySurveyor() == null
                            || cocDto.getEditableBySurveyor() != null
                                    && cocDto.getEditableBySurveyor().equals(template.getEditableBySurveyor()))
                    && cocDto.getCategory().equals(template.getCategory());

            if (!valid)
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.DOES_NOT_MATCH_TEMPLATE, "CoC"));
            }

            if (cocDto.getAssetItem() != null && template.getItemType() != null)
            {
                this.itemValidationService.verifyItemIsCorrectType(cocDto.getAssetItem().getId(), template.getItemType().getId());
            }
        }
    }

    @Override
    public void validateDefectIdMatches(final String defectIdFromUri, final CoCDto coc) throws BadRequestException
    {
        String defectIdFromCoc = null;
        final LinkResource defect = coc.getDefect();
        if (defect != null)
        {
            defectIdFromCoc = defect.getId().toString();
        }
        this.validationService.verifyField(defectIdFromCoc, defectIdFromUri,
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "WIP defect", defectIdFromCoc, defectIdFromUri));
    }

    @Override
    public void verifyWIPDefectHasWIPCoC(final Long defectId, final Long cocId) throws RecordNotFoundException
    {
        final List<WIPCoCDO> cocs = this.wipCoCDao.getCoCsForDefect(defectId);
        if (cocs == null || cocs.isEmpty())
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }

        final Long count = cocs.stream()
                .filter(coc -> coc.getId().equals(cocId))
                .count();

        if (!EXPECTED_COC_COUNT.equals(count))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyCoCImmutableFields(final CoCDto updatedCocDto) throws BadRequestException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = this.coCDao.getCoC(updatedCocDto.getId());
        if (originalCoCDto == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }

        try
        {
            final String[] ignoredFields = {"id", "dueDate", "description", "updatedBy", "updatedDate", "deleted", "status", "dueStatus",
                                            "stalenessHash"};
            DtoUtils.compareFields(updatedCocDto, originalCoCDto, false, ignoredFields);
        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ComparisonException exception)
        {
            throw new BadRequestException(exception);
        }

    }

    @Override
    public void verifyWIPCoCImmutableFields(final CoCDto updatedCocDto) throws BadRequestException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = this.wipCoCDao.getCoC(updatedCocDto.getId());
        if (originalCoCDto == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }

        // Story 10.47 AC 2 & 3
        if (originalCoCDto.getParent() != null)
        {
            try
            {
                final String[] ignoredFields = {"id", "dueDate", "description", "updatedBy", "updatedDate", "deleted", "status", "dueStatus"};
                DtoUtils.compareFields(updatedCocDto, originalCoCDto, false, ignoredFields);
            }
            catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ComparisonException exception)
            {
                throw new BadRequestException(exception);
            }
        }
    }

    /**
     * The WIP DOs needs Job Scope Confirmed, whereas the non-wip versions do not (hence this can't be handled by
     * NotNull annotation on the DTO as they use the same DTO)
     *
     * @param cocDto
     * @throws BadRequestException
     */
    @Override
    public void validateWIPJobScopeConfirmed(final CoCDto cocDto) throws BadRequestException
    {
        if (cocDto.getJobScopeConfirmed() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));
        }
    }
}
