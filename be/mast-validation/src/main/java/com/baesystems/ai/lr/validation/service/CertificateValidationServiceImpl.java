package com.baesystems.ai.lr.validation.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dao.references.jobs.CertificateReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.CertificateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CertificateValidationService")
@Loggable(Loggable.DEBUG)
public class CertificateValidationServiceImpl implements CertificateValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private CertificateReferenceDataDao certificateReferenceDataDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    private WIPCertificateDao wipCertificateDao;

    /**
     * Validates job and input body for saving a certificate.
     *
     * Validates the body that the id is null the job
     */
    @Override
    public void validateCreateCertificate(final CertificateDto certificate, final String intputJobId)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyBody(certificate);
        this.validationService.verifyIdNull(certificate.getId());

        validateJob(certificate, intputJobId);
        validateActionStatusAndType(certificate);
        validateIssueDateCreate(certificate);
    }

    /**
     * Validates job, certificate and input body for updating a certificate.
     *
     * Validates the body the job that the certificate exist and fields match the url
     */
    @Override
    public void validateUpdateCertificate(final CertificateDto certificate, final String intputJobId, final String inputCertificateId)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyBody(certificate);

        validateJob(certificate, intputJobId);
        validateCertificate(certificate, inputCertificateId);
        validateActionStatusAndType(certificate);
        validateIssueDateUpdate(certificate, inputCertificateId);
    }

    /**
     * Validates a job
     *
     * Validates the job exists that the id given in the certificate body (if present) matches the id in the url.
     */
    private void validateJob(final CertificateDto certificate, final String intputJobId) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(intputJobId);

        final Long jobId = this.validationService.verifyNumberFormat(intputJobId);

        if (this.jobDao.getJob(jobId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, jobId));
        }

        if (certificate.getJob() != null && !jobId.equals(certificate.getJob().getId()))
        {
            throw new BadRequestException(ExceptionMessagesUtils.FIELD_MISMATCH);
        }
    }

    /**
     * Validates a job
     *
     * Validates the certificate exists that the id given in the certificate body (if present) matches the id in the
     * url.
     */
    private void validateCertificate(final CertificateDto certificate, final String inputCertificateId)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(inputCertificateId);

        this.validationService.verifyId(certificate.getId());

        final Long certificateId = this.validationService.verifyNumberFormat(inputCertificateId);

        if (this.wipCertificateDao.getCertificate(certificateId) == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, certificateId));
        }

        if (!certificateId.equals(certificate.getId()))
        {
            throw new BadRequestException(ExceptionMessagesUtils.FIELD_MISMATCH);
        }
    }

    /**
     * Validates the certificate action status and type (if present) to make sure they are valid choices (ie they are in
     * the database).
     *
     * Not that a null check has already been performed on the sub ids in the body verification.
     */
    private void validateActionStatusAndType(final CertificateDto certificate) throws RecordNotFoundException
    {
        final LinkResource certificateAction = certificate.getCertificateAction();

        if (certificateAction != null && this.certificateReferenceDataDao.getCertificateAction(certificateAction.getId()) == null)
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, certificateAction.getId()));
        }

        final LinkResource certificateStatus = certificate.getCertificateStatus();

        if (certificateStatus != null && this.certificateReferenceDataDao.getCertificateStatus(certificateStatus.getId()) == null)
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, certificateStatus.getId()));
        }

        // not null in dto so no need to check
        final LinkResource certificateType = certificate.getCertificateType();

        if (this.certificateReferenceDataDao.getCertificateType(certificateType.getId()) == null)
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, certificateType.getId()));
        }
    }

    /**
     * Validates the issue date can only be changed if status is issued.
     */
    private void validateIssueDateCreate(final CertificateDto certificate) throws RecordNotFoundException, BadRequestException
    {
        if (certificate.getCertificateAction() != null)
        {
            final CertificateActionDto certificateAction = this.certificateReferenceDataDao.getCertificateAction(certificate.getCertificateAction()
                    .getId());

            if (certificate.getIssueDate() != null && (certificateAction.getIssueDateMutable() == null || !certificateAction.getIssueDateMutable()))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.FIELD_CANNOT_BE_UPDATED, "Issue date"));
            }
        }
    }

    /**
     * Validates the issue date can only be changed if status is issued.
     */
    private void validateIssueDateUpdate(final CertificateDto certificate, final String inputCertificateId)
            throws RecordNotFoundException, BadRequestException
    {
        final Long certificateId = this.validationService.verifyNumberFormat(inputCertificateId);

        final Date issueDate = this.wipCertificateDao.getCertificateIssueDate(certificateId);

        if (certificate.getCertificateAction() != null)
        {
            final CertificateActionDto certificateAction = this.certificateReferenceDataDao.getCertificateAction(certificate.getCertificateAction()
                    .getId());

            if (dateChangeViolation(certificate, issueDate, certificateAction))
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.FIELD_CANNOT_BE_UPDATED, "Issue date"));
            }
        }
    }

    private boolean dateChangeViolation(final CertificateDto certificate, final Date issueDate, final CertificateActionDto certificateAction)
    {
        return (certificate.getIssueDate() == null && issueDate != null
                || certificate.getIssueDate() != null && issueDate == null
                || certificate.getIssueDate() != null && !certificate.getIssueDate().equals(issueDate))
                && (certificateAction == null || certificateAction.getIssueDateMutable() == null || !certificateAction.getIssueDateMutable());
    }
}
