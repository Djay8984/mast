package com.baesystems.ai.lr.validation.utils;

public final class ValidationConstantUtils
{
    private ValidationConstantUtils()
    {
    }

    public static final Long MAX_ID = 9999999999L;
    public static final Long MAX_IMO = 9999999L;
    public static final int MAX_ID_LENGTH = 10;
    public static final int MAX_IMO_NUMBER_LENGTH = 7;

    public static final class STRINGTYPE
    {
        public static final String ALPHANUMERIC = "ALPHANUMERIC";
        public static final String NUMERIC = "NUMERIC";
        public static final String STRICTALPHA = "STRICTALPHA";
        public static final String ALPHA = "ALPHA";
        public static final String BOOLEAN = "BOOLEAN";
        public static final String DATE = "DATE";
    }

    public static final class STRINGREGEX
    {
        public static final String ALPHANUMERIC = "^[a-zA-Z0-9 ,.\\-\\/\\(\\)\\&]*$";
        public static final String NUMERIC = "^[0-9 .,\\-]*$";
        public static final String STRICTALPHA = "^[a-zA-Z]*$";
        public static final String ALPHA = "^[a-zA-Z ,.\\-\\/\\(\\)\\&]*$";
    }

    public static final String TRUE = "true";
    public static final String FALSE = "false";
}
