package com.baesystems.ai.lr.validation.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.enums.ServiceType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ServiceDateValidationService;
import com.baesystems.ai.lr.service.validation.ServiceValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ServiceValidationService")
@Loggable(Loggable.DEBUG)
public class ServiceValidationServiceImpl implements ServiceValidationService
{
    private static final Logger LOG = LoggerFactory.getLogger(ServiceValidationServiceImpl.class);

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ServiceValidationHelper serviceValidationHelper;

    @Autowired
    private ServiceDateValidationService dateValidator;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private AssetValidationService assetValidationService;

    @Autowired
    private DateHelper dateHelper;

    @Autowired
    private ServiceReferenceDataValidation serviceReferenceDataValidation;

    private static final int THREE_MONTHS = 3;
    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();

    /**
     * Validates if:
     *
     * - inputAssetId is a valid id.
     *
     * - There is a valid asset for inputAssetId.
     *
     * - There are allowed product catalogues for inputAssetId.
     *
     * - selectedProductList is not null.
     *
     * - Make sure all requested products are in the rule set of the asset: all items in selectedProductList are allowed
     * on asset for inputAssetId.
     *
     * - Ensure that any products to be deleted do not have services still attached to them.
     *
     * @throws MastBusinessException
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateProductList(final String inputAssetId, final ProductListDto selectedProductList)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.validationService.verifyId(inputAssetId);

        if (selectedProductList == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.BODY_NOT_NULL);
        }

        final Long assetId = this.validationService.verifyNumberFormat(inputAssetId);

        // Verify that the asset exists first to avoid the wrong error being displayed if it doesn't.
        if (!this.assetDao.assetExists(assetId))
        {
            final String message = String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, assetId);
            LOG.debug(message);
            throw new RecordNotFoundException(message);
        }

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final List<ProductCatalogueDto> allowedProductCatalogues = this.serviceReferenceDataDao.getProductCataloguesForAsset(assetId, productTypes);

        if (allowedProductCatalogues == null)
        {
            final String message = String.format(ExceptionMessagesUtils.SUBITEM_NOT_FOUND_ERROR_MSG, "Product catalogues", "asset", assetId);
            LOG.debug(message);
            throw new BadRequestException(message);
        }

        // Get all allowed catalogue ids
        final Set<Long> allowedCatalogueIds = allowedProductCatalogues
                .stream()
                .map(allowedCat -> allowedCat.getId())
                .collect(Collectors.toSet());

        // Check whether any of the passed products contains a catalogue id not in the allowed set
        final List<Long> invalidCatalogueIds = selectedProductList.getProductList().stream()
                .map(product -> product.getProductCatalogueId())
                .distinct()
                .filter(catId -> !allowedCatalogueIds.contains(catId))
                .collect(Collectors.toList());

        // make sure all the products requested are in the rule set of the asset.
        if (!invalidCatalogueIds.isEmpty())
        {
            final String message = String.format(ExceptionMessagesUtils.INVALID_LIST_ERROR_MSG_WITH_FAILURES, "Product catalogues",
                    invalidCatalogueIds.toString());
            LOG.debug(message);
            throw new BadRequestException(message);
        }

        checkAnyDeletedProductsDoNotHaveServices(assetId, selectedProductList);
    }

    /**
     * Ensure that any products to be deleted do not have services still attached to them.
     *
     * @param assetId
     * @param selectedProductList
     * @throws MastBusinessException
     */
    private void checkAnyDeletedProductsDoNotHaveServices(final Long assetId, final ProductListDto selectedProductList)
            throws MastBusinessException
    {
        final List<ProductDto> productsForAsset = this.productDao.getProducts(assetId);

        if (productsForAsset != null)
        {
            final List<Long> passedProductIds = selectedProductList.getProductList().stream().map(dto -> dto.getId())
                    .collect(Collectors.toList());

            // For those unfamiliar with streams, this next section does the following...
            //
            // - Converts the list of product dtos into a stream that can have functions applied to it.
            // - Keeps any product dto that has not be passed in from selectedProductList
            // - Converts the dto stream into a stream of longs that correspond to the product dto ids
            // - Keep any id where hasServices evaluates to true.
            // - Collect resulting id stream up into a list of Longs
            //

            final List<Long> productIdsThatHaveServices = productsForAsset.stream()
                    .filter(product -> !passedProductIds.contains(product.getId()))
                    .map(product -> product.getId())
                    .filter(hasServices())
                    .collect(Collectors.toList());

            if (!productIdsThatHaveServices.isEmpty())
            {
                // One or more products still have services attached
                throw new MastBusinessException(
                        String.format(ExceptionMessagesUtils.PRODUCT_STILL_HAS_ATTACHED_SERVICES, productIdsThatHaveServices.toString()));
            }
        }
    }

    /**
     * This predicate may me used on a stream of longs corresponding to product ids, returning true if there are
     * services associated with it.
     *
     * @return true/the passed long value if the long value corresponds to a product id that has services.
     */
    private Predicate<Long> hasServices()
    {
        return productId ->
        {
            final List<ScheduledServiceDto> servicesForProduct = this.serviceDao.getServicesForProduct(productId);
            return servicesForProduct != null && !servicesForProduct.isEmpty();
        };
    }

    /**
     * Verifies if:
     *
     * - newService is not null.
     *
     * - all of the reference data attached to the service exists.
     *
     * - all of the product catalogues upon which the service catalogues depend are active on the asset
     *
     * - that all of the constraints on the service catalogue relationships are met.
     *
     * - there are no duplicate services being added.
     *
     * - the cycle periodicity is valid for the product catalogue.
     *
     */
    @Override
    public void validateService(final String inputAssetId, final ScheduledServiceListDto newServices)
            throws BadRequestException, RecordNotFoundException, ParseException, MastBusinessException
    {
        this.validationService.verifyId(inputAssetId);
        this.validationService.verifyBody(newServices);

        final Long assetId = Long.parseLong(inputAssetId);

        for (final ScheduledServiceDto newService : newServices.getScheduledServices())
        { // validate that the refdata attached to the service exists and that the ids are valid.
            this.validationService.verifyField(newService.getAsset().getId().toString(), inputAssetId,
                    String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", newService.getAsset().getId().toString(), inputAssetId));
            this.serviceReferenceDataValidation.validateServiceRefData(newService);
            this.dateValidator.validateAssignedDate(newService);
            this.dateValidator.validateDateRanges(newService);
        }

        final List<ScheduledServiceDto> fullServiceList = this.serviceDao.getServicesForAsset(assetId);
        fullServiceList.addAll(newServices.getScheduledServices());

        // Check for the presence of a product with the product catalogue id that the service catalogue depends on. If
        // this does not exist append to the error message.
        final String errorMessage = newServices.getScheduledServices().stream()
                .map(service -> this.serviceDao.getServiceCatalogue(service.getServiceCatalogueId()))
                .filter(serviceCatalogue -> !this.productDao.isProductCatalogueSelectedForAsset(assetId,
                        serviceCatalogue.getProductCatalogue().getId(),
                        serviceCatalogue.getSchedulingRegime() == null ? null : serviceCatalogue.getSchedulingRegime().getId()))
                .map(serviceCatalogue -> String.format(ExceptionMessagesUtils.DEPENDENCY_ERROR_MIXED_TYPE, "service catalogue",
                        serviceCatalogue.getId(), "product catalogue", serviceCatalogue.getProductCatalogue().getId()))
                .collect(Collectors.joining(" "));

        if (errorMessage.length() > 0)
        { // if an error message has been generated throw an exception.
            LOG.debug(errorMessage);
            throw new MastBusinessException(errorMessage);
        }

        final List<ServiceCatalogueDto> serviceCatalogues = fullServiceList.stream()
                .map(service -> this.serviceDao.getServiceCatalogue(service.getServiceCatalogueId()))
                .collect(Collectors.toList());

        this.serviceValidationHelper.validateCyclePeriodicty(fullServiceList, serviceCatalogues);
        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogues);
        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogues, fullServiceList);
    }

    /**
     * Checks that the Service being updated exists and prevents an update if that Service is marked as deleted. Also
     * verifies that the given schedule is valid and that the value of the assignedDateIsHarmonisationDate flag
     * represents that actual state of those dates.
     *
     * @param serviceId
     * @param assetId
     * @param {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     * @throws MastBusinessException where Service is marked as deleted.
     * @throws ParseException
     */
    @Override
    public void validateServiceUpdate(final String serviceId, final String assetId, final ScheduledServiceDto serviceDto)
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.validationService.verifyBody(serviceDto);

        final ScheduledServiceDto existingService = this.serviceDao.getService(Long.parseLong(serviceId));

        if (existingService != null)
        {
            validateAgainstExistingService(serviceDto, existingService);
        }

        verifyAssetHasService(serviceId, assetId);

        final ServiceCatalogueDto serviceCatalogue = this.serviceReferenceDataDao.getServiceCatalogue(serviceDto.getServiceCatalogueId());

        if (!serviceCatalogue.getCyclePeriodicityEditable() && !serviceCatalogue.getCyclePeriodicity().equals(serviceDto.getCyclePeriodicity()))
        {
            final String errorMessage = String.format(ExceptionMessagesUtils.FIELD_IS_NOT_EDITABLE, "cycle periodicity", "services",
                    "service catalogue id", serviceCatalogue.getId().toString());
            LOG.debug(errorMessage);
            throw new BadRequestException(errorMessage);
        }

        this.dateValidator.validateAssignedDate(serviceDto);
        this.dateValidator.validateDateRanges(serviceDto);
    }

    /**
     * Checks that the given IDs for a Service, Asset represent entities which exist and are related.
     *
     * @param serviceId
     * @param assetId
     * @return {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void verifyAssetHasService(final String serviceId, final String assetId)
            throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(serviceId);
        this.validationService.verifyId(assetId);

        if (this.serviceDao.getService(Long.parseLong(serviceId), Long.parseLong(assetId)) == null)
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Service", Long.parseLong(serviceId), "asset",
                            Long.parseLong(assetId)));
        }
    }

    // todo can we delete this, its not being used anywhere, or should it be plugged in?
    @Override
    public void verifyHarmonisationDate(final Long assetId, final DateDto harmonisationDateDto) throws BadRequestException, MastBusinessException
    {
        final Date harmonisationDate = harmonisationDateDto.getDate();
        if (harmonisationDate == null)
        {
            throw new BadRequestException(ExceptionMessagesUtils.NO_HARMONISATION_DATE);
        }

        final AssetLightDto asset = this.assetDao.getAsset(assetId, 1L, AssetLightDto.class);

        final Date buildDate = asset.getBuildDate();

        if (buildDate == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.NO_ASSET_BUILD_DATE, asset.getName(), assetId));
        }

        // Deliberately checking "not after" rather than "before" because the story implies the check is exclusive of
        // the build date.
        if (!harmonisationDate.after(buildDate))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.DATE_TOO_EARLY, "Harmonisation", this.dateFormat.format(harmonisationDate),
                            this.dateFormat.format(buildDate)));
        }

        // Scrub time element from the date
        final Date threeMonthsFromNow = this.dateHelper.adjustDate(Calendar.getInstance().getTime(), THREE_MONTHS, Calendar.MONTH);

        // Once again, deliberately checking "not before" rather than "after" because the story implies the check is
        // exclusive of the future date.
        if (!harmonisationDate.before(threeMonthsFromNow))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.DATE_TOO_LATE, "Harmonisation", this.dateFormat.format(harmonisationDate),
                            this.dateFormat.format(threeMonthsFromNow)));
        }
    }

    private void validateAgainstExistingService(final ScheduledServiceDto serviceDto, final ScheduledServiceDto existingService)
            throws MastBusinessException
    {
        final ServiceCatalogueDto existingCatalogue = this.serviceReferenceDataDao.getServiceCatalogue(existingService.getServiceCatalogueId());
        final ServiceCatalogueDto serviceCatalogue = this.serviceReferenceDataDao.getServiceCatalogue(serviceDto.getServiceCatalogueId());
        if (existingCatalogue.getServiceType() != null)
        {
            if (ServiceType.ANNUAL.value().equals(existingCatalogue.getServiceType().getId())
                    && !existingCatalogue.getServiceType().equals(serviceCatalogue.getServiceType()))
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Service Type"));
            }
            if (ServiceType.RENEWAL.value().equals(existingCatalogue.getServiceType().getId())
                    && serviceDto.getAssignedDateManual() == null
                    && !Objects.equals(serviceDto.getDueDateManual(), existingService.getDueDateManual()))
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Assigned Date"));
            }
        }
    }

    /**
     * Method to validate a list of service ids proposed for deletion from an asset. The method validates all ids
     * (including the asset's) and validates that the asset and all of the services currently exist. The method also
     * validates that the proposed list of services following the delete satisfies all service relationships.
     */
    @Override
    public void validateDeleteServices(final String inputAssetId, final List<Long> inputServiceIds)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        // verify the asset id
        this.assetValidationService.verifyAssetIdExists(inputAssetId);

        // call verify id for all input service ids and concatenate any error messages in a string builder
        final StringBuilder finalErrorMessage = new StringBuilder();
        inputServiceIds.stream()
                .forEach(inputServiceId ->
                {
                    try
                    {
                        this.validationService.verifyId(inputServiceId);
                    }
                    catch (final BadRequestException exception)
                    {
                        finalErrorMessage.append(finalErrorMessage.length() > 0 ? " " + exception.getMessage() : exception.getMessage());
                    }
                });

        if (finalErrorMessage.length() > 0)
        { // if there were errors from the previous validation then throw a bad request exception (same as the exception
          // that was caught) and show the errors
            throw new BadRequestException(finalErrorMessage.toString());
        }

        final Long assetId = Long.parseLong(inputAssetId);

        // for all of the service ids check that they exist and compile a list of any that don't
        final List<Long> serviceIdsNotFound = inputServiceIds.stream()
                .filter(id -> !this.serviceDao.serviceExistsForAsset(assetId, id))
                .collect(Collectors.toList());

        // if any ids were not found throw an exception
        if (!serviceIdsNotFound.isEmpty())
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.ID_LIST_NOT_FOUND, "scheduled services", serviceIdsNotFound.toString()));
        }

        final List<ScheduledServiceDto> fullServiceList = this.serviceDao.getServicesForAsset(assetId);

        // only get the service catalogues for the services that are not in the list to be deleted
        final List<ServiceCatalogueDto> serviceCatalogues = fullServiceList.stream()
                .filter(service -> !inputServiceIds.contains(service.getId()))
                .map(service -> this.serviceDao.getServiceCatalogue(service.getServiceCatalogueId()))
                .collect(Collectors.toList());

        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogues);
    }

}
