package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.references.attachments.AttachmentReferenceDataDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ActionableItemValidationService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.service.validation.UpdateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "ActionableItemValidationService")
@Loggable(Loggable.DEBUG)
public class ActionableItemValidationServiceImpl extends CommonActionableItemValidationService implements ActionableItemValidationService
{
    @Autowired
    private ActionableItemDao actionableItemDao;

    @Autowired
    private ItemValidationService itemValidationService;

    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private AttachmentReferenceDataDao attachmentReferenceDataDao;

    @Autowired
    private AssetValidationService assetValidationService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private UpdateValidationService updateValidationService;

    @Autowired
    private ServiceHelper serviceHelper;

    /**
     * Tests to see if an asset has an associated actionable item with the given id.
     *
     * @param assetId
     * @param actionableItemId
     * @throws RecordNotFoundException
     */
    @Override
    public void verifyAssetHasActionableItem(final Long assetId, final Long actionableItemId)
            throws RecordNotFoundException
    {
        if (!this.actionableItemDao.actionableItemExistsForAsset(assetId, actionableItemId))
        {
            throw new RecordNotFoundException(
                    String.format(ExceptionMessagesUtils.ACTIONABLE_ITEM_DOES_NOT_EXIST_FOR_ASSET, actionableItemId, assetId));
        }
    }

    /**
     * Runs validation for posting an actionable item. This includes all of the common validation and a check that the
     * id is null and the item id in the header matches the one in the body if one is given in the body.
     *
     * @param assetId
     * @param itemId
     * @param actionableItem
     * @param allowNullDueDate
     * @throws RecordNotFoundException
     * @throws BadRequestException
     * @throws MastBusinessException
     */
    @Override
    public void verifyActionableItemPost(final String assetId, final String itemId, final ActionableItemDto actionableItem,
            final Boolean allowNullDueDate)
                    throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        if (actionableItem.getAssetItem() != null)
        {
            this.updateValidationService.verifyIds(actionableItem.getAssetItem().getId().toString(), itemId);
        }
        else
        {
            actionableItem.setAssetItem(new ItemLightDto());
            actionableItem.getAssetItem().setId(Long.parseLong(itemId));
        }

        this.validationService.verifyIdNull(actionableItem.getId());
        commonValidation(assetId, actionableItem, allowNullDueDate);
    }

    /**
     * Runs validation for posting an actionable item. This includes all of the common validation, a check that the id
     * is null and a check on the imposed date.
     *
     * @param assetId
     * @param actionableItem
     * @param allowNullDueDate
     * @throws RecordNotFoundException
     * @throws BadRequestException
     * @throws MastBusinessException
     */
    @Override
    public void verifyActionableItemPost(final String assetId, final ActionableItemDto actionableItem, final Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.validationService.verifyIdNull(actionableItem.getId());
        commonValidation(assetId, actionableItem, allowNullDueDate);

        checkImposedDateIsTodayOrInTheFuture(actionableItem);
    }

    /**
     * Runs validation for posting an actionable item without having to specify an AssetId. This is required in the case
     * of a BatchActionActionableItem.
     *
     * @param actionableItemDto
     * @param allowNullDueDate
     * @throws MastBusinessException
     * @throws BadRequestException
     * @throws RecordNotFoundException
     */
    @Override
    public void verifyActionableItemPost(final ActionableItemDto actionableItemDto, final Boolean allowNullDueDate)
            throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyIdNull(actionableItemDto.getId());
        commonValidation(actionableItemDto, allowNullDueDate);
        checkImposedDateIsTodayOrInTheFuture(actionableItemDto);
    }

    /**
     * Runs validation for updating an actionable item. This includes all of the common validation and a check that the
     * actionable item is associated with the given asset. For the put the imposed date can be in the past to allow
     * access to all statuses.
     *
     * @param assetId
     * @param actionableItemId
     * @param actionableItem
     * @param allowNullDueDate
     * @throws RecordNotFoundException
     * @throws BadRequestException
     * @throws MastBusinessException
     */
    @Override
    public void verifyActionableItemPut(final String assetId, final String actionableItemId, final ActionableItemDto actionableItem,
            final Boolean allowNullDueDate)
                    throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.updateValidationService.verifyIds(actionableItem.getId() == null ? "null" : actionableItem.getId().toString(), actionableItemId);

        commonValidation(assetId, actionableItem, allowNullDueDate);

        this.verifyAssetHasActionableItem(Long.parseLong(assetId), Long.parseLong(actionableItemId));
    }

    /**
     * Common validation for put and post methods. This includes checks that: The asset that the actionable item is
     * linked to exists, the body is not null and that all of the annotations in the dto are satisfied, the given
     * codicil category and confidentiality type exist, the linked asset item belongs to the linked asset if this is
     * present, that the actionable item matches the codicil template if one is given and that the status exists and is
     * correct for the current date.
     *
     * @param assetId
     * @param actionableItem
     * @param allowNullDueDate
     * @throws BadRequestException if the ids in the url are not correct.
     * @throws RecordNotFoundException if a linked entity is not found
     * @throws MastBusinessException if the entered object does not match the template
     */
    protected void commonValidation(final String assetId, final ActionableItemDto actionableItem, final Boolean allowNullDueDate)
            throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        this.assetValidationService.verifyAssetIdExists(assetId);

        if (actionableItem.getAssetItem() != null)
        {
            this.itemValidationService.verifyAssetHasItem(assetId, actionableItem.getAssetItem().getId().toString());
        }
        commonValidation(actionableItem, allowNullDueDate);
    }

    /**
     * Common validation method to validate an ActionableItemDto without having to specifify a valid AssetId. This is
     * required in the case of BatchActionActionableItem
     *
     * @param actionableItem
     * @param allowNullDueDate
     * @throws RecordNotFoundException
     * @throws BadRequestException
     * @throws MastBusinessException
     */
    protected void commonValidation(final ActionableItemDto actionableItem, final Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.validationService.verifyBody(actionableItem);
        this.serviceHelper.verifyModel(this.codicilReferenceDataDao.getCodicilCategory(actionableItem.getCategory().getId()),
                actionableItem.getCategory().getId());
        this.serviceHelper.verifyModel(this.attachmentReferenceDataDao.getConfidentialityType(actionableItem.getConfidentialityType().getId()),
                actionableItem.getConfidentialityType().getId());
        validateStatus(actionableItem);
        validateDefect(actionableItem);
        validateEmployee(actionableItem);
        validateTemplate(actionableItem);
        validateDueDate(actionableItem, allowNullDueDate);
    }
}
