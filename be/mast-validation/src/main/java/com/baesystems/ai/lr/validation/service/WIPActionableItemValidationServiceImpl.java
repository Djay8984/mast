package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.references.attachments.AttachmentReferenceDataDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.service.validation.WIPActionableItemValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "WIPActionableItemValidationService")
@Loggable(Loggable.DEBUG)
public class WIPActionableItemValidationServiceImpl extends CommonActionableItemValidationService implements WIPActionableItemValidationService
{
    @Autowired
    private ActionableItemDao actionableItemDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private AttachmentReferenceDataDao attachmentReferenceDataDao;

    @Autowired
    private JobValidationService jobValidationService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public void verifyJobHasWIPActionableItem(final Long jobId, final Long actionableItemId)
            throws RecordNotFoundException
    {
        if (!this.wipActionableItemDao.actionableItemExistsForJob(jobId, actionableItemId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.WIP_ACTIONABLE_ITEM_DOES_NOT_EXIST_FOR_JOB, actionableItemId,
                    jobId));
        }
    }

    @Override
    public void verifyWIPActionableItemPost(final String jobId, final ActionableItemDto actionableItem, final Boolean allowNullDueDate)
            throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        this.validationService.verifyIdNull(actionableItem.getId());
        commonWIPValidation(jobId, actionableItem, allowNullDueDate);
        checkImposedDateIsTodayOrInTheFuture(actionableItem);
    }

    @Override
    public void verifyWIPActionableItemPut(final String jobId, final String actionableItemId, final ActionableItemDto actionableItem,
            final Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.validationService.verifyId(actionableItemId);
        commonWIPValidation(jobId, actionableItem, allowNullDueDate);
        verifyJobHasWIPActionableItem(Long.parseLong(jobId), Long.parseLong(actionableItemId));
    }

    @Override
    public void verifyOptionalParentIdExistsAndIsOfCorrectType(final Long parentCodicilId) throws RecordNotFoundException
    {
        if (parentCodicilId != null)
        {
            final ActionableItemDto parentCoc = actionableItemDao.getActionableItem(parentCodicilId);
            if (parentCoc == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.PARENT_CODICIL_INVALID, parentCodicilId));
            }
        }
    }

    /**
     * This method bundles together common validation methods for WIP actionable items, specifically:
     *
     * - verifying that the job exists and the JSON payload is valid
     *
     * - the codicil category exists - the confidentiality type exists
     *
     * - if it exists, that the status is valid
     *
     * - if it exists on the payload, that the defect exists
     *
     * - if it exists on the payload, that the employee exists
     *
     * - if it exists on the payload, that the fields on the template match the fields passed in the payload.
     *
     *
     * @param jobId
     * @param actionableItem
     * @param allowNullDueDate
     * @throws BadRequestException
     * @throws RecordNotFoundException
     * @throws MastBusinessException
     */
    private void commonWIPValidation(final String jobId, final ActionableItemDto actionableItem, final Boolean allowNullDueDate)
            throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        this.jobValidationService.verifyJobIdExists(jobId);
        this.validationService.verifyBody(actionableItem);
        this.serviceHelper.verifyModel(this.codicilReferenceDataDao.getCodicilCategory(actionableItem.getCategory().getId()),
                actionableItem.getCategory().getId());
        this.serviceHelper.verifyModel(this.attachmentReferenceDataDao.getConfidentialityType(actionableItem.getConfidentialityType().getId()),
                actionableItem.getConfidentialityType().getId());

        validateStatus(actionableItem);
        validateDefect(actionableItem);
        validateEmployee(actionableItem);
        validateTemplate(actionableItem);
        validateDueDate(actionableItem, allowNullDueDate);
        validateWIPJobScopeConfirmed(actionableItem);
    }

    /**
     * The WIP DOs needs Job Scope Confirmed, whereas the non-wip versions do not (hence this can't be handled by
     * NotNull annotation on the DTO as they use the same DTO)
     *
     * @param actionableItemDto
     * @throws BadRequestException
     */
    private void validateWIPJobScopeConfirmed(final ActionableItemDto actionableItemDto) throws BadRequestException
    {
        if (actionableItemDto.getJobScopeConfirmed() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));
        }
    }
}
