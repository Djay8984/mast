package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.FollowUpActionDao;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.FollowUpActionValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "FollowUpActionValidationService")
@Loggable(Loggable.DEBUG)
public class FollowUpActionValidationServiceImpl implements FollowUpActionValidationService
{
    @Autowired
    private FollowUpActionDao followUpActionDao;

    @Override
    public void verifyJobHasFollowUpAction(final Long jobId, final Long followUpActionId) throws RecordNotFoundException
    {
        if (!this.followUpActionDao.followUpActionExistsForJob(jobId, followUpActionId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "FollowUpAction", followUpActionId, "Job", jobId));
        }
    }
}
