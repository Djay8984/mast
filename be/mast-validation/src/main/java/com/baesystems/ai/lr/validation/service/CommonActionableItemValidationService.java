package com.baesystems.ai.lr.validation.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.employees.LrEmployeeDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;

public class CommonActionableItemValidationService
{

    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private DateHelper dateHelper;

    @Autowired
    private ItemValidationService itemValidationService;

    @Autowired
    private DefectDao defectDao;

    @Autowired
    private LrEmployeeDao lrEmployeeDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ActionableItemDao actionableItemDao;

    protected void validateDefect(final ActionableItemDto actionableItem) throws RecordNotFoundException
    {
        if (actionableItem.getDefect() != null && !this.defectDao.defectExists(actionableItem.getDefect().getId()))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, actionableItem.getDefect().getId()));
        }
    }

    protected void validateTemplate(final ActionableItemDto actionableItem) throws MastBusinessException, RecordNotFoundException
    {
        if (actionableItem.getTemplate() != null)
        {
            validateAgainstTemplate(actionableItem);
        }
    }

    protected void validateEmployee(final ActionableItemDto actionableItem) throws RecordNotFoundException
    {
        if (actionableItem.getEmployee() != null && !this.lrEmployeeDao.lrEmployeeExists(actionableItem.getEmployee().getId()))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, actionableItem.getEmployee().getId()));
        }
    }

    /**
     * Ensure the status on the actionable item exists.
     *
     * @param actionableItem
     */
    protected void validateStatus(final ActionableItemDto actionableItem) throws RecordNotFoundException, MastBusinessException
    {
        if (actionableItem.getStatus() != null)
        {
            this.serviceHelper.verifyModel(this.codicilReferenceDataDao.getCodicilStatus(actionableItem.getStatus().getId()),
                    actionableItem.getStatus().getId());
        }
    }

    /**
     * This method compares the fields in the actionable item that must match the template to the give template, if one
     * is given. It checks that the template exist in the data base and that the title, description, surveyor guidance,
     * confidentiality type category and editable by surveyor flag match. If an item is give it checks that is is of the
     * correct type.
     *
     * @param actionableItem
     * @throws MastBusinessException if the entered object does not match the template.
     * @throws RecordNotFoundException if the template is not found.
     */
    private void validateAgainstTemplate(final ActionableItemDto actionableItem) throws MastBusinessException, RecordNotFoundException
    {
        final CodicilTemplateDto template = this.codicilReferenceDataDao.getCodicilTemplate(actionableItem.getTemplate().getId());

        if (template == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, actionableItem.getTemplate().getId()));
        }

        final boolean valid = actionableItem.getTitle().equals(template.getTitle())
                && actionableItem.getDescription().equals(template.getDescription())
                && (actionableItem.getSurveyorGuidance() == null && template.getSurveyorGuidance() == null
                || actionableItem.getSurveyorGuidance() != null
                        && actionableItem.getSurveyorGuidance().equals(template.getSurveyorGuidance()))
                && actionableItem.getConfidentialityType().equals(template.getConfidentialityType())
                && actionableItem.getCategory().equals(template.getCategory());

        if (!valid)
        {
            throw new MastBusinessException(String.format(ExceptionMessagesUtils.DOES_NOT_MATCH_TEMPLATE, "actionable item"));
        }

        if (actionableItem.getAssetItem() != null && template.getItemType() != null)
        {
            this.itemValidationService.verifyItemIsCorrectType(actionableItem.getAssetItem().getId(), template.getItemType().getId());
        }
    }

    // todo double check if this needs to be called on UPDATE as well as CREATE, currently it isn't.
    protected void checkImposedDateIsTodayOrInTheFuture(final ActionableItemDto actionableItem) throws BadRequestException
    {
        // At the point of creating a new AI the imposed date must be the current date or in the future.
        if (!this.dateHelper.isTodayOrAfter(actionableItem.getImposedDate()))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.BEFORE_CURRENT_DATE, "imposed date"));
        }
    }

    /**
     * Verifies that the actionable item id is valid and that the actionable item exist.
     *
     * @param inputActionableItemId
     * @throws BadRequestException
     * @throws RecordNotFoundException
     */
    protected void verifyActionableItemIdExists(final String inputActionableItemId) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(inputActionableItemId);

        final Long actionableItemId = this.validationService.verifyNumberFormat(inputActionableItemId);

        if (!this.actionableItemDao.actionableItemExists(actionableItemId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, actionableItemId));
        }
    }

    /**
     * Validation to confirm that the due date isn't null. This isn't validated on the DTO itself as some routes permit
     * a null due date.
     *
     * @param actionableItem
     * @throws BadRequestException
     */
    protected void validateDueDate(final ActionableItemDto actionableItem, final Boolean allowNullDueDate) throws BadRequestException
    {
        final boolean validateNullDueDate = allowNullDueDate == null || !allowNullDueDate.booleanValue();
        if (validateNullDueDate && actionableItem.getDueDate() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"));
        }
    }
}
