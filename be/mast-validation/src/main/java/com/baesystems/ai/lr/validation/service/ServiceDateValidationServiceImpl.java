package com.baesystems.ai.lr.validation.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.service.validation.ServiceDateValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Component(value = "ServiceDateValidationService")
@Loggable(Loggable.DEBUG)
public class ServiceDateValidationServiceImpl implements ServiceDateValidationService
{
    @Autowired
    private DateHelper dateHelper;

    private static final int THREE_MONTHS = 3;
    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();

    /**
     * Validates the assigned date of an updated service is within 3 months of todays date and is after the services due
     * date.
     *
     * @param serviceDto
     * @throws BadRequestException
     * @throws MastBusinessException
     */
    @Override
    public void validateAssignedDate(final ScheduledServiceDto serviceDto) throws BadRequestException, MastBusinessException
    {
        // if assigned date is set...
        final Date assignedDate = serviceDto.getAssignedDateManual();
        if (assignedDate != null)
        {
            // Assigned date must be equal to the date three months from now, or earlier.
            final Date threeMonthsFromNow = this.dateHelper.adjustDate(Calendar.getInstance().getTime(), THREE_MONTHS, Calendar.MONTH);
            if (assignedDate.after(threeMonthsFromNow))
            {
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.DATE_TOO_LATE, "Assigned", this.dateFormat.format(assignedDate),
                                this.dateFormat.format(threeMonthsFromNow)));
            }

            // If due date is set, due date must be AFTER assigned date
            final Date dueDate = serviceDto.getDueDateManual();
            if (dueDate != null && !dueDate.after(assignedDate))
            {
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.DATE_TOO_EARLY, "Due", this.dateFormat.format(dueDate),
                                this.dateFormat.format(assignedDate)));
            }
        }
    }

    /**
     * Checks the due date of the updated service is within the date ranges.
     *
     * @param serviceDto
     * @throws BadRequestException
     */
    @Override
    public void validateDateRanges(final ScheduledServiceDto serviceDto) throws BadRequestException
    {
        validateUpperDateRange(serviceDto.getDueDateManual(), serviceDto.getUpperRangeDateManual());
        validateLowerDateRange(serviceDto.getAssignedDateManual(), serviceDto.getDueDateManual(), serviceDto.getLowerRangeDateManual());
    }

    private void validateUpperDateRange(final Date dueDate, final Date upperRangeDate) throws BadRequestException
    {
        if (dueDate != null
                && upperRangeDate != null
                && upperRangeDate.before(dueDate))
        {
            // upper range date must not be before the due date
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.DATE_TOO_LATE, "Due", this.dateFormat.format(dueDate),
                            this.dateFormat.format(upperRangeDate)));
        }
    }

    private void validateLowerDateRange(final Date assignedDate, final Date dueDate, final Date lowerRangeDate) throws BadRequestException
    {
        if (lowerRangeDate != null)
        {
            if (dueDate != null && lowerRangeDate.after(dueDate))
            {
                // lower range must not be after due date
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.DUE_DATE_AFTER_LOWER_RANGE_DATE, this.dateFormat.format(dueDate),
                                this.dateFormat.format(lowerRangeDate)));
            }

            if (assignedDate != null && !assignedDate.before(lowerRangeDate))
            {
                // lower range must be after the due date
                throw new BadRequestException(
                        String.format(ExceptionMessagesUtils.DATE_TOO_LATE, "Assigned", this.dateFormat.format(assignedDate),
                                this.dateFormat.format(lowerRangeDate)));
            }
        }
    }
}
