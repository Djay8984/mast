package com.baesystems.ai.lr.validation.service;

import static java.lang.String.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.service.validation.WorkItemValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "WorkItemValidationService")
@Loggable(Loggable.DEBUG)
public class WorkItemValidationServiceImpl implements WorkItemValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private JobValidationService jobValidationService;

    @Autowired
    private WIPWorkItemDao wipWorkItemDao;

    @Autowired
    private WorkItemDao workItemDao;

    @Autowired
    private SurveyService surveyService;

    /**
     * Validates:</br>
     * - Job ID and Body are in a valid format.</br>
     * - Job ID exists (taken from the header).</br>
     * - Each Task ID in the given list exists.</br>
     * - The Job ID given in the header matches the Job ID from the Surveys that these Tasks are linked to.</br>
     * - Since this validation is for updating WIP Tasks, Survey is a mandatory field.</br>
     *
     * @param inputJobId
     * @param {@link WorkItemLightListDto}
     * @throws BadRequestException
     * @throws RecordNotFoundException
     */
    @Override
    public void validateWIPWorkItemListForUpdate(final String inputJobId, final WorkItemLightListDto tasks)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(inputJobId);
        this.validationService.verifyBody(tasks);
        this.jobValidationService.verifyJobIdExists(inputJobId);

        for (final WorkItemLightDto task : tasks.getTasks())
        {
            this.verifyWIPTaskIdExists(task.getId().toString());
            this.validateWIPWorkItem(inputJobId, task);
        }
    }

    @Override
    public void validateWIPWorkItemForUpdate(final String inputJobId, final WorkItemLightDto task) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(inputJobId);
        this.validationService.verifyBody(task);
        this.jobValidationService.verifyJobIdExists(inputJobId);

        this.verifyWIPTaskIdExists(task.getId().toString());
        this.validateWIPWorkItem(inputJobId, task);
    }

    @Override
    public void validateWIPWorkItemForCreate(final String inputJobId, final WorkItemLightDto task) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyId(inputJobId);
        this.validationService.verifyBody(task);
        this.validationService.verifyIdNull(task.getId());
        this.jobValidationService.verifyJobIdExists(inputJobId);

        this.validateWIPWorkItem(inputJobId, task);
    }

    private void validateWIPWorkItem(final String inputJobId, final WorkItemLightDto task) throws BadRequestException, RecordNotFoundException
    {
        if (task.getSurvey() != null)
        {
            final SurveyDto survey = this.surveyService.getSurvey(task.getSurvey().getId());
            final String surveyJobId = survey.getJob().getId().toString();

            this.validationService.verifyField(surveyJobId, inputJobId,
                    String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", surveyJobId, inputJobId));
        }
        else
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Survey"));
        }
    }

    @Override
    public void verifyWIPTaskIdExists(final String inputTaskId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputTaskId);

        final Long taskId = this.validationService.verifyNumberFormat(inputTaskId);

        if (!this.wipWorkItemDao.taskExists(taskId))
        {
            throw new RecordNotFoundException(format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "Task", taskId));
        }
    }

    @Override
    public void verifyTaskIdExists(final String inputTaskId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputTaskId);

        final Long taskId = this.validationService.verifyNumberFormat(inputTaskId);

        if (!this.workItemDao.taskExists(taskId))
        {
            throw new RecordNotFoundException(format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "Task", taskId));
        }
    }

    public void validateWIPWorkItemIsValidForJob(final String jobId, final String taskId)
    {
        //todo impl via survey
    }

    public void validateWorkItemIsValidForAsset(final String assetId, final String taskId)
    {
        //todo impl via sched service or asset item
    }

}
