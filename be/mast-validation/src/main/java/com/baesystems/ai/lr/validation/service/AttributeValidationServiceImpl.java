package com.baesystems.ai.lr.validation.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AttributeValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.AttributeValidator;
import com.jcabi.aspects.Loggable;

@Service(value = "AttributeValidationService")
@Loggable(Loggable.DEBUG)
public class AttributeValidationServiceImpl implements AttributeValidationService
{
    private static final Logger LOG = LoggerFactory.getLogger(AttributeValidationServiceImpl.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AttributeDao attributeDao;

    @Autowired
    private AttributeValidator attributeValidator;

    public void verifyAssetAndItemHasAttribute(final Long attributeId, final Long itemId, final Long assetId) throws RecordNotFoundException
    {
        if (!this.attributeDao.attributeExistsForItemAndAsset(attributeId, itemId, assetId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.INVALID_ATTRIBUTE_PATH,
                    attributeId, assetId, itemId));
        }
    }

    public void validateAttributes(final List<AttributeDto> attributes, final Long itemId, final Long assetId)
            throws RecordNotFoundException, BadRequestException
    {
        if (attributes != null)
        {
            for (final AttributeDto attribute : attributes)
            {
                final Long attributeId = attribute.getId();
                if (attributeId != null)
                {
                    verifyAssetAndItemHasAttribute(attributeId, itemId, assetId);
                }

                validateAttributeType(attribute);
            }
        }
    }

    public void validateAttributeType(final AttributeDto attributeDto) throws BadRequestException
    {
        this.validationService.verifyBody(attributeDto);
        this.verifyAttribute(attributeDto);
    }

    public void verifyAttribute(final AttributeDto attributeDto) throws BadRequestException
    {
        final List<String> results = attributeValidator.validate(attributeDto);

        if (!results.isEmpty())
        {
            final String[] resultsArray = results.toArray(new String[results.size()]);
            final String errorMessage = String.format(ExceptionMessagesUtils.VALIDATION_FAILED, Arrays.toString(resultsArray));
            LOG.debug(errorMessage);
            throw new BadRequestException(errorMessage);
        }
    }
}
