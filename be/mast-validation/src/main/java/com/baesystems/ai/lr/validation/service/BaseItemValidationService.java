package com.baesystems.ai.lr.validation.service;


import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dto.assets.ItemBaseDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

/**
 * Shared between DM and "Draft Item Validation"
 */
public class BaseItemValidationService
{
    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ValidationService validationService;

    /**
     * Validates the parent and child types against the reference table for item relationship types for the give asset.
     *
     * @param childTypeId
     * @param parentTypeId
     * @param assetCategoryId
     * @param parentId (only used in the error message)
     * @throws MastBusinessException
     */
    protected void validateChildItemType(final Long childTypeId, final Long parentTypeId, final Integer assetCategoryId,
            final Long parentId)
                    throws MastBusinessException
    {
        if (!this.itemDao.isProposedRelationshipValidForTypes(childTypeId, parentTypeId, assetCategoryId,
                RelationshipType.IS_PART_OF.getValue()))
        {
            throw new MastBusinessException(
                    String.format(ExceptionMessagesUtils.INVALID_PARENT_ITEM, parentId, childTypeId));
        }
    }

    /**
     * Validates that adding the new item(s) will not exceed the maxOccurs for the parent item. If maxOccurs is
     * unlimited the validation will pass.
     *
     * @param parent
     * @param typeId
     * @param occurs
     * @param assetCategoryId
     * @throws MastBusinessException
     */
    protected void validateWithinMaxOccurs(final ItemBaseDto parent, final Long typeId, final Integer occurs,
            final Integer assetCategoryId)
                    throws MastBusinessException
    {
        final Integer maxOccurs = this.itemTypeDao.getMaxOccursForItemType(parent.getItemType().getId(), typeId, assetCategoryId);

        if (maxOccurs != null && maxOccurs > 0 && this.itemDao.countChildItemsOfType(parent.getId(), typeId) + occurs > maxOccurs)
        {
            throw new MastBusinessException(
                    String.format(ExceptionMessagesUtils.TOO_MANY_CHILD_ITEMS, parent.getId(),
                            maxOccurs, typeId));
        }
    }


    /**
     * Verifies that a given item belongs to a given asset.
     *
     * @param inputAssetId
     * @param inputItemId
     * @throws RecordNotFoundException if the combination does not exist.
     */
    public void verifyAssetHasItem(final String inputAssetId, final String inputItemId)
            throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputAssetId);
        this.validationService.verifyId(inputItemId);

        final Long assetId = Long.parseLong(inputAssetId);
        final Long itemId = Long.parseLong(inputItemId);

        if (!(this.itemDao.itemExistsForAsset(assetId, itemId)))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.INVALID_ITEM_PATH, itemId, assetId));
        }
    }
}
