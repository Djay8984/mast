package com.baesystems.ai.lr.validation.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.references.defects.DefectReferenceDataDao;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.DefectValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "DefectValidationService")
@Loggable(Loggable.DEBUG)
public class DefectValidationServiceImpl implements DefectValidationService
{
    private static final Logger LOG = LoggerFactory.getLogger(DefectValidationServiceImpl.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private DefectReferenceDataDao defectReferenceDataDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private DefectDao defectDao;

    @Autowired
    private WIPDefectDao wipDefectDao;

    @Override
    public void validateDefectCreate(final DefectDto defectDto) throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyBody(defectDto);
        this.validationService.verifyIdNull(defectDto.getId());
        validateValues(defectDto);
        validateItems(defectDto);
    }

    @Override
    public void validateDefectUpdate(final Long defectId, final DefectDto defectDto)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyBody(defectDto);
        this.validationService.verifyId(defectId);
        validateValues(defectDto);
        validateItems(defectDto);
    }

    @Override
    public void verifyAssetHasDefect(final Long assetId, final Long defectId)
            throws RecordNotFoundException
    {
        if (!this.defectDao.defectExistsForAsset(assetId, defectId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyJobHasWIPDefect(final Long jobId, final Long defectId)
            throws RecordNotFoundException
    {
        if (!this.wipDefectDao.defectExistsForJob(jobId, defectId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    /**
     * Verifies that immutable fields have not changed in the specified DefectDto if outside the current job i.e. parent
     * is NOT null. (Ref. Story 10.46)
     *
     * @param updatedDefectDto the DefectDto used to update the corresponding DefectDo
     * @throws BadRequestException when an attempt has been made to update an immutable field.
     * @throws RecordNotFoundException the DefectDo corresponding to the specified DefectDto cannot be found.
     */
    @Override
    public void verifyWIPDefectImmutableFields(final DefectDto updatedDefectDto)
            throws BadRequestException, RecordNotFoundException
    {
        final DefectDto originalDefectDto = this.wipDefectDao.getDefect(updatedDefectDto.getId());
        if (originalDefectDto != null)
        {
            if (originalDefectDto.getParent() != null)
            {
                if (!originalDefectDto.getTitle().equals(updatedDefectDto.getTitle()))
                {
                    throw new BadRequestException(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Title"));
                }

                if (!originalDefectDto.getDefectCategory().equals(updatedDefectDto.getDefectCategory()))
                {
                    throw new BadRequestException(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Category"));
                }
            }
        }
        else
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    /**
     * Verifies that all values exist in the database
     */
    private void validateValues(final DefectDto defect) throws BadRequestException, RecordNotFoundException
    {
        final List<DefectDefectValueDto> defectDefectValues = defect.getValues();

        if (defectDefectValues != null)
        {
            final List<Long> idList = new ArrayList<Long>();

            for (final DefectDefectValueDto defectDefectValue : defectDefectValues)
            {
                this.validationService.addIdToIdList(defectDefectValue.getDefectValue(), idList);
            }

            if (!idList.isEmpty() && defectReferenceDataDao.countMatchingDefectValueIds(idList) != idList.size())
            {
                final String message = String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG_LIST, "values");
                LOG.debug(message);
                throw new RecordNotFoundException(message);
            }
        }
    }

    /**
     * Verifies that all items exist in the database
     */
    private void validateItems(final DefectDto defect) throws BadRequestException, RecordNotFoundException
    {
        final List<DefectItemDto> defectItems = defect.getItems();

        if (defectItems != null)
        {
            final List<Long> idList = new ArrayList<Long>();

            for (final DefectItemDto defectItem : defectItems)
            {
                this.validationService.addIdToIdList(defectItem.getItem(), idList);
            }

            if (!idList.isEmpty() && assetDao.countMatchingItemIds(idList) != idList.size())
            {
                final String message = String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG_LIST, "items");
                LOG.debug(message);
                throw new RecordNotFoundException(message);
            }
        }
    }
}
