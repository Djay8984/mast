package com.baesystems.ai.lr.validation.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.domain.mast.repositories.SupplementaryInformationRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.dto.validation.AttributeRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemTemplateDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.jcabi.aspects.Loggable;

@Service(value = "ValidationDataService")
@Loggable(Loggable.DEBUG)
public class ValidationDataServiceImpl implements ValidationDataService
{
    @Autowired
    private CaseDao caseDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private IhsAssetDao ihsAssetDao;

    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private SupplementaryInformationRepository supplementaryInformationRepository;

    @Autowired
    private ItemTypeDao itemTypeDao;

    private static final Logger LOG = LoggerFactory.getLogger(ValidationDataServiceImpl.class);

    @Override
    public List<Long> findDuplicateCases(final Long caseTypeId, final Long assetId)
    {
        final List<Long> duplicateCases = this.caseDao.findDuplicateCasesByAssetId(assetId, caseTypeId);

        LOG.debug(String.format("Found a total of %d duplicate Case(s).", duplicateCases.size()));

        return duplicateCases;
    }

    /**
     * Creates a model template {@link AssetModelTemplateDto} which includes the list of all item and attribute type
     * relationship rules for every existing item type.
     *
     * @return List of item ({@link ItemRuleDto}) and attribute ({@link AttributeRuleDto}) rules for each item type.
     */
    @Override
    public AssetModelTemplateDto getAssetModelTemplate()
    {
        final AssetModelTemplateDto assetModelTemplate = new AssetModelTemplateDto();
        final Map<Long, ItemTemplateDto> itemTemplate = new ConcurrentHashMap<Long, ItemTemplateDto>();
        final List<ItemRuleDto> itemRules = this.itemTypeDao.getItemTypeRelationships();
        final List<AttributeRuleDto> attributeRules = this.assetReferenceDataDao.getAttributeRules();

        if (itemRules != null)
        {
            // Create valid item list
            for (final ItemRuleDto itemRule : itemRules)
            {
                createValidItemList(itemTemplate, itemRule);
            }
        }

        if (attributeRules != null)
        {
            // Create valid attribute list
            for (final AttributeRuleDto attributeRule : attributeRules)
            {
                final Long itemTypeId = attributeRule.getItemTypeId();

                if (itemTemplate.get(itemTypeId) == null)
                {
                    itemTemplate.put(itemTypeId, new ItemTemplateDto());
                }
                itemTemplate.get(itemTypeId).getValidAttributes().add(attributeRule);
            }
        }
        assetModelTemplate.setAssetModelTemplate(itemTemplate);
        return assetModelTemplate;
    }

    /**
     * Creates {@link LinkResource} if a IHS Asset is found for a IMO number.
     *
     * @param imoNumber
     * @return {@link LinkResource}
     */
    private LinkResource getIhsAssetLink(final Long imoNumber)
    {
        LinkResource ihsAssetlink = null;

        if (imoNumber != null)
        {
            final IhsAssetDto ihsAsset = this.ihsAssetDao.getIhsAsset(imoNumber);

            if (ihsAsset != null)
            {
                ihsAssetlink = new LinkResource(ihsAsset.getId());
            }
        }

        return ihsAssetlink;
    }

    /**
     * Creates {@link LinkResource} if a IDS Asset is found for a IMO number.
     *
     * @param imoNumber
     * @return {@link LinkResource}
     */
    private LinkResource getIdsAssetLink(final Long imoNumber)
    {
        LinkResource idsAssetlink = null;

        if (imoNumber != null)
        {
            final AssetLightDto assetDto = this.assetDao.getAssetByImoNumber(imoNumber);

            if (assetDto != null)
            {
                idsAssetlink = new LinkResource(assetDto.getId());
            }
        }

        return idsAssetlink;
    }

    @Override
    public CaseValidationDto verifyCase(final Long imoNumber, final String builder, final String yardNumber, final Long businessProcess)
    {
        final CaseValidationDto caseValidation = new CaseValidationDto();

        caseValidation.setIdsAsset(getIdsAssetLink(imoNumber));
        caseValidation.setIhsAsset(getIhsAssetLink(imoNumber));

        final Long assetId = this.assetDao.findAssetIdByImoOrBuilderAndYard(imoNumber, builder, yardNumber);

        final List<Long> caseList = findDuplicateCases(businessProcess, assetId);

        for (final Long caseId : caseList)
        {
            final LinkResource caseLink = new LinkResource(caseId);
            caseValidation.getCases().add(caseLink);
        }

        return caseValidation;
    }

    @Override
    @Transactional
    @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.StdCyclomaticComplexity"})
    public Long findAttachmentByObject(final AttachmentTargetType target, final Long objectId, final Long attachmentId)
    {
        Long retVal = null;

        switch (target)
        {
            case CASE:
                retVal = this.supplementaryInformationRepository.findCaseAttachment(objectId, attachmentId);
                break;
            case MILESTONE:
                retVal = this.supplementaryInformationRepository.findMilestoneAttachment(objectId, attachmentId);
                break;
            case SERVICE:
                retVal = this.supplementaryInformationRepository.findServiceAttachment(objectId, attachmentId);
                break;
            case ASSET_NOTE:
                retVal = this.supplementaryInformationRepository.findAssetNoteAttachment(objectId, attachmentId);
                break;
            case WIP_ASSET_NOTE:
                retVal = this.supplementaryInformationRepository.findWIPAssetNoteAttachment(objectId, attachmentId);
                break;
            case COC:
                retVal = this.supplementaryInformationRepository.findCoCAttachment(objectId, attachmentId);
                break;
            case WIP_COC:
                retVal = this.supplementaryInformationRepository.findWIPCoCAttachment(objectId, attachmentId);
                break;
            case ASSET:
                retVal = this.supplementaryInformationRepository.findAssetAttachment(objectId, attachmentId);
                break;
            case JOB:
                retVal = this.supplementaryInformationRepository.findJobAttachment(objectId, attachmentId);
                break;
            case ITEM:
                retVal = this.supplementaryInformationRepository.findItemAttachment(objectId, attachmentId);
                break;
            case ACTIONABLE_ITEM:
                retVal = this.supplementaryInformationRepository.findActionableItemAttachment(objectId, attachmentId);
                break;
            case DEFECT:
                retVal = this.supplementaryInformationRepository.findDefectAttachment(objectId, attachmentId);
                break;
            case WIP_DEFECT:
                retVal = this.supplementaryInformationRepository.findWIPDefectAttachment(objectId, attachmentId);
                break;
            case WIP_ACTIONABLE_ITEM:
                retVal = this.supplementaryInformationRepository.findWIPActionableItemAttachment(objectId, attachmentId);
                break;
            case TASK:
                retVal = this.supplementaryInformationRepository.findWorkItemAttachment(objectId, attachmentId);
                break;
            case WIP_TASK:
                retVal = this.supplementaryInformationRepository.findWIPWorkItemAttachment(objectId, attachmentId);
                break;
            case SURVEY:
                retVal = this.supplementaryInformationRepository.findSurveyAttachment(objectId, attachmentId);
                break;
            case REPAIR:
                retVal = this.supplementaryInformationRepository.findRepairAttachment(objectId, attachmentId);
                break;
            case WIP_REPAIR:
                retVal = this.supplementaryInformationRepository.findWIPRepairAttachment(objectId, attachmentId);
                break;
            default:
                throw new MastSystemException("Invalid AttachmentTargetType");
        }

        return retVal;
    }

    private Map<Long, ItemTemplateDto> createValidItemList(final Map<Long, ItemTemplateDto> itemTemplate, final ItemRuleDto itemRule)
    {
        final Long itemTypeId = itemRule.getFromItemType();
        if (itemTypeId != null)
        {
            if (itemTemplate.get(itemTypeId) == null)
            {
                itemTemplate.put(itemTypeId, new ItemTemplateDto());
            }
            itemTemplate.get(itemTypeId).getValidItems().add(itemRule);
        }
        return itemTemplate;
    }

    @Override
    public Long getCurrentStatusForCase(final Long caseId)
    {
        return this.caseDao.getCaseStatusId(caseId);
    }
}
