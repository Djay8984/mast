package com.baesystems.ai.lr.validation;

import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.INCONSISTENT_ATTRIBUTE_VALUE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.validation.utils.ValidationStringUtils;
import com.baesystems.ai.lr.validation.utils.ValidationUtils;

@Component(value = "AttributeValidator")
public class AttributeValidator
{
    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private ValidationUtils validationUtils;

    @Autowired
    private ValidationStringUtils validationStringUtils;

    /**
     * Validates that the attribute type description matches a specified pattern.
     **/
    public List<String> validate(final AttributeDto attribute)
    {
        final List<String> results = new ArrayList<String>();
        final LinkResource type = attribute.getAttributeType();
        if (validationUtils.validatePresence(type, "attributeType", results))
        {
            final Long typeId = type.getId();

            if (validationUtils.validatePresence(type.getId(), "attributeType.id", results))
            {
                final ReferenceDataDto referenceTypeValue = assetReferenceDataDao.getAttributeType(typeId);
                validationStringUtils.validateString(attribute.getValue(), referenceTypeValue.getDescription(), "value", results,
                        String.format(INCONSISTENT_ATTRIBUTE_VALUE, typeId.intValue()));
            }
        }
        return results;
    }
}
