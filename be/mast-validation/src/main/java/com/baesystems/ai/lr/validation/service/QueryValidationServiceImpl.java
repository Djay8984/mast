package com.baesystems.ai.lr.validation.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.QueryValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "QueryValidationService")
@Loggable(Loggable.DEBUG)
public class QueryValidationServiceImpl implements QueryValidationService
{
    @Autowired
    private ValidationService validationService;

    private static final Integer TWO = 2;

    @Override
    public void verifyQueryId(final Object[] inputIds) throws BadRequestException
    {
        if (inputIds != null && inputIds.length != 0)
        {
            for (final Object inputId : inputIds)
            {
                if (inputId != null && !"0".equals(inputId))
                {
                    if (inputId instanceof Number)
                    {
                        validationService.verifyId(((Number) inputId).longValue());
                    }
                    else
                    {
                        validationService.verifyId(inputId.toString());
                    }
                }
            }
        }
    }

    /**
     * This method checks the query parameters. It will need to be reverted if a better way is found.
     */
    @Override
    public void verifyQuery(final String query, final String allowedParams) throws BadRequestException
    {
        if (query != null)
        {
            if (allowedParams == null || allowedParams.length() == 0)
            {
                throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "parameters"));
            }

            final List<String> allowed = Arrays.asList(allowedParams.split("&"));

            final String[] params = query.split("&");
            for (final String param : params)
            {
                final List<String> parts = Arrays.asList(param.split("="));

                if (parts.size() != TWO)
                {
                    throw new BadRequestException(String.format(ExceptionMessagesUtils.INVALID_LIST_ERROR_MSG, "parts"));
                }

                if (!allowed.contains(parts.get(0)))
                {
                    throw new BadRequestException(String.format(ExceptionMessagesUtils.INVALID_VALUE, parts.get(0)));
                }
            }
        }
    }

}
