package com.baesystems.ai.lr.validation.service;

import static java.lang.String.format;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.enums.JobStatusType;
import com.baesystems.ai.lr.enums.OfficeRoleType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "JobValidationService")
@Loggable(Loggable.DEBUG)
public class JobValidationServiceImpl implements JobValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private JobDao jobDao;

    @Override
    public void validateJobCreate(final JobDto jobDto) throws BadRequestException, MastBusinessException
    {
        this.validationService.verifyBody(jobDto);
        this.validationService.verifyIdNull(jobDto.getId());
        this.verifyServiceDeliveryOfficePresent(jobDto.getOffices());
    }

    @Override
    public void validateJobUpdate(final Long jobId, final JobDto jobDto) throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        this.validationService.verifyBody(jobDto);
        this.validationService.verifyId(jobId);

        if (!jobDto.getId().equals(jobId))
        {
            throw new BadRequestException(ExceptionMessagesUtils.FIELD_MISMATCH);
        }

        this.verifyJobIdExists(jobId.toString());

        final ReferenceDataDto currentJobStatus = this.jobDao.getJobStatusForJob(jobId);
        if (JobStatusType.CANCELLED.getValue().equals(currentJobStatus.getId())
                || JobStatusType.ABORTED.getValue().equals(currentJobStatus.getId())
                || JobStatusType.CLOSED.getValue().equals(currentJobStatus.getId()))
        {
            throw new BadRequestException(format(ExceptionMessagesUtils.INVALID_STATUS_FOR_UPDATE, "Job", jobId, currentJobStatus.getName()));
        }

        this.verifyServiceDeliveryOfficePresent(jobDto.getOffices());

        if (JobStatusType.CLOSED.getValue().equals(jobDto.getJobStatus().getId()) && jobDto.getCompletedOn() == null)
        {
            throw new BadRequestException(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "completedOn"));
        }
    }

    @Override
    public void verifyJobIdExists(final String inputJobId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputJobId);

        final Long jobId = this.validationService.verifyNumberFormat(inputJobId);

        if (!this.jobDao.jobExists(jobId))
        {
            throw new RecordNotFoundException(format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, jobId));
        }
    }

    @Override
    public void verifyServiceDeliveryOfficePresent(final List<OfficeLinkDto> offices) throws MastBusinessException
    {
        boolean sdoPresent = false;

        if (offices != null && !offices.isEmpty())
        {
            int officeCounter = 0;
            while (officeCounter < offices.size() && !sdoPresent)
            {
                final LinkResource officeRole = offices.get(officeCounter).getOfficeRole();
                sdoPresent = officeRole != null && OfficeRoleType.SDO.getValue().equals(officeRole.getId());
                officeCounter++;
            }
        }

        if (!sdoPresent)
        {
            throw new MastBusinessException(format(ExceptionMessagesUtils.VALIDATION_FAILED, "A Job must have a Service Delivery Office"));
        }
    }

    @Override
    public void validateEmployeeAndTeamId(final Long employeeId, final Long teamId) throws BadRequestException
    {
        if (employeeId != null && teamId != null)
        {
            throw new BadRequestException(format(ExceptionMessagesUtils.CANNOT_SPECIFY_EMPLOYEE_AND_TEAM, employeeId, teamId));
        }
    }

    public void verifyJobIdExistsforAssetId(final Long assetId, final Long jobId) throws RecordNotFoundException
    {
        final Long jobVersion = this.jobDao.getVersionForAssetAndJob(assetId, jobId);
        if (jobVersion == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG));
        }
    }
}
