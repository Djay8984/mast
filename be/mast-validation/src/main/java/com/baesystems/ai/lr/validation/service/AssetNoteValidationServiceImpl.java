package com.baesystems.ai.lr.validation.service;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetNoteValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "AssetNoteValidationService")
@Loggable(Loggable.DEBUG)
public class AssetNoteValidationServiceImpl implements AssetNoteValidationService
{
    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AssetNoteDao assetNoteDao;

    @Autowired
    private WIPAssetNoteDao wipAssetNoteDao;

    @Autowired
    private DateHelper dateHelper;

    @Override
    public void validateAsset(final String assetNoteId, final String inputAssetId, final String headerAssetId)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService.verifyField(inputAssetId, headerAssetId,
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Asset", inputAssetId, headerAssetId));

        if (assetNoteId != null)
        {
            final AssetNoteDto existingDto = assetNoteDao.getAssetNote(this.validationService.verifyNumberFormat(assetNoteId));

            if (existingDto != null && existingDto.getAsset() != null)
            {
                this.validationService.verifyField(existingDto.getAsset().getId(), this.validationService.verifyNumberFormat(inputAssetId),
                        ExceptionMessagesUtils.FIELD_MISMATCH);
            }
        }
    }

    @Override
    public void validateJob(final String assetNoteId, final String inputJobId, final String headerJobId)
            throws BadRequestException, RecordNotFoundException
    {
        this.validationService
                .verifyField(inputJobId, headerJobId, String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", inputJobId, headerJobId));

        if (assetNoteId != null)
        {
            final AssetNoteDto existingDto = wipAssetNoteDao.getAssetNote(this.validationService.verifyNumberFormat(assetNoteId));

            if (existingDto != null && existingDto.getJob() != null)
            {
                this.validationService.verifyField(existingDto.getJob().getId(), this.validationService.verifyNumberFormat(inputJobId),
                        ExceptionMessagesUtils.FIELD_MISMATCH);
            }
        }
    }

    @Override
    public void validateAssetAndItem(final String assetId, final String itemId, final AssetNoteDto assetNoteDto)
            throws BadRequestException
    {
        this.validationService.verifyId(assetId);
        this.validationService.verifyId(itemId);
        this.validationService.verifyNumberFormat(assetId);
        this.validationService.verifyNumberFormat(itemId);
        this.validationService.verifyBody(assetNoteDto);
        this.validationService.verifyIdNull(assetNoteDto.getId());

        final LinkResource bodyAsset = assetNoteDto.getAsset();
        if (bodyAsset == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Asset"));
        }

        final ItemLightDto bodyItem = assetNoteDto.getAssetItem();
        if (bodyItem == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Item"));
        }

        this.validationService.verifyField(Long.toString(bodyAsset.getId()), assetId,
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Asset", bodyAsset.getId(), assetId));

        this.validationService.verifyField(Long.toString(bodyItem.getId()), itemId,
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Item", bodyItem.getId(), itemId));
    }

    @Override
    public void verifyAssetHasAssetNote(final Long assetId, final Long assetNoteId)
            throws RecordNotFoundException
    {
        if (!this.assetNoteDao.assetNoteExistsForAsset(assetId, assetNoteId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyJobHasWIPAssetNote(final Long jobId, final Long assetNoteId)
            throws RecordNotFoundException
    {
        if (!this.wipAssetNoteDao.assetNoteExistsForJob(jobId, assetNoteId))
        {
            throw new RecordNotFoundException(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);
        }
    }

    @Override
    public void verifyOptionalParentIdExistsAndIsOfCorrectType(final Long parentCodicilId) throws RecordNotFoundException
    {
        if (parentCodicilId != null)
        {
            final AssetNoteDto parentCoc = this.assetNoteDao.getAssetNote(parentCodicilId);
            if (parentCoc == null)
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.PARENT_CODICIL_INVALID, parentCodicilId));
            }
        }
    }

    @Override
    public void verifyImposedDateNotHistorical(final AssetNoteDto assetNoteDto) throws BadRequestException
    {
        if (!dateHelper.isTodayOrAfter(assetNoteDto.getImposedDate()))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.HISTORICAL_IMPOSED_DATE, dateFormat.format(assetNoteDto.getImposedDate())));
        }
    }

    /**
     * The WIP DOs needs Job Scope Confirmed, whereas the non-wip versions do not (hence this can't be handled by
     * NotNull annotation on the DTO as they use the same DTO)
     *
     * @param assetNoteDto
     * @throws BadRequestException
     */
    @Override
    public void validateWIPJobScopeConfirmed(final AssetNoteDto assetNoteDto) throws BadRequestException
    {
        if (assetNoteDto.getJobScopeConfirmed() == null)
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));
        }
    }
}
