package com.baesystems.ai.lr.validation.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.DefectStatusType;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.reports.ReportService;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.ReportValidationService;
import com.baesystems.ai.lr.service.validation.UpdateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.utils.ValidationMessageUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ReportValidationService")
@Loggable(Loggable.DEBUG)
public class ReportValidationServiceImpl implements ReportValidationService
{
    @Autowired
    private ReportService reportService;

    @Autowired
    private WIPDefectDao wipDefectDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private JobValidationService jobValidationService;

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private UpdateValidationService updateValidationService;

    /**
     * Method to validate report update. The method checks that the job exists and has this report and that the ids in
     * the body and header are valid an match.
     */
    @Override
    public void validateReportUpdate(final String reportId, final String jobId, final ReportDto reportDto)
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        this.jobValidationService.verifyJobIdExists(jobId);

        this.validationService.verifyBody(reportDto);
        this.validationService.verifyNumberFormat(jobId);
        this.validationService.verifyNumberFormat(reportId);

        this.updateValidationService.verifyIds(reportDto.getId() == null ? null : reportDto.getId().toString(), reportId);
        this.updateValidationService.verifyIds(reportDto.getJob() == null ? null : reportDto.getJob().getId().toString(), jobId);
    }

    @Override
    public void validateReportSave(final String jobId, final ReportDto reportDto)
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        this.jobValidationService.verifyJobIdExists(jobId);

        this.validationService.verifyBody(reportDto);
        this.validationService.verifyIdNull(reportDto.getId());
        final Long reportTypeId = reportDto.getReportType().getId();
        final Long jobIdLong = this.validationService.verifyNumberFormat(jobId);

        if (ReportType.FAR.value().equals(reportTypeId) || ReportType.FSR.value().equals(reportTypeId))
        {
            // Check there are no open defects that do not have an open CoC
            final List<DefectDto> openDefects = this.wipDefectDao.getDefectsForJobWithStatus(jobIdLong,
                    Collections.singletonList(DefectStatusType.OPEN.getValue()));

            List<Long> defectIdsBlockingReport = openDefects.stream()
                    .map(defect -> defect.getId())
                    .filter(defectId -> !this.wipCoCDao.defectHasCoCInStatus(defectId,
                            Collections.singletonList(CodicilStatusType.COC_OPEN.getValue())))
                    .collect(Collectors.toList());

            if (!defectIdsBlockingReport.isEmpty())
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, reportTypeId,
                        "open defects", "no open CoCs", defectIdsBlockingReport.toString()));
            }

            // Check there are no closed defects that still have open CoCs
            final List<DefectDto> closedDefects = this.wipDefectDao.getDefectsForJobWithStatus(jobIdLong,
                    Collections.singletonList(DefectStatusType.CLOSED.getValue()));

            defectIdsBlockingReport = closedDefects.stream()
                    .map(defect -> defect.getId())
                    .filter(defectId -> this.wipCoCDao.defectHasCoCInStatus(defectId,
                            Collections.singletonList(CodicilStatusType.COC_OPEN.getValue())))
                    .collect(Collectors.toList());

            if (!defectIdsBlockingReport.isEmpty())
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, reportTypeId,
                        "closed defects", "open CoCs", defectIdsBlockingReport.toString()));
            }

            // Check that there are no surveys with narratives that are not credited
            final List<Long> surveyIdsBlockingReport = this.surveyDao.getUncreditedSurveysWithNarrativeForJob(jobIdLong);

            if (!surveyIdsBlockingReport.isEmpty())
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, reportTypeId,
                        "surveys", "narratives, but are not credited", surveyIdsBlockingReport.toString()));
            }
        }

        validateReportContents(jobIdLong, reportTypeId);
    }

    /**
     * Validate that reports that come after FAR have a parent report.
     */
    protected void validateReportContents(final Long jobId, final Long reportTypeId) throws MastBusinessException
    {

        if (ReportType.isAfter(reportTypeId, ReportType.FAR))
        {
            final Long previousType = ReportType.getTypeBeforeCurrent(reportTypeId);
            final ReportDto parent = this.reportService.getLatestReport(jobId, previousType);

            // DSR and FSR should have a parent report
            if (parent == null)
            {
                throw new MastBusinessException(
                        String.format(ValidationMessageUtils.FIELD_CANNOT_BE_NULL, new Object[]{"parent has not been submitted"}));
            }
        }
    }

    /**
     * Validation to check that the report we're retrieving belongs to the job id specified.
     *
     * @param inputJobId the job ID as a {@link String}
     * @param inputReportId the report ID as a {@link String}
     * @throws RecordNotFoundException
     * @throws BadRequestException
     */
    @Override
    public void validateJobHasReport(final String inputJobId, final String inputReportId) throws RecordNotFoundException, BadRequestException
    {
        this.validationService.verifyId(inputJobId);
        this.validationService.verifyId(inputReportId);

        final Long jobId = Long.parseLong(inputJobId);
        final Long reportId = Long.parseLong(inputReportId);

        if (!this.reportDao.reportExistsForJob(reportId, jobId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Report", reportId,
                    "Job", jobId));
        }
    }
}
