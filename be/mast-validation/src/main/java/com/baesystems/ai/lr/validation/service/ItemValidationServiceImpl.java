package com.baesystems.ai.lr.validation.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.codicils.CodicilDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ItemValidationService")
@Loggable(Loggable.DEBUG)
@SuppressWarnings("PMD.GodClass")
public class ItemValidationServiceImpl extends BaseItemValidationService implements ItemValidationService
{
    @Autowired
    private ItemDao itemDao;

    @Autowired
    private AttributeDao attributeDao;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private CodicilDao codicilDao;

    @Autowired
    private DefectDao defectDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private AssetValidationService assetValidationService;

    public static final String ITEM_NAME_NOT_UNIQUE = "The Item %s cannot be marked as complete because there is another completed item with the same name.";
    public static final String ITEM_MISSING_MANDATORY_ATTRIBUTES = "The Item %s cannot be marked as complete because it is missing mandatory attributes.";
    public static final String ITEM_ATTRIBUTE_HAS_NULL_VALUE = "The Item %s cannot be marked as complete because attribute %d has a null value.";

    /**
     * This class holds the data that we have processed to prevent the same data being validated more than once.
     */
    private static class ValidationContext
    {
        private final AttributeDao attributeDao;
        private final Set<ItemDto> itemsWithVerifiedAttributes = new HashSet<>();
        private final Map<Long, List<Long>> mandatoryAttributesForItemTypesMap = new HashMap<>();

        ValidationContext(final AttributeDao attributeDao)
        {
            this.attributeDao = attributeDao;
        }

        public boolean hasThisItemBeenVerified(final ItemDto item)
        {
            return this.itemsWithVerifiedAttributes.contains(item);
        }

        public void addVerifiedItem(final ItemDto item)
        {
            this.itemsWithVerifiedAttributes.add(item);
        }

        public List<Long> getMandatoryAttributeTypeIdsForItem(final ItemDto item)
        {
            final Long itemTypeId = item.getItemType().getId();

            List<Long> result = this.mandatoryAttributesForItemTypesMap.get(itemTypeId);
            if (result == null)
            {
                result = this.attributeDao.getMandatoryAttributeTypeIdsForAnItemTypeId(itemTypeId);
                this.mandatoryAttributesForItemTypesMap.put(itemTypeId, result);
            }
            return result;
        }

    }

    /**
     * Verifies that a given item has a given relationship type.
     *
     * @param itemId
     * @throws RecordNotFoundException if the item is on of the correct relationship type.
     */
    @Override
    public void verifyItemHasItemRelationship(final Long itemId, final Long relationshipId, final RelationshipType relationshipType)
            throws RecordNotFoundException
    {
        if (!this.itemDao.relationshipExistsForItem(itemId, relationshipId, relationshipType))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.INVALID_RELATIONSHIP_PATH,
                    relationshipId, relationshipType.getValue(), itemId));
        }
    }

    /**
     * Verifies that a given item is of a given type.
     *
     * @param itemId
     * @param typeId
     * @throws MastBusinessException if the item is on of the correct type.
     */
    @Override
    public void verifyItemIsCorrectType(final Long itemId, final Long typeId) throws MastBusinessException
    {
        if (!this.itemDao.itemIsOfType(itemId, typeId))
        {
            throw new MastBusinessException(String.format(ExceptionMessagesUtils.INCORRECT_TYPE, itemId, typeId));
        }
    }

    /**
     * This method checks whether the reviewed state of the item may be updated.
     * <p>
     * If the reviewed state has not changed, then no further validation is required. If the reviewed state is to be set
     * to false, no further validation is required.
     * <p>
     * However, if the review state is to be set to true then the item data and all its child items is retrieved and the
     * following is verified:
     * <p>
     * - That the item name is unique among its reviewed peers
     * <p>
     * - That all mandatory fields have been set
     *
     * @param itemId
     * @throws MastBusinessException
     */
    @Override
    public void validateItemReview(final Long itemId) throws MastBusinessException
    {
        // final LazyItemDto lazyItemDto = this.itemDao.getItem(itemId);
        final LazyItemDto lazyItemDto = new LazyItemDto();

        final boolean reviewedInTheDatabase = lazyItemDto.getReviewed() == null || lazyItemDto.getReviewed();

        // If the reviewed flag has not changed and reviewed flag is to be set to false, update nothing.
        if (!reviewedInTheDatabase)
        {
            // get full dto object tree
            final ItemDto itemDto = this.itemDao.getItemWithFullHierarchy(itemId);

            final ValidationContext context = new ValidationContext(this.attributeDao);
            validateItemReviewWithContext(itemDto, context);
        }

    }

    /**
     * Checks whether the decommissioned flag for the item can be set to true according to the following rules:
     * <p>
     * - Cannot decommission an item if it has active services, defects or codicils associated with it. This applies to
     * all child items. </br>
     *
     * @param itemId
     * @throws MastBusinessException
     */
    @Override
    public void validateDecommisionItem(final Long itemId) throws MastBusinessException
    {
        checkForActiveAssociations(itemId, "decommissioned");
    }

    /**
     * Verifies that a specified ItemTypeId exists. A value of null is ignored and the method returns.
     *
     * @param itemTypeId the itemType id to verify.
     * @throws RecordNotFoundException if the itemTypeId does not exist.
     */
    @Override
    public void verifyItemTypeIdExists(final Long itemTypeId) throws RecordNotFoundException
    {
        if (itemTypeId != null && !this.itemTypeDao.itemTypeExists(itemTypeId))
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "ItemTypeId", itemTypeId));
        }
    }

    private void validateItemReviewWithContext(final ItemDto item, final ValidationContext context) throws MastBusinessException
    {
        verifyItemNameUnique(item);
        if (!context.hasThisItemBeenVerified(item))
        {
            verifyAllAttributes(item, context);
        }

        final List<ItemDto> childItems = item.getItems();
        if (childItems != null && !childItems.isEmpty())
        {
            for (final ItemDto childItem : childItems)
            {
                validateItemReviewWithContext(childItem, context);
            }
        }
    }

    private void verifyAllAttributes(final ItemDto item, final ValidationContext context) throws MastBusinessException
    {
        final List<Long> mandatoryAttributeTypeIds = context.getMandatoryAttributeTypeIdsForItem(item);

        // Only check if there are any mandatory fields
        if (!mandatoryAttributeTypeIds.isEmpty())
        {
            // Check that all attribute type ids are defined in the item.
            final Set<Long> availableAttributeTypeIds = new HashSet<>();
            for (final AttributeDto itemAttribute : item.getAttributes())
            {
                availableAttributeTypeIds.add(itemAttribute.getAttributeType().getId());
            }

            if (!availableAttributeTypeIds.containsAll(mandatoryAttributeTypeIds))
            {
                throw new MastBusinessException(String.format(ITEM_MISSING_MANDATORY_ATTRIBUTES, item.getName()));
            }
        }

        // Verify that all attributes have values
        if (item.getAttributes() != null)
        {
            for (final AttributeDto attribute : item.getAttributes())
            {
                if (attribute.getValue() == null)
                {
                    throw new MastBusinessException(String.format(ITEM_ATTRIBUTE_HAS_NULL_VALUE, item.getName(), attribute.getId()));
                }
            }
        }

        context.addVerifiedItem(item);
    }

    private void verifyItemNameUnique(final ItemDto item) throws MastBusinessException
    {
        if (item.getParentItem() != null)
        {
            final String itemName = item.getName();
            final Long parentId = item.getParentItem().getId();
            if (!this.itemDao.isItemNameUnique(itemName, parentId))
            {
                throw new MastBusinessException(String.format(ITEM_NAME_NOT_UNIQUE, itemName));
            }
        }

    }

    /**
     * Method to validate the creation of new "is related to" item relationships.
     *
     * Validates that the relationship: Is "is related to". Is not between an item and itself. Is not between an item
     * and any of its child or parent items (immediate) Is valid for the item types in question according to the
     * reference data. Is not equivalent to any existing relationship.
     *
     * @param assetId
     * @param fromItemId
     * @param relationship
     * @throws MastBusinessException
     * @throws BadRequestException
     * @throws RecordNotFoundException
     */
    public void verifyItemRelationship(final Long assetId, final Long fromItemId, final ItemRelationshipDto relationship)
            throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        final Long draftVersion = this.assetDao.getDraftVersion(assetId);

        final Long toItemId = relationship.getToItem().getId();

        final RelationshipType passedRelationshipType = RelationshipType.getTypeForId(relationship.getType().getId());

        this.verifyAssetHasItem(assetId.toString(), toItemId.toString());

        final LinkVersionedResource parentOfFromItem = this.itemDao.getItem(new VersionedItemPK(fromItemId, assetId, draftVersion)).getParentItem();
        final LinkVersionedResource parentOfToItem = this.itemDao.getItem(new VersionedItemPK(fromItemId, assetId, draftVersion)).getParentItem();

        if (RelationshipType.IS_RELATED_TO != passedRelationshipType
                || parentOfFromItem.getId().equals(toItemId)
                || parentOfToItem.getId().equals(fromItemId))

        {
            // If the to item is a parent or child (of any generation) of the from item or there are the same item or
            // the relationship is not is related to.
            throw new BadRequestException(String.format(ExceptionMessagesUtils.CANNOT_CREATE, "item relationship"));
        }

        if (this.itemDao.relationshipExists(toItemId, fromItemId, RelationshipType.IS_RELATED_TO.getValue())
                || this.itemDao.relationshipExists(fromItemId, toItemId, RelationshipType.IS_RELATED_TO.getValue()))
        {
            // If the same relationship already exists.
            throw new MastBusinessException(String.format(ExceptionMessagesUtils.ALREADY_EXISTS, "item relationship", "relationship"));
        }

        // TODO Peer relationships do not contain any reference data to validate against. Have been advised to remove
        // this check until this is confirmed.

        // validate that the relationship is in the reference data.
        // if (RelationshipType.IS_RELATED_TO != passedRelationshipType)
        // {
        // validPeerToPeerRelationshipTypes(this.itemDao.getItemType(toItemId), this.itemDao.getItemType(fromItemId),
        // this.assetDao.getAssetCategory(assetId).intValue(), RelationshipType.IS_RELATED_TO.getValue());
        // }
    }

    /**
     * Validates peer to peer (is related to) type relationships against the reference data. Both orders are used as
     * these relationships are not order depended.
     *
     * @param toItemTypeId
     * @param itemTypeId
     * @param assetCategoryId
     * @param relationshipTypeId
     * @throws MastBusinessException
     */
    protected void validPeerToPeerRelationshipTypes(final Long toItemTypeId, final Long itemTypeId,
            final Integer assetCategoryId, final Long relationshipTypeId) throws MastBusinessException
    {
        if (!this.itemDao.isProposedRelationshipValidForTypes(toItemTypeId, itemTypeId, assetCategoryId, relationshipTypeId)
                && !this.itemDao.isProposedRelationshipValidForTypes(itemTypeId, toItemTypeId, assetCategoryId, relationshipTypeId))
        {
            throw new MastBusinessException(
                    String.format(ExceptionMessagesUtils.IVALID_ITEM_RELATIONSHIP, relationshipTypeId,
                            itemTypeId, this.itemTypeDao.getItemTypeName(itemTypeId),
                            toItemTypeId, this.itemTypeDao.getItemTypeName(toItemTypeId)));
        }
    }

    /**
     * Validate that a given item can be deleted. This validates the ids and that that item is linked to the asset and
     * that neither the item nor its children have open defects or open inactive or change recommended codicils, and
     * that there are no open services
     *
     * @param inputAssetId
     * @param inputItemId
     * @throws RecordNotFoundException if the item or asset is not found or the item is not linked to the asset.
     * @throws BadRequestException if there is a problem with the ids
     * @throws MastBusinessException if the item cannot be deleted because it is linked to a codicil or defect.
     */
    @Override
    public void verifyDeleteItem(final String inputAssetId, final String inputItemId)
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.assetValidationService.verifyAssetIdExists(inputAssetId);
        this.verifyAssetHasItem(inputAssetId, inputItemId);
        final Long itemId = Long.parseLong(inputItemId);
        final Long assetId = Long.parseLong(inputAssetId);

        final LazyItemDto rootDraftItem = this.itemDao.getRootItem(assetId, null, true);

        if (rootDraftItem != null && rootDraftItem.getId().equals(itemId))
        {
            throw new BadRequestException(
                    String.format(ExceptionMessagesUtils.CANNOT_DELETE_ROOT_ITEM, itemId));
        }

        checkForActiveAssociations(itemId, "deleted");
    }

    /**
     * Examine an Item and all of its children to determine if there are any open Codicils, Defects or Services. Throw
     * an exception if any are found.
     *
     * @param topLevelItemId - ID of Item to check and whose children will also be checked
     * @param action - String to be included in the exception message indicating what action was being attempted.
     * @throws MastBusinessException
     */
    private void checkForActiveAssociations(final Long topLevelItemId, final String action) throws MastBusinessException
    {
        final List<Long> itemIdsToCheck = this.itemDao.getItemChildIdList(topLevelItemId);
        itemIdsToCheck.add(topLevelItemId);
        if (!itemIdsToCheck.isEmpty())
        {
            if (this.codicilDao.doAnyItemsHaveOpenCodicils(itemIdsToCheck))
            {
                throw new MastBusinessException(
                        String.format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, topLevelItemId, action, "open codicils"));
            }

            if (this.defectDao.doAnyItemsHaveOpenDefects(itemIdsToCheck))
            {
                throw new MastBusinessException(
                        String.format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, topLevelItemId, action, "open defects"));
            }

            if (this.serviceDao.doAnyItemsHaveOpenServices(itemIdsToCheck))
            {
                throw new MastBusinessException(
                        String.format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, topLevelItemId, action, "open services"));
            }
        }
    }
}
