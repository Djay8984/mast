package com.baesystems.ai.lr.validation.utils;

import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.FIELD_CANNOT_BE_NULL;

import java.util.List;
import java.util.Objects;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Component(value = "ValidationUtils")
@Primary
public class ValidationUtils
{
    public boolean validatePresence(final Object obj, final String fieldName, final List<String> results)
    {
        return validatePresence(obj, fieldName, results, "");
    }

    /**
     * Tests the presence of an object and adds to the list of errors if it is not found.
     *
     * @param obj, the object whose presence is to be tested.
     * @param fieldName, the name of the field to which the object belongs, to be used it the error message.
     * @param results, list of errors to be appended if need be.
     * @param message, an optional string to append to the default message.
     * @return isPresent, whether or not the value was present, to be used by the calling method to avoid null pointer
     *         exceptions if the argument is a class whose fields are checked later.
     */
    public boolean validatePresence(final Object obj, final String fieldName, final List<String> results, final String message)
    {
        boolean isPresent = true;

        if (obj == null)
        {
            results.add(String.format(FIELD_CANNOT_BE_NULL, fieldName) + message);
            isPresent = false;
        }

        return isPresent;
    }

    /**
     * Validate that the two objects are equal (including both null).
     */
    public void verifyObjectIsUnchanged(final Object obj1, final Object obj2, final String objName) throws BadRequestException
    {
        if (!Objects.equals(obj1, obj2))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.FIELD_CANNOT_BE_UPDATED, objName));
        }
    }
}
