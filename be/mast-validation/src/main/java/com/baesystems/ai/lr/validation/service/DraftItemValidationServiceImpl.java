package com.baesystems.ai.lr.validation.service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.DraftItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.DraftItemValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.VersioningHelper;
import com.jcabi.aspects.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

@Service(value = "DraftItemValidationService")
@Loggable(Loggable.DEBUG)
public class DraftItemValidationServiceImpl extends BaseItemValidationService implements DraftItemValidationService
{
    @Autowired
    private ValidationService validationService;

    @Autowired
    private AssetValidationService assetValidationService;

    @Autowired
    private VersioningHelper versioningHelper;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private AssetDao assetDao;

    private static final String ITEM_NOT_FOUND = "Item %d does not exist";

    /**
     * Validates that the asset and draft item that the new draft items are to be added to exist and that the new items
     * are of the correct type to be the child of the existing draft item and that the maxOccurs of the parent will not
     * be exceeded by the addition.
     * <p>
     * Then delegates to any process specific validation (From ref data, recursive or manual)
     *
     * @param inputAssetId
     * @param draftItemList
     * @throws BadRequestException
     * @throws RecordNotFoundException
     * @throws MastBusinessException
     */

    public void validateAppendDraftItems(final String inputAssetId, final DraftItemListDto draftItemList)
            throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        this.validationService.verifyBody(draftItemList);
        this.assetValidationService.verifyAssetIdExists(inputAssetId);

        final List<DraftItemDto> draftItemDtoList = draftItemList.getDraftItemDtoList();

        if (draftItemDtoList == null || draftItemDtoList.isEmpty())
        {
            throw new MastBusinessException(
                    String.format(ExceptionMessagesUtils.EMPTY_DRAFT_ITEM_LIST, inputAssetId));
        }

        final Long assetId = Long.parseLong(inputAssetId);

        // General validation based on type compatibility for parent-child relationship
        for (final DraftItemDto draftItemDto : draftItemDtoList)
        {
            this.verifyAssetHasItem(inputAssetId, draftItemDto.getToParentId() == null ? null : draftItemDto.getToParentId().toString());
        }

        final Integer assetCategoryId = this.assetDao.getAssetCategory(assetId).intValue();

        if (draftItemDtoList.get(0).getItemTypeId() != null) // From reference Data
        {
            validateAppendDraftItemFromReferenceData(draftItemList, assetCategoryId, assetId);
        }
        else if (draftItemDtoList.get(0).getItemRelationshipDto() != null) // Explicit, non-recursive copy
        {
            validateAppendSpecificCopiedItems(draftItemList, assetCategoryId);
        }
        else // Full recursive copy
        {
            validateAppendCopiedAssetModel(draftItemList, assetCategoryId);
        }

        validateChildCounts(draftItemList, assetCategoryId, assetId);
    }

    /**
     * Validates that each child is of the correct type to be added to the give parent.
     *
     * @param draftItemList
     * @param assetCategoryId
     * @throws MastBusinessException
     */
    private void validateAppendDraftItemFromReferenceData(final DraftItemListDto draftItemList, final Integer assetCategoryId, final Long assetId)
            throws MastBusinessException
    {
        for (final DraftItemDto draftItemDto : draftItemList.getDraftItemDtoList())
        {
            final VersionedItemPK itemPK = this.versioningHelper.getVersionedItemPK(draftItemDto.getToParentId(), assetId, null, true);
            final LazyItemDto parentDraftItem = this.itemDao.getItem(itemPK);
            validateChildItemType(draftItemDto.getItemTypeId(), parentDraftItem.getItemType().getId(), assetCategoryId, parentDraftItem.getId());
        }
    }

    /**
     * Validates that each child is of the correct type to be added to the give parent.
     *
     * @param draftItemList
     * @param assetCategoryId
     * @throws MastBusinessException
     */
    private void validateAppendCopiedAssetModel(final DraftItemListDto draftItemList, final Integer assetCategoryId) throws MastBusinessException
    {
        for (final DraftItemDto newDraftItem : draftItemList.getDraftItemDtoList())
        {
            final VersionedItemPK toItemPK = this.versioningHelper
                    .getVersionedItemPK(newDraftItem.getToParentId(), newDraftItem.getToAssetId(), null, true);
            final LazyItemDto parentDraftItem = this.itemDao.getItem(toItemPK);

            final VersionedItemPK newItemPK = this.versioningHelper
                    .getVersionedItemPK(newDraftItem.getFromId(), newDraftItem.getFromAssetId(), null,
                            Objects.equals(newDraftItem.getFromAssetId(), newDraftItem.getToAssetId()));
            final LazyItemDto newItem = this.itemDao.getItem(newItemPK);

            validateChildItemType(newItem.getItemType().getId(), parentDraftItem.getItemType().getId(), assetCategoryId, parentDraftItem.getId());
        }
    }

    /**
     * Validates that each child is of the correct type to be added to the give parent. And that any relationships given
     * are valid.
     *
     * @param draftItemList
     * @param assetCategoryId
     * @throws MastBusinessException
     * @throws RecordNotFoundException
     */
    private void validateAppendSpecificCopiedItems(final DraftItemListDto draftItemList, final Integer assetCategoryId)
            throws MastBusinessException, RecordNotFoundException
    {
        for (final DraftItemDto newItem : draftItemList.getDraftItemDtoList())
        {
            final VersionedItemPK toItemPK = this.versioningHelper.getVersionedItemPK(newItem.getToParentId(), newItem.getToAssetId(), null, true);
            final LazyItemDto toItem = this.itemDao.getItem(toItemPK);

            final VersionedItemPK newItemPK = this.versioningHelper.getVersionedItemPK(newItem.getFromId(), newItem.getFromAssetId(), null, false);
            final LazyItemDto fromItem = this.itemDao.getItem(newItemPK);

            validateChildItemType(fromItem.getItemType().getId(), toItem.getItemType().getId(), assetCategoryId, toItem.getId());
            validateItemRelationships(fromItem, newItem.getItemRelationshipDto(), newItem.getFromAssetId());
        }
    }

    /**
     * Loops through the input item list and counts the children of each type and then for each type verifies that the
     * proposed parent can accept this number of children.
     *
     * @param draftItemList
     * @param assetCategoryId
     * @throws MastBusinessException
     */
    private void validateChildCounts(final DraftItemListDto draftItemList, final Integer assetCategoryId, final Long assetId)
            throws MastBusinessException, RecordNotFoundException
    {
        final VersionedItemPK toItemPK = this.versioningHelper
                .getVersionedItemPK(draftItemList.getDraftItemDtoList().get(0).getToParentId(), assetId, null, true);

        final LazyItemDto parentDraftItem = this.itemDao.getItem(toItemPK);

        final Map<Long, Integer> typeCount = new HashMap<Long, Integer>();

        for (final DraftItemDto draftItemDto : draftItemList.getDraftItemDtoList())
        {
            final Long itemTypeId = getItemType(draftItemDto);
            if (typeCount.keySet().contains(itemTypeId))
            {
                typeCount.put(itemTypeId, typeCount.get(itemTypeId) + 1);
            }
            else
            {
                typeCount.put(itemTypeId, 1);
            }
        }

        for (final Entry<Long, Integer> type : typeCount.entrySet())
        {
            validateWithinMaxOccurs(parentDraftItem, type.getKey(), type.getValue(), assetCategoryId);
        }
    }

    private Long getItemType(final DraftItemDto draftItemDto) throws RecordNotFoundException
    {
        Long itemTypeId;
        if (draftItemDto.getItemTypeId() != null)
        {
            itemTypeId = draftItemDto.getItemTypeId();
        }
        else
        {
            final VersionedItemPK newItemPK = this.versioningHelper
                    .getVersionedItemPK(draftItemDto.getFromId(), draftItemDto.getFromAssetId(), null, false);

            final LazyItemDto itemDto = this.itemDao.getItem(newItemPK);

            if (itemDto.getId() == null)
            {
                throw new RecordNotFoundException(String.format(ITEM_NOT_FOUND, draftItemDto.getFromId()));
            }
            itemTypeId = itemDto.getItemType().getId();
        }
        return itemTypeId;
    }

    /**
     * Loops through and descends into a nested list of relationships validating that the items in the database do
     * indeed have that relationship.
     *
     * @param item
     * @param relationships
     * @throws RecordNotFoundException
     */
    private void validateItemRelationships(final LazyItemDto item, final List<DraftItemRelationshipDto> relationships, final Long fromAssetId)
            throws RecordNotFoundException
    {
        for (final DraftItemRelationshipDto relationship : relationships)
        {
            if (!verifyRelationship(relationship, item))
            {
                throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.INVALID_RELATIONSHIP, "Items", item.getId(), item.getName(),
                        relationship.getOriginalItemId()));
            }

            if (relationship.getItemRelationshipDto() != null && !relationship.getItemRelationshipDto().isEmpty())
            {
                final VersionedItemPK toItemPK = this.versioningHelper.getVersionedItemPK(relationship.getOriginalItemId(), fromAssetId, null, false);
                final LazyItemDto parentItem = this.itemDao.getItem(toItemPK);
                validateItemRelationships(parentItem, relationship.getItemRelationshipDto(), fromAssetId);
            }
        }
    }

    /**
     * Verifies that an individual relationship is correct. ie that the two items do have that relationship in the
     * database. Or if no relationship is given that they have a parent child relationship in the database.
     *
     * @param draftRelationship
     * @return
     */

    private boolean verifyRelationship(final DraftItemRelationshipDto draftRelationship, final LazyItemDto lazyItemDto)
    {
        Boolean valid = false;

        for (final LinkVersionedResource originalRelationship : lazyItemDto.getRelated()) //NON-PARENT CHILD RELATIONSHIPS
        {
            final boolean relationshipsMatch = RelationshipType.IS_RELATED_TO.getValue().equals(draftRelationship.getRelationshipTypeId());

            if (originalRelationship.getId().equals(draftRelationship.getOriginalItemId()) && relationshipsMatch)
            {
                valid = true;
                break;
            }
        }

        for (final LinkVersionedResource originalRelationship : lazyItemDto.getItems()) // PARENT CHILD RELATIONSHIPS
        {
            final boolean isChildRelationshipByDefault = draftRelationship.getRelationshipTypeId() == null;
            final boolean relationshipsMatch = RelationshipType.IS_PART_OF.getValue().equals(draftRelationship.getRelationshipTypeId());

            if (originalRelationship.getId().equals(draftRelationship.getOriginalItemId()) && (isChildRelationshipByDefault || relationshipsMatch))
            {
                valid = true;
                break;
            }
        }
        return valid;
    }
}
