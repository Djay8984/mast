package com.baesystems.ai.lr.validation.service;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueRelationshipDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.ServiceCatalogueRelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(ServiceValidationHelper.class)
public class ServiceValidationHelperTest
{
    @Mock
    private ServiceService serviceService;

    @Mock
    private ValidationService validationService;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private ProductDao productDao;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @InjectMocks
    private final ServiceValidationHelper serviceValidationHelper = new ServiceValidationHelper();

    @Rule
    private final ExpectedException thrown = ExpectedException.none();

    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L;
    private static final Long PRODUCT_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_2_ID = 2L;
    private static final Long PRODUCT_CATALOGUE_1_ID = 1L;

    private final ProductDO product = new ProductDO();
    private final ProductCatalogueDO productCatalogue = new ProductCatalogueDO();
    private final ServiceCatalogueDO serviceCatalogue = new ServiceCatalogueDO();

    @Before
    public void setUp() throws JsonParseException, JsonMappingException, IOException, ParseException
    {
        productCatalogue.setId(PRODUCT_CATALOGUE_1_ID);

        product.setId(PRODUCT_1_ID);
        product.setProductCatalogue(new LinkDO());
        product.getProductCatalogue().setId(PRODUCT_1_ID);

        serviceCatalogue.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue.setProductCatalogue(productCatalogue);
    }

    @Test
    public void validateProductFails() throws RecordNotFoundException, BadRequestException
    {
        when(this.validationService.verifyNumberFormat(String.valueOf(PRODUCT_1_ID))).thenReturn(PRODUCT_1_ID);
        when(this.productDao.getProductDO(PRODUCT_1_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.serviceValidationHelper.validateProduct(String.valueOf(PRODUCT_1_ID));
    }

    @Test
    public void validateProductPasses() throws RecordNotFoundException, BadRequestException
    {
        when(this.validationService.verifyNumberFormat(String.valueOf(PRODUCT_1_ID))).thenReturn(PRODUCT_1_ID);
        when(this.productDao.getProductDO(PRODUCT_1_ID)).thenReturn(this.product);

        final ProductDO validatedProduct = this.serviceValidationHelper.validateProduct(String.valueOf(PRODUCT_1_ID));

        assertEquals(this.product, validatedProduct);
    }

    @Test
    public void validateServiceRelationshipsPassesDependencyPresent() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueRelationshipDto dependentRelationship = new ServiceCatalogueRelationshipDto();
        dependentRelationship.setFromServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_1_ID));
        dependentRelationship.setToServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_2_ID));
        dependentRelationship.setRelationshipType(new LinkResource(ServiceCatalogueRelationshipType.IS_DEPENDENT_ON.getValue()));

        final ServiceCatalogueRelationshipDto dependencyRelationship = new ServiceCatalogueRelationshipDto();
        dependencyRelationship.setFromServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_2_ID));
        dependencyRelationship.setToServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_1_ID));
        dependencyRelationship.setRelationshipType(new LinkResource(ServiceCatalogueRelationshipType.IS_DEPENDENT_ON.getValue()));

        final ServiceCatalogueDto dependent = new ServiceCatalogueDto();
        dependent.setId(SERVICE_CATALOGUE_1_ID);
        dependent.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        dependent.getRelationships().add(dependentRelationship);

        final ServiceCatalogueDto dependency = new ServiceCatalogueDto();
        dependency.setId(SERVICE_CATALOGUE_2_ID);
        dependency.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        dependency.getRelationships().add(dependencyRelationship);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(dependency);
        serviceCatalogueList.add(dependent);

        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogueList);
    }

    @Test
    public void validateServiceRelationshipsPassesNoDependency() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue1);

        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogueList);
    }

    @Test
    public void validateServiceRelationshipsFailsDependencyNotPresent() throws MastBusinessException, BadRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.DEPENDENCY_ERROR, "service catalogue", SERVICE_CATALOGUE_1_ID, "all",
                Collections.singletonList(SERVICE_CATALOGUE_2_ID).toString()));

        final ServiceCatalogueRelationshipDto dependentRelationship = new ServiceCatalogueRelationshipDto();
        dependentRelationship.setFromServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_1_ID));
        dependentRelationship.setToServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_2_ID));
        dependentRelationship.setRelationshipType(new LinkResource(ServiceCatalogueRelationshipType.IS_DEPENDENT_ON.getValue()));

        final ServiceCatalogueDto dependent = new ServiceCatalogueDto();
        dependent.setId(SERVICE_CATALOGUE_1_ID);
        dependent.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        dependent.getRelationships().add(dependentRelationship);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(dependent);

        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogueList);
    }

    @Test
    public void validateServiceRelationshipsFailsCounterpartPresent() throws MastBusinessException, BadRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.MUTUALLY_EXCLUSIVE_ERROR, "service catalogue", SERVICE_CATALOGUE_2_ID,
                "any", Collections.singletonList(SERVICE_CATALOGUE_1_ID).toString())
                + String.format(ExceptionMessagesUtils.MUTUALLY_EXCLUSIVE_ERROR, "service catalogue", SERVICE_CATALOGUE_1_ID,
                        "any", Collections.singletonList(SERVICE_CATALOGUE_2_ID).toString()));

        final ServiceCatalogueRelationshipDto relationship1 = new ServiceCatalogueRelationshipDto();
        relationship1.setFromServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_1_ID));
        relationship1.setToServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_2_ID));
        relationship1.setRelationshipType(new LinkResource(ServiceCatalogueRelationshipType.IS_COUNTERPART_OF.getValue()));

        final ServiceCatalogueRelationshipDto relationship2 = new ServiceCatalogueRelationshipDto();
        relationship2.setFromServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_2_ID));
        relationship2.setToServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_1_ID));
        relationship2.setRelationshipType(new LinkResource(ServiceCatalogueRelationshipType.IS_COUNTERPART_OF.getValue()));

        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.getRelationships().add(relationship1);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_2_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.getRelationships().add(relationship2);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        this.serviceValidationHelper.validateServiceRelationships(serviceCatalogueList);
    }

    @Test
    public void validateServiceRelationshipsFailsNullList() throws MastBusinessException, BadRequestException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "The list of services"));

        this.serviceValidationHelper.validateServiceRelationships(null);
    }

    @Test
    public void validateServiceDulicationPassesNonMultiple() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(false);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_2_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(false);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);

        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_2_ID);

        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDulicationFailsNonMultiple() throws MastBusinessException, BadRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.DUPLICATE_ERROR, "non-multiple service", "service catalogue id",
                Collections.singletonList(SERVICE_CATALOGUE_1_ID).toString()));

        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(false);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(false);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service1.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));


        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service2.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));


        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDulicationPassesMultipleDifferentServiceCatalogues() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(true);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_2_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(true);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service1.setAssetItem(new LinkResource(ITEM_1_ID));

        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_2_ID);
        service2.setAssetItem(new LinkResource(ITEM_1_ID));

        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDulicationPassesMultipleDifferentItems() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(true);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(true);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service1.setAssetItem(new LinkResource(ITEM_1_ID));
        service1.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));

        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service2.setAssetItem(new LinkResource(ITEM_2_ID));
        service2.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));


        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDulicationFailsMultiple() throws MastBusinessException, BadRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.DUPLICATE_ERROR, "multiple service", "service catalogue id and item id",
                Collections.singletonList(SERVICE_CATALOGUE_1_ID).toString()));

        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(true);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(true);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service1.setAssetItem(new LinkResource(ITEM_1_ID));
        service1.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));

        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service2.setAssetItem(new LinkResource(ITEM_1_ID));
        service2.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));

        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDuplicationPassesMultipleIncludingClosed() throws MastBusinessException, BadRequestException
    {
        final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
        serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue1.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue1.setMultipleIndicator(true);

        final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();
        serviceCatalogue2.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue2.setRelationships(new ArrayList<ServiceCatalogueRelationshipDto>());
        serviceCatalogue2.setMultipleIndicator(true);

        final List<ServiceCatalogueDto> serviceCatalogueList = new ArrayList<ServiceCatalogueDto>();
        serviceCatalogueList.add(serviceCatalogue2);
        serviceCatalogueList.add(serviceCatalogue1);

        final ScheduledServiceDto service1 = new ScheduledServiceDto();
        service1.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service1.setAssetItem(new LinkResource(ITEM_1_ID));
        service1.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));

        final ScheduledServiceDto service2 = new ScheduledServiceDto();
        service2.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service2.setAssetItem(new LinkResource(ITEM_1_ID));
        service2.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.COMPLETE.value()));

        final List<ScheduledServiceDto> serviceList = new ArrayList<ScheduledServiceDto>();
        serviceList.add(service2);
        serviceList.add(service1);

        this.serviceValidationHelper.validateServiceDuplication(serviceCatalogueList, serviceList);
    }

    @Test
    public void validateServiceDulicationFailsNullLists() throws MastBusinessException, BadRequestException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "The list of services"));

        this.serviceValidationHelper.validateServiceDuplication(null, null);
    }
}
