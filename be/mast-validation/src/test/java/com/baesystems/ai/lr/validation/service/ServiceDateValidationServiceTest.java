package com.baesystems.ai.lr.validation.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(MockitoJUnitRunner.class)
public class ServiceDateValidationServiceTest
{
    @Mock
    private ValidationService validationService;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private ServiceService serviceService;

    @Mock
    private ServiceValidationHelper serviceValidationHelper;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Mock
    private DateHelper dateHelper;

    @Mock
    private AssetValidationService assetValidationService;

    @InjectMocks
    private final ServiceDateValidationServiceImpl serviceDateValidationService = new ServiceDateValidationServiceImpl();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static final Long SERVICE_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_1_ID = 1L;

    private static final int THREE_MONTHS = 3;

    private static final DateFormat SHORT_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat HARMONISATION_ERROR_DATE_FORMATTER = new SimpleDateFormat("dd MMM yyyy");
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    private Date threeMonthsFromNow = new Date();
    private String threeMonthsFromNowDateString = new String();

    private ScheduledServiceDto updateableService;

    private Date assignedDate;
    private Date dueDate;
    private Date lowerRangeDate;
    private Date upperRangeDate;

    @Before
    public void setUp() throws JsonParseException, JsonMappingException, IOException, ParseException
    {

        this.assignedDate = SHORT_DATE_FORMATTER.parse("2015-10-25");
        this.dueDate = SHORT_DATE_FORMATTER.parse("2016-10-25");
        this.lowerRangeDate = SHORT_DATE_FORMATTER.parse("2016-10-15");
        this.upperRangeDate = SHORT_DATE_FORMATTER.parse("2016-11-05");
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, THREE_MONTHS);
        this.threeMonthsFromNow = cal.getTime();
        this.threeMonthsFromNowDateString = HARMONISATION_ERROR_DATE_FORMATTER.format(this.threeMonthsFromNow);

        this.updateableService = getService(SERVICE_1_ID);
    }

    private ScheduledServiceDto getService(final Long serviceId) throws ParseException
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();

        service.setId(serviceId);
        service.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service.setAssignedDateManual(this.assignedDate);
        service.setDueDateManual(this.dueDate);
        service.setLowerRangeDateManual(this.lowerRangeDate);
        service.setUpperRangeDateManual(this.upperRangeDate);

        return service;
    }

    @Test
    public void assignedDateAfterDueDateFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        this.updateableService.setAssignedDateManual(this.updateableService.getDueDateManual());

        final Calendar cal = Calendar.getInstance();
        cal.setTime(this.updateableService.getAssignedDateManual());
        cal.add(Calendar.DAY_OF_MONTH, -1);

        this.updateableService.setDueDateManual(cal.getTime());

        final String expectedMessage = "The passed Due Date ["
                + SHORT_DATE_FORMATTER.format(cal.getTime())
                + "] must be after the asset build date of ["
                + SHORT_DATE_FORMATTER.format(this.updateableService.getAssignedDateManual())
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateAssignedDate(this.updateableService);
    }

    @Test
    public void assignedDateSameAsDueDateFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        this.updateableService.setAssignedDateManual(this.updateableService.getDueDateManual());

        this.updateableService.setDueDateManual(this.updateableService.getAssignedDateManual());

        final String expectedMessage = "The passed Due Date ["
                + SHORT_DATE_FORMATTER.format(this.updateableService.getDueDateManual())
                + "] must be after the asset build date of ["
                + SHORT_DATE_FORMATTER.format(this.updateableService.getAssignedDateManual())
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateAssignedDate(this.updateableService);
    }

    @Test
    public void assignedDateThreeMonthsAndOneDayFromNowFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);

        final Calendar cal = Calendar.getInstance();
        cal.setTime(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));
        cal.add(Calendar.DATE, 1);

        this.updateableService.setAssignedDateManual(cal.getTime());
        this.thrown.expect(BadRequestException.class);

        final String expectedMessage = "The passed Assigned Date ["
                + SHORT_DATE_FORMATTER.format(cal.getTime())
                + "] must be before ["
                + SHORT_DATE_FORMATTER.format(this.threeMonthsFromNow)
                + "].";

        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateAssignedDate(this.updateableService);
    }

    @Test
    public void dueDateBeforeLowerRangeFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        final Calendar dueDateBeforeLower = Calendar.getInstance();
        dueDateBeforeLower.setTime(this.lowerRangeDate);
        dueDateBeforeLower.add(Calendar.DATE, -1);
        final Date newDueDate = dueDateBeforeLower.getTime();
        this.updateableService.setDueDateManual(newDueDate);

        final String expectedMessage = "The passed Due Date ["
                + SHORT_DATE_FORMATTER.format(newDueDate)
                + "] must not be after the lower range date of ["
                + SHORT_DATE_FORMATTER.format(this.updateableService.getLowerRangeDateManual())
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateDateRanges(this.updateableService);
    }

    @Test
    public void dueDateAfterUpperRangeFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        final Calendar dueDateAfterUpperLower = Calendar.getInstance();
        dueDateAfterUpperLower.setTime(this.upperRangeDate);
        dueDateAfterUpperLower.add(Calendar.MONTH, 1);
        final Date newDueDate = dueDateAfterUpperLower.getTime();
        this.updateableService.setDueDateManual(newDueDate);

        final String expectedMessage = "The passed Due Date ["
                + SHORT_DATE_FORMATTER.format(newDueDate)
                + "] must be before ["
                + SHORT_DATE_FORMATTER.format(this.upperRangeDate)
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateDateRanges(this.updateableService);
    }

    @Test
    public void assignedDateAfterLowerRangeFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        final Calendar assignedDateAfterLower = Calendar.getInstance();
        assignedDateAfterLower.setTime(this.lowerRangeDate);
        assignedDateAfterLower.add(Calendar.DATE, 1);
        final Date newAssignedDate = assignedDateAfterLower.getTime();
        this.updateableService.setAssignedDateManual(newAssignedDate);

        final String expectedMessage = "The passed Assigned Date ["
                + SHORT_DATE_FORMATTER.format(newAssignedDate)
                + "] must be before ["
                + SHORT_DATE_FORMATTER.format(this.lowerRangeDate)
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateDateRanges(this.updateableService);
    }

    @Test
    public void assignedDateEqualsLowerRangeFails() throws MastBusinessException, ParseException, BadRequestException
    {
        this.updateableService = getService(SERVICE_1_ID);
        this.updateableService.setAssignedDateManual(this.lowerRangeDate);

        final String expectedMessage = "The passed Assigned Date ["
                + SHORT_DATE_FORMATTER.format(this.updateableService.getAssignedDateManual())
                + "] must be before ["
                + SHORT_DATE_FORMATTER.format(this.lowerRangeDate)
                + "].";

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(expectedMessage);

        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(this.threeMonthsFromNowDateString));

        this.serviceDateValidationService.validateDateRanges(this.updateableService);
    }
}
