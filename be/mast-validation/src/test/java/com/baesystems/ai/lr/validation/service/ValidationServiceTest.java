package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.when;

import java.util.HashSet;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.base.Dto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.ValidationDataService;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest
{
    private static final String VALID_NUMBER = "1";
    private static final String INVALID_NUMBER = "invalid";

    @Mock
    private Validator validator;

    @Mock
    private ValidationDataService validationDataService;

    @InjectMocks
    private final ValidationServiceImpl validationService = new ValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void verifyValidIdTest() throws BadRequestException
    {
        this.validationService.verifyId(VALID_NUMBER);
    }

    @Test(expected = BadRequestException.class)
    public void verifyInvalidIdTest() throws BadRequestException
    {
        this.validationService.verifyId(INVALID_NUMBER);
    }

    @Test
    public void verifyIdNullTest() throws BadRequestException
    {
        this.validationService.verifyIdNull(null);
    }

    @Test(expected = BadRequestException.class)
    public void verifyIdNotNullTest() throws BadRequestException
    {
        this.validationService.verifyIdNull(Long.parseLong(VALID_NUMBER));
    }

    @Test(expected = BadRequestException.class)
    public void verifyBodyNullTest() throws BadRequestException
    {
        this.validationService.verifyBody(null);
    }

    @Test
    public void verifyBodyNotNullTest() throws BadRequestException
    {
        final Dto base = new BaseDto();
        when(this.validator.validate(base)).thenReturn(new HashSet<ConstraintViolation<Dto>>());
        this.validationService.verifyBody(base);
    }

    @Test
    public void verifyFieldEqualTest() throws BadRequestException
    {
        this.validationService.verifyField(VALID_NUMBER, VALID_NUMBER, "");
    }

    @Test
    public void verifyFieldBothNullTest() throws BadRequestException
    {
        this.validationService.verifyField(null, null, "");
    }

    @Test
    public void verifyFieldNullFieldTest() throws BadRequestException
    {
        thrown.expect(BadRequestException.class);
        this.validationService.verifyField(null, VALID_NUMBER, "");
    }

    @Test
    public void verifyFieldNullHeaderTest() throws BadRequestException
    {
        thrown.expect(BadRequestException.class);
        this.validationService.verifyField(VALID_NUMBER, null, "");
    }

    @Test
    public void verifyFieldNotEqualTest() throws BadRequestException
    {
        thrown.expect(BadRequestException.class);
        this.validationService.verifyField(VALID_NUMBER, INVALID_NUMBER, "");
    }
}
