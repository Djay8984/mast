package com.baesystems.ai.lr.validation.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.domain.mast.repositories.SupplementaryInformationRepository;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.dto.validation.AttributeRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;
import com.baesystems.ai.lr.dto.validation.ItemTemplateDto;

@RunWith(MockitoJUnitRunner.class)
public class ValidationDataServiceTest
{
    private static final Long ITEM_TYPE_KEY_1 = 1L;
    private static final Long ITEM_TYPE_KEY_2 = 2L;
    private static final Long CASE_ID_1 = 1L;
    private static final Long CASE_ID_2 = 2L;
    private static final Long ASSET_ID = 4L;
    private static final Long IMO_NUMBER = 1L;
    private static final String BUILDER = "1";
    private static final String YARD_NUMBER = "Y0001";
    private static final Long BUSINESS_PROCESS = 1L;
    private static final Long CASE_STATUS_ID_1 = 1L;
    private static final Integer ZERO = 0;
    private static final Integer ONE = 1;
    private static final Integer TWO = 2;

    private static List<ItemRuleDto> itemRules;
    private static List<AttributeRuleDto> attributeRules;
    private static IhsAssetDto ihsAsset;
    private static AssetLightDto asset;
    private static List<Long> cases;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private IhsAssetDao ihsAssetDao;

    @Mock
    private CaseDao caseDao;

    @Mock
    private SupplementaryInformationRepository supplementaryInformationRepository;

    @Mock
    private ItemTypeDao itemTypeDao;

    @InjectMocks
    private final ValidationDataServiceImpl validationDataService = new ValidationDataServiceImpl();

    @BeforeClass
    public static void startUp()
    {
        itemRules = new ArrayList<ItemRuleDto>();
        itemRules.add(createItemRule(ITEM_TYPE_KEY_1));
        itemRules.add(createItemRule(ITEM_TYPE_KEY_2));

        attributeRules = new ArrayList<AttributeRuleDto>();
        attributeRules.add(createAttributeRule(ITEM_TYPE_KEY_1));
        attributeRules.add(createAttributeRule(ITEM_TYPE_KEY_2));

        cases = new ArrayList<Long>();
        cases.add(CASE_ID_1);
        cases.add(CASE_ID_2);

        createIhsAsset();
        createAsset();
    }

    /**
     * Verifies asset model template for existing rules for both items and attributes.
     */
    @Test
    public void testGetAssetModelTemplate()
    {
        when(this.itemTypeDao.getItemTypeRelationships()).thenReturn(itemRules);
        when(this.assetReferenceDataDao.getAttributeRules()).thenReturn(attributeRules);

        final AssetModelTemplateDto assetModel = validationDataService.getAssetModelTemplate();
        assertNotNull(assetModel);

        final Map<Long, ItemTemplateDto> itemTemplateMap = assetModel.getAssetModelTemplate();
        assertNotNull(itemTemplateMap);
        assertFalse(itemTemplateMap.isEmpty());
        assertTrue(itemTemplateMap.containsKey(ITEM_TYPE_KEY_1));
        assertTrue(itemTemplateMap.containsKey(ITEM_TYPE_KEY_2));

        final ItemTemplateDto itemTemplate1 = itemTemplateMap.get(ITEM_TYPE_KEY_1);
        final ItemTemplateDto itemTemplate2 = itemTemplateMap.get(ITEM_TYPE_KEY_2);

        assertNotNull(itemTemplate1.getValidAttributes());
        assertNotNull(itemTemplate2.getValidAttributes());

        assertNotNull(itemTemplate1.getValidItems());
        assertNotNull(itemTemplate2.getValidItems());

        assertFalse(itemTemplate1.getValidAttributes().isEmpty());
        assertFalse(itemTemplate2.getValidAttributes().isEmpty());

        assertFalse(itemTemplate1.getValidItems().isEmpty());
        assertFalse(itemTemplate2.getValidItems().isEmpty());
    }

    /**
     * Verifies if get asset model template API's doesn't fail if there are no rules for items or attributes.
     */
    @Test
    public void testGetAssetModelEmptyTemplate()
    {
        when(this.itemTypeDao.getItemTypeRelationships()).thenReturn(null);
        when(this.assetReferenceDataDao.getAttributeRules()).thenReturn(null);

        final AssetModelTemplateDto assetModel = validationDataService.getAssetModelTemplate();
        assertNotNull(assetModel);
        assertNotNull(assetModel.getAssetModelTemplate());
        assertTrue(assetModel.getAssetModelTemplate().isEmpty());
    }

    private static ItemRuleDto createItemRule(final Long version)

    {
        final ItemRuleDto itemRule = new ItemRuleDto();
        itemRule.setFromItemType(version);
        itemRule.setToItemType(version);
        itemRule.setRelationshipTypeId(version);
        itemRule.setMaxOccurs(TWO);
        itemRule.setMinOccurs(ZERO);

        return itemRule;
    }

    private static AttributeRuleDto createAttributeRule(final Long version)
    {
        final AttributeRuleDto attributeRule = new AttributeRuleDto();
        attributeRule.setAttributeTypeId(version);
        attributeRule.setItemTypeId(version);
        attributeRule.setMinOccurs(ONE);
        attributeRule.setMaxOccurs(ONE);
        attributeRule.setTransferable(true);

        return attributeRule;
    }

    /**
     * Verifies when case validation finds ihs asset, ids asset and cases for the given data.
     */
    @Test
    public void testVerifyCase()
    {
        when(this.ihsAssetDao.getIhsAsset(IMO_NUMBER)).thenReturn(ihsAsset);
        when(this.assetDao.getAssetByImoNumber(IMO_NUMBER)).thenReturn(asset);
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(IMO_NUMBER, BUILDER, YARD_NUMBER)).thenReturn(ASSET_ID);
        when(this.caseDao.findDuplicateCasesByAssetId(ASSET_ID, BUSINESS_PROCESS)).thenReturn(cases);

        final CaseValidationDto caseValidation = this.validationDataService.verifyCase(IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS);

        assertNotNull(caseValidation);

        assertNotNull(caseValidation.getIdsAsset());
        assertEquals(ASSET_ID, caseValidation.getIdsAsset().getId());

        assertNotNull(caseValidation.getIhsAsset());
        assertEquals(IMO_NUMBER, caseValidation.getIhsAsset().getId());

        assertNotNull(caseValidation.getCases());
        assertFalse(caseValidation.getCases().isEmpty());
        assertEquals(cases.size(), caseValidation.getCases().size());
    }

    /**
     * Verifies when case validation finds ihs asset and ids asset for the given data.
     */
    @Test
    public void testVerifyCaseNullCases()
    {
        when(this.ihsAssetDao.getIhsAsset(IMO_NUMBER)).thenReturn(ihsAsset);
        when(this.assetDao.getAssetByImoNumber(IMO_NUMBER)).thenReturn(asset);
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(IMO_NUMBER, BUILDER, YARD_NUMBER)).thenReturn(ASSET_ID);
        when(this.validationDataService.findDuplicateCases(BUSINESS_PROCESS, ASSET_ID)).thenReturn(Collections.<Long>emptyList());

        final CaseValidationDto caseValidation = this.validationDataService.verifyCase(IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS);

        assertNotNull(caseValidation);

        assertNotNull(caseValidation.getIdsAsset());
        assertEquals(ASSET_ID, caseValidation.getIdsAsset().getId());

        assertNotNull(caseValidation.getIhsAsset());
        assertEquals(IMO_NUMBER, caseValidation.getIhsAsset().getId());

        assertNotNull(caseValidation.getCases());
        assertTrue(caseValidation.getCases().isEmpty());
    }

    /**
     * Verifies when case validation finds ids asset and cases for the given data.
     */
    @Test
    public void testVerifyCaseNullIhsAsset()
    {
        when(this.ihsAssetDao.getIhsAsset(IMO_NUMBER)).thenReturn(null);
        when(this.assetDao.getAssetByImoNumber(IMO_NUMBER)).thenReturn(asset);
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(IMO_NUMBER, BUILDER, YARD_NUMBER)).thenReturn(ASSET_ID);
        when(this.validationDataService.findDuplicateCases(BUSINESS_PROCESS, ASSET_ID)).thenReturn(cases);

        final CaseValidationDto caseValidation = this.validationDataService.verifyCase(IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS);

        assertNotNull(caseValidation);

        assertNotNull(caseValidation.getIdsAsset());
        assertEquals(ASSET_ID, caseValidation.getIdsAsset().getId());

        assertNull(caseValidation.getIhsAsset());

        assertNotNull(caseValidation.getCases());
        assertFalse(caseValidation.getCases().isEmpty());
        assertEquals(cases.size(), caseValidation.getCases().size());
    }

    /**
     * Verifies when case validation finds ihs asset and cases for the given data.
     */
    @Test
    public void testVerifyCaseNullAsset()
    {
        when(this.ihsAssetDao.getIhsAsset(IMO_NUMBER)).thenReturn(ihsAsset);
        when(this.assetDao.getAssetByImoNumber(IMO_NUMBER)).thenReturn(null);
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(IMO_NUMBER, BUILDER, YARD_NUMBER)).thenReturn(ASSET_ID);
        when(this.validationDataService.findDuplicateCases(BUSINESS_PROCESS, ASSET_ID)).thenReturn(cases);

        final CaseValidationDto caseValidation = this.validationDataService.verifyCase(IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS);

        assertNotNull(caseValidation);

        assertNull(caseValidation.getIdsAsset());

        assertNotNull(caseValidation.getIhsAsset());
        assertEquals(IMO_NUMBER, caseValidation.getIhsAsset().getId());

        assertNotNull(caseValidation.getCases());
        assertFalse(caseValidation.getCases().isEmpty());
        assertEquals(cases.size(), caseValidation.getCases().size());
    }

    private static void createIhsAsset()
    {
        ihsAsset = new IhsAssetDto();
        ihsAsset.setId(IMO_NUMBER);
    }

    private static void createAsset()
    {
        asset = new AssetLightDto();
        asset.setId(ASSET_ID);
    }

    /**
     * Verifies getting the case status for an ID.
     */
    @Test
    public void testGetCurrentStatusForCase()
    {
        when(this.caseDao.getCaseStatusId(CASE_ID_1)).thenReturn(CASE_STATUS_ID_1);
        final Long currentStatus = this.validationDataService.getCurrentStatusForCase(CASE_ID_1);
        assertEquals(CASE_STATUS_ID_1, currentStatus);
    }
}
