package com.baesystems.ai.lr.validation.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.DefectStatusType;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.reports.ReportService;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.UpdateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.validation.utils.ValidationMessageUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class ReportValidationServiceTest
{
    private static ObjectMapper objectMapper;

    @Mock
    private ValidationService validationService;

    @Mock
    private JobValidationService jobValidationService;

    @Mock
    private UpdateValidationService updateValidationService;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private WIPDefectDao wipDefectDao;

    @Mock
    private SurveyDao surveyDao;

    @Mock
    private ReportService reportService;

    @Mock
    private ReportDao reportDao;

    @InjectMocks
    private final ReportValidationServiceImpl reportValidationService = new ReportValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long VALID_JOB_ID = 1L;
    private static final Long VALID_REPORT_ID = 1L;
    private static final Long SURVEY_1_ID = 1L;
    private static final Long DEFECT_1_ID = 1L;
    private static final Long DEFECT_2_ID = 2L;
    private static final List<DefectDto> DEFECTS = new ArrayList<DefectDto>();
    private static final List<Long> SURVEY_IDS = new ArrayList<Long>();

    private ReportDto reportDto;

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();

        final DefectDto defect1 = new DefectDto();
        defect1.setId(DEFECT_1_ID);
        final DefectDto defect2 = new DefectDto();
        defect2.setId(DEFECT_2_ID);
        DEFECTS.add(defect1);
        DEFECTS.add(defect2);

        SURVEY_IDS.add(SURVEY_1_ID);
    }

    @Before
    public void setUp() throws JsonParseException, JsonMappingException, IOException, RecordNotFoundException, BadPageRequestException
    {
        reportDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/reportValidationTest/ReportDto.json"), ReportDto.class);
    }

    @Test
    public void reportSavePasses() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(new ArrayList<Long>());
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSavePassesDefectsHaveOpenCoCs()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(DEFECTS);
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(new ArrayList<Long>());
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsDefectsHaveNoOpenCoCs()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, ReportType.FAR.value(),
                "open defects", "no open CoCs", Arrays.toString(new Long[] {DEFECT_1_ID, DEFECT_2_ID})));

        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(false);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSavePassesClosedDefectsHaveNoOpenCoCs()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(false);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(new ArrayList<Long>());
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsClosedDefectsHaveOpenCoCs()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, ReportType.FAR.value(),
                "closed defects", "open CoCs", Arrays.toString(new Long[] {DEFECT_1_ID, DEFECT_2_ID})));

        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsClosedDefectsHaveOpenCoCsFSR()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, ReportType.FSR.value(),
                "closed defects", "open CoCs", Arrays.toString(new Long[] {DEFECT_1_ID, DEFECT_2_ID})));

        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FSR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSavePassesClosedDefectsHaveOpenCoCsButReportIsDAR()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.DAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(SURVEY_IDS);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsUncreditedSurveys()
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.REPORT_SUBMISSION_BLOCKED, ReportType.FAR.value(),
                "surveys", "narratives, but are not credited", SURVEY_IDS.toString()));

        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FAR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(DEFECTS);
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(false);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(SURVEY_IDS);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSavePassesFSRContentValid() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FSR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(new ArrayList<Long>());
        when(this.reportService.getLatestReport(reportDto.getJob().getId(), ReportType.getTypeBeforeCurrent(ReportType.FSR.value())))
                .thenReturn(new ReportDto());
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsFSRContentInvalid() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage(String.format(ValidationMessageUtils.FIELD_CANNOT_BE_NULL, new Object[] {"parent has not been submitted"}));

        reportDto.setId(null);
        reportDto.getReportType().setId(ReportType.FSR.value());

        when(this.validationService.verifyNumberFormat(reportDto.getJob().getId().toString())).thenReturn(reportDto.getJob().getId());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.OPEN.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipDefectDao.getDefectsForJobWithStatus(reportDto.getJob().getId(),
                Collections.singletonList(DefectStatusType.CLOSED.getValue()))).thenReturn(new ArrayList<DefectDto>());
        when(this.wipCoCDao.defectHasCoCInStatus(any(Long.class), eq(Collections.singletonList(CodicilStatusType.COC_OPEN.getValue()))))
                .thenReturn(true);
        when(this.surveyDao.getUncreditedSurveysWithNarrativeForJob(reportDto.getJob().getId())).thenReturn(new ArrayList<Long>());
        when(this.reportService.getLatestReport(reportDto.getJob().getId(), ReportType.getTypeBeforeCurrent(ReportType.FSR.value())))
                .thenReturn(null);
        this.reportValidationService.validateReportSave(reportDto.getJob().getId().toString(), reportDto);
    }

    @Test
    public void reportSaveFailsBadJob() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final String badId = "NaN";

        doThrow(new BadRequestException()).when(this.jobValidationService).verifyJobIdExists(badId);
        thrown.expect(BadRequestException.class);

        this.reportValidationService.validateReportSave(badId, reportDto);
    }

    @Test
    public void reportSaveFailsAbsentJob() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final String absentId = "999";

        doThrow(new RecordNotFoundException()).when(this.jobValidationService).verifyJobIdExists(absentId);
        thrown.expect(RecordNotFoundException.class);

        this.reportValidationService.validateReportSave(absentId, reportDto);
    }

    @Test
    public void reportSaveFailsNullBody() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);
        thrown.expect(BadRequestException.class);

        this.reportValidationService.validateReportSave(VALID_JOB_ID.toString(), null);
    }

    @Test
    public void reportSaveFailsNotNullId() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(reportDto.getId());
        thrown.expect(BadRequestException.class);

        this.reportValidationService.validateReportSave(VALID_JOB_ID.toString(), reportDto);
    }

    @Test
    public void reportSaveFailsBadReport() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(reportDto);

        thrown.expect(BadRequestException.class);

        this.reportValidationService.validateReportSave(VALID_JOB_ID.toString(), reportDto);
    }

    @Test
    public void validateJobHasReportPasses() throws RecordNotFoundException, BadRequestException
    {
        when(this.reportDao.reportExistsForJob(VALID_REPORT_ID, VALID_JOB_ID)).thenReturn(true);
        this.reportValidationService.validateJobHasReport(VALID_REPORT_ID.toString(), VALID_JOB_ID.toString());
    }

    @Test
    public void validateJobHasReportFails() throws RecordNotFoundException, BadRequestException
    {
        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Report", VALID_REPORT_ID,
                "Job", VALID_JOB_ID));

        when(this.reportDao.reportExistsForJob(VALID_REPORT_ID, VALID_JOB_ID)).thenReturn(false);
        this.reportValidationService.validateJobHasReport(VALID_REPORT_ID.toString(), VALID_JOB_ID.toString());
    }

    @Test
    public void validateReportUpdate() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        reportDto.setId(VALID_REPORT_ID);

        doNothing().when(this.jobValidationService).verifyJobIdExists(VALID_JOB_ID.toString());
        doNothing().when(this.validationService).verifyBody(reportDto);
        when(this.validationService.verifyNumberFormat(VALID_JOB_ID.toString())).thenReturn(VALID_JOB_ID);
        when(this.validationService.verifyNumberFormat(VALID_REPORT_ID.toString())).thenReturn(VALID_REPORT_ID);

        this.reportValidationService.validateReportUpdate(VALID_REPORT_ID.toString(), VALID_JOB_ID.toString(), reportDto);
    }

    @Test
    public void validateReportUpdateNullJobId() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(BadRequestException.class);

        reportDto.setId(VALID_REPORT_ID);
        reportDto.setJob(null);

        doNothing().when(this.jobValidationService).verifyJobIdExists(VALID_JOB_ID.toString());
        doNothing().when(this.validationService).verifyBody(reportDto);
        when(this.validationService.verifyNumberFormat(VALID_JOB_ID.toString())).thenReturn(VALID_JOB_ID);
        when(this.validationService.verifyNumberFormat(VALID_REPORT_ID.toString())).thenReturn(VALID_REPORT_ID);

        doThrow(new BadRequestException()).when(this.updateValidationService).verifyIds(eq(null), any(String.class));

        this.reportValidationService.validateReportUpdate(VALID_REPORT_ID.toString(), VALID_JOB_ID.toString(), reportDto);
    }

    @Test
    public void validateReportUpdateNullId() throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        thrown.expect(BadRequestException.class);

        reportDto.setId(null);

        doNothing().when(this.jobValidationService).verifyJobIdExists(VALID_JOB_ID.toString());
        doNothing().when(this.validationService).verifyBody(reportDto);
        when(this.validationService.verifyNumberFormat(VALID_JOB_ID.toString())).thenReturn(VALID_JOB_ID);
        when(this.validationService.verifyNumberFormat(VALID_REPORT_ID.toString())).thenReturn(VALID_REPORT_ID);

        doThrow(new BadRequestException()).when(this.updateValidationService).verifyIds(eq(null), any(String.class));

        this.reportValidationService.validateReportUpdate(VALID_REPORT_ID.toString(), VALID_JOB_ID.toString(), reportDto);
    }
}
