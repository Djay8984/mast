package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.MilestoneDueDateReferenceType;
import com.baesystems.ai.lr.enums.MilestoneStatusType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

import ma.glasnost.orika.impl.DefaultMapperFactory;

@RunWith(MockitoJUnitRunner.class)
public class CaseMilestoneValidationServiceTest
{
    private static final String CASE_MILESTONE = "case milestone";

    @Mock
    private CaseDao caseDao;

    @Mock
    private CaseMilestoneDao caseMilestoneDao;

    @Mock
    private DefaultMapperFactory defaultMapperFactory;

    @Mock
    private CaseReferenceDataDao caseReferenceDataDao;

    @Mock
    private ValidationService validationService;

    @InjectMocks
    private final CaseMilestoneValidationServiceImpl caseMilestoneValidationService = new CaseMilestoneValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Date DATE = new Date();
    private static final Long MILESTONE_1_ID = 1L;
    private static final Long MILESTONE_2_ID = 2L;
    private static final Long MILESTONE_STATUS_OPEN = 1L;
    private static final Long MILESTONE_STATUS_CLOSED = 2L;
    private static final List<Long> VAILD_ID_LIST = new ArrayList<Long>();
    private static final Long CASE_ID = 1L;
    private static final Long INVALID_CASE_ID = 2L;
    private static final Long CASE_TYPE_ID = 10L;
    private static final LinkResource CASE_TYPE = new LinkResource(CASE_TYPE_ID);
    private static final CaseMilestoneListDto VALID_LIST = new CaseMilestoneListDto();
    private static final MilestoneDto MILESTONE = new MilestoneDto();

    private static final List<CaseMilestoneDto> CASE_MILESTONE_LIST = new ArrayList<CaseMilestoneDto>();

    @Before
    public void setUp()
    {
        VAILD_ID_LIST.clear();
        CASE_MILESTONE_LIST.clear();
    }

    private static CaseMilestoneDto createCaseMilestone(final Long milestoneId, final Long milestoneStatusId, final Long caseId)
    {
        final CaseMilestoneDto caseMilestoneDto = new CaseMilestoneDto();

        final ReferenceDataDto milestoneStatus = new ReferenceDataDto();
        final LinkResource aCase = new LinkResource(caseId);
        MILESTONE.setId(milestoneId);
        MILESTONE.setCaseType(CASE_TYPE);
        MILESTONE.setDisplayOrder(milestoneId);
        MILESTONE.setPredecessorMilestones(null);
        milestoneStatus.setId(milestoneStatusId);
        caseMilestoneDto.setMilestone(new LinkResource(MILESTONE.getId()));
        caseMilestoneDto.setaCase(aCase);
        caseMilestoneDto.setInScope(false);
        caseMilestoneDto.setId(milestoneId);
        caseMilestoneDto.setCompletionDate(DATE);
        caseMilestoneDto.setDueDate(DATE);
        if (milestoneStatusId == MILESTONE_STATUS_OPEN)
        {
            milestoneStatus.setId(MilestoneStatusType.OPEN.value());
            milestoneStatus.setName(MilestoneStatusType.OPEN.name());
        }
        else if (milestoneStatusId == MILESTONE_STATUS_CLOSED)
        {
            milestoneStatus.setId(MilestoneStatusType.COMPLETE.value());
            milestoneStatus.setName(MilestoneStatusType.COMPLETE.name());
        }
        milestoneStatus.setDeleted(false);
        caseMilestoneDto.setMilestoneStatus(new LinkResource(milestoneStatus.getId()));

        return caseMilestoneDto;
    }

    @Test
    public void testUpdateMilestoneNoChange() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(caseMilestone);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneCaseInWrongStatus() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.INCORRECT_STATUS, "case milestones", "the case"));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.CLOSED.getValue());

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneRepeatUpdate() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(ExceptionMessagesUtils.MULTIPLE_UPDATES);

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(caseMilestone);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteNoParent() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestone.getMilestone().getId())).thenReturn(null);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteParentAndChild() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto caseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        CASE_MILESTONE_LIST.add(caseMilestoneChild);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);
        VAILD_ID_LIST.add(MILESTONE_2_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneParent.getMilestone().getId())).thenReturn(null);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneChild.getId())).thenReturn(origionalCaseMilestoneChild);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneChild.getMilestone().getId()))
                .thenReturn(Collections.singletonList(origionalCaseMilestoneParent.getId()));
        when(this.caseMilestoneDao.getStatusIdMilestoneIdAndCaseId(origionalCaseMilestoneParent.getId(), CASE_ID)).thenReturn(MILESTONE_STATUS_OPEN);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteChildParentAlreadyComplete() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto caseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestoneChild);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_2_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneParent.getMilestone().getId())).thenReturn(null);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneChild.getId())).thenReturn(origionalCaseMilestoneChild);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneChild.getMilestone().getId()))
                .thenReturn(Collections.singletonList(origionalCaseMilestoneParent.getId()));
        when(this.caseMilestoneDao.getStatusIdMilestoneIdAndCaseId(origionalCaseMilestoneParent.getId(), CASE_ID))
                .thenReturn(MILESTONE_STATUS_CLOSED);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteChildParentNotComplete() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(
                String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, MILESTONE_2_ID, "parent"));

        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto caseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestoneChild);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_2_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneParent.getMilestone().getId())).thenReturn(null);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneChild.getId())).thenReturn(origionalCaseMilestoneChild);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestoneChild.getMilestone().getId()))
                .thenReturn(Collections.singletonList(origionalCaseMilestoneParent.getId()));
        when(this.caseMilestoneDao.getStatusIdMilestoneIdAndCaseId(origionalCaseMilestoneParent.getId(), CASE_ID))
                .thenReturn(MILESTONE_STATUS_OPEN);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteNoDueDate() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_MARKED_COMPLETE, CASE_MILESTONE,
                MILESTONE_1_ID, "dueDate", "null"));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestone.setDueDate(null);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestone.getMilestone().getId())).thenReturn(null);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneSetCompleteNoCompletionDate() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_MARKED_COMPLETE, CASE_MILESTONE,
                MILESTONE_1_ID, "completionDate", "null"));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestone.setCompletionDate(null);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.getParentMilestoneIds(caseMilestone.getMilestone().getId())).thenReturn(null);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScope() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestone.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getChildMilestoneIds(MILESTONE_1_ID)).thenReturn(null);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScopeWithChild() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto caseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestoneParent.setInScope(true);
        origionalCaseMilestoneChild.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        CASE_MILESTONE_LIST.add(caseMilestoneChild);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);
        VAILD_ID_LIST.add(MILESTONE_2_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneChild.getId())).thenReturn(origionalCaseMilestoneChild);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_2_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getChildMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(true);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScopeChildAlreadyOutOfScope() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestoneParent.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getChildMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(false);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScopeChildInOfScope() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(
                String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, MILESTONE_1_ID, "children"));

        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestoneParent.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getChildMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(true);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScopeCopmplete() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, MILESTONE_1_ID, "complete"));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_CLOSED, CASE_ID);
        origionalCaseMilestone.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveOutOfScopeMandatory() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED, MILESTONE_1_ID, "mandatory"));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestone.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(true);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveIntoScope() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestone.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getParentMilestoneIds(MILESTONE_1_ID)).thenReturn(null);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveIntoScopeWithParent() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto caseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneChild = createCaseMilestone(MILESTONE_2_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestoneParent.setInScope(true);
        caseMilestoneChild.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        CASE_MILESTONE_LIST.add(caseMilestoneChild);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);
        VAILD_ID_LIST.add(MILESTONE_2_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneChild.getId())).thenReturn(origionalCaseMilestoneChild);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_2_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getParentMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseReferenceDataDao.getParentMilestoneIds(MILESTONE_2_ID)).thenReturn(null);
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(false);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveIntoScopeParentAlreadyInScope() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestoneParent.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getParentMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(true);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneMoveIntoScopeParentOutOfScope() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_WITHOUT_RELATED, CASE_MILESTONE, MILESTONE_1_ID, "parent"));

        final CaseMilestoneDto caseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestoneParent = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        caseMilestoneParent.setInScope(true);
        CASE_MILESTONE_LIST.add(caseMilestoneParent);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestoneParent.getId())).thenReturn(origionalCaseMilestoneParent);
        when(this.caseReferenceDataDao.isMilestoneMandatory(MILESTONE_1_ID)).thenReturn(false);
        when(this.caseReferenceDataDao.getParentMilestoneIds(MILESTONE_1_ID)).thenReturn(Collections.singletonList(MILESTONE_2_ID));
        when(this.caseMilestoneDao.isCaseMilestoneInScopeByCaseAndMilestoneId(MILESTONE_2_ID, CASE_ID)).thenReturn(false);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneUpdateDueDate() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestone.setDueDate(null);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.getMilestoneDueDateReferenceId(MILESTONE_1_ID)).thenReturn(MilestoneDueDateReferenceType.MANUAL.getValue());

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneUpdateDueDateNotManualEntry() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_REF_DATA, MILESTONE_1_ID));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestone.setDueDate(null);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);
        when(this.caseReferenceDataDao.getMilestoneDueDateReferenceId(MILESTONE_1_ID))
                .thenReturn(MilestoneDueDateReferenceType.MILESTONE_COMPLETION.getValue());

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneChangeCase() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_REF_DATA, MILESTONE_1_ID)));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, INVALID_CASE_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }

    @Test
    public void testUpdateMilestoneChangeMiledstone() throws BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(String.format(ExceptionMessagesUtils.CANNOT_BE_UPDATED_REF_DATA, MILESTONE_1_ID)));

        final CaseMilestoneDto caseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        final CaseMilestoneDto origionalCaseMilestone = createCaseMilestone(MILESTONE_1_ID, MILESTONE_STATUS_OPEN, CASE_ID);
        origionalCaseMilestone.getMilestone().setId(MILESTONE_2_ID);
        CASE_MILESTONE_LIST.add(caseMilestone);
        VALID_LIST.setCaseMilestoneList(CASE_MILESTONE_LIST);
        VAILD_ID_LIST.add(MILESTONE_1_ID);

        doNothing().when(this.validationService).verifyBody(CASE_MILESTONE_LIST);
        when(this.caseDao.getCaseStatusId(CASE_ID)).thenReturn(CaseStatusType.POPULATING.getValue());
        when(this.caseMilestoneDao.getCaseMilestoneIdsForCase(CASE_ID)).thenReturn(VAILD_ID_LIST);
        when(this.caseMilestoneDao.getCaseMilestone(caseMilestone.getId())).thenReturn(origionalCaseMilestone);

        this.caseMilestoneValidationService.validateCaseMilestoneListUpdate(CASE_ID, VALID_LIST);
    }
}
