package com.baesystems.ai.lr.validation.utils;

import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.FIELD_CANNOT_BE_NULL;
import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.STRING_FIELD_DOES_NOT_MATCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.validation.utils.ValidationConstantUtils.STRINGTYPE;

@RunWith(MockitoJUnitRunner.class)
public class ValidationStringUtilsTest
{
    private List<String> results;
    private static final String OBJECT_NAME = "anObject";
    private static final String MESSAGE = "An optional message.";
    private static final String STRING = "A String";
    private static final String ALPHANUMERIC_STRING = "A String 123";
    private static final String SPECIAL_STRING = "A String 123!";
    private static final String NUMERIC_STRING = "-123";
    private static final String BOOLEAN_STRING = "tRuE";
    private static final String DATE_STRING = "2015-12-17";
    private static final String ABC = "ABC";
    private static final String ABC_REGEX = "^[A-C]*$";

    @InjectMocks
    private final ValidationStringUtils validationStringUtils = new ValidationStringUtils();

    private String genarateMessage(final String fieldName, final String reference, final String message)
    {
        return String.format(STRING_FIELD_DOES_NOT_MATCH, fieldName, reference) + message;
    }

    private String genarateMessage(final String fieldName, final String reference)
    {
        return String.format(STRING_FIELD_DOES_NOT_MATCH, fieldName, reference);
    }

    private String genarateMessagePresence(final String fieldName)
    {
        return String.format(FIELD_CANNOT_BE_NULL, fieldName);
    }

    @Before
    public void setUp()
    {
        results = new ArrayList<String>();
    }

    @Test
    public void testValidateStringEqualPass()
    {
        this.validationStringUtils.validateStringEqual(STRING, STRING, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringEqualFailNullTest()
    {
        this.validationStringUtils.validateStringEqual(null, STRING, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessagePresence(OBJECT_NAME), results.get(0));
    }

    @Test
    public void testValidateStringEqualPassesNullReference()
    {
        this.validationStringUtils.validateStringEqual(STRING, null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringEqualFailBothStringsNull()
    {
        this.validationStringUtils.validateStringEqual(null, null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessagePresence(OBJECT_NAME), results.get(0));
    }

    @Test
    public void testValidateStringEqualFailStringsNotEqual()
    {
        this.validationStringUtils.validateStringEqual(STRING, ABC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, ABC, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringEqualFailStringsNotEqualNoMessage()
    {
        this.validationStringUtils.validateStringEqual(STRING, ABC, OBJECT_NAME, results);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, ABC), results.get(0));
    }

    @Test
    public void testValidateStringFailNullTest()
    {
        this.validationStringUtils.validateString(null, STRING, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessagePresence(OBJECT_NAME), results.get(0));
    }

    @Test
    public void testValidateStringPassNullPattern()
    {
        this.validationStringUtils.validateString(SPECIAL_STRING, null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringPassInvalidPattern()
    {
        this.validationStringUtils.validateString(SPECIAL_STRING, ABC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringPassAlphanumeric()
    {
        this.validationStringUtils.validateString(ALPHANUMERIC_STRING, STRINGTYPE.ALPHANUMERIC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailAlphanumeric()
    {
        this.validationStringUtils.validateString(SPECIAL_STRING, STRINGTYPE.ALPHANUMERIC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.ALPHANUMERIC, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringPassAlphanumericNoMessage()
    {
        this.validationStringUtils.validateString(ALPHANUMERIC_STRING, STRINGTYPE.ALPHANUMERIC, OBJECT_NAME, results);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailAlphanumericNoMessage()
    {
        this.validationStringUtils.validateString(SPECIAL_STRING, STRINGTYPE.ALPHANUMERIC, OBJECT_NAME, results);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.ALPHANUMERIC), results.get(0));
    }

    @Test
    public void testValidateStringPassNumeric()
    {
        this.validationStringUtils.validateString(NUMERIC_STRING, STRINGTYPE.NUMERIC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailNumeric()
    {
        this.validationStringUtils.validateString(ALPHANUMERIC_STRING, STRINGTYPE.NUMERIC, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.NUMERIC, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringPassAlpha()
    {
        this.validationStringUtils.validateString(STRING, STRINGTYPE.ALPHA, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailAlpha()
    {
        this.validationStringUtils.validateString(ALPHANUMERIC_STRING, STRINGTYPE.ALPHA, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.ALPHA, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringPassBoolean()
    {
        this.validationStringUtils.validateString(BOOLEAN_STRING, STRINGTYPE.BOOLEAN, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailBoolean()
    {
        this.validationStringUtils.validateString(STRING, STRINGTYPE.BOOLEAN, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.BOOLEAN, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringPassDate()
    {
        this.validationStringUtils.validateString(DATE_STRING, STRINGTYPE.DATE, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringFailDate()
    {
        this.validationStringUtils.validateString(ABC, STRINGTYPE.DATE, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, STRINGTYPE.DATE, MESSAGE), results.get(0));
    }

    @Test
    public void validateStringCustomPass()
    {
        this.validationStringUtils.validateStringCustom(ABC, ABC_REGEX, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringCustomFailNullTest()
    {
        this.validationStringUtils.validateStringCustom(null, ABC_REGEX, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessagePresence(OBJECT_NAME), results.get(0));
    }

    @Test
    public void testValidateStringCustomPassesNullReference()
    {
        this.validationStringUtils.validateStringCustom(STRING, null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidateStringCustomFailBothStringsNull()
    {
        this.validationStringUtils.validateStringCustom(null, null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessagePresence(OBJECT_NAME), results.get(0));
    }

    @Test
    public void testValidateStringCustomFailStringsNotEqual()
    {
        this.validationStringUtils.validateStringCustom(STRING, ABC_REGEX, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, ABC_REGEX, MESSAGE), results.get(0));
    }

    @Test
    public void testValidateStringCustomFailStringsNotEqualNoMessage()
    {
        this.validationStringUtils.validateStringCustom(STRING, ABC_REGEX, OBJECT_NAME, results);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, ABC_REGEX), results.get(0));
    }
}
