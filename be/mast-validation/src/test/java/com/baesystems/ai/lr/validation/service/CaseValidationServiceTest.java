package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.EmployeeRoleType;
import com.baesystems.ai.lr.enums.OfficeRoleType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.DuplicateCaseException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.CaseReferenceDataService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.service.validation.ValidationService;

@RunWith(MockitoJUnitRunner.class)
public class CaseValidationServiceTest
{
    private static final Long DUPLICATE_CASE_TYPE = 1L;
    private static final Long UNIQUE_CASE_TYPE = 101L;

    private static final Long DUPLICATE_ASSET = 2L;
    private static final Long UNIQUE_ASSET = 202L;

    private static final Long DUPLICATE_CASE_ONE = 1L;
    private static final Long DUPLICATE_CASE_TWO = 2L;

    private static final Long LEAD_IMO = 1234567L;

    private static final Long ONE = 1L;

    private static final AssetLightDto ASSET = new AssetLightDto();

    private static List<Long> noCases = null;
    private static List<Long> duplicateCases;
    private static String duplicateCaseExceptionMessage;

    private static CaseStatusDto caseStatusCheckErrors;

    @Mock
    private ValidationService validationService;

    @Mock
    private ValidationDataService validationDataService;

    @Mock
    private AssetValidationService assetValidationService;

    @Mock
    private AssetDao assetDao;

    @Mock
    private AssetAndCaseValidationUtils assetAndCaseValidationUtils;

    @Mock
    private CaseReferenceDataService caseReferenceDataService;

    @InjectMocks
    private final CaseValidationServiceImpl casevalidationService = new CaseValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        duplicateCases = new ArrayList<Long>();
        duplicateCases.add(DUPLICATE_CASE_ONE);
        duplicateCases.add(DUPLICATE_CASE_TWO);

        final DuplicateCaseException duplicateCaseException = new DuplicateCaseException(Arrays.asList(DUPLICATE_CASE_ONE, DUPLICATE_CASE_TWO));
        duplicateCaseExceptionMessage = duplicateCaseException.getMessage();

        caseStatusCheckErrors = new CaseStatusDto();
        caseStatusCheckErrors.setId(ONE);
        caseStatusCheckErrors.setCanSaveWithErrors(false);
    }

    @Test
    public void noDuplicateCasesFound() throws DuplicateCaseException
    {
        when(this.validationDataService.findDuplicateCases(UNIQUE_CASE_TYPE, UNIQUE_ASSET))
                .thenReturn(noCases);
        final CaseWithAssetDetailsDto uniqueCase = createCase(UNIQUE_CASE_TYPE, UNIQUE_ASSET);

        this.casevalidationService.verifyCaseIsUnique(uniqueCase);
    }

    @Test
    public void duplicateCasesFound() throws DuplicateCaseException
    {
        when(this.validationDataService.findDuplicateCases(DUPLICATE_CASE_TYPE, DUPLICATE_ASSET))
                .thenReturn(duplicateCases);

        final CaseWithAssetDetailsDto duplicateCase = createCase(DUPLICATE_CASE_TYPE, DUPLICATE_ASSET);

        this.thrown.expect(DuplicateCaseException.class);
        this.thrown.expectMessage(duplicateCaseExceptionMessage);

        this.casevalidationService.verifyCaseIsUnique(duplicateCase);
    }

    /**
     * Test that if a case is going from a state that permit duplicates to a state that doesn't permit duplicates, we
     * require validation.
     */
    @Test
    public void testUpdateUniquenessValidationRequired()
    {
        final CaseWithAssetDetailsDto duplicateCase = createCase(DUPLICATE_CASE_TYPE, DUPLICATE_ASSET);
        final Long caseStatusDuplicatePermitted = CaseStatusType.DELETED.getValue();
        final Long caseStatusDuplicateNotPermitted = CaseStatusType.ONHOLD.getValue();
        final LinkResource caseStatusDuplicateNotPermittedLinkResource = new LinkResource(caseStatusDuplicateNotPermitted);
        duplicateCase.setCaseStatus(caseStatusDuplicateNotPermittedLinkResource);
        duplicateCase.setId(ONE);

        when(this.validationDataService.getCurrentStatusForCase(ONE)).thenReturn(caseStatusDuplicatePermitted);
        final boolean validationRequired = this.casevalidationService.caseUpdateRequiresUniquenessValidation(duplicateCase);
        Assert.assertTrue(validationRequired);
    }

    /**
     * Test that if a case is going from a state that permits duplicates to a state that does permit duplicates, we
     * require validation.
     */
    @Test
    public void testUpdateUniquenessValidationNotRequired()
    {
        final CaseWithAssetDetailsDto duplicateCase = createCase(DUPLICATE_CASE_TYPE, DUPLICATE_ASSET);
        final Long caseStatusDuplicatePermitted = CaseStatusType.DELETED.getValue();
        final Long caseStatusDuplicateNotPermitted = CaseStatusType.ONHOLD.getValue();
        final LinkResource caseStatusDuplicatePermittedLinkResource = new LinkResource(caseStatusDuplicatePermitted);
        duplicateCase.setCaseStatus(caseStatusDuplicatePermittedLinkResource);
        duplicateCase.setId(ONE);

        when(this.validationDataService.getCurrentStatusForCase(ONE)).thenReturn(caseStatusDuplicateNotPermitted);
        final boolean validationRequired = this.casevalidationService.caseUpdateRequiresUniquenessValidation(duplicateCase);
        Assert.assertFalse(validationRequired);
    }

    @Test
    public void assetWithLead() throws RecordNotFoundException, ParseException, BadRequestException, MastBusinessException
    {
        final CaseWithAssetDetailsDto uniqueCase = createCase(UNIQUE_CASE_TYPE, UNIQUE_ASSET);

        ASSET.setIsLead(false);
        ASSET.setLeadImo(LEAD_IMO);

        when(this.caseReferenceDataService.getCaseStatus(uniqueCase.getCaseStatus().getId())).thenReturn(caseStatusCheckErrors);
        when(this.assetDao.getPublishedVersion(UNIQUE_ASSET)).thenReturn(1L);
        when(this.assetDao.getAsset(UNIQUE_ASSET, 1L, AssetLightDto.class)).thenReturn(ASSET);
        doNothing().when(this.assetAndCaseValidationUtils).validateLinks(uniqueCase, true);
        doNothing().when(this.assetValidationService).verifyImoNumberExists(LEAD_IMO);

        this.casevalidationService.verifyCase(uniqueCase);
    }

    @Test
    public void assetWithInvalidLead() throws RecordNotFoundException, ParseException, BadRequestException, MastBusinessException
    {
        final CaseWithAssetDetailsDto uniqueCase = createCase(UNIQUE_CASE_TYPE, UNIQUE_ASSET);

        ASSET.setIsLead(false);
        ASSET.setLeadImo(LEAD_IMO);

        when(this.caseReferenceDataService.getCaseStatus(uniqueCase.getCaseStatus().getId())).thenReturn(caseStatusCheckErrors);
        when(this.assetDao.getAsset(UNIQUE_ASSET, 1L, AssetLightDto.class)).thenReturn(ASSET);
        doThrow(new RecordNotFoundException()).when(this.assetValidationService).verifyImoNumberExists(LEAD_IMO);

        this.thrown.expect(RecordNotFoundException.class);

        this.casevalidationService.verifyCase(uniqueCase);
    }

    private static CaseWithAssetDetailsDto createCase(final Long caseTypeId, final Long assetId)
    {
        final LinkResource caseType = new LinkResource(caseTypeId);

        final LinkResource asset = new LinkResource(assetId);

        final LinkResource caseStatus = new LinkResource(ONE);

        final CaseWithAssetDetailsDto caseDto = new CaseWithAssetDetailsDto();
        caseDto.setCaseType(caseType);
        caseDto.setAsset(asset);
        caseDto.setCaseStatus(caseStatus);

        final OfficeLinkDto office = new OfficeLinkDto();

        office.setOffice(new LinkResource(ONE));
        office.setOfficeRole(new LinkResource(OfficeRoleType.CASE_SDO.getValue()));

        final CaseSurveyorWithLinksDto surveyor1 = new CaseSurveyorWithLinksDto();

        surveyor1.setSurveyor(new LinkResource(ONE));
        surveyor1.setEmployeeRole(new LinkResource(EmployeeRoleType.EIC_ADMIN.getValue()));

        final CaseSurveyorWithLinksDto surveyor2 = new CaseSurveyorWithLinksDto();

        surveyor2.setSurveyor(new LinkResource(ONE));
        surveyor2.setEmployeeRole(new LinkResource(EmployeeRoleType.MANAGEMENT.getValue()));

        final CustomerLinkDto customer = new CustomerLinkDto();

        customer.setCustomer(new PartyDto());
        customer.setRelationship(new LinkResource(ONE));
        customer.setFunctions(new ArrayList<CustomerHasFunctionDto>());
        customer.getCustomer().setId(ONE);

        caseDto.setOffices(new ArrayList<OfficeLinkDto>());
        caseDto.getOffices().add(office);
        caseDto.setSurveyors(new ArrayList<CaseSurveyorWithLinksDto>());
        caseDto.getSurveyors().add(surveyor1);
        caseDto.getSurveyors().add(surveyor2);
        caseDto.setCustomers(new ArrayList<CustomerLinkDto>());
        caseDto.getCustomers().add(customer);

        return caseDto;
    }
}
