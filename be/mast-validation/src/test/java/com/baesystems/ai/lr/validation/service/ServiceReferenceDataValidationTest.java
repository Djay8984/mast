package com.baesystems.ai.lr.validation.service;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(ServiceValidationHelper.class)
public class ServiceReferenceDataValidationTest
{
    @Mock
    private ValidationService validationService;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @InjectMocks
    private final ServiceReferenceDataValidation serviceReferenceDataValidation = new ServiceReferenceDataValidation();

    @Rule
    private final ExpectedException thrown = ExpectedException.none();

    private static ObjectMapper objectMapper;

    private static final DateFormat SHORT_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    private static final Long SERVICE_1_ID = 1L;
    private static final Long SERVICE_STATUS_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_1_ID = 1L;

    private ScheduledServiceDto goodService;
    private Date assignedDate;
    private Date dueDate;
    private Date lowerRangeDate;
    private Date upperRangeDate;

    private ScheduledServiceDto updateableService;

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();
    }

    @Before
    public void setUp() throws JsonParseException, JsonMappingException, IOException, ParseException
    {
        assignedDate = SHORT_DATE_FORMATTER.parse("2015-10-25");
        dueDate = SHORT_DATE_FORMATTER.parse("2016-10-25");
        lowerRangeDate = SHORT_DATE_FORMATTER.parse("2016-10-15");
        upperRangeDate = SHORT_DATE_FORMATTER.parse("2016-11-05");

        goodService = objectMapper.readValue(
                this.getClass().getResourceAsStream("/serviceValidationTest/validService.json"), ScheduledServiceDto.class);

        updateableService = getService(SERVICE_1_ID);
    }

    private ScheduledServiceDto getService(final Long serviceId) throws ParseException
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();

        service.setId(serviceId);
        service.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service.setAssignedDate(this.assignedDate);
        service.setDueDate(this.dueDate);
        service.setLowerRangeDate(this.lowerRangeDate);
        service.setUpperRangeDate(this.upperRangeDate);

        return service;
    }

    @Test
    public void validateServiceRefDataFails() throws BadRequestException, RecordNotFoundException
    {
        updateableService.setServiceStatus(new LinkResource());
        updateableService.getServiceStatus().setId(SERVICE_STATUS_1_ID);

        when(this.serviceReferenceDataDao.getServiceStatus(SERVICE_STATUS_1_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.serviceReferenceDataValidation.validateServiceRefData(this.updateableService);
    }

    @Test
    public void validateServiceRefrenceDataPasses() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(new ServiceCatalogueDO());
        when(this.serviceReferenceDataDao.getServiceCreditStatus(goodService.getServiceCreditStatus().getId())).thenReturn(new ServiceCreditStatusDto());
        when(this.serviceReferenceDataDao.getServiceStatus(goodService.getServiceStatus().getId())).thenReturn(new ReferenceDataDto());

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }

    @Test
    public void validateServiceRefrenceDataPassesNullStatuses() throws BadRequestException, RecordNotFoundException
    {
        goodService.setServiceStatus(null);
        goodService.setServiceCreditStatus(null);

        doNothing().when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(new ServiceCatalogueDO());

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }

    @Test
    public void validateServiceRefrenceDataFailsCatalogueNotFound() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(
                String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service catalogue", goodService.getServiceCatalogueId()));

        doNothing().when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(null);
        when(this.serviceReferenceDataDao.getServiceCreditStatus(goodService.getServiceCreditStatus().getId())).thenReturn(new ServiceCreditStatusDto());
        when(this.serviceReferenceDataDao.getServiceStatus(goodService.getServiceStatus().getId())).thenReturn(new ReferenceDataDto());

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }

    @Test
    public void validateServiceRefrenceDataFailsStatusNotFound() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(
                String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service status", goodService.getServiceStatus().getId()));

        doNothing().when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(new ServiceCatalogueDO());
        when(this.serviceReferenceDataDao.getServiceCreditStatus(goodService.getServiceCreditStatus().getId())).thenReturn(new ServiceCreditStatusDto());
        when(this.serviceReferenceDataDao.getServiceStatus(goodService.getServiceStatus().getId())).thenReturn(null);

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }

    @Test
    public void validateServiceRefrenceDataFailsCreditStatusNotFound() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(
                String.format(ExceptionMessagesUtils.SPECIFIC_NOT_FOUND_ERROR_MSG, "service credit status",
                        goodService.getServiceCreditStatus().getId()));

        doNothing().when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(new ServiceCatalogueDO());
        when(this.serviceReferenceDataDao.getServiceCreditStatus(goodService.getServiceCreditStatus().getId())).thenReturn(null);
        when(this.serviceReferenceDataDao.getServiceStatus(goodService.getServiceStatus().getId())).thenReturn(new ReferenceDataDto());

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }

    @Test
    public void validateServiceRefrenceDataFailsBadId() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);

        doThrow(new BadRequestException()).when(this.validationService).verifyId(any(String.class));
        when(this.serviceDao.getServiceCatalogueDO(goodService.getServiceCatalogueId())).thenReturn(new ServiceCatalogueDO());
        when(this.serviceReferenceDataDao.getServiceCreditStatus(goodService.getServiceCreditStatus().getId())).thenReturn(null);
        when(this.serviceReferenceDataDao.getServiceStatus(goodService.getServiceStatus().getId())).thenReturn(new ReferenceDataDto());

        this.serviceReferenceDataValidation.validateServiceRefData(goodService);
    }
}
