package com.baesystems.ai.lr.validation.service;

import static java.lang.String.format;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.codicils.CodicilDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class ItemValidationServiceTest
{
    @Mock
    private ItemDao itemDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private CodicilDao codicilDao;

    @Mock
    private DefectDao defectDao;

    @Mock
    private ValidationService validationService;

    @Mock
    private AssetValidationService assetValidationService;

    @InjectMocks
    private final ItemValidationServiceImpl itemValidationService = new ItemValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long ASSET_ID = 1L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L;
    private static final Long ITEM_3_ID = 3L;
    private static final Long IS_PART_OF_ID = 1L;
    private static final Long IS_RELATED_TO_ID = 9L;
    private static final Long ITEM_TYPE_1_ID = 1L;
    private static final Long ITEM_TYPE_2_ID = 2L;
    private static final Long ITEM_RELATIONSHIP_1_ID = 1L;
    private static final Long ASSET_CATEGORY_1_ID = 1L;
    private static final LazyItemDto ITEM_1 = new LazyItemDto();
    private static final LazyItemDto ITEM_2 = new LazyItemDto();
    private static final ItemRelationshipDto ITEM_RELATIONSHIP = new ItemRelationshipDto();
    private static final Long CHILD_ITEM_ID_10 = 10L;
    private static final Long CHILD_ITEM_ID_11 = 11L;
    private static final Long CHILD_ITEM_ID_12 = 12L;
    private static final List<Long> CHILD_ID_LIST = Arrays.asList(CHILD_ITEM_ID_10, CHILD_ITEM_ID_11, CHILD_ITEM_ID_12);

    @Before
    public void setUp()
    {
        ITEM_RELATIONSHIP.setToItem(new ItemLightDto());
        ITEM_RELATIONSHIP.getToItem().setId(ITEM_2_ID);
        ITEM_RELATIONSHIP.setType(new LinkResource(IS_RELATED_TO_ID));

        // TODO Uncertain version
        ITEM_1.setParentItem(new LinkVersionedResource(ITEM_3_ID, 1L));
    }

    @Test
    public void testVerifyAssetHasItemPasses() throws RecordNotFoundException, BadRequestException
    {
        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(true);
        this.itemValidationService.verifyAssetHasItem(ASSET_ID.toString(), ITEM_1_ID.toString());
    }

    @Test
    public void testVerifyAssetHasItemFails() throws RecordNotFoundException, BadRequestException
    {
        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(false);
        this.thrown.expect(RecordNotFoundException.class);
        this.itemValidationService.verifyAssetHasItem(ASSET_ID.toString(), ITEM_1_ID.toString());

    }

    @Test
    public void verifyItemHasItemRelationshipPasses() throws RecordNotFoundException
    {
        when(this.itemDao.relationshipExistsForItem(ITEM_1_ID, ITEM_RELATIONSHIP_1_ID, RelationshipType.IS_PART_OF)).thenReturn(true);
        this.itemValidationService.verifyItemHasItemRelationship(ITEM_1_ID, ITEM_RELATIONSHIP_1_ID, RelationshipType.IS_PART_OF);
    }

    @Test
    public void verifyItemHasItemRelationshipFails() throws RecordNotFoundException
    {
        when(this.itemDao.relationshipExistsForItem(ITEM_1_ID, ITEM_RELATIONSHIP_1_ID, RelationshipType.IS_PART_OF)).thenReturn(false);
        this.thrown.expect(RecordNotFoundException.class);
        this.itemValidationService.verifyItemHasItemRelationship(ITEM_1_ID, ITEM_RELATIONSHIP_1_ID, RelationshipType.IS_PART_OF);
    }

    @Test
    public void testVerifyItemIsCorrectType() throws MastBusinessException
    {
        when(this.itemDao.itemIsOfType(ITEM_1_ID, ITEM_TYPE_1_ID)).thenReturn(true);
        this.itemValidationService.verifyItemIsCorrectType(ITEM_1_ID, ITEM_TYPE_1_ID);
    }

    @Test
    public void testVerifyItemIsCorrectTypeIncorrectType() throws MastBusinessException
    {
        this.thrown.expect(MastBusinessException.class);

        when(this.itemDao.itemIsOfType(ITEM_1_ID, ITEM_TYPE_1_ID)).thenReturn(false);
        this.itemValidationService.verifyItemIsCorrectType(ITEM_1_ID, ITEM_TYPE_1_ID);
    }

    @Test
    public void verifyItemRelationshipPass() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);
        when(this.itemDao.getItemType(ITEM_1_ID)).thenReturn(ITEM_TYPE_1_ID);
        when(this.itemDao.getItemType(ITEM_2_ID)).thenReturn(ITEM_TYPE_2_ID);
        when(this.itemDao.relationshipExists(ITEM_2_ID, ITEM_1_ID, IS_RELATED_TO_ID)).thenReturn(false);
        when(this.assetDao.getAssetCategory(ASSET_ID)).thenReturn(ASSET_CATEGORY_1_ID);
        when(this.itemDao.isProposedRelationshipValidForTypes(ITEM_TYPE_1_ID, ITEM_TYPE_2_ID, ASSET_CATEGORY_1_ID.intValue(), IS_RELATED_TO_ID))
                .thenReturn(true);
        when(this.itemDao.isProposedRelationshipValidForTypes(ITEM_TYPE_2_ID, ITEM_TYPE_1_ID, ASSET_CATEGORY_1_ID.intValue(), IS_RELATED_TO_ID))
                .thenReturn(false);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipPassDefaultRelationshipType() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        ITEM_RELATIONSHIP.setType(null);

        doNothing().when(this.validationService).verifyId(ASSET_ID.toString());
        doNothing().when(this.validationService).verifyId(ITEM_2_ID.toString());
        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);
        when(this.itemDao.getItemType(ITEM_1_ID)).thenReturn(ITEM_TYPE_1_ID);
        when(this.itemDao.getItemType(ITEM_2_ID)).thenReturn(ITEM_TYPE_2_ID);
        when(this.itemDao.relationshipExists(ITEM_2_ID, ITEM_1_ID, IS_RELATED_TO_ID)).thenReturn(false);
        when(this.assetDao.getAssetCategory(ASSET_ID)).thenReturn(ASSET_CATEGORY_1_ID);
        when(this.itemDao.isProposedRelationshipValidForTypes(ITEM_TYPE_1_ID, ITEM_TYPE_2_ID, ASSET_CATEGORY_1_ID.intValue(), IS_RELATED_TO_ID))
                .thenReturn(true);
        when(this.itemDao.isProposedRelationshipValidForTypes(ITEM_TYPE_2_ID, ITEM_TYPE_1_ID, ASSET_CATEGORY_1_ID.intValue(), IS_RELATED_TO_ID))
                .thenReturn(false);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipFailsToItemDoesNotBelongToAsset() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(RecordNotFoundException.class);

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(false);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipFailsIncorrectRelationship() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(BadRequestException.class);

        ITEM_RELATIONSHIP.getType().setId(IS_PART_OF_ID);

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipFailsToChildOfFrom() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(BadRequestException.class);

        ITEM_1.getParentItem().setId(ITEM_2_ID);

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipFailsFromChildOfTo() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(BadRequestException.class);

        // TODO Uncertain version
        ITEM_2.setParentItem(new LinkVersionedResource(ITEM_1_ID, 1L));

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyItemRelationshipFailsRelationshipAlreadyExists() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_2_ID)).thenReturn(true);
        // when(this.itemDao.getItem(ITEM_1_ID)).thenReturn(ITEM_1);
        // when(this.itemDao.getItem(ITEM_2_ID)).thenReturn(ITEM_2);
        when(this.itemDao.getItemType(ITEM_1_ID)).thenReturn(ITEM_TYPE_1_ID);
        when(this.itemDao.getItemType(ITEM_2_ID)).thenReturn(ITEM_TYPE_2_ID);
        when(this.itemDao.relationshipExists(ITEM_2_ID, ITEM_1_ID, IS_RELATED_TO_ID)).thenReturn(true);

        this.itemValidationService.verifyItemRelationship(ASSET_ID, ITEM_1_ID, ITEM_RELATIONSHIP);
    }

    @Test
    public void verifyDeleteItemFailsAsRootItemsMayNotBeDeleted() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.CANNOT_DELETE_ROOT_ITEM, ITEM_1_ID));

        final LazyItemDto mockRootItem = new LazyItemDto();
        mockRootItem.setId(ITEM_1_ID); // Same as passed id! This should fail.

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(true);
        when(this.itemDao.getRootItem(ASSET_ID, null, true)).thenReturn(mockRootItem);

        this.itemValidationService.verifyDeleteItem(String.valueOf(ASSET_ID), String.valueOf(ITEM_1_ID));
    }

    @Test
    public void verifyDeleteItemFailsAsItHasOpenCodicils() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "deleted", "open codicils"));

        final LazyItemDto mockRootItem = new LazyItemDto();
        mockRootItem.setId(ITEM_2_ID); // Anything but the root id

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(true);
        when(this.itemDao.getRootItem(ASSET_ID, null, true)).thenReturn(mockRootItem);
        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.verifyDeleteItem(String.valueOf(ASSET_ID), String.valueOf(ITEM_1_ID));
    }

    @Test
    public void verifyDeleteItemFailsAsItHasOpenDefects() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "deleted", "open defects"));

        final LazyItemDto mockRootItem = new LazyItemDto();
        mockRootItem.setId(ITEM_2_ID); // Anything but the root id

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(true);
        when(this.itemDao.getRootItem(ASSET_ID, null, true)).thenReturn(mockRootItem);
        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.defectDao.doAnyItemsHaveOpenDefects(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.verifyDeleteItem(String.valueOf(ASSET_ID), String.valueOf(ITEM_1_ID));
    }

    @Test
    public void verifyDeleteItemFailsAsItHasOpenServices() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "deleted", "open services"));

        final LazyItemDto mockRootItem = new LazyItemDto();
        mockRootItem.setId(ITEM_2_ID); // Anything but the root id

        when(this.itemDao.itemExistsForAsset(ASSET_ID, ITEM_1_ID)).thenReturn(true);
        when(this.itemDao.getRootItem(ASSET_ID, null, true)).thenReturn(mockRootItem);
        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.serviceDao.doAnyItemsHaveOpenServices(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.verifyDeleteItem(String.valueOf(ASSET_ID), String.valueOf(ITEM_1_ID));
    }

    @Test
    public void validateDecommissionItemFailsAsItHasOpenCodicils() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "decommissioned", "open codicils"));

        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.validateDecommisionItem(ITEM_1_ID);
    }

    @Test
    public void validateDecommissionItemFailsAsItHasOpenDefects() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "decommissioned", "open defects"));

        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.defectDao.doAnyItemsHaveOpenDefects(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.validateDecommisionItem(ITEM_1_ID);
    }

    @Test
    public void validateDecommissionItemFailsAsItHasOpenServices() throws MastBusinessException, BadRequestException, RecordNotFoundException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(format(ExceptionMessagesUtils.ACTION_CANNOT_BE_TAKEN_FOR_ITEM, ITEM_1_ID, "decommissioned", "open services"));

        when(this.itemDao.getItemChildIdList(ITEM_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.serviceDao.doAnyItemsHaveOpenServices(CHILD_ID_LIST)).thenReturn(true);

        this.itemValidationService.validateDecommisionItem(ITEM_1_ID);
    }
}
