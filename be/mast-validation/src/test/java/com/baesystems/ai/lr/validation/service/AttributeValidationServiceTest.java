package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.validation.AttributeValidator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class AttributeValidationServiceTest
{
    private static ObjectMapper objectMapper;

    @Mock
    private ValidationService validationService;

    @Mock
    private AttributeDao attributeDao;

    @Mock
    private AttributeValidator attributeValidator;

    @InjectMocks
    private final AttributeValidationServiceImpl attributeValidationService = new AttributeValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long VALID_ATTRIBUTE_ID = 4L;
    private static final Long VALID_ITEM_ID = 96L;
    private static final Long VALID_ASSET_ID = 1L;
    private static final Long INVALID_ATTRIBUTE_ID = 999L;

    private static final List<String> RESULTS = new ArrayList<String>();
    private static final List<String> EMPTY_LIST = new ArrayList<String>();
    private static final String VALIDATION_ERROR = "An Error";

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();

        RESULTS.add(VALIDATION_ERROR);
    }

    @Test
    public void attributePassesValidation()
            throws JsonParseException, JsonMappingException, IOException, BadRequestException
    {
        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"), AttributeDto.class);

        when(this.attributeValidator.validate(attributeDto)).thenReturn(EMPTY_LIST);

        this.attributeValidationService.validateAttributeType(attributeDto);
    }

    @Test
    public void attributeFailsValidation()
            throws JsonParseException, JsonMappingException, IOException, BadRequestException
    {
        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"), AttributeDto.class);
        attributeDto.setValue("?");

        when(this.attributeValidator.validate(attributeDto)).thenReturn(RESULTS);

        thrown.expect(BadRequestException.class);

        this.attributeValidationService.validateAttributeType(attributeDto);
    }

    @Test
    public void attributePathPassesValidation() throws RecordNotFoundException
    {
        when(this.attributeDao.attributeExistsForItemAndAsset(VALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID))
                .thenReturn(true);

        this.attributeValidationService.verifyAssetAndItemHasAttribute(VALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void attributePathFailsValidation() throws RecordNotFoundException
    {
        when(this.attributeDao.attributeExistsForItemAndAsset(INVALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID))
                .thenReturn(false);

        thrown.expect(RecordNotFoundException.class);

        this.attributeValidationService.verifyAssetAndItemHasAttribute(INVALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void updateAttributesDoesNotFailWhenPassedNull() throws RecordNotFoundException, BadRequestException
    {
        this.attributeValidationService.validateAttributes(null, VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void updateAttributesDoesNotFailWhenPassedAnEmptyListOfAttributes() throws RecordNotFoundException, BadRequestException
    {
        this.attributeValidationService.validateAttributes(new ArrayList<AttributeDto>(), VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void updateAttributesFailsWhenPassedAnAttributeIdDoesNotExistForTheItemAndAsset() throws RecordNotFoundException, BadRequestException
    {
        when(this.attributeDao.attributeExistsForItemAndAsset(INVALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID))
                .thenReturn(false);

        final ArrayList<AttributeDto> attributes = new ArrayList<AttributeDto>();
        final AttributeDto attribute = new AttributeDto();
        attribute.setId(INVALID_ATTRIBUTE_ID);
        attributes.add(attribute);

        thrown.expect(RecordNotFoundException.class);

        this.attributeValidationService.validateAttributes(attributes, VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void updateAttributesFailsWhenPassedAttributeFailsValidation() throws RecordNotFoundException, BadRequestException, JsonParseException,
            JsonMappingException, IOException
    {
        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"), AttributeDto.class);
        attributeDto.setValue("?");

        final ArrayList<AttributeDto> attributes = new ArrayList<AttributeDto>();
        attributes.add(attributeDto);

        when(this.attributeDao.attributeExistsForItemAndAsset(VALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID))
                .thenReturn(true);

        when(this.attributeValidator.validate(attributeDto)).thenReturn(RESULTS);

        thrown.expect(BadRequestException.class);

        this.attributeValidationService.validateAttributes(attributes, VALID_ITEM_ID, VALID_ASSET_ID);
    }

    @Test
    public void updateAttributesSucceeds() throws RecordNotFoundException, BadRequestException, JsonParseException, JsonMappingException, IOException
    {
        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"), AttributeDto.class);
        attributeDto.setValue("?");

        final ArrayList<AttributeDto> attributes = new ArrayList<AttributeDto>();
        attributes.add(attributeDto);

        when(this.attributeDao.attributeExistsForItemAndAsset(VALID_ATTRIBUTE_ID, VALID_ITEM_ID, VALID_ASSET_ID))
                .thenReturn(true);

        when(this.attributeValidator.validate(attributeDto)).thenReturn(EMPTY_LIST);

        this.attributeValidationService.validateAttributes(attributes, VALID_ITEM_ID, VALID_ASSET_ID);
    }

}
