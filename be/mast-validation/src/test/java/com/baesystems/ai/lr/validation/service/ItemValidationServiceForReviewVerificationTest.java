package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.exception.MastBusinessException;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class ItemValidationServiceForReviewVerificationTest
{
    @Mock
    private ItemDao itemDao;

    @Mock
    private AttributeDao attributeDao;

    @InjectMocks
    private final ItemValidationServiceImpl itemValidationService = new ItemValidationServiceImpl();

    private static final Long TOP_LEVEL_ITEM_ID = 1L;
    private static final Long LEVEL_2_CHILD_1_ID = 21L;
    private static final Long LEVEL_2_CHILD_2_ID = 22L;
    private static final Long LEVEL_2_CHILD_3_ID = 23L;
    private static final Long LEVEL_3_CHILD_1_ID = 31L;
    private static final Long LEVEL_3_CHILD_2_ID = 32L;
    private static final Long LEVEL_3_CHILD_3_ID = 33L;
    private static final Long LEVEL_3_CHILD_4_ID = 33L;
    private static final Long LEVEL_4_CHILD_1_ID = 41L;
    private static final Long LEVEL_4_CHILD_2_ID = 42L;
    private static final Long LEVEL_4_CHILD_3_ID = 43L;
    private static final Long LEVEL_4_CHILD_4_ID = 44L;
    private static final Long LEVEL_4_CHILD_5_ID = 45L;

    private static final Long ITEM_TYPE_1_ID = 1L;
    private static final Long ITEM_TYPE_2_ID = 2L;
    private static final Long ITEM_TYPE_3_ID = 3L;
    private static final Long ITEM_TYPE_4_ID = 4L;
    private static final Long ITEM_TYPE_5_ID = 5L;

    private static final String LEVEL_2_CHILD_1_NAME = "level2Child1";
    private static final String LEVEL_2_CHILD_2_NAME = "level2Child2";
    private static final String LEVEL_2_CHILD_3_NAME = "level2Child3";
    private static final String LEVEL_3_CHILD_1_NAME = "level3Child1";
    private static final String LEVEL_3_CHILD_2_NAME = "level3Child2";
    private static final String LEVEL_3_CHILD_3_NAME = "level3Child3";
    private static final String LEVEL_4_CHILD_1_NAME = "level4Child1";
    private static final String LEVEL_4_CHILD_2_NAME = "level4Child2";
    private static final String LEVEL_4_CHILD_3_NAME = "level4Child3";
    private static final String LEVEL_4_CHILD_4_NAME = "level4Child4";
    private static final String LEVEL_4_CHILD_5_NAME = "level4Child5";

    private static ItemDto topLevelDto;
    private static ItemDto level2Child1;
    private static ItemDto level2Child2;
    private static ItemDto level2Child3;
    private static ItemDto level3Child1;
    private static ItemDto level3Child2;
    private static ItemDto level3Child3;
    private static ItemDto level3Child4;
    private static ItemDto level4Child1;
    private static ItemDto level4Child2;
    private static ItemDto level4Child3;
    private static ItemDto level4Child4;
    private static ItemDto level4Child5;

    private static final LinkResource ITEM_TYPE_1 = new LinkResource(ITEM_TYPE_1_ID);
    private static final LinkResource ITEM_TYPE_2 = new LinkResource(ITEM_TYPE_2_ID);
    private static final LinkResource ITEM_TYPE_3 = new LinkResource(ITEM_TYPE_3_ID);
    private static final LinkResource ITEM_TYPE_4 = new LinkResource(ITEM_TYPE_4_ID);
    private static final LinkResource ITEM_TYPE_5 = new LinkResource(ITEM_TYPE_5_ID);

    private static final Long ATTRIBUTE_ID_1 = 1L;
    private static final Long ATTRIBUTE_ID_2 = 2L;
    private static final Long ATTRIBUTE_ID_3 = 3L;

    private static final Long MANDATORY_ATTRIBUTE_TYPE_ID_1 = 1L;
    private static final Long MANDATORY_ATTRIBUTE_TYPE_ID_2 = 2L;
    private static final Long MANDATORY_ATTRIBUTE_TYPE_ID_3 = 3L;
    private static final Long MANDATORY_ATTRIBUTE_TYPE_ID_4 = 4L;

    private static final String DUMMY_ATTRIBUTE_VALUE = "TestAttributeValue";

    private static LinkVersionedResource level3c2Parent;
    private static LinkVersionedResource level3c3Parent;
    private static LinkVersionedResource level4c1Parent;
    private static LinkVersionedResource level4c2Parent;

    private static List<ItemDto> level2Items;
    private static List<ItemDto> level2c2Children;
    private static List<ItemDto> level2c3Children;
    private static List<ItemDto> level3c1Children;
    private static List<ItemDto> level3c2Children;

    @BeforeClass
    public static void createTestDtos()
    {

        // Here is the tree structure of dto objects set up below, and used at various points in the test.
        //
        // topLevelDto
        // - L2C1 (leaf)
        // - L2C2 (2 children)
        // --- L3C1 (2 children)
        // ------L4C1 (leaf)
        // ------L4C2 (leaf)
        // --- L3C2 (3 children)
        // ------L4C3 (leaf)
        // ------L4C4 (leaf)
        // ------L4C5 (leaf)
        // - L2C3 (2 children)
        // --- L3C3 (leaf)
        // --- L3C4 (leaf, with the same name as its peer)

        topLevelDto = new ItemDto();
        topLevelDto.setId(TOP_LEVEL_ITEM_ID);
        topLevelDto.setName("Top Level Dto");
        topLevelDto.setAttributes(new ArrayList<AttributeDto>());
        topLevelDto.setItemType(ITEM_TYPE_1);
        level2Items = new ArrayList<ItemDto>();
        topLevelDto.setItems(level2Items);

        setUpLevel2();
        setUpLevel3();
        setUpLevel4();
    }

    private static void setUpLevel2()
    {
        // TODO version uncertain.
        final LinkVersionedResource topLevelParent = new LinkVersionedResource(TOP_LEVEL_ITEM_ID, 1L);

        // Level 2, three children of top level node
        level2Child1 = new ItemDto();
        level2Child1.setId(LEVEL_2_CHILD_1_ID);
        level2Child1.setName(LEVEL_2_CHILD_1_NAME);
        level2Child1.setItemType(ITEM_TYPE_2);
        level2Child1.setParentItem(topLevelParent);
        level2Child1.setAttributes(new ArrayList<AttributeDto>());
        level2Items.add(level2Child1);

        level2Child2 = new ItemDto();
        level2Child2.setId(LEVEL_2_CHILD_2_ID);
        level2Child2.setName(LEVEL_2_CHILD_2_NAME);
        level2Child2.setItemType(ITEM_TYPE_3);
        level2Child2.setParentItem(topLevelParent);
        level2Child2.setAttributes(new ArrayList<AttributeDto>());
        level2c2Children = new ArrayList<ItemDto>();
        level2Child2.setItems(level2c2Children);
        level2Items.add(level2Child2);

        level2Child3 = new ItemDto();
        level2Child3.setId(LEVEL_2_CHILD_3_ID);
        level2Child3.setName(LEVEL_2_CHILD_3_NAME);
        level2Child3.setItemType(ITEM_TYPE_2);
        level2Child3.setParentItem(topLevelParent);
        level2Child3.setAttributes(new ArrayList<AttributeDto>());
        level2Child3.setReviewed(Boolean.FALSE);
        level2c3Children = new ArrayList<ItemDto>();
        level2Child3.setItems(level2c3Children);
        level2Items.add(level2Child3);
    }

    private static void setUpLevel3()
    {
        // Level 3. C2 has two children, C3 has two as well

        // TODO version uncertain.
        level3c2Parent = new LinkVersionedResource(LEVEL_2_CHILD_2_ID, 1L);
        level3c3Parent = new LinkVersionedResource(LEVEL_2_CHILD_3_ID, 1L);

        level3Child1 = new ItemDto();
        level3Child1.setId(LEVEL_3_CHILD_1_ID);
        level3Child1.setName(LEVEL_3_CHILD_1_NAME);
        level3Child1.setItemType(ITEM_TYPE_4);
        level3Child1.setParentItem(level3c2Parent);
        level3Child1.setAttributes(new ArrayList<AttributeDto>());
        level3c1Children = new ArrayList<ItemDto>();
        level3Child1.setItems(level3c1Children);
        level2c2Children.add(level3Child1);

        level3Child2 = new ItemDto();
        level3Child2.setId(LEVEL_3_CHILD_2_ID);
        level3Child2.setName(LEVEL_3_CHILD_2_NAME);
        level3Child2.setItemType(ITEM_TYPE_4);
        level3Child2.setParentItem(level3c2Parent);
        level3Child2.setAttributes(new ArrayList<AttributeDto>());
        level3c2Children = new ArrayList<ItemDto>();
        level3Child2.setItems(level3c2Children);
        level2c2Children.add(level3Child2);

        level3Child3 = new ItemDto();
        level3Child3.setId(LEVEL_3_CHILD_3_ID);
        level3Child3.setName(LEVEL_3_CHILD_3_NAME);
        level3Child3.setItemType(ITEM_TYPE_4);
        level3Child3.setParentItem(level3c3Parent);
        level3Child3.setAttributes(new ArrayList<AttributeDto>());
        level2c3Children.add(level3Child3);

        // This node is deliberately dodgy. It has the same name as its peer, L3C3
        level3Child4 = new ItemDto();
        level3Child4.setId(LEVEL_3_CHILD_4_ID);
        level3Child4.setName(LEVEL_3_CHILD_3_NAME);
        level3Child4.setItemType(ITEM_TYPE_4);
        level3Child4.setParentItem(level3c3Parent);
        level3Child4.setAttributes(new ArrayList<AttributeDto>());
        level2c3Children.add(level3Child4);
    }

    private static void setUpLevel4()
    {

        // TODO version uncertain.
        level4c1Parent = new LinkVersionedResource(LEVEL_3_CHILD_1_ID, 1L);

        level4c2Parent = new LinkVersionedResource(LEVEL_3_CHILD_2_ID, 1L);

        final ArrayList<AttributeDto> typeFiveAttributes = new ArrayList<AttributeDto>();

        final LinkResource attributeTypeOne = new LinkResource(ATTRIBUTE_ID_1);
        final LinkResource attributeTypeTwo = new LinkResource(ATTRIBUTE_ID_2);
        final LinkResource attributeTypeThree = new LinkResource(ATTRIBUTE_ID_3);

        final AttributeDto attributeOne = new AttributeDto();
        attributeOne.setId(ATTRIBUTE_ID_1);
        attributeOne.setAttributeType(attributeTypeOne);
        attributeOne.setValue(DUMMY_ATTRIBUTE_VALUE);

        final AttributeDto attributeTwo = new AttributeDto();
        attributeTwo.setId(ATTRIBUTE_ID_2);
        attributeTwo.setAttributeType(attributeTypeTwo);
        attributeTwo.setValue(DUMMY_ATTRIBUTE_VALUE);

        final AttributeDto attributeThree = new AttributeDto();
        attributeThree.setId(ATTRIBUTE_ID_3);
        attributeThree.setAttributeType(attributeTypeThree);
        attributeThree.setValue(DUMMY_ATTRIBUTE_VALUE);

        typeFiveAttributes.add(attributeOne);
        typeFiveAttributes.add(attributeTwo);
        typeFiveAttributes.add(attributeThree);

        level4Child1 = new ItemDto();
        level4Child1.setId(LEVEL_4_CHILD_1_ID);
        level4Child1.setName(LEVEL_4_CHILD_1_NAME);
        level4Child1.setItemType(ITEM_TYPE_5);
        level4Child1.setParentItem(level4c1Parent);
        level4Child1.setAttributes(typeFiveAttributes);
        level3c1Children.add(level4Child1);

        level4Child2 = new ItemDto();
        level4Child2.setId(LEVEL_4_CHILD_2_ID);
        level4Child2.setName(LEVEL_4_CHILD_2_NAME);
        level4Child2.setItemType(ITEM_TYPE_5);
        level4Child2.setParentItem(level4c1Parent);
        level4Child2.setAttributes(typeFiveAttributes);
        level3c1Children.add(level4Child2);

        level4Child3 = new ItemDto();
        level4Child3.setId(LEVEL_4_CHILD_3_ID);
        level4Child3.setName(LEVEL_4_CHILD_3_NAME);
        level4Child3.setItemType(ITEM_TYPE_5);
        level4Child3.setParentItem(level4c2Parent);
        level4Child3.setAttributes(typeFiveAttributes);
        level3c2Children.add(level4Child3);

        level4Child4 = new ItemDto();
        level4Child4.setId(LEVEL_4_CHILD_4_ID);
        level4Child4.setName(LEVEL_4_CHILD_4_NAME);
        level4Child4.setItemType(ITEM_TYPE_5);
        level4Child4.setParentItem(level4c2Parent);
        level4Child4.setAttributes(typeFiveAttributes);
        level3c2Children.add(level4Child4);

        level4Child5 = new ItemDto();
        level4Child5.setId(LEVEL_4_CHILD_5_ID);
        level4Child5.setName(LEVEL_4_CHILD_5_NAME);
        level4Child5.setItemType(ITEM_TYPE_5);
        level4Child5.setParentItem(level4c2Parent);
        level4Child5.setAttributes(typeFiveAttributes);
        level3c2Children.add(level4Child5);
    }

    @Test
    public void doNotTryToVerifyIfTheReviewStateHasNotChanged()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(LEVEL_2_CHILD_2_ID);
        mockedReturnedDto.setReviewed(Boolean.TRUE);

        // when(this.itemDao.getItem(LEVEL_2_CHILD_2_ID)).thenReturn(mockedReturnedDto);

        try
        {
            this.itemValidationService.validateItemReview(LEVEL_2_CHILD_2_ID);
        }
        catch (MastBusinessException e)
        {
            Assert.fail(e.getMessage());
        }

        // Mockito.verify(this.itemDao).getItem(LEVEL_2_CHILD_2_ID);
        Mockito.verifyNoMoreInteractions(this.itemDao);
    }

    @Test
    public void traversalOfSuccessfulHierarchy()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(LEVEL_2_CHILD_2_ID);
        mockedReturnedDto.setReviewed(Boolean.FALSE);

        // when(this.itemDao.getItem(LEVEL_2_CHILD_2_ID)).thenReturn(mockedReturnedDto);

        when(this.itemDao.getItemWithFullHierarchy(LEVEL_2_CHILD_2_ID)).thenReturn(level2Child2);
        when(this.itemDao.isItemNameUnique(LEVEL_2_CHILD_2_NAME, TOP_LEVEL_ITEM_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_3_CHILD_1_NAME, LEVEL_2_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_3_CHILD_2_NAME, LEVEL_2_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_1_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_2_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_3_NAME, LEVEL_3_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_4_NAME, LEVEL_3_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_5_NAME, LEVEL_3_CHILD_2_ID)).thenReturn(true);

        when(this.attributeDao.getMandatoryAttributeTypeIdsForAnItemTypeId(ITEM_TYPE_5_ID)).thenReturn(
                Arrays.asList(MANDATORY_ATTRIBUTE_TYPE_ID_1, MANDATORY_ATTRIBUTE_TYPE_ID_2, MANDATORY_ATTRIBUTE_TYPE_ID_3));

        try
        {
            this.itemValidationService.validateItemReview(LEVEL_2_CHILD_2_ID);
        }
        catch (MastBusinessException e)
        {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void traversalOfHierarchyWithANonUniqueNameFailure()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(LEVEL_2_CHILD_3_ID);
        mockedReturnedDto.setReviewed(Boolean.FALSE);

        // when(this.itemDao.getItem(LEVEL_2_CHILD_3_ID)).thenReturn(mockedReturnedDto);

        when(this.itemDao.getItemWithFullHierarchy(LEVEL_2_CHILD_3_ID)).thenReturn(level2Child3);
        when(this.itemDao.isItemNameUnique(LEVEL_2_CHILD_3_NAME, TOP_LEVEL_ITEM_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_3_CHILD_3_NAME, LEVEL_2_CHILD_3_ID)).thenReturn(false);

        try
        {
            this.itemValidationService.validateItemReview(LEVEL_2_CHILD_3_ID);
            Assert.fail("Expected a MastBusinessException to be thrown.");
        }
        catch (MastBusinessException e)
        {
            Assert.assertEquals("The Item level3Child3 cannot be marked as complete because there is another completed item with the same name.",
                    e.getMessage());
        }
    }

    @Test
    public void traversalOfHierarchyWithAMissingAttributeFailure()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(LEVEL_3_CHILD_1_ID);
        mockedReturnedDto.setReviewed(Boolean.FALSE);

        // when(this.itemDao.getItem(LEVEL_3_CHILD_1_ID)).thenReturn(mockedReturnedDto);

        when(this.itemDao.getItemWithFullHierarchy(LEVEL_3_CHILD_1_ID)).thenReturn(level3Child1);
        when(this.itemDao.isItemNameUnique(LEVEL_3_CHILD_1_NAME, LEVEL_2_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_1_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_2_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);

        when(this.attributeDao.getMandatoryAttributeTypeIdsForAnItemTypeId(ITEM_TYPE_5_ID)).thenReturn(
                Arrays.asList(MANDATORY_ATTRIBUTE_TYPE_ID_1, MANDATORY_ATTRIBUTE_TYPE_ID_2, MANDATORY_ATTRIBUTE_TYPE_ID_3,
                        MANDATORY_ATTRIBUTE_TYPE_ID_4));

        try
        {
            this.itemValidationService.validateItemReview(LEVEL_3_CHILD_1_ID);
            Assert.fail("Expected a MastBusinessException to be thrown.");
        }
        catch (MastBusinessException e)
        {
            Assert.assertEquals("The Item level4Child1 cannot be marked as complete because it is missing mandatory attributes.", e.getMessage());
        }
    }

    @Test
    public void traversalOfHierarchyWithASingleDatabaseCallForAttributeIds()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(LEVEL_3_CHILD_1_ID);
        mockedReturnedDto.setReviewed(Boolean.FALSE);

        // when(this.itemDao.getItem(LEVEL_3_CHILD_1_ID)).thenReturn(mockedReturnedDto);

        when(this.itemDao.getItemWithFullHierarchy(LEVEL_3_CHILD_1_ID)).thenReturn(level3Child1);
        when(this.itemDao.isItemNameUnique(LEVEL_3_CHILD_1_NAME, LEVEL_2_CHILD_2_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_1_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);
        when(this.itemDao.isItemNameUnique(LEVEL_4_CHILD_2_NAME, LEVEL_3_CHILD_1_ID)).thenReturn(true);

        final IllegalArgumentException iae = new IllegalArgumentException("Did not expect the db to be called more than once.");
        when(this.attributeDao.getMandatoryAttributeTypeIdsForAnItemTypeId(ITEM_TYPE_5_ID)).thenReturn(
                Arrays.asList(MANDATORY_ATTRIBUTE_TYPE_ID_1, MANDATORY_ATTRIBUTE_TYPE_ID_2, MANDATORY_ATTRIBUTE_TYPE_ID_3)).thenThrow(iae);

        try
        {
            this.itemValidationService.validateItemReview(LEVEL_3_CHILD_1_ID);
        }
        catch (MastBusinessException e)
        {
            Assert.fail("Expected a MastBusinessException to be thrown.");
        }
        catch (IllegalArgumentException e)
        {
            Assert.fail("Database called more than once to retrieve attribute type data.");
        }
    }

}
