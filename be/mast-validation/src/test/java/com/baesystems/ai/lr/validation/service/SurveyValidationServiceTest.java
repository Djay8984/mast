package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@RunWith(MockitoJUnitRunner.class)
public class SurveyValidationServiceTest
{
    private static final Long JOB_ID = 1L;
    private static final Long SURVEY_ID = 2L;
    private static final Long OTHER_ID = 99L;

    private SurveyDto createSurveyDto;
    private SurveyDto updateSurveyDto;

    @Mock
    private JobValidationService jobValidationService;

    @Mock
    private SurveyDao surveyDao;

    @Spy
    private ValidationServiceImpl validationService = new ValidationServiceImpl();

    @InjectMocks
    private SurveyValidationServiceImpl surveyValidationService = new SurveyValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup()
    {
        createSurveyDto = new SurveyDto();
        createSurveyDto.setJob(new LinkResource(JOB_ID));

        updateSurveyDto = new SurveyDto();
        updateSurveyDto.setId(SURVEY_ID);
        updateSurveyDto.setJob(new LinkResource(JOB_ID));
    }

    @Test
    public void createSurveyPasses() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).validate(createSurveyDto);
        this.surveyValidationService.validateSurveyCreate(JOB_ID, createSurveyDto);
    }

    @Test
    public void createSurveyFailsIdShouldBeNull() throws BadRequestException, RecordNotFoundException
    {
        createSurveyDto.setId(SURVEY_ID);

        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.ID_NOT_NULL, SURVEY_ID));
        doNothing().when(this.validationService).validate(createSurveyDto);

        this.surveyValidationService.validateSurveyCreate(JOB_ID, createSurveyDto);
    }

    @Test
    public void createSurveyFailsHeaderJobIdNull() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, null, "ID"));

        this.surveyValidationService.validateSurveyCreate(null, createSurveyDto);
    }

    @Test
    public void createSurveyFailsBodyNull() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(ExceptionMessagesUtils.BODY_NOT_NULL);

        this.surveyValidationService.validateSurveyCreate(JOB_ID, null);
    }

    @Test
    public void createSurveyFailsJobIdNotFound() throws BadRequestException, RecordNotFoundException
    {
        final String message = String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, JOB_ID);

        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(message);

        doNothing().when(this.validationService).validate(createSurveyDto);
        doThrow(new RecordNotFoundException(message)).when(this.jobValidationService).verifyJobIdExists(JOB_ID.toString());

        this.surveyValidationService.validateSurveyCreate(JOB_ID, createSurveyDto);
    }

    @Test
    public void createSurveyFailsJobIdDoesNotMatchBody() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", createSurveyDto.getJob().getId(), OTHER_ID));

        doNothing().when(this.validationService).validate(createSurveyDto);
        this.surveyValidationService.validateSurveyCreate(OTHER_ID, createSurveyDto);
    }

    @Test
    public void updateSurveyPasses() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).validate(updateSurveyDto);
        when(this.surveyDao.surveyExists(SURVEY_ID)).thenReturn(true);
        when(this.surveyDao.surveyExistsForJob(JOB_ID, SURVEY_ID)).thenReturn(true);
        this.surveyValidationService.validateSurveyUpdate(JOB_ID, SURVEY_ID, updateSurveyDto);
    }

    @Test
    public void updateSurveyFailsIdShouldNotBeNull() throws BadRequestException, RecordNotFoundException
    {
        updateSurveyDto.setId(null);

        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, null, "ID"));
        doNothing().when(this.validationService).validate(updateSurveyDto);

        this.surveyValidationService.validateSurveyUpdate(JOB_ID, null, updateSurveyDto);
    }

    @Test
    public void updateSurveyFailsHeaderJobIdNull() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BINDING_ERROR_MSG, null, "ID"));

        this.surveyValidationService.validateSurveyUpdate(null, SURVEY_ID, updateSurveyDto);
    }

    @Test
    public void updateSurveyFailsBodyNull() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(ExceptionMessagesUtils.BODY_NOT_NULL);

        this.surveyValidationService.validateSurveyUpdate(JOB_ID, SURVEY_ID, null);
    }

    @Test
    public void updateSurveyFailsJobIdNotFound() throws BadRequestException, RecordNotFoundException
    {
        final String message = String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, JOB_ID);

        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(message);

        doNothing().when(this.validationService).validate(updateSurveyDto);
        doThrow(new RecordNotFoundException(message)).when(this.jobValidationService).verifyJobIdExists(JOB_ID.toString());

        this.surveyValidationService.validateSurveyUpdate(JOB_ID, SURVEY_ID, updateSurveyDto);
    }

    @Test
    public void updateSurveyFailsJobIdDoesNotMatchBody() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", updateSurveyDto.getJob().getId(), OTHER_ID));

        doNothing().when(this.validationService).validate(updateSurveyDto);
        this.surveyValidationService.validateSurveyUpdate(OTHER_ID, SURVEY_ID, updateSurveyDto);
    }

    @Test
    public void updateSurveyFailsSurveyIdDoesNotMatchBody() throws BadRequestException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Survey", updateSurveyDto.getId(), OTHER_ID));

        doNothing().when(this.validationService).validate(updateSurveyDto);
        when(this.surveyDao.surveyExists(OTHER_ID)).thenReturn(true);
        when(this.surveyDao.surveyExistsForJob(JOB_ID, OTHER_ID)).thenReturn(true);
        this.surveyValidationService.validateSurveyUpdate(JOB_ID, OTHER_ID, updateSurveyDto);
    }
}
