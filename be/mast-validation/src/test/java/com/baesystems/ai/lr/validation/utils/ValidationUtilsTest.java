package com.baesystems.ai.lr.validation.utils;

import static com.baesystems.ai.lr.validation.utils.ValidationMessageUtils.FIELD_CANNOT_BE_NULL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationUtilsTest
{
    private List<String> results;
    private static final String OBJECT_NAME = "anObject";
    private static final String MESSAGE = "An optional message.";
    private static final Object OBJECT = new Object();

    @InjectMocks
    private final ValidationUtils validationUtils = new ValidationUtils();

    private String genarateMessage(final String fieldName, final String message)
    {
        return String.format(FIELD_CANNOT_BE_NULL, fieldName) + message;
    }

    private String genarateMessage(final String fieldName)
    {
        return String.format(FIELD_CANNOT_BE_NULL, fieldName);
    }

    @Before
    public void setUp()
    {
        results = new ArrayList<String>();
    }

    @Test
    public void testValidatePresencePass()
    {
        this.validationUtils.validatePresence(OBJECT, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidatePresenceFail()
    {
        this.validationUtils.validatePresence(null, OBJECT_NAME, results, MESSAGE);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME, MESSAGE), results.get(0));
    }

    @Test
    public void testValidatePresencePassNoMessage()
    {
        this.validationUtils.validatePresence(OBJECT, OBJECT_NAME, results);

        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testValidatePresenceFailNoMessage()
    {
        this.validationUtils.validatePresence(null, OBJECT_NAME, results);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(genarateMessage(OBJECT_NAME), results.get(0));
    }
}
