package com.baesystems.ai.lr.validation.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.references.attachments.AttachmentReferenceDataDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.service.validation.JobValidationService;
import com.baesystems.ai.lr.service.validation.UpdateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class ActionableItemValidationServiceTest
{
    private static ObjectMapper objectMapper;

    @Mock
    private ActionableItemDao actionableItemDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Mock
    private ItemValidationService itemValidationService;

    @Mock
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Mock
    private AttachmentReferenceDataDao attachmentReferenceDataDao;

    @Mock
    private AssetValidationService assetValidationService;

    @Mock
    private JobValidationService jobValidationService;

    @Mock
    private ValidationService validationService;

    @Mock
    private UpdateValidationService updateValidationService;

    @Mock
    private ServiceHelper serviceHelper;

    @Mock
    private DateHelper dateHelper;

    @InjectMocks
    private final ActionableItemValidationServiceImpl actionableItemValidationService = new ActionableItemValidationServiceImpl();

    @InjectMocks
    private final WIPActionableItemValidationServiceImpl wipActionableItemValidationService = new WIPActionableItemValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long ASSET_1_ID = 1L;
    private static final Long ACTIONABLE_ITEM_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long CODICIL_CATEGORY_ID = 50L;
    private static final Long CONFIDENTIALITY_TYPE_ID = 60L;
    private static final Long STATUS_ID = 70L;
    private static final Long INVALID_ACTIONABLE_ITEM = 999L;
    private static final ReferenceDataDto REF_DATA = new ReferenceDataDto();

    private static final String JOB_1_ID_STRING = String.valueOf(JOB_1_ID);
    private static final String ACTIONABLE_ITEM_1_ID_STRING = String.valueOf(ACTIONABLE_ITEM_1_ID);

    private ActionableItemDto actionableItemDto;
    private CodicilTemplateDto template;

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();
    }

    @Before
    public void setUp() throws IOException
    {
        actionableItemDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/actionableItemValidationTest/valid.json"), ActionableItemDto.class);
        template = objectMapper.readValue(
                this.getClass().getResourceAsStream("/actionableItemValidationTest/validTemplate.json"), CodicilTemplateDto.class);
    }

    @Test
    public void testVerifyAssetHasActionableItem() throws RecordNotFoundException
    {
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID))
                .thenReturn(true);
        this.actionableItemValidationService.verifyAssetHasActionableItem(ASSET_1_ID, ACTIONABLE_ITEM_1_ID);
    }

    @Test
    public void testVerifyAssetHasActionableItemFails() throws RecordNotFoundException
    {
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, INVALID_ACTIONABLE_ITEM))
                .thenReturn(false);
        thrown.expect(RecordNotFoundException.class);
        this.actionableItemValidationService.verifyAssetHasActionableItem(ASSET_1_ID, ACTIONABLE_ITEM_1_ID);
    }

    @Test
    public void testUpdateWithoutTemplateOrItem() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setTemplate(null);
        actionableItemDto.setAssetItem(null);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithoutTemplateOrItemCategoryDoesNotExist() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setTemplate(null);
        actionableItemDto.setAssetItem(null);

        thrown.expect(RecordNotFoundException.class);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(null);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(eq(null), any(Long.class));
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithoutTemplateOrItemConfidentialityTypeDoesNotExist()
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setTemplate(null);
        actionableItemDto.setAssetItem(null);

        thrown.expect(RecordNotFoundException.class);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(null);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(eq(null), any(Long.class));
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithoutTemplateWithItem() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setTemplate(null);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        doNothing().when(this.itemValidationService).verifyAssetHasItem(ASSET_1_ID.toString(), actionableItemDto.getAssetItem().getId().toString());
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithTemplateWithoutItem() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setAssetItem(null);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        when(this.codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithTemplateWithoutItemTemplateNotFound() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setAssetItem(null);

        thrown.expect(RecordNotFoundException.class);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        when(this.codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(null);
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithTemplateWithoutItemMissmatch() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        actionableItemDto.setAssetItem(null);
        actionableItemDto.setTitle("null");

        thrown.expect(MastBusinessException.class);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        when(this.codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithTemplateAndItem() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        doNothing().when(this.itemValidationService).verifyAssetHasItem(ASSET_1_ID.toString(), actionableItemDto.getAssetItem().getId().toString());
        when(this.codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        doNothing().when(this.itemValidationService).verifyItemIsCorrectType(actionableItemDto.getAssetItem().getId(),
                template.getItemType().getId());
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void testUpdateWithTemplateAndItemItemDoesNotMatch() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        thrown.expect(MastBusinessException.class);

        doNothing().when(this.updateValidationService).verifyIds(ACTIONABLE_ITEM_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString());
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());
        doNothing().when(this.validationService).verifyBody(actionableItemDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(actionableItemDto.getCategory().getId())).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(actionableItemDto.getConfidentialityType().getId())).thenReturn(REF_DATA);
        doNothing().when(this.serviceHelper).verifyModel(eq(REF_DATA), any(Long.class));
        doNothing().when(this.itemValidationService).verifyAssetHasItem(ASSET_1_ID.toString(), actionableItemDto.getAssetItem().getId().toString());
        when(this.codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        doThrow(new MastBusinessException()).when(this.itemValidationService).verifyItemIsCorrectType(actionableItemDto.getAssetItem().getId(),
                template.getItemType().getId());
        when(this.actionableItemDao.actionableItemExistsForAsset(ASSET_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(actionableItemDto.getImposedDate())).thenReturn(true);
        this.actionableItemValidationService.verifyActionableItemPut(ASSET_1_ID.toString(), ACTIONABLE_ITEM_1_ID.toString(), actionableItemDto,
                Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfItHasAnId() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(BadRequestException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        actionableItem.setId(ACTIONABLE_ITEM_1_ID);

        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(Long.valueOf(ACTIONABLE_ITEM_1_ID));

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfTheJobIdDoesNotExist() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();

        doThrow(new RecordNotFoundException()).when(this.jobValidationService).verifyJobIdExists(JOB_1_ID_STRING);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfTheBodyIsInvalid() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(BadRequestException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();

        doThrow(new BadRequestException()).when(this.validationService).verifyBody(actionableItem);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfCodicilCategoryWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfConfidentialityTypeWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(CONFIDENTIALITY_TYPE_ID)).thenReturn(REF_DATA);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfStatusIsWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        final LinkResource status = new LinkResource(STATUS_ID);
        actionableItem.setStatus(status);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, STATUS_ID);

        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(CONFIDENTIALITY_TYPE_ID)).thenReturn(REF_DATA);
        when(this.codicilReferenceDataDao.getCodicilStatus(STATUS_ID)).thenReturn(REF_DATA);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsIfImposedDateIsHistorical() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(BadRequestException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        final Date imposedDate = new Date();
        actionableItem.setImposedDate(imposedDate);

        // As date checking is mocked, it's enough that a date is present rather than old.
        when(this.dateHelper.isTodayOrAfter(imposedDate)).thenReturn(false);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemSucceedsWithNoStatusDefectEmployeeOrTemplate() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);
        actionableItem.setJobScopeConfirmed(Boolean.TRUE);
        actionableItem.setDueDate(new Date());

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        final Date imposedDate = new Date();
        actionableItem.setImposedDate(imposedDate);

        when(this.dateHelper.isTodayOrAfter(imposedDate)).thenReturn(true);

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyCreateWipActionableItemFailsWithNullJobScopeConfirmed() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);
        actionableItem.setDueDate(new Date());

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        final Date imposedDate = new Date();
        actionableItem.setImposedDate(imposedDate);

        when(this.dateHelper.isTodayOrAfter(imposedDate)).thenReturn(true);

        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));

        this.wipActionableItemValidationService.verifyWIPActionableItemPost(JOB_1_ID_STRING, actionableItem, Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfItHasNoId() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(BadRequestException.class);

        doThrow(new BadRequestException()).when(this.validationService).verifyId(ACTIONABLE_ITEM_1_ID_STRING);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(null, ACTIONABLE_ITEM_1_ID_STRING, null, Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfTheJobIdDoesNotExist() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();

        doThrow(new RecordNotFoundException()).when(this.jobValidationService).verifyJobIdExists(JOB_1_ID_STRING);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfTheBodyIsInvalid() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(BadRequestException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();

        doThrow(new BadRequestException()).when(this.validationService).verifyBody(actionableItem);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfCodicilCategoryWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfConfidentialityTypeWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(CONFIDENTIALITY_TYPE_ID)).thenReturn(REF_DATA);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfStatusIsWrong() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        final LinkResource status = new LinkResource(STATUS_ID);
        actionableItem.setStatus(status);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        doThrow(new RecordNotFoundException()).when(this.serviceHelper).verifyModel(REF_DATA, STATUS_ID);

        when(this.codicilReferenceDataDao.getCodicilCategory(CODICIL_CATEGORY_ID)).thenReturn(REF_DATA);
        when(this.attachmentReferenceDataDao.getConfidentialityType(CONFIDENTIALITY_TYPE_ID)).thenReturn(REF_DATA);
        when(this.codicilReferenceDataDao.getCodicilStatus(STATUS_ID)).thenReturn(REF_DATA);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemFailsIfActionableItemNotAssociatedWithJob() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);

        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);
        actionableItem.setDueDate(new Date());
        actionableItem.setJobScopeConfirmed(Boolean.TRUE);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        when(this.wipActionableItemDao.actionableItemExistsForJob(JOB_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(false);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyUpdateWipActionableItemSucceedsWithNoStatusDefectEmployeeOrTemplate() throws BadRequestException, RecordNotFoundException,
            MastBusinessException
    {
        final ActionableItemDto actionableItem = new ActionableItemDto();
        final LinkResource category = new LinkResource(CODICIL_CATEGORY_ID);
        actionableItem.setCategory(category);
        actionableItem.setDueDate(new Date());
        actionableItem.setJobScopeConfirmed(Boolean.TRUE);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        actionableItem.setConfidentialityType(confidentialityType);

        doNothing().when(this.validationService).verifyBody(actionableItem);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CODICIL_CATEGORY_ID);
        doNothing().when(this.serviceHelper).verifyModel(REF_DATA, CONFIDENTIALITY_TYPE_ID);

        final Date imposedDate = new Date();
        actionableItem.setImposedDate(imposedDate);

        when(this.dateHelper.isTodayOrAfter(imposedDate)).thenReturn(true);

        when(this.wipActionableItemDao.actionableItemExistsForJob(JOB_1_ID, ACTIONABLE_ITEM_1_ID)).thenReturn(true);

        this.wipActionableItemValidationService.verifyWIPActionableItemPut(JOB_1_ID_STRING, ACTIONABLE_ITEM_1_ID_STRING, actionableItem,
                Boolean.FALSE);
    }

    @Test
    public void verifyParentPassesWhenPassedNull()
    {
        try
        {
            wipActionableItemValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(null);
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentPassesWhenPassedAValidParentCodicil()
    {
        final Long parentCodicilId = 20L;

        Mockito.when(actionableItemDao.getActionableItem(parentCodicilId)).thenReturn(new ActionableItemDto());

        try
        {
            wipActionableItemValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentFailsWhenPassedAnInvalidParentCodicil()
    {
        final Long parentCodicilId = 20L;

        Mockito.when(actionableItemDao.getActionableItem(parentCodicilId)).thenReturn(null);

        try
        {
            wipActionableItemValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
            Assert.fail("Expected an exception to be thrown.");
        }
        catch (final RecordNotFoundException e)
        {
            // Do nothing. This is expected
        }
    }

    @Test
    public void verifyFailWhenDueDateNull() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"));
        actionableItemDto.setDueDate(null);
        when(codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        actionableItemValidationService.verifyActionableItemPost(actionableItemDto, Boolean.FALSE);
    }

    @Test
    public void verifyPassWhenDueDateNotNull() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        when(codicilReferenceDataDao.getCodicilTemplate(actionableItemDto.getTemplate().getId())).thenReturn(template);
        when(this.dateHelper.isTodayOrAfter(actionableItemDto.getImposedDate())).thenReturn(true);
        actionableItemValidationService.verifyActionableItemPost(actionableItemDto, Boolean.TRUE);
    }
}
