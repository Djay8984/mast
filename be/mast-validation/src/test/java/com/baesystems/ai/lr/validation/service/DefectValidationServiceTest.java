package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.references.defects.DefectReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(DefectValidationServiceImpl.class)
public class DefectValidationServiceTest
{
    private static final Long DEFECT_1_ID = 1L;
    private static final Long VALUE_1_ID = 1L;
    private static final Long INVALID_VALUE_ID = 9999L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long INVALID_ITEM_ID = 9999L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long INVALID_DEFECT_ID = 7874L;
    private static final Long VALID_JOB_ID = 1L;
    private static final Long VALID_WIP_DEFECT_ID = 1L;
    private static final Long INVALID_WIP_DEFECT_ID = 9999L;
    private static final Long VALID_PARENT_ID = 1L;
    private static final Long VALID_DEFECT_CATEGORY_1_ID = 1L;
    private static final Long VALID_DEFECT_CATEGORY_2_ID = 2L;

    private DefectDto defect;

    @Mock
    private ValidationService validationService;

    @Mock
    private DefectReferenceDataDao defectReferenceDataDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private WIPDefectDao wipDefectDao;

    @Mock
    private DefectDao defectDao;

    @InjectMocks
    private final DefectValidationServiceImpl defectValidationService = new DefectValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp()
    {
        defect = new DefectDto();
    }

    private void setUpDefectValues()
    {
        defect.setValues(new ArrayList<DefectDefectValueDto>());
        defect.getValues().add(new DefectDefectValueDto());
        defect.getValues().get(0).setDefectValue(new DefectValueDto());
    }

    private void setUpDefectItems()
    {
        defect.setItems(new ArrayList<DefectItemDto>());
        defect.getItems().add(new DefectItemDto());
        defect.getItems().get(0).setItem(new ItemLightDto());
    }

    @Test
    public void createDefectPassesValidationWithNullValues() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectFailsValidationWithNullBody() throws BadRequestException, RecordNotFoundException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.defectValidationService.validateDefectCreate(null);
    }

    @Test
    public void createDefectFailsValidationWithId() throws BadRequestException, RecordNotFoundException
    {
        defect.setId(DEFECT_1_ID);

        doNothing().when(this.validationService).verifyBody(defect);
        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(defect.getId());

        thrown.expect(BadRequestException.class);

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectPassesValidationWithNoValues() throws BadRequestException, RecordNotFoundException
    {
        defect.setValues(new ArrayList<DefectDefectValueDto>());

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectPassesValidationWithValidValue() throws Exception
    {
        setUpDefectValues();
        defect.getValues().get(0).getDefectValue().setId(VALUE_1_ID);
        final List<Long> idList = new ArrayList<Long>();
        idList.add(VALUE_1_ID);

        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn((ArrayList<Long>) idList);

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doNothing().when(this.validationService).addIdToIdList(defect.getValues().get(0).getDefectValue(), idList);
        when(this.defectReferenceDataDao.countMatchingDefectValueIds(idList)).thenReturn(idList.size());

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectFailsValidationWithNullValueId() throws BadRequestException, RecordNotFoundException
    {
        setUpDefectValues();
        defect.getValues().get(0).getDefectValue().setId(null);
        final List<Long> idList = new ArrayList<Long>();

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doThrow(new BadRequestException()).when(this.validationService).addIdToIdList(defect.getValues().get(0).getDefectValue(), idList);

        thrown.expect(BadRequestException.class);

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectFaildsValidationWithInvalidValueId() throws Exception
    {
        setUpDefectValues();
        defect.getValues().get(0).getDefectValue().setId(INVALID_VALUE_ID);
        final List<Long> idList = new ArrayList<Long>();
        idList.add(INVALID_VALUE_ID);

        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn((ArrayList<Long>) idList);

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doNothing().when(this.validationService).addIdToIdList(defect.getValues().get(0).getDefectValue(), idList);
        when(this.defectReferenceDataDao.countMatchingDefectValueIds(idList)).thenReturn(idList.size() - 1);

        thrown.expect(RecordNotFoundException.class);

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectPassesValidationWithValidItem() throws Exception
    {
        setUpDefectItems();
        defect.getItems().get(0).getItem().setId(ITEM_1_ID);
        final List<Long> idList = new ArrayList<Long>();
        idList.add(ITEM_1_ID);

        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn((ArrayList<Long>) idList);

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doNothing().when(this.validationService).addIdToIdList(defect.getItems().get(0).getItem(), idList);
        when(this.assetDao.countMatchingItemIds(idList)).thenReturn(idList.size());

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectFailsValidationWithNullItemId() throws BadRequestException, RecordNotFoundException
    {
        setUpDefectItems();
        defect.getItems().get(0).getItem().setId(null);
        final List<Long> idList = new ArrayList<Long>();

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doThrow(new BadRequestException()).when(this.validationService).addIdToIdList(defect.getItems().get(0).getItem(), idList);

        thrown.expect(BadRequestException.class);

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void createDefectFaildsValidationWithInvalidItemId() throws Exception
    {
        setUpDefectItems();
        defect.getItems().get(0).getItem().setId(INVALID_ITEM_ID);
        final List<Long> idList = new ArrayList<Long>();
        idList.add(INVALID_ITEM_ID);

        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn((ArrayList<Long>) idList);

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyIdNull(defect.getId());
        doNothing().when(this.validationService).addIdToIdList(defect.getItems().get(0).getItem(), idList);
        when(this.assetDao.countMatchingItemIds(idList)).thenReturn(idList.size() - 1);

        thrown.expect(RecordNotFoundException.class);

        this.defectValidationService.validateDefectCreate(defect);
    }

    @Test
    public void updateDefectPassesValidationWithNullValues() throws BadRequestException, RecordNotFoundException
    {
        defect.setId(DEFECT_1_ID);

        doNothing().when(this.validationService).verifyBody(defect);
        doNothing().when(this.validationService).verifyId(defect.getId());

        this.defectValidationService.validateDefectUpdate(defect.getId(), defect);
    }

    @Test
    public void updateDefectFailsValidationWithNullId() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).verifyBody(defect);
        doThrow(new BadRequestException()).when(this.validationService).verifyId(defect.getId());

        thrown.expect(BadRequestException.class);

        this.defectValidationService.validateDefectUpdate(defect.getId(), defect);
    }

    @Test
    public void testVerifyAssetHasDefect() throws RecordNotFoundException
    {

        when(defectDao.defectExistsForAsset(ASSET_1_ID, DEFECT_1_ID)).thenReturn(true);

        this.defectValidationService.verifyAssetHasDefect(ASSET_1_ID, DEFECT_1_ID);
    }

    @Test
    public void testVerifyAssetHasDefectFails() throws RecordNotFoundException
    {
        when(defectDao.defectExistsForAsset(ASSET_1_ID, INVALID_DEFECT_ID)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        this.defectValidationService.verifyAssetHasDefect(ASSET_1_ID, INVALID_DEFECT_ID);
    }

    @Test
    public void testVerifyWIPDefectExistsForJob() throws RecordNotFoundException
    {
        when(wipDefectDao.defectExistsForJob(VALID_JOB_ID, VALID_WIP_DEFECT_ID)).thenReturn(true);

        defectValidationService.verifyJobHasWIPDefect(VALID_JOB_ID, VALID_WIP_DEFECT_ID);
    }

    @Test
    public void testVerifyInvalidWIPDefectExistsForJobFails() throws RecordNotFoundException
    {
        when(wipDefectDao.defectExistsForJob(VALID_JOB_ID, INVALID_WIP_DEFECT_ID)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        defectValidationService.verifyJobHasWIPDefect(VALID_JOB_ID, INVALID_WIP_DEFECT_ID);
    }

    @Test
    public void testVerifyWIPDefectTitleImmutableField() throws RecordNotFoundException
    {
        final DefectDto originalDefectDto = getDefectDto(true);

        final DefectDto modifiedDefectDto = getDefectDto(true);
        modifiedDefectDto.setTitle("This is not the original DefectDto title");

        when(wipDefectDao.getDefect(VALID_WIP_DEFECT_ID)).thenReturn(originalDefectDto);

        try
        {
            defectValidationService.verifyWIPDefectImmutableFields(modifiedDefectDto);
            Assert.fail("Expected WIP Defect modified title to throw BadRequestException");
        }
        catch (BadRequestException e)
        {
            Assert.assertEquals(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Title"), e.getMessage());
        }
    }

    @Test
    public void testVerifyWIPDefectCategoryImmutableField() throws RecordNotFoundException
    {
        final DefectDto originalDefectDto = getDefectDto(true);

        final DefectDto modifiedDefectDto = getDefectDto(true);
        modifiedDefectDto.getDefectCategory().setId(VALID_DEFECT_CATEGORY_2_ID);

        when(wipDefectDao.getDefect(VALID_WIP_DEFECT_ID)).thenReturn(originalDefectDto);

        try
        {
            defectValidationService.verifyWIPDefectImmutableFields(modifiedDefectDto);
            Assert.fail("Expected WIP Defect modified category to throw BadRequestException.");
        }
        catch (BadRequestException e)
        {
            Assert.assertEquals(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Category"), e.getMessage());
        }

    }

    @Test
    public void testVerifyMutableFieldsWithinCurrentJob()
    {
        final DefectDto originalDefectDto = getDefectDto(false);
        final DefectDto modifiedDefectDto = getDefectDto(false);

        modifiedDefectDto.setTitle("This is change should be allowed within current job");
        modifiedDefectDto.getDefectCategory().setId(VALID_DEFECT_CATEGORY_2_ID);

        when(wipDefectDao.getDefect(VALID_WIP_DEFECT_ID)).thenReturn(originalDefectDto);

        try
        {
            defectValidationService.verifyWIPDefectImmutableFields(modifiedDefectDto);
        }
        catch (BadRequestException | RecordNotFoundException e)
        {
            Assert.fail("Expected modification to Title or Category to be allowed: " + e.getMessage());
        }

    }

    /**
     * Create a valid DefectDto with optional parent.
     *
     * @param outsideCurrentJob if TRUE sets the Parent to be NON null.
     * @return valid DefectDto.
     */
    private DefectDto getDefectDto(boolean outsideCurrentJob)
    {
        final DefectDto defectDto = new DefectDto();
        defectDto.setId(VALID_WIP_DEFECT_ID);
        defectDto.setTitle("Original Title");
        defectDto.setDefectCategory(new DefectCategoryDto());
        defectDto.getDefectCategory().setDescription("Defect Category description");
        defectDto.getDefectCategory().setName("Defect category name");
        defectDto.getDefectCategory().setId(VALID_DEFECT_CATEGORY_1_ID);
        if (outsideCurrentJob)
        {
            defectDto.setParent(new LinkResource(VALID_PARENT_ID));
        }

        return defectDto;
    }

}
