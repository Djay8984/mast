package com.baesystems.ai.lr.validation.service;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.ServiceReferenceDataService;
import com.baesystems.ai.lr.service.validation.ItemValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class CoCValidationServiceTest
{
    private static final Long CONFIDENTIALITY_TYPE_ID = 200L;
    private static final Long CATEGORY_ID = 500L;
    private static final Long INVALID_ITEM_TYPE_ID = 123L;
    private static final Long TEMPLATE_ID = 50L;
    private static final Long INVALID_ASSET_ITEM_ID = 90L;
    private static final Long COC_ID = 1L;
    private static final Long COC_ID_2 = 2L;
    private static final Long DEFECT_ID_1 = 1L;
    private static final Long DEFECT_ID_2 = 2L;

    private static ObjectMapper objectMapper;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Mock
    private ValidationService validationService;

    @Mock
    private ItemValidationService itemValidationService;

    @Mock
    private CoCDao cocDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @InjectMocks
    private final CoCValidationServiceImpl coCValidationService = new CoCValidationServiceImpl();

    @Mock
    private ServiceReferenceDataService serviceReferenceDataService;

    @Mock
    private DateHelper dateHelper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testVerifyAssetHasCoC() throws BadRequestException, RecordNotFoundException, IOException
    {
        final Long cocId = 20L;
        final Long assetId = 30L;

        when(cocDao.coCExistsForAsset(assetId, cocId)).thenReturn(true);
        coCValidationService.verifyAssetHasCoC(assetId, cocId);
    }

    @Test
    public void testVerifyAssetHasCoCFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final Long cocId = 20L;
        final Long assetId = 30L;

        when(cocDao.coCExistsForAsset(assetId, cocId)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        coCValidationService.verifyAssetHasCoC(assetId, cocId);
    }

    public void testVerifyJobHasWIPCoC() throws BadRequestException, RecordNotFoundException, IOException
    {
        final CoCDto coCDto = getTestDto();
        final Long cocId = coCDto.getId();
        final Long jobId = 1L; // assume, not contained in dto

        when(wipCoCDao.coCExistsForJob(jobId, cocId)).thenReturn(true);
        coCValidationService.verifyJobHasWIPCoC(jobId, cocId);
    }

    @Test
    public void testVerifyJobHasWIPCoCFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final CoCDto coCDto = getTestDto();
        final Long cocId = coCDto.getId();
        final Long jobId = 1L; // assume, not contained in dto

        when(wipCoCDao.coCExistsForJob(jobId, cocId)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        coCValidationService.verifyJobHasWIPCoC(jobId, cocId);
    }

    @Test
    public void testVerifyImposedDueDateNullDto()
    {
        try
        {
            coCValidationService.verifyImposedDueDate(null);
            Assert.fail("Expected NULL Dto to throw exception");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("CocDto is null", e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyImposedDateNotNull()
    {
        final CoCDto cocDto = new CoCDto();
        cocDto.setImposedDate(null);

        try
        {
            coCValidationService.verifyImposedDueDate(cocDto);
            Assert.fail("Expected NULL imposed date to throw exception");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Imposed date is null", e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyDueDateNotNull()
    {
        final CoCDto cocDto = new CoCDto();
        cocDto.setImposedDate(new Date());
        cocDto.setDueDate(null);

        try
        {
            coCValidationService.verifyImposedDueDate(cocDto);
            Assert.fail("Expected NULL due date to throw exception");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Due date is null", e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyImposedDateNotHistorical() throws ParseException
    {
        final CoCDto cocDto = new CoCDto();
        final String historical = "2000-10-01";

        cocDto.setDueDate(new Date());
        cocDto.setImposedDate(dateFormat.parse(historical));

        when(this.dateHelper.isTodayOrAfter(any(Date.class))).thenReturn(false);

        try
        {
            coCValidationService.verifyImposedDueDate(cocDto);
            Assert.fail("Expected non-historical imposed date.");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Imposed date is in the past: 2000-10-01.", e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyDueMayNotBeforeImposedDate()
    {
        final CoCDto cocDto = new CoCDto();
        final Date currentDate = new Date();
        cocDto.setImposedDate(currentDate);
        final Date yesterdaysDate = DateUtils.addDays(currentDate, -1);
        cocDto.setDueDate(yesterdaysDate);

        when(this.dateHelper.isTodayOrAfter(any(Date.class))).thenReturn(true);

        try
        {
            coCValidationService.verifyImposedDueDate(cocDto);
            Assert.fail("Due date is before imposed date.");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Due date is before imposed date: " + dateFormat.format(yesterdaysDate), e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyEditedImposedDateRejected()
    {
        final CoCDto originalCocDto = new CoCDto();
        final CoCDto editedCocDto = new CoCDto();
        final Date currentDate = new Date();
        final Date yesterdaysDate = DateUtils.addDays(currentDate, -1);

        originalCocDto.setImposedDate(yesterdaysDate);
        originalCocDto.setId(COC_ID);
        editedCocDto.setImposedDate(currentDate);
        editedCocDto.setId(COC_ID);

        when(cocDao.getCoC(COC_ID)).thenReturn(originalCocDto);

        try
        {
            coCValidationService.verifyEditedImposedDueDate(editedCocDto);
            Assert.fail("Attempt to modify imposed date.");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Attempting to modify imposed date. Original: "
                    + dateFormat.format(originalCocDto.getImposedDate())
                    + " Modified: "
                    + dateFormat.format(editedCocDto.getImposedDate()),
                    e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyEditedDueNotBeforeImposed()
    {
        final CoCDto cocDto = new CoCDto();
        final Date currentDate = new Date();
        cocDto.setImposedDate(currentDate);
        final Date yesterdaysDate = DateUtils.addDays(currentDate, -1);
        cocDto.setDueDate(yesterdaysDate);
        cocDto.setId(COC_ID);

        when(cocDao.getCoC(COC_ID)).thenReturn(cocDto);

        try
        {
            coCValidationService.verifyEditedImposedDueDate(cocDto);
            Assert.fail("Due date is before imposed date.");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Due date is before imposed date: " + dateFormat.format(yesterdaysDate), e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyEditedDueInFuture()
    {
        final CoCDto originalCocDto = new CoCDto();
        final CoCDto editedCocDto = new CoCDto();
        final Date currentDate = new Date();
        final Date imposedDate = DateUtils.addDays(currentDate, -10);
        final Date originalDueDate = DateUtils.addDays(currentDate, -5);
        final Date editedDueDate = DateUtils.addDays(currentDate, -1);

        originalCocDto.setImposedDate(imposedDate);
        editedCocDto.setImposedDate(imposedDate);

        originalCocDto.setDueDate(originalDueDate);
        editedCocDto.setDueDate(editedDueDate);

        originalCocDto.setId(COC_ID);
        editedCocDto.setId(COC_ID);

        when(cocDao.getCoC(COC_ID)).thenReturn(originalCocDto);
        when(this.dateHelper.isTodayOrAfter(any(Date.class))).thenReturn(false);

        try
        {
            coCValidationService.verifyEditedImposedDueDate(editedCocDto);
            Assert.fail("Edited due date is not current or in future.");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("Edited due date is in the past: " + dateFormat.format(editedDueDate), e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testVerifyNullDtoOnTemplateVerification()
    {
        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(null);
            Assert.fail("Expected NULL Dto to throw exception");
        }
        catch (final BadRequestException e)
        {
            Assert.assertEquals("CocDto is null", e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
        catch (RecordNotFoundException | MastBusinessException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void testTemplateDoesNotExistOnTemplateVerification()
    {
        final CoCDto cocDto = new CoCDto();
        cocDto.setTemplate(makeLinkResource(0L));
        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(cocDto);
            Assert.fail("Template should not exist.");
        }
        catch (BadRequestException | MastBusinessException e)
        {
            Assert.fail("Unexpected exception.");
        }
        catch (final RecordNotFoundException e)
        {
            Assert.assertEquals("Entity not found for identifier: 0.", e.getMessage());
            Assert.assertEquals(HttpStatus.NOT_FOUND.value(), e.getErrorCode());
        }
    }

    /**
     * This test exercises the case when a CoC dto with a template id contains details that do not match the template it
     * is associated with.
     */
    @Test
    public void testVerifyTemplateDoesNotMatch()
    {
        final ReferenceDataDto confidentialityTypeDataDto = new ReferenceDataDto();
        confidentialityTypeDataDto.setName("ConfidentialityType");

        confidentialityTypeDataDto.setId(CONFIDENTIALITY_TYPE_ID);
        final ReferenceDataDto codicilCategoryDataDto = new ReferenceDataDto();
        codicilCategoryDataDto.setName("codicilCategory");
        codicilCategoryDataDto.setId(CATEGORY_ID);

        final CodicilTemplateDto mockTemplateDto = new CodicilTemplateDto();
        mockTemplateDto.setId(TEMPLATE_ID);

        mockTemplateDto.setTitle("MockTemplate");
        mockTemplateDto.setDescription("this is a mock");
        mockTemplateDto.setSurveyorGuidance("Surveyor Guidance String");
        mockTemplateDto.setConfidentialityType(confidentialityTypeDataDto);
        mockTemplateDto.setEditableBySurveyor(true);
        mockTemplateDto.setCategory(codicilCategoryDataDto);

        when(codicilReferenceDataDao.getCodicilTemplate(TEMPLATE_ID)).thenReturn(mockTemplateDto);
        final CoCDto cocDto = new CoCDto();
        cocDto.setTemplate(makeLinkResource(TEMPLATE_ID));
        cocDto.setTitle("cocDto");
        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        cocDto.setConfidentialityType(confidentialityType);

        final LinkResource category = new LinkResource(CATEGORY_ID);
        cocDto.setCategory(category);
        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(cocDto);
            Assert.fail("Expected template to not match.");
        }
        catch (BadRequestException | RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
        catch (final MastBusinessException e)
        {
            Assert.assertEquals(HttpStatus.CONFLICT.value(), e.getErrorCode());
            Assert.assertEquals("The CoC entered does not match the template it is linked to.", e.getMessage());
        }

    }

    /**
     * This test exercises the case when a CoC dto data matches template data.
     */
    @Test
    public void testVerifyTemplateMatches()
    {

        final String templateTitle = "MockTemplate";
        final String templateDescription = "templateDescription";
        final String surveyorGuidance = "Surveyor Guidance String";
        final boolean editableBySurveyor = true;
        final ReferenceDataDto confidentialityTypeDataDto = new ReferenceDataDto();
        confidentialityTypeDataDto.setName("ConfidentialityType");
        confidentialityTypeDataDto.setId(CONFIDENTIALITY_TYPE_ID);
        final ReferenceDataDto codicilCategoryDataDto = new ReferenceDataDto();
        codicilCategoryDataDto.setName("codicilCategory");
        codicilCategoryDataDto.setId(CATEGORY_ID);

        final CodicilTemplateDto mockTemplateDto = new CodicilTemplateDto();
        mockTemplateDto.setId(TEMPLATE_ID);
        mockTemplateDto.setTitle(templateTitle);
        mockTemplateDto.setDescription(templateDescription);
        mockTemplateDto.setSurveyorGuidance(surveyorGuidance);
        mockTemplateDto.setConfidentialityType(confidentialityTypeDataDto);
        mockTemplateDto.setEditableBySurveyor(editableBySurveyor);
        mockTemplateDto.setCategory(codicilCategoryDataDto);

        when(codicilReferenceDataDao.getCodicilTemplate(TEMPLATE_ID)).thenReturn(mockTemplateDto);

        final CoCDto cocDto = new CoCDto();
        cocDto.setTemplate(makeLinkResource(TEMPLATE_ID));
        cocDto.setTitle(templateTitle);
        cocDto.setDescription(templateDescription);
        cocDto.setSurveyorGuidance(surveyorGuidance);
        cocDto.setEditableBySurveyor(editableBySurveyor);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        cocDto.setConfidentialityType(confidentialityType);

        final LinkResource category = new LinkResource(CATEGORY_ID);
        cocDto.setCategory(category);
        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(cocDto);
        }
        catch (BadRequestException | RecordNotFoundException | MastBusinessException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    /**
     * This test exercises the case when a CoC dto data matches template data where all of the fields that can be null,
     * are.
     */
    @Test
    public void testVerifyTemplateMatchesWithNullFields()
    {

        final String templateTitle = "MockTemplate";
        final String templateDescription = "templateDescription";
        final ReferenceDataDto confidentialityTypeDataDto = new ReferenceDataDto();
        confidentialityTypeDataDto.setName("ConfidentialityType");
        confidentialityTypeDataDto.setId(CONFIDENTIALITY_TYPE_ID);
        final ReferenceDataDto codicilCategoryDataDto = new ReferenceDataDto();
        codicilCategoryDataDto.setName("codicilCategory");
        codicilCategoryDataDto.setId(CATEGORY_ID);

        final CodicilTemplateDto mockTemplateDto = new CodicilTemplateDto();
        mockTemplateDto.setId(TEMPLATE_ID);
        mockTemplateDto.setTitle(templateTitle);
        mockTemplateDto.setDescription(templateDescription);
        mockTemplateDto.setConfidentialityType(confidentialityTypeDataDto);
        mockTemplateDto.setCategory(codicilCategoryDataDto);

        when(codicilReferenceDataDao.getCodicilTemplate(TEMPLATE_ID)).thenReturn(mockTemplateDto);

        final CoCDto cocDto = new CoCDto();
        cocDto.setTemplate(makeLinkResource(TEMPLATE_ID));
        cocDto.setTitle(templateTitle);
        cocDto.setDescription(templateDescription);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        cocDto.setConfidentialityType(confidentialityType);

        final LinkResource category = new LinkResource(CATEGORY_ID);
        cocDto.setCategory(category);
        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(cocDto);
        }
        catch (BadRequestException | RecordNotFoundException | MastBusinessException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    /**
     * This test exercises the case when an incorrect assetItem breaks validation (through a mock db call)
     *
     * @throws MastBusinessException
     */
    @Test
    public void testVerifyTemplateMatchWithAssetItemCheckFailure() throws MastBusinessException
    {

        final String templateTitle = "MockTemplate";
        final String templateDescription = "templateDescription";
        final ReferenceDataDto confidentialityTypeDataDto = new ReferenceDataDto();
        confidentialityTypeDataDto.setName("ConfidentialityType");
        confidentialityTypeDataDto.setId(CONFIDENTIALITY_TYPE_ID);
        final ReferenceDataDto codicilCategoryDataDto = new ReferenceDataDto();
        codicilCategoryDataDto.setName("codicilCategory");
        codicilCategoryDataDto.setId(CATEGORY_ID);

        final CodicilTemplateDto mockTemplateDto = new CodicilTemplateDto();
        mockTemplateDto.setId(TEMPLATE_ID);
        mockTemplateDto.setTitle(templateTitle);
        mockTemplateDto.setDescription(templateDescription);
        mockTemplateDto.setConfidentialityType(confidentialityTypeDataDto);
        mockTemplateDto.setCategory(codicilCategoryDataDto);
        final ItemTypeDto itemType = new ItemTypeDto();
        itemType.setId(INVALID_ITEM_TYPE_ID);
        mockTemplateDto.setItemType(itemType);

        when(codicilReferenceDataDao.getCodicilTemplate(TEMPLATE_ID)).thenReturn(mockTemplateDto);

        final CoCDto cocDto = new CoCDto();
        cocDto.setTemplate(makeLinkResource(TEMPLATE_ID));
        cocDto.setTitle(templateTitle);
        cocDto.setDescription(templateDescription);

        final LinkResource confidentialityType = new LinkResource(CONFIDENTIALITY_TYPE_ID);
        cocDto.setConfidentialityType(confidentialityType);

        final LinkResource category = new LinkResource(CATEGORY_ID);
        cocDto.setCategory(category);

        final LinkResource assetItem = new LinkResource(INVALID_ASSET_ITEM_ID);
        cocDto.setAssetItem(assetItem);

        final MastBusinessException mockedMbe = new MastBusinessException("Asset Id Problem");
        doThrow(mockedMbe).when(itemValidationService).verifyItemIsCorrectType(INVALID_ASSET_ITEM_ID, INVALID_ITEM_TYPE_ID);

        try
        {
            coCValidationService.verifyTemplatedFieldsCorrect(cocDto);
            Assert.fail("Expected a MastBusinessException to be thrown.");
        }
        catch (BadRequestException | RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
        catch (final MastBusinessException e)
        {
            Assert.assertEquals(mockedMbe, e);
        }
    }

    @Test
    public void verifyParentPassesWhenPassedNull()
    {
        try
        {
            coCValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(null);
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentPassesWhenPassedAValidParentCodicil()
    {
        final Long parentCodicilId = 20L;

        when(cocDao.getCoC(parentCodicilId)).thenReturn(new CoCDto());

        try
        {
            coCValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentFailsWhenPassedAnInvalidParentCodicil()
    {
        final Long parentCodicilId = 20L;

        when(cocDao.getCoC(parentCodicilId)).thenReturn(null);

        try
        {
            coCValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
            Assert.fail("Expected an exception to be thrown.");
        }
        catch (final RecordNotFoundException e)
        {
            // Do nothing. This is expected
        }
    }

    @Test
    public void testMatchingIdsSuccess() throws IOException, BadRequestException
    {
        final CoCDto coc = getTestDto();
        coc.setId(null);
        final LinkResource defect = new LinkResource(DEFECT_ID_1);
        coc.setDefect(defect);
        this.coCValidationService.validateDefectIdMatches(DEFECT_ID_1.toString(), coc);
    }

    @Test
    public void testVerifyMismatchedIdsFails() throws BadRequestException, IOException
    {
        final CoCDto coc = getTestDto();
        coc.setId(null);
        final LinkResource defect = new LinkResource(DEFECT_ID_2);
        coc.setDefect(defect);
        final String message = String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "WIP defect", DEFECT_ID_2.toString(), DEFECT_ID_1.toString());
        final BadRequestException mockedBre = new BadRequestException(message);
        doThrow(mockedBre).when(validationService).verifyField(defect.getId().toString(), DEFECT_ID_1.toString(), message);
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(message);
        this.coCValidationService.validateDefectIdMatches(DEFECT_ID_1.toString(), coc);
    }

    @Test
    public void testWIPDefectHasWIPCoCSuccess() throws BadRequestException, IOException, RecordNotFoundException
    {
        final List<WIPCoCDO> results = Arrays.asList(new WIPCoCDO(), new WIPCoCDO());
        results.get(0).setDefect(new LinkDO());
        results.get(0).getDefect().setId(DEFECT_ID_1);
        results.get(0).setId(COC_ID);
        results.get(1).setDefect(new LinkDO());
        results.get(1).getDefect().setId(DEFECT_ID_2);
        results.get(1).setId(COC_ID_2);
        when(wipCoCDao.getCoCsForDefect(DEFECT_ID_1)).thenReturn(results);

        this.coCValidationService.verifyWIPDefectHasWIPCoC(DEFECT_ID_1, COC_ID);

    }

    @Test
    public void testWIPDefectHasWIPCoCFailsOnNullResult() throws RecordNotFoundException
    {
        when(wipCoCDao.getCoCsForDefect(DEFECT_ID_1)).thenReturn(null);
        thrown.expect(RecordNotFoundException.class);
        thrown.expectMessage(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG);

        this.coCValidationService.verifyWIPDefectHasWIPCoC(DEFECT_ID_1, COC_ID);
    }

    @Test
    public void testVerifyCoCUpdateFieldsSuccess() throws IOException, BadRequestException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = getTestDto();
        originalCoCDto.setId(COC_ID);
        originalCoCDto.setParent(null);
        originalCoCDto.setDescription("Test description");
        originalCoCDto.setDueDate(new Date());
        when(cocDao.getCoC(COC_ID)).thenReturn(originalCoCDto);

        // changing the description and due date is permitted
        final CoCDto updatedCoCDto = getTestDto();
        updatedCoCDto.setDescription("I'm changing this description");
        updatedCoCDto.setId(COC_ID);

        this.coCValidationService.verifyCoCImmutableFields(updatedCoCDto);
    }

    @Test
    public void testVerifyCoCUpdateFieldsFails() throws IOException, BadRequestException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = getTestDto();
        originalCoCDto.setId(COC_ID);
        originalCoCDto.setParent(null);
        originalCoCDto.setTitle("Test title");
        originalCoCDto.setDueDate(new Date());
        when(cocDao.getCoC(COC_ID)).thenReturn(originalCoCDto);

        final CoCDto updatedCoCDto = getTestDto();
        updatedCoCDto.setTitle("I'm changing this title");
        updatedCoCDto.setDueDate(DateUtils.addDays(new Date(), 2));
        updatedCoCDto.setId(COC_ID);

        this.thrown.expect(BadRequestException.class);
        this.coCValidationService.verifyCoCImmutableFields(updatedCoCDto);
    }

    @Test
    public void testVerifyWIPCoCUpdateFieldsWithinJob() throws IOException, BadRequestException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = getTestDto();
        originalCoCDto.setId(COC_ID);
        originalCoCDto.setParent(null);
        originalCoCDto.setTitle("Test title");
        originalCoCDto.setDescription("This description can change");
        originalCoCDto.setDueDate(new Date());
        when(wipCoCDao.getCoC(COC_ID)).thenReturn(originalCoCDto);

        final CoCDto updatedCoCDto = getTestDto();
        updatedCoCDto.setTitle("I'm changing this title");
        updatedCoCDto.setDescription("This description change should be allowed.");
        updatedCoCDto.setDueDate(DateUtils.addDays(new Date(), 2));
        updatedCoCDto.setId(COC_ID);

        this.coCValidationService.verifyWIPCoCImmutableFields(updatedCoCDto);
    }

    @Test
    @Ignore
    public void testVerifyWIPCoCUpdateFieldsOutsideJob() throws IOException, RecordNotFoundException
    {
        final CoCDto originalCoCDto = getTestDto();
        originalCoCDto.setId(COC_ID);
        originalCoCDto.setParent(new LinkResource());
        originalCoCDto.setTitle("Test title");
        originalCoCDto.setDescription("This description can change");
        originalCoCDto.setDueDate(new Date());
        originalCoCDto.setImposedDate(DateUtils.addDays(new Date(), 1));
        when(wipCoCDao.getCoC(COC_ID)).thenReturn(originalCoCDto);

        final CoCDto updatedCoCDto = getTestDto();
        updatedCoCDto.setTitle("This title change should NOT be allowed");
        updatedCoCDto.setDescription("This description change should be allowed.");
        updatedCoCDto.setDueDate(DateUtils.addDays(new Date(), 2));  // Allowed
        updatedCoCDto.setImposedDate(DateUtils.addDays(new Date(), 2)); // NOT allowed
        updatedCoCDto.setId(COC_ID);

        try
        {
            this.coCValidationService.verifyWIPCoCImmutableFields(updatedCoCDto);
        }
        catch (final BadRequestException e)
        {
            Assert.assertTrue(e.getMessage().toLowerCase().contains("title"));
            Assert.assertTrue(e.getMessage().toLowerCase().contains("parent"));
            Assert.assertTrue(e.getMessage().toLowerCase().contains("imposeddate"));
            Assert.assertTrue(!e.getMessage().toLowerCase().contains("description"));
            Assert.assertTrue(!e.getMessage().toLowerCase().contains("duedate"));
        }
    }

    @Test
    public void testValidateNonNullJobScopeConfirmed() throws IOException, BadRequestException
    {
        final CoCDto cocDto = getTestDto();
        cocDto.setJobScopeConfirmed(Boolean.TRUE);
        this.coCValidationService.validateWIPJobScopeConfirmed(cocDto);

    }

    @Test
    public void testValidateNullJobScopeConfirmed() throws IOException, BadRequestException
    {
        final CoCDto cocDto = getTestDto();
        cocDto.setJobScopeConfirmed(null);
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));
        this.coCValidationService.validateWIPJobScopeConfirmed(cocDto);
    }

    private CoCDto getTestDto() throws IOException
    {
        return objectMapper.readValue(
                this.getClass().getResourceAsStream("/coCValidationServiceTest/CoCDto.json"), CoCDto.class);
    }

    private LinkResource makeLinkResource(final Long subId)
    {
        final LinkResource resource = new LinkResource(subId);

        return resource;
    }

}
