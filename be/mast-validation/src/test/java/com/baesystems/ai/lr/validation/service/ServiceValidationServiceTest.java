package com.baesystems.ai.lr.validation.service;

import static org.mockito.AdditionalMatchers.and;
import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.codicils.CodicilDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.enums.ServiceType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ServiceDateValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class ServiceValidationServiceTest
{

    private static final int MONTHS_FROM_NOW_FOR_HARMONISATION_DATE_CHECK = 3;

    @Mock
    private ValidationService validationService;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private ServiceService serviceService;

    @Mock
    private ProductDao productDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private CodicilDao codicilDao;

    @Mock
    private ServiceValidationHelper serviceValidationHelper;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Mock
    private ServiceDateValidationService serviceDateValidation;

    @Mock
    private DateHelper dateHelper;

    @Mock
    private AssetValidationService assetValidationService;

    @Mock
    private ServiceReferenceDataValidation serviceReferenceDataValidation;

    @Mock
    private ServiceDateValidationService dateValidator;

    @InjectMocks
    private final ServiceValidationServiceImpl serviceValidationService = new ServiceValidationServiceImpl();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static ObjectMapper objectMapper;

    private static final Long ZERO = 0L;
    private static final Long ONE = 1L;
    private static final Long MAX_ID = 999999999L;

    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final String ASSET_1_NAME = "Asset One Name";
    private static final Long PRODUCT_1_ID = 1L;
    private static final Long SERVICE_1_ID = 1L;
    private static final Long SERVICE_2_ID = 2L;
    private static final Long ASSET_ITEM_500_ID = 500L;

    private static final Long SERVICE_CATALOGUE_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_2_ID = 2L;
    private static final Long PRODUCT_CATALOGUE_1_ID = 1L;
    private static final Long PRODUCT_CATALOGUE_2_ID = 2L;
    private static final Long PRODUCT_CATALOGUE_3_ID = 3L;
    private static final Long PRODUCT_CATALOGUE_4_ID = 4L;

    private static final Long PRODUCT_ID_5 = 5L;
    private static final Long PRODUCT_ID_6 = 6L;
    private static final Long PRODUCT_ID_7 = 7L;

    private static final int THREE_MONTHS = 3;

    private static final List<String> RESULTS = new ArrayList<String>();
    private static final String VALIDATION_ERROR = "An Error";

    private static final SimpleDateFormat DATE_FORMAT = DateHelper.mastFormatter();

    private final ProductDO product = new ProductDO();
    private final ProductCatalogueDO productCatalogueOne = new ProductCatalogueDO();
    private final ProductCatalogueDO productCatalogueTwo = new ProductCatalogueDO();
    private final ServiceCatalogueDO serviceCatalogueDo1 = new ServiceCatalogueDO();
    private final ServiceCatalogueDO serviceCatalogueDo2 = new ServiceCatalogueDO();

    private final ProductCatalogueDto productCatalogue1 = new ProductCatalogueDto();
    private final ServiceCatalogueDto serviceCatalogue1 = new ServiceCatalogueDto();
    private final ServiceCatalogueDto serviceCatalogue2 = new ServiceCatalogueDto();

    private ScheduledServiceDto goodService;
    private ScheduledServiceDto goodService2;
    private ScheduledServiceDto badService;
    private ScheduledServiceDto updateableService;

    private Date assignedDate;
    private Date dueDate;
    private Date lowerRangeDate;
    private Date upperRangeDate;

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();

        RESULTS.add(VALIDATION_ERROR);
    }

    @Before
    public void setUp() throws JsonParseException, JsonMappingException, IOException, ParseException
    {
        this.goodService = objectMapper.readValue(
                this.getClass().getResourceAsStream("/serviceValidationTest/validService.json"), ScheduledServiceDto.class);
        this.goodService2 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/serviceValidationTest/validService.json"), ScheduledServiceDto.class);
        this.badService = objectMapper.readValue(
                this.getClass().getResourceAsStream("/serviceValidationTest/badServiceCatalogueId.json"), ScheduledServiceDto.class);

        this.goodService2.setServiceCatalogueId(SERVICE_CATALOGUE_2_ID);

        this.productCatalogueOne.setId(PRODUCT_CATALOGUE_1_ID);
        this.productCatalogueTwo.setId(PRODUCT_CATALOGUE_2_ID);

        this.serviceCatalogueDo1.setId(SERVICE_CATALOGUE_1_ID);
        this.serviceCatalogueDo1.setProductCatalogue(this.productCatalogueOne);

        this.serviceCatalogueDo2.setId(SERVICE_CATALOGUE_2_ID);
        this.serviceCatalogueDo2.setProductCatalogue(this.productCatalogueOne);

        this.product.setId(PRODUCT_1_ID);
        this.product.setProductCatalogue(new LinkDO());
        this.product.getProductCatalogue().setId(this.productCatalogueOne.getId());

        this.assignedDate = DATE_FORMAT.parse("2015-10-25");
        this.dueDate = DATE_FORMAT.parse("2016-10-25");
        this.lowerRangeDate = DATE_FORMAT.parse("2016-10-15");
        this.upperRangeDate = DATE_FORMAT.parse("2016-11-05");

        this.updateableService = getService(SERVICE_1_ID);

        this.productCatalogue1.setId(PRODUCT_CATALOGUE_1_ID);

        this.serviceCatalogue1.setId(SERVICE_CATALOGUE_1_ID);
        this.serviceCatalogue1.setProductCatalogue(this.productCatalogue1);
        this.serviceCatalogue1.setCyclePeriodicity(ONE.intValue());
        this.serviceCatalogue1.setCyclePeriodicityEditable(true);

        this.serviceCatalogue2.setId(SERVICE_CATALOGUE_2_ID);
        this.serviceCatalogue2.setProductCatalogue(this.productCatalogue1);
        this.serviceCatalogue2.setCyclePeriodicity(ONE.intValue());
        this.serviceCatalogue2.setCyclePeriodicityEditable(true);
    }

    private ScheduledServiceDto getService(final Long serviceId) throws ParseException
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();

        service.setId(serviceId);
        service.setServiceCatalogueId(SERVICE_CATALOGUE_1_ID);
        service.setAssignedDateManual(this.assignedDate);
        service.setDueDateManual(this.dueDate);
        service.setLowerRangeDateManual(this.lowerRangeDate);
        service.setUpperRangeDateManual(this.upperRangeDate);
        service.setActive(true);
        service.setAssetItem(new LinkResource(ASSET_ITEM_500_ID));

        return service;
    }

    private DateDto createDateDto(final Date date)
    {
        final DateDto dateDto = new DateDto();
        dateDto.setDate(date);
        return dateDto;
    }

    @Test
    public void serviceFailsServiceCatalogueNotFound() throws BadRequestException, RecordNotFoundException, ParseException, MastBusinessException
    {
        doThrow(new RecordNotFoundException()).when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService);
        doNothing().when(this.validationService).verifyBody(this.goodService);

        this.thrown.expect(RecordNotFoundException.class);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), wrapService(this.goodService));
    }

    @Test
    public void serviceFailsValidation() throws RecordNotFoundException, BadRequestException, ParseException, MastBusinessException
    {
        final ScheduledServiceListDto serviceList = wrapService(this.badService);
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(serviceList);

        this.thrown.expect(BadRequestException.class);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), serviceList);
    }

    @Test
    public void servicePassesValidation() throws RecordNotFoundException, BadRequestException, ParseException, MastBusinessException
    {
        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService);
        when(this.serviceDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, null)).thenReturn(true);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), wrapService(this.goodService));
    }

    @Test
    public void serviceFailsValidationNoProductCatalogueSelected()
            throws RecordNotFoundException, BadRequestException, ParseException, MastBusinessException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.DEPENDENCY_ERROR_MIXED_TYPE, "service catalogue",
                SERVICE_CATALOGUE_1_ID, "product catalogue", PRODUCT_CATALOGUE_1_ID));

        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService);
        when(this.serviceDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, null)).thenReturn(false);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), wrapService(this.goodService));
    }

    @Test
    public void multipleServicesPassesValidation() throws RecordNotFoundException, BadRequestException, ParseException, MastBusinessException
    {
        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        doNothing().when(this.validationService).verifyField(ASSET_1_ID.toString(), ASSET_1_ID.toString(),
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", ASSET_1_ID.toString(), ASSET_1_ID.toString()));
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService);
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService2);
        when(this.serviceDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        when(this.serviceDao.getServiceCatalogue(SERVICE_CATALOGUE_2_ID)).thenReturn(this.serviceCatalogue2);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, null)).thenReturn(true);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), wrapService(this.goodService, this.goodService2));
    }

    @Test
    public void multipleServicesFailsValidationMixedAssetId()
            throws RecordNotFoundException, BadRequestException, ParseException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);

        this.goodService2.setAsset(new LinkResource(ASSET_2_ID));
        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        doNothing().when(this.validationService).verifyField(ASSET_1_ID.toString(), ASSET_1_ID.toString(),
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", ASSET_1_ID.toString(), ASSET_1_ID.toString()));
        doThrow(new BadRequestException()).when(this.validationService).verifyField(ASSET_2_ID.toString(), ASSET_1_ID.toString(),
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", ASSET_2_ID.toString(), ASSET_1_ID.toString()));
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService);
        doNothing().when(this.serviceReferenceDataValidation).validateServiceRefData(this.goodService2);

        this.serviceValidationService.validateService(ASSET_1_ID.toString(), wrapService(this.goodService, this.goodService2));
    }

    /**
     * Update Service Passes Validation.
     */
    @Test
    public void updateServiceFailsServiceNotFound() throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.thrown.expect(RecordNotFoundException.class);

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(null);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(null);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    /**
     * Update Service Fails Validation. A Service that is Deleted cannot be updated.
     *
     * @throws ParseException
     */
    @Test
    public void updateServiceFailsForDeletedFlag() throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(null);

        this.thrown.expect(RecordNotFoundException.class);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(),
                this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingSucceedsNoType()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingSucceedsAnnual()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.ANNUAL.value()));

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingFailsAnnual()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "Service Type"));

        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
        this.serviceCatalogue2.setServiceType(new LinkResource(ServiceType.ANNUAL.value()));

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.goodService2);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_2_ID)).thenReturn(this.serviceCatalogue2);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingSucceedsRenewal()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.RENEWAL.value()));

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingSucceedsRenewalNullAssignedDate()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
        this.updateableService.setAssignedDateManual(null);

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateAgainstExistingFailsRenewal()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Assigned Date"));

        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
        this.serviceCatalogue2.setServiceType(new LinkResource(ServiceType.RENEWAL.value()));
        this.updateableService.setAssignedDateManual(null);
        this.goodService2.setDueDateManual(new Date());

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.goodService2);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_2_ID)).thenReturn(this.serviceCatalogue2);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateCyclePeriodictyChanged()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(String.format(String.format(ExceptionMessagesUtils.FIELD_IS_NOT_EDITABLE, "cycle periodicity", "services",
                "service catalogue id", this.serviceCatalogue1.getId().toString())));

        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.ANNUAL.value()));
        this.serviceCatalogue1.setCyclePeriodicityEditable(false);

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void updateServiceValidateCyclePeriodictySame()
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException
    {
        this.serviceCatalogue1.setServiceType(new LinkResource(ServiceType.ANNUAL.value()));
        this.serviceCatalogue1.setCyclePeriodicityEditable(false);
        this.updateableService.setCyclePeriodicity(this.serviceCatalogue1.getCyclePeriodicity());

        when(this.serviceService.getHarmonisationDate(ASSET_1_ID)).thenReturn(createDateDto(this.updateableService.getAssignedDateManual()));
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(this.updateableService);
        when(this.serviceDao.getService(SERVICE_1_ID, ASSET_1_ID)).thenReturn(this.updateableService);
        when(this.serviceReferenceDataDao.getServiceCatalogue(SERVICE_CATALOGUE_1_ID)).thenReturn(this.serviceCatalogue1);
        doNothing().when(this.dateValidator).validateAssignedDate(this.updateableService);
        doNothing().when(this.dateValidator).validateDateRanges(this.updateableService);

        this.serviceValidationService.validateServiceUpdate(SERVICE_1_ID.toString(), ASSET_1_ID.toString(), this.updateableService);
    }

    @Test
    public void validateProdListFailNullAssetId() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final String mockedExceptionString = "Mocked Exception";
        doThrow(new BadRequestException(mockedExceptionString)).when(this.validationService).verifyId(null);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(mockedExceptionString);

        this.serviceValidationService.validateProductList(null, new ProductListDto());
    }

    @Test
    public void validateProdListFailNullDto() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("Cannot create or update empty entities.");
        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), null);
    }

    @Test
    public void validateProdListFailAssetIdNotANumber() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {

        final String mockedExceptionString = "Mocked Exception";
        doThrow(new BadRequestException(mockedExceptionString)).when(this.validationService).verifyNumberFormat(VALIDATION_ERROR);

        final ProductListDto selectedProductList = new ProductListDto();
        selectedProductList.setProductList(new ArrayList<>());

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(mockedExceptionString);

        this.serviceValidationService.validateProductList(VALIDATION_ERROR, selectedProductList);
    }

    @Test
    public void validateProdListFailAssetDoesNotExist() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductList = new ProductListDto();

        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        when(this.assetDao.assetExists(ASSET_1_ID)).thenReturn(false);

        this.thrown.expect(RecordNotFoundException.class);
        this.thrown.expectMessage("Entity not found for identifier: 1.");

        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductList);
    }

    @Test
    public void validateProdListFailNoProductCataloguesForAsset() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductList = new ProductListDto();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        when(this.assetDao.assetExists(ASSET_1_ID)).thenReturn(true);
        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(null);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("No Product catalogues found for asset id 1.");

        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductList);
    }

    @Test
    public void validateProdListFailSelectedProductsHaveCataloguesNotInRefData()
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = new ProductListDto();
        final List<ProductDto> selectedProductList = new ArrayList<>();
        selectedProductList.add(createProductDto(PRODUCT_ID_5, PRODUCT_CATALOGUE_1_ID));
        selectedProductList.add(createProductDto(PRODUCT_ID_6, PRODUCT_CATALOGUE_2_ID));
        selectedProductList.add(createProductDto(PRODUCT_ID_7, PRODUCT_CATALOGUE_3_ID));
        selectedProductListDto.setProductList(selectedProductList);

        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        when(this.assetDao.assetExists(ASSET_1_ID)).thenReturn(true);

        final List<ProductCatalogueDto> allowedProductCatalogues = new ArrayList<>();
        allowedProductCatalogues.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        allowedProductCatalogues.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));

        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, null)).thenReturn(allowedProductCatalogues);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("List of Product catalogues has invalid members [1, 2, 3].");

        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    @Test
    public void validateProdListSkippedNoProductsForThisAsset() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = setUpTestForAssociatedServiceTests();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final ArrayList<ProductCatalogueDto> productcataloguesForAsset = new ArrayList<>();
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));

        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(null);
        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(productcataloguesForAsset);
        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    @Test
    public void validateProdListPassesWhenAnEmptyListReturnedFromGetProductsForAsset()
            throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = setUpTestForAssociatedServiceTests();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final ArrayList<ProductCatalogueDto> productcataloguesForAsset = new ArrayList<>();
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));

        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(new ArrayList<>());
        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(productcataloguesForAsset);
        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    @Test
    public void validateProdListFailsWithAssociatedServices() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = setUpTestForAssociatedServiceTests();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final ArrayList<ProductDto> productsForAsset = new ArrayList<>();
        productsForAsset.add(createProductDto(PRODUCT_ID_5, PRODUCT_CATALOGUE_2_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_6, PRODUCT_CATALOGUE_3_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_7, PRODUCT_CATALOGUE_4_ID));
        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(productsForAsset);

        final ArrayList<ProductCatalogueDto> productcataloguesForAsset = new ArrayList<>();
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));

        final List<ScheduledServiceDto> associatedServices = new ArrayList<>();
        associatedServices.add(new ScheduledServiceDto());

        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(productcataloguesForAsset);
        when(this.serviceDao.getServicesForProduct(PRODUCT_ID_7)).thenReturn(associatedServices);

        this.thrown.expect(MastBusinessException.class);
        this.thrown.expectMessage("One or more products still have attached services. Product ids [7]");
        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    @Test
    public void validateProdListPasses() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = setUpTestForAssociatedServiceTests();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final ArrayList<ProductDto> productsForAsset = new ArrayList<>();
        productsForAsset.add(createProductDto(PRODUCT_ID_5, PRODUCT_CATALOGUE_2_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_6, PRODUCT_CATALOGUE_3_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_7, PRODUCT_CATALOGUE_4_ID));
        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(productsForAsset);

        final ArrayList<ProductCatalogueDto> productcataloguesForAsset = new ArrayList<>();
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));
        when(this.serviceDao.getServicesForProduct(PRODUCT_ID_7)).thenReturn(new ArrayList<>());

        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(productcataloguesForAsset);

        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    @Test
    public void validateProdListPasses2() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        final ProductListDto selectedProductListDto = setUpTestForAssociatedServiceTests();

        final List<ProductType> productTypes = new ArrayList<ProductType>();
        productTypes.add(ProductType.STATUTORY);

        final ArrayList<ProductDto> productsForAsset = new ArrayList<>();
        productsForAsset.add(createProductDto(PRODUCT_ID_5, PRODUCT_CATALOGUE_2_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_6, PRODUCT_CATALOGUE_3_ID));
        productsForAsset.add(createProductDto(PRODUCT_ID_7, PRODUCT_CATALOGUE_4_ID));
        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(productsForAsset);

        final ArrayList<ProductCatalogueDto> productcataloguesForAsset = new ArrayList<>();
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        productcataloguesForAsset.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));
        when(this.serviceDao.getServicesForProduct(PRODUCT_ID_7)).thenReturn(null);

        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(productcataloguesForAsset);

        this.serviceValidationService.validateProductList(ASSET_1_ID.toString(), selectedProductListDto);
    }

    private ProductDto createProductDto(final Long productId, final Long productCatalogueId)
    {
        final ProductDto productDto = new ProductDto();
        productDto.setId(productId);
        productDto.setProductCatalogueId(productCatalogueId);
        return productDto;
    }

    private ProductCatalogueDto createProductCatalogueDto(final Long id)
    {
        final ProductCatalogueDto productCatalogueDto = new ProductCatalogueDto();
        productCatalogueDto.setId(id);
        return productCatalogueDto;
    }

    private ProductListDto setUpTestForAssociatedServiceTests() throws BadRequestException
    {
        final ProductListDto selectedProductListDto = new ProductListDto();
        final List<ProductDto> selectedProductList = new ArrayList<>();
        selectedProductList.add(createProductDto(PRODUCT_ID_5, PRODUCT_CATALOGUE_1_ID));
        selectedProductList.add(createProductDto(PRODUCT_ID_6, PRODUCT_CATALOGUE_2_ID));
        selectedProductListDto.setProductList(selectedProductList);

        when(this.validationService.verifyNumberFormat(ASSET_1_ID.toString())).thenReturn(ASSET_1_ID);
        when(this.assetDao.assetExists(ASSET_1_ID)).thenReturn(true);

        final List<ProductType> productTypes = new ArrayList<ProductType>();

        final List<ProductCatalogueDto> allowedProductCatalogues = new ArrayList<>();
        allowedProductCatalogues.add(createProductCatalogueDto(PRODUCT_CATALOGUE_1_ID));
        allowedProductCatalogues.add(createProductCatalogueDto(PRODUCT_CATALOGUE_2_ID));
        allowedProductCatalogues.add(createProductCatalogueDto(PRODUCT_CATALOGUE_3_ID));
        when(this.serviceReferenceDataDao.getProductCataloguesForAsset(ASSET_1_ID, productTypes)).thenReturn(allowedProductCatalogues);
        return selectedProductListDto;
    }

    @Test
    public void verifyHarmonisationDateWithNoDate() throws BadRequestException, MastBusinessException
    {
        final DateDto harmonisationDateDto = new DateDto();

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("No Harmonisation Date passed.");

        this.serviceValidationService.verifyHarmonisationDate(null, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateWithNoAssetBuildDate() throws BadRequestException, MastBusinessException
    {
        final DateDto harmonisationDateDto = createDateDto(new Date());

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("No Build Date on the Asset with name [Asset One Name] and ID : [1]");

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateSameAsBuildDate() throws BadRequestException, MastBusinessException
    {

        final Date commonDate = new Date();
        final String commonDateString = DATE_FORMAT.format(commonDate);

        final DateDto harmonisationDateDto = createDateDto(commonDate);

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);
        asset.setBuildDate(commonDate);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage(
                "The passed Harmonisation Date [" + commonDateString + "] must be after the asset build date of [" + commonDateString + "].");

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateOneDayBeforeBuildDate() throws BadRequestException, MastBusinessException
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        final Date yesterday = cal.getTime();

        cal.add(Calendar.DATE, -1);
        final Date dayBeforeYesterday = cal.getTime();

        final String yesterdayDateString = DATE_FORMAT.format(yesterday);
        final String dayBeforeYesterdayDateString = DATE_FORMAT.format(dayBeforeYesterday);

        final DateDto harmonisationDateDto = createDateDto(dayBeforeYesterday);

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);
        asset.setBuildDate(yesterday);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("The passed Harmonisation Date [" + dayBeforeYesterdayDateString + "] must be after the asset build date of ["
                + yesterdayDateString + "].");

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateSameDayAsThreeMonthsFromNow() throws BadRequestException, MastBusinessException, ParseException
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        final Date yesterday = cal.getTime();

        cal.add(Calendar.DATE, 1);
        cal.add(Calendar.MONTH, MONTHS_FROM_NOW_FOR_HARMONISATION_DATE_CHECK);
        final Date threeMonthsFromNow = cal.getTime();

        final String threeMonthsFromNowDateString = DATE_FORMAT.format(threeMonthsFromNow);

        final DateDto harmonisationDateDto = createDateDto(threeMonthsFromNow);

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);
        asset.setBuildDate(yesterday);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);
        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(threeMonthsFromNowDateString));

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("The passed Harmonisation Date [" + threeMonthsFromNowDateString + "] must be before ["
                + threeMonthsFromNowDateString + "].");

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateLessThreeMonthsFromNow() throws BadRequestException, MastBusinessException, ParseException
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        final Date yesterday = cal.getTime();

        cal.add(Calendar.DATE, 1);
        cal.add(Calendar.MONTH, MONTHS_FROM_NOW_FOR_HARMONISATION_DATE_CHECK);
        final Date threeMonthsFromNow = cal.getTime();

        final String threeMonthsFromNowDateString = DATE_FORMAT.format(threeMonthsFromNow);

        final DateDto harmonisationDateDto = createDateDto(new Date());

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);
        asset.setBuildDate(yesterday);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);
        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(threeMonthsFromNowDateString));

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void verifyHarmonisationDateSameDayAsThreeMonthsAndOneDayFromNow() throws BadRequestException, MastBusinessException, ParseException
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        final Date yesterday = cal.getTime();

        cal.add(Calendar.DATE, 1);
        cal.add(Calendar.MONTH, MONTHS_FROM_NOW_FOR_HARMONISATION_DATE_CHECK);
        final Date threeMonthsFromNow = cal.getTime();

        cal.add(Calendar.DATE, 1);
        final Date threeMonthsAndOneDayFromNow = cal.getTime();

        final Calendar cal2 = Calendar.getInstance();
        cal2.setTime(threeMonthsFromNow);

        final String threeMonthsFromNowDateString = DATE_FORMAT.format(threeMonthsFromNow);
        final String threeMonthsAndOneDayFromNowDateString = DATE_FORMAT.format(threeMonthsAndOneDayFromNow);

        final DateDto harmonisationDateDto = createDateDto(threeMonthsAndOneDayFromNow);

        final AssetLightDto asset = new AssetLightDto();
        asset.setId(ASSET_1_ID);
        asset.setName(ASSET_1_NAME);
        asset.setBuildDate(yesterday);

        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(asset);
        when(this.dateHelper.adjustDate(any(Date.class), eq(THREE_MONTHS), eq(Calendar.MONTH)))
                .thenReturn(DATE_FORMAT.parse(threeMonthsFromNowDateString));

        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("The passed Harmonisation Date [" + threeMonthsAndOneDayFromNowDateString + "] must be before ["
                + threeMonthsFromNowDateString + "].");

        this.serviceValidationService.verifyHarmonisationDate(ASSET_1_ID, harmonisationDateDto);
    }

    @Test
    public void validateDeleteServicesTestPasses() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());

        doNothing().when(this.validationService).verifyId(and(gt(ZERO), lt(MAX_ID)));
        doThrow(new BadRequestException("Error")).when(this.validationService).verifyId(or(lt(ONE), gt(MAX_ID)));

        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), or(eq(SERVICE_1_ID), eq(SERVICE_2_ID)))).thenReturn(true);
        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), not(or(eq(SERVICE_1_ID), eq(SERVICE_2_ID))))).thenReturn(false);

        this.serviceValidationService.validateDeleteServices(ASSET_1_ID.toString(), createIdList(SERVICE_1_ID, SERVICE_2_ID));
    }

    @Test
    public void validateDeleteServicesTestFailsInvalidId() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.thrown.expect(BadRequestException.class);
        this.thrown.expectMessage("Error");

        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());

        doNothing().when(this.validationService).verifyId(and(gt(ZERO), lt(MAX_ID)));
        doThrow(new BadRequestException("Error")).when(this.validationService).verifyId(or(lt(ONE), gt(MAX_ID)));

        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), or(eq(SERVICE_1_ID), eq(SERVICE_2_ID)))).thenReturn(true);
        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), not(or(eq(SERVICE_1_ID), eq(SERVICE_2_ID))))).thenReturn(false);

        this.serviceValidationService.validateDeleteServices(ASSET_1_ID.toString(), createIdList(SERVICE_1_ID, ZERO));
    }

    @Test
    public void validateDeleteServicesTestFailsServiceNotFound() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.thrown.expect(RecordNotFoundException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.ID_LIST_NOT_FOUND, "scheduled services", "[2]"));

        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());

        doNothing().when(this.validationService).verifyId(and(gt(ZERO), lt(MAX_ID)));
        doThrow(new BadRequestException("Error")).when(this.validationService).verifyId(or(lt(ONE), gt(MAX_ID)));

        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), eq(SERVICE_1_ID))).thenReturn(true);
        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), not(eq(SERVICE_1_ID)))).thenReturn(false);

        this.serviceValidationService.validateDeleteServices(ASSET_1_ID.toString(), createIdList(SERVICE_1_ID, SERVICE_2_ID));
    }

    @Test
    public void validateDeleteServicesTestFailsNeitherServiceFound() throws RecordNotFoundException, BadRequestException, MastBusinessException
    {
        this.thrown.expect(RecordNotFoundException.class);
        this.thrown.expectMessage(String.format(ExceptionMessagesUtils.ID_LIST_NOT_FOUND, "scheduled services", "[1, 2]"));

        doNothing().when(this.assetValidationService).verifyAssetIdExists(ASSET_1_ID.toString());

        doNothing().when(this.validationService).verifyId(and(gt(ZERO), lt(MAX_ID)));
        doThrow(new BadRequestException("Error")).when(this.validationService).verifyId(or(lt(ONE), gt(MAX_ID)));

        when(this.serviceDao.serviceExistsForAsset(eq(ASSET_1_ID), any(Long.class))).thenReturn(false);

        this.serviceValidationService.validateDeleteServices(ASSET_1_ID.toString(), createIdList(SERVICE_1_ID, SERVICE_2_ID));
    }

    private ScheduledServiceListDto wrapService(final ScheduledServiceDto... services)
    {
        final ScheduledServiceListDto serviceList = new ScheduledServiceListDto();
        serviceList.setScheduledServices(new ArrayList<ScheduledServiceDto>());

        for (final ScheduledServiceDto service : services)
        {
            serviceList.getScheduledServices().add(service);
        }

        return serviceList;
    }

    private List<Long> createIdList(final Long... ids)
    {
        final List<Long> idList = Arrays.asList(ids);
        return idList;
    }
}
