package com.baesystems.ai.lr.validation.service;

import com.baesystems.ai.lr.dao.jobs.FollowUpActionDao;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FollowUpActionValidationServiceTest
{
    private static ObjectMapper objectMapper;
    private static final Long JOB_1_ID = 1L;
    private static final Long VALID_FUA_1_ID = 1L;
    private static final Long INVALID_FUA_2_ID = 2L;

    @InjectMocks
    private final FollowUpActionValidationServiceImpl followUpActionValidationService = new FollowUpActionValidationServiceImpl();

    @Mock
    private FollowUpActionDao followUpActionDao;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testValidateFollowUpAction() throws RecordNotFoundException
    {
        when(followUpActionDao.followUpActionExistsForJob(JOB_1_ID, VALID_FUA_1_ID)).thenReturn(true);
        followUpActionValidationService.verifyJobHasFollowUpAction(JOB_1_ID, VALID_FUA_1_ID);
    }

    @Test
    public void testValidateFollowUpActionFails() throws RecordNotFoundException
    {
        when(followUpActionDao.followUpActionExistsForJob(JOB_1_ID, INVALID_FUA_2_ID)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        followUpActionValidationService.verifyJobHasFollowUpAction(JOB_1_ID, INVALID_FUA_2_ID);
    }
}
