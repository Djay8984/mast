package com.baesystems.ai.lr.validation.service;

import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.enums.JobStatusType;
import com.baesystems.ai.lr.enums.OfficeRoleType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.SurveyValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JobValidationServiceTest
{
    private static final Long JOB_ID = 1L;
    private static final Long BAD_JOB_ID = -1L;
    private static final Long VALID_EMPLOYEE = 1L;
    private static final Long VALID_TEAM = 1L;

    private JobDto jobDto;
    private OfficeLinkDto sdoOffice;
    private OfficeLinkDto nonSdoOffice;
    private final List<OfficeLinkDto> officesWithoutSdo = new ArrayList<OfficeLinkDto>();
    private final List<OfficeLinkDto> officesWithSdo = new ArrayList<OfficeLinkDto>();
    private final ReferenceDataDto jobStatus = new ReferenceDataDto();

    @Mock
    private ValidationService validationService;

    @Mock
    private JobDao jobDao;

    @Mock
    private SurveyValidationService surveyValidationService;

    @InjectMocks
    private final JobValidationServiceImpl jobValidationService = new JobValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp()
    {
        jobStatus.setId(JobStatusType.SDO_ASSIGNED.getValue());
        jobStatus.setName(JobStatusType.SDO_ASSIGNED.name());

        sdoOffice = new OfficeLinkDto();
        sdoOffice.setOfficeRole(new LinkResource(OfficeRoleType.SDO.getValue()));

        nonSdoOffice = new OfficeLinkDto();
        nonSdoOffice.setOfficeRole(new LinkResource(OfficeRoleType.CFO.getValue()));

        officesWithoutSdo.add(nonSdoOffice);
        officesWithSdo.add(nonSdoOffice);
        officesWithSdo.add(sdoOffice);

        jobDto = new JobDto();
        jobDto.setId(JOB_ID);
        jobDto.setOffices(officesWithSdo);
        jobDto.setJobStatus(new LinkResource(JobStatusType.UNDER_SURVEY.getValue()));
    }

    @Test
    public void updateJobFailsNullId() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        when(this.validationService.verifyNumberFormat(JOB_ID.toString())).thenReturn(JOB_ID);
        when(this.jobDao.jobExists(JOB_ID)).thenReturn(Boolean.TRUE);
        doThrow(new BadRequestException()).when(this.validationService).verifyId(null);

        thrown.expect(BadRequestException.class);

        this.jobValidationService.validateJobUpdate(null, this.jobDto);
    }

    @Test
    public void updateJobFailsInvalidId() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        when(this.validationService.verifyNumberFormat(JOB_ID.toString())).thenReturn(JOB_ID);
        when(this.jobDao.jobExists(JOB_ID)).thenReturn(Boolean.TRUE);
        doThrow(new BadRequestException()).when(this.validationService).verifyId(BAD_JOB_ID);

        thrown.expect(BadRequestException.class);

        this.jobValidationService.validateJobUpdate(BAD_JOB_ID, this.jobDto);
    }

    @Test
    public void updateJobFailsValidIdNullBody() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        when(this.validationService.verifyNumberFormat(JOB_ID.toString())).thenReturn(JOB_ID);
        when(this.jobDao.jobExists(JOB_ID)).thenReturn(Boolean.TRUE);
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.jobValidationService.validateJobUpdate(JOB_ID, null);
    }

    @Test
    public void updateJobSucceedsValidIdValidBody() throws BadRequestException, MastBusinessException, RecordNotFoundException
    {
        when(this.validationService.verifyNumberFormat(JOB_ID.toString())).thenReturn(JOB_ID);
        when(this.jobDao.getJobStatusForJob(JOB_ID)).thenReturn(jobStatus);
        when(this.jobDao.jobExists(JOB_ID)).thenReturn(Boolean.TRUE);
        doNothing().when(this.surveyValidationService).validateCompletion(any(SurveyDto.class));

        this.jobValidationService.validateJobUpdate(JOB_ID, this.jobDto);
    }

    @Test
    public void createJobFailsNullBody() throws BadRequestException, MastBusinessException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.jobValidationService.validateJobCreate(null);
    }

    @Test
    public void createJobFailsIdNotNull() throws BadRequestException, MastBusinessException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(JOB_ID);

        thrown.expect(BadRequestException.class);

        this.jobValidationService.validateJobCreate(this.jobDto);
    }

    @Test
    public void createJobSucceeds() throws BadRequestException, MastBusinessException
    {
        this.jobDto.setId(null);
        this.jobValidationService.validateJobCreate(this.jobDto);
    }

    @Test
    public void verifySDOPresentNullOffices() throws MastBusinessException
    {
        thrown.expect(MastBusinessException.class);
        this.jobValidationService.verifyServiceDeliveryOfficePresent(null);
    }

    @Test
    public void verifySDOPresentEmptyOffices() throws MastBusinessException
    {
        thrown.expect(MastBusinessException.class);
        this.jobValidationService.verifyServiceDeliveryOfficePresent(new ArrayList<OfficeLinkDto>());
    }

    @Test
    public void verifySDOPresentOfficesNoSDO() throws MastBusinessException
    {
        thrown.expect(MastBusinessException.class);
        this.jobValidationService.verifyServiceDeliveryOfficePresent(officesWithoutSdo);
    }

    @Test
    public void verifySDOPresentOfficesWithSDO() throws MastBusinessException
    {
        this.jobValidationService.verifyServiceDeliveryOfficePresent(officesWithSdo);
    }

    @Test
    public void verifyEmployeeAndTeamIdsCannotBeSpecifiedTogether() throws BadRequestException
    {
        thrown.expect(BadRequestException.class);
        this.jobValidationService.validateEmployeeAndTeamId(VALID_EMPLOYEE, VALID_TEAM);
    }

    @Test
    public void verifyValidEmployeeWithNullTeamID() throws BadRequestException
    {
        this.jobValidationService.validateEmployeeAndTeamId(VALID_EMPLOYEE, null);
    }

    @Test
    public void verifyValidTeamIDWithNullemployeeID() throws BadRequestException
    {
        this.jobValidationService.validateEmployeeAndTeamId(null, VALID_TEAM);
    }
}
