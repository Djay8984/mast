package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.doThrow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.service.validation.ValidationService;

@RunWith(MockitoJUnitRunner.class)
public class QueryValidationServiceTest
{
    private static final Object ID = 1;
    private static final Object INVALID_ID = "@";
    private static final String ALLOWED_PARAMS = "page&size&sort&order&search&type";
    private static final String VALID_SEARCH = "page=1&size=1";
    private static final String INVALID_SEARCH = "page=1&size=1&invalid=1";

    @Mock
    private ValidationService validationService;

    @InjectMocks
    private final QueryValidationServiceImpl queryValidationService = new QueryValidationServiceImpl();

    @Test
    public void verifyValidQueryIdsTest() throws BadRequestException
    {
        final Object[] inputIds = new Object[2];
        inputIds[0] = ID;
        inputIds[1] = ID;
        this.queryValidationService.verifyQueryId(inputIds);
    }

    @Test(expected = BadRequestException.class)
    public void verifyInvalidQueryIdsTest() throws BadRequestException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyId(INVALID_ID);

        final Object[] inputIds = new Object[1];
        inputIds[0] = INVALID_ID;
        this.queryValidationService.verifyQueryId(inputIds);
    }

    @Test
    public void verifyValidQueryTest() throws BadRequestException
    {
        this.queryValidationService.verifyQuery(VALID_SEARCH, ALLOWED_PARAMS);
    }

    @Test(expected = BadRequestException.class)
    public void verifyInvalidQueryTest() throws BadRequestException
    {
        this.queryValidationService.verifyQuery(INVALID_SEARCH, ALLOWED_PARAMS);
    }
}
