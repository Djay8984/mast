package com.baesystems.ai.lr.validation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.validation.utils.ValidationStringUtils;
import com.baesystems.ai.lr.validation.utils.ValidationUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class AttributeValidatorTest
{
    @Spy
    private ValidationUtils validationUtils;

    @Spy
    private ValidationStringUtils validationStringUtils;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @InjectMocks
    private final AttributeValidator attributeValidator = new AttributeValidator();

    private static final Logger LOGGER = LoggerFactory.getLogger(AttributeValidatorTest.class);

    private static final Long ATTRIBUTE_TYPE_ID = 93L;
    private static final String ATTRIBUTE_VALUE_TYPE = "ALPHANUMERIC";

    private final AttributeTypeDto valueType = new AttributeTypeDto();

    @Before
    public void startUp()
    {
        valueType.setDescription(ATTRIBUTE_VALUE_TYPE);
    }

    @Test
    public void testValidValue() throws JsonParseException, JsonMappingException, IOException, ParseException
    {
        final ObjectMapper objectMapper = new ObjectMapper();

        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"),
                AttributeDto.class);

        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(valueType);

        final long start = System.currentTimeMillis();
        final List<String> results = attributeValidator.validate(attributeDto);
        final long end = System.currentTimeMillis();

        LOGGER.info("validateModel: time spent [" + (end - start) + "ms] on validation");

        for (final String result : results)
        {
            LOGGER.info("validateModel: (error) -> " + result);
        }

        final int expectedErrorCount = 0;
        assertEquals("check for 0 errors", expectedErrorCount, results.size());
        LOGGER.info("validateModel(-)");
    }

    @Test
    public void testInvalidValue() throws JsonParseException, JsonMappingException, IOException, ParseException
    {
        final ObjectMapper objectMapper = new ObjectMapper();

        final AttributeDto attributeDto = objectMapper.readValue(
                this.getClass().getResourceAsStream("/attributeValidationTest/AttributeDto.json"),
                AttributeDto.class);
        attributeDto.setValue("?");

        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(valueType);

        final long start = System.currentTimeMillis();
        final List<String> results = attributeValidator.validate(attributeDto);
        final long end = System.currentTimeMillis();

        LOGGER.info("validateModel: time spent [" + (end - start) + "ms] on validation");

        for (final String result : results)
        {
            LOGGER.info("validateModel: (error) -> " + result);
        }

        final int expectedErrorCount = 1;
        assertEquals("check for 1 errors", expectedErrorCount, results.size());
        LOGGER.info("validateModel(-)");
    }
}
