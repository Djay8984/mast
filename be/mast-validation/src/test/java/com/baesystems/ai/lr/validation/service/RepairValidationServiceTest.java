package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@RunWith(MockitoJUnitRunner.class)
public class RepairValidationServiceTest
{

    @Mock
    private ValidationService validationService;

    @Mock
    private RepairDao repairDao;

    @Mock
    private WIPRepairDao wipRepairDao;

    @InjectMocks
    private RepairValidationServiceImpl repairValidationService;

    private static final Long VALID_REPAIR_ID_1 = 1L;
    private static final Long VALID_DEFECT_ID_2 = 2L;
    private static final Long VALID_COC_ID_2 = 2L;
    private static final Long VALID_COC_ID_3 = 3L;
    private static final Long INVALID_DEFECT_ID_2222 = 2222L;

    /*
     * We don't expect any issue when we pass in a valid repair ID and a corresponding valid defect ID
     */
    @Test
    public void coCHasRepairValidationSuccess() throws BadRequestException, RecordNotFoundException
    {
        when(this.repairDao.repairExistsForCoC(VALID_REPAIR_ID_1, VALID_COC_ID_2)).thenReturn(Boolean.TRUE);
        try
        {
            this.repairValidationService.validateCoCHasRepair(VALID_COC_ID_2.toString(), VALID_REPAIR_ID_1.toString());
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected RecordNotFoundException");
        }
    }

    /*
     * We don't expect any issue when we pass in a valid repair ID and a corresponding valid defect ID
     */
    @Test
    public void defectHasRepairValidationSuccess() throws BadRequestException, RecordNotFoundException
    {
        when(this.repairDao.repairExistsForDefect(VALID_REPAIR_ID_1, VALID_DEFECT_ID_2)).thenReturn(Boolean.TRUE);
        try
        {
            this.repairValidationService.validateDefectHasRepair(VALID_DEFECT_ID_2.toString(), VALID_REPAIR_ID_1.toString());
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected RecordNotFoundException");
        }
    }

    /*
     * When the defect id and repair id both exist, but the repair ID doesn't belong to the defect ID given, then we
     * expect a record not found exception.
     */
    @Test
    public void defectHasRepairValidationFailureRepairHasIncorrectDefect() throws BadRequestException, RecordNotFoundException
    {
        when(this.repairDao.repairExistsForDefect(VALID_REPAIR_ID_1, INVALID_DEFECT_ID_2222)).thenReturn(Boolean.FALSE);
        try
        {
            this.repairValidationService.validateDefectHasRepair(INVALID_DEFECT_ID_2222.toString(), VALID_REPAIR_ID_1.toString());
            Assert.fail("Expected RecordNotFoundException");
        }
        catch (final RecordNotFoundException e)
        {
            Assert.assertEquals(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Repair", VALID_REPAIR_ID_1, "Defect",
                    INVALID_DEFECT_ID_2222),
                    e.getMessage());
        }
    }

    @Test
    public void validateWIPDefectHasWIPRepairSuccess() throws BadRequestException, RecordNotFoundException
    {
        when(this.wipRepairDao.repairExistsForDefect(VALID_REPAIR_ID_1, VALID_DEFECT_ID_2)).thenReturn(Boolean.TRUE);
        try
        {
            this.repairValidationService.validateWIPDefectHasWIPRepair(VALID_DEFECT_ID_2.toString(), VALID_REPAIR_ID_1.toString());
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected RecordNotFoundException");
        }
    }

    @Test
    public void validateWIPDCoCHasWIPRepairSuccess() throws BadRequestException, RecordNotFoundException
    {
        when(this.wipRepairDao.repairExistsForCoC(VALID_REPAIR_ID_1, VALID_COC_ID_3)).thenReturn(Boolean.TRUE);
        try
        {
            this.repairValidationService.validateWIPCoCHasWIPRepair(VALID_COC_ID_3.toString(), VALID_REPAIR_ID_1.toString());
        }
        catch (final RecordNotFoundException e)
        {
            Assert.fail("Unexpected RecordNotFoundException");
        }
    }

}
