package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.ServiceReferenceDataService;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class AssetNoteValidationServiceTest
{
    private static ObjectMapper objectMapper;
    private static final Long INCORRECT_ID = 99L;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    @Mock
    private ValidationDataService validationDataService;

    @Mock
    private ValidationService validationService;

    @Mock
    private AssetValidationService assetValidationService;

    @Mock
    private AssetNoteDao assetNoteDao;

    @Mock
    private WIPAssetNoteDao wipAssetNoteDao;

    @InjectMocks
    private final AssetNoteValidationServiceImpl assetNoteValidationService = new AssetNoteValidationServiceImpl();

    @Mock
    private ServiceReferenceDataService serviceReferenceDataService;

    @Mock
    private DateHelper dateHelper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testValidateAsset() throws IOException, BadRequestException, RecordNotFoundException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long inputAssetId = assetNoteDto.getAsset().getId();

        when(validationService.verifyNumberFormat(Long.toString(inputAssetId))).thenReturn(inputAssetId);
        when(validationService.verifyNumberFormat(Long.toString(assetNoteId))).thenReturn(assetNoteId);
        when(assetNoteDao.getAssetNote(assetNoteId)).thenReturn(assetNoteDto);

        assetNoteValidationService.validateAsset(Long.toString(assetNoteId), Long.toString(inputAssetId), Long.toString(1L));
    }

    /*
     * This test represents PUTting to the asset endpoint of asset A, but the payload referencing asset B
     */
    @Test
    public void testValidateAssetFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long inputAssetId = assetNoteDto.getAsset().getId();

        when(validationService.verifyNumberFormat(Long.toString(inputAssetId))).thenReturn(inputAssetId);
        when(validationService.verifyNumberFormat(Long.toString(assetNoteId))).thenReturn(assetNoteId);

        doThrow(new BadRequestException()).when(validationService)
                .verifyField(Long.toString(inputAssetId), Long.toString(INCORRECT_ID),
                        String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Asset", inputAssetId, INCORRECT_ID));

        thrown.expect(BadRequestException.class);
        assetNoteValidationService.validateAsset(Long.toString(assetNoteId), Long.toString(inputAssetId), Long.toString(INCORRECT_ID));
    }

    @Test
    public void testValidateJob() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long jobId = assetNoteDto.getJob().getId();

        when(validationService.verifyNumberFormat(Long.toString(jobId))).thenReturn(jobId);
        when(validationService.verifyNumberFormat(Long.toString(assetNoteId))).thenReturn(assetNoteId);
        when(wipAssetNoteDao.getAssetNote(assetNoteId)).thenReturn(assetNoteDto);

        assetNoteValidationService.validateJob(Long.toString(assetNoteId), Long.toString(jobId), Long.toString(jobId));
    }

    /*
     * This test represents PUTting to the wip-asset-note endpoint of job A, but the payload referencing job B
     */
    @Test
    public void testValidateJobFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long inputJobId = assetNoteDto.getJob().getId();

        when(validationService.verifyNumberFormat(Long.toString(inputJobId))).thenReturn(inputJobId);
        when(validationService.verifyNumberFormat(Long.toString(assetNoteId))).thenReturn(assetNoteId);
        doThrow(new BadRequestException()).when(validationService)
                .verifyField(Long.toString(inputJobId), Long.toString(INCORRECT_ID),
                        String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Job", inputJobId, INCORRECT_ID));

        thrown.expect(BadRequestException.class);
        assetNoteValidationService.validateJob(Long.toString(assetNoteId), Long.toString(inputJobId), Long.toString(INCORRECT_ID));
    }

    @Test
    public void testVerifyAssetHasAssetNote() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long assetId = assetNoteDto.getAsset().getId();

        when(assetNoteDao.assetNoteExistsForAsset(assetId, assetNoteId)).thenReturn(true);
        assetNoteValidationService.verifyAssetHasAssetNote(assetId, assetNoteId);
    }

    @Test
    public void testVerifyAssetHasAssetNoteFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long assetId = assetNoteDto.getAsset().getId();

        when(assetNoteDao.assetNoteExistsForAsset(assetId, assetNoteId)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        assetNoteValidationService.verifyAssetHasAssetNote(assetId, assetNoteId);
    }

    @Test
    public void testValidateAssetAndItem() throws BadRequestException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDtoWithItem();
        final Long assetId = assetNoteDto.getAsset().getId();
        final Long itemId = assetNoteDto.getAssetItem().getId();

        assetNoteValidationService.validateAssetAndItem(Long.toString(assetId), Long.toString(itemId), assetNoteDto);
    }

    @Test
    public void testValidateAssetAndItemFailsForNullAsset() throws BadRequestException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDtoWithItem();
        final Long assetId = assetNoteDto.getAsset().getId();
        final Long itemId = assetNoteDto.getAssetItem().getId();

        assetNoteDto.setAsset(null);

        thrown.expect(BadRequestException.class);

        assetNoteValidationService.validateAssetAndItem(Long.toString(assetId), Long.toString(itemId), assetNoteDto);
    }

    @Test
    public void testValidateAssetAndItemFailsForNullItem() throws BadRequestException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDtoWithItem();
        final Long assetId = assetNoteDto.getAsset().getId();
        final Long itemId = assetNoteDto.getAssetItem().getId();

        assetNoteDto.setAssetItem(null);

        thrown.expect(BadRequestException.class);

        assetNoteValidationService.validateAssetAndItem(Long.toString(assetId), Long.toString(itemId), assetNoteDto);
    }

    @Test
    public void testValidateAssetAndItemFailsForAssetMismatch() throws BadRequestException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDtoWithItem();
        final Long assetId = assetNoteDto.getAsset().getId();
        final Long itemId = assetNoteDto.getAssetItem().getId();

        doThrow(new BadRequestException()).when(this.validationService)
                .verifyField(Long.toString(assetId), Long.toString(INCORRECT_ID),
                        String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Asset", Long.toString(assetId), INCORRECT_ID));

        thrown.expect(BadRequestException.class);

        assetNoteValidationService.validateAssetAndItem(Long.toString(INCORRECT_ID), Long.toString(itemId), assetNoteDto);
    }

    @Test
    public void testValidateAssetAndItemFailsForItemMismatch() throws BadRequestException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDtoWithItem();
        final Long assetId = assetNoteDto.getAsset().getId();
        final Long itemId = assetNoteDto.getAssetItem().getId();

        doThrow(new BadRequestException()).when(this.validationService)
                .verifyField(Long.toString(itemId), Long.toString(INCORRECT_ID),
                        String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "Item", Long.toString(itemId), INCORRECT_ID));

        thrown.expect(BadRequestException.class);

        assetNoteValidationService.validateAssetAndItem(Long.toString(assetId), Long.toString(INCORRECT_ID), assetNoteDto);
    }

    @Test
    public void testVerifyJobHasWIPAssetNote() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long jobId = assetNoteDto.getJob().getId();

        when(wipAssetNoteDao.assetNoteExistsForJob(jobId, assetNoteId)).thenReturn(true);
        assetNoteValidationService.verifyJobHasWIPAssetNote(jobId, assetNoteId);
    }

    @Test
    public void testVerifyJobHasWIPAssetNoteFails() throws BadRequestException, RecordNotFoundException, IOException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        final Long assetNoteId = assetNoteDto.getId();
        final Long jobId = assetNoteDto.getJob().getId();

        when(wipAssetNoteDao.assetNoteExistsForJob(jobId, assetNoteId)).thenReturn(false);
        thrown.expect(RecordNotFoundException.class);

        assetNoteValidationService.verifyJobHasWIPAssetNote(jobId, assetNoteId);
    }

    @Test
    public void verifyParentPassesWhenPassedNull()
    {
        try
        {
            assetNoteValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(null);
        }
        catch (RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentPassesWhenPassedAValidParentCodicil() throws RecordNotFoundException
    {
        final Long parentCodicilId = 20L;

        when(assetNoteDao.getAssetNote(parentCodicilId)).thenReturn(new AssetNoteDto());

        try
        {
            assetNoteValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
        }
        catch (RecordNotFoundException e)
        {
            Assert.fail("Unexpected exception.");
        }
    }

    @Test
    public void verifyParentFailsWhenPassedAnInvalidParentCodicil() throws RecordNotFoundException
    {
        final Long parentCodicilId = 20L;

        when(assetNoteDao.getAssetNote(parentCodicilId)).thenReturn(null);

        try
        {
            assetNoteValidationService.verifyOptionalParentIdExistsAndIsOfCorrectType(parentCodicilId);
            Assert.fail("Expected an exception to be thrown.");
        }
        catch (RecordNotFoundException e)
        {
            // Do nothing. This is expected
        }
    }

    @Test
    public void testVerifyImposedDateNotHistorical() throws ParseException
    {
        final AssetNoteDto assetNoteDto = new AssetNoteDto();
        final String historical = "2000-10-01";

        assetNoteDto.setImposedDate(dateFormat.parse(historical));
        when(dateHelper.isTodayOrAfter(assetNoteDto.getImposedDate())).thenReturn(false);

        try
        {
            assetNoteValidationService.verifyImposedDateNotHistorical(assetNoteDto);
            Assert.fail("Expected non-historical imposed date.");
        }
        catch (BadRequestException e)
        {
            Assert.assertEquals(String.format(ExceptionMessagesUtils.HISTORICAL_IMPOSED_DATE, historical), e.getMessage());
            Assert.assertEquals(HttpStatus.BAD_REQUEST.value(), e.getErrorCode());
        }
    }

    @Test
    public void testValidateNonNullJobScopeConfirmed() throws IOException, BadRequestException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        assetNoteDto.setJobScopeConfirmed(Boolean.TRUE);
        this.assetNoteValidationService.validateWIPJobScopeConfirmed(assetNoteDto);

    }

    @Test
    public void testValidateNullJobScopeConfirmed() throws IOException, BadRequestException
    {
        final AssetNoteDto assetNoteDto = getTestDto();
        assetNoteDto.setJobScopeConfirmed(null);
        thrown.expect(BadRequestException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"));
        this.assetNoteValidationService.validateWIPJobScopeConfirmed(assetNoteDto);
    }

    private AssetNoteDto getTestDto() throws IOException
    {
        return objectMapper.readValue(
                this.getClass().getResourceAsStream("/assetNoteValidationServiceTest/AssetNoteDto.json"), AssetNoteDto.class);
    }

    private AssetNoteDto getTestDtoWithItem() throws IOException
    {
        return objectMapper.readValue(
                this.getClass().getResourceAsStream("/assetNoteValidationServiceTest/AssetNoteDtoWithItem.json"), AssetNoteDto.class);
    }
}
