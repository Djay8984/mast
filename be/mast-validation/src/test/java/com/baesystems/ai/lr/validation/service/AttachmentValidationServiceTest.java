package com.baesystems.ai.lr.validation.service;

import static org.mockito.Mockito.when;

import com.baesystems.ai.lr.exception.MastBusinessException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.ServiceReferenceDataService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.service.validation.ValidationService;

@RunWith(MockitoJUnitRunner.class)
public class AttachmentValidationServiceTest
{
    @Mock
    private ValidationDataService validationDataService;

    @Mock
    private ValidationService validationService;

    @InjectMocks
    private final AttachmentValidationServiceImpl attachmentValidationService = new AttachmentValidationServiceImpl();

    @Mock
    private ServiceReferenceDataService serviceReferenceDataService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testVerifyObjectHasAttachment() throws RecordNotFoundException, MastBusinessException
    {
        final Long genericId = 1L;
        when(this.validationDataService.findAttachmentByObject(AttachmentTargetType.CASE, genericId, genericId))
                .thenReturn(genericId);

        this.attachmentValidationService.verifyHasAttachment(genericId, "CASE", genericId);
    }

    @Test
    public void testVerifyObjectHasAttachmentFails() throws RecordNotFoundException, MastBusinessException
    {
        final Long genericId = 1L;
        when(this.validationDataService.findAttachmentByObject(AttachmentTargetType.CASE, genericId, genericId))
                .thenReturn(null);
        thrown.expect(RecordNotFoundException.class);

        this.attachmentValidationService.verifyHasAttachment(genericId, "CASE", genericId);
    }
}
