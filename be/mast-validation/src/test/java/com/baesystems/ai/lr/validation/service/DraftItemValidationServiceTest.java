package com.baesystems.ai.lr.validation.service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.codicils.CodicilDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.DraftItemRelationshipDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.AssetValidationService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DraftItemValidationServiceTest
{
    private static final DraftItemListDto INPUT = new DraftItemListDto();
    private static final DraftItemDto ITEM1 = new DraftItemDto();
    private static final DraftItemDto ITEM2 = new DraftItemDto();
    private static final ItemRelationshipDto RELATIONSHIP1 = new ItemRelationshipDto();
    private static final DraftItemRelationshipDto DRAFT_RELATIONSHIP1 = new DraftItemRelationshipDto();
    private static final LazyItemDto ROOT_ITEM = new LazyItemDto();
    private static final LazyItemDto LAZY_ITEM_DTO_1 = new LazyItemDto();
    private static final List<Long> CHILD_ITEM_LIST = new ArrayList<Long>();
    private static final List<Long> CHILD_ITEM_LIST_ORIGIONAL = new ArrayList<Long>();
    private static final Long ZERO = 0L;
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final Long FOUR = 4L;
    private static final Long FIVE = 5L;
    private static final Long SIX = 6L;
    private static final String STRING_ONE = "1";

    @Mock
    private ValidationService validationService;

    @Mock
    private AssetValidationService assetValidationService;

    /*@Mock
    private DraftItemDao draftItemDao;*/

    @Mock
    private CodicilDao codicilDao;

    @Mock
    private DefectDao defectDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private ItemDao itemDao;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @Mock
    private ItemTypeDao itemTypeDao;

    @InjectMocks
    private final DraftItemValidationServiceImpl draftItemValidationService = new DraftItemValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp()
    {
        ITEM1.setId(ONE);
        ITEM1.setItemTypeId(ONE);
        ITEM1.setToParentId(ONE);
        ITEM2.setId(TWO);
        ITEM2.setItemTypeId(TWO);
        ITEM2.setToParentId(ONE);

        INPUT.setDraftItemDtoList(new ArrayList<DraftItemDto>());
        INPUT.getDraftItemDtoList().add(ITEM1);
        INPUT.getDraftItemDtoList().add(ITEM2);

        RELATIONSHIP1.setToItem(new ItemLightDto());
        RELATIONSHIP1.getToItem().setId(TWO);
        DRAFT_RELATIONSHIP1.setRelationshipTypeId(ONE);
        DRAFT_RELATIONSHIP1.setOriginalItemId(TWO);

        LAZY_ITEM_DTO_1.setId(ONE);
        LAZY_ITEM_DTO_1.setItemType(new LinkResource(ONE));
        LAZY_ITEM_DTO_1.setRelated(new ArrayList<LinkVersionedResource>());
//        LAZY_ITEM_DTO_1.getRelated().add(RELATIONSHIP1);

        CHILD_ITEM_LIST.add(ONE);
        CHILD_ITEM_LIST.add(THREE);
        CHILD_ITEM_LIST.add(FOUR);

        CHILD_ITEM_LIST_ORIGIONAL.add(FIVE);
        CHILD_ITEM_LIST_ORIGIONAL.add(SIX);

        ROOT_ITEM.setId(SIX);
    }


    @Test
    public void validateDraftItemsFromReferenceDataPass() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(TWO, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, TWO, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, TWO)).thenReturn(ZERO.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    @Test
    public void validateDraftItemsFromReferenceDataFailTooManyChildItems() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*thrown.expect(MastBusinessException.class);

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(TWO, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, TWO, ONE.intValue())).thenReturn(ONE.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, TWO)).thenReturn(ONE.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    @Test
    public void validateDraftItemsFromReferenceDataFail() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*thrown.expect(MastBusinessException.class);

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(false);

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    //Keep these
//
//    @Test
//    public void verifyDeleteDraftItemPass() throws BadRequestException, RecordNotFoundException, MastBusinessException
//    {
//        doNothing().when(this.validationService).verifyBody(INPUT);
//        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
//        doNothing().when(this.validationService).verifyId(STRING_ONE);
//        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getRootItem(ONE, LazyItemDto.class)).thenReturn(ROOT_ITEM);
//        when(this.draftItemDao.getItemChildIdList(ONE)).thenReturn(CHILD_ITEM_LIST);
//        when(this.draftItemDao.getOriginalItemIdsFromDraftItemIds(CHILD_ITEM_LIST)).thenReturn(CHILD_ITEM_LIST_ORIGIONAL);
//        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(false);
//        when(this.defectDao.doAnyItemsHaveOpenDefects(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(false);
//
//        this.draftItemValidationService.verifyDeleteDraftItem(STRING_ONE, STRING_ONE);
//    }
//
//    @Test
//    public void verifyDeleteDraftItemPassNoChildrenCopiedFromAssetModel() throws BadRequestException, RecordNotFoundException, MastBusinessException
//    {
//        doNothing().when(this.validationService).verifyBody(INPUT);
//        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
//        doNothing().when(this.validationService).verifyId(STRING_ONE);
//        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getRootItem(ONE, LazyItemDto.class)).thenReturn(ROOT_ITEM);
//        when(this.draftItemDao.getItemChildIdList(ONE)).thenReturn(CHILD_ITEM_LIST);
//        when(this.draftItemDao.getOriginalItemIdsFromDraftItemIds(CHILD_ITEM_LIST)).thenReturn(new ArrayList<Long>());
//
//        this.draftItemValidationService.verifyDeleteDraftItem(STRING_ONE, STRING_ONE);
//    }
//
//    @Test
//    public void verifyDeleteDraftItemFailsDefectsAttached() throws BadRequestException, RecordNotFoundException, MastBusinessException
//    {
//        thrown.expect(MastBusinessException.class);
//
//        doNothing().when(this.validationService).verifyBody(INPUT);
//        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
//        doNothing().when(this.validationService).verifyId(STRING_ONE);
//        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getRootItem(ONE, LazyItemDto.class)).thenReturn(ROOT_ITEM);
//        when(this.draftItemDao.getItemChildIdList(ONE)).thenReturn(CHILD_ITEM_LIST);
//        when(this.draftItemDao.getOriginalItemIdsFromDraftItemIds(CHILD_ITEM_LIST)).thenReturn(CHILD_ITEM_LIST_ORIGIONAL);
//        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(false);
//        when(this.defectDao.doAnyItemsHaveOpenDefects(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(true);
//
//        this.draftItemValidationService.verifyDeleteDraftItem(STRING_ONE, STRING_ONE);
//    }
//
//    @Test
//    public void verifyDeleteDraftItemFailsCodicilsAttached() throws BadRequestException, RecordNotFoundException, MastBusinessException
//    {
//        thrown.expect(MastBusinessException.class);
//
//        doNothing().when(this.validationService).verifyBody(INPUT);
//        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
//        doNothing().when(this.validationService).verifyId(STRING_ONE);
//        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getRootItem(ONE, LazyItemDto.class)).thenReturn(ROOT_ITEM);
//        when(this.draftItemDao.getItemChildIdList(ONE)).thenReturn(CHILD_ITEM_LIST);
//        when(this.draftItemDao.getOriginalItemIdsFromDraftItemIds(CHILD_ITEM_LIST)).thenReturn(CHILD_ITEM_LIST_ORIGIONAL);
//        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(true);
//
//        this.draftItemValidationService.verifyDeleteDraftItem(STRING_ONE, STRING_ONE);
//    }
//
//    @Test
//    public void verifyDeleteDraftItemFailsRootItem() throws BadRequestException, RecordNotFoundException, MastBusinessException
//    {
//        thrown.expect(BadRequestException.class);
//
//        doNothing().when(this.validationService).verifyBody(INPUT);
//        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
//        doNothing().when(this.validationService).verifyId(STRING_ONE);
//        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getRootItem(ONE, LazyItemDto.class)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.draftItemDao.getItemChildIdList(ONE)).thenReturn(CHILD_ITEM_LIST);
//        when(this.draftItemDao.getOriginalItemIdsFromDraftItemIds(CHILD_ITEM_LIST)).thenReturn(CHILD_ITEM_LIST_ORIGIONAL);
//        when(this.codicilDao.doAnyItemsHaveOpenCodicils(CHILD_ITEM_LIST_ORIGIONAL)).thenReturn(true);
//
//        this.draftItemValidationService.verifyDeleteDraftItem(STRING_ONE, STRING_ONE);
//    }

    @Test
    public void validateAppendSpecificCopiedItems() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*INPUT.getDraftItemDtoList().remove(ITEM2);
        ITEM1.setItemTypeId(null);
        ITEM1.setFromId(TWO);
        ITEM1.setItemRelationshipDto(new ArrayList<DraftItemRelationshipDto>());
        ITEM1.getItemRelationshipDto().add(DRAFT_RELATIONSHIP1);

        RELATIONSHIP1.setType(new LinkResource(ONE));

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(TWO)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    @Test
    public void validateAppendNullItemList() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage("The passed draft item list for asset id [1] should not be empty.");

        final DraftItemListDto draftItemList = new DraftItemListDto();

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, draftItemList);
    }

    @Test
    public void validateAppendEmptyItemList() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        thrown.expect(MastBusinessException.class);
        thrown.expectMessage("The passed draft item list for asset id [1] should not be empty.");

        final DraftItemListDto draftItemList = new DraftItemListDto();
        draftItemList.setDraftItemDtoList(new ArrayList<>());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, draftItemList);
    }

    @Test
    public void validateAppendSpecificCopiedItemsDelfaultToChild() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*INPUT.getDraftItemDtoList().remove(ITEM2);
        ITEM1.setItemTypeId(null);
        ITEM1.setFromId(TWO);

        RELATIONSHIP1.setType(new LinkResource(ONE));

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(TWO)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    @Test
    public void validateAppendSpecificCopiedItemsWrongChildItem() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*thrown.expect(RecordNotFoundException.class);

        INPUT.getDraftItemDtoList().remove(ITEM2);
        ITEM1.setItemTypeId(null);
        ITEM1.setFromId(TWO);

        DRAFT_RELATIONSHIP1.setOriginalItemId(THREE);

        ITEM1.setItemRelationshipDto(new ArrayList<DraftItemRelationshipDto>());
        ITEM1.getItemRelationshipDto().add(DRAFT_RELATIONSHIP1);

        RELATIONSHIP1.setType(new LinkResource(ONE));

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(TWO)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }

    @Test
    public void validateAppendSpecificCopiedItemsRelationshipIsWorngType() throws BadRequestException, RecordNotFoundException, MastBusinessException
    {
        /*thrown.expect(RecordNotFoundException.class);

        INPUT.getDraftItemDtoList().remove(ITEM2);
        ITEM1.setItemTypeId(null);
        ITEM1.setFromId(TWO);
        ITEM1.setItemRelationshipDto(new ArrayList<DraftItemRelationshipDto>());
        ITEM1.getItemRelationshipDto().add(DRAFT_RELATIONSHIP1);

        RELATIONSHIP1.setType(new LinkResource(TWO));

        doNothing().when(this.validationService).verifyBody(INPUT);
        doNothing().when(this.assetValidationService).verifyAssetIdExists(STRING_ONE);
        doNothing().when(this.validationService).verifyId(STRING_ONE);
        when(this.draftItemDao.itemExistsForAsset(ONE, ONE)).thenReturn(true);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(ONE)).thenReturn(LAZY_ITEM_DTO_1);
//        when(this.itemDao.getItem(TWO)).thenReturn(LAZY_ITEM_DTO_1);
        when(this.assetDao.getAssetCategory(ONE)).thenReturn(ONE);
        when(this.draftItemDao.isProposedRelationshipValidForTypes(ONE, ONE, ONE.intValue(), RelationshipType.IS_PART_OF.getValue()))
                .thenReturn(true);
        when(this.itemTypeDao.getMaxOccursForItemType(ONE, ONE, ONE.intValue())).thenReturn(ZERO.intValue());
        when(this.draftItemDao.countChildItemsOfType(ONE, ONE)).thenReturn(ONE.intValue());

        this.draftItemValidationService.validateAppendDraftItems(STRING_ONE, INPUT);*/
    }
}
