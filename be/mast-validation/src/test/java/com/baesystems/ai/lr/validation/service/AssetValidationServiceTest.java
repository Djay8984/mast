package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(AssetValidationServiceImpl.class)
public class AssetValidationServiceTest
{
    private static final Long ID_ONE = 1L;
    private static final Long ID_TWO = 2L;
    private static final Long ID_THREE = 3L;

    private static final Long IMO_NUMBER = 456L;
    private static final String BUILDER = "21";
    private static final String YARD_NUMBER = "Y123";

    private AssetDto asset;

    @Mock
    private ValidationService validationService;

    @Mock
    private AssetDao assetDao;

    @Mock
    private IhsAssetDao ihsAssetDao;

    @InjectMocks
    private final AssetValidationServiceImpl assetValidationService = new AssetValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp()
    {
        asset = new AssetDto();
    }

    @Test
    public void createAssetFailsValidationWithNullBody() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.assetValidationService.validateCreateAsset(null);
    }

    @Test
    public void createAssetFailsValidationWithId() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        asset.setId(ID_ONE);

        doNothing().when(this.validationService).verifyBody(asset);
        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(asset.getId());

        thrown.expect(BadRequestException.class);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void createAssetFailsValidationWithoutImoOrBuilderAndYard() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        asset.setIhsAsset(null);
        asset.setBuilder(null);
        asset.setYardNumber(null);

        doNothing().when(this.validationService).verifyBody(asset);
        doNothing().when(this.validationService).verifyIdNull(null);

        thrown.expect(MastBusinessException.class);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void createAssetFailsValidationImoDoesNotExist() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        asset.setIhsAsset(new LinkResource(ID_ONE));

        doNothing().when(this.validationService).verifyBody(asset);
        doNothing().when(this.validationService).verifyIdNull(null);
        when(this.ihsAssetDao.getIhsAsset(ID_ONE)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void createAssetFailsValidationAssetAlreadyExists() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        asset.setIhsAsset(new LinkResource(ID_ONE));

        doNothing().when(this.validationService).verifyBody(asset);
        doNothing().when(this.validationService).verifyIdNull(null);
        when(this.ihsAssetDao.getIhsAsset(ID_ONE)).thenReturn(new IhsAssetDto());
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(ID_ONE, null, null)).thenReturn(ID_TWO);

        thrown.expect(MastBusinessException.class);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void createAssetPassesValidation() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        asset.setIhsAsset(new LinkResource(ID_ONE));

        doNothing().when(this.validationService).verifyBody(asset);
        doNothing().when(this.validationService).verifyIdNull(null);
        when(this.ihsAssetDao.getIhsAsset(ID_ONE)).thenReturn(new IhsAssetDto());
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(ID_ONE, null, null)).thenReturn(null);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void createAssetPassesValidationWithImoAndBuilderAndYard() throws RecordNotFoundException, MastBusinessException, BadRequestException
    {
        final IhsAssetDto ihsAsset = new IhsAssetDto();
        ihsAsset.setId(ID_THREE);

        asset.setIhsAsset(new LinkResource(ID_THREE));
        asset.setBuilder("New Builder Name");
        asset.setYardNumber(YARD_NUMBER);

        doNothing().when(this.validationService).verifyBody(asset);
        doNothing().when(this.validationService).verifyIdNull(null);
        when(this.ihsAssetDao.getIhsAsset(ID_THREE)).thenReturn(ihsAsset);
        when(this.assetDao.findAssetIdByImoOrBuilderAndYard(ID_THREE, "New Builder Name", YARD_NUMBER)).thenReturn(null);

        this.assetValidationService.validateCreateAsset(asset);
    }

    @Test
    public void verifyImoBuildYardComboFailOne() throws Exception
    {
        thrown.expect(MastBusinessException.class);
        testVerifyImoBuilderAndYardCombination(null, null, null);
    }

    @Test
    public void verifyImoBuildYardComboFailTwo() throws Exception
    {
        thrown.expect(MastBusinessException.class);
        testVerifyImoBuilderAndYardCombination(null, null, YARD_NUMBER);
    }

    @Test
    public void verifyImoBuildYardComboFailThree() throws Exception
    {
        thrown.expect(MastBusinessException.class);
        testVerifyImoBuilderAndYardCombination(null, BUILDER, null);
    }

    @Test
    public void verifyImoBuildYardComboPassOne() throws Exception
    {
        testVerifyImoBuilderAndYardCombination(null, BUILDER, YARD_NUMBER);
    }

    @Test
    public void verifyImoBuildYardComboPassTwo() throws Exception
    {
        testVerifyImoBuilderAndYardCombination(IMO_NUMBER, null, null);
    }

    @Test
    public void verifyImoBuildYardComboPassThree() throws Exception
    {
        testVerifyImoBuilderAndYardCombination(IMO_NUMBER, BUILDER, YARD_NUMBER);
    }

    @Test
    public void verifyImoBuildYardComboPassFour() throws Exception
    {
        testVerifyImoBuilderAndYardCombination(IMO_NUMBER, null, YARD_NUMBER);
    }

    @Test
    public void verifyImoBuildYardComboPassFive() throws Exception
    {
        testVerifyImoBuilderAndYardCombination(IMO_NUMBER, BUILDER, null);
    }

    private void testVerifyImoBuilderAndYardCombination(final Long imoNumber, final String builder, final String yardNumber) throws Exception
    {
        Whitebox.invokeMethod(this.assetValidationService, "verifyImoBuilderAndYardCombination",
                imoNumber,
                builder,
                yardNumber);
    }
}
