package com.baesystems.ai.lr.validation.service;

import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dao.references.jobs.CertificateReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationService;

@RunWith(MockitoJUnitRunner.class)
public class CertificateValidationServiceTest
{
    private static final Long INVALID_ID = 9999L;
    private static final Long CERTIFICATE_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long ONE = 1L;
    private static final String CERTIFICATE_NUMBER = "ADH15000001";
    private static final CertificateActionDto ISSUED = new CertificateActionDto();
    private static final CertificateActionDto NOT_ISSUED = new CertificateActionDto();

    private CertificateDto certificate;

    @Mock
    private ValidationService validationService;

    @Mock
    private CertificateReferenceDataDao certificateReferenceDataDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private WIPCertificateDao wipCertificateDao;

    @InjectMocks
    private final CertificateValidationServiceImpl certificateValidationService = new CertificateValidationServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp()
    {
        ISSUED.setIssueDateMutable(true);

        certificate = new CertificateDto();
        certificate.setId(CERTIFICATE_1_ID);
        certificate.setCertificateNumber(CERTIFICATE_NUMBER);
        certificate.setCertificateStatus(new LinkResource(ONE));
        certificate.setCertificateType(new LinkResource(ONE));
        certificate.setCertificateAction(new LinkResource(ONE));
        certificate.setExpiryDate(new Date());
        certificate.setExtendedDate(new Date());
        certificate.setIssueDate(new Date());
        certificate.setOffice(new LinkResource(ONE));
        certificate.setEmployee(new LinkResource(ONE));
        certificate.setJob(new LinkResource(ONE));
    }

    @Test
    public void testValidateCreateCertificateNullBody() throws BadRequestException, RecordNotFoundException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateCreateCertificate(null, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateIdNotNull() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).verifyBody(certificate);
        doThrow(new BadRequestException()).when(this.validationService).verifyIdNull(CERTIFICATE_1_ID);

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateNoJobFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.getJob().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(INVALID_ID.toString())).thenReturn(INVALID_ID);
        when(this.jobDao.getJob(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, INVALID_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateJobIdDoesNotMatch() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.getJob().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateActionNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.getCertificateAction().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateStatusNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.getCertificateStatus().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateTypeNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.getCertificateType().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateCanNotChangeDate() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(ONE)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(NOT_ISSUED);

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateCanNullDate() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.setIssueDate(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(ONE)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(NOT_ISSUED);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificatePass() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(ONE)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(ISSUED);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateAutoFillJobFromHeader() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.setJob(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(ONE)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(ISSUED);

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateCreateCertificateNullStatusAndAction() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(null);
        certificate.setCertificateStatus(null);
        certificate.setCertificateAction(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());

        this.certificateValidationService.validateCreateCertificate(certificate, JOB_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateNullBody() throws BadRequestException, RecordNotFoundException
    {
        doThrow(new BadRequestException()).when(this.validationService).verifyBody(null);

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateUpdateCertificate(null, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateNoJobFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.getJob().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(INVALID_ID.toString())).thenReturn(INVALID_ID);
        when(this.jobDao.getJob(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, INVALID_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateJobIdDoesNotMatch() throws BadRequestException, RecordNotFoundException
    {
        certificate.getJob().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateNoCertificateFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(INVALID_ID.toString())).thenReturn(INVALID_ID);
        when(this.wipCertificateDao.getCertificate(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), INVALID_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateCertificateIdDoesNotMatch() throws BadRequestException, RecordNotFoundException
    {
        certificate.setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        thrown.expect(BadRequestException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateActionNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.getCertificateAction().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateAction(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateStatusNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.getCertificateStatus().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateTypeNotFound() throws BadRequestException, RecordNotFoundException
    {
        certificate.getCertificateType().setId(INVALID_ID);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(INVALID_ID)).thenReturn(null);

        thrown.expect(RecordNotFoundException.class);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificatePass() throws BadRequestException, RecordNotFoundException
    {
        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(CERTIFICATE_1_ID)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(ISSUED);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateAutoFillJobFromHeader() throws BadRequestException, RecordNotFoundException
    {
        certificate.setJob(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(new CertificateActionDto());
        when(this.certificateReferenceDataDao.getCertificateStatus(ONE)).thenReturn(new ReferenceDataDto());
        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());
        when(this.wipCertificateDao.getCertificateIssueDate(CERTIFICATE_1_ID)).thenReturn(new Date());
        when(this.certificateReferenceDataDao.getCertificateAction(ONE)).thenReturn(ISSUED);

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }

    @Test
    public void testValidateUpdateCertificateNullStatusAndAction() throws BadRequestException, RecordNotFoundException
    {
        certificate.setCertificateStatus(null);
        certificate.setCertificateAction(null);

        doNothing().when(this.validationService).verifyBody(certificate);
        doNothing().when(this.validationService).verifyIdNull(null);

        when(this.validationService.verifyNumberFormat(JOB_1_ID.toString())).thenReturn(JOB_1_ID);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(new JobDto());

        when(this.validationService.verifyNumberFormat(CERTIFICATE_1_ID.toString())).thenReturn(CERTIFICATE_1_ID);
        when(this.wipCertificateDao.getCertificate(CERTIFICATE_1_ID)).thenReturn(new CertificateDto());

        when(this.certificateReferenceDataDao.getCertificateType(ONE)).thenReturn(new CertificateTypeDto());

        this.certificateValidationService.validateUpdateCertificate(certificate, JOB_1_ID.toString(), CERTIFICATE_1_ID.toString());
    }
}
