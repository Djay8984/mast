package com.baesystems.ai.lr;

import java.net.MalformedURLException;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.junit.BeforeClass;
import org.junit.Test;
import org.lr.ids.schema.AccountDetailsType;
import org.lr.ids.schema.AccountDetailsType.AddressDetails;
import org.lr.ids.schema.AddressDetailsType;
import org.lr.ids.schema.CdhDetails;
import org.lr.ids.schema.CdhDetails.Accounts;
import org.lr.ids.schema.ContactDetailsType;
import org.lr.ids.schema.EmailType;
import org.lr.ids.schema.OrgDetailsType;
import org.lr.ids.service.wsdl.CreateUpdateOrgPt;

/*
 * todo add mechanism to check for failure
 *
 * The "Tests" in this class currently do not have any way of measuring failure. Need to read from DB to confirm
 * data state or somehow tap into the proxied mechanism to catch exceptions
 */

public class CDHTest
{
    private static final String ORG_PT_SERVICE = "createUpdateOrgPtService";
    private static String testUrl;
    private static String testUsername;
    private static String testPassword;

    @BeforeClass
    public static void setup()
    {
        testUsername = System.getProperty("mast-integration-username");
        testPassword = System.getProperty("mast-integration-password");

        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(System.getProperty("mast-integration-url"));
        if (!(urlBuilder.lastIndexOf("/") == urlBuilder.length() - 1))
        {
            urlBuilder.append('/');
        }
        urlBuilder.append(ORG_PT_SERVICE);

        testUrl = urlBuilder.toString();
    }

    @Test
    public void testCreateCustomerMinimum() throws MalformedURLException
    {
        final CreateUpdateOrgPt createOrUpdate = getProxyInstance();
        final OrgDetailsType orgDetails = new OrgDetailsType();
        final CdhDetails.Organization organisation = new CdhDetails.Organization();
        organisation.setOrgDetails(orgDetails);
        final CdhDetails details = new CdhDetails();

        orgDetails.setPartyId("1234");
        orgDetails.setCustomerId("12345");
        orgDetails.setStatus("A");
        orgDetails.setCustomerName(testUrl);

        details.setOrganization(organisation);
        createOrUpdate.createUpdateOrgOp(details);
    }

    @Test
    public void testCreateCustomerComplete() throws MalformedURLException
    {
        final CreateUpdateOrgPt createOrUpdate = getProxyInstance();
        final AccountDetailsType account = createAccount();

        final Accounts accounts = new Accounts();
        accounts.getAccount().add(account);

        final CdhDetails.Organization organisation = new CdhDetails.Organization();
        final OrgDetailsType orgDetails = createOrgDetails();
        organisation.setOrgDetails(orgDetails);

        final EmailType emailType = new EmailType();
        emailType.setEmail("1@2.com");

        final ContactDetailsType.Emails emails = new ContactDetailsType.Emails();
        emails.getEmail().add(emailType);

        final ContactDetailsType contactDetailsType = new ContactDetailsType();
        contactDetailsType.setContactId("1");
        contactDetailsType.setEmails(emails);

        final CdhDetails.Organization.ContactDetails contactDetails = new CdhDetails.Organization.ContactDetails();
        contactDetails.getContact().add(contactDetailsType);

        final CdhDetails.Organization.AddressDetails ad = new CdhDetails.Organization.AddressDetails();
        ad.setAddress(createAddress());
        organisation.setAddressDetails(ad);
        organisation.setContactDetails(contactDetails);

        final CdhDetails details = new CdhDetails();
        details.setOrganization(organisation);
        details.setAccounts(accounts);
        createOrUpdate.createUpdateOrgOp(details);
    }

    @Test
    public void testCreateCustomerNullChecking()
    {
        final CreateUpdateOrgPt createOrUpdate = getProxyInstance();
        final AccountDetailsType account = createAccount();
        account.setClientFacingOffice(null);

        final Accounts accounts = new Accounts();
        accounts.getAccount().add(account);

        final CdhDetails.Organization organisation = new CdhDetails.Organization();
        final OrgDetailsType orgDetails = createOrgDetails();
        organisation.setOrgDetails(orgDetails);

        final ContactDetailsType contactDetailsType = new ContactDetailsType();
        contactDetailsType.setContactId("1");

        //Phones List but no phones
        final ContactDetailsType.Phones phones = new ContactDetailsType.Phones();
        contactDetailsType.setPhones(phones);

        final CdhDetails.Organization.ContactDetails contactDetails = new CdhDetails.Organization.ContactDetails();
        contactDetails.getContact().add(contactDetailsType);

        //Address details but no address
        final CdhDetails.Organization.AddressDetails ad = new CdhDetails.Organization.AddressDetails();
        organisation.setAddressDetails(ad);

        organisation.setContactDetails(contactDetails);

        final CdhDetails details = new CdhDetails();
        details.setOrganization(organisation);
        details.setAccounts(accounts);
        createOrUpdate.createUpdateOrgOp(details);
    }


    private CreateUpdateOrgPt getProxyInstance()
    {
        final JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(CreateUpdateOrgPt.class);
        factory.setUsername(testUsername);
        factory.setPassword(testPassword);
        factory.setAddress(testUrl);
        return (CreateUpdateOrgPt) factory.create();
    }

    private OrgDetailsType createOrgDetails()
    {
        final OrgDetailsType orgDetails = new OrgDetailsType();
        orgDetails.setPartyId("123456");
        orgDetails.setCustomerId("1234567");
        orgDetails.setStatus("A");
        orgDetails.setCustomerName("TEST CUSTOMER");
        return orgDetails;
    }

    private AccountDetailsType createAccount()
    {
        final AccountDetailsType account = new AccountDetailsType();
        account.setAccountNumber("1234");
        account.setJdeReferenceNumber("1234");
        account.setClientFacingOffice("N");
        account.setPartyId("1");
        account.setStream("M");

        final AddressDetailsType address = createAddress();
        final AccountDetailsType.AddressDetails addressDetailsTypeAccounts = new AddressDetails();
        addressDetailsTypeAccounts.setAddress(address);
        account.setAddressDetails(addressDetailsTypeAccounts);
        return account;
    }

    private AddressDetailsType createAddress()
    {
        final AddressDetailsType address = new AddressDetailsType();
        address.setAddressDescription("1");
        address.setAddressLine1("1");
        address.setAddressLine2("2");
        address.setAddressLine3("3");
        address.setAddressLine4("4");
        address.setCity("city");
        address.setCountry("GB");
        address.setCounty("NHATS");
        address.setPostalCode("NE12");
        return address;
    }

}
