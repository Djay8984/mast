privateKey = null, 
requestFilter = {
    urls : ["<all_urls>"]
},
options = new Object(), 
handler = function (details) {

    // stash current request on window object
    var currentRequest = {};
    currentRequest.url = details.url;
    currentRequest.requestBody = details.requestBody;
    currentRequest.method = details.method;
    this.currentRequest = currentRequest;
    return {
        cancel : false
    };
},
headerHandler = function (details) {
    // pull out currentRequest from window object
    
    if (!privateKey) {
        console.log("headerHandler - no private key"); 
        return {
            requestHeaders : details.requestHeaders 
        };
    }
       
    var currentRequest = this.currentRequest;        
    if (!currentRequest || !currentRequest.url) {
        console.log("headerHandler - no current request"); 
        return {
            requestHeaders : details.requestHeaders 
        };
    }
    
    var options = this.options;
    var pos = currentRequest.url.indexOf(options.apiPrefix);
    if (pos >= 0) {
        currentRequest.url = currentRequest.url.substring(pos);
    }

    var data = currentRequest.method + ':' + currentRequest.url;

    if (currentRequest.requestBody && currentRequest.requestBody.raw) {
        var rawBody = currentRequest.requestBody.raw[0].bytes;
        var encode = String.fromCharCode.apply(null, new Uint8Array(rawBody));
        data += '[' + encode + ']';

    }

    var sig = new KJUR.crypto.Signature({
            'alg' : options.algorithm,
            'prov' : 'cryptojs/jsrsa'
        });
    sig.initSign(privateKey);

    // calculate signature
    var sigValueHex = sig.signString(data) + '=';

    details.requestHeaders.push({
        name : "IDSClientSign",
        value : sigValueHex
    });
    details.requestHeaders.push({
        name : "IDSClientVersion",
        value : options.clientVersion
    });
    details.requestHeaders.push({
        name : "groups",
        value : options.groups
    });
    return {
        requestHeaders : details.requestHeaders
    };
};

chrome.storage.sync.get({init:false}, function(options) {
    if (!options.init) {
        var pemKey = '-----BEGIN RSA PRIVATE KEY-----' +
        'MIICXgIBAAKBgQCeINOcF8PowseGWV3xrkoXqMvx1xLOHD+bQaxNTCYar90Kw+Cw' +
        'aVyND0ZYmbSZjL2rQtypPA2ApAoXgwAkuEkvDwgfFCEPFHu6AD14RJDSY5zIVZOB' +
        'EXYg9tYsoToczth9gykjbHzfVuUvbjU+Zsvc6wEs9sdPj82BQ8vax8jyxQIDAQAB' +
        'AoGARqWj5Tk47eUX/44tyqxzrd5cP9A3Np6oTFUrBd3fqEyqFQUufaKVFvCcHTmO' +
        '1otqsflNXM7XuYGQgLCmf8FV8FyUFJke5fMQbTyy/OjRUGRUtinduNBj7rt3wNaq' +
        'vg/NOigis4xszuZvw/QOae8gtziZTtgpGDpVBHreFCDpCAECQQDNk2H2qtysUyVz' +
        'KzB3PLkW/E4X/tWWLjct9tF/CpBOPnUgJDmWMT224IOW0NlSXmUVVfmSl2u4bzFI' +
        'f4+xBhYVAkEAxOoYxJm8vdsUBEGekiuXAv/uXL4N11uBbnJGBhs5LNktIZPrYi7a' +
        'z99elpohZOlbE1seQUjtUup+SVWsvdDF8QJBAIHTTc6lYO9DIyd0YwsqQgmOFRN6' +
        'UBCj5x0T/oGofjGrp/RAbE25kyvm5bNc+aHXEydCQHafQdwb/Je4V1qIaXECQQCa' +
        'ffg0t1Gnuyx3MmyQzfZK+jUvOkVtw3NHDgz2WYGAOFv6Ti2M+KngEaYKjp7Ip3U3' +
        'OHQCUI8yUJwIOVmwCw5hAkEAtQKt0RWImXrV4SqPpjker1ib221mnCREdZMOvMB5' +
        '5i6LYG2M104REF+psYQkm4/VgBSUcv5vBVehvTQ53fqZGg==' +
        '-----END RSA PRIVATE KEY-----';
    
        var requiredOptions = new Object(); 
        requiredOptions.clientVersion = 'chrome-plugin';
        requiredOptions.apiPrefix = '/api';
        requiredOptions.algorithm = 'SHA256withRSA';
        requiredOptions.groups = 'Admin';
        requiredOptions.pemKey = pemKey;
        requiredOptions.init = true; 

        chrome.storage.sync.set(requiredOptions);
    }
});

var requiredOptions = new Object(); 
requiredOptions.clientVersion = null;
requiredOptions.apiPrefix = null;
requiredOptions.algorithm = null;
requiredOptions.groups = null;
requiredOptions.pemKey = null;

chrome.storage.sync.get(requiredOptions, function(changes) {
    for (key in changes) {
        options[key] = changes[key];
    }
});

chrome.storage.sync.get( {"pemKey":null}, function(o) {
    if (o.pemKey) {
        try {
            privateKey = new RSAKey();
            privateKey.readPrivateKeyFromPEMString(o.pemKey);
        }
        catch (e) 
        {
            console.log("error applying new key", e); 
        }
    }
});
    
chrome.storage.onChanged.addListener(
    function(changes, namespace) {
    for (key in changes) {
          var storageChange = changes[key];
          console.log("applying change on", this); 
          console.log('Storage key "%s" in namespace "%s" changed. ' +
                      'Old value was "%s", new value is "%s".',
                      key,
                      namespace,
                      storageChange.oldValue,
                      storageChange.newValue);
                      
            if (key == "pemKey" && storageChange.newValue) {
                try {
                    privateKey = new RSAKey();
                    privateKey.readPrivateKeyFromPEMString(storageChange.newValue);
                }
                catch (e) 
                {
                    console.log("error applying new key", e); 
                }
            }
            options[key] = storageChange.newValue;
        }
    }
);

chrome.webRequest.onBeforeRequest.addListener(handler, requestFilter, ['requestBody', 'blocking']);
chrome.webRequest.onBeforeSendHeaders.addListener(headerHandler, requestFilter, ['requestHeaders', 'blocking']);
