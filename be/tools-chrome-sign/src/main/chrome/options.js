// Saves options to chrome.storage.sync.
function save_options() {
  var clientVersion = document.getElementById('clientVersion').value;
  var algorithm = document.getElementById('algorithm').value;
  var apiPrefix = document.getElementById('apiPrefix').value;  
  var pemKey = document.getElementById('pemKey').value;
  var groups = document.getElementById('groups').value;
  
  chrome.storage.sync.set({
    clientVersion: clientVersion,
    algorithm: algorithm,
    apiPrefix: apiPrefix,
    groups: groups,
    pemKey: pemKey
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {  
            
  chrome.storage.sync.get({
    clientVersion: null,
    algorithm: null,
    apiPrefix: null,
    groups:  null,   
    pemKey : pemKey    
  }, function(items) {
    document.getElementById('clientVersion').value = items.clientVersion;
    document.getElementById('algorithm').value = items.algorithm;
    document.getElementById('apiPrefix').value = items.apiPrefix;
    document.getElementById('groups').value = items.groups;
    document.getElementById('pemKey').value = items.pemKey;    
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
var element = document.getElementById('save');
if (element) {
    element.addEventListener('click', save_options);
 }