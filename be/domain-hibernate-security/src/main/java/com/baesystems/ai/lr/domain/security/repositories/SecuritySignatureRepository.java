package com.baesystems.ai.lr.domain.security.repositories;

import java.util.List;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.SecuritySignatureDO;

public interface SecuritySignatureRepository extends SpringDataRepository<SecuritySignatureDO, Long>
{
    List<SecuritySignatureDO> findByEnabled(Boolean enabled);
}
