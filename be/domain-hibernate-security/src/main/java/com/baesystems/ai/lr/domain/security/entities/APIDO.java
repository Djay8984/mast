package com.baesystems.ai.lr.domain.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "RBAC_API")
public class APIDO extends BaseDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "uri", length = 100, nullable = false)
    private String uri;

    @Column(name = "method", length = 6, nullable = false)
    private String method;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @JoinColumn(name = "role_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private RoleDO role;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(final String uri)
    {
        this.uri = uri;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(final String method)
    {
        this.method = method;
    }

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }

    public RoleDO getRole()
    {
        return role;
    }

    public void setRole(final RoleDO role)
    {
        this.role = role;
    }
}
