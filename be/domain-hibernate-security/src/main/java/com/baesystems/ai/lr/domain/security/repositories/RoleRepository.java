package com.baesystems.ai.lr.domain.security.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.RoleDO;

public interface RoleRepository extends SpringDataRepository<RoleDO, Long>
{
    @Query(value = "select g.name, api.uri, api.method from GroupRoleDO gr"
            + " join gr.group g"
            + " join gr.role r"
            + " join r.apiList api"
            + " where "
            + " g.enabled = true"
            + " and r.enabled = true"
            + " and api.enabled = true"
            + " and gr.enabled = true")
    List<Object[]> getGroupRoles();
}
