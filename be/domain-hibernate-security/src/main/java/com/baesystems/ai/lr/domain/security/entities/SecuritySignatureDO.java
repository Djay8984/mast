package com.baesystems.ai.lr.domain.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "MAST_SEC_SIGNATURE")
public class SecuritySignatureDO extends BaseDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private String description;

    @Column(nullable = false)
    private String publicKey;

    @Column(nullable = false)
    private Boolean enabled;

    @Column(nullable = false)
    private String algorithm;

    @Column(nullable = false)
    private Long timeDelta;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getPublicKey()
    {
        return publicKey;
    }

    public void setPublicKey(final String publicKey)
    {
        this.publicKey = publicKey;
    }

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getAlgorithm()
    {
        return algorithm;
    }

    public void setAlgorithm(final String algorithm)
    {
        this.algorithm = algorithm;
    }

    public Long getTimeDelta()
    {
        return timeDelta;
    }

    public void setTimeDelta(final Long timeDelta)
    {
        this.timeDelta = timeDelta;
    }
}
