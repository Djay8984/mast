package com.baesystems.ai.lr.domain.security.repositories;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.APIDO;

public interface APIRepository extends SpringDataRepository<APIDO, Long>
{
}
