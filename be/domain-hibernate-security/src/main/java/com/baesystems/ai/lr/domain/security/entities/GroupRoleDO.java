package com.baesystems.ai.lr.domain.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "RBAC_GROUP_ROLE")
public class GroupRoleDO extends BaseDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "group_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private GroupDO group;

    @JoinColumn(name = "role_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private RoleDO role;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public GroupDO getGroup()
    {
        return group;
    }

    public void setGroup(final GroupDO group)
    {
        this.group = group;
    }

    public RoleDO getRole()
    {
        return role;
    }

    public void setRole(final RoleDO role)
    {
        this.role = role;
    }

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }
}
