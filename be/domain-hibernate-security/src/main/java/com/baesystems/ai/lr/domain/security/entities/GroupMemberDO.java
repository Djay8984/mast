package com.baesystems.ai.lr.domain.security.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "RBAC_GROUP_MEMBER")
public class GroupMemberDO extends BaseDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "user_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private UserDO user;

    @JoinColumn(name = "group_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private GroupDO group;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public UserDO getUser()
    {
        return user;
    }

    public void setUser(final UserDO user)
    {
        this.user = user;
    }

    public GroupDO getGroup()
    {
        return group;
    }

    public void setGroup(final GroupDO group)
    {
        this.group = group;
    }

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }
}
