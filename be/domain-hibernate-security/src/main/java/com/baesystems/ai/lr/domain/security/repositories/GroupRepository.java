package com.baesystems.ai.lr.domain.security.repositories;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.GroupDO;

public interface GroupRepository extends SpringDataRepository<GroupDO, Long>
{
}
