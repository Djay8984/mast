package com.baesystems.ai.lr.domain.security.repositories;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.UserDO;

public interface UserRepository extends SpringDataRepository<UserDO, Long>
{
}
