package com.baesystems.ai.lr.domain.security.repositories;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.GroupRoleDO;

public interface GroupRoleRepository extends SpringDataRepository<GroupRoleDO, Long>
{
}
