package com.baesystems.ai.lr.domain.security.repositories;

import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.domain.security.entities.GroupMemberDO;

public interface GroupMemberRepository extends SpringDataRepository<GroupMemberDO, Long>
{
}
