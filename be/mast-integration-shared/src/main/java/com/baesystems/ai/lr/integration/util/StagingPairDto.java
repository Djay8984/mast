package com.baesystems.ai.lr.integration.util;

public class StagingPairDto<S, T>
{
    private S sourceObject;

    private T targetObject;

    public S getSourceObject()
    {
        return sourceObject;
    }

    public void setSourceObject(final S sourceObject)
    {
        this.sourceObject = sourceObject;
    }

    public T getTargetObject()
    {
        return targetObject;
    }

    public void setTargetObject(final T targetObject)
    {
        this.targetObject = targetObject;
    }
}
