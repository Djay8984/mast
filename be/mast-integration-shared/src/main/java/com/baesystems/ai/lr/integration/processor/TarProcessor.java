package com.baesystems.ai.lr.integration.processor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.apache.camel.Exchange;
import org.apache.camel.Message;

public class TarProcessor extends AbstractTarProcessor
{
    @Override
    public void process(final Exchange exchange) throws IOException, NoSuchAlgorithmException
    {
        @SuppressWarnings("unchecked")
        final Iterator<Message> iterator = exchange.getIn().getBody(Iterator.class);

        byte[] file = null;
        for (Message message = iterator.next(); message != null; message = iterator.next())
        {
            final InputStream stream = message.getBody(InputStream.class);
            file = this.convertToByteArray(stream);
        }

        final File tempFile = File.createTempFile(TEMP_FILE_NAME_PREFIX, getFileType());
        tempFile.deleteOnExit();

        try (FileOutputStream writer = new FileOutputStream(tempFile))
        {
            writer.write(file);
        }

        // Add clean up operation
        exchange.addOnCompletion(new OnCompletionEvent(tempFile));
        exchange.getOut().setHeader(Exchange.FILE_NAME_PRODUCED, tempFile.toURI().toURL().toExternalForm());
    }
}
