package com.baesystems.ai.lr.integration.util;

import java.util.ArrayList;
import java.util.List;

/**
 * ItemProcessor wraps output in a List to allow batch commit size control, therefore if we wish to pass an actual list of entities to a JPAWriter
 * we need to iterate.
 */
public class ListJPAItemWriter<T extends List> extends JPAItemWriter<T>
{
    @Override
    public List<T> writeList(final List<? extends T> items)
    {
        List result = null;
        if (items != null)
        {
            result = new ArrayList<>();
            for (final List itemList : items)
            {
                if (!itemList.isEmpty())
                {
                    result.add(super.writeList(itemList));
                }
            }
        }
        return result;
    }
}
