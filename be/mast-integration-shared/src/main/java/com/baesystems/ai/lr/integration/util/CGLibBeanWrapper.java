package com.baesystems.ai.lr.integration.util;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.support.DefaultPropertyEditorRegistrar;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.validation.BindException;

public class CGLibBeanWrapper<T> extends DefaultPropertyEditorRegistrar implements FieldSetMapper<T>,
        BeanFactoryAware
{
    private String prototypeBeanName;

    @Autowired
    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(final BeanFactory beanFactory)
    {
        this.beanFactory = beanFactory;
    }

    public void setPrototypeBeanName(final String prototypeBeanName)
    {
        this.prototypeBeanName = prototypeBeanName;
    }

    @Override
    public T mapFieldSet(final FieldSet fieldSet) throws BindException
    {
        final T copy = this.getBean();

        final String[] keys = fieldSet.getNames();
        final String[] values = fieldSet.getValues();

        final BeanMap entity = BeanMap.create(copy);
        for (int index = 0; index < keys.length; index++)
        {
            entity.put(keys[index], values[index]);
        }
        return copy;
    }

    @SuppressWarnings("unchecked")
    private T getBean()
    {
        T result = null;
        if (prototypeBeanName != null)
        {
            result = (T) beanFactory.getBean(prototypeBeanName);
        }
        return result;
    }
}
