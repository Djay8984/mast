package com.baesystems.ai.lr.integration.processor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spi.Synchronization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTarProcessor implements Processor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTarProcessor.class);
    private static final int BYTE_BUFFER_SIZE = 16384;
    protected static final String TEMP_FILE_NAME_PREFIX = "tar-contents";

    private String fileType;

    public String getFileType()
    {
        return fileType.toLowerCase(Locale.ENGLISH);
    }

    public void setFileType(final String fileType)
    {
        this.fileType = fileType;
    }

    protected byte[] convertToByteArray(final InputStream stream) throws IOException
    {
        try (final ByteArrayOutputStream buffer = new ByteArrayOutputStream();)
        {
            final byte[] data = new byte[BYTE_BUFFER_SIZE];

            for (int nRead = stream.read(data, 0, data.length); nRead != -1; nRead = stream.read(data, 0, data.length))
            {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        }
    }

    static class OnCompletionEvent implements Synchronization
    {
        private final File tempFile;

        OnCompletionEvent(final File tempFile)
        {
            this.tempFile = tempFile;
        }

        @Override
        public void onComplete(final Exchange exchange)
        {
            final boolean result = tempFile.delete();
            LOGGER.debug("onComplete - temp file deleted:" + result);
        }

        @Override
        public void onFailure(final Exchange exchange)
        {
            final boolean result = tempFile.delete();
            LOGGER.debug("onFailure - temp file deleted:" + result);
        }
    }
}
