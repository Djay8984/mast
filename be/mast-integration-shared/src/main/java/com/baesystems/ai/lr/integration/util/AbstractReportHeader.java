package com.baesystems.ai.lr.integration.util;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

import java.io.IOException;
import java.io.Writer;

public abstract class AbstractReportHeader implements FlatFileHeaderCallback
{
    // Report Types
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";

    private String reportType;
    private String delimiter;

    protected abstract String getFailureHeader();

    protected abstract String getSuccessHeader();

    @Override
    public void writeHeader(final Writer writer) throws IOException
    {
        writer.write(getHeader());
    }

    private String getHeader()
    {
        String header = "";

        if (SUCCESS.equals(this.reportType))
        {
            header = getSuccessHeader();
        }
        else if (FAILURE.equals(this.reportType))
        {
            header = getFailureHeader();
        }

        return header;
    }

    public void setReportType(final String reportType)
    {
        this.reportType = reportType;
    }

    public void setDelimiter(final String delimiter)
    {
        this.delimiter = delimiter;
    }

    public String getDelimiter()
    {
        return delimiter;
    }
}
