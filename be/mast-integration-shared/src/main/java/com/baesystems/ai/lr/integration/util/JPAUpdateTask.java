package com.baesystems.ai.lr.integration.util;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

public class JPAUpdateTask extends AbstractJPAUpdateTask
{
    private String queryString;

    public void setQueryString(final String queryString)
    {
        this.queryString = queryString;
    }

    /**
     * Check mandatory properties - there must be an entityManagerFactory.
     */
    @Override
    public void afterPropertiesSet()
    {
        Assert.notNull(this.queryString, "A queryString is required");
    }

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext)
    {
        final TransactionTemplate template = new TransactionTemplate(getTransactionManager());
        template.execute(new ExecuteCallback());
        return RepeatStatus.FINISHED;
    }

    private class ExecuteCallback implements TransactionCallback<Integer>
    {
        @Override
        @Transactional
        public Integer doInTransaction(final TransactionStatus status)
        {
            final EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(getEntityManagerFactory());
            if (entityManager == null)
            {
                throw new DataAccessResourceFailureException("Unable to obtain a transactional EntityManager");
            }
            final Query queryImpl = entityManager.createQuery(JPAUpdateTask.this.queryString);
            final int updateCount = queryImpl.executeUpdate();

            entityManager.flush();

            return Integer.valueOf(updateCount);
        }

    }
}
