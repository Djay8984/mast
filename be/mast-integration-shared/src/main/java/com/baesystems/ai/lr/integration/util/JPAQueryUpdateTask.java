package com.baesystems.ai.lr.integration.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

public class JPAQueryUpdateTask extends AbstractJPAUpdateTask
{
    private String queryString;

    private String updateString;

    public void setUpdateString(final String updateString)
    {
        this.updateString = updateString;
    }

    public void setQueryString(final String queryString)
    {
        this.queryString = queryString;
    }

    /**
     * Check mandatory properties - there must be an entityManagerFactory.
     */
    @Override
    public void afterPropertiesSet()
    {
        super.afterPropertiesSet();

        Assert.notNull(this.queryString, "A queryString is required");
        Assert.notNull(this.updateString, "A updateQueryString is required");
    }

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext)
    {
        final TransactionTemplate template = new TransactionTemplate(getTransactionManager());
        template.execute(new ExecuteCallback());
        return RepeatStatus.FINISHED;
    }

    private class ExecuteCallback implements TransactionCallback<Integer>
    {
        @Override
        @Transactional
        public Integer doInTransaction(final TransactionStatus status)
        {
            final EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(getEntityManagerFactory());
            if (entityManager == null)
            {
                throw new DataAccessResourceFailureException("Unable to obtain a transactional EntityManager");
            }
            final Query queryImpl = entityManager.createQuery(JPAQueryUpdateTask.this.queryString);
            final List<?> results = queryImpl.getResultList();

            int updateCount = 0;
            if (!results.isEmpty())
            {
                final Query updateQueryImpl = entityManager.createQuery(JPAQueryUpdateTask.this.updateString);
                updateQueryImpl.setParameter("results", results);
                updateCount = updateQueryImpl.executeUpdate();
                entityManager.flush();
            }
            return Integer.valueOf(updateCount);
        }
    }
}
