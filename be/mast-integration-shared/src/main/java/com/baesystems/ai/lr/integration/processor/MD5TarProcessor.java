package com.baesystems.ai.lr.integration.processor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Locale;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.commons.codec.binary.Hex;

public class MD5TarProcessor extends AbstractTarProcessor
{
    private static final String ENCODING = "us-ascii";
    private static final String MD5_FILE_EXTENSION = ".MD5";
    private static final String MD5_DIGEST_ALGORITHM = "MD5";

    @Override
    public void process(final Exchange exchange) throws IOException, NoSuchAlgorithmException
    {
        @SuppressWarnings("unchecked")
        final Iterator<Message> iterator = exchange.getIn().getBody(Iterator.class);

        byte[] md5File = null;
        byte[] file = null;
        for (Message message = iterator.next(); message != null; message = iterator.next())
        {
            final String fileName = message.getHeader(Exchange.FILE_NAME, String.class).toUpperCase(Locale.ENGLISH);

            final InputStream stream = message.getBody(InputStream.class);
            final byte[] results = this.convertToByteArray(stream);
            if (fileName.endsWith(MD5_FILE_EXTENSION))
            {
                md5File = results;
            }
            else
            {
                file = results;
            }
        }

        final File tempFile = File.createTempFile(TEMP_FILE_NAME_PREFIX, getFileType());
        tempFile.deleteOnExit();

        try (FileOutputStream writer = new FileOutputStream(tempFile))
        {
            writer.write(file);
        }

        // Add clean up operation
        exchange.addOnCompletion(new OnCompletionEvent(tempFile));

        final boolean checksumValid = this.validateChecksum(md5File, file);
        exchange.getOut().setHeader("CHECKSUM_VALID", checksumValid);
        exchange.getOut().setHeader(Exchange.FILE_NAME_PRODUCED, tempFile.toURI().toURL().toExternalForm());
    }

    private boolean validateChecksum(final byte[] md5File, final byte[] file) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        final MessageDigest messageDigest = MessageDigest.getInstance(MD5_DIGEST_ALGORITHM);
        final byte[] digestBytes = messageDigest.digest(file);
        final String hexString = Hex.encodeHexString(digestBytes);

        final String providedHexString = new String(md5File, ENCODING).split(" ")[0];
        return providedHexString.equals(hexString);
    }
}
