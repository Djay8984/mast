package com.baesystems.ai.lr.integration.util;

public final class ErrorMessage
{
    private ErrorMessage()
    {
    }

    public static final String INVALID_JSON = "Invalid JSON. Expected %s but found %s. Format must be %s";
}
