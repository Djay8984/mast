package com.baesystems.ai.lr.integration.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class JPAStagingTask<S, T> implements ItemWriter<StagingPairDto<S, T>>, InitializingBean
{
    private final ItemProcessor<StagingPairDto<S, T>, T> itemProcessor;

    private final JPAItemWriter<S> sourceTask;

    private final JPAItemWriter<T> targetTask;

    public JPAStagingTask(final JPAItemWriter<S> sourceTaskLocal, final JPAItemWriter<T> targetTaskLocal,
            final ItemProcessor<StagingPairDto<S, T>, T> itemProcessorLocal)
    {
        this.sourceTask = sourceTaskLocal;
        this.targetTask = targetTaskLocal;
        this.itemProcessor = itemProcessorLocal;
    }

    @Override
    public void afterPropertiesSet()
    {
        Assert.notNull(itemProcessor, "Item Processor");
        Assert.notNull(sourceTask, "Source Task");
        Assert.notNull(targetTask, "Target Task");
    }

    @Override
    @SuppressWarnings("PMD")
    public void write(final List<? extends StagingPairDto<S, T>> items) throws Exception
    {
        // save source list
        final List<S> sourceList = new ArrayList<S>(items.size());
        for (final StagingPairDto<S, T> pair : items)
        {
            sourceList.add(pair.getSourceObject());
        }

        final List<S> updatedSourceList = sourceTask.writeList(sourceList);

        // should always be 100% the same, but safety and same order
        final int length = Math.min(sourceList.size(), updatedSourceList.size());
        final Map<S, S> sourceMap = new HashMap<S, S>(length, 1);
        for (int index = 0; index < length; index++)
        {
            sourceMap.put(sourceList.get(index), updatedSourceList.get(index));
        }

        // push changes to target
        final List<T> targetList = new ArrayList<T>(items.size());
        for (final StagingPairDto<S, T> pair : items)
        {
            S source = pair.getSourceObject();
            source = sourceMap.get(source);

            T target = pair.getTargetObject();

            final StagingPairDto<S, T> targetPair = new StagingPairDto<S, T>();
            targetPair.setSourceObject(source);
            targetPair.setTargetObject(target);

            target = itemProcessor.process(targetPair);

            targetList.add(target);
        }

        targetTask.write(targetList);
    }
}
