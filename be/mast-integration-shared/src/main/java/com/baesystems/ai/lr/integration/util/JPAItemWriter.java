package com.baesystems.ai.lr.integration.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.util.Assert;

public class JPAItemWriter<T> implements ItemWriter<T>, InitializingBean
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAItemWriter.class);

    private EntityManagerFactory entityManagerFactory;

    /**
     * Set the EntityManager to be used internally.
     *
     * @param entityManagerFactory the entityManagerFactory to set
     */
    public void setEntityManagerFactory(final EntityManagerFactory entityManagerFactory)
    {
        this.entityManagerFactory = entityManagerFactory;
    }

    /**
     * Check mandatory properties - there must be an entityManagerFactory.
     */
    @Override
    public void afterPropertiesSet()
    {
        Assert.notNull(entityManagerFactory, "An EntityManagerFactory is required");
    }

    /**
     * Merge all provided items that aren't already in the persistence context and then flush the entity manager.
     *
     * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
     */
    @Override
    public void write(final List<? extends T> items)
    {
        writeList(items);
    }

    public List<T> writeList(final List<? extends T> items)
    {
        final EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
        if (entityManager == null)
        {
            throw new DataAccessResourceFailureException("Unable to obtain a transactional EntityManager");
        }

        final List<T> results = doWrite(entityManager, items);
        entityManager.flush();

        return results;
    }

    /**
     * Do perform the actual write operation. This can be overridden in a subclass if necessary.
     *
     * @param entityManager the EntityManager to use for the operation
     * @param items the list of items to use for the write
     */
    protected List<T> doWrite(final EntityManager entityManager, final List<? extends T> items)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Writing to JPA with " + items.size() + " items.");
        }

        final List<T> results = new ArrayList<T>(items.size());
        if (!items.isEmpty())
        {
            long mergeCount = 0;
            for (final T item : items)
            {
                if (!entityManager.contains(item))
                {
                    final T result = entityManager.merge(item);
                    results.add(result);
                    mergeCount++;
                }
            }

            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug(mergeCount + " entities merged.");
                LOGGER.debug((items.size() - mergeCount) + " entities found in persistence context.");
            }
        }
        return results;
    }
}
