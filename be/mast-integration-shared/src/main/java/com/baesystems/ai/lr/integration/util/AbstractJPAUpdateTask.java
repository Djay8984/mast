package com.baesystems.ai.lr.integration.util;

import javax.persistence.EntityManagerFactory;

import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.util.Assert;

public abstract class AbstractJPAUpdateTask implements Tasklet, InitializingBean
{

    private PlatformTransactionManager transactionManager;

    private EntityManagerFactory entityManagerFactory;

    /**
     * Set the EntityManager to be used internally.
     *
     * @param entityManagerFactory the entityManagerFactory to set
     */
    public final void setEntityManagerFactory(final EntityManagerFactory entityManagerFactory)
    {
        this.entityManagerFactory = entityManagerFactory;
    }

    public final void setTransactionManager(final PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public final EntityManagerFactory getEntityManagerFactory()
    {
        return entityManagerFactory;
    }

    public final PlatformTransactionManager getTransactionManager()
    {
        return transactionManager;
    }

    @Override
    public void afterPropertiesSet()
    {
        Assert.notNull(entityManagerFactory, "An EntityManagerFactory is required");
        Assert.notNull(transactionManager, "A transaction manager must be provided");
    }

}
