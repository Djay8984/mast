SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `${jdbc.security.schema}` ;
CREATE SCHEMA IF NOT EXISTS `${jdbc.security.schema}` DEFAULT CHARACTER SET utf8 ;
USE `${jdbc.security.schema}` ;

-- -----------------------------------------------------
-- Table RBAC_USER
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_USER`;

CREATE TABLE IF NOT EXISTS `RBAC_USER`(
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL UNIQUE,
  `password` VARCHAR(50) NOT NULL,
  `enabled` BOOLEAN NOT NULL,
   PRIMARY KEY (`id`));
   
-- -----------------------------------------------------
-- Table RBAC_GROUP
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_GROUP`;

CREATE TABLE IF NOT EXISTS `RBAC_GROUP` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `enabled` BOOLEAN NOT NULL,
   PRIMARY KEY (`id`));
   
   
-- -----------------------------------------------------
-- Table RBAC_ROLE
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_ROLE`;

CREATE TABLE IF NOT EXISTS `RBAC_ROLE` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `enabled` BOOLEAN NOT NULL,
   PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table RBAC_API
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_API`;

CREATE TABLE IF NOT EXISTS `RBAC_API` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `role_id` BIGINT(11),
  `enabled` BOOLEAN NOT NULL,
  `uri` VARCHAR(100) NOT NULL,
  `method` VARCHAR(6) NOT NULL,
   PRIMARY KEY (`id`),
   INDEX `FK_ROLE_ROLE_idx` (`role_id` ASC),  
   CONSTRAINT `FK_ROLE_ROLE`
   FOREIGN KEY (`role_id`)
   REFERENCES `RBAC_ROLE` (`id`)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table RBAC_GROUP_MEMBER
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_GROUP_MEMBER`;

CREATE TABLE IF NOT EXISTS `RBAC_GROUP_MEMBER` (
   `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
   `user_id` BIGINT(11) NOT NULL,
   `group_id` BIGINT(11) NOT NULL,
   `enabled` BOOLEAN NOT NULL,
   CONSTRAINT FOREIGN KEY(`user_id`) REFERENCES `RBAC_USER`(`id`),
   CONSTRAINT FOREIGN KEY(`group_id`) REFERENCES `RBAC_GROUP`(`id`),
   PRIMARY KEY (`id`));

-- -----------------------------------------------------
-- Table RBAC_GROUP_ROLE
-- -----------------------------------------------------
DROP TABLE IF EXISTS `RBAC_GROUP_ROLE` ;

CREATE TABLE IF NOT EXISTS `RBAC_GROUP_ROLE` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `group_id` BIGINT(11) NOT NULL,
  `role_id` BIGINT(11) NOT NULL,
  `enabled` BOOLEAN NOT NULL,
  PRIMARY KEY (`id`), 
  CONSTRAINT RBAC_GROUP_ROLE_UNQ UNIQUE (group_id, role_id),
  INDEX `FK_GR_ROLE_idx` (`role_id` ASC),  
  CONSTRAINT `FK_GR_GROUP`
    FOREIGN KEY (`group_id`)
    REFERENCES `RBAC_GROUP` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_GR_ROLE`
    FOREIGN KEY (`role_id`)
    REFERENCES `RBAC_ROLE` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table `${jdbc.mast.schema}`.`MAST_SEC_SIGNATURE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MAST_SEC_SIGNATURE` ;

CREATE  TABLE IF NOT EXISTS `MAST_SEC_SIGNATURE` (
  `id` BIGINT(10) NOT NULL ,
  `name` VARCHAR(50) NOT NULL ,
  `description` VARCHAR(255) NOT NULL ,
  `publicKey` VARCHAR(20000) NOT NULL ,
  `algorithm` VARCHAR(50) NOT NULL ,
  `timeDelta` BIGINT(10) NOT NULL ,
  `enabled` BIGINT(10) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
