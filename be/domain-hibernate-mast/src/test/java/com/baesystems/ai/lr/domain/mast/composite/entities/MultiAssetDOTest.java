package com.baesystems.ai.lr.domain.mast.composite.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.ihs.entities.DummyLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.exception.MastSystemException;

public class MultiAssetDOTest
{
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testConstructorAndGetContentIhs()
    {
        final DummyIhsAssetDO asset = new DummyIhsAssetDO();
        asset.setIhsAsset(new DummyLinkDO());
        asset.getIhsAsset().setId(ONE);

        final MultiAssetDO multiAsset = new MultiAssetDO(asset, null);

        assertEquals(ONE, multiAsset.getImoNumber());
        assertTrue(DummyIhsAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testConstructorAndGetContentMast()
    {
        final VersionedAssetDO asset = new VersionedAssetDO();
        asset.setIhsAsset(new LinkDO(ONE));

        final MultiAssetDO multiAsset = new MultiAssetDO(null, asset);

        assertEquals(ONE, multiAsset.getImoNumber());
        assertTrue(VersionedAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testConstructorNullImoIhs()
    {
        final DummyIhsAssetDO asset = new DummyIhsAssetDO();
        asset.setIhsAsset(new DummyLinkDO());

        final MultiAssetDO multiAsset = new MultiAssetDO(asset, null);

        assertNull(multiAsset.getImoNumber());
        assertTrue(DummyIhsAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testConstructorNullImoMast()
    {
        final VersionedAssetDO asset = new VersionedAssetDO();
        asset.setIhsAsset(new LinkDO());

        final MultiAssetDO multiAsset = new MultiAssetDO(null, asset);

        assertNull(multiAsset.getImoNumber());
        assertTrue(VersionedAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testConstructorNullIhsAssetIhs()
    {
        final DummyIhsAssetDO asset = new DummyIhsAssetDO();

        final MultiAssetDO multiAsset = new MultiAssetDO(asset, null);

        assertNull(multiAsset.getImoNumber());
        assertTrue(DummyIhsAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testConstructorNull()
    {
        final MultiAssetDO multiAsset = new MultiAssetDO(null, null);

        assertNull(multiAsset.getImoNumber());
        assertNull(multiAsset.getContent());
    }

    @Test
    public void testConstructorNullIhsAssetMast()
    {
        final VersionedAssetDO asset = new VersionedAssetDO();

        final MultiAssetDO multiAsset = new MultiAssetDO(null, asset);

        assertNull(multiAsset.getImoNumber());
        assertTrue(VersionedAssetDO.class.isInstance(multiAsset.getContent()));
    }

    @Test
    public void testSetContentIhs()
    {
        final DummyIhsAssetDO asset1 = new DummyIhsAssetDO();
        asset1.setIhsAsset(new DummyLinkDO());
        asset1.getIhsAsset().setId(ONE);

        final VersionedAssetDO asset2 = new VersionedAssetDO();
        asset2.setIhsAsset(new LinkDO(TWO));

        final MultiAssetDO multiAsset = new MultiAssetDO(asset1, null);

        assertNull(multiAsset.getMastAsset());

        multiAsset.setContent(asset2);

        assertNotNull(multiAsset.getMastAsset());
        assertEquals(ONE, multiAsset.getImoNumber());
        assertEquals(TWO, multiAsset.getMastAsset().getIhsAsset().getId());
    }

    @Test
    public void testSetContentMast()
    {
        final VersionedAssetDO asset1 = new VersionedAssetDO();
        asset1.setIhsAsset(new LinkDO(ONE));

        final DummyIhsAssetDO asset2 = new DummyIhsAssetDO();
        asset2.setIhsAsset(new DummyLinkDO());
        asset2.getIhsAsset().setId(TWO);

        final MultiAssetDO multiAsset = new MultiAssetDO(null, asset1);

        assertNull(multiAsset.getIhsAsset());

        multiAsset.setContent(asset2);

        assertNotNull(multiAsset.getIhsAsset());
        assertEquals(ONE, multiAsset.getImoNumber());
        assertEquals(TWO, multiAsset.getIhsAsset().getIhsAsset().getId());
    }

    @Test
    public void testEqualsEqual()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);

        multiAsset1.setImoNumber(ONE);
        multiAsset2.setImoNumber(ONE);

        assertTrue(multiAsset1.equals(multiAsset2));
    }

    @Test
    public void testEqualsNotEqual()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);

        multiAsset1.setImoNumber(ONE);
        multiAsset2.setImoNumber(TWO);

        assertFalse(multiAsset1.equals(multiAsset2));
    }

    @Test
    public void testEqualsNullImo1()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);

        multiAsset2.setImoNumber(TWO);

        assertFalse(multiAsset1.equals(multiAsset2));
    }

    @Test
    public void testEqualsNullImo2()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);

        multiAsset1.setImoNumber(ONE);

        assertFalse(multiAsset1.equals(multiAsset2));
    }

    @Test
    public void testEqualsDifferentClass()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);

        multiAsset1.setImoNumber(ONE);

        assertFalse(multiAsset1.equals(new Object()));
    }

    @Test
    public void testSetContentWhenFull()
    {
        this.thrown.expect(MastSystemException.class);
        this.thrown.expectMessage("The calling method should not be trying to overwrite an exsting entity");

        final MultiAssetDO multiAsset = new MultiAssetDO(new DummyIhsAssetDO(), new VersionedAssetDO());

        multiAsset.setContent(new DummyIhsAssetDO());
    }

    @Test
    public void testHashCode()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset3 = new MultiAssetDO(null, null);

        multiAsset2.setImoNumber(ONE);
        multiAsset3.setImoNumber(ONE);

        final int hashCode1 = multiAsset1.hashCode();
        final int hashCode2 = multiAsset2.hashCode();
        final int hashCode3 = multiAsset3.hashCode();

        assertNotNull(hashCode1);
        assertNotNull(hashCode2);
        assertNotNull(hashCode3);
        assertTrue(hashCode1 != hashCode2 && hashCode2 == hashCode3);
    }

    @Test
    public void testMissedSetters()
    {
        final MultiAssetDO multiAsset1 = new MultiAssetDO(null, null);
        final MultiAssetDO multiAsset2 = new MultiAssetDO(null, null);

        multiAsset1.setIhsAsset(new DummyIhsAssetDO());
        multiAsset2.setMastAsset(new VersionedAssetDO());

        assertTrue(DummyIhsAssetDO.class.isInstance(multiAsset1.getContent()));
        assertTrue(VersionedAssetDO.class.isInstance(multiAsset2.getContent()));
    }
}
