package com.baesystems.ai.lr.domain.mast.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.bind.DatatypeConverter;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.baesystems.ai.lr.domain.mast.entities.references.FlagStateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.PortOfRegistryDO;
import com.baesystems.ai.lr.domain.mast.repositories.utils.TestUtils;
import com.baesystems.ai.lr.domain.repositories.BaseRepositoryFactory;

public class TestFlagRepository
{
    private static final String NAME = "Name";

    private static FlagStateRepository flagRepository;
    private static PortOfRegistryRepository portRepository;
    private static EntityManager em;

    /**
     * Sets up a {@link SimpleJpaRepository} instance.
     */
    @BeforeClass
    public static void setUp()
    {

        final EntityManagerFactory factory = Persistence.createEntityManagerFactory(TestUtils.MAST_PERSISTENCE);
        em = factory.createEntityManager();

        portRepository = new BaseRepositoryFactory(em).getRepository(PortOfRegistryRepository.class);
        flagRepository = new BaseRepositoryFactory(em).getRepository(FlagStateRepository.class);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testSavePort()
    {
        em.getTransaction().begin();

        try
        {
            PortOfRegistryDO port = new PortOfRegistryDO();
            port.setName(NAME);
            port = portRepository.save(port);
            assertEquals(port, portRepository.findOne(port.getId()));
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveFlag()
    {
        em.getTransaction().begin();

        try
        {
            FlagStateDO flagState = new FlagStateDO();
            flagState.setName(NAME);
            flagState = flagRepository.save(flagState);

            assertEquals(flagState, flagRepository.findOne(flagState.getId()));
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveFlagWithLinkedPorts()
    {
        em.getTransaction().begin();

        try
        {
            final PortOfRegistryDO portOne = new PortOfRegistryDO();
            portOne.setName("port1");
            portRepository.save(portOne);

            final PortOfRegistryDO portTwo = new PortOfRegistryDO();
            portTwo.setName("port2");
            portRepository.save(portTwo);

            final List<PortOfRegistryDO> ports = new ArrayList<PortOfRegistryDO>();
            ports.add(portOne);
            ports.add(portTwo);

            FlagStateDO flagState = new FlagStateDO();
            flagState.setName(NAME);
            flagState.setPorts(ports);
            flagState = flagRepository.save(flagState);

            assertEquals(flagState, flagRepository.findOne(flagState.getId()));
            assertFalse(flagState.getPorts() == null);
            assertTrue(flagState.getPorts().size() == 2);
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveFlagWithImage() throws FileNotFoundException, IOException, URISyntaxException
    {
        final String base64EncodedImage = "iVBORw0KGgoAAAANSUhEUgAAAFAAAAAoCAYAAABpYH0BAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAA"
                + "DsQAAA7EAZUrDhsAAAAGYktHRAD/AP8A/6C9p5MAAAYsSURBVGhD7ZtbUBNXGMf/uxDCPdRAlIsihKhAUFRuWrVTe0HbB3"
                + "uRsZ3O9PLQvrTTl87Uaat9KL29OFP70M60nd5mOuqotZ1Oq3RanfHKTUFBtIYA3oIGogJyCcGk5zvZrRBCSEyCIeXH5CHZ"
                + "PWf3+2V3s+d/FuH8YxWOrG+3IyojFROxe38zXn97H7rOmoDMWUBkhLQkAFzvwe7dr2FjeT5/e1wQEJuiBxwO/v4/2OcDXc"
                + "1YKX2+p+osKiq+Amar+PuAMHIHuHgDKflp+GL7JmxcuwAjXRZ0vLEZPb8dRER8HBzWYQz1tiM+twRZ33wGcaDmNE7OnYfm"
                + "wodhNV6UehpLxXo9zE1bUXtmK9KTYoFWs3Nj4QLVwmqi2urOV/JaNxaq0VK2DrUaDXr3H4GoUGCw6zwUWWlY1mpAQctRxK"
                + "8sgjjca0J0YjZsHZ04lZPjUWSxPg1XGt5DbdP74SFytLh/PuS1FakFLu64WgPruQ7EqHSw9rVJ4lqhbzwEpTZT6gAQS/uv"
                + "QZmbhcEeAxOp/X+I9CQu+a64oV4jFPNT3YqTEf9stiCv+gBWdpuZyPnhLTKA4oiqo60Qy8s+QcbSj1BvcYSvyACLq2s2cW"
                + "fr1myDCG0Krt4aQPHCLeEnMkjiSgo+4M7InciX0G1JjiZ8RAZbHHMl38o5BcpMd5FTKE5mrECZqRIpSCv4A/VxH8TJuBco"
                + "E2yRhi5WvJ058N0kb8PaUh/3Q5yMZ4Ey1ImORA6ieNFWZCz7GPU3JJEWdyJ1aF66Fta2yUVGqGJh6uqTlngPtaG29QZJXD"
                + "ITt2IdTiTPvgdxnbymksWVvEaqdTJxMgLi33QZdE4CG5PCxk6ZIQuUqfOwa9er2LCaibt6DYZnX8atmioooIEdVvbXg4S0"
                + "XOTs/A4Jq0ulDsYzcsfO9tf5XXo7Fpbb2Jta0PTUC7jV1ohoqNgRocQIupGwZA10e7+fUBqx769zeP7FH2DtvAREqwEFk+"
                + "a63UkQ9la1+NZChjwykcaLFqxdkY2ywrn84+FOM/qrT0KIUrCaBditNnZKt0P1xCOIyVvA1/GEtwKJodZ23Pz5d0TrtBCV"
                + "UawJW2azIaawAMr5GdJa7qluvIyDJ9qgzVRDwcVJC3xEYBu9x6bBwReBoYB318ApxJOe0FLnRDgWQvtFOxKnWQzYJ7hXFC"
                + "PQbz5zD7/ZwUNoSNGH1hc72SnKTuVQQmhIzg/FM2PaEHLXwOnGjEA/mRHoJzM/In4ycxvjJyE3EjnGjrC4CUYi/Wwk8uDM"
                + "SMQzno6u0Dp5nQQ8TJgIa8cVDDY2gY3c2cFEIcMwhgxGPPDMk4jOyZLWCsxYeLDlAnr++BtKbRZEpYJ15YBj2Ia4suWISt"
                + "XwdQIWJvgbZ+348SU8/WiutHA8FK5SzNV3+jAikcxjriH0ICm7EAW//ASxIM+vOGsi+o7UoPW5V9BnOgelFHPZYEZSaTmP"
                + "uRTpc/DrESM2bfrarzhLxBwVvHqlsldyAnDbivTcVNQ2bcOQ6dMJ5ZE4SqcppaaQNTpRhxFYkFBWhocs3VhibEBDzGxEpm"
                + "7GlzvqpFbeQ22oLYWh7qD8cenVFpQaOxBXuIxt+yZiVAt52FqXkc5T6w35SbyGuguf85qoNl4j1erOgZuXd9dAmnMw0JxD"
                + "DH8+5sqpd1Gsd/8w0nhxWp5WU2q9ovs68k4cQH23g0fnRbotuNMzgLQUttM+Qm2oLcXv1BfF8e5QZmdC33AQ9DwLpdOUUl"
                + "NaTSLp8Q0SWTRL4DXRczFUI9XKa/YCzwJHTdZQ/E7ROcXx7vAkjuZPKP6n+RQqluZX+JyDLgWIFNnlx/cLEG/D2lIf1Nek"
                + "IrVMZOMhHu+PE5ksiVQzkaxGml8ZM/nlAfcCgy1Onqzx3dt4qI9Rk19TLXKswKkSFyzug0inwOkuzpUpFCnC6JxXDQtxrg"
                + "RbJHMnHjj8VviJcyVIIquq34FYvipHajqWsBDnSoBFPq5Xj/8VDktxrgREZDtq4ubcFThGXLsJyoTsu+IsYSLOFb9EtiEq"
                + "MQ3i7eP1aMpbxcTpuLiYlEWw22xIXL8aJWanuD2NFmgKKvlzMWEhzpXRIhdX8lrpXzvcIYtcfvkSYkuX4F9UOdTjZysOxg"
                + "AAAABJRU5ErkJggg==";

        em.getTransaction().begin();

        try
        {
            final byte[] decodedImage = DatatypeConverter.parseBase64Binary(base64EncodedImage);

            // Save a new FlagState to the DB
            final FlagStateDO flagState = new FlagStateDO();
            flagState.setName(NAME);
            flagState.setFlag(decodedImage);

            final FlagStateDO savedFlagState = flagRepository.save(flagState);

            assertNotNull(savedFlagState.getId());

            // Now pull the saved FlagState out of the DB and check it matches
            final FlagStateDO retrievedFlagState = flagRepository.findOne(savedFlagState.getId());

            assertEquals(savedFlagState.getId(), retrievedFlagState.getId());
            assertEquals(savedFlagState.getName(), retrievedFlagState.getName());
            assertTrue(retrievedFlagState.getFlag().equals(decodedImage));
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }
}
