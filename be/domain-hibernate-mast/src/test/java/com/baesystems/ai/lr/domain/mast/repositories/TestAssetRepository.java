package com.baesystems.ai.lr.domain.mast.repositories;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.repositories.utils.TestUtils;
import com.baesystems.ai.lr.domain.repositories.BaseRepositoryFactory;

public class TestAssetRepository
{
    // private static final String ITEM_1_NAME = "Item 1";
    // private static final String ITEM_2_NAME = "Item 2. Child of Item 1";
    // private static final String ITEM_3_NAME = "Item 3. Eligible for Child of Item 1";
    // private static final String ITEM_4_NAME = "Item 4. Eligible for child of Item 3";
    //
    // private ItemDO item1;
    // private ItemDO item2;
    // private LinkDO itemType;
    //
    // private static VersionedAssetRepository versionedAssetRepository;
    // private static ItemRepository itemRepository;
    // private static AttributeRepository attributeRepository;
    //
    // private static EntityManager em;
    //
    // /**
    // * Sets up a {@link SimpleJpaRepository} instance.
    // */
    // @BeforeClass
    // public static void setUp()
    // {
    //
    // final EntityManagerFactory factory = Persistence.createEntityManagerFactory(TestUtils.MAST_PERSISTENCE);
    // em = factory.createEntityManager();
    //
    // versionedAssetRepository = new BaseRepositoryFactory(em).getRepository(VersionedAssetRepository.class);
    // itemRepository = new BaseRepositoryFactory(em).getRepository(ItemRepository.class);
    // attributeRepository = new BaseRepositoryFactory(em).getRepository(AttributeRepository.class);
    //
    // TestUtils.setUp(em);
    // }
    private static final String ITEM_1_NAME = "Item 1";
    private static final String ITEM_2_NAME = "Item 2. Child of Item 1";
    private static final String ITEM_3_NAME = "Item 3. Eligible for Child of Item 1";
    private static final String ITEM_4_NAME = "Item 4. Eligible for child of Item 3";

    private ItemDO item1;
    private ItemDO item2;
    private LinkDO itemType;

    private static VersionedAssetRepository versionedAssetRepository;
    private static ItemRepository itemRepository;
    private static AttributeRepository attributeRepository;

    private static EntityManager em;

    /**
     * Sets up a {@link SimpleJpaRepository} instance.
     */
    @BeforeClass
    public static void setUp()
    {

        final EntityManagerFactory factory = Persistence.createEntityManagerFactory(TestUtils.MAST_PERSISTENCE);
        em = factory.createEntityManager();

        versionedAssetRepository = new BaseRepositoryFactory(em).getRepository(VersionedAssetRepository.class);
        itemRepository = new BaseRepositoryFactory(em).getRepository(ItemRepository.class);
        attributeRepository = new BaseRepositoryFactory(em).getRepository(AttributeRepository.class);

        TestUtils.setUp(em);
    }

    //
    // @After
    // public void tearDown()
    // {
    // }
    //
    // @Before
    // public void setUpBeforeTest()
    // {
    // itemType = TestUtils.setUpLinkDO();
    // final List<ItemDO> childItems = new ArrayList<>();
    // item1 = TestUtils.setUpItem(ITEM_1_NAME, itemType);
    // item2 = TestUtils.setUpItem(ITEM_2_NAME, itemType);
    // item1 = itemRepository.save(item1);
    // item2 = itemRepository.save(item2);
    //
    // childItems.add(item2);
    // item1.setItems(childItems);
    // }
    //
    // @Test
    // public void testSaveAsset()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    // asset = versionedAssetRepository.save(asset);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //

    @Test
    public void testSaveAsset()
    {
        em.getTransaction().begin();

        try
        {
            VersionedAssetDO asset = TestUtils.setUpAsset();
            asset = versionedAssetRepository.save(asset);
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    // @Test
    // public void testSaveAssetEditAssetName()
    // {
    // em.getTransaction().begin();
    //
    // final String assetName = "Name At Start.";
    // final String editedName = "Edited Name.";
    //
    // try
    // {
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    // asset.setName(assetName);
    // asset = versionedAssetRepository.save(asset);
    //
    // final VersionedAssetDO returnedAsset = versionedAssetRepository.findOne(asset.getId());
    // assertEquals(asset, returnedAsset);
    // assertFalse(returnedAsset == null);
    // assertTrue(returnedAsset.getId() == asset.getId());
    // assertTrue(returnedAsset.getName() == assetName);
    //
    // returnedAsset.setName(editedName);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(asset.getId());
    // assertTrue(returnedAssetSecondTime.getId() == asset.getId());
    // assertTrue(returnedAssetSecondTime.getName() == editedName);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testSaveAssetWithNestedItems()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // initialCreateAssetWithItemsAndPostSaveChecks();
    //
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testAddItemToAsset()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    //
    // ItemDO item3 = TestUtils.setUpItem(ITEM_3_NAME, itemType);
    // item3 = itemRepository.save(item3);
    // returnedAsset.getAssetItem().getItems().add(item3);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    //
    // basicPostSaveChecks(returnedAssetSecondTime, returnedAsset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getId() == item3.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getName() == ITEM_3_NAME);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // /**
    // * Tests adding additional children to the asset item, 1 is parent of 3 is parent of 4
    // */
    // @Test
    // public void testAddChildItemsToItems()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    //
    // ItemDO item3 = TestUtils.setUpItem(ITEM_3_NAME, itemType);
    // ItemDO item4 = TestUtils.setUpItem(ITEM_4_NAME, itemType);
    //
    // item3 = itemRepository.save(item3);
    // item4 = itemRepository.save(item4);
    //
    // returnedAsset.getAssetItem().getItems().add(item3);
    // returnedAsset.getAssetItem().getItems().get(1).getItems().add(item4);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    // basicPostSaveChecks(returnedAssetSecondTime, returnedAsset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getName() == ITEM_3_NAME);
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getId() == item3.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getItems().get(0).getName() == ITEM_4_NAME);
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(1).getItems().get(0).getId() == item4.getId());
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // /**
    // * Tests changing the name of the primary asset item, item 1.
    // */
    // @Test
    // public void testEditItemsOnAsset()
    // {
    // em.getTransaction().begin();
    //
    // final String item1NewName = "Item 1 new name";
    //
    // try
    // {
    // final VersionedAssetDO asset = createAssetItems();
    //
    // final VersionedAssetDO returnedAsset = versionedAssetRepository.findOne(asset.getId());
    // assertFalse(returnedAsset == null);
    // basicPostSaveChecks(returnedAsset, asset.getId());
    //
    // returnedAsset.getAssetItem().setName(item1NewName);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(asset.getId());
    // assertFalse(returnedAssetSecondTime == null);
    // assertTrue(returnedAssetSecondTime.getId() == asset.getId());
    // assertFalse(returnedAssetSecondTime.getAssetItem().getItems() == null);
    // assertFalse(returnedAssetSecondTime.getAssetItem().getItems().isEmpty());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getId() == item1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getName() == item1NewName);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // /**
    // * Tests changing the name of a child item asset item, item 2.
    // */
    // @Test
    // public void testEditChildItemsOnItem()
    // {
    // em.getTransaction().begin();
    //
    // final String item2NewName = "New Item 2 Name. Still a child of Item 1";
    //
    // try
    // {
    //
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    //
    // returnedAsset.getAssetItem().getItems().get(0).setName(item2NewName);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    //
    // // todo refactor for re-usage
    // assertFalse(returnedAssetSecondTime == null);
    // assertTrue(returnedAssetSecondTime.getId() == returnedAsset.getId());
    // assertFalse(returnedAssetSecondTime.getAssetItem().getItems() == null);
    // assertFalse(returnedAssetSecondTime.getAssetItem().getItems().isEmpty());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getId() == item1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getName() == ITEM_1_NAME);
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getId() == item2.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getName() == item2NewName);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // /**
    // * Tests the deletion of the asset-item item, item 1.
    // */
    // // @Test
    // // public void testDeleteItemFromAsset()
    // // {
    // // em.getTransaction().begin();
    // //
    // // try
    // // {
    // // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    // //
    // // returnedAsset.getAssetItem().setAsset(null);
    // // returnedAsset.setAssetItem(null);
    // //
    // // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    // // assertFalse(returnedAssetSecondTime == null);
    // // assertTrue(returnedAssetSecondTime.getId() == returnedAsset.getId());
    // // assertTrue(returnedAssetSecondTime.getAssetItem() == null);
    // // }
    // // finally
    // // {
    // // em.getTransaction().rollback();
    // // }
    // // }
    //
    // /**
    // * Tests the deletion of a child item, item 2.
    // */
    // @Test
    // public void testDeleteItemFromItem()
    // {
    // em.getTransaction().begin();
    //
    // final int sizeBefore = 1;
    // final int sizeAfter = 0;
    //
    // try
    // {
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    // assertTrue(returnedAsset.getAssetItem().getItems().size() == sizeBefore);
    //
    // returnedAsset.getAssetItem().getItems().remove(0);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getId() == item1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getName() == ITEM_1_NAME);
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().size() == sizeAfter);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testAssetWithNestedItemsAndAttributes()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // final LinkDO itemType = TestUtils.setUpLinkDO();
    //
    // final LinkDO attributeType = TestUtils.setUpLinkDO();
    //
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    //
    // ItemDO item3 = TestUtils.setUpItem(ITEM_2_NAME, itemType);
    // item3 = itemRepository.save(item3);
    //
    // final List<AttributeDO> item1Attributes = new ArrayList<>();
    // final List<AttributeDO> item3Attributes = new ArrayList<>();
    //
    // AttributeDO attribute1 = TestUtils.setUpAttribute(attributeType, item1);
    // AttributeDO attribute3 = TestUtils.setUpAttribute(attributeType, item3);
    // AttributeDO attribute4 = TestUtils.setUpAttribute(attributeType, item3);
    // attribute1 = attributeRepository.save(attribute1);
    // attribute3 = attributeRepository.save(attribute3);
    // attribute4 = attributeRepository.save(attribute4);
    //
    // item1Attributes.add(attribute1);
    // item3Attributes.add(attribute3);
    // item3Attributes.add(attribute4);
    //
    // item1.setAttributes(item1Attributes);
    // item3.setAttributes(item3Attributes);
    //
    // item2.setItems(Collections.singletonList(item3));
    //
    // asset.setAssetItem(item1);
    // asset = versionedAssetRepository.save(asset);
    //
    // final VersionedAssetDO returnedAsset = versionedAssetRepository.findOne(asset.getId());
    // basicPostSaveChecks(returnedAsset, asset.getId());
    //
    // assertTrue(returnedAsset.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId() ==
    // attribute3.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId() ==
    // attribute4.getId());
    //
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testAssetWithNestedItemsAddAttribute()
    // {
    // em.getTransaction().begin();
    //
    // final int sizeBefore = 2;
    // final int sizeAfter = 3;
    //
    // try
    // {
    // final LinkDO itemType = TestUtils.setUpLinkDO();
    // final LinkDO attributeType = TestUtils.setUpLinkDO();
    //
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    // ItemDO item3 = TestUtils.setUpItem(ITEM_2_NAME, itemType);
    // item3 = itemRepository.save(item3);
    //
    // final List<AttributeDO> item1Attributes = new ArrayList<>();
    // final List<AttributeDO> item2Attributes = new ArrayList<>();
    // final List<AttributeDO> item3Attributes = new ArrayList<>();
    //
    // AttributeDO attribute1 = TestUtils.setUpAttribute(attributeType, item1);
    // AttributeDO attribute2 = TestUtils.setUpAttribute(attributeType, item2);
    // AttributeDO attribute3 = TestUtils.setUpAttribute(attributeType, item3);
    // AttributeDO attribute4 = TestUtils.setUpAttribute(attributeType, item3);
    // attribute1 = attributeRepository.save(attribute1);
    // attribute2 = attributeRepository.save(attribute2);
    // attribute3 = attributeRepository.save(attribute3);
    // attribute4 = attributeRepository.save(attribute4);
    //
    // item1Attributes.add(attribute1);
    // item2Attributes.add(attribute2);
    // item3Attributes.add(attribute3);
    // item3Attributes.add(attribute4);
    //
    // item1.setAttributes(item1Attributes);
    // item2.setAttributes(item2Attributes);
    // item3.setAttributes(item3Attributes);
    //
    // item2.setItems(Collections.singletonList(item3));
    // item1.setItems(Collections.singletonList(item2));
    // asset.setAssetItem(item1);
    // asset = versionedAssetRepository.save(asset);
    //
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    // assertTrue(returnedAsset.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getAttributes().get(0).getId() == attribute2.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId() ==
    // attribute3.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId() ==
    // attribute4.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().size() ==
    // sizeBefore);
    //
    // AttributeDO attribute5 = TestUtils.setUpAttribute(attributeType, item3);
    // attribute5 = attributeRepository.save(attribute5);
    //
    // returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().add(attribute5);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(returnedAsset.getId());
    // basicPostSaveChecks(returnedAssetSecondTime, returnedAsset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getAttributes().get(0).getId() ==
    // attribute2.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId()
    // == attribute3
    // .getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId()
    // == attribute4
    // .getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(2).getId()
    // == attribute5
    // .getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().size() ==
    // sizeAfter);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testAssetWithNestedItemsEditAttribute()
    // {
    // em.getTransaction().begin();
    //
    // try
    // {
    // final LinkDO itemType = TestUtils.setUpLinkDO();
    // final LinkDO attributeType = TestUtils.setUpLinkDO();
    //
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    // ItemDO item3 = TestUtils.setUpItem(ITEM_2_NAME, itemType);
    // item3 = itemRepository.save(item3);
    //
    // final List<AttributeDO> item1Attributes = new ArrayList<>();
    // final List<AttributeDO> item2Attributes = new ArrayList<>();
    // final List<AttributeDO> item3Attributes = new ArrayList<>();
    //
    // AttributeDO attribute1 = TestUtils.setUpAttribute(attributeType, item1);
    // AttributeDO attribute2 = TestUtils.setUpAttribute(attributeType, item2);
    // AttributeDO attribute3 = TestUtils.setUpAttribute(attributeType, item3);
    // AttributeDO attribute4 = TestUtils.setUpAttribute(attributeType, item3);
    // attribute1 = attributeRepository.save(attribute1);
    // attribute2 = attributeRepository.save(attribute2);
    // attribute3 = attributeRepository.save(attribute3);
    // attribute4 = attributeRepository.save(attribute4);
    //
    // item1Attributes.add(attribute1);
    // item2Attributes.add(attribute2);
    // item3Attributes.add(attribute3);
    // item3Attributes.add(attribute4);
    //
    // item1.setAttributes(item1Attributes);
    // item2.setAttributes(item2Attributes);
    // item3.setAttributes(item3Attributes);
    //
    // item2.setItems(Collections.singletonList(item3));
    // item1.setItems(Collections.singletonList(item2));
    // asset.setAssetItem(item1);
    // asset = versionedAssetRepository.save(asset);
    //
    // final VersionedAssetDO returnedAsset = initialCreateAssetWithItemsAndPostSaveChecks();
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getId() == item3.getId());
    // assertTrue(returnedAsset.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getAttributes().get(0).getId() == attribute2.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId() ==
    // attribute3.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId() ==
    // attribute4.getId());
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(asset.getId());
    // basicPostSaveChecks(returnedAssetSecondTime, asset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getId() == item3.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getName() == ITEM_2_NAME);
    // assertTrue(returnedAssetSecondTime.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getAttributes().get(0).getId() ==
    // attribute2.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId()
    // == attribute3
    // .getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId()
    // == attribute4
    // .getId());
    //
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // @Test
    // public void testAssetWithNestedItemsDeleteAttribute()
    // {
    // em.getTransaction().begin();
    //
    // final int sizeBefore = 2;
    // final int sizeAfter = 1;
    //
    // try
    // {
    // final LinkDO itemType = TestUtils.setUpLinkDO();
    // final LinkDO attributeType = TestUtils.setUpLinkDO();
    //
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    //
    // ItemDO item3 = TestUtils.setUpItem(ITEM_2_NAME, itemType);
    // item3 = itemRepository.save(item3);
    //
    // final List<AttributeDO> item1Attributes = new ArrayList<>();
    // final List<AttributeDO> item2Attributes = new ArrayList<>();
    // final List<AttributeDO> item3Attributes = new ArrayList<>();
    //
    // AttributeDO attribute1 = TestUtils.setUpAttribute(attributeType, item1);
    // AttributeDO attribute2 = TestUtils.setUpAttribute(attributeType, item2);
    // AttributeDO attribute3 = TestUtils.setUpAttribute(attributeType, item3);
    // AttributeDO attribute4 = TestUtils.setUpAttribute(attributeType, item3);
    // attribute1 = attributeRepository.save(attribute1);
    // attribute2 = attributeRepository.save(attribute2);
    // attribute3 = attributeRepository.save(attribute3);
    // attribute4 = attributeRepository.save(attribute4);
    //
    // item1Attributes.add(attribute1);
    // item2Attributes.add(attribute2);
    // item3Attributes.add(attribute3);
    // item3Attributes.add(attribute4);
    //
    // item1.setAttributes(item1Attributes);
    // item2.setAttributes(item2Attributes);
    // item3.setAttributes(item3Attributes);
    //
    // item2.setItems(Collections.singletonList(item3));
    // item1.setItems(Collections.singletonList(item2));
    // asset.setAssetItem(item1);
    // asset = versionedAssetRepository.save(asset);
    //
    // final VersionedAssetDO returnedAsset = versionedAssetRepository.findOne(asset.getId());
    // basicPostSaveChecks(asset, asset.getId());
    //
    // assertTrue(returnedAsset.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getAttributes().get(0).getId() == attribute2.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId() ==
    // attribute3.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(1).getId() ==
    // attribute4.getId());
    //
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().size() ==
    // sizeBefore);
    //
    // returnedAsset.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().remove(1);
    //
    // final VersionedAssetDO returnedAssetSecondTime = versionedAssetRepository.findOne(asset.getId());
    // basicPostSaveChecks(returnedAssetSecondTime, asset.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getAttributes().get(0).getId() == attribute1.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getAttributes().get(0).getId() ==
    // attribute2.getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().get(0).getId()
    // == attribute3
    // .getId());
    // assertTrue(returnedAssetSecondTime.getAssetItem().getItems().get(0).getItems().get(0).getAttributes().size() ==
    // sizeAfter);
    // }
    // finally
    // {
    // em.getTransaction().rollback();
    // }
    // }
    //
    // /*******************************************************************
    // * Helper methods for setting up Item based tests
    // *******************************************************************/
    //
    // private VersionedAssetDO createAssetItems()
    // {
    //
    // VersionedAssetDO asset = TestUtils.setUpAsset();
    // asset.setAssetItem(item1);
    // asset = versionedAssetRepository.save(asset);
    //
    // return asset;
    // }
    //
    // private VersionedAssetDO initialCreateAssetWithItemsAndPostSaveChecks()
    // {
    // final VersionedAssetDO versionedAssetDO = createAssetItems();
    // final VersionedAssetDO returnedAsset = versionedAssetRepository.findOne(versionedAssetDO.getId());
    // basicPostSaveChecks(returnedAsset, versionedAssetDO.getId());
    // return returnedAsset;
    // }
    //
    // private void basicPostSaveChecks(final VersionedAssetDO returnedAsset, final Long originalAssetId)
    // {
    // assertFalse(returnedAsset == null);
    // assertTrue(returnedAsset.getId() == originalAssetId);
    // assertFalse(returnedAsset.getAssetItem() == null);
    // assertFalse(returnedAsset.getAssetItem().getItems() == null);
    // assertFalse(returnedAsset.getAssetItem().getItems().isEmpty());
    // assertTrue(returnedAsset.getAssetItem().getId() == item1.getId());
    // assertTrue(returnedAsset.getAssetItem().getName() == ITEM_1_NAME);
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getId() == item2.getId());
    // assertTrue(returnedAsset.getAssetItem().getItems().get(0).getName() == ITEM_2_NAME);
    // }
}
