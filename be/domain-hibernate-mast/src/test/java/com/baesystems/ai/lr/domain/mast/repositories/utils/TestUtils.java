package com.baesystems.ai.lr.domain.mast.repositories.utils;

import java.util.Date;

import javax.persistence.EntityManager;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseSurveyorDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeDataTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.AttributeDataTypeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.OfficeRepository;
import com.baesystems.ai.lr.domain.repositories.BaseRepositoryFactory;

public final class TestUtils
{
    private static final String VALUE = "BOOLEAN";
    private static final String NAME = "name";
    private static final Date DATE = new Date();
    private static final Long ID = 9999L;

    private static OfficeRepository officeRepository;
    private static AttributeDataTypeRepository attributeDataTypeRepository;
    public static final String MAST_PERSISTENCE = "testMastPersistence";

    private TestUtils()
    {
        // Utility classes should not have a public/default constructor
    }

    public static void setUp(final EntityManager em)
    {
        officeRepository = new BaseRepositoryFactory(em).getRepository(OfficeRepository.class);
        attributeDataTypeRepository = new BaseRepositoryFactory(em).getRepository(AttributeDataTypeRepository.class);
    }

    public static CaseDO setUpCase()
    {
        final LinkDO caseType = new LinkDO();
        caseType.setId(ID);

        final LinkDO caseStatus = new LinkDO();
        caseStatus.setId(ID);

        final CaseDO aCase = new CaseDO();
        aCase.setCaseType(caseType);
        aCase.setCaseStatus(caseStatus);

        return aCase;
    }

    public static CaseSurveyorDO setUpCaseSurveyor(final CaseDO aCase)
    {
        final CaseSurveyorDO caseLrEmployee = new CaseSurveyorDO();

        final LinkDO surveyorType = new LinkDO();
        surveyorType.setId(ID);

        final LinkDO LrEmployee = new LinkDO();
        LrEmployee.setId(ID);

        caseLrEmployee.setEmployeeRole(surveyorType);
        caseLrEmployee.setSurveyor(LrEmployee);
        caseLrEmployee.setaCase(aCase);

        return caseLrEmployee;
    }

    public static CaseOfficeDO setUpCaseOffice(final CaseDO aCase, final Long roleId)
    {
        final CaseOfficeDO caseOffice = new CaseOfficeDO();

        OfficeDO office = new OfficeDO();
        office.setId(ID);
        office = officeRepository.save(office);

        final LinkDO officeRole = new LinkDO();
        officeRole.setId(roleId);

        caseOffice.setOffice(office);
        caseOffice.setOfficeRole(officeRole);
        caseOffice.setaCase(aCase);

        return caseOffice;
    }

    public static ItemTypeDO setUpItemType()
    {
        final ItemTypeDO itemType = new ItemTypeDO();
        itemType.setId(ID);
        itemType.setName(NAME);
        return itemType;
    }

    public static AttributeTypeDO setUpAttributeType()
    {
        AttributeDataTypeDO dataType = new AttributeDataTypeDO();
        dataType.setId(ID);
        dataType = attributeDataTypeRepository.save(dataType);

        final AttributeTypeDO attributeType = new AttributeTypeDO();
        attributeType.setId(1L);
        attributeType.setName(NAME);
        attributeType.setValueType(dataType);
        return attributeType;
    }

    public static ItemDO setUpItem(final String name, final LinkDO type)
    {
        final ItemDO item = new ItemDO();
        /*
         * item.setName(name); item.setItemType(type); item.setReviewed(true); item.setItems(new ArrayList<ItemDO>());
         */

        return item;
    }

    public static AttributeDO setUpAttribute(final LinkDO type, final VersionedItemDO item)
    {
        final AttributeDO attribute = new AttributeDO();
        attribute.setValue(VALUE);
        attribute.setAttributeType(type);
        attribute.setItem(item);

        return attribute;
    }

    public static VersionedAssetDO setUpAsset()
    {
        final VersionedAssetDO asset = new VersionedAssetDO();
        asset.setName(NAME);
        asset.setBuildDate(DATE);

        return asset;
    }

    public static LinkDO setUpLinkDO()
    {
        final LinkDO link = new LinkDO();
        link.setId(ID);

        return link;
    }

}
