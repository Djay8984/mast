package com.baesystems.ai.lr.domain.mast.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseSurveyorDO;
import com.baesystems.ai.lr.domain.mast.repositories.utils.TestUtils;
import com.baesystems.ai.lr.domain.repositories.BaseRepositoryFactory;

public class TestCaseRepository
{
    private static CaseRepository caseRepository;
    private static CaseSurveyorRepository caseSurveyorRepository;
    private static CaseOfficeRepository caseOfficeRepository;

    private static EntityManager em;

    /**
     * Sets up a {@link SimpleJpaRepository} instance.
     */
    @BeforeClass
    public static void setUp()
    {

        final EntityManagerFactory factory = Persistence.createEntityManagerFactory(TestUtils.MAST_PERSISTENCE);
        em = factory.createEntityManager();

        caseRepository = new BaseRepositoryFactory(em).getRepository(CaseRepository.class);
        caseSurveyorRepository = new BaseRepositoryFactory(em).getRepository(CaseSurveyorRepository.class);
        caseOfficeRepository = new BaseRepositoryFactory(em).getRepository(CaseOfficeRepository.class);

        TestUtils.setUp(em);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testSaveCaseManditoryFieldMissing()
    {
        em.getTransaction().begin();

        try
        {
            try
            {
                final LinkDO caseType = TestUtils.setUpLinkDO();

                CaseDO aCase = new CaseDO();
                aCase.setCaseType(caseType);
                aCase = caseRepository.save(aCase);

                fail("Save should not be allowed without saving manditory fields first.");
            }
            catch (final PersistenceException e)
            {
                // test passes
            }
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveCase()
    {
        em.getTransaction().begin();

        try
        {
            CaseDO aCase = TestUtils.setUpCase();
            aCase = caseRepository.save(aCase);
            assertEquals(aCase, caseRepository.findOne(aCase.getId()));
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveCaseWithAllFields()
    {
        em.getTransaction().begin();

        try
        {
            final AssetDO asset = new AssetDO();
            asset.setId(1L);

            CaseDO aCase = TestUtils.setUpCase();

            final Date tocAcceptanceDate = new Date(0);
            final String contractReferenceNumber = "";

            aCase.setPreEicInspectionStatus(TestUtils.setUpLinkDO());
            aCase.setRiskAssessmentStatus(TestUtils.setUpLinkDO());
            aCase.setAsset(asset);
            aCase.setTocAcceptanceDate(tocAcceptanceDate);
            aCase.setFlagState(TestUtils.setUpLinkDO());
            aCase.setClassMaintenanceStatus(TestUtils.setUpLinkDO());
            aCase.setContractReferenceNumber(contractReferenceNumber);
            aCase = caseRepository.save(aCase);

            // Can't add offices or surveyors until the case exists because they cannot be stored until the case has
            // been stored and they cannot be added until they are stored.

            final List<CaseSurveyorDO> caseSurveyors = new ArrayList<CaseSurveyorDO>();
            CaseSurveyorDO caseSurveyor1 = TestUtils.setUpCaseSurveyor(aCase);
            caseSurveyor1 = caseSurveyorRepository.save(caseSurveyor1);
            caseSurveyors.add(caseSurveyor1);
            CaseSurveyorDO caseSurveyor2 = TestUtils.setUpCaseSurveyor(aCase);
            caseSurveyor2 = caseSurveyorRepository.save(caseSurveyor2);
            caseSurveyors.add(caseSurveyor2);

            final List<CaseOfficeDO> caseOffices = new ArrayList<CaseOfficeDO>();

            CaseOfficeDO caseOffice1 = TestUtils.setUpCaseOffice(aCase, 1L);
            caseOffice1 = caseOfficeRepository.save(caseOffice1);
            caseOffices.add(caseOffice1);
            final CaseOfficeDO caseOffice2 = TestUtils.setUpCaseOffice(aCase, 2L);
            caseOffice1 = caseOfficeRepository.save(caseOffice2);
            caseOffices.add(caseOffice2);

            aCase.setSurveyors(caseSurveyors);
            aCase.setOffices(caseOffices);

            final CaseDO returnedCase = caseRepository.findOne(aCase.getId());
            assertEquals(aCase, returnedCase);
            assertFalse(returnedCase.getCaseType() == null);
            assertFalse(returnedCase.getCaseStatus() == null);
            assertFalse(returnedCase.getPreEicInspectionStatus() == null);
            assertFalse(returnedCase.getRiskAssessmentStatus() == null);
            assertFalse(returnedCase.getAsset() == null);
            assertFalse(returnedCase.getTocAcceptanceDate() == null);
            assertFalse(returnedCase.getFlagState() == null);
            assertFalse(returnedCase.getSurveyors() == null);
            assertFalse(returnedCase.getOffices() == null);
            assertFalse(returnedCase.getClassMaintenanceStatus() == null);
            assertFalse(returnedCase.getContractReferenceNumber() == null);
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }
}
