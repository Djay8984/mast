package com.baesystems.ai.lr.domain.mast.repositories;

import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.baesystems.ai.lr.domain.mast.entities.references.CaseTypeDO;
import com.baesystems.ai.lr.domain.mast.repositories.utils.TestUtils;
import com.baesystems.ai.lr.domain.repositories.BaseRepositoryFactory;

public class TestCaseTypeRepository
{
    private static final Long ID = 1L;
    private static final String NAME = "Name";

    private static CaseTypeRepository caseTypeRepository;
    private static EntityManager em;

    /**
     * Sets up a {@link SimpleJpaRepository} instance.
     */
    @BeforeClass
    public static void setUp()
    {

        final EntityManagerFactory factory = Persistence.createEntityManagerFactory(TestUtils.MAST_PERSISTENCE);
        em = factory.createEntityManager();

        caseTypeRepository = new BaseRepositoryFactory(em).getRepository(CaseTypeRepository.class);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testSaveCaseType()
    {
        em.getTransaction().begin();

        try
        {
            CaseTypeDO caseType = new CaseTypeDO();
            caseType.setId(ID);
            caseType.setName(NAME);
            caseType = caseTypeRepository.save(caseType);
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }

    @Test
    public void testSaveCaseTypeNoId()
    {
        em.getTransaction().begin();

        try
        {
            CaseTypeDO caseType = new CaseTypeDO();
            try
            {
                caseType = caseTypeRepository.save(caseType);
                fail("Should not be able to save case type when ID is NULL");
            }
            catch (final PersistenceException e)
            {
                // test passes
            }
        }
        finally
        {
            em.getTransaction().rollback();
        }
    }
}
