package com.baesystems.ai.lr.domain.mast.entities;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@MappedSuperclass
public abstract class MastDO extends BaseDO
{
    @Column(name = "deleted", nullable = false)
    private Boolean deleted;

    @PrePersist
    @PreUpdate
    public void setDefaults()
    {
        if (deleted == null)
        {
            deleted = false;
        }
    }

    public Boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(final Boolean deleted)
    {
        this.deleted = deleted;
    }
}
