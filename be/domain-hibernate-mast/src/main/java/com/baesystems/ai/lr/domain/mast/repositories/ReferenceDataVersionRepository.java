package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ReferenceDataVersionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReferenceDataVersionRepository extends SpringDataRepository<ReferenceDataVersionDO, Long>
{
    Long TOP_LEVEL_ID = 1L;

    @Query("update ReferenceDataVersionDO set version = version +1 where id = :id")
    @Modifying
    void incrementVersion(@Param("id") Long id);
}
