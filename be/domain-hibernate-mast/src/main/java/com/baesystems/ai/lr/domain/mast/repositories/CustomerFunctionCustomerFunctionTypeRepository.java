package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CustomerFunctionCustomerFunctionTypeRepository extends SpringDataRepository<AssetCustomerFunctionCustomerFunctionTypeDO, Long>
{
    @Query("select customerFunctionLink from AssetCustomerFunctionCustomerFunctionTypeDO customerFunctionLink"
            + " join customerFunctionLink.customer assetCustomer"
            + " join assetCustomer.asset asset"
            + " where asset.id = :assetId")
    Page<AssetCustomerFunctionCustomerFunctionTypeDO> findAll(final Pageable pageable, @Param("assetId") final Long assetId);
}
