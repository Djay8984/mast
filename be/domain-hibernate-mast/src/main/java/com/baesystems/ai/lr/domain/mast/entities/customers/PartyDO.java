package com.baesystems.ai.lr.domain.mast.entities.customers;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;

@Entity
@Table(name = "MAST_CDH_Party")
@Inheritance(strategy = InheritanceType.JOINED)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CDH_Party SET deleted = true WHERE id = ?")
public class PartyDO extends MastDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "cdh_customer_id")
    private Long cdhCustomerId;

    @Column(name = "jde_reference_number", length = 240)
    private String jdeReferenceNumber;

    @Column(name = "imo_number")
    private String imoNumber;

    @Column(name = "phone_number", length = 30)
    private String phoneNumber;

    @Column(name = "fax_number", length = 20)
    private String faxNumber;

    @JoinColumn(name = "office_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private OfficeDO office;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "party", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<PartyAddressDO> addresses;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "party", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<PartyContactDO> contacts;

    public List<PartyContactDO> getContacts()
    {
        return contacts;
    }

    public void setContacts(final List<PartyContactDO> contacts)
    {
        this.contacts = contacts;
    }

    public List<PartyAddressDO> getAddresses()
    {
        return addresses;
    }

    public void setAddresses(final List<PartyAddressDO> addresses)
    {
        this.addresses = addresses;
    }

    public String getJdeReferenceNumber()
    {
        return jdeReferenceNumber;
    }

    public void setJdeReferenceNumber(final String jdeReferenceNumber)
    {
        this.jdeReferenceNumber = jdeReferenceNumber;
    }

    public String getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber()
    {
        return faxNumber;
    }

    public void setFaxNumber(final String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Long getCdhCustomerId()
    {
        return cdhCustomerId;
    }

    public void setCdhCustomerId(final Long cdhCustomerId)
    {
        this.cdhCustomerId = cdhCustomerId;
    }

    public OfficeDO getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDO office)
    {
        this.office = office;
    }
}
