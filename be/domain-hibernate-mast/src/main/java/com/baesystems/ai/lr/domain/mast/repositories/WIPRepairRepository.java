package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPRepairDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WIPRepairRepository extends SpringDataRepository<WIPRepairDO, Long>, LockingRepository<WIPRepairDO>
{
    @Query("select r from WIPRepairDO r"
            + " where defect.id = :defectId")
    Page<WIPRepairDO> findRepairsForDefect(Pageable pageable, @Param("defectId") Long defectId);

    @Query("select r from WIPRepairDO r"
            + " where (coalesce(:wipDefectId) is not null and r.defect.id in (:wipDefectId))"
            + " or (coalesce(:wipCoCId) is not null and r.codicil.id in (:wipDefectId))")
    List<WIPRepairDO> findWIPRepairsForWIPDefectsAndWipCoCs(
            @Param("wipDefectId") List<Long> wipDefectId,
            @Param("wipCoCId") List<Long> wipCoCId);

    @Query("select repair from WIPRepairDO repair where repair.codicil.id = :cocId")
    Page<WIPRepairDO> findAllByCoC(Pageable pageable, @Param("cocId") Long cocId);

    @Query("select count(repair.id) > 0 from WIPRepairDO repair"
            + " where repair.id = :repairId"
            + " and repair.defect.id = :defectId")
    Boolean repairExistsForDefect(@Param("repairId") Long repairId, @Param("defectId") Long defectId);

    @Query("select count(repair.id) > 0 from WIPRepairDO repair"
            + " where repair.id = :repairId"
            + " and repair.codicil.id = :cocId")
    Boolean repairExistsForCoC(@Param("repairId") Long repairId, @Param("cocId") Long cocId);

    @Query("select count(item.id) from WIPDefectDO wipDefect "
            + " join wipDefect.items item "
            + " where wipDefect.id = :wipDefectId "
            + " and item.id not in ("
            + " select item.id from WIPDefectDO wipDefect "
            + "  join wipDefect.items item "
            + "  join item.repairItems repairItems "
            + "  join repairItems.repair repair "
            + "  join repair.repairAction action "
            + "  where wipDefect.id = :wipDefectId and repair.confirmed = true and action.id = :repairActionId "
            + ")")
    Integer countItemsRequiringRepair(@Param("wipDefectId") Long wipDefectId, @Param("repairActionId") Long repairActionId);

    @Query("select count(wipRepair.id) from WIPRepairDO wipRepair"
            + " join wipRepair.defect wipDefect"
            + " join wipRepair.repairAction action"
            + " where wipDefect.id = :wipDefectId"
            + " and action.id = :repairActionId"
            + " and wipRepair.confirmed = true")
    Integer countPermanentRepairs(@Param("wipDefectId") Long defectId, @Param("repairActionId") Long repairActionId);

    /**
     * Counts the number of permanent and temporary repairs associated with a defect.
     */
    @Query("select count(defectRepair.id) from WIPRepairDO defectRepair"
            + " where defectRepair.defect.id = :defectId"
            + " and defectRepair.repairAction.id in (:repairActionIdList)")
    Integer countRepairsForDefect(@Param("defectId") Long defectId, @Param("repairActionIdList") List<Long> repairActionIdList);

    @Query("select count(wipRepair.id) from WIPRepairDO wipRepair"
            + " join wipRepair.codicil wipCodicil"
            + " join wipRepair.repairAction action"
            + " where wipCodicil.id = :wipCocId"
            + " and action.id = :repairActionId"
            + " and wipRepair.confirmed = true")
    Integer countCocPermanentRepairs(@Param("wipCocId") Long cocId, @Param("repairActionId") Long repairActionId);
}
