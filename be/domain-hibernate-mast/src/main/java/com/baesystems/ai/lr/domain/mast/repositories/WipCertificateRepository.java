package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.jobs.WipCertificateDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WipCertificateRepository extends LockingRepository<WipCertificateDO>, SpringDataRepository<WipCertificateDO, Long>
{
    @Query("select wipCertificate from WipCertificateDO wipCertificate"
            + " join wipCertificate.job job"
            + " where job.id = :jobId")
    Page<WipCertificateDO> findAllCertificatesForJob(Pageable pageable, @Param("jobId") final Long jobId);

    @Query("select wipCertificate from WipCertificateDO wipCertificate"
            + " join wipCertificate.job job"
            + " where job.id = :jobId")
    List<WipCertificateDO> findAllCertificatesForJob(@Param("jobId") final Long jobId);

    @Query("select wipCertificate.issueDate from WipCertificateDO wipCertificate"
            + " where wipCertificate.id = :certificateId")
    Date findCertificateIssueDate(@Param("certificateId") final Long certificateId);
}
