package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.ActionableItemDO;

public interface ActionableItemRepository
        extends LockingRepository<ActionableItemDO>, CodicilRepositoryHelper<ActionableItemDO>, CourseOfActionRepositoryHelper<ActionableItemDO>,
        SequenceRepository
{
    @Query("select ai.id from ActionableItemDO ai"
            + " where ai.asset.id = :assetId and ai.id = :actionableItemId")
    Integer actionableItemExistsForAsset(@Param("assetId") Long assetId,
            @Param("actionableItemId") Long actionableItemId);

    @Query("select ai from ActionableItemDO ai"
            + " where ai.asset.id = :assetId"
            + " and (:statusId is null"
            + " or (ai.status.id = :statusId"
            + " and (:open is null"
            + " or (:open = true and ai.imposedDate <= :currentDate)"
            + " or (:open = false and ai.imposedDate >:currentDate)"
            + ")))")
    Page<ActionableItemDO> findActionableItemsForAsset(Pageable pageable, @Param("assetId") Long assetId, @Param("statusId") Long statusId,
            @Param("currentDate") Date currentDate, @Param("open") Boolean isOpen);

    @Query("select new map (ai.template.id as " + TEMPLATE_COLUMN_NAME + ", count(distinct ai.asset) as " + ASSET_COUNT_COLUMN_NAME + ")"
            + " from ActionableItemDO ai"
            + " join ai.asset asset"
            + " join asset.publishedVersion publishedAsset"
            + " where ai.template.id in (:templateIds)"
            + " and publishedAsset.assetLifecycleStatus.id in (:assetLifecycleStatuses)"
            + " group by ai.template.id")
    List<Map<String, Long>> countAssetsWithActionableItemsForTemplate(
            @Param("templateIds") List<Long> templateIds,
            @Param("assetLifecycleStatuses") List<Long> assetLifecycleStatuses);

    @Query("select ai from ActionableItemDO ai"
            + " where ai.id = :actionableItemId")
    ActionableItemDO findActionableItem(@Param("actionableItemId") Long actionableItemId);

    @Query("select ai from ActionableItemDO ai"
            + " where ai.scheduledService.id = :serviceId")
    List<ActionableItemDO> findActionableItemsForAService(@Param("serviceId") Long serviceId);

}
