package com.baesystems.ai.lr.domain.mast.entities.cases;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyDO;

@Entity
@Table(name = "MAST_CASE_Case_Party")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CASE_Case_Party SET deleted = true WHERE id = ?")
public class CaseCustomerDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "party_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private PartyDO customer;

    @JoinColumn(name = "case_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CaseDO aCase;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "party_role_id"))})
    private LinkDO relationship;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<CaseCustomerFunctionCustomerFunctionTypeDO> functions;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public PartyDO getCustomer()
    {
        return customer;
    }

    public void setCustomer(final PartyDO customer)
    {
        this.customer = customer;
    }

    public LinkDO getRelationship()
    {
        return relationship;
    }

    public void setRelationship(final LinkDO relationship)
    {
        this.relationship = relationship;
    }

    public List<CaseCustomerFunctionCustomerFunctionTypeDO> getFunctions()
    {
        return functions;
    }

    public void setFunctions(final List<CaseCustomerFunctionCustomerFunctionTypeDO> functions)
    {
        this.functions = functions;
    }

    public CaseDO getaCase()
    {
        return aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }
}
