package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.CodicilDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CodicilRepository extends SpringDataRepository<CodicilDO, Long>
{
    @Query("select count(codicil.id) > 0 from CodicilDO codicil"
            + " where codicil.assetItem.id in (:itemIds)"
            + " and codicil.status.id in (:statuses)")
    Boolean doAnyItemsHaveCodicils(@Param("itemIds") List<Long> itemIds, @Param("statuses") List<Long> statuses);
}
