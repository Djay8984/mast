package com.baesystems.ai.lr.domain.mast.entities.tasks;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_JOB_WorkItemDictionaryLink")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_WorkItemDictionaryLink SET deleted = true WHERE id = ?")
public class WorkItemDictionaryLinkDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "work_item_id") ) })
    private LinkDO workItem;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "work_item_dictionary_id") ) })
    private LinkDO workItemDictionary;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final LinkDO workItem)
    {
        this.workItem = workItem;
    }

    public LinkDO getWorkItemDictionary()
    {
        return workItemDictionary;
    }

    public void setWorkItemDictionary(final LinkDO workItemDictionary)
    {
        this.workItemDictionary = workItemDictionary;
    }
}
