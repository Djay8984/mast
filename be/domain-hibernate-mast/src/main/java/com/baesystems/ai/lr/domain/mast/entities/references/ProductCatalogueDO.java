package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_ProductCatalogue")
public class ProductCatalogueDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @JoinColumn(name = "product_group_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ProductGroupDO productGroup;

    @Column(name = "display_order", nullable = false)
    private Integer displayOrder;

    @OneToMany(mappedBy = "productCatalogue", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private List<ServiceCatalogueDO> serviceCatalogues;

    @JoinTable(name = "MAST_REF_ServiceCatalogue", joinColumns = {@JoinColumn(name = "product_catalogue_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "service_ruleset_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<ProductRuleSetDO> ruleSets;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public ProductGroupDO getProductGroup()
    {
        return productGroup;
    }

    public void setProductGroup(final ProductGroupDO productGroup)
    {
        this.productGroup = productGroup;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public List<ServiceCatalogueDO> getServiceCatalogues()
    {
        return serviceCatalogues;
    }

    public void setServiceCatalogues(final List<ServiceCatalogueDO> serviceCatalogues)
    {
        this.serviceCatalogues = serviceCatalogues;
    }

    public List<ProductRuleSetDO> getRuleSets()
    {
        return ruleSets;
    }

    public void setRuleSets(final List<ProductRuleSetDO> ruleSets)
    {
        this.ruleSets = ruleSets;
    }
}
