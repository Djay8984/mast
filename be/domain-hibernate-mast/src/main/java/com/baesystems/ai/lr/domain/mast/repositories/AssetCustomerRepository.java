package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

import java.util.List;

public interface AssetCustomerRepository extends SpringDataRepository<AssetCustomerDO, Long>, HardDeleteRepository
{
    @Query("select assetCustomer from AssetCustomerDO assetCustomer"
            + " join assetCustomer.asset asset where asset.id = :assetId")
    Page<AssetCustomerDO> findAll(Pageable pageable, @Param("assetId") Long assetId);

    @Query("select assetCustomer from AssetCustomerDO assetCustomer"
            + " join fetch assetCustomer.asset asset"
            + " where asset.id = :assetId and asset.assetVersionId = :assetVersionId")
    List<AssetCustomerDO> getAssetCustomersForVersionedAsset(@Param("assetId") Long assetId, @Param("assetVersionId") Long assetVersionId);
}
