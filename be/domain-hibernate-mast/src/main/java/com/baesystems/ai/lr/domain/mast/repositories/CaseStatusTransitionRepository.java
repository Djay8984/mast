package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.CaseStatusTransitionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseStatusTransitionRepository extends SpringDataRepository<CaseStatusTransitionDO, Long>
{
    @Query("select transition.toStatusId from CaseStatusTransitionDO transition"
            + " where transition.fromStatusId = :initialStatusId")
    public List<Long> findValidNewCaseStatuses(@Param("initialStatusId") Long initialStatusId);
}
