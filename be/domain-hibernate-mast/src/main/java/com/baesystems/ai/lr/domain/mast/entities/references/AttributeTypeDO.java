package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_AssetAttributeType")
public class AttributeTypeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @JoinColumn(name = "item_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ItemTypeDO itemType;

    @JoinColumn(name = "value_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private AttributeDataTypeDO valueType;

    @Column(name = "description_order")
    private Long descriptionOrder;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "naming_order")
    private Long namingOrder;

    @Column(name = "naming_decoration")
    private String namingDecoration;

    @Column(name = "min_occurance", nullable = false)
    private Integer minOccurs;

    @Column(name = "max_occurance", nullable = false)
    private Integer maxOccurs;

    @Column(name = "transferable", nullable = false)
    private Boolean transferable;

    @OneToMany(mappedBy = "attributeType", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<AllowedAssetAttributeValueDO> allowedValues;

    @JoinColumn(name = "attribute_copy_rule_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private AttributeCopyRuleDO attributeCopyRule;

    @Column(name = "is_default")
    private Boolean isDefault;

    @Column(name = "display_order", nullable = false)
    private Long displayOrder;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public AttributeDataTypeDO getValueType()
    {
        return valueType;
    }

    public void setValueType(final AttributeDataTypeDO valueType)
    {
        this.valueType = valueType;
    }

    public ItemTypeDO getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDO itemType)
    {
        this.itemType = itemType;
    }

    public Long getDescriptionOrder()
    {
        return descriptionOrder;
    }

    public void setDescriptionOrder(final Long descriptionOrder)
    {
        this.descriptionOrder = descriptionOrder;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(final Date endDate)
    {
        this.endDate = endDate;
    }

    public Long getNamingOrder()
    {
        return namingOrder;
    }

    public void setNamingOrder(final Long namingOrder)
    {
        this.namingOrder = namingOrder;
    }

    public String getNamingDecoration()
    {
        return namingDecoration;
    }

    public void setNamingDecoration(final String namingDecoration)
    {
        this.namingDecoration = namingDecoration;
    }

    public Integer getMinOccurs()
    {
        return minOccurs;
    }

    public void setMinOccurs(final Integer minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public Integer getMaxOccurs()
    {
        return maxOccurs;
    }

    public void setMaxOccurs(final Integer maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public Boolean getTransferable()
    {
        return transferable;
    }

    public void setTransferable(final Boolean transferable)
    {
        this.transferable = transferable;
    }

    public List<AllowedAssetAttributeValueDO> getAllowedValues()
    {
        return allowedValues;
    }

    public void setAllowedValues(final List<AllowedAssetAttributeValueDO> allowedValues)
    {
        this.allowedValues = allowedValues;
    }

    public AttributeCopyRuleDO getAttributeCopyRule()
    {
        return attributeCopyRule;
    }

    public void setAttributeCopyRule(final AttributeCopyRuleDO attributeCopyRule)
    {
        this.attributeCopyRule = attributeCopyRule;
    }

    public Boolean getIsDefault()
    {
        return isDefault;
    }

    public void setIsDefault(final Boolean isDefault)
    {
        this.isDefault = isDefault;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }
}
