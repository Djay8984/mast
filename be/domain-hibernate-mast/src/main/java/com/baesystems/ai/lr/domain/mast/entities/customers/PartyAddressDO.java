package com.baesystems.ai.lr.domain.mast.entities.customers;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "MAST_CDH_Party_Address")
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumns({
                        @PrimaryKeyJoinColumn(name = "ADDRESS_ID", referencedColumnName = "ID")
})
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CDH_Party_Address SET deleted = true WHERE id = ?")
public class PartyAddressDO extends AddressDO
{

    @JoinColumn(name = "party_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private PartyDO party;

    public PartyDO getParty()
    {
        return party;
    }

    public void setParty(final PartyDO party)
    {
        this.party = party;
    }
}
