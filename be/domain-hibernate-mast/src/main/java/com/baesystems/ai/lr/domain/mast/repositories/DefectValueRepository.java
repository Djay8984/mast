package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectValueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectValueRepository extends SpringDataRepository<DefectValueDO, Long>
{
    @Query("select count(defectValue) from DefectValueDO defectValue"
            + " where defectValue.id in (:valueIds)")
    Integer countMatchingIds(@Param("valueIds") List<Long> valueIds);
}
