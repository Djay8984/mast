package com.baesystems.ai.lr.domain.mast.entities.tasks;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_JOB_Postponement")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_Postponement SET deleted = true WHERE id = ?")
public class PostponementDO extends AuditedDO
{
    private static final long serialVersionUID = -5047870607023816201L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "postponement_type_id", nullable = false) )})
    private LinkDO postponementType;

    @Column(name = "conditions", length = 500, nullable = false)
    private String conditions;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Date getDate()
    {
        return this.date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    public LinkDO getPostponementType()
    {
        return this.postponementType;
    }

    public void setPostponementType(final LinkDO postponementType)
    {
        this.postponementType = postponementType;
    }

    public String getConditions()
    {
        return this.conditions;
    }

    public void setConditions(final String conditions)
    {
        this.conditions = conditions;
    }

}
