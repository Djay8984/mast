package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;

public interface DefectRepository extends LockingRepository<DefectDO>, FaultRepositoryHelper<DefectDO>, SequenceRepository
{
    @Modifying()
    @Query("update DefectDO defect set defect.defectStatus = :status "
            + " where defect.id = :defectId and defect.defectStatus <> :status ")
    Integer updateStatus(@Param("defectId") Long defectId, @Param("status") DefectStatusDO status);

    @Query("select u from DefectDO u"
            + " join u.asset a"
            + " join u.defectStatus s"
            + " where a.id = :assetId"
            + " and (:statusId is null or s.id = :statusId)")
    Page<DefectDO> findDefectsByAsset(Pageable pageable, @Param("assetId") Long assetId, @Param("statusId") Long statusId);

    @Query("select count(defect.id) > 0 from DefectDO defect"
            + " join defect.items defectItems"
            + " join defectItems.item item"
            + " where item.id in (:itemIds)"
            + " and defect.defectStatus.id in (:statuses)")
    Boolean doAnyItemsHaveOpenDefects(@Param("itemIds") List<Long> itemIds, @Param("statuses") List<Long> statuses);

    @Query("select n from DefectDO n"
            + " where n.asset.id = :assetId")
    Page<DefectDO> findAll(Pageable pageable,
            @Param("assetId") Long assetId);

    @Query("select distinct e from DefectDO e"
            + " left join e.items defectItems"
            + " left join defectItems.item item"
            + " where e.asset.id = :assetId"
            + " and (:searchString is null or"
            + " (e.incidentDescription like :searchString or"
            + " e.title like :searchString))"
            + " and (coalesce(:categoryList) is null or e.defectCategory.id in (:categoryList))"
            + " and (coalesce(:statusList) is null or e.defectStatus.id in (:statusList))"
            + " and (coalesce(:itemIdList) is null or item.id in (:itemIdList))")
    Page<DefectDO> findAll(Pageable pageable,
            @Param("assetId") Long assetId,
            @Param("searchString") String searchString,
            @Param("categoryList") List<Long> categoryList,
            @Param("statusList") List<Long> statusList,
            @Param("itemIdList") List<Long> itemIdList);

    @Query("select count(defect.id) from DefectDO defect"
            + " where defect.asset.id = :assetId"
            + " and defect.id = :defectId")
    Integer defectExistsForAsset(@Param("assetId") Long assetId, @Param("defectId") Long assetNoteId);
}
