package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPersistDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface VersionedItemPersistRepository extends SpringDataRepository<VersionedItemPersistDO, VersionedItemPK>
{
    @Modifying
    @Query("update VersionedItemPersistDO item"
            + " set item.deleted = true"
            + " where item.id in (:itemId)")
    void bulkLogicalDelete(@Param("itemId") List<Long> itemId);

    @Modifying
    @Query("update VersionedItemPersistDO item"
            + " set item.deleted = true"
            + " where item.id in (:itemId)"
            + " AND item.assetVersionId = :versionId")
    void bulkLogicalDelete(@Param("itemId") List<Long> itemIds, @Param("versionId") Long versionId);

    @Modifying
    @Query("update VersionedItemPersistDO item"
            + " set item.deleted = true"
            + " where item.id = :itemId"
            + " AND item.assetId = :assetId"
            + " AND item.assetVersionId = :assetVersionId")
    void logicalDeleteItem(
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId,
            @Param("assetVersionId") Long assetVersionId);

    @Modifying
    @Query("update VersionedItemPersistDO item set item.displayOrder = (item.displayOrder - 1)"
            + " where item.id in (:itemIds)"
            + " and item.displayOrder > :deletedDisplayOrder"
            + " and item.deleted = false")
    void reorganiseDisplayOrdersAfterDelete(@Param("deletedDisplayOrder") Integer deletedDisplayOrder,
            @Param("itemIds") List<Long> itemIds);

    @Query(nativeQuery = true, value = "select v.* from MAST_ASSET_VersionedAssetItem as v"
            + " where v.asset_version_id ="
            + " (select MAX(vv.asset_version_id)"
            + " from MAST_ASSET_VersionedAssetItem as vv"
            + " where v.id = vv.id"
            + " and vv.id = :id"
            + " and vv.asset_id = :assetId"
            + " and vv.asset_version_id <= :version)")
    VersionedItemPersistDO findOne(
            @Param("id") Long id,
            @Param("assetId") Long assetId,
            @Param("version") Long version);

    @Modifying
    @Query("update VersionedItemPersistDO di set di.displayOrder = :displayOrder "
            + " WHERE di.id = :itemId"
            + " AND di.assetId = :assetId"
            + " AND di.assetVersionId = :assetVersionId")
    void updateDisplayOrder(
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId,
            @Param("assetVersionId") Long assetVersionId,
            @Param("displayOrder") Integer displayOrder);

    @Modifying
    @Query("delete VersionedItemPersistDO draftItem"
            + " where draftItem.id in (:draftItemId)")
    void bulkDeleteByItemId(@Param("draftItemId") List<Long> draftItem);

    // This will mark all items in the list of ids that don;t have the current version as unpublised
    @Modifying
    @Query("update VersionedItemPersistDO item"
            + " set item.published = false"
            + " where item.id in (:itemIds)"
            + " and item.assetVersionId <> :assetVersionId")
    void unpublishUpdated(@Param("itemIds") List<Long> itemIds, @Param("assetVersionId") Long assetVersionId);

    // This will mark the items in the list that have the current version as published
    @Modifying
    @Query("update VersionedItemPersistDO item"
            + " set item.published = true"
            + " where item.id in (:itemIds)"
            + " and item.assetVersionId = :assetVersionId")
    void publishUpdates(@Param("itemIds") List<Long> itemIds, @Param("assetVersionId") Long assetVersionId);
}
