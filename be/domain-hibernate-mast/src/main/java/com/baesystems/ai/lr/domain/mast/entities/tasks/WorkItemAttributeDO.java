package com.baesystems.ai.lr.domain.mast.entities.tasks;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "MAST_ASSET_WorkItemAttribute")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_WorkItemAttribute SET deleted = true WHERE id = ?")
public class WorkItemAttributeDO extends BaseWorkItemAttributeDO
{
    @JoinColumn(name = "work_item_id")
    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = {CascadeType.REFRESH})
    private WorkItemDO workItem;

    public WorkItemDO getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final WorkItemDO workItem)
    {
        this.workItem = workItem;
    }
}
