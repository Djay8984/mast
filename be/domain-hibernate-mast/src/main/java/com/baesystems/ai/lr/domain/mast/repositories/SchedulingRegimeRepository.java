package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingRegimeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface SchedulingRegimeRepository extends SpringDataRepository<SchedulingRegimeDO, Long>
{

}
