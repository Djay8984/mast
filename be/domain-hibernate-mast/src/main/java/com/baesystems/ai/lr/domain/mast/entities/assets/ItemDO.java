package com.baesystems.ai.lr.domain.mast.entities.assets;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_ASSET_AssetItem")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetItem SET deleted = true WHERE id = ?")
public class ItemDO extends AuditedDO
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "asset_id")
    private AssetDO asset;

    public void setId(final Long id)
    {
        this.id = id;
    }

    @Override
    public Long getId()
    {
        return this.id;
    }

    public AssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

}
