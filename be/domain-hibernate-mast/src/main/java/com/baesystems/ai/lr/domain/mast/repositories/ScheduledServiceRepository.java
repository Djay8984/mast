package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ScheduledServiceRepository extends LockingRepository<ScheduledServiceDO>, SpringDataRepository<ScheduledServiceDO, Long>
{
    @Query("SELECT u from ScheduledServiceDO u, ServiceCatalogueDO sc"
            + " LEFT JOIN u.postponement p "
            + " WHERE u.serviceCatalogue.id = sc.id"
            + " AND u.asset.id = :assetId"
            + " AND (coalesce(:statusIds) is null or u.serviceCreditStatus.id IN (:statusIds))"
            + " AND (coalesce(:productFamilyIds) is null or sc.productCatalogue.productGroup.productType.id IN (:productFamilyIds))"
            + " AND (:minDueDate is null or (p is not null and p.date >= :minDueDate) or (p is null and u.dueDate >= :minDueDate))"
            + " AND (:maxDueDate is null or (p is not null and p.date <= :maxDueDate) or (p is null and u.dueDate <= :maxDueDate))")
    List<ScheduledServiceDO> findServicesForAsset(@Param("assetId") Long assetId,
            @Param("statusIds") List<Long> statusIds,
            @Param("productFamilyIds") List<Long> productFamilyIds,
            @Param("minDueDate") Date minDueDate,
            @Param("maxDueDate") Date maxDueDate);

    @Query("SELECT u from ScheduledServiceDO u"
            + " WHERE u.asset.id = :assetId"
            + " AND u.deleted = false")
    List<ScheduledServiceDO> findServicesForAsset(@Param("assetId") Long assetId);

    @Query("SELECT u from ScheduledServiceDO u"
            + " WHERE u.asset.id = :assetId"
            + " AND u.serviceCreditStatus.id IN (:statusIds)"
            + " AND u.deleted = false")
    List<ScheduledServiceDO> findServicesForAssetByStatus(@Param("assetId") Long assetId, @Param("statusIds") List<Long> statusIds);

    @Query("SELECT u from ScheduledServiceDO u"
            + " WHERE u.id = :scheduledServiceId"
            + " AND u.asset.id = :assetId"
            + " AND u.deleted = false")
    ScheduledServiceDO findServiceForAsset(@Param("scheduledServiceId") Long scheduledServiceId, @Param("assetId") Long assetId);

    @Query("select service from ScheduledServiceDO service"
            + " where service.serviceCatalogue.id in (:serviceCatalogueIds)"
            + " and service.asset.id = :assetId"
            + " and service.deleted = false")
    List<ScheduledServiceDO> findServicesByServiceCatalogueId(@Param("assetId") Long assetId,
            @Param("serviceCatalogueIds") List<Long> serviceCatalogueIds);

    @Modifying
    @Query("UPDATE ScheduledServiceDO u SET u.deleted = :deleted WHERE u.id = :id")
    void updateDeletedFlag(@Param("id") Long id, @Param("deleted") Boolean isDelete);

    @Query("SELECT count(u) > 0 from ScheduledServiceDO u"
            + " WHERE u.id = :scheduledServiceId"
            + " AND u.asset.id = :assetId"
            + " AND u.deleted = false")
    Boolean existsForAsset(@Param("assetId") Long assetId, @Param("scheduledServiceId") Long scheduledServiceId);

    @Query("SELECT catalogue.code FROM ScheduledServiceDO service, ServiceCatalogueDO catalogue"
            + " WHERE service.serviceCatalogue.id = catalogue.id"
            + " AND service.id = :scheduledServiceId")
    String findServiceCodeFromScheduledService(@Param("scheduledServiceId") Long scheduledServiceId);

    @Query("select count(u) > 0 from ScheduledServiceDO u"
            + " WHERE u.assetItem.id in (:itemIds)"
            + " and u.serviceCreditStatus.id in (:statuses)"
            + " AND u.deleted = false")
    boolean doAnyItemsHaveOpenServices(@Param("itemIds") List<Long> itemIdsToCheck, @Param("statuses") List<Long> open);
}
