package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_ASSET_Party_PartyRole")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_Party_PartyRole SET deleted = true WHERE id = ?")
public class AssetCustomerFunctionCustomerFunctionTypeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "party_id", nullable = false, insertable = true, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private AssetCustomerDO customer;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "party_role_id"))})
    private LinkDO customerFunction;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public AssetCustomerDO getCustomer()
    {
        return customer;
    }

    public void setCustomer(final AssetCustomerDO customer)
    {
        this.customer = customer;
    }

    public LinkDO getCustomerFunction()
    {
        return customerFunction;
    }

    public void setCustomerFunction(final LinkDO customerFunction)
    {
        this.customerFunction = customerFunction;
    }
}
