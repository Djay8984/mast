package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectValueDO;

@Entity
@Table(name = "MAST_DEFECT_WIP_Defect_DefectValue")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_WIP_Defect_DefectValue SET deleted = true WHERE id = ?")
public class WIPDefectDefectValueDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "other", length = 50)
    private String otherDetails;

    @JoinColumn(name = "defect_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private WIPDefectDO defect;

    @JoinColumn(name = "defect_value_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private DefectValueDO defectValue;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parent;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getOtherDetails()
    {
        return otherDetails;
    }

    public void setOtherDetails(final String otherDetails)
    {
        this.otherDetails = otherDetails;
    }

    public WIPDefectDO getDefect()
    {
        return this.defect;
    }

    public void setDefect(final WIPDefectDO defect)
    {
        this.defect = defect;
    }

    public DefectValueDO getDefectValue()
    {
        return this.defectValue;
    }

    public void setDefectValue(final DefectValueDO defectValue)
    {
        this.defectValue = defectValue;
    }

    public LinkDO getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }
}
