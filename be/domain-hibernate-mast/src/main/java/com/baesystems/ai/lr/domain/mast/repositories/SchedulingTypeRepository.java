package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface SchedulingTypeRepository extends SpringDataRepository<SchedulingTypeDO, Long>
{
    @Query("select schedulingType from SchedulingTypeDO schedulingType,"
            + "ServiceCatalogueDO serviceCatalogue"
            + " where schedulingType.id = serviceCatalogue.schedulingType.id"
            + " and serviceCatalogue.id = :serviceCatalogueId)")
    List<SchedulingTypeDO> findAllSchedulingTypesForServiceCatalogue(@Param("serviceCatalogueId") Long serviceCatalogueId);
}
