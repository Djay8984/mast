package com.baesystems.ai.lr.domain.mast.entities.services;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_ASSET_AssetProduct")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetProduct SET deleted = true WHERE id = ?")
public class ProductDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "product_catalogue_id"))})
    private LinkDO productCatalogue;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "scheduling_regime_id"))})
    private LinkDO schedulingRegime;

    @JoinColumn(name = "asset_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private AssetDO asset;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getProductCatalogue()
    {
        return productCatalogue;
    }

    public void setProductCatalogue(final LinkDO productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public LinkDO getSchedulingRegime()
    {
        return schedulingRegime;
    }

    public void setSchedulingRegime(final LinkDO schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    public AssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }
}
