package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseCustomerFunctionCustomerFunctionTypeRepository extends SpringDataRepository<CaseCustomerFunctionCustomerFunctionTypeDO, Long>
{
}
