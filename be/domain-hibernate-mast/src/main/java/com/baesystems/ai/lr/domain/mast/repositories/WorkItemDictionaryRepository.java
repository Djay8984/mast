package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemDictionaryDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemDictionaryRepository extends SpringDataRepository<WorkItemDictionaryDO, Long>
{

}
