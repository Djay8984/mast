package com.baesystems.ai.lr.domain.mast.entities;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * The PK for MAST_ASSET_VersionedAsset is id + asset_version_id
 * <p>
 * This is for entities that are persisted / merged alongside the main VersionedAsset via cascade, usually
 * set up by back-referencing in DaoUtils
 * <p>
 * Please note the required column names below, also see LinkedVersionDO if a Link-like
 * association is required rather than a full Asset
 */
@MappedSuperclass
public abstract class MutableVersionedDO extends AuditedDO
{
    @JoinColumns({
            @JoinColumn(name = "asset_id", referencedColumnName = "id"),
            @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id")
    })
    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = {CascadeType.REFRESH})
    private VersionedAssetDO asset;

    public VersionedAssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final VersionedAssetDO asset)
    {
        this.asset = asset;
    }

}
