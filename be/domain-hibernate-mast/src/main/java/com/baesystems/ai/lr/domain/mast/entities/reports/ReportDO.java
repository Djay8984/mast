package com.baesystems.ai.lr.domain.mast.entities.reports;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;

@Entity
@Table(name = "MAST_JOB_Report")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_Report SET deleted = true WHERE id = ?")
public class ReportDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "reference_code", length = 25)
    private String referenceCode;

    @Column(name = "report_version")
    private Long reportVersion;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "type_id", nullable = false) )})
    private LinkDO reportType;

    @JoinColumn(name = "job_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private JobDO job;

    @Lob
    @Column(name = "generated_content", columnDefinition = "MEDIUMTEXT")
    private String content;

    @Column(name = "recommendation ", length = 2000)
    private String classRecommendation;

    @Column(name = "second_recommendation ", length = 2000)
    private String secondClassRecommendation;

    @Column(name = "issue_date")
    private Date issueDate;

    @Column(name = "authorisation_date")
    private Date authorisationDate;

    @Column(name = "notes", length = 2000)
    private String notes;

    @Column(name = "confidential_notes", length = 2000)
    private String confidentialNotes;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "issued_by_id") )})
    private LinkDO issuedBy;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "authorised_by_id") )})
    private LinkDO authorisedBy;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "endorsed_by_id") )})
    private LinkDO endorsedBy;

    @Column(name = "approved")
    private Boolean approved;

    @Column(name = "rejection_comments", length = 1000)
    private String rejectionComments;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public Long getReportVersion()
    {
        return reportVersion;
    }

    public void setReportVersion(final Long reportVersion)
    {
        this.reportVersion = reportVersion;
    }

    public LinkDO getReportType()
    {
        return reportType;
    }

    public void setReportType(final LinkDO reportType)
    {
        this.reportType = reportType;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public String getClassRecommendation()
    {
        return classRecommendation;
    }

    public void setClassRecommendation(final String classRecommendation)
    {
        this.classRecommendation = classRecommendation;
    }

    public String getSecondClassRecommendation()
    {
        return secondClassRecommendation;
    }

    public void setSecondClassRecommendation(final String secondClassRecommendation)
    {
        this.secondClassRecommendation = secondClassRecommendation;
    }

    public Date getIssueDate()
    {
        return issueDate;
    }

    public void setIssueDate(final Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public Date getAuthorisationDate()
    {
        return authorisationDate;
    }

    public void setAuthorisationDate(final Date authorisationDate)
    {
        this.authorisationDate = authorisationDate;
    }

    public String getNotes()
    {
        return notes;
    }

    public void setNotes(final String notes)
    {
        this.notes = notes;
    }

    public String getConfidentialNotes()
    {
        return confidentialNotes;
    }

    public void setConfidentialNotes(final String confidentialNotes)
    {
        this.confidentialNotes = confidentialNotes;
    }

    public LinkDO getIssuedBy()
    {
        return issuedBy;
    }

    public void setIssuedBy(final LinkDO issuedBy)
    {
        this.issuedBy = issuedBy;
    }

    public LinkDO getAuthorisedBy()
    {
        return authorisedBy;
    }

    public void setAuthorisedBy(final LinkDO authorisedBy)
    {
        this.authorisedBy = authorisedBy;
    }

    public LinkDO getEndorsedBy()
    {
        return endorsedBy;
    }

    public void setEndorsedBy(final LinkDO endorsedBy)
    {
        this.endorsedBy = endorsedBy;
    }

    public Boolean getApproved()
    {
        return approved;
    }

    public void setApproved(final Boolean approved)
    {
        this.approved = approved;
    }

    public String getRejectionComments()
    {
        return rejectionComments;
    }

    public void setRejectionComments(final String rejectionComments)
    {
        this.rejectionComments = rejectionComments;
    }
}
