package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;

public interface WIPActionableItemRepository
        extends LockingRepository<WIPActionableItemDO>, WIPCodicilRepositoryHelper<WIPActionableItemDO>,
        CourseOfActionRepositoryHelper<WIPActionableItemDO>, WIPSequenceRepository
{
    @Query("select wipAi.id from WIPActionableItemDO wipAi"
            + " where wipAi.job.id = :jobId and wipAi.id = :actionableItemId")
    Long actionableItemExistsForJob(@Param("jobId") Long jobId, @Param("actionableItemId") Long actionableItemId);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " where wipAi.id = :actionableItemId")
    WIPActionableItemDO findActionableItem(@Param("actionableItemId") Long actionableItemId);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " where wipAi.job.id = :jobId"
            + " and (wipAi.status.id = :statusId or :statusId is null)")
    Page<WIPActionableItemDO> findActionableItemsForJob(Pageable pageable,
            @Param("jobId") Long jobId, @Param("statusId") Long statusId);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " where wipAi.job.id = :jobId"
            + " and (coalesce(:statusList) is null"
            + " or (wipAi.status.id in (:statusList)"
            + " and (:open is null or wipAi.status.id <> :openStatus"
            + " or (:open = true and wipAi.imposedDate <= :currentDate)"
            + " or (:open = false and wipAi.imposedDate >:currentDate)"
            + ")))")
    List<WIPActionableItemDO> findActionableItemsForJob(@Param("jobId") Long jobId, @Param("statusList") List<Long> statusList,
            @Param("currentDate") Date currentDate, @Param("open") Boolean isOpen, @Param("openStatus") Long openStatusId);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " where wipAi.job.id = :jobId"
            + " and wipAi.actionTakenDate is not null")
    List<WIPActionableItemDO> findActionableItemsForReportContent(@Param("jobId") Long jobId);

    @Query("select ai from WIPActionableItemDO ai"
            + " where ai.job.id = :jobId"
            + " and ai.updatedBy = :employeeName"
            + " and day(ai.actionTakenDate) = day(:lastUpdatedDate)"
            + " and month(ai.actionTakenDate) = month(:lastUpdatedDate)"
            + " and year(ai.actionTakenDate) = year(:lastUpdatedDate)")
    List<WIPActionableItemDO> findActionableItemsByJob(@Param("jobId") Long jobId, @Param("employeeName") String employeeName,
            @Param("lastUpdatedDate") Date lastUpdatedDate);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " where wipAi.job.id = :jobId")
    List<WIPActionableItemDO> findActionableItemsForJob(@Param("jobId") Long jobId);

    @Query("select wipAi from WIPActionableItemDO wipAi"
            + " left outer join wipAi.defect defect"
            + " where wipAi.job.id = :jobId"
            + " and (:defectId is null or defect.id = :defectId)")
    Page<WIPActionableItemDO> findActionableItemsForJobDefect(Pageable pageable,
            @Param("jobId") Long jobId, @Param("defectId") Long defectId);

    @Modifying
    @Query("update WIPActionableItemDO wipAi set wipAi.jobScopeConfirmed = :scopeConfirmed"
            + " where wipAi.job.id = :jobId"
            + " and (coalesce(:statusList) is null or wipAi.status.id in (:statusList))")
    Integer updateScopeConfirmed(@Param("jobId") Long jobId,
            @Param("scopeConfirmed") Boolean scopeConfirmed,
            @Param("statusList") List<Long> statusList);
}
