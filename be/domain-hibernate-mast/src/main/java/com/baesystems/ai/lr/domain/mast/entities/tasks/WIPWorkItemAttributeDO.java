package com.baesystems.ai.lr.domain.mast.entities.tasks;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table (name = "MAST_JOB_WorkItemAttribute")
@Where (clause = "deleted = false")
@SQLDelete (sql = "UPDATE MAST_JOB_WorkItemAttribute SET deleted = true WHERE id = ?")
public class WIPWorkItemAttributeDO extends BaseWorkItemAttributeDO
{
    @JoinColumn (name = "work_item_id", nullable = false)
    @ManyToOne (fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WIPWorkItemDO workItem;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parent;

    public WIPWorkItemDO getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final WIPWorkItemDO workItem)
    {
        this.workItem = workItem;
    }

    public LinkDO getParent()
    {
        return parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }
}
