package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.DeficiencyDO;

public interface DeficiencyRepository extends LockingRepository<DeficiencyDO>, FaultRepositoryHelper<DeficiencyDO>
{
    @Query("select n from DeficiencyDO n"
            + " where n.asset.id = :assetId")
    Page<DeficiencyDO> findAll(Pageable pageable,
            @Param("assetId") Long assetId);
}
