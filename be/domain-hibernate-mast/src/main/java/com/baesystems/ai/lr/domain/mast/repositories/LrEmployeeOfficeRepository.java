package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeOfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface LrEmployeeOfficeRepository extends SpringDataRepository<LrEmployeeOfficeDO, Long>
{

}
