package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.SocietyRuleSetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface SocietyRuleSetRepository extends SpringDataRepository<SocietyRuleSetDO, Long>
{
    @Query("select societyRuleset from SocietyRuleSetDO societyRuleset"
            + " where (societyRuleset.category = :category or :category is null)"
            + " and (societyRuleset.isLrRuleset = :isLrRuleset or :isLrRuleset is null)")
    List<SocietyRuleSetDO> findAll(@Param("category") String category, @Param("isLrRuleset") Boolean isLrRuleset);
}
