package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_AllowedWorkItemAttributeValue")
public class AllowedWorkItemAttributeValueDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "value", length = 100)
    private String value;

    @JoinColumn(name = "work_item_conditional_attribute_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WorkItemConditionalAttributeDO workItemAttribute;

    @Column(name = "is_follow_on_trigger")
    private Boolean isFollowOnTrigger;

    @Column(name = "display_order")
    private Integer displayOrder;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public WorkItemConditionalAttributeDO getWorkItemAttribute()
    {
        return workItemAttribute;
    }

    public void setWorkItemAttribute(final WorkItemConditionalAttributeDO workItemAttribute)
    {
        this.workItemAttribute = workItemAttribute;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public Boolean getIsFollowOnTrigger()
    {
        return isFollowOnTrigger;
    }

    public void setIsFollowOnTrigger(final Boolean isFollowOnTrigger)
    {
        this.isFollowOnTrigger = isFollowOnTrigger;
    }
}
