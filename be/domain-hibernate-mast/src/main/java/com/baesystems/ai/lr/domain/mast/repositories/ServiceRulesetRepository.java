package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceRulesetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceRulesetRepository extends SpringDataRepository<ServiceRulesetDO, Long>
{

}
