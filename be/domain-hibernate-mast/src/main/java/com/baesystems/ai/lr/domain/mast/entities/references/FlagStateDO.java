package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_FlagAdministration")
public class FlagStateDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Lob
    @Column(name = "flag_image", columnDefinition = "")
    private byte[] flag;

    @Column(name = "flag_code", length = 3)
    private String flagCode;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "MAST_REF_PortFlagAdministration", joinColumns = @JoinColumn(name = "flag_state_id") , inverseJoinColumns = @JoinColumn(name = "port_id") )
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 300)
    private List<PortOfRegistryDO> ports;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public List<PortOfRegistryDO> getPorts()
    {
        return ports;
    }

    public void setPorts(final List<PortOfRegistryDO> ports)
    {
        this.ports = ports;
    }

    public byte[] getFlag()
    {
        return flag;
    }

    public void setFlag(final byte[] flag)
    {
        this.flag = flag;
    }

    public String getFlagCode()
    {
        return flagCode;
    }

    public void setFlagCode(final String flagCode)
    {
        this.flagCode = flagCode;
    }
}
