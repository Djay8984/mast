package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CustomerRelationshipDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CustomerRelationshipRepository extends SpringDataRepository<CustomerRelationshipDO, Long>
{

}
