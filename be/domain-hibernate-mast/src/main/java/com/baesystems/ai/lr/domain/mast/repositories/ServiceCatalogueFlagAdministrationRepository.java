package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueFlagAdministrationDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceCatalogueFlagAdministrationRepository extends SpringDataRepository<ServiceCatalogueDO, Long>
{
    @Query("select count(id) from ServiceCatalogueFlagAdministrationDO "
            + "where flagStateDO.id = :flagStateID "
            + "and serviceCatalogueDO.id = :serviceCatalogID")
    Integer countByServiceCatalogAndFlag(@Param("serviceCatalogID") Long serviceCatalogID,
            @Param("flagStateID") Long flagStateID);

    @Query("select scfaDO from ServiceCatalogueFlagAdministrationDO scfaDO "
            + "where flagStateDO.id = :flagStateID "
            + "and serviceCatalogueDO.id = :serviceCatalogID")
    List<ServiceCatalogueFlagAdministrationDO> findByServiceCatalogAndFlag(
            @Param("serviceCatalogID") Long serviceCatalogID,
            @Param("flagStateID") Long flagStateID);
}
