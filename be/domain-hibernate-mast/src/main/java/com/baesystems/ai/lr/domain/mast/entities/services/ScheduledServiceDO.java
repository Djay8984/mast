package com.baesystems.ai.lr.domain.mast.entities.services;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.PostponementDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;

@Entity
@Table(name = "MAST_ASSET_ScheduledService")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_ScheduledService SET deleted = true WHERE id = ?")
public class ScheduledServiceDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_service_id") )})
    private LinkDO parentService;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_id", nullable = false) )})
    private LinkDO asset;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_item_id", nullable = false) )})
    private LinkDO assetItem;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "service_catalogue_id") )})
    private LinkDO serviceCatalogue;

    @Column(name = "cycle_periodicity")
    private Integer cyclePeriodicity;

    @JoinColumn(name = "postponement_id")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private PostponementDO postponement;

    @Column(name = "assigned_date")
    private Date assignedDate;

    @Column(name = "assigned_date_manual")
    private Date assignedDateManual;

    @Column(name = "due_date", nullable = false)
    private Date dueDate;

    @Column(name = "due_date_manual", nullable = false)
    private Date dueDateManual;

    @Column(name = "lower_range_date")
    private Date lowerRangeDate;

    @Column(name = "lower_range_date_manual")
    private Date lowerRangeDateManual;

    @Column(name = "upper_range_date")
    private Date upperRangeDate;

    @Column(name = "upper_range_date_manual")
    private Date upperRangeDateManual;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "service_status_id") )})
    private LinkDO serviceStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "service_credit_status_id") )})
    private LinkDO serviceCreditStatus;

    @Column(name = "completion_date")
    private Date completionDate;

    @Column(name = "provisional_date_flag", nullable = false)
    private Boolean provisionalDates;

    @Column(name = "crediting_date")
    private Date creditingDate;

    @Column(name = "service_cycle_number")
    private Integer serviceCycleNumber;

    @Column(name = "service_occurence_number")
    private Integer serviceOccurenceNumber;

    @Column(name = "last_credited_job", length = 9)
    private String lastCreditedJob;

    @Column(name = "last_partheld_job", length = 9)
    private String lastPartheldJob;

    @Column(name = "provisional")
    private Boolean provisional;

    @Column(name = "continuous")
    private Boolean continuous;

    @Column(name = "active")
    private Boolean active;

    @OneToMany(mappedBy = "service", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @Where(clause = "deleted = false")
    private List<AttachmentLinkDO> attachments;

    @OneToMany(mappedBy = "scheduledService", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH, CascadeType.REMOVE}, orphanRemoval = false)
    @Where(clause = "deleted = false")
    private List<WorkItemDO> workItems;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkDO asset)
    {
        this.asset = asset;
    }

    public LinkDO getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkDO assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkDO getServiceCatalogue()
    {
        return this.serviceCatalogue;
    }

    public void setServiceCatalogue(final LinkDO serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    public Integer getCyclePeriodicity()
    {
        return this.cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getAssignedDateManual()
    {
        return this.assignedDateManual;
    }

    public void setAssignedDateManual(final Date assignedDateManual)
    {
        this.assignedDateManual = assignedDateManual;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getDueDateManual()
    {
        return this.dueDateManual;
    }

    public void setDueDateManual(final Date dueDateManual)
    {
        this.dueDateManual = dueDateManual;
    }

    public Date getLowerRangeDate()
    {
        return this.lowerRangeDate;
    }

    public void setLowerRangeDate(final Date lowerRangeDate)
    {
        this.lowerRangeDate = lowerRangeDate;
    }

    public Date getLowerRangeDateManual()
    {
        return this.lowerRangeDateManual;
    }

    public void setLowerRangeDateManual(final Date lowerRangeDateManual)
    {
        this.lowerRangeDateManual = lowerRangeDateManual;
    }

    public Date getUpperRangeDate()
    {
        return this.upperRangeDate;
    }

    public void setUpperRangeDate(final Date upperRangeDate)
    {
        this.upperRangeDate = upperRangeDate;
    }

    public Date getUpperRangeDateManual()
    {
        return this.upperRangeDateManual;
    }

    public void setUpperRangeDateManual(final Date upperRangeDateManual)
    {
        this.upperRangeDateManual = upperRangeDateManual;
    }

    public LinkDO getServiceStatus()
    {
        return this.serviceStatus;
    }

    public void setServiceStatus(final LinkDO serviceStatus)
    {
        this.serviceStatus = serviceStatus;
    }

    public LinkDO getServiceCreditStatus()
    {
        return this.serviceCreditStatus;
    }

    public void setServiceCreditStatus(final LinkDO serviceCreditStatus)
    {
        this.serviceCreditStatus = serviceCreditStatus;
    }

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Boolean getProvisionalDates()
    {
        return this.provisionalDates;
    }

    public void setProvisionalDates(final Boolean provisionalDates)
    {
        this.provisionalDates = provisionalDates;
    }

    public Date getCreditingDate()
    {
        return this.creditingDate;
    }

    public void setCreditingDate(final Date creditingDate)
    {
        this.creditingDate = creditingDate;
    }

    public Integer getServiceCycleNumber()
    {
        return this.serviceCycleNumber;
    }

    public void setServiceCycleNumber(final Integer serviceCycleNumber)
    {
        this.serviceCycleNumber = serviceCycleNumber;
    }

    public Integer getServiceOccurenceNumber()
    {
        return this.serviceOccurenceNumber;
    }

    public void setServiceOccurenceNumber(final Integer serviceOccurenceNumber)
    {
        this.serviceOccurenceNumber = serviceOccurenceNumber;
    }

    public String getLastCreditedJob()
    {
        return this.lastCreditedJob;
    }

    public void setLastCreditedJob(final String lastCreditedJob)
    {
        this.lastCreditedJob = lastCreditedJob;
    }

    public String getLastPartheldJob()
    {
        return this.lastPartheldJob;
    }

    public void setLastPartheldJob(final String lastPartheldJob)
    {
        this.lastPartheldJob = lastPartheldJob;
    }

    public Boolean getProvisional()
    {
        return this.provisional;
    }

    public void setProvisional(final Boolean provisional)
    {
        this.provisional = provisional;
    }

    public Boolean getContinuous()
    {
        return this.continuous;
    }

    public void setContinuous(final Boolean continuous)
    {
        this.continuous = continuous;
    }

    public List<AttachmentLinkDO> getAttachments()
    {
        return this.attachments;
    }

    public void setAttachments(final List<AttachmentLinkDO> attachments)
    {
        this.attachments = attachments;
    }

    public LinkDO getParentService()
    {
        return this.parentService;
    }

    public void setParentService(final LinkDO parentService)
    {
        this.parentService = parentService;
    }

    public List<WorkItemDO> getWorkItems()
    {
        return this.workItems;
    }

    public void setWorkItems(final List<WorkItemDO> workItems)
    {
        this.workItems = workItems;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(final Boolean active)
    {
        this.active = active;
    }

    public PostponementDO getPostponement()
    {
        return this.postponement;
    }

    public void setPostponement(final PostponementDO postponement)
    {
        this.postponement = postponement;
    }
}
