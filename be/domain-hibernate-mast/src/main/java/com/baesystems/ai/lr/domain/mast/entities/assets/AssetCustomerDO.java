package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.MutableVersionedDO;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyDO;

@Entity
@Table(name = "MAST_ASSET_Asset_Party")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_Asset_Party SET deleted = true WHERE id = ?")
public class AssetCustomerDO extends MutableVersionedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "party_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private PartyDO customer;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "party_role_id"))})
    private LinkDO relationship;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<AssetCustomerFunctionCustomerFunctionTypeDO> functions;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public PartyDO getCustomer()
    {
        return customer;
    }

    public void setCustomer(final PartyDO customer)
    {
        this.customer = customer;
    }

    public LinkDO getRelationship()
    {
        return relationship;
    }

    public void setRelationship(final LinkDO relationship)
    {
        this.relationship = relationship;
    }

    public List<AssetCustomerFunctionCustomerFunctionTypeDO> getFunctions()
    {
        return functions;
    }

    public void setFunctions(final List<AssetCustomerFunctionCustomerFunctionTypeDO> functions)
    {
        this.functions = functions;
    }
}
