package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.repository.NoRepositoryBean;

import com.baesystems.ai.lr.domain.mast.entities.defects.BaseFaultDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

@NoRepositoryBean
public interface FaultRepositoryHelper<T extends BaseFaultDO> extends SpringDataRepository<T, Long>
{

}
