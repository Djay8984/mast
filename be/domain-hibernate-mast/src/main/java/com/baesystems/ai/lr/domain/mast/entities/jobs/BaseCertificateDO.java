package com.baesystems.ai.lr.domain.mast.entities.jobs;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@MappedSuperclass
public class BaseCertificateDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "certificate_number", nullable = false, length = 25)
    private String certificateNumber;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "certificate_status_id") )})
    private LinkDO certificateStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "certificate_type_id") )})
    private LinkDO certificateType;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "certificate_action_id") )})
    private LinkDO certificateAction;

    @Column(name = "expiry_date")
    private Date expiryDate;

    @Column(name = "extended_date")
    private Date extendedDate;

    @Column(name = "issue_date")
    private Date issueDate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "lr_office_id") )})
    private LinkDO office;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "lr_employee_id") )})
    private LinkDO employee;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCertificateNumber()
    {
        return this.certificateNumber;
    }

    public void setCertificateNumber(final String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public LinkDO getCertificateStatus()
    {
        return this.certificateStatus;
    }

    public void setCertificateStatus(final LinkDO certificateStatus)
    {
        this.certificateStatus = certificateStatus;
    }

    public LinkDO getCertificateType()
    {
        return this.certificateType;
    }

    public void setCertificateType(final LinkDO certificateType)
    {
        this.certificateType = certificateType;
    }

    public LinkDO getCertificateAction()
    {
        return this.certificateAction;
    }

    public void setCertificateAction(final LinkDO certificateAction)
    {
        this.certificateAction = certificateAction;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setExpiryDate(final Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Date getExtendedDate()
    {
        return this.extendedDate;
    }

    public void setExtendedDate(final Date extendedDate)
    {
        this.extendedDate = extendedDate;
    }

    public Date getIssueDate()
    {
        return this.issueDate;
    }

    public void setIssueDate(final Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public LinkDO getOffice()
    {
        return this.office;
    }

    public void setOffice(final LinkDO office)
    {
        this.office = office;
    }

    public LinkDO getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkDO employee)
    {
        this.employee = employee;
    }
}
