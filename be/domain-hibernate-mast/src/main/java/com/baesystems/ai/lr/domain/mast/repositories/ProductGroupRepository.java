package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ProductGroupDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ProductGroupRepository extends SpringDataRepository<ProductGroupDO, Long>
{

}
