package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;

public interface WorkItemRepository extends SpringDataRepository<WorkItemDO, Long>
{
    @Query("select workItem from WorkItemDO workItem"
            + " where (coalesce(:#{#query.itemId}) is null or workItem.assetItem.id in (:#{#query.itemId}))"
            + " and (coalesce(:#{#query.scheduledServiceId}) is null or workItem.scheduledService.id in (:#{#query.scheduledServiceId}))")
    Page<WorkItemDO> findAll(Pageable pageable, @Param("query") WorkItemQueryDto query);

    @Query("select workItem from WorkItemDO workItem"
            + " where workItem.scheduledService.asset.id = :assetId")
    List<WorkItemDO> findAllForAsset(@Param("assetId") Long assetId);

    @Query("select workItem from WorkItemDO workItem"
            + " where workItem.scheduledService.id = :serviceId")
    List<WorkItemDO> getTasksForService(@Param("serviceId") Long serviceId);
}
