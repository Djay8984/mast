package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceCatalogueRepository extends SpringDataRepository<ServiceCatalogueDO, Long>
{
    @Query("select serviceCatalogue from ServiceCatalogueDO serviceCatalogue"
            + " join serviceCatalogue.surveyType surveyType"
            + " where serviceCatalogue.categoryLetter = :categoryLetter"
            + " and surveyType.name = :surveyType")
    List<ServiceCatalogueDO> findByCategoryAndSurveyType(@Param("categoryLetter") String categoryLetter, @Param("surveyType") String surveyType);

    @Query("select serviceCatalogue from ServiceCatalogueDO serviceCatalogue"
            + " where serviceCatalogue.code = :catalogueCode")
    List<ServiceCatalogueDO> findByCode(@Param("catalogueCode") String catalogueCode);

    @Query("select serviceCatalogue from ServiceCatalogueDO serviceCatalogue"
            + " join serviceCatalogue.surveyType surveyType"
            + " where serviceCatalogue.code = :categoryCode"
            + " and surveyType.name = :surveyType")
    List<ServiceCatalogueDO> findByCodeAndSurveyType(@Param("categoryCode") String categoryCode, @Param("surveyType") String surveyType);

    @Query("select serviceCatalogue.id from ServiceCatalogueDO serviceCatalogue "
            + "where serviceCatalogue.code = :surveyCode")
    List<Long> findIDsBySurveyCode(@Param("surveyCode") String surveyCode);

    @Query("select sc.id from ServiceCatalogueDO sc"
            + " where sc.productCatalogue.id = :productCatalogueId")
    List<Long> findServiceCatalogueIdsForProductCatalogue(@Param("productCatalogueId") Long productCatalogueId);
}
