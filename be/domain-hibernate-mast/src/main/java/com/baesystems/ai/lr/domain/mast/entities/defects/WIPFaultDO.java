package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;

@Entity
@Table(name = "MAST_FAULT_WIP_Fault")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "fault_type_id", discriminatorType = DiscriminatorType.INTEGER)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_WIP_Fault SET deleted = true WHERE id = ?")
public class WIPFaultDO extends BaseFaultDO
{
    @JoinColumn(name = "job_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private JobDO job;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parent;


    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public LinkDO getParent()
    {
        return parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }
}
