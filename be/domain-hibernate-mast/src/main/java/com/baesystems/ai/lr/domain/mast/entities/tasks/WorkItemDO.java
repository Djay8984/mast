package com.baesystems.ai.lr.domain.mast.entities.tasks;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;

@Entity
@Table(name = "MAST_ASSET_WorkItem")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_WorkItem SET deleted = true WHERE id = ?")
public class WorkItemDO extends BaseWorkItemDO
{
    @JoinColumn(name = "scheduled_service_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ScheduledServiceDO scheduledService;

    @OneToMany(mappedBy = "workItem", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<WorkItemAttributeDO> attributes;

    public ScheduledServiceDO getScheduledService()
    {
        return scheduledService;
    }

    public void setScheduledService(final ScheduledServiceDO scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    public List<WorkItemAttributeDO> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(final List<WorkItemAttributeDO> attributes)
    {
        this.attributes = attributes;
    }
}
