package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;

@Embeddable
public class IHSAssetLinkDO extends MastDO
{
    @Column
    private Long id;

    public void setId(final Long id)
    {
        this.id = id;
    }

    @Override
    public Long getId()
    {
        return this.id;
    }

}
