package com.baesystems.ai.lr.domain.mast.entities.tasks;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttributeDataTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemConditionalAttributeDO;

@MappedSuperclass
public class BaseWorkItemAttributeDO extends AuditedDO
{
    @Id
    @Column (name = "id")
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "description", length = 300)
    private String description;

    @Column(name = "attribute_value_date")
    @Temporal(TemporalType.DATE)
    private Date attributeValueDate;

    @Column(name = "attribute_value_float")
    private Float attributeValueFloat;

    @Column(name = "attribute_value_string", length = 500)
    private String attributeValueString;

    @JoinColumn(name = "attribute_data_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private AttributeDataTypeDO attributeDataType;

    @JoinColumn(name = "work_item_conditional_attribute_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WorkItemConditionalAttributeDO workItemConditionalAttribute;

    @Embedded
    @AttributeOverrides ({@AttributeOverride (name = "id", column = @Column (name = "followed_on_from_attribute_id"))})
    private LinkDO followedOnFrom;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Date getAttributeValueDate()
    {
        return attributeValueDate;
    }

    public void setAttributeValueDate(final Date attributeValueDate)
    {
        this.attributeValueDate = attributeValueDate;
    }

    public Float getAttributeValueFloat()
    {
        return attributeValueFloat;
    }

    public void setAttributeValueFloat(final Float attributeValueFloat)
    {
        this.attributeValueFloat = attributeValueFloat;
    }

    public String getAttributeValueString()
    {
        return attributeValueString;
    }

    public void setAttributeValueString(final String attributeValueString)
    {
        this.attributeValueString = attributeValueString;
    }

    public AttributeDataTypeDO getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final AttributeDataTypeDO attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public WorkItemConditionalAttributeDO getWorkItemConditionalAttribute()
    {
        return workItemConditionalAttribute;
    }

    public void setWorkItemConditionalAttribute(final WorkItemConditionalAttributeDO workItemConditionalAttribute)
    {
        this.workItemConditionalAttribute = workItemConditionalAttribute;
    }

    public LinkDO getFollowedOnFrom()
    {
        return followedOnFrom;
    }

    public void setFollowedOnFrom(final LinkDO followedOnFrom)
    {
        this.followedOnFrom = followedOnFrom;
    }
}
