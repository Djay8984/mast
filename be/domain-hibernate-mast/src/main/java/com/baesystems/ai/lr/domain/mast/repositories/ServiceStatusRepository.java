package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceStatusRepository extends SpringDataRepository<ServiceStatusDO, Long>
{

}
