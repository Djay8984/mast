package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.VersionedDO;

@MappedSuperclass
@IdClass(VersionedItemPK.class)
public abstract class BaseItemDO extends VersionedDO
{
    private static final long serialVersionUID = -109456508078503635L;

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Id
    @Column(name = "asset_id", nullable = false, updatable = false)
    private Long assetId;

    @Id
    @Column(name = "asset_version_id", nullable = false, updatable = false)
    private Long assetVersionId;

    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @Column(name = "reviewed", nullable = false)
    private Boolean reviewed;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "item_type_id", nullable = false) )})
    private LinkDO itemType;

    @Column(name = "display_order")
    private Integer displayOrder;

    @Column(name = "decommissioned")
    private Boolean decommissioned;

    @Column(name = "published", nullable = false)
    private Boolean published;

    @Override
    @PrePersist
    @PreUpdate
    public void setDefaults()
    {
        super.setDefaults();
        if (this.published == null)
        {
            this.published = false;
        }
    }

    public LinkDO getItemType()
    {
        return this.itemType;
    }

    public void setItemType(final LinkDO itemType)
    {
        this.itemType = itemType;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Boolean getReviewed()
    {
        return this.reviewed;
    }

    public void setReviewed(final Boolean reviewed)
    {
        this.reviewed = reviewed;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getAssetVersionId()
    {
        return this.assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    public Boolean getDecommissioned()
    {
        return this.decommissioned;
    }

    public void setDecommissioned(final Boolean decommissioned)
    {
        this.decommissioned = decommissioned;
    }

    public Boolean getPublished()
    {
        return this.published;
    }

    public void setPublished(final Boolean published)
    {
        this.published = published;
    }
}
