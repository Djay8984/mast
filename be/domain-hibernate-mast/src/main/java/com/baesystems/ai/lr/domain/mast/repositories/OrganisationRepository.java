package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.customers.OrganisationDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface OrganisationRepository extends SpringDataRepository<OrganisationDO, Long>
{
    @Query("select u from OrganisationDO u where u.name like :search or u.id like :search")
    Page<OrganisationDO> findAll(Pageable pageable, @Param("search") String search);
}
