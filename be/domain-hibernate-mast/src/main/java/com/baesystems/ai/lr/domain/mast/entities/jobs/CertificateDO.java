package com.baesystems.ai.lr.domain.mast.entities.jobs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "MAST_JOB_Certificate")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_Certificate SET deleted = true WHERE id = ?")
public class CertificateDO extends BaseCertificateDO
{
    @Column(name = "approved_flag", nullable = false)
    private Boolean approved;

    public Boolean getApproved()
    {
        return approved;
    }

    public void setApproved(final Boolean approved)
    {
        this.approved = approved;
    }

}
