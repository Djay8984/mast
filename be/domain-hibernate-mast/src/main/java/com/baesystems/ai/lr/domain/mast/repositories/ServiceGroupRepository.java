package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceGroupDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceGroupRepository extends SpringDataRepository<ServiceGroupDO, Long>
{

}
