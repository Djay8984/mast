package com.baesystems.ai.lr.domain.mast.entities.cases;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;

@Entity
@Table(name = "MAST_CASE_CaseResource")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CASE_CaseResource SET deleted = true WHERE id = ?")
public class CaseSurveyorDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "employee_id", nullable = false))})
    private LinkDO surveyor;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "employee_role_id", nullable = false))})
    private LinkDO employeeRole;

    @JoinColumn(name = "case_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CaseDO aCase;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getSurveyor()
    {
        return surveyor;
    }

    public void setSurveyor(final LinkDO surveyor)
    {
        this.surveyor = surveyor;
    }

    public CaseDO getaCase()
    {
        return aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }

    public LinkDO getEmployeeRole()
    {
        return employeeRole;
    }

    public void setEmployeeRole(final LinkDO employeeRole)
    {
        this.employeeRole = employeeRole;
    }
}
