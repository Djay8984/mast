package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@DiscriminatorValue(value = "2")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_WIP_Fault SET deleted = true WHERE id = ?")
public class WIPDeficiencyDO extends WIPFaultDO
{

}
