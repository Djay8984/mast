package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.jobs.JobResourceDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface JobResourceRepository extends SpringDataRepository<JobResourceDO, Long>
{
}
