package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.JobStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface JobStatusRepository extends SpringDataRepository<JobStatusDO, Long>
{

    JobStatusDO findByName(String status);
}
