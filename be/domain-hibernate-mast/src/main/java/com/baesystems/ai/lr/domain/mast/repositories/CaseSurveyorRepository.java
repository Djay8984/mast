package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseSurveyorDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseSurveyorRepository extends SpringDataRepository<CaseSurveyorDO, Long>
{

}
