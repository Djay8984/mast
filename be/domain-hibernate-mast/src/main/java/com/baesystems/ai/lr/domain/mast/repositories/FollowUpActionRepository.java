package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.jobs.FollowUpActionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface FollowUpActionRepository extends LockingRepository<FollowUpActionDO>, SpringDataRepository<FollowUpActionDO, Long>
{
    @Query("select n from FollowUpActionDO n"
            + " where n.report.job.id = :jobId")
    Page<FollowUpActionDO> findFollowUpActionByJob(Pageable pageable, @Param("jobId") Long jobId);

    @Query("select count(fua.id) from FollowUpActionDO fua"
            + " where fua.report.job.id = :jobId and fua.id = :followUpActionId")
    Integer followUpActionExistsForJob(@Param("jobId") Long jobId, @Param("followUpActionId") Long followUpActionId);
}
