package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.BaseItemDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

@NoRepositoryBean
public interface BaseItemRepository<T extends BaseItemDO> extends SpringDataRepository<T, VersionedItemPK>
{
    @Query("select count(item.id) > 0 from #{#entityName} item join item.asset asset"
            + " where item.id = :itemId"
            + " and asset.id = :assetId"
            + " and item.deleted = false")
    Boolean itemExistsForAsset(@Param("itemId") Long itemId, @Param("assetId") Long assetId);


//    todo reimpl this, used for deletion
//    @Query("select distinct toItem.id from #{#entityName} item"
//            + " join item.related relationship"
//            + " join relationship.toItem toItem"
//            + " join relationship.fromItem fromItem"
//            + " where fromItem.id in (:parentItemId)"
//            + " and relationship.type.id = :relationshipTypeId"
//            + " and item.deleted = false"
//            + " and relationship.deleted = false")
//    List<Long> findChildIds(@Param("parentItemId") List<Long> parentItemId, @Param("relationshipTypeId") Long relationshipTypeId);

    @Query("select distinct item.itemType.id from #{#entityName} item"
            + " where item.id = :itemId")
    Long getItemType(@Param("itemId") Long itemId);

    @Query("select item from #{#entityName} item where item.deleted = false")
    Page<T> findAllUndeleted(Pageable pageable);
}
