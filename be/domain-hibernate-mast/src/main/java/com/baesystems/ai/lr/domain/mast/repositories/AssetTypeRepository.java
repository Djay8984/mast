package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AssetTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssetTypeRepository extends SpringDataRepository<AssetTypeDO, Long>
{

}
