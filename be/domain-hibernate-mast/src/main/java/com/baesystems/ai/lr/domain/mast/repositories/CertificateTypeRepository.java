package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.CertificateTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CertificateTypeRepository extends SpringDataRepository<CertificateTypeDO, Long>
{
    @Query("select serviceCatalogue.cyclePeriodicity from CertificateTypeDO certificateType"
            + " join certificateType.serviceCatalogue serviceCatalogue"
            + " where certificateType.id = :certificateTypeId")
    Integer findCyclePeriodicityOfCertificateType(@Param("certificateTypeId") final Long certificateTypeId);
}
