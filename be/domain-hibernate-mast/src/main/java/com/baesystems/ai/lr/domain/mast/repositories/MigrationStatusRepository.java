package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.MigrationStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MigrationStatusRepository extends SpringDataRepository<MigrationStatusDO, Long>
{

}
