package com.baesystems.ai.lr.domain.mast.entities.customers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CountryDO;

@Entity
@Table(name = "MAST_CDH_Address")
@Inheritance(strategy = InheritanceType.JOINED)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CDH_Address SET deleted = true WHERE id = ?")
public class AddressDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "address_line1", length = 100, nullable = false)
    private String addressLine1;

    @Column(name = "address_line2", length = 100)
    private String addressLine2;

    @Column(name = "address_line3", length = 100)
    private String addressLine3;

    @Column(name = "town", length = 30, nullable = false)
    private String town;

    @Column(name = "city", length = 100)
    private String city;

    @Column(name = "postcode", length = 10, nullable = false)
    private String postcode;

    @JoinColumn(name = "country_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private CountryDO country;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(final String town)
    {
        this.town = town;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(final String city)
    {
        this.city = city;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    public CountryDO getCountry()
    {
        return country;
    }

    public void setCountry(final CountryDO country)
    {
        this.country = country;
    }
}
