package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseOfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseOfficeRepository extends SpringDataRepository<CaseOfficeDO, Long>
{

}
