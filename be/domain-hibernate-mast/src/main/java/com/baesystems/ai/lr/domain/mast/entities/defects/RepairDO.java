package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MaterialDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairTypeDO;

@Entity
@Table(name = "MAST_DEFECT_Repair")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_Repair SET deleted = true WHERE id = ?")
public class RepairDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "prompt_thorough_flag")
    private Boolean promptThoroughRepair;

    @Column(name = "description", length = 2000, nullable = false)
    private String description;

    @Column(name = "repair_type_description", length = 5000)
    private String repairTypeDescription;

    @Column(name = "narrative", length = 1000)
    private String narrative;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinTable(name = "MAST_DEFECT_Repair_RepairType", joinColumns = @JoinColumn(name = "repair_id"), inverseJoinColumns = @JoinColumn(name = "repair_type_id"))
    @Where(clause = "deleted = false")
    private List<RepairTypeDO> repairTypes;

    @JoinColumn(name = "action_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private RepairActionDO repairAction;

    @JoinColumn(name = "defect_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectDO defect;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinTable(name = "MAST_DEFECT_Repair_Material", joinColumns = @JoinColumn(name = "repair_id"), inverseJoinColumns = @JoinColumn(name = "material_used_id"))
    @Where(clause = "deleted = false")
    private List<MaterialDO> materialsUsed;

    @OneToMany(mappedBy = "repair", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<RepairItemDO> repairs;

    @Column(name = "confirmed", nullable = false)
    private Boolean confirmed;

    @JoinColumn(name = "codicil_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private CoCDO codicil;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Boolean getPromptThoroughRepair()
    {
        return promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public List<RepairTypeDO> getRepairTypes()
    {
        return repairTypes;
    }

    public void setRepairTypes(final List<RepairTypeDO> repairTypes)
    {
        this.repairTypes = repairTypes;
    }

    public RepairActionDO getRepairAction()
    {
        return repairAction;
    }

    public void setRepairAction(final RepairActionDO repairAction)
    {
        this.repairAction = repairAction;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getRepairTypeDescription()
    {
        return repairTypeDescription;
    }

    public void setRepairTypeDescription(final String repairTypeDescription)
    {
        this.repairTypeDescription = repairTypeDescription;
    }

    public String getNarrative()
    {
        return narrative;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public List<MaterialDO> getMaterialsUsed()
    {
        return materialsUsed;
    }

    public void setMaterialsUsed(final List<MaterialDO> materialsUsed)
    {
        this.materialsUsed = materialsUsed;
    }

    public List<RepairItemDO> getRepairs()
    {
        return repairs;
    }

    public void setRepairs(final List<RepairItemDO> repairs)
    {
        this.repairs = repairs;
    }

    public DefectDO getDefect()
    {
        return defect;
    }

    public void setDefect(final DefectDO defect)
    {
        this.defect = defect;
    }

    public Boolean getConfirmed()
    {
        return confirmed;
    }

    public void setConfirmed(final Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    public CoCDO getCodicil()
    {
        return codicil;
    }

    public void setCodicil(final CoCDO codicil)
    {
        this.codicil = codicil;
    }

}
