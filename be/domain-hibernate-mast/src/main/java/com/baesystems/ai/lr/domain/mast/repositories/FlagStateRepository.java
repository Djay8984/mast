package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.FlagStateDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface FlagStateRepository extends SpringDataRepository<FlagStateDO, Long>
{
    @Query("select u from FlagStateDO u where u.name like :search")
    Page<FlagStateDO> findAll(Pageable pageSpecification,
            @Param("search") String search);

    @Query("select fa.id from FlagStateDO fa where upper(fa.name) = upper(:name))")
    List<Long> findIDsByName(@Param("name") String name);
}
