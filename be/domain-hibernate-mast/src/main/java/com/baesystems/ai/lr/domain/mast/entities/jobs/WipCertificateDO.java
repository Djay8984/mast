package com.baesystems.ai.lr.domain.mast.entities.jobs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_JOB_CertificateWIP")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_CertificateWIP SET deleted = true WHERE id = ?")
public class WipCertificateDO extends BaseCertificateDO
{
    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parentCertificate;

    @JoinColumn(name = "job_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private JobDO job;

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public LinkDO getParentCertificate()
    {
        return parentCertificate;
    }

    public void setParentCertificate(final LinkDO parentCertificate)
    {
        this.parentCertificate = parentCertificate;
    }
}
