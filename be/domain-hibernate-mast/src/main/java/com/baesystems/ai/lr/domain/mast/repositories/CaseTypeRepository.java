package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CaseTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseTypeRepository extends SpringDataRepository<CaseTypeDO, Long>
{

}
