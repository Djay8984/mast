package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectCategoryDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;

@MappedSuperclass
public class BaseFaultDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "internal_sequence_number", updatable = false)
    private Integer sequenceNumber;

    @Column(name = "first_frame_number")
    private Integer firstFrameNumber;

    @Column(name = "last_frame_number")
    private Integer lastFrameNumber;

    @Column(name = "incident_date")
    private Date incidentDate;

    @Column(name = "description", length = 2000)
    private String incidentDescription;

    @Column(name = "title", length = 50, nullable = false)
    private String title;

    @Column(name = "prompt_thorough_flag")
    private Boolean promptThoroughRepair;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "confidentiality_type_id"))})
    private LinkDO confidentialityType;

    @JoinColumn(name = "defect_category_id", nullable = false, insertable = true, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private DefectCategoryDO defectCategory;

    @JoinColumn(name = "defect_status_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private DefectStatusDO defectStatus;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Integer getFirstFrameNumber()
    {
        return firstFrameNumber;
    }

    public void setFirstFrameNumber(final Integer firstFrameNumber)
    {
        this.firstFrameNumber = firstFrameNumber;
    }

    public Integer getLastFrameNumber()
    {
        return lastFrameNumber;
    }

    public void setLastFrameNumber(final Integer lastFrameNumber)
    {
        this.lastFrameNumber = lastFrameNumber;
    }

    public Date getIncidentDate()
    {
        return incidentDate;
    }

    public void setIncidentDate(final Date incidentDate)
    {
        this.incidentDate = incidentDate;
    }

    public String getIncidentDescription()
    {
        return incidentDescription;
    }

    public void setIncidentDescription(final String incidentDescription)
    {
        this.incidentDescription = incidentDescription;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public Boolean getPromptThoroughRepair()
    {
        return promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public LinkDO getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final LinkDO confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public Integer getSequenceNumber()
    {
        return sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public DefectCategoryDO getDefectCategory()
    {
        return defectCategory;
    }

    public void setDefectCategory(final DefectCategoryDO defectCategory)
    {
        this.defectCategory = defectCategory;
    }

    public DefectStatusDO getDefectStatus()
    {
        return defectStatus;
    }

    public void setDefectStatus(final DefectStatusDO defectStatus)
    {
        this.defectStatus = defectStatus;
    }
}
