package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemActionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemActionRepository extends SpringDataRepository<WorkItemActionDO, Long>
{
    @Query("select action from WorkItemActionDO action"
            + " where lower(action.referenceCode) = :referenceCode")
    WorkItemActionDO findByReferenceCode(@Param("referenceCode") String referenceCode);
}
