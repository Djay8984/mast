package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ItemTypeRepository extends SpringDataRepository<ItemTypeDO, Long>
{
    @Query("select itemType.name from ItemTypeDO itemType"
            + " where itemType.id = :itemTypeId")
    String findName(@Param("itemTypeId") Long itemTypeId);

    @Query("select it from ItemTypeDO it"
            + " where it.id = (select itr.toItemType.id from ItemTypeRelationshipDO itr"
            + " where itr.fromItemType.id is null"
            + " and itr.deleted = false"
            + " and itr.assetCategoryId = :assetCategoryId)")
    ItemTypeDO getRootItemForAssetCategory(@Param("assetCategoryId") final Integer assetCategoryId);

    @Query("select itemType.id from ItemTypeDO itemType"
            + " where itemType.name like :name")
    List<Long> findIdsWithNameLike(@Param("name") String name);
}
