package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AssetLifecycleStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssetLifecycleStatusRepository extends SpringDataRepository<AssetLifecycleStatusDO, Long> {

}
