package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MilestoneStatusRepository extends SpringDataRepository<MilestoneStatusDO, Long>
{
    @Query("select milestone.name from MilestoneStatusDO milestone"
            + " where milestone.id = :milestoneId")
    String findName(@Param("milestoneId") Long milestoneId);
}
