package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.AttributeTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AttributeTypeRepository extends SpringDataRepository<AttributeTypeDO, Long>
{
    @Query("select attributeType from AttributeTypeDO attributeType"
            + " where attributeType.itemType.id = :itemTypeId"
            + " and (attributeType.minOccurs > 0 or attributeType.isDefault = true)")
    List<AttributeTypeDO> getMandatoryAttributeTypesForItemType(@Param("itemTypeId") Long itemTypeId);

    @Query("select attrType.id from AttributeTypeDO attrType"
            + " where attrType.itemType.id = :itemTypeId"
            + " and attrType.minOccurs > 0")
    List<Long> getMandatoryAttributeTypeIdsForAnItemTypeId(@Param("itemTypeId") Long itemTypeId);

    @Query("select attributeType.id from AttributeTypeDO attributeType"
            + " where attributeType.name like :name")
    List<Long> findIdsWithNameLike(@Param("name") String name);
}
