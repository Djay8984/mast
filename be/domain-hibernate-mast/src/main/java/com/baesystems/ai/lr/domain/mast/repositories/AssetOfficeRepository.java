package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetOfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssetOfficeRepository extends SpringDataRepository<AssetOfficeDO, Long>, HardDeleteRepository
{
    @Query("select office from AssetOfficeDO office"
            + " join fetch office.asset asset"
            + " where asset.id = :assetId and asset.assetVersionId = :assetVersionId")
    List<AssetOfficeDO> getAssetOfficeForVersionedAsset(@Param("assetId") Long assetId, @Param("assetVersionId") Long assetVersionId);
}
