package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "MAST_ASSET_AssetItemAttribute")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetItemAttribute SET deleted = true WHERE id = ?")
public class AttributePersistDO extends BaseAttributeDO
{

    @Column(name = "item_id")
    private Long itemId;

    //Mutable
    @Column(name = "asset_id")
    private Long assetId;

    @Column(name = "asset_version_id")
    private Long assetVersionId;

    public Long getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(final Long itemId)
    {
        this.itemId = itemId;
    }

    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }
}
