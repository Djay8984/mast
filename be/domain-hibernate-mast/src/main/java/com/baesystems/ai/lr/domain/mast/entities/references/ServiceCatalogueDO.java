package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_ServiceCatalogue")
public class ServiceCatalogueDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "code", length = 10, nullable = false)
    private String code;

    @JoinColumn(name = "service_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceTypeDO serviceType;

    @JoinColumn(name = "survey_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SurveyTypeDO surveyType;

    @JoinColumn(name = "product_catalogue_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ProductCatalogueDO productCatalogue;

    @JoinColumn(name = "defect_category_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private DefectCategoryDO defectCategory;

    @JoinColumn(name = "service_ruleset_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceRulesetDO serviceRuleset;

    @JoinColumn(name = "service_group_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceGroupDO serviceGroup;

    @JoinColumn(name = "scheduling_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private SchedulingTypeDO schedulingType;

    @Column(name = "continuous_indicator")
    private Boolean continuousIndicator;

    @JoinColumn(name = "harmonisation_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private HarmonisationTypeDO harmonisationType;

    @Column(name = "display_order", nullable = false)
    private Integer displayOrder;

    @Column(name = "cycle_periodicity", nullable = false)
    private Integer cyclePeriodicity;

    @Column(name = "cycle_periodicity_editable")
    private Boolean cyclePeriodicityEditable;

    @Column(name = "lower_range_date_offset_months")
    private Integer lowerRangeDateOffsetMonths;

    @Column(name = "upper_range_date_offset_months")
    private Integer upperRangeDateOffsetMonths;

    @Column(name = "description", length = 45)
    private String description;

    @Column(name = "category_letter", length = 1)
    private String categoryLetter;

    @Column(name = "multiple_indicator")
    private Boolean multipleIndicator;

    @JoinColumn(name = "scheduling_regime_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SchedulingRegimeDO schedulingRegime;

    @JoinTable(name = "MAST_REF_ServiceCatalogue_FlagAdministration", joinColumns = {@JoinColumn(name = "service_catalogue_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "flag_administration_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<FlagStateDO> flags;

    @OneToMany(mappedBy = "fromServiceCatalogue", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<ServiceCatalogueRelationshipDO> relationships;

    @OneToMany(mappedBy = "toServiceCatalogue", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<ServiceCatalogueRelationshipDO> relationshipsRelatedByToServiceCatalogue;

    @PostLoad
    public void updatePostLoad()
    { // Add any service catalogues that are mapped the opposite way.
        if (this.relationships != null && this.relationshipsRelatedByToServiceCatalogue != null)
        {
            this.relationships.addAll(this.relationshipsRelatedByToServiceCatalogue);
        }
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public ServiceTypeDO getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(final ServiceTypeDO serviceType)
    {
        this.serviceType = serviceType;
    }

    public SurveyTypeDO getSurveyType()
    {
        return surveyType;
    }

    public void setSurveyType(final SurveyTypeDO surveyType)
    {
        this.surveyType = surveyType;
    }

    public ProductCatalogueDO getProductCatalogue()
    {
        return productCatalogue;
    }

    public void setProductCatalogue(final ProductCatalogueDO productCatalogue)
    {
        this.productCatalogue = productCatalogue;
    }

    public DefectCategoryDO getDefectCategory()
    {
        return defectCategory;
    }

    public void setDefectCategory(final DefectCategoryDO defectCategory)
    {
        this.defectCategory = defectCategory;
    }

    public ServiceRulesetDO getServiceRuleset()
    {
        return serviceRuleset;
    }

    public void setServiceRuleset(final ServiceRulesetDO serviceRuleset)
    {
        this.serviceRuleset = serviceRuleset;
    }

    public ServiceGroupDO getServiceGroup()
    {
        return serviceGroup;
    }

    public void setServiceGroup(final ServiceGroupDO serviceGroup)
    {
        this.serviceGroup = serviceGroup;
    }

    public SchedulingTypeDO getSchedulingType()
    {
        return schedulingType;
    }

    public void setSchedulingType(final SchedulingTypeDO schedulingType)
    {
        this.schedulingType = schedulingType;
    }

    public Boolean getContinuousIndicator()
    {
        return continuousIndicator;
    }

    public void setContinuousIndicator(final Boolean continuousIndicator)
    {
        this.continuousIndicator = continuousIndicator;
    }

    public HarmonisationTypeDO getHarmonisationType()
    {
        return harmonisationType;
    }

    public void setHarmonisationType(final HarmonisationTypeDO harmonisationType)
    {
        this.harmonisationType = harmonisationType;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Integer getCyclePeriodicity()
    {
        return cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Boolean getCyclePeriodicityEditable()
    {
        return cyclePeriodicityEditable;
    }

    public void setCyclePeriodicityEditable(final Boolean cyclePeriodicityEditable)
    {
        this.cyclePeriodicityEditable = cyclePeriodicityEditable;
    }

    public Integer getLowerRangeDateOffsetMonths()
    {
        return lowerRangeDateOffsetMonths;
    }

    public void setLowerRangeDateOffsetMonths(final Integer lowerRangeDateOffsetMonths)
    {
        this.lowerRangeDateOffsetMonths = lowerRangeDateOffsetMonths;
    }

    public Integer getUpperRangeDateOffsetMonths()
    {
        return upperRangeDateOffsetMonths;
    }

    public void setUpperRangeDateOffsetMonths(final Integer upperRangeDateOffsetMonths)
    {
        this.upperRangeDateOffsetMonths = upperRangeDateOffsetMonths;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getCategoryLetter()
    {
        return categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public Boolean getMultipleIndicator()
    {
        return multipleIndicator;
    }

    public void setMultipleIndicator(final Boolean multipleIndicator)
    {
        this.multipleIndicator = multipleIndicator;
    }

    public SchedulingRegimeDO getSchedulingRegime()
    {
        return schedulingRegime;
    }

    public void setSchedulingRegime(final SchedulingRegimeDO schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    public List<FlagStateDO> getFlags()
    {
        return flags;
    }

    public void setFlags(final List<FlagStateDO> flags)
    {
        this.flags = flags;
    }

    public List<ServiceCatalogueRelationshipDO> getRelationships()
    {
        return relationships;
    }

    public void setRelationships(final List<ServiceCatalogueRelationshipDO> relationships)
    {
        this.relationships = relationships;
    }

    public List<ServiceCatalogueRelationshipDO> getRelationshipsRelatedByToServiceCatalogue()
    {
        return relationshipsRelatedByToServiceCatalogue;
    }

    public void setRelationshipsRelatedByToServiceCatalogue(final List<ServiceCatalogueRelationshipDO> relationshipsRelatedByToServiceCatalogue)
    {
        this.relationshipsRelatedByToServiceCatalogue = relationshipsRelatedByToServiceCatalogue;
    }
}
