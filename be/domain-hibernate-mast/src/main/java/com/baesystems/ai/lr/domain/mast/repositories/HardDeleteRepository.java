package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HardDeleteRepository
{
    // This crude delete method used because I couldn't get HQL bulk-delete to work if the Entity uses a composite key.
    @Modifying
    @Query("delete #{#entityName} e where e.id = :id")
    Integer hardDelete(@Param("id") Long id);
}
