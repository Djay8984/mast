package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDefectDO;

@Entity
@DiscriminatorValue(value = "1")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_Fault SET deleted = true WHERE id = ?")
public class DefectDO extends FaultDO
{
    @JoinColumn(name = "asset_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private AssetDO asset;

    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<JobDefectDO> jobs;

    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<DefectItemDO> items;

    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<DefectDefectValueDO> values;

    public AssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

    public List<JobDefectDO> getJobs()
    {
        return jobs;
    }

    public void setJobs(final List<JobDefectDO> jobs)
    {
        this.jobs = jobs;
    }

    public List<DefectItemDO> getItems()
    {
        return items;
    }

    public void setItems(final List<DefectItemDO> items)
    {
        this.items = items;
    }

    public List<DefectDefectValueDO> getValues()
    {
        return values;
    }

    public void setValues(final List<DefectDefectValueDO> values)
    {
        this.values = values;
    }
}
