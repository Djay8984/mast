package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.RepairTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface RepairTypeRepository extends SpringDataRepository<RepairTypeDO, Long>
{

}
