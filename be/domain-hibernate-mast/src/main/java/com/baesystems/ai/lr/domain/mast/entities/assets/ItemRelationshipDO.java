package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.SQLDelete;

import com.baesystems.ai.lr.domain.mast.entities.references.BaseItemRelationshipDO;

@Entity
@Immutable
@Table(name = "MAST_ASSET_AssetItemRelationship")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetItemRelationship SET deleted = true WHERE id = ?")
public class ItemRelationshipDO extends BaseItemRelationshipDO
{
    @JoinColumns({
            @JoinColumn(name = "to_item_id", referencedColumnName = "id", updatable = false, insertable = false),
            @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", insertable = false, updatable = false),
            @JoinColumn(name = "to_version_id", referencedColumnName = "asset_version_id", updatable = false, insertable = false)
    })
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private VersionedItemDO toItem;

    @JoinColumns({
            @JoinColumn(name = "from_item_id", referencedColumnName = "id", updatable = false, insertable = false),
            @JoinColumn(name = "from_asset_id", referencedColumnName = "asset_id", insertable = false, updatable = false),
            @JoinColumn(name = "from_version_id", referencedColumnName = "asset_version_id", updatable = false, insertable = false)
    })
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private VersionedItemDO fromItem;

    public VersionedItemDO getToItem()
    {
        return toItem;
    }

    public void setToItem(final VersionedItemDO toItem)
    {
        this.toItem = toItem;
    }

    public VersionedItemDO getFromItem()
    {
        return fromItem;
    }

    public void setFromItem(final VersionedItemDO fromItem)
    {
        this.fromItem = fromItem;
    }
}
