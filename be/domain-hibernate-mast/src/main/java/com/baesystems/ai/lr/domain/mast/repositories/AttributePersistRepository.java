package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.AttributeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributePersistDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AttributePersistRepository extends SpringDataRepository<AttributePersistDO, Long>
{
    @Modifying
    @Query("UPDATE AttributePersistDO attr SET attr.deleted = true WHERE attr.id = :id")
    void setAsDeleted(@Param("id") Long id);

    @Modifying
    @Query("update AttributePersistDO attribute"
            + " set attribute.deleted = true"
            + " where attribute.itemId in (:itemId)")
    void bulkLogicalDeleteByItemId(@Param("itemId") List<Long> itemId);

    @Query("select attr from AttributePersistDO attr, AttributeTypeDO attrType"
            + " where attr.itemId = :itemId"
            + " and (attr.deleted is null or attr.deleted = false)"
            + " and attr.attributeType.id  = attrType.id"
            + " and (attrType.namingOrder is not null AND attrType.namingOrder > 0)"
            + " order by attrType.namingOrder, attr.id asc")
    List<AttributeDO> findAttributesWithNamingOrderForItem(@Param("itemId") Long itemId);
}
