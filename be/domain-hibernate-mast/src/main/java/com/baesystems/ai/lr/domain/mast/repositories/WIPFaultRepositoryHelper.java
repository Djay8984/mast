package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.repository.NoRepositoryBean;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPFaultDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

@NoRepositoryBean
public interface WIPFaultRepositoryHelper<T extends WIPFaultDO> extends SpringDataRepository<T, Long>
{

}
