package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectCategoryDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectCategoryRepository extends SpringDataRepository<DefectCategoryDO, Long>
{

}
