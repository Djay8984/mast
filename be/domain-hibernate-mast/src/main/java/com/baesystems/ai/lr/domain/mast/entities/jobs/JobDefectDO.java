package com.baesystems.ai.lr.domain.mast.entities.jobs;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;

@Entity
@Table(name = "MAST_JOB_Job_Defect")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_Job_Defect SET deleted = true WHERE id = ?")
public class JobDefectDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "job_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private JobDO job;

    // Can't change this until Defect partition has been done as job-defect tightly coupled.
    @JoinColumn(name = "defect_id")
    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = {CascadeType.REFRESH})
    private DefectDO defect;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public DefectDO getDefect()
    {
        return defect;
    }

    public void setDefect(final DefectDO defect)
    {
        this.defect = defect;
    }
}
