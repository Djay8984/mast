package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface TeamRepository extends SpringDataRepository<TeamDO, Long>
{
}
