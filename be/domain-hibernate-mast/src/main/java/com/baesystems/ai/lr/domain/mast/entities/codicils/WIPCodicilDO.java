package com.baesystems.ai.lr.domain.mast.entities.codicils;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;

@Entity
@Table(name = "MAST_JOB_CodicilWIP")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "codicil_type_id", discriminatorType = DiscriminatorType.INTEGER)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_CodicilWIP SET deleted = true WHERE id = ?")
public class WIPCodicilDO extends BaseCodicilDO
{
    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_id") )})
    private LinkDO asset;

    @JoinColumn(name = "job_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private JobDO job;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id") )})
    private LinkDO parent;

    @Column(name = "job_scope_confirmed", nullable = false)
    private Boolean jobScopeConfirmed;

    @Column(name = "action_taken_date")
    private Date actionTakenDate;

    public LinkDO getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkDO asset)
    {
        this.asset = asset;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public LinkDO getParent()
    {
        return parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }

    public Boolean getJobScopeConfirmed()
    {
        return jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public Date getActionTakenDate()
    {
        return actionTakenDate;
    }

    public void setActionTakenDate(final Date actionTakenDate)
    {
        this.actionTakenDate = actionTakenDate;
    }
}
