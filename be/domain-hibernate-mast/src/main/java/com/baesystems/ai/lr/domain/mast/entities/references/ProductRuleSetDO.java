package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_RulesetCategory")
public class ProductRuleSetDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @JoinTable(name = "MAST_REF_RulesetCategory_ProductCatalogue", joinColumns = {@JoinColumn(name = "ruleset_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "product_catalogue_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    @OrderBy("id")
    private List<ProductCatalogueDO> productCatalogues;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public List<ProductCatalogueDO> getProductCatalogues()
    {
        return productCatalogues;
    }

    public void setProductCatalogues(final List<ProductCatalogueDO> productCatalogues)
    {
        this.productCatalogues = productCatalogues;
    }
}
