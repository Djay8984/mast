package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AttachmentLinkRepository extends SpringDataRepository<AttachmentLinkDO, Long>
{

}
