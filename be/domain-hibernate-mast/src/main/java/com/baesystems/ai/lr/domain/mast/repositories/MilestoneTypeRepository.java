package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MilestoneTypeRepository extends SpringDataRepository<MilestoneTypeDO, Long>
{

}
