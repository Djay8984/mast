package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CertificateActionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CertificateActionRepository extends SpringDataRepository<CertificateActionDO, Long>
{

}
