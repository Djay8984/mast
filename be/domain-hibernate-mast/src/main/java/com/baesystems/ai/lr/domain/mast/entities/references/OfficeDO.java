package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobOfficeDO;

@Entity
@SQLDelete(sql = "UPDATE MAST_REF_Office SET deleted = true WHERE id = ?")
@Table(name = "MAST_REF_Office")
public class OfficeDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "code", length = 30)
    private String code;

    @OneToMany(mappedBy = "office", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @Where(clause = "deleted = false")
    private List<JobOfficeDO> jobOffices;

    @OneToMany(mappedBy = "office", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @Where(clause = "deleted = false")
    private List<CaseOfficeDO> caseOffices;

    @OneToMany(mappedBy = "office", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @Where(clause = "deleted = false")
    private List<AssetOfficeDO> assetOffices;

    @OneToMany(mappedBy = "office", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<LrEmployeeOfficeDO> employees;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "office_role_id")
    private OfficeRoleDO officeRole;

    @Column(name = "email_address", length = 200)
    private String emailAddress;

    @Column(name = "phone_number", length = 50)
    private String phoneNumber;

    @Column(name = "address_line_1", length = 200)
    private String addressLine1;

    @Column(name = "address_line_2", length = 200)
    private String addressLine2;

    @Column(name = "address_line_3", length = 200)
    private String addressLine3;
    
    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public List<JobOfficeDO> getJobOffices()
    {
        return jobOffices;
    }

    public void setJobOffices(final List<JobOfficeDO> jobOffices)
    {
        this.jobOffices = jobOffices;
    }

    public List<LrEmployeeOfficeDO> getEmployees()
    {
        return employees;
    }

    public void setEmployees(final List<LrEmployeeOfficeDO> employees)
    {
        this.employees = employees;
    }

    public OfficeRoleDO getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final OfficeRoleDO officeRole)
    {
        this.officeRole = officeRole;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public List<CaseOfficeDO> getCaseOffices()
    {
        return caseOffices;
    }

    public void setCaseOffices(final List<CaseOfficeDO> caseOffices)
    {
        this.caseOffices = caseOffices;
    }

    public List<AssetOfficeDO> getAssetOffices()
    {
        return assetOffices;
    }

    public void setAssetOffices(final List<AssetOfficeDO> assetOffices)
    {
        this.assetOffices = assetOffices;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }
    
}
