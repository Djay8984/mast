package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_WorkItemAction")
public class WorkItemActionDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "reference_code", nullable = false, length = 5)
    private String referenceCode;

    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Column(name = "task_category")
    private String taskCategory;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getTaskCategory()
    {
        return taskCategory;
    }

    public void setTaskCategory(final String taskCategory)
    {
        this.taskCategory = taskCategory;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }
}
