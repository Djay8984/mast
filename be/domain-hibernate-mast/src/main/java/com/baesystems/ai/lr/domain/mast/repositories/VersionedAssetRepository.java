package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;

public interface VersionedAssetRepository extends SpringDataRepository<VersionedAssetDO, VersionedAssetPK>,
        HardDeleteVersionedAssetRepository, LockingRepository<VersionedAssetDO>
{

    // Published only clause that can be appended to exitsing queries to ensure only published versions are returned.
    String PUBLISHED_ONLY = " and asset.asset.publishedVersionId = asset.assetVersionId";

    // coalesce is used here to get around the problem of potentially comparing a list and a single value.
    @Query(countQuery = "select count(distinct asset) from VersionedAssetDO asset"
            + " left join asset.customers assetCustomer"
            + " left join assetCustomer.functions customerFunctionLink"
            + " left join customerFunctionLink.customerFunction customerFunction"
            + FIND_ALL_QUERY_WHERE_CLAUSE, value = "select distinct asset from VersionedAssetDO asset"
                    + " left join fetch asset.customers assetCustomer"
                    + " left join assetCustomer.functions customerFunctionLink"
                    + " left join customerFunctionLink.customerFunction customerFunction"
                    + FIND_ALL_QUERY_WHERE_CLAUSE)
    Page<VersionedAssetDO> findAll(Pageable pageable, @Param("query") AssetQueryDto query);

    String FIND_ALL_QUERY_WHERE_CLAUSE = " where (:#{#query.itemTypeId} is null or coalesce(:#{#query.idList}) is not null)"
            + " and (coalesce(:#{#query.lifecycleStatusId}) is null or asset.assetLifecycleStatus.id in (:#{#query.lifecycleStatusId}))"
            + " and (coalesce(:#{#query.imoNumber}) is null or asset.ihsAsset.id in (:#{#query.imoNumber}))"
            + " and (coalesce(:#{#query.assetTypeId}) is null or asset.assetType.id in (:#{#query.assetTypeId}))"
            + " and (:#{#query.buildDateMin} is null or asset.buildDate >= :#{#query.buildDateMin})"
            + " and (:#{#query.buildDateMax} is null or asset.buildDate <= :#{#query.buildDateMax})"
            + " and (:#{#query.effectiveDateMin} is null or asset.effectiveDate >= :#{#query.effectiveDateMin})"
            + " and (:#{#query.effectiveDateMax} is null or asset.effectiveDate <= :#{#query.effectiveDateMax})"
            + " and (coalesce(:#{#query.classStatusId}) is null or asset.classStatus.id in (:#{#query.classStatusId}))"
            + " and (coalesce(:#{#query.classDepartmentId}) is null or asset.classDepartment.id in (:#{#query.classDepartmentId}))"
            + " and (coalesce(:#{#query.flagStateId}) is null or asset.flagState.id in (:#{#query.flagStateId}))"
            + " and (:#{#query.grossTonnageMin} is null or asset.grossTonnage >= :#{#query.grossTonnageMin})"
            + " and (:#{#query.grossTonnageMax} is null or asset.grossTonnage <= :#{#query.grossTonnageMax})"
            + " and (:#{#query.deadWeightMin} is null or asset.deadWeight >= :#{#query.deadWeightMin})"
            + " and (:#{#query.deadWeightMax} is null or asset.deadWeight <= :#{#query.deadWeightMax})"
            + " and (coalesce(:#{#query.idList}) is null or asset.id in (:#{#query.idList}))"
            + " and (coalesce(:#{#query.categoryId}) is null or asset.assetCategory.id in (:#{#query.categoryId}))"
            + " and (coalesce(:#{#query.partyId}) is null or assetCustomer.customer.id in (:#{#query.partyId}))"
            + " and (coalesce(:#{#query.partyFunctionTypeId}) is null or customerFunction.id in (:#{#query.partyFunctionTypeId}))"
            + " and (coalesce(:#{#query.partyRoleId}) is null or assetCustomer.relationship.id in (:#{#query.partyRoleId}))"
            + " and (coalesce(:#{#query.leadImo}) is null or asset.ihsAsset.id = (:#{#query.leadImo}) or asset.leadImo = (:#{#query.leadImo}))"
            + " and (:#{#query.builder} is null or asset.builder = :#{#query.builder})"
            // Query for search.
            + " and (:#{#query.search} is null"
            + " or cast(asset.ihsAsset.id as string) like :#{#query.search}"
            + " or cast(asset.id as string) like :#{#query.search}"
            + " or asset.name like :#{#query.search})"
            // Query for yard number
            + " and (:#{#query.yardNumber} is null or asset.yardNumber like :#{#query.yardNumber})"
            + PUBLISHED_ONLY;

    @Query("select asset from VersionedAssetDO asset join asset.ihsAsset ihsAsset"
            + " where cast(ihsAsset.id as string) like :search"
            + " or cast(asset.id as string) like :search "
            + " or asset.name like :search"
            + PUBLISHED_ONLY)
    Page<VersionedAssetDO> findAll(Pageable pageable, @Param("search") String search);

    @Query("select distinct asset.id from VersionedAssetDO asset join fetch asset.ihsAsset ihsAsset where ihsAsset.id = :imoNumber"
            + PUBLISHED_ONLY)
    Long findIdByImoNumber(@Param("imoNumber") Long imoNumber);

    @Query("select distinct asset from VersionedAssetDO asset join fetch asset.ihsAsset ihsAsset where ihsAsset.id = :imoNumber"
            + PUBLISHED_ONLY)
    VersionedAssetDO findByImoNumber(@Param("imoNumber") Long imoNumber);

    @Query("select asset.id from VersionedAssetDO asset"
            + " where"
            + " (:ihsAssetId is not null and asset.ihsAsset.id = :ihsAssetId)"
            + " or"
            + " (:builder is not null and :yardNumber is not null"
            + " and builder = :builder and asset.yardNumber = :yardNumber)"
            + PUBLISHED_ONLY)
    Long findAssetIdByImoOrBuilderAndYard(@Param("ihsAssetId") Long ihsAssetId,
            @Param("builder") String builder,
            @Param("yardNumber") String yardNumber);

    // todo This should take version PK and ensure draft
    @Modifying
    @Query("UPDATE VersionedAssetDO a SET a.harmonisationDate = :harmonisationDate WHERE a.id = :assetId")
    void updateHarmonisationDate(@Param("assetId") Long assetId, @Param("harmonisationDate") Date harmonisationDate);

    // todo This should take version PK and ensure draft
    @Modifying
    @Query("UPDATE VersionedAssetDO a SET a.hullIndicator = :hullIndicator WHERE a.id = :assetId")
    void updateHullIndicator(@Param("assetId") Long assetId, @Param("hullIndicator") Integer hullIndicator);

    @Query("select asset.assetCategory.id from VersionedAssetDO asset"
            + " where asset.id = :assetId"
            + PUBLISHED_ONLY)
    Long findCategory(@Param("assetId") Long assetId);

    @Query("select asset.assetItem.id from VersionedAssetDO asset"
            + " where asset.id = :assetId")
    Long findRootItemId(@Param("assetId") Long assetId);

    // todo This should take version PK and ensure draft
    @Modifying
    @Query("UPDATE VersionedAssetDO a SET a.assetItem.id = :itemId WHERE a.id = :assetId")
    void updateRootItem(@Param("assetId") Long assetId, @Param("itemId") Long itemId);

    @Query("select asset.harmonisationDate from VersionedAssetDO asset where asset.id = :assetId"
            + PUBLISHED_ONLY)
    Date findAssetHarmonisationDate(@Param("assetId") Long assetId);

    /*
     * This is the only place that looks to use the Asset -> Versioned Asset assoc.
     */
    @Override
    @Query(value = "select asset.publishedVersion from AssetDO asset", countQuery = "select count(id) from AssetDO asset")
    Page<VersionedAssetDO> findAll(Pageable pageable);

    @Query("select (max(assetVersionId) + 1) from VersionedAssetDO va where va.id = :assetId")
    Long getNextVersion(@Param("assetId") Long assetId);
}
