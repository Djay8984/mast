package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_SchedulingType")
public class SchedulingTypeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @JoinColumn(name = "assigned_scheduling_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private AssignedSchedulingTypeDO assignedSchedulingType;

    @JoinColumn(name = "scheduling_due_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private SchedulingDueTypeDO schedulingDueType;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public AssignedSchedulingTypeDO getAssignedSchedulingType()
    {
        return assignedSchedulingType;
    }

    public void setAssignedSchedulingType(final AssignedSchedulingTypeDO assignedSchedulingType)
    {
        this.assignedSchedulingType = assignedSchedulingType;
    }

    public SchedulingDueTypeDO getSchedulingDueType()
    {
        return schedulingDueType;
    }

    public void setSchedulingDueType(final SchedulingDueTypeDO schedulingDueType)
    {
        this.schedulingDueType = schedulingDueType;
    }
}
