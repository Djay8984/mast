package com.baesystems.ai.lr.domain.mast.entities.customers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "MAST_CDH_Organisation")
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumns({
                        @PrimaryKeyJoinColumn(name = "PARTY_ID", referencedColumnName = "ID")
})
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CDH_Organisation SET deleted = true WHERE id = ?")
public class OrganisationDO extends PartyDO
{
    @Column(name = "lr_approved_flag", nullable = false)
    private Boolean approvalStatus;

    public Boolean getApprovalStatus()
    {
        return this.approvalStatus;
    }

    public void setApprovalStatus(final Boolean approvalStatus)
    {
        this.approvalStatus = approvalStatus;
    }
}
