package com.baesystems.ai.lr.domain.mast.entities.attachments;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AttachmentTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ConfidentialityTypeDO;

@Entity
@Table(name = "MAST_XXX_SupplementaryInformation")
@Inheritance(strategy = InheritanceType.JOINED)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_XXX_SupplementaryInformation SET deleted = true WHERE id = ?")
public class SupplementaryInformationDO extends AuditedDO
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title", length = 50)
    private String title;

    @Column(name = "note", length = 2500)
    private String note;

    @Column(name = "attachment_url", length = 500)
    private String attachmentUrl;

    @Column(name = "asset_location_viewpoint", length = 100)
    private String assetLocationViewpoint;

    @JoinColumn(name = "attachment_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private AttachmentTypeDO attachmentType;

    @JoinColumn(name = "confidentiality_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private ConfidentialityTypeDO confidentialityType;

    @JoinColumn(name = "author_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private LrEmployeeDO author;

    @Column(name = "creation_date", nullable = false)
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date creationDate;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getNote()
    {
        return note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public String getAttachmentUrl()
    {
        return attachmentUrl;
    }

    public void setAttachmentUrl(final String attachmentUrl)
    {
        this.attachmentUrl = attachmentUrl;
    }

    public String getAssetLocationViewpoint()
    {
        return assetLocationViewpoint;
    }

    public void setAssetLocationViewpoint(final String assetLocationViewpoint)
    {
        this.assetLocationViewpoint = assetLocationViewpoint;
    }

    public AttachmentTypeDO getAttachmentType()
    {
        return attachmentType;
    }

    public void setAttachmentType(final AttachmentTypeDO attachmentType)
    {
        this.attachmentType = attachmentType;
    }

    public ConfidentialityTypeDO getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final ConfidentialityTypeDO confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LrEmployeeDO getAuthor()
    {
        return author;
    }

    public void setAuthor(final LrEmployeeDO author)
    {
        this.author = author;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate)
    {
        this.creationDate = creationDate;
    }
}
