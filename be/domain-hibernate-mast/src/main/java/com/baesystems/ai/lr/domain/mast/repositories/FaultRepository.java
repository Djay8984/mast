package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.defects.FaultDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface FaultRepository extends SpringDataRepository<FaultDO, Long>
{

}
