package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectValueSeverityDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectValueSeverityRepository extends SpringDataRepository<DefectValueSeverityDO, Long>
{

}
