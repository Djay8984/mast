package com.baesystems.ai.lr.domain.mast.entities.references;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_REF_TeamOffice")
public class TeamOfficeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "team_id", nullable = false))
    private LinkDO team;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "office_id", nullable = false))
    private LinkDO office;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getTeam()
    {
        return team;
    }

    public void setTeam(final LinkDO team)
    {
        this.team = team;
    }

    public LinkDO getOffice()
    {
        return office;
    }

    public void setOffice(final LinkDO office)
    {
        this.office = office;
    }
}
