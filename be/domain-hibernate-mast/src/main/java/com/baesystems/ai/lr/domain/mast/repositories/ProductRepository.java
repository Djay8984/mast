package com.baesystems.ai.lr.domain.mast.repositories;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.services.ProductDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

import java.util.List;

public interface ProductRepository extends LockingRepository<ProductDO>, SpringDataRepository<ProductDO, Long>
{
    @Query("select product from ProductDO product"
            + " join fetch product.asset asset"
            + " where asset.id = :assetId")
    List<ProductDO> findSelectedProductsForAsset(@Param("assetId") Long assetId);

    @Query("select product.productCatalogue.id from ProductDO product"
            + " where product.id = :productId")
    Long findProductCatalogueId(@Param("productId") Long productId);

    @Query("select asset.id from ProductDO product join product.asset asset"
            + " where product.id = :productId")
    Long findAssetIdForProduct(@Param("productId") Long productId);

    @Query("select count(product.id) > 0 from ProductDO product join product.asset asset"
            + " where product.productCatalogue.id = :productCatalogueId"
            + " and (:schedulingRegimeId is null or product.schedulingRegime.id = :schedulingRegimeId)"
            + " and asset.id = :assetId")
    Boolean isProductCatalogueSelectedForAsset(@Param("assetId") final Long assetId, @Param("productCatalogueId") final Long productCatalogueId,
            @Param("schedulingRegimeId") Long schedulingRegimeId);

}
