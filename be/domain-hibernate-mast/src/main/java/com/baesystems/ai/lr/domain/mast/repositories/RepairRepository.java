package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.RepairDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface RepairRepository extends LockingRepository<RepairDO>, SpringDataRepository<RepairDO, Long>
{

    @Query("select count(item.id) from DefectDO defect "
            + " join defect.items item "
            + " where defect.id = :defectId "
            + " and item.id not in ("
            + " select item.id from DefectDO defect "
            + "  join defect.items item "
            + "  join item.repairItems repairItems "
            + "  join repairItems.repair repair "
            + "  join repair.repairAction action "
            + "  where defect.id = :defectId and repair.confirmed = true and action.id = :repairActionId "
            + ")")
    Integer countItemsRequiringRepair(@Param("defectId") Long defectId, @Param("repairActionId") Long repairActionId);

    /**
     * Counts the number of confirmed permanent repairs associated with a defect.
     *
     * @param defectId, the id of the defect.
     * @return true if the defect has at least one permanent repair.
     */
    @Query("select count(defectRepair.id) from RepairDO defectRepair"
            + " join defectRepair.defect defect"
            + " join defectRepair.repairAction action"
            + " where defect.id = :defectId"
            + " and action.id = :repairActionId"
            + " and defectRepair.confirmed = true")
    Integer countPermanentRepairs(@Param("defectId") Long defectId, @Param("repairActionId") Long repairActionId);

    @Query("select repair from RepairDO repair join repair.defect defect where defect.id = :defectId")
    Page<RepairDO> findAllByDefectId(Pageable pageable, @Param("defectId") Long defectId);

    /**
     * Counts the number of permanent and temporary repairs associated with a defect.
     */
    @Query("select count(defectRepair.id) from RepairDO defectRepair"
            + " join defectRepair.defect defect"
            + " join defectRepair.repairAction action"
            + " where defect.id = :defectId"
            + " and action.id in (:repairActionIdList)")
    Integer countRepairsForDefect(@Param("defectId") Long defectId, @Param("repairActionIdList") List<Long> repairActionIdList);

    @Query("select repair from RepairDO repair where repair.codicil.id = :cocId")
    Page<RepairDO> findAllByCoC(Pageable pageable, @Param("cocId") Long cocId);

    @Query("select count(repair.id) > 0 from RepairDO repair"
            + " where repair.id = :repairId"
            + " and repair.defect.id = :defectId")
    Boolean repairExistsForDefect(@Param("repairId") Long repairId, @Param("defectId") Long defectId);

    @Query("select count(repair.id) > 0 from RepairDO repair"
            + " where repair.id = :repairId"
            + " and repair.codicil.id = :cocId")
    Boolean repairExistsForCoC(@Param("repairId") Long repairId, @Param("cocId") Long cocId);

    @Query("select r from RepairDO r"
            + " where (r.defect.asset.id = :assetId"
            + " or r.codicil.asset.id = :assetId)")
    List<RepairDO> findRepairsForAsset(@Param("assetId") Long assetId);
    
    @Query("select count(repair.id) from RepairDO repair"
            + " join repair.codicil wipCodicil"
            + " join repair.repairAction action"
            + " where wipCodicil.id = :cocId"
            + " and action.id = :repairActionId"
            + " and repair.confirmed = true")
    Integer countCocPermanentRepairs(@Param("cocId") Long cocId, @Param("repairActionId") Long repairActionId);
}
