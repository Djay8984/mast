package com.baesystems.ai.lr.domain.mast.entities.jobs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_JOB_JobResource")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_JobResource SET deleted = true WHERE id = ?")
public class JobResourceDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "job_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private JobDO job;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "lr_employee_id", nullable = false))})
    private LinkDO lrEmployee;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "employee_role_id", nullable = false))})
    private LinkDO employeeRole;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public LinkDO getLrEmployee()
    {
        return lrEmployee;
    }

    public void setLrEmployee(final LinkDO lrEmployee)
    {
        this.lrEmployee = lrEmployee;
    }

    public LinkDO getEmployeeRole()
    {
        return employeeRole;
    }

    public void setEmployeeRole(final LinkDO employeeRole)
    {
        this.employeeRole = employeeRole;
    }
}
