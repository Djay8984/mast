package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.ItemTypeRelationshipDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ItemTypeRelationshipRepository extends SpringDataRepository<ItemTypeRelationshipDO, Long>
{
    @Query("select count(itemTypeRelationship.id) > 0 from ItemTypeRelationshipDO itemTypeRelationship"
            + " join itemTypeRelationship.itemRelationshipType itemRelationshipType"
            + " where itemTypeRelationship.fromItemType.id = :proposedFromItemTypeId"
            + " and itemTypeRelationship.toItemType.id = :proposedToItemTypeId"
            + " and itemRelationshipType.id = :relationshipType"
            + " and itemTypeRelationship.assetCategoryId = :assetCategoryId"
            + " and itemTypeRelationship.deleted = false")
    Boolean isProposedRelationshipValidForTypes(
            @Param("proposedToItemTypeId") Long proposedToItemTypeId,
            @Param("proposedFromItemTypeId") Long proposedFromItemTypeId,
            @Param("assetCategoryId") Integer assetCategoryId,
            @Param("relationshipType") Long relationshipType);

    @Query("select itemTypeRelationship.maxOccurs from ItemTypeRelationshipDO itemTypeRelationship"
            + " where itemTypeRelationship.fromItemType.id = :parentTypeId"
            + " and itemTypeRelationship.toItemType.id = :childTypeId"
            + " and itemTypeRelationship.assetCategoryId = :assetCategoryId"
            + " and itemTypeRelationship.itemRelationshipType.id = :relationshipTypeId"
            + " and itemTypeRelationship.deleted = false")
    Integer getMaxOccurs(@Param("parentTypeId") final Long parentTypeId, @Param("childTypeId") final Long childTypeId,
            @Param("assetCategoryId") Integer assetCategoryId, @Param("relationshipTypeId") Long relationshipTypeId);
}
