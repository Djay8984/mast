package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.EndorsementTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface EndorsementTypeRepository extends SpringDataRepository<EndorsementTypeDO, Long>
{
}
