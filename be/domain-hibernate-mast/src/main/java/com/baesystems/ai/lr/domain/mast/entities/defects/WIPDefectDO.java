package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;

@Entity
@DiscriminatorValue(value = "1")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_WIP_Fault SET deleted = true WHERE id = ?")
public class WIPDefectDO extends WIPFaultDO
{
    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<WIPDefectItemDO> items;

    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<WIPRepairDO> repairs;

    @OneToMany(mappedBy = "wipDefect", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<AttachmentLinkDO> attachments;

    @OneToMany(mappedBy = "defect", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<WIPDefectDefectValueDO> values;

    @PreRemove
    private void preRemove()
    {
        if (attachments != null)
        {
            for (final AttachmentLinkDO attachment : attachments)
            {
                attachment.setDeleted(Boolean.TRUE);
            }
        }

        if (repairs != null)
        {
            for (final WIPRepairDO repair : repairs)
            {
                repair.setDeleted(Boolean.TRUE);
            }
        }
    }

    public List<WIPDefectItemDO> getItems()
    {
        return items;
    }

    public void setItems(final List<WIPDefectItemDO> items)
    {
        this.items = items;
    }

    public List<WIPDefectDefectValueDO> getValues()
    {
        return values;
    }

    public void setValues(final List<WIPDefectDefectValueDO> values)
    {
        this.values = values;
    }

    public List<WIPRepairDO> getRepairs()
    {
        return repairs;
    }

    public void setRepairs(final List<WIPRepairDO> repairs)
    {
        this.repairs = repairs;
    }

    public List<AttachmentLinkDO> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(final List<AttachmentLinkDO> attachments)
    {
        this.attachments = attachments;
    }
}
