package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ClassNotationDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ClassNotationRepository extends SpringDataRepository<ClassNotationDO, Long>
{

}
