package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;

public interface WIPCoCRepository extends LockingRepository<WIPCoCDO>, WIPCodicilRepositoryHelper<WIPCoCDO>, CourseOfActionRepositoryHelper<WIPCoCDO>
{
    @Query("select c from WIPCoCDO c"
            + " left outer join c.defect defect"
            + " where c.job.id = :jobId"
            + " and (:defectId is null or defect.id = :defectId)")
    Page<WIPCoCDO> findCoCsForJob(Pageable pageable, @Param("jobId") Long jobId, @Param("defectId") Long defectId);

    @Query("select coc from WIPCoCDO coc"
            + " where coc.job.id = :jobId")
    List<WIPCoCDO> findCoCsForJob(@Param("jobId") Long jobId);

    @Query("select coc from WIPCoCDO coc"
            + " where coc.job.id = :jobId"
            + " and coc.actionTakenDate is not null")
    List<WIPCoCDO> findCoCsForReportContent(@Param("jobId") Long jobId);

    @Query("select coc from WIPCoCDO coc"
            + " where coc.job.id = :jobId"
            + " and deleted = false"
            + " and (coalesce(:statusList) is null or coc.status.id in (:statusList))")
    List<WIPCoCDO> findCoCsForJob(@Param("jobId") Long jobId, @Param("statusList") List<Long> statusList);

    @Query("select coc.id from WIPCoCDO coc"
            + " where coc.job.id = :jobId and coc.id = :cocId")
    Long findCoCForJob(@Param("jobId") Long jobId, @Param("cocId") Long cocId);

    @Query("select coc from WIPCoCDO coc"
            + " where coc.job.id = :jobId"
            + " and coc.updatedBy = :employeeName"
            + " and day(coc.actionTakenDate) = day(:lastUpdatedDate)"
            + " and month(coc.actionTakenDate) = month(:lastUpdatedDate)"
            + " and year(coc.actionTakenDate) = year(:lastUpdatedDate)")
    List<WIPCoCDO> findCoCsByJob(@Param("jobId") Long jobId, @Param("employeeName") String employeeName,
            @Param("lastUpdatedDate") Date lastUpdatedDate);

    @Query("select coc from WIPCoCDO coc"
            + " where coc.defect.id = :defectId")
    List<WIPCoCDO> findCoCsForDefect(@Param("defectId") Long defectId);

    @Modifying
    @Query("update WIPCoCDO coc set coc.jobScopeConfirmed = :scopeConfirmed"
            + " where coc.job.id = :jobId"
            + " and (coalesce(:statusList) is null or coc.status.id in (:statusList))")
    Integer updateScopeConfirmed(@Param("jobId") Long jobId,
            @Param("scopeConfirmed") Boolean scopeConfirmed,
            @Param("statusList") List<Long> statusList);

    @Query("select count(coc.id) > 0  from WIPCoCDO coc"
            + " where coc.defect.id = :defectId"
            + " and coc.status.id in (:statusIds)")
    Boolean defectHasCoCInStatus(@Param("defectId") Long defectId, @Param("statusIds") List<Long> statusIds);

    @Modifying()
    @Query("update WIPCoCDO wipCodicil set wipCodicil.status = :status "
            + " where wipCodicil.id = :wipCodicilId")
    Integer updateStatus(@Param("wipCodicilId") Long wipCodicilId, @Param("status") LinkDO status);
}
