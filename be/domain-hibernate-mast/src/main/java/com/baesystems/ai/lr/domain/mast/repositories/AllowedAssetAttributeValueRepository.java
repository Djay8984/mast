package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AllowedAssetAttributeValueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AllowedAssetAttributeValueRepository extends SpringDataRepository<AllowedAssetAttributeValueDO, Long>
{
}
