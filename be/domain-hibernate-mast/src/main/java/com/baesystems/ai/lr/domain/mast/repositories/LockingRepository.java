package com.baesystems.ai.lr.domain.mast.repositories;

import javax.persistence.LockModeType;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.entities.BaseDO;

/**
 * Locking repository
 * @param <T> Entity type
 */
public interface LockingRepository<T extends BaseDO>
{
    @Query("SELECT entity FROM #{#entityName} entity"
            + " WHERE entity.id = :id")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    T findOneWithLock(@Param("id") Long id);

    @Query("SELECT entity FROM VersionedAssetDO entity"
            + " WHERE entity.id = :id"
            + " AND entity.assetVersionId = :assetVersionId")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    VersionedAssetDO findOneWithLock(@Param("id") Long id, @Param("assetVersionId") Long assetVersionId);
}
