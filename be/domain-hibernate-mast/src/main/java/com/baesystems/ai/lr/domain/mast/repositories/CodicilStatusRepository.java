package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CodicilStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CodicilStatusRepository extends SpringDataRepository<CodicilStatusDO, Long>
{
}
