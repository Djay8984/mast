package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ActionTakenDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ActionTakenRepository extends SpringDataRepository<ActionTakenDO, Long>
{

}
