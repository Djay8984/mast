package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CodicilDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;

@Entity
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_Asset SET deleted = true WHERE id = ?")
@Table(name = "MAST_ASSET_Asset")
public class AssetDO extends AuditedDO
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumns({
                  @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false),
                  @JoinColumn(name = "published_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)})
    private VersionedAssetDO publishedVersion;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumns({
                  @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false),
                  @JoinColumn(name = "draft_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)})
    private VersionedAssetDO draftVersion;

    @Column(name = "checked_out_by")
    private String checkedOutBy;

    /*
     * Hibernate restriction on the above DO associations not being updatable, so we have to duplicate with the
     * below.... Maybe we should get rid of the above and just keep Longs.
     */
    @Column(name = "published_version_id")
    private Long publishedVersionId;

    @Column(name = "draft_version_id")
    private Long draftVersionId;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH}, mappedBy = "asset")
    @Where(clause = "deleted = false")
    private List<CodicilDO> codicils;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH}, mappedBy = "asset")
    @Where(clause = "deleted = false")
    private List<ScheduledServiceDO> services;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "asset_id")
    @Where(clause = "deleted = false and published = true")
    private List<VersionedItemDO> publishedItems;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public VersionedAssetDO getPublishedVersion()
    {
        return this.publishedVersion;
    }

    public void setPublishedVersion(final VersionedAssetDO publishedVersion)
    {
        this.publishedVersion = publishedVersion;
    }

    public VersionedAssetDO getDraftVersion()
    {
        return this.draftVersion;
    }

    public void setDraftVersion(final VersionedAssetDO draftVersion)
    {
        this.draftVersion = draftVersion;
    }

    public String getCheckedOutBy()
    {
        return this.checkedOutBy;
    }

    public void setCheckedOutBy(final String checkedOutBy)
    {
        this.checkedOutBy = checkedOutBy;
    }

    public Long getPublishedVersionId()
    {
        return this.publishedVersionId;
    }

    public void setPublishedVersionId(final Long publishedVersionId)
    {
        this.publishedVersionId = publishedVersionId;
    }

    public Long getDraftVersionId()
    {
        return this.draftVersionId;
    }

    public void setDraftVersionId(final Long draftVersionId)
    {
        this.draftVersionId = draftVersionId;
    }

    public List<CodicilDO> getCodicils()
    {
        return this.codicils;
    }

    public void setCodicils(final List<CodicilDO> codicils)
    {
        this.codicils = codicils;
    }

    public List<ScheduledServiceDO> getServices()
    {
        return this.services;
    }

    public void setServices(final List<ScheduledServiceDO> services)
    {
        this.services = services;
    }

    public List<VersionedItemDO> getPublishedItems()
    {
        return this.publishedItems;
    }

    public void setPublishedItems(final List<VersionedItemDO> publishedItems)
    {
        this.publishedItems = publishedItems;
    }
}
