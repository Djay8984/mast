package com.baesystems.ai.lr.domain.mast.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LinkDO
{
    @Column
    private Long id;

    public LinkDO(final Long id)
    {
        this.id = id;
    }

    public LinkDO(){}

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return this.id;
    }

    public boolean equals(Object other)
    {
        boolean result = true;

        if (this != other)
        {
            result = false;
        }

        if (!(other instanceof LinkDO))
        {
            result = false;
        }
        else
        {
            final LinkDO baseDo = (LinkDO) other;

            if (baseDo.getId() != null && !baseDo.getId().equals(getId()))
            {
                result = false;
            }
        }

        return result;
    }

    public int hashCode()
    {
        int result;

        if (getId() != null)
        {
            result = getId().hashCode();
        }
        else
        {
            result = super.hashCode();
        }

        return result;
    }
}
