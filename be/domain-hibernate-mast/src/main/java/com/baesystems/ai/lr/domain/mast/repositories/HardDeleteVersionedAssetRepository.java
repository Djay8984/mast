package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HardDeleteVersionedAssetRepository
{
    @Modifying
    @Query("delete #{#entityName} e where e.id = :assetId and e.assetVersionId = :versionId")
    Integer hardDelete(@Param("assetId") Long assetId, @Param("versionId") Long versionId);
}
