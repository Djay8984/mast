package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "MAST_FAULT_Fault")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "fault_type_id", discriminatorType = DiscriminatorType.INTEGER)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_Fault SET deleted = true WHERE id = ?")
public class FaultDO extends BaseFaultDO
{

}
