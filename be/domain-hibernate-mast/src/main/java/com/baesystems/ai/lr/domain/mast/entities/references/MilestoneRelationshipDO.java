package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_MilestoneRelationship")
public class MilestoneRelationshipDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "milestone_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private MilestoneDO milestone;

    @JoinColumn(name = "predecessor_milestone_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private MilestoneDO predecessorMilestone;

    @Column(name = "date_dependency")
    private Boolean dateDependency;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public MilestoneDO getMilestone()
    {
        return milestone;
    }

    public void setMilestone(final MilestoneDO milestone)
    {
        this.milestone = milestone;
    }

    public MilestoneDO getPredecessorMilestone()
    {
        return predecessorMilestone;
    }

    public void setPredecessorMilestone(final MilestoneDO predecessorMilestone)
    {
        this.predecessorMilestone = predecessorMilestone;
    }

    public Boolean getDateDependency()
    {
        return dateDependency;
    }

    public void setDateDependency(final Boolean dateDependency)
    {
        this.dateDependency = dateDependency;
    }
}
