package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_CaseStatus")
public class CaseStatusDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "can_delete", nullable = false)
    private Boolean canDelete;

    @Column(name = "can_save_with_errors", nullable = false)
    private Boolean canSaveWithErrors;

    @Column(name = "synchronise_asset")
    private Boolean synchroniseAsset;

    @Column(name = "open")
    private Boolean open;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Boolean getCanDelete()
    {
        return canDelete;
    }

    public void setCanDelete(final Boolean canDelete)
    {
        this.canDelete = canDelete;
    }

    public Boolean getCanSaveWithErrors()
    {
        return canSaveWithErrors;
    }

    public void setCanSaveWithErrors(final Boolean canSaveWithErrors)
    {
        this.canSaveWithErrors = canSaveWithErrors;
    }

    public Boolean getSynchroniseAsset()
    {
        return synchroniseAsset;
    }

    public void setSynchroniseAsset(final Boolean synchroniseAsset)
    {
        this.synchroniseAsset = synchroniseAsset;
    }

    public Boolean getOpen()
    {
        return open;
    }

    public void setOpen(final Boolean open)
    {
        this.open = open;
    }
}
