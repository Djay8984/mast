package com.baesystems.ai.lr.domain.mast.entities.cases;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;

@Entity
@Table(name = "MAST_CASE_CaseOffice")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CASE_CaseOffice SET deleted = true WHERE id = ?")
public class CaseOfficeDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "office_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private OfficeDO office;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "office_role_id", nullable = false))})
    private LinkDO officeRole;

    @JoinColumn(name = "case_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CaseDO aCase;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public OfficeDO getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDO office)
    {
        this.office = office;
    }

    public LinkDO getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkDO officeRole)
    {
        this.officeRole = officeRole;
    }

    public CaseDO getaCase()
    {
        return aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }
}
