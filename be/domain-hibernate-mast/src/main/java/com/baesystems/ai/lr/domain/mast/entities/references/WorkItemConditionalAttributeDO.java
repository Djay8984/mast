package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_WorkItemConditionalAttribute")
public class WorkItemConditionalAttributeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "description", length = 300)
    private String description;

    @Column(name = "generates_follow_on_attribute")
    private Boolean generatesFollowOnAttribute;

    @Column(name = "service_code", length = 10)
    private String serviceCode;

    @JoinColumn(name = "item_type_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private ItemTypeDO itemType;

    @JoinColumn(name = "work_item_action_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private WorkItemActionDO workItemAction;

    @JoinColumn(name = "attribute_data_type_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private AttributeDataTypeDO attributeDataType;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, mappedBy = "workItemAttribute", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<AllowedWorkItemAttributeValueDO> allowedAttributeValues;

    @JoinColumn(name = "follow_on_attribute_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private WorkItemConditionalAttributeDO followOnAttribute;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public ItemTypeDO getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDO itemType)
    {
        this.itemType = itemType;
    }

    public WorkItemActionDO getWorkItemAction()
    {
        return workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDO workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public AttributeDataTypeDO getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final AttributeDataTypeDO attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public Boolean getGeneratesFollowOnAttribute()
    {
        return generatesFollowOnAttribute;
    }

    public void setGeneratesFollowOnAttribute(final Boolean generatesFollowOnAttribute)
    {
        this.generatesFollowOnAttribute = generatesFollowOnAttribute;
    }

    public WorkItemConditionalAttributeDO getFollowOnAttribute()
    {
        return followOnAttribute;
    }

    public void setFollowOnAttribute(final WorkItemConditionalAttributeDO followOnAttribute)
    {
        this.followOnAttribute = followOnAttribute;
    }

    public List<AllowedWorkItemAttributeValueDO> getAllowedAttributeValues()
    {
        return allowedAttributeValues;
    }

    public void setAllowedAttributeValues(final List<AllowedWorkItemAttributeValueDO> allowedAttributeValues)
    {
        this.allowedAttributeValues = allowedAttributeValues;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }
}
