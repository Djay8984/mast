package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.io.Serializable;
import java.util.Objects;

public class VersionedAssetPK implements Serializable
{
    private static final long serialVersionUID = -6383891710464739906L;

    private Long id;
    private Long assetVersionId;

    public VersionedAssetPK()
    {
    }

    public VersionedAssetPK(final Long id, final Long version)
    {
        this.id = id;
        this.assetVersionId = version;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        final VersionedAssetPK that = (VersionedAssetPK) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(assetVersionId, that.assetVersionId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, assetVersionId);
    }
}
