package com.baesystems.ai.lr.domain.mast.entities.codicils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@DiscriminatorValue(value = "3")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_CodicilWIP SET deleted = true WHERE id = ?")
public class WIPAssetNoteDO extends WIPCodicilDO
{
}
