package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface SurveyRepository extends LockingRepository<SurveyDO>, SpringDataRepository<SurveyDO, Long>
{
    @Query("SELECT survey FROM SurveyDO survey"
            + " WHERE survey.job.id = :jobId")
    Page<SurveyDO> findByJob(Pageable pageable, @Param("jobId") Long jobId);

    @Query("SELECT survey FROM SurveyDO survey"
            + " WHERE survey.job.id = :jobId")
    List<SurveyDO> findByJob(@Param("jobId") Long jobId);

    @Query("SELECT survey FROM SurveyDO survey"
            + " WHERE survey.job.id = :jobId"
            + " AND survey.actionTakenDate is not null")
    List<SurveyDO> findForReportContent(@Param("jobId") Long jobId);

    @Query("select survey from WIPAssetNoteDO survey"
            + " where survey.job.id = :jobId"
            + " and survey.updatedBy = :employeeName"
            + " and day(survey.actionTakenDate) = day(:lastUpdatedDate)"
            + " and month(survey.actionTakenDate) = month(:lastUpdatedDate)"
            + " and year(survey.actionTakenDate) = year(:lastUpdatedDate)")
    List<SurveyDO> findSurveysByJob(@Param("jobId") Long jobId, @Param("employeeName") String employeeName,
            @Param("lastUpdatedDate") Date lastUpdatedDate);

    @Modifying
    @Query("UPDATE SurveyDO s SET s.scheduleDatesUpdated = :scheduleDatesUpdated WHERE s.id = :id")
    void updateScheduleDatesFlag(@Param("id") Long id, @Param("scheduleDatesUpdated") Boolean scheduleDatesUpdated);

    @Query("select count(survey.id) from SurveyDO survey"
            + " where survey.job.id = :jobId and survey.id = :surveyId")
    Integer surveyExistsForJob(@Param("jobId") Long jobId, @Param("surveyId") Long surveyId);

    @Modifying
    @Query("update SurveyDO survey set survey.jobScopeConfirmed = :scopeConfirmed"
            + " where survey.job.id = :jobId"
            + " and (coalesce(:statusList) is null or survey.surveyStatus.id in (:statusList))")
    Integer updateScopeConfirmed(@Param("jobId") Long jobId,
            @Param("scopeConfirmed") Boolean scopeConfirmed,
            @Param("statusList") List<Long> statusList);

    @Query("select survey.id from SurveyDO survey"
            + " where survey.job.id = :jobId"
            + " and survey.surveyStatus.id in (:statusIds)"
            + " and survey.narrative is not null")
    List<Long> findSurveysWithNarrativeInStatus(@Param("jobId") Long jobId, @Param("statusIds") List<Long> statusIds);
}
