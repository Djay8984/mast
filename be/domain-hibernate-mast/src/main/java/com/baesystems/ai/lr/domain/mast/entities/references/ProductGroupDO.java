package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_ProductGroup")
public class ProductGroupDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "display_order")
    private Integer displayOrder;

    @JoinColumn(name = "product_family_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ProductTypeDO productType;

    @OneToMany(mappedBy = "productGroup", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private List<ProductCatalogueDO> productCatalogues;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public ProductTypeDO getProductType()
    {
        return this.productType;
    }

    public void setProductType(final ProductTypeDO productType)
    {
        this.productType = productType;
    }

    public List<ProductCatalogueDO> getProductCatalogues()
    {
        return this.productCatalogues;
    }

    public void setProductCatalogues(final List<ProductCatalogueDO> productCatalogues)
    {
        this.productCatalogues = productCatalogues;
    }
}
