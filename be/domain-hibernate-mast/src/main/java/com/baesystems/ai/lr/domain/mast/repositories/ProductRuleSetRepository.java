package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ProductRuleSetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ProductRuleSetRepository extends SpringDataRepository<ProductRuleSetDO, Long>
{

}
