package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDefectValueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectDefectValueRepository extends SpringDataRepository<DefectDefectValueDO, Long>
{

}
