package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.MaterialTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MaterialTypeRepository extends SpringDataRepository<MaterialTypeDO, Long>
{

}
