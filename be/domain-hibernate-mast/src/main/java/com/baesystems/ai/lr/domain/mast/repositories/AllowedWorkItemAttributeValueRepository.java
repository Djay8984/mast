package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AllowedWorkItemAttributeValueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AllowedWorkItemAttributeValueRepository extends SpringDataRepository<AllowedWorkItemAttributeValueDO, Long>
{

}
