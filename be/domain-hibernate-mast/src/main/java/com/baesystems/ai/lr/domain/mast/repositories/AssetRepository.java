package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssetRepository extends SpringDataRepository<AssetDO, Long>
{
    @Query("select a from AssetDO a where"
            + " (:userId is not null and a.checkedOutBy = :userId)"
            + " or (:userId is null and a.checkedOutBy is not null)")
    List<AssetDO> getCheckedOutAssetsForUser(@Param("userId") String userId);
}
