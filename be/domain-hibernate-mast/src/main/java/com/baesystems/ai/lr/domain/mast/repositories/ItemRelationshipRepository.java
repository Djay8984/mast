package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ItemRelationshipRepository extends SpringDataRepository<ItemRelationshipDO, Long>
{
    @Query("select count(r.id) > 0 from ItemRelationshipDO r"
            + " join r.fromItem fromItem"
            + " where r.id = :relationshipId"
            + " and fromItem.id = :itemId"
            + " and r.type.id = :relationshipTypeId"
            + " and r.deleted = false")
    Boolean itemRelationshipExistsForItem(@Param("relationshipId") Long relationshipId, @Param("itemId") Long itemId,
            @Param("relationshipTypeId") Long relationshipTypeId);

    @Query("select count(r.id) > 0 from ItemRelationshipDO r"
            + " join r.toItem toItem"
            + " join r.fromItem fromItem"
            + " where toItem.id = :toItemId"
            + " and fromItem.id = :fromItemId"
            + " and r.type.id = :relationshipTypeId"
            + " and r.deleted = false")
    Boolean relationshipExists(@Param("toItemId") Long toItemId, @Param("fromItemId") Long fromItemId,
            @Param("relationshipTypeId") Long relationshipTypeId);

    @Query(nativeQuery = true, value = "select v.* from MAST_ASSET_AssetItemRelationship as v"
            + " where v.from_version_id ="
            + " (select MAX(vv.from_version_id)"
            + " from MAST_ASSET_AssetItemRelationship as vv"
            + " where v.asset_id = vv.asset_id"
            + " and v.from_item_id = vv.from_item_id"
            + " and v.to_item_id = vv.to_item_id"
            + " and vv.asset_id = :assetId"
            + " and vv.from_item_id = :itemId"
            + " and vv.from_version_id <= :versionId"
            + " and v.to_version_id ="
            + "(select MAX(vvv.to_version_id)"
            + " from MAST_ASSET_AssetItemRelationship as vvv"
            + " where v.asset_id = vvv.asset_id"
            + " and v.from_item_id = vvv.from_item_id"
            + " and v.to_item_id = vvv.to_item_id"
            + " and vvv.asset_id = :assetId"
            + " and vvv.from_item_id = :itemId"
            + " and vvv.to_version_id <= :versionId)"
            + " and (:relationshipType is null or vv.relationship_type_id = :relationshipType))")
    List<ItemRelationshipDO> getItemRelationships(
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId,
            @Param("versionId") Long versionId,
            @Param("relationshipType") Long relationshipType);

    @Query("select r.id from ItemRelationshipDO r"
            + " join r.toItem toItem"
            + " join r.fromItem fromItem"
            + " where toItem.id = :toItemId"
            + " and toItem.assetId = :assetId"
            + " and toItem.assetVersionId = :toVersion"
            + " and fromItem.id = :fromItemId"
            + " and fromItem.assetId = :assetId"
            + " and fromItem.assetVersionId = :fromVersion"
            + " and r.deleted = false"
            + " and toItem.deleted = false"
            + " and fromItem.deleted = false")
    Long findRelationshipId(
            @Param("assetId") Long assetId,
            @Param("fromItemId") Long fromItemId,
            @Param("fromVersion") Long fromVersion,
            @Param("toItemId") Long toItemId,
            @Param("toVersion") Long toVersion);

    // @Modifying
    // @Query("update ItemRelationshipWithLinksDO itemRelationship"
    // + " join itemRelationship.toItem toItem"
    // + " join itemRelationship.fromItem fromItem"
    // + " set itemRelationship.deleted = true"
    // + " where toItem.id in (:itemId)"
    // + " or fromItem.id in (:itemId)")
    // void bulkLogicalDeleteByItemId(@Param("itemId") List<Long> itemId);

    // @Modifying
    // @Query("update ItemRelationshipWithLinksDO itemRelationship"
    // + " set itemRelationship.deleted = true"
    // + " where itemRelationship.id = :relationshipId")
    // void logicalDelete(@Param("relationshipId") Long relationshipId);

}
