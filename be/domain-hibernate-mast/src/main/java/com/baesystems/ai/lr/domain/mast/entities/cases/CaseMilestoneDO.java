package com.baesystems.ai.lr.domain.mast.entities.cases;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;

@Entity
@Table(name = "MAST_CASE_CaseMilestone")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CASE_CaseMilestone SET deleted = true WHERE id = ?")
public class CaseMilestoneDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "completion_date")
    private Date completionDate;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "in_scope_flag", nullable = false, columnDefinition = "default 1")
    private Boolean inScope;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "milestone_status_id", nullable = false))})
    private LinkDO milestoneStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "milestone_id", nullable = false))})
    private LinkDO milestone;

    @JoinColumn(name = "case_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CaseDO aCase;

    @OneToMany(mappedBy = "milestone", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @Where(clause = "deleted = false")
    private List<AttachmentLinkDO> attachments;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Date getCompletionDate()
    {
        return completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Boolean getInScope()
    {
        return inScope;
    }

    public void setInScope(final Boolean inScope)
    {
        this.inScope = inScope;
    }

    public LinkDO getMilestoneStatus()
    {
        return milestoneStatus;
    }

    public void setMilestoneStatus(final LinkDO milestoneStatus)
    {
        this.milestoneStatus = milestoneStatus;
    }

    public LinkDO getMilestone()
    {
        return milestone;
    }

    public void setMilestone(final LinkDO milestone)
    {
        this.milestone = milestone;
    }

    public CaseDO getaCase()
    {
        return aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }

    public List<AttachmentLinkDO> getAttachments()
    {
        return attachments;
    }

    public void setAttachments(final List<AttachmentLinkDO> attachments)
    {
        this.attachments = attachments;
    }

}
