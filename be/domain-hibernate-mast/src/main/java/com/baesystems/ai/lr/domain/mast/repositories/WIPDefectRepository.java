package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WIPDefectRepository extends SpringDataRepository<WIPDefectDO, Long>, WIPSequenceRepository, LockingRepository<WIPDefectDO>
{
    @Query("select n from WIPDefectDO n"
            + " where n.job.id = :jobId")
    Page<WIPDefectDO> findAllForJob(Pageable pageable,
            @Param("jobId") Long jobId);

    @Query("select distinct e from WIPDefectDO e"
            + " left join e.items defectItems"
            + " left join defectItems.item item"
            + " where e.job.id = :jobId"
            + " and (:searchString is null or"
            + " (e.incidentDescription like :searchString or"
            + " e.title like :searchString))"
            + " and (coalesce(:categoryList) is null or e.defectCategory.id in (:categoryList))"
            + " and (coalesce(:statusList) is null or e.defectStatus.id in (:statusList))"
            + " and (coalesce(:itemIdList) is null or item.id in (:itemIdList))")
    Page<WIPDefectDO> findAllForJob(Pageable pageable,
            @Param("jobId") Long jobId,
            @Param("searchString") String searchString,
            @Param("categoryList") List<Long> categoryList,
            @Param("statusList") List<Long> statusList,
            @Param("itemIdList") List<Long> itemIdList);

    @Query("select defect.id from WIPDefectDO defect"
            + " where defect.job.id = :jobId and defect.id = :defectId")
    Long findDefectForJob(@Param("jobId") Long jobId,
            @Param("defectId") Long defectId);

    @Query("select defect from WIPDefectDO defect"
            + " where defect.job.id = :jobId")
    List<WIPDefectDO> findDefectByJobId(@Param("jobId") Long jobId);

    @Query("select defect from WIPDefectDO defect"
            + " where defect.job.id = :jobId"
            + " and defect.defectStatus.id in (:statusIds)")
    List<WIPDefectDO> findDefectByJobId(@Param("jobId") Long jobId, @Param("statusIds") List<Long> statusIds);

    @Modifying()
    @Query("update WIPDefectDO wipDefect set wipDefect.defectStatus = :status "
            + " where wipDefect.id = :wipDefectId and wipDefect.defectStatus <> :status ")
    Integer updateStatus(@Param("wipDefectId") Long wipDefectId, @Param("status") DefectStatusDO status);

    @Query("select defect.parent.id from WIPDefectDO defect"
            + " where defect.id = :defectId")
    Long findParentId(@Param("defectId") Long defectId);
}
