package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDeficiencyDO;

public interface WIPDeficiencyRepository extends LockingRepository<WIPDeficiencyDO>, WIPFaultRepositoryHelper<WIPDeficiencyDO>
{
    @Query("select n from WIPDeficiencyDO n"
            + " where n.job.id = :jobId")
    Page<WIPDeficiencyDO> findAllForJob(Pageable pageable,
            @Param("jobId") Long jobId);
}
