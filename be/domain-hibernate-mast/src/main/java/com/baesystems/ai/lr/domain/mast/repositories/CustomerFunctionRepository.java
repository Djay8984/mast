package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CustomerFunctionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CustomerFunctionRepository extends SpringDataRepository<CustomerFunctionDO, Long>
{

}
