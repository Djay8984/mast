package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.SchedulingDueTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface SchedulingDueTypeRepository extends SpringDataRepository<SchedulingDueTypeDO, Long>
{

}
