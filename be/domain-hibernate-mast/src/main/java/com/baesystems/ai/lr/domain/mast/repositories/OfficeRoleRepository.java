package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.OfficeRoleDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface OfficeRoleRepository extends SpringDataRepository<OfficeRoleDO, Long>
{

}
