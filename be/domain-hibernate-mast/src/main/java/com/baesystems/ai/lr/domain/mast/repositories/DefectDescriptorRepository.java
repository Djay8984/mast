package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectDescriptorDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectDescriptorRepository extends SpringDataRepository<DefectDescriptorDO, Long>
{

}
