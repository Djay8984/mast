package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.RiskAssessmentStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface RiskAssessmentStatusRepository extends SpringDataRepository<RiskAssessmentStatusDO, Long>
{

}
