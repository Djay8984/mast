package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.customers.PartyDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface PartyRepository extends SpringDataRepository<PartyDO, Long>
{

    @Query("select u from PartyDO u where u.name like :search or u.id like :search")
    Page<PartyDO> findAll(Pageable pageable, @Param("search") String search);
}
