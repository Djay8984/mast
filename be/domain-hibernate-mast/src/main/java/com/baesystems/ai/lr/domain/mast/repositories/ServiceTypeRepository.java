package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ServiceTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ServiceTypeRepository extends SpringDataRepository<ServiceTypeDO, Long>
{

}
