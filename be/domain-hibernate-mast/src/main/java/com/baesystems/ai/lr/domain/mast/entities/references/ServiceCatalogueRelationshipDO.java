package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_ServiceCatalogueRelationship")
public class ServiceCatalogueRelationshipDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "to_service_catalogue_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceCatalogueDO toServiceCatalogue;

    @JoinColumn(name = "from_service_catalogue_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceCatalogueDO fromServiceCatalogue;

    @JoinColumn(name = "service_catalogue_relationship_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceCatalogueRelationshipTypeDO relationshipType;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public ServiceCatalogueDO getToServiceCatalogue()
    {
        return toServiceCatalogue;
    }

    public void setToServiceCatalogue(final ServiceCatalogueDO toServiceCatalogue)
    {
        this.toServiceCatalogue = toServiceCatalogue;
    }

    public ServiceCatalogueDO getFromServiceCatalogue()
    {
        return fromServiceCatalogue;
    }

    public void setFromServiceCatalogue(final ServiceCatalogueDO fromServiceCatalogue)
    {
        this.fromServiceCatalogue = fromServiceCatalogue;
    }

    public ServiceCatalogueRelationshipTypeDO getRelationshipType()
    {
        return relationshipType;
    }

    public void setRelationshipType(final ServiceCatalogueRelationshipTypeDO relationshipType)
    {
        this.relationshipType = relationshipType;
    }
}
