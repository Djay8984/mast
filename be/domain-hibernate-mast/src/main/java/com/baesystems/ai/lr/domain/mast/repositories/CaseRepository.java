package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;

public interface CaseRepository extends LockingRepository<CaseDO>, SpringDataRepository<CaseDO, Long>
{

    @Query("select distinct aCase from CaseDO aCase"
            + " left join aCase.caseStatus caseStatus"
            + " left join aCase.surveyors caseSurveyor"
            + " left join caseSurveyor.surveyor surveyor"
            + " left join aCase.asset asset"
            + " where (coalesce(:#{#query.caseStatusId}) is null or aCase.caseStatus.id in (:#{#query.caseStatusId}))"
            + " and (coalesce(:#{#query.caseTypeId}) is null or aCase.caseType.id in (:#{#query.caseTypeId}))"
            + " and (coalesce(:#{#query.employeeId}) is null or caseSurveyor.surveyor.id in (:#{#query.employeeId}))"
            + " and (coalesce(:#{#query.assetId}) is null or aCase.asset.id in (:#{#query.assetId}))")
    Page<CaseDO> findAll(Pageable pageable, @Param("query") CaseQueryDto query);

    @Query("select aCase.id from CaseDO aCase"
            + " where aCase.asset.id = :assetId"
            + " and aCase.caseType.id = :caseTypeId"
            + " and aCase.caseStatus.id in (:caseStatusId)")
    List<Long> findDuplicateCasesByAssetId(@Param("assetId") Long assetId, @Param("caseTypeId") Long caseTypeId,
            @Param("caseStatusId") List<Long> caseStatusId);

    @Query("select u from CaseDO u"
            + " where u.asset.id = :assetId"
            + " and u.caseStatus.id in (:caseStatuses)")
    List<CaseDO> findCasesByAssetId(@Param("assetId") Long assetId, @Param("caseStatuses") List<Long> caseStatuses);

    @Query("select count(u) from CaseDO u"
            + " where u.asset.id = :assetId")
    Long countCasesForAssetId(@Param("assetId") Long assetId);

    @Query("select u.caseStatus.id from CaseDO u"
            + " where u.id = :caseId")
    Long findCaseStatusId(@Param("caseId") Long caseId);

    @Query("select u.previousCaseStatus.id from CaseDO u"
            + " where u.id = :caseId")
    Long findPreviousCaseStatusId(@Param("caseId") Long caseId);

    @Modifying
    @Query("update CaseDO u set u.caseStatus.id = :statusId "
            + " where u.id = :caseId")
    void updateCaseStatus(@Param("caseId") Long caseId, @Param("statusId") Long statusId);
}
