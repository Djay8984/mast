package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AttachmentTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AttachmentTypeRepository extends SpringDataRepository<AttachmentTypeDO, Long>
{

}
