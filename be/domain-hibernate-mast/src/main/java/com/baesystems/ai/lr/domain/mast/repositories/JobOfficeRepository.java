package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.jobs.JobOfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface JobOfficeRepository extends SpringDataRepository<JobOfficeDO, Long>
{
}
