package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssetCustomerFunctionRepository extends SpringDataRepository<AssetCustomerFunctionCustomerFunctionTypeDO, Long>,
        HardDeleteRepository
{
}
