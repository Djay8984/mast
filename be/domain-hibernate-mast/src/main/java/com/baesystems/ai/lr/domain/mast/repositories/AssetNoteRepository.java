package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.AssetNoteDO;

public interface AssetNoteRepository extends LockingRepository<AssetNoteDO>, CodicilRepositoryHelper<AssetNoteDO>, SequenceRepository
{
    @Query("select n from AssetNoteDO n"
            + " where n.asset.id = :assetId"
            + " and (:statusId is null"
            + " or (n.status.id = :statusId"
            + " and (:open is null"
            + " or (:open = true and n.imposedDate <= :currentDate)"
            + " or (:open = false and n.imposedDate >:currentDate)"
            + ")))")
    Page<AssetNoteDO> findAssetNotesByAsset(Pageable pageable, @Param("assetId") Long assetId, @Param("statusId") Long statusId,
            @Param("currentDate") Date currentDate, @Param("open") Boolean isOpen);

    @Query("select n from AssetNoteDO n"
            + " join n.job j"
            + " where j.id = :jobId")
    Page<AssetNoteDO> findAssetNotesByJob(Pageable pageable, @Param("jobId") Long jobId);

    @Query("select n from AssetNoteDO n"
            + " join n.job j"
            + " where j.id = :jobId")
    List<AssetNoteDO> findAssetNotesByJob(@Param("jobId") Long jobId);

    @Query("select count(note.id) from AssetNoteDO note"
            + " where note.asset.id = :assetId and note.id = :assetNoteId")
    Integer assetNoteExistsForAsset(@Param("assetId") Long assetId, @Param("assetNoteId") Long assetNoteId);

    @Query("select new map (note.template.id as " + TEMPLATE_COLUMN_NAME + ", count(distinct note.asset) as " + ASSET_COUNT_COLUMN_NAME + ")"
            + " from AssetNoteDO note"
            + " join note.asset asset"
            + " join asset.publishedVersion publishedAsset"
            + " where note.template.id in (:templateIds)"
            + " and publishedAsset.assetLifecycleStatus.id in (:assetLifecycleStatuses)"
            + " group by note.template.id")
    List<Map<String, Long>> countAssetsWithAssetNotesForTemplate(
            @Param("templateIds") List<Long> templateIds,
            @Param("assetLifecycleStatuses") List<Long> assetLifecycleStatuses);

    @Query("select n from AssetNoteDO n"
            + " where n.id = :assetNoteId")
    AssetNoteDO findAssetNote(@Param("assetNoteId") Long assetNoteId);
}
