package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_DefectCategory_DefectValue")
public class DefectCategoryValueDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "defect_value_id", nullable = false)
    private Long defectValue;

    @Column(name = "defect_category_id", nullable = false)
    private Long defectCategory;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getDefectValue()
    {
        return this.defectValue;
    }

    public void setDefectValue(final Long defectValue)
    {
        this.defectValue = defectValue;
    }

    public Long getDefectCategory()
    {
        return this.defectCategory;
    }

    public void setDefectCategory(final Long defectCategory)
    {
        this.defectCategory = defectCategory;
    }


}
