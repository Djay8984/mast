package com.baesystems.ai.lr.domain.mast.entities.cases;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;

@Entity
@Table(name = "MAST_CASE_Case")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_CASE_Case SET deleted = true WHERE id = ?")
public class CaseDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "case_type_id", nullable = false))})
    private LinkDO caseType;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "case_status_id", nullable = false))})
    private LinkDO caseStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "previous_case_status_id"))})
    private LinkDO previousCaseStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "pre_eic_inspection_status_id"))})
    private LinkDO preEicInspectionStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "risk_assessment_status_id"))})
    private LinkDO riskAssessmentStatus;

    @JoinColumn(name = "asset_id")
    @OneToOne(cascade = {CascadeType.REFRESH})
    private AssetDO asset;

    @Column(name = "toc_acceptance_date")
    @Temporal(TemporalType.DATE)
    private Date tocAcceptanceDate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "flag_state_id"))})
    private LinkDO flagState;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "proposed_flag_state_id"))})
    private LinkDO proposedFlagState;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "aCase", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<CaseSurveyorDO> surveyors;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "aCase", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<CaseOfficeDO> offices;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "aCase", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<CaseMilestoneDO> milestones;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "class_maintenance_status_id"))})
    private LinkDO classMaintenanceStatus;

    @Column(name = "contract_reference", length = 30)
    private String contractReferenceNumber;

    @Column(name = "created_on", updatable = false)
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "case_acceptance_date")
    @Temporal(TemporalType.DATE)
    private Date caseAcceptanceDate;

    @Column(name = "case_closed_date")
    @Temporal(TemporalType.DATE)
    private Date caseClosedDate;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "aCase", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<AttachmentLinkDO> attachments;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "iacs_society_id"))})
    private LinkDO losingSociety;

    @Column(name = "asset_name", length = 100)
    private String assetName;

    @Column(name = "imo_number")
    private Long imoNumber;

    @Column(name = "gross_tonnage")
    private Double grossTonnage;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_lifecycle_status_id"))})
    private LinkDO assetLifecycleStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_type_id"))})
    private LinkDO assetType;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "class_status_id"))})
    private LinkDO classStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "registered_port_id"))})
    private LinkDO registeredPort;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "society_ruleset_id"))})
    private LinkDO ruleSet;

    @Column(name = "hull_class_notation", length = 500)
    private String classNotation;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "co_classification_society_id"))})
    private LinkDO coClassificationSociety;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "aCase", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<CaseCustomerDO> customers;

    @Column(name = "build_date")
    @Temporal(TemporalType.DATE)
    private Date buildDate;

    @Column(name = "estimated_build_date")
    @Temporal(TemporalType.DATE)
    private Date estimatedBuildDate;

    @Column(name = "date_of_registry")
    @Temporal(TemporalType.DATE)
    private Date dateOfRegistry;

    @Column(name = "keel_laying_date")
    @Temporal(TemporalType.DATE)
    private Date keelLayingDate;

    @Column(name = "status_reason", length = 500)
    private String statusReason;

    @OneToMany(cascade = {CascadeType.REFRESH}, mappedBy = "aCase")
    private List<JobDO> jobs;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getCaseType()
    {
        return this.caseType;
    }

    public void setCaseType(final LinkDO caseType)
    {
        this.caseType = caseType;
    }

    public LinkDO getCaseStatus()
    {
        return this.caseStatus;
    }

    public void setCaseStatus(final LinkDO caseStatus)
    {
        this.caseStatus = caseStatus;
    }

    public LinkDO getPreviousCaseStatus()
    {
        return this.previousCaseStatus;
    }

    public void setPreviousCaseStatus(final LinkDO previousCaseStatus)
    {
        this.previousCaseStatus = previousCaseStatus;
    }

    public LinkDO getPreEicInspectionStatus()
    {
        return this.preEicInspectionStatus;
    }

    public void setPreEicInspectionStatus(final LinkDO preEicInspectionStatus)
    {
        this.preEicInspectionStatus = preEicInspectionStatus;
    }

    public LinkDO getRiskAssessmentStatus()
    {
        return this.riskAssessmentStatus;
    }

    public void setRiskAssessmentStatus(final LinkDO riskAssessmentStatus)
    {
        this.riskAssessmentStatus = riskAssessmentStatus;
    }

    public AssetDO getAsset()
    {
        return this.asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

    public Date getTocAcceptanceDate()
    {
        return this.tocAcceptanceDate;
    }

    public void setTocAcceptanceDate(final Date tocAcceptanceDate)
    {
        this.tocAcceptanceDate = tocAcceptanceDate;
    }

    public LinkDO getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final LinkDO flagState)
    {
        this.flagState = flagState;
    }

    public LinkDO getProposedFlagState()
    {
        return this.proposedFlagState;
    }

    public void setProposedFlagState(final LinkDO proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public List<CaseSurveyorDO> getSurveyors()
    {
        return this.surveyors;
    }

    public void setSurveyors(final List<CaseSurveyorDO> surveyors)
    {
        this.surveyors = surveyors;
    }

    public List<CaseOfficeDO> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<CaseOfficeDO> offices)
    {
        this.offices = offices;
    }

    public List<CaseMilestoneDO> getMilestones()
    {
        return this.milestones;
    }

    public void setMilestones(final List<CaseMilestoneDO> milestones)
    {
        this.milestones = milestones;
    }

    public LinkDO getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final LinkDO classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public String getContractReferenceNumber()
    {
        return this.contractReferenceNumber;
    }

    public void setContractReferenceNumber(final String contractReferenceNumber)
    {
        this.contractReferenceNumber = contractReferenceNumber;
    }

    public Date getCreatedOn()
    {
        return this.createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getCaseAcceptanceDate()
    {
        return this.caseAcceptanceDate;
    }

    public void setCaseAcceptanceDate(final Date caseAcceptanceDate)
    {
        this.caseAcceptanceDate = caseAcceptanceDate;
    }

    public List<AttachmentLinkDO> getAttachments()
    {
        return this.attachments;
    }

    public void setAttachments(final List<AttachmentLinkDO> attachments)
    {
        this.attachments = attachments;
    }

    public LinkDO getLosingSociety()
    {
        return this.losingSociety;
    }

    public void setLosingSociety(final LinkDO losingSociety)
    {
        this.losingSociety = losingSociety;
    }

    public String getAssetName()
    {
        return this.assetName;
    }

    public void setAssetName(final String assetName)
    {
        this.assetName = assetName;
    }

    public Long getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final Long imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public LinkDO getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final LinkDO assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public LinkDO getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final LinkDO assetType)
    {
        this.assetType = assetType;
    }

    public LinkDO getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final LinkDO classStatus)
    {
        this.classStatus = classStatus;
    }

    public LinkDO getRegisteredPort()
    {
        return this.registeredPort;
    }

    public void setRegisteredPort(final LinkDO registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public LinkDO getRuleSet()
    {
        return this.ruleSet;
    }

    public void setRuleSet(final LinkDO ruleSet)
    {
        this.ruleSet = ruleSet;
    }

    public String getClassNotation()
    {
        return this.classNotation;
    }

    public void setClassNotation(final String classNotation)
    {
        this.classNotation = classNotation;
    }

    public LinkDO getCoClassificationSociety()
    {
        return this.coClassificationSociety;
    }

    public void setCoClassificationSociety(final LinkDO coClassificationSociety)
    {
        this.coClassificationSociety = coClassificationSociety;
    }

    public List<CaseCustomerDO> getCustomers()
    {
        return this.customers;
    }

    public void setCustomers(final List<CaseCustomerDO> customers)
    {
        this.customers = customers;
    }

    public Date getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public Date getEstimatedBuildDate()
    {
        return this.estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public Date getKeelLayingDate()
    {
        return this.keelLayingDate;
    }

    public void setKeelLayingDate(final Date keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

    public String getStatusReason()
    {
        return this.statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Date getCaseClosedDate()
    {
        return this.caseClosedDate;
    }

    public void setCaseClosedDate(final Date caseClosedDate)
    {
        this.caseClosedDate = caseClosedDate;
    }

    public Date getDateOfRegistry()
    {
        return this.dateOfRegistry;
    }

    public void setDateOfRegistry(final Date dateOfRegistry)
    {
        this.dateOfRegistry = dateOfRegistry;
    }

    public List<JobDO> getJobs()
    {
        return this.jobs;
    }

    public void setJobs(final List<JobDO> jobs)
    {
        this.jobs = jobs;
    }
}
