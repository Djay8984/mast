package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ClassRecommendationDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ClassRecommendationRepository extends SpringDataRepository<ClassRecommendationDO, Long>
{

}
