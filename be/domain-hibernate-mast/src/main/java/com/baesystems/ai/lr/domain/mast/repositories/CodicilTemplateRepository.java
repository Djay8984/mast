package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CodicilTemplateDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CodicilTemplateRepository extends SpringDataRepository<CodicilTemplateDO, Long>
{

}
