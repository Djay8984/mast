package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_CaseStatusTransition")
public class CaseStatusTransitionDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "from_status_id")
    private Long fromStatusId;

    @Column(name = "to_status_id")
    private Long toStatusId;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long finalId)
    {
        id = finalId;
    }

    public Long getFromStatusId()
    {
        return fromStatusId;
    }

    public void setFromStatusId(final Long finalFromStatusId)
    {
        fromStatusId = finalFromStatusId;
    }

    public Long getToStatusId()
    {
        return toStatusId;
    }

    public void setToStatusId(final Long finalToStatusId)
    {
        toStatusId = finalToStatusId;
    }
}
