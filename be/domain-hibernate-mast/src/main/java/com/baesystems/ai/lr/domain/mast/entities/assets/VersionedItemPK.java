package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.io.Serializable;
import java.util.Objects;

public class VersionedItemPK implements Serializable
{
    private static final long serialVersionUID = -4732492341431441548L;

    private Long id;
    private Long assetId;
    private Long assetVersionId;

    public VersionedItemPK()
    {
    }

    public VersionedItemPK(final Long id, final Long assetId, final Long version)
    {
        this.id = id;
        this.assetId = assetId;
        this.assetVersionId = version;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        final VersionedItemPK that = (VersionedItemPK) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(assetId, that.assetId) &&
                Objects.equals(assetVersionId, that.assetVersionId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, assetId, assetVersionId);
    }
}
