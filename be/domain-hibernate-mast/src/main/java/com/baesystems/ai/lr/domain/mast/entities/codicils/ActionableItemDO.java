package com.baesystems.ai.lr.domain.mast.entities.codicils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@DiscriminatorValue("2")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_Codicil SET deleted = true WHERE id = ?")
public class ActionableItemDO extends CodicilDO
{

}
