package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ProductTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ProductTypeRepository extends SpringDataRepository<ProductTypeDO, Long>
{

}
