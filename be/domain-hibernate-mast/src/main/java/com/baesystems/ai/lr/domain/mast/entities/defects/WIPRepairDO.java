package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.domain.mast.entities.references.MaterialDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.RepairTypeDO;

@Entity
@Table(name = "MAST_DEFECT_WIP_Repair")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_WIP_Repair SET deleted = true WHERE id = ?")
public class WIPRepairDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "prompt_thorough_flag")
    private Boolean promptThoroughRepair;

    @Column(name = "description", length = 2000, nullable = false)
    private String description;

    @Column(name = "repair_type_description", length = 5000)
    private String repairTypeDescription;

    @Column(name = "narrative", length = 1000)
    private String narrative;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinTable(name = "MAST_DEFECT_WIP_Repair_RepairType", joinColumns = @JoinColumn(name = "repair_id"), inverseJoinColumns = @JoinColumn(name = "repair_type_id"))
    @Where(clause = "deleted = false")
    private List<RepairTypeDO> repairTypes;

    @JoinColumn(name = "action_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private RepairActionDO repairAction;

    @JoinColumn(name = "defect_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WIPDefectDO defect;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinTable(name = "MAST_DEFECT_WIP_Repair_Material", joinColumns = @JoinColumn(name = "repair_id"), inverseJoinColumns = @JoinColumn(name = "material_used_id"))
    @Where(clause = "deleted = false")
    private List<MaterialDO> materialsUsed;

    @OneToMany(mappedBy = "repair", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<WIPRepairItemDO> repairs;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parent;

    @Column(name = "confirmed", nullable = false)
    private Boolean confirmed;

    @JoinColumn(name = "codicil_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPCoCDO codicil;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Boolean getPromptThoroughRepair()
    {
        return promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public List<RepairTypeDO> getRepairTypes()
    {
        return repairTypes;
    }

    public void setRepairTypes(final List<RepairTypeDO> repairTypes)
    {
        this.repairTypes = repairTypes;
    }

    public RepairActionDO getRepairAction()
    {
        return repairAction;
    }

    public void setRepairAction(final RepairActionDO repairAction)
    {
        this.repairAction = repairAction;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getRepairTypeDescription()
    {
        return repairTypeDescription;
    }

    public void setRepairTypeDescription(final String repairTypeDescription)
    {
        this.repairTypeDescription = repairTypeDescription;
    }

    public String getNarrative()
    {
        return this.narrative;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public List<MaterialDO> getMaterialsUsed()
    {
        return this.materialsUsed;
    }

    public void setMaterialsUsed(final List<MaterialDO> materialsUsed)
    {
        this.materialsUsed = materialsUsed;
    }

    public List<WIPRepairItemDO> getRepairs()
    {
        return this.repairs;
    }

    public void setRepairs(final List<WIPRepairItemDO> repairs)
    {
        this.repairs = repairs;
    }

    public WIPDefectDO getDefect()
    {
        return this.defect;
    }

    public void setDefect(final WIPDefectDO defect)
    {
        this.defect = defect;
    }

    public Boolean getConfirmed()
    {
        return this.confirmed;
    }

    public void setConfirmed(final Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    public WIPCoCDO getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final WIPCoCDO codicil)
    {
        this.codicil = codicil;
    }

    public LinkDO getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }
}
