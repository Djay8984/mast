package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ConfidentialityTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ConfidentialityTypeRepository extends SpringDataRepository<ConfidentialityTypeDO, Long>
{

}
