package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.BaseCodicilDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

@NoRepositoryBean
public interface CodicilRepositoryHelper<T extends BaseCodicilDO> extends SpringDataRepository<T, Long>
{
    String TEMPLATE_COLUMN_NAME = "template";
    String ASSET_COUNT_COLUMN_NAME = "assetCount";

    @Query("select n from #{#entityName} n"
            + " where n.asset.id = :assetId")
    Page<T> findAll(Pageable pageable,
            @Param("assetId") Long assetId);

    @Query("select distinct e from #{#entityName} e"
            + " where e.asset.id = :assetId"
            + " and (:searchString is null or"
            + " (e.description like :searchString or"
            + " e.title like :searchString or"
            + " e.surveyorGuidance like :searchString))"
            + " and (coalesce(:categoryList) is null or e.category.id in (:categoryList))"
            + " and (coalesce(:confidentialityList) is null or e.confidentialityType.id in (:confidentialityList))"
            + " and (coalesce(:itemIdList) is null or e.assetItem.id in (:itemIdList))"
            + " and (:dueDateMin is null or e.dueDate >= :dueDateMin)"
            + " and (:dueDateMax is null or e.dueDate <= :dueDateMax)"
            + " and (coalesce(:statusList) is null"
            + " or (e.status.id in (:statusList)"
            + " and (:open is null or e.status.id <> :openStatus"
            + " or (:open = true and e.imposedDate <= :currentDate)"
            + " or (:open = false and e.imposedDate >:currentDate)"
            + ")))")
    Page<T> findAll(Pageable pageable,
            @Param("assetId") Long assetId,
            @Param("searchString") String searchString,
            @Param("categoryList") List<Long> categoryList,
            @Param("statusList") List<Long> statusList,
            @Param("confidentialityList") List<Long> confidentialityList,
            @Param("itemIdList") List<Long> itemIdList,
            @Param("dueDateMin") Date dueDateMin,
            @Param("dueDateMax") Date dueDateMax,
            @Param("currentDate") Date currentDate,
            @Param("open") Boolean isOpen,
            @Param("openStatus") Long openStatusId);
}
