package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ClassDepartmentDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ClassDepartmentRepository extends SpringDataRepository<ClassDepartmentDO, Long>
{

}
