package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;

@Entity
@DiscriminatorValue(value = "2")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_FAULT_Fault SET deleted = true WHERE id = ?")
public class DeficiencyDO extends FaultDO
{
    @JoinColumn(name = "asset_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private AssetDO asset;

    public AssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }
}
