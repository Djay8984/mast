package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.MaterialDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MaterialRepository extends SpringDataRepository<MaterialDO, Long>
{

}
