package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_AssetItemTypeRelationship")
public class ItemTypeRelationshipDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "from_item_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private ItemTypeDO fromItemType;

    @JoinColumn(name = "to_item_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private ItemTypeDO toItemType;

    @JoinColumn(name = "relationship_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private ItemRelationshipTypeDO itemRelationshipType;

    @Column(name = "min_occurance", nullable = false)
    private Integer minOccurs;

    @Column(name = "max_occurance", nullable = false)
    private Integer maxOccurs;

    @Column(name = "asset_category_id", nullable = false)
    private Integer assetCategoryId;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public ItemTypeDO getFromItemType()
    {
        return fromItemType;
    }

    public void setFromItemType(final ItemTypeDO fromItemType)
    {
        this.fromItemType = fromItemType;
    }

    public ItemTypeDO getToItemType()
    {
        return toItemType;
    }

    public void setToItemType(final ItemTypeDO toItemType)
    {
        this.toItemType = toItemType;
    }

    public ItemRelationshipTypeDO getItemRelationshipType()
    {
        return itemRelationshipType;
    }

    public void setItemRelationshipType(final ItemRelationshipTypeDO itemRelationshipType)
    {
        this.itemRelationshipType = itemRelationshipType;
    }

    public Integer getMinOccurs()
    {
        return minOccurs;
    }

    public void setMinOccurs(final Integer minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public Integer getMaxOccurs()
    {
        return maxOccurs;
    }

    public void setMaxOccurs(final Integer maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public Integer getAssetCategoryId()
    {
        return assetCategoryId;
    }

    public void setAssetCategoryId(final Integer assetCategoryId)
    {
        this.assetCategoryId = assetCategoryId;
    }
}
