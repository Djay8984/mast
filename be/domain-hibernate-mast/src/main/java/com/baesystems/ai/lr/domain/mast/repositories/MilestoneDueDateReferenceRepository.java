package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneDueDateReferenceDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MilestoneDueDateReferenceRepository extends SpringDataRepository<MilestoneDueDateReferenceDO, Long>
{

}
