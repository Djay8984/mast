package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ResolutionStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ResolutionStatusRepository extends SpringDataRepository<ResolutionStatusDO, Long>
{

}
