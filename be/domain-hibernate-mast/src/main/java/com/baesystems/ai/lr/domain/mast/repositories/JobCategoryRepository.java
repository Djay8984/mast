package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.JobCategoryDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface JobCategoryRepository extends SpringDataRepository<JobCategoryDO, Long>
{
}
