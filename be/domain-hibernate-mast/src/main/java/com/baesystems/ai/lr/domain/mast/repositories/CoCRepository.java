package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;

public interface CoCRepository extends LockingRepository<CoCDO>, CodicilRepositoryHelper<CoCDO>, CourseOfActionRepositoryHelper<CoCDO>
{
    @Query("select c from CoCDO c"
            + " where c.asset.id = :assetId"
            + " and (c.status.id = :statusId or :statusId is null)")
    Page<CoCDO> findCoCByAsset(Pageable pageable, @Param("assetId") Long assetId, @Param("statusId") Long statusId);

    @Query("select c from CoCDO c"
            + " where c.defect.id = :defectId"
            + " and c.asset.id = :assetId"
            + " and (c.status.id = :statusId or :statusId is null)")
    Page<CoCDO> findCoCByDefect(Pageable pageable, @Param("assetId") Long assetId, @Param("defectId") Long defectId,
            @Param("statusId") Long statusId);

    @Query("select count(c.id) from CoCDO c"
            + " where c.asset.id = :assetId and c.id = :cocId")
    Integer coCExistsForAsset(@Param("assetId") Long assetId, @Param("cocId") Long cocId);

    @Query("select coc from CoCDO coc"
            + " where coc.id = :cocId")
    CoCDO findCoc(@Param("cocId") Long cocId);
}
