package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.MilestoneDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface MilestoneRepository extends SpringDataRepository<MilestoneDO, Long>
{
    @Query("select u from MilestoneDO u"
            + " where u.caseType.id = :caseTypeId")
    List<MilestoneDO> findMilestoneByCaseType(@Param("caseTypeId") Long caseTypeId);

    @Query("select milestone.workingDaysOffset from MilestoneDO milestone"
            + " where milestone.id = :milestoneId")
    Integer findMilestoneWorkingDaysOffset(@Param("milestoneId") Long milestoneId);

    @Query("select milestone.id from MilestoneDO milestone"
            + " join milestone.predecessorMilestones predecessorMilestone"
            + " where predecessorMilestone.predecessorMilestone.id = :parentMilestoneId")
    List<Long> findChildMilestones(@Param("parentMilestoneId") Long paranetMinestoneId);

    @Query("select predecessorMilestone.predecessorMilestone.id from MilestoneDO milestone"
            + " join milestone.predecessorMilestones predecessorMilestone"
            + " where milestone.id = :childMilestoneId")
    List<Long> findParentMilestones(@Param("childMilestoneId") Long childMinestoneId);

    @Query("select milestone.mandatory from MilestoneDO milestone"
            + " where milestone.id = :milestoneId")
    Boolean isMilestoneMandatory(@Param("milestoneId") Long milestoneId);

    @Query("select milestone.dueDateReference.id from MilestoneDO milestone"
            + " where milestone.id = :milestoneId")
    Long findMilestoneDueDateReferenceId(@Param("milestoneId") Long milestoneId);

    @Query("select predecessorMilestone.predecessorMilestone.id from MilestoneDO milestone"
            + " join milestone.predecessorMilestones predecessorMilestone"
            + " where milestone.id = :childMilestoneId"
            + " and predecessorMilestone.dateDependency = true")
    Long findDateDependentParentMilestone(@Param("childMilestoneId") Long childMinestoneId);
}
