package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;

public interface VersionedItemRepository extends BaseItemRepository<VersionedItemDO>
{

    @Query(nativeQuery = true, value = "select v.* from MAST_ASSET_VersionedAssetItem as v"
            + " where v.asset_version_id ="
            + " (select MAX(item.asset_version_id) from MAST_ASSET_VersionedAssetItem item"
            + " where v.id = item.id"
            + " and item.asset_id = :assetId"
            + " and item.asset_version_id <= :assetVersionId"
            + " and (coalesce(:itemId) is null or item.id in (:itemId))"
            + " and (coalesce(:itemTypeId) is null or item.item_type_id in (:itemTypeId))"
            + " and ((:itemName is null and coalesce(:typeIdsMatchingName) is null)"
            + " or (:itemName is not null and item.name like :itemName)"
            + " or (coalesce(:typeIdsMatchingName) is not null and item.item_type_id in (:typeIdsMatchingName))))")
    List<VersionedItemDO> findItemsByAssetId(
            @Param("assetId") Long assetId,
            @Param("assetVersionId") Long assetVersionId,
            @Param("typeIdsMatchingName") List<Long> typeIdsMatchingName,
            @Param("itemId") List<Long> itemId,
            @Param("itemTypeId") List<Long> itemTypeId,
            @Param("itemName") String itemName);

    // nativeQuery pagination example

    // @Query(
    // value = "select * from (select rownum() as RN, u.* from SD_User u) where RN between ?#{ #pageable.offset -1} and
    // ?#{#pageable.offset + #pageable.pageSize}",
    // countQuery = "select count(u.id) from SD_User u", nativeQuery = true)
    // Page<User> findUsersInNativeQueryWithPagination(Pageable pageable);

    @Query("select count(item) from ItemDO item"
            + " where item.id in (:itemIds)")
    Integer countMatchingIds(@Param("itemIds") List<Long> itemIds);

    @Query("select item.itemType.id = :typeId from VersionedItemDO item"
            + " where item.id = :itemId")
    Boolean isItemOfType(@Param("itemId") Long itemId, @Param("typeId") Long typeId);

    @Modifying
    @Query("UPDATE VersionedItemDO item SET item.name = SUBSTRING(:itemName, 1, 200) WHERE item.id = :itemId")
    void updateItemName(@Param("itemId") Long itemId, @Param("itemName") String itemName);

    @Modifying
    @Query("UPDATE VersionedItemDO item SET item.reviewed = :reviewed WHERE item.id in (:itemIds)")
    void updateReviewedStateOfItems(@Param("reviewed") Boolean reviewed, @Param("itemIds") Set<Long> itemIds);

    @Modifying
    @Query("UPDATE VersionedItemDO item SET item.decommissioned = :flagState"
            + " WHERE item.assetId = :assetId"
            + " AND item.assetVersionId = :versionId"
            + " AND item.id IN (:itemIds)")
    void updateDecommissionedFlag(@Param("flagState") Boolean flagState, @Param("assetId") Long assetId, @Param("itemIds") List<Long> itemIds,
            @Param("versionId") Long versionId);

    @Query("select item.id from VersionedItemDO item join item.asset asset"
            + " where asset.id = :assetId"
            + " and (coalesce(:itemTypeList) is null or item.itemType.id in (:itemTypeList))")
    List<Long> getItemIdsFromAssetAndType(
            @Param("assetId") Long assetId,
            @Param("itemTypeList") List<Long> itemTypeList);

    @Query("select item from VersionedItemDO item"
            + " where item.id = :itemId")
    VersionedItemDO findOneUndeleted(@Param("itemId") Long itemId);

    @Query("select (max(assetVersionId) + 1) from VersionedItemDO va where va.id = :itemId and va.assetId = :assetId")
    Long getNextVersion(@Param("assetId") Long assetId, @Param("itemId") Long itemId);

    @Query(nativeQuery = true, value = "select v.* from MAST_ASSET_VersionedAssetItem as v"
            + " where v.asset_version_id ="
            + " (select MAX(vv.asset_version_id)"
            + " from MAST_ASSET_VersionedAssetItem as vv"
            + " where v.id = vv.id"
            + " and vv.id = :id"
            + " and vv.asset_id = :assetId"
            + " and vv.asset_version_id <= :version)")
    VersionedItemDO findOne(
            @Param("id") Long id,
            @Param("assetId") Long assetId,
            @Param("version") Long version);

    @Query("select count(id) from VersionedItemDO"
            + " where assetId = :assetId"
            + " and assetVersionId = :version")
    Integer countItemsOfSpecificVersion(@Param("assetId") Long assetId,
            @Param("version") Long version);

    @Query(nativeQuery = true, value = "select v.* from MAST_ASSET_VersionedAssetItem as v"
            + " where v.asset_version_id ="
            + " (select MAX(vv.asset_version_id)"
            + " from MAST_ASSET_VersionedAssetItem as vv"
            + " where v.id = vv.id"
            + " and vv.asset_id = :assetId"
            + " and vv.asset_version_id <= :versionId) LIMIT :limit OFFSET :offset")
    List<VersionedItemDO> findItemsByAssetId(
            @Param("assetId") Long assetId,
            @Param("versionId") Long versionId,
            @Param("limit") Long limit,
            @Param("offset") Long offset);

    // @Query("select di from VersionedItemDO di"
    // + " join di.parentItems parentItem"
    // + " where parentItem.id = :fromItemId"
    // + " and di.displayOrder = :displayOrder"
    // + " and di.deleted = false")
    // VersionedItemDO findMatchingDisplayOrder(@Param("displayOrder") Integer displayOrder, @Param("fromItemId") Long
    // fromItemId);

    // @Query("select max(i.displayOrder) from VersionedItemDO i"
    // + " join i.parentItems parentItem"
    // + " where parentItem.id = :parentItemId"
    // + " and i.deleted = false")
    // Integer getMaximumChildDisplayOrder(@Param("parentItemId") Long parentItemId);

    @Query("select distinct v.assetId from VersionedItemDO as v"
            + " where :#{#query.itemTypeId} is not null"
            + " and v.itemType.id = :#{#query.itemTypeId}"
            + " and v.published is true")
    List<Long> findPublishedAssetByItemType(@Param("query") AssetQueryDto query);

    @Query("select item.id from VersionedItemDO item "
            + " where item.assetId = :assetId"
            + " and item.assetVersionId = :assetVersionId")
    List<Long> findUpdatedIds(
            @Param("assetId") Long assetId,
            @Param("assetVersionId") Long assetVersionId);
}
