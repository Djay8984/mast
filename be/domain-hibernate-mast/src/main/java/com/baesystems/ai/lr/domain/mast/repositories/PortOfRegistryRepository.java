package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.PortOfRegistryDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface PortOfRegistryRepository extends SpringDataRepository<PortOfRegistryDO, Long>
{
    @Query("select distinct u from FlagStateDO fs"
            + " left join fs.ports u"
            + " where fs.id = :flagStateId")
    Page<PortOfRegistryDO> findAll(Pageable pageSpecification, @Param("flagStateId") Long flagStateId);
}
