package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface LrEmployeeRoleRepository extends SpringDataRepository<LrEmployeeRoleDO, Long>
{
    LrEmployeeRoleDO findByName(String name);
}
