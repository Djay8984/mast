package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCodicilDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WIPCodicilRepository extends SpringDataRepository<WIPCodicilDO, Long>
{
    @Query("select wipCodicil.parent.id from WIPCodicilDO wipCodicil"
            + " where wipCodicil.id = :wipId")
    Long findParentId(@Param("wipId") Long wipId);
}
