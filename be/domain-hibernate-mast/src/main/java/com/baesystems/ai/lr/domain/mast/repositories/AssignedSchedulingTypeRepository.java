package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AssignedSchedulingTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssignedSchedulingTypeRepository extends SpringDataRepository<AssignedSchedulingTypeDO, Long>
{

}
