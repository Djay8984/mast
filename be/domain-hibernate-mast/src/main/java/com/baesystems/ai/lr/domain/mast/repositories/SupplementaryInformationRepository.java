package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.SupplementaryInformationDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;

public interface SupplementaryInformationRepository extends SpringDataRepository<SupplementaryInformationDO, Long>
{
    @Query("select a from AttachmentLinkDO a "
            + " join a.aCase c"
            + " where c.id = :caseId")
    Page<AttachmentLinkDO> findCaseAttachments(Pageable pageable, @Param("caseId") Long caseId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.aCase.id = :caseId and a.id = :attachmentId")
    Long findCaseAttachment(@Param("caseId") Long caseId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.milestone m"
            + " where m.id = :milestoneId")
    Page<AttachmentLinkDO> findMilestoneAttachments(Pageable pageable, @Param("milestoneId") Long milestoneId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.milestone.id = :milestoneId and a.id = :attachmentId")
    Long findMilestoneAttachment(@Param("milestoneId") Long milestoneId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.service m"
            + " where m.id = :serviceId")
    Page<AttachmentLinkDO> findServiceAttachments(Pageable pageable, @Param("serviceId") Long serviceId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.service.id = :serviceId and a.id = :attachmentId")
    Long findServiceAttachment(@Param("serviceId") Long serviceId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.coC c"
            + " where c.id = :cocId")
    Page<AttachmentLinkDO> findCoCAttachments(Pageable pageable, @Param("cocId") Long cocId);

    @Query("select a.id from AttachmentLinkDO a "
            + " where a.coC.id = :cocId and a.id = :attachmentId")
    Long findCoCAttachment(@Param("cocId") Long cocId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.wipCoC c"
            + " where c.id = :cocId")
    Page<AttachmentLinkDO> findWIPCoCAttachments(Pageable pageable, @Param("cocId") Long cocId);

    @Query("select a.id from AttachmentLinkDO a "
            + " where a.wipCoC.id = :cocId and a.id = :attachmentId")
    Long findWIPCoCAttachment(@Param("cocId") Long cocId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.assetNote c"
            + " where c.id = :assetNoteId")
    Page<AttachmentLinkDO> findAssetNoteAttachments(Pageable pageable, @Param("assetNoteId") Long assetNoteId);

    @Query("select a.id from AttachmentLinkDO a "
            + " where a.assetNote.id = :assetNoteId and a.id = :attachmentId")
    Long findAssetNoteAttachment(@Param("assetNoteId") Long assetNoteId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.wipAssetNote c"
            + " where c.id = :assetNoteId")
    Page<AttachmentLinkDO> findWIPAssetNoteAttachments(Pageable pageable, @Param("assetNoteId") Long assetNoteId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.workItem m"
            + " where m.id = :workItemId")
    Page<AttachmentLinkDO> findWorkItemAttachments(Pageable pageable, @Param("workItemId") Long workItemId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.wipWorkItem m"
            + " where m.id = :wipWorkItemId")
    Page<AttachmentLinkDO> findWIPWorkItemAttachments(Pageable pageable, @Param("wipWorkItemId") Long wipWorkItemId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.survey m"
            + " where m.id = :surveyId")
    Page<AttachmentLinkDO> findSurveyAttachments(Pageable pageable, @Param("surveyId") Long surveyId);

    @Query("select a.id from AttachmentLinkDO a "
            + " where a.wipAssetNote.id = :assetNoteId and a.id = :attachmentId")
    Long findWIPAssetNoteAttachment(@Param("assetNoteId") Long assetNoteId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.asset m"
            + " where m.id = :assetId")
    Page<AttachmentLinkDO> findAssetAttachments(Pageable pageable, @Param("assetId") Long assetId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.asset.id = :assetId and a.id = :attachmentId")
    Long findAssetAttachment(@Param("assetId") Long assetId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.job m"
            + " where m.id = :jobId")
    Page<AttachmentLinkDO> findJobAttachments(Pageable pageable, @Param("jobId") Long jobId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.job.id = :jobId and a.id = :attachmentId")
    Long findJobAttachment(@Param("jobId") Long jobId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.item m"
            + " where m.id = :itemId")
    Page<AttachmentLinkDO> findItemAttachments(Pageable pageable, @Param("itemId") Long itemId);

    @Query("select a from AttachmentLinkDO a "
            + " where a.defect.id = :defectId")
    Page<AttachmentLinkDO> findDefectAttachments(Pageable pageable, @Param("defectId") Long defectId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.actionableItem m"
            + " where m.id = :actionableItemId")
    Page<AttachmentLinkDO> findActionableItemsAttachments(Pageable pageable, @Param("actionableItemId") Long actionableItemId);

    @Query("select a from AttachmentLinkDO a "
            + " join a.wipActionableItem m"
            + " where m.id = :actionableItemId")
    Page<AttachmentLinkDO> findWIPActionableItemsAttachments(Pageable pageable, @Param("actionableItemId") Long actionableItemId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.actionableItem.id = :actionableItemId and a.id = :attachmentId")
    Long findActionableItemAttachment(@Param("actionableItemId") Long actionableItemId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.wipActionableItem.id = :actionableItemId and a.id = :attachmentId")
    Long findWIPActionableItemAttachment(@Param("actionableItemId") Long actionableItemId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.defect.id = :defectId and a.id = :attachmentId")
    Long findDefectAttachment(@Param("defectId") Long defectId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.wipDefect.id = :defectId and a.id = :attachmentId")
    Long findWIPDefectAttachment(@Param("defectId") Long defectId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a"
            + " where a.wipDefect.id = :defectId")
    Page<AttachmentLinkDO> findWIPDefectAttachments(Pageable pageable, @Param("defectId") Long defectId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.item.id = :itemId and a.id = :attachmentId")
    Long findItemAttachment(@Param("itemId") Long itemId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.repair.id = :repairId and a.id = :attachmentId")
    Long findRepairAttachment(@Param("repairId") Long repairId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a"
            + " where a.repair.id = :repairId")
    Page<AttachmentLinkDO> findRepairAttachments(Pageable pageable, @Param("repairId") Long repairId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.wipRepair.id = :wipRepairId and a.id = :attachmentId")
    Long findWIPRepairAttachment(@Param("wipRepairId") Long wipRepairId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a"
            + " where a.wipRepair.id = :wipRepairId")
    Page<AttachmentLinkDO> findWIPRepairAttachments(Pageable pageable, @Param("wipRepairId") Long wipRepairId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.workItem.id = :workItemId and a.id = :attachmentId")
    Long findWorkItemAttachment(@Param("workItemId") Long workItemId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.wipWorkItem.id = :wipWorkItemId and a.id = :attachmentId")
    Long findWIPWorkItemAttachment(@Param("wipWorkItemId") Long wipWorkItemId, @Param("attachmentId") Long attachmentId);

    @Query("select a.id from AttachmentLinkDO a"
            + " where a.survey.id = :surveyId and a.id = :attachmentId")
    Long findSurveyAttachment(@Param("surveyId") Long surveyId, @Param("attachmentId") Long attachmentId);

    @Query("select a from AttachmentLinkDO a" +
            " where (coalesce(:#{#query.defect}) is not null and a.defect.id in (:#{#query.defect}))" +
            " or (coalesce(:#{#query.wipDefect}) is not null and a.wipDefect.id in (:#{#query.wipDefect}))" +
            " or (coalesce(:#{#query.job}) is not null and a.job.id in (:#{#query.job}))" +
            " or (coalesce(:#{#query.coC}) is not null and a.coC.id in (:#{#query.coC}))" +
            " or (coalesce(:#{#query.wipCoC}) is not null and a.wipCoC.id in (:#{#query.wipCoC}))" +
            " or (coalesce(:#{#query.assetNote}) is not null and a.assetNote.id in (:#{#query.assetNote}))" +
            " or (coalesce(:#{#query.wipAssetNote}) is not null and a.wipAssetNote.id in (:#{#query.wipAssetNote}))" +
            " or (coalesce(:#{#query.asset}) is not null and a.asset.id in (:#{#query.asset}))" +
            " or (coalesce(:#{#query.item}) is not null and a.item.id in (:#{#query.item}))" +
            " or (coalesce(:#{#query.milestone}) is not null and a.milestone.id in (:#{#query.milestone}))" +
            " or (coalesce(:#{#query.repair}) is not null and a.repair.id in (:#{#query.repair}))" +
            " or (coalesce(:#{#query.certificate}) is not null and a.certificate.id in (:#{#query.certificate}))" +
            " or (coalesce(:#{#query.report}) is not null and a.report.id in (:#{#query.report}))" +
            " or (coalesce(:#{#query.aCase}) is not null and a.aCase.id in (:#{#query.aCase}))" +
            " or (coalesce(:#{#query.service}) is not null and a.service.id in (:#{#query.service}))" +
            " or (coalesce(:#{#query.actionableItem}) is not null and a.actionableItem.id in (:#{#query.actionableItem}))" +
            " or (coalesce(:#{#query.workItem}) is not null and a.workItem.id in (:#{#query.workItem}))" +
            " or (coalesce(:#{#query.wipWorkItem}) is not null and a.wipWorkItem.id in (:#{#query.wipWorkItem}))" +
            " or (coalesce(:#{#query.survey}) is not null and a.survey.id in (:#{#query.survey}))" +
            " or (coalesce(:#{#query.wipActionableItem}) is not null and a.wipActionableItem.id in (:#{#query.wipActionableItem}))")
    List<AttachmentLinkDO> findAttachmentsByQuery(@Param("query") AttachmentLinkQueryDto attachmentLinkQueryDto);
}
