package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_CertificateType")
public class CertificateTypeDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "form_number", length = 30)
    private String formNumber;

    @Column(name = "version")
    private Double version;

    @JoinColumn(name = "service_catalogue_id")
    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = {CascadeType.REFRESH})
    private ServiceCatalogueDO serviceCatalogue;

    @Column(name = "display_order")
    private Integer displayOrder;

    @Column(name = "description", length = 500)
    private String description;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getFormNumber()
    {
        return formNumber;
    }

    public void setFormNumber(final String formNumber)
    {
        this.formNumber = formNumber;
    }

    public Double getVersion()
    {
        return version;
    }

    public void setVersion(final Double version)
    {
        this.version = version;
    }

    public ServiceCatalogueDO getServiceCatalogue()
    {
        return serviceCatalogue;
    }

    public void setServiceCatalogue(final ServiceCatalogueDO serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

}
