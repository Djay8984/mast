package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ClassMaintenanceStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ClassMaintenanceStatusRepository extends SpringDataRepository<ClassMaintenanceStatusDO, Long>
{

}
