package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.BaseCodicilDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

@NoRepositoryBean
public interface CourseOfActionRepositoryHelper<T extends BaseCodicilDO> extends SpringDataRepository<T, Long>
{
    @Query("select count(e.id) from #{#entityName} e"
            + " where e.defect.id = :defectId"
            + " and (coalesce(:status) is null or e.status.id in (:status))")
    Integer countCoursesOfActionForDefect(@Param("defectId") Long defectId, @Param("status") List<Long> status);
}
