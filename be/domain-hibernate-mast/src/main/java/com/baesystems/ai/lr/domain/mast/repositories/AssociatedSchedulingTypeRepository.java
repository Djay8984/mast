package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AssociatedSchedulingTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AssociatedSchedulingTypeRepository extends SpringDataRepository<AssociatedSchedulingTypeDO, Long>
{

}
