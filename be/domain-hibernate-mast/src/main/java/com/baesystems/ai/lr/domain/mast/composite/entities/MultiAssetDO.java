package com.baesystems.ai.lr.domain.mast.composite.entities;

import com.baesystems.ai.lr.domain.entities.MultiDO;
import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.exception.MastSystemException;

public class MultiAssetDO implements MultiDO
{
    private Long imoNumber;

    private DummyIhsAssetDO ihsAsset;

    private VersionedAssetDO mastAsset;

    public MultiAssetDO(final DummyIhsAssetDO ihsAsset, final VersionedAssetDO mastAsset)
    {
        super();
        this.imoNumber = ihsAsset == null ? mastAsset == null ? null
                : mastAsset.getIhsAsset() == null ? null : mastAsset.getIhsAsset().getId()
                : ihsAsset.getIhsAsset() == null ? null : ihsAsset.getIhsAsset().getId();
        this.ihsAsset = ihsAsset;
        this.mastAsset = mastAsset;
    }

    public Long getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final Long imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public DummyIhsAssetDO getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final DummyIhsAssetDO ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public VersionedAssetDO getMastAsset()
    {
        return this.mastAsset;
    }

    public void setMastAsset(final VersionedAssetDO mastAsset)
    {
        this.mastAsset = mastAsset;
    }

    @Override
    public boolean equals(final Object obj2)
    {
        boolean returnValue = false;

        if (MultiAssetDO.class.isInstance(obj2))
        {
            final MultiAssetDO multiAsset2 = MultiAssetDO.class.cast(obj2);
            returnValue = multiAsset2.getImoNumber() != null && this.imoNumber != null && this.imoNumber.equals(multiAsset2.getImoNumber());
        }

        return returnValue;
    }

    @Override
    public int hashCode()
    {
        int hashCode = super.hashCode();

        if (this.imoNumber != null)
        {
            hashCode = this.imoNumber.hashCode();
        }

        return hashCode;
    }

    @Override
    public Object getContent()
    {
        return this.ihsAsset == null ? this.mastAsset : this.ihsAsset;
    }

    @Override
    public void setContent(final Object content)
    {
        if (this.ihsAsset == null)
        {
            this.ihsAsset = DummyIhsAssetDO.class.cast(content);
        }
        else if (this.mastAsset == null)
        {
            this.mastAsset = VersionedAssetDO.class.cast(content);
        }
        else
        {
            throw new MastSystemException("The calling method should not be trying to overwrite an exsting entity");
        }
    }

}
