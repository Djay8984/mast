package com.baesystems.ai.lr.domain.mast.entities.references;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_REF_JobStatusToRole")
public class JobStatusToRoleDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "job_status_id", nullable = false))
    private LinkDO jobStatus;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "employee_role_id", nullable = false))
    private LinkDO employeeRole;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getJobStatus()
    {
        return jobStatus;
    }

    public void setJobStatus(final LinkDO jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public LinkDO getEmployeeRole()
    {
        return employeeRole;
    }

    public void setEmployeeRole(final LinkDO employeeRole)
    {
        this.employeeRole = employeeRole;
    }
}
