package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;

public interface WIPWorkItemRepository extends LockingRepository<WIPWorkItemDO>, SpringDataRepository<WIPWorkItemDO, Long>
{
    @Query("select workItem from WIPWorkItemDO workItem"
            + " where workItem.survey.job.id = :jobId")
    List<WIPWorkItemDO> findAllForJob(@Param("jobId") Long jobId);

    @Query("select workItem from WIPWorkItemDO workItem"
            + " where workItem.survey.job.id = :jobId"
            + " and workItem.actionTakenDate is not null")
    List<WIPWorkItemDO> findForReportContent(@Param("jobId") Long jobId);

    @Query("select workItem from WIPWorkItemDO workItem"
            + " where workItem.survey.job.id = :jobId"
            + " and workItem.updatedBy = :employeeName"
            + " and day(workItem.actionTakenDate) = day(:queryDate)"
            + " and month(workItem.actionTakenDate) = month(:queryDate)"
            + " and year(workItem.actionTakenDate) = year(:queryDate)")
    List<WIPWorkItemDO> findTasksByJob(@Param("jobId") Long jobId, @Param("employeeName") String employeeName,
            @Param("queryDate") Date queryDate);

    @Query("select workItem from WIPWorkItemDO workItem"
            + " where (coalesce(:#{#query.itemId}) is null or workItem.assetItem.id in (:#{#query.itemId}))"
            + " and (coalesce(:#{#query.surveyId}) is null or workItem.survey.id in (:#{#query.surveyId}))")
    Page<WIPWorkItemDO> findAll(Pageable pageable, @Param("query") WorkItemQueryDto query);
}
