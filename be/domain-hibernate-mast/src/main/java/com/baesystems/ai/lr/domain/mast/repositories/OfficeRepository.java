package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface OfficeRepository extends SpringDataRepository<OfficeDO, Long>
{
    @Query("select distinct office from OfficeDO office"
            + " left join office.officeRole role"
            + " left join office.employees employeeLink"
            + " left join employeeLink.employee employee"
            + " where office.name like :search"
            + " and (role.name = :roleName or :roleName is null)"
            + " and (employee.id = :employeeId or :employeeId is null)")
    Page<OfficeDO> findAll(Pageable pageable, @Param("search") String search, @Param("roleName") String roleName,
            @Param("employeeId") Long employeeId);

    OfficeDO findByName(String code);

    OfficeDO findByCode(String code);

    @Query("select office.name from OfficeDO office"
            + " where office.id = :officeId")
    String findNameById(@Param("officeId") Long officeId);
}
