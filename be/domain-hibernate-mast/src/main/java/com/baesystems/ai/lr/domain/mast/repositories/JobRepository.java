package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.references.JobStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.JobQueryDto;

public interface JobRepository extends LockingRepository<JobDO>, SpringDataRepository<JobDO, Long>
{
    @Override
    Page<JobDO> findAll(Pageable pageable);

    @Query("select j from JobDO j"
            + " join j.asset a"
            + " where a.id = :assetId")
    Page<JobDO> findJobsForAsset(Pageable pageable, @Param("assetId") Long assetId);

    @Query(countQuery = "select count(distinct job) from JobDO job"
            + " left join job.offices link_office"
            + " left join job.employees link_employee"
            + " left join job.asset asset"
            + " left join job.jobStatus status"
            + FIND_ALL_WITH_QUERY_WHERE_CLAUSE, value = "select distinct job from JobDO job"
                    + " left join fetch job.offices link_office"
                    + " left join job.employees link_employee"
                    + " left join job.asset asset"
                    + " left join job.jobStatus status"
                    + FIND_ALL_WITH_QUERY_WHERE_CLAUSE)
    Page<JobDO> findAll(Pageable pageable, @Param("query") JobQueryDto query);

    String FIND_ALL_WITH_QUERY_WHERE_CLAUSE = " where (coalesce(:#{#query.employeeId}) is null or link_employee.lrEmployee.id in (:#{#query.employeeId}))"
            + " and (coalesce(:#{#query.officeId}) is null or link_office.office.id in (:#{#query.officeId}))"
            + " and (coalesce(:#{#query.assetId}) is null or asset.id in (:#{#query.assetId}))"
            + " and (coalesce(:#{#query.jobStatusId}) is null or status.id in (:#{#query.jobStatusId}))"
            + " and (coalesce(:#{#query.caseId}) is null or job.aCase.id in (:#{#query.caseId}))"
            + " and (:#{#query.startDateMin} is null or job.etaDate >= :#{#query.startDateMin})"
            + " and (:#{#query.startDateMax} is null or job.etaDate <= :#{#query.startDateMax})"
            + " and (:#{#query.search} is null or job.id = :#{#query.search})";

    @Query(countQuery = "select count(distinct job) from JobDO job, JobResourceDO resource, JobStatusToRoleDO statusToRole"
            + " join job.asset asset"
            + " where job.id = resource.job.id"
            + " and resource.lrEmployee.id = ?#{[1]}"
            + " and job.jobStatus.id = statusToRole.jobStatus.id"
            + " and resource.employeeRole.id = statusToRole.employeeRole.id"
            + FIND_ALL_FOR_EMPLOYEE_WITH_QUERY_WHERE_CLAUSE, value = "select job from JobDO job, JobResourceDO resource, JobStatusToRoleDO statusToRole"
                    + " join job.asset asset"
                    + " where job.id = resource.job.id"
                    + " and resource.lrEmployee.id = ?#{[1]}"
                    + " and job.jobStatus.id = statusToRole.jobStatus.id"
                    + " and resource.employeeRole.id = statusToRole.employeeRole.id"
                    + FIND_ALL_FOR_EMPLOYEE_WITH_QUERY_WHERE_CLAUSE)
    Page<JobDO> findAllJobsForEmployee(Pageable pageable, Long employeeId, JobQueryDto query);

    // https://spring.io/blog/2014/07/15/spel-support-in-spring-data-jpa-query-definitions
    // Using more than one @Param in the method signature causes a binding error - see above link. Use positional
    // parameters instead.
    String FIND_ALL_FOR_EMPLOYEE_WITH_QUERY_WHERE_CLAUSE = " and (coalesce(?#{[2].assetId}) is null or asset.id in (?#{[2].assetId}))";

    @Query("select asset.id from JobDO j join j.asset asset where j.id =:jobId")
    Long getAssetIdForJob(@Param("jobId") Long jobId);

    @Modifying()
    @Query("update JobDO job set job.jobStatus.id = :statusId "
            + " where job.id = :jobId")
    void updateStatus(@Param("jobId") Long defectId, @Param("statusId") Long statusId);

    @Query("select status from JobStatusDO status, JobDO job"
            + " where job.jobStatus.id = status.id"
            + " and job.id =:jobId")
    JobStatusDO getJobStatusForJob(@Param("jobId") Long jobId);

    @Modifying
    @Query("update JobDO job set job.completedBy = :completedBy, job.completedOn = :completedOn"
            + " where job.id = :jobId")
    Integer updateCompletedFields(@Param("jobId") Long jobId, @Param("completedBy") String completedBy, @Param("completedOn") Date completedOn);

    @Query("select asset.id from JobDO j"
            + " join j.asset asset"
            + " where j.id = :jobId")
    Long findAssetIdForJob(@Param("jobId") Long jobId);

    @Query("select asset.assetVersionId from JobDO job"
            + " join job.asset asset"
            + " where asset.id = :assetId and job.id = :jobId")
    Long getVersionForAssetAndJob(@Param("assetId") Long assetId, @Param("jobId") Long jobId);
}
