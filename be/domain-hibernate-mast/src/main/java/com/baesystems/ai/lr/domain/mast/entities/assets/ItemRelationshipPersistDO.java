package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.references.BaseItemRelationshipDO;
import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "MAST_ASSET_AssetItemRelationship")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetItemRelationship SET deleted = true WHERE id = ?")
public class ItemRelationshipPersistDO extends BaseItemRelationshipDO
{
    //Mutable
    @Column(name = "asset_id")
    private Long assetId;

    @Column(name = "to_item_id")
    private Long toItemId;

    @Column(name = "to_version_id")
    private Long toVersionId;

    //There is redundancy here, fromAsset should always be equal to to_asset, but we need separate fields for some clever Hibernate mappings
    //on VersionedItemDO
    @Column(name = "from_asset_id")
    private Long fromAssetId;

    @Column(name = "from_item_id")
    private Long fromItemId;

    @Column(name = "from_version_id")
    private Long fromVersionId;


    @JoinColumns({
            @JoinColumn(name = "to_item_id", referencedColumnName = "id", updatable = false, insertable = false),
            @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", insertable = false, updatable = false),
            @JoinColumn(name = "to_version_id", referencedColumnName = "asset_version_id", updatable = false, insertable = false)
    })
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private VersionedItemPersistDO toItem;

    @JoinColumns({
            @JoinColumn(name = "from_item_id", referencedColumnName = "id", updatable = false, insertable = false),
            @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", insertable = false, updatable = false),
            @JoinColumn(name = "from_version_id", referencedColumnName = "asset_version_id", updatable = false, insertable = false)
    })
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private VersionedItemPersistDO fromItem;

    public VersionedItemPersistDO getToItem()
    {
        return toItem;
    }

    public void setToItem(final VersionedItemPersistDO toItem)
    {
        this.toItem = toItem;
    }

    public VersionedItemPersistDO getFromItem()
    {
        return fromItem;
    }

    public void setFromItem(final VersionedItemPersistDO fromItem)
    {
        this.fromItem = fromItem;
    }

    public Long getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getToItemId()
    {
        return toItemId;
    }

    public void setToItemId(final Long toItemId)
    {
        this.toItemId = toItemId;
    }

    public Long getToVersionId()
    {
        return toVersionId;
    }

    public void setToVersionId(final Long toVersionId)
    {
        this.toVersionId = toVersionId;
    }

    public Long getFromAssetId()
    {
        return fromAssetId;
    }

    public void setFromAssetId(final Long fromAssetId)
    {
        this.fromAssetId = fromAssetId;
    }

    public Long getFromItemId()
    {
        return fromItemId;
    }

    public void setFromItemId(final Long fromItemId)
    {
        this.fromItemId = fromItemId;
    }

    public Long getFromVersionId()
    {
        return fromVersionId;
    }

    public void setFromVersionId(final Long fromVersionId)
    {
        this.fromVersionId = fromVersionId;
    }
}
