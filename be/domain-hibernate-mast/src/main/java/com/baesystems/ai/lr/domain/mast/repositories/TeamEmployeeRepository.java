package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamEmployeeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface TeamEmployeeRepository extends SpringDataRepository<TeamEmployeeDO, Long>
{
}
