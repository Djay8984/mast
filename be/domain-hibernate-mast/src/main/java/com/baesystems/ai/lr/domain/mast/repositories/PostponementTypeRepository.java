package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.PostponementTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface PostponementTypeRepository extends SpringDataRepository<PostponementTypeDO, Long>
{

}
