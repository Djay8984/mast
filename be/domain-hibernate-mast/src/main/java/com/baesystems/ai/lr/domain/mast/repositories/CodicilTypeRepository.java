package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CodicilTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CodicilTypeRepository extends SpringDataRepository<CodicilTypeDO, Long>
{

}
