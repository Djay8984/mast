package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_REF_Milestone")
public class MilestoneDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "description", length = 256, nullable = false)
    private String description;

    @Column(name = "display_order", nullable = false)
    private Long displayOrder;

    @JoinColumn(name = "case_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private CaseTypeDO caseType;

    @OneToMany(mappedBy = "milestone", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @Where(clause = "deleted = false")
    private List<MilestoneRelationshipDO> predecessorMilestones;

    @OneToMany(mappedBy = "predecessorMilestone", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @Where(clause = "deleted = false")
    private List<MilestoneRelationshipDO> childMilestones;

    @Column(name = "mandatory")
    private Boolean mandatory;

    @Column(name = "include_on_summary_report")
    private Boolean includeOnSummaryReport;

    @Column(name = "working_days_offset")
    private Integer workingDaysOffset;

    @JoinColumn(name = "due_date_reference_id", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private MilestoneDueDateReferenceDO dueDateReference;

    @Column(name = "change_class_status", length = 50, nullable = true)
    private String changeClassStatus;

    @JoinColumn(name = "owner", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private LrEmployeeRoleDO owner;

    @Column(name = "comments", length = 256, nullable = true)
    private String comments;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "change_case_status_to_id") )})
    private LinkDO changeCaseStatusTo;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public CaseTypeDO getCaseType()
    {
        return caseType;
    }

    public void setCaseType(final CaseTypeDO caseType)
    {
        this.caseType = caseType;
    }

    public List<MilestoneRelationshipDO> getPredecessorMilestones()
    {
        return predecessorMilestones;
    }

    public void setPredecessorMilestones(final List<MilestoneRelationshipDO> predecessorMilestones)
    {
        this.predecessorMilestones = predecessorMilestones;
    }

    public List<MilestoneRelationshipDO> getChildMilestones()
    {
        return childMilestones;
    }

    public void setChildMilestones(final List<MilestoneRelationshipDO> childMilestones)
    {
        this.childMilestones = childMilestones;
    }

    public Boolean getIncludeOnSummaryReport()
    {
        return includeOnSummaryReport;
    }

    public void setIncludeOnSummaryReport(final Boolean includeOnSummaryReport)
    {
        this.includeOnSummaryReport = includeOnSummaryReport;
    }

    public String getChangeClassStatus()
    {
        return changeClassStatus;
    }

    public void setChangeClassStatus(final String changeClassStatus)
    {
        this.changeClassStatus = changeClassStatus;
    }

    public LrEmployeeRoleDO getOwner()
    {
        return owner;
    }

    public void setOwner(final LrEmployeeRoleDO owner)
    {
        this.owner = owner;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(final String comments)
    {
        this.comments = comments;
    }

    public Boolean getMandatory()
    {
        return mandatory;
    }

    public void setMandatory(final Boolean mandatory)
    {
        this.mandatory = mandatory;
    }

    public MilestoneDueDateReferenceDO getDueDateReference()
    {
        return dueDateReference;
    }

    public void setDueDateReference(final MilestoneDueDateReferenceDO dueDateReference)
    {
        this.dueDateReference = dueDateReference;
    }

    public LinkDO getChangeCaseStatusTo()
    {
        return changeCaseStatusTo;
    }

    public void setChangeCaseStatusTo(final LinkDO changeCaseStatusTo)
    {
        this.changeCaseStatusTo = changeCaseStatusTo;
    }

    public Integer getWorkingDaysOffset()
    {
        return workingDaysOffset;
    }

    public void setWorkingDaysOffset(final Integer workingDaysOffset)
    {
        this.workingDaysOffset = workingDaysOffset;
    }
}
