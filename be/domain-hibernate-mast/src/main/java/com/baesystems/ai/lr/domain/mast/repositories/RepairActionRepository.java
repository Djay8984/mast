package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.RepairActionDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface RepairActionRepository extends SpringDataRepository<RepairActionDO, Long>
{

}
