package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_Ruleset2")
public class SocietyRuleSetDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 500, nullable = false)
    private String name;

    @Column(name = "category", length = 500, nullable = false)
    private String category;

    @Column(name = "is_lr_ruleset", nullable = false)
    private Boolean isLrRuleset;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(final String category)
    {
        this.category = category;
    }

    public Boolean getIsLrRuleset()
    {
        return isLrRuleset;
    }

    public void setIsLrRuleset(final Boolean isLrRuleset)
    {
        this.isLrRuleset = isLrRuleset;
    }
}
