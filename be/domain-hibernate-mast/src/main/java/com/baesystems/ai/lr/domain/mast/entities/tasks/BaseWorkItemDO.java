package com.baesystems.ai.lr.domain.mast.entities.tasks;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CodicilDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.references.CreditStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.IacsSocietyDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ResolutionStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemActionDO;
import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemTypeDO;

@MappedSuperclass
public class BaseWorkItemDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "reference_code", length = 14, nullable = false)
    private String referenceCode;

    @Column(name = "name", length = 850)
    private String name;

    @Column(name = "description", length = 30)
    private String description;

    @Column(name = "long_description", length = 850)
    private String longDescription;

    @Column(name = "service_code", length = 10)
    private String serviceCode;

    @JoinColumn(name = "asset_item_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ItemDO assetItem;

    @JoinColumn(name = "work_item_type_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WorkItemTypeDO workItemType;

    @JoinColumn(name = "work_item_action_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WorkItemActionDO workItemAction;

    @Column(name = "attribute_mandatory")
    private Boolean attributeMandatory;

    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    @Column(name = "notes", length = 2000)
    private String notes;

    @JoinColumn(name = "codicil_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CodicilDO codicil;

    @Column(name = "attachment_required")
    private Boolean attachmentRequired;

    @Column(name = "item_order")
    private Integer itemOrder;

    @Column(name = "resolution_date")
    @Temporal(TemporalType.DATE)
    private Date resolutionDate;

    @JoinColumn(name = "resolution_status_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ResolutionStatusDO resolutionStatus;

    @JoinColumn(name = "class_society_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private IacsSocietyDO classSociety;

    @JoinColumn(name = "assigned_to_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private LrEmployeeDO assignedTo;

    @Column(name = "assigned_date")
    @Temporal(TemporalType.DATE)
    private Date assignedDate;

    @JoinColumn(name = "resolved_by_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private LrEmployeeDO resolvedBy;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "conditional_parent_id") )})
    private LinkDO conditionalParent;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "work_item_id") )})
    private LinkDO workItem;

    @JoinColumn(name = "credit_status_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private CreditStatusDO creditStatus;

    @Column(name = "task_number", length = 20, updatable = false, nullable = false)
    private String taskNumber;

    @JoinColumn(name = "postponement_id")
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private PostponementDO postponement;

    @JoinColumn(name = "job_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private JobDO creditedByJob;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getReferenceCode()
    {
        return this.referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getLongDescription()
    {
        return this.longDescription;
    }

    public void setLongDescription(final String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getServiceCode()
    {
        return this.serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public ItemDO getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final ItemDO assetItem)
    {
        this.assetItem = assetItem;
    }

    public WorkItemTypeDO getWorkItemType()
    {
        return this.workItemType;
    }

    public void setWorkItemType(final WorkItemTypeDO workItemType)
    {
        this.workItemType = workItemType;
    }

    public WorkItemActionDO getWorkItemAction()
    {
        return this.workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDO workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public Boolean getAttributeMandatory()
    {
        return this.attributeMandatory;
    }

    public void setAttributeMandatory(final Boolean attributeMandatory)
    {
        this.attributeMandatory = attributeMandatory;
    }

    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getNotes()
    {
        return this.notes;
    }

    public void setNotes(final String notes)
    {
        this.notes = notes;
    }

    public CodicilDO getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final CodicilDO codicil)
    {
        this.codicil = codicil;
    }

    public Boolean getAttachmentRequired()
    {
        return this.attachmentRequired;
    }

    public void setAttachmentRequired(final Boolean attachmentRequired)
    {
        this.attachmentRequired = attachmentRequired;
    }

    public Integer getItemOrder()
    {
        return this.itemOrder;
    }

    public void setItemOrder(final Integer itemOrder)
    {
        this.itemOrder = itemOrder;
    }

    public Date getResolutionDate()
    {
        return this.resolutionDate;
    }

    public void setResolutionDate(final Date resolutionDate)
    {
        this.resolutionDate = resolutionDate;
    }

    public ResolutionStatusDO getResolutionStatus()
    {
        return this.resolutionStatus;
    }

    public void setResolutionStatus(final ResolutionStatusDO resolutionStatus)
    {
        this.resolutionStatus = resolutionStatus;
    }

    public IacsSocietyDO getClassSociety()
    {
        return this.classSociety;
    }

    public void setClassSociety(final IacsSocietyDO classSociety)
    {
        this.classSociety = classSociety;
    }

    public LrEmployeeDO getAssignedTo()
    {
        return this.assignedTo;
    }

    public void setAssignedTo(final LrEmployeeDO assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public LrEmployeeDO getResolvedBy()
    {
        return this.resolvedBy;
    }

    public void setResolvedBy(final LrEmployeeDO resolvedBy)
    {
        this.resolvedBy = resolvedBy;
    }

    public LinkDO getConditionalParent()
    {
        return this.conditionalParent;
    }

    public void setConditionalParent(final LinkDO conditionalParent)
    {
        this.conditionalParent = conditionalParent;
    }

    public LinkDO getWorkItem()
    {
        return this.workItem;
    }

    public void setWorkItem(final LinkDO workItem)
    {
        this.workItem = workItem;
    }

    public CreditStatusDO getCreditStatus()
    {
        return this.creditStatus;
    }

    public void setCreditStatus(final CreditStatusDO creditStatus)
    {
        this.creditStatus = creditStatus;
    }

    public String getTaskNumber()
    {
        return this.taskNumber;
    }

    public void setTaskNumber(final String taskNumber)
    {
        this.taskNumber = taskNumber;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public PostponementDO getPostponement()
    {
        return this.postponement;
    }

    public void setPostponement(final PostponementDO postponement)
    {
        this.postponement = postponement;
    }

    public JobDO getCreditedByJob()
    {
        return this.creditedByJob;
    }

    public void setCreditedByJob(final JobDO creditedByJob)
    {
        this.creditedByJob = creditedByJob;
    }
}
