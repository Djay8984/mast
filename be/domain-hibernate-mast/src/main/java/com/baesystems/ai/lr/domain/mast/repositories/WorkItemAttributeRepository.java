package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemAttributeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemAttributeRepository extends SpringDataRepository<WorkItemAttributeDO, Long>
{

}
