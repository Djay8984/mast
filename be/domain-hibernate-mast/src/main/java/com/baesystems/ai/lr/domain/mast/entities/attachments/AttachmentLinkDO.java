package com.baesystems.ai.lr.domain.mast.entities.attachments;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.ActionableItemDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.AssetNoteDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.CoCDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPActionableItemDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPAssetNoteDO;
import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPCoCDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.WIPDefectDO;
import com.baesystems.ai.lr.domain.mast.entities.jobs.JobDO;
import com.baesystems.ai.lr.domain.mast.entities.services.ScheduledServiceDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemDO;
import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDO;

@Entity
@Table(name = "MAST_XXX_SupplementaryInformationLink")
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumns({@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")})
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_XXX_SupplementaryInformationLink SET deleted = true WHERE id = ?")
public class AttachmentLinkDO extends SupplementaryInformationDO
{
    @JoinColumn(name = "defect_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private DefectDO defect;

    @JoinColumn(name = "wip_defect_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPDefectDO wipDefect;

    @JoinColumn(name = "job_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private JobDO job;

    @JoinColumn(name = "coc_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private CoCDO coC;

    @JoinColumn(name = "wip_coc_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPCoCDO wipCoC;

    @JoinColumn(name = "asset_note_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private AssetNoteDO assetNote;

    @JoinColumn(name = "wip_asset_note_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPAssetNoteDO wipAssetNote;

    @JoinColumn(name = "asset_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private AssetDO asset;

    @JoinColumn(name = "item_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private ItemDO item;

    @JoinColumn(name = "milestone_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private CaseMilestoneDO milestone;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "repair_id", updatable = false))})
    private LinkDO repair;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "wip_repair_id", updatable = false))})
    private LinkDO wipRepair;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "certificate_id", updatable = false))})
    private LinkDO certificate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "report_id", updatable = false))})
    private LinkDO report;

    @JoinColumn(name = "case_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private CaseDO aCase;

    @JoinColumn(name = "scheduled_service_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private ScheduledServiceDO service;

    @JoinColumn(name = "actionable_item_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private ActionableItemDO actionableItem;

    @JoinColumn(name = "wip_actionable_item_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPActionableItemDO wipActionableItem;

    @JoinColumn(name = "work_item_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WorkItemDO workItem;

    @JoinColumn(name = "wip_work_item_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private WIPWorkItemDO wipWorkItem;

    @JoinColumn(name = "survey_id", updatable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private SurveyDO survey;

    public DefectDO getDefect()
    {
        return defect;
    }

    public void setDefect(final DefectDO defect)
    {
        this.defect = defect;
    }

    public WIPDefectDO getWipDefect()
    {
        return wipDefect;
    }

    public void setWipDefect(final WIPDefectDO wipDefect)
    {
        this.wipDefect = wipDefect;
    }

    public CaseDO getaCase()
    {
        return aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public AssetDO getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

    public ItemDO getItem()
    {
        return item;
    }

    public void setItem(final ItemDO item)
    {
        this.item = item;
    }

    public CaseMilestoneDO getMilestone()
    {
        return milestone;
    }

    public void setMilestone(final CaseMilestoneDO milestone)
    {
        this.milestone = milestone;
    }

    public LinkDO getRepair()
    {
        return repair;
    }

    public void setRepair(final LinkDO repair)
    {
        this.repair = repair;
    }

    public LinkDO getWipRepair()
    {
        return wipRepair;
    }

    public void setWipRepair(final LinkDO wipRepair)
    {
        this.wipRepair = wipRepair;
    }

    public LinkDO getCertificate()
    {
        return certificate;
    }

    public void setCertificate(final LinkDO certificate)
    {
        this.certificate = certificate;
    }

    public LinkDO getReport()
    {
        return report;
    }

    public void setReport(final LinkDO report)
    {
        this.report = report;
    }

    public ScheduledServiceDO getService()
    {
        return service;
    }

    public void setService(final ScheduledServiceDO service)
    {
        this.service = service;
    }

    public CoCDO getCoC()
    {
        return coC;
    }

    public void setCoC(final CoCDO coC)
    {
        this.coC = coC;
    }

    public WIPCoCDO getWipCoC()
    {
        return wipCoC;
    }

    public void setWipCoC(final WIPCoCDO wipCoC)
    {
        this.wipCoC = wipCoC;
    }

    public AssetNoteDO getAssetNote()
    {
        return assetNote;
    }

    public void setAssetNote(final AssetNoteDO assetNote)
    {
        this.assetNote = assetNote;
    }

    public WIPAssetNoteDO getWipAssetNote()
    {
        return wipAssetNote;
    }

    public void setWipAssetNote(final WIPAssetNoteDO wipAssetNote)
    {
        this.wipAssetNote = wipAssetNote;
    }

    public ActionableItemDO getActionableItem()
    {
        return actionableItem;
    }

    public void setActionableItem(final ActionableItemDO actionableItem)
    {
        this.actionableItem = actionableItem;
    }

    public WIPActionableItemDO getWipActionableItem()
    {
        return this.wipActionableItem;
    }

    public void setWipActionableItem(final WIPActionableItemDO wipActionableItem)
    {
        this.wipActionableItem = wipActionableItem;
    }

    public WorkItemDO getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final WorkItemDO workItem)
    {
        this.workItem = workItem;
    }

    public WIPWorkItemDO getWipWorkItem()
    {
        return wipWorkItem;
    }

    public void setWipWorkItem(final WIPWorkItemDO wipWorkItem)
    {
        this.wipWorkItem = wipWorkItem;
    }

    public SurveyDO getSurvey()
    {
        return survey;
    }

    public void setSurvey(final SurveyDO survey)
    {
        this.survey = survey;
    }
}
