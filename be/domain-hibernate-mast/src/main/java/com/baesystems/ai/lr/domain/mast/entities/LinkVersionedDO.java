package com.baesystems.ai.lr.domain.mast.entities;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.util.Objects;

@Embeddable
public class LinkVersionedDO
{
    @Column
    private Long id;

    @Column
    private Long version;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(final Long version)
    {
        this.version = version;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final LinkVersionedDO that = (LinkVersionedDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(version, that.version);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, version);
    }
}
