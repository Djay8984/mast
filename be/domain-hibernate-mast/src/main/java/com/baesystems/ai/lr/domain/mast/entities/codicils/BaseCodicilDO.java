package com.baesystems.ai.lr.domain.mast.entities.codicils;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;

@MappedSuperclass
public class BaseCodicilDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "imposed_date")
    private Date imposedDate;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "narrative", length = 2000)
    private String description;

    @Column(name = "action_taken", length = 100)
    private String actionTaken;

    @Column(name = "inherited_flag", nullable = false)
    private Boolean inheritedFlag;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "defect_id"))})
    private LinkDO defect;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "status_id"))})
    private LinkDO status;

    @Embedded
    @JoinColumn(name = "asset_item_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private ItemDO assetItem;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "employee_id"))})
    private LinkDO employee;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "template_id"))})
    private LinkDO template;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "category_id"))})
    private LinkDO category;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "confidentiality_type_id"))})
    private LinkDO confidentialityType;

    @Column(name = "surveyor_guidance", length = 2000)
    private String surveyorGuidance;

    @Column(name = "require_approval")
    private Boolean requireApproval;

    @Column(name = "editable_by_surveyor")
    private Boolean editableBySurveyor;

    @Column(name = "title", length = 50)
    private String title;

    @Column(name = "internal_sequence_number", updatable = false)
    private Integer sequenceNumber;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "credited_by"))})
    private LinkDO creditedBy;

    @Column(name = "date_of_crediting")
    private Date dateOfCrediting;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Date getImposedDate()
    {
        return imposedDate;
    }

    public void setImposedDate(final Date imposedDate)
    {
        this.imposedDate = imposedDate;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getActionTaken()
    {
        return actionTaken;
    }

    public void setActionTaken(final String actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    public Boolean getInheritedFlag()
    {
        return inheritedFlag;
    }

    public void setInheritedFlag(final Boolean inheritedFlag)
    {
        this.inheritedFlag = inheritedFlag;
    }

    public LinkDO getDefect()
    {
        return defect;
    }

    public void setDefect(final LinkDO defect)
    {
        this.defect = defect;
    }

    public LinkDO getStatus()
    {
        return status;
    }

    public void setStatus(final LinkDO status)
    {
        this.status = status;
    }

    public ItemDO getAssetItem()
    {
        return assetItem;
    }

    public void setAssetItem(final ItemDO assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkDO getEmployee()
    {
        return employee;
    }

    public void setEmployee(final LinkDO employee)
    {
        this.employee = employee;
    }

    public LinkDO getTemplate()
    {
        return template;
    }

    public void setTemplate(final LinkDO template)
    {
        this.template = template;
    }

    public LinkDO getCategory()
    {
        return category;
    }

    public void setCategory(final LinkDO category)
    {
        this.category = category;
    }

    public LinkDO getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final LinkDO confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public String getSurveyorGuidance()
    {
        return surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public Boolean isRequireApproval()
    {
        return requireApproval;
    }

    public void setRequireApproval(final Boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public Boolean getEditableBySurveyor()
    {
        return editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public Integer getSequenceNumber()
    {
        return sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public LinkDO getCreditedBy()
    {
        return creditedBy;
    }

    public void setCreditedBy(final LinkDO creditedBy)
    {
        this.creditedBy = creditedBy;
    }

    public Date getDateOfCrediting()
    {
        return dateOfCrediting;
    }

    public void setDateOfCrediting(final Date dateOfCrediting)
    {
        this.dateOfCrediting = dateOfCrediting;
    }
}
