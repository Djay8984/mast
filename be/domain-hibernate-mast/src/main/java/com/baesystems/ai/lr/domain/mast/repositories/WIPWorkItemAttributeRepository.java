package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WIPWorkItemAttributeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WIPWorkItemAttributeRepository extends SpringDataRepository<WIPWorkItemAttributeDO, Long>
{

}
