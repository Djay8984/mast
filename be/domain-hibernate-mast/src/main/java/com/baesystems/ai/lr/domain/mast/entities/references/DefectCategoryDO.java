package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_DefectCategory")
public class DefectCategoryDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "category_letter", length = 1)
    private String categoryLetter;

    @JoinColumn(name = "parent_id", nullable = true)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectCategoryDO parent;

    @JoinTable(name = "MAST_REF_DefectCategory_DefectValue", joinColumns = {@JoinColumn(name = "defect_category_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "defect_value_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<DefectValueDO> values;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getCategoryLetter()
    {
        return categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public DefectCategoryDO getParent()
    {
        return parent;
    }

    public void setParent(final DefectCategoryDO parent)
    {
        this.parent = parent;
    }

    public List<DefectValueDO> getValues()
    {
        return values;
    }

    public void setValues(final List<DefectValueDO> values)
    {
        this.values = values;
    }
}
