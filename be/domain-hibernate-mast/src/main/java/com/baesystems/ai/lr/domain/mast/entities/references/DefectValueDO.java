package com.baesystems.ai.lr.domain.mast.entities.references;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.defects.DefectDefectValueDO;

@Entity
@Table(name = "MAST_REF_DefectValue")
public class DefectValueDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @JoinColumn(name = "severity_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectValueSeverityDO severity;

    @JoinColumn(name = "defect_descriptor_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectDescriptorDO defectDescriptor;

    @OneToMany(mappedBy = "defectValue", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<DefectDefectValueDO> defects;

    @JoinTable(name = "MAST_REF_DefectCategory_DefectValue", joinColumns = {@JoinColumn(name = "defect_value_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "defect_category_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<DefectCategoryDO> defectCategories;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public DefectValueSeverityDO getSeverity()
    {
        return severity;
    }

    public void setSeverity(final DefectValueSeverityDO severity)
    {
        this.severity = severity;
    }

    public DefectDescriptorDO getDefectDescriptor()
    {
        return defectDescriptor;
    }

    public void setDefectDescriptor(final DefectDescriptorDO defectDescriptor)
    {
        this.defectDescriptor = defectDescriptor;
    }

    public List<DefectDefectValueDO> getDefects()
    {
        return defects;
    }

    public void setDefects(final List<DefectDefectValueDO> defects)
    {
        this.defects = defects;
    }

    public List<DefectCategoryDO> getDefectCategories()
    {
        return defectCategories;
    }

    public void setDefectCategories(final List<DefectCategoryDO> defectCategories)
    {
        this.defectCategories = defectCategories;
    }
}
