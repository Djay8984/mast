package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.FollowUpActionStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface FollowUpActionStatusRepository extends SpringDataRepository<FollowUpActionStatusDO, Long>
{

}
