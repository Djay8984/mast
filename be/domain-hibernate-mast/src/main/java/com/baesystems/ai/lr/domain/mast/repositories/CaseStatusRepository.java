package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CaseStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseStatusRepository extends SpringDataRepository<CaseStatusDO, Long>
{

}
