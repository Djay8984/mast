package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.codicils.WIPAssetNoteDO;

public interface WIPAssetNoteRepository extends LockingRepository<WIPAssetNoteDO>, WIPCodicilRepositoryHelper<WIPAssetNoteDO>, WIPSequenceRepository
{
    @Query("select n from WIPAssetNoteDO n"
            + " where n.asset.id = :assetId"
            + " and (:statusId is null"
            + " or (n.status.id = :statusId"
            + " and (:open is null"
            + " or (:open = true and n.imposedDate <= :currentDate)"
            + " or (:open = false and n.imposedDate >:currentDate)"
            + ")))")
    Page<WIPAssetNoteDO> findAssetNotesByAsset(Pageable pageable, @Param("assetId") Long assetId, @Param("statusId") Long statusId,
            @Param("currentDate") Date currentDate, @Param("open") Boolean isOpen);

    @Query("select n from WIPAssetNoteDO n"
            + " where n.job.id = :jobId")
    Page<WIPAssetNoteDO> findAssetNotesByJob(Pageable pageable, @Param("jobId") Long jobId);

    @Query("select n from WIPAssetNoteDO n"
            + " where n.job.id = :jobId"
            + " and deleted = false"
            + " and (coalesce(:statusList) is null"
            + " or (n.status.id in (:statusList)"
            + " and (:open is null or n.status.id <> :openStatus"
            + " or (:open = true and n.imposedDate <= :currentDate)"
            + " or (:open = false and n.imposedDate >:currentDate)"
            + ")))")
    List<WIPAssetNoteDO> findAssetNotesByJob(@Param("jobId") Long jobId, @Param("statusList") List<Long> statusList,
            @Param("currentDate") Date currentDate, @Param("open") Boolean isOpen, @Param("openStatus") Long openStatusId);

    @Query("select an from WIPAssetNoteDO an"
            + " where an.job.id = :jobId")
    List<WIPAssetNoteDO> findAssetNotesByJob(@Param("jobId") Long jobId);

    @Query("select an from WIPAssetNoteDO an"
            + " where an.job.id = :jobId"
            + " and an.actionTakenDate is not null")
    List<WIPAssetNoteDO> findAssetNotesForReportContent(@Param("jobId") Long jobId);

    @Query("select an from WIPAssetNoteDO an"
            + " where an.job.id = :jobId"
            + " and an.updatedBy = :employeeName"
            + " and day(an.actionTakenDate) = day(:lastUpdatedDate)"
            + " and month(an.actionTakenDate) = month(:lastUpdatedDate)"
            + " and year(an.actionTakenDate) = year(:lastUpdatedDate)")
    List<WIPAssetNoteDO> findAssetNotesByJob(@Param("jobId") Long jobId, @Param("employeeName") String employeeName,
            @Param("lastUpdatedDate") Date lastUpdatedDate);

    @Query("select count(note.id) from WIPAssetNoteDO note"
            + " where note.job.id = :jobId and note.id = :assetNoteId")
    Integer assetNoteExistsForJob(@Param("jobId") Long jobId, @Param("assetNoteId") Long assetNoteId);

    @Modifying
    @Query("update WIPAssetNoteDO n set n.jobScopeConfirmed = :scopeConfirmed"
            + " where n.job.id = :jobId"
            + " and (coalesce(:statusList) is null or n.status.id in (:statusList))")
    Integer updateScopeConfirmed(@Param("jobId") Long jobId,
            @Param("scopeConfirmed") Boolean scopeConfirmed,
            @Param("statusList") List<Long> statusList);
}
