package com.baesystems.ai.lr.domain.mast.entities.jobs;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.reports.ReportDO;

@Entity
@Table(name = "MAST_JOB_FollowUpAction")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_FollowUpAction SET deleted = true WHERE id = ?")
public class FollowUpActionDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title", length = 50)
    private String title;

    @Column(name = "description", length = 2000)
    private String description;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "raised_by_id", nullable = false) )})
    private LinkDO raisedBy;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "completed_by_id", nullable = true) )})
    private LinkDO completedBy;

    @Column(name = "added_manually")
    private Boolean addedManually;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "follow_up_action_status_id", nullable = false) )})
    private LinkDO followUpActionStatus;

    @Column(name = "completion_date")
    private Date completionDate;

    @Column(name = "target_date")
    private Date targetDate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_id", nullable = false) )})
    private LinkDO asset;

    @JoinColumn(name = "report_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ReportDO report;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "job_service_instance_id", nullable = true) )})
    private LinkDO survey;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkDO getRaisedBy()
    {
        return raisedBy;
    }

    public void setRaisedBy(final LinkDO raisedBy)
    {
        this.raisedBy = raisedBy;
    }

    public LinkDO getCompletedBy()
    {
        return completedBy;
    }

    public void setCompletedBy(final LinkDO completedBy)
    {
        this.completedBy = completedBy;
    }

    public Boolean getAddedManually()
    {
        return addedManually;
    }

    public void setAddedManually(final Boolean addedManually)
    {
        this.addedManually = addedManually;
    }

    public LinkDO getFollowUpActionStatus()
    {
        return followUpActionStatus;
    }

    public void setFollowUpActionStatus(final LinkDO followUpActionStatus)
    {
        this.followUpActionStatus = followUpActionStatus;
    }

    public Date getCompletionDate()
    {
        return completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Date getTargetDate()
    {
        return targetDate;
    }

    public void setTargetDate(final Date targetDate)
    {
        this.targetDate = targetDate;
    }

    public LinkDO getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkDO asset)
    {
        this.asset = asset;
    }

    public ReportDO getReport()
    {
        return report;
    }

    public void setReport(final ReportDO report)
    {
        this.report = report;
    }

    public LinkDO getSurvey()
    {
        return survey;
    }

    public void setSurvey(final LinkDO survey)
    {
        this.survey = survey;
    }
}
