package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipPersistDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ItemRelationshipPersistRepository extends SpringDataRepository<ItemRelationshipPersistDO, Long>
{
    @Modifying
    @Query(nativeQuery = true, value =
            "update MAST_ASSET_AssetItemRelationship set deleted = true"
                + " where to_item_id in (:itemId)"
                + " and from_item_id = :fromItemId"
                + " and from_version_id = :fromVersionId"
                + " and to_version_id = :toVersionId")
    void bulkLogicalDeleteByItemId(@Param("itemId") List<Long> itemId, @Param("fromItemId") Long fromItemId,
            @Param("fromVersionId") Long fromVersionId, @Param("toVersionId") Long toVersionId);

    @Query(nativeQuery = true, value =
            "select v.* from MAST_ASSET_AssetItemRelationship as v"
                    + " where v.from_version_id ="
                    + " (select MAX(vv.from_version_id)"
                    + " from MAST_ASSET_AssetItemRelationship as vv"
                    + " where v.asset_id = vv.asset_id"
                    + " and v.from_item_id = vv.from_item_id"
                    + " and v.to_item_id = vv.to_item_id"
                    + " and vv.asset_id = :assetId"
                    + " and vv.from_item_id = :itemId"
                    + " and vv.from_version_id <= :versionId"
                    + " and v.to_version_id ="
                    + "(select MAX(vvv.to_version_id)"
                    + " from MAST_ASSET_AssetItemRelationship as vvv"
                    + " where v.asset_id = vvv.asset_id"
                    + " and v.from_item_id = vvv.from_item_id"
                    + " and v.to_item_id = vvv.to_item_id"
                    + " and vvv.asset_id = :assetId"
                    + " and vvv.from_item_id = :itemId"
                    + " and vvv.to_version_id <= :versionId)"
                    + " and (:relationshipType is null or vv.relationship_type_id = :relationshipType))")
    List<ItemRelationshipPersistDO> getItemRelationships(
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId,
            @Param("versionId") Long versionId,
            @Param("relationshipType") Long relationshipType);

    @Query("select v from ItemRelationshipPersistDO as v"
                    + " where v.fromVersionId ="
                    + " (select MAX(vv.fromVersionId)"
                    + " from ItemRelationshipPersistDO as vv"
                    + " where v.assetId = vv.assetId"
                    + " and v.fromItemId = vv.fromItemId"
                    + " and v.toItemId = vv.toItemId"
                    + " and vv.assetId = :assetId"
                    + " and vv.toItemId = :itemId"
                    + " and vv.fromVersionId <= :versionId"
                    + " and v.toVersionId ="
                    + " (select MAX(vvv.toVersionId)"
                    + " from ItemRelationshipPersistDO as vvv"
                    + " where v.assetId = vvv.assetId"
                    + " and v.fromItemId = vvv.fromItemId"
                    + " and v.toItemId = vvv.toItemId"
                    + " and vvv.assetId = :assetId"
                    + " and vvv.toItemId = :itemId"
                    + " and vvv.toVersionId <= :versionId)"
                    + " and (:relationshipType is null or vv.type.id = :relationshipType))")
    List<ItemRelationshipPersistDO> getInverseRelationships(
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId,
            @Param("versionId") Long versionId,
            @Param("relationshipType") Long relationshipType);
}
