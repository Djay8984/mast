package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "MAST_ASSET_VersionedAssetItem")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_VersionedAssetItem SET deleted = true WHERE id = ?")
public class VersionedItemPersistDO extends BaseItemDO
{
    private static final long serialVersionUID = -9100541421981355739L;

    @JoinColumns({
                  @JoinColumn(name = "item_id", referencedColumnName = "id"),
                  @JoinColumn(name = "asset_id", referencedColumnName = "asset_id"),
                  @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id")
    })
    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<AttributePersistDO> attributes;

    // Do not treat this assoc as returning the exact set of relationships for the item it will return everything
    // regardless of version, it is
    // Used for cascading persistence functionality only.
    @OneToMany(mappedBy = "fromItem", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<ItemRelationshipPersistDO> related;

    public List<AttributePersistDO> getAttributes()
    {
        return this.attributes;
    }

    public void setAttributes(final List<AttributePersistDO> attributes)
    {
        this.attributes = attributes;
    }

    public List<ItemRelationshipPersistDO> getRelated()
    {
        return this.related;
    }

    public void setRelated(final List<ItemRelationshipPersistDO> related)
    {
        this.related = related;
    }
}
