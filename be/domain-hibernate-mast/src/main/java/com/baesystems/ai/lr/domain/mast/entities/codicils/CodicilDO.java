package com.baesystems.ai.lr.domain.mast.entities.codicils;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetDO;
import com.baesystems.ai.lr.enums.CodicilStatusType;

@Entity
@Table(name = "MAST_ASSET_Codicil")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "codicil_type_id", discriminatorType = DiscriminatorType.INTEGER)
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_Codicil SET deleted = true WHERE id = ?")
public class CodicilDO extends BaseCodicilDO
{
    @JoinColumn(name = "asset_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private AssetDO asset;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "job_id") )})
    private LinkDO job;

    // todo this to be removed
    @Column(name = "wip_flag", nullable = false, columnDefinition = "default 0")
    private Boolean wipFlag;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<WIPCodicilDO> childWIPs;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "scheduled_service_id") )})
    private LinkDO scheduledService;

    @Override
    @PrePersist
    @PreUpdate
    public void setDefaults()
    {
        super.setDefaults();
        if (this.wipFlag == null)
        {
            this.wipFlag = false;
        }

        if (this.getStatus() != null)
        { // don't save statuses as inactive, use open instead to simplify queries and then use the imposed date to
          // differentiate.
            final Long statusId = this.getStatus().getId();

            if (CodicilStatusType.AN_INACTIVE.getValue().equals(statusId))
            {
                this.getStatus().setId(CodicilStatusType.AN_OPEN.getValue());
            }
            else if (CodicilStatusType.AI_INACTIVE.getValue().equals(statusId))
            {
                this.getStatus().setId(CodicilStatusType.AI_OPEN.getValue());
            }
        }
    }

    public AssetDO getAsset()
    {
        return this.asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

    public LinkDO getJob()
    {
        return this.job;
    }

    public void setJob(final LinkDO job)
    {
        this.job = job;
    }

    public Boolean getWipFlag()
    {
        return this.wipFlag;
    }

    public void setWipFlag(final Boolean wipFlag)
    {
        this.wipFlag = wipFlag;
    }

    public List<WIPCodicilDO> getChildWIPs()
    {
        return this.childWIPs;
    }

    public void setChildWIPs(final List<WIPCodicilDO> childWIPs)
    {
        this.childWIPs = childWIPs;
    }

    public LinkDO getScheduledService()
    {
        return this.scheduledService;
    }

    public void setScheduledService(final LinkDO scheduledService)
    {
        this.scheduledService = scheduledService;
    }
}
