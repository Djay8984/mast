package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CountryDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CountryRepository extends SpringDataRepository<CountryDO, Long>
{

}
