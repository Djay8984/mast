package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.TeamOfficeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface TeamOfficeRepository extends SpringDataRepository<TeamOfficeDO, Long>
{
}
