package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.tasks.WorkItemDictionaryLinkDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemDictionaryLinkRepository extends SpringDataRepository<WorkItemDictionaryLinkDO, Long>
{

}
