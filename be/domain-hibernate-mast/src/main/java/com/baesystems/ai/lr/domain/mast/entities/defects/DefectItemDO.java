package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;

@Entity
@Table(name = "MAST_DEFECT_Defect_AssetItem")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_Defect_AssetItem SET deleted = true WHERE id = ?")
public class DefectItemDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "item_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ItemDO item;

    @JoinColumn(name = "defect_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectDO defect;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<RepairItemDO> repairItems;

    @PreRemove
    private void preRemove()
    {
        if (repairItems != null)
        {
            for (final RepairItemDO repairItem : repairItems)
            {
                repairItem.setDeleted(Boolean.TRUE);
            }
        }

        defect.setDeleted(Boolean.TRUE);
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public ItemDO getItem()
    {
        return item;
    }

    public void setItem(final ItemDO item)
    {
        this.item = item;
    }

    public DefectDO getDefect()
    {
        return defect;
    }

    public void setDefect(final DefectDO defect)
    {
        this.defect = defect;
    }

    public List<RepairItemDO> getRepairItems()
    {
        return repairItems;
    }

    public void setRepairItems(final List<RepairItemDO> repairItems)
    {
        this.repairItems = repairItems;
    }

}
