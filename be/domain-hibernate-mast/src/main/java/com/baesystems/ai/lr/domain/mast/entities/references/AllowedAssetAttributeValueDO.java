package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_AllowedAssetAttributeValue")
public class AllowedAssetAttributeValueDO extends AuditedDO
{

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @JoinColumn(name = "attribute_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private AttributeTypeDO attributeType;

    @JoinColumn(name = "item_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private ItemTypeDO itemType;

    @Column(name = "description_order")
    private Long descriptionOrder;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public AttributeTypeDO getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(final AttributeTypeDO attributeType) {
        this.attributeType = attributeType;
    }

    public ItemTypeDO getItemType() {
        return itemType;
    }

    public void setItemType(final ItemTypeDO itemType) {
        this.itemType = itemType;
    }

    public Long getDescriptionOrder() {
        return descriptionOrder;
    }

    public void setDescriptionOrder(final Long descriptionOrder) {
        this.descriptionOrder = descriptionOrder;
    }

}
