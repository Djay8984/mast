package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.jobs.CertificateDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CertificateRepository extends SpringDataRepository<CertificateDO, Long>
{

}
