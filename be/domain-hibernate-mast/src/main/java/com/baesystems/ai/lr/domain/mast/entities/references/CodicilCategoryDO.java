package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_REF_CodicilCategory")
public class CodicilCategoryDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "description", length = 200)
    private String description;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "codicil_type_id"))})
    private LinkDO codicilTypeId;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkDO getCodicilTypeId()
    {
        return codicilTypeId;
    }

    public void setCodicilTypeId(final LinkDO codicilTypeId)
    {
        this.codicilTypeId = codicilTypeId;
    }
}
