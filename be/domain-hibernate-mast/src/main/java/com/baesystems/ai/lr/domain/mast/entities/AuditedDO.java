package com.baesystems.ai.lr.domain.mast.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditedDO extends MastDO
{

    @Column(name = "last_update_user_id", length = 150)
    @LastModifiedBy
    private String updatedBy;

    @Column(name = "last_update_date")
    @LastModifiedDate
    private Date updatedDate;

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(final String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(final Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

}
