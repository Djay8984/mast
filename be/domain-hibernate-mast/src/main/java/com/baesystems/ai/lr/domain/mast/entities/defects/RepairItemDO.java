package com.baesystems.ai.lr.domain.mast.entities.defects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;

@Entity
@Table(name = "MAST_DEFECT_Repair_AssetItem")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_Repair_AssetItem SET deleted = true WHERE id = ?")
public class RepairItemDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "defect_item_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private DefectItemDO item;

    @JoinColumn(name = "repair_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private RepairDO repair;

    @PreRemove
    private void preRemove()
    {
        repair.setDeleted(Boolean.TRUE);
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public DefectItemDO getItem()
    {
        return item;
    }

    public void setItem(final DefectItemDO item)
    {
        this.item = item;
    }

    public RepairDO getRepair()
    {
        return repair;
    }

    public void setRepair(final RepairDO repair)
    {
        this.repair = repair;
    }

}
