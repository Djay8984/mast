package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.CreditStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CreditStatusRepository extends SpringDataRepository<CreditStatusDO, Long>
{
}
