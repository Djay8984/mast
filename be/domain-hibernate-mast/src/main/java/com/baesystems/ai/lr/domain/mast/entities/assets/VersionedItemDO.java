package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Immutable
@Table(name = "MAST_ASSET_VersionedAssetItem")
// The following @Where annotation will break the draft item APIs.
// @Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_VersionedAssetItem SET deleted = true WHERE id = ?")
public class VersionedItemDO extends BaseItemDO
{
    private static final long serialVersionUID = -3711050785703297492L;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @Where(clause = "deleted = 0")
    private List<AttributeDO> attributes;


    public List<AttributeDO> getAttributes()
    {
        return this.attributes;
    }

    public void setAttributes(final List<AttributeDO> attributes)
    {
        this.attributes = attributes;
    }
}
