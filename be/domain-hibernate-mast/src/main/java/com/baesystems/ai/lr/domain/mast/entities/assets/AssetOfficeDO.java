package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.MutableVersionedDO;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;

@Entity
@Table(name = "MAST_ASSET_AssetOffice")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetOffice SET deleted = true WHERE id = ?")
public class AssetOfficeDO extends MutableVersionedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "office_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private OfficeDO office;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "office_role_id", nullable = false))})
    private LinkDO officeRole;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public OfficeDO getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDO office)
    {
        this.office = office;
    }

    public LinkDO getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkDO officeRole)
    {
        this.officeRole = officeRole;
    }
}
