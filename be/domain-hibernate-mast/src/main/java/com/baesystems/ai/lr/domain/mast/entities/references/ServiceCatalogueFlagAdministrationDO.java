package com.baesystems.ai.lr.domain.mast.entities.references;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_REF_ServiceCatalogue_FlagAdministration")
public class ServiceCatalogueFlagAdministrationDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "flag_administration_id"))})
    private LinkDO flagStateDO;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "service_catalogue_id"))})
    private LinkDO serviceCatalogueDO;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getFlagStateDO()
    {
        return flagStateDO;
    }

    public void setFlagStateDO(final LinkDO flagStateDO)
    {
        this.flagStateDO = flagStateDO;
    }

    public LinkDO getServiceCatalogueDO()
    {
        return serviceCatalogueDO;
    }

    public void setServiceCatalogueDO(final LinkDO serviceCatalogueDO)
    {
        this.serviceCatalogueDO = serviceCatalogueDO;
    }
}
