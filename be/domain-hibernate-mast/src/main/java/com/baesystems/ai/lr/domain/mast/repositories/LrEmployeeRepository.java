package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface LrEmployeeRepository extends SpringDataRepository<LrEmployeeDO, Long>
{

    @Query("select distinct employee from LrEmployeeDO employee"
            + " join employee.roles role"
            + " where role.id = :roleId")
    Page<LrEmployeeDO> findAllEmployeeByRole(Pageable pageable, @Param("roleId") Long roleId);

    @Query("select distinct employee from LrEmployeeDO employee"
            + " left join employee.offices officesLink"
            + " left join officesLink.office office"
            + " left join employee.roles role"
            + " where (:search is null"
            + " or employee.firstName||' '||employee.lastName like :search"
            + " or (employee.lastName like :search)"
            + " or (employee.firstName like :search)"
            + " or (employee.lastName is null and employee.firstName is null))"
            + " and (office.id = :officeId or :officeId is null)"
            + " and (role.id = :roleId or :roleId is null)")
    Page<LrEmployeeDO> findAll(Pageable pageable, @Param("search") String search, @Param("officeId") Long officeId,
            @Param("roleId") Long roleId);

    LrEmployeeDO findByOneWorldNumber(String oneWorldNumber);

    @Query("select employee from LrEmployeeDO employee"
            + " where concat(employee.firstName, ' ', employee.lastName) = :fullName")
    LrEmployeeDO findByFullName(@Param("fullName") String fullName);

    @Query("select employee.firstName||' '||employee.lastName from LrEmployeeDO employee"
            + " where employee.id = :employeeId")
    String findFullNameById(@Param("employeeId") Long employeeId);
}
