package com.baesystems.ai.lr.domain.mast.entities.assets;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Immutable
@Table(name = "MAST_ASSET_AssetItemAttribute")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_AssetItemAttribute SET deleted = true WHERE id = ?")
public class AttributeDO extends BaseAttributeDO
{

    @JoinColumns({
            @JoinColumn(name = "item_id", referencedColumnName = "id", updatable = false, insertable = false),
            @JoinColumn(name = "asset_id", referencedColumnName = "asset_id", insertable = false, updatable = false),
            @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", updatable = false, insertable = false)
    })
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private VersionedItemDO item;

    public VersionedItemDO getItem()
    {
        return item;
    }

    public void setItem(final VersionedItemDO item)
    {
        this.item = item;
    }
}
