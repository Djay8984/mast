package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;

@Entity
@Table(name = "MAST_REF_CodicilTemplate")
public class CodicilTemplateDO extends AuditedDO
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @JoinColumn(name = "category_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private CodicilCategoryDO category;

    @Column(name = "title", length = 50)
    private String title;

    @JoinColumn(name = "confidentiality_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ConfidentialityTypeDO confidentialityType;

    @Column(name = "editable_by_surveyor")
    private Boolean editableBySurveyor;

    @Column(name = "narrative", length = 2000)
    private String description;

    @Column(name = "surveyor_guidance", length = 2000)
    private String surveyorGuidance;

    @JoinColumn(name = "template_status_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private TemplateStatusDO templateStatus;

    @Column(name = "version", length = 10)
    private String version;

    @JoinColumn(name = "codicil_type_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private CodicilTypeDO codicilType;

    @JoinColumn(name = "item_type_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private ItemTypeDO itemType;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public CodicilCategoryDO getCategory()
    {
        return category;
    }

    public void setCategory(final CodicilCategoryDO category)
    {
        this.category = category;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public ConfidentialityTypeDO getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final ConfidentialityTypeDO confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public Boolean getEditableBySurveyor()
    {
        return editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getSurveyorGuidance()
    {
        return surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public TemplateStatusDO getTemplateStatus()
    {
        return templateStatus;
    }

    public void setTemplateStatus(final TemplateStatusDO templateStatus)
    {
        this.templateStatus = templateStatus;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(final String version)
    {
        this.version = version;
    }

    public CodicilTypeDO getCodicilType()
    {
        return codicilType;
    }

    public void setCodicilType(final CodicilTypeDO codicilType)
    {
        this.codicilType = codicilType;
    }

    public ItemTypeDO getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDO itemType)
    {
        this.itemType = itemType;
    }
}
