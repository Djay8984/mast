package com.baesystems.ai.lr.domain.mast.entities.employees;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;

@Entity
@Table(name = "MAST_LRP_Employee_Office")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_LRP_Employee_Office SET deleted = true WHERE id = ?")
public class LrEmployeeOfficeDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "lr_employee_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private LrEmployeeDO employee;

    @JoinColumn(name = "lr_office_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private OfficeDO office;

    @Column(name = "primary_flag", nullable = false, columnDefinition = "default 0")
    private Boolean primary;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LrEmployeeDO getEmployee()
    {
        return employee;
    }

    public void setEmployee(final LrEmployeeDO employee)
    {
        this.employee = employee;
    }

    public OfficeDO getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDO office)
    {
        this.office = office;
    }

    public Boolean getPrimary()
    {
        return primary;
    }

    public void setPrimary(final Boolean primary)
    {
        this.primary = primary;
    }
}
