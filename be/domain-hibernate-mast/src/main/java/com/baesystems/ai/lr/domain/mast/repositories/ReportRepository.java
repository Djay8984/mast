package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.reports.ReportDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ReportRepository extends LockingRepository<ReportDO>, SpringDataRepository<ReportDO, Long>
{
    @Override
    Page<ReportDO> findAll(Pageable pageable);

    @Query("select r from ReportDO r"
            + " join r.job j"
            + " where j.id = :jobId")
    List<ReportDO> findReportsForJob(@Param("jobId") Long jobId);

    @Query("select coalesce(max(r.reportVersion) + 1, 1) from ReportDO r"
            + " join r.job j"
            + " where j.id = :jobId and r.reportType.id = :reportTypeId")
    Long findNextReportVersion(@Param("jobId") Long jobId, @Param("reportTypeId") Long reportTypeId);

    @Query("select r from ReportDO r"
            + " join r.job j"
            + " where j.id = :jobId and r.reportType.id = :typeId and r.reportVersion = :reportVersion")
    ReportDO getReport(@Param("jobId") Long jobId, @Param("typeId") Long typeId, @Param("reportVersion") Long reportVersion);

    @Query("select count(report.id) > 0 from ReportDO report"
            + " where report.id = :reportId"
            + " and report.job.id = :jobId")
    Boolean reportExistsForJob(@Param("reportId") Long reportId, @Param("jobId") Long jobId);
}
