package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.cases.CaseMilestoneDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface CaseMilestoneRepository extends LockingRepository<CaseMilestoneDO>, SpringDataRepository<CaseMilestoneDO, Long>
{
    @Query("select u from CaseMilestoneDO u"
            + " where u.aCase.id = :caseId")
    List<CaseMilestoneDO> findMilestonesForCase(@Param("caseId") Long caseId);

    @Query("select u.id from CaseMilestoneDO u"
            + " where u.aCase.id = :caseId")
    List<Long> findMilestoneIdsForCase(@Param("caseId") Long caseId);

    @Query("select u from CaseMilestoneDO u"
            + " where u.milestone.id = :milestoneId and u.aCase.id = :caseId")
    CaseMilestoneDO findParentMilestone(@Param("milestoneId") Long milestoneId, @Param("caseId") Long caseId);

    @Query("select u from CaseMilestoneDO u"
            + " where u.aCase.id = :caseId"
            + " and u.milestone.id in (:milestoneId)")
    List<CaseMilestoneDO> findChildMilestones(@Param("milestoneId") List<Long> milestoneId, @Param("caseId") Long caseId);

    @Query("select u.inScope from CaseMilestoneDO u"
            + " where u.milestone.id = :milestoneId and u.aCase.id = :caseId")
    Boolean isCaseMilestoneInScopeByCaseAndMilestoneId(@Param("milestoneId") Long milestoneId, @Param("caseId") Long caseId);

    @Query("select u.milestoneStatus.id from CaseMilestoneDO u"
            + " where u.milestone.id = :milestoneId and u.aCase.id = :caseId")
    Long findStatusIdMilestoneIdAndCaseId(@Param("milestoneId") Long milestoneId, @Param("caseId") Long caseId);

    @Query("select count(caseMilestone.id) from CaseMilestoneDO caseMilestone"
            + " where caseMilestone.aCase.id = :caseId"
            + " and (:inScope is null or caseMilestone.inScope = :inScope)"
            + " and (:statusId is null or caseMilestone.milestoneStatus.id = :statusId)")
    Integer countCaseMilestonesInStatusAndScope(@Param("caseId") Long caseId, @Param("inScope") Boolean inScope, @Param("statusId") Long statusId);
}
