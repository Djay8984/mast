package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.HarmonisationTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface HarmonisationTypeRepository extends SpringDataRepository<HarmonisationTypeDO, Long>
{

}
