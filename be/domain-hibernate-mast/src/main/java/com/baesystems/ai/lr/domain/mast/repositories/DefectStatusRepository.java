package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.DefectStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DefectStatusRepository extends SpringDataRepository<DefectStatusDO, Long>
{

    DefectStatusDO findByName(String name);

}
