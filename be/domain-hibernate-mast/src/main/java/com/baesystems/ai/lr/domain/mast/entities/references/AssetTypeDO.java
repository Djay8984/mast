package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;

@Entity
@Table(name = "MAST_REF_AssetType")
public class AssetTypeDO extends MastDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @JoinColumn(name = "category_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private AssetCategoryDO category;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "level_indication")
    private Integer levelIndication;

    @JoinColumn(name = "default_class_department_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private ClassDepartmentDO defaultClassDepartment;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public AssetCategoryDO getCategory()
    {
        return this.category;
    }

    public void setCategory(final AssetCategoryDO category)
    {
        this.category = category;
    }

    public Long getParentId()
    {
        return this.parentId;
    }

    public void setParentId(final Long parentId)
    {
        this.parentId = parentId;
    }

    public Integer getLevelIndication()
    {
        return this.levelIndication;
    }

    public void setLevelIndication(final Integer levelIndication)
    {
        this.levelIndication = levelIndication;
    }

    public ClassDepartmentDO getDefaultClassDepartment()
    {
        return this.defaultClassDepartment;
    }

    public void setDefaultClassDepartment(final ClassDepartmentDO defaultClassDepartment)
    {
        this.defaultClassDepartment = defaultClassDepartment;
    }

}
