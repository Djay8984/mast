package com.baesystems.ai.lr.domain.mast.entities.references;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_REF_Team")
public class TeamDO extends AuditedDO
{

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 150)
    private String name;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "office_role_id", nullable = false))
    private LinkDO officeRole;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public LinkDO getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkDO officeRole)
    {
        this.officeRole = officeRole;
    }
}
