package com.baesystems.ai.lr.domain.mast.entities.references;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_REF_ClassRecommendation")
public class ClassRecommendationDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "report_type_id") )})
    private LinkDO reportType;

    @Column(name = "narrative_title", length = 100)
    private String narrativeTitle;

    @Column(name = "narrative_text", length = 2000)
    private String narrativeText;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public LinkDO getReportType()
    {
        return reportType;
    }

    public void setReportType(final LinkDO reportType)
    {
        this.reportType = reportType;
    }

    public String getNarrativeTitle()
    {
        return narrativeTitle;
    }

    public void setNarrativeTitle(final String narrativeTitle)
    {
        this.narrativeTitle = narrativeTitle;
    }

    public String getNarrativeText()
    {
        return narrativeText;
    }

    public void setNarrativeText(final String narrativeText)
    {
        this.narrativeText = narrativeText;
    }
}
