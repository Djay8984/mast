package com.baesystems.ai.lr.domain.mast.entities.employees;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;

import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;

@Entity
@Table(name = "MAST_LRP_Employee")
@SQLDelete(sql = "UPDATE MAST_LRP_Employee SET deleted = true WHERE id = ?")
public class LrEmployeeDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "network_user_id", length = 150)
    private String networkUserId;

    @Column(name = "oneworld_number", length = 30, nullable = false)
    private String oneWorldNumber;

    @Column(name = "first_name", length = 150)
    private String firstName;

    @Column(name = "middle_name", length = 60)
    private String middleNames;

    @Column(name = "last_name", length = 150, nullable = false)
    private String lastName;

    @Column(name = "work_phone", length = 60)
    private String workPhone;

    @Column(name = "work_mobile", length = 60)
    private String workMobile;

    @Column(name = "department", length = 240)
    private String department;

    @Column(name = "email_address", length = 240)
    private String emailAddress;

    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<LrEmployeeOfficeDO> offices;

    @JoinTable(name = "MAST_LRP_Employee_Role", joinColumns = {@JoinColumn(name = "lr_employee_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "employee_role_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<LrEmployeeRoleDO> roles;

    @Column(name = "job_title", length = 150)
    private String jobTitle;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getMiddleNames()
    {
        return this.middleNames;
    }

    public void setMiddleNames(final String middleNames)
    {
        this.middleNames = middleNames;
    }

    public String getWorkPhone()
    {
        return this.workPhone;
    }

    public void setWorkPhone(final String workPhone)
    {
        this.workPhone = workPhone;
    }

    public String getDepartment()
    {
        return this.department;
    }

    public void setDepartment(final String department)
    {
        this.department = department;
    }

    public String getOneWorldNumber()
    {
        return this.oneWorldNumber;
    }

    public void setOneWorldNumber(final String oneWorldNumber)
    {
        this.oneWorldNumber = oneWorldNumber;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmailAddress()
    {
        return this.emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public List<LrEmployeeOfficeDO> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<LrEmployeeOfficeDO> offices)
    {
        this.offices = offices;
    }

    public List<LrEmployeeRoleDO> getRoles()
    {
        return this.roles;
    }

    public void setRoles(final List<LrEmployeeRoleDO> roles)
    {
        this.roles = roles;
    }

    public String getNetworkUserId()
    {
        return this.networkUserId;
    }

    public void setNetworkUserId(final String networkUserId)
    {
        this.networkUserId = networkUserId;
    }

    public String getWorkMobile()
    {
        return this.workMobile;
    }

    public void setWorkMobile(final String workMobile)
    {
        this.workMobile = workMobile;
    }

    public String getJobTitle()
    {
        return this.jobTitle;
    }

    public void setJobTitle(final String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

}
