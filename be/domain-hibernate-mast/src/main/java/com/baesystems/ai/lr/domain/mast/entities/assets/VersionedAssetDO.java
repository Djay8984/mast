package com.baesystems.ai.lr.domain.mast.entities.assets;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.customers.PartyDO;
import com.baesystems.ai.lr.domain.mast.entities.references.AssetTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ClassStatusDO;
import com.baesystems.ai.lr.domain.mast.entities.references.FlagStateDO;

@Entity
@Table(name = "MAST_ASSET_VersionedAsset")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_ASSET_VersionedAsset SET deleted = true WHERE id = ?1 and asset_version_id = ?2")
@IdClass(VersionedAssetPK.class)
@SuppressWarnings({"PMD.GodClass", "PMD.ExcessivePublicCount"})
public class VersionedAssetDO extends AuditedDO
{
    private static final String SHIP_BUILDER_ROLE = "8";
    private static final String SHIP_MANAGER_ROLE = "3";
    private static final String SHIP_OPERATOR_ROLE = "2";
    private static final String TECHNICAL_MANAGER_ROLE = "14";
    private static final String DOC_COMPANY_ROLE = "10";
    private static final String REGISTERED_OWNER_ROLE = "4";
    private static final String GROUP_BENEFICIAL_OWNER = "6";

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Id
    @Column(name = "asset_version_id", nullable = false, updatable = false)
    private Long assetVersionId;

    @Column(name = "parent_published_version_id")
    private Long parentPublishVersionId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "asset_type_id")
    private AssetTypeDO assetType;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_category_id"))})
    private LinkDO assetCategory;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "imo_number"))})
    private LinkDO ihsAsset;

    @Column(name = "build_date")
    @Temporal(TemporalType.DATE)
    private Date buildDate;

    @Column(name = "estimated_build_date")
    @Temporal(TemporalType.DATE)
    private Date estimatedBuildDate;

    @Column(name = "date_of_registry")
    @Temporal(TemporalType.DATE)
    private Date dateOfRegistry;

    @Column(name = "keel_laying_date")
    @Temporal(TemporalType.DATE)
    private Date keelLayingDate;

    @Column(name = "gross_tonnage")
    private Double grossTonnage;

    @Column(name = "builder", length = 50)
    private String builder;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "class_status_id")
    private ClassStatusDO classStatus;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "flag_state_id")
    private FlagStateDO flagState;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "registered_port_id"))})
    private LinkDO registeredPort;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "hull_class_notation", length = 500)
    private String classNotation;

    @Column(name = "machinery_class_notation", length = 500)
    private String machineryClassNotation;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "class_maintenance_status_id"))})
    private LinkDO classMaintenanceStatus;

    @Column(name = "lead_asset_flag", columnDefinition = "default 1")
    private Boolean isLead;

    @Column(name = "lead_imo")
    private Long leadImo;

    @JoinColumn(name = "asset_item_id")
    @OneToOne(cascade = {CascadeType.REFRESH})
    private ItemDO assetItem;

    @Column(name = "primary_yard_number", length = 20)
    private String yardNumber;

    @OneToMany(mappedBy = "asset", fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<AssetCustomerDO> customers;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "society_ruleset_id"))})
    private LinkDO ruleSet;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "previous_society_ruleset_id"))})
    private LinkDO previousRuleSet;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "product_ruleset_id"))})
    private LinkDO productRuleSet;

    @Column(name = "harmonisation_date")
    private Date harmonisationDate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "asset_lifecycle_status_id"))})
    private LinkDO assetLifecycleStatus;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "co_classification_society_id"))})
    private LinkDO coClassificationSociety;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "asset", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<AssetOfficeDO> offices;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "country_of_build_id"))})
    private LinkDO countryOfBuild;

    @Column(name = "call_sign", length = 100)
    private String callSign;

    @Column(name = "descriptive_note", length = 500)
    private String descriptiveNote;

    @Column(name = "hull_indicator")
    private Integer hullIndicator;

    @JoinColumn(name = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private AssetDO asset;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "class_department_id"))})
    private LinkDO classDepartment;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "linked_asset_id"))})
    private LinkDO linkedAsset;

    @Column(name = "effective_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;

    @Column(name = "dead_weight")
    private Double deadWeight;

    @Column(name = "imo_number", insertable = false, updatable = false)
    private String imoNumber;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + SHIP_BUILDER_ROLE)
    private List<PartyDO> builderInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + SHIP_MANAGER_ROLE)
    private List<PartyDO> shipManagerInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + SHIP_OPERATOR_ROLE)
    private List<PartyDO> operatorInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + TECHNICAL_MANAGER_ROLE)
    private List<PartyDO> techManagerInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + DOC_COMPANY_ROLE)
    private List<PartyDO> docCompanyInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + REGISTERED_OWNER_ROLE)
    private List<PartyDO> registeredOwnerInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "MAST_ASSET_Asset_Party", joinColumns = {
                                                               @JoinColumn(name = "asset_id", referencedColumnName = "id", insertable = false, updatable = false),
                                                               @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id", insertable = false, updatable = false)},
                                                               inverseJoinColumns = {@JoinColumn(name = "party_id", referencedColumnName = "id", insertable = false, updatable = false)})
    @WhereJoinTable(clause = "deleted = false AND party_role_id = " + GROUP_BENEFICIAL_OWNER)
    private List<PartyDO> groupOwnerInfo;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getAssetVersionId()
    {
        return this.assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    public AssetTypeDO getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final AssetTypeDO assetType)
    {
        this.assetType = assetType;
    }

    public LinkDO getAssetCategory()
    {
        return this.assetCategory;
    }

    public void setAssetCategory(final LinkDO assetCategory)
    {
        this.assetCategory = assetCategory;
    }

    public LinkDO getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final LinkDO ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public Date getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public Date getEstimatedBuildDate()
    {
        return this.estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public Date getKeelLayingDate()
    {
        return this.keelLayingDate;
    }

    public void setKeelLayingDate(final Date keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public String getBuilder()
    {
        return this.builder;
    }

    public void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public ClassStatusDO getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final ClassStatusDO classStatus)
    {
        this.classStatus = classStatus;
    }

    public LinkDO getRegisteredPort()
    {
        return this.registeredPort;
    }

    public void setRegisteredPort(final LinkDO registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getClassNotation()
    {
        return this.classNotation;
    }

    public void setClassNotation(final String classNotation)
    {
        this.classNotation = classNotation;
    }

    public Boolean getIsLead()
    {
        return this.isLead;
    }

    public void setIsLead(final Boolean isLead)
    {
        this.isLead = isLead;
    }

    public Long getLeadImo()
    {
        return this.leadImo;
    }

    public void setLeadImo(final Long leadImo)
    {
        this.leadImo = leadImo;
    }

    public ItemDO getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final ItemDO assetItem)
    {
        this.assetItem = assetItem;
    }

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public List<AssetCustomerDO> getCustomers()
    {
        return this.customers;
    }

    public void setCustomers(final List<AssetCustomerDO> customers)
    {
        this.customers = customers;
    }

    public LinkDO getRuleSet()
    {
        return this.ruleSet;
    }

    public void setRuleSet(final LinkDO ruleSet)
    {
        this.ruleSet = ruleSet;
    }

    public LinkDO getPreviousRuleSet()
    {
        return this.previousRuleSet;
    }

    public void setPreviousRuleSet(final LinkDO previousRuleSet)
    {
        this.previousRuleSet = previousRuleSet;
    }

    public LinkDO getProductRuleSet()
    {
        return this.productRuleSet;
    }

    public void setProductRuleSet(final LinkDO productRuleSet)
    {
        this.productRuleSet = productRuleSet;
    }

    public Date getHarmonisationDate()
    {
        return this.harmonisationDate;
    }

    public void setHarmonisationDate(final Date harmonisationDate)
    {
        this.harmonisationDate = harmonisationDate;
    }

    // public List<AttachmentLinkDO> getAttachments()
    // {
    // return attachments;
    // }
    //
    // public void setAttachments(final List<AttachmentLinkDO> attachments)
    // {
    // this.attachments = attachments;
    // }

    public LinkDO getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final LinkDO assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public LinkDO getCoClassificationSociety()
    {
        return this.coClassificationSociety;
    }

    public void setCoClassificationSociety(final LinkDO coClassificationSociety)
    {
        this.coClassificationSociety = coClassificationSociety;
    }

    public List<AssetOfficeDO> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<AssetOfficeDO> offices)
    {
        this.offices = offices;
    }

    public FlagStateDO getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final FlagStateDO flagState)
    {
        this.flagState = flagState;
    }

    public LinkDO getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final LinkDO classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public Date getDateOfRegistry()
    {
        return this.dateOfRegistry;
    }

    public void setDateOfRegistry(final Date dateOfRegistry)
    {
        this.dateOfRegistry = dateOfRegistry;
    }

    public LinkDO getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final LinkDO countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getDescriptiveNote()
    {
        return this.descriptiveNote;
    }

    public void setDescriptiveNote(final String descriptiveNote)
    {
        this.descriptiveNote = descriptiveNote;
    }

    public String getMachineryClassNotation()
    {
        return this.machineryClassNotation;
    }

    public void setMachineryClassNotation(final String machineryClassNotation)
    {
        this.machineryClassNotation = machineryClassNotation;
    }

    public Integer getHullIndicator()
    {
        return this.hullIndicator;
    }

    public void setHullIndicator(final Integer hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    public Long getParentPublishVersionId()
    {
        return this.parentPublishVersionId;
    }

    public void setParentPublishVersionId(final Long parentPublishVersionId)
    {
        this.parentPublishVersionId = parentPublishVersionId;
    }

    public AssetDO getAsset()
    {
        return this.asset;
    }

    public void setAsset(final AssetDO asset)
    {
        this.asset = asset;
    }

    public LinkDO getClassDepartment()
    {
        return this.classDepartment;
    }

    public void setClassDepartment(final LinkDO classDepartment)
    {
        this.classDepartment = classDepartment;
    }

    public LinkDO getLinkedAsset()
    {
        return this.linkedAsset;
    }

    public void setLinkedAsset(final LinkDO linkedAsset)
    {
        this.linkedAsset = linkedAsset;
    }

    public Date getEffectiveDate()
    {
        return this.effectiveDate;
    }

    public void setEffectiveDate(final Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public Double getDeadWeight()
    {
        return this.deadWeight;
    }

    public void setDeadWeight(final Double deadWeight)
    {
        this.deadWeight = deadWeight;
    }

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public List<PartyDO> getBuilderInfo()
    {
        return this.builderInfo;
    }

    public void setBuilderInfo(final List<PartyDO> builderInfo)
    {
        this.builderInfo = builderInfo;
    }

    public List<PartyDO> getShipManagerInfo()
    {
        return this.shipManagerInfo;
    }

    public void setShipManagerInfo(final List<PartyDO> shipManagerInfo)
    {
        this.shipManagerInfo = shipManagerInfo;
    }

    public List<PartyDO> getOperatorInfo()
    {
        return this.operatorInfo;
    }

    public void setOperatorInfo(final List<PartyDO> operatorInfo)
    {
        this.operatorInfo = operatorInfo;
    }

    public List<PartyDO> getTechManagerInfo()
    {
        return this.techManagerInfo;
    }

    public void setTechManagerInfo(final List<PartyDO> techManagerInfo)
    {
        this.techManagerInfo = techManagerInfo;
    }

    public List<PartyDO> getDocCompanyInfo()
    {
        return this.docCompanyInfo;
    }

    public void setDocCompanyInfo(final List<PartyDO> docCompanyInfo)
    {
        this.docCompanyInfo = docCompanyInfo;
    }

    public List<PartyDO> getRegisteredOwnerInfo()
    {
        return this.registeredOwnerInfo;
    }

    public void setRegisteredOwnerInfo(final List<PartyDO> registeredOwnerInfo)
    {
        this.registeredOwnerInfo = registeredOwnerInfo;
    }

    public List<PartyDO> getGroupOwnerInfo()
    {
        return this.groupOwnerInfo;
    }

    public void setGroupOwnerInfo(final List<PartyDO> groupOwnerInfo)
    {
        this.groupOwnerInfo = groupOwnerInfo;
    }
}
