package com.baesystems.ai.lr.domain.mast.entities.defects;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;

@Entity
@Table(name = "MAST_DEFECT_WIP_Defect_AssetItem")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_DEFECT_WIP_Defect_AssetItem SET deleted = true WHERE id = ?")
public class WIPDefectItemDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "item_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    private ItemDO item;

    @JoinColumn(name = "defect_id")
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private WIPDefectDO defect;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @Where(clause = "deleted = false")
    private List<WIPRepairItemDO> repairItems;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "parent_id"))})
    private LinkDO parent;

    @PreRemove
    private void preRemove()
    {
        if (repairItems != null)
        {
            for (final WIPRepairItemDO repairItem : repairItems)
            {
                repairItem.setDeleted(Boolean.TRUE);
            }
        }

        defect.setDeleted(Boolean.TRUE);
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public ItemDO getItem()
    {
        return item;
    }

    public void setItem(final ItemDO item)
    {
        this.item = item;
    }

    public List<WIPRepairItemDO> getRepairItems()
    {
        return repairItems;
    }

    public void setRepairItems(final List<WIPRepairItemDO> repairItems)
    {
        this.repairItems = repairItems;
    }

    public WIPDefectDO getDefect()
    {
        return this.defect;
    }

    public void setDefect(final WIPDefectDO defect)
    {
        this.defect = defect;
    }

    public LinkDO getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }
}
