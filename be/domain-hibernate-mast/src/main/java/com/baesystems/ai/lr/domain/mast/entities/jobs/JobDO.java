package com.baesystems.ai.lr.domain.mast.entities.jobs;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.AuditedDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetDO;
import com.baesystems.ai.lr.domain.mast.entities.attachments.AttachmentLinkDO;
import com.baesystems.ai.lr.domain.mast.entities.cases.CaseDO;
import com.baesystems.ai.lr.domain.mast.entities.reports.ReportDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;

@Entity
@Table(name = "MAST_JOB_Job")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_Job SET deleted = true WHERE id = ?")
public class JobDO extends AuditedDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "notes", length = 2500)
    private String note;

    @Column(name = "request_by_telephone_flag")
    private Boolean requestByTelephoneIndicator;

    @Column(name = "scoped_confirmed")
    private Boolean scopeConfirmed;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "scope_confirmed_by_user_id"))})
    private LinkDO scopeConfirmedBy;

    @Column(name = "scope_confirmed_date")
    private Date scopeConfirmedDate;

    @Column(name = "requested_attendance_date")
    @Temporal(TemporalType.DATE)
    private Date requestedAttendanceDate;

    @Column(name = "status_reason", length = 500)
    private String statusReason;

    @Column(name = "work_order_number")
    private Long workOrderNumber;

    @Column(name = "written_service_request_received_date")
    private Date writtenServiceRequestReceivedDate;

    @Column(name = "written_service_response_sent_date")
    private Date writtenServiceResponseSentDate;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumns({
                  @JoinColumn(name = "asset_id", referencedColumnName = "id"),
                  @JoinColumn(name = "asset_version_id", referencedColumnName = "asset_version_id")})
    private VersionedAssetDO asset;

    @JoinColumn(name = "case_id")
    @ManyToOne(cascade = {CascadeType.REFRESH})
    private CaseDO aCase;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "job", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<JobOfficeDO> offices;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "job", orphanRemoval = true)
    @Where(clause = "deleted = false")
    private List<JobResourceDO> employees;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "job")
    @Where(clause = "deleted = false")
    private List<AttachmentLinkDO> attachments;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "job")
    @Where(clause = "deleted = false")
    private List<ReportDO> reports;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "status_id", nullable = false))})
    private LinkDO jobStatus;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH}, mappedBy = "job")
    @Where(clause = "deleted = false")
    private List<SurveyDO> surveys;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "job")
    @Where(clause = "deleted = false")
    private List<JobDefectDO> defects;

    @Column(name = "created_date", updatable = false)
    @Temporal(TemporalType.DATE)
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "estimated_start_date")
    @Temporal(TemporalType.DATE)
    private Date etaDate;

    @Column(name = "estimated_end_date")
    @Temporal(TemporalType.DATE)
    private Date etdDate;

    @Column(name = "location", length = 50)
    private String location;

    @Column(name = "narrative", length = 50)
    private String description;

    @Column(name = "first_visit_date")
    @Temporal(TemporalType.DATE)
    private Date firstVisitDate;

    @Column(name = "last_visit_date")
    @Temporal(TemporalType.DATE)
    private Date lastVisitDate;

    @Column(name = "completed_by_id", length = 150)
    private String completedBy;

    @Column(name = "completed_on")
    @Temporal(TemporalType.DATE)
    private Date completedOn;

    @Column(name = "zero_visit_job")
    private Boolean zeroVisitJob;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "proposed_flag_state_id"))})
    private LinkDO proposedFlagState;

    @Column(name = "updated_on_date")
    @Temporal(TemporalType.DATE)
    private Date updatedOn;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "updated_on_behalf_of_id"))})
    private LinkDO updatedOnBehalfOf;

    @Column(name = " class_group_job", columnDefinition = "default 0")
    private Boolean classGroupJob;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "job_category_id", nullable = false))})
    private LinkDO jobCategory;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "job_team_id"))})
    private LinkDO jobTeam;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getNote()
    {
        return this.note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public Boolean getRequestByTelephoneIndicator()
    {
        return this.requestByTelephoneIndicator;
    }

    public void setRequestByTelephoneIndicator(final Boolean requestByTelephoneIndicator)
    {
        this.requestByTelephoneIndicator = requestByTelephoneIndicator;
    }

    public Date getRequestedAttendanceDate()
    {
        return this.requestedAttendanceDate;
    }

    public void setRequestedAttendanceDate(final Date requestedAttendanceDate)
    {
        this.requestedAttendanceDate = requestedAttendanceDate;
    }

    public String getStatusReason()
    {
        return this.statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Long getWorkOrderNumber()
    {
        return this.workOrderNumber;
    }

    public void setWorkOrderNumber(final Long workOrderNumber)
    {
        this.workOrderNumber = workOrderNumber;
    }

    public Date getWrittenServiceRequestReceivedDate()
    {
        return this.writtenServiceRequestReceivedDate;
    }

    public void setWrittenServiceRequestReceivedDate(final Date writtenServiceRequestReceivedDate)
    {
        this.writtenServiceRequestReceivedDate = writtenServiceRequestReceivedDate;
    }

    public Date getWrittenServiceResponseSentDate()
    {
        return this.writtenServiceResponseSentDate;
    }

    public void setWrittenServiceResponseSentDate(final Date writtenServiceResponseSentDate)
    {
        this.writtenServiceResponseSentDate = writtenServiceResponseSentDate;
    }

    public VersionedAssetDO getAsset()
    {
        return this.asset;
    }

    public void setAsset(final VersionedAssetDO asset)
    {
        this.asset = asset;
    }

    public List<JobOfficeDO> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<JobOfficeDO> offices)
    {
        this.offices = offices;
    }

    public List<JobResourceDO> getEmployees()
    {
        return this.employees;
    }

    public void setEmployees(final List<JobResourceDO> employees)
    {
        this.employees = employees;
    }

    public List<AttachmentLinkDO> getAttachments()
    {
        return this.attachments;
    }

    public void setAttachments(final List<AttachmentLinkDO> attachments)
    {
        this.attachments = attachments;
    }

    public List<ReportDO> getReports()
    {
        return this.reports;
    }

    public void setReports(final List<ReportDO> reports)
    {
        this.reports = reports;
    }

    public LinkDO getJobStatus()
    {
        return this.jobStatus;
    }

    public void setJobStatus(final LinkDO jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public List<SurveyDO> getSurveys()
    {
        return this.surveys;
    }

    public void setSurveys(final List<SurveyDO> surveys)
    {
        this.surveys = surveys;
    }

    public List<JobDefectDO> getDefects()
    {
        return this.defects;
    }

    public void setDefects(final List<JobDefectDO> defects)
    {
        this.defects = defects;
    }

    public Date getCreatedOn()
    {
        return this.createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getEtaDate()
    {
        return this.etaDate;
    }

    public void setEtaDate(final Date etaDate)
    {
        this.etaDate = etaDate;
    }

    public Date getEtdDate()
    {
        return this.etdDate;
    }

    public void setEtdDate(final Date etdDate)
    {
        this.etdDate = etdDate;
    }

    public CaseDO getaCase()
    {
        return this.aCase;
    }

    public void setaCase(final CaseDO aCase)
    {
        this.aCase = aCase;
    }

    public String getLocation()
    {
        return this.location;
    }

    public void setLocation(final String location)
    {
        this.location = location;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Boolean getScopeConfirmed()
    {
        return this.scopeConfirmed;
    }

    public void setScopeConfirmed(final Boolean scopeConfirmed)
    {
        this.scopeConfirmed = scopeConfirmed;
    }

    public LinkDO getScopeConfirmedBy()
    {
        return this.scopeConfirmedBy;
    }

    public void setScopeConfirmedBy(final LinkDO scopeConfirmedBy)
    {
        this.scopeConfirmedBy = scopeConfirmedBy;
    }

    public Date getScopeConfirmedDate()
    {
        return this.scopeConfirmedDate;
    }

    public void setScopeConfirmedDate(final Date scopeConfirmedDate)
    {
        this.scopeConfirmedDate = scopeConfirmedDate;
    }

    public Date getFirstVisitDate()
    {
        return this.firstVisitDate;
    }

    public void setFirstVisitDate(final Date firstVisitDate)
    {
        this.firstVisitDate = firstVisitDate;
    }

    public Date getLastVisitDate()
    {
        return this.lastVisitDate;
    }

    public void setLastVisitDate(final Date lastVisitDate)
    {
        this.lastVisitDate = lastVisitDate;
    }

    public String getCompletedBy()
    {
        return this.completedBy;
    }

    public void setCompletedBy(final String completedBy)
    {
        this.completedBy = completedBy;
    }

    public Date getCompletedOn()
    {
        return this.completedOn;
    }

    public void setCompletedOn(final Date completedOn)
    {
        this.completedOn = completedOn;
    }

    public Boolean getZeroVisitJob()
    {
        return this.zeroVisitJob;
    }

    public void setZeroVisitJob(final Boolean zeroVisitJob)
    {
        this.zeroVisitJob = zeroVisitJob;
    }

    public LinkDO getProposedFlagState()
    {
        return this.proposedFlagState;
    }

    public void setProposedFlagState(final LinkDO proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    public LinkDO getUpdatedOnBehalfOf()
    {
        return this.updatedOnBehalfOf;
    }

    public void setUpdatedOnBehalfOf(final LinkDO updatedOnBehalfOf)
    {
        this.updatedOnBehalfOf = updatedOnBehalfOf;
    }

    public Boolean getClassGroupJob()
    {
        return this.classGroupJob;
    }

    public void setClassGroupJob(final Boolean classGroupJob)
    {
        this.classGroupJob = classGroupJob;
    }

    public LinkDO getJobCategory()
    {
        return this.jobCategory;
    }

    public void setJobCategory(final LinkDO jobCategory)
    {
        this.jobCategory = jobCategory;
    }

    public LinkDO getJobTeam()
    {
        return this.jobTeam;
    }

    public void setJobTeam(final LinkDO jobTeam)
    {
        this.jobTeam = jobTeam;
    }
}
