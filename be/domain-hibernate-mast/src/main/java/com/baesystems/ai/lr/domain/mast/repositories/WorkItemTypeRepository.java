package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemTypeRepository extends SpringDataRepository<WorkItemTypeDO, Long>
{

}
