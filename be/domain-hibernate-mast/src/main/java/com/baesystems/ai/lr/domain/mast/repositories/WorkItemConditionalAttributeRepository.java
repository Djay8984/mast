package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.WorkItemConditionalAttributeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface WorkItemConditionalAttributeRepository extends SpringDataRepository<WorkItemConditionalAttributeDO, Long>
{
    @Query("select conditionalAttr from WorkItemConditionalAttributeDO conditionalAttr, VersionedItemDO item"
            + " where conditionalAttr.serviceCode = :serviceCode"
            + " and item.id = :itemId"
            + " and conditionalAttr.itemType.id = item.itemType.id"
            + " and conditionalAttr.workItemAction.id = :workItemActionId")
    WorkItemConditionalAttributeDO findConditionalAttribute(@Param("serviceCode") String serviceCode, @Param("itemId") Long itemId,
            @Param("workItemActionId") Long workItemActionId);
}
