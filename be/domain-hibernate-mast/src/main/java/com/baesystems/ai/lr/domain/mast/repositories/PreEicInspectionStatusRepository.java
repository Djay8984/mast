package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.PreEicInspectionStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface PreEicInspectionStatusRepository extends SpringDataRepository<PreEicInspectionStatusDO, Long>
{

}
