package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.AttributeDataTypeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface AttributeDataTypeRepository extends SpringDataRepository<AttributeDataTypeDO, Long>
{

}
