package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import com.baesystems.ai.lr.domain.mast.entities.assets.AttributeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;

public interface AttributeRepository extends SpringDataRepository<AttributeDO, Long>
{
    @Query("select attr from AttributeDO attr join attr.item item where item.id = :itemId")
    Page<AttributeDO> findAttributesByItemId(@Param("itemId") Long itemId, Pageable pageable);

    @Query("select count(attr.id) from AttributeDO attr"
            + " join attr.item ite"
            + " join ite.asset ass"
            + " where attr.id = :attributeId"
            + " and ite.id = :itemId"
            + " and ass.id = :assetId")
    Integer attributeExistsForItemAndAsset(@Param("attributeId") Long attributeId,
            @Param("itemId") Long itemId,
            @Param("assetId") Long assetId);

    @Query("select count(u.id) from AttributeDO u join u.item item"
            + " where item.id = :itemId"
            + " and u.attributeType.id = :attributeTypeId"
            + " and item.asset.assetVersionId = :versionId")
    Integer countAttributesByItemAndAttributeType(
            @Param("itemId") Long itemId,
            @Param("attributeTypeId") Long attributeTypeId,
            @Param("versionId") Long versionId);

    @Query("select attr from AttributeDO attr join attr.item item, AttributeTypeDO attrType"
            + " where item.id = :itemId"
            + " and attr.attributeType.id  = attrType.id"
            + " and (attrType.namingOrder is not null AND attrType.namingOrder > 0)"
            + " order by attrType.namingOrder, attr.id asc")
    List<AttributeDO> findAttributesWithNamingOrderForItem(@Param("itemId") Long itemId);

    @Query("select attr from AttributeDO attr"
            + " where (:#{#query.attributeTypeId} is null or attr.attributeType.id = :#{#query.attributeTypeId})")
    List<AttributeDO> findAttributesByType(@Param("query") AssetQueryDto query);
}
