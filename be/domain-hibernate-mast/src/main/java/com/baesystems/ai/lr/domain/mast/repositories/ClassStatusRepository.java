package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.ClassStatusDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ClassStatusRepository extends SpringDataRepository<ClassStatusDO, Long>{

}
