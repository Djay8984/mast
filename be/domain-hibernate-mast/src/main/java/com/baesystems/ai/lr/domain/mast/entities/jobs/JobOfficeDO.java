package com.baesystems.ai.lr.domain.mast.entities.jobs;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.MastDO;

@Entity
@Table(name = "MAST_JOB_JobOffice")
@Where(clause = "deleted = false")
@SQLDelete(sql = "UPDATE MAST_JOB_JobOffice SET deleted = true WHERE id = ?")
public class JobOfficeDO extends MastDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "job_id", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private JobDO job;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "office_role_id", nullable = false))})
    private LinkDO officeRole;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "lr_office_id", nullable = false))})
    private LinkDO office;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public JobDO getJob()
    {
        return job;
    }

    public void setJob(final JobDO job)
    {
        this.job = job;
    }

    public LinkDO getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkDO officeRole)
    {
        this.officeRole = officeRole;
    }

    public LinkDO getOffice()
    {
        return office;
    }

    public void setOffice(final LinkDO office)
    {
        this.office = office;
    }
}
