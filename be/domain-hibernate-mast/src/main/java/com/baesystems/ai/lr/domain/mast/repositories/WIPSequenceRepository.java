package com.baesystems.ai.lr.domain.mast.repositories;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WIPSequenceRepository
{
    @Query("select max(sequenceNumber) from #{#entityName} e"
            + " where e.job.id in (select j.id from JobDO j join j.asset asset where asset.id = :assetId)")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Integer maxSequenceNumberForAssetIdWithLock(@Param("assetId") Long assetId);
}
