package com.baesystems.ai.lr.domain.mast.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.assets.ItemDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ItemRepository extends SpringDataRepository<ItemDO, Long>
{
    @Query("select u.assetItem from VersionedAssetDO u "
            + " where u.id = :assetId"
            + " and u.assetVersionId = :versionId")
    ItemDO getRootItem(
            @Param("assetId") Long assetId,
            @Param("versionId") Long versionId);
}
