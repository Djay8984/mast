package com.baesystems.ai.lr.domain.mast.repositories;

import com.baesystems.ai.lr.domain.mast.entities.references.IacsSocietyDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IacsSocietyRepository extends SpringDataRepository<IacsSocietyDO, Long>
{

}
