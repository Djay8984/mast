package com.baesystems.ai.lr.domain.mast.entities.tasks;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.services.SurveyDO;

import java.util.List;

@Entity
@Table (name = "MAST_JOB_WorkItem")
@Where (clause = "deleted = false")
@SQLDelete (sql = "UPDATE MAST_JOB_WorkItem SET deleted = true WHERE id = ?")
public class WIPWorkItemDO extends BaseWorkItemDO
{
    @Embedded
    @AttributeOverrides ({@AttributeOverride (name = "id", column = @Column (name = "parent_id"))})
    private LinkDO parent;

    @JoinColumn (name = "job_service_instance_id")
    @ManyToOne (fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
    private SurveyDO survey;

    @Column(name = "action_taken_date")
    private Date actionTakenDate;

    @OneToMany (fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy = "workItem", orphanRemoval = true)
    @Where (clause = "deleted = false")
    private List<WIPWorkItemAttributeDO> attributes;

    public LinkDO getParent()
    {
        return parent;
    }

    public void setParent(final LinkDO parent)
    {
        this.parent = parent;
    }

    public SurveyDO getSurvey()
    {
        return survey;
    }

    public void setSurvey(final SurveyDO survey)
    {
        this.survey = survey;
    }

    public List<WIPWorkItemAttributeDO> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(final List<WIPWorkItemAttributeDO> attributes)
    {
        this.attributes = attributes;
    }

    public Date getActionTakenDate()
    {
        return actionTakenDate;
    }

    public void setActionTakenDate(final Date actionTakenDate)
    {
        this.actionTakenDate = actionTakenDate;
    }
}
