package com.baesystems.ai.lr.domain.mast.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.mast.entities.references.ProductCatalogueDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface ProductCatalogueRepository extends SpringDataRepository<ProductCatalogueDO, Long>
{
    @Query("select distinct productCatalogue from ProductCatalogueDO productCatalogue"
            + " left join productCatalogue.ruleSets ruleSet"
            + " where ruleSet.id = :rulesetId"
            + " or productCatalogue.productGroup.productType.id in (:productTypeId)")
    List<ProductCatalogueDO> findByRulesetIdAndProductType(@Param("rulesetId") Long rulesetId, @Param("productTypeId") List<Long> productTypeId);

    @Query("select productCatalogue.productGroup.productType.id = :productTypeId from ProductCatalogueDO productCatalogue"
            + " where productCatalogue.id = :productCatalogueId")
    Boolean productCatalogueIsOfType(@Param("productCatalogueId") Long productCatalogueId, @Param("productTypeId") Long productTypeId);
}
