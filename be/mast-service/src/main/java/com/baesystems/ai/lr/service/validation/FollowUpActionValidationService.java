package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface FollowUpActionValidationService
{
    void verifyJobHasFollowUpAction(Long jobId, Long followUpActionId) throws RecordNotFoundException;
}
