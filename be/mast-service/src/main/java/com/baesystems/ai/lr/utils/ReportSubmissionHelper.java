package com.baesystems.ai.lr.utils;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

public interface ReportSubmissionHelper
{
    WorkItemLightDto mapWipTaskToTask(WorkItemDto wipTask, Long parentCodicilId);

    void mergeDefects(DefectDto wipDefect, DefectDto defect);

    void addJob(List<JobDefectDto> jobLinks, Long jobId);

    List<DefectItemDto> mergeItemLists(List<DefectItemDto> wipDefectItemList, List<DefectItemDto> defectItemList);

    List<DefectDefectValueDto> mergeValueLists(List<DefectDefectValueDto> wipDefectValueList, List<DefectDefectValueDto> defectValueList);

    <T extends BaseDto> void generalWipToNonWipMap(T wip, T nonWip);

    DefectDto mapNewDefect(DefectDto wipDefect);

    ScheduledServiceDto serviceWipToNonWipMap(SurveyDto survey);
}
