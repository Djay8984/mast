package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.AllowedWorkItemAttributeValueDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.ClassRecommendationDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface JobReferenceDataService
{
    List<CertificateTypeDto> getCertificateTypes();

    CertificateTypeDto getCertificateType(Long certificateTypeId) throws RecordNotFoundException;

    List<ReferenceDataDto> getCertificateStatuses();

    ReferenceDataDto getCertificateStatus(Long certificateStatusId) throws RecordNotFoundException;

    List<ReferenceDataDto> getCertificateActions();

    ReferenceDataDto getCertificateAction(Long certificateActionId) throws RecordNotFoundException;

    List<ReferenceDataDto> getJobCategories();

    ReferenceDataDto getJobCategory(final Long categoryId) throws RecordNotFoundException;

    List<ReferenceDataDto> getJobStatuses();

    List<ReferenceDataDto> getCreditStatuses();

    List<ReferenceDataDto> getEndorsementTypes();

    List<ReferenceDataDto> getWorkItemTypes();

    List<ReferenceDataDto> getResolutionStatuses();

    List<WorkItemActionDto> getWorkItemActions();

    List<ReferenceDataDto> getReportTypes();

    List<ClassRecommendationDto> getClassRecommendations();

    List<AllowedWorkItemAttributeValueDto> getAllowedWorkItemAttributeValues();

    WorkItemActionDto findWorkItemActionFromReferenceCode(String referenceCode);
}
