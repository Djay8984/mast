package com.baesystems.ai.lr.service.reports;

import java.util.List;

import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface ReportService
{
    ReportDto getReport(Long reportId) throws RecordNotFoundException, BadRequestException;

    List<ReportLightDto> getReports(Long jobId) throws RecordNotFoundException;

    ReportDto saveReport(ReportDto reportDto) throws BadRequestException;

    Long getNextReportVersion(Long jobId, Long reportTypeId);

    ReportDto getLatestReport(Long jobId, Long typeId);

    ReportDto updateReport(ReportDto updatedReport) throws BadRequestException, StaleObjectException;
}
