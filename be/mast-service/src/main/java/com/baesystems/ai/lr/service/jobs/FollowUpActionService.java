package com.baesystems.ai.lr.service.jobs;

import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface FollowUpActionService
{

    PageResource<FollowUpActionDto> getFollowUpActionsForJob(Integer page, Integer size, String sort, String order, Long jobId)
            throws BadPageRequestException;

    void generateFollowUpActions(ReportDto report);

    FollowUpActionDto updateFollowUpAction(FollowUpActionDto followUpActionDto) throws StaleObjectException;

}
