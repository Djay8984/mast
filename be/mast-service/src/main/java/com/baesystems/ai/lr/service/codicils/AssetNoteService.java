package com.baesystems.ai.lr.service.codicils;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface AssetNoteService
{
    AssetNoteDto getAssetNote(Long assetNoteId) throws RecordNotFoundException;

    PageResource<AssetNoteDto> getAssetNotesByAsset(Integer page, Integer size, String sort, String order, Long assetId, Long statusId)
            throws BadPageRequestException;

    AssetNoteDto saveAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException;

    PageResource<AssetNoteDto> getWIPAssetNotesForJob(Integer page, Integer size, String sort, String order, Long jobId)
            throws BadPageRequestException;

    AssetNoteDto updateAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException, StaleObjectException;

    Boolean deleteAssetNote(Long assetNoteId) throws RecordNotFoundException;

    Boolean deleteWIPAssetNote(Long assetNoteId) throws RecordNotFoundException;

    TemplateAssetCountDto countAssetsWithAssetNotesForTemplate(List<Long> templateIds);

    PageResource<AssetNoteDto> getWIPAssetNotesByQuery(Long jobId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;

    AssetNoteDto updateWIPAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException, StaleObjectException;

    AssetNoteDto saveWIPAssetNote(AssetNoteDto assetNoteDto) throws RecordNotFoundException;

    AssetNoteDto getWIPAssetNote(Long assetNoteId) throws RecordNotFoundException;

    PageResource<AssetNoteDto> getAssetNotesByQuery(Long assetId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto) throws BadPageRequestException;

}
