package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.AllowedAssetAttributeValueDto;
import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ClassDepartmentDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AssetReferenceDataService
{
    List<AssetCategoryDto> getAssetCategories();

    AssetCategoryDto getAssetCategory(Long assetCategoryId) throws RecordNotFoundException;

    List<AssetTypeDto> getAssetTypes();

    AssetTypeDto getAssetType(Long assetTypeId) throws RecordNotFoundException;

    List<ReferenceDataDto> getCustomerFunctions();

    ReferenceDataDto getCustomerFunction(Long customerFunctionId) throws RecordNotFoundException;

    List<AttributeTypeDto> getAttributeTypes();

    AttributeTypeDto getAttributeType(Long attributeTypeId) throws RecordNotFoundException;

    List<ItemTypeDto> getItemTypes();

    ItemTypeDto getItemType(Long itemTypeId) throws RecordNotFoundException;

    List<ReferenceDataDto> getCustomerRelationships();

    ReferenceDataDto getCustomerRelationship(Long customerRelationshipId) throws RecordNotFoundException;

    AssetModelTemplateDto getAssetModelTemplate();

    ReferenceDataDto getIacsSociety(Long id) throws RecordNotFoundException;

    List<ReferenceDataDto> getIacsSocieties();

    SocietyRulesetDto getRuleSet(Long id) throws RecordNotFoundException;

    List<SocietyRulesetDto> getRuleSets(String category, Boolean isLrRuleset);

    List<ReferenceDataDto> getAssetLifecycleStatuses();

    List<ReferenceDataDto> getClassStatuses();

    List<ReferenceDataDto> getAttributeTypeValues();

    List<AllowedAssetAttributeValueDto> getAllowedAssetAttributeValues();

    List<ItemRuleDto> getItemTypeRelationships();

    List<ReferenceDataDto> getItemRelationshipTypes();

    ReferenceDataDto getClassStatus(Long classStatusId);

    ClassDepartmentDto getClassDepartment(Long classDepartmentId);

    ReferenceDataDto getAssetLifecycleStatus(Long lifecycleStatusId);

    ReferenceDataDto getProductRuleSet(Long id);

    List<ReferenceDataDto> getProductRuleSets();

    List<ReferenceDataDto> getCountries();

    List<ClassDepartmentDto> getClassDepartments();

    List<ReferenceDataDto> getClassMaintenanceStatuses();

    List<ReferenceDataDto> getActionsTaken();

    List<ReferenceDataDto> getClassNotations();

    List<ReferenceDataDto> getMigrationStatuses();
}
