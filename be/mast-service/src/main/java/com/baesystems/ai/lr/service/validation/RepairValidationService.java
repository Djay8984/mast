package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface RepairValidationService
{
    void validateCoCHasRepair(String inputCoCId, String inputRepairId) throws RecordNotFoundException, BadRequestException;

    void validateDefectHasRepair(String inputDefectId, String inputRepairId) throws RecordNotFoundException, BadRequestException;

    void verifyRepairIdExists(String inputRepairId) throws RecordNotFoundException, BadRequestException;

    void validateWIPCoCHasWIPRepair(String inputCoCId, String inputRepairId) throws RecordNotFoundException, BadRequestException;

    void validateWIPDefectHasWIPRepair(String inputDefectId, String inputRepairId) throws RecordNotFoundException, BadRequestException;

    void verifyWIPRepairIdExists(String inputRepairId) throws RecordNotFoundException, BadRequestException;

    Boolean isRepairPermanent(RepairDto repair);
}
