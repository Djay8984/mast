package com.baesystems.ai.lr.service.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface BatchActionValidationService
{
    void verifyActionableItemStatus(ActionableItemDto actionableItemDto) throws BadRequestException;

    void verifyAssetIds(List<Long> assetIds) throws BadRequestException, RecordNotFoundException;

    void verifyAssetIdListNotNull(List<Long> assetIdList) throws BadRequestException;

    void verifyAssetNoteStatus(AssetNoteDto assetNoteDto) throws BadRequestException;
}
