package com.baesystems.ai.lr.service.flagports;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface FlagReferenceDataService
{
    FlagStateDto getFlagState(Long flagStateId) throws RecordNotFoundException;

    PageResource<FlagStateDto> getFlagStates(Integer page, Integer size, String sort, String order, String searchString)
            throws BadRequestException, BadPageRequestException;

    List<FlagStateDto> getFlags();
}
