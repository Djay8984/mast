package com.baesystems.ai.lr.service.validation;

import java.text.ParseException;

import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.DuplicateCaseException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CaseValidationService
{
    boolean caseUpdateRequiresUniquenessValidation(CaseWithAssetDetailsDto caseDto);

    void verifyCase(CaseWithAssetDetailsDto caseDto)
            throws ParseException, RecordNotFoundException, BadRequestException, MastBusinessException;

    void verifyCaseIdExists(String inputCaseId)
            throws RecordNotFoundException, BadRequestException;

    void verifyCaseIsUnique(CaseWithAssetDetailsDto caseDto)
            throws DuplicateCaseException;

    void validateCaseSave(CaseWithAssetDetailsDto caseDto)
            throws BadRequestException, ParseException, RecordNotFoundException, MastBusinessException;

    void validateCaseUpdate(Long caseId, CaseWithAssetDetailsDto caseDto)
            throws BadRequestException, ParseException, RecordNotFoundException, MastBusinessException;
}
