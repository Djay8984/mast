package com.baesystems.ai.lr.service.spellcheck;

import com.baesystems.ai.lr.exception.MastBusinessException;

public interface DictionaryService
{
    byte[] getDictionary(String dictionaryPath) throws MastBusinessException;
}
