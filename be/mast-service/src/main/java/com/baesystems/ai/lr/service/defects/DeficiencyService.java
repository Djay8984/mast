package com.baesystems.ai.lr.service.defects;

import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;

public interface DeficiencyService
{
    PageResource<DeficiencyDto> getDeficienciesByQuery(Long assetId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;

    PageResource<DeficiencyDto> getWIPDeficienciesByQuery(Long jobId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;
}
