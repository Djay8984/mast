package com.baesystems.ai.lr.exception;

import com.baesystems.ai.lr.enums.ExceptionType;

public class InverseRelationshipException extends MastBusinessException
{
    private static final long serialVersionUID = 7548321094324233422L;

    public InverseRelationshipException(final String message)
    {
        super(message);
    }

    @Override
    public ExceptionType getType()
    {
        return ExceptionType.INVERSE_RELATIONSHIP;
    }
}
