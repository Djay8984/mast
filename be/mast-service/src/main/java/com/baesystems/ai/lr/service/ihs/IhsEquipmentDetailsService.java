package com.baesystems.ai.lr.service.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsEquipmentDetailsService
{
    IhsEquipmentDetailsDto getEquipmentDetails(Long imoNumber) throws RecordNotFoundException;
}
