package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.exception.BadRequestException;

public interface QueryValidationService
{
    void verifyQueryId(Object[] inputIds) throws BadRequestException;

    void verifyQuery(String query, String allowedParams) throws BadRequestException;
}
