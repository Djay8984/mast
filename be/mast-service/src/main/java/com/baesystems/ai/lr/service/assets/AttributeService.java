package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AttributeService
{
    AttributeDto updateAttribute(Long itemId, AttributeDto attributeDto) throws RecordNotFoundException;
    
    ItemWithAttributesDto updateAttributes(Long assetId, Long itemId, ItemWithAttributesDto itemWithAttributesDto)
            throws RecordNotFoundException, MastBusinessException, BadPageRequestException;

    AttributeDto createAttribute(Long assetId, Long itemId, AttributeDto attributeDto) throws RecordNotFoundException, MastBusinessException;

    void deleteAttribute(Long assetId, Long itemId, Long attributeId) throws RecordNotFoundException, MastBusinessException;

    PageResource<AttributeDto> getAttributes(Integer page, Integer size, String sort, String order, Long itemId)
            throws RecordNotFoundException, BadPageRequestException;

    AttributeDto getAttribute(Long attributeId);
}
