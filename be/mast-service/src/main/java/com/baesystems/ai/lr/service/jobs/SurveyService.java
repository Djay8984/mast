package com.baesystems.ai.lr.service.jobs;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface SurveyService
{
    PageResource<SurveyDto> getSurveys(Integer page, Integer size, String sort, String order, Long jobId)
            throws RecordNotFoundException, BadPageRequestException;

    Boolean deleteSurvey(Long surveyId);

    SurveyDto createSurvey(Long jobId, SurveyDto surveyDto);

    SurveyDto updateSurvey(Long surveyId, SurveyDto surveyDto) throws RecordNotFoundException, StaleObjectException;

    SurveyListDto updateSurveys(SurveyListDto surveys) throws RecordNotFoundException, BadPageRequestException, StaleObjectException;

    void createWIPCoCSurveyForPermanentRepairs(Long jobId, RepairDto repairDto) throws RecordNotFoundException;

    void createAssetManagementSurveyForWipCoC(Long jobId, Long wipCoCId) throws RecordNotFoundException;

    SurveyDto getSurvey(Long surveyId) throws RecordNotFoundException;
}
