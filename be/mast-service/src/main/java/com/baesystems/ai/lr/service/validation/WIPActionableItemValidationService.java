package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface WIPActionableItemValidationService
{
    void verifyJobHasWIPActionableItem(Long jobId, Long actionableItemId)
            throws RecordNotFoundException;

    void verifyOptionalParentIdExistsAndIsOfCorrectType(Long parentCodicilId)
            throws RecordNotFoundException;

    void verifyWIPActionableItemPost(String jobId, ActionableItemDto actionableItem, Boolean allowNullDueDate)
            throws BadRequestException, RecordNotFoundException, MastBusinessException;

    void verifyWIPActionableItemPut(String jobId, String actionableItemId, ActionableItemDto actionableItem, Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;
}
