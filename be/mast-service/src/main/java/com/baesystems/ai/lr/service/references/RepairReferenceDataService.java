package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.RepairReferenceDataDto;
import com.baesystems.ai.lr.dto.references.MaterialDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface RepairReferenceDataService
{
    List<MaterialDto> getMaterials();

    List<ReferenceDataDto> getMaterialTypes();

    List<ReferenceDataDto> getRepairTypes();

    List<ReferenceDataDto> getRepairActions();

    ReferenceDataDto getRepairAction(Long repairActionId) throws RecordNotFoundException;

    RepairReferenceDataDto getReferenceData();
}
