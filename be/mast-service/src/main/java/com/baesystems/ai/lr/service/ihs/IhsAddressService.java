package com.baesystems.ai.lr.service.ihs;

import java.util.List;

import com.baesystems.ai.lr.dto.ihs.CompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsAddressService
{
     List<CompanyContactDto> getAddress(Long imoNumber) throws RecordNotFoundException;
     
     List<IhsPersonnelContactDto> getPersonnelAddress(final Long imoNumber) throws RecordNotFoundException;
}
