package com.baesystems.ai.lr.service.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface JobValidationService
{
    void validateJobCreate(JobDto jobDto) throws BadRequestException, MastBusinessException;

    void validateJobUpdate(Long jobId, JobDto jobDto) throws BadRequestException, MastBusinessException, RecordNotFoundException;

    void verifyJobIdExists(String inputJobId) throws RecordNotFoundException, BadRequestException;

    void verifyServiceDeliveryOfficePresent(List<OfficeLinkDto> offices) throws MastBusinessException;

    void validateEmployeeAndTeamId(Long employeeId, Long teamId) throws BadRequestException;

    void verifyJobIdExistsforAssetId(Long assetId, Long jobId) throws RecordNotFoundException;
}
