package com.baesystems.ai.lr.service.validation;

import java.text.ParseException;
import java.util.List;

import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ServiceValidationService
{
    void validateDeleteServices(String inputAssetId, List<Long> inputServiceIds)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void validateProductList(String inputAssetId, ProductListDto selectedProductList)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void validateService(String inputAssetId, ScheduledServiceListDto newServices)
            throws BadRequestException, RecordNotFoundException, ParseException, MastBusinessException;

    void validateServiceUpdate(String serviceId, String assetId, ScheduledServiceDto serviceDto)
            throws RecordNotFoundException, BadRequestException, MastBusinessException, ParseException;

    void verifyAssetHasService(String serviceId, String assetId)
            throws RecordNotFoundException, BadRequestException;

    void verifyHarmonisationDate(Long assetId, DateDto harmonisationDateDto)
            throws BadRequestException, MastBusinessException;
}
