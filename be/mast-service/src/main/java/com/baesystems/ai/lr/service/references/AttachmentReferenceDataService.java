package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AttachmentReferenceDataService
{
    List<ReferenceDataDto> getAttachmentTypes();

    ReferenceDataDto getAttachmentType(Long attachmentTypeId) throws RecordNotFoundException;

    List<ReferenceDataDto> getConfidentialityTypes();

    ReferenceDataDto getConfidentialityType(Long confidentialityTypeId) throws RecordNotFoundException;
}
