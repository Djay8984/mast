package com.baesystems.ai.lr.service.assets;

import java.util.Date;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface AssetService
{
    AssetLightDto getAsset(Long assetId, Long versionId, Long jobId, Boolean getPublished, Boolean getDraft) throws RecordNotFoundException;

    AssetModelDto getAssetModel(Long assetId, Long versionId) throws RecordNotFoundException;

    AssetDto createAsset(AssetLightDto assetLightDto) throws MastBusinessException;

    AssetLightDto updateAsset(AssetLightDto assetLightDto) throws RecordNotFoundException, StaleObjectException;

    void deleteAsset(Long assetId);

    PageResource<AssetLightDto> getAssets(Integer page, Integer size, String sort, String order, AssetQueryDto query)
            throws BadRequestException, BadPageRequestException;

    PageResource<AssetLightDto> getAssetsAbstract(Integer page, Integer size, String sort, String order, AbstractQueryDto query)
            throws BadRequestException, BadPageRequestException;

    Boolean isHarmonisationDateDifferent(Long assetId, Date harmonisationDate) throws RecordNotFoundException;

    void updateHarmonisationDate(Long jobId, Date harmonisationDate) throws RecordNotFoundException;

    PageResource<MultiAssetDto> getAssetsFromBothDataBasesAbstract(Integer page, Integer size, String sort, String order, AbstractQueryDto query)
            throws BadRequestException, BadPageRequestException;
}
