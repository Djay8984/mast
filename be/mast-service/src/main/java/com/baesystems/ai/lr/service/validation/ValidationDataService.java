package com.baesystems.ai.lr.service.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;

public interface ValidationDataService
{
    AssetModelTemplateDto getAssetModelTemplate();

    Long findAttachmentByObject(AttachmentTargetType target, Long objectId, Long attachmentId);

    List<Long> findDuplicateCases(Long businessProcessId, Long assetId);

    CaseValidationDto verifyCase(Long imoNumber, String builder, String yardNumber, Long businessProcess);

    Long getCurrentStatusForCase(Long caseId);
}
