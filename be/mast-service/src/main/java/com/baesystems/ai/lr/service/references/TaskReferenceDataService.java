package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface TaskReferenceDataService
{
    List<ReferenceDataDto> getPostponementTypes();
}
