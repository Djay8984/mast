package com.baesystems.ai.lr.service.employees;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface LrEmployeeService
{
    LrEmployeeDto getLrEmployee(Long employeeId) throws RecordNotFoundException;

    LrEmployeeDto getLrEmployeeByFullName(String userFullName) throws RecordNotFoundException;

    PageResource<LrEmployeeDto> searchEmployees(Integer page, Integer size, String sort, String order,
            String searchString, Long officeId) throws BadPageRequestException;

    String getEmployeeFullName(Long employeeId);

    List<LrEmployeeDto> getEmployees();
}
