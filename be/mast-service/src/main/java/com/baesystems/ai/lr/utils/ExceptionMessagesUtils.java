package com.baesystems.ai.lr.utils;

public final class ExceptionMessagesUtils
{
    private ExceptionMessagesUtils()
    {
    }

    // Security error messages
    public static final String CREDENTIAL_ERROR = "Error getting credential group attribute: %s";

    // General validation error message
    public static final String VALIDATION_FAILED = "Validation failed with errors: %s.";

    // Rule Engine error messages
    public static final String RULE_ENGINE_ERROR = "An error was thrown by the %s Rule Engine: %s";
    public static final String RULE_ENGINE_INVALID_RESPONSE = "An invalid response was received from the %s Rule Engine.";
    public static final String RULE_ENGINE_COMMUNICATION_FAILURE = "Unable to retrieve a resource from the %s Rules Engine";
    public static final String RULES_TASK_UNKNOWN_TASK_CODE = "Rules engine returned a task code that does not exist in reference data: %s, skipping";
    public static final String RULES_TASK_UNKNOWN_ASSET_ITEM = "Rules engine returned an asset item that does not exist: %d, skipping";

    // Versioning specific messages
    public static final String ASSET_IS_CHECKED_OUT_BY_ANOTHER_USER = "Asset %d is checked out by user %s.";
    public static final String ASSET_CHECKED_OUT_BY_SAME_USER = "Asset %d is already checked out by same user %s.";
    public static final String ASSET_IS_NOT_CHECKED_OUT = "Asset %d has not been checked out.";
    public static final String ITEM_DEFINED_WITH_MULTIPLE_PARENTS = "The following item has been incorrectly defined with multiple parents [Item %d Asset %d Version %d]";

    public static final String CANNOT_BE_NULL_ERROR_MSG = "%s cannot be null.";
    public static final String GENERAL_NOT_FOUND_ERROR_MSG = "Entity combination not found.";
    public static final String NOT_FOUND_ERROR_MSG = "Entity not found for identifier: %d.";
    public static final String NOT_FOUND_ERROR_MSG_LIST = "At least one entity in the list of %s could not be found.";
    public static final String SPECIFIC_NOT_FOUND_ERROR_MSG = "No %s found for id %d.";
    public static final String SUBITEM_NOT_FOUND_ERROR_MSG = "No %s found for %s id %d.";
    public static final String INVALID_LIST_ERROR_MSG = "List of %s has invalid members.";
    public static final String INVALID_LIST_ERROR_MSG_WITH_FAILURES = "List of %s has invalid members %s.";
    public static final String MISMATCH_ERROR_MSG = "%s with id %d is not applicable to %s with id %d.";
    public static final String BINDING_ERROR_MSG = "Parameter with invalid type: %s is not a valid %s";
    public static final String GENERAL_BINDING_ERROR_MSG = "Parameter with invalid type";
    public static final String INTERNAL_SERVER_ERROR_MSG = "Internal Server Error.";
    public static final String DETAILED_BINDING_ERROR_MSG = "Could not bind parameter %s to %s.";
    public static final String BODY_NOT_NULL = "Cannot create or update empty entities.";
    public static final String ID_NOT_NULL = "Field ID is expected to be null in the creation of new entities: %d";
    public static final String DELETED_CANNOT_AMEND = "%s %d is marked as deleted and therefore cannot be amended.";
    public static final String INVALID_ATTRIBUTE_PATH = "Attribute with identifier %d not found for asset %d, item %d.";
    public static final String INVALID_VALUE = "%s is invalid.";
    public static final String INVALID_ATTACHMENT_PATH = "Attachment with identifier %d not found for %s %d.";
    public static final String CANNOT_BE_UPDATED = "Entity with id %d is in a state of %s and cannot be updated.";
    public static final String PARENT_NOT_UPDATEABLE = "Entity with id %d has a parent entity with id %d which must be updated.";
    public static final String INVALID_PAGE_SIZE = "Cannot request a page with a size of 0 or less.";
    public static final String INVALID_FIELD = "Invalid field, %s, in request.";
    public static final String INVALID_PAGE_INDEX = "Page index must not be less than zero!";
    public static final String FIELD_MISMATCH = "Field values do not match or do not match request.";
    public static final String FIELD_CANNOT_BE_UPDATED = "%s cannot be updated.";
    public static final String BODY_ID_MISMATCH = "The %s ID in the request body %s, does not match the resource id %s";
    public static final String STALE_DATA_TRYING_TO_BE_PERSISTED = "This element is being updated from another source. Please try again.";
    public static final String QUERY_PARAM_NOT_NULL = "Query parameter %s cannot be null.";
    public static final String DOES_NOT_MATCH_TEMPLATE = "The %s entered does not match the template it is linked to.";
    public static final String INCORRECT_TYPE = "Entity with id %d is not of type %d.";
    public static final String INCORRECT_STATUS_SPECIFIC = "Cannot be in status with id %d %s.";
    public static final String BEFORE_CURRENT_DATE = "The %s cannot be before today.";
    public static final String INVALID_ITEM_PATH = "Item with identifier %d not found for asset %d";
    public static final String INVALID_RELATIONSHIP_PATH = "Item Relationship with identifier %d, type %d, not found for Item %d";
    public static final String INVALID_PARENT_ITEM = "The parent item with identifier %d cannot be linked to a child of type %d";
    public static final String TOO_MANY_CHILD_ITEMS = "The parent item with identifier %d cannot be linked to more than %d items of type %d";
    public static final String ACTION_CANNOT_BE_TAKEN_FOR_ITEM = "Item with id %d cannot be %s because it, or one of its child items, is linked to one or more %s.";
    public static final String INVALID_RELATIONSHIP = "%s with id %d (%s) and id %d do not have the specified relationship.";
    public static final String ALREADY_EXISTS = "Cannot create the current %s because it conflicts with an existing %s.";
    public static final String CANNOT_CREATE = "Cannot create %s because it is of an invalid type.";
    public static final String INVALID_STATUS_FOR_UPDATE = "%s with ID %d cannot be updated because its status is %s.";
    public static final String CANNOT_DELETE_ROOT_ITEM = "Entity with identifier: %d cannot be deleted because it is a root item.";
    public static final String ACTIONABLE_ITEM_DOES_NOT_EXIST_FOR_ASSET = "Actionable item with id %s does not exist for Asset %s.";
    public static final String WIP_ACTIONABLE_ITEM_DOES_NOT_EXIST_FOR_JOB = "WIP Actionable item with id %s does not exist for Job %s.";
    public static final String PARENT_CODICIL_INVALID = "The original codicil id relating to this new WIP codicil is invalid. ID : %s.";
    public static final String HISTORICAL_IMPOSED_DATE = "Imposed date is in the past: %s.";
    public static final String INCORRECT_STATUS = "Cannot update %s because %s is not in a status that allows updates.";
    public static final String MULTIPLE_UPDATES = "Cannot perform multiple updates on the same entity.";
    public static final String SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG = "%s with id %d is not linked to the %s with id %d";
    public static final String CANNOT_BE_MARKED_COMPLETE = "The %s with id %d cannot be marked as complete while the field %s is %s.";
    public static final String CANNOT_BE_UPDATED_WITHOUT_RELATED = "The %s with id %d cannot be updated without updating its %s.";
    public static final String CANNOT_BE_UPDATED_REF_DATA = "Entity with id %d cannot be updated because the value being updated comes from the reference data.";
    public static final String MANDATORY_TYPE_MISSING = "A mandatory %s is missing. The list of %s must contain at least one of these.";
    public static final String MORE_THAN_ONE_OF_TYPE = "Cannot add more than one %s with the same %s.";
    public static final String INVALID_STATUS_UPDATE = "Cannot move to the proposed status, id %d, from the current status, id %d.";
    public static final String CANNOT_ADD_MORE_THAN_ONCE = "Cannot add the same %s more than once with the same %s.";
    public static final String ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD = "Attempting to change immutable field: %s.";
    public static final String PRODUCT_STILL_HAS_ATTACHED_SERVICES = "One or more products still have attached services. Product ids %s";
    public static final String NO_HARMONISATION_DATE = "No Harmonisation Date passed.";
    public static final String NO_ASSET_BUILD_DATE = "No Build Date on the Asset with name [%s] and ID : [%d]";
    public static final String DATE_TOO_EARLY = "The passed %s Date [%s] must be after the asset build date of [%s].";
    public static final String DUE_DATE_AFTER_LOWER_RANGE_DATE = "The passed Due Date [%s] must not be after the lower range date of [%s].";
    public static final String DATE_TOO_LATE = "The passed %s Date [%s] must be before [%s].";
    public static final String EMPTY_DRAFT_ITEM_LIST = "The passed draft item list for asset id [%s] should not be empty.";
    public static final String ID_LIST_NOT_FOUND = "No %s were found with the following id(s) %s.";
    public static final String DEPENDENCY_ERROR = "Cannot add %s %d without %s of its dependencies %s. ";
    public static final String DEPENDENCY_ERROR_MIXED_TYPE = "Cannot add %s %d without %s %d. ";
    public static final String DEPENDENCY_ERROR_GENERAL = "Cannot add %s %d without %s of its %s %s. ";
    public static final String DUPLICATE_ERROR = "Cannot add more than one %s with the same %s. The following duplicates were found %s.";
    public static final String MUTUALLY_EXCLUSIVE_ERROR = "Cannot add %s %d while %s of the following are present %s. ";
    public static final String FIELD_IS_NOT_EDITABLE = "Cannot update %s on the following %s with %s %s.";
    public static final String CANNOT_ADD_RELATIONSHIP = "Cannot add relationship of type %s.";
    public static final String IVALID_ITEM_RELATIONSHIP = "Cannot add relationship of type %d, between items of types %d (%s) and %d (%s).";
    public static final String MIN_MAX_OCCURS_VIOLATION = "The %s %d (%s) must occur at %s %d time(s). ";
    public static final String CANNOT_CLOSE_JOB_OPEN_FUA = "Cannot close job %d due to incomplete follow up actions.";
    public static final String REPORT_SUBMISSION_BLOCKED = "Cannot submit report of type %d due to the following %s that have %s. %s.";
    public static final String UPDATING_STALE_OBJECT = "The following entity type cannot be updated as there is a newer version someone else has saved. \"%s\"";
    public static final String MISSING_DICTIONARY = "A problem occurred trying to read a dictionary from here : [%s]";
    public static final String CANNOT_SPECIFY_EMPLOYEE_AND_TEAM = "Cannot specify Employee ID (%d) and Team ID (%d) together. One or the other.";
    public static final String CANNOT_FIND_CLASHING_DISPLAY_ORDER_ITEM = "Cannot find Item that clashes with Item ID : %d, Display Order: %d.";
    public static final String TOO_MANY_INVERSE_RELATIONSHIPS = "Too many inverse relationships for item %d. Expected: %d Found %d";
}
