package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface DefectValidationService
{
    void verifyAssetHasDefect(Long assetId, Long defectId) throws RecordNotFoundException;

    void validateDefectCreate(DefectDto defectDto) throws BadRequestException, RecordNotFoundException;

    void validateDefectUpdate(Long defectId, DefectDto defectDto) throws BadRequestException, RecordNotFoundException;

    void verifyJobHasWIPDefect(Long jobId, Long defectId) throws RecordNotFoundException;

    void verifyWIPDefectImmutableFields(DefectDto updatedDefectDto) throws BadRequestException, RecordNotFoundException;
}
