package com.baesystems.ai.lr.service.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsAssetService
{
    IhsAssetDetailsDto getIhsAsset(Long imoNumber) throws RecordNotFoundException;
}
