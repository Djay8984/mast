package com.baesystems.ai.lr.service.ihs;

import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface IhsPrincipalDimensionService
{
    IhsPrincipalDimensionsDto getPrincipalDimensions(Long imoNumber) throws RecordNotFoundException;

}
