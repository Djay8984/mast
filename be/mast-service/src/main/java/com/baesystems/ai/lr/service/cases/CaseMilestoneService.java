package com.baesystems.ai.lr.service.cases;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface CaseMilestoneService
{
    CaseMilestoneListDto getMilestonesForCase(Long caseId) throws BadRequestException;

    void updateMilestonesForCase(Long caseId, CaseMilestoneListDto milestones) throws MastBusinessException;

    void updateMilestoneDates(Long caseId, List<CaseMilestoneDto> caseMilestones, Date caseAcceptenceDate, Date estimatedBuildDate)
            throws MastBusinessException;
}
