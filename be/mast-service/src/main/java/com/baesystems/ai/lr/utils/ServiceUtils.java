package com.baesystems.ai.lr.utils;

import java.util.List;

public interface ServiceUtils
{
    /**
     * This method returns the authenticated user name held in the Spring Security Context.
     *
     * @return the user name as a {@link String}
     */
    String getSecurityContextPrincipal();

    /**
     * Gets a list of the groups assigned to the authenticated user.
     * 
     * @return {@link List} of {@link String} group names
     */
    List<String> getUserGroups();

}
