package com.baesystems.ai.lr.utils;

import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ServiceHelper
{
    Pageable createPageable(Integer page, Integer size, String sort, String order)
            throws BadPageRequestException;

    void verifyModel(Object model, Long id) throws RecordNotFoundException;
}
