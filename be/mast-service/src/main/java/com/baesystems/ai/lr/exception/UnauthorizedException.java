package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends AbstractMastException
{
    private static final long serialVersionUID = 7010875396932529635L;

    public UnauthorizedException()
    {
        super();
    }

    public UnauthorizedException(final String message)
    {
        super(message);
    }

    public UnauthorizedException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public UnauthorizedException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.UNAUTHORIZED.value();
    }
}
