package com.baesystems.ai.lr.service.services;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ServiceService
{
    List<ReferenceDataDto> getSchedulingTypesForServiceCatalogue(Long serviceCatalogueId) throws RecordNotFoundException;

    List<ScheduledServiceDto> getServicesForAsset(Long assetId, List<Long> statusIds, List<Long> productFamilyIds, Date dueDateMin, Date dueDateMax)
            throws RecordNotFoundException;

    ScheduledServiceDto getService(Long serviceId, Long assetId) throws RecordNotFoundException;

    ScheduledServiceDto updateService(Long serviceId, ScheduledServiceDto serviceDto) throws RecordNotFoundException, MastBusinessException;

    ScheduledServiceListDto saveServices(ScheduledServiceListDto newService) throws MastBusinessException, RecordNotFoundException;

    void deleteServices(List<Long> idList);

    DateDto getHarmonisationDate(Long assetId) throws RecordNotFoundException;

    ServiceCatalogueLightDto getServiceCatalogue(Long serviceCatalogueId) throws RecordNotFoundException;

    void updateServiceScheduleFollowingFARorFSR(List<SurveyDto> surveys, Boolean provisionalDates)
            throws MastBusinessException, RecordNotFoundException;

    List<ScheduledServiceDto> updateServiceCertStatusFollowingFAR(Long jobId);

    void runSchedulingRulesOnNewServices(ScheduledServiceListDto newServices) throws MastBusinessException, RecordNotFoundException;

    void runSchedulingRulesAfterUpdatingService(ScheduledServiceDto newService) throws MastBusinessException, RecordNotFoundException;
}
