package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface WorkItemValidationService
{
    void validateWIPWorkItemListForUpdate(String inputJobId, WorkItemLightListDto tasks) throws BadRequestException, RecordNotFoundException;

    void validateWIPWorkItemForUpdate(String inputJobId, WorkItemLightDto task) throws BadRequestException, RecordNotFoundException;

    void validateWIPWorkItemForCreate(String inputJobId, WorkItemLightDto task) throws BadRequestException, RecordNotFoundException;

    void verifyWIPTaskIdExists(String inputTaskId) throws RecordNotFoundException, BadRequestException;

    void verifyTaskIdExists(String inputTaskId) throws RecordNotFoundException, BadRequestException;
}
