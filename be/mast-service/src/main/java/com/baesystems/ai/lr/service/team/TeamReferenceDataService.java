package com.baesystems.ai.lr.service.team;

import com.baesystems.ai.lr.dto.references.TeamDto;
import com.baesystems.ai.lr.dto.references.TeamEmployeeDto;
import com.baesystems.ai.lr.dto.references.TeamOfficeDto;

import java.util.List;

public interface TeamReferenceDataService
{
    List<TeamDto> getTeams();

    List<TeamEmployeeDto> getTeamEmployees();

    List<TeamOfficeDto> getTeamOffices();
}
