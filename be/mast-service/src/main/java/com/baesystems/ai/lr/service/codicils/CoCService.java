package com.baesystems.ai.lr.service.codicils;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface CoCService
{
    CoCDto getCoC(Long cocId, boolean wip) throws RecordNotFoundException;

    PageResource<CoCDto> getCoCsByAsset(Integer page, Integer size, String sort, String order, Long assetId, Long statusId)
            throws BadPageRequestException;

    CoCDto saveCoC(Long assetId, CoCDto cocDto) throws RecordNotFoundException;

    PageResource<CoCDto> getWIPCoCsForJob(Integer page, Integer size, String sort, String order, Long jobId, Long defectId)
            throws BadPageRequestException;

    CoCDto updateCoC(CoCDto coCDto) throws RecordNotFoundException, StaleObjectException;

    CoCDto saveWIPCoC(Long jobId, CoCDto coCDto) throws RecordNotFoundException;

    Boolean deleteCoC(Long coCId) throws RecordNotFoundException;

    Boolean deleteWIPCoC(Long coCId) throws RecordNotFoundException;

    PageResource<CoCDto> getCoCsByQuery(Long assetId, Integer page, Integer size, String sort, String order, CodicilDefectQueryDto queryDto)
            throws BadPageRequestException;

    PageResource<CoCDto> getDefectCocs(Long assetId, Long defectId, Integer page, Integer size, String sort, String order, Long statusId)
            throws BadPageRequestException;

    PageResource<CoCDto> getWIPCoCsByQuery(Long jobId, Integer page, Integer size, String sort, String order, CodicilDefectQueryDto queryDto)
            throws BadPageRequestException;

    CoCDto updateWIPCoC(CoCDto coCDto) throws RecordNotFoundException, StaleObjectException;
}
