package com.baesystems.ai.lr.utils;

import java.util.List;

import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.codicils.CodicilDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;

public interface DueStatusService
{
    <T extends DueStatusUpdateableDto> void calculateDueStatus(T object);

    <T extends DueStatusUpdateableDto> void calculateDueStatuses(List<T> objects);

    void calculateServiceDueStatus(ScheduledServiceDto service);

    void calculateServiceDueStatuses(List<ScheduledServiceDto> services);

    <T extends CodicilDto> void updateCodicilStatus(T codicil);
}
