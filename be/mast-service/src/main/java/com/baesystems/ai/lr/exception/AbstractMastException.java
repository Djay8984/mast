package com.baesystems.ai.lr.exception;

public abstract class AbstractMastException extends Exception implements MastException
{
    private static final long serialVersionUID = 5279446655902678115L;

    public AbstractMastException()
    {
        super();
    }

    public AbstractMastException(final String message)
    {
        super(message);
    }

    public AbstractMastException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public AbstractMastException(final Throwable cause)
    {
        super(cause);
    }
}
