package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

public class MastSystemException extends RuntimeException implements MastException
{
    private static final long serialVersionUID = -4717845427740816751L;

    public MastSystemException()
    {
        super();
    }

    public MastSystemException(final String message)
    {
        super(message);
    }

    public MastSystemException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public MastSystemException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }
}
