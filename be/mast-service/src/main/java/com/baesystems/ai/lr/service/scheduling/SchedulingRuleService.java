package com.baesystems.ai.lr.service.scheduling;

import java.util.Date;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface SchedulingRuleService
{
    void scheduleServicesFollowingServiceCompletion(ScheduledServiceDto completedService, Date harmonisationDate, Boolean provisionalDates)
            throws RecordNotFoundException;

    void scheduleServicesFollowingServiceCreation(ScheduledServiceDto newService, Date harmonisationDate, Boolean provisionalDates)
            throws RecordNotFoundException;

    void scheduleServicesFollowingAssetUpdate(Long assetId, Boolean provisionalDates) throws RecordNotFoundException;
}
