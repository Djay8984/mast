package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.enums.ExceptionType;

public class MastBusinessException extends AbstractMastException
{
    private static final long serialVersionUID = -4717845427740816751L;

    public MastBusinessException()
    {
        super();
    }

    public MastBusinessException(final String message)
    {
        super(message);
    }

    public MastBusinessException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public MastBusinessException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.CONFLICT.value();
    }

    public ExceptionType getType()
    {
        return ExceptionType.NON_SPECIFIC_EXCEPTION;
    }
}
