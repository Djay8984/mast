package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.CaseReferenceDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CaseReferenceDataService
{
    List<ReferenceDataDto> getRiskAssessmentStatuses();

    ReferenceDataDto getRiskAssessmentStatus(Long riskAssessmentStatusId) throws RecordNotFoundException;

    List<ReferenceDataDto> getPreEicInspectionStatuses();

    ReferenceDataDto getPreEicInspectionStatus(Long preEicInspectionStatusId) throws RecordNotFoundException;

    List<ReferenceDataDto> getCaseTypes();

    ReferenceDataDto getCaseType(Long caseTypeId) throws RecordNotFoundException;

    List<CaseStatusDto> getCaseStatuses();

    CaseStatusDto getCaseStatus(Long caseStatusId) throws RecordNotFoundException;

    CaseReferenceDto getCaseReferenceData();

    ReferenceDataDto getOfficeRole(Long id) throws RecordNotFoundException;

    List<ReferenceDataDto> getOfficeRoles();

    List<MilestoneDto> getMilestones();

    List<Long> getValidNewCaseStatuses(Long initialStatusId);

    List<ReferenceDataDto> getMilestoneDueDateReferences();

    List<ReferenceDataDto> getMilestoneStatuses();
}
