package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

public class RecordNotFoundException extends AbstractMastException
{
    private static final long serialVersionUID = -4214715834480680609L;

    public RecordNotFoundException()
    {
        super();
    }

    public RecordNotFoundException(final String message)
    {
        super(message);
    }

    public RecordNotFoundException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public RecordNotFoundException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.NOT_FOUND.value();
    }
}
