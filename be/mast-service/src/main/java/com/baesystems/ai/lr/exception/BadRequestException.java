package com.baesystems.ai.lr.exception;

import java.util.Arrays;

import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public class BadRequestException extends AbstractMastException
{
    private static final long serialVersionUID = -4214715834480680609L;

    private String[] errors;

    public BadRequestException()
    {
        super();
    }

    public BadRequestException(final String message)
    {
        super(message);
    }

    public BadRequestException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public BadRequestException(final Throwable cause)
    {
        super(cause);
    }

    public BadRequestException(final String... errors)
    {
        super(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, Arrays.toString(errors)));
        this.errors = errors;
    }

    public String[] getErrors()
    {
        return errors.clone();
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.BAD_REQUEST.value();
    }
}
