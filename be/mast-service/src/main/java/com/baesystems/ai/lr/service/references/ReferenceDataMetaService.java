package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;

public interface ReferenceDataMetaService
{
    ReferenceDataVersionDto getTopLevelReferenceDataVersion();

    List<ReferenceDataDto> getDueStatuses();
}
