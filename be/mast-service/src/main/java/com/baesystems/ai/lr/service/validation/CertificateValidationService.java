package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CertificateValidationService
{
    void validateCreateCertificate(CertificateDto certificate, String intputJobId)
            throws BadRequestException, RecordNotFoundException;

    void validateUpdateCertificate(CertificateDto certificate, String intputJobId, String inputCertificateId)
            throws BadRequestException, RecordNotFoundException;
}
