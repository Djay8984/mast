package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.CodicilStatusDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.CodicilTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CodicilReferenceDataService
{
    List<CodicilTemplateDto> getCodicilTemplates();

    List<ReferenceDataDto> getCodicilCategories();

    List<ReferenceDataDto> getTemplateStatuses();

    List<CodicilTypeDto> getCodicilTypes();

    CodicilTypeDto getCodicilType(Long codicilTypeId) throws RecordNotFoundException;

    List<CodicilStatusDto> getCodicilStatuses();

    ReferenceDataDto getCodicilStatus(Long codicilStatusId) throws RecordNotFoundException;
}
