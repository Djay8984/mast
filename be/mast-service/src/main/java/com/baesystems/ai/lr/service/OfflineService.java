package com.baesystems.ai.lr.service;

import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.jobs.JobBundleDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface OfflineService
{
    JobBundleDto getJobBundle(Long jobId) throws RecordNotFoundException, BadPageRequestException;

    JobBundleDto updateAnyRelatedWipEntitiesForNewWipDefect(JobBundleDto jobBundleDto, Long temporalDefectId, DefectDto persistedDefect);

    JobBundleDto updateAnyRelatedWipEntitiesForNewWipCoC(JobBundleDto jobBundleDto, Long temporalCoCId, Long actualId);

    JobBundleDto updateAnyRelatedWipEntitiesForNewSurvey(JobBundleDto jobBundleDto, Long temporalSurveyId, Long actualId);
}
