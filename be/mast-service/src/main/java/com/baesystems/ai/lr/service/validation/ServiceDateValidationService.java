package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface ServiceDateValidationService
{
    void validateAssignedDate(ScheduledServiceDto serviceDto) throws BadRequestException, MastBusinessException;

    void validateDateRanges(ScheduledServiceDto serviceDto) throws BadRequestException;
}
