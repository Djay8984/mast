package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ReportValidationService
{
    void validateReportUpdate(String reportId, String jobId, ReportDto reportDto)
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException;

    void validateReportSave(String jobId, ReportDto reportDto)
            throws BadRequestException, RecordNotFoundException, MastBusinessException, BadPageRequestException;

    void validateJobHasReport(String inputJobId, String inputReportId)
            throws RecordNotFoundException, BadRequestException;
}
