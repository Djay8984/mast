package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ProductTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SchedulingTypeDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import com.baesystems.ai.lr.dto.references.ServiceGroupDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ServiceReferenceDataService
{
    List<ReferenceDataDto> getServiceStatuses();

    ReferenceDataDto getServiceStatus(Long serviceStatusId) throws RecordNotFoundException;

    List<ReferenceDataDto> getServiceTypes();

    List<ProductTypeDto> getProductTypes();

    List<ProductGroupDto> getProductGroups();

    List<ServiceCatalogueDto> getServiceCatalogues();

    List<ReferenceDataDto> getSchedulingRegimes();

    List<SchedulingTypeDto> getSchedulingTypes();

    List<ServiceGroupDto> getServiceGroups();

    List<ReferenceDataDto> getServiceRulesets();

    List<ReferenceDataDto> getAssignedSchedulingTypes();

    List<ReferenceDataDto> getAssociatedSchedulingTypes();

    List<ReferenceDataDto> getHarmonisationTypes();

    List<ReferenceDataDto> getSchedulingDueTypes();

    List<ServiceCreditStatusDto> getServiceCreditStatuses();
}
