package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AssetNoteValidationService
{
    void validateAsset(String assetNoteId, String inputAssetId, String headerAssetId) throws BadRequestException, RecordNotFoundException;

    void validateAssetAndItem(String assetId, String itemId, AssetNoteDto assetNoteDto) throws BadRequestException;

    void verifyAssetHasAssetNote(Long assetId, Long assetNoteId) throws RecordNotFoundException;

    void validateJob(String assetNoteId, String inputJobId, String headerJobId) throws BadRequestException, RecordNotFoundException;

    void verifyImposedDateNotHistorical(AssetNoteDto assetNoteDto) throws BadRequestException;

    void verifyJobHasWIPAssetNote(Long jobId, Long assetNoteId) throws RecordNotFoundException;

    void verifyOptionalParentIdExistsAndIsOfCorrectType(Long parentCodicilId) throws RecordNotFoundException;

    void validateWIPJobScopeConfirmed(AssetNoteDto assetNoteDto) throws BadRequestException;
}
