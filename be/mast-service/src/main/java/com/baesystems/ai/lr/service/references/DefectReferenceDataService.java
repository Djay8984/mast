package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectDetailDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface DefectReferenceDataService
{
    List<DefectCategoryDto> getDefectCategories();

    DefectCategoryDto getDefectCategory(Long defectCategoryId) throws RecordNotFoundException;

    String getDefectCategoryLetter(DefectCategoryDto defectCategoryDto) throws RecordNotFoundException;

    List<DefectDetailDto> getDefectDetails();

    List<ReferenceDataDto> getDefectStatuses();

    ReferenceDataDto getDefectStatus(Long defectStatusId) throws RecordNotFoundException;
}
