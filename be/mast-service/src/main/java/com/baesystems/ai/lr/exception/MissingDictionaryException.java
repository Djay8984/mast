package com.baesystems.ai.lr.exception;

import java.io.IOException;

import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public class MissingDictionaryException extends MastBusinessException
{
    private static final long serialVersionUID = 3439956616941141845L;

    public MissingDictionaryException(final String dictionaryPath, final IOException ioException)
    {
        super(String.format(ExceptionMessagesUtils.MISSING_DICTIONARY, dictionaryPath), ioException);
    }

    @Override
    public ExceptionType getType()
    {
        return ExceptionType.MISSING_DICTIONARY;
    }
}
