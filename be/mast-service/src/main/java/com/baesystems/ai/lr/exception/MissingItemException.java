package com.baesystems.ai.lr.exception;

import com.baesystems.ai.lr.enums.ExceptionType;

public class MissingItemException extends MastBusinessException
{
    private static final long serialVersionUID = 5372432908674318432L;

    public MissingItemException(final String message)
    {
        super(message);
    }

    @Override
    public ExceptionType getType()
    {
        return ExceptionType.MISSING_ITEM;
    }
}
