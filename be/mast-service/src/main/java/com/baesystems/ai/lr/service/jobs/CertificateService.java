package com.baesystems.ai.lr.service.jobs;

import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CertificateService
{
    CertificateDto createCertificate(CertificateDto certificateDto, Long jobId) throws MastBusinessException;

    CertificateDto updateCertificate(CertificateDto certificateDto, Long jobId, Long certificateId) throws MastBusinessException;

    PageResource<CertificateDto> getCertificatesForJob(Integer page, Integer size, String sort, String order, Long jobId)
            throws MastBusinessException, BadPageRequestException;

    void updateCertificates(Long jobId, boolean approved) throws RecordNotFoundException;
}
