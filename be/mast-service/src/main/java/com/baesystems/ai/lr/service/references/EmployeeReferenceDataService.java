package com.baesystems.ai.lr.service.references;

import java.util.List;

import com.baesystems.ai.lr.dto.references.EmployeeOfficeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

public interface EmployeeReferenceDataService
{
    List<ReferenceDataDto> getEmployeeRoles();

    ReferenceDataDto getEmployeeRole(Long roleId);

    List<EmployeeOfficeDto> getEmployeeOffices();
}
