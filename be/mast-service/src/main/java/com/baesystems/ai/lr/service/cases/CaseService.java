package com.baesystems.ai.lr.service.cases;

import java.util.List;

import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CaseService
{
    CaseWithAssetDetailsDto getCase(Long caseId) throws RecordNotFoundException;

    CaseWithAssetDetailsDto updateCase(Long id, CaseWithAssetDetailsDto caseDto)
            throws RecordNotFoundException, MastBusinessException, BadRequestException;

    void deleteCase(Long caseId);

    PageResource<CaseDto> getCases(Integer page, Integer size, String sort, String order, CaseQueryDto query)
            throws BadPageRequestException;

    Boolean isAssetDeletable(Long assetId);

    Boolean getCanDeleteCase(Long caseId);

    CaseWithAssetDetailsDto saveCase(CaseWithAssetDetailsDto caseDto) throws RecordNotFoundException, MastBusinessException, BadRequestException;

    CaseValidationDto verifyCase(Long imoNumber, String builder, String yardNumber, Long businessProcess);

    List<CaseDto> getCasesForAsset(Long assetId);

    PageResource<CaseDto> getCasesAbstract(Integer page, Integer size, String sort, String order, AbstractQueryDto query)
            throws BadPageRequestException;
}
