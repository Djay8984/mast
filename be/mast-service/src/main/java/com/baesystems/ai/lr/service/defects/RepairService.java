package com.baesystems.ai.lr.service.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface RepairService
{
    RepairDto getRepair(Long repairId) throws RecordNotFoundException, BadRequestException;

    PageResource<RepairDto> getDefectRepairs(Long defectId, Integer page, Integer size, String sort, String order)
            throws BadPageRequestException, BadRequestException;

    RepairDto createRepair(Long defectId, RepairDto repairDto) throws BadRequestException;

    RepairDto createWIPRepair(Long defectId, RepairDto repairDto) throws BadRequestException;

    RepairDto updateRepair(RepairDto repairDto) throws BadRequestException, StaleObjectException;

    RepairDto updateWIPRepair(RepairDto repairDto) throws BadRequestException, StaleObjectException;

    Boolean getAreAllItemsPermanentlyRepaired(Long defectId);

    Boolean getAreAllItemsPermanentlyWIPRepaired(Long wipDefectId);

    Boolean getDefectHasPermanentRepair(Long defectId);

    Boolean getWIPDefectHasPermanentRepair(Long wipDefectId);

    PageResource<RepairDto> getDefectRepairsForCoC(Long cocId, Integer page, Integer size, String sort, String order)
            throws BadPageRequestException, BadRequestException;

    PageResource<RepairDto> getDefectRepairsForWIPCoC(Long cocId, Integer page, Integer size, String sort, String order)
            throws BadPageRequestException, BadRequestException;

    RepairDto createWIPCocRepair(Long jobId, RepairDto repairDto) throws BadRequestException, RecordNotFoundException;

    PageResource<RepairDto> getWIPRepairsForDefect(Integer page, Integer size, String sort, String order, Long defectId)
            throws BadPageRequestException;

    List<RepairDto> getWIPRepairsForWIPDefectsAndWipCoCs(List<Long> wipDefectIds, List<Long> wipCoCIds);

    RepairDto getWIPRepair(Long repairId) throws RecordNotFoundException, BadRequestException;

    List<RepairDto> getRepairsForAsset(Long assetId);

    Boolean getRepairIsPermanentAndConfirmed(final Long repairId) throws RecordNotFoundException;

    Boolean getWIPCocHasPermanentRepair(Long wipCocId);
}
