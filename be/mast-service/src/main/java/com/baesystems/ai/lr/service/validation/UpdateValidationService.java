package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.exception.BadRequestException;

public interface UpdateValidationService
{
    void verifyIds(String bodyId, String headerId) throws BadRequestException;
}
