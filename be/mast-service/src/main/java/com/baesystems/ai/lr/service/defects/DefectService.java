package com.baesystems.ai.lr.service.defects;

import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface DefectService
{
    DefectDto getDefect(Long defectId) throws RecordNotFoundException;

    DefectDto getWIPDefect(Long defectId) throws RecordNotFoundException;

    DefectLightDto getDefectLight(Long defectId) throws RecordNotFoundException;

    DefectDto createDefect(DefectDto defectDto) throws RecordNotFoundException;

    DefectDto createWIPDefect(DefectDto defectDto) throws RecordNotFoundException;

    DefectDto updateDefect(Long defectId, DefectDto defectDto) throws RecordNotFoundException;

    DefectDto updateDefect(DefectDto defectDto, boolean wip) throws RecordNotFoundException, StaleObjectException;

    Boolean closeDefect(Long defectId) throws BadRequestException;

    Boolean closeWIPDefect(Long defectId) throws BadRequestException;

    Boolean deleteWIPDefect(Long defectId) throws RecordNotFoundException, MastBusinessException;

    PageResource<DefectDto> getDefectsForAsset(Integer page, Integer size, String sort, String order, Long assetId, Long statusId)
            throws BadPageRequestException;

    PageResource<DefectDto> getWIPDefectsForJob(Long jobId, Integer page, Integer size, String sort, String order)
            throws BadPageRequestException;

    PageResource<DefectDto> getDefectsByQuery(Long assetId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;

    PageResource<DefectDto> getWIPDefectsByQuery(Long jobId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;
}
