package com.baesystems.ai.lr.service.security;

import com.baesystems.ai.lr.dto.security.UserDto;

public interface SecurityService
{
    UserDto getUserInfo();
}
