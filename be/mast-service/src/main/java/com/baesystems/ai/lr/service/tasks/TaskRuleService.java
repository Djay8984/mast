package com.baesystems.ai.lr.service.tasks;

public interface TaskRuleService
{
    void syncTasksForAsset(Long assetId);

    void syncTasksForScheduledService(Long assetId, Long scheduledServiceId);
}
