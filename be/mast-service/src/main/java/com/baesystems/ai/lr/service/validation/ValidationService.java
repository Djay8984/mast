package com.baesystems.ai.lr.service.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.base.IdDto;
import com.baesystems.ai.lr.exception.BadRequestException;

public interface ValidationService
{
    void verifyId(Object string) throws BadRequestException;

    void verifyIdNull(Long caseDto) throws BadRequestException;

    void verifyBody(Object entityDto) throws BadRequestException;

    Long verifyNumberFormat(String value) throws BadRequestException;

    void validate(Object dto) throws BadRequestException;

    void addIdToIdList(IdDto value, List<Long> idList) throws BadRequestException;

    void verifyField(Object field, Object headerfield, String message) throws BadRequestException;

    void verifyNullableId(Object inputId) throws BadRequestException;

    void verifyQueryParamNotNull(Object entity, String queryParameterName) throws BadRequestException;

}
