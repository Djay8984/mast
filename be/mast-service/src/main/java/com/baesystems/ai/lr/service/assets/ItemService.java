package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ItemService
{
    LazyItemDto getItem(Long itemId, Long assetId, Long assetVersion, Boolean allowDraft) throws RecordNotFoundException;

    LazyItemDto getRootItem(Long assetId, Long versionId, Boolean allowDraft) throws RecordNotFoundException;

    PageResource<LazyItemDto> getItems(Integer page, Integer size, String sort, String order, Long assetId, Long versionId, Boolean allowDraft,
            ItemQueryDto itemQueryDto) throws RecordNotFoundException, BadPageRequestException;

    void updateItemName(AttributeDto attribute, Long itemId);

    String updateItemName(ItemWithAttributesDto itemWithAttributes);

    void deleteItem(Long itemId, Long assetId);

    void setItemReviewed(Long itemId);

    void setItemDecommissionState(Long assetId, Long itemId, Boolean flagState);

    void deleteItemRelationship(Long relationshipId);

    ItemRelationshipDto createItemRelationship(Long assetId, Long itemId, ItemRelationshipDto itemRelationshipDto);

}
