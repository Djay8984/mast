package com.baesystems.ai.lr.service.flagports;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface PortService
{
    PortOfRegistryDto getPortOfRegistry(Long portOfRegistryId) throws RecordNotFoundException;

    PageResource<PortOfRegistryDto> getPorts(Integer page, Integer size, String sort, String order, Long flagState)
            throws BadRequestException, BadPageRequestException;

    List<PortOfRegistryDto> getPortList();
}
