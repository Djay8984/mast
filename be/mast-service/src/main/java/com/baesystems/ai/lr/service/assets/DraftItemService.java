package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface DraftItemService
{
    void deleteDraftItem(Long draftItemId);

    void updateDisplayOrders(DraftItemDto newItem) throws MastBusinessException;

    Long appendDraftItems(Long assetId, DraftItemListDto draftItemListDto) throws MastBusinessException, BadRequestException;

}
