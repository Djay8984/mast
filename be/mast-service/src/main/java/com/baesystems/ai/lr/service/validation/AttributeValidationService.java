package com.baesystems.ai.lr.service.validation;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AttributeValidationService
{
    void verifyAssetAndItemHasAttribute(Long attributeId, Long itemId, Long assetId) throws RecordNotFoundException;

    void validateAttributes(List<AttributeDto> attributes, Long itemId, Long assetId) throws RecordNotFoundException, BadRequestException;

    void validateAttributeType(AttributeDto attributeDto) throws BadRequestException;

    void verifyAttribute(AttributeDto attributeDto) throws BadRequestException;
}
