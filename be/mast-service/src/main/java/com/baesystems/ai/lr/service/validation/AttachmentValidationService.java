package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AttachmentValidationService
{
    void verifyHasAttachment(Long entityId, String attachmentTargetTypeString, Long attachmentId)
            throws RecordNotFoundException, MastBusinessException;
}
