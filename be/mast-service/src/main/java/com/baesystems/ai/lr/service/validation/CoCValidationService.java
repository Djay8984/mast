package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CoCValidationService
{
    void validateDefectIdMatches(String defectIdFromUri, CoCDto coc) throws BadRequestException;

    void verifyAssetHasCoC(Long assetId, Long cocId) throws RecordNotFoundException;

    void verifyCoCImmutableFields(CoCDto updatedCocDto) throws BadRequestException, RecordNotFoundException;

    void verifyEditedImposedDueDate(CoCDto editedCocDto) throws BadRequestException;

    void verifyImposedDueDate(CoCDto cocDto) throws BadRequestException;

    void verifyJobHasWIPCoC(Long jobId, Long cocId) throws RecordNotFoundException;

    void verifyOptionalParentIdExistsAndIsOfCorrectType(Long parentCocId) throws RecordNotFoundException;

    void verifyTemplatedFieldsCorrect(CoCDto cocDto) throws BadRequestException, RecordNotFoundException, MastBusinessException;

    void verifyWIPCoCImmutableFields(CoCDto updatedCocDto) throws BadRequestException, RecordNotFoundException;

    void verifyWIPDefectHasWIPCoC(Long defectId, Long cocId) throws RecordNotFoundException;

    void validateWIPJobScopeConfirmed(CoCDto cocDto) throws BadRequestException;
}
