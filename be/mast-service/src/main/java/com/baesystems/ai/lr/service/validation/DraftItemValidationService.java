package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface DraftItemValidationService
{
    void validateAppendDraftItems(String inputAssetId, DraftItemListDto draftItemList)
            throws BadRequestException, RecordNotFoundException, MastBusinessException;

}
