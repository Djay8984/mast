package com.baesystems.ai.lr.exception;

import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public class StaleObjectException extends MastBusinessException
{
    private static final long serialVersionUID = 6437596214500935887L;

    public StaleObjectException(final String entityName)
    {
        super(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, entityName));
    }

    public StaleObjectException(final String entityName, final Throwable cause)
    {
        super(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, entityName), cause);
    }

    @Override
    public ExceptionType getType()
    {
        return ExceptionType.STALE_ENTITY_UPDATED;
    }
}
