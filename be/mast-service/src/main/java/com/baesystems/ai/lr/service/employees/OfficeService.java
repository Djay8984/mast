package com.baesystems.ai.lr.service.employees;

import java.util.List;

import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface OfficeService
{
    List<OfficeDto> getOffices();

    OfficeDto getOffice(Long officeId) throws RecordNotFoundException;

    OfficeDto getOfficeByCode(String code) throws RecordNotFoundException;

    String getOfficeName(Long officeId);
}
