package com.baesystems.ai.lr.service.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface AssetVersionUpdateService
{
    void checkOutAsset(Long assetId, Long jobVersionId) throws BadRequestException;

    Long checkInAsset(Long assetId, Long jobVersionId) throws BadRequestException;

    AssetLightDto updateAsset(AssetLightDto assetLightDto) throws RecordNotFoundException, BadRequestException, StaleObjectException;

    void createJobVersionedAsset(JobDto jobDto) throws RecordNotFoundException;

    boolean hasAVersionableChangeOccurred(Long assetId, Long versionId);

    List<AssetMetaDto> getCheckedOutAssets(Boolean all);

    Long upVersionSingleItem(Long assetId, Long itemId, Boolean linkFromDraftVersion);

    void hardDeleteDraftAsset(Long assetId, Long versionId);
}
