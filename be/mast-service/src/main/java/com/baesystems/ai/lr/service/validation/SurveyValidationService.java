package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface SurveyValidationService
{
    void validateSurveyCreate(Long jobId, SurveyDto surveyDto) throws BadRequestException, RecordNotFoundException;

    void validateSurveyListForUpdate(Long jobId, SurveyListDto surveys) throws BadRequestException, RecordNotFoundException;

    void validateSurveyUpdate(Long jobId, Long surveyId, SurveyDto surveyDto) throws BadRequestException, RecordNotFoundException;

    void verifySurveyIdExists(Long surveyId) throws BadRequestException, RecordNotFoundException;

    void verifyJobHasSurvey(Long jobId, Long surveyId) throws RecordNotFoundException;

    void validateCompletion(SurveyDto survey) throws BadRequestException;
}
