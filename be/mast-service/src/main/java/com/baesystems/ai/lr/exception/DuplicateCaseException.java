package com.baesystems.ai.lr.exception;

import java.util.List;

import com.baesystems.ai.lr.enums.ExceptionType;

public class DuplicateCaseException extends MastBusinessException
{
    private static final long serialVersionUID = -3160430333346258833L;

    private static final String DUPLICATE_MESSAGE = "Duplicate Cases with the following IDs were found: ";

    private final List<Long> duplicateCaseIds;

    public DuplicateCaseException(final List<Long> duplicateCaseIds)
    {
        super(DUPLICATE_MESSAGE + duplicateCaseIds);

        this.duplicateCaseIds = duplicateCaseIds;
    }

    public List<Long> getDuplicateCaseIds()
    {
        return duplicateCaseIds;
    }

    @Override
    public ExceptionType getType()
    {
        return ExceptionType.DUPLICATE_CASE;
    }
}
