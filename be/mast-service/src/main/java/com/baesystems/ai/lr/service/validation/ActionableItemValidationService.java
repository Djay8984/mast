package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ActionableItemValidationService
{
    void verifyActionableItemPost(ActionableItemDto actionableItemDto, Boolean allowNullDueDate)
            throws MastBusinessException, BadRequestException, RecordNotFoundException;

    void verifyActionableItemPost(String assetId, ActionableItemDto actionableItem, Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void verifyActionableItemPost(String assetId, String itemId, ActionableItemDto actionableItem, Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void verifyActionableItemPut(String assetId, String actionableItemId, ActionableItemDto actionableItem, Boolean allowNullDueDate)
            throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void verifyAssetHasActionableItem(Long assetId, Long actionableItemId)
            throws RecordNotFoundException;
}
