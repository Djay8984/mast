package com.baesystems.ai.lr.service.customers;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface CustomerService
{
    PartyDto getCustomer(Long customerId) throws RecordNotFoundException;

    List<PartyLightDto> getCustomers();

    void saveCustomer(PartyDto customer);

    PageResource<CustomerLinkDto> getAssetCustomers(Integer page, Integer size, String sort, String order, Long assetId)
            throws BadPageRequestException;
}
