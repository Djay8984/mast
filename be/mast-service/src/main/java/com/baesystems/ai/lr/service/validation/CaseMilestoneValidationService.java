package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface CaseMilestoneValidationService
{
    void validateCaseMilestoneListUpdate(Long caseId, CaseMilestoneListDto caseMilestoneList) throws BadRequestException, MastBusinessException;
}
