package com.baesystems.ai.lr.service.codicils;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface ActionableItemService
{
    ActionableItemDto getActionableItem(Long id) throws RecordNotFoundException;

    PageResource<ActionableItemDto> getActionableItemsForAssets(Integer page, Integer size, String sort, String order, Long assetId, Long statusId)
            throws BadPageRequestException;

    ActionableItemDto createActionableItem(Long assetId, ActionableItemDto actionableItem);

    ActionableItemDto createActionableItem(Long assetId, Long itemId, ActionableItemDto actionableItem);

    TemplateAssetCountDto countAssetsWithActionableItemsForTemplate(List<Long> templateIds);

    ActionableItemDto updateActionableItem(Long assetId, ActionableItemDto actionableItem) throws StaleObjectException;

    Boolean deleteActionableItem(Long actionableItemId) throws RecordNotFoundException;

    PageResource<ActionableItemDto> getActionableItemsByQuery(Long assetId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;

    ActionableItemDto createWIPActionableItem(ActionableItemDto actionableItem);

    ActionableItemDto updateWIPActionableItem(ActionableItemDto actionableItem) throws StaleObjectException;

    Boolean deleteWIPActionableItem(Long actionableItemId);

    PageResource<ActionableItemDto> getWIPActionableItemsByQuery(Long jobId, Integer page, Integer size, String sort, String order,
            CodicilDefectQueryDto queryDto)
                    throws BadPageRequestException;

    ActionableItemDto getWIPActionableItem(Long id) throws RecordNotFoundException;

    PageResource<ActionableItemDto> getWIPActionableItemsForJobId(Integer page, Integer size, String sort,
            String order, Long jobId, Long statusId) throws BadPageRequestException;

    PageResource<ActionableItemDto> getWIPActionableItemsForJobWIPDefect(Integer page, Integer size, String sort, String order, Long jobId,
            Long defectId)
                    throws BadPageRequestException;

    List<ActionableItemDto> getActionableItemsForAService(Long serviceId) throws BadPageRequestException;
}
