package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

public class BadPageRequestException extends AbstractMastException
{
    private static final long serialVersionUID = 3826269713968077556L;

    public BadPageRequestException()
    {
        super();
    }

    public BadPageRequestException(final String message)
    {
        super(message);
    }

    public BadPageRequestException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public BadPageRequestException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.UNPROCESSABLE_ENTITY.value();
    }
}
