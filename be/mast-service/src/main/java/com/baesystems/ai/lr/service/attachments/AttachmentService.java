package com.baesystems.ai.lr.service.attachments;

import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AttachmentService
{
    SupplementaryInformationDto getAttachment(Long supplementaryInformationId) throws RecordNotFoundException;

    Boolean deleteAttachment(Long supplementaryInformationId);

    PageResource<SupplementaryInformationDto> getAttachments(Integer page, Integer size, String sort, String order,
            String target, Long targetId) throws BadPageRequestException, MastBusinessException;

    SupplementaryInformationDto saveAttachment(Long entityId, String attachmentTargetTypeString,
            SupplementaryInformationDto supplementaryInformationDto) throws RecordNotFoundException, MastBusinessException;

    SupplementaryInformationDto updateAttachment(SupplementaryInformationDto attachmentDto) throws RecordNotFoundException;

    List<SupplementaryInformationDto> getAttachmentsByQuery(AttachmentLinkQueryDto attachmentLinkQueryDto);

    void migrateAttachmentsFromWipEntities(Map<Long, Long> wipToNonWipIdMap, AttachmentTargetType typeFrom, AttachmentTargetType typeTo)
            throws BadPageRequestException;
}
