package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface AssetValidationService
{
    void verifyAssetDoesNotExist(Long imoNumber, String builder, String yardNumber) throws MastBusinessException;

    void verifyAssetIdExists(String inputAssetId) throws RecordNotFoundException, BadRequestException;

    void validateAssetUpdate(AssetLightDto asset) throws BadRequestException, RecordNotFoundException, MastBusinessException;

    void validateCreateAsset(AssetDto asset) throws RecordNotFoundException, MastBusinessException, BadRequestException;

    void verifyImoNumber(String imoNumberValue) throws BadRequestException;

    void verifyImoNumberExists(Long imoNumber) throws RecordNotFoundException;
}
