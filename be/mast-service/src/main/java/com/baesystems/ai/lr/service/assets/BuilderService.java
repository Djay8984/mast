package com.baesystems.ai.lr.service.assets;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.BuilderDto;

public interface BuilderService
{
    List<BuilderDto> getBuilders();
}
