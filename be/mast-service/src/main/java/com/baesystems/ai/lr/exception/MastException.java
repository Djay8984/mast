package com.baesystems.ai.lr.exception;

public interface MastException
{

    /**
     * Gets the expected http code response.
     *
     * @return http status code
     */
    int getErrorCode();
}
