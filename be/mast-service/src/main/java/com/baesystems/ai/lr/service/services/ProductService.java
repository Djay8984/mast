package com.baesystems.ai.lr.service.services;

import java.util.List;

import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface ProductService
{
    List<ProductCatalogueDto> getProductCatalogues();

    List<ProductDto> getProducts(Long assetId) throws RecordNotFoundException;

    List<ProductDto> updateProducts(Long assetId, ProductListDto selectedProductList) throws RecordNotFoundException,
            StaleObjectException;

    List<ProductTypeExtendedDto> getProductModel();
}
