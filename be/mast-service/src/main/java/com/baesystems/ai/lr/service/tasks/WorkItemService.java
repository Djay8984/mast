package com.baesystems.ai.lr.service.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;

public interface WorkItemService
{
    PageResource<WorkItemLightDto> getTasks(Integer page, Integer size, String sort, String order, WorkItemQueryDto query)
            throws BadPageRequestException;

    PageResource<WorkItemLightDto> getWIPTasks(Integer page, Integer size, String sort, String order, WorkItemQueryDto query)
            throws BadPageRequestException;

    List<WorkItemLightDto> getTasksForAsset(Long assetId);

    List<WorkItemLightDto> getWIPTasksForJob(Long jobId);

    WorkItemLightListDto updateWIPTasks(WorkItemLightListDto tasks) throws RecordNotFoundException, StaleObjectException;

    WorkItemLightDto updateWIPTask(WorkItemLightDto task) throws RecordNotFoundException, StaleObjectException;

    WorkItemLightDto createWIPTask(WorkItemLightDto task) throws RecordNotFoundException;

    void generateWIPTasks(SurveyDto survey) throws RecordNotFoundException, BadPageRequestException;

    void updateTasksFollowingFARorFSR(Long assetId);
}
