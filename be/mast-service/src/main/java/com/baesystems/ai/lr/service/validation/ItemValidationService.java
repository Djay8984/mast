package com.baesystems.ai.lr.service.validation;

import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ItemValidationService
{
    void validateItemReview(Long itemId) throws MastBusinessException;

    void validateDecommisionItem(Long itemId) throws MastBusinessException;

    void verifyAssetHasItem(String inputAssetId, String inputItemId) throws RecordNotFoundException, BadRequestException;

    void verifyDeleteItem(String inputAssetId, String inputDraftItemId) throws RecordNotFoundException, BadRequestException, MastBusinessException;

    void verifyItemHasItemRelationship(Long itemId, Long relationshipId, RelationshipType relationshipType) throws RecordNotFoundException;

    void verifyItemIsCorrectType(Long itemId, Long typeId) throws MastBusinessException;

    void verifyItemTypeIdExists(Long itemTypeId) throws RecordNotFoundException;
}
