package com.baesystems.ai.lr.service.employees;

import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface SurveyorService
{
    SurveyorDto getSurveyor(Long surveyorId) throws RecordNotFoundException;

    PageResource<SurveyorDto> getSurveyors(Integer page, Integer size, String sort, String order, String search, Long officeId)
            throws BadPageRequestException;

}
