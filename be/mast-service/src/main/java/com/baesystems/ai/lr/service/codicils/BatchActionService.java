package com.baesystems.ai.lr.service.codicils;

import com.baesystems.ai.lr.dto.codicils.BatchActionActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.BatchActionAssetNoteDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface BatchActionService
{
    BatchActionAssetNoteDto saveBatchActionAssetNote(BatchActionAssetNoteDto batchActionAssetNoteDto) throws RecordNotFoundException;

    BatchActionActionableItemDto saveBatchActionActionableItem(BatchActionActionableItemDto batchActionActionableItemDto);
}
