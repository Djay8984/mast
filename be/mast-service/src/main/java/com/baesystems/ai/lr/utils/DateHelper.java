package com.baesystems.ai.lr.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.exception.MastBusinessException;

public interface DateHelper
{
    Date adjustDate(Date startDate, Integer increment, int incrementType) throws MastBusinessException;

    DateDto createDateDto(Date date);

    Boolean isTodayOrAfter(Date date);

    Boolean isTodayOrBefore(Date date);

    Boolean isTodayOrAfterStrict(Date date);

    Boolean isTodayOrBeforeStrict(Date date);

    Boolean isTodayOrAfterStrict(Date date, Date today);

    Boolean isTodayOrBeforeStrict(Date date, Date today);

    static SimpleDateFormat mastFormatter()
    {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    }
}
