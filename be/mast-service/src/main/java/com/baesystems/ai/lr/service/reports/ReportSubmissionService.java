package com.baesystems.ai.lr.service.reports;

import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface ReportSubmissionService
{
    void updateDefectsFollowingReport(Long jobId) throws RecordNotFoundException, BadPageRequestException;

    void updateSurveysFollowingReport(Long jobId, Boolean provisionalDates) throws RecordNotFoundException, BadPageRequestException;

    void updateTasksFollowingReport(Long jobId) throws BadPageRequestException;

    void updateCoCsFollowingReport(Long jobId) throws RecordNotFoundException, BadPageRequestException;

    void updateAssetNotesFollowingReport(Long jobId) throws RecordNotFoundException, BadPageRequestException;

    void updateActionableItemsFollowingReport(Long jobId) throws RecordNotFoundException, BadPageRequestException;
}
