package com.baesystems.ai.lr.exception;

import org.springframework.http.HttpStatus;

public class RuleEngineException extends RuntimeException implements MastException
{
    private static final long serialVersionUID = 7289443131553611831L;

    public RuleEngineException()
    {
        super();
    }

    public RuleEngineException(final String message)
    {
        super(message);
    }

    public RuleEngineException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public RuleEngineException(final Throwable cause)
    {
        super(cause);
    }

    @Override
    public int getErrorCode()
    {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }
}
