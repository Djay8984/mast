package com.baesystems.ai.lr.service.jobs;

import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public interface JobService
{
    PageResource<JobDto> getJobsForAsset(Integer page, Integer size, String sort, String order, Long assetId) throws BadPageRequestException;

    JobDto getJob(Long jobId) throws RecordNotFoundException;

    JobDto createJob(JobDto jobDto) throws RecordNotFoundException;

    JobDto updateJob(Long jobId, JobDto jobDto) throws RecordNotFoundException, BadPageRequestException, MastBusinessException;

    PageResource<JobDto> getJobs(Integer page, Integer size, String sort, String order, Long employeeId, Long teamID, JobQueryDto query)
            throws BadPageRequestException;

    PageResource<JobDto> getJobsAbstract(Integer page, Integer size, String sort, String order, AbstractQueryDto query)
            throws BadPageRequestException;
}
