package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class is needed by the DummyIhsAssetDO class to match the structure of the AssetTypeDO class in the
 * VesrionedAssetDO class. The real AssetTypeDO cannot be used because it has too many fields and the hibernate enhancer
 * only work when both classes are in the same project.
 */
@Embeddable
public class DummyAssetTypeDO
{
    @Column
    private String code;

    @Column
    private String statDeCode;

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getStatDeCode()
    {
        return this.statDeCode;
    }

    public void setStatDeCode(final String statDeCode)
    {
        this.statDeCode = statDeCode;
    }
}
