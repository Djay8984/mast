package com.baesystems.ai.lr.domain.ihs.entities;


public class PersonnelContactDO
{
    private Long id;

    private String addressLine1;

    private String addressLine2;

    private String addressLine3;

    private String streetNo;

    private String streetName;

    private String street1;

    private String street2;

    private String townName1;

    private String townName2;

    private String fullCountryName;

    private String prePostCode;

    private String postPostCode;

    private String telephone;

    private String fax;

    private String initials;

    private String surname;

    private String personfullName;

    private String jobPosition;

    private String personTelephone;

    private String email;

    private String personMobileTelephone;

    public PersonnelContactDO()
    {

    }

    public PersonnelContactDO(Long id, String addressLine1, String addressLine2, String addressLine3, String streetNo, String streetName,
            String street1, String street2, String townName1, String townName2, String fullCountryName, String prePostCode, String postPostCode,
            String telephone, String fax, String initials, String surname, String personfullName, String jobPosition, String personTelephone, String email,
            String personMobileTelephone)
    {
        this.id = id;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.streetNo = streetNo;
        this.streetName = streetName;
        this.street1 = street1;
        this.street2 = street2;
        this.townName1 = townName1;
        this.townName2 = townName2;
        this.fullCountryName = fullCountryName;
        this.prePostCode = prePostCode;
        this.postPostCode = postPostCode;
        this.telephone = telephone;
        this.fax = fax;
        this.initials = initials;
        this.surname = surname;
        this.personfullName = personfullName;
        this.jobPosition = jobPosition;
        this.personTelephone = personTelephone;
        this.email = email;
        this.personMobileTelephone = personMobileTelephone;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getStreetNo()
    {
        return streetNo;
    }

    public void setStreetNo(final String streetNo)
    {
        this.streetNo = streetNo;
    }

    public String getStreetName()
    {
        return streetName;
    }

    public void setStreetName(final String streetName)
    {
        this.streetName = streetName;
    }

    public String getStreet1()
    {
        return street1;
    }

    public void setStreet1(final String street1)
    {
        this.street1 = street1;
    }

    public String getStreet2()
    {
        return street2;
    }

    public void setStreet2(final String street2)
    {
        this.street2 = street2;
    }

    public String getTownName1()
    {
        return townName1;
    }

    public void setTownName1(final String townName1)
    {
        this.townName1 = townName1;
    }

    public String getTownName2()
    {
        return townName2;
    }

    public void setTownName2(final String townName2)
    {
        this.townName2 = townName2;
    }

    public String getFullCountryName()
    {
        return fullCountryName;
    }

    public void setFullCountryName(final String fullCountryName)
    {
        this.fullCountryName = fullCountryName;
    }

    public String getPrePostCode()
    {
        return prePostCode;
    }

    public void setPrePostCode(final String prePostCode)
    {
        this.prePostCode = prePostCode;
    }

    public String getPostPostCode()
    {
        return postPostCode;
    }

    public void setPostPostCode(final String postPostCode)
    {
        this.postPostCode = postPostCode;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(final String fax)
    {
        this.fax = fax;
    }

    public String getInitials()
    {
        return initials;
    }

    public void setInitials(final String initials)
    {
        this.initials = initials;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(final String surname)
    {
        this.surname = surname;
    }

    public String getPersonfullName()
    {
        return personfullName;
    }

    public void setPersonfullName(String personfullName)
    {
        this.personfullName = personfullName;
    }

    public String getJobPosition()
    {
        return jobPosition;
    }

    public void setJobPosition(final String jobPosition)
    {
        this.jobPosition = jobPosition;
    }

    public String getPersonTelephone()
    {
        return personTelephone;
    }

    public void setPersonTelephone(final String personTelephone)
    {
        this.personTelephone = personTelephone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }

    public String getPersonMobileTelephone()
    {
        return personMobileTelephone;
    }

    public void setPersonMobileTelephone(final String personMobileTelephone)
    {
        this.personMobileTelephone = personMobileTelephone;
    }
}
