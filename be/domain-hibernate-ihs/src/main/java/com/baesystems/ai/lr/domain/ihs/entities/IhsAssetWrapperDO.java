package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * This class is meant to mimic the structure of AssetDO so that the same field mappings can be used in the queries.
 */
@Entity
@Immutable
@Table(name = "ABSD_SHIP_SEARCH")
public class IhsAssetWrapperDO
{
    @Id
    @Column(name = "LRNO")
    private Long hiddenId; // This is needed for so there is a primary key, but should not be used by the query

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "LRNO", insertable = false, updatable = false)
    private DummyIhsAssetDO publishedVersion;

    public Long getHiddenId()
    {
        return this.hiddenId;
    }

    public void setHiddenId(final Long hiddenId)
    {
        this.hiddenId = hiddenId;
    }

    public DummyIhsAssetDO getPublishedVersion()
    {
        return this.publishedVersion;
    }

    public void setPublishedVersion(final DummyIhsAssetDO publishedVersion)
    {
        this.publishedVersion = publishedVersion;
    }

}
