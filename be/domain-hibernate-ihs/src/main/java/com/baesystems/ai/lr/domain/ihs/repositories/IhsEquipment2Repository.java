package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsEquipment2DO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsEquipment2Repository extends SpringDataRepository<IhsEquipment2DO, Long> 
{

}
