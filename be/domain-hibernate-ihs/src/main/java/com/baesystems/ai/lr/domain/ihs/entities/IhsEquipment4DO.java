package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_LREQ4")
public class IhsEquipment4DO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "F08_EQIP_LET", length = 1)
    private String equipmentLetter;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getEquipmentLetter()
    {
        return equipmentLetter;
    }

    public void setEquipmentLetter(final String equipmentLetter)
    {
        this.equipmentLetter = equipmentLetter;
    }
}
