package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_SSCH2_COLLATED")
public class IhsSsch2DO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "GROSS")
    private Integer grossTonnage;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Integer getGrossTonnage()
    {
        return grossTonnage;
    }

    public void setGrossTonnage(final Integer grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }
}
