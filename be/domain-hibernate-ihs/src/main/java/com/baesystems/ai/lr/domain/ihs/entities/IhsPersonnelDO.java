package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_PERSONNEL")
public class IhsPersonnelDO extends BaseDO
{
    @Id
    @Column(name = "PERSONNEL_ID")
    private Long id;

    @Column(name = "OWCODE", length = 7)
    private Long owcode;
    
    @Column(name = "INITIALS", length = 20)
    private String initials;

    @Column(name = "SURNAME", length = 40)
    private String surname;

    @Column(name = "FULL_NAME", length = 100)
    private String fullName;

    @Column(name = "JOB_POSITION", length = 254)
    private String jobPosition;
    
    @Column(name = "MOBILE_TELEPHONE", length = 22)
    private String mobileTelephone;

    @Column(name = "AOH_TELEPHONE", length = 22)
    private String aohTelephone;

    @Column(name = "EMAIL", length = 100)
    private String email;
    
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Long getOwcode()
    {
        return owcode;
    }

    public void setOwcode(final Long owcode)
    {
        this.owcode = owcode;
    }

    public String getInitials()
    {
        return initials;
    }

    public void setInitials(final String initials)
    {
        this.initials = initials;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(final String surname)
    {
        this.surname = surname;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(final String fullName)
    {
        this.fullName = fullName;
    }

    public String getJobPosition()
    {
        return jobPosition;
    }

    public void setJobPosition(final String jobPosition)
    {
        this.jobPosition = jobPosition;
    }

    public String getMobileTelephone()
    {
        return mobileTelephone;
    }

    public void setMobileTelephone(final String mobileTelephone)
    {
        this.mobileTelephone = mobileTelephone;
    }

    public String getAohTelephone()
    {
        return aohTelephone;
    }

    public void setAohTelephone(final String aohTelephone)
    {
        this.aohTelephone = aohTelephone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }

}
