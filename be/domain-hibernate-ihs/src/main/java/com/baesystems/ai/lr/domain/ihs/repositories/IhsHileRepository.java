package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHileDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHileRepository extends SpringDataRepository<IhsHileDO, Long>
{

}
