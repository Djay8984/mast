package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_GROUP_FLEET")
public class IhsGroupFleetDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "REGD_OWNER", length = 7)
    private String regdOwner;

    @Column(name = "GROUP_OWNER", length = 7)
    private String groupOwner;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getRegdOwner()
    {
        return regdOwner;
    }

    public void setRegdOwner(final String regdOwner)
    {
        this.regdOwner = regdOwner;
    }

    public String getGroupOwner()
    {
        return groupOwner;
    }

    public void setGroupOwner(final String groupOwner)
    {
        this.groupOwner = groupOwner;
    }

}
