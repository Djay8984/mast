package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHidrDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHidrRepository extends SpringDataRepository<IhsHidrDO, Long> 
{

}
