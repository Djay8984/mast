package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsAssetRepository extends SpringDataRepository<IhsAssetDO, Long>
{
}
