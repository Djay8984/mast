package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsAssetWrapperDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsAssetWrapperRepository extends SpringDataRepository<IhsAssetWrapperDO, Long>
{
    void setStrictMissingFields(boolean strictMissingFields);
}
