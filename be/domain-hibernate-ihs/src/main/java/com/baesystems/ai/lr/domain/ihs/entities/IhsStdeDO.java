package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "SUPPLEMENTAL_ABSD_STDE")
public class IhsStdeDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "C05_NO_DECKS", length = 2)
    private String decks;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getDecks()
    {
        return decks;
    }

    public void setDecks(final String decks)
    {
        this.decks = decks;
    }
}
