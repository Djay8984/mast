package com.baesystems.ai.lr.domain.ihs.entities.references;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class IhsAssetTypeDO
{

    @Column
    private String stat5Code;

    @Column
    private String statDeCode;

    public String getStat5Code()
    {
        return stat5Code;
    }

    public void setStat5Code(String stat5Code)
    {
        this.stat5Code = stat5Code;
    }

    public String getStatDeCode()
    {
        return statDeCode;
    }

    public void setStatDeCode(String statDeCode)
    {
        this.statDeCode = statDeCode;
    }
}
