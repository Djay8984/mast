package com.baesystems.ai.lr.domain.ihs.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * This class is meant to mimic the structure of VersionedAssetDO so that the same field mappings can be used in the
 * queries.
 */
@Entity
@Immutable
@Table(name = "ABSD_SHIP_SEARCH")
@SecondaryTable(name = "ABSD_SSCH2_COLLATED", pkJoinColumns = @PrimaryKeyJoinColumn(name = "LRNO"))
@SuppressWarnings("PMD.ExcessivePublicCount")
public class DummyIhsAssetDO
{
    @Id
    @Column(name = "LRNO")
    private Long hiddenId; // This is needed for so there is a primary key, but should not be used by the query

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "LRNO", insertable = false, updatable = false))})
    private DummyLinkDO ihsAsset;

    @Column(name = "LRNO", insertable = false, updatable = false)
    private String imoNumber;

    @Column(name = "YARDNO")
    private String yardNumber;

    @Column(name = "VESSELNAME")
    private String name;

    @Column(name = "DATEOFBUILD")
    private Date buildDate;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "flagCode", column = @Column(name = "FLAG"))})
    private DummyFlagStateDO flagState;

    @Embedded
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "STAT5CODE")),
                         @AttributeOverride(name = "statDeCode", column = @Column(name = "STATDECODE"))
    })
    private DummyAssetTypeDO assetType;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "name", column = @Column(name = "STATUS"))})
    private DummyClassStatusDO classStatus;

    @Column(name = "GROSS", table = "ABSD_SSCH2_COLLATED")
    private Double grossTonnage;

    @ElementCollection
    @CollectionTable(name = "ABSD_SHIP_SEARCH", joinColumns = @JoinColumn(name = "LRNO"))
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "BUILDERCODE")),
                         @AttributeOverride(name = "name", column = @Column(name = "SHIPBUILDER"))
    })
    private List<DummyPartyDO> builderInfo;

    @ElementCollection
    @CollectionTable(name = "ABSD_SHIP_SEARCH", joinColumns = @JoinColumn(name = "LRNO"))
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "SHIPMANAGERCODE")),
                         @AttributeOverride(name = "name", column = @Column(name = "SHIPMANAGER"))
    })
    private List<DummyPartyDO> shipManagerInfo;

    @ElementCollection
    @CollectionTable(name = "ABSD_SHIP_SEARCH", joinColumns = @JoinColumn(name = "LRNO"))
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "OPERATORCODE")),
                         @AttributeOverride(name = "name", column = @Column(name = "OPERATOR"))
    })
    private List<DummyPartyDO> operatorInfo;

    @ElementCollection
    @CollectionTable(name = "ABSD_SHIP_SEARCH", joinColumns = @JoinColumn(name = "LRNO"))
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "TECHMANCODE")),
                         @AttributeOverride(name = "name", column = @Column(name = "TECHMANAGER"))
    })
    private List<DummyPartyDO> techManagerInfo;

    @ElementCollection
    @CollectionTable(name = "ABSD_SHIP_SEARCH", joinColumns = @JoinColumn(name = "LRNO"))
    @AttributeOverrides({
                         @AttributeOverride(name = "code", column = @Column(name = "DOCCODE")),
                         @AttributeOverride(name = "name", column = @Column(name = "DOCCOMPANY"))
    })
    private List<DummyPartyDO> docCompanyInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "ABSD_GROUP_FLEET", joinColumns = 
    @JoinColumn(name = "LRNO", referencedColumnName = "LRNO"), inverseJoinColumns = @JoinColumn(name = "REGD_OWNER", referencedColumnName = "OWCODE"))
    private List<DummyIhsOwnaDO> registeredOwnerInfo;

    @OneToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "ABSD_GROUP_FLEET", joinColumns = 
    @JoinColumn(name = "LRNO", referencedColumnName = "LRNO"), inverseJoinColumns = @JoinColumn(name = "GROUP_OWNER", referencedColumnName = "OWCODE"))
    private List<DummyIhsOwnaDO> groupOwnerInfo;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "LRNO")
    private IhsHistDO ihsHist;

    @Column(name = "PORTNAME", length = 50)
    private String portName;

    @Column(name = "CALLSIGN", length = 13)
    private String callSign;

    @Column(name = "OFFICIALNO", length = 30)
    private String officialNo;

    @Column(name = "LOA", precision = 6, scale = 3)
    private Double lengthOverall;

    @Column(name = "COB", length = 3)
    private String countryOfBuild;

    public Long getHiddenId()
    {
        return this.hiddenId;
    }

    public void setHiddenId(final Long hiddenId)
    {
        this.hiddenId = hiddenId;
    }

    public DummyLinkDO getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final DummyLinkDO ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public String getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final String imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Date getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public DummyFlagStateDO getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final DummyFlagStateDO flagState)
    {
        this.flagState = flagState;
    }

    public DummyAssetTypeDO getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final DummyAssetTypeDO assetType)
    {
        this.assetType = assetType;
    }

    public DummyClassStatusDO getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final DummyClassStatusDO classStatus)
    {
        this.classStatus = classStatus;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public List<DummyPartyDO> getBuilderInfo()
    {
        return this.builderInfo;
    }

    public void setBuilderInfo(final List<DummyPartyDO> builderInfo)
    {
        this.builderInfo = builderInfo;
    }

    public List<DummyPartyDO> getShipManagerInfo()
    {
        return this.shipManagerInfo;
    }

    public void setShipManagerInfo(final List<DummyPartyDO> shipManagerInfo)
    {
        this.shipManagerInfo = shipManagerInfo;
    }

    public List<DummyPartyDO> getOperatorInfo()
    {
        return this.operatorInfo;
    }

    public void setOperatorInfo(final List<DummyPartyDO> operatorInfo)
    {
        this.operatorInfo = operatorInfo;
    }

    public List<DummyPartyDO> getTechManagerInfo()
    {
        return this.techManagerInfo;
    }

    public void setTechManagerInfo(final List<DummyPartyDO> techManagerInfo)
    {
        this.techManagerInfo = techManagerInfo;
    }

    public List<DummyPartyDO> getDocCompanyInfo()
    {
        return this.docCompanyInfo;
    }

    public void setDocCompanyInfo(final List<DummyPartyDO> docCompanyInfo)
    {
        this.docCompanyInfo = docCompanyInfo;
    }

    public List<DummyIhsOwnaDO> getRegisteredOwnerInfo()
    {
        return this.registeredOwnerInfo;
    }

    public void setRegisteredOwnerInfo(final List<DummyIhsOwnaDO> registeredOwnerInfo)
    {
        this.registeredOwnerInfo = registeredOwnerInfo;
    }

    public List<DummyIhsOwnaDO> getGroupOwnerInfo()
    {
        return this.groupOwnerInfo;
    }

    public void setGroupOwnerInfo(final List<DummyIhsOwnaDO> groupOwnerInfo)
    {
        this.groupOwnerInfo = groupOwnerInfo;
    }

    public IhsHistDO getIhsHist()
    {
        return this.ihsHist;
    }

    public void setIhsHist(final IhsHistDO ihsHist)
    {
        this.ihsHist = ihsHist;
    }

    public String getPortName()
    {
        return this.portName;
    }

    public void setPortName(final String portName)
    {
        this.portName = portName;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getOfficialNo()
    {
        return this.officialNo;
    }

    public void setOfficialNo(final String officialNo)
    {
        this.officialNo = officialNo;
    }

    public Double getLengthOverall()
    {
        return this.lengthOverall;
    }

    public void setLengthOverall(final Double lengthOverall)
    {
        this.lengthOverall = lengthOverall;
    }

    public String getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final String countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }
}
