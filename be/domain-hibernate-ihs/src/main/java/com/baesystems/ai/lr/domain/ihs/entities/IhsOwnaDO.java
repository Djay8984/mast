package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_OWNA")
public class IhsOwnaDO extends BaseDO
{
    @Id
    @Column(name = "OWCODE")
    private Long id;

    @Column(name = "RBNMSTYL_NAMSTYL_1", length = 40)
    private String companyName;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(final String companyName)
    {
        this.companyName = companyName;
    }
}
