package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class is needed by the DummyIhsAssetDO class to match the structure of the ClassStatusDO class in the
 * VesrionedAssetDO class. The real ClassStatusDO cannot be used because it has too many fields and the hibernate
 * enhancer only work when both classes are in the same project.
 */
@Embeddable
public class DummyClassStatusDO
{
    @Column
    private String name;

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
