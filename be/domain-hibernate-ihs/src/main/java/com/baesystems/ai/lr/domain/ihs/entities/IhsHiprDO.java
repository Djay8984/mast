package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HIPR")
public class IhsHiprDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "D05_PRTYP", length = 4)
    private String propulsion;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getPropulsion()
    {
        return propulsion;
    }

    public void setPropulsion(final String propulsion)
    {
        this.propulsion = propulsion;
    }
}
