package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHiprDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHiprRepository extends SpringDataRepository<IhsHiprDO, Long> 
{

}
