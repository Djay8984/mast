package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class is needed by the DummyIhsAssetDO class to match the structure of the LinkDOs in the VesrionedAssetDO
 * class. The real LinkDO cannot be used because the hibernate enhancer only work when both classes are in the same
 * project.
 */
@Embeddable
public class DummyLinkDO
{
    @Column
    private Long id;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }
}
