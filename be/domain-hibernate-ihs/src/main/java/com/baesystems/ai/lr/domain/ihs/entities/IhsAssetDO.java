package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;
import com.baesystems.ai.lr.domain.ihs.entities.references.IhsAssetTypeDO;

@Entity
@Table(name = "ABSD_SHIP_SEARCH")
public class IhsAssetDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "YARDNO", length = 50)
    private String yardNumber;

    @Column(name = "SHIPBUILDER", length = 50)
    private String builder;

    @Column(name = "BUILDERCODE", length = 9)
    private String builderCode;

    @Column(name = "VESSELNAME", length = 50)
    private String name;

    @Column(name = "DATEOFBUILD", length = 6)
    private String dateOfBuild;

    @Embedded
    @AttributeOverrides({
                         @AttributeOverride(name = "stat5Code", column = @Column(name = "STAT5CODE")),
                         @AttributeOverride(name = "statDeCode", column = @Column(name = "STATDECODE"))})
    private IhsAssetTypeDO ihsAssetType;

    @Column(name = "PORTNAME", length = 50)
    private String portName;

    @Column(name = "SHIPMANAGER", length = 50)
    private String shipManager;

    @Column(name = "CALLSIGN", length = 13)
    private String callSign;

    @Column(name = "DOCCOMPANY", length = 50)
    private String docCompany;

    @Column(name = "FLAG", length = 3)
    private String flag;

    @Column(name = "OFFICIALNO", length = 30)
    private String officialNo;

    @Column(name = "OPERATOR", length = 50)
    private String operator;

    @Column(name = "TECHMANCODE", length = 7)
    private String techManagerCode;

    @Column(name = "LOA", precision = 6, scale = 3)
    private Double lengthOverall;

    @Column(name = "SHIPMANAGERCODE", length = 7)
    private String shipManagerCode;

    @Column(name = "OPERATORCODE", length = 7)
    private String operatorCode;

    @Column(name = "TECHMANAGER", length = 50)
    private String techManager;

    @Column(name = "DOCCODE", length = 7)
    private String docCode;

    @Column(name = "COB", length = 3)
    private String countryOfBuild;

    @Column(name = "STATUS", length = 50)
    private String status;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public String getBuilder()
    {
        return this.builder;
    }

    public void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public String getBuilderCode()
    {
        return this.builderCode;
    }

    public void setBuilderCode(final String builderCode)
    {
        this.builderCode = builderCode;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDateOfBuild()
    {
        return this.dateOfBuild;
    }

    public void setDateOfBuild(final String dateOfBuild)
    {
        this.dateOfBuild = dateOfBuild;
    }

    public IhsAssetTypeDO getIhsAssetType()
    {
        return this.ihsAssetType;
    }

    public void setIhsAssetType(final IhsAssetTypeDO ihsAssetType)
    {
        this.ihsAssetType = ihsAssetType;
    }

    public String getPortName()
    {
        return this.portName;
    }

    public void setPortName(final String portName)
    {
        this.portName = portName;
    }

    public String getShipManager()
    {
        return this.shipManager;
    }

    public void setShipManager(final String shipManager)
    {
        this.shipManager = shipManager;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getDocCompany()
    {
        return this.docCompany;
    }

    public void setDocCompany(final String docCompany)
    {
        this.docCompany = docCompany;
    }

    public String getFlag()
    {
        return this.flag;
    }

    public void setFlag(final String flag)
    {
        this.flag = flag;
    }

    public String getOfficialNo()
    {
        return this.officialNo;
    }

    public void setOfficialNo(final String officialNo)
    {
        this.officialNo = officialNo;
    }

    public String getOperator()
    {
        return this.operator;
    }

    public void setOperator(final String operator)
    {
        this.operator = operator;
    }

    public Double getLengthOverall()
    {
        return this.lengthOverall;
    }

    public void setLengthOverall(final Double lengthOverall)
    {
        this.lengthOverall = lengthOverall;
    }

    public String getShipManagerCode()
    {
        return this.shipManagerCode;
    }

    public void setShipManagerCode(final String shipManagerCode)
    {
        this.shipManagerCode = shipManagerCode;
    }

    public String getOperatorCode()
    {
        return this.operatorCode;
    }

    public void setOperatorCode(final String operatorCode)
    {
        this.operatorCode = operatorCode;
    }

    public String getTechManagerCode()
    {
        return this.techManagerCode;
    }

    public void setTechManagerCode(final String techManagerCode)
    {
        this.techManagerCode = techManagerCode;
    }

    public String getTechManager()
    {
        return this.techManager;
    }

    public void setTechManager(final String techManager)
    {
        this.techManager = techManager;
    }

    public String getDocCode()
    {
        return this.docCode;
    }

    public void setDocCode(final String docCode)
    {
        this.docCode = docCode;
    }

    public String getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final String countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }

    public String getStatus()
    {
        return this.status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }
}
