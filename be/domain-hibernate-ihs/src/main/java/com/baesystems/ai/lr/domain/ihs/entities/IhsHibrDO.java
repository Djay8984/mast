package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HIBR")
public class IhsHibrDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "C09_MLD", precision = 6, scale = 3)
    private Double breadthMoulded;

    @Column(name = "C09_EX", precision = 6, scale = 3)
    private Double breadthExtreme;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Double getBreadthMoulded()
    {
        return breadthMoulded;
    }

    public void setBreadthMoulded(final Double breadthMoulded)
    {
        this.breadthMoulded = breadthMoulded;
    }

    public Double getBreadthExtreme()
    {
        return breadthExtreme;
    }

    public void setBreadthExtreme(final Double breadthExtreme)
    {
        this.breadthExtreme = breadthExtreme;
    }

}
