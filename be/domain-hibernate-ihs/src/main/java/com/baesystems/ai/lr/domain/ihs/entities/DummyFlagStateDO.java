package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class is needed by the DummyIhsAssetDO class to match the structure of FlagStateDO in the VesrionedAssetDO
 * class. The real FlagStateDO cannot be used because the hibernate enhancer only work when both classes are in the same
 * project.
 */
@Embeddable
public class DummyFlagStateDO
{
    @Column
    private String flagCode;

    public String getFlagCode()
    {
        return this.flagCode;
    }

    public void setFlagCode(final String flagCode)
    {
        this.flagCode = flagCode;
    }
}
