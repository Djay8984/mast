package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsEquipment4DO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsEquipment4Repository extends SpringDataRepository<IhsEquipment4DO, Long> 
{

}
