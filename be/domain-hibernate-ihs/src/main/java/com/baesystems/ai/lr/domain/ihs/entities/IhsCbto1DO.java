package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_CBTO1")
public class IhsCbto1DO extends BaseDO
{
    @Id
    @Column(name = "KEY")
    private Long id;

    @Column(name = "CNTRY", length = 3)
    private String country;

    @Column(name = "TOWN", length = 4)
    private String town;

    @Column(name = "TNS1", length = 19)
    private String townName1;

    @Column(name = "TNS2", length = 29)
    private String townName2;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getTownName1()
    {
        return townName1;
    }

    public void setTownName1(final String townName1)
    {
        this.townName1 = townName1;
    }

    public String getTownName2()
    {
        return townName2;
    }

    public void setTownName2(final String townName2)
    {
        this.townName2 = townName2;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(final String country)
    {
        this.country = country;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(final String town)
    {
        this.town = town;
    }

}
