package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHitlDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHitlRepository extends SpringDataRepository<IhsHitlDO, Long> 
{

}
