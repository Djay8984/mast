package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_CBTO2")
public class IhsCbto2DO extends BaseDO
{
    @Id
    @Column(name = "KEY", length = 4)
    private Long id;

    @Column(name = "CNTRY", length = 3)
    private String country;

    @Column(name = "TOWN", length = 4)
    private String town;

    @Column(name = "STYL1", length = 30)
    private String streetName;

    @Column(name = "STYL2", length = 30)
    private String street1;

    @Column(name = "STYL3", length = 30)
    private String street2;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(final String country)
    {
        this.country = country;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(final String town)
    {
        this.town = town;
    }

    public String getStreetName()
    {
        return streetName;
    }

    public void setStreetName(final String streetName)
    {
        this.streetName = streetName;
    }

    public String getStreet1()
    {
        return street1;
    }

    public void setStreet1(final String street1)
    {
        this.street1 = street1;
    }

    public String getStreet2()
    {
        return street2;
    }

    public void setStreet2(final String street2)
    {
        this.street2 = street2;
    }
}
