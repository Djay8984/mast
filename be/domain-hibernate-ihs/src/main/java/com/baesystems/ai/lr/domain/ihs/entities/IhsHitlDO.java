package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HITL")
public class IhsHitlDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "B07_NET")
    private Integer netTonnage;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Integer getNetTonnage()
    {
        return netTonnage;
    }

    public void setNetTonnage(final Integer netTonnage)
    {
        this.netTonnage = netTonnage;
    }
}
