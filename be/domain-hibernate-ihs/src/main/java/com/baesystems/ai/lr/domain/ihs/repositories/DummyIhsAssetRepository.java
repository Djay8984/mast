package com.baesystems.ai.lr.domain.ihs.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.ihs.entities.DummyIhsAssetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface DummyIhsAssetRepository extends SpringDataRepository<DummyIhsAssetDO, Long>
{
    @Query("select asset from DummyIhsAssetDO asset"
            + " where asset.ihsAsset.id = :imoNumber")
    DummyIhsAssetDO findByImoNumber(@Param("imoNumber") Long imoNumber);
}
