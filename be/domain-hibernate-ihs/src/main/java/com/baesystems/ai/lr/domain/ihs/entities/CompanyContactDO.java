package com.baesystems.ai.lr.domain.ihs.entities;

public class CompanyContactDO
{

    private Long id;

    private String companyName;

    private String addressLine1;

    private String addressLine2;

    private String addressLine3;

    private String streetNo;

    private String streetName;

    private String streetName1;

    private String streetName2;

    private String townName1;

    private String townName2;

    private String countryFullName;

    private String prePostCode;

    private String postPostCode;

    private String telephone;

    private String fax;

    private String email;

    public CompanyContactDO()
    {

    }

    public CompanyContactDO(Long id, String companyName, String addressLine1, String addressLine2, String addressLine3, String streetNo,
            String streetName, String streetName1, String streetName2, String townName1, String townName2, String countryFullName, String prePostCode,
            String postPostCode, String telephone, String fax, String email1, String email2, String email3, String email4, String email5,
            String email6)
    {
        this.id = id;
        this.companyName = companyName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.streetNo = streetNo;
        this.streetName = streetName;
        this.streetName1 = streetName1;
        this.streetName2 = streetName2;
        this.townName1 = townName1;
        this.townName2 = townName2;
        this.countryFullName = countryFullName;
        this.prePostCode = prePostCode;
        this.postPostCode = postPostCode;
        this.telephone = telephone;
        this.fax = fax;
        this.email = null == email1 ? ""
                : email1.concat(null == email2 ? "" : email2).concat(null == email3 ? "" : email3).concat(null == email4 ? "" : email4)
                        .concat(null == email5 ? "" : email5).concat(null == email6 ? "" : email6);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(final String companyName)
    {
        this.companyName = companyName;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getStreetNo()
    {
        return streetNo;
    }

    public void setStreetNo(final String streetNo)
    {
        this.streetNo = streetNo;
    }

    public String getStreetName()
    {
        return streetName;
    }

    public void setStreetName(final String streetName)
    {
        this.streetName = streetName;
    }

    public String getStreetName1()
    {
        return streetName1;
    }

    public void setStreetName1(final String streetName1)
    {
        this.streetName1 = streetName1;
    }

    public String getStreetName2()
    {
        return streetName2;
    }

    public void setStreetName2(final String streetName2)
    {
        this.streetName2 = streetName2;
    }

    public String getTownName1()
    {
        return townName1;
    }

    public void setTownName1(final String townName1)
    {
        this.townName1 = townName1;
    }

    public String getTownName2()
    {
        return townName2;
    }

    public void setTownName2(final String townName2)
    {
        this.townName2 = townName2;
    }

    public String getCountryFullName()
    {
        return countryFullName;
    }

    public void setCountryFullName(final String countryFullName)
    {
        this.countryFullName = countryFullName;
    }

    public String getPrePostCode()
    {
        return prePostCode;
    }

    public void setPrePostCode(final String prePostCode)
    {
        this.prePostCode = prePostCode;
    }

    public String getPostPostCode()
    {
        return postPostCode;
    }

    public void setPostPostCode(final String postPostCode)
    {
        this.postPostCode = postPostCode;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(final String fax)
    {
        this.fax = fax;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }

}
