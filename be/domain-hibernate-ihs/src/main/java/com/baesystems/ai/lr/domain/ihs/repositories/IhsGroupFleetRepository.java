package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsGroupFleetDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsGroupFleetRepository extends SpringDataRepository<IhsGroupFleetDO, Long> {
	
}
