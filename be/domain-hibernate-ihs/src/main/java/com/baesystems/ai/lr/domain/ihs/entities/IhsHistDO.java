package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HIST")
public class IhsHistDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "A02_EFD", length = 8)
    private String keelLayingDate;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getKeelLayingDate()
    {
        return keelLayingDate;
    }

    public void setKeelLayingDate(final String keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

}
