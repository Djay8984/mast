package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHibrDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHibrRepository extends SpringDataRepository<IhsHibrDO, Long> 
{

}
