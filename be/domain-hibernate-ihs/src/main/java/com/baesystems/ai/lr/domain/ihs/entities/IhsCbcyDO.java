package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_CBCY")
public class IhsCbcyDO extends BaseDO
{
    @Id
    @Column(name = "KEY")
    private Long id;

    @Column(name = "GNST", length = 40)
    private String fullCountryName;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getFullCountryName()
    {
        return fullCountryName;
    }

    public void setFullCountryName(final String fullCountryName)
    {
        this.fullCountryName = fullCountryName;
    }
}
