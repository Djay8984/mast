package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_OWIN")
public class IhsOwinDO extends BaseDO
{
    @Id
    @Column(name = "OWCODE")
    private Long id;

    @Column(name = "CNTRY", length = 3)
    private String country;

    @Column(name = "TOWN", length = 4)
    private String town;
    
    @Column(name = "SRFB_1", length = 35)
    private String addressLine1;

    @Column(name = "SRFB_2", length = 35)
    private String addressLine2;

    @Column(name = "SRFB_3", length = 35)
    private String addressLine3;

    @Column(name = "STREET", length = 4)
    private String street;

    @Column(name = "STNO", length = 18)
    private String streetNo;

    @Column(name = "PRE_POSTCODE", length = 15)
    private String prePostCode;

    @Column(name = "POST_POSTCODE", length = 15)
    private String postPostCode;

    @Column(name = "TELPHN", length = 30)
    private String telephone;

    @Column(name = "FAX", length = 30)
    private String fax;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(final String country)
    {
        this.country = country;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(final String town)
    {
        this.town = town;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(final String street)
    {
        this.street = street;
    }

    public String getStreetNo()
    {
        return streetNo;
    }

    public void setStreetNo(final String streetNo)
    {
        this.streetNo = streetNo;
    }

    public String getPrePostCode()
    {
        return prePostCode;
    }

    public void setPrePostCode(final String prePostCode)
    {
        this.prePostCode = prePostCode;
    }

    public String getPostPostCode()
    {
        return postPostCode;
    }

    public void setPostPostCode(final String postPostCode)
    {
        this.postPostCode = postPostCode;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(final String telephone)
    {
        this.telephone = telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public void setFax(final String fax)
    {
        this.fax = fax;
    }

}
