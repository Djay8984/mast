package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_OWGE")
public class IhsOwgeDO extends BaseDO
{
    @Id
    @Column(name = "OWCODE")
    private Long id;

    @Column(name = "SHNAME", length = 30)
    private String shName;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getShName()
    {
        return shName;
    }

    public void setShName(final String shName)
    {
        this.shName = shName;
    }  
    
}
