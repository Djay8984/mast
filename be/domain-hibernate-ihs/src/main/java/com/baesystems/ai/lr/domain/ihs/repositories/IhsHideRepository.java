package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHideDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHideRepository extends SpringDataRepository<IhsHideDO, Long> 
{

}
