package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsStdeDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsStdeRepository extends SpringDataRepository<IhsStdeDO, Long>
{

}
