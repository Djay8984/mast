package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsHistDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsHistRepository  extends SpringDataRepository<IhsHistDO, Long> 
{

}
