package com.baesystems.ai.lr.domain.ihs.repositories;

import com.baesystems.ai.lr.domain.ihs.entities.IhsSsch2DO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsSsch2Repository extends SpringDataRepository<IhsSsch2DO, Long> 
{

}
