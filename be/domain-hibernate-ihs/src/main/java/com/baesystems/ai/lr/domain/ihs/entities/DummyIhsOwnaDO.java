package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ABSD_OWNA")
public class DummyIhsOwnaDO
{
    @Id
    @Column(name = "OWCODE")
    private Long code;

    @Column(name = "RBNMSTYL_NAMSTYL_1", length = 40)
    private String name;

    public Long getCode()
    {
        return this.code;
    }

    public void setCode(final Long code)
    {
        this.code = code;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
