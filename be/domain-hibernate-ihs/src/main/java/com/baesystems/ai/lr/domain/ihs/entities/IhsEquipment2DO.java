package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_LREQ2")
public class IhsEquipment2DO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "F08_CABLEN_MET", precision = 5, scale = 1)
    private Double cableLength;

    @Column(name = "F08_CABGRAD", length = 2)
    private String cableGrade;

    @Column(name = "F08_CABDIAM_IMP", length = 5)
    private String cableDiameter;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Double getCableLength()
    {
        return cableLength;
    }

    public void setCableLength(final Double cableLength)
    {
        this.cableLength = cableLength;
    }

    public String getCableGrade()
    {
        return cableGrade;
    }

    public void setCableGrade(final String cableGrade)
    {
        this.cableGrade = cableGrade;
    }

    public String getCableDiameter()
    {
        return cableDiameter;
    }

    public void setCableDiameter(final String cableDiameter)
    {
        this.cableDiameter = cableDiameter;
    }
}
