package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class is needed by the DummyIhsAssetDO class to match the structure of the PartyDO in the VesrionedAssetDO
 * class.
 */
@Embeddable
public class DummyPartyDO
{
    @Column
    private String code;

    @Column
    private String name;

    public String getCode()
    {
        return this.code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
