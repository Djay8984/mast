package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_OWCO")
public class IhsOwcoDO extends BaseDO
{
    @Id
    @Column(name = "OWCODE")
    private Long id;

    @Column(name = "OWSEQN", length = 2)
    private String owseqn;

    @Column(name = "TYPE", length = 1)
    private String type;
    
    @Column(name = "SEQN", length = 2)
    private String seqn;
    
    @Column(name = "CNUM1", length = 30)
    private String cnum1;

    @Column(name = "CNUM2", length = 30)
    private String cnum2;
    
    @Column(name = "CNUM3", length = 30)
    private String cnum3;

    @Column(name = "CNUM4", length = 30)
    private String cnum4;
    
    @Column(name = "CNUM5", length = 30)
    private String cnum5;
    
    @Column(name = "CNUM6", length = 30)
    private String cnum6;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getOwseqn()
    {
        return owseqn;
    }

    public void setOwseqn(final String owseqn)
    {
        this.owseqn = owseqn;
    }

    public String getType()
    {
        return type;
    }

    public void setType(final String type)
    {
        this.type = type;
    }

    public String getSeqn()
    {
        return seqn;
    }

    public void setSeqn(final String seqn)
    {
        this.seqn = seqn;
    }

    public String getCnum1()
    {
        return cnum1;
    }

    public void setCnum1(final String cnum1)
    {
        this.cnum1 = cnum1;
    }

    public String getCnum2()
    {
        return cnum2;
    }

    public void setCnum2(final String cnum2)
    {
        this.cnum2 = cnum2;
    }

    public String getCnum3()
    {
        return cnum3;
    }

    public void setCnum3(final String cnum3)
    {
        this.cnum3 = cnum3;
    }

    public String getCnum4()
    {
        return cnum4;
    }

    public void setCnum4(final String cnum4)
    {
        this.cnum4 = cnum4;
    }

    public String getCnum5()
    {
        return cnum5;
    }

    public void setCnum5(final String cnum5)
    {
        this.cnum5 = cnum5;
    }

    public String getCnum6()
    {
        return cnum6;
    }

    public void setCnum6(final String cnum6)
    {
        this.cnum6 = cnum6;
    }
    
}
