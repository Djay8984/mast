package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "vwABSD_HILE")
public class IhsHileDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "C02_LBP", length = 6)
    private String lengthBtwPerpendiculars;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getLengthBtwPerpendiculars()
    {
        return lengthBtwPerpendiculars;
    }

    public void setLengthBtwPerpendiculars(final String lengthBtwPerpendiculars)
    {
        this.lengthBtwPerpendiculars = lengthBtwPerpendiculars;
    }
}
