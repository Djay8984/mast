package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HIDR")
public class IhsHidrDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "C07_DL", precision = 6, scale = 3)
    private Double draughtMax;

    @Column(name = "C07_DWTL")
    private Integer deadWeight;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Double getDraughtMax()
    {
        return draughtMax;
    }

    public void setDraughtMax(final Double draughtMax)
    {
        this.draughtMax = draughtMax;
    }

    public Integer getDeadWeight()
    {
        return deadWeight;
    }

    public void setDeadWeight(final Integer deadWeight)
    {
        this.deadWeight = deadWeight;
    }
}
