package com.baesystems.ai.lr.domain.ihs.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "ABSD_HIDE")
public class IhsHideDO extends BaseDO
{
    @Id
    @Column(name = "LRNO")
    private Long id;

    @Column(name = "C10_MLD", precision = 6, scale = 3)
    private Double depthMoulded;

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public Double getDepthMoulded()
    {
        return depthMoulded;
    }

    public void setDepthMoulded(final Double depthMoulded)
    {
        this.depthMoulded = depthMoulded;
    }
}
