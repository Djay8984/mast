package com.baesystems.ai.lr.domain.ihs.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.baesystems.ai.lr.domain.ihs.entities.CompanyContactDO;
import com.baesystems.ai.lr.domain.ihs.entities.IhsOwgeDO;
import com.baesystems.ai.lr.domain.ihs.entities.PersonnelContactDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;

public interface IhsOwgeRepository extends SpringDataRepository<IhsOwgeDO, Long>
{

    @Query("SELECT new com.baesystems.ai.lr.domain.ihs.entities.CompanyContactDO(  "
            + " ship.id, "
            + " company.companyName,"
            + " address.addressLine1,"
            + " address.addressLine2,"
            + " address.addressLine3,"
            + " address.streetNo,"
            + " street.streetName,"
            + " street.street1,"
            + " street.street2,"
            + " town.townName1,"
            + " town.townName2,"
            + " country.fullCountryName,"
            + " address.prePostCode,"
            + " address.postPostCode,"
            + " address.telephone,"
            + " address.fax,"
            + " cemail.cnum1,"
            + " cemail.cnum2,"
            + " cemail.cnum3,"
            + " cemail.cnum4,"
            + " cemail.cnum5,"
            + " cemail.cnum6 )"
            + " FROM IhsOwgeDO ship,"
            + " IhsOwnaDO company,"
            + " IhsOwinDO address,"
            + " IhsCbto2DO street,"
            + " IhsCbto1DO town,"
            + " IhsCbcyDO country,"
            + " IhsOwcoDO cemail"
            + " WHERE ship.id = company.id and ship.id = address.id"
            + " AND address.street = street.id and address.town = street.town"
            + " AND cemail.id = company.id"
            + " AND address.country = country.id"
            + " AND address.town = town.town"
            + " AND address.country = town.country"
            + " AND address.country = street.country "
            + " AND cemail.seqn = :sequence"
            + " AND cemail.type = :emailType"
            + " AND ship.id IN (:imoNumber)")
    List<CompanyContactDO> getAddress(@Param("imoNumber") Long imoNumber, @Param("sequence") String sequence, @Param("emailType") String emailType);

    @Query("SELECT new com.baesystems.ai.lr.domain.ihs.entities.PersonnelContactDO( "
            + " personnel.owcode,"
            + " address.addressLine1,"
            + " address.addressLine2,"
            + " address.addressLine3,"
            + " address.streetNo,"
            + " street.streetName,"
            + " street.street1,"
            + " street.street2,"
            + " town.townName1,"
            + " town.townName2,"
            + " country.fullCountryName,"
            + " address.prePostCode,"
            + " address.postPostCode,"
            + " address.telephone,"
            + " address.fax,"
            + " personnel.initials,"
            + " personnel.surname,"
            + " personnel.fullName,"
            + " personnel.jobPosition,"
            + " personnel.aohTelephone,"
            + " personnel.email,"
            + " personnel.mobileTelephone  )"
            + " FROM IhsPersonnelDO personnel,"
            + " IhsOwinDO address,"
            + " IhsCbto2DO street ,"
            + " IhsCbto1DO town,"
            + " IhsCbcyDO country WHERE"
            + " personnel.owcode = address.id"
            + " AND address.street = street.id"
            + " AND address.town = town.town"
            + " AND address.country = country.id"
            + " AND address.country = town.country"
            + " AND address.country = street.country"
            + " AND personnel.owcode IN (:imoNumber)")
    List<PersonnelContactDO> getPersonnelAddress(@Param("imoNumber") Long imoNumber);

}
