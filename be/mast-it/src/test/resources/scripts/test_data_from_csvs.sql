-- Use different schema for working area
DROP SCHEMA IF EXISTS `${jdbc.mast.schema}_staging` ;
CREATE SCHEMA IF NOT EXISTS `${jdbc.mast.schema}_staging` DEFAULT CHARACTER SET utf8 ;

-- Import Assets
LOAD DATA LOCAL INFILE 'csvs/ASSET.csv'
INTO TABLE ${jdbc.mast.schema}.MAST_ASSET_Asset
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id, name, imo_number, @UNKNOWN_REG_TYPE_ID, asset_category_id)
SET
  asset_type_id=378,
  class_status_id=1,
  build_date='2015-08-19',
  gross_tonnage=100.0,
  builder='Ership Internacional SA',
  hull_class_notation='This asset was loaded from LR supplied CSV, but most of the fields are dafaulted',
  lead_asset_flag=TRUE,
  product_ruleset_id=7,
  society_ruleset_id=1,
  previous_society_ruleset_id=1,
  harmonisation_date='2001-01-01',
  registered_port_id=1,
  asset_lifecycle_status_id=3,
  flag_state_id=100;

-- Import items
DROP TABLE IF EXISTS ${jdbc.mast.schema}_staging.item;
CREATE TABLE ${jdbc.mast.schema}_staging.item (
  id INT NOT NULL,
  asset_id INT NOT NULL,
  name VARCHAR(100) NOT NULL,
  item_type_id INT NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

LOAD DATA LOCAL INFILE 'csvs/ITEM.csv'
INTO TABLE ${jdbc.mast.schema}_staging.item
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id, asset_id, name, @UNKNOWN_ITEM_ID, item_type_id);

DROP TABLE IF EXISTS ${jdbc.mast.schema}_staging.item_relationship;
CREATE TABLE ${jdbc.mast.schema}_staging.item_relationship (
  id INT NOT NULL,
  from_item_id INT NOT NULL,
  to_item_id INT NOT NULL,
  level INT NOT NULL,
  relationship_type_id INT NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

LOAD DATA LOCAL INFILE 'csvs/ITEM_RELATIONSHIP.csv'
INTO TABLE ${jdbc.mast.schema}_staging.item_relationship
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id, from_item_id, @IGNORED_ASSET_ID, to_item_id, @IGNORED_ASSET_ID, level, @IGNORED_SEQUENCE, relationship_type_id, @IGNORED_QUALIFIER);

INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItem (id, asset_id, name, item_type_id, reviewed)
SELECT i.id, i.asset_id, i.name, i.item_type_id, 0
FROM ${jdbc.mast.schema}_staging.item AS i
WHERE id NOT IN (
    SELECT to_item_id
    FROM ${jdbc.mast.schema}_staging.item_relationship
    WHERE relationship_type_id = 1);

UPDATE ${jdbc.mast.schema}.MAST_ASSET_Asset as a
JOIN ${jdbc.mast.schema}.MAST_ASSET_AssetItem as i ON a.id = i.asset_id
SET a.asset_item_id = i.id
WHERE i.id NOT IN (
    SELECT to_item_id
    FROM ${jdbc.mast.schema}_staging.item_relationship
    WHERE relationship_type_id = 1);

INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItem (id, asset_id, name, item_type_id, reviewed)
SELECT i.id, i.asset_id, i.name, i.item_type_id, 0
FROM ${jdbc.mast.schema}_staging.item as i
JOIN ${jdbc.mast.schema}_staging.item_relationship as r on i.id = r.to_item_id
WHERE r.relationship_type_id = 1
ORDER BY r.level;

INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship (id, from_item_id, to_item_id, relationship_type_id)
SELECT id, from_item_id, to_item_id, relationship_type_id
FROM ${jdbc.mast.schema}_staging.item_relationship
WHERE relationship_type_id IN (SELECT id from ${jdbc.mast.schema}.MAST_REF_AssetItemRelationshipType);

-- Import Attributes
DROP TABLE IF EXISTS ${jdbc.mast.schema}_staging.attribute;
CREATE TABLE ${jdbc.mast.schema}_staging.attribute (
  id INT NOT NULL,
  asset INT NOT NULL,
  item INT NOT NULL,
  value VARCHAR(100) NULL DEFAULT NULL,
  lov INT NULL DEFAULT NULL,
  attr_type INT NOT NULL,
  item_type INT NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

LOAD DATA LOCAL INFILE 'csvs/ATTRIBUTE.csv'
INTO TABLE ${jdbc.mast.schema}_staging.attribute
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(id, asset, item, value, lov, attr_type, item_type);

INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItemAttribute (id, item_id, attribute_type_id, value)
SELECT a.id, a.item, a.attr_type, a.value
FROM ${jdbc.mast.schema}_staging.attribute as a
WHERE a.lov IS NULL;

INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItemAttribute (id, item_id, attribute_type_id, value)
SELECT a.id, a.item, a.attr_type, l.name
FROM ${jdbc.mast.schema}_staging.attribute as a
JOIN ${jdbc.mast.schema}.MAST_REF_AllowedAssetAttributeValue as l ON l.id = a.lov
WHERE a.lov IS NOT NULL;
