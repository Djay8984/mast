INSERT INTO ${jdbc.ihs.schema}.ABSD_SHIP_SEARCH (LRNO, YARDNO, SHIPBUILDER, BUILDERCODE, VESSELNAME, DATEOFBUILD, STAT5CODE, STATDECODE, PORTNAME, SHIPMANAGER, OFFICIALNO, OPERATOR, CALLSIGN, DOCCOMPANY, FLAG, LOA, SHIPMANAGERCODE, OPERATORCODE, TECHMANCODE, TECHMANAGER, DOCCODE, COB, STATUS)
VALUES(1000019,'y1','A builder','b1','Alan Tank','111111','A11A2TN','LNG Tanker', 'KL Port', 'KL Ship Manager', '010-987-001','KL Operator', 'KL Call Sign', 'Docking Company', '1' , 101.11, '111','112','113', 'TMAN1', '1000191','MY','UNIDENTIFIED SHIP DATA'),
      (1000021,'y2','A builder','b1','Ben Tank','111111','A11B2TG','LPG Tanker', 'SG Port', 'SG Ship Manager', '010-987-002','SG Operator', 'SG Call Sign', 'Docking Company', '1', 102.213, '211','212','213', 'TMAN2','1000192','MY','UNIDENTIFIED SHIP DATA'),
      (1000033,'y3','A builder','b1','Charles Trawl','111111','B11A2FG','Factory Stern Trawler','Port 1', 'Ship Manager 1', '010-987-003','Operator 1', 'Call Sign 1', 'Docking Company 1', '1', 103.12, '311','312','313', 'TMAN3','1000193','MY','UNIDENTIFIED SHIP DATA'),
      (1000045,'y4','A builder','b1','David Trawl','111111','B11A2FS','Trawler','Port 2', 'Ship Manager 2', '010-987-004','Operator 2', 'Call Sign 2', 'Docking Company 2', '2', 104.2, '411','412','413', 'TMAN4','1000194','MY','UNIDENTIFIED SHIP DATA'),
      (1000057,'y5','A builder','b1','Edward Kelp','111111','B12G2FK','Kelp Dredger','Port 3', 'Ship Manager 3', '030-987-003','Operator 3', 'Call Sign 3', 'Docking Company 3', '2', 105.33, '511','512','513', 'TMAN5','1000195','MY','UNIDENTIFIED SHIP DATA'),
      (1000069,'y6','A builder','b1','Fiona Diver','111111','B22A2OV','Diving Support Vessel','Port 4', 'Ship Manager 4', '040-987-003','Operator 4', 'Call Sign 4', 'Docking Company 4', '2', 106.56, '611','612','113', 'TMAN6','1000196','MY','UNIDENTIFIED SHIP DATA'),
      (1000071,'y7','A builder','b1','Gerry Chemy','111111','W11A5TC','Chemical Tanker, Inland Waterways','Port 1', 'Ship Manager 1', '010-987-003','Operator 1', 'Call Sign 1', 'Docking Company 1', '2', 107.76,'711','712','713', 'TMAN7','1000197','MY','UNIDENTIFIED SHIP DATA'),
      (1000083,'y8','A builder','b1','Howard Howse','111111','X11A2YH','Houseboat','Port 5', 'Ship Manager 5', '050-987-003','Operator 5', 'Call Sign 5', 'Docking Company 5', '2', 102.44, '811','812','813', 'TMAN8','1000198','MY','UNIDENTIFIED SHIP DATA'),
      (1000095,'yyy1','b builder','b2','Iain Yot','111111','X11A2YP','Yacht','Port 1', 'Ship Manager 1', '010-987-003','Operator 1', 'Call Sign 1', 'Docking Company 1', '2', 102.3, '911','912','913', 'TMAN9','1000199','MY','UNIDENTIFIED SHIP DATA'),
      (1000100,'yyy2','b builder','b2','Jerry Krane','111111','X11C2AA','Crane Vessel, Naval Auxiliary','Port 6', 'Ship Manager 6', '010-987-006','Operator 6', 'Call Sign 6', 'Docking Company 6', '3', 102.4, '1011','1012','1013', 'TMAN10','1000200','MY','UNIDENTIFIED SHIP DATA'),
      (1000112,'yyy3','b builder','b2','Kerry S Pontoon','111111','Y11B4DS','Suction Dredger Pontoon','Port 7', 'Ship Manager 7', '070-987-003','Operator 7', 'Call Sign 7', 'Docking Company 7', '4', 102.4, '1111','1112','1113', 'TMAN11','1000201','MY','UNIDENTIFIED SHIP DATA'),
      (1000124,'yyy4','b builder','b2','Lionel Parker','111111','Y11B4WV','Car Park','Port 8', 'Ship Manager 8', '080-987-003','Operator 8', 'Call Sign 8', 'Docking Company 8', '3', 12.5, '1211','1212','1213', 'TMAN12','1000202','MY','UNIDENTIFIED SHIP DATA'),
      (1000136,'yyy5','b builder','b2','Matt Float','111111','Z11B4WF','Floating Dock','Port 9', 'Ship Manager 9', '090-987-003','Operator 9', 'Call Sign 9', 'Docking Company 9', '4', 1.345, '1311','1312','1313', 'TMAN13','1000203','MY','UNIDENTIFIED SHIP DATA'),
      (1000148,'yyy6','b builder','b2','Nemo S Mariner','111111','X11C4NS','Submarine','Port 10', 'Ship Manager 10', '010-987-003','Operator 10', 'Call Sign 10', 'Docking Company 10', '2', 132.55, '1411','1412','1413', 'TMAN14','1000204','MY','UNIDENTIFIED SHIP DATA'),
      (1000150,'yy1','C builder','b3','Obara I','111111','B35Y2XV','Sailing Vessel','Port 11', 'Ship Manager 11', '0110-987-003','Operator 11', 'Call Sign 11', 'Docking Company 11', '2', 152.23, '1511','1512','1513', 'TMAN15','1000205','MY','DOUBT'),
      (1000162,'yy2','C builder','b3','Obara II','111111','B35Y2XV','Sailing Vessel','Port 12', 'Ship Manager 12', '0120-987-003','Operator 12', 'Call Sign 12', 'Docking Company 12', '1', 152.65, '1611','1612','1613', 'TMAN16','1000206','MY','TO BE BROKEN UP'),
      (1000174,'yy3','C builder','b3','Obara III','111111','B35Y2XV','Sailing Vessel','Port 13', 'Ship Manager 13', '0130-987-003','Operator 13', 'Call Sign 13', 'Docking Company 13', '2', 143.89, '1711','1712','1713', 'TMAN17','1000207','MY','NAVAL VESSELS'),
      (1000186,'yy4','C builder','b3','Obara IV','111111','B35Y2XV','Sailing Vessel','Port 14', 'Ship Manager 14', '0140-987-003','Operator 14', 'Call Sign 14', 'Docking Company 14', '5', 102.09, '1811','1812','1813', 'TMAN18','1000208','MY','IN SERVICE/COMMISSION');

INSERT INTO ${jdbc.ihs.schema}.ABSD_GROUP_FLEET (LRNO, REGD_OWNER, GROUP_OWNER)
VALUES(1000019,'ROwnr1', 'GOwn1' ),
      (1000021,'ROwnr2', 'GOwn2'),
      (1000033,'ROwnr3', 'GOwn3'),
      (1000045,'ROwnr4', 'GOwn4'),
      (1000057,'ROwnr5', 'GOwn5'),
      (1000069,'ROwnr6', 'GOwn6'),
      (1000071,'ROwnr7', 'GOwn7'),
      (1000083,'ROwnr8', 'GOwn8'),
      (1000095,'ROwnr9', 'GOwn9'),
      (1000100,'ROwnr10', 'GOwn10'),
      (1000112,'ROwnr11', 'GOwn11'),
      (1000124,'ROwnr12', 'GOwn12'),
      (1000136,'ROwnr13', 'GOwn13'),
      (1000148,'ROwnr14', 'GOwn14'),
      (1000150,'ROwnr15', 'GOwn15'),
      (1000162,'ROwnr16', 'GOwn16'),
      (1000174,'ROwnr17', 'GOwn17'),
      (1000186,'ROwnr18', 'GOwn18');
      
INSERT INTO ${jdbc.ihs.schema}.ABSD_HIST (LRNO, A02_EFD)
VALUES(1000019,'20160901'),
      (1000021,'20160902'),
      (1000033,'20160903'),
      (1000045,'20160904'),
      (1000057,'20160905'),
      (1000069,'20160906'),
      (1000071,'20160907'),
      (1000083,'20160908'),
      (1000095,'20160909'),
      (1000100,'20160910'),
      (1000112,'20160911'),
      (1000124,'20160912'),
      (1000136,'20160913'),
      (1000148,'20160914'),
      (1000150,'20160915'),
      (1000162,'20160916'),
      (1000174,'20160917'),
      (1000186,'20160918');      

INSERT INTO ${jdbc.ihs.schema}.ABSD_LREQ2 (LRNO, F08_CABLEN_MET, F08_CABGRAD, F08_CABDIAM_IMP) 
VALUES 	(1000019, 10.4,'G1', '20'),
		(1000021, 1.4,'G2', '20'),
		(1000033, 3.4,'G3', '20'),
		(1000045, 4.4,'G4', '20'),
		(1000057, 5.4,'G5', '20'),
		(1000069, 5.4,'G5', '20'),
		(1000071, 5.4,'G5', '20'),
		(1000083, 5.4,'G5', '20'),
		(1000095, 5.4,'G5', '20'),
		(1000100, 5.4,'G5', '20'),
		(1000112, 5.4,'G5', '20'),
		(1000124, 5.4,'G5', '20'),
		(1000136, 5.4,'G5', '20'),
		(1000148, 5.4,'G5', '20'),
		(1000150, 5.4,'G5', '20'),
		(1000162, 5.4,'G5', '20'),
		(1000174, 5.4,'G5', '20'),
		(1000186, 5.4,'G5', '20'); 


INSERT INTO ${jdbc.ihs.schema}.ABSD_LREQ4 (LRNO, F08_EQIP_LET)
VALUES (1000019, '5'),
		(1000021,'4'),
		(1000033,'3'),
		(1000045,'2'),
		(1000057,'1'),
		(1000069,'1'),
		(1000071,'2'),
		(1000083,'3'),
		(1000095,'4'),
		(1000100,'2'),
		(1000112,'3'),
		(1000124,'4'),
		(1000136,'2'),
		(1000148,'3'),
		(1000150,'4'),
		(1000162,'1'),
		(1000174,'2'),
		(1000186,'4');       

		
INSERT INTO ${jdbc.ihs.schema}.vwABSD_HILE (LRNO, C02_LBP)
VALUES (1000019, '10000'),
		(1000021,'20000'),
		(1000033,'30000'),
		(1000045,'20000'),
		(1000057,'10000'),
		(1000069,'10000'),
		(1000071,'20000'),
		(1000083,'30000'),
		(1000095,'40000'),
		(1000100,'20000'),
		(1000112,'30000'),
		(1000124,'40000'),
		(1000136,'20000'),
		(1000148,'30000'),
		(1000150,'40000'),
		(1000162,'10000'),
		(1000174,'20000'),
		(1000186,'40000'); 



INSERT INTO ${jdbc.ihs.schema}.ABSD_HIBR (LRNO, C09_MLD, C09_EX)
VALUES (1000019, 100.99, 100.99),
		(1000021, 101.99, 101.99),
		(1000033, 102.99, 102.99),
		(1000045, 103.99, 103.99),
		(1000057, 104.99, 104.99),
		(1000069, 105.99, 105.99),
		(1000071, 106.99, 106.99),
		(1000083, 107.99, 107.99),
		(1000095, 108.99, 108.99),
		(1000100, 109.99, 109.99),
		(1000112, 110.99, 110.99),
		(1000124, 111.99, 111.99),
		(1000136, 112.99, 112.99),
		(1000148, 113.99, 113.99),
		(1000150, 114.99, 114.99),
		(1000162, 115.99, 115.99),
		(1000174, 116.99, 116.99),
		(1000186, 117.99, 117.99);


INSERT INTO ${jdbc.ihs.schema}.ABSD_HIDR (LRNO, C07_DL, C07_DWTL)
VALUES (1000019, 100.99, 100.99),
		(1000021, 101.99, 101.99),
		(1000033, 102.99, 102.99),
		(1000045, 103.99, 103.99),
		(1000057, 104.99, 104.99),
		(1000069, 105.99, 105.99),
		(1000071, 106.99, 106.99),
		(1000083, 107.99, 107.99),
		(1000095, 108.99, 108.99),
		(1000100, 109.99, 109.99),
		(1000112, 110.99, 110.99),
		(1000124, 111.99, 111.99),
		(1000136, 112.99, 112.99),
		(1000148, 113.99, 113.99),
		(1000150, 114.99, 114.99),
		(1000162, 115.99, 115.99),
		(1000174, 116.99, 116.99),
		(1000186, 117.99, 117.99);		
		
		
INSERT INTO ${jdbc.ihs.schema}.ABSD_SSCH2_COLLATED (LRNO, GROSS)
VALUES (1000019, 100),
		(1000021, 101),
		(1000033, 102),
		(1000045, 103),
		(1000057, 104),
		(1000069, 105),
		(1000071, 106),
		(1000083, 107),
		(1000095, 108),
		(1000100, 109),
		(1000112, 110),
		(1000124, 111),
		(1000136, 112),
		(1000148, 113),
		(1000150, 114),
		(1000162, 115),
		(1000174, 116),
		(1000186, 117);		
		
		

INSERT INTO ${jdbc.ihs.schema}.ABSD_HITL (LRNO, B07_NET)
VALUES (1000019, '12'),
		(1000021, '2'),
		(1000033, '3'),
		(1000045, '4'),
		(1000057, '3'),
		(1000069, '1'),
		(1000071, '22'),
		(1000083, '21'),
		(1000095, '22'),
		(1000100, '16'),
		(1000112, '13'),
		(1000124, '22'),
		(1000136, '22'),
		(1000148, '11'),
		(1000150, '31'),
		(1000162, '33'),
		(1000174, '22'),
		(1000186, '14');		

INSERT INTO ${jdbc.ihs.schema}.SUPPLEMENTAL_ABSD_STDE (LRNO, C05_NO_DECKS)
VALUES (1000019, '12'),
		(1000021, '2'),
		(1000033, '3'),
		(1000045, '4'),
		(1000057, '3'),
		(1000069, '1'),
		(1000071, '22'),
		(1000083, '21'),
		(1000095, '22'),
		(1000100, '16'),
		(1000112, '13'),
		(1000124, '22'),
		(1000136, '22'),
		(1000148, '11'),
		(1000150, '31'),
		(1000162, '33'),
		(1000174, '22'),
		(1000186, '14');
		
		
INSERT INTO ${jdbc.ihs.schema}.ABSD_HIPR (LRNO, D05_PRTYP)
VALUES (1000019, '12'),
		(1000021, '2'),
		(1000033, '3'),
		(1000045, '4'),
		(1000057, '3'),
		(1000069, '1'),
		(1000071, '22'),
		(1000083, '21'),
		(1000095, '22'),
		(1000100, '16'),
		(1000112, '13'),
		(1000124, '22'),
		(1000136, '22'),
		(1000148, '11'),
		(1000150, '31'),
		(1000162, '33'),
		(1000174, '22'),
		(1000186, '14');
		
		
INSERT INTO ${jdbc.ihs.schema}.ABSD_HIDE (LRNO, C10_MLD)
VALUES (1000019, 100.99),
		(1000021, 101.99),
		(1000033, 102.99),
		(1000045, 103.99),
		(1000057, 104.99),
		(1000069, 105.99),
		(1000071, 106.99),
		(1000083, 107.99),
		(1000095, 108.99),
		(1000100, 109.99),
		(1000112, 110.99),
		(1000124, 111.99),
		(1000136, 112.99),
		(1000148, 113.99),
		(1000150, 114.99),
		(1000162, 115.99),
		(1000174, 116.99),
		(1000186, 117.99);
		
		
INSERT INTO ${jdbc.ihs.schema}.ABSD_OWGE (OWCODE, SHNAME)
VALUES(1000191,'ROwnr1'),
      (1000192,'ROwnr2'),
      (1000193,'ROwnr3'),
      (1000194,'ROwnr4'),
      (1000195,'ROwnr5'),
      (1000211,'ROwnr6'),
      (1000212,'ROwnr7'),
      (1000213,'ROwnr8'),
      (1000214,'ROwnr9'),
      (1000215,'ROwnr10'),
      (1000331,'ROwnr11'),
      (1000332,'ROwnr12'),
      (1000333,'ROwnr13'),
      (1000334,'ROwnr14'),
      (1000335,'ROwnr15'),
      (1000451,'ROwnr16'),
      (1000452,'ROwnr17'),
      (1000453,'ROwnr18');   
      
      
      

INSERT INTO ${jdbc.ihs.schema}.ABSD_OWNA (OWCODE,RBNMSTYL_NAMSTYL_1)
VALUES(1000191,'CompanyName1'),
      (1000192,'CompanyName2'),
      (1000193,'CompanyName3'),
      (1000194,'CompanyName4'),
      (1000195,'CompanyName5'),
      (1000211,'CompanyName6'),
      (1000212,'CompanyName7'),
      (1000213,'CompanyName8'),
      (1000214,'CompanyName9'),
      (1000215,'CompanyName10'),
      (1000331,'CompanyName11'),
      (1000332,'CompanyName12'),
      (1000333,'CompanyName13'),
      (1000334,'CompanyName14'),
      (1000335,'CompanyName15'),
      (1000451,'CompanyName16'),
      (1000452,'CompanyName17'),
      (1000453,'CompanyName18');  
      
      
      
INSERT INTO ${jdbc.ihs.schema}.ABSD_OWIN (OWCODE,CNTRY, TOWN, SRFB_1, SRFB_2, SRFB_3, STREET, STNO, PRE_POSTCODE, POST_POSTCODE,TELPHN,FAX)
VALUES(1000191,'CT1','TWN1', 'ADDR LINE 1','ADDR LINE 2','ADDR LINE 3', 'ST1','STNO1','PRE POSTCODE1','POST POSTCODE1','7869879876','237982322');


INSERT INTO  ${jdbc.ihs.schema}.ABSD_CBTO2 (`KEY` ,	CNTRY ,	TOWN , STYL1,STYL2,	STYL3)
VALUES ('ST1','CT1','TWN1','STREETNAME','STREETNAME2','STREETNAME3');
      

INSERT INTO ${jdbc.ihs.schema}.ABSD_CBTO1 (CNTRY, TOWN, TNS1, TNS2)
 VALUES ('CT1','TWN1', 'TOWNNAME1','TOWNNAME2');



INSERT INTO ${jdbc.ihs.schema}.ABSD_CBCY (`KEY`, GNST)
 VALUES ('CT1','Country1 Full Name');
 
 
INSERT INTO ${jdbc.ihs.schema}.ABSD_PERSONNEL (PERSONNEL_ID, OWCODE, INITIALS, SURNAME, FULL_NAME, JOB_POSITION, MOBILE_TELEPHONE, AOH_TELEPHONE, EMAIL)
VALUES (1, '3000107', 'Mr.', 'Mock', 'Mock', 'Lead', '9638527410', '7891234560', 'apple.id@hotmail.com'); 

INSERT INTO  ${jdbc.ihs.schema}.ABSD_OWCO (OWCODE,OWSEQN,`TYPE`,SEQN,CNUM1,CNUM2,CNUM3,CNUM4,CNUM5,CNUM6) 
values ('1000191', null, 'E','01', 'email@lr.com',null,null,null,null,null);

