DELIMITER $$
DROP PROCEDURE IF EXISTS ${jdbc.mast.schema}.add_display_orders$$
CREATE PROCEDURE ${jdbc.mast.schema}.add_display_orders()
BEGIN
    DECLARE Parent Int;
    DECLARE Child Int;
    DECLARE fetch_status BOOLEAN DEFAULT TRUE;
    DECLARE exit_flag INT DEFAULT 0;
    DECLARE DisplayOrder Int;
    DECLARE ParentCursor CURSOR FOR (
        SELECT from_item_id FROM ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship
        WHERE relationship_type_id = 1
        group by from_item_id);
    DECLARE ChildCursor CURSOR FOR (
        SELECT i.id FROM ${jdbc.mast.schema}.MAST_ASSET_AssetItem i
        JOIN ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship ir ON i.id = ir.to_item_id
        WHERE ir.from_item_id = Parent);

    OPEN ParentCursor;
    BEGIN
        DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
        parent_loop : LOOP
            FETCH ParentCursor INTO Parent;
            IF exit_flag THEN
                LEAVE parent_loop;
            END IF;
            SET DisplayOrder = 0;
            OPEN ChildCursor;
            BEGIN
                DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET fetch_status = false;
                child_loop : LOOP
                    FETCH ChildCursor INTO Child;
                    IF fetch_status = true THEN
                        UPDATE ${jdbc.mast.schema}.MAST_ASSET_VersionedAssetItem SET display_order = DisplayOrder where id = Child;
                        SET DisplayOrder = DisplayOrder + 1;
                        ELSE LEAVE child_loop;
                    END IF;
                END LOOP child_loop;
            END;
            CLOSE ChildCursor;
            SET fetch_status = true;
        END LOOP parent_loop;
        CLOSE ParentCursor;

        SET SQL_SAFE_UPDATES = 0;
        UPDATE ${jdbc.mast.schema}.MAST_ASSET_VersionedAssetItem SET display_order = 0 WHERE display_order IS NULL;
        SET SQL_SAFE_UPDATES = 1;

    END;
END$$
DELIMITER ;

CALL ${jdbc.mast.schema}.add_display_orders();
