DROP PROCEDURE IF EXISTS copy_asset_model;
DROP PROCEDURE IF EXISTS duplicate_asset;
DROP PROCEDURE IF EXISTS duplicate_asset_multiple_times;

DELIMITER //
CREATE PROCEDURE copy_asset_model (from_asset_id BIGINT, to_asset_id BIGINT)
BEGIN

-- Procedure to copy the asset model from one asset to another.
-- Use by CALL copy_asset_model(from_asset, to_asset);

-- Error handler
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
ROLLBACK;
SELECT 'An error has occurred in copy_asset_model, operation rollbacked and the stored procedure was terminated';
END;

-- Record the ids of the items that are selected. This cannot be a temporary table as it needs to be used twice in the same query and restrictions apply to temporary tables.
CREATE TABLE selected_items (id BIGINT, INDEX id_index (id));
INSERT INTO selected_items SELECT id FROM ${jdbc.mast.schema}.mast_asset_item WHERE asset_id=from_asset_id;

-- Create teporary tables for items item relationships and attributes. This is where the updates will be done.
CREATE TEMPORARY TABLE item_temp
SELECT * FROM ${jdbc.mast.schema}.mast_asset_item WHERE asset_id=from_asset_id;

CREATE TEMPORARY TABLE item_relationship_temp
SELECT DISTINCT ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship.* FROM ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship JOIN selected_items ON ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship.from_item_id = selected_items.id OR ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship.to_item_id = selected_items.id;

CREATE TEMPORARY TABLE attribute_temp
SELECT ${jdbc.mast.schema}.mast_asset_attribute.* FROM ${jdbc.mast.schema}.mast_asset_attribute JOIN selected_items ON ${jdbc.mast.schema}.mast_asset_attribute.item_id = selected_items.id;

SET @MAX_ITEM_ID = (SELECT max(id) FROM ${jdbc.mast.schema}.mast_asset_item);
SET @MAX_ITEM_RELATIONSHIP_ID = (SELECT max(id) FROM ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship);
SET @MAX_ATTRIBUTE_ID = (SELECT max(id) FROM ${jdbc.mast.schema}.mast_asset_attribute);

-- Allow bulk updates.
SET SQL_SAFE_UPDATES = 0;

-- Update asset and set new id and parent ids that correspont to the correct newly created items.
UPDATE item_temp SET id=id + 1 + @MAX_ITEM_ID;
UPDATE item_temp SET asset_id=to_asset_id;

-- Update all the new item relationships to the ids of the newly created items.
UPDATE item_relationship_temp SET id=id + 1 + @MAX_ITEM_RELATIONSHIP_ID;
UPDATE item_relationship_temp SET from_item_id=from_item_id + 1 + @MAX_ITEM_ID;
UPDATE item_relationship_temp SET to_item_id=to_item_id + 1 + @MAX_ITEM_ID;

-- Update new attributes to the ids to the newly created items.
UPDATE attribute_temp SET id=id + 1 + @MAX_ATTRIBUTE_ID;
UPDATE attribute_temp SET item_id=item_id + 1 + @MAX_ITEM_ID;

-- Go back to safe mode.
SET SQL_SAFE_UPDATES = 1;

-- Commit the new data back into the correspoding tables.
INSERT INTO ${jdbc.mast.schema}.mast_asset_item SELECT * FROM item_temp;
INSERT INTO ${jdbc.mast.schema}.MAST_ASSET_AssetItemRelationship SELECT * FROM item_relationship_temp;
INSERT INTO ${jdbc.mast.schema}.mast_asset_attribute  SELECT * FROM attribute_temp;

-- Drop the temporary tables.
DROP TABLE selected_items;
DROP TABLE item_temp;
DROP TABLE item_relationship_temp;
DROP TABLE attribute_temp;
END//

CREATE PROCEDURE duplicate_asset (from_asset_id BIGINT)
BEGIN

-- Procedue to make a copy of a given asset.
-- Use by CALL duplicate_asset(from_asset);

-- Error handler
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
ROLLBACK;
SELECT 'An error has occurred in duplicate_asset, operation rollbacked and the stored procedure was terminated';
END;

START TRANSACTION;
-- Store the new id and imo number
SET @NEW_ASSET_ID = (SELECT MAX(id) FROM ${jdbc.mast.schema}.mast_asset_asset) + 1;
SET @NEW_IMO_NUMBER =(SELECT MAX(LRNO) FROM ihs_db.ABSD_SHIP_SEARCH) + 1;

-- Copy the asset and ihs table
CREATE TEMPORARY TABLE asset_temp
SELECT * FROM ${jdbc.mast.schema}.mast_asset_asset WHERE id=from_asset_id;

-- This has a limit of one to stop duplictaes as there is no constaint in the table itself.
CREATE TEMPORARY TABLE imo_temp
SELECT * FROM ihs_db.ABSD_SHIP_SEARCH WHERE LRNO = (SELECT imo_number FROM ${jdbc.mast.schema}.mast_asset_asset WHERE id=from_asset_id) LIMIT 1;

-- Allow bulk updates.
SET SQL_SAFE_UPDATES = 0;

-- Set new asset id and imo and set the new imo in the ihs database.
UPDATE asset_temp SET id = @NEW_ASSET_ID;
UPDATE asset_temp SET imo_number = @NEW_IMO_NUMBER;
UPDATE imo_temp SET LRNO = @NEW_IMO_NUMBER;

-- Go back to safe mode.
SET SQL_SAFE_UPDATES = 1;

-- Commit the new data back into the correspoding tables.
INSERT INTO ihs_db.ABSD_SHIP_SEARCH SELECT * FROM imo_temp;
INSERT INTO ${jdbc.mast.schema}.mast_asset_asset SELECT * FROM asset_temp;

-- Drop the temporary tables.
DROP TABLE asset_temp;
DROP TABLE imo_temp;

CALL copy_asset_model(from_asset_id, @NEW_ASSET_ID);
COMMIT;
END//

CREATE PROCEDURE duplicate_asset_multiple_times (from_asset_id BIGINT, number_of_duplicates BIGINT)
BEGIN

-- Procedure to loop copy asset to make multiple copies.
-- Use by CALL duplicate_asset_multiple_times(from_asset, number_of_duplicates);

-- Error handler
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
ROLLBACK;
SELECT 'An error has occurred in duplicate_asset_multiple_times, operation rollbacked and the stored procedure was terminated';
END;

SET @i =0;
WHILE @i < number_of_duplicates DO
  SET @i = @i + 1;
  CALL duplicate_asset(from_asset_id);
END WHILE;

END//

DELIMITER ;
