out=$1

echo inserting test data from sql
mysql -u${jdbc.mast.username} -p${jdbc.mast.password} < insert_test_data.sql || exit 1

mysql -u${jdbc.ihs.username} -p${jdbc.ihs.password} < insert_test_data_ihs.sql || exit 1

echo changing line endings
#dos2unix csvs/*

echo inserting test data from csvs
#mysql -u${jdbc.mast.username} -p${jdbc.mast.password} < test_data_from_csvs.sql || exit 1

echo adding display orders to items
mysql -u${jdbc.mast.username} -p${jdbc.mast.password} < add_display_order_procedure.sql || exit 1

echo dumping test data to outfile
mysqldump -u${jdbc.mast.username} -p${jdbc.mast.password} ${jdbc.mast.schema} > $out || exit 1

echo adding explicit db name to outfile
sed -i 's/ `mast_/ `${jdbc.mast.schema}`.`mast_/' $out || exit 1
sed -i 's/ `MAST_/ `${jdbc.mast.schema}`.`MAST_/' $out || exit 1

echo making outfile more readable
sed -i 's/),(/),\r\n(/g' $out || exit 1

echo make outfile writable again
chmod +w $out
