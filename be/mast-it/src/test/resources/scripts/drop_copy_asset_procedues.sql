DROP PROCEDURE IF EXISTS copy_asset_model;
DROP PROCEDURE IF EXISTS duplicate_asset;
DROP PROCEDURE IF EXISTS duplicate_asset_multiple_times;