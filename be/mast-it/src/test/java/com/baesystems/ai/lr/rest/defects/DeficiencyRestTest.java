package com.baesystems.ai.lr.rest.defects;

import static java.lang.String.format;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

import org.dbunit.DatabaseUnitException;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.DeficiencyPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class DeficiencyRestTest extends BaseRestTest
{
    private static final String ASSET_DEFICIENCY = urlBuilder(BASE_PATH, "/asset/%d/deficiency");
    private static final String DEFICIENCY_QUERY = urlBuilder(ASSET_DEFICIENCY, "/query");
    private static final String JOB_WIP_DEFICIENCY = urlBuilder(BASE_PATH, "/job/%d/wip-deficiency");
    private static final String WIP_DEFICIENCY_QUERY = urlBuilder(JOB_WIP_DEFICIENCY, "/query");

    private static final Long ASSET_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long DEFICIENCY_1_ID = 10L;
    private static final Long WIP_DEFICIENCY_1_ID = 12L;
    private static final Long STATUS_1_ID = 1L;

    private static final Integer QUERY_TEST_SIZE = 1;

    private DeficiencyPage deficiencyPage;
    private DeficiencyPage wipDeficiencyPage;

    @DataPoints("NON_WIP_QUERIES")
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints("WIP_QUERIES")
    public static final QueryAndResult[] WIP_QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;

        //for now there are no search options, so a query will just return all deficiencies against an asset
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_1_ID), new Long[]{DEFICIENCY_1_ID});

        index = 0;

        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_1_ID), new Long[]{WIP_DEFICIENCY_1_ID});
    }

    private void initialiseDeficiencyPage(final Long... ids)
    {
        this.deficiencyPage = new DeficiencyPage();

        DeficiencyDto tempDeficiencyDto;
        for (final Long id : ids)
        {
            try
            {
                tempDeficiencyDto = DataUtils.getDeficiency(id);
                this.deficiencyPage.getContent().add(tempDeficiencyDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    private void initialiseWipDeficiencyPage(final Long... ids)
    {
        this.wipDeficiencyPage = new DeficiencyPage();

        DeficiencyDto tempDeficiencyDto;
        for (final Long id : ids)
        {
            try
            {
                tempDeficiencyDto = DataUtils.getWipDeficiency(id);
                this.wipDeficiencyPage.getContent().add(tempDeficiencyDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Test /asset/x/deficiency/query with various parameters
     */
    @Theory
    public final void queries(@FromDataPoints("NON_WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseDeficiencyPage(queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final DeficiencyPage results = TestUtils.postResponse(format(DEFICIENCY_QUERY, ASSET_1_ID), query, HttpStatus.OK, DeficiencyPage.class);

        TestUtils.compareFields(results, this.deficiencyPage, true);
    }

    /**
     * Test /job/x/wip-deficiency/query with various parameters
     */
    @Theory
    public final void wipQueries(@FromDataPoints("WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWipDeficiencyPage(queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final DeficiencyPage results = TestUtils.postResponse(format(WIP_DEFICIENCY_QUERY, JOB_1_ID), query, HttpStatus.OK, DeficiencyPage.class);

        TestUtils.compareFields(results, this.wipDeficiencyPage, true);
    }
}
