package com.baesystems.ai.lr.single_use;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.validation.utils.ValidationStringUtils;

/**
 * One off helper class to validate that all the attribute entries are of the correct type and structure
 **/

public class AssetAttributeValueTest
{
    private List<AttributeDto> attributes;
    private Map<Long, ReferenceDataDto> attributeDataTypeMap;
    private ValidationStringUtils validationStringUtils;

    private static final Logger LOGGER = LoggerFactory.getLogger(AssetAttributeValueTest.class);

    @Before
    public void setUp() throws DatabaseUnitException
    {
        try
        {
            DataUtils.loadTestData();
        }
        catch (ClassNotFoundException | SQLException | InstantiationException | IOException | IllegalAccessException e)
        {
            e.printStackTrace();
        }
        attributeDataTypeMap = new HashMap<>();
        validationStringUtils = new ValidationStringUtils();
        attributes = DataUtils.getAllAttributes();
        final List<ReferenceDataDto> attributeDataTypeList = DataUtils.getAttributeDataTypes();
        for (final ReferenceDataDto attributeDataType : attributeDataTypeList)
        {
            attributeDataTypeMap.put(attributeDataType.getId(), attributeDataType);
        }
    }

    @Test
    public void testValidateAttributeValues() throws DatabaseUnitException
    {
        boolean failed = false;
        for (final AttributeDto attribute : attributes)
        {
            final List<String> results = new ArrayList<>();
            final AttributeTypeDto type = getAttributeType(attribute.getAttributeType().getId());
            final Long valueTypeId = type.getValueType().getId();
            final ReferenceDataDto referenceTypeValue = attributeDataTypeMap.get(valueTypeId);
            validationStringUtils.validateString(attribute.getValue(), referenceTypeValue.getDescription(), attribute.getValue(), results);
            if (!results.isEmpty())
            {
                LOGGER.info("Bad attribute: " + attribute.getId());
                LOGGER.info(results.get(0));
                failed = true;
            }
        }
        Assert.assertFalse(failed);
    }

    private AttributeTypeDto getAttributeType(final Long typeId) throws DatabaseUnitException
    {
        return DataUtils.getAttributeType(typeId);
    }
}
