package com.baesystems.ai.lr.rest.assets;

import static com.baesystems.ai.lr.utils.TestUtils.parseDate;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.AssetPage;
import com.baesystems.ai.lr.pages.AttributePage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class AssetRestTest extends BaseRestTest
{
    /**
     * API Rest paths
     */
    public static final String ASSET_QUERY = new StringBuffer().append(BASE_ASSET).append("/query").toString();
    public static final String ASSET_ABSTRACT_QUERY = new StringBuffer().append(BASE_ASSET).append("/abstract-query").toString();
    public static final String BASE_ITEM_TYPE = new StringBuffer().append(BASE_PATH).append("/item-type").toString();
    public static final String ITEM_TYPE = new StringBuffer().append(BASE_ITEM_TYPE).append("/%d").toString();
    public static final String CASES_FOR_ASSET = new StringBuffer(SPECIFIC_ASSET).append("/case").toString();

    /**
     * Asset search: These are set to give a clearer idea of which assets are expected in each case and avoid warnings
     * about magic numbers
     */
    private static final Long INVALID_ASSET = 99999L;
    private static final Long NEGATIVE = -1L;
    private static final Long ZERO = 0L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long ASSET_3_ID = 3L;
    private static final Long ASSET_5_ID = 5L;
    private static final Long ASSET_6_ID = 6L;
    private static final Long ASSET_7_ID = 7L;
    private static final Long ASSET_8_ID = 8L;
    private static final Long ASSET_45_ID = 45L;
    private static final Long ASSET_205_ID = 205L;
    private static final Long ASSET_1_IMO = 1000019L;
    private static final Long ITEM_1324_ID = 1324L;
    private static final Long ATTRIBUTE_7717_ID = 7717L;

    private static final Long ATTRIBUTE_8792_ID = 8792L;

    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final Long SEVEN = 7L;
    private static final Long ONE_HUNDRED = 100L;
    private static final Double FIVE = 5D;
    private static final Double TWO_THOUSAND = 2000D;
    private static final Long ASSET_MODEL_1_ITEM_1_ID = 1L;
    private static final boolean ASSET_MODEL_1_ITEM_2_REVIEWED = true;
    private static final Long ATTRIBUTE_4 = 4L;
    private static final Long ATTRIBUTE_5 = 5L;
    private static final Long ATTRIBUTE_6 = 6L;
    private static final Long ATTRIBUTE_7 = 7L;
    private static final Long ATTRIBUTE_8 = 8L;
    private static final Long ITEM_91_ID = 91L;
    private static final Long ITEM_1140_ID = 1140L;
    private static final Long ITEM_1156_ID = 1156L;

    private static final String YARD_NUMBER_1 = "SomeYard1";
    private static final String YARD_NUMBER_2 = "DiffYard2";
    private static final String YARD_NUMBER_WITH_15_CHARS = "123456789012345";
    private static final String BUILDER_WITH_100_CHARS = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    private static final Long ATTRIBUTE_TYPE_ID_FOR_MANDATORY_NON_MULTIPLE_TYPE = 366L;
    private static final Long ATTRIBUTE_TYPE_ID_FOR_MULTIPLE = 367L;

    private static final Long PARTY_ROLE_ID_DISPONENT_OWNER = 13L;

    private AssetDto asset;
    private AssetPage assetPage;
    private AttributePage attributePage;
    private AssetModelDto assetModel;
    private AttributeDto attribute;

    @DataPoints
    public static final String[] BAD_REQUESTS = {// Needs to be public for Theory to work.
                                                 String.format(SPECIFIC_ASSET, ZERO.longValue()),
                                                 String.format(ASSET_MODEL, ZERO.longValue()),
                                                 BASE_ASSET + "/SomeLetters",
                                                 BASE_ASSET_MODEL + "/SomeLetters",
                                                 String.format(SPECIFIC_ASSET, NEGATIVE.longValue()),
                                                 String.format(ASSET_MODEL, NEGATIVE.longValue()),
                                                 String.format(SPECIFIC_ASSET, ASSET_1_ID.longValue()) + "&page=0&sort=someField",
                                                 String.format(CASES_FOR_ASSET, ZERO.longValue()),
                                                 String.format(CASES_FOR_ASSET, NEGATIVE.longValue()),
                                                 BASE_ASSET + "/SomeLetters" + "/case"};

    private static final int QUERY_TEST_SIZE = 18;

    @DataPoints
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        QUERIES[index] = new QueryAndResult("search", "*LA*", new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("lifecycleStatusId", Arrays.asList(new Long[]{THREE}), new Long[]{ASSET_1_ID, ASSET_2_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("imoNumber", Arrays.asList(new Long[]{ASSET_1_IMO}), new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("assetTypeId", Arrays.asList(new Long[]{TWO}), new Long[]{ASSET_2_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("yardNumber", "Yard1", new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("buildDateMin", parseDate("2010-01-01"),
                new Long[]{ASSET_1_ID, ASSET_2_ID, ASSET_3_ID, ASSET_5_ID, ASSET_6_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("buildDateMax", parseDate("2010-01-01"), new Long[]{ASSET_7_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("classStatusId", Arrays.asList(new Long[]{SEVEN}), new Long[]{ASSET_7_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("flagStateId", Arrays.asList(new Long[]{ONE_HUNDRED}), new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("grossTonnageMin", TWO_THOUSAND, new Long[]{ASSET_3_ID, ASSET_5_ID, ASSET_6_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("grossTonnageMax", FIVE, new Long[]{ASSET_7_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("partyId", Arrays.asList(new Long[]{ONE}), new Long[]{ASSET_1_ID});
        ++index;

        // todo Can we get rid of this, needlessly complicated
        // QUERIES[index] = new QueryAndResult("partyFunctionTypeId", Arrays.asList(new Long[]{ONE}), new
        // Long[]{ASSET_1_ID});
        // ++index;

        QUERIES[index] = new QueryAndResult("partyRoleId", Arrays.asList(new Long[]{PARTY_ROLE_ID_DISPONENT_OWNER}), new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("builder", "Ership Internacional SA", new Long[]{ASSET_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("idList", Arrays.asList(new Long[]{ONE, TWO}), new Long[]{ASSET_1_ID, ASSET_2_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("categoryId", Arrays.asList(new Long[]{TWO}), new Long[]{ASSET_7_ID, ASSET_205_ID});
        ++index;
        QUERIES[index] = new QueryAndResult(new String[]{"idList", "assetTypeId"},
                new Object[]{Arrays.asList(new Long[]{ONE, TWO}), Arrays.asList(new Long[]{TWO})},
                new Long[]{ASSET_2_ID});
        ++index;
        QUERIES[index] = new QueryAndResult(new String[]{"grossTonnageMin", "grossTonnageMax"}, new Object[]{FIVE, TWO_THOUSAND},
                new Long[]{ASSET_1_ID, ASSET_2_ID, ASSET_3_ID, ASSET_5_ID, ASSET_6_ID});
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.asset = DataUtils.getAsset(ASSET_2_ID);
        // validAsset = DataUtils.getAsset(ASSET_8_ID);

        this.assetModel = new AssetModelDto();
        this.assetModel.setItems(new ArrayList<ItemDto>());
        this.assetModel.getItems().add(new ItemDto());
        this.assetModel.getItems().get(0).setId(ASSET_MODEL_1_ITEM_1_ID);
        this.assetModel.getItems().get(0).setReviewed(ASSET_MODEL_1_ITEM_2_REVIEWED);

        // initialiseAssetPage(ASSET_1_ID);

        this.attribute = DataUtils.getAttribute(ATTRIBUTE_4);
        //
        // asset = DataUtils.getAsset(ASSET_2_ID);
        // validAsset = DataUtils.getAsset(ASSET_8_ID);
        //
        // assetModel = new AssetModelDto();
        // assetModel.setItems(new ArrayList<ItemDto>());
        // assetModel.getItems().add(new ItemDto());
        // assetModel.getItems().get(0).setId(ASSET_MODEL_1_ITEM_1_ID);
        // assetModel.getItems().get(0).setReviewed(ASSET_MODEL_1_ITEM_2_REVIEWED);
        //
        // initialiseAssetPage(ASSET_1_ID);
        //
        // attribute = DataUtils.getAttribute(ATTRIBUTE_4);
    }

    private void initialiseAssetPage(final Long... ids)
    {
        this.assetPage = new AssetPage();

        AssetDto tempAsset;
        for (final Long id : ids)
        {
            try
            {
                tempAsset = DataUtils.getAsset(id);
                this.assetPage.getContent().add(tempAsset);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    private void initialiseAttributePage(final Long... ids)
    {
        this.attributePage = new AttributePage();
        AttributeDto tempAsset;
        for (final Long id : ids)
        {
            try
            {
                tempAsset = DataUtils.getAttribute(id);
                this.attributePage.getContent().add(tempAsset);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    private AssetLightDto createAssetLightDto()
    {
        final AssetLightDto assetLightDto = new AssetLightDto();

        assetLightDto.setId(ASSET_45_ID);
        assetLightDto.setName(this.asset.getName());
        assetLightDto.setIsLead(this.asset.getIsLead());
        assetLightDto.setLeadImo(this.asset.getLeadImo());
        assetLightDto.setClassNotation("changing class notation");
        assetLightDto.setBuildDate(this.asset.getBuildDate());
        assetLightDto.setKeelLayingDate(this.asset.getKeelLayingDate());
        assetLightDto.setGrossTonnage(this.asset.getGrossTonnage());
        assetLightDto.setAssetType(this.asset.getAssetType());
        assetLightDto.setAssetCategory(this.asset.getAssetCategory());
        assetLightDto.setClassStatus(this.asset.getClassStatus());
        assetLightDto.setBuilder(this.asset.getBuilder());
        assetLightDto.setYardNumber(this.asset.getYardNumber());
        assetLightDto.setIhsAsset(this.asset.getIhsAsset());
        assetLightDto.setRuleSet(this.asset.getRuleSet());
        assetLightDto.setPreviousRuleSet(this.asset.getPreviousRuleSet());
        assetLightDto.setProductRuleSet(this.asset.getProductRuleSet());
        assetLightDto.setHarmonisationDate(this.asset.getHarmonisationDate());
        assetLightDto.setHullIndicator(this.asset.getHullIndicator());
        assetLightDto.setRegisteredPort(this.asset.getRegisteredPort());
        assetLightDto.setAssetLifecycleStatus(this.asset.getAssetLifecycleStatus());
        assetLightDto.setCoClassificationSociety(this.asset.getCoClassificationSociety());
        assetLightDto.setStalenessHash(this.asset.getStalenessHash());
        return assetLightDto;
    }

    /**
     * Test that a valid GET request for ../asset results in a positive response for exact search. Expecting a paginated
     * list of JSONs containing Assets 1-4.
     */
    @Test
    public final void testGetAssetList()
    {
        this.initialiseAssetPage(ASSET_1_ID, ASSET_2_ID, ASSET_3_ID, ASSET_5_ID, ASSET_6_ID, ASSET_7_ID);
        TestUtils.listItemsFound(BASE_ASSET, this.assetPage, true, false);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id} results in a positive response. Expecting a JSON containing
     * asset 1.
     */
    @Test
    public final void assetUriAssetFound()
    {
        TestUtils.singleItemFound(format(SPECIFIC_ASSET, ASSET_2_ID.intValue()), this.asset, true);
    }

    /**
     * Test that a valid GET request for ../asset-model/{asset_model_id} results in a positive response. Expecting a
     * JSON containing asset model 1.
     * <p>
     * This API is being used on the Crediting page.
     */
    @Test
    public final void assetModelUriAssetModelFound()
    {
        TestUtils.singleItemFound(format(ASSET_MODEL, ASSET_2_ID.intValue()), this.assetModel, true);
    }

    /**
     * Test that a valid GET request for ../asset-model/{asset_model_id} results in a positive response for exact
     * search. Expecting a error message because no asset model was found.
     * <p>
     * This API is being used on the Crediting page.
     */
    @Test
    public final void assetModelUriAssetModelNotFound()
    {
        TestUtils.singleItemNotFound(format(ASSET_MODEL, INVALID_ASSET));
    }

    @Test
    public final void attributeGetForItem()
    {
        this.initialiseAttributePage(ATTRIBUTE_4, ATTRIBUTE_5, ATTRIBUTE_6, ATTRIBUTE_7);
        TestUtils.listItemsFound(format(BASE_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID), this.attributePage, false, false);
    }

    @Test
    public final void createAsset()
    {
        final AssetLightDto existingAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID), HttpStatus.OK)
                .as(AssetLightDto.class);

        existingAsset.setId(null);
        existingAsset.setBuilder(existingAsset.getBuilder().concat(Integer.toString(new Random().nextInt())));
        existingAsset.setIhsAsset(null);

        CollectionUtils.nullSafeStream(existingAsset.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });
        CollectionUtils.nullSafeStream(existingAsset.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));

        final AssetDto newAsset = TestUtils.postResponse(AssetRestTest.BASE_ASSET, existingAsset, HttpStatus.OK).as(AssetDto.class);
        assertNotNull(newAsset);
        assertNotNull(newAsset.getId());
        assertNotNull(newAsset.getAssetModel().getItems());
        assertEquals(1, newAsset.getAssetModel().getItems().size());
    }

    @Test
    public final void createAssetWithBuilderAndYardNumberWithMaximumSizedValues()
    {
        final AssetLightDto existingAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID), HttpStatus.OK)
                .as(AssetLightDto.class);

        existingAsset.setId(null);
        existingAsset.setIhsAsset(null);
        existingAsset.setYardNumber(YARD_NUMBER_WITH_15_CHARS);
        existingAsset.setBuilder(BUILDER_WITH_100_CHARS);

        CollectionUtils.nullSafeStream(existingAsset.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });
        CollectionUtils.nullSafeStream(existingAsset.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));

        final AssetDto newAsset = TestUtils.postResponse(AssetRestTest.BASE_ASSET, existingAsset, HttpStatus.OK).as(AssetDto.class);
        assertNotNull(newAsset);
        assertNotNull(newAsset.getId());
        assertNotNull(newAsset.getAssetModel().getItems());
        assertEquals(1, newAsset.getAssetModel().getItems().size());
    }

    @Test
    public final void createAssetWithoutModelNoBuildDate()
    {
        final AssetLightDto existingAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID), HttpStatus.OK)
                .as(AssetLightDto.class);

        existingAsset.setId(null);
        existingAsset.setIhsAsset(null);
        existingAsset.setBuildDate(null);

        CollectionUtils.nullSafeStream(existingAsset.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });
        CollectionUtils.nullSafeStream(existingAsset.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));

        TestUtils.postResponse(AssetRestTest.BASE_ASSET, existingAsset, HttpStatus.BAD_REQUEST);
    }

    /**
     * Theory that runs all of the queries specified in the list of {@Link QueryAndResult} objects at the to of this
     * class. These objects are designed so that only one argument needs to be passed to this method. They contain a
     * field name (or list of field names) and its corresponding value (or list of values) if lists are given it is
     * assumed that the values occupy the same positions in their list as the name of the field they are to be added to
     * does in the field name list and that these list are the same length.
     * <p>
     * The method then creates a new AssetQuery object for each run and populates the fields stipulated vie reflection.
     * Because some of the fields can be lists it is not possible to call the setters so the field are set as accessible
     * and populated directly.
     * <p>
     * The last field in the argument object is for a list of ids for the expected assets. The method used these to
     * populate the expected page and compares this with the result of the query.
     */
    @Theory
    public final void queries(final QueryAndResult queryAndResult) throws NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.initialiseAssetPage(queryAndResult.getResult());

        final AssetQueryDto query = new AssetQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final AssetPage results = TestUtils.postResponse(ASSET_QUERY, query, HttpStatus.OK, AssetPage.class);

        System.out.println(queryAndResult.getQueryField());
        System.out.println(queryAndResult.getQueryValue());

        TestUtils.compareFields(results, this.assetPage, true);
    }

    /**
     * Test query validation. Expecting bad request error
     */
    @Test
    public final void queryValidationFailsDates()
    {
        final AssetQueryDto query = new AssetQueryDto();

        query.setBuildDateMax(parseDate("2000-01-01"));
        query.setBuildDateMin(parseDate("2001-01-01"));

        TestUtils.postResponse(ASSET_QUERY, query, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test query validation. Expecting bad request error
     */
    @Test
    public final void queryValidationFailsGrossTonnage()
    {
        final AssetQueryDto query = new AssetQueryDto();

        query.setGrossTonnageMax(FIVE);
        query.setGrossTonnageMin(TWO_THOUSAND);

        TestUtils.postResponse(ASSET_QUERY, query, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test abstract query. Query on services. Expecting 1, 2 and 3, but not 5 as its only service is in 2019.
     */
    @Test
    public final void queryOnServices()
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();

        query1.setJoinField("services");
        query1.setField("dueDate");
        query1.setValue("2018-12-31");
        query1.setRelationship(QueryRelationshipType.LE);

        query2.setJoinField("services");
        query2.setField("dueDate");
        query2.setValue("2008-01-01");
        query2.setRelationship(QueryRelationshipType.GE);

        query.setAnd(Arrays.asList(query1, query2));

        final AssetPage result = TestUtils.postResponse(ASSET_ABSTRACT_QUERY, query, HttpStatus.OK, AssetPage.class);

        assertEquals(THREE, result.getPagination().getTotalElements());
        assertEquals(ASSET_1_ID, result.getContent().get(0).getId());
        assertEquals(ASSET_2_ID, result.getContent().get(1).getId());
        assertEquals(ASSET_3_ID, result.getContent().get(2).getId());
    }

    /**
     * Test abstract query. Query on codicils. Expecting 1, 2 as they are are only ones that have codicils.
     */
    @Test
    public final void queryOnCodicils()
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();

        query1.setJoinField("codicils");
        query1.setField("dueDate");
        query1.setValue("2018-12-31");
        query1.setRelationship(QueryRelationshipType.LE);

        query2.setJoinField("codicils");
        query2.setField("dueDate");
        query2.setValue("2008-01-01");
        query2.setRelationship(QueryRelationshipType.GE);

        query.setAnd(Arrays.asList(query1, query2));

        final AssetPage result = TestUtils.postResponse(ASSET_ABSTRACT_QUERY, query, HttpStatus.OK, AssetPage.class);

        assertEquals(TWO, result.getPagination().getTotalElements());
        assertEquals(ASSET_1_ID, result.getContent().get(0).getId());
        assertEquals(ASSET_2_ID, result.getContent().get(1).getId());
    }

    /**
     * Test that a valid GET request for ../asset/{id}, ../asset..., ../asset-model/{id} and ../item-type/{id} results
     * in a positive response. Expecting an error for a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ihsAssetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/case results in a positive response. Expecting a not found
     * exception.
     */
    @Test
    public final void getCasesForAssetButAssetNotFound()
    {
        TestUtils.getResponse(format(CASES_FOR_ASSET, INVALID_ASSET), HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/case results in a positive response. Expecting an empty
     * list because no cases are associated with this asset.
     */
    @Test
    public final void getCasesForAssetReturnsEmptyListNoCasesFound()
    {
        final CaseDto[] cases = TestUtils.getResponse(format(CASES_FOR_ASSET, ASSET_8_ID), HttpStatus.OK).getBody().as(CaseDto[].class);
        assertTrue(cases.length == 0);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/case results in a positive response. Expecting an empty
     * list because no there are cases associated with this asset but they are not open.
     */
    @Test
    public final void getCasesForAssetReturnsEmptyListClosedCasesOnly()
    {
        final CaseDto[] cases = TestUtils.getResponse(format(CASES_FOR_ASSET, ASSET_7_ID), HttpStatus.OK).getBody().as(CaseDto[].class);
        assertTrue(cases.length == 0);
    }

    @Test
    public void getAttribute()
    {
        TestUtils.singleItemFound(String.format(SPECIFIC_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID, ATTRIBUTE_4), this.attribute, false);
    }

    @Test
    public void getAttributeNotFound()
    {
        TestUtils.singleItemNotFound(String.format(SPECIFIC_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID, ATTRIBUTE_8));
    }
}
