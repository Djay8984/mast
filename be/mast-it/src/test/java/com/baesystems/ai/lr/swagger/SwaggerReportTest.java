package com.baesystems.ai.lr.swagger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.security.rbac.authorisation.AntMatcher;
import com.baesystems.ai.lr.utils.TestUtils;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectWriter;

import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.properties.RefProperty;
import io.swagger.parser.SwaggerParser;
import io.swagger.util.Yaml;

@Category(IntegrationTest.class)
public class SwaggerReportTest extends BaseRestTest
{
    private static final Pattern TOKEN_FORMAT = Pattern.compile("\\{([^}]*)\\}");
    private static final int CSV_START_INDEX = 25;

    @Test
    public void testSwaggerUrl()
    {
        final String implFirstSwaggerLocation = "http://localhost:" + BaseRestTest.PORT_NUMBER + "/mast/webjars/swagger-ui/2.1.4/index.html";
        TestUtils.getResponse(implFirstSwaggerLocation, HttpStatus.OK).asString();
    }

    @Test
    public void validateThatImplementationFitsDesignSpecification() throws Exception
    {

        final String implFirstSwaggerLocation = "http://localhost:" + BaseRestTest.PORT_NUMBER + "/mast/api-docs/mastContext";

        System.setProperty("debugParser", "true");

        final SwaggerParser parser = new SwaggerParser();

        final String swaggerText = TestUtils.getResponse(implFirstSwaggerLocation, HttpStatus.OK).asString();
        final Swagger swaggerActual = parser.parse(swaggerText);
        final ObjectWriter objectWriter = Yaml.mapper().writer(new DefaultPrettyPrinter());
        objectWriter.writeValue(new File("api.yaml"), swaggerActual);

        generateApiList(swaggerActual);
        generateRbacRole(swaggerActual);

        validateResponses(swaggerActual);
    }

    private void validateResponses(final Swagger swaggerActual)
    {
        for (final Map.Entry<String, Path> entry : swaggerActual.getPaths().entrySet())
        {
            final String key = entry.getKey();
            final Path path = entry.getValue();

            for (final Operation operation : path.getOperations())
            {
                final Map<String, Response> responses = operation.getResponses();

                if (!responses.containsKey(HttpStatus.OK.value() + ""))
                {
                    throw new RuntimeException("missing response for: " + operation.getDescription() + entry.getKey());
                }

                final Response response = responses.get(HttpStatus.OK.value() + "");
                if (response.getDescription() == null || response.getDescription().length() == 0)
                {
                    assertNotNull(key + " response missing description", response.getDescription());
                }
            }

            if (path.getGet() != null)
            {
                validateGetResponse(key, path);
            }
        }
    }

    private void validateGetResponse(final String key, final Path path)
    {
        final Operation operation = path.getGet();
        final Map<String, Response> responses = operation.getResponses();
        final Response response = responses.get(HttpStatus.OK.value() + "");

        if (!"/ping".equals(key) && !"/spellcheck/dictionary".equals(key))
        {
            assertNotNull(key, response.getSchema());

            final String lastKey = key.lastIndexOf('/') >= 0 ? key.substring(key.lastIndexOf('/') + 1) : key;

            if (lastKey.endsWith("}")
                    || lastKey.endsWith("root-item")
                    || lastKey.endsWith("root-draft-item")
                    || lastKey.endsWith("milestone")
                    //|| lastKey.endsWith("checked-out")
                    || lastKey.endsWith("check-out")
                    || lastKey.endsWith("check-in")
                    || lastKey.endsWith("reference-data-version"))
            {
                // single-items
                final String type = response.getSchema().getType();
                assertEquals(key + " response expecting ref", "ref", type);
            }
            else
            {
                // lists of paginations
                final String type = response.getSchema().getType();
                if ("ref".equals(type))
                {
                    // We don't expect paths ending in these keys to return a page resource
                    final boolean requirePageResource = !(lastKey.startsWith("verify")
                            || lastKey.endsWith("-date")
                            || lastKey.endsWith("-data")
                            || lastKey.endsWith("count")
                            || lastKey.endsWith("current-user"));

                    if (requirePageResource)
                    {
                        final RefProperty property = (RefProperty) response.getSchema();
                        assertTrue(key + " expecting page resource", property.getSimpleRef().endsWith("PageResourceDto"));
                    }
                }
                else
                {
                    assertEquals(key + " response expecting array", "array", type);
                }
            }
        }
    }

    private void generateApiList(final Swagger swaggerActual) throws IOException
    {
        try (final FileWriter writer = new FileWriter("api.txt"))
        {
            for (final Map.Entry<String, Path> entry : swaggerActual.getPaths().entrySet())
            {
                final String url = entry.getKey();
                final Path path = entry.getValue();
                if (path.getDelete() != null)
                {
                    writer.write("DELETE:" + url + "\n");
                }

                if (path.getGet() != null)
                {
                    writer.write("GET:" + url + "\n");
                }

                if (path.getPut() != null)
                {
                    writer.write("PUT:" + url + "\n");
                }

                if (path.getPost() != null)
                {
                    writer.write("POST:" + url + "\n");
                }
            }
        }
    }

    private void generateRbacRole(final Swagger swaggerActual) throws IOException
    {
        int startIndex = CSV_START_INDEX;
        try (final FileWriter writer = new FileWriter("rbac_role.txt"))
        {

            writer.write("INSERT INTO ${jdbc.security.schema}.RBAC_ROLE (id, name, uri, method, enabled)" + "\n");
            writer.write("VALUES \n");

            writer.write(processPath("GET", "Base MAST URL", "/*", startIndex++));
            writer.write(processPath("GET", "Swagger DOCS API", "/api-docs", startIndex++));
            writer.write(processPath("GET", "HTML Jars", "/webjars/**", startIndex++));

            for (final Map.Entry<String, Path> entry : swaggerActual.getPaths().entrySet())
            {
                final String url = entry.getKey();
                final Path path = entry.getValue();

                if (path.getDelete() != null)
                {
                    writer.write(processPath("DELETE", path.getDelete().getSummary(), url, startIndex++));
                }
                if (path.getGet() != null)
                {
                    writer.write(processPath("GET", path.getGet().getSummary(), url, startIndex++));
                }
                if (path.getPut() != null)
                {
                    writer.write(processPath("PUT", path.getPut().getSummary(), url, startIndex++));
                }
                if (path.getPost() != null)
                {
                    writer.write(processPath("POST", path.getPost().getSummary(), url, startIndex++));
                }
            }
        }

        try (final FileWriter writer = new FileWriter("rbac_group.txt"))
        {
            final String tab = "       ";
            for (int index = 1; index < startIndex; index++)
            {
                writer.write(tab + "(16," + index + ",1)," + "\n");
            }
        }
    }

    private String processPath(final String method, final String description, final String url, final int index)
    {

        final String uri = new AntMatcher().toMatcherFormat(process(url));

        final String tab = "       ";
        return tab + String.format("(%s,'%s','%s','%s',1)", index, description.split(":")[0], uri, method) + ",\n";
    }

    public static String process(final String template)
    {
        final StringBuffer sb = new StringBuffer();
        final Matcher myMatcher = TOKEN_FORMAT.matcher(template);
        while (myMatcher.find())
        {
            myMatcher.group(1);
            myMatcher.appendReplacement(sb, "*");
        }
        myMatcher.appendTail(sb);
        return sb.toString();
    }
}
