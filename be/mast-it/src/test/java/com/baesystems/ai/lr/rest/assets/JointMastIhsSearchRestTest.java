package com.baesystems.ai.lr.rest.assets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.pages.MultiAssetPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
@SuppressWarnings({"PMD.AvoidDuplicateLiterals", "PMD.GodClass"})
public class JointMastIhsSearchRestTest extends BaseRestTest
{
    private enum AssetsFound
    {
        BOTH,
        MAST,
        IHS;
    }

    private enum JoinType
    {
        AND,
        OR;
    }

    private class ExpectedResult
    {
        private final Long imoNumber;

        private final AssetsFound expected;

        ExpectedResult(final Long imoNumber, final AssetsFound expected)
        {
            this.imoNumber = imoNumber;
            this.expected = expected;
        }

        public Long getImoNumber()
        {
            return this.imoNumber;
        }

        public AssetsFound getExpected()
        {
            return this.expected;
        }
    }

    private class QueryAndResult
    {
        private final AbstractQueryDto query;

        private final List<ExpectedResult> expected;

        private final String sort;

        QueryAndResult(final AbstractQueryDto query, final List<ExpectedResult> expected, final String sort)
        {
            this.query = query;
            this.expected = expected;
            this.sort = sort;
        }

        public AbstractQueryDto getQuery()
        {
            return this.query;
        }

        public List<ExpectedResult> getExpected()
        {
            return this.expected;
        }

        public String getSort()
        {
            return this.sort;
        }
    }

    private static AbstractQueryDto newQuery(final String field, final Object value, final QueryRelationshipType type)
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        query.setField(field);
        query.setValue(value);
        query.setRelationship(type);
        return query;
    }

    private static AbstractQueryDto newQuery(final String joinField, final String field, final Object value,
            final QueryRelationshipType type, final Boolean selectWhenFieldIsMissing)
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        query.setJoinField(joinField);
        query.setField(field);
        query.setValue(value);
        query.setRelationship(type);
        query.setSelectWhenFieldIsMissing(selectWhenFieldIsMissing);
        return query;
    }

    private static AbstractQueryDto newQuery(final String joinField, final String field, final Object value, final QueryRelationshipType type)
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        query.setJoinField(joinField);
        query.setField(field);
        query.setValue(value);
        query.setRelationship(type);
        return query;
    }

    private static AbstractQueryDto newQuery(final JoinType joinType, final String joinField, final Boolean selectWhenFieldIsMissing,
            final AbstractQueryDto... queries)
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        query.setJoinField(joinField);
        query.setSelectWhenFieldIsMissing(selectWhenFieldIsMissing);

        if (JoinType.AND.equals(joinType))
        {
            query.setAnd(Arrays.asList(queries));
        }
        else
        {
            query.setOr(Arrays.asList(queries));
        }

        return query;
    }

    private static final Long IMO_1000019 = 1000019L;
    private static final Long IMO_1000021 = 1000021L;
    private static final Long IMO_1000033 = 1000033L;
    private static final Long IMO_1000057 = 1000057L;
    private static final Long IMO_1000069 = 1000069L;
    private static final Long IMO_1000071 = 1000071L;
    private static final Long IMO_1000083 = 1000083L;
    private static final Long IMO_1000099 = 1000099L;
    private static final Long IMO_1000100 = 1000100L;
    private static final Long IMO_1000174 = 1000174L;
    private static final Long IMO_1000186 = 1000186L;
    private static final Long MAX_ORIGINAL_ID = 9999L;

    private static final int THREE = 3;
    private static final int ONE_HUNDRED = 100;

    private static final String ASSET_QUERY = new StringBuffer().append(BASE_ASSET).append("/mast-and-ihs-query").toString();
    private static final String ASSET_QUERY_WITH_SORTING = new StringBuffer().append(ASSET_QUERY).append("?sort=%s").toString();
    private static final String SORT_FAIL = "Sorting test failed on %s. %s is greater than %s.";

    private static final Map<String, String> MAST_NAME_MAP = new HashMap<String, String>();
    private static final Map<String, String> IHS_NAME_MAP = new HashMap<String, String>();

    private static final int QUERY_TEST_SIZE = 13;

    @DataPoints
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints
    public static final String[][] SORT_FIELDS = {{"publishedVersion.name"}, {"publishedVersion.buildDate"}, {"publishedVersion.yardNumber"},
                                                  {"publishedVersion.buildDate", "publishedVersion.name"}};

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        final AssetsFound both = AssetsFound.BOTH;
        final AssetsFound ihs = AssetsFound.IHS;
        final AssetsFound mast = AssetsFound.MAST;

        final JointMastIhsSearchRestTest thisClass = new JointMastIhsSearchRestTest();

        int index = 0;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.assetType.code", "W11A5TC", QueryRelationshipType.EQ),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000071, ihs)), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.flagState.flagCode", "5", QueryRelationshipType.EQ),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000186, ihs)), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.grossTonnage", ONE_HUNDRED, QueryRelationshipType.LE),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both),
                        thisClass.new ExpectedResult(IMO_1000069, both),
                        thisClass.new ExpectedResult(IMO_1000099, mast),
                        thisClass.new ExpectedResult(null, mast),
                        thisClass.new ExpectedResult(null, mast)),
                "publishedVersion.ihsAsset.id");
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.buildDate", "2016-01-01", QueryRelationshipType.GE),
                Arrays.asList(thisClass.new ExpectedResult(null, mast),
                        thisClass.new ExpectedResult(null, mast)),
                null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.classStatus.name", "NAVAL VESSELS", QueryRelationshipType.EQ),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000174, ihs)), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion", "id", THREE, QueryRelationshipType.LT, false),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both),
                        thisClass.new ExpectedResult(IMO_1000021, both)),
                null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.builderInfo", "id", 1, QueryRelationshipType.EQ, false),
                new ArrayList<ExpectedResult>(), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.yardNumber", "YARD-1", QueryRelationshipType.EQ),
                Arrays.asList(thisClass.new ExpectedResult(null, mast)), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.assetType.code", "X11A2YH", QueryRelationshipType.EQ),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000083, ihs),
                        thisClass.new ExpectedResult(null, mast),
                        thisClass.new ExpectedResult(null, mast)),
                null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("codicils", "dueDate", "2016-12-12", QueryRelationshipType.EQ, false),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both),
                        thisClass.new ExpectedResult(IMO_1000021, both)),
                null);
        QUERIES[index] = thisClass.new QueryAndResult(newQuery(JoinType.AND, "services", false,
                newQuery("dueDate", "2019-01-01", QueryRelationshipType.LT),
                newQuery("dueDate", "2017-12-31", QueryRelationshipType.GT)),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000033, both)),
                null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.name", "*lan Tank", QueryRelationshipType.LIKE),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both)), null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(newQuery("publishedVersion.imoNumber", "*9", QueryRelationshipType.LIKE),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both),
                        thisClass.new ExpectedResult(IMO_1000069, both),
                        thisClass.new ExpectedResult(IMO_1000099, mast)),
                null);
        ++index;
        QUERIES[index] = thisClass.new QueryAndResult(
                newQuery("publishedVersion.ihsAsset.id", new Long[]{IMO_1000019, IMO_1000100}, QueryRelationshipType.IN),
                Arrays.asList(thisClass.new ExpectedResult(IMO_1000019, both),
                        thisClass.new ExpectedResult(IMO_1000100, ihs)),
                null);
        ++index;

        MAST_NAME_MAP.put("publishedVersion.name", "mastAsset.name");
        MAST_NAME_MAP.put("publishedVersion.buildDate", "mastAsset.buildDate");
        MAST_NAME_MAP.put("publishedVersion.yardNumber", "mastAsset.yardNumber");
        IHS_NAME_MAP.put("publishedVersion.name", "ihsAsset.ihsAsset.name");
        IHS_NAME_MAP.put("publishedVersion.buildDate", "ihsAsset.ihsAsset.dateOfBuild");
        IHS_NAME_MAP.put("publishedVersion.yardNumber", "ihsAsset.ihsAsset.yardNumber");
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
    }

    @Theory
    public void testQueries(final QueryAndResult queryAndResult)
    {
        final MultiAssetPage result = TestUtils.postResponse(
                queryAndResult.getSort() == null ? ASSET_QUERY : String.format(ASSET_QUERY_WITH_SORTING, queryAndResult.getSort()),
                queryAndResult.getQuery(), HttpStatus.OK, MultiAssetPage.class);
        final Iterator<ExpectedResult> expected = queryAndResult.getExpected().iterator();

        result.getContent().stream().forEach(asset ->
        {
            if (asset.getMastAsset() == null || asset.getMastAsset().getId() < MAX_ORIGINAL_ID)
            {
                boolean nullCheckPass = false;
                assertTrue(expected.hasNext());

                final ExpectedResult expect = expected.next();

                if (AssetsFound.BOTH.equals(expect.getExpected()))
                {
                    nullCheckPass = asset.getIhsAsset() != null && asset.getMastAsset() != null;
                }
                else if (AssetsFound.MAST.equals(expect.getExpected()))
                {
                    nullCheckPass = asset.getIhsAsset() == null && asset.getMastAsset() != null;
                }
                else
                {
                    nullCheckPass = asset.getIhsAsset() != null && asset.getMastAsset() == null;
                }

                assertTrue(nullCheckPass);
                assertEquals(expect.getImoNumber(), asset.getImoNumber());
            }
        });

        assertFalse(expected.hasNext());
    }

    @Theory
    public void testSort(final String... sortFields)
    {
        final String sort = String.join(",", sortFields);
        final MultiAssetPage result = TestUtils.postResponse(String.format(ASSET_QUERY_WITH_SORTING, sort), new AbstractQueryDto(), HttpStatus.OK,
                MultiAssetPage.class);

        MultiAssetDto lastAsset = null;
        boolean first = true;

        for (final MultiAssetDto currentAsset : result.getContent())
        {
            if (first)
            {
                first = false;
                lastAsset = currentAsset;
                continue;
            }
            for (final String sortField : sortFields)
            {
                final Object currentField = this.getCorrectField(currentAsset, sortField);
                final Object lastField = this.getCorrectField(lastAsset, sortField);

                if (this.lessThan(lastField, currentField))
                {
                    lastAsset = currentAsset;
                    break;
                }
                else if (this.lessThan(currentField, lastField))
                {
                    fail(String.format(SORT_FAIL, sortField, lastField.toString(), currentField.toString()));
                }
            }
        }
    }

    @SuppressWarnings("PMD.NPathComplexity")
    private Object getCorrectField(final MultiAssetDto asset, final String sortField)
    {
        Object field = null;
        try
        {
            final Object mastAssetField = asset.getMastAsset() == null ? null : this.getProperty(asset, MAST_NAME_MAP.get(sortField));
            final Object ihsAssetField = asset.getIhsAsset() == null ? null : this.getProperty(asset, IHS_NAME_MAP.get(sortField));
            field = asset.getMastAsset() != null && asset.getIhsAsset() != null
                    ? this.lessThan(mastAssetField, ihsAssetField) ? mastAssetField : ihsAssetField
                    : asset.getMastAsset() == null ? ihsAssetField : mastAssetField;

        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            fail(exception.getMessage());
        }

        return field;
    }

    private boolean lessThan(final Object obj1, final Object obj2)
    {
        boolean returnValue = false;

        if (obj2 == null && obj1 != null)
        {
            returnValue = true;
        }
        else if (obj2 != null && obj1 != null)
        {
            returnValue = this.lessThanComparitor(obj1, obj2);
        }

        return returnValue;
    }

    @SuppressWarnings("PMD.CyclomaticComplexity")
    private boolean lessThanComparitor(final Object obj1, final Object obj2)
    {
        boolean returnValue = false;
        final SimpleDateFormat formatter = DateHelper.mastFormatter();

        if (Date.class.isInstance(obj1) && Date.class.isInstance(obj2))
        {
            final Date date1 = Date.class.cast(obj1);
            final Date date2 = Date.class.cast(obj2);
            returnValue = date1.before(date2);
        }
        else if (Date.class.isInstance(obj1) && String.class.isInstance(obj2))
        {
            final Date date1 = Date.class.cast(obj1);
            final String date2 = String.class.cast(obj2);
            try
            {
                returnValue = date1.before(formatter.parse(date2));
            }
            catch (final ParseException exception)
            {
                returnValue = false;
            }
        }
        else if (String.class.isInstance(obj1) && Date.class.isInstance(obj2))
        {
            final String date1 = String.class.cast(obj1);
            final Date date2 = Date.class.cast(obj2);
            try
            {
                returnValue = formatter.parse(date1).before(date2);
            }
            catch (final ParseException exception)
            {
                returnValue = false;
            }
        }
        else if (Long.class.isInstance(obj1) && Long.class.isInstance(obj2))
        {
            final Long long1 = Long.class.cast(obj1);
            final Long long2 = Long.class.cast(obj2);
            returnValue = long1 < long2;
        }
        else if (Double.class.isInstance(obj1) && Double.class.isInstance(obj2))
        {
            final Double double1 = Double.class.cast(obj1);
            final Double double2 = Double.class.cast(obj2);
            returnValue = double1 < double2;
        }
        else if (String.class.isInstance(obj1) && String.class.isInstance(obj2))
        {
            final String string1 = String.class.cast(obj1);
            final String string2 = String.class.cast(obj2);
            returnValue = string1.compareTo(string2) < 0 || string1.compareToIgnoreCase(string2) < 0;
        }

        return returnValue;
    }

    private Object getProperty(final Object obj, final String field) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        final String[] segments = field.split("\\.");

        Object requestedObject = obj;

        for (final String segment : segments)
        {
            requestedObject = PropertyUtils.getProperty(requestedObject, segment);

            if (requestedObject == null)
            {
                break;
            }
        }

        return requestedObject;
    }
}
