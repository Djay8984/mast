package com.baesystems.ai.lr.pages;

import java.util.ArrayList;

import com.baesystems.ai.lr.dto.services.ProductDto;

public class ProductList extends ArrayList<ProductDto>
{
    private static final long serialVersionUID = -4935775958196771531L;
}
