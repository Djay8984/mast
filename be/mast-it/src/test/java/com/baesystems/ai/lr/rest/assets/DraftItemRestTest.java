package com.baesystems.ai.lr.rest.assets;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;

import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class DraftItemRestTest extends BaseRestTest
{
    public static final String CHECK_OUT = urlBuilder(BASE_PATH, "/asset/%d/check-out");
    public static final String CHECK_IN = urlBuilder(BASE_PATH, "/asset/%d/check-in");
    public static final String BASE_DRAFT_ITEM = urlBuilder(BASE_PATH, "/asset/%d/draft-item");
    private static final String SINGLE_ITEM = urlBuilder(BASE_PATH, "/asset/%d/item/%d");

    // This value has been updated to a massive size to cope with a recent reference data change where the max occurs
    // for everything is set to 75.
    private static final int MAX_OCCURS = 75;

    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_3_ID = 3L;

    private static final Long ITEM_TYPE_26_ID = 26L; // parent type = 27
    private static final Long ITEM_TYPE_109_ID = 109L; // parent type = 4

    private static final Long DRAFT_ITEM_1_ID = 1L;
    private static final Long DRAFT_ITEM_70_ID = 70L;
    private static final Long ITEM_TYPE_451 = 451L;



    /* OLD */

    //todo adapt
    @Test
    @Ignore
    public void postDraftItemsFromRefDataTestWrongType() throws DatabaseUnitException
    {
        final DraftItemListDto draftItemList = getValidDraftItemList(1L);
        draftItemList.getDraftItemDtoList().get(0).setItemTypeId(ITEM_TYPE_109_ID);

        final ErrorMessageDto error = TestUtils.postResponse(String.format(BASE_DRAFT_ITEM, ASSET_3_ID), draftItemList, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("The parent item with identifier ");
        errorMessage.append(DRAFT_ITEM_70_ID);
        errorMessage.append(" (");
        errorMessage.append(DataUtils.getDraftItem(DRAFT_ITEM_70_ID).getName());
        errorMessage.append(") cannot be linked to a child of type ");
        errorMessage.append(ITEM_TYPE_109_ID);
        errorMessage.append(" (");
        errorMessage.append(DataUtils.getItemType(ITEM_TYPE_109_ID).getName());
        errorMessage.append(")");

        assertEquals(errorMessage.toString(), error.getMessage());
    }

    //todo adapt
    @Test
    @Ignore
    public void postDraftItemsFromRefDataTestTooManyFieldsNotNull()
    {
        final DraftItemListDto draftItemList = getValidDraftItemList(1L);
        draftItemList.getDraftItemDtoList().get(0).setFromId(DRAFT_ITEM_1_ID);
        draftItemList.getDraftItemDtoList().get(0).setFromId(DRAFT_ITEM_1_ID);

        final ErrorMessageDto error = TestUtils.postResponse(String.format(BASE_DRAFT_ITEM, ASSET_3_ID), draftItemList, HttpStatus.BAD_REQUEST)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertTrue(error.getMessage().contains("draftItemDtoList[0] Fields fromId and itemTypeId are mutually exclusive"));
        assertTrue(error.getMessage().contains("draftItemDtoList[1] Fields fromId and itemTypeId are mutually exclusive"));
    }

    @Test
    public void postDraftItemsFromRefDataTestMixNullPattern()
    {
        final DraftItemListDto draftItemList = getValidDraftItemList(1L);
        draftItemList.getDraftItemDtoList().get(0).setItemTypeId(null);
        draftItemList.getDraftItemDtoList().get(0).setFromId(DRAFT_ITEM_1_ID);

        final ErrorMessageDto error = TestUtils.postResponse(String.format(BASE_DRAFT_ITEM, ASSET_3_ID), draftItemList, HttpStatus.BAD_REQUEST)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(
                "Validation failed with errors: draftItemDtoList Null patterns of list members do not match for specified field(s).",
                error.getMessage());
    }

    //todo adapt
    @Test
    @Ignore
    public void postDraftItemWithTooManyChildItems() throws DatabaseUnitException, IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, IntegrationTestException
    {
        final DraftItemListDto validDraftItemList = getValidDraftItemList(1L);

        createNewDraftItemsFromRefData(ASSET_3_ID, validDraftItemList);

        validDraftItemList.setDraftItemDtoList(new ArrayList<>());

        for (int i = 0; i < MAX_OCCURS + 1; ++i)
        {
            validDraftItemList.getDraftItemDtoList().add(new DraftItemDto());
            validDraftItemList.getDraftItemDtoList().get(i).setItemTypeId(ITEM_TYPE_26_ID);
            validDraftItemList.getDraftItemDtoList().get(i).setToParentId(DRAFT_ITEM_70_ID);
        }

        final ErrorMessageDto error = TestUtils.postResponse(String.format(BASE_DRAFT_ITEM, ASSET_3_ID), validDraftItemList, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("The parent item with identifier ");
        errorMessage.append(DRAFT_ITEM_70_ID);
        errorMessage.append(" (");
        errorMessage.append(DataUtils.getDraftItem(DRAFT_ITEM_70_ID).getName());
        errorMessage.append(") cannot be linked to more than ");
        errorMessage.append(MAX_OCCURS);
        errorMessage.append(" items of type ");
        errorMessage.append(ITEM_TYPE_26_ID);
        errorMessage.append(" (");
        errorMessage.append(DataUtils.getItemType(ITEM_TYPE_26_ID).getName());
        errorMessage.append(")");

        assertEquals(errorMessage.toString(), error.getMessage());
    }

    public static DraftItemListDto getValidDraftItemList(final Long parentId)
    {
        final DraftItemListDto validDraftItemList = new DraftItemListDto();
        final DraftItemDto item1 = new DraftItemDto();
        final DraftItemDto item2 = new DraftItemDto();

        item1.setItemTypeId(ITEM_TYPE_451);
        item2.setItemTypeId(ITEM_TYPE_451);
        item1.setToParentId(parentId);
        item2.setToParentId(parentId);

        validDraftItemList.setDraftItemDtoList(new ArrayList<>());
        validDraftItemList.getDraftItemDtoList().add(item1);
        validDraftItemList.getDraftItemDtoList().add(item2);

        return validDraftItemList;
    }

    public static List<LinkVersionedResource> createNewDraftItemsFromRefData(final Long assetId, final DraftItemListDto items)
            throws IntegrationTestException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        final LazyItemDto responseBeforePost = TestUtils
                .getResponse(String.format(SINGLE_ITEM, assetId, items.getDraftItemDtoList().get(0).getToParentId()), HttpStatus.OK)
                .jsonPath().getObject("", LazyItemDto.class); // get the parent item before the post

        final List<LinkVersionedResource> childrenBeforeCreate = responseBeforePost.getItems();

        final LazyItemDto response = TestUtils.postResponse(String.format(BASE_DRAFT_ITEM, assetId), items, HttpStatus.OK)
                .jsonPath().getObject("", LazyItemDto.class); // create some new items

        final List<LinkVersionedResource> childrenAfterCreate = response.getItems();

        childrenAfterCreate.removeAll(childrenBeforeCreate); // remove the old items from the list

        final List<LinkVersionedResource> newItems = new ArrayList<LinkVersionedResource>();

        // extract the IDs and check that the new items added have the expected consecutive display orders
        for (final LinkVersionedResource newChildItem : childrenAfterCreate)
        {
            newItems.add(newChildItem);
        }

        return newItems;
    }

}
