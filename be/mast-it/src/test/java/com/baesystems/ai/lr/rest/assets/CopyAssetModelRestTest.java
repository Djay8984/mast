package com.baesystems.ai.lr.rest.assets;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.DatabaseUtils;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.TestUtils;

/**
 * Ignored as superseeded by other Versioning tests, but there may be some useful details to migrate
 */
@Category(IntegrationTest.class)
@RunWith(Theories.class)
@Ignore
public class CopyAssetModelRestTest extends BaseRestTest
{
    // APIs
    public static final String BASE_ASSET = new StringBuffer().append(BASE_PATH).append("/asset").toString();
    public static final String ASSET = new StringBuffer().append(BASE_ASSET).append("/%d").toString();
    private static final String CREATE_DRAFT_ITEMS = urlBuilder(BASE_PATH, "/asset/%d/create-draft-items");
    private static final String PUBLISH_DRAFT_ITEMS = urlBuilder(BASE_PATH, "/asset/%d/publish-draft-items");
    private static final String DRAFT_ITEM = urlBuilder(BASE_PATH, "/asset/%d/draft-item");
    public static final String BASE_ATTRIBUTE = new StringBuffer().append(BASE_ASSET).append("/%d/item/%d/attribute").toString();
    public static final String ATTRIBUTE = new StringBuffer().append(BASE_ATTRIBUTE).append("/%d").toString();
    private static final String ITEMS_FOR_ASSET_VIA_ROOT = urlBuilder(BASE_PATH, "/asset/%d/item/%d");

    // Table names
    private static final String MAST_ASSET_DRAFTASSETITEM = "MAST_ASSET_DraftAssetItem";
    private static final String MAST_ASSET_DRAFTASSETITEMRELATIONSHIP = "MAST_ASSET_DraftAssetItemRelationship";
    private static final String MAST_ASSET_DRAFTASSETATTRIBUTE = "MAST_ASSET_DraftAssetAttribute";

    private static final Long ASSET_7778_ID = 7778L;  // Asset with linked items
    private static final Long ITEM_10000_ID = 10000L; // Root item for Asset 7778
    private static final Long ITEM_10001_ID = 10001L;

    private static final Long ATTRIBUTE_TYPE_93_ID = 19L;
    private static final Long ATTRIBUTE_TYPE_97_ID = 1106L;
    private static final Long ATTRIBUTE_TYPE_1085_ID = 1085L;

    private static final String COPY_MODE_ANY = "COPY MODE ANY";
    private static final String COPY_MODE_MUST_COPY = "COPY MODE MUST COPY";
    private static String copyModeDoNotCopy;

    private static final Long ASSET_7780_ID = 7780L;  // Asset with no linked items (except root)

    private DraftItemListDto draftItemList;

    private static List<Long> newAttributeIds = new ArrayList<Long>();

    @DataPoints
    public static final Boolean[] COPY_ATTRIBUTES = {true, false};

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        draftItemList = new DraftItemListDto();
        draftItemList.setDraftItemDtoList(new ArrayList<DraftItemDto>());

        copyModeDoNotCopy = DateHelper.mastFormatter().format(new Date());
    }

    /*
     * Need to reset incrementors in order to be able to test with known IDS in multiple places
     */
    public void resetIncrementors()
            throws SQLException, ClassNotFoundException, IOException, DatabaseUnitException, InstantiationException, IllegalAccessException
    {
        DatabaseUtils.refreshIncrementor("mast", MAST_ASSET_DRAFTASSETITEM);
        DatabaseUtils.refreshIncrementor("mast", MAST_ASSET_DRAFTASSETITEMRELATIONSHIP);
        DatabaseUtils.refreshIncrementor("mast", MAST_ASSET_DRAFTASSETATTRIBUTE);
    }

    private LazyItemDto createDraftItems(final Long assetId)
    {
        return TestUtils.postResponse(String.format(CREATE_DRAFT_ITEMS, assetId), "", HttpStatus.OK, LazyItemDto.class);
    }

    private void publishDraftItems(final Long assetId)
    {
        TestUtils.postResponse(String.format(PUBLISH_DRAFT_ITEMS, assetId), "", HttpStatus.OK);
        try
        {
            resetIncrementors();
        }
        catch (final SQLException | IOException | DatabaseUnitException | InstantiationException | IllegalAccessException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private Long createNewAsset()
    {
        final AssetDto asset7780 = TestUtils.getResponse(String.format(ASSET, ASSET_7780_ID), AssetDto.class);

        asset7780.setId(null);
        asset7780.setIhsAsset(null);
        asset7780.setYardNumber(String.format("%d", (new Date()).getTime())); // set the yard number to something unique

        final AssetDto newAsset = TestUtils.postResponse(BASE_ASSET, asset7780, HttpStatus.OK, AssetDto.class);

        return newAsset.getId();
    }

    private void setUpNewAttributes()
    {
        // Copy Mode Any
        AttributeDto attribute = new AttributeDto();
        attribute.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_97_ID));
        attribute.setValue(COPY_MODE_ANY);

        AttributeDto newAttribute = TestUtils
                .postResponse(String.format(BASE_ATTRIBUTE, ASSET_7778_ID, ITEM_10001_ID), attribute, HttpStatus.OK, AttributeDto.class);

        newAttributeIds.add(newAttribute.getId());

        // Copy Mode Must Copy
        attribute = new AttributeDto();
        attribute.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_93_ID));
        attribute.setValue(COPY_MODE_MUST_COPY);

        newAttribute = TestUtils
                .postResponse(String.format(BASE_ATTRIBUTE, ASSET_7778_ID, ITEM_10001_ID), attribute, HttpStatus.OK, AttributeDto.class);

        newAttributeIds.add(newAttribute.getId());

        // Copy Mode Do Not Copy
        attribute = new AttributeDto();
        attribute.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_1085_ID));
        attribute.setValue(copyModeDoNotCopy);

        newAttribute = TestUtils
                .postResponse(String.format(BASE_ATTRIBUTE, ASSET_7778_ID, ITEM_10001_ID), attribute, HttpStatus.OK, AttributeDto.class);

        newAttributeIds.add(newAttribute.getId());
    }

    private void deleteAttributes()
    {
        for (final Long id : newAttributeIds)
        {
            TestUtils.deleteResponse(format(ATTRIBUTE, ASSET_7778_ID, ITEM_10001_ID, id), HttpStatus.OK);
        }
    }

    /**
     * Test the full asset model copy using POST /asset/x/draft-item
     */
    @Test
    @Ignore("Ignoring temporarily until attribute type is changed to reflect new reference data")
    public void copyFullAssetModel() throws DataSetException
    {
        // Create draft items for the asset (A) that we are going to copy from
        createDraftItems(ASSET_7778_ID);

        // Count the items in the draft tables to check how many we expect to copy later
        final Long originItemCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEM);
        final Long originRelationshipCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEMRELATIONSHIP);
        final Long originAttributeCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETATTRIBUTE);

        // POST /asset/x/publish-draft-items for asset (A) to clean up the data
        publishDraftItems(ASSET_7778_ID);

        final Long newAssetId = createNewAsset();

        // Create draft items for the asset (B) that we are going to copy onto (this will just be the root item)
        final LazyItemDto draftRootItemB = createDraftItems(newAssetId);

        // Get the items from asset (A)
        final LazyItemDto rootItemA = TestUtils.getResponse(String.format(ITEMS_FOR_ASSET_VIA_ROOT, ASSET_7778_ID, ITEM_10000_ID), HttpStatus.OK,
                LazyItemDto.class);

        final List<LinkVersionedResource> items = rootItemA.getItems();
        final Set<DraftItemDto> draftItemsToCopy = new HashSet<DraftItemDto>();

        // Loop through those items, converting them to a set of DefectItemDto
        for (final LinkVersionedResource item : items)
        {
            // While looping, set the parent ID to be the 'root' item on asset (B)
            final DraftItemDto draftItem = new DraftItemDto();
            draftItem.setFromId(item.getId());
            draftItem.setToParentId(draftRootItemB.getId());

            draftItemsToCopy.add(draftItem);
        }

        draftItemList.setCopyAttributes(true);
        draftItemList.getDraftItemDtoList().addAll(draftItemsToCopy);

        try
        {
            // POST /asset/x/draft-item to copy the items to asset (B)
            TestUtils.postResponse(String.format(DRAFT_ITEM, newAssetId), draftItemList, HttpStatus.OK);

            // Count the items in the draft tables to check that the correct number was copied from asset (A) to asset
            // (B)
            final Long copiedItemCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEM);
            final Long copiedRelationshipCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEMRELATIONSHIP);
            final Long copiedAttributeCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETATTRIBUTE);

            assertEquals(copiedItemCount, originItemCount);
            assertEquals(copiedRelationshipCount, originRelationshipCount);
            assertEquals(copiedAttributeCount, originAttributeCount);
        }
        finally
        {
            // POST /asset/x/publish-draft-items for asset (B) to clean up the data
            publishDraftItems(newAssetId);
        }
    }

    /**
     * Test the full asset model copy using POST /asset/x/draft-item to make sure must copy attributes are copied
     */
    @Theory
    @Ignore("Ignoring temporarily until attribute type is changed to reflect new reference data")
    public void copyFullAssetModelTestCopyAttributes(final Boolean copyAttributes) throws DatabaseUnitException
    {
        setUpNewAttributes();

        // Create draft items for the asset (A) that we are going to copy from
        createDraftItems(ASSET_7778_ID);

        // Count the items in the draft tables to check how many we expect to copy later
        final Long originItemCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEM);
        final Long originRelationshipCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEMRELATIONSHIP);
        final Long originAttributeCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETATTRIBUTE);

        // POST /asset/x/publish-draft-items for asset (A) to clean up the data
        publishDraftItems(ASSET_7778_ID);

        final List<Long> oldAttributeIds = DataUtils.getIdList(MAST_ASSET_DRAFTASSETATTRIBUTE);

        final Long newAssetId = createNewAsset();

        // Create draft items for the asset (B) that we are going to copy onto (this will just be the root item)
        final LazyItemDto draftRootItemB = createDraftItems(newAssetId);

        // Get the items from asset (A)
        final LazyItemDto rootItemA = TestUtils.getResponse(String.format(ITEMS_FOR_ASSET_VIA_ROOT, ASSET_7778_ID, ITEM_10000_ID), HttpStatus.OK,
                LazyItemDto.class);

        final List<LinkVersionedResource> items = rootItemA.getItems();
        final Set<DraftItemDto> draftItemsToCopy = new HashSet<DraftItemDto>();

        // Loop through those items, converting them to a set of DefectItemDto
        for (final LinkVersionedResource item : items)
        {
            // While looping, set the parent ID to be the 'root' item on asset (B)
            final DraftItemDto draftItem = new DraftItemDto();
            draftItem.setFromId(item.getId());
            draftItem.setToParentId(draftRootItemB.getId());

            draftItemsToCopy.add(draftItem);
        }

        draftItemList.setCopyAttributes(copyAttributes);
        draftItemList.getDraftItemDtoList().addAll(draftItemsToCopy);

        try
        {
            // POST /asset/x/draft-item to copy the items to asset (B)
            TestUtils.postResponse(String.format(DRAFT_ITEM, newAssetId), draftItemList, HttpStatus.OK);

            // Count the items in the draft tables to check that the correct number was copied from asset (A) to asset
            // (B)
            final Long copiedItemCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEM);
            final Long copiedRelationshipCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETITEMRELATIONSHIP);
            final Long copiedAttributeCount = DataUtils.getRowCount(MAST_ASSET_DRAFTASSETATTRIBUTE);

            assertEquals(copiedItemCount, originItemCount);
            assertEquals(copiedRelationshipCount, originRelationshipCount);
            assertEquals(copiedAttributeCount, originAttributeCount);

            final List<Long> copiedAttributeIds = DataUtils.getIdList(MAST_ASSET_DRAFTASSETATTRIBUTE);
            copiedAttributeIds.removeAll(oldAttributeIds);

            final List<String> attributeValues = new ArrayList<String>();

            for (final Long id : copiedAttributeIds)
            {
                if (!DataUtils.isDeleted(MAST_ASSET_DRAFTASSETATTRIBUTE, id))
                {
                    attributeValues.add(DataUtils.getDraftAttribute(id).getValue());
                }
            }

            // Make sure the right attribute values have been copied
            assertEquals(copyAttributes, attributeValues.contains(COPY_MODE_ANY));
            assertTrue(attributeValues.contains(COPY_MODE_MUST_COPY));
            assertFalse(attributeValues.contains(copyModeDoNotCopy));
        }
        finally
        {
            // POST /asset/x/publish-draft-items for asset (B) to clean up the data
            publishDraftItems(newAssetId);
            deleteAttributes();
            newAttributeIds = new ArrayList<Long>();
        }
    }
}
