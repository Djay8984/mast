package com.baesystems.ai.lr.rest.defects;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.DefectPage;
import com.baesystems.ai.lr.pages.SurveyPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class DefectRestTest extends BaseRestTest
{
    public static final String BASE_DEFECT = urlBuilder(BASE_PATH, "/defect");
    private static final String BASE_JOB = urlBuilder(BASE_PATH, "/job/%d");
    public static final String SPECIFIC_DEFECT = urlBuilder(BASE_DEFECT, "/%d");
    private static final String JOB_SURVEY_PATH = urlBuilder(BASE_JOB, "/survey");
    private static final String SPECIFIC_JOB_SURVEY_PATH = urlBuilder(JOB_SURVEY_PATH, "/%d");
    private static final String ASSET_DEFECT = urlBuilder(BASE_PATH, "/asset/%d/defect");
    private static final String DEFECT_QUERY = urlBuilder(ASSET_DEFECT, "/query");
    private static final String WIP_DEFECT = urlBuilder(BASE_JOB, "/wip-defect");
    private static final String WIP_DEFECT_QUERY = urlBuilder(WIP_DEFECT, "/query");
    private static final String WIP_DEFECT_SPECIFIC = urlBuilder(WIP_DEFECT, "/%d");

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;

    private static final Long INVALID_ID = 9999L;
    private static final Long DEFECT_1_ID = 1L;
    private static final Long DEFECT_2_ID = 2L;
    private static final Long DEFECT_3_ID = 3L;
    private static final Long DEFECT_7_ID = 7L;
    private static final Long DEFECT_8_ID = 8L;
    private static final Long WIP_DEFECT_1_ID = 1L;
    private static final Long WIP_DEFECT_10_ID = 10L;
    private static final Long VALUE_1_ID = 1L;
    private static final Long VALUE_2_ID = 2L;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long JOB_DEFECT_1_ID = 1L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L;
    private static final Long ITEM_4_ID = 4L;
    private static final Long ITEM_TYPE_2 = 2L;
    private static final Long ITEM_TYPE_327_ID = 327L;
    private static final Long ITEM_TYPE_451_ID = 451L;
    private static final String DETAILS = "This defect has some other severity.";
    private static final Integer FIRST_FRAME_NUMBER = 1;
    private static final Long WIP_DEFECT_ID_WITH_NO_PARENT = 7L;
    private static final Long WIP_DEFECT_ID_WITH_A_PARENT = 8L;
    private static final Long WIP_DEFECT_NEW_CATEGORY = 1L;
    private static final Long WIP_DEFECT_FOR_DELETION_ID = 9L;
    private static final Long WIP_REPAIR_FOR_DELETION_ID = 2L;
    private static final String WIP_FAULT_TABLE = "MAST_FAULT_WIP_Fault";
    private static final String WIP_REPAIR_TABLE = "MAST_DEFECT_WIP_Repair";
    private static final Long STATUS_RETURN_NOTHING = -1L;
    private static final Long DEFECT_SERVICE_CAT_ID = 261L;

    // Query
    private static final Long CATEGORY_ID_2 = 2L;
    private static final Long STATUS_1_ID = 1L;

    private static final Integer QUERY_TEST_SIZE = 6;

    private DefectDto defect1;
    private DefectDto wipDefect1;
    private DefectLightDto defectLight1;
    private DefectDto defect2;
    private DefectDto defect3;
    private DefectDto defect7;
    private DefectDto defect8;
    private DefectDto defectWithValues;
    private DefectDto wipDefectWithValues;
    private LinkResource job;
    private JobDefectDto jobDefect;
    private List<JobDefectDto> jobs;
    private DefectDto defectWithItems;
    private DefectDto wipDefectWithItems;
    private ItemLightDto item1;
    private ItemLightDto item2;
    private ItemLightDto item4;
    private DefectPage defectPage;
    private DefectPage wipDefectPage;

    @DataPoints("NON_WIP_QUERIES")
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints("WIP_QUERIES")
    public static final QueryAndResult[] WIP_QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        // Search title
        QUERIES[index] = new QueryAndResult("searchString", "Defect with minimum required data 1",
                new Long[]{DEFECT_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_2),
                new Long[]{DEFECT_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_1_ID),
                new Long[]{DEFECT_1_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING),
                new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_451_ID),
                new Long[]{DEFECT_7_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327_ID), new Long[]{});

        index = 0;
        // Search title
        WIP_QUERIES[index] = new QueryAndResult("searchString", "WIP Defect with minimum required data 1",
                new Long[]{WIP_DEFECT_1_ID});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_2),
                new Long[]{WIP_DEFECT_1_ID});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_1_ID),
                new Long[]{WIP_DEFECT_1_ID});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING),
                new Long[]{});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_451_ID),
                new Long[]{WIP_DEFECT_10_ID});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327_ID),
                new Long[]{});
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.defect1 = DataUtils.getDefect(DEFECT_1_ID);
        this.defect2 = DataUtils.getDefect(DEFECT_2_ID);
        this.defect3 = DataUtils.getDefect(DEFECT_3_ID);
        this.defect7 = DataUtils.getDefect(DEFECT_7_ID);
        this.defect8 = DataUtils.getDefect(DEFECT_8_ID);

        this.defectLight1 = DataUtils.getDefectLight(DEFECT_1_ID);
        this.wipDefect1 = DataUtils.getWIPDefect(WIP_DEFECT_1_ID);

        this.defect1.setRepairCount(ZERO);
        this.defect2.setRepairCount(TWO);
        this.defect3.setRepairCount(ONE);
        this.wipDefect1.setRepairCount(ZERO);

        this.defect1.setCourseOfActionCount(ZERO);
        this.defect2.setCourseOfActionCount(TWO);
        this.defect3.setCourseOfActionCount(THREE);
        this.wipDefect1.setCourseOfActionCount(TWO);

        // Create survey
        final SurveyDto survey = new SurveyDto();
        survey.setJob(this.wipDefect1.getJob());
        survey.setName(this.wipDefect1.getTitle());
        survey.setScheduleDatesUpdated(Boolean.FALSE);
        survey.setJobScopeConfirmed(Boolean.FALSE);
        survey.setServiceCatalogue(new LinkResource(DEFECT_SERVICE_CAT_ID));
        survey.setSurveyStatus(new LinkResource(ServiceCreditStatusType.COMPLETE.value()));
        survey.setJobScopeConfirmed(Boolean.TRUE);
        this.wipDefect1.setSurvey(survey);

        this.defectPage = new DefectPage();
        this.defectPage.getContent().add(this.defect1);
        this.defectPage.getContent().add(this.defect3);
        this.defectPage.getContent().add(this.defect7);
        this.defectPage.getContent().add(this.defect8);

        this.wipDefectPage = new DefectPage();
        this.wipDefectPage.setContent(Arrays.asList(this.wipDefect1));

        this.defectWithValues = DataUtils.getDefect(DEFECT_1_ID);
        this.defectWithItems = DataUtils.getDefect(DEFECT_1_ID);
        this.wipDefectWithValues = DataUtils.getWIPDefect(WIP_DEFECT_1_ID);
        this.wipDefectWithItems = DataUtils.getWIPDefect(WIP_DEFECT_1_ID);

        this.item1 = DataUtils.getItemLight(ITEM_1_ID);
        this.item2 = DataUtils.getItemLight(ITEM_2_ID);
        this.item4 = DataUtils.getItemLight(ITEM_4_ID);

        populateDefectValues(this.defectWithValues);
        populateDefectValues(this.wipDefectWithValues);

        populateDefectItems(this.defectWithItems);
        populateDefectItems(this.wipDefectWithItems);
    }

    private void populateDefectItems(final DefectDto defectDto)
    {
        defectDto.setItems(new ArrayList<>());
        defectDto.getItems().add(new DefectItemDto());
        defectDto.getItems().add(new DefectItemDto());

        defectDto.getItems().get(0).setItem(this.item1);
        defectDto.getItems().get(1).setItem(this.item2);
    }

    private void populateDefectValues(final DefectDto defectDto)
    {
        defectDto.setValues(new ArrayList<>());
        defectDto.getValues().add(new DefectDefectValueDto());
        defectDto.getValues().add(new DefectDefectValueDto());

        defectDto.getValues().get(0).setDefectValue(new DefectValueDto());
        defectDto.getValues().get(1).setDefectValue(new DefectValueDto());

        defectDto.getValues().get(0).getDefectValue().setId(VALUE_1_ID);
        defectDto.getValues().get(1).getDefectValue().setId(VALUE_2_ID);
        defectDto.getValues().get(1).setOtherDetails(DETAILS);
    }

    private void setJobDefectForCreate(final DefectDto defect)
    {
        this.job = new LinkResource(JOB_1_ID);
        this.jobDefect = new JobDefectDto();
        this.jobDefect.setJob(this.job);
        this.jobs = new ArrayList<JobDefectDto>();
        this.jobs.add(this.jobDefect);

        defect.setJobs(this.jobs);
        defect.getAsset().setId(ASSET_2_ID); // put new defects on asset 2 to avoid interfering with other tests.
    }

    private void setJobDefectForGetOrUpdate(final DefectDto defect)
    {
        this.job = new LinkResource(JOB_1_ID);
        this.jobDefect = new JobDefectDto();
        this.jobDefect.setJob(this.job);
        this.jobs = new ArrayList<JobDefectDto>();
        this.jobs.add(this.jobDefect);
        this.jobDefect.setId(JOB_DEFECT_1_ID);
        defect.setJobs(this.jobs);
    }

    @Test
    public void getSingleDefect()
    {
        TestUtils.singleItemFound(format(SPECIFIC_DEFECT, DEFECT_1_ID), this.defectLight1, true);
    }

    @Test
    public void getSingleDefectWithRepair()
    {
        TestUtils.singleItemFound(format(SPECIFIC_DEFECT, DEFECT_2_ID), this.defect2, true);
    }

    @Test
    public void getSingleDefectWithCoC()
    {
        TestUtils.singleItemFound(format(SPECIFIC_DEFECT, DEFECT_3_ID), this.defect3, true);
    }

    @Test
    public void createDefectWithMinimumData()
    {
        setJobDefectForCreate(this.defect1);
        this.defect1.setId(null);

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.defect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
    }

    /**
     * Check that when we create a non-WIP defect followed by a WIP defect, the sequence number is incremented
     * correctly.
     */
    @Test
    public void testCreateDefectsSequenceNumberNonWipFirst()
    {
        final Integer firstSequenceNumber = createDefectAndCheckSequenceNumber();
        final Integer secondSequenceNumber = createWipDefectAndCheckSequenceNumber();
        assertEquals(firstSequenceNumber.intValue() + 1, secondSequenceNumber.intValue());
    }

    /**
     * Check that when we create a WIP defect followed by a non-WIP defect, the sequence number is incremented
     * correctly.
     */
    @Test
    public void testCreateDefectsSequenceNumberWipFirst()
    {
        final Integer firstSequenceNumber = createWipDefectAndCheckSequenceNumber();
        final Integer secondSequenceNumber = createDefectAndCheckSequenceNumber();
        assertEquals(firstSequenceNumber.intValue() + 1, secondSequenceNumber.intValue());
    }

    private Integer createDefectAndCheckSequenceNumber()
    {
        setJobDefectForCreate(this.defect1);
        this.defect1.setId(null);
        this.defect1.setAsset(new LinkResource(ASSET_2_ID));
        this.defect1.setJob(new LinkResource(JOB_2_ID));
        final DefectDto defect = TestUtils.postResponse(BASE_DEFECT, this.defect1, HttpStatus.OK, DefectDto.class);
        return defect.getSequenceNumber();
    }

    private Integer createWipDefectAndCheckSequenceNumber()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.setJob(new LinkResource(JOB_2_ID));
        this.wipDefect1.getSurvey().setJob(new LinkResource(JOB_2_ID));
        this.wipDefect1.setAsset(new LinkResource(ASSET_2_ID));
        final DefectDto wipDefect = TestUtils.postResponse(String.format(WIP_DEFECT, JOB_2_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);
        return wipDefect.getSequenceNumber();
    }

    @Test
    public void createWIPDefectWithMinimumData()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));
        this.wipDefect1.setAsset(new LinkResource(ASSET_1_ID));
        final DefectDto newDefect = TestUtils.postResponse(String.format(WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);
        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
        // check that the creation of the defect has also resulted in the
        // creation of a survey.
        final SurveyDto damageSurvey = checkDamageSurveyExistence(newDefect);
        assertNotNull(damageSurvey);
        // clean up the survey that was created
        TestUtils.deleteResponse(
                String.format(SPECIFIC_JOB_SURVEY_PATH, damageSurvey.getJob().getId(), damageSurvey.getId()),
                HttpStatus.OK);
    }

    @Test
    public void updateDefect()
    {
        setJobDefectForGetOrUpdate(this.defect1);
        this.defect1.setFirstFrameNumber(FIRST_FRAME_NUMBER);

        final DefectDto updatedDefect = TestUtils.putResponse(String.format(SPECIFIC_DEFECT, DEFECT_1_ID), this.defect1, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(updatedDefect);
        assertNotNull(updatedDefect.getId());
        assertTrue(updatedDefect.getFirstFrameNumber() == FIRST_FRAME_NUMBER);
    }

    @Test
    public void createDefectWithSomeValuesAttached()
    {
        setJobDefectForCreate(this.defectWithValues);
        this.defectWithValues.setId(null);

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.defectWithValues, HttpStatus.OK, DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
    }

    @Test
    public void createWIPDefectWithSomeValuesAttached()
    {
        this.wipDefectWithValues.setId(null);
        this.wipDefectWithValues.setJob(new LinkResource(JOB_1_ID));
        this.wipDefectWithValues.setAsset(new LinkResource(ASSET_1_ID));

        final DefectDto newDefect = TestUtils.postResponse(String.format(WIP_DEFECT, JOB_1_ID), this.wipDefectWithValues, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
    }

    @Test
    public void updateWIPDefectFailsWhenOldTokenIsUsed()
    {
        DefectDto defect = TestUtils.getResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, DEFECT_1_ID), HttpStatus.OK, DefectDto.class);

        final String originalStalenessHash = defect.getStalenessHash();

        // Make a small change
        defect.setIncidentDescription(defect.getIncidentDescription() + "X");
        defect = TestUtils.putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, DEFECT_1_ID), defect, HttpStatus.OK, DefectDto.class);

        // Make another small change, but use the hash from the original GET
        defect.setIncidentDescription(defect.getIncidentDescription() + "X");
        defect.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, DEFECT_1_ID), defect, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.DEFECT.getTypeString()), error.getMessage());
    }

    @Test
    public void amendWIPDefectTitleInCurrentJob() throws DatabaseUnitException
    {
        final DefectDto originalDefect = TestUtils.getResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_NO_PARENT),
                HttpStatus.OK, DefectDto.class);
        originalDefect.setTitle("This is a changed title");

        final DefectDto updatedDefect = TestUtils
                .putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_NO_PARENT), originalDefect, HttpStatus.OK,
                        DefectDto.class);
        assertEquals(originalDefect.getId(), updatedDefect.getId());
        assertEquals(originalDefect.getTitle(), updatedDefect.getTitle());
    }

    @Test
    public void amendWIPDefectTitleOutsideCurrentJob() throws DatabaseUnitException
    {
        final DefectDto originalDefect = DataUtils.getWIPDefect(WIP_DEFECT_ID_WITH_A_PARENT);
        originalDefect.setTitle("This title change should not be allowed.");

        TestUtils.putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_A_PARENT), originalDefect, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void amendWIPDefectCategoryOutsideCurrentJob() throws DatabaseUnitException
    {
        final DefectDto originalDefect = DataUtils.getWIPDefect(WIP_DEFECT_ID_WITH_A_PARENT);
        originalDefect.getDefectCategory().setId(WIP_DEFECT_NEW_CATEGORY);

        TestUtils.putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_A_PARENT), originalDefect, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void amendWIPDefectCategoryInCurrentJob() throws DatabaseUnitException
    {
        final DefectDto originalDefect = TestUtils.getResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_NO_PARENT),
                HttpStatus.OK, DefectDto.class);
        originalDefect.getDefectCategory().setId(WIP_DEFECT_NEW_CATEGORY);

        final DefectDto updatedDefect = TestUtils
                .putResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_ID_WITH_NO_PARENT), originalDefect, HttpStatus.OK,
                        DefectDto.class);
        assertEquals(originalDefect.getDefectCategory().getId(), updatedDefect.getDefectCategory().getId());
    }

    @Test
    public void deleteWIPDefect() throws DatabaseUnitException
    {
        assertEquals(false, DataUtils.isDeleted(WIP_FAULT_TABLE, WIP_DEFECT_FOR_DELETION_ID));
        assertEquals(false, DataUtils.isDeleted(WIP_REPAIR_TABLE, WIP_REPAIR_FOR_DELETION_ID));

        TestUtils.deleteResponse(String.format(WIP_DEFECT_SPECIFIC, JOB_1_ID, WIP_DEFECT_FOR_DELETION_ID), HttpStatus.OK);

        assertEquals(true, DataUtils.isDeleted(WIP_FAULT_TABLE, WIP_DEFECT_FOR_DELETION_ID));
        assertEquals(true, DataUtils.isDeleted(WIP_REPAIR_TABLE, WIP_REPAIR_FOR_DELETION_ID));
    }

    @Test
    public void updateDefectWithSomeValuesAttached()
    {
        setJobDefectForGetOrUpdate(this.defectWithValues);
        final DefectDto updatedDefect = TestUtils.putResponse(String.format(SPECIFIC_DEFECT, DEFECT_1_ID), this.defectWithValues, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(updatedDefect);
        assertNotNull(updatedDefect.getId());
        assertNotNull(updatedDefect.getValues());
        assertEquals(updatedDefect.getValues().size(), TWO);
    }

    @Test
    public void updateDefectByChangingValuesAttached()
    {
        setJobDefectForCreate(this.defectWithValues);
        this.defectWithValues.setId(null);

        // create a new defect to use to avoid changing test data used in other tests.
        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.defectWithValues, HttpStatus.OK, DefectDto.class);

        newDefect.getValues().get(0).setOtherDetails(DETAILS);
        newDefect.getValues().remove(1);

        final DefectDto updatedDefect = TestUtils.putResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), newDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(updatedDefect);
        assertNotNull(updatedDefect.getId());
        assertNotNull(updatedDefect.getValues());
        assertEquals(updatedDefect.getValues().size(), 1);
        assertNotNull(updatedDefect.getValues().get(0));
    }

    @Test
    public void createDefectWithIncorrectValuesAttached()
    {
        this.defectWithValues.setId(null);
        this.defectWithValues.getValues().get(0).getDefectValue().setId(INVALID_ID);
        TestUtils.postResponse(BASE_DEFECT, this.defectWithValues, HttpStatus.NOT_FOUND);
    }

    @Test
    public void updateDefectWithSomeItemsAttached()
    {
        setJobDefectForCreate(this.defectWithItems);
        this.defectWithItems.setId(null);

        // create a new defect to use to avoid changing test data used in other tests.
        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.defectWithItems, HttpStatus.OK, DefectDto.class);

        newDefect.getItems().remove(1);
        newDefect.getItems().get(0).setItem(this.item4);

        final DefectDto updatedDefect = TestUtils.putResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), newDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(updatedDefect);
        assertNotNull(updatedDefect.getId());
        assertNotNull(updatedDefect.getItems());
        assertEquals(updatedDefect.getItems().size(), 1);
        assertNotNull(updatedDefect.getItems().get(0));
        assertEquals(updatedDefect.getItems().get(0).getItem().getId(), ITEM_4_ID);
    }

    @Test
    public void createDefectWithIncorrectItemsAttached()
    {
        this.defectWithItems.setId(null);
        this.defectWithItems.getItems().get(0).getItem().setId(INVALID_ID);
        TestUtils.postResponse(BASE_DEFECT, this.defectWithItems, HttpStatus.NOT_FOUND);
    }

    @Test
    public void createWIPDefectWithIncorrectItemsAttached()
    {
        this.wipDefectWithItems.setId(null);
        this.wipDefectWithItems.getItems().get(0).getItem().setId(INVALID_ID);
        TestUtils.postResponse(String.format(WIP_DEFECT, JOB_1_ID), this.wipDefectWithItems, HttpStatus.NOT_FOUND);
    }

    @Test
    public void getAssetDefects()
    {
        TestUtils.listItemsFound(String.format(ASSET_DEFECT, ASSET_1_ID.intValue()), this.defectPage, true, false);
    }

    @Test
    public void getOpenAssetDefects()
    {
        TestUtils.listItemsFound(String.format(ASSET_DEFECT, ASSET_1_ID.intValue()) + "?statusId=1", this.defectPage, true, false);
    }

    /**
     * Test /asset/x/defect/query with various parameters
     */
    @Theory
    public final void queries(@FromDataPoints("NON_WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseDefectPage(queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final DefectPage results = TestUtils.postResponse(format(DEFECT_QUERY, ASSET_1_ID), query, HttpStatus.OK, DefectPage.class);

        TestUtils.compareFields(results, this.defectPage, true);
    }

    @Theory
    public final void wipQueries(@FromDataPoints("WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWipDefectPage(queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final DefectPage results = TestUtils.postResponse(format(WIP_DEFECT_QUERY, JOB_1_ID), query, HttpStatus.OK, DefectPage.class);

        TestUtils.compareFields(results, this.wipDefectPage, true);
    }

    private void initialiseDefectPage(final Long... ids)
    {
        this.defectPage = new DefectPage();

        DefectDto tempDefectDo;
        for (final Long id : ids)
        {
            try
            {
                tempDefectDo = DataUtils.getDefect(id);
                this.defectPage.getContent().add(tempDefectDo);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    private void initialiseWipDefectPage(final Long... ids)
    {
        this.wipDefectPage = new DefectPage();

        DefectDto tempDto;
        for (final Long id : ids)
        {
            try
            {
                tempDto = DataUtils.getWIPDefect(id);
                this.wipDefectPage.getContent().add(tempDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Utility method to find a survey of the appropriate type for the defect.
     *
     * @param wipDefect
     * @param shouldExist
     */
    private SurveyDto checkDamageSurveyExistence(final DefectDto wipDefect)
    {
        final SurveyPage surveyPage = TestUtils.getResponse(String.format(JOB_SURVEY_PATH, wipDefect.getJob().getId()), HttpStatus.OK,
                SurveyPage.class);

        SurveyDto defectSurveyFound = null;

        for (final SurveyDto survey : surveyPage.getContent())
        {
            if (wipDefect.getTitle().equals(survey.getName())
                    && survey.getServiceCatalogue().getId().equals(DEFECT_SERVICE_CAT_ID))
            {
                // there should only be one.
                assertNull(defectSurveyFound);
                defectSurveyFound = survey;
                assertEquals(new LinkResource(ServiceCreditStatusType.COMPLETE.value()), survey.getSurveyStatus());
                assertFalse(survey.getScheduleDatesUpdated());
                assertTrue(survey.getJobScopeConfirmed());

                break;
            }
        }

        return defectSurveyFound;
    }
}
