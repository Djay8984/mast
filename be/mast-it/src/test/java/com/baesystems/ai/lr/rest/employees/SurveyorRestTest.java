package com.baesystems.ai.lr.rest.employees;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.pages.SurveyorPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class SurveyorRestTest extends BaseRestTest
{
    /**
     * API Rest paths
     */
    public static final String SURVEYOR = new StringBuffer().append(BASE_PATH).append("/surveyor").toString();
    public static final String SURVEYOR_WITH_ALL_PARAMETERS = new StringBuffer().append(BASE_PATH)
            .append("/surveyor?search=%s&officeId=%d").toString();
    public static final String SURVEYOR_WITH_SEARCH = new StringBuffer().append(BASE_PATH).append("/surveyor?search=%s")
            .toString();
    public static final String SURVEYOR_WITH_OFFICE = new StringBuffer().append(BASE_PATH)
            .append("/surveyor?officeId=%d").toString();

    private static final Long SURVEYOR_3_ID = 3L;
    private static final Long SURVEYOR_4_ID = 4L;
    private static final Long SURVEYOR_5_ID = 5L;

    private static final String SEARCH = "*n*";
    private static final Long OFFICE_1_ID = 1L;
    private static final Long OFFICE_2_ID = 2L;

    private SurveyorPage surveyorPage;

    private void initialiseSurveyorPage(final Long... ids)
    {
        this.surveyorPage = new SurveyorPage();

        SurveyorDto tempSurveyor;
        for (final Long id : ids)
        {
            try
            {
                tempSurveyor = DataUtils.getSurveyor(id);
                this.surveyorPage.getContent().add(tempSurveyor);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
    }

    /**
     * Test that a valid GET request for ../surveyor results in a positive response. Expecting a list of JSONs
     * containing Surveyors 1-3.
     */
    @Test
    public final void surveyorsUri()
    {
        initialiseSurveyorPage(SURVEYOR_3_ID, SURVEYOR_4_ID, SURVEYOR_5_ID);
        TestUtils.listItemsFound(SURVEYOR, this.surveyorPage, true, false);
    }

    /**
     * Test that a valid GET request for ../surveyor?params... results in a positive response. Expecting a list of JSONs
     * containing Surveyor 3.
     */
    @Test
    @Ignore
    // this API call currently returns no data with the current test
    // data
    public final void surveyorsUriWithAllParameters()
    {
        initialiseSurveyorPage(SURVEYOR_5_ID);
        TestUtils.listItemsFound(String.format(SURVEYOR_WITH_ALL_PARAMETERS, SEARCH, OFFICE_2_ID.intValue()), this.surveyorPage, true, false);
    }

    /**
     * Test that a valid GET request for ../surveyor?params... results in a positive response. Expecting a list of JSONs
     * containing Surveyors 1 and 3.
     */
    @Test
    public final void surveyorsUriSearch()
    {
        initialiseSurveyorPage(SURVEYOR_3_ID, SURVEYOR_5_ID);
        TestUtils.listItemsFound(String.format(SURVEYOR_WITH_SEARCH, SEARCH), this.surveyorPage, true, false);
    }

    /**
     * Test that a valid GET request for ../surveyor?params... results in a positive response. Expecting a list of JSONs
     * containing Surveyors 1 and 2.
     */
    // @Test
    public final void surveyorsUriOffice()
    {
        initialiseSurveyorPage(SURVEYOR_3_ID, SURVEYOR_4_ID);
        TestUtils.listItemsFound(String.format(SURVEYOR_WITH_OFFICE, OFFICE_1_ID.intValue()), this.surveyorPage, true, false);
    }
}
