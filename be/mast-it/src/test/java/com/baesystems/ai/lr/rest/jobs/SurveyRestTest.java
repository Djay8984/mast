package com.baesystems.ai.lr.rest.jobs;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.util.Assert.notEmpty;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.dto.services.SurveyPageResourceDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.SurveyPage;
import com.baesystems.ai.lr.pages.WorkItemPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class SurveyRestTest extends BaseRestTest
{
    /** NON-WIP Paths */
    public static final String JOB_SURVEY_PATH = urlBuilder(BASE_PATH, "/job/%d/survey");
    public static final String JOB_SPECIFIC_SURVEY = urlBuilder(JOB_SURVEY_PATH, "/%d");
    public static final String WIP_TASKS = urlBuilder(BASE_PATH, "/wip-task/query");

    private static final Long JOB_1_ID = 1L;
    private static final Long SURVEY_1_ID = 1L;
    private static final Long DELETE_SURVEY_ID = 2L;
    private static final Long SERVICE_1_ID = 1L;
    private static final Long SERVICE_101_ID = 101L;
    private static final Long SERVICE_243_ID = 243L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long INVALID_ID = 9999L;

    private SurveyPage surveyPage;
    private SurveyDto surveyDto;
    private SurveyListDto surveyListDto;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        surveyDto = DataUtils.getSurvey(SURVEY_1_ID);

        final ScheduledServiceDto serviceDto = DataUtils.getService(SERVICE_1_ID);
        serviceDto.setAsset(new LinkResource(ASSET_1_ID));
        surveyDto.setScheduledService(serviceDto);

        final SurveyDto survey = DataUtils.getSurvey(SURVEY_1_ID);
        surveyListDto = new SurveyListDto();
        surveyListDto.setSurveys(Collections.singletonList(survey));
    }

    private void initialiseSurveyPage(final Long... ids)
    {
        surveyPage = new SurveyPage();

        SurveyDto tempSurvey;
        for (final Long id : ids)
        {
            try
            {
                tempSurvey = DataUtils.getSurvey(id);
                surveyPage.getContent().add(tempSurvey);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Test a valid GET request for ../job/x/survey returns list of Surveys.
     */
    @Test
    public final void getSurveys() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            IntegrationTestException
    {
        initialiseSurveyPage(SURVEY_1_ID);
        TestUtils.listItemsFound(format(JOB_SURVEY_PATH, JOB_1_ID), surveyPage, true, false);
    }

    /**
     * Test that an valid POST request for ../job/x/survey results in a positive response. Expecting new Survey
     * returned.
     */
    @Test
    public void createSurvey()
    {
        // Clear down the Survey ID so that we can create a new one
        surveyDto.setId(null);
        surveyDto.getScheduledService().setId(SERVICE_101_ID);
        surveyDto.getScheduledService().setServiceCatalogueId(SERVICE_243_ID);
        surveyDto.getServiceCatalogue().setId(SERVICE_243_ID);

        final SurveyDto newSurveyDto = TestUtils.postResponse(format(JOB_SURVEY_PATH, JOB_1_ID), surveyDto, HttpStatus.OK, SurveyDto.class);

        assertNotNull(newSurveyDto);
        assertNotNull(newSurveyDto.getId());
        assertEquals(JOB_1_ID, newSurveyDto.getJob().getId());
        final WorkItemQueryDto wipTaskQuery = new WorkItemQueryDto();
        wipTaskQuery.setSurveyId(new ArrayList<Long>());
        wipTaskQuery.getSurveyId().add(newSurveyDto.getId());

        final WorkItemPage wipTasks = TestUtils.postResponse(WIP_TASKS, wipTaskQuery, HttpStatus.OK, WorkItemPage.class);

        assertNotNull(wipTasks);
        assertNotNull(wipTasks.getContent());
        assertFalse(wipTasks.getContent().isEmpty());
    }

    /**
     * Test that an invalid POST request for ../job/x/survey results in a positive response. Expecting not found
     * exception, for an invalid jobId.
     */
    @Test
    public void createSurveyFailsJobNotFound()
    {
        TestUtils.postResponse(format(JOB_SURVEY_PATH, INVALID_ID), surveyDto, HttpStatus.NOT_FOUND);
    }

    @Test
    public void updateSurvey()
    {
        final SurveyDto existingSurvey = getExistingSurvey(SURVEY_1_ID);

        existingSurvey.getSurveyStatus().setId(ServiceCreditStatusType.COMPLETE.value());
        existingSurvey.setDateOfCrediting(new Date());

        final SurveyDto updatedSurveyDto = TestUtils.putResponse(format(JOB_SPECIFIC_SURVEY, JOB_1_ID, SURVEY_1_ID), existingSurvey, HttpStatus.OK,
                SurveyDto.class);

        assertNotNull(updatedSurveyDto);
        assertEquals(SURVEY_1_ID, updatedSurveyDto.getId());
        assertNotNull(updatedSurveyDto.getDateOfCrediting());
    }

    @Test
    public void updateSurveyFailsWithIncorrectHash()
    {
        final SurveyDto existingSurvey = getExistingSurvey(SURVEY_1_ID);

        existingSurvey.setDateOfCrediting(new Date());
        existingSurvey.setStalenessHash("Invalid Hash");

        final ErrorMessageDto error = TestUtils
                .putResponse(format(JOB_SPECIFIC_SURVEY, JOB_1_ID, SURVEY_1_ID), existingSurvey, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.SURVEY.getTypeString()),
                error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    private SurveyDto getExistingSurvey(final Long searchedSurveyId)
    {
        final SurveyPageResourceDto surveyPageResourceDto = TestUtils.getResponse(format(JOB_SURVEY_PATH, JOB_1_ID), HttpStatus.OK)
                .as(SurveyPageResourceDto.class);

        final SurveyDto existingSurvey = surveyPageResourceDto.getContent()
                .stream()
                .filter(survey -> survey.getId() == searchedSurveyId)
                .findFirst()
                .get();
        return existingSurvey;
    }

    @Test
    public void deleteSurvey() throws DatabaseUnitException
    {
        TestUtils.deleteResponse(format(JOB_SPECIFIC_SURVEY, JOB_1_ID, DELETE_SURVEY_ID), HttpStatus.OK);
        assertTrue(DataUtils.isDeleted("MAST_JOB_JobServiceInstance", DELETE_SURVEY_ID));
    }

    @Test
    public void updateSurveys()
    {
        final List<SurveyDto> surveyList = getSurveyListForJob(JOB_1_ID);
        surveyList.forEach(surveyDto1 -> {
            if (surveyDto1.getDateOfCrediting() == null && surveyDto1.getSurveyStatus().getId().equals(ServiceCreditStatusType.COMPLETE.value()))
            {
                surveyDto1.setDateOfCrediting(new Date());
            }
        });

        final SurveyDto surveyToChange = surveyList.get(0);
        final LinkResource originalStatus = surveyToChange.getSurveyStatus();

        // Change the service crediting status on the survey
        surveyToChange.setSurveyStatus(new LinkResource(2L));

        final SurveyListDto updatedSurveyList = new SurveyListDto();
        updatedSurveyList.setSurveys(surveyList);

        final SurveyListDto result = TestUtils.putResponse(String.format(JOB_SURVEY_PATH, JOB_1_ID), updatedSurveyList, HttpStatus.OK,
                SurveyListDto.class);

        assertNotNull(result);
        assertNotNull(result.getSurveys());
        notEmpty(result.getSurveys());
        assertEquals(new LinkResource(2L), result.getSurveys().get(0).getSurveyStatus());

        // Revert the service crediting status change
        result.getSurveys().get(0).setSurveyStatus(originalStatus);
        TestUtils.putResponse(String.format(JOB_SURVEY_PATH, JOB_1_ID), result, HttpStatus.OK);

    }

    @Test
    public void updateSurveysFailIfHashIsInvalid()
    {
        final List<SurveyDto> surveyList = getSurveyListForJob(JOB_1_ID);

        surveyList.forEach(surveyDto1 -> {
            if (surveyDto1.getDateOfCrediting() == null && surveyDto1.getSurveyStatus().getId().equals(ServiceCreditStatusType.COMPLETE.value()))
            {
                surveyDto1.setDateOfCrediting(new Date());
            }
        });

        final SurveyDto surveyToChange = surveyList.get(0);

        surveyToChange.setSurveyStatus(new LinkResource(2L));
        surveyToChange.setStalenessHash("Invalid Hash");

        final SurveyListDto updatedSurveyList = new SurveyListDto();
        updatedSurveyList.setSurveys(surveyList);

        final ErrorMessageDto error = TestUtils.putResponse(String.format(JOB_SURVEY_PATH, JOB_1_ID), updatedSurveyList, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.SURVEY.getTypeString()),
                error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    private List<SurveyDto> getSurveyListForJob(final Long jobId)
    {
        final SurveyPageResourceDto surveyPageResourceDto = TestUtils.getResponse(format(JOB_SURVEY_PATH, jobId), HttpStatus.OK)
                .as(SurveyPageResourceDto.class);

        return surveyPageResourceDto.getContent();
    }

    @Test
    public void updateSurveyIncludingANewSurvey() throws DatabaseUnitException
    {
        // Create a new survey based off an existing one.
        final SurveyDto newSurvey = DataUtils.getSurvey(SURVEY_1_ID);
        newSurvey.setId(null);

        final Long existingSurveyId = surveyListDto.getSurveys().get(0).getId();

        final SurveyDto existingSurvey = getExistingSurvey(existingSurveyId);

        final SurveyListDto newSurveyListDto = new SurveyListDto();
        newSurveyListDto.setSurveys(Arrays.asList(newSurvey, existingSurvey));

        final SurveyListDto result = TestUtils.putResponse(String.format(JOB_SURVEY_PATH, JOB_1_ID), newSurveyListDto, HttpStatus.OK,
                SurveyListDto.class);

        assertNotNull(result);
        assertNotNull(result.getSurveys());
        notEmpty(result.getSurveys());

        assertEquals(newSurveyListDto.getSurveys().size(), result.getSurveys().size());

        final SurveyDto firstResultSurvey = result.getSurveys().get(0);
        assertNotNull(firstResultSurvey.getId());

        final SurveyDto secondResultSurvey = result.getSurveys().get(1);

        assertEquals(existingSurvey, secondResultSurvey);

    }
}
