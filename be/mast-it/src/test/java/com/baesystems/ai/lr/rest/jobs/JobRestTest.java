package com.baesystems.ai.lr.rest.jobs;

import static com.baesystems.ai.lr.utils.TestUtils.parseDate;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.jobs.JobPageResourceDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.JobStatusType;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.JobPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.codicils.AssetNoteRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class JobRestTest extends BaseRestTest
{
    public static final String BASE_JOB = new StringBuffer().append(BASE_PATH).append("/job").toString();
    public static final String SPECIFIC_JOB = new StringBuffer(BASE_JOB).append("/%d").toString();
    public static final String SPECIFIC_JOB_SURVEY = new StringBuffer(SPECIFIC_JOB).append("/survey").toString();
    public static final String BASE_ASSET_JOB = new StringBuffer().append(BASE_PATH).append("/asset/%d/job").toString();
    public static final String JOB_EMPLOYEE = new StringBuffer().append(BASE_PATH).append("/job?employeeId=%d")
            .toString();
    public static final String JOB_OFFICE = new StringBuffer().append(BASE_PATH).append("/job?officeId=%d").toString();
    public static final String JOB_OFFICE_AND_EMPLOYEE = new StringBuffer().append(BASE_PATH)
            .append("/job?officeId=%d&employeeId=%d").toString();
    public static final String JOB_QUERY = new StringBuffer().append(BASE_JOB).append("/query").toString();
    public static final String JOB_ABSTRACT_QUERY = new StringBuffer().append(BASE_JOB).append("/abstract-query").toString();
    public static final String JOB_EMPLOYEE_QUERY = new StringBuffer().append(JOB_QUERY).append("?employee=%d").toString();
    public static final String JOB_TEAM_QUERY = new StringBuffer().append(JOB_QUERY).append("?team=%d").toString();
    public static final String JOB_EMPLOYEE_AND_TEAM_QUERY = new StringBuffer().append(JOB_QUERY).append("?employee=%d&team=%d").toString();
    public static final String WIP_AI = new StringBuffer().append(BASE_JOB).append("/%d/wip-actionable-item").toString();
    public static final String WIP_AN = new StringBuffer().append(BASE_JOB).append("/%d/wip-asset-note").toString();
    public static final String WIP_COC = new StringBuffer().append(BASE_JOB).append("/%d/wip-coc").toString();
    public static final String SURVEY = new StringBuffer().append(BASE_JOB).append("/%d/survey").toString();

    private static final Long ONE = 1L;
    private static final int FIVE = 5;

    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long JOB_3_ID = 3L;
    private static final Long JOB_4_ID = 4L;
    private static final Long JOB_5_ID = 5L;
    private static final Long JOB_6_ID = 6L;
    private static final Long JOB_7_ID = 7L;
    private static final Long JOB_8_ID = 8L;
    private static final Long JOB_9_ID = 9L;
    private static final Long JOB_10_ID = 10L;
    private static final Long JOB_11_ID = 11L;

    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_9_ID = 9L;
    private static final Long CASE_1_ID = 1L;
    private static final Long EMPLOYEE_1_ID = 1L;
    private static final Long EMPLOYEE_10_ID = 10L;
    private static final Long EMPLOYEE_11_ID = 11L;
    private static final Long EMPLOYEE_20_ID = 20L;
    private static final Long OFFICE_6_ID = 6L;
    private static final Long OFFICE_60_ID = 60L;
    private static final Long JOB_STATUS_1_ID = 1L;
    private static final Long JOB_STATUS_3_ID = 3L;
    private static final Long JOB_STATUS_4_ID = 4L;
    private static final Long JOB_STATUS_5_ID = 5L;
    private static final Long JOB_STATUS_11_ID = 11L;
    private static final Long INVALID_ID = 999L;
    private static final Long ZERO = 0L;
    private static final Long NEGATIVE = -1L;
    private static final Long SERVICE_CATALOGUE_ID = 1L;
    private static final Long SDO_OFFICE_ROLE_ID = 1L;

    private static final Long APPLICABLE_JOB_COUNT_EMPLOYEE_10 = 1L;
    private static final Long APPLICABLE_JOB_COUNT_EMPLOYEE_11 = 1L;

    private static final String UNEXPECTED_JOB_MESSAGE = "Unexpected Job ID returned. Expected ID %d, Actual ID %d, Job Status ID %d.";

    private JobDto jobDto;
    private JobPage jobPage;

    private final JobRestTestHelper jobRestTestHelper = new JobRestTestHelper();

    // Needs to be public for Theory to work.
    @DataPoints
    public static final String[] BAD_REQUESTS = {String.format(BASE_ASSET_JOB, ZERO.longValue()),
                                                 String.format(BASE_ASSET_JOB, NEGATIVE.longValue()), BASE_PATH + "/asset" + "/SomeLetters" + "/job"};

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.jobDto = DataUtils.getJob(JOB_1_ID);
        this.jobDto.setOffices(this.jobRestTestHelper.getOfficeList());
    }

    private JobDto getJob(final JobPage jobs, final Long jobId)
    {
        JobDto chosenJob = null;

        for (final JobDto job : jobs.getContent())
        {
            if (job.getId() == jobId)
            {
                chosenJob = job;
                break;
            }
        }

        return chosenJob;
    }

    private void initialiseJobPage(final Long... ids)
    {
        this.jobPage = new JobPage();

        JobDto tempJob;
        for (final Long id : ids)
        {
            try
            {
                tempJob = DataUtils.getJob(id);
                this.jobPage.getContent().add(tempJob);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    private void clearJobIds(final JobDto job)
    {
        job.setId(null);

        for (final OfficeLinkDto office : job.getOffices())
        {
            office.setId(null);
        }
        for (final EmployeeLinkDto employee : job.getEmployees())
        {
            employee.setId(null);
        }
    }

    /**
     * Test that a valid GET request for ../job?parameters results in a positive response. Expecting an error for a bad
     * request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual
    // feedback
    public final void ruleSetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    /**
     * Test that a valid GET request for ../job/{jobId} results in a positive response. Expecting job 1.
     */
    @Test
    public void getSingleJob()
    {
        this.jobDto.getOffices().get(0).setId(ONE); // this cannot be set in the creation of the dto initially because
        // that method is used for posts
        TestUtils.singleItemFound(format(SPECIFIC_JOB, JOB_1_ID), this.jobDto, true);
    }

    @Test
    public void getSingleJobInvalidId()
    {
        TestUtils.getResponse(format(SPECIFIC_JOB, INVALID_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid GET request for ../job results in a positive response. Expecting jobs 1 to 6.
     */
    @Test
    public void getAllJobs()
    {
        this.initialiseJobPage(JOB_1_ID, JOB_2_ID, JOB_3_ID, JOB_4_ID, JOB_5_ID, JOB_6_ID);
        TestUtils.listItemsFound(BASE_JOB, this.jobPage, true, false);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/job results in a positive response. Expecting job 1.
     */
    @Test
    public void getAllJobsForAsset()
    {
        this.initialiseJobPage(JOB_1_ID);
        TestUtils.listItemsFound(format(BASE_ASSET_JOB, ASSET_1_ID), this.jobPage, true, false);
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/job results in a positive response. Expecting a not found
     * exception.
     */
    @Test
    public void getAllJobsForAssetNotFound()
    {
        TestUtils.getResponse(format(BASE_ASSET_JOB, INVALID_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public void createEmptyJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = this.jobRestTestHelper.createSkeletonJob(ASSET_1_ID, CASE_1_ID, JOB_STATUS_1_ID);

        final JobDto newJob = TestUtils.postResponse(BASE_JOB, job, HttpStatus.OK, JobDto.class);

        assertNotNull(newJob);
        assertNotNull(newJob.getId());
        assertNotNull(newJob.getCreatedOn());
    }

    @Test
    public void createJobWithLinksTest()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.createJobWithLinks();
    }

    protected JobDto createJobWithLinks()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class), JOB_3_ID);
        this.clearJobIds(job);

        final JobDto newJob = TestUtils.postResponse(BASE_JOB, job, HttpStatus.OK, JobDto.class);

        assertNotNull(newJob);
        assertNotNull(newJob.getId());
        assertNotNull(newJob.getCreatedOn());
        assertNotNull(newJob.getOffices());
        assertFalse(newJob.getOffices().isEmpty());
        assertNotNull(newJob.getEmployees());
        assertFalse(newJob.getEmployees().isEmpty());
        return newJob;
    }

    @Test
    public void createJobWithAConfirmedFlag()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = this.createJobWithLinks();

        // Confirm that the flag defaults to false
        assertFalse(job.getScopeConfirmed());

        this.clearJobIds(job);
        job.setScopeConfirmed(true);

        final JobDto newJob = TestUtils.postResponse(BASE_JOB, job, HttpStatus.OK, JobDto.class);

        assertTrue(newJob.getScopeConfirmed());

        final JobDto jobFromDb = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class),
                newJob.getId());

        // Check that the job in the database has the flag set as true.
        assertTrue(jobFromDb.getScopeConfirmed());
    }

    @Test
    public void updateJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = this.createJobWithLinks();

        job.setNote("Something to change in the put.");

        final JobDto updatedJob = TestUtils.putResponse(String.format(SPECIFIC_JOB, job.getId()), job, HttpStatus.OK, JobDto.class);
        TestUtils.compareFields(updatedJob, job, true, new String[]{"stalenessHash"});
    }

    @Test
    public void updateJobFailsWhenOldTokenIsUsed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        JobDto job = this.createJobWithLinks();

        final String originalStalenessHash = job.getStalenessHash();

        // Make a small change
        job.setNote(job.getNote() + "X");
        final Response putResponse = TestUtils.putResponse(format(SPECIFIC_JOB, job.getId()), job, HttpStatus.OK);
        job = putResponse.jsonPath().getObject("", JobDto.class);

        // Make another small change, but use the hash from the original GET
        job.setNote(job.getNote() + "X");
        job.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(SPECIFIC_JOB, job.getId()), job, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.JOB.getTypeString()), error.getMessage());
    }

    @Test
    public void updateJobInvalidId()
    {
        this.jobDto.setId(INVALID_ID);
        this.jobDto.setScopeConfirmed(Boolean.FALSE);
        TestUtils.putResponse(String.format(SPECIFIC_JOB, INVALID_ID), this.jobDto, HttpStatus.NOT_FOUND);
    }

    @Test
    public void updateJobWithAConfirmedFlag()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = this.createJobWithLinks();

        job.setScopeConfirmed(true);

        final JobDto updatedJob = TestUtils.putResponse(String.format(SPECIFIC_JOB, job.getId()), job, HttpStatus.OK, JobDto.class);
        TestUtils.compareFields(updatedJob, job, true, new String[]{"stalenessHash"});

        final JobDto jobFromDb = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class),
                job.getId());

        // Check that the job in the database has the flag set as true.
        assertTrue(jobFromDb.getScopeConfirmed());
    }

    @Test
    public void updateJobAndCodicilsWithConfirmedFlag() throws InvocationTargetException, NoSuchMethodException,
            IntegrationTestException, IllegalAccessException, DatabaseUnitException
    {
        final JobDto job = this.createJobWithLinks();
        final List<AssetNoteDto> anDtos = createWIPAssetNotes(3, job.getId());
        final List<ActionableItemDto> aiDtos = createActionableItems(5, job.getId());
        final List<CoCDto> cocDtos = createCoCs(7, job.getId());
        final List<SurveyDto> surveyDtos = createSurveys(4, job.getId());
        surveyDtos.stream().forEach(dto -> dto.setJobScopeConfirmed(false));

        // To get the above surveys on the job I have to retrieve the job from
        // BE - JobDto accepts only SurveyDto
        // but the PUT
        // i.e. update, requires SurveyDto. Using SurveyDto in the PUT causes
        // validation error - SurveyDto
        // does not
        // contain the JobId. If I GET the job, I get the job with the surveys as SurveyDto and hence can update.
        final JobDto testJob = TestUtils.getResponse(String.format(SPECIFIC_JOB, job.getId()), HttpStatus.OK, JobDto.class);
        testJob.setScopeConfirmed(true);

        // Now actually do the update
        final JobDto updatedJob = TestUtils.putResponse(String.format(SPECIFIC_JOB, testJob.getId()), testJob, HttpStatus.OK, JobDto.class);

        // The List<OfficeLinkDto> will fail compareFields() - 'name' is not set
        // in each office in updatedJob but the ID
        // is.
        // Circumvent #1 - force both office lists to be the same.
        updatedJob.setOffices(new ArrayList<>(testJob.getOffices()));

        TestUtils.compareFields(updatedJob, testJob, true, new String[]{"stalenessHash"});

        final JobDto jobFromDb = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class),
                job.getId());

        // Check that the job in the database has the flag set as true.
        assertTrue(jobFromDb.getScopeConfirmed());

        for (final AssetNoteDto anDto : anDtos)
        {
            final AssetNoteDto storedAnDto = DataUtils.getAssetNote(anDto.getId(), true);
            assertTrue(storedAnDto.getJobScopeConfirmed());
        }

        for (final ActionableItemDto aiDto : aiDtos)
        {
            final ActionableItemDto storedAiDto = DataUtils.getActionableItem(aiDto.getId(), true);
            assertTrue(storedAiDto.getJobScopeConfirmed());
        }

        for (final CoCDto cocDto : cocDtos)
        {
            final CoCDto storedCocDto = DataUtils.getCoC(cocDto.getId(), true);
            assertTrue(storedCocDto.getJobScopeConfirmed());
        }

        for (final SurveyDto surveyDto : surveyDtos)
        {
            final SurveyDto storedSurveyDto = DataUtils.getSurvey(surveyDto.getId());
            assertTrue(storedSurveyDto.getJobScopeConfirmed());
        }
    }

    @Test
    public void testAuditingByUpdatingJob() throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, IntegrationTestException, DatabaseUnitException, InterruptedException
    {
        TestUtils.flushCookies();
        final String creatingUser = "Creating User";
        final String updatingUser = "Jing Yu";
        TestUtils.addCustomHeader(new Header("user", creatingUser));

        // Create Job
        final JobDto job = this.createJobWithLinks();

        // Get Audit for Last Updated By and Updated Time
        final Pair<String, Date> afterCreation = DataUtils.getAudit("MAST_JOB_Job", job.getId());

        Assert.assertEquals(creatingUser, afterCreation.getLeft());

        TestUtils.flushCookies();
        TestUtils.addCustomHeader(new Header("user", updatingUser));
        job.setNote("I have been updated");

        final Integer oneThousandMs = 1000;
        Thread.sleep(oneThousandMs); // Too fast and the last update date delta
        // won't be detectable

        final JobDto updatedJob = TestUtils.putResponse(String.format(SPECIFIC_JOB, job.getId()), job, HttpStatus.OK, JobDto.class);

        // Compare Dto update
        TestUtils.compareFields(updatedJob, job, true, new String[]{"stalenessHash"});

        // Verify Audit columns have been updated.
        final Pair<String, Date> afterUpdate = DataUtils.getAudit("MAST_JOB_Job", job.getId());

        Assert.assertEquals(updatingUser, afterUpdate.getLeft());
        Assert.assertNotEquals(afterCreation.getRight(), afterUpdate.getRight());
        TestUtils.addCustomHeader(null); // have to manually clear as static
        // reference will remain between
        // test classes
    }

    @Test
    public void updateClosedJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        // create new to avoid damaging database
        final JobDto job = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class), JOB_2_ID);

        this.clearJobIds(job);
        job.getJobStatus().setId(JobStatusType.CLOSED.getValue());

        final JobDto newJob = TestUtils.postResponse(BASE_JOB, job, HttpStatus.OK, JobDto.class);

        newJob.setStatusReason("To see if it will update.");

        TestUtils.putResponse(String.format(SPECIFIC_JOB, newJob.getId().intValue()), newJob, HttpStatus.BAD_REQUEST);
    }

    @Test
    public void updateJobVisitDates()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = TestUtils.getResponse(String.format(SPECIFIC_JOB, JOB_2_ID), JobDto.class);

        job.setFirstVisitDate(parseDate("2016-07-11"));
        job.setLastVisitDate(parseDate("2016-07-12"));

        final JobDto updatedJob = TestUtils.putResponse(String.format(SPECIFIC_JOB, job.getId()), job, HttpStatus.OK, JobDto.class);
        TestUtils.compareFields(updatedJob, job, true, new String[]{"stalenessHash"});
    }

    @Test
    public void updateJobVisitDatesLastVisitBeforeFirst()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final JobDto job = TestUtils.getResponse(String.format(SPECIFIC_JOB, JOB_2_ID), JobDto.class);

        job.setFirstVisitDate(parseDate("2016-07-12"));
        job.setLastVisitDate(parseDate("2016-07-11"));

        final ErrorMessageDto error = TestUtils.putResponse(String.format(SPECIFIC_JOB, job.getId()), job, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        assertEquals("Validation failed with errors:  The last visit date cannot be before the first visit date.",
                error.getMessage());
    }

    /**
     * Test that a valid GET request for ../job?employeeId results in a positive response. Expecting jobs 1, 2 and 3.
     */
    @Test
    public void getAllJobsForEmployee()
    {
        this.initialiseJobPage(JOB_1_ID, JOB_2_ID, JOB_3_ID);
        TestUtils.listItemsFound(String.format(JOB_EMPLOYEE, EMPLOYEE_1_ID), this.jobPage, true, false);
    }

    /**
     * Test that a valid GET request for ../job?officeId results in a positive response. Expecting jobs 2, and 5.
     */
    @Test
    public void getAllJobsForOffice()
    {
        this.initialiseJobPage(JOB_2_ID, JOB_5_ID);
        TestUtils.listItemsFound(String.format(JOB_OFFICE, OFFICE_6_ID), this.jobPage, true, false);
    }

    /**
     * Test that a valid GET request for ../job?officeId&employeeId results in a positive response. Expecting job 2.
     */
    @Test
    public void getAllJobsForOfficeAndEmployee()
    {
        this.initialiseJobPage(JOB_2_ID);
        TestUtils.listItemsFound(String.format(JOB_OFFICE_AND_EMPLOYEE, OFFICE_6_ID, EMPLOYEE_1_ID), this.jobPage, true, false);
    }

    /**
     * Test that a valid POST request for ../job/query with dates earlier than our known test data set returns no jobs.
     */
    @Test
    public void queryNoJobsWhenPassedOldDates()
    {
        final String oldDateString = "2015-10-31";

        final JobQueryDto query = this.createQuery(null, null, Arrays.asList(OFFICE_6_ID), null, null, oldDateString,
                oldDateString);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        assertTrue(results.getContent().isEmpty());
    }

    /**
     * Test that a valid POST request for ../job/query with dates later than our known test data set returns no jobs.
     */
    @Test
    public void queryNoJobsWhenPassedFutureDates()
    {
        final String newDateString = "2015-11-02";

        final JobQueryDto query = this.createQuery(null, null, Arrays.asList(OFFICE_6_ID), null, null, newDateString,
                newDateString);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        assertTrue(results.getContent().isEmpty());
    }

    /**
     * Test that an invalid POST request for ../job/query passing a to date later than a from date does not cause a
     * catastrophic failure.
     */
    @Test
    public void queryNoJobsWhenToAndFromDatesSwapped()
    {
        final String oldDateString = "2015-10-31";
        final String newDateString = "2015-11-02";

        final JobQueryDto query = this.createQuery(null, null, Arrays.asList(OFFICE_6_ID), null, null, newDateString,
                oldDateString);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        assertTrue(results.getContent().isEmpty());
    }

    /**
     * Test that a valid POST request for ../job/query with a single date results in a positive response.
     */
    @Test
    public void queryJobsForOfficeAndDateRange()
    {
        final String dateString = "2015-11-14";

        final JobQueryDto query = this.createQuery(null, null, Arrays.asList(OFFICE_6_ID), null, null, dateString,
                dateString);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        jobs.forEach(job -> assertTrue(DateUtils.isSameDay(query.getStartDateMin(), job.getEtaDate())));
    }

    /**
     * Test that a valid POST request for ../job/query with a single date results in a positive response.
     */
    @Test
    public void abstractQueryJobsForOfficeAndDateRange() throws ParseException
    {
        final String dateString = "2015-11-14";

        final AbstractQueryDto query = new AbstractQueryDto();
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();

        query1.setField("etaDate");
        query1.setValue(parseDate(dateString));
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setJoinField("offices");
        query2.setField("office.id");
        query2.setValue(OFFICE_6_ID);
        query2.setRelationship(QueryRelationshipType.EQ);

        query.setAnd(new ArrayList<AbstractQueryDto>());
        query.getAnd().add(query1);
        query.getAnd().add(query2);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_ABSTRACT_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        jobs.stream().forEach(job -> assertTrue(DateUtils.isSameDay(parseDate(dateString), job.getEtaDate())));
    }

    /**
     * Test that a valid POST request for ../job/query with an employee id results in a positive response.
     */
    @Test
    public void queryJobsForAnEmployee()
    {
        final AbstractQueryDto query = new AbstractQueryDto();

        query.setJoinField("employees");
        query.setField("lrEmployee.id");
        query.setValue(Arrays.asList(EMPLOYEE_1_ID));
        query.setRelationship(QueryRelationshipType.IN);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_ABSTRACT_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        jobs.stream().forEach(job -> this.checkThatTheEmployeeIsPresent(job, EMPLOYEE_1_ID));
    }

    /**
     * Test that a valid POST request for ../job/query with an employee id results in a positive response.
     */
    @Test
    public void abstractQueryJobsForAnEmployee()
    {
        final JobQueryDto query = this.createQuery(null, null, null, Arrays.asList(EMPLOYEE_1_ID), null, null, null);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        jobs.stream().forEach(job -> this.checkThatTheEmployeeIsPresent(job, EMPLOYEE_1_ID));
    }

    /**
     * Test that a valid POST request for ../job/abstract-query with a date range for asset build date results in a
     * positive response.
     */
    @Test
    public void abstractQueryJobsForAssetsInBuildDateRange() throws ParseException
    {
        final AbstractQueryDto query = new AbstractQueryDto();
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();

        query1.setField("asset.buildDate");
        query1.setValue(parseDate("2014-01-01"));
        query1.setRelationship(QueryRelationshipType.LT);

        query2.setField("asset.buildDate");
        query2.setValue(parseDate("2013-01-01"));
        query2.setRelationship(QueryRelationshipType.GE);

        query.setAnd(new ArrayList<AbstractQueryDto>());
        query.getAnd().add(query1);
        query.getAnd().add(query2);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_ABSTRACT_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertEquals(FIVE, jobs.size());
    }

    // TODO An excellent candidate for lambdas/streams when we migrate to Java
    // 8.
    private void checkThatTheEmployeeIsPresent(final JobDto job, final Long employeeId)
    {
        boolean missingEmployee = true;
        for (final EmployeeLinkDto employee : job.getEmployees())
        {
            if (employeeId.equals(employee.getLrEmployee().getId()))
            {
                missingEmployee = false;
                break;
            }
        }
        if (missingEmployee)
        {
            Assert.fail(
                    "A job has been returned that does not contain the requested employee. Job id : " + job.getId());
        }
    }

    // TODO An excellent candidate for lambdas/streams when we migrate to Java
    // 8.
    private void checkThatTheOfficeIsPresent(final JobDto job, final Long officeId)
    {
        boolean missingOffice = true;
        for (final OfficeLinkDto office : job.getOffices())
        {
            if (officeId.equals(office.getOffice().getId()))
            {
                missingOffice = false;
                break;
            }
        }
        if (missingOffice)
        {
            Assert.fail("A job has been returned that does not contain the requested office. Job id : " + job.getId());
        }
    }

    /**
     * Test that a valid POST request for ../job/query with a job status results in a positive response.
     */
    @Test
    public void queryJobsForAStatus()
    {
        final JobQueryDto query = this.createQuery(null, null, null, null, Arrays.asList(JOB_STATUS_3_ID), null, null);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        for (final JobDto job : jobs)
        {
            Assert.assertEquals(JOB_STATUS_3_ID, job.getJobStatus().getId());
        }
    }

    /**
     * Test that only jobs for a specific employee are returned that are relevant to their role. Story DB.01
     * LRD-3939/4102 AC 12
     */
    @Test
    public void queryForEmployeeJobs()
    {
        final JobQueryDto query = this.createQuery(null, null, null, null, null, null, null);

        final JobPageResourceDto results = TestUtils.postResponse(format(JOB_EMPLOYEE_QUERY, EMPLOYEE_10_ID), query, HttpStatus.OK,
                JobPageResourceDto.class);
        final List<JobDto> jobs = results.getContent();
        assertTrue(jobs.size() == APPLICABLE_JOB_COUNT_EMPLOYEE_10);

        // For Employee 10 I only expect 1 Job of ID #9, status UNDER_REPORTING. Anything else is a failed test.
        for (final JobDto job : jobs)
        {
            switch (JobStatusType.getJobStatusType(job.getJobStatus().getId()))
            {
                case UNDER_REPORTING:
                    Assert.assertEquals(format(UNEXPECTED_JOB_MESSAGE, JOB_9_ID, job.getId(), job.getJobStatus().getId()), JOB_9_ID, job.getId());
                    break;
                default:
                    fail(format(UNEXPECTED_JOB_MESSAGE, JOB_9_ID, job.getId(), job.getJobStatus()));
                    break;
            }
        }
    }

    /**
     * Tests that a job is available for a particular employee and is then no longer applicable if job status is
     * changed. Story DB.01 LRD-3939/4102 AC 12
     */
    @Test
    public void queryForEmployeeJobsAndChangeJobStatus() throws DatabaseUnitException
    {
        final JobQueryDto query = this.createQuery(null, null, null, null, null, null, null);

        final JobPageResourceDto results = TestUtils.postResponse(format(JOB_EMPLOYEE_QUERY, EMPLOYEE_11_ID), query, HttpStatus.OK,
                JobPageResourceDto.class);
        final List<JobDto> jobs = results.getContent();
        assertTrue(jobs.size() == APPLICABLE_JOB_COUNT_EMPLOYEE_11);

        // For Employee 11 I only expect APPLICABLE_JOB_COUNT_EMPLOYEE_11 job of ID #11, status AWAITING_TR_ASSIGNMENT.
        // Anything else is a failed test.
        for (final JobDto job : jobs)
        {
            switch (JobStatusType.getJobStatusType(job.getJobStatus().getId()))
            {
                case AWAITING_TECHNICAL_REVIEWER_ASSIGNMENT:
                    Assert.assertEquals(format(UNEXPECTED_JOB_MESSAGE, JOB_11_ID, job.getId(), job.getJobStatus().getId()), JOB_11_ID, job.getId());
                    break;
                default:
                    fail(format(UNEXPECTED_JOB_MESSAGE, JOB_11_ID, job.getId(), job.getJobStatus()));
                    break;
            }
        }

        // Now change the job status of #11 so that is is no longer applicable to to employee #11
        final JobDto job11Dto = TestUtils.getResponse(format(SPECIFIC_JOB, JOB_11_ID), HttpStatus.OK).as(JobDto.class);
        job11Dto.getJobStatus().setId(JobStatusType.AWAITING_ENDORSER_ASSIGNMENT.getValue());
        TestUtils.putResponse(format(SPECIFIC_JOB, JOB_11_ID), job11Dto, HttpStatus.OK);

        // Query again
        final JobPageResourceDto results2 = TestUtils.postResponse(format(JOB_EMPLOYEE_QUERY, EMPLOYEE_11_ID), query, HttpStatus.OK,
                JobPageResourceDto.class);

        assertTrue(results2.getContent().isEmpty());

        // Restore job 11 - need to fetch again due to staleness check.
        final JobDto job11UpdateDto = TestUtils.getResponse(format(SPECIFIC_JOB, JOB_11_ID), HttpStatus.OK).as(JobDto.class);
        job11UpdateDto.getJobStatus().setId(JobStatusType.AWAITING_TECHNICAL_REVIEWER_ASSIGNMENT.getValue());
        TestUtils.putResponse(format(SPECIFIC_JOB, JOB_11_ID), job11UpdateDto, HttpStatus.OK);
    }

    /**
     * Test failure results if specifying employee and team on query. Story DB.01 LRD-3939/4102
     */
    @Test
    public void verifyQueryWithEmployeeAndTeamFails()
    {
        final JobQueryDto query = this.createQuery(null, null, null, null, null, null, null);

        final JobPageResourceDto results = TestUtils.postResponse(format(JOB_EMPLOYEE_AND_TEAM_QUERY, EMPLOYEE_10_ID, EMPLOYEE_10_ID),
                query, HttpStatus.BAD_REQUEST, JobPageResourceDto.class);
    }

    /**
     * Test that a valid POST request for ../job/query with a Job Number as the search string results in a positive
     * response.
     */
    @Test
    public void queryJobsUsingSearchString()
    {
        final JobQueryDto query = this.createQuery(JOB_1_ID, null, null, null, null, null, null);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        for (final JobDto job : jobs)
        {
            Assert.assertEquals(JOB_1_ID, job.getId());
        }

    }

    /**
     * Test that a valid POST request for ../job/query with all criteria set that results in a positive response.
     */
    @Test
    public void queryJobsUsingAllCriteria()
    {
        final String dateString = "2015-11-01";

        final JobQueryDto query = this.createQuery(null, Arrays.asList(ASSET_1_ID), Arrays.asList(OFFICE_60_ID),
                Arrays.asList(EMPLOYEE_1_ID), Arrays.asList(JOB_STATUS_3_ID), dateString, dateString);

        final JobPageResourceDto results = TestUtils.postResponse(JOB_QUERY, query, HttpStatus.OK,
                JobPageResourceDto.class);

        final List<JobDto> jobs = results.getContent();
        assertFalse(jobs.isEmpty());

        for (final JobDto job : jobs)
        {
            Assert.assertEquals(ASSET_1_ID, job.getAsset().getId());
            this.checkThatTheEmployeeIsPresent(job, EMPLOYEE_1_ID);
            this.checkThatTheOfficeIsPresent(job, OFFICE_60_ID);
            assertTrue(DateUtils.isSameDay(query.getStartDateMin(), job.getEtaDate()));
        }
    }

    @Test
    public void testCloseJobWithNoFollowUpActions() throws DatabaseUnitException
    {
        final JobDto closeThisJobDto = this.getJob(TestUtils.getResponse(BASE_JOB, HttpStatus.OK).getBody().as(JobPage.class), JOB_7_ID);

        final OfficeLinkDto sdo = new OfficeLinkDto();
        sdo.setOffice(new LinkResource(OFFICE_6_ID));
        sdo.setOfficeRole(new LinkResource(SDO_OFFICE_ROLE_ID));
        closeThisJobDto.setOffices(Arrays.asList(sdo));
        closeThisJobDto.setScopeConfirmed(false);
        closeThisJobDto.getJobStatus().setId(JobStatusType.CLOSED.getValue());
        closeThisJobDto.setCompletedOn(new Date());

        TestUtils.putResponse(String.format(SPECIFIC_JOB, JOB_7_ID), closeThisJobDto, HttpStatus.OK);

        assertEquals("Job Status is not closed.", JobStatusType.CLOSED.getValue(),
                DataUtils.getJob(JOB_7_ID).getJobStatus().getId());
    }

    @Test
    public void testTryClosingJobWithFollowUpActions() throws DatabaseUnitException
    {
        final JobDto closeThisJobDto = DataUtils.getJob(JOB_8_ID);
        final OfficeLinkDto sdo = new OfficeLinkDto();
        sdo.setOffice(new LinkResource(OFFICE_6_ID));
        sdo.setOfficeRole(new LinkResource(SDO_OFFICE_ROLE_ID));
        closeThisJobDto.setOffices(Arrays.asList(sdo));
        closeThisJobDto.setScopeConfirmed(false);
        closeThisJobDto.getJobStatus().setId(JobStatusType.CLOSED.getValue());
        closeThisJobDto.setCompletedOn(new Date());

        TestUtils.putResponse(String.format(SPECIFIC_JOB, JOB_8_ID), closeThisJobDto, HttpStatus.CONFLICT);

        assertNotEquals("Job should not be CLOSED", JobStatusType.CLOSED.getValue(),
                DataUtils.getJob(JOB_8_ID).getJobStatus().getId());
    }

    private JobQueryDto createQuery(final Long searchString, final List<Long> assetIds, final List<Long> officeIds,
            final List<Long> employeeIds, final List<Long> jobStatuses, final String oldDateString,
            final String newDateString)
    {
        final JobQueryDto query = new JobQueryDto();

        query.setSearch(searchString);
        query.setStartDateMin(parseDate(oldDateString));
        query.setStartDateMax(parseDate(newDateString));
        query.setOfficeId(officeIds);
        query.setAssetId(assetIds);
        query.setJobStatusId(jobStatuses);
        query.setEmployeeId(employeeIds);

        return query;
    }

    private static List<AssetNoteDto> createWIPAssetNotes(final int count, final Long jobId)
    {
        final List<AssetNoteDto> assetNoteDtos = new ArrayList<>();
        final AssetNoteDto assetNoteDto = AssetNoteRestTest.getAssetNoteForWipPost();
        assetNoteDto.setJob(new LinkResource(jobId));
        assetNoteDto.setTitle("This AN is for test");
        assetNoteDto.setConfidentialityType(new LinkResource(1L));
        assetNoteDto.setCategory(new LinkResource(1L));
        assetNoteDto.setStatus(new LinkResource(CodicilStatusType.AN_OPEN.getValue()));
        assetNoteDto.setJobScopeConfirmed(false);

        for (int index = 0; index < count; index++)
        {
            assetNoteDto.setDescription("Created in JobRestTest #" + index);
            final AssetNoteDto body = TestUtils.postResponse(format(WIP_AN, jobId), assetNoteDto, HttpStatus.OK, AssetNoteDto.class);
            assetNoteDtos.add(body);
        }
        return assetNoteDtos;
    }

    private static List<ActionableItemDto> createActionableItems(final int count, final Long jobId)
    {
        final List<ActionableItemDto> actionableItemDtos = new ArrayList<>();
        final ActionableItemDto aiDto = createActionableItem();
        aiDto.getJob().setId(jobId);
        aiDto.setJobScopeConfirmed(false);

        for (int index = 0; index < count; index++)
        {
            aiDto.setDescription("Created in JobRestTest #" + index);
            final ActionableItemDto body = TestUtils.postResponse(format(WIP_AI, jobId), aiDto, HttpStatus.OK, ActionableItemDto.class);
            actionableItemDtos.add(body);
        }

        return actionableItemDtos;
    }

    private static List<CoCDto> createCoCs(final int count, final Long jobId)
    {
        final List<CoCDto> coCDtos = new ArrayList<>();
        final CoCDto coCDto = createCoC();
        coCDto.getJob().setId(jobId);
        coCDto.setJobScopeConfirmed(false);

        for (int index = 0; index < count; index++)
        {
            coCDto.setDescription("Created in JobRestTest #" + index);
            final CoCDto body = TestUtils.postResponse(format(WIP_COC, jobId), coCDto, HttpStatus.OK, CoCDto.class);
            coCDtos.add(body);
        }

        return coCDtos;
    }

    public static List<SurveyDto> createSurveys(final int count, final Long jobId)
    {
        final List<SurveyDto> surveyDtos = new ArrayList<>();
        final SurveyDto surveyDto = createSurvey();
        surveyDto.getJob().setId(jobId);

        for (int index = 0; index < count; index++)
        {
            surveyDto.setName("Created in JobRestTest survey #" + index);
            final SurveyDto body = TestUtils.postResponse(format(SURVEY, jobId), surveyDto, HttpStatus.OK, SurveyDto.class);
            surveyDtos.add(body);
        }

        return surveyDtos;
    }

    public static ActionableItemDto createActionableItem()
    {
        final ActionableItemDto aiDto = new ActionableItemDto();
        aiDto.setJob(new LinkResource());
        aiDto.setCategory(new LinkResource(1L));
        aiDto.setConfidentialityType(new LinkResource(1L));
        aiDto.setDescription("JobRestTest Description");
        aiDto.setTitle("JobRestTest Title");
        aiDto.setStatus(new LinkResource(CodicilStatusType.AI_OPEN.getValue()));
        aiDto.setImposedDate(new Date());
        aiDto.setDueDate(new Date());
        aiDto.setRequireApproval(false);
        aiDto.setJobScopeConfirmed(false);

        return aiDto;
    }

    public static CoCDto createCoC()
    {
        final CoCDto cocDto = new CoCDto();
        cocDto.setJob(new LinkResource());
        cocDto.setCategory(new LinkResource(1L));
        cocDto.setConfidentialityType(new LinkResource(1L));
        cocDto.setDescription("JobRestTest Description");
        cocDto.setTitle("JobRestTest Title");
        cocDto.setStatus(new LinkResource(CodicilStatusType.COC_OPEN.getValue()));
        cocDto.setImposedDate(new Date());
        cocDto.setDueDate(new Date());
        cocDto.setRequireApproval(false);
        cocDto.setJobScopeConfirmed(false);
        cocDto.setInheritedFlag(false);

        return cocDto;
    }

    public static SurveyDto createSurvey()
    {
        final SurveyDto surveyDto = new SurveyDto();
        surveyDto.setJob(new LinkResource());
        surveyDto.setServiceCatalogue(new LinkResource(SERVICE_CATALOGUE_ID));
        surveyDto.setCreditedBy(new LinkResource(1L));
        surveyDto.setScheduleDatesUpdated(false);
        surveyDto.setJobScopeConfirmed(false);
        surveyDto.setSurveyStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));

        return surveyDto;
    }
}
