package com.baesystems.ai.lr.utils;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jayway.restassured.response.Headers;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.base.IdDto;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.BaseTestPage;
import com.baesystems.ai.lr.utils.exception.ComparisonException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

public final class TestUtils
{
    private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);

    private static final String EXPECTED_OBJECT_NOT_FOUND = "The expected object with ID {0} was not found.";
    private static final String UNEXPECTED_NON_EMPTY_COLLECTION = "Expected empty \"{0}\" however got {1}";
    private static final String COLLECTION_NAME_PAGE = "Page";
    private static final SecureURLHelper SECUREURL = new SecureURLHelper();

    private TestUtils()
    {
        // Prevents instantiation
    }

    /*
     * Top level public test wrapper methods
     */

    /**
     * Submits a GET request to the server and expects a single JSON object to be returned.<br>
     * Should also be used for requests which return a single piece of reference data.
     *
     * @param getString the HTTP GET request string
     * @param expectedDto the expected resulting DTO
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     */
    public static void singleItemFound(final String getString, final Object expectedDto, final boolean allowNull)
    {
        singleItemFound(getString, expectedDto, allowNull, null);
    }

    public static void singleItemFound(final String getString, final Object expectedDto, final boolean allowNull, final String[] ignored)
    {
        jsonSingleItemTest(getString, Arrays.asList(HttpStatus.OK), expectedDto, allowNull, ignored);
    }

    /**
     * Submits a GET request to the server and expects a 404 error in response.
     *
     * @param getString the HTTP GET request string
     */
    public static void singleItemNotFound(final String getString)
    {
        jsonSingleItemTest(getString, Arrays.asList(HttpStatus.NOT_FOUND), null, true);
    }

    /**
     * Submits a GET request to the server and expects a 400 error in response.
     *
     * @param getString the HTTP GET request string
     */
    public static void singleItemBadRequest(final String getString)
    {
        jsonSingleItemTest(getString, Arrays.asList(HttpStatus.BAD_REQUEST), null, true);
    }

    /**
     * Submits a GET request to the server and expects a paginated list of JSON objects in response.
     *
     * @param getString the HTTP GET request string
     * @param expectedPage the expected resulting paginated list of DTOs
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @param strictContentSize whether or not to assert the size of the contents list against the contents list in the
     *        expectedDto. This should be set to true when an empty contents list is expected
     */
    public static void listItemsFound(final String getString, final BaseTestPage<?> expectedPage,
            final boolean allowNull, final boolean strictContentSize)
    {
        jsonPaginationTest(getString, HttpStatus.OK, expectedPage, allowNull, strictContentSize);
    }

    /**
     * Submits a GET request to the server and expects an empty paginated list in response.
     *
     * @param getString the HTTP GET request string
     */
    public static void listItemsNotFound(final String getString)
    {
        jsonPaginationTest(getString, HttpStatus.OK, new BaseTestPage<BaseDto>(), true, true);
    }

    /**
     * Submits a GET request to the server and expects a list of JSON objects in response.
     *
     * @param getString the HTTP GET request string
     * @param expectedValues the expected resulting list of reference DTOs (should be supplied as an array)
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     */
    public static void referenceDataListFound(final String getString, final Object[] expectedValues, final boolean allowNull)
    {
        jsonReferenceDataTest(getString, HttpStatus.OK, expectedValues, allowNull, false);
    }

    /*
     * Top level private test methods
     */

    /**
     * Gets a response from the server with the request in {@code getString} and asserts the received HttpStatus against
     * the expected status. If a JSON object is expected in response, it is compared against the values in the
     * expectedDto.<br>
     * <br>
     * See {@link TestUtils#jsonPageTest(String, HttpStatus, BaseTestPage, boolean, boolean)} for results that will be
     * paginated.<br>
     * See {@link TestUtils#jsonRefTest(String, HttpStatus, Object[], boolean, boolean)} for results from the Reference
     * APIs.
     *
     * @param getString the query String used when contacting the server
     * @param expectedStatus the expected HttpStatus of the response
     * @param expectedDto the expected return object (should be set to {@code null} for no returned object)
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     */
    private static void jsonSingleItemTest(final String getString, final List<HttpStatus> expectedStatus,
            final Object expectedDto, final boolean allowNull)
    {
        jsonSingleItemTest(getString, expectedStatus, expectedDto, allowNull, null);
    }

    private static void jsonSingleItemTest(final String getString, final List<HttpStatus> expectedStatus,
            final Object expectedDto, final boolean allowNull, final String[] ignored)
    {
        final Response response = getResponse(getString, expectedStatus);

        if (expectedDto != null)
        {
            try
            {
                final Object responseObj = response.getBody().as(expectedDto.getClass(), ObjectMapperType.GSON);
                assertNotNull(responseObj);
                compareFields(responseObj, expectedDto, allowNull, ignored);
            }
            catch (IllegalAccessException | InvocationTargetException
                    | NoSuchMethodException | IntegrationTestException excep)
            {
                fail("Exception thrown: " + excep.getMessage());
            }
        }
    }

    /**
     * Gets a response from the server with the request in {@code getString} and asserts the received HttpStatus against
     * the expected status. A JSON object is always expected to be present from the HTTP GET statement, and this method
     * will throw an AssertionExcepion if there isn't one. The contents is compared to the contents section in the
     * expectedPage. All elements should be in same order in both the response and expected contents, however if
     * strictContentSize is set to {@code false}, then only the items present in the expected contents will be checked
     * if there are fewer of them than in the received contents. This method will always fail if there are more expected
     * contents items than received contents items.<br>
     * <br>
     * See {@link TestUtils#jsonTest(String, HttpStatus, Object, boolean)} for results that might not include a JSON
     * object.<br>
     * See {@link TestUtils#jsonRefTest(String, HttpStatus, Object[], boolean, boolean)} for results from the Reference
     * APIs.
     *
     * @param getString the query String used when contacting the server
     * @param expectedStatus the expected HttpStatus of the response
     * @param expectedPage the expected return object (should be set to {@code null} for no returned object)
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @param strictContentSize whether or not to assert the size of the contents list against the contents list in the
     *        expectedDto. This should be set to true when an empty contents list is expected
     */
    private static void jsonPaginationTest(final String getString, final HttpStatus expectedStatus,
            final BaseTestPage<?> expectedPage, final boolean allowNull, final boolean strictContentSize)
    {
        final Response response = getResponse(getString, expectedStatus);

        try
        {
            final BaseTestPage<?> page = response.getBody().as(expectedPage.getClass(), ObjectMapperType.GSON);
            assertNotNull(page);
            assertNotNull(page.getContent());

            if (strictContentSize)
            {
                assertTrue(page.getContent().size() == expectedPage.getContent().size());
            }
            else
            {
                assertTrue(page.getContent().size() >= expectedPage.getContent().size());
            }

            for (int count = 0; count < expectedPage.getContent().size(); count++)
            {
                compareCollection(page.getContent(), expectedPage.getContent(), allowNull, COLLECTION_NAME_PAGE);
            }
        }
        catch (ClassCastException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException | IntegrationTestException excep)
        {
            fail("Exception thrown: " + excep.getMessage());
        }
    }

    /**
     * Gets a response from the server with the request in {@code getString} and asserts the received HttpStatus against
     * the expected status. A JSON object is always expected to be present from the HTTP GET statement, and this method
     * will throw an AssertionExcepion if there isn't one. The returned list is compared against the expectedValues. All
     * elements should be in same order in both the response and expected contents, however if strictContentSize is set
     * to {@code false}, then only the items present in the expected contents will be checked if there are fewer of them
     * than in the received contents. This method will always fail if there are more expected contents items than
     * received contents items.<br>
     * <br>
     * See {@link TestUtils#jsonPageTest(String, HttpStatus, BaseTestPage, boolean, boolean)} for results that will be
     * paginated.<br>
     * See {@link TestUtils#jsonTest(String, HttpStatus, Object, boolean)} for results that might not include a JSON
     * object.
     *
     * @param getString the query String used when contacting the server
     * @param expectedStatus the expected HttpStatus of the response
     * @param expectedValues the expected return object (should be set to {@code null} for no returned object)
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @param strictContentSize whether or not to assert the size of the contents list against the contents list in the
     *        expectedDto. This should be set to true when an empty contents list is expected
     */
    private static void jsonReferenceDataTest(final String getString, final HttpStatus expectedStatus,
            final Object[] expectedValues, final boolean allowNull, final boolean strictContentSize)
    {
        final Response response = getResponse(getString, expectedStatus);

        try
        {
            final Object[] responseValues = response.getBody().as(expectedValues.getClass());
            assertNotNull(responseValues);

            if (strictContentSize)
            {
                assertTrue(responseValues.length == expectedValues.length);
            }
            else
            {
                assertTrue(responseValues.length > 0);
            }

            for (int count = 0; count < expectedValues.length; count++)
            {
                compareFields(responseValues[count], expectedValues[count], allowNull);
            }

        }
        catch (ClassCastException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException | IntegrationTestException excep)
        {
            fail("Exception thrown: " + excep.getMessage());
        }
    }

    /*
     * Comparison methods
     */

    public static void compareFields(final Object responseObj, final Object expectedDto, final boolean allowNull)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        compareFields(responseObj, expectedDto, allowNull, null);
    }

    public static void compareFields(final Object responseObj, final Object expectedDto, final boolean allowNull, final String[] ignored)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        try
        {
            DtoUtils.compareFields(responseObj, expectedDto, allowNull, ignored);
        }
        catch (final ComparisonException exception)
        {
            throw new IntegrationTestException(exception);
        }
    }

    /**
     * Iterates through both Collections using their {@link Iterator}s and calls
     * {@link #compareFields(Object, Object, boolean)} on each element.<br>
     * Note that if the elements in both Collections are not in the same order, this method is likely to fail. It is
     * probably wise to avoid using this method for Maps at the moment.
     *
     * @param responseColl the Collection in the response
     * @param expectedColl the expected Collection
     * @param allowNull whether or not to allow {@code null} values in the expectedDto to exist and not throw an error
     *        when they don't match the corresponding field in the returned object
     * @param fieldName the name of the field containing this collection
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws IntegrationTestException
     */
    private static void compareCollection(final Object responseColl, final Object expectedColl, final boolean allowNull, final String fieldName)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final Iterator<?> responseIt = getIterator(responseColl);
        final Iterator<?> expectedIt = getIterator(expectedColl);

        if (responseIt.hasNext() && !expectedIt.hasNext())
        {
            throw new IntegrationTestException(format(UNEXPECTED_NON_EMPTY_COLLECTION, fieldName, responseColl));
        }

        Object responseObj;
        Object expectedObj;
        while (expectedIt.hasNext())
        {
            expectedObj = expectedIt.next();
            responseObj = getResponseItem(getIterator(responseColl), getCollectionItemId(expectedObj));

            compareFields(responseObj, expectedObj, allowNull, new String[]{"stalenessHash"});
        }
    }

    /*
     * Utility methods
     */

    public static Response getResponse(final String getString, final HttpStatus expectedStatus)
    {
        final List<HttpStatus> statuses = new ArrayList<HttpStatus>();
        statuses.add(expectedStatus);
        return getResponse(getString, statuses);
    }

    public static <T extends Object> T getResponse(final String getString, final HttpStatus expectedStatus, final Class<T> cls)
    {
        final List<HttpStatus> statuses = new ArrayList<HttpStatus>();
        statuses.add(expectedStatus);
        return as(cls, getResponse(getString, statuses));
    }

    /**
     * Gets the {@link Response} from the server with the given GET request String and asserts the status of the
     * response against the expected status
     *
     * @param getString the GET request
     * @param expectedStatus the expected HttpStatus of the response
     * @return the Response
     */
    public static Response getResponse(final String getString, final List<HttpStatus> expectedStatus)
    {
        final Response response = getResponseInner(getString);

        boolean found = false;
        final StringBuilder statusCodes = new StringBuilder();
        for (final HttpStatus status : expectedStatus)
        {
            if (status.value() == response.getStatusCode())
            {
                found = true;
            }
            statusCodes.append(status.value() + ",");
        }

        final String errorMessage = new StringBuffer().append(response.getStatusCode()).append(" not in ").append(statusCodes.toString())
                .append(": ").append(getErrorMessage(response)).toString();

        assertTrue(errorMessage, found);
        return response;
    }

    /**
     * Gets the {@link Response} from the server with the given GET request String
     *
     * @param getString the GET request
     * @return the Response
     */
    public static Response getResponse(final String getString)
    {
        return getResponse(getString, HttpStatus.OK);
    }

    /**
     * Gets the {@link Response} from the server with the given GET request String
     *
     * @saparam getString the GET request
     sc* @return the Response
     */
    private static Response getResponseInner(final String getString)
    {
        final String checkSum = SignUtils.generateChecksum("GET", getString, null);
        return SECUREURL.invokeRequest(checkSum, specification -> specification.get(getString));
    }

    public static <T extends Object> T getResponse(final String getString, final Class<T> cls)
    {
        return as(cls, getResponse(getString));
    }

    public static Response postResponse(final String url, final Object body, final HttpStatus expectedStatus)
    {
        final String bodyAsString = convertBodyToString(body);
        return postResponse(url, bodyAsString, expectedStatus);
    }

    public static <T extends Object> T postResponse(final String url, final Object body, final HttpStatus expectedStatus, final Class<T> cls)
    {
        final String bodyAsString = convertBodyToString(body);
        return as(cls, postResponse(url, bodyAsString, expectedStatus));
    }

    public static Response putResponse(final String url, final Object body, final HttpStatus expectedStatus)
    {
        return putResponse(url, convertBodyToString(body), expectedStatus);
    }

    public static <T extends Object> T putResponse(final String url, final Object body, final HttpStatus expectedStatus, final Class<T> cls)
    {
        return as(cls, putResponse(url, convertBodyToString(body), expectedStatus));
    }

    public static Response putResponse(final String url, final Object body)
    {
        return putResponse(url, convertBodyToString(body));
    }

    public static String convertBodyToString(final Object body)
    {
        String bodyAsString = null;
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(DateHelper.mastFormatter());
        try
        {
            bodyAsString = mapper.writeValueAsString(body);
        }
        catch (final JsonProcessingException e)
        {
            LOG.error(e.getMessage());
        }
        return bodyAsString;
    }

    public static Response deleteResponse(final String getString, final HttpStatus expectedStatus)
    {
        final String checkSum = SignUtils.generateChecksum("DELETE", getString, null);
        final Response response = SECUREURL.invokeRequest(checkSum, specification -> specification.delete(getString));
        assertEquals(getErrorMessage(response), expectedStatus.value(), response.statusCode());
        return response;
    }

    public static <T extends Object> T deleteResponse(final String getString, final HttpStatus expectedStatus, final Class<T> cls)
    {
        return as(cls, deleteResponse(getString, expectedStatus));
    }

    public static Response postResponse(final String getString, final String body, final HttpStatus expectedStatus)
    {
        final String checkSum = SignUtils.generateChecksum("POST", getString, body);
        final Response response = SECUREURL.invokeRequest(checkSum, specification -> specification.body(body).post(getString));
        assertEquals(getErrorMessage(response), expectedStatus.value(), response.statusCode());
        return response;
    }

    /**
     * Post response method with a provided private key
     *
     * @param getString
     * @param body
     * @param expectedStatus
     * @return
     */
    public static Response postResponse(final String getString, final String body, final HttpStatus expectedStatus, final Headers headers)
    {
        final Response response = SECUREURL.invokeRequest(specification -> specification.body(body).post(getString), headers);
        assertEquals(getErrorMessage(response), expectedStatus.value(), response.statusCode());
        return response;
    }

    public static Response putResponse(final String getString, final String body, final HttpStatus expectedStatus)
    {
        final Response response = putResponse(getString, body);
        assertEquals(expectedStatus.value(), response.statusCode());
        return response;
    }

    public static Response putResponse(final String getString, final String body)
    {
        final String checkSum = SignUtils.generateChecksum("PUT", getString, body);
        return SECUREURL.invokeRequest(checkSum, specification -> specification.body(body).put(getString));
    }

    /**
     * @param obj the {@link Collection} or {@link Map} object
     * @return the object's {@link Iterator}
     */
    private static Iterator<?> getIterator(final Object obj)
    {
        Iterator<?> retVal;
        if (obj instanceof Collection)
        {
            retVal = ((Collection<?>) obj).iterator();
        }
        else
        {
            retVal = ((Map<?, ?>) obj).entrySet().iterator();
        }
        return retVal;
    }

    private static Object getResponseItem(final Iterator<?> responseIt, final Long id) throws IntegrationTestException
    {
        Object responseObj = null;
        boolean found = false;
        while (responseIt.hasNext())
        {
            responseObj = responseIt.next();
            if (getCollectionItemId(responseObj).equals(id))
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            throw new IntegrationTestException(format(EXPECTED_OBJECT_NOT_FOUND, id));
        }

        return responseObj;
    }

    private static Long getCollectionItemId(final Object item)
    {
        Long retVal = null;
        try
        {
            // map
            retVal = (Long) ((Entry<?, ?>) item).getKey();
        }
        catch (final ClassCastException cce)
        {
            // list
            retVal = ((IdDto) item).getId();
        }
        return retVal;
    }

    public static void addCustomHeader(final Header customHeader)
    {
        SECUREURL.setCustomHeader(customHeader);
    }

    public static void flushCookies()
    {
        SECUREURL.flushCookies();
    }

    public static Date parseDate(final String stringDate)
    {
        if (stringDate == null)
        {
            return null;
        }

        Date date = null;
        try
        {
            date = DateHelper.mastFormatter().parse(stringDate);
        }
        catch (final ParseException exception)
        {
            Assert.fail("Test date failed to parse : " + exception.toString());
        }
        return date;
    }

    private static String getErrorMessage(final Response response)
    {
        return response != null && response.getBody() != null ? response.getBody().asString() : null;
    }

    public static <T extends Object> T as(final Class<T> cls, final Response response)
    {
        return response.as(cls, ObjectMapperType.GSON);
    }
}
