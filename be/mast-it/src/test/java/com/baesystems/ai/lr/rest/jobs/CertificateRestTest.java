package com.baesystems.ai.lr.rest.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.pages.CertificatePage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class CertificateRestTest extends BaseRestTest
{
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static final String BASE_JOB = new StringBuffer().append(BASE_PATH).append("/job").toString();
    public static final String SPECIFIC_JOB = new StringBuffer(BASE_JOB).append("/%d").toString();
    public static final String WIP_CERTIFICATE = new StringBuffer(SPECIFIC_JOB).append("/wip-certificate").toString();
    public static final String SPECIFIC_WIP_CERTIFICATE = new StringBuffer(WIP_CERTIFICATE).append("/%d").toString();

    private static final Long INVALID_ID = 9999L;
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long JOB_3_ID = 3L;
    private static final Long CERTIFICATE_1_ID = 1L;
    private static final Long CERTIFICATE_2_ID = 2L;

    private CertificateDto certificate1;
    private CertificatePage certificatePage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.certificate1 = DataUtils.getWipCertificate(CERTIFICATE_1_ID);

        initialiseCertificatePage(CERTIFICATE_1_ID, CERTIFICATE_2_ID);
    }

    private void initialiseCertificatePage(final Long... ids)
    {
        this.certificatePage = new CertificatePage();

        CertificateDto tempCertificate;
        for (final Long id : ids)
        {
            try
            {
                tempCertificate = DataUtils.getWipCertificate(id);
                this.certificatePage.getContent().add(tempCertificate);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    private CertificateDto getCertificate(final CertificatePage certificates, final Long certificateId)
    {
        CertificateDto chosenCertificate = null;

        for (final CertificateDto certificate : certificates.getContent())
        {
            if (certificate.getId() == certificateId)
            {
                chosenCertificate = certificate;
                break;
            }
        }

        return chosenCertificate;
    }

    /**
     * Test that a valid GET request for ../job/{jobId}/wip-certificate results in a positive response. Expecting
     * certificates 1 and 2.
     */
    @Test
    public void testGetCertificates()
    {
        TestUtils.listItemsFound(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), this.certificatePage, true, true);
    }

    /**
     * Test that a valid GET request for ../job/{jobId}/wip-certificate results in a positive response. Expecting empty
     * page.
     */
    @Test
    public void testGetCertificatesJobNasNoCertificates()
    {
        TestUtils.listItemsNotFound(String.format(WIP_CERTIFICATE, JOB_3_ID.intValue()));
    }

    /**
     * Test that a valid GET request for ../job/{jobId}/wip-certificate results in a positive response. Expecting record
     * not found exception.
     */
    @Test
    public void testGetCertificatesJobNotFound()
    {
        TestUtils.singleItemNotFound(String.format(WIP_CERTIFICATE, INVALID_ID.intValue()));
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting
     * certificate 1 on job 2.
     */
    @Test
    public void testPostCertificate()
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(JOB_2_ID);

        final CertificateDto testCertificate = TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID),
                this.certificate1, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertFalse(CERTIFICATE_1_ID.equals(testCertificate.getId()));
        assertEquals(JOB_2_ID, testCertificate.getJob().getId());
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting not
     * found exception.
     */
    @Test
    public void testPostCertificateJobNotFound()
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(INVALID_ID);

        TestUtils.postResponse(String.format(WIP_CERTIFICATE, INVALID_ID), this.certificate1, HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public void testPostCertificateNoType()
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(JOB_2_ID);
        this.certificate1.setCertificateType(null);

        TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public void testPostCertificateFailToSetDate()
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(JOB_2_ID);
        this.certificate1.getCertificateAction().setId(ONE);

        TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public void testPostCertificateIdNotNull()
    {
        this.certificate1.getJob().setId(JOB_2_ID);

        TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public void testPostCertificateNullSubIdOnNullableField()
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(JOB_2_ID);
        this.certificate1.getCertificateStatus().setId(null);

        TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid POST request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting certificate 1 with correctly updated dates.
     */
    @Test
    public void testPostCertificateCheckDateCalculation() throws DatabaseUnitException
    {
        this.certificate1.setId(null);
        this.certificate1.getJob().setId(JOB_2_ID);

        final Date issueData = new Date();

        final CertificateTypeDto certificateType = DataUtils.getCertificateType(this.certificate1.getCertificateType().getId());

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(issueData);
        calendar.add(Calendar.MONTH, DataUtils.getServiceCatalogue(certificateType.getServiceCatalogue().getId()).getCyclePeriodicity());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        final Date expiryData = calendar.getTime();

        this.certificate1.setIssueDate(issueData);

        final CertificateDto testCertificate = TestUtils.postResponse(String.format(WIP_CERTIFICATE, JOB_2_ID),
                this.certificate1, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertFalse(CERTIFICATE_1_ID.equals(testCertificate.getId()));
        assertEquals(JOB_2_ID, testCertificate.getJob().getId());
        assertEquals(this.dateFormat.format(issueData), this.dateFormat.format(testCertificate.getIssueDate()));
        assertEquals(this.dateFormat.format(expiryData), this.dateFormat.format(testCertificate.getExpiryDate()));
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate results in a positive response. Expecting not
     * found exception.
     */
    @Test
    public void testPutCertificateJobNotFound()
    {
        this.certificate1.getJob().setId(INVALID_ID);

        TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, INVALID_ID, CERTIFICATE_1_ID), this.certificate1, HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate results in a positive response. Expecting not
     * found exception.
     */
    @Test
    public void testPutCertificateCertificateNotFound()
    {
        TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, INVALID_ID), this.certificate1, HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting certificate 2 still with job 1.
     */
    @Test
    public void testPutCertificateNoJob()
    {
        final CertificatePage certificates = TestUtils.getResponse(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), HttpStatus.OK,
                CertificatePage.class);
        final CertificateDto certificate = getCertificate(certificates, CERTIFICATE_2_ID);

        certificate.setJob(null);

        final CertificateDto testCertificate = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_2_ID),
                certificate, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertEquals(CERTIFICATE_2_ID, testCertificate.getId());
        assertEquals(JOB_1_ID, testCertificate.getJob().getId());
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting certificate 1.
     */
    @Test
    public void testPutCertificateDateChange()
    {
        final CertificatePage certificates = TestUtils.getResponse(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), HttpStatus.OK,
                CertificatePage.class);
        final CertificateDto certificate = getCertificate(certificates, CERTIFICATE_1_ID);

        certificate.setIssueDate(new Date());

        final CertificateDto testCertificate = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID),
                certificate, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertEquals(CERTIFICATE_1_ID, testCertificate.getId());
        assertEquals(JOB_1_ID, testCertificate.getJob().getId());
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting certificate 2.
     */
    @Test
    public void testPutCertificateNotIssued()
    {
        final CertificatePage certificates = TestUtils.getResponse(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), HttpStatus.OK,
                CertificatePage.class);
        final CertificateDto certificate = getCertificate(certificates, CERTIFICATE_2_ID);

        certificate.getCertificateAction().setId(ONE);

        final CertificateDto testCertificate = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_2_ID),
                certificate, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertEquals(CERTIFICATE_2_ID, testCertificate.getId());
        assertEquals(JOB_1_ID, testCertificate.getJob().getId());
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting a bad request exception.
     */
    @Test
    public void testPutCertificateNoType()
    {
        this.certificate1.setCertificateType(null);

        TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting a bad request exception.
     */
    @Test
    public void testPutCertificateFailToChangeDate()
    {
        this.certificate1.getCertificateAction().setId(ONE);
        this.certificate1.setIssueDate(new Date());

        TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting a bad request exception.
     */
    @Test
    public void testPutCertificateNullSubIdOnNullableField()
    {
        this.certificate1.getCertificateStatus().setId(null);

        TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID), this.certificate1, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that a valid PUT request for ../job/{jobId}/wip-certificate/{certificateId} results in a positive response.
     * Expecting certificate 1 with correctly updated dates.
     */
    @Test
    public void testPutCertificateCheckDateCalculation() throws DatabaseUnitException
    {
        final CertificatePage certificates = TestUtils.getResponse(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), HttpStatus.OK,
                CertificatePage.class);
        final CertificateDto certificate = getCertificate(certificates, CERTIFICATE_1_ID);

        final Date issueDate = new Date();

        final CertificateTypeDto certificateType = DataUtils.getCertificateType(certificate.getCertificateType().getId());

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(issueDate);
        calendar.add(Calendar.MONTH, DataUtils.getServiceCatalogue(certificateType.getServiceCatalogue().getId())
                .getCyclePeriodicity());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        final Date expiryData = calendar.getTime();

        certificate.setIssueDate(issueDate);

        final CertificateDto testCertificate = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID),
                certificate, HttpStatus.OK, CertificateDto.class);

        assertNotNull(testCertificate);
        assertEquals(CERTIFICATE_1_ID, testCertificate.getId());
        assertEquals(JOB_1_ID, testCertificate.getJob().getId());
        assertEquals(this.dateFormat.format(issueDate), this.dateFormat.format(testCertificate.getIssueDate()));
        assertEquals(this.dateFormat.format(expiryData), this.dateFormat.format(testCertificate.getExpiryDate()));
    }

    @Test
    public void updateCertificateFailsWhenOldTokenIsUsed()
    {
        final CertificatePage originalCertificates = TestUtils.getResponse(String.format(WIP_CERTIFICATE, JOB_1_ID.intValue()), HttpStatus.OK,
                CertificatePage.class);
        CertificateDto certificate = getCertificate(originalCertificates, CERTIFICATE_1_ID);

        final LinkResource originalCertificateType = certificate.getCertificateType();
        final String originalStalenessHash = certificate.getStalenessHash();

        // Make a small change
        certificate.setCertificateType(new LinkResource(TWO));
        certificate = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID), certificate, HttpStatus.OK,
                CertificateDto.class);

        // Make another small change, but use the hash from the original GET
        certificate.setCertificateType(originalCertificateType);
        certificate.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(String.format(SPECIFIC_WIP_CERTIFICATE, JOB_1_ID, CERTIFICATE_1_ID), certificate,
                HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.CERTIFICATE.getTypeString()), error.getMessage());
    }
}
