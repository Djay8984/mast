package com.baesystems.ai.lr.rest.jobs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.enums.OfficeRoleType;

public class JobRestTestHelper
{
    private static final String JOB_DESCRIPTION = "General job description";
    private static final String JOB_LOCATION = "A place";
    private static final Long OFFICE_60_ID = 60L;
    private static final Long ASSET_VERSION_1_ID = 1L;
    private static final Long JOB_CATEGORY_1_ID = 1L;

    public JobDto createSkeletonJob(final Long assetId, final Long caseId, final Long jobStatusId)
    {
        final JobDto job = new JobDto();

        job.setAsset(new LinkVersionedResource());
        job.getAsset().setId(assetId);
        job.getAsset().setVersion(ASSET_VERSION_1_ID);
        job.setaCase(new LinkResource(caseId));
        job.setOffices(getOfficeList());
        job.setEmployees(Collections.<EmployeeLinkDto>emptyList());
        job.setJobStatus(new LinkResource(jobStatusId));
        job.setRequestedAttendanceDate(new Date());
        job.setDescription(JOB_DESCRIPTION);
        job.setLocation(JOB_LOCATION);
        job.setEtaDate(new Date());
        job.setEtdDate(new Date());
        job.setScopeConfirmed(false);
        job.setClassGroupJob(false);
        job.setJobCategory(new LinkResource(JOB_CATEGORY_1_ID));

        return job;
    }

    public List<OfficeLinkDto> getOfficeList()
    {
        final OfficeLinkDto sdoOfficeLink = new OfficeLinkDto();
        final LinkResource officeRole = new LinkResource(OfficeRoleType.SDO.getValue());

        sdoOfficeLink.setOffice(new LinkResource(OFFICE_60_ID));
        sdoOfficeLink.setOfficeRole(officeRole);

        final List<OfficeLinkDto> offices = new ArrayList<OfficeLinkDto>();
        offices.add(sdoOfficeLink);

        return offices;
    }
}
