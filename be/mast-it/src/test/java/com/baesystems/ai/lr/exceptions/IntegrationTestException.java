package com.baesystems.ai.lr.exceptions;

public class IntegrationTestException extends Exception
{
    private static final long serialVersionUID = 3017119986145276319L;

    public IntegrationTestException(final String msg)
    {
        super(msg);
    }
    
    public IntegrationTestException(final Throwable cause)
    {
        super(cause);
    }
    
    public IntegrationTestException(final String msg, final Throwable cause)
    {
        super(msg, cause);
    }
}
