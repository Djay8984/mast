package com.baesystems.ai.lr.rest.defects;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.DefectItemLinkDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.defects.RepairPageResourceDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.RepairPage;
import com.baesystems.ai.lr.pages.SurveyPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class RepairRestTest extends BaseRestTest
{
    private static final String BASE_DEFECT = urlBuilder(BASE_PATH, "/defect");
    private static final String BASE_JOB = urlBuilder(BASE_PATH, "/job/%d");
    private static final String SPECIFIC_DEFECT = urlBuilder(BASE_DEFECT, "/%d");
    private static final String JOB_SURVEY_PATH = urlBuilder(BASE_JOB, "/survey");
    private static final String SPECIFIC_JOB_SURVEY_PATH = urlBuilder(JOB_SURVEY_PATH, "/%d");
    private static final String COC_REPAIRS_PATH = urlBuilder(BASE_PATH, "/asset/%d/coc/%d/repair");
    private static final String SPECIFIC_COC_REPAIR_PATH = urlBuilder(BASE_PATH, "/asset/%d/coc/%d/repair/%d");
    private static final String ASSET_DEFECT_REPAIRS_PATH = urlBuilder(BASE_PATH, "/asset/%d/defect/%d/repair");
    private static final String SPECIFIC_ASSET_DEFECT_REPAIR_PATH = urlBuilder(ASSET_DEFECT_REPAIRS_PATH, "/%d");
    private static final String BASE_JOB_WIP_DEFECT = urlBuilder(BASE_JOB, "/wip-defect");
    private static final String SPECIFIC_JOB_WIP_DEFECT = urlBuilder(BASE_JOB_WIP_DEFECT, "/%d");
    private static final String BASE_JOB_WIP_DEFECT_WIP_REPAIR = urlBuilder(SPECIFIC_JOB_WIP_DEFECT, "/wip-repair");
    private static final String SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR = urlBuilder(BASE_JOB_WIP_DEFECT_WIP_REPAIR, "/%d");
    private static final String WIP_COC_REPAIR_PATH = urlBuilder(BASE_JOB, "/wip-coc/%d/wip-repair");
    private static final String SPECIFIC_JOB_WIP_COC_WIP_REPAIR = urlBuilder(WIP_COC_REPAIR_PATH, "/%d");

    private static final Long DEFECT_OPEN = 1L;
    private static final Long DEFECT_CLOSED = 2L;
    private static final Long MODIFICATION = 3L;
    private static final Long PERMANENT = RepairActionType.PERMANENT.value();
    private static final Long TEMPORARY = 2L;
    private static final Long DEFECT_2_ID = 2L;
    private static final Long REPAIR_1_ID = 1L;
    private static final Long ITEM_7_ID = 7L;
    private static final Long ITEM_8_ID = 8L;
    private static final Long REPAIR_SERVICE_CAT_ID = 270L;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_DEFECT_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long VALID_COC_REPAIR_ID = 20L;
    private static final Long VALID_REPAIR_WITH_VALID_COC_ID = 9L;
    private static final Long VALID_REPAIR_COC_ID = 1L;
    private static final Long INVALID_REPAIR_COC_ID = 4L;
    private static final Long VALID_REPAIR_ASSET_ID = 3L;
    private static final Long INVALID_REPAIR_ASSET_ID = 999L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long DEFECT_3_ID = 3L;
    private static final Long DEFECT_4_ID = 4L;
    private static final Long INVALID_DEFECT_REPAIR_ID = 999L;
    private static final Long INVALID_DEFECT_ID = 998L;
    private static final Long INVALID_JOB_ID = 997L;
    private static final Long VALID_JOB_ID_WITHOUT_WIP_DEFECTS = 2L;

    private static final Long WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT = 1L;
    private static final Long INVALID_WIP_DEFECT_ID = 999L;
    private static final Long VALID_JOB_ID_FOR_WIP_DEFECT = 1L;
    private static final Long VALID_WIP_DEFECT_ID_FOR_JOB = 5L;
    private static final Long WIP_DEFECT_ID_WITH_NO_WIP_REPAIR = 6L;
    private static final Long WIP_DEFECT_ID_1 = 1L;
    private static final Long[] PROTECTED_SURVEY_IDS = {1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L};

    private static final Long ONE = 1L;

    private DefectDto minimalDefect;
    private DefectDto defect2;
    private DefectDto wipDefect1;
    private RepairDto minimalRepair;
    private RepairDto wipRepair1;
    private ItemLightDto itemLight7;
    private ItemLightDto itemLight8;
    private ServiceCatalogueLightDto repairServiceCatalogue;
    private RepairPage repairPage;

    private static Boolean firstRun = true;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        defect2 = DataUtils.getDefect(DEFECT_2_ID);
        minimalDefect = DataUtils.getDefect(DEFECT_2_ID);
        minimalDefect.setAsset(new LinkResource(ASSET_2_ID)); // put new defects on asset 2 to avoid interfering with
                                                              // other tests.
        minimalRepair = DataUtils.getRepair(REPAIR_1_ID, false);
        wipRepair1 = DataUtils.getRepair(REPAIR_1_ID, true);
        wipRepair1.setCodicil(new LinkResource(VALID_REPAIR_COC_ID));
        wipDefect1 = DataUtils.getWIPDefect(WIP_DEFECT_ID_1);

        this.itemLight7 = DataUtils.getItemLight(ITEM_7_ID);
        this.itemLight8 = DataUtils.getItemLight(ITEM_8_ID);

        this.repairServiceCatalogue = DataUtils.getServiceCatalogue(REPAIR_SERVICE_CAT_ID);

        // Create survey
        final SurveyDto survey = new SurveyDto();
        survey.setJob(wipDefect1.getJob());
        survey.setName(repairServiceCatalogue.getName());
        survey.setScheduleDatesUpdated(Boolean.FALSE);
        survey.setJobScopeConfirmed(Boolean.FALSE);
        survey.setServiceCatalogue(new LinkResource(repairServiceCatalogue.getId()));
        survey.setSurveyStatus(new LinkResource(ServiceCreditStatusType.COMPLETE.value()));
        survey.setJobScopeConfirmed(Boolean.TRUE);
        survey.setAutoGenerated(Boolean.TRUE);
        wipRepair1.setSurvey(survey);

        if (firstRun)
        {
            cleanJob();
            firstRun = false;
        }
    }

    private void cleanJob()
    {
        final SurveyPage surveyPage = TestUtils.getResponse(String.format(JOB_SURVEY_PATH, JOB_1_ID), HttpStatus.OK, SurveyPage.class);

        final List<Long> protectedSurveys = Arrays.asList(PROTECTED_SURVEY_IDS);

        for (final SurveyDto survey : surveyPage.getContent())
        {
            if (!protectedSurveys.contains(survey.getId()))
            {
                TestUtils.deleteResponse(format(SPECIFIC_JOB_SURVEY_PATH, JOB_1_ID, survey.getId()), HttpStatus.OK);
            }
        }
    }

    @Test
    public void getSingleRepair()
    {
        TestUtils.singleItemFound(format(SPECIFIC_DEFECT, DEFECT_2_ID), this.defect2, true);
        TestUtils.singleItemFound(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, DEFECT_2_ID, REPAIR_1_ID), this.minimalRepair,
                true);
    }

    /**
     * Test a GET request for defect/{defectId}/repair/{repairId} results in an appropriate error when the repair ID
     * doesn't exist
     */
    @Test
    public void getRepairWithInvalidRepairId()
    {
        TestUtils.singleItemNotFound(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, DEFECT_2_ID, INVALID_DEFECT_REPAIR_ID));
    }

    /**
     * Test a GET request for defect/{defectId}/repair/{repairId} results in an appropriate error when the repair ID
     * relates to a defect different from the defect id supplied. Check the error message is correct too.
     */
    @Test
    @Ignore
    // AssetItem
    public void getRepairWithIncorrectDefectId()
    {
        final ErrorMessageDto error = TestUtils
                .getResponse(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, INVALID_DEFECT_ID, REPAIR_1_ID), HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);
        assertEquals(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG, error.getMessage());
    }

    /**
     * Test a POST to defect/{defectId}/repair/{repairId} results in an appropriate error when the repair ID relates to
     * a defect different from the defect id supplied. We also need to check that the repair hasn't actually been
     * updated even if we do get the error message, since the error message might have come from the GET request we do
     * after having performed the PUT.
     */
    @Test
    @Ignore
    // Bad data fix later
    public final void updateRepairWithIncorrectDefectId()
    {
        // get the current repair for repair 1 from using the correct defect ID (2)...
        final RepairDto originalRepairDto = TestUtils
                .getResponse(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, DEFECT_2_ID, REPAIR_1_ID), HttpStatus.OK,
                        RepairDto.class);
        // ...make a minor change...
        final String originalDescription = originalRepairDto.getRepairTypeDescription();
        final String newDescription = originalDescription + "TESTUPDATE";
        final RepairDto repairDtoToPut = originalRepairDto;
        repairDtoToPut.setRepairTypeDescription(newDescription);
        // ...PUT the changed DTO to the endpoint using the incorrect defect ID...
        final ErrorMessageDto error = TestUtils
                .putResponse(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, DEFECT_3_ID, REPAIR_1_ID), repairDtoToPut,
                        HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        final RepairDto repairDtoAfterAttemptedUpdate = TestUtils
                .getResponse(format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, VALID_REPAIR_ASSET_ID, DEFECT_2_ID, REPAIR_1_ID), HttpStatus.OK,
                        RepairDto.class);
        // ...and check we get an error message and that the repair has not been updated.
        assertEquals(String.format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Repair", REPAIR_1_ID,
                "Defect", DEFECT_3_ID), error.getMessage());
        final String latestDescription = repairDtoAfterAttemptedUpdate.getRepairTypeDescription();
        assertEquals(originalDescription, latestDescription); // check that no update has happened.
    }

    @Test
    public void repairList()
    {
        TestUtils.getResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, VALID_REPAIR_ASSET_ID, DEFECT_2_ID), HttpStatus.OK);
    }

    @Test
    public void testGetInvalidCocRepair()
    {
        TestUtils.getResponse(String.format(SPECIFIC_COC_REPAIR_PATH, ASSET_2_ID, VALID_COC_REPAIR_ID, INVALID_REPAIR_COC_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testGetValidCocRepair()
    {
        TestUtils.getResponse(String.format(SPECIFIC_COC_REPAIR_PATH, 1L, VALID_COC_REPAIR_ID, VALID_REPAIR_WITH_VALID_COC_ID),
                HttpStatus.OK);
    }

    @Test
    public void testGetInvalidCocRepairs()
    {
        TestUtils.getResponse(String.format(COC_REPAIRS_PATH, ASSET_2_ID, INVALID_REPAIR_COC_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testGetValidCocRepairs()
    {
        TestUtils.getResponse(String.format(COC_REPAIRS_PATH, ONE, VALID_REPAIR_COC_ID), HttpStatus.OK);
    }

    @Test
    public void testGetInvalidAssetWithValidCocRepairs()
    {
        TestUtils.getResponse(String.format(COC_REPAIRS_PATH, INVALID_REPAIR_ASSET_ID, VALID_REPAIR_COC_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testGetInvalidWIPCocRepair()
    {
        TestUtils.getResponse(String.format(WIP_COC_REPAIR_PATH, JOB_1_ID, INVALID_REPAIR_COC_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public void testGetValidWIPCocRepair()
    {
        final RepairDto newRepair = createRepairOnWIPCoC("Valid WIP CoC Repair");

        // Check that the created repair is in the result
        final RepairPageResourceDto newRepairPage = TestUtils.getResponse(format(WIP_COC_REPAIR_PATH, JOB_1_ID, VALID_REPAIR_COC_ID), HttpStatus.OK,
                RepairPageResourceDto.class);

        final List<RepairDto> content = newRepairPage.getContent();

        final long numberOfCreatedRepairInstances = content.stream().filter(r -> r.getId().equals(newRepair.getId())).count();

        Assert.assertEquals(1L, numberOfCreatedRepairInstances);
    }

    private RepairDto createRepairOnWIPCoC(final String description)
    {
        // Create a repair
        final RepairDto repairToAdd = new RepairDto();
        repairToAdd.setCodicil(makeLinkResource(VALID_REPAIR_COC_ID));
        repairToAdd.setConfirmed(false);
        repairToAdd.setDescription("New WIP Repair");
        repairToAdd.setMaterialsUsed(new ArrayList<LinkResource>());
        repairToAdd.setNarrative("New WIP Repair Narrative.");
        repairToAdd.setPromptThoroughRepair(false);
        repairToAdd.setRepairAction(makeLinkResource(TEMPORARY));
        repairToAdd.setRepairs(new ArrayList<RepairItemDto>());
        repairToAdd.setRepairTypeDescription(description);
        repairToAdd.setRepairTypes(new ArrayList<LinkResource>());
        repairToAdd.setUpdatedDate(new Date());

        final RepairDto newlyReturnedDto = TestUtils
                .postResponse(format(WIP_COC_REPAIR_PATH, JOB_1_ID, VALID_REPAIR_COC_ID), repairToAdd, HttpStatus.OK, RepairDto.class);

        return newlyReturnedDto;
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-repair returns a valid WIP Repair.
     */
    @Test
    public void testGetWIPRepairForJobAndWIPDefect()
    {
        initialiseRepairPage(true, WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT);
        TestUtils.listItemsFound(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB),
                this.repairPage,
                true, false);
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-repair returns an empty WIP-Repair list.
     */
    @Test
    public void testGetWIPRepairForJobAndWIPDefectReturnsEmpty()
    {
        TestUtils.listItemsNotFound(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, WIP_DEFECT_ID_WITH_NO_WIP_REPAIR));
    }

    /**
     * Test invalid GET ../job/{x}/wip-defect/{y}/wip-repair.
     */
    @Test
    public void testGetWIPRepairForJobAndWIPDefectFails()
    {
        TestUtils
                .getResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, INVALID_WIP_DEFECT_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test valid POST ../job/{x}/wip-defect/{y}/wip-repair.
     *
     * @throws IntegrationTestException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    public void testCreateWIPRepairForJobAndDefect() throws DatabaseUnitException, IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "updatedDate", "stalenessHash", "survey"};
        wipDefect1.setId(null);
        wipDefect1.getDefectStatus().setId(DEFECT_OPEN);
        wipDefect1.setItems(new ArrayList<DefectItemDto>());
        wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = createWipDefect(JOB_1_ID);
        final RepairDto newRepairDtoFromPost = createWipRepair(newWIPDefect);

        final SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        final Long newId = newRepairDtoFromPost.getId();
        // get the version of the repair from the DB
        final RepairDto newRepairDtoFromDb = DataUtils.getRepair(newId, Boolean.TRUE);
        // both flavours of the DTO should match what we originally passed up (except for the ID).
        TestUtils.compareFields(newRepairDtoFromPost, this.wipRepair1, false, ignoredFields);
        TestUtils.compareFields(newRepairDtoFromDb, this.wipRepair1, false, ignoredFields);
        // check that the creation of the repair has also resulted in the creation of a survey.
        assertNotNull(repairSurvey);
        // clean up the survey that was created
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);
    }

    @Test
    public void testSurveyCreationForWipRepairUpdate()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());

        this.wipRepair1.setId(null);
        this.wipRepair1.setDefect(new LinkResource(newWIPDefect.getId()));
        this.wipRepair1.setDescription("New WIP Repair for Defect #1 via POST");
        this.wipRepair1.setRepairAction(new LinkResource(PERMANENT));
        // needs to be unconfirmed so we don't create a survey to start with
        this.wipRepair1.setConfirmed(Boolean.FALSE);
        // confirm that there are no surveys against it when we begin
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));
        final RepairDto newRepairDtoFromPost = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        // Check there's still no survey (as the repair wasn't confirmed)
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));
        newRepairDtoFromPost.setConfirmed(Boolean.TRUE);
        newRepairDtoFromPost.setRepairAction(new LinkResource(TEMPORARY));
        newRepairDtoFromPost.setSurvey(wipRepair1.getSurvey());
        final RepairDto updatedAsTemporaryRepair = TestUtils
                .putResponse(format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newRepairDtoFromPost.getId()),
                        newRepairDtoFromPost, HttpStatus.OK, RepairDto.class);

        // check the survey is still null as although it's now confirmed, the repair action is temporary.
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));
        updatedAsTemporaryRepair.setRepairAction(new LinkResource(PERMANENT));
        updatedAsTemporaryRepair.setSurvey(wipRepair1.getSurvey());
        TestUtils
                .putResponse(format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), updatedAsTemporaryRepair.getId()),
                        updatedAsTemporaryRepair, HttpStatus.OK, RepairDto.class);
        // finally, the update this time should have created a survey as it's permanent and confirmed.
        final SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);
    }

    /*
     * Amendments to ensure surveys are created: 1) Need repair to be confirmed 2) Need repair action to be permanent -
     * i.e. action_type_id = 1 3) Ensure the defect in question has a category 3 (Hull) against it 4) Use the
     * serviceCatalogue for SurveyName "Hull Repairs" (270) which is the catalogue for defects with defect category 3 5)
     * To begin with, assert that the job under the defect doesn't have a survey against it. 6) After the post, assert
     * that the job does does have a survey against it against them with name "Hull Repairs"
     */

    /**
     * Utility method to test that a survey exists or doesn't exist, as required, against the given defect's job
     *
     * @param wipDefect
     * @param shouldExist
     */
    private SurveyDto checkSingleRepairSurveyExistance(final DefectDto wipDefect)
    {
        final SurveyPage surveyPage = TestUtils.getResponse(String.format(JOB_SURVEY_PATH, wipDefect.getJob().getId()), HttpStatus.OK,
                SurveyPage.class);

        SurveyDto repairSurveyFound = null;

        for (final SurveyDto survey : surveyPage.getContent())
        {
            if (this.repairServiceCatalogue.getName().equals(survey.getName()) && survey.getServiceCatalogue().getId().equals(REPAIR_SERVICE_CAT_ID))
            {
                // there should only be one.
                assertNull(repairSurveyFound);
                repairSurveyFound = survey;
                assertTrue(survey.getAutoGenerated());
                assertEquals(new LinkResource(ServiceCreditStatusType.COMPLETE.value()), survey.getSurveyStatus());
                assertFalse(survey.getScheduleDatesUpdated());
                assertTrue(survey.getJobScopeConfirmed());
            }
        }

        return repairSurveyFound;
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-repair as given job does not exist
     */
    @Test
    public void testInvalidCreateWipRepairInvalidJobId()
    {
        this.wipRepair1.setId(null);
        this.wipRepair1.setDefect(new LinkResource(WIP_DEFECT_ID_1));
        this.wipRepair1.setDescription("New WIP Repair for Defect #1 via POST");
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, INVALID_JOB_ID, WIP_DEFECT_ID_1), this.wipRepair1, HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);
        Assert.assertEquals(format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, INVALID_JOB_ID), error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-repair as given wip-defect does not belong to given job
     */
    @Test
    public void testInvalidCreateWipRepairWrongJobForDefect()
    {
        this.wipRepair1.setId(null);
        this.wipRepair1.setDefect(new LinkResource(WIP_DEFECT_ID_1));
        this.wipRepair1.setDescription("New WIP Repair for Defect #1 via POST");
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_WITHOUT_WIP_DEFECTS, WIP_DEFECT_ID_1), this.wipRepair1,
                        HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        Assert.assertEquals(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG, error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-repair as a required field on the Repair DTO is null
     */
    @Test
    public void testInvalidCreateWipRepairNullDescription()
    {
        this.wipRepair1.setId(null);
        this.wipRepair1.setDefect(new LinkResource(WIP_DEFECT_ID_1));
        this.wipRepair1.setDescription(null);
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, WIP_DEFECT_ID_1), this.wipRepair1,
                        HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        Assert.assertEquals(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "description may not be null"), error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-repair as the ID in the repair DTO is non-null
     */
    @Test
    public void testInvalidCreateWipRepairNonNullRepairId()
    {
        this.wipRepair1.setDefect(new LinkResource(WIP_DEFECT_ID_1));
        this.wipRepair1.setDescription("New WIP Repair for Defect #1 via POST");
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, WIP_DEFECT_ID_1), this.wipRepair1,
                        HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        Assert.assertEquals(format(ExceptionMessagesUtils.ID_NOT_NULL, this.wipRepair1.getId()), error.getMessage());
    }

    /**
     * Tests valid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z}
     *
     * @throws IntegrationTestException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DatabaseUnitException
     */
    @Ignore
    @Test
    public void testUpdateWipRepairForJobAndDefect() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            IntegrationTestException, DatabaseUnitException
    {
        final String[] ignoredFields = {"updatedDate", "stalenessHash"};
        final String originalDescription = this.wipRepair1.getDescription();
        this.wipRepair1.setDescription(originalDescription + " updated via PUT");
        final RepairDto newRepairDtoFromPost = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.OK, RepairDto.class);
        // get the new version of the repair from the DB
        final RepairDto newRepairDtoFromDb = DataUtils.getRepair(WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT, Boolean.TRUE);
        // both flavours of the DTO should match what we originally passed up.
        TestUtils.compareFields(newRepairDtoFromPost, this.wipRepair1, false, ignoredFields);
        TestUtils.compareFields(newRepairDtoFromDb, this.wipRepair1, false, ignoredFields);
        // put it back to how it was after the test.
        this.wipRepair1.setDescription(originalDescription);
        TestUtils.putResponse(
                format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB,
                        WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                this.wipRepair1, HttpStatus.OK);

    }

    /**
     * Tests invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as the job does not exist
     */
    @Test
    public void testInvalidUpdateWipRepairInvalidJobId()
    {
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, INVALID_JOB_ID, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        Assert.assertEquals(format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, INVALID_JOB_ID), error.getMessage());
    }

    /**
     * Test invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as given wip-defect does not belong to given job
     */
    @Test
    public void testInvalidUpdateWipRepairWrongJobForDefect()
    {
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_WITHOUT_WIP_DEFECTS, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        Assert.assertEquals(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG, error.getMessage());
    }

    /**
     * Test invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as given wip-repair does not belong to given wip-defect
     */
    @Test
    public void testInvalidUpdateWipRepairWrongDefectForRepair()
    {
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, DEFECT_2_ID,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        Assert.assertEquals(
                format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Repair", WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT, "Defect",
                        DEFECT_2_ID),
                error.getMessage());
    }

    /**
     * Test invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as a required field on the Repair DTO is null
     */
    @Test
    public void testInvalidUpdateWipRepairNullDescription()
    {
        this.wipRepair1.setDescription(null);
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        Assert.assertEquals(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "description may not be null"), error.getMessage());
    }

    /**
     * Test invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as the Defect ID in the URI doesn't match the one on
     * the DTO.
     */
    @Test
    @Ignore
    // AssetItem
    public void testInvalidUpdateWipRepairDefectIdMismatch()
    {
        this.wipRepair1.setDefect(new LinkResource(INVALID_DEFECT_ID));
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        Assert.assertEquals(
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", INVALID_DEFECT_ID, VALID_WIP_DEFECT_ID_FOR_JOB),
                error.getMessage());
    }

    /**
     * Test invalid PUT ../job/{x}/wip-defect/{y}/wip-repair/{z} as the Repair ID in the URI doesn't match the one on
     * the DTO.
     */
    @Test
    public void testInvalidUpdateWipRepairRepairIdMismatch()
    {
        this.wipRepair1.setId(INVALID_DEFECT_REPAIR_ID);
        final ErrorMessageDto error = TestUtils
                .putResponse(
                        format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB,
                                WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                        this.wipRepair1, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        Assert.assertEquals(
                String.format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "", INVALID_DEFECT_REPAIR_ID, WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                error.getMessage());
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-repair/{z} returns a valid WIP Repair.
     */
    @Test
    public void getSingleWIPRepair()
    {
        // Changing the wip repair object to the way it is already persisted.
        wipRepair1.setSurvey(null);
        wipRepair1.setCodicil(null);

        TestUtils.singleItemFound(format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR,
                VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB, WIP_REPAIR_ID_FOR_JOB_AND_WIP_DEFECT),
                this.wipRepair1, true);
    }

    /**
     * Test for creating defect repairs and making sure that the defect is only closed when the item has a permanent
     * repair that is confirmed.
     */
    @Test
    public void testRepairLifeCycle()
    {
        this.minimalDefect.setId(null);
        this.minimalDefect.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item = new DefectItemDto();
        item.setItem(this.itemLight7);
        this.minimalDefect.setItems(new ArrayList<DefectItemDto>());
        this.minimalDefect.getItems().add(item);
        this.minimalDefect.setJobs(new ArrayList<JobDefectDto>());
        this.minimalDefect.getJobs().add(new JobDefectDto());
        this.minimalDefect.getJobs().get(0).setJob(new LinkResource(JOB_1_ID));
        this.minimalDefect.getJobs().get(0).setId(JOB_DEFECT_1_ID);

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.minimalDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newDefect.getDefectStatus().getId());
        cleanRepair(newDefect.getId(), MODIFICATION, this.minimalRepair);

        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(newDefect.getItems().iterator().next().getId()));
        this.minimalRepair.getRepairs().add(repairItem);

        final RepairDto newRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newRepair);
        assertNotNull(newRepair.getId());

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // permanent repair
        newRepair.setConfirmed(Boolean.TRUE);
        newRepair.getRepairAction().setId(PERMANENT);

        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        // testRepairSurveysAreCreated(newDefect);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // ensure closed
        assertEquals("defect closed", DEFECT_CLOSED, response2.getDefectStatus().getId());
    }

    @Test
    public void testUpdateRepairFailsWithAnIncorrectStalenessHash() throws DatabaseUnitException
    {
        // Create a new defect for the repair we're testing.

        final DefectDto defect = createDefect();
        final RepairDto repair = cleanRepair(defect.getId(), MODIFICATION, this.minimalRepair);
        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(defect.getItems().iterator().next().getId()));
        repair.getRepairs().add(repairItem);

        // Create the repair

        final RepairDto createdRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, defect.getAsset().getId(), defect.getId()), repair, HttpStatus.OK,
                        RepairDto.class);

        // Update it with a dodgy hash.

        createdRepair.setDescription(createdRepair.getDescription() + "X");
        createdRepair.setStalenessHash("Incorrect Hash");

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, defect.getAsset().getId(), defect.getId(), createdRepair.getId()),
                        createdRepair, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.REPAIR.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public void testUpdateRepairFailsWithAnOldHash() throws DatabaseUnitException
    {
        // Create a new defect and repair

        final DefectDto defect = createDefect();
        final RepairDto repair = cleanRepair(defect.getId(), MODIFICATION, this.minimalRepair);
        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(defect.getItems().iterator().next().getId()));
        repair.getRepairs().add(repairItem);

        // Create the repair

        final RepairDto createdRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, defect.getAsset().getId(), defect.getId()), repair, HttpStatus.OK,
                        RepairDto.class);

        createdRepair.setDescription(createdRepair.getDescription() + "X");
        final String originalStalenessHash = createdRepair.getStalenessHash();

        // Successfully update the repair

        final RepairDto updatedRepair = TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, defect.getAsset().getId(), defect.getId(), createdRepair.getId()),
                        createdRepair, HttpStatus.OK, RepairDto.class);

        // Attempt to update the repair using an old staleness hash.

        updatedRepair.setDescription(updatedRepair.getDescription() + "X");
        updatedRepair.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, defect.getAsset().getId(), defect.getId(), updatedRepair.getId()),
                        updatedRepair, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.REPAIR.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public void testUpdateWIPRepairForDefectFailsWithAnOldHash() throws DatabaseUnitException
    {
        // Create a new WIP defect and WIP repair
        final DefectDto newDefect = createWipDefect(JOB_1_ID);
        final RepairDto newRepair = createWipRepair(newDefect);

        newRepair.setDescription(newRepair.getDescription() + "X");
        final String originalStalenessHash = newRepair.getStalenessHash();

        // Successfully update the repair
        final RepairDto updatedRepair = TestUtils.putResponse(
                String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, newDefect.getJob().getId(), newDefect.getId(), newRepair.getId()), newRepair,
                HttpStatus.OK, RepairDto.class);

        // Attempt to update the repair using an old staleness hash.
        updatedRepair.setDescription(updatedRepair.getDescription() + "X");
        updatedRepair.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(
                String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, newDefect.getJob().getId(), newDefect.getId(), updatedRepair.getId()),
                updatedRepair, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.REPAIR.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public void testUpdateWIPRepairForCoCFailsWithAnOldHash() throws DatabaseUnitException
    {
        // Create a new WIP repair for a WIP CoC
        final RepairDto newRepair = createRepairOnWIPCoC("WIP Repair on WIP CoC for Staleness test");

        newRepair.setDescription(newRepair.getDescription() + "X");
        final String originalStalenessHash = newRepair.getStalenessHash();

        // Successfully update the repair
        final RepairDto updatedRepair = TestUtils.putResponse(
                String.format(SPECIFIC_JOB_WIP_COC_WIP_REPAIR, JOB_1_ID, VALID_REPAIR_COC_ID, newRepair.getId()), newRepair,
                HttpStatus.OK, RepairDto.class);

        // Attempt to update the repair using an old staleness hash.
        updatedRepair.setDescription(updatedRepair.getDescription() + "X");
        updatedRepair.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(
                String.format(SPECIFIC_JOB_WIP_COC_WIP_REPAIR, JOB_1_ID, VALID_REPAIR_COC_ID, updatedRepair.getId()),
                updatedRepair, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.REPAIR.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    private DefectDto createDefect()
    {
        this.minimalDefect.setId(null);
        this.minimalDefect.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item = new DefectItemDto();
        item.setItem(this.itemLight7);
        this.minimalDefect.setItems(new ArrayList<DefectItemDto>());
        this.minimalDefect.getItems().add(item);
        this.minimalDefect.setJobs(new ArrayList<JobDefectDto>());
        this.minimalDefect.getJobs().add(new JobDefectDto());
        this.minimalDefect.getJobs().get(0).setJob(new LinkResource(JOB_1_ID));
        this.minimalDefect.getJobs().get(0).setId(JOB_DEFECT_1_ID);

        return TestUtils.postResponse(BASE_DEFECT, this.minimalDefect, HttpStatus.OK,
                DefectDto.class);
    }

    /**
     * Test for creating WIP defect repairs and making sure that the defect is only closed when the item has a permanent
     * repair that is confirmed.
     */
    @Test
    @Ignore
    public void testWIPRepairLifeCycle()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item = new DefectItemDto();
        item.setItem(this.itemLight7);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.getItems().add(item);
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());
        cleanRepair(newWIPDefect.getId(), MODIFICATION, this.wipRepair1);

        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(newWIPDefect.getItems().iterator().next().getId()));
        this.wipRepair1.getRepairs().add(repairItem);

        this.wipRepair1.setId(null);

        final RepairDto newWIPRepair = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newWIPRepair);
        assertNotNull(newWIPRepair.getId());

        // no survey as repair isn't permanent
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // permanent repair
        newWIPRepair.setConfirmed(Boolean.TRUE);
        newWIPRepair.getRepairAction().setId(PERMANENT);

        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // ensure closed
        assertEquals("defect closed", DEFECT_CLOSED, response2.getDefectStatus().getId());

        // survey created
        final SurveyDto survey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(survey);
        // clean it up
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, survey.getJob().getId(), survey.getId()), HttpStatus.OK);

    }

    /**
     * Test for creating defect repairs on a defect with multiple items and making sure that the defect is only closed
     * when each item has a permanent repair that is confirmed.
     */
    @Test
    public void testRepairLifeCycleForDefectWithTwoItems() throws DatabaseUnitException
    {
        this.minimalDefect.setId(null);
        this.minimalDefect.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item1 = new DefectItemDto();
        item1.setItem(this.itemLight7);
        final DefectItemDto item2 = new DefectItemDto();
        item2.setItem(this.itemLight8);
        this.minimalDefect.setItems(new ArrayList<DefectItemDto>());
        this.minimalDefect.getItems().add(item1);
        this.minimalDefect.getItems().add(item2);

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.minimalDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newDefect.getDefectStatus().getId());

        cleanRepair(newDefect.getId(), MODIFICATION, this.minimalRepair);

        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(newDefect.getItems().get(0).getId()));
        this.minimalRepair.getRepairs().add(repairItem);

        final RepairDto newRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newRepair);
        assertNotNull(newRepair.getId());

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // permanent repair on item 1
        newRepair.getRepairAction().setId(PERMANENT);
        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response2.getDefectStatus().getId());

        // temporary repair on item 2
        cleanRepair(newDefect.getId(), TEMPORARY, this.minimalRepair);

        final RepairItemDto repairItem2 = new RepairItemDto();
        repairItem2.setItem(makeDefectItemLinkResource(newDefect.getItems().get(1).getId()));
        this.minimalRepair.getRepairs().add(repairItem2);

        final RepairDto newRepair2 = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        final DefectDto response3 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response3.getDefectStatus().getId());

        // make repair to item 2 permanent, but don't confirm.
        newRepair2.getRepairAction().setId(PERMANENT);
        newRepair2.setConfirmed(false);
        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair2.getId()),
                        newRepair2, HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response4.getDefectStatus().getId());

        final RepairDto repair2ToGetStalenessHash = TestUtils
                .getResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair2.getId(),
                        HttpStatus.OK), RepairDto.class);

        // confirm the permanent repair on item 2.
        newRepair2.setConfirmed(true);
        newRepair2.setStalenessHash(repair2ToGetStalenessHash.getStalenessHash());

        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair2.getId()),
                        newRepair2, HttpStatus.OK, RepairDto.class);

        final DefectDto response5 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // the defect should now be closed.
        assertEquals("defect open", DEFECT_CLOSED, response5.getDefectStatus().getId());
    }

    /**
     * Test for creating WIP defect repairs on a defect with multiple items and making sure that the WIP defect is only
     * closed when each item has a permanent repair that is confirmed.
     */
    @Test
    @Ignore
    public void testWIPRepairLifeCycleForDefectWithTwoItems() throws DatabaseUnitException
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item1 = new DefectItemDto();
        item1.setItem(this.itemLight7);
        final DefectItemDto item2 = new DefectItemDto();
        item2.setItem(this.itemLight8);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.getItems().add(item1);
        this.wipDefect1.getItems().add(item2);
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());

        cleanRepair(newWIPDefect.getId(), MODIFICATION, this.wipRepair1);

        final RepairItemDto wipRepairItem = new RepairItemDto();
        wipRepairItem.setItem(makeDefectItemLinkResource(newWIPDefect.getItems().get(0).getId()));
        this.wipRepair1.getRepairs().add(wipRepairItem);

        final RepairDto newWIPRepair = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newWIPRepair);
        assertNotNull(newWIPRepair.getId());

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // no repair survey yet as no confirmed permanent repair
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));

        // permanent repair on item 1
        newWIPRepair.getRepairAction().setId(PERMANENT);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);

        // first survey should have been created. Clean it up at this point.
        SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response2.getDefectStatus().getId());

        // temporary repair on item 2
        cleanRepair(newWIPDefect.getId(), TEMPORARY, this.wipRepair1);

        final RepairItemDto wipRepairItem2 = new RepairItemDto();
        wipRepairItem2.setItem(makeDefectItemLinkResource(newWIPDefect.getItems().get(1).getId()));
        this.wipRepair1.getRepairs().add(wipRepairItem2);

        final RepairDto newWIPRepair2 = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        final DefectDto response3 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response3.getDefectStatus().getId());

        // make repair to item 2 permanent, but don't confirm.
        newWIPRepair2.getRepairAction().setId(PERMANENT);
        newWIPRepair2.setConfirmed(false);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair2.getId()), newWIPRepair2,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // the defect should still be open
        assertEquals("defect open", DEFECT_OPEN, response4.getDefectStatus().getId());

        // confirm the permanent repair on item 2.
        newWIPRepair2.setConfirmed(true);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair2.getId()), newWIPRepair2,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response5 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // the defect should now be closed.
        assertEquals("defect open", DEFECT_CLOSED, response5.getDefectStatus().getId());

        // second survey should have been created here. Clean that up too.
        repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);
    }

    /**
     * Test to make sure a defect is only closed when there is a confirmed permanent repair associated with it. This
     * test makes sure that only one is needed even if there are other repairs.
     */
    @Test
    public void testRepairLifeCycleForDefectWithNoItemsButTwoRepairs()
    {
        this.minimalDefect.setId(null);
        this.minimalDefect.getDefectStatus().setId(DEFECT_OPEN);
        this.minimalDefect.setItems(new ArrayList<DefectItemDto>());

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.minimalDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newDefect.getDefectStatus().getId());

        cleanRepair(newDefect.getId(), MODIFICATION, this.minimalRepair);

        final RepairDto newRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newRepair);
        assertNotNull(newRepair.getId());

        // post a second repair
        TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // temporary repair, confirmed
        newRepair.getRepairAction().setId(PERMANENT);
        newRepair.setConfirmed(true);
        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // defect should be closed
        assertEquals("defect closed", DEFECT_CLOSED, response4.getDefectStatus().getId());
    }

    /**
     * Test to make sure a defect is only closed when there is a confirmed permanent repair associated with it. This
     * test makes sure that only one is needed even if there are other repairs.
     */
    @Test
    @Ignore
    // AssetItem
    public void testWIPRepairLifeCycleForDefectWithNoItemsButTwoRepairs()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());

        cleanRepair(newWIPDefect.getId(), MODIFICATION, this.wipRepair1);

        final RepairDto newWIPRepair = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newWIPRepair);
        assertNotNull(newWIPRepair.getId());

        // post a second repair
        TestUtils.postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                RepairDto.class);

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));

        // temporary repair, confirmed
        newWIPRepair.getRepairAction().setId(PERMANENT);
        newWIPRepair.setConfirmed(true);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // defect should be closed
        assertEquals("defect closed", DEFECT_CLOSED, response4.getDefectStatus().getId());

        // survey should have been created. Clean it up too.
        final SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);
    }

    /**
     * Test to make sure a defect is only closed when there is a confirmed permanent repair associated with it.
     */
    @Test
    public void testRepairLifeCycleForDefectWithNoItems()
    {
        this.minimalDefect.setId(null);
        this.minimalDefect.getDefectStatus().setId(DEFECT_OPEN);
        this.minimalDefect.setItems(new ArrayList<DefectItemDto>());

        final DefectDto newDefect = TestUtils.postResponse(BASE_DEFECT, this.minimalDefect, HttpStatus.OK,
                DefectDto.class);

        assertNotNull(newDefect);
        assertNotNull(newDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newDefect.getDefectStatus().getId());

        cleanRepair(newDefect.getId(), MODIFICATION, this.minimalRepair);

        final RepairDto newRepair = TestUtils
                .postResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, newDefect.getAsset().getId(), newDefect.getId()), this.minimalRepair,
                        HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newRepair);
        assertNotNull(newRepair.getId());

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // permanent repair, but not confirmed
        newRepair.getRepairAction().setId(PERMANENT);
        newRepair.setConfirmed(false);
        final RepairDto newRepairUpdate = TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // assert defect not changed
        assertEquals("defect closed", DEFECT_OPEN, response2.getDefectStatus().getId());

        // temporary repair, confirmed
        newRepair.getRepairAction().setId(TEMPORARY);
        newRepair.setConfirmed(true);
        newRepair.setStalenessHash(newRepairUpdate.getStalenessHash());

        final RepairDto secondRepairUpdate = TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        final DefectDto response3 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // assert defect not changed
        assertEquals("defect closed", DEFECT_OPEN, response3.getDefectStatus().getId());

        // permanent repair, confirmed
        newRepair.getRepairAction().setId(PERMANENT);
        newRepair.setConfirmed(true);
        newRepair.setStalenessHash(secondRepairUpdate.getStalenessHash());
        TestUtils
                .putResponse(String.format(SPECIFIC_ASSET_DEFECT_REPAIR_PATH, newDefect.getAsset().getId(), newDefect.getId(), newRepair.getId()),
                        newRepair, HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_DEFECT, newDefect.getId()), HttpStatus.OK, DefectDto.class);
        // defect should be closed
        assertEquals("defect closed", DEFECT_CLOSED, response4.getDefectStatus().getId());
    }

    /**
     * Test to make sure a defect is only closed when there is a confirmed permanent repair associated with it.
     */
    @Test
    @Ignore
    // AssetItem
    public void testWIPRepairLifeCycleForDefectWithNoItems()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());

        cleanRepair(newWIPDefect.getId(), MODIFICATION, this.wipRepair1);

        final RepairDto newWIPRepair = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newWIPRepair);
        assertNotNull(newWIPRepair.getId());

        // assert defect not changed
        final DefectDto response = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        assertEquals("defect open", DEFECT_OPEN, response.getDefectStatus().getId());

        // permanent repair, but not confirmed
        newWIPRepair.getRepairAction().setId(PERMANENT);
        newWIPRepair.setConfirmed(false);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // assert defect not changed
        assertEquals("defect closed", DEFECT_OPEN, response2.getDefectStatus().getId());

        // temporary repair, confirmed
        newWIPRepair.getRepairAction().setId(TEMPORARY);
        newWIPRepair.setConfirmed(true);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);
        final DefectDto response3 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // assert defect not changed
        assertEquals("defect closed", DEFECT_OPEN, response3.getDefectStatus().getId());

        // after all this, no repair survey should have been created as we don't have a permanent repair.
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));

        // permanent repair, confirmed
        newWIPRepair.getRepairAction().setId(PERMANENT);
        newWIPRepair.setConfirmed(true);
        TestUtils
                .putResponse(String.format(SPECIFIC_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId(), newWIPRepair.getId()), newWIPRepair,
                        HttpStatus.OK, RepairDto.class);

        final DefectDto response4 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // defect should be closed
        assertEquals("defect closed", DEFECT_CLOSED, response4.getDefectStatus().getId());
        // should have a survey that we need to clean up
        final SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);

    }

    @Test
    @Ignore
    public final void testCreatingConfirmedPermanentWIPRepairImmediatelyClosesDefect()
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);

        final DefectItemDto item = new DefectItemDto();
        item.setItem(this.itemLight7);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.getItems().add(item);
        this.wipDefect1.setJob(new LinkResource(JOB_1_ID));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, JOB_1_ID), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());
        cleanRepair(newWIPDefect.getId(), MODIFICATION, this.wipRepair1);

        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setItem(makeDefectItemLinkResource(newWIPDefect.getItems().iterator().next().getId()));
        this.wipRepair1.getRepairs().add(repairItem);
        this.wipRepair1.setId(null);
        this.wipRepair1.setConfirmed(Boolean.TRUE);

        // permanent repair
        this.wipRepair1.setConfirmed(Boolean.TRUE);
        this.wipRepair1.getRepairAction().setId(PERMANENT);

        // no survey yet
        assertNull(checkSingleRepairSurveyExistance(newWIPDefect));

        final RepairDto newWIPRepair = TestUtils
                .postResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, JOB_1_ID, newWIPDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        assertNotNull(newWIPRepair);
        assertNotNull(newWIPRepair.getId());

        final DefectDto response2 = TestUtils.getResponse(String.format(SPECIFIC_JOB_WIP_DEFECT, JOB_1_ID, newWIPDefect.getId()), HttpStatus.OK,
                DefectDto.class);
        // ensure defect is closed immediately
        assertEquals("defect closed", DEFECT_CLOSED, response2.getDefectStatus().getId());

        // survey should have been created. Clean it up too.
        final SurveyDto repairSurvey = checkSingleRepairSurveyExistance(newWIPDefect);
        assertNotNull(repairSurvey);
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY_PATH, repairSurvey.getJob().getId(), repairSurvey.getId()), HttpStatus.OK);
    }

    @Test
    public final void testGetDefectRepairs()
    {
        TestUtils.getResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, ASSET_2_ID, DEFECT_4_ID), HttpStatus.OK);
    }

    @Test
    public final void testGetInvalidDefectRepairs()
    {
        TestUtils.getResponse(String.format(ASSET_DEFECT_REPAIRS_PATH, ASSET_1_ID, INVALID_DEFECT_REPAIR_ID), HttpStatus.NOT_FOUND);
    }

    private LinkResource makeLinkResource(final Long id)
    {
        final LinkResource linkResource = new LinkResource(id);
        return linkResource;
    }

    private DefectItemLinkDto makeDefectItemLinkResource(final Long id)
    {

        final DefectItemLinkDto linkResource = new DefectItemLinkDto();
        linkResource.setId(id);
        linkResource.setItem(makeLinkResource(id));
        return linkResource;
    }

    private RepairDto cleanRepair(final Long defectId, final Long actionId, final RepairDto repair)
    {
        repair.setId(null);
        repair.setRepairAction(new LinkResource(actionId));
        repair.setDefect(new LinkResource(defectId));
        repair.setConfirmed(true);
        repair.setRepairs(new ArrayList<RepairItemDto>());

        return repair;
    }

    private void initialiseRepairPage(final Boolean wip, final Long... ids)
    {
        this.repairPage = new RepairPage();
        RepairDto tempRepairDto;
        for (final Long id : ids)
        {
            try
            {
                tempRepairDto = DataUtils.getRepair(id, wip);
                this.repairPage.getContent().add(tempRepairDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    private DefectDto createWipDefect(final Long jobId)
    {
        this.wipDefect1.setId(null);
        this.wipDefect1.getDefectStatus().setId(DEFECT_OPEN);
        this.wipDefect1.setItems(new ArrayList<DefectItemDto>());
        this.wipDefect1.setJob(new LinkResource(jobId));

        final DefectDto newWIPDefect = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT, jobId), this.wipDefect1, HttpStatus.OK, DefectDto.class);

        assertNotNull(newWIPDefect);
        assertNotNull(newWIPDefect.getId());
        assertEquals("defect open", DEFECT_OPEN, newWIPDefect.getDefectStatus().getId());

        return newWIPDefect;
    }

    private RepairDto createWipRepair(final DefectDto wipDefect)
    {
        this.wipRepair1.setId(null);
        this.wipRepair1.setDefect(new LinkResource(wipDefect.getId()));
        this.wipRepair1.setDescription("New WIP Repair for Defect " + wipDefect.getId() + " via POST");

        // needs to be confirmed to test the creation of surveys
        this.wipRepair1.setConfirmed(Boolean.TRUE);

        // confirm that there are no surveys against it when we begin
        assertNull(checkSingleRepairSurveyExistance(wipDefect));
        final RepairDto newRepairDtoFromPost = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_REPAIR, wipDefect.getJob().getId(), wipDefect.getId()), this.wipRepair1, HttpStatus.OK,
                        RepairDto.class);

        return newRepairDtoFromPost;
    }
}
