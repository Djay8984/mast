package com.baesystems.ai.lr.monitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.PostIntegrationTest;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(PostIntegrationTest.class)
public class MonitorReportTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorReportTest.class);

    @Test
    public void scheduleReport()
    {
        final String monitoringUrl = "http://localhost:" + BaseRestTest.PORT_NUMBER + "/mast/monitoring?format=pdf";
        final byte[] result = TestUtils.getResponse(monitoringUrl, HttpStatus.OK).asByteArray();

        final File file = new File("monitoring_report.pdf");

        try (final FileOutputStream writer = new FileOutputStream(file))
        {
            writer.write(result, 0, result.length);
            LOGGER.info("scheduleReport - report created in:" + file.getAbsolutePath() + " exists=" + file.exists());
        }
        catch (final IOException exception)
        {
            LOGGER.error("scheduleReport", exception);
        }
    }
}
