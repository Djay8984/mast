package com.baesystems.ai.lr.rest.sequence;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.dbunit.DatabaseUnitException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.codicils.AssetNoteRestTest;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Response;

/**
 * Test class for testing the integrity of asset specific sequence implementation when multiple users create entities
 * which use the sequence in quick succession
 */
@Category(IntegrationTest.class)
public class AssetSequenceRestTest extends BaseRestTest
{
    private static final Long ASSET_1 = 1L;
    private static final Long ASSET_NOTE_9 = 9L;
    private final Map<Long, Integer> idSequenceMap = new LinkedHashMap<>();

    private AssetNoteDto assetNoteDto;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        assetNoteDto = getAssetNote();
    }

    private AssetNoteDto getAssetNote()
    {
        final AssetNoteDto temp = TestUtils
                .getResponse(format(AssetNoteRestTest.ASSET_ASSET_NOTE.concat("/%d"), ASSET_1, ASSET_NOTE_9), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);
        temp.setId(null);
        AssetNoteRestTest.setAssetNoteMandatoryFields(temp);
        return temp;
    }

    @Test
    @Ignore
    public void testCreateMultipleAssetNotes() throws InterruptedException
    {

        // Create many callables that all try to update the same case, on a limited number of threads.
        ExecutorService executorService = null;

        final int threadPoolSize = 5;
        final int numberOfUpdateAttempts = 100;
        final int futureTimeoutSeconds = 5;

        try
        {
            executorService = Executors.newFixedThreadPool(threadPoolSize);

            final Set<Callable<Integer>> callables = new HashSet<Callable<Integer>>();

            for (int index = 0; index < numberOfUpdateAttempts; index++)
            {
                callables.add(() ->
                {
                    try
                    {
                        return createAssetNote();
                    }
                    catch (final Exception e)
                    {
                        throw e;
                    }
                });
            }

            // Run them all
            final List<Future<Integer>> allFutures = executorService.invokeAll(callables);
            final Set<Integer> callResults = new HashSet<>();
            for (final Future<Integer> future : allFutures)
            {
                try
                {
                    callResults.add(future.get(futureTimeoutSeconds, TimeUnit.SECONDS));
                }
                catch (final ExecutionException ee)
                {
                    Assert.fail("An unexpected exception occurred on a callable trying to create an Asset Note : " + ee.getMessage());
                }
                catch (final TimeoutException e)
                {
                    Assert.fail("An unexpected time out exception occurred on a callable trying to create an Asset Note : " + e.getMessage());
                }
            }
        }
        finally
        {
            if (executorService != null)
            {
                executorService.shutdown();
            }
        }

        final Collection<Integer> allValues = idSequenceMap.values();
        final Set<Integer> valueSet = new HashSet<>(allValues);
        System.out.println(idSequenceMap.values());
        assertEquals(allValues.size(), valueSet.size());
    }

    private int createAssetNote()
    {
        final Response response = TestUtils.postResponse(format(AssetNoteRestTest.ASSET_ASSET_NOTE, ASSET_1), assetNoteDto, HttpStatus.OK);
        final AssetNoteDto returnedDto = TestUtils.as(AssetNoteDto.class, response);
        idSequenceMap.put(returnedDto.getId(), returnedDto.getSequenceNumber());
        return response.getStatusCode();
    }

    @After
    public void tearDown() throws Exception
    {
        for (final Long id : idSequenceMap.keySet())
        {
            TestUtils.deleteResponse(format(AssetNoteRestTest.DELETE_ASSET_ASSET_NOTE, ASSET_1, id), HttpStatus.OK);
        }
    }
}
