package com.baesystems.ai.lr.rest.assets;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetLightPageResourceDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.validation.DraftItemRelationshipDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.BusinessProcesses;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.NewObjects;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.jayway.restassured.response.Header;

@Category(IntegrationTest.class)
public class AssetVersioningRestTest extends BaseRestTest
{
    private static final String INCORRECT_HASH_DATA = "IncorrectHashData";

    // Table names
    private static final String MAST_ASSET_VERSIONED_ASSETITEM = "MAST_ASSET_VersionedAssetItem";
    private static final String MAST_ASSET_ASSETITEM_RELATIONSHIP = "MAST_ASSET_AssetItemRelationship";
    private static final String MAST_ASSET_ASSETITEM_ATTRIBUTE = "MAST_ASSET_AssetItemAttribute";

    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;

    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long REPORT_1_ID = 1L;

    private static final Long VERSION_1 = 1L;
    private static final Long VERSION_2 = 2L;
    private static final Long EIGHT_NINE = 89L;

    private static final Long ATTRIBUTE_4 = 4L;
    private static final Long ATTRIBUTE_TYPE_ID_FOR_MANDATORY_NON_MULTIPLE_TYPE = 366L;
    private static final Long ATTRIBUTE_TYPE_ID_FOR_MULTIPLE = 367L;
    private static final Long ITEM_91_ID = 91L;
    private static final Long ASSET_5_ID = 5L;
    private static final Long ITEM_1156_ID = 1156L;
    private static final Long ITEM_1324_ID = 1324L;
    private static final Long ATTRIBUTE_7717_ID = 7717L;

    private static final Long IS_RELATED_TO_ID = 9L;

    @Test
    public void getAssetVersionParameterWorks()
    {
        // Get Asset 1.
        final AssetLightDto assetV1 = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID).concat("?version=1")).as(AssetLightDto.class);
        assertTrue(assetV1.getAssetVersionId().equals(VERSION_1));

        // Get Asset 2.
        final AssetLightDto assetV2 = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID).concat("?version=2")).as(AssetLightDto.class);
        assertTrue(assetV2.getAssetVersionId().equals(VERSION_2));
    }

    @Test
    public void getAssetDefaultsToPublished() throws DatabaseUnitException
    {
        // Get published version when the 'version' parameter is omitted.
        final Long publishedVersion = DataUtils.getAssetPublishedVersion(ASSET_1_ID);
        final AssetLightDto publishedAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1_ID), HttpStatus.OK).as(AssetLightDto.class);
        assertTrue(publishedAsset.getAssetVersionId().equals(publishedVersion));
    }

    @Test
    public void getAssets()
    {
        // Get All Assets.
        final AssetLightPageResourceDto assetPage = TestUtils.getResponse(BASE_ASSET, HttpStatus.OK).as(AssetLightPageResourceDto.class,
                ObjectMapperType.GSON);
        final AssetLightDto assetLightDto = assetPage.getContent().get(0);

        // Confirm that the version return on a given asset is the published version
        TestUtils.singleItemFound(String.format(SPECIFIC_ASSET, assetLightDto.getId()), assetLightDto, false);

    }

    @Test
    public void verifyCannotCheckoutAssetTwice()
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        // Check is not already checked out, ignore the checkout if the user has already checked it out
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        final Long maxVersion = DataUtils.getMaxVersionForAsset(ASSET_1_ID);

        TestUtils.postResponse(String.format(CHECKOUT_ASSET, ASSET_1_ID), "", HttpStatus.BAD_REQUEST);
        final Long maxVersionAfterSecondCheckout = DataUtils.getMaxVersionForAsset(ASSET_1_ID);

        assertEquals("Max asset version incremented after second checkout.", maxVersion.compareTo(maxVersionAfterSecondCheckout), 0);
        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public void testGetCheckedOutByUser()
    {
        AssetMetaDto[] assetMetaDtos = TestUtils.getResponse(ASSETS_CHECKED_OUT_BY_USER).as(AssetMetaDto[].class);
        Assert.assertTrue(assetMetaDtos.length == 0);

        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        assetMetaDtos = TestUtils.getResponse(ASSETS_CHECKED_OUT_BY_USER).as(AssetMetaDto[].class);
        Assert.assertTrue(assetMetaDtos.length == 1);

        BusinessProcesses.checkAssetOut(ASSET_2_ID);
        assetMetaDtos = TestUtils.getResponse(ASSETS_CHECKED_OUT_BY_USER).as(AssetMetaDto[].class);
        Assert.assertTrue(assetMetaDtos.length == 2);

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
        BusinessProcesses.checkAssetIn(ASSET_2_ID);
        assetMetaDtos = TestUtils.getResponse(ASSETS_CHECKED_OUT_BY_USER).as(AssetMetaDto[].class);
        Assert.assertTrue(assetMetaDtos.length == 0);
    }

    @Test
    public void cannotCheckinWithoutFirstCheckingOut()
    {
        TestUtils.postResponse(String.format(CHECKIN_ASSET, ASSET_1_ID), "", HttpStatus.BAD_REQUEST);
    }

    @Test
    public void updateAssetHeader()
            throws DatabaseUnitException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        // Checkout
        final Long oldPublishedVersion = DataUtils.getMaxVersionForAsset(newAssetId);
        BusinessProcesses.checkAssetOut(newAssetId);

        // Update stuff
        final AssetLightDto currentAssetDto = TestUtils.getResponse(String.format(SPECIFIC_ASSET, newAssetId) + "?draft=true")
                .as(AssetLightDto.class);

        currentAssetDto.setHullIndicator(currentAssetDto.getHullIndicator() + 1);
        TestUtils.putResponse(String.format(SPECIFIC_ASSET, newAssetId), currentAssetDto, HttpStatus.OK);

        // Check in
        BusinessProcesses.checkAssetIn(newAssetId);

        final Long newPublishedVersion = DataUtils.getAssetPublishedVersion(newAssetId);
        assertEquals("Published version has not updated after AssetHeader update.", newPublishedVersion.compareTo(oldPublishedVersion + 1), 0);
    }

    @Test
    @Ignore
    // Discussed this with Tom and we decided to ignore it. He says he will fix it later.
    public void verifyNotUpVersionedWithNoChangeToAsset()
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final Long newAssetId = NewObjects.createAsset(ASSET_1_ID).getId();

        final Long publishedVersionAtStart = DataUtils.getAssetPublishedVersion(newAssetId);
        BusinessProcesses.checkAssetOut(newAssetId);

        final AssetLightDto currentAssetDto = TestUtils.getResponse(String.format(SPECIFIC_ASSET, newAssetId) + "?draft=true")
                .as(AssetLightDto.class);
        TestUtils.putResponse(String.format(SPECIFIC_ASSET, newAssetId), currentAssetDto, HttpStatus.OK);

        BusinessProcesses.checkAssetIn(newAssetId);

        final Long publishedVersionAtEnd = DataUtils.getAssetPublishedVersion(newAssetId);
        assertEquals("Published version has been updated after NO change to the asset", publishedVersionAtStart, publishedVersionAtEnd);
    }

    @Test
    public void preventAnotherUserFromCheckingOutTheAsset() throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        TestUtils.addCustomHeader(new Header("user", "A. Notheruser"));
        // Checkout an expect error
        TestUtils.postResponse(String.format(CHECKOUT_ASSET, ASSET_1_ID), "", HttpStatus.BAD_REQUEST);

        TestUtils.addCustomHeader(new Header("user", "Allan Hyde"));
        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public final void assetUpdateFailsWhenStaleTokenIsDifferent()
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        final AssetLightDto assetLightDto = TestUtils
                .getResponse(format(SPECIFIC_ASSET, ASSET_1_ID).concat("?draft=true"), HttpStatus.OK, AssetLightDto.class);

        assetLightDto.setStalenessHash(INCORRECT_HASH_DATA);

        final ErrorMessageDto error = TestUtils.putResponse(format(SPECIFIC_ASSET, ASSET_1_ID), assetLightDto, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ASSET.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public final void assetUpdateFailsWhenOldTokenIsUsed()
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        AssetLightDto assetLightDto = TestUtils.getResponse(format(SPECIFIC_ASSET, ASSET_1_ID).concat("?draft=true"), HttpStatus.OK,
                AssetLightDto.class);

        final String originalStalenessHash = assetLightDto.getStalenessHash();

        // Make a small change
        assetLightDto.setBuilder(assetLightDto.getBuilder() + "X");
        assetLightDto = TestUtils.putResponse(format(SPECIFIC_ASSET, ASSET_1_ID), assetLightDto, HttpStatus.OK, AssetLightDto.class);

        // Make another small change, but use the hash from the original GET
        assetLightDto.setBuilder(assetLightDto.getBuilder() + "X");
        assetLightDto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(SPECIFIC_ASSET, ASSET_1_ID), assetLightDto, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ASSET.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public final void assetUpdateAssetBadRequest()
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);

        final AssetLightDto assetLightDto = TestUtils
                .getResponse(format(SPECIFIC_ASSET, ASSET_1_ID).concat("?draft=true"), HttpStatus.OK, AssetLightDto.class);
        assetLightDto.getAssetCategory().setId(0L);

        TestUtils.putResponse(format(SPECIFIC_ASSET, ASSET_1_ID), assetLightDto, HttpStatus.BAD_REQUEST);

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public void createJobConfirmAssetIsVersioned()
    {
        final AssetDto publishedAsset = NewObjects.createAsset(ASSET_1_ID);

        final JobDto jobDto = TestUtils.getResponse(String.format(SPECIFIC_JOB, 1L)).as(JobDto.class);
        jobDto.setId(null);
        jobDto.setAsset(new LinkVersionedResource());
        jobDto.getAsset().setId(publishedAsset.getId());
        CollectionUtils.nullSafeStream(jobDto.getEmployees()).forEach(employeeLinkDto -> employeeLinkDto.setId(null));

        final OfficeLinkDto officeLinkDto = new OfficeLinkDto();
        officeLinkDto.setOffice(new LinkResource(2L));
        officeLinkDto.setOfficeRole(new LinkResource(1L));
        jobDto.setOffices(Collections.singletonList(officeLinkDto));

        final JobDto job = TestUtils.postResponse(BASE_JOB, jobDto, HttpStatus.OK).as(JobDto.class);

        assertEquals(job.getAsset().getVersion(), new Long(publishedAsset.getAssetVersionId() + 1));

        final AssetLightDto jobAsset = TestUtils
                .getResponse(String.format(SPECIFIC_ASSET_VERSION, publishedAsset.getId(), job.getAsset().getVersion()))
                .as(AssetLightDto.class);

        assertNotNull(jobAsset);
    }

    /*
     * This test is not hermetic and a candidate for refactor
     */
    @Test
    public void submitFARWithChangesPromotesPromotesJob()
            throws DatabaseUnitException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException
    {
        final Long maxVersion = DataUtils.getMaxVersionForAsset(ASSET_2_ID);

        final JobDto jobDto = TestUtils.getResponse(String.format(SPECIFIC_JOB, JOB_2_ID), HttpStatus.OK).as(JobDto.class);
        jobDto.setId(null);
        jobDto.getAsset().setId(ASSET_2_ID);
        final OfficeLinkDto officeLinkDto = new OfficeLinkDto();
        officeLinkDto.setOffice(new LinkResource(2L));
        officeLinkDto.setOfficeRole(new LinkResource(1L));
        jobDto.setOffices(Collections.singletonList(officeLinkDto));

        final JobDto newJobDto = TestUtils.postResponse(BASE_JOB, jobDto, HttpStatus.OK).as(JobDto.class);
        assertEquals("Asset not up-versioned after Job creation.", newJobDto.getAsset().getVersion().longValue(), maxVersion + 1);
        final Long newJobId = newJobDto.getId();

        final ReportDto reportDto = TestUtils.getResponse(String.format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), HttpStatus.OK).as(ReportDto.class);
        reportDto.setJob(new LinkResource());
        reportDto.getJob().setId(newJobId);
        reportDto.setId(null);
        reportDto.setHarmonisationDate(new Date());

        TestUtils.postResponse(String.format(BASE_REPORT, newJobId), reportDto, HttpStatus.OK);

        final Long publishedVersionAtEnd = DataUtils.getAssetPublishedVersion(ASSET_2_ID);
        assertEquals("Published version does not equal job version", publishedVersionAtEnd, newJobDto.getAsset().getVersion());

        final JobDto jobBubble = DataUtils.getJob(newJobId);
        assertEquals("Job version has not been created after submitting FAR.", jobBubble.getAsset().getVersion().longValue(),
                newJobDto.getAsset().getVersion() + 1);

    }

    @Test
    @Ignore
    // todo passing locally, failing on Jenkins - dependant on test order.. to refactor
    public void submitFARWithNoChangesDoesNotPromoteJob()
            throws DatabaseUnitException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException
    {
        final Long maxVersion = DataUtils.getMaxVersionForAsset(ASSET_2_ID);
        final Long publishedVersionAtStart = DataUtils.getAssetPublishedVersion(ASSET_2_ID);

        final JobDto jobDto = TestUtils.getResponse(String.format(SPECIFIC_JOB, JOB_2_ID), HttpStatus.OK).as(JobDto.class);
        jobDto.setId(null);
        jobDto.getAsset().setId(ASSET_2_ID);
        final OfficeLinkDto officeLinkDto = new OfficeLinkDto();
        officeLinkDto.setOffice(new LinkResource(2L));
        officeLinkDto.setOfficeRole(new LinkResource(1L));
        jobDto.setOffices(Collections.singletonList(officeLinkDto));

        final JobDto newJobDto = TestUtils.postResponse(BASE_JOB, jobDto, HttpStatus.OK).as(JobDto.class);
        final Long newJobVersion = newJobDto.getAsset().getVersion();
        assertEquals("Asset not up-versioned after Job creation.", newJobVersion.longValue(), maxVersion + 1);
        final Long newJobId = newJobDto.getId();

        final ReportDto reportDto = TestUtils.getResponse(String.format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), HttpStatus.OK).as(ReportDto.class);
        reportDto.setJob(new LinkResource());
        reportDto.getJob().setId(newJobId);
        reportDto.setId(null);
        final AssetLightDto jobVersionAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET_VERSION, ASSET_2_ID, newJobVersion), HttpStatus.OK)
                .as(AssetLightDto.class);
        reportDto.setHarmonisationDate(jobVersionAsset.getHarmonisationDate());
        TestUtils.postResponse(String.format(BASE_REPORT, newJobId), reportDto, HttpStatus.OK);

        final Long publishedVersionAtEnd = DataUtils.getAssetPublishedVersion(ASSET_2_ID);
        assertEquals("Published version has changed after no versionable changes submitted.", publishedVersionAtStart, publishedVersionAtEnd);

        final JobDto jobBubble = DataUtils.getJob(newJobId);
        assertEquals("Job has been up-versioned after no versionable changes.", jobBubble.getAsset().getVersion().longValue(),
                newJobDto.getAsset().getVersion().longValue());
    }

    /**
     * Tests the check in and out process, such that no items are expected to be created.
     */
    @Test
    public void testPassiveCheckinCheckoutDoesNotCreateItems()
            throws DatabaseUnitException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException,
            InterruptedException
    {
        final Long beforeItemCount = DataUtils.getRowCount(MAST_ASSET_VERSIONED_ASSETITEM);
        final Long beforeRelationshipCount = DataUtils.getRowCount(MAST_ASSET_ASSETITEM_RELATIONSHIP);
        final Long beforeAttributeCount = DataUtils.getRowCount(MAST_ASSET_ASSETITEM_ATTRIBUTE);

        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        BusinessProcesses.checkAssetIn(ASSET_1_ID);

        final Long afterItemCount = DataUtils.getRowCount(MAST_ASSET_VERSIONED_ASSETITEM);
        final Long afterItemRelationshipCount = DataUtils.getRowCount(MAST_ASSET_ASSETITEM_RELATIONSHIP);
        final Long afterAttributeCount = DataUtils.getRowCount(MAST_ASSET_ASSETITEM_ATTRIBUTE);

        Assert.assertEquals(beforeItemCount, afterItemCount);
        Assert.assertEquals(beforeRelationshipCount, afterItemRelationshipCount);
        Assert.assertEquals(beforeAttributeCount, afterAttributeCount);
    }

    /**
     * Test the creation of two new items, and confirm the attributes are created as defined in reference data
     *
     * @throws IntegrationTestException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    public void postDraftItemsFromRefDataTest() throws DatabaseUnitException, IllegalAccessException, InvocationTargetException,
            NoSuchMethodException, IntegrationTestException
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        BusinessProcesses.checkAssetOut(newAssetId);

        final DraftItemListDto draftItemListDto = DraftItemRestTest.getValidDraftItemList(newAsset.getAssetModel().getItems().get(0).getId());
        final List<LinkVersionedResource> newItems = DraftItemRestTest.createNewDraftItemsFromRefData(newAssetId, draftItemListDto);

        assertEquals(draftItemListDto.getDraftItemDtoList().size(), newItems.size());

        final LazyItemDto newItem1 = getItemHelper(newAssetId, newItems.get(0).getId(), true);
        Assert.assertEquals("ROU PLATFORM", newItem1.getName());
        Assert.assertEquals(TEN, newItem1.getAttributes().size());

        final LazyItemDto newItem2 = getItemHelper(newAssetId, newItems.get(1).getId(), true);
        Assert.assertEquals("ROU PLATFORM", newItem2.getName());
        Assert.assertEquals(TEN, newItem2.getAttributes().size());

        BusinessProcesses.checkAssetIn(newAssetId);
    }

    /**
     * This tests the recursive copy of Asset Model for Asset 1 onto a new Asset. Tightly coupled to the Asset 1 data
     * e.g. item names and structure
     * <p>
     * Asset one assumed structure
     * <p>
     * .    87
     * .     \
     * .      88
     * .     /  \
     * .    89   91
     * .   /    /  \
     * .  90   92  93
     */
    @Test
    public void copyFullAssetModel()
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        final LazyItemDto rootItem = BusinessProcesses.copyFullAssetModel(ASSET_1_ID, newAsset);

        BusinessProcesses.checkAssetOut(newAssetId);

        final LazyItemDto item88 = getItemHelper(ASSET_1_ID, 88L, false);
        final LazyItemDto copyOfItem88 = getItemHelper(newAssetId, rootItem, 0);
        Assert.assertEquals(item88.getName(), copyOfItem88.getName());

        final LazyItemDto item89 = getItemHelper(ASSET_1_ID, 89L, false);
        final LazyItemDto copyOfItem89 = getItemHelper(newAssetId, copyOfItem88, 0);
        Assert.assertEquals(item89.getName(), copyOfItem89.getName());

        final LazyItemDto item91 = getItemHelper(ASSET_1_ID, 91L, false);
        final LazyItemDto copyOfItem91 = getItemHelper(newAssetId, copyOfItem88, 1);
        Assert.assertEquals(item91.getName(), copyOfItem91.getName());

        final LazyItemDto item90 = getItemHelper(ASSET_1_ID, 90L, false);
        final LazyItemDto copyOfItem90 = getItemHelper(newAssetId, copyOfItem89, 0);
        Assert.assertEquals(item90.getName(), copyOfItem90.getName());

        final LazyItemDto item92 = getItemHelper(ASSET_1_ID, 92L, false);
        final LazyItemDto copyOfItem92 = getItemHelper(newAssetId, copyOfItem91, 0);
        Assert.assertEquals(item92.getName(), copyOfItem92.getName());

        final LazyItemDto item93 = getItemHelper(ASSET_1_ID, 93L, false);
        final LazyItemDto copyOfItem93 = getItemHelper(newAssetId, copyOfItem91, 1);
        Assert.assertEquals(item93.getName(), copyOfItem93.getName());
        Assert.assertEquals(FOUR, copyOfItem93.getAttributes().size());

        BusinessProcesses.checkAssetIn(newAssetId);
    }

    /**
     * This tests the explicit copy of Asset Model for Asset 1 onto a new Asset. Tightly coupled to the Asset 1 data
     * e.g. item names and structure
     * <p>
     * Asset one assumed structure
     * <p>
     * .    87
     * .     \
     * .      88
     * .     /  \
     * .    89   91
     * .   /    /  \
     * .  90   92  93
     * <p>
     * <p>
     * We want to copy the below branch only
     * <p>
     * <p>
     * .    87
     * .     \
     * .      88
     * .     /
     * .   89
     */
    @Test
    public void copyPartialAssetModel()
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        final LazyItemDto existingRootItem = TestUtils.getResponse(String.format(ROOT_ITEM, ASSET_1_ID), HttpStatus.OK)
                .as(LazyItemDto.class);
        final LinkVersionedResource itemToCopy = existingRootItem.getItems().get(0); // 88

        BusinessProcesses.checkAssetOut(newAssetId);

        final DraftItemDto draftItemDto = new DraftItemDto();
        draftItemDto.setFromAssetId(ASSET_1_ID);
        draftItemDto.setFromId(itemToCopy.getId());
        draftItemDto.setToAssetId(newAssetId);
        draftItemDto.setToParentId(newAsset.getAssetModel().getItems().get(0).getId()); // auto created root item

        final DraftItemRelationshipDto draftItemRelationshipDto = new DraftItemRelationshipDto();
        draftItemRelationshipDto.setRelationshipTypeId(1L);
        draftItemRelationshipDto.setOriginalItemId(EIGHT_NINE);
        draftItemRelationshipDto.setItemRelationshipDto(Collections.emptyList());

        draftItemDto.setItemRelationshipDto(Collections.singletonList(draftItemRelationshipDto));

        final DraftItemListDto draftItemListDto = new DraftItemListDto();
        draftItemListDto.setDraftItemDtoList(Collections.singletonList(draftItemDto));
        draftItemListDto.setCopyAttributes(true);

        final LazyItemDto rootItem = TestUtils
                .postResponse(String.format(DraftItemRestTest.BASE_DRAFT_ITEM, newAssetId), draftItemListDto, HttpStatus.OK)
                .as(LazyItemDto.class); // returns the root item on the new Asset, which now references a new child

        final LazyItemDto copyOfItem88 = getItemHelper(newAssetId, rootItem, 0);
        Assert.assertEquals("v2 ROU PLATFORM", copyOfItem88.getName());

        final LazyItemDto copyOfItem89 = getItemHelper(newAssetId, copyOfItem88, 0);
        Assert.assertEquals("v2 ANCHORING, MOORING AND TETHERING GROUP", copyOfItem89.getName());

        BusinessProcesses.checkAssetIn(newAssetId);
    }

    @Test
    public void createItemRelationship() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        BusinessProcesses.checkAssetOut(newAssetId);

        final Long rootItemId = newAsset.getAssetModel().getItems().get(0).getId();
        final DraftItemListDto draftItemListDto = DraftItemRestTest.getValidDraftItemList(rootItemId);
        final List<LinkVersionedResource> newItems = DraftItemRestTest.createNewDraftItemsFromRefData(newAssetId, draftItemListDto);

        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(newItems.get(0).getId());
        newItemRelationshipDto.setType(new LinkResource(IS_RELATED_TO_ID));

        TestUtils
                .postResponse(format(ITEM_RELATIONSHIP_BASE, newAssetId, newItems.get(1).getId()), newItemRelationshipDto, HttpStatus.OK,
                        ItemRelationshipDto.class);

        BusinessProcesses.checkAssetIn(newAssetId);

    }

    @Test
    public void deleteItemRelationship() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetDto newAsset = NewObjects.createAsset(ASSET_1_ID);
        final Long newAssetId = newAsset.getId();

        BusinessProcesses.checkAssetOut(newAssetId);

        // Add new items and add a relationship
        final Long rootItemId = newAsset.getAssetModel().getItems().get(0).getId();
        final DraftItemListDto draftItemListDto = DraftItemRestTest.getValidDraftItemList(rootItemId);
        final List<LinkVersionedResource> newItems = DraftItemRestTest.createNewDraftItemsFromRefData(newAssetId, draftItemListDto);

        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(newItems.get(0).getId());
        newItemRelationshipDto.setType(new LinkResource(IS_RELATED_TO_ID));

        final ItemRelationshipDto relationshipDto = TestUtils
                .postResponse(format(ItemRestTest.ITEM_RELATIONSHIP_BASE, newAssetId, newItems.get(1).getId()), newItemRelationshipDto,
                        HttpStatus.OK,
                        ItemRelationshipDto.class);

        // Now delete it.

        TestUtils
                .deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, newAssetId, newItems.get(1).getId(), relationshipDto.getId()), HttpStatus.OK);

        BusinessProcesses.checkAssetIn(newAssetId);

    }

    // /**
    // * Update an existing attribute, new versioned item should be created automatically, existing attributes copied,
    // * new attribute then updated
    // */
    // @Test
    // public final void attributeUpdateSuccessful()
    // {
    // checkAssetOut(ASSET_1_ID);
    //
    // final AttributeDto originalAttribute = new AttributeDto();
    // originalAttribute.setId(ATTRIBUTE_4);
    // originalAttribute.setAttributeType(new LinkResource(93L));
    // originalAttribute.setValue("CHANGED VALUE");
    //
    // final AttributeDto newAttribute = TestUtils
    // .putResponse(format(SPECIFIC_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID, ATTRIBUTE_4), originalAttribute, HttpStatus.OK)
    // .as(AttributeDto.class);
    //
    // assertNotEquals(originalAttribute.getId(), newAttribute.getId());
    // checkAssetIn(ASSET_1_ID);
    // }

    // /**
    // * TODO to be reworked with versioning
    // */
    // @Test
    // @Ignore
    // public final void attributeUpdateNotSuccessful()
    // {
    // this.attribute.setValue("?");
    // this.attribute.getAttributeType().setId(ZERO);
    // TestUtils.putResponse(format(SPECIFIC_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID, ATTRIBUTE_4), this.attribute,
    // HttpStatus.BAD_REQUEST);
    // }

    /*
     * This should be successful as the attribute is not mandatory
     */
    @Test
    public final void attributeCreateSuccessful() throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        final AttributeDto attribute = new AttributeDto();
        attribute.setId(null);
        attribute.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_ID_FOR_MULTIPLE));
        attribute.setValue("attributeCreateSuccessful");

        final AttributeDto newAttribute = TestUtils.postResponse(format(BASE_ATTRIBUTE, ASSET_1_ID, ITEM_91_ID), attribute, HttpStatus.OK,
                AttributeDto.class);

        assertNotNull(newAttribute);
        assertNotNull(newAttribute.getId());
        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    /*
     * This should fail as the attribute is mandatory and not multiple
     *
     * more data
     */
    @Test
    public final void attributeCreateNotSuccessful()
    {
        BusinessProcesses.checkAssetOut(ASSET_5_ID);

        final AttributeDto attribute = new AttributeDto();
        attribute.setId(null);
        attribute.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_ID_FOR_MANDATORY_NON_MULTIPLE_TYPE));
        attribute.setValue("attributeCreateSuccessful");
        TestUtils.postResponse(format(BASE_ATTRIBUTE, ASSET_5_ID, ITEM_1156_ID), attribute, HttpStatus.CONFLICT);

        BusinessProcesses.checkAssetIn(ASSET_5_ID);
    }

    // /*
    // * This should be successful as the attribute is not mandatory
    // */
    // @Test
    // public final void attributeDeleteSuccessful()
    // {
    // TestUtils.deleteResponse(format(SPECIFIC_ATTRIBUTE, ASSET_5_ID, ITEM_1324_ID, ATTRIBUTE_7717_ID), HttpStatus.OK);
    // }
    //
    // /**
    // * This should fail as the attribute is associated with a mandatory attribute type and even though it is
    // * Mandatory-multiple, there is only on instance in the test data.
    // * todo move this somewhere else either versioning or attribute Test
    // */
    // @Test
    // public final void attributeDeleteNotSuccessful()
    // {
    // TestUtils.deleteResponse(format(SPECIFIC_ATTRIBUTE, ASSET_5_ID, ITEM_1140_ID, ATTRIBUTE_8792_ID),
    // HttpStatus.CONFLICT);
    // }

    private static LazyItemDto getItemHelper(final Long assetId, final LazyItemDto parentItem, final Integer index)
    {
        return TestUtils.getResponse(
                String.format(SPECIFIC_ITEM, assetId, parentItem.getItems().get(index).getId()).concat("?includeDraft=true"),
                HttpStatus.OK)
                .as(LazyItemDto.class);
    }

    private static LazyItemDto getItemHelper(final Long assetId, final Long itemId, final boolean includeDraft)
    {
        return includeDraft
                ? TestUtils.getResponse(
                        String.format(SPECIFIC_ITEM, assetId, itemId).concat("?includeDraft=true"),
                        HttpStatus.OK)
                        .as(LazyItemDto.class)
                : TestUtils.getResponse(
                        String.format(SPECIFIC_ITEM, assetId, itemId),
                        HttpStatus.OK)
                        .as(LazyItemDto.class);
    }

    // NO uses get rid?
    // @Test
    // public void checkAssetOutAndIn() throws DatabaseUnitException
    // {
    // assertNull("Why is the asset checked out at the beginning of this test", DataUtils.getCheckedOutBy(ASSET_1_ID));
    // assertNull("Why is the asset checked out at the beginning of this test",
    // DataUtils.getAssetDraftVersion(ASSET_1_ID));
    //
    // final Long publishedVersion = DataUtils.getAssetPublishedVersion(ASSET_1_ID);
    // checkAssetOut(ASSET_1_ID);
    // final Long newDraftVersion = DataUtils.getAssetDraftVersion(ASSET_1_ID);
    // assertEquals(new Long(publishedVersion + 1), newDraftVersion);
    //
    // checkAssetIn(ASSET_1_ID);
    //
    // assertNull("Asset draft version has not been nulled on check-in", DataUtils.getAssetDraftVersion(ASSET_1_ID));
    // }
}
