package com.baesystems.ai.lr.rest.services;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class DictionaryServiceRestTest extends BaseRestTest
{
    private static final String DICTIONARY_PATH = urlBuilder(BASE_PATH, "/spellcheck/dictionary");

    /**
     * Test that a valid GET request returns text.
     */
    @Test
    public final void getDictionary()
    {
        TestUtils.getResponse(DICTIONARY_PATH, HttpStatus.OK);
    }
}
