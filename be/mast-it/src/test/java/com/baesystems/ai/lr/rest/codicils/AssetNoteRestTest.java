package com.baesystems.ai.lr.rest.codicils;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.BatchActionAssetNoteDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.AssetNotePage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DateHelperImpl;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class AssetNoteRestTest extends BaseRestTest
{
    /**
     * API Rest Paths
     */
    public static final String ASSET_ASSET_NOTE = urlBuilder(SPECIFIC_ASSET, "/asset-note");
    private static final String ASSET_ASSET_NOTE_WITH_STATUS = urlBuilder(ASSET_ASSET_NOTE, "?statusId=%d");
    private static final String ASSET_NOTE_FOR_ITEM = urlBuilder(SPECIFIC_ASSET, "/item/%d/asset-note");
    public static final String JOB_WIP_ASSET_NOTE = urlBuilder(SPECIFIC_JOB, "/wip-asset-note");
    private static final String NO_PARAM_TEMPLATE_ASSET_COUNT = urlBuilder(BASE_PATH, "/asset/asset-note/count");
    private static final String TEMPLATE_ASSET_COUNT = urlBuilder(NO_PARAM_TEMPLATE_ASSET_COUNT, "?template=%s");
    private static final String ASSET_NOTE_QUERY = urlBuilder(ASSET_ASSET_NOTE, "/query");
    public static final String DELETE_ASSET_ASSET_NOTE = urlBuilder(ASSET_ASSET_NOTE, "/%d");
    private static final String JOB_WIP_ASSET_NOTE_ITEM = urlBuilder(JOB_WIP_ASSET_NOTE, "/%d");
    private static final String WIP_ASSET_NOTE_QUERY = urlBuilder(JOB_WIP_ASSET_NOTE, "/query");
    private static final String BATCH_ACTION_ASSET_NOTE = urlBuilder(BASE_PATH, "/batch-action/asset-note");

    /**
     * Test Data
     */
    private static final Long NEGATIVE = -1L;
    private static final Long ZERO = 0L;
    private static final Long INVALID_ID = 8888L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long ASSET_3_ID = 3L;
    private static final Long ITEM_87_ID = 87L;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long STATUS_1_ID = 1L;
    private static final Long STATUS_2_ID = 2L;
    // These values are set because the CoC data comes before the asset note data in the Codicil table
    private static final Long ASSET_NOTE_1_ID = 6L;
    private static final Long WIP_ASSET_NOTE_1_ID = 8L;
    private static final Long EXISTING_CODICIL_OF_WRONG_TYPE_ID = 4L;
    private static final Long VALID_PARENT_CODICIL_ID = 9L;
    private static final Long CONFIDENTIALITY_RETURN_NOTHING = -1L;
    private static final Long STATUS_RETURN_NOTHING = -1L;
    private static final Long ONE = 1L;

    private static final Long TEMPLATE_1_ID = 1L;
    private static final Long TEMPLATE_2_ID = 2L;
    private static final Long TEMPLATE_4_ID = 4L;
    private static final Integer TEMPLATE_1_COUNT = 2;
    private static final Integer TEMPLATE_2_COUNT = 1;
    private static final Integer TEMPLATE_4_COUNT = 0;

    // Query search
    private static final Long ASSET_NOTE_9 = 9L;
    private static final Long WIP_ASSET_NOTE_23 = 23L;
    private static final Long CATEGORY_ID_1 = 1L;
    private static final Long STATUS_ID_1 = 1L;
    private static final Long ITEM_TYPE_25 = 25L;
    private static final Long ITEM_TYPE_327 = 327L;
    private static final Long CONFIDENTIALITY_1 = 1L;
    private static final Long ITEM_TYPE_2 = 2L;

    private static final Long ASSET_WITH_ASSET_NOTE_ID = 1L;
    private static final Long ASSET_WITHOUT_ASSET_NOTE_ID = 3L;
    private static final Long VALID_ASSET_NOTE_ID = 6L;
    private static final Long DELETE_THIS_ASSET_NOTE = 6L;
    private static final Long INVALID_ASSET_NOTE_ID = 999L;

    private static final Date TODAY = new Date();
    private static final Date A_LONG_TIME_AGO = new Date(0);
    private static final int FUTURE_IMPOSED = 1;

    private static final Integer QUERY_TEST_SIZE = 11;

    private static final Long ITEM_TYPE_315_ID = 315L;
    private static final Long INVALID_ITEM_TYPE_ID = 99999L;
    private static final int MINUS_2 = -2;

    private AssetNoteDto assetNoteDto1;
    private AssetNoteDto wipAssetNoteDto1;
    private AssetNotePage assetNotePage;

    private final DateHelper dateUtils = new DateHelperImpl();

    // Needs to be public for Theory to work.
    @DataPoints
    public static final String[] BAD_REQUESTS = {String.format(ASSET_ASSET_NOTE, NEGATIVE),
                                                 String.format(ASSET_ASSET_NOTE, ZERO),
                                                 BASE_PATH + "/asset" + "/SomeLetters" + "/asset-note",
                                                 BASE_PATH + "/asset" + "/SomeLetters",
                                                 String.format(JOB_WIP_ASSET_NOTE, NEGATIVE),
                                                 String.format(JOB_WIP_ASSET_NOTE, ZERO),
                                                 BASE_PATH + "/job" + "/SomeLetters" + "/wip-asset-note",
                                                 BASE_PATH + "/job" + "/SomeLetters"
    };

    @DataPoints("NON_WIP_QUERIES")
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints("WIP_QUERIES")
    public static final QueryAndResult[] WIP_QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        // Search description
        QUERIES[index] = new QueryAndResult("searchString", "Desc 9", new Long[]{ASSET_NOTE_9});
        ++index;
        // Search title
        QUERIES[index] = new QueryAndResult("searchString", "9title", new Long[]{ASSET_NOTE_9});
        ++index;
        // Search Surveyor guidance
        QUERIES[index] = new QueryAndResult("searchString", "sur9", new Long[]{ASSET_NOTE_9});
        ++index;
        QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1), new Long[]{ASSET_NOTE_9});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_ID_1), new Long[]{ASSET_NOTE_9});
        ++index;
        // QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_25), new
        // Long[]{ASSET_NOTE_9});
        // ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1), new Long[]{ASSET_NOTE_9});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{ASSET_NOTE_9});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{ASSET_NOTE_9});
        ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING), new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327), new Long[]{});

        index = 0;
        // Search description
        WIP_QUERIES[index] = new QueryAndResult("searchString", "*NOTE*", new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        // Search title
        WIP_QUERIES[index] = new QueryAndResult("searchString", "WIPA*", new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        // Search Surveyor guidance
        WIP_QUERIES[index] = new QueryAndResult("searchString", "SurvW*", new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1), new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_2_ID), new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        // WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_2), new
        // Long[]{WIP_ASSET_NOTE_23});
        // ++index;
        WIP_QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1), new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{WIP_ASSET_NOTE_23});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING), new Long[]{});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327), new Long[]{});
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        this.assetNoteDto1 = DataUtils.getAssetNote(ASSET_NOTE_1_ID, false);
        this.wipAssetNoteDto1 = DataUtils.getAssetNote(WIP_ASSET_NOTE_1_ID, true);
    }

    private void initialiseAssetNotePage(final Boolean wip, final Long... ids)
    {
        this.assetNotePage = new AssetNotePage();

        AssetNoteDto tempAssetNoteDto;
        for (final Long id : ids)
        {
            try
            {
                tempAssetNoteDto = DataUtils.getAssetNote(id, wip);
                this.assetNotePage.getContent().add(tempAssetNoteDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Test that a valid GET request for ../asset/x/asset-note/y results in a positive response. Expecting JSON
     * containing asset-note 1.
     */
    @Test
    public final void testGetAssetNote()
    {
        TestUtils.singleItemFound(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), this.assetNoteDto1, true);
    }

    /**
     * Confirms that one of the known associated WIP elements is in the dto.
     */
    @Test
    public final void testGetAssetNoteConfirmWIPChildrenPresent()
    {
        final AssetNoteDto dto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        boolean containsExpectedChild = false;
        for (final LinkResource link : dto.getChildWIPs())
        {
            if (link.getId().equals(WIP_ASSET_NOTE_23))
            {
                containsExpectedChild = true;
                break;
            }
        }

        Assert.assertTrue(containsExpectedChild);
    }

    /**
     * Test that an invalid GET request for ../asset/x/asset-note/y results in a positive response. Expecting not found
     * exception for a non-existent asset-note
     */
    @Test
    public final void testGetAssetNoteFails()
    {
        TestUtils.singleItemNotFound(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, INVALID_ID));
    }

    /**
     * Test that a valid GET request for ../job/x/wip-asset-note/y results in a positive response. Expecting JSON
     * containing wip-asset-note 1.
     */
    @Test
    public final void testGetWIPAssetNote()
    {
        TestUtils.singleItemFound(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), this.wipAssetNoteDto1, true);
    }

    /**
     * Test that an invalid GET request for ../job/x/wip-asset-note/y results in a positive response. Expecting not
     * found exception for a non-existent asset-note
     */
    @Test
    public final void testGetWIPAssetNoteFails()
    {
        TestUtils.singleItemNotFound(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, INVALID_ID));
    }

    /**
     * Test that a valid POST request for ../asset/x/asset-note results in a positive response. Expecting JSON
     * containing newly saved asset-note.
     */
    @Test
    public final void testCreateAssetNote()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "stalenessHash"};
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK,
                AssetNoteDto.class);
        assetNoteDto.setId(null);
        setAssetNoteMandatoryFields(assetNoteDto);

        final AssetNoteDto body = TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_1_ID), assetNoteDto, HttpStatus.OK, AssetNoteDto.class);

        TestUtils.compareFields(body, assetNoteDto, false, ignoredFields);
    }

    @Test
    public final void testCreateAssetNoteHistoricalImposedFails()
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);
        assetNoteDto.setId(null);
        setAssetNoteMandatoryFields(assetNoteDto);
        assetNoteDto.setImposedDate(A_LONG_TIME_AGO);
        TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_1_ID), assetNoteDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../asset/x/asset-note results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testCreateAssetNoteFailsWithIDPopulated()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_1_ID), assetNoteDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../asset/x/asset-note results in a positive response. Expecting not found
     * exception, for an invalid assetId
     */
    @Test
    public final void testCreateAssetNoteFailsAssetNotFound()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);
        TestUtils.postResponse(format(ASSET_ASSET_NOTE, INVALID_ID), assetNoteDto, HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid POST request for ../job/x/wip-asset-note results in a positive response. Expecting JSON
     * containing newly saved wip-asset-note.
     */
    @Test
    public final void testCreateWIPAssetNote()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "stalenessHash"};
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();

        setAssetNoteMandatoryFields(assetNoteDto);

        final AssetNoteDto body = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.OK, AssetNoteDto.class);

        TestUtils.compareFields(body, assetNoteDto, false, ignoredFields);
    }

    /**
     * Test that a valid POST request for ../job/x/wip-asset-note with null job scope confirmed results in and error.
     */
    @Test
    public final void testCreateWIPAssetNoteFailsNullJobScopeConfirmed()
    {
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();
        setAssetNoteMandatoryFields(assetNoteDto);
        assetNoteDto.setJobScopeConfirmed(null);

        final ErrorMessageDto error = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());

    }

    /**
     * Tests an invalid POST request for ../job/x/wip-asset-note results in a positive response. Expecting JSON Bad
     * Request Exception.
     */
    @Test
    public final void testCreateWIPAssetNoteFailsWithIDPopulated()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils
                .getResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.OK).getBody().as(AssetNoteDto.class);
        TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Tests an invalid POST request for ../job/x/wip-asset-note when the parent id doesn't exist.
     */
    @Test
    public final void testCreateWIPCoCFailsWhenParentIdNotInDatabase()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();
        setAssetNoteMandatoryFields(assetNoteDto);
        final LinkResource parent = new LinkResource(INVALID_ASSET_NOTE_ID);
        assetNoteDto.setParent(parent);
        final String errorMessage = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.NOT_FOUND).jsonPath()
                .get("message");
        assertEquals(String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.", INVALID_ASSET_NOTE_ID),
                errorMessage);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-asset-note fails when the parent is not of the same codicil
     * type.
     */
    @Test
    public final void testCreateWIPCoCFailsWhenParentTypeDifferent()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();
        setAssetNoteMandatoryFields(assetNoteDto);
        final LinkResource parent = new LinkResource(EXISTING_CODICIL_OF_WRONG_TYPE_ID);
        assetNoteDto.setParent(parent);
        final String errorMessage = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.NOT_FOUND).jsonPath()
                .get("message");
        assertEquals(
                String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.", EXISTING_CODICIL_OF_WRONG_TYPE_ID),
                errorMessage);
    }

    public static AssetNoteDto getAssetNoteForWipPost()
    {
        final AssetNoteDto assetNoteDto = TestUtils
                .getResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.OK, AssetNoteDto.class);
        assetNoteDto.setId(null);
        final LinkResource parent = new LinkResource(VALID_PARENT_CODICIL_ID);
        assetNoteDto.setParent(parent);

        return assetNoteDto;
    }

    /**
     * Tests valid GET ../asset/x/asset-note returns a List of valid asset-notes including asset-note1
     */
    @Test
    public final void testGetAssetNotesForAsset()
    {
        this.initialiseAssetNotePage(false, ASSET_NOTE_1_ID);
        TestUtils.listItemsFound(String.format(ASSET_ASSET_NOTE, ASSET_1_ID), this.assetNotePage, true, false);
    }

    /**
     * Tests valid GET ../job/x/wip-asset-note returns a List of valid wip-assetnotes including wip-asset-note1
     */
    @Test
    public final void testGetWIPAssetNotesForJob()
    {
        this.initialiseAssetNotePage(true, WIP_ASSET_NOTE_1_ID);
        TestUtils.listItemsFound(String.format(JOB_WIP_ASSET_NOTE, JOB_1_ID), this.assetNotePage, true, false);
    }

    /**
     * Tests valid GET ../asset/x/coc with Status parameter returns a List of valid CoC including Coc1
     */
    @Test
    public final void testGetAssetNoteForAssetWithStatusFound()
    {
        this.initialiseAssetNotePage(false, ASSET_NOTE_1_ID);
        TestUtils.listItemsFound(String.format(ASSET_ASSET_NOTE_WITH_STATUS, ASSET_1_ID, STATUS_1_ID), this.assetNotePage, true, false);
    }

    /**
     * Tests valid GET ../asset/x/coc with Status parameter and returns an empty list
     */
    @Test
    public final void testGetAssetNoteForAssetWithStatusNonFound()
    {
        TestUtils.listItemsNotFound(String.format(ASSET_ASSET_NOTE_WITH_STATUS, ASSET_1_ID, STATUS_2_ID));
    }

    /**
     * Tests valid PUT ../asset/x/asset-note/y updates asset-note and returns the updated value
     */
    @Test
    public final void testUpdateAssetNote()
    {
        final AssetNoteDto existingAssetNoteDto = TestUtils
                .getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        existingAssetNoteDto.setId(null);
        setAssetNoteMandatoryFields(existingAssetNoteDto);
        existingAssetNoteDto.setDescription("AN Ready For Updating");

        // TODO Currently, the new Asset Note Dto being returned includes child WIPS where they are no being persisted.
        // This causes the hash returned from the POST to be different to the hash returned from a GET.
        final AssetNoteDto newAssetNoteDto = TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_1_ID), existingAssetNoteDto, HttpStatus.OK,
                AssetNoteDto.class);

        final AssetNoteDto gettingNewAssetNoteDto = TestUtils
                .getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, newAssetNoteDto.getId()), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        gettingNewAssetNoteDto.setDescription(gettingNewAssetNoteDto.getDescription() + "X");

        final AssetNoteDto updatedAssetNoteDto = TestUtils
                .putResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, gettingNewAssetNoteDto.getId()), gettingNewAssetNoteDto,
                        HttpStatus.OK,
                        AssetNoteDto.class);

        assertEquals(gettingNewAssetNoteDto.getId(), updatedAssetNoteDto.getId());
        assertEquals(updatedAssetNoteDto.getDescription(), gettingNewAssetNoteDto.getDescription());
    }

    @Test
    public final void testUpdateAssetNoteFailsWhenAnOldTokenIsUsed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {

        final AssetNoteDto existingAssetNoteDto = TestUtils
                .getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        existingAssetNoteDto.setId(null);
        setAssetNoteMandatoryFields(existingAssetNoteDto);
        existingAssetNoteDto.setDescription("AN Ready For Updating");

        // TODO Currently, the new Asset Note Dto being returned includes child WIPS where they are no being persisted.
        // This causes the hash returned from the POST to be different to the hash returned from a GET.
        final AssetNoteDto newAssetNoteDto = TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_1_ID), existingAssetNoteDto, HttpStatus.OK,
                AssetNoteDto.class);

        final AssetNoteDto gettingNewAssetNoteDto = TestUtils
                .getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, newAssetNoteDto.getId()), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        final String originalStalenessHash = gettingNewAssetNoteDto.getStalenessHash();

        // Make a small change
        gettingNewAssetNoteDto.setDescription(gettingNewAssetNoteDto.getDescription() + "X");

        final AssetNoteDto updatedAssetNoteDto = TestUtils
                .putResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, gettingNewAssetNoteDto.getId()), gettingNewAssetNoteDto,
                        HttpStatus.OK, AssetNoteDto.class);

        // Make another small change, but use the hash from the original GET
        updatedAssetNoteDto.setDescription(updatedAssetNoteDto.getDescription() + "X");
        updatedAssetNoteDto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, updatedAssetNoteDto.getId()),
                updatedAssetNoteDto, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ASSET_NOTE.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    /**
     * Tests an invalid PUT ../asset/x/asset-note/y where the child WIPs for an existing element are attempted to be
     * removed. The DTO returned on the put displays the codicil in the correct state.
     */
    @Test
    public final void testInvalidUpdateChildEntitiesDoesNotBreak()
    {
        final AssetNoteDto originalAssetNoteDto = TestUtils
                .getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK).getBody().as(AssetNoteDto.class);

        originalAssetNoteDto.setChildWIPs(null);

        final AssetNoteDto updatedAssetNoteDto = TestUtils
                .putResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), originalAssetNoteDto, HttpStatus.OK,
                        AssetNoteDto.class);

        Assert.assertNotNull(updatedAssetNoteDto.getChildWIPs());
    }

    /**
     * Tests valid PUT ../job/x/wip-asset-note/y updates wip-asset-note and returns the updated value
     */
    @Test
    public final void testUpdateWIPAssetNote()
    {
        final AssetNoteDto originalAssetNoteDto = TestUtils
                .getResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        final String newDescription = "new Description";
        originalAssetNoteDto.setDescription(newDescription);

        final AssetNoteDto updatedAssetNoteDto = TestUtils
                .putResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), originalAssetNoteDto, HttpStatus.OK,
                        AssetNoteDto.class);

        assertEquals(originalAssetNoteDto.getId(), updatedAssetNoteDto.getId());
        assertEquals(updatedAssetNoteDto.getDescription(), newDescription);
    }

    @Test
    public final void testUpdateWIPAssetNoteFailsWhenAnOldTokenIsUsed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto existingAssetNoteDto = TestUtils
                .getResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        existingAssetNoteDto.setId(null);
        setAssetNoteMandatoryFields(existingAssetNoteDto);
        existingAssetNoteDto.setDescription("AN Ready For Updating");

        final AssetNoteDto newAssetNoteDto = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), existingAssetNoteDto, HttpStatus.OK)
                .jsonPath().getObject("", AssetNoteDto.class);

        final String originalStalenessHash = newAssetNoteDto.getStalenessHash();

        // Make a small change
        newAssetNoteDto.setDescription(newAssetNoteDto.getDescription() + "X");

        final AssetNoteDto updatedAssetNoteDto = TestUtils
                .putResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, newAssetNoteDto.getId()), newAssetNoteDto,
                        HttpStatus.OK)
                .jsonPath().getObject("", AssetNoteDto.class);

        // Make another small change, but use the hash from the original GET
        updatedAssetNoteDto.setDescription(updatedAssetNoteDto.getDescription() + "X");
        updatedAssetNoteDto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, updatedAssetNoteDto.getId()),
                updatedAssetNoteDto, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ASSET_NOTE.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    /**
     * Tests invalid PUT ../job/x/wip-asset-note/y fails when job scope confirmed is null
     */
    @Test
    public final void testUpdateWIPAssetNoteFailsNullJobScopeConfirmed()
    {
        final AssetNoteDto originalAssetNoteDto = TestUtils
                .getResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        originalAssetNoteDto.setJobScopeConfirmed(null);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(JOB_WIP_ASSET_NOTE.concat("/%d"), JOB_1_ID, WIP_ASSET_NOTE_1_ID), originalAssetNoteDto, HttpStatus.BAD_REQUEST,
                        ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    /**
     * Test that a valid POST request for ../asset/x/item/y/asset-note results in a positive response. Expecting JSON
     * containing newly saved asset-note.
     */
    @Test
    public final void testCreateAssetNoteForItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "stalenessHash"};
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK,
                AssetNoteDto.class);

        assetNoteDto.setId(null);
        final ItemLightDto assetItem = new ItemLightDto();
        assetItem.setId(ITEM_87_ID);
        assetNoteDto.setAssetItem(assetItem);
        setAssetNoteMandatoryFields(assetNoteDto);

        final AssetNoteDto body = TestUtils.postResponse(format(ASSET_NOTE_FOR_ITEM, ASSET_1_ID, ITEM_87_ID), assetNoteDto, HttpStatus.OK,
                AssetNoteDto.class);

        TestUtils.compareFields(body, assetNoteDto, false, ignoredFields);
    }

    /**
     * Test that an invalid POST request for ../asset/x/item/y/asset-note results in a positive response. Expecting a
     * bad request exception.
     */
    @Test
    public final void testCreateAssetNoteForItemFailsWithIDPopulated()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        final ItemLightDto assetItem = new ItemLightDto();
        assetItem.setId(ITEM_87_ID);
        assetNoteDto.setAssetItem(assetItem);

        TestUtils.postResponse(format(ASSET_NOTE_FOR_ITEM, ASSET_1_ID, ITEM_87_ID), assetNoteDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../asset/x/item/y/asset-note results in a positive response. Expecting not
     * found exception, for an invalid assetId and itemId pair.
     */
    @Test
    public final void testCreateAssetNoteForItemFailsAssetItemPairNotFound1()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        assetNoteDto.setId(null);
        final ItemLightDto assetItem = new ItemLightDto();
        assetItem.setId(ITEM_87_ID);
        assetNoteDto.setAssetItem(assetItem);
        setAssetNoteMandatoryFields(assetNoteDto);

        TestUtils.postResponse(format(ASSET_NOTE_FOR_ITEM, ASSET_1_ID, INVALID_ID), assetNoteDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../asset/x/item/y/asset-note results in a positive response. Expecting not
     * found exception, for an invalid assetId and itemId pair.
     */
    @Test
    public final void testCreateAssetNoteForItemFailsAssetItemPairNotFound2()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final AssetNoteDto assetNoteDto = TestUtils.getResponse(format(ASSET_ASSET_NOTE.concat("/%d"), ASSET_1_ID, ASSET_NOTE_1_ID), HttpStatus.OK)
                .getBody()
                .as(AssetNoteDto.class);

        assetNoteDto.setId(null);
        assetNoteDto.getAsset().setId(INVALID_ID);
        final ItemLightDto assetItem = new ItemLightDto();
        assetItem.setId(ITEM_87_ID);
        assetNoteDto.setAssetItem(assetItem);
        setAssetNoteMandatoryFields(assetNoteDto);

        TestUtils.postResponse(format(ASSET_NOTE_FOR_ITEM, INVALID_ID, ITEM_87_ID), assetNoteDto, HttpStatus.NOT_FOUND);
    }

    /**
     * Path: /asset/asset-note/count <br>
     * Check that query parameter 'template' is considered mandatory by not passing it in at all.
     */
    @Test
    public final void countAssetsMissingTemplateIdParameter()
    {
        final ErrorMessageDto error = TestUtils.getResponse(NO_PARAM_TEMPLATE_ASSET_COUNT, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Query parameter template cannot be null.", error.getMessage());
    }

    /**
     * Path: /asset/asset-note/count?template= <br>
     * Check that query parameter 'template' is considered mandatory by including it but with no value.
     */
    @Test
    public final void countAssetsForEmptyTemplateId()
    {
        final ErrorMessageDto error = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, ""), HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Could not bind parameter  to number.", error.getMessage());
    }

    /**
     * Path: /asset/asset-note/count?template=abc <br>
     * Check that giving query parameter 'template' a non-number value will result in a friendly error message.
     */
    @Test
    public final void countAssetsForNonNumberTemplateId()
    {
        final ErrorMessageDto error = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "abc"), HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Could not bind parameter abc to number.", error.getMessage());
    }

    /**
     * Path: /asset/asset-note/count?template=1 <br>
     * Check that a single valid template ID will result in a positive response.
     */
    @Test
    public final void countAssetsForValidSingleTemplateId()
    {
        final TemplateAssetCountDto responseDto = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "1"), HttpStatus.OK,
                TemplateAssetCountDto.class);

        final Map<Long, Integer> counts = responseDto.getCounts();

        assertTrue(counts.containsKey(TEMPLATE_1_ID));
        assertEquals(TEMPLATE_1_COUNT, counts.get(TEMPLATE_1_ID));
    }

    /**
     * Path: /asset/asset-note/count?template=1,2,4 <br>
     * Check that multiple valid template IDs will result in a positive response.
     */
    @Test
    public final void countAssetsForValidMultipleTemplateId()
    {
        final TemplateAssetCountDto responseDto = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "1,2,4"), HttpStatus.OK,
                TemplateAssetCountDto.class);

        final Map<Long, Integer> counts = responseDto.getCounts();

        assertTrue(counts.containsKey(TEMPLATE_1_ID));
        assertEquals(TEMPLATE_1_COUNT, counts.get(TEMPLATE_1_ID));

        assertTrue(counts.containsKey(TEMPLATE_2_ID));
        assertEquals(TEMPLATE_2_COUNT, counts.get(TEMPLATE_2_ID));

        assertTrue(counts.containsKey(TEMPLATE_4_ID));
        assertEquals(TEMPLATE_4_COUNT, counts.get(TEMPLATE_4_ID));
    }

    @Test
    public final void testDeleteAssetNoteThatDoesNotExist()
    {
        final String url = format(DELETE_ASSET_ASSET_NOTE, ASSET_WITH_ASSET_NOTE_ID, INVALID_ASSET_NOTE_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteAssetNoteNotAssociatedWithThePassedAsset()
    {
        final String url = format(DELETE_ASSET_ASSET_NOTE, ASSET_WITHOUT_ASSET_NOTE_ID, VALID_ASSET_NOTE_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteAssetNote()
    {
        final String url = format(DELETE_ASSET_ASSET_NOTE, ASSET_WITH_ASSET_NOTE_ID, DELETE_THIS_ASSET_NOTE);

        final AssetNoteDto deletedAssetNote = TestUtils.getResponse(url, HttpStatus.OK, AssetNoteDto.class);

        TestUtils.deleteResponse(url, HttpStatus.OK);

        // Confirm the Asset Note is no longer returned.
        TestUtils.getResponse(format(DELETE_ASSET_ASSET_NOTE, ASSET_WITH_ASSET_NOTE_ID, DELETE_THIS_ASSET_NOTE), HttpStatus.NOT_FOUND);

        // Replace the deleted AssetNote to avoid interfering with other tests.
        deletedAssetNote.setId(null);
        deletedAssetNote.setImposedDate(new Date());
        deletedAssetNote.setDueDate(new Date());
        setAssetNoteMandatoryFields(deletedAssetNote);
        TestUtils.postResponse(format(ASSET_ASSET_NOTE, ASSET_WITH_ASSET_NOTE_ID), deletedAssetNote, HttpStatus.OK);
    }

    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ruleSetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    @Theory
    public final void queries(@FromDataPoints("NON_WIP_QUERIES") final QueryAndResult queryAndResult) throws NoSuchFieldException, SecurityException,
            IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.initialiseAssetNotePage(false, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final AssetNotePage results = TestUtils.postResponse(format(ASSET_NOTE_QUERY, ASSET_1_ID), query, HttpStatus.OK,
                AssetNotePage.class);

        TestUtils.compareFields(results, this.assetNotePage, true);
    }

    @Theory
    public final void wipQueries(@FromDataPoints("WIP_QUERIES") final QueryAndResult queryAndResult) throws NoSuchFieldException, SecurityException,
            IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.initialiseAssetNotePage(true, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final AssetNotePage results = TestUtils.postResponse(format(WIP_ASSET_NOTE_QUERY, JOB_1_ID), query, HttpStatus.OK,
                AssetNotePage.class);

        TestUtils.compareFields(results, this.assetNotePage, true);
    }

    @Test
    public final void testDeleteWIPAssetNoteThatDoesNotExist()
    {
        TestUtils.deleteResponse(format(JOB_WIP_ASSET_NOTE_ITEM, JOB_1_ID, INVALID_ASSET_NOTE_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteWIPAssetNoteNotAssociatedWithThePassedJob()
    {
        TestUtils.deleteResponse(format(JOB_WIP_ASSET_NOTE_ITEM, JOB_2_ID, WIP_ASSET_NOTE_1_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteWIPAssetNote()
    {
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();

        setAssetNoteMandatoryFields(assetNoteDto);

        final AssetNoteDto body = TestUtils.postResponse(format(JOB_WIP_ASSET_NOTE, JOB_1_ID), assetNoteDto, HttpStatus.OK, AssetNoteDto.class);

        TestUtils.deleteResponse(format(JOB_WIP_ASSET_NOTE_ITEM, JOB_1_ID, body.getId()), HttpStatus.OK);
    }

    @Test
    public final void testSaveValidBatchActionAssetNote()
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        final List<Long> outgoingAssetIds = new ArrayList<>(dto.getAssetIds());
        final BatchActionAssetNoteDto result = TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.OK, BatchActionAssetNoteDto.class);
        final Map<Long, List<AssetNoteDto>> assetNoteDtoMap = result.getSavedAssetNoteDtos();
        assertNotNull(assetNoteDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(assetNoteDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(outgoingAssetIds.contains(savedId));
        }

        for (final Long outgoingId : outgoingAssetIds)
        {
            assertTrue(savedAssetIds.contains(outgoingId));
        }
    }

    @Test
    public final void testSaveValidBatchActionAssetNoteFiltersNull()
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, null, ASSET_2_ID));
        final List<Long> outgoingAssetIds = new ArrayList<>(dto.getAssetIds());
        final BatchActionAssetNoteDto result = TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.OK, BatchActionAssetNoteDto.class);
        final Map<Long, List<AssetNoteDto>> assetNoteDtoMap = result.getSavedAssetNoteDtos();
        assertNotNull(assetNoteDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(assetNoteDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(outgoingAssetIds.contains(savedId));
        }

        for (final Long outgoingId : outgoingAssetIds)
        {
            if (outgoingId != null)
            {
                assertTrue(savedAssetIds.contains(outgoingId));
            }
            else
            {
                assertTrue(!savedAssetIds.contains(outgoingId));
            }
        }
    }

    @Test
    public final void testSaveValidBatchActionAssetRemovesDuplicateAssetIds()
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_1_ID));
        final BatchActionAssetNoteDto result = TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.OK, BatchActionAssetNoteDto.class);
        final Map<Long, List<AssetNoteDto>> assetNoteDtoMap = result.getSavedAssetNoteDtos();
        assertNotNull(assetNoteDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = new ArrayList<>();
        for (final Long assetId : assetNoteDtoMap.keySet())
        {
            assetNoteDtoMap.get(assetId).stream()
                    .forEach(localDto -> savedAssetIds.add(localDto.getAsset().getId()));
        }
        assertTrue(savedAssetIds.size() == 1);
        assertTrue(savedAssetIds.contains(ASSET_1_ID));
    }

    @Test
    public final void testSaveValidBatchActionAssetNoteFailsAllWithSingleInvalidAssetId()
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_2_ID, ASSET_3_ID, INVALID_ID));
        TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.NOT_FOUND);
    }

    @Test
    @Ignore
    public final void testSaveValidBatchAssetNoteWithItemType() throws DatabaseUnitException
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.setItemTypeId(ITEM_TYPE_315_ID);
        dto.setAssetIds(Arrays.asList(ASSET_2_ID, ASSET_3_ID, ASSET_1_ID));
        final List<Long> expectedSavedAssetIds = Arrays.asList(ASSET_1_ID, ASSET_3_ID);
        final List<Long> expectedRejectedAssetIds = Arrays.asList(ASSET_2_ID);

        final BatchActionAssetNoteDto result = TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.OK, BatchActionAssetNoteDto.class);

        final Map<Long, List<AssetNoteDto>> assetNoteDtoMap = result.getSavedAssetNoteDtos();
        final List<Long> rejectedAssetIds = result.getRejectedAssetIds();
        assertNotNull(assetNoteDtoMap);
        assertNotNull(rejectedAssetIds);

        assertEquals(expectedSavedAssetIds.size(), assetNoteDtoMap.size());
        assertEquals(expectedRejectedAssetIds.size(), rejectedAssetIds.size());
        assertTrue(rejectedAssetIds.contains(ASSET_2_ID));

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(assetNoteDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(expectedSavedAssetIds.contains(savedId));
        }

        for (final Long expectedId : expectedSavedAssetIds)
        {
            assertTrue(savedAssetIds.contains(expectedId));
        }

        for (final Long assetId : assetNoteDtoMap.keySet())
        {
            final List<AssetNoteDto> inspectThisAssetNoteDtoList = assetNoteDtoMap.get(assetId);
            for (final AssetNoteDto inspectThisAssetNoteDto : inspectThisAssetNoteDtoList)
            {
                final ItemLightDto itemDto = DataUtils.getItemLight(inspectThisAssetNoteDto.getAssetItem().getId());
                assertEquals(itemDto.getItemType().getId(), ITEM_TYPE_315_ID);
            }
        }
    }

    @Test
    public final void testBatchActionAssetNoteInvalidTypeId()
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.setItemTypeId(INVALID_ITEM_TYPE_ID);
        TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.NOT_FOUND);
    }

    // Story 7.6 AC 5
    @Test
    public final void testSaveBatchActionFailsWithHistoricalImposed() throws MastBusinessException
    {
        final BatchActionAssetNoteDto dto = this.createValidBatchActionAssetNoteDto();
        dto.getAssetNoteDto().getStatus().setId(CodicilStatusType.AI_INACTIVE.getValue());
        dto.getAssetNoteDto().setImposedDate(this.dateUtils.adjustDate(new Date(), MINUS_2, Calendar.DAY_OF_MONTH));
        final String errorMessage = TestUtils.postResponse(BATCH_ACTION_ASSET_NOTE, dto, HttpStatus.BAD_REQUEST)
                .jsonPath().get("message");
        assertTrue(errorMessage.startsWith("Imposed date is in the past:"));
    }

    public static void setAssetNoteMandatoryFields(final AssetNoteDto assetNoteDto)
    {
        assetNoteDto.setCategory(new LinkResource(ONE));
        assetNoteDto.setConfidentialityType(new LinkResource(ONE));
        assetNoteDto.setTitle("This is a test title.");
        assetNoteDto.setImposedDate(DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DATE), FUTURE_IMPOSED));
        assetNoteDto.setStatus(new LinkResource(CodicilStatusType.AN_OPEN.getValue()));

    }

    private List<Long> getAssetIdsFromDtoMap(final Map<Long, List<AssetNoteDto>> actionableItemDtoMap)
    {
        final List<Long> extractedAssetIds = new ArrayList<>();
        for (final Long assetId : actionableItemDtoMap.keySet())
        {
            actionableItemDtoMap.get(assetId).stream()
                    .forEach(localDto -> extractedAssetIds.add(localDto.getAsset().getId()));
        }
        return extractedAssetIds;
    }

    private BatchActionAssetNoteDto createValidBatchActionAssetNoteDto()
    {
        final BatchActionAssetNoteDto dto = new BatchActionAssetNoteDto();
        final AssetNoteDto assetNoteDto = getAssetNoteForWipPost();
        assetNoteDto.setDescription("This is a batch action AssetNote");
        setAssetNoteMandatoryFields(assetNoteDto);
        dto.setAssetNoteDto(assetNoteDto);
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_3_ID, ASSET_2_ID));
        return dto;
    }
}
