package com.baesystems.ai.lr.rest.references;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.references.ReferenceDataMapDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class ReferenceDataRestTest extends BaseRestTest
{
    private static final String REFERENCE_DATA_PATH = urlBuilder(BASE_PATH, "/reference-data");
    private static final String REFERENCE_DATA_VERSION_PATH = urlBuilder(BASE_PATH, "/reference-data-version");

    private ReferenceDataMapDto referenceDataMapDto;
    private ReferenceDataVersionDto referenceDataVersionDto;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        referenceDataVersionDto = DataUtils.getReferenceDataVersion();
    }

    @Test
    public final void testGetReferenceData()
    {
        TestUtils.singleItemFound(REFERENCE_DATA_PATH, referenceDataMapDto, false);
    }

    @Test
    public final void testReferenceDataVersioning()
    {
        TestUtils.singleItemFound(REFERENCE_DATA_VERSION_PATH, referenceDataVersionDto, false);
    }

}
