package com.baesystems.ai.lr.rest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import com.baesystems.ai.lr.utils.Paths;
import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.google.gson.GsonBuilder;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.DecoderConfig;
import com.jayway.restassured.config.EncoderConfig;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.mapper.factory.GsonObjectMapperFactory;

/**
 * Base class for all REST tests allowing for common configuration, such as using a non-default port number.
 */
public abstract class BaseRestTest extends Paths implements IntegrationTest
{
    public static final int PORT_NUMBER = Integer.parseInt(System.getProperty("container.http.port", "9080"));

    public static final long ONE = 1L;
    public static final long FOUR = 4L;
    public static final long TEN = 10L;

    public static final int MAX_ID_LENGTH = 11;
    public static final int MAX_IMO_NUMBER_LENGTH = 7;

    /**
     * This template asset has the following structure:
     * <p>
     * .     10071
     * .        \
     * .        10072
     * .       /     \
     * .     10073   10074
     * .     /   \       \
     * . 10075  10076   10077
     */
    public static final Long TEMPLATE_ASSET_ID = 9999L;

    public BaseRestTest()
    {
        RestAssured.port = PORT_NUMBER;
        RestAssured.config = new RestAssuredConfig();
        RestAssured.config = RestAssured.config.encoderConfig(EncoderConfig.encoderConfig().defaultContentCharset("UTF-8"));
        RestAssured.config = RestAssured.config.decoderConfig(DecoderConfig.decoderConfig().defaultContentCharset("UTF-8"));
        RestAssured.config = RestAssured.config.objectMapperConfig(getGsonObjectMapperConfig());
    }

    private static ObjectMapperConfig getGsonObjectMapperConfig()
    {
        return ObjectMapperConfig.objectMapperConfig().gsonObjectMapperFactory(
                (GsonObjectMapperFactory) (cls, charset) -> new GsonBuilder().registerTypeAdapter(Date.class, new GsonMastDateAdapter()).create());
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        try
        {
            DataUtils.loadTestData();
        }
        catch (ClassNotFoundException | SQLException | IOException | InstantiationException | IllegalAccessException exc)
        {
            Assert.fail(exc.getMessage());
        }
    }
}
