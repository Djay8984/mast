package com.baesystems.ai.lr.rest.cases;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Response;

/**
 * Class to test the REST interface for Cases functionality called rapidly and concurrently.
 *
 * See test/resources/insert_test_data.sql for the data used by these tests.
 */
@Category(IntegrationTest.class)
public class ConcurrentCaseRestTest extends BaseRestTest
{

    private static final Long CASE_ID = 2L;
    private static final String NAME = "New name";
    private static final Long STATUS_FOR_ERROR_CHECKING = 2L;
    private static final Long SURVEYOR_1_ID = 1L;
    private static final Long SURVEYOR_TYPE_1_ID = 1L;
    private static final Long OFFICE_2_ID = 2L;
    private static final Long OFFICE_ROLE_1_ID = 1L;

    @Test
    @Ignore
    public void concurrentUpdate() throws InterruptedException
    {

        // Create many callables that all try to update the same case, on a limited number of threads.
        ExecutorService executorService = null;

        final int threadPoolSize = 5;
        final int numberOfUpdateAttempts = 20;
        final int futureTimeoutSeconds = 5;

        try
        {
            executorService = Executors.newFixedThreadPool(threadPoolSize);

            final Set<Callable<Integer>> callables = new HashSet<Callable<Integer>>();

            for (int index = 0; index < numberOfUpdateAttempts; index++)
            {
                callables.add(new Callable<Integer>()
                {
                    @Override
                    public Integer call() throws Exception
                    {
                        try
                        {
                            return updateConcurrentCase();
                        }
                        catch (final Exception e)
                        {
                            throw e;
                        }
                    }
                });
            }

            // Run them all
            final List<Future<Integer>> allFutures = executorService.invokeAll(callables);
            final Set<Integer> callResults = new HashSet<>();
            for (final Future<Integer> future : allFutures)
            {
                try
                {
                    callResults.add(future.get(futureTimeoutSeconds, TimeUnit.SECONDS));
                }
                catch (final ExecutionException ee)
                {
                    Assert.fail("An unexpected exception occurred on a callable trying to update a case : " + ee.getMessage());
                }
                catch (final TimeoutException e)
                {
                    Assert.fail("An unexpected time out exception occurred on a callable trying to update a case : " + e.getMessage());
                }
            }

            // Confirm at least one of the threads gets a 409 (Conflict) response and a 200 (OK) response
            final boolean conflictFound = callResults.contains(HttpStatus.CONFLICT.value());
            final boolean okFound = callResults.contains(HttpStatus.OK.value());
            assertTrue(
                    "When trying to test whether a hibernate conflict occurs with too many threads, we expect a mix of conflicts and successes. Conflict found : "
                            + conflictFound + ", OK found : " + okFound,
                    conflictFound && okFound);

        }
        finally
        {
            if (executorService != null)
            {
                executorService.shutdown();
            }
        }

    }

    public final int updateConcurrentCase()
    {
        final CaseWithAssetDetailsDto template = getCaseForTest();

        updateSomeFieldsOnTheDto(template);

        final Response putResponse = TestUtils.putResponse(BASE_PATH + "/case/" + template.getId(), template);

        assertNotNull(putResponse);

        final boolean isEitherConflictOrOk = putResponse.getStatusCode() == HttpStatus.CONFLICT.value()
                || putResponse.getStatusCode() == HttpStatus.OK.value();

        assertTrue("Response code is neither conflicted nor ok : " + putResponse.getStatusCode(), isEitherConflictOrOk);

        return putResponse.getStatusCode();
    }

    private void updateSomeFieldsOnTheDto(final CaseDto template)
    {
        // Note: the multiple update lines below make it more likely that the separate execution paths will clash as
        // planned.
        template.setSurveyors(new ArrayList<CaseSurveyorWithLinksDto>());
        template.getSurveyors().add(new CaseSurveyorWithLinksDto());
        template.getSurveyors().get(0).setSurveyor(new LinkResource(SURVEYOR_1_ID));
        template.getSurveyors().get(0).setEmployeeRole(new LinkResource(SURVEYOR_TYPE_1_ID));
        template.setOffices(new ArrayList<OfficeLinkDto>());
        template.getOffices().add(new OfficeLinkDto());
        template.getOffices().get(0).setOffice(new LinkResource(OFFICE_2_ID));
        template.getOffices().get(0).setOfficeRole(new LinkResource(OFFICE_ROLE_1_ID));
        template.getCaseStatus().setId(STATUS_FOR_ERROR_CHECKING);
        template.setCreatedOn(null);
    }

    private CaseWithAssetDetailsDto getCaseForTest()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + CASE_ID, HttpStatus.OK).getBody()
                .as(CaseWithAssetDetailsDto.class);

        return template;
    }
}
