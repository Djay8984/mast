package com.baesystems.ai.lr.utils;

import java.util.Random;

import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.rest.BaseRestTest;

public final class NewObjects extends BaseRestTest
{
    /**
     * Create a new Asset using the default template Asset and also copy over its full asset model.
     * <p>
     * Structure of template Asset Model:
     * <p>
     * .     10071
     * .        \
     * .        10072
     * .       /     \
     * .     10073   10074
     * .     /   \       \
     * . 10075  10076   10077
     *
     * @return {@link AsstLightDto}
     */
    public static AssetLightDto createAssetAndModel()
    {
        return createAssetAndModel(TEMPLATE_ASSET_ID);
    }

    /**
     * Create a new Asset using the given template Asset and also copy over its full asset model.
     *
     * @param templateAssetId
     * @return {@link AsstLightDto}
     */
    public static AssetLightDto createAssetAndModel(final Long templateAssetId)
    {
        final AssetDto newAsset = createAsset(templateAssetId);
        BusinessProcesses.copyFullAssetModel(templateAssetId, newAsset);
        return TestUtils.getResponse(String.format(SPECIFIC_ASSET, newAsset.getId()), HttpStatus.OK).as(AssetLightDto.class);
    }

    /**
     * Create a new Asset using the default template Asset.
     *
     * @return {@link AssetDto} new Asset
     */
    public static AssetDto createAsset()
    {
        return createAsset(TEMPLATE_ASSET_ID);
    }

    /**
     * Create a new Asset using the given template Asset.
     *
     * @param templateAssetId
     * @return {@link AssetDto} new Asset
     */
    public static AssetDto createAsset(final Long templateAssetId)
    {
        final AssetLightDto existingAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, templateAssetId), HttpStatus.OK)
                .as(AssetLightDto.class);

        existingAsset.setId(null);
        existingAsset.setBuilder(existingAsset.getBuilder().concat(Integer.toString(new Random().nextInt())));
        existingAsset.setIhsAsset(null);

        CollectionUtils.nullSafeStream(existingAsset.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });

        CollectionUtils.nullSafeStream(existingAsset.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));

        return TestUtils.postResponse(BASE_ASSET, existingAsset, HttpStatus.OK).as(AssetDto.class);
    }
}
