package com.baesystems.ai.lr.rest.jobs;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.FollowUpActionPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class FollowUpActionRestTest extends BaseRestTest
{
    public static final String LIST_FUA = new StringBuffer().append(BASE_PATH).append("/job/%d/follow-up-action")
            .toString();
    public static final String UPDATE_FUA = new StringBuffer().append(BASE_PATH).append("/job/%d/follow-up-action/%d")
            .toString();

    private static final Long FUA_1_ID = 1L;
    private static final Long FUA_2_ID = 2L;
    private static final Long JOB_1_ID = 1L;
    private static final Long FUA_STATUS_2_ID = 2L;

    private FollowUpActionPage followUpActionPage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
    }

    private void initialiseFUAPage(final Long... ids)
    {
        followUpActionPage = new FollowUpActionPage();

        FollowUpActionDto tempFUA;
        for (final Long id : ids)
        {
            try
            {
                tempFUA = DataUtils.getFUA(id);
                followUpActionPage.getContent().add(tempFUA);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/job results in a positive response. Expecting job 1.
     */
    @Test
    public void getAllFollowUpActionsForJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseFUAPage(FUA_1_ID);
        final FollowUpActionPage result = TestUtils.getResponse(format(LIST_FUA, JOB_1_ID), HttpStatus.OK).jsonPath().getObject("",
                FollowUpActionPage.class);
        TestUtils.compareFields(result, followUpActionPage, true);
    }

    @Test
    public void testUpdateFollowUpActionStatus() throws DatabaseUnitException
    {
        final FollowUpActionPage page = TestUtils.getResponse(format(LIST_FUA, JOB_1_ID), HttpStatus.OK).as(FollowUpActionPage.class);

        final FollowUpActionDto fua2 = page.getContent()
                .stream()
                .filter(followUpAction -> followUpAction.getId() == FUA_2_ID)
                .findFirst()
                .get();

        fua2.setFollowUpActionStatus(new LinkResource());
        fua2.getFollowUpActionStatus().setId(FUA_STATUS_2_ID);

        final FollowUpActionDto updatedFUA = TestUtils.putResponse(format(UPDATE_FUA, JOB_1_ID, FUA_2_ID), fua2, HttpStatus.OK)
                .as(FollowUpActionDto.class);

        assertEquals(
                "Status of returned FollowUpAction " + updatedFUA.getFollowUpActionStatus().getId()
                        + " does not match expected: " + FUA_STATUS_2_ID,
                FUA_STATUS_2_ID, updatedFUA.getFollowUpActionStatus().getId());
    }

    @Test
    public void updateFollowUpActionFailsWithBadStalenessKey()
    {
        final FollowUpActionPage page = TestUtils.getResponse(format(LIST_FUA, JOB_1_ID), HttpStatus.OK).as(FollowUpActionPage.class);

        final FollowUpActionDto originalActionDto = page.getContent().get(0);

        originalActionDto.setStalenessHash("Invalid Hash");

        final ErrorMessageDto error = TestUtils
                .putResponse(format(UPDATE_FUA, JOB_1_ID, originalActionDto.getId()), originalActionDto, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.FOLLOW_UP_ACTION.getTypeString()),
                error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }
}
