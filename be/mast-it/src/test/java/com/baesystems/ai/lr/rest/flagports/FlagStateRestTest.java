package com.baesystems.ai.lr.rest.flagports;

import static java.lang.String.format;

import java.util.ArrayList;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.pages.FlagStatePage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

/**
 * Class to test the REST interface for the Flag State functionality. See test/resources/insert_test_data.sql for the
 * data used by these tests.
 */
@Category(IntegrationTest.class)
public class FlagStateRestTest extends BaseRestTest
{
    private static final String BASE_FLAG_STATE_PATH = new StringBuffer(BASE_PATH).append("/flag-state").toString();
    private static final String SINGLE_FLAG_STATE_PATH = new StringBuffer(BASE_FLAG_STATE_PATH).append("/%d").toString();
    private static final String FLAG_STATE_SEARCH_PATH = new StringBuffer(BASE_FLAG_STATE_PATH).append("?search=%s").toString();

    private static final Long FLAG_STATE_ALGERIA_ID = 2L;
    private static final Long FLAG_STATE_BRAZIL_ID = 23L;
    private static final Long FLAG_STATE_SPAIN_ID = 174L;

    private static final Long FLAG_STATE_BAHRAIN_ID = 13L;
    private static final Long FLAG_STATE_UKRAINE_ID = 194L;

    private static final Long PORT_OF_REGISTRY_3_ID = 3L;
    private static final Long PORT_OF_REGISTRY_169_ID = 169L;
    private static final Long PORT_OF_REGISTRY_573_ID = 573L;
    private static final Long PORT_OF_REGISTRY_781_ID = 781L;
    private static final Long PORT_OF_REGISTRY_1305_ID = 1305L;
    private static final Long PORT_OF_REGISTRY_4614_ID = 4614L;
    private static final Long PORT_OF_REGISTRY_5743_ID = 5743L;

    private static final Long INVALID_FLAG_STATE_ID = 999L;

    private FlagStateDto flagState;
    private FlagStatePage flagStatePage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.flagState = DataUtils.getFlagState(FLAG_STATE_ALGERIA_ID);
        this.flagState.setPorts(new ArrayList<LinkResource>());
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_3_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_169_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_573_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_781_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_1305_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_4614_ID));
        this.flagState.getPorts().add(new LinkResource(PORT_OF_REGISTRY_5743_ID));

        initialiseFlagStatePage(FLAG_STATE_ALGERIA_ID);
    }

    private void initialiseFlagStatePage(final Long... ids)
    {
        this.flagStatePage = new FlagStatePage();

        FlagStateDto tempAsset;
        for (final Long id : ids)
        {
            tempAsset = new FlagStateDto();
            tempAsset.setId(id);
            this.flagStatePage.getContent().add(tempAsset);
        }
    }

    @Test
    public final void getFlagState()
    {
        TestUtils.singleItemFound(format(SINGLE_FLAG_STATE_PATH, FLAG_STATE_ALGERIA_ID.longValue()), this.flagState, true);
    }

    @Test
    public final void getInvalidFlagState()
    {
        TestUtils.singleItemNotFound(format(SINGLE_FLAG_STATE_PATH, INVALID_FLAG_STATE_ID.longValue()));
    }

    @Test
    public final void getFlagStates()
    {
        initialiseFlagStatePage(FLAG_STATE_ALGERIA_ID, FLAG_STATE_BRAZIL_ID, FLAG_STATE_SPAIN_ID);
        TestUtils.listItemsFound(BASE_FLAG_STATE_PATH, this.flagStatePage, true, false);
    }

    @Test
    public final void searchExactMatchFound()
    {
        initialiseFlagStatePage(FLAG_STATE_BRAZIL_ID);
        TestUtils.listItemsFound(format(FLAG_STATE_SEARCH_PATH, "Brazil"), this.flagStatePage, true, false);
    }

    @Test
    public final void searchExactMatchNotFound()
    {
        TestUtils.listItemsNotFound(format(FLAG_STATE_SEARCH_PATH, "Tibet"));
    }

    @Test
    public final void searchFuzzyMatchFound()
    {
        initialiseFlagStatePage(FLAG_STATE_BAHRAIN_ID, FLAG_STATE_UKRAINE_ID);
        TestUtils.listItemsFound(format(FLAG_STATE_SEARCH_PATH, "*rai*"), this.flagStatePage, true, false);
    }

    @Test
    public final void searchFuzzyMatchNotFound()
    {
        TestUtils.listItemsNotFound(format(FLAG_STATE_SEARCH_PATH, "*qq*"));
    }

    @Test
    public final void searchWildcardLeftFound()
    {
        initialiseFlagStatePage(FLAG_STATE_SPAIN_ID);
        TestUtils.listItemsFound(format(FLAG_STATE_SEARCH_PATH, "*ain"), this.flagStatePage, true, false);
    }

    @Test
    public final void searchWildcardLeftNotFound()
    {
        TestUtils.listItemsNotFound(format(FLAG_STATE_SEARCH_PATH, "*gg"));
    }

    @Test
    public final void searchWildcardRightFound()
    {
        initialiseFlagStatePage(FLAG_STATE_SPAIN_ID);
        TestUtils.listItemsFound(format(FLAG_STATE_SEARCH_PATH, "Spa*"), this.flagStatePage, true, false);
    }

    @Test
    public final void searchWildcardRightNotFound()
    {
        TestUtils.listItemsNotFound(format(FLAG_STATE_SEARCH_PATH, "En*"));
    }
}
