package com.baesystems.ai.lr.single_use;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ItemRelationshipRulesTest
{
    private Map<Long, Asset> allAssets;
    private Map<Long, Item> allItems;
    private List<ItemRelationship> allItemRelationships;
    private List<ItemTypeRelationship> allItemTypeRelationships;

    @Before
    public void setUp() throws DatabaseUnitException
    {
        try
        {
            SingleUseDataUtils.loadTestData();
        }
        catch (ClassNotFoundException | SQLException | InstantiationException | IOException | IllegalAccessException e)
        {
            e.printStackTrace();
        }

        allAssets = SingleUseDataUtils.getAllAssets();
        allItems = SingleUseDataUtils.getAllItems();
        allItemRelationships = SingleUseDataUtils.getAllItemRelationships();
        allItemTypeRelationships = SingleUseDataUtils.getAllItemTypeRelationships();
    }

    @Test
    public void testAssetModelIntegrityAgainstRelationshipRules() throws DatabaseUnitException
    {
        System.out.println("Asset, Item-From, Item-To, Relationship-Type, Asset-Category, Item-From-Type, Item-To-Type");
        boolean error = false;
        for (final ItemRelationship itemRelationship : allItemRelationships)
        {
            final Item fromItem = allItems.get(itemRelationship.getFromId());
            final Item toItem = allItems.get(itemRelationship.getToId());
            final Asset asset = allAssets.get(fromItem.getAssetId());

            final Long fromItemType = fromItem.getItemTypeId();
            final Long toItemType = toItem.getItemTypeId();
            final Long category = asset.getCategoryId();
            final Long relationshipType = itemRelationship.getRelationshipTypeId();

            final ItemTypeRelationship target = new ItemTypeRelationship();
            target.setFromTypeId(fromItemType);
            target.setToTypeId(toItemType);
            target.setAssetCategoryId(category);
            target.setRelationshipTypeId(relationshipType);

            if (!allItemTypeRelationships.contains(target))
            {
                error = true;
                System.out.printf(String.format("%d, %d, %d, %d, %d, %d, %d \n",
                        asset.getId(), fromItem.getId(), toItem.getId(), relationshipType, category, fromItemType, toItemType));
            }
        }
        Assert.assertFalse(error);
    }

    /**
     * Internal classes used as some of the corresponding dtos did not have required properties for this test Didn't
     * want do change application objects for the sake of a test eg ItemRelationshipDto does not have fromId;
     */
    static class Asset
    {
        private Long id;
        private Long categoryId;

        public Long getId()
        {
            return id;
        }

        public void setId(final Long id)
        {
            this.id = id;
        }

        public Long getCategoryId()
        {
            return categoryId;
        }

        public void setCategoryId(final Long categoryId)
        {
            this.categoryId = categoryId;
        }
    }

    static class Item
    {
        private Long id;
        private Long itemTypeId;
        private Long assetId;

        public Long getId()
        {
            return id;
        }

        public void setId(final Long id)
        {
            this.id = id;
        }

        public Long getItemTypeId()
        {
            return itemTypeId;
        }

        public void setItemTypeId(final Long itemTypeId)
        {
            this.itemTypeId = itemTypeId;
        }

        public Long getAssetId()
        {
            return assetId;
        }

        public void setAssetId(final Long assetId)
        {
            this.assetId = assetId;
        }
    }

    static class ItemRelationship
    {
        private Long fromId;
        private Long toId;
        private Long relationshipTypeId;

        public Long getFromId()
        {
            return fromId;
        }

        public void setFromId(final Long fromId)
        {
            this.fromId = fromId;
        }

        public Long getToId()
        {
            return toId;
        }

        public void setToId(final Long toId)
        {
            this.toId = toId;
        }

        public Long getRelationshipTypeId()
        {
            return relationshipTypeId;
        }

        public void setRelationshipTypeId(final Long relationshipTypeId)
        {
            this.relationshipTypeId = relationshipTypeId;
        }

    }

    static class ItemTypeRelationship
    {
        private Long assetCategoryId;
        private Long fromTypeId;
        private Long toTypeId;
        private Long relationshipTypeId;

        private static final int HASHCODE_BASE = 31;

        public Long getAssetCategoryId()
        {
            return assetCategoryId;
        }

        public void setAssetCategoryId(final Long assetCategoryId)
        {
            this.assetCategoryId = assetCategoryId;
        }

        public Long getFromTypeId()
        {
            return fromTypeId;
        }

        public void setFromTypeId(final Long fromTypeId)
        {
            this.fromTypeId = fromTypeId;
        }

        public Long getToTypeId()
        {
            return toTypeId;
        }

        public void setToTypeId(final Long toTypeId)
        {
            this.toTypeId = toTypeId;
        }

        public Long getRelationshipTypeId()
        {
            return relationshipTypeId;
        }

        public void setRelationshipTypeId(final Long relationshipTypeId)
        {
            this.relationshipTypeId = relationshipTypeId;
        }

        @Override
        public boolean equals(final Object o)
        {
            boolean result;
            if (this == o)
            {
                result = true;
            }
            else if (o == null || getClass() != o.getClass())
            {
                result = false;
            }
            else
            {
                final ItemTypeRelationship that = (ItemTypeRelationship) o;
                result = Objects.equals(assetCategoryId, that.assetCategoryId)
                        && Objects.equals(fromTypeId, that.fromTypeId)
                        && Objects.equals(toTypeId, that.toTypeId)
                        && Objects.equals(relationshipTypeId, that.relationshipTypeId);
            }
            return result;
        }

        @Override
        public int hashCode()
        {
            int result = assetCategoryId.hashCode();
            result = HASHCODE_BASE * result + fromTypeId.hashCode();
            result = HASHCODE_BASE * result + toTypeId.hashCode();
            result = HASHCODE_BASE * result + relationshipTypeId.hashCode();
            return result;
        }
    }
}
