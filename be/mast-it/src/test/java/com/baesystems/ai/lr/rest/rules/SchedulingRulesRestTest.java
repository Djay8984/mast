package com.baesystems.ai.lr.rest.rules;

import static java.lang.String.format;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.scheduling.SREUncertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.BusinessProcesses;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class SchedulingRulesRestTest extends BaseRestTest
{
    @Autowired
    private DateHelper dateHelper;

    public static final String ASSET_PATH = urlBuilder(BASE_PATH, "/asset/%d");
    public static final String SERVICE_PATH = urlBuilder(ASSET_PATH, "/service");
    public static final String SPECIFIC_SERVICE_PATH = urlBuilder(SERVICE_PATH, "/%d");
    public static final String MOCK_DROOLS_PATH = urlBuilder("mock-drools/scheduling/");

    private static final Long ASSET_1_ID = 1L;

    // These IDs are those for Asset 1, not contained in the Mock.
    private static final Long SERVICE_101 = 101L;
    private static final Long SERVICE_103 = 103L;
    private Date harmonisationDate;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        this.harmonisationDate = new Date();
    }

    /**
     * Test that the mock rules engine responds. This doesn't really test anything not already covered, but should make
     * it easier to debug the other tests if they fail due to the mock not responding.
     */
    @Test
    public final void testMockDroolsResponse()
    {
        final StringBuilder url = new StringBuilder();
        url.append(MOCK_DROOLS_PATH);
        url.append("manualEntryService?creditedServiceId=");
        url.append(ASSET_1_ID);
        url.append("&assetHarmonisationDate=");
        url.append(DateHelper.mastFormatter().format(this.harmonisationDate).replaceAll(" ", "%"));

        assertNotNull(TestUtils.getResponse(url.toString(), HttpStatus.OK).getBody());
    }

    /**
     * Ultimately we don't care what 'data' comes back from the Scheduling Rules Engine - for development, and therefore
     * for these tests, we are hitting a mock that will always return the same thing. However, we should and can check
     * that if we update the harmonisation date on an asset, then the mock is hit, and as a result, the due dates on the
     * services which are already linked to the asset (and match those hard coded service catalogue IDs on the mock)
     * should have changed.
     */
    @Test
    public final void updateHarmonisationDateTriggersRules()
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        // Get a list of the existing Scheduled Services associated with this Asset.
        final ScheduledServiceDto[] scheduledServices = TestUtils.getResponse(format(SERVICE_PATH, ASSET_1_ID), HttpStatus.OK).getBody()
                .as(ScheduledServiceDto[].class);

        final StringBuilder url = new StringBuilder();
        url.append(MOCK_DROOLS_PATH);
        url.append("manualEntryService?creditedServiceId=");
        url.append(ASSET_1_ID);
        url.append("&assetHarmonisationDate=");
        url.append(DateHelper.mastFormatter().format(this.harmonisationDate).replaceAll(" ", "%"));

        final SREUncertifiedSuccessResponseDto droolsResponce = TestUtils.getResponse(url.toString(), HttpStatus.OK).getBody()
                .as(SREUncertifiedSuccessResponseDto.class);
        final Long updatedServiceCatalogueId = droolsResponce.getService().getServiceCatalogueIdentifier().longValue();

        // Update the Harmonisation Date to 'now', which will always be different
        // to the Harmonisation Date currently on the Asset, and as such will trigger
        // a call to the Scheduling Rules Engine to get new Service dates.
        final AssetLightDto assetLightDto = TestUtils
                .getResponse(format(ASSET_PATH, ASSET_1_ID).concat("?draft=true"), HttpStatus.OK, AssetLightDto.class);
        assetLightDto.setHarmonisationDate(this.harmonisationDate);
        TestUtils.putResponse(format(ASSET_PATH, ASSET_1_ID), assetLightDto, HttpStatus.OK);

        try
        {
            // Get the list of Scheduled Services again to check that their schedule was changed.
            final ScheduledServiceDto[] rescheduledServices = TestUtils.getResponse(format(SERVICE_PATH, ASSET_1_ID),
                    HttpStatus.OK).getBody()
                    .as(ScheduledServiceDto[].class);

            for (int i = 0; i < scheduledServices.length; i++)
            {
                // Check that the schedule has changed
                if (updatedServiceCatalogueId.equals(scheduledServices[i].getServiceCatalogueId())
                        && (updatedServiceCatalogueId.equals(SERVICE_101)
                        || updatedServiceCatalogueId.equals(SERVICE_103)))
                {
                    assertFalse(scheduledServices[i].getDueDate().getTime() == rescheduledServices[i].getDueDate().getTime());
                }
            }
        }
        finally
        {
            // Update the services to their state before harmonisation change so that this test can be run multiple
            // times.
            for (final ScheduledServiceDto scheduledService : scheduledServices)
            {
                TestUtils.putResponse(format(SPECIFIC_SERVICE_PATH, ASSET_1_ID, scheduledService.getId()), scheduledService, HttpStatus.OK);
            }
            BusinessProcesses.checkAssetIn(ASSET_1_ID);

        }
    }
}
