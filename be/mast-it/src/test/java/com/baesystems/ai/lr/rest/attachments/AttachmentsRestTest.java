package com.baesystems.ai.lr.rest.attachments;

import static java.lang.String.format;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.pages.SupplementaryInformationPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class AttachmentsRestTest extends BaseRestTest
{
    private static final String BASE_ASSET_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/attachment");
    public static final String BASE_JOB_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/attachment");
    private static final String BASE_MILESTONE_ATTACHMENT = urlBuilder(BASE_PATH,
            "/case/%d/milestone/%d/attachment");
    private static final String BASE_ITEM_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/item/%d/attachment");
    private static final String BASE_SERVICE_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/service/%d/attachment");
    private static final String BASE_ACTIONABLE_ITEM_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/actionable-item/%d/attachment");
    private static final String BASE_WIP_ACTIONABLE_ITEM_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-actionable-item/%d/attachment");
    private static final String BASE_ASSET_NOTE_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/asset-note/%d/attachment");
    private static final String BASE_COC_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/coc/%d/attachment");
    private static final String BASE_WIP_COC_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-coc/%d/attachment");
    private static final String BASE_WIP_ASSET_NOTE_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-asset-note/%d/attachment");
    private static final String BASE_DEFECT_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/defect/%d/attachment");
    private static final String BASE_WIP_DEFECT_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-defect/%d/attachment");
    // private static final String BASE_REPORT_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/report/%d/attachment");
    private static final String BASE_REPAIR_COC_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/coc/%d/repair/%d/attachment");
    private static final String BASE_REPAIR_DEFECT_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/defect/%d/repair/%d/attachment");
    private static final String BASE_WIP_REPAIR_COC_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-coc/%d/wip-repair/%d/attachment");
    private static final String BASE_WIP_REPAIR_DEFECT_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-defect/%d/wip-repair/%d/attachment");
    private static final String BASE_TASK_ATTACHMENT = urlBuilder(BASE_PATH, "/asset/%d/task/%d/attachment");
    private static final String BASE_WIP_TASK_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/wip-task/%d/attachment");
    private static final String BASE_SURVEY_ATTACHMENT = urlBuilder(BASE_PATH, "/job/%d/survey/%d/attachment");

    private static final String SINGLE_ATTACHMENT_SUFFIX = "/%d";

    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final Long FOUR = 4L;
    private static final Long ASSET_NOTE_1_ID = 6L;
    private static final Long ACTIONABLE_ITEM_1_ID = 4L;
    private static final Long WIP_ACTIONABLE_ITEM_1 = 21L;
    private static final Long WIP_COC_1_ID = 20L;
    private static final Long WIP_ASSET_NOTE_1_ID = 8L;
    private static final Long DEFECT_ID = 1L;
    private static final Long WIP_DEFECT_ID = 1L;
    // private static final Long REPORT_1_ID = 1L;

    private static final Long TASK_1 = 1L;
    private static final Long WIP_TASK_1 = 1L;
    private static final Long SURVEY_1 = 1L;

    private static final Long ATTACHMENT_1 = 1L;
    private static final Long ATTACHMENT_2 = 2L;
    private static final Long ATTACHMENT_3 = 3L;
    private static final Long ATTACHMENT_4 = 4L;
    private static final Long ATTACHMENT_5 = 5L;
    private static final Long ATTACHMENT_6 = 6L;
    private static final Long ATTACHMENT_7 = 7L;
    private static final Long ATTACHMENT_8 = 8L;
    private static final Long ATTACHMENT_9 = 9L;
    private static final Long ATTACHMENT_10 = 10L;
    private static final Long ATTACHMENT_11 = 11L;
    private static final Long ATTACHMENT_12 = 12L;
    private static final Long ATTACHMENT_13 = 13L;
    // private static final Long ATTACHMENT_14 = 14L;
    private static final Long ATTACHMENT_15 = 15L;
    private static final Long ATTACHMENT_16 = 16L;
    private static final Long ATTACHMENT_17 = 17L;
    private static final Long ATTACHMENT_18 = 18L;
    private static final Long ATTACHMENT_19 = 19L;
    private static final Long ATTACHMENT_20 = 20L;
    private static final Long ATTACHMENT_21 = 21L;

    private SupplementaryInformationDto baseAttachment;

    @DataPoints
    public static final PathAndResult[] PATH_AND_RESULTS;

    static
    {
        final List<PathAndResult> pathAndResultsList = new ArrayList<>();

        pathAndResultsList.add(new PathAndResult(String.format(BASE_ASSET_ATTACHMENT, ONE), new Long[]{ATTACHMENT_1}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_JOB_ATTACHMENT, ONE), new Long[]{ATTACHMENT_2}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_MILESTONE_ATTACHMENT, ONE, ONE), new Long[]{ATTACHMENT_3}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_ITEM_ATTACHMENT, TWO, ONE), new Long[]{ATTACHMENT_4}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_SERVICE_ATTACHMENT, THREE, FOUR), new Long[]{ATTACHMENT_5}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_ASSET_NOTE_ATTACHMENT, ONE, ASSET_NOTE_1_ID, ONE), new Long[]{ATTACHMENT_6}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_ASSET_NOTE_ATTACHMENT, ONE, WIP_ASSET_NOTE_1_ID), new Long[]{ATTACHMENT_7}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_COC_ATTACHMENT, ONE, ONE), new Long[]{ATTACHMENT_8}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_COC_ATTACHMENT, ONE, WIP_COC_1_ID), new Long[]{ATTACHMENT_9}));
        pathAndResultsList
                .add(new PathAndResult(String.format(BASE_ACTIONABLE_ITEM_ATTACHMENT, TWO, ACTIONABLE_ITEM_1_ID), new Long[]{ATTACHMENT_10}));
        pathAndResultsList
                .add(new PathAndResult(String.format(BASE_WIP_ACTIONABLE_ITEM_ATTACHMENT, ONE, WIP_ACTIONABLE_ITEM_1), new Long[]{ATTACHMENT_11}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_DEFECT_ATTACHMENT, ONE, DEFECT_ID), new Long[]{ATTACHMENT_12}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_DEFECT_ATTACHMENT, ONE, WIP_DEFECT_ID), new Long[]{ATTACHMENT_13}));
        // pathAndResultsList.add(new PathAndResult(String.format(BASE_REPORT_ATTACHMENT, ONE, REPORT_1_ID), new
        // Long[]{ATTACHMENT_14}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_TASK_ATTACHMENT, ONE, TASK_1), new Long[]{ATTACHMENT_15}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_TASK_ATTACHMENT, ONE, WIP_TASK_1), new Long[]{ATTACHMENT_16}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_SURVEY_ATTACHMENT, ONE, SURVEY_1), new Long[]{ATTACHMENT_17}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_REPAIR_DEFECT_ATTACHMENT, THREE, TWO, ONE), new Long[]{ATTACHMENT_18}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_REPAIR_COC_ATTACHMENT, TWO, TWO, ONE), new Long[]{ATTACHMENT_19}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_REPAIR_DEFECT_ATTACHMENT, ONE, ONE, THREE), new Long[]{ATTACHMENT_20}));
        pathAndResultsList.add(new PathAndResult(String.format(BASE_WIP_REPAIR_COC_ATTACHMENT, ONE, ONE, FOUR), new Long[]{ATTACHMENT_21}));

        PATH_AND_RESULTS = pathAndResultsList.toArray(new PathAndResult[pathAndResultsList.size()]);
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        baseAttachment = DataUtils.getAttachment(ATTACHMENT_1);
    }

    @Theory
    public final void testGetAttachments(final PathAndResult pathAndResult)
    {
        final SupplementaryInformationPage supplementaryInformationPage = TestUtils.getResponse(pathAndResult.getPath(), HttpStatus.OK).getBody()
                .as(SupplementaryInformationPage.class);
        boolean found = false;
        for (final SupplementaryInformationDto attachment : supplementaryInformationPage.getContent())
        {
            if (attachment.getId().equals(pathAndResult.getResult()[0])) // urgh
            {
                found = true;
            }
        }
        assertTrue(found);
    }

    @Theory
    @Ignore //
    public final void testPostAndUpdateAndDeleteAttachment(final PathAndResult pathAndResult)
    {
        // POST
        baseAttachment.setId(null);
        final SupplementaryInformationDto newAttachmentDto = TestUtils.postResponse(pathAndResult.getPath(), baseAttachment, HttpStatus.OK,
                SupplementaryInformationDto.class);

        final SupplementaryInformationPage attachments = TestUtils.getResponse(pathAndResult.getPath(), HttpStatus.OK)
                .as(SupplementaryInformationPage.class);
        assertTrue(attachments.getContent().contains(newAttachmentDto));

        // UPDATE
        final String singlePath = String.format(pathAndResult.getPath().concat(SINGLE_ATTACHMENT_SUFFIX), newAttachmentDto.getId());

        final String newUrl = "www.testPutURL.com";
        newAttachmentDto.setAttachmentUrl(newUrl);

        final SupplementaryInformationDto result = TestUtils.putResponse(singlePath, newAttachmentDto, HttpStatus.OK,
                SupplementaryInformationDto.class);

        Assert.assertEquals(newUrl, result.getAttachmentUrl());

        // DELETE
        final Boolean response2 = TestUtils.deleteResponse(singlePath, HttpStatus.OK).body().as(Boolean.class);
        assertTrue(response2.booleanValue());

        // DELETE again and check for 404 response
        TestUtils.deleteResponse(singlePath, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void confirmBadPaginationQueryDataReturnsAnAppropriateError()
    {
        TestUtils.getResponse(format(BASE_ASSET_ATTACHMENT + "?page=test&size=test&sort=test", ONE), HttpStatus.BAD_REQUEST);
    }

    static class PathAndResult
    {
        public PathAndResult(final String path, final Long[] result)
        {
            this.result = result;
            this.path = path;
        }

        private Long[] result;
        private String path;

        public Long[] getResult()
        {
            return result;
        }

        public void setResult(final Long[] result)
        {
            this.result = result;
        }

        public String getPath()
        {
            return path;
        }

        public void setPath(final String path)
        {
            this.path = path;
        }
    }
}
