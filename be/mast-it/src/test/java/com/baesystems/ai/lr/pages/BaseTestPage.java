package com.baesystems.ai.lr.pages;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.dto.paging.PaginationDto;

public class BaseTestPage<T>
{
    private PaginationDto pagination;

    private List<T> content;
    
    public BaseTestPage()
    {
        this.pagination = new PaginationDto();
        this.content = new ArrayList<T>();
    }

    public PaginationDto getPagination()
    {
        return pagination;
    }

    public void setPagination(final PaginationDto pagination)
    {
        this.pagination = pagination;
    }

    public List<T> getContent()
    {
        return content;
    }

    public void setContent(final List<T> content)
    {
        this.content = content;
    }
}
