package com.baesystems.ai.lr.rest.ihs;

import static java.lang.String.format;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class IhsAssetRestTest extends BaseRestTest
{
    /**
     * API Rest paths
     */
    public static final String BASE_IHS = new StringBuffer().append(BASE_PATH).append("/ihs").toString();

    public static final String IHS_ASSET = new StringBuffer().append(BASE_IHS).append("/%d").toString();


    public static final String IHS_EQUIPMENT = new StringBuffer().append(BASE_IHS).append("/equipment/%d").toString();

    public static final String IHS_PRINCIPAL_DIMENSIONS = new StringBuffer().append(BASE_IHS).append("/dimensions/%d").toString();

    /**
     * Test data
     */
    private static final Long INVALID_ID = 9999L;
    private static final Long NEGATIVE = -1L;
    private static final Long ZERO = 0L;
    private static final Long IMO_NUMBER = 1000019L;

    // Needs to be public for Theory to work.
    @DataPoints
    public static final String[] BAD_REQUESTS = {String.format(IHS_ASSET, ZERO.longValue()),
                                                 BASE_IHS + "/SomeLetters",
                                                 String.format(IHS_ASSET, NEGATIVE.longValue())};

    private IhsAssetDetailsDto ihsAssetDetails;

    private IhsEquipmentDetailsDto ihsEquipmentDetailsDto;

    private IhsPrincipalDimensionsDto ihsPrincipalDimensionsDto;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.ihsAssetDetails = DataUtils.getIhsAsset(IMO_NUMBER);

        this.ihsEquipmentDetailsDto = DataUtils.getEquipmentDetails(IMO_NUMBER);

        this.ihsPrincipalDimensionsDto = DataUtils.getPrincipalDimensions(IMO_NUMBER);
    }

    /**
     * Test that a valid GET request for ../ihs/{ihs_asset_id} results in a positive response. Expecting JSON containing
     * asset.
     */
    @Test
    public final void ihsAssetUriAssetFound()
    {
        TestUtils.singleItemFound(format(IHS_ASSET, IMO_NUMBER.longValue()), this.ihsAssetDetails, true);
    }

    /**
     * Test that a valid GET request for ../ihs/{ihs_asset_id} results in a positive response. Expecting error for
     * record not found.
     */
    @Test
    public final void ihsAssetUriAssetNotFound()
    {
        TestUtils.singleItemNotFound(format(IHS_ASSET, INVALID_ID.longValue()));
    }

    /**
     * Test that a valid GET request for ../ihs/{ihs_asset_id} and ../ihs-asset-type/{ihs_asset_id} results in a
     * positive response. Expecting an error for a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ihsAssetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    /**
     * Test that a valid GET request for ../ihs/equipment/{ihs_asset_id} results in a positive response. Expecting JSON containing
     * equipment info.
     */
    @Test
    public final void ihsEquipmentUriFound()
    {
        TestUtils.singleItemFound(format(IHS_EQUIPMENT, IMO_NUMBER.longValue()), this.ihsEquipmentDetailsDto, true);
    }

    /**
     * Test that a valid GET request for ../ihs/equipment/{ihs_asset_id} results in a positive response. Expecting error for
     * record not found.
     */
    @Test
    public final void ihsEquipmentUriNotFound()
    {
        TestUtils.singleItemNotFound(format(IHS_EQUIPMENT, INVALID_ID.longValue()));
    }

    /**
     * Expecting an error for a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ihsEquipmentUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    /**
     * Test that a valid GET request for ../ihs/dimensions/{ihs_asset_id} results in a positive response. Expecting JSON containing
     * Principal Dimensions info.
     */
    @Test
    public final void ihsPrincipalDimensionsUriFound()
    {
        TestUtils.singleItemFound(format(IHS_PRINCIPAL_DIMENSIONS, IMO_NUMBER.longValue()), this.ihsPrincipalDimensionsDto, true);
    }

    /**
     * Test that a valid GET request for ../ihs/dimensions/{ihs_asset_id} results in a positive response. Expecting error for
     * record not found.
     */
    @Test
    public final void ihsPrincipalDimensionsUriNotFound()
    {
        TestUtils.singleItemNotFound(format(IHS_PRINCIPAL_DIMENSIONS, INVALID_ID.longValue()));
    }

    /**
     * Expecting an error for a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ihsPrincipalDimensionsUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }
}
