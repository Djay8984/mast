package com.baesystems.ai.lr.rest.cases;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.dbunit.DatabaseUnitException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CaseMilestoneStatusType;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.EmployeeRoleType;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Response;

@Category(IntegrationTest.class)
public class CaseMilestoneRestTest extends BaseRestTest
{

    // These set of constants have been created to prevent Checkstyle complaining about Magic Numbers, despite not
    // needing to be shared.
    private static final long SECOND_SURVEYOR_ID_TO_ALLOW_CASE_CREATION = 20L;
    private static final long FIRST_SURVEYOR_ID_TO_ALLOW_CASE_CREATION = 2L;
    private static final long OFFICE_ROLE_TO_ALLOW_CASE_CREATION = 14L;
    private static final long OFFICE_TYPE_TO_ALLOW_CASE_CREATION = 3L;
    private static final long CASE_TYPE_TO_ALLOW_CREATION = 3L;
    private static final long CASE_STATUS_TO_ALLOW_CREATION = 2L;
    private static final int MAXIMUM_LENGTH_OF_YARD_NUMBER = 14;
    private static final int EXISTING_ASSET_ID_TO_COPY = 8;
    /**
     * API Rest Paths
     */
    public static final String BASE_CASE = new StringBuffer().append(BASE_PATH).append("/case/%d").toString();
    public static final String BASE_CASE_MILESTONES = new StringBuffer().append(BASE_PATH).append("/case/%d/milestone").toString();

    public static final String BASE_ASSET = new StringBuffer().append(BASE_PATH).append("/asset").toString();
    public static final String ASSET = new StringBuffer().append(BASE_ASSET).append("/%d").toString();

    public static final Long CASE_1_ID = 1L;
    public static final Long CASE_2_ID = 2L;
    public static final Long INVALID_CASE_ID = 999L;
    public static final Long CASE_MILESTONE_101_ID = 101L;
    public static final Long CASE_MILESTONE_102_ID = 102L;
    public static final Long CASE_MILESTONE_103_ID = 103L;
    public static final Long CASE_MILESTONE_104_ID = 104L;
    public static final Long CASE_MILESTONE_105_ID = 105L;
    public static final Long CASE_MILESTONE_106_ID = 106L;
    public static final Long CASE_MILESTONE_107_ID = 107L;
    public static final Long CASE_MILESTONE_109_ID = 109L;
    public static final Long CASE_MILESTONE_110_ID = 110L;
    public static final Long CASE_MILESTONE_114_ID = 114L;
    public static final Long CASE_MILESTONE_118_ID = 118L;
    public static final Long CASE_MILESTONE_121_ID = 121L;
    public static final Long CASE_MILESTONE_124_ID = 124L;
    public static final Long CASE_MILESTONE_125_ID = 125L;
    public static final Long CASE_MILESTONE_132_ID = 132L;
    public static final Long CASE_MILESTONE_133_ID = 133L;
    private static final int FOUR = 4;
    private static CaseMilestoneListDto caseMilestoneListDto = new CaseMilestoneListDto();

    private static Long originalCaseStatus;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        caseMilestoneListDto.setCaseMilestoneList(new ArrayList<CaseMilestoneDto>());

        final CaseWithAssetDetailsDto case2 = TestUtils.getResponse(String.format(BASE_CASE, CASE_2_ID), CaseWithAssetDetailsDto.class);
        originalCaseStatus = case2.getCaseStatus().getId();
        case2.getCaseStatus().setId(CaseStatusType.POPULATING.getValue());
        TestUtils.putResponse(String.format(BASE_CASE, CASE_2_ID), case2);
    }

    @After
    public void after()
    {
        final CaseWithAssetDetailsDto case2 = TestUtils.getResponse(String.format(BASE_CASE, CASE_2_ID),
                CaseWithAssetDetailsDto.class);
        originalCaseStatus = case2.getCaseStatus().getId();
        case2.getCaseStatus().setId(originalCaseStatus);
        TestUtils.putResponse(String.format(BASE_CASE, CASE_2_ID), case2);
    }

    private void addRange(final CaseMilestoneListDto caseMilestoneList, final Long fromId, final Long toId) throws DatabaseUnitException
    {
        for (Long i = fromId; i <= toId; ++i)
        {
            caseMilestoneList.getCaseMilestoneList().add(DataUtils.getCaseMilestone(i));
        }
    }

    private CaseMilestoneDto getCaseMilestone(final Long id, final CaseMilestoneListDto caseMilestoneList)
    {
        CaseMilestoneDto returnValue = null;
        for (final CaseMilestoneDto caseMilestone : caseMilestoneList.getCaseMilestoneList())
        {
            if (caseMilestone.getId().equals(id))
            {
                returnValue = caseMilestone;
                break;
            }
        }

        return returnValue;
    }

    @Test
    public void testGetMilestonesForCase() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_101_ID, CASE_MILESTONE_118_ID);
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_121_ID, CASE_MILESTONE_132_ID);
        TestUtils.singleItemFound(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, true);
    }

    @Test
    public void testGetMilestonesForCaseCaseNotFound() throws DatabaseUnitException
    {
        TestUtils.singleItemNotFound(String.format(BASE_CASE_MILESTONES, INVALID_CASE_ID));
    }

    @Test
    @Ignore("Milestone relationship ref data changes may be repsonsible for this breaking.")
    public void testCompleteSingleMilestone() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_106_ID, CASE_MILESTONE_106_ID);
        caseMilestoneListDto.getCaseMilestoneList().get(0).getMilestoneStatus().setId(CaseMilestoneStatusType.COMPLETE.getValue());
        caseMilestoneListDto.getCaseMilestoneList().get(0).setCompletionDate(new Date());

        final CaseMilestoneListDto responce = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.OK, CaseMilestoneListDto.class);

        assertEquals(caseMilestoneListDto.getCaseMilestoneList().get(0).getMilestoneStatus().getId(),
                getCaseMilestone(CASE_MILESTONE_106_ID, responce).getMilestoneStatus().getId());

        // Call again to verify that an second update is not allowed.
        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("Entity with id ");
        errorMessage.append(CASE_MILESTONE_106_ID);
        errorMessage.append(" is in a state of complete and cannot be updated.");

        assertEquals(errorMessage.toString(), error.getMessage());
    }

    @Test
    @Ignore
    public void testChangeScopeOfSingleMilestone() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_107_ID, CASE_MILESTONE_107_ID);

        for (int i = 0; i < FOUR; ++i)
        { // loop to try both ways and show that this can change as many times as needed.
            caseMilestoneListDto.getCaseMilestoneList().get(0).setInScope(!caseMilestoneListDto.getCaseMilestoneList().get(0).getInScope());

            final CaseMilestoneListDto responce = TestUtils
                    .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.OK, CaseMilestoneListDto.class);

            assertEquals(caseMilestoneListDto.getCaseMilestoneList().get(0).getInScope(),
                    getCaseMilestone(CASE_MILESTONE_107_ID, responce).getInScope());
        }
    }

    @Test
    @Ignore
    public void testChangeScopeOfTreeOfMilestones() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_107_ID, CASE_MILESTONE_107_ID);
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_124_ID, CASE_MILESTONE_125_ID);

        for (int i = 0; i < FOUR; ++i)
        { // loop to try both ways and show that this can change as many times as needed.
            caseMilestoneListDto.getCaseMilestoneList().get(0).setInScope(!caseMilestoneListDto.getCaseMilestoneList().get(0).getInScope());
            caseMilestoneListDto.getCaseMilestoneList().get(1).setInScope(!caseMilestoneListDto.getCaseMilestoneList().get(1).getInScope());
            caseMilestoneListDto.getCaseMilestoneList().get(2).setInScope(!caseMilestoneListDto.getCaseMilestoneList().get(2).getInScope());

            final CaseMilestoneListDto responce = TestUtils
                    .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.OK, CaseMilestoneListDto.class);

            assertEquals(caseMilestoneListDto.getCaseMilestoneList().get(0).getInScope(),
                    getCaseMilestone(CASE_MILESTONE_107_ID, responce).getInScope());
            assertEquals(caseMilestoneListDto.getCaseMilestoneList().get(1).getInScope(),
                    getCaseMilestone(CASE_MILESTONE_124_ID, responce).getInScope());
            assertEquals(caseMilestoneListDto.getCaseMilestoneList().get(2).getInScope(),
                    getCaseMilestone(CASE_MILESTONE_125_ID, responce).getInScope());
        }
    }

    public void testChangeScopeOfSingleMilestoneMandatory() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_132_ID, CASE_MILESTONE_132_ID);

        caseMilestoneListDto.getCaseMilestoneList().get(0).setInScope(!caseMilestoneListDto.getCaseMilestoneList().get(0).getInScope());

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("Entity with id ");
        errorMessage.append(CASE_MILESTONE_132_ID);
        errorMessage.append(" is in a state of mandatory and cannot be updated");

        assertEquals(errorMessage.toString(), error.getMessage());
    }

    @Test
    @Ignore("Currently, case creation does not work. Unignore when this fundamental issue is fixed in ref data.")
    public void testMilestoneUpdateFailsWithStaleData() throws DatabaseUnitException
    {
        // create an asset
        final AssetDto existingAsset = TestUtils.getResponse(format(ASSET, EXISTING_ASSET_ID_TO_COPY), HttpStatus.OK).jsonPath().getObject("",
                AssetDto.class);

        existingAsset.setId(null);
        existingAsset.setAssetModel(null);
        existingAsset.setYardNumber(UUID.randomUUID().toString().substring(0, MAXIMUM_LENGTH_OF_YARD_NUMBER));

        final AssetDto newAsset = TestUtils.postResponse(BASE_ASSET, existingAsset, HttpStatus.OK).jsonPath().getObject("",
                AssetDto.class);

        // create a case
        final CaseDto newCase = new CaseDto();
        newCase.setAsset(new LinkResource(newAsset.getId()));
        newCase.setCaseAcceptanceDate(new Date());
        newCase.setCaseStatus(new LinkResource(CASE_STATUS_TO_ALLOW_CREATION));
        newCase.setCaseType(new LinkResource(CASE_TYPE_TO_ALLOW_CREATION));
        newCase.setContractReferenceNumber("Case");

        final OfficeLinkDto officeLinkDto = new OfficeLinkDto();
        officeLinkDto.setOffice(new LinkResource(OFFICE_TYPE_TO_ALLOW_CASE_CREATION));
        officeLinkDto.setOfficeRole(new LinkResource(OFFICE_ROLE_TO_ALLOW_CASE_CREATION));
        newCase.setOffices(Arrays.asList(officeLinkDto));

        final CaseSurveyorWithLinksDto caseSurveyorWithLinksDtoOne = new CaseSurveyorWithLinksDto();
        caseSurveyorWithLinksDtoOne.setEmployeeRole(new LinkResource(EmployeeRoleType.MANAGEMENT.getValue()));
        caseSurveyorWithLinksDtoOne.setSurveyor(new LinkResource(FIRST_SURVEYOR_ID_TO_ALLOW_CASE_CREATION));
        final CaseSurveyorWithLinksDto caseSurveyorWithLinksDtoTwo = new CaseSurveyorWithLinksDto();
        caseSurveyorWithLinksDtoTwo.setEmployeeRole(new LinkResource(EmployeeRoleType.EIC_ADMIN.getValue()));
        caseSurveyorWithLinksDtoTwo.setSurveyor(new LinkResource(SECOND_SURVEYOR_ID_TO_ALLOW_CASE_CREATION));
        newCase.setSurveyors(Arrays.asList(caseSurveyorWithLinksDtoOne, caseSurveyorWithLinksDtoTwo));

        final CaseWithAssetDetailsDto caseWithAssetDetailsDto = TestUtils.postResponse(BASE_PATH + "/case", newCase, HttpStatus.OK).jsonPath()
                .getObject("",
                        CaseWithAssetDetailsDto.class);

        // get the milestones for the case

        final Response getResponse = TestUtils.getResponse(format(BASE_CASE_MILESTONES, caseWithAssetDetailsDto.getId()), HttpStatus.OK);
        final CaseMilestoneListDto existingListDto = getResponse.jsonPath()
                .getObject("", CaseMilestoneListDto.class);

        existingListDto.getCaseMilestoneList().get(0).setStalenessHash("Invalid Staleness Hash");

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_1_ID), existingListDto, HttpStatus.BAD_REQUEST).jsonPath()
                .getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.CASE_MILESTONE.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public void testCompleteSingleMilestoneNoCompletionDate() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_101_ID, CASE_MILESTONE_101_ID);
        caseMilestoneListDto.getCaseMilestoneList().get(0).getMilestoneStatus().setId(CaseMilestoneStatusType.COMPLETE.getValue());

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("The case milestone with id ");
        errorMessage.append(CASE_MILESTONE_101_ID);
        errorMessage.append(" cannot be marked as complete while the field completionDate is null.");

        assertEquals(errorMessage.toString(), error.getMessage());
    }

    @Test
    @Ignore
    // no milestone fulfil the conditions needed for this test
    public void testCompleteSingleMilestoneParentNotComplete() throws DatabaseUnitException
    {
        this.addRange(caseMilestoneListDto, CASE_MILESTONE_103_ID, CASE_MILESTONE_103_ID);
        caseMilestoneListDto.getCaseMilestoneList().get(0).getMilestoneStatus().setId(CaseMilestoneStatusType.COMPLETE.getValue());
        caseMilestoneListDto.getCaseMilestoneList().get(0).setCompletionDate(new Date());

        final ErrorMessageDto error = TestUtils
                .putResponse(String.format(BASE_CASE_MILESTONES, CASE_2_ID), caseMilestoneListDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();
        errorMessage.append("The case milestone with id ");
        errorMessage.append(CASE_MILESTONE_103_ID);
        errorMessage.append(" cannot be updated without updating its parent.");

        assertEquals(errorMessage.toString(), error.getMessage());
    }
}
