package com.baesystems.ai.lr.rest.assets;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemPageResourceDto;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.LazyItemPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.BusinessProcesses;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.NewObjects;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class ItemRestTest extends BaseRestTest
{
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long ASSET_3_ID = 3L;
    private static final Long ASSET_5_ID = 5L;
    private static final Long ASSET_INVALID_ID = 10L;
    private static final Long ASSET_181_ID = 181L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L;
    private static final Long ITEM_3_ID = 3L;
    private static final Long ITEM_4_ID = 4L;
    private static final Long ITEM_5_ID = 5L;
    private static final Long ITEM_6_ID = 6L;
    private static final Long ITEM_7_ID = 7L;
    private static final Long ITEM_23_ID = 23L;
    private static final Long ITEM_40_ID = 40L;
    private static final Long ITEM_50_ID = 50L;
    private static final Long ITEM_91_ID = 91L;
    private static final Long ITEM_92_ID = 92L;
    private static final Long ITEM_93_ID = 93L;
    private static final Long ITEM_94_ID = 94L;
    private static final Long ITEM_95_ID = 95L;
    private static final Long ITEM_96_ID = 96L;
    private static final Long ITEM_1191_ID = 1191L;
    private static final Long ITEM_34435_ID = 34435L;
    private static final Long IS_RELATED_TO_ID = 9L;
    private static final Long IS_PART_OF_ID = 1L;
    private static final Long INVALID_ITEM_ID = 400L;
    private static final Long RELATIONSHIP_5_ID = 94L;
    private static final Long RELATIONSHIP_94_ID = 94L;
    private static final Long ITEM_RELATIONSHIP_93_ID = 93L;
    private static final Long ITEM_RELATIONSHIP_94_ID = 94L;
    private static final Long ITEM_TYPE_458_ID = 458L;
    private static final String ATTRIBUTE_VALUE_65 = "65";
    private static final Long ATTRIBUTE_9_ID = 9L;
    private static final Long ATTRIBUTE_10_ID = 10L;
    private static final Long ATTRIBUTE_11_ID = 11L;
    private static final Long ATTRIBUTE_12_ID = 12L;
    private static final Long ATTRIBUTE_14_ID = 14L;
    private static final Long ATTRIBUTE_21_ID = 21L;
    private static final Long ATTRIBUTE_TYPE_367_ID = 367L;
    private static final Long ATTRIBUTE_TYPE_889_ID = 889L;
    private static final Long ATTRIBUTE_TYPE_891_ID = 891L;
    private static final Long ATTRIBUTE_TYPE_1009_ID = 1009L;
    private static final Long ATTRIBUTE_TYPE_1488_ID = 1488L;
    private static final Long ATTRIBUTE_TYPE_1502_ID = 1502L;
    private static final String ITEM_NAME_ANCHORING_GROUP = "Anchoring group";
    private static final String ITEM_NAME_ANCHORING_GR_WILDCARD = "Anchoring gr*";
    private static final String ITEM_NAME_ANCHORING_GR_PERCENT = "Anchoring gr%";
    private static final String ITEM_TYPE_NAME_ANCHORING_RELEASE_RETRIEVING_SYSTEM = "ANCHOR RELEASE & RETRIEVING SYSTEM";
    private static final String ITEM_TYPE_NAME_ANCHOR_RELEASE_WILDCARD = "ANCHOR RELEASE*";
    private static final String ITEM_TYPE_NAME_ANCHOR_RELEASE_PERCENT = "ANCHOR RELEASE%";
    private static final String INDEPENDENT_TANK_WILDCARD_ATTRIBUTE_NAME = "INDEPENDENT TANK*";
    private static final String INDEPENDENT_TANK_SHAPE_ATTRIBUTE_NAME = "INDEPENDENT TANK SHAPE";
    private static final String QUERY_ATTRIBUTE_NAME_ALPHANUMERIC_ID = "ALPHANUMERIC ID";
    private static final String QUERY_NON_EXISTENT_ATTRIBUTE_NAME = "THIS ATTRIBUTE NAME DOES NOT EXIST";
    private static final String ITEM_NAME_CORROSION_CONTROL_GROUP = "CORROSION CONTROL GROUP";
    private static final String QUERY_NON_EXISTENT_ITEM_NAME = "THIS ITEM NAME DOES NOT EXIST";
    private static final String COMBUSTION_CHAMBER = "COMBUSTION CHAMBER";
    private static final int DISPLAY_ORDER_TEST_NUMBER_OF_ITEMS = 7;
    private static final int DISPLAY_ORDER_TEST_ITEM_TO_DELETE = 3;
    private static final Long VERSION_TWO_ID = 2L;
    private static final Long ITEM_93_ATTRIBUTE_COUNT = 4L;

    private LazyItemPage lazyItemPage;

    private static final List<Long> ATTRIBUTE_TYPE_IDS = new ArrayList<Long>();

    private static final int QUERY_TEST_SIZE = 12;

    @DataPoints
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints
    public static final String[] DELETE_APIS = {VALIDATE_DELETE_ITEM, SPECIFIC_ITEM};

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        QUERIES[index] = new QueryAndResult("itemId", Arrays.asList(ITEM_4_ID), new Long[]{ITEM_4_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeId", Arrays.asList(ITEM_TYPE_458_ID), new Long[]{ITEM_6_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemName", ITEM_NAME_ANCHORING_GROUP, new Long[]{ITEM_4_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemName", ITEM_NAME_ANCHORING_GR_WILDCARD, new Long[]{ITEM_4_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemName", ITEM_NAME_ANCHORING_GR_PERCENT, new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeName", ITEM_TYPE_NAME_ANCHORING_RELEASE_RETRIEVING_SYSTEM, new Long[]{ITEM_5_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeName", ITEM_TYPE_NAME_ANCHOR_RELEASE_WILDCARD, new Long[]{ITEM_5_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeName", ITEM_TYPE_NAME_ANCHOR_RELEASE_PERCENT, new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("attributeValue", ATTRIBUTE_VALUE_65, new Long[]{ITEM_40_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("attributeId", Arrays.asList(ATTRIBUTE_14_ID, ATTRIBUTE_21_ID), new Long[]{ITEM_40_ID, ITEM_50_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("attributeName", INDEPENDENT_TANK_SHAPE_ATTRIBUTE_NAME, new Long[]{ITEM_40_ID});
        ++index;
        QUERIES[index] = new QueryAndResult("attributeName", INDEPENDENT_TANK_WILDCARD_ATTRIBUTE_NAME, new Long[]{ITEM_40_ID});
    }

    private void initialiseItemPage(final Long... ids)
    {
        this.lazyItemPage = new LazyItemPage();

        LazyItemDto tempDto;
        for (final Long id : ids)
        {
            try
            {
                tempDto = DataUtils.getLazyItem(id, false);
                this.lazyItemPage.getContent().add(tempDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    @Theory
    public final void queries(final QueryAndResult queryAndResult)
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException, NoSuchFieldException
    {
        initialiseItemPage(queryAndResult.getResult());
        final ItemQueryDto query = new ItemQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final LazyItemPage results = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), query, HttpStatus.OK, LazyItemPage.class);

        TestUtils.compareFields(results, this.lazyItemPage, true);
    }

    /*
     * Tests GET /asset/x/item/y expecting positive response
     */
    @Test
    public void testGetSingleItemFound()
    {
        TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_3_ID, ITEM_4_ID), HttpStatus.OK);
    }

    // Todo needs adapting
    @Ignore
    @Test
    public void ensureGetRootItemDoesNotReturnDeletedChildItems()
    {
        TestUtils.deleteResponse(String.format(SPECIFIC_ITEM, ASSET_181_ID, ITEM_34435_ID), HttpStatus.OK);

        final LazyItemDto itemDto = TestUtils.getResponse(String.format(ROOT_ITEM, ASSET_181_ID), HttpStatus.OK, LazyItemDto.class);

        final long numberOfOccurrencesOfDeletedItem = itemDto.getItems().stream().filter(r -> r.getId().equals(ITEM_34435_ID)).count();

        assertEquals(0, numberOfOccurrencesOfDeletedItem);
    }

    /*
     * Tests POST /asset/x/item/query passing in an item name and an item type that mismatch. Because itemAndType is
     * left as its default of false, this should return no results.
     */
    @Test
    public void testPostQueryWithItemAndTypeMismatchDisallowed()
    {
        final ItemQueryDto queryDto = new ItemQueryDto();
        // item type 458 DOESN'T correspond to corrosion control group
        final List<Long> itemTypeIdAsList = Arrays.asList(ITEM_TYPE_458_ID);
        queryDto.setItemTypeId(itemTypeIdAsList);
        queryDto.setItemName(ITEM_NAME_CORROSION_CONTROL_GROUP);
        final LazyItemPageResourceDto response = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), queryDto, HttpStatus.OK,
                LazyItemPageResourceDto.class);
        assertTrue(response.getContent().isEmpty());
    }

    /*
     * Tests POST /asset/x/item/query passing in an item name and an item type that mismatch. Because itemAndType is set
     * to true, this should return a result as only one needs to pass the filter.
     */
    @Test
    public void testPostQueryWithItemAndTypeMismatchAllowed()
    {
        final ItemQueryDto queryDto = new ItemQueryDto();
        queryDto.setItemName(ITEM_NAME_CORROSION_CONTROL_GROUP);
        queryDto.setItemTypeName(QUERY_NON_EXISTENT_ITEM_NAME);
        final LazyItemPageResourceDto response = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), queryDto, HttpStatus.OK,
                LazyItemPageResourceDto.class);
        assertFalse(response.getContent().isEmpty());
    }

    /*
     * Tests POST /asset/x/item/query passing in an item name and an item type that mismatch. Because itemAndType is set
     * to true, this should return a result as only one needs to pass the filter.
     */
    @Test
    public void testPostQueryWithItemAndTypeMismatchAllowed2()
    {
        final ItemQueryDto queryDto = new ItemQueryDto();
        queryDto.setItemName(QUERY_NON_EXISTENT_ITEM_NAME);
        queryDto.setItemTypeName(COMBUSTION_CHAMBER);
        final LazyItemPageResourceDto response = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), queryDto, HttpStatus.OK,
                LazyItemPageResourceDto.class);
        assertFalse(response.getContent().isEmpty());
    }

    /*
     * Tests POST /asset/x/item/query passing an attribute name expecting a positive response and content, as asset 3
     * has items with 'ALPHANUMERIC_ID' attributes
     */
    @Test
    public void testPostQueryWithAttributeNameSuccess()
    {
        final ItemQueryDto queryDto = new ItemQueryDto();
        queryDto.setAttributeName(QUERY_ATTRIBUTE_NAME_ALPHANUMERIC_ID);
        final LazyItemPageResourceDto response = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), queryDto, HttpStatus.OK,
                LazyItemPageResourceDto.class);
        assertFalse(response.getContent().isEmpty());
    }

    /*
     * Tests POST /asset/x/item/query passing in an attribute name that doesn't relate to any attribute types returns an
     * empty response.
     */
    @Test
    public void testPostQueryWithNonExistentAttributeName()
    {
        final ItemQueryDto queryDto = new ItemQueryDto();
        queryDto.setAttributeName(QUERY_NON_EXISTENT_ATTRIBUTE_NAME);
        final LazyItemPageResourceDto response = TestUtils.postResponse(String.format(ITEM_QUERY, ASSET_3_ID), queryDto, HttpStatus.OK,
                LazyItemPageResourceDto.class);
        assertTrue(response.getContent().isEmpty());
    }

    /*
     * Tests GET /asset/x/item/y expecting a 404 as the item is not associated with the asset
     */
    @Test
    public void testGetSingleItemNotFound()
    {
        TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_INVALID_ID, ITEM_3_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * This test calls the post and the put attribute methods repeatedly to check that the item name is being updated
     * correctly each time and that the order of the names is correct.
     * <p>
     * It tries to clean up the database afterwards (or if it fails) by deleting the attributes, but the item name
     * cannot be reset.
     */
    @Test
    @Ignore
    public void testNameUpdateOnPostAndPutAttribute() throws DatabaseUnitException
    {
        final List<Long> attributeIds = new ArrayList<Long>();

        final ItemLightDto item = DataUtils.getItemLight(ITEM_1_ID);
        final ItemTypeDto itemType = DataUtils.getItemType(item.getItemType().getId());

        final ItemDto response = TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_2_ID, ITEM_1_ID), HttpStatus.OK, ItemDto.class);

        assertTrue(response.getAttributes().isEmpty());
        assertEquals(item.getName(), response.getName());

        try
        {
            testAddAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_367_ID);
            testAddAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_889_ID);
            testAddAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_891_ID);
            testAddAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_1009_ID);
            testUpdateAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_1009_ID, ATTRIBUTE_TYPE_1488_ID);
            testUpdateAttribute(attributeIds, itemType, ATTRIBUTE_TYPE_889_ID, ATTRIBUTE_TYPE_1502_ID);
        }
        finally
        {
            deleteAttributes(attributeIds);
        }
    }

    /**
     * Call the post attribute api to change the attribute type from attributeTypeId to newAttributeTypeId. This type is
     * used to identify the attribute so two attributes of the same type cannot exist at this point, though this is not
     * needed in the test.
     * <p>
     * The list of currently added attributes and their types are maintained for validation and rolling back later.
     *
     * @param attributeIds
     * @param itemType
     * @param attributeTypeId
     * @param newAttributeTypeId
     */
    private void testUpdateAttribute(final List<Long> attributeIds, final ItemTypeDto itemType, final Long attributeTypeId,
            final Long newAttributeTypeId) throws DatabaseUnitException
    {
        final int index = ATTRIBUTE_TYPE_IDS.indexOf(attributeTypeId);
        ATTRIBUTE_TYPE_IDS.set(index, newAttributeTypeId);

        final AttributeDto attribute = new AttributeDto();
        final AttributeTypeDto attributeType = DataUtils.getAttributeType(newAttributeTypeId);
        attribute.setValue(attributeType.getName());
        attribute.setId(attributeIds.get(index));
        attribute.setAttributeType(new LinkResource(newAttributeTypeId));

        TestUtils.putResponse(String.format(SPECIFIC_ATTRIBUTE, ASSET_2_ID, ITEM_1_ID, attributeIds.get(index)), attribute, HttpStatus.OK,
                AttributeDto.class).getId();

        final ItemDto response = TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_2_ID, ITEM_1_ID), HttpStatus.OK, ItemDto.class);

        assertEquals((Integer) attributeIds.size(), (Integer) response.getAttributes().size());
        assertEquals(getItemName(itemType, ATTRIBUTE_TYPE_IDS), response.getName());
    }

    /**
     * Calls the post method to add an attribute of a given type.
     * <p>
     * The list of currently added attributes and their types are maintained for validation and rolling back later.
     *
     * @param attributeIds
     * @param itemType
     * @param attributeTypeId
     */
    private void testAddAttribute(final List<Long> attributeIds, final ItemTypeDto itemType, final Long attributeTypeId)
            throws DatabaseUnitException
    {
        ATTRIBUTE_TYPE_IDS.add(attributeTypeId);

        final AttributeDto attribute = new AttributeDto();
        final AttributeTypeDto attributeType = DataUtils.getAttributeType(attributeTypeId);
        attribute.setValue(attributeType.getName());
        attribute.setAttributeType(new LinkResource(attributeTypeId));

        attributeIds
                .add(TestUtils.postResponse(String.format(SPECIFIC_ATTRIBUTE, ASSET_2_ID, ITEM_1_ID), attribute, HttpStatus.OK, AttributeDto.class)
                        .getId());

        final ItemDto response = TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_2_ID, ITEM_1_ID), HttpStatus.OK, ItemDto.class);

        assertEquals((Integer) attributeIds.size(), (Integer) response.getAttributes().size());
        assertEquals(getItemName(itemType, ATTRIBUTE_TYPE_IDS), response.getName());
    }

    /**
     * Test that when we try to PUT an item which has null value, we get the appropriate error response back. For empty
     * attributes we should be using the empty string, not null.
     *
     * @throws DatabaseUnitException
     */
    @Test
    @Ignore
    public void testPutAttributeWithNullValue() throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);

        final LazyItemDto itemResponse = TestUtils
                .getResponse(String.format(SPECIFIC_ITEM, ASSET_3_ID, ITEM_40_ID), HttpStatus.OK, LazyItemDto.class);
        final ItemWithAttributesDto itemWithAttributes = new ItemWithAttributesDto();

        final List<AttributeDto> attributes = itemResponse.getAttributes();
        final AttributeDto attributeDto = attributes.get(0);
        attributeDto.setValue(null);

        itemWithAttributes.setAttributes(attributes);

        itemWithAttributes.setId(ITEM_40_ID);
        itemWithAttributes.setName(itemResponse.getName());
        itemWithAttributes.setDisplayOrder(itemResponse.getDisplayOrder());
        itemWithAttributes.setItemType(itemResponse.getItemType());
        itemWithAttributes.setReviewed(itemResponse.getReviewed());

        final ErrorMessageDto error = TestUtils.putResponse(String.format(SPECIFIC_ATTRIBUTE, ASSET_3_ID, ITEM_40_ID, null), itemWithAttributes,
                HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(String.format(ExceptionMessagesUtils.VALIDATION_FAILED, "attributes[0].value may not be null"), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    /**
     * Deletes newly created attributes so the test can run more than once.
     *
     * @param attributeIds
     */
    private void deleteAttributes(final List<Long> attributeIds)
    {
        for (final Long attributeId : attributeIds)
        {
            TestUtils.deleteResponse(String.format(SPECIFIC_ATTRIBUTE, ASSET_2_ID, ITEM_1_ID, attributeId), HttpStatus.OK);
        }

        ATTRIBUTE_TYPE_IDS.clear();
    }

    /**
     * Calculates the correct name for the item bases on the list of attribute types that are connected to it. The names
     * are sorted first by naming order (as requiered) and then by id (to match the results obtained from querying the
     * database). The itemType name goes on the end.
     *
     * @param itemType
     * @param attributeTypeIds
     */
    private String getItemName(final ItemTypeDto itemType, final List<Long> attributeTypeIds) throws DatabaseUnitException
    {
        final List<AttributeTypeDto> attributeTypes = new ArrayList<AttributeTypeDto>();

        for (final Long attributeTypeId : attributeTypeIds)
        {
            attributeTypes.add(DataUtils.getAttributeType(attributeTypeId));
        }

        Collections.sort(attributeTypes, new Comparator<AttributeTypeDto>()
        {
            @Override
            public int compare(final AttributeTypeDto o1, final AttributeTypeDto o2)
            {
                return o1.getNamingOrder().equals(o2.getNamingOrder()) ? o1.getId() < o2.getId() ? -1 : 1
                        : o1.getNamingOrder() < o2.getNamingOrder() ? -1 : 1;
            }
        });

        final StringBuilder name = new StringBuilder();

        for (final AttributeTypeDto attributeType : attributeTypes)
        {
            name.append(attributeType.getName());
            name.append(' ');
        }

        name.append(itemType.getName());

        return name.toString().trim();
    }

    /*
     * Tests GET /asset/x/root-item expecting positive response
     */
    @Test
    public void testGetSingleRootItemFound()
    {
        TestUtils.getResponse(String.format(ROOT_ITEM, ASSET_2_ID), HttpStatus.OK);
    }

    /*
     * Tests GET /asset/x/root-item expecting a 404 with invalid asset
     */
    @Test
    public void testGetSingleRootItemNotFound()
    {
        TestUtils.getResponse(String.format(ROOT_ITEM, ASSET_INVALID_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Tests PUT /asset/x/item/y/attribute passing a dto with a full list of attributes expected to replace what's in
     * the database.
     */
    @Test
    @Ignore
    public void confirmPutItemAttributesUpdatesFullSetOfAttributeData()
    {
        final long assetId = 3L;
        final long itemId = 40L;
        final long firstAttributeTypeId = 250L;
        final String newAttributeValue = "1234";
        final long secondAttributeTypeId = 93L;
        final long existingAttributeId = 13L;
        final String updatedAttributeValue = "4321";
        final int numberOfAttributes = 3;

        final long zeroNameOrderAttributeTypeId = 4L;
        final long zeroNameOrderExistingAttributeId = 14L;
        final String zeroNameOrderUpdatedAttributeValue = "NO.2";

        final LinkResource newAttributeType = new LinkResource(firstAttributeTypeId);

        final ItemWithAttributesDto requestDto = new ItemWithAttributesDto();
        requestDto.setId(itemId);

        final List<AttributeDto> attributes = new ArrayList<>();

        // New Attribute
        final AttributeDto newAttribute = new AttributeDto();
        newAttribute.setAttributeType(newAttributeType);

        newAttribute.setValue(newAttributeValue);
        attributes.add(newAttribute);

        // Updating an existing Attribute
        final LinkResource attributeType = new LinkResource(secondAttributeTypeId);

        final AttributeDto existingAttribute = new AttributeDto();
        existingAttribute.setAttributeType(attributeType);
        existingAttribute.setValue(updatedAttributeValue);
        existingAttribute.setId(existingAttributeId);

        attributes.add(existingAttribute);

        // Updating an existing attribute where the attribute type with a naming order of zero to confirm that the item
        // name hasn't changed.

        final LinkResource zeroNameOrderAttributeType = new LinkResource(zeroNameOrderAttributeTypeId);

        final AttributeDto zeroNameOrderExistingAttribute = new AttributeDto();
        zeroNameOrderExistingAttribute.setAttributeType(zeroNameOrderAttributeType);
        zeroNameOrderExistingAttribute.setValue(zeroNameOrderUpdatedAttributeValue);
        zeroNameOrderExistingAttribute.setId(zeroNameOrderExistingAttributeId);

        attributes.add(zeroNameOrderExistingAttribute);

        requestDto.setAttributes(attributes);

        final ItemWithAttributesDto responseDto = TestUtils.putResponse(String.format(SPECIFIC_ATTRIBUTE, assetId, itemId), requestDto,
                HttpStatus.OK,
                ItemWithAttributesDto.class);

        assertEquals(itemId, responseDto.getId().longValue());

        assertEquals(numberOfAttributes, responseDto.getAttributes().size());

        final AttributeDto firstAttributeDto = responseDto.getAttributes().get(0);
        assertNotNull(firstAttributeDto.getId());
        assertEquals(newAttributeValue, firstAttributeDto.getValue());

        final AttributeDto secondAttributeDto = responseDto.getAttributes().get(1);
        assertEquals(existingAttributeId, secondAttributeDto.getId().longValue());
        assertEquals(updatedAttributeValue, secondAttributeDto.getValue());

        final AttributeDto thirdAttributeDto = responseDto.getAttributes().get(2);
        assertEquals(zeroNameOrderExistingAttributeId, thirdAttributeDto.getId().longValue());
        assertEquals(zeroNameOrderUpdatedAttributeValue, thirdAttributeDto.getValue());

        // Confirm the item name is made up of attribute values with a non-zero naming order then the item type.
        assertEquals("4321 1234 SUCTION SCOOP", responseDto.getName());

        // This extra step confirms that deleted attributes are not returned
        final LazyItemDto itemResponseDto = TestUtils.getResponse(String.format(SPECIFIC_ITEM, assetId, itemId), HttpStatus.OK, LazyItemDto.class);

        assertEquals(numberOfAttributes, itemResponseDto.getAttributes().size());
    }

    @Test
    public void testDisplayOrderReorganisation()
            throws DatabaseUnitException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException
    {
        BusinessProcesses.checkAssetOut(ASSET_5_ID);
        final LazyItemDto parentItemDto = TestUtils.getResponse(String.format(SPECIFIC_ITEM, ASSET_5_ID, ITEM_1191_ID), HttpStatus.OK).jsonPath()
                .getObject("", LazyItemDto.class);
        final List<LinkVersionedResource> items = parentItemDto.getItems();
        final HashMap<Integer, ItemLightDto> originalDisplayOrders = new HashMap<Integer, ItemLightDto>();
        for (final LinkVersionedResource item : items)
        {
            final ItemLightDto childItem = DataUtils.getItemLight(item.getId());
            originalDisplayOrders.put(childItem.getDisplayOrder(), childItem);
        }

        // this is how many we expect for this item - to ensure the test data doesn't change and render this test
        // useless.
        assertEquals(DISPLAY_ORDER_TEST_NUMBER_OF_ITEMS, items.size());
        final Long idForDeletion = originalDisplayOrders.get(Integer.valueOf(DISPLAY_ORDER_TEST_ITEM_TO_DELETE)).getId();
        TestUtils.deleteResponse(String.format(SPECIFIC_ITEM, ASSET_5_ID, idForDeletion), HttpStatus.OK);
        for (int i = 0; i < DISPLAY_ORDER_TEST_ITEM_TO_DELETE; i++)
        {
            final Integer originalDisplayOrder = Integer.valueOf(i);
            final Integer currentDisplayOrder = DataUtils.getItemLight(originalDisplayOrders.get(originalDisplayOrder).getId()).getDisplayOrder();
            assertEquals(originalDisplayOrder, currentDisplayOrder);
        }

        final Long draftVersion = DataUtils.getAssetDraftVersion(ASSET_5_ID);

        assertTrue(DataUtils.isVersionedItemDeleted(idForDeletion, draftVersion));

        for (int i = (DISPLAY_ORDER_TEST_ITEM_TO_DELETE + 1); i < DISPLAY_ORDER_TEST_NUMBER_OF_ITEMS; i++)
        {
            final Integer originalDisplayOrder = Integer.valueOf(i);
            final Integer currentDisplayOrder = DataUtils.getItemLight(originalDisplayOrders.get(originalDisplayOrder).getId(), draftVersion)
                    .getDisplayOrder();
            final Integer expectedDisplayOrder = Integer.valueOf(originalDisplayOrder.intValue() - 1);
            assertEquals(expectedDisplayOrder, currentDisplayOrder);
        }
        BusinessProcesses.checkAssetIn(ASSET_5_ID);
    }

    @Theory
    public void testDeleteItem(final String url)
            throws DatabaseUnitException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, IOException
    {
        // Cannot rely on the published version being an items' most recent version
        final Long mostRecentItem91Version = DataUtils.getMostRecentVersionIdForItem(ITEM_91_ID);
        final Long mostRecentItem93Version = DataUtils.getMostRecentVersionIdForItem(ITEM_93_ID);
        final Long mostRecentItem94Version = DataUtils.getMostRecentVersionIdForItem(ITEM_94_ID);
        final Long mostRecentItem95Version = DataUtils.getMostRecentVersionIdForItem(ITEM_95_ID);

        BusinessProcesses.checkAssetOut(ASSET_1_ID);

        TestUtils.deleteResponse(String.format(url, ASSET_1_ID, ITEM_93_ID), HttpStatus.OK);

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
        final Long publishedVersion = DataUtils.getAssetPublishedVersion(ASSET_1_ID);
        if (!url.contains("validate"))
        {
            // Check original items still exist
            assertTrue(!DataUtils.isVersionedItemDeleted(ITEM_93_ID, mostRecentItem93Version));
            assertTrue(!DataUtils.isVersionedItemDeleted(ITEM_94_ID, mostRecentItem94Version));
            assertTrue(!DataUtils.isVersionedItemDeleted(ITEM_95_ID, mostRecentItem95Version));

            //Check the up-versioned items have been deleted.
            assertTrue(DataUtils.isVersionedItemDeleted(ITEM_93_ID, publishedVersion));
            assertTrue(DataUtils.isVersionedItemDeleted(ITEM_94_ID, publishedVersion));
            assertTrue(DataUtils.isVersionedItemDeleted(ITEM_95_ID, publishedVersion));

            //Check original attributes still exist - This works because the PK is still the ID  column
            assertTrue(!DataUtils.isDeleted("MAST_ASSET_AssetItemAttribute", ATTRIBUTE_9_ID));
            assertTrue(!DataUtils.isDeleted("MAST_ASSET_AssetItemAttribute", ATTRIBUTE_10_ID));
            assertTrue(!DataUtils.isDeleted("MAST_ASSET_AssetItemAttribute", ATTRIBUTE_11_ID));
            assertTrue(!DataUtils.isDeleted("MAST_ASSET_AssetItemAttribute", ATTRIBUTE_12_ID));

            // Check attributes have been up-versioned for asset 93 - there should be four
            final List<AttributeDto> upversionedAttributes = DataUtils.getAttributesForVersionedItem(ITEM_93_ID, publishedVersion);
            assertTrue(upversionedAttributes.size() == ITEM_93_ATTRIBUTE_COUNT);
            for (AttributeDto attribute : upversionedAttributes)
            {
                assertTrue(DataUtils.attributeIsDeleted(attribute.getItem().getId(), publishedVersion));
            }

            // Check the original relationships have NOT been deleted.
            assertTrue(!DataUtils.isVersionedItemRelationshipDeleted(ITEM_91_ID, mostRecentItem91Version, ITEM_93_ID, mostRecentItem93Version));
            assertTrue(!DataUtils.isVersionedItemRelationshipDeleted(ITEM_93_ID, mostRecentItem93Version, ITEM_94_ID, mostRecentItem94Version));
            assertTrue(!DataUtils.isVersionedItemRelationshipDeleted(ITEM_93_ID, mostRecentItem93Version, ITEM_95_ID, mostRecentItem95Version));

            // Check the up-versioned relationships HAVE been deleted
            assertTrue(DataUtils.isVersionedItemRelationshipDeleted(ITEM_91_ID, mostRecentItem91Version, ITEM_93_ID, publishedVersion));
            assertTrue(DataUtils.isVersionedItemRelationshipDeleted(ITEM_93_ID, publishedVersion, ITEM_94_ID, publishedVersion));
            assertTrue(DataUtils.isVersionedItemRelationshipDeleted(ITEM_93_ID, publishedVersion, ITEM_95_ID, publishedVersion));
        }


    }

    @Theory
    public void testDeleteItemRootItem(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_3_ID, ITEM_4_ID), HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Entity with identifier: ");
        errorMessage.append(ITEM_4_ID);
        errorMessage.append(" cannot be deleted because it is a root item.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Theory
    public void testDeleteItemLinkedtoDefect(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_3_ID, ITEM_7_ID), HttpStatus.CONFLICT, ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with id ");
        errorMessage.append(ITEM_7_ID);
        errorMessage.append(" cannot be deleted because it, or one of its child items, is linked to one or more open defects.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Theory
    public void testDeleteItemImmediateChildLinkedtoDefect(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_3_ID, ITEM_6_ID), HttpStatus.CONFLICT, ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with id ");
        errorMessage.append(ITEM_6_ID);
        errorMessage.append(" cannot be deleted because it, or one of its child items, is linked to one or more open defects.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Theory
    public void testDeleteItemDistantChildLinkedtoDefect(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_3_ID, ITEM_5_ID), HttpStatus.CONFLICT, ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with id ");
        errorMessage.append(ITEM_5_ID);
        errorMessage.append(" cannot be deleted because it, or one of its child items, is linked to one or more open defects.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Theory
    public void testDeleteItemLinkedtoCodicil(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_1_ID, ITEM_92_ID), HttpStatus.CONFLICT,
                ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with id ");
        errorMessage.append(ITEM_92_ID);
        errorMessage.append(" cannot be deleted because it, or one of its child items, is linked to one or more open codicils.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Theory
    public void testDeleteItemImmediateChildLinkedtoCodicil(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);

        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_1_ID, ITEM_91_ID), HttpStatus.CONFLICT,
                ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with id ");
        errorMessage.append(ITEM_91_ID);
        errorMessage.append(" cannot be deleted because it, or one of its child items, is linked to one or more open codicils.");

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Theory
    public void testDeleteItemWrongAsset(final String url) throws DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);
        final ErrorMessageDto error = TestUtils.deleteResponse(String.format(url, ASSET_3_ID, ITEM_91_ID), HttpStatus.NOT_FOUND,
                ErrorMessageDto.class);
        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with identifier ");
        errorMessage.append(ITEM_91_ID);
        errorMessage.append(" not found for asset ");
        errorMessage.append(ASSET_3_ID);

        assertEquals(errorMessage.toString(), error.getMessage());

        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    /*
     * Tests the creation and deletion of an Item Relationship
     */
    // todo adapt
    @Test
    @Ignore
    public final void createItemRelationshipAndDeleteIt()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException, DatabaseUnitException
    {
        // create
        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(ITEM_23_ID);
        newItemRelationshipDto.setType(new LinkResource(IS_RELATED_TO_ID));

        final ItemRelationshipDto createdDto1 = TestUtils
                .postResponse(format(ITEM_RELATIONSHIP_BASE, ASSET_3_ID, ITEM_50_ID), newItemRelationshipDto, HttpStatus.OK,
                        ItemRelationshipDto.class);

        assertNotNull(createdDto1);
        assertNotNull(createdDto1.getId());
        TestUtils.compareFields(createdDto1, newItemRelationshipDto, true);

        // delete
        TestUtils.deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, ASSET_3_ID, ITEM_50_ID, createdDto1.getId()), HttpStatus.OK);

        assertTrue(DataUtils.isDeleted("MAST_ASSET_AssetItemRelationship", createdDto1.getId()));

        // recreate (this should be allowed)
        final ItemRelationshipDto createdDto2 = TestUtils
                .postResponse(format(ITEM_RELATIONSHIP_BASE, ASSET_3_ID, ITEM_50_ID), newItemRelationshipDto, HttpStatus.OK,
                        ItemRelationshipDto.class);

        assertFalse(createdDto2.getId().equals(createdDto1.getId()));

        TestUtils.deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, ASSET_3_ID, ITEM_50_ID, createdDto2.getId()), HttpStatus.OK);
    }

    @Test
    public final void createItemRelationshipWrongType()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(ITEM_23_ID);
        newItemRelationshipDto.setType(new LinkResource(IS_PART_OF_ID));

        TestUtils.postResponse(format(ITEM_RELATIONSHIP_BASE, ASSET_3_ID, ITEM_50_ID), newItemRelationshipDto, HttpStatus.BAD_REQUEST);
    }

    @Test
    public final void createItemRelationshipWrongAssetFrom()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);
        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(ITEM_23_ID);
        newItemRelationshipDto.setType(new LinkResource(IS_RELATED_TO_ID));

        TestUtils.postResponse(format(ITEM_RELATIONSHIP_BASE, ASSET_3_ID, ITEM_96_ID), newItemRelationshipDto, HttpStatus.NOT_FOUND);
        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Test
    public final void createItemRelationshipWrongAssetTo()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);
        final ItemRelationshipDto newItemRelationshipDto = new ItemRelationshipDto();
        newItemRelationshipDto.setToItem(new ItemLightDto());
        newItemRelationshipDto.getToItem().setId(ITEM_96_ID);
        newItemRelationshipDto.setType(new LinkResource(IS_RELATED_TO_ID));

        TestUtils.postResponse(format(ITEM_RELATIONSHIP_BASE, ASSET_3_ID, ITEM_50_ID), newItemRelationshipDto, HttpStatus.NOT_FOUND);
        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Test
    public final void deleteItemRelationshipForWrongItem()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        BusinessProcesses.checkAssetOut(ASSET_1_ID);
        final ErrorMessageDto error = TestUtils
                .deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, ASSET_1_ID, INVALID_ITEM_ID, RELATIONSHIP_94_ID), HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item with identifier ");
        errorMessage.append(INVALID_ITEM_ID);
        errorMessage.append(" not found for asset ");
        errorMessage.append(ASSET_1_ID);

        assertEquals(errorMessage.toString(), error.getMessage());
        BusinessProcesses.checkAssetIn(ASSET_1_ID);
    }

    @Test
    public final void deleteItemRelationshipForWrongRealtionship()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException, DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);
        final ErrorMessageDto error = TestUtils
                .deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, ASSET_3_ID, ITEM_5_ID, RELATIONSHIP_94_ID), HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item Relationship with identifier ");
        errorMessage.append(RELATIONSHIP_94_ID);
        errorMessage.append(", type 9, not found for Item ");
        errorMessage.append(ITEM_5_ID);

        assertEquals(errorMessage.toString(), error.getMessage());
        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    @Test
    public final void deleteItemRelationshipForNotIsRelatedTo()
            throws InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException, DatabaseUnitException
    {
        BusinessProcesses.checkAssetOut(ASSET_3_ID);
        final ErrorMessageDto error = TestUtils
                .deleteResponse(format(SPECIFIC_ITEM_RELATIONSHIP, ASSET_3_ID, ITEM_4_ID, RELATIONSHIP_5_ID), HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);

        final StringBuilder errorMessage = new StringBuilder();

        errorMessage.append("Item Relationship with identifier ");
        errorMessage.append(RELATIONSHIP_5_ID);
        errorMessage.append(", type 9, not found for Item ");
        errorMessage.append(ITEM_4_ID);

        assertEquals(errorMessage.toString(), error.getMessage());
        BusinessProcesses.checkAssetIn(ASSET_3_ID);
    }

    /**
     * This template asset has the following structure, which we will clone onto a new asset. This means that the IDs
     * will be different, but the structure will be the same.
     * <p>
     * <<<<<<< HEAD
     * .     10071
     *      * .        \
     *      * .        10072
     *      * .       /     \
     *      * .     10073   10074
     *      * .     /   \       \
     *      * . 10075  10076   10077
     * =======
     *      * .     10071
     *      * .        \
     *      * .        10072
     *      * .       /     \
     *      * .     10073   10074
     *      * .     /   \       \
     *      * . 10075  10076   10077
     * >>>>>>> Versioning: Enabling and modifying Item Rest Tests as required & implement delete item. Work In Progress - Some tests are still ignored.
     * <p>
     * For this test we will be decommissioning 10072, meaning that 10073, 10074, 10075, 10076 and 10077
     * should also be decommissioned.
     */
    @Test
    public final void decommissionItemWithChildren()
    {
        final AssetLightDto asset = NewObjects.createAssetAndModel();
        final Long assetId = asset.getId();

        decommissionItem(assetId);
    }

    private Long decommissionItem(final Long assetId)
    {
        BusinessProcesses.checkAssetOut(assetId);

        final AssetModelDto assetModel = TestUtils.getResponse(String.format(ASSET_MODEL, assetId), HttpStatus.OK, AssetModelDto.class);
        final Long itemId = assetModel.getItems().get(0).getItems().get(0).getId();

        TestUtils.putResponse(String.format(DECOMMISSION_ITEM, assetId, itemId), "", HttpStatus.OK);

        BusinessProcesses.checkAssetIn(assetId);

        // Retrieve the item and check the flag has been set to true
        final LazyItemDto item = TestUtils.getResponse(String.format(SPECIFIC_ITEM, assetId, itemId), HttpStatus.OK, LazyItemDto.class);

        assertTrue(item.getDecommissioned());

        // Check that the child items were also decommissioned
        checkChildren(item.getItems(), assetId, true);

        return item.getId();
    }

    /**
     * This template asset has the following structure, which we will clone onto a new asset. This means that the IDs
     * will be different, but the structure will be the same.
     * <p>
     * <<<<<<< HEAD
     * .     10071
     *      * .        \
     *      * .        10072
     *      * .       /     \
     *      * .     10073   10074
     *      * .     /   \       \
     *      * . 10075  10076   10077
     * =======
     *      * .     10071
     *      * .        \
     *      * .        10072
     *      * .       /     \
     *      * .     10073   10074
     *      * .     /   \       \
     *      * . 10075  10076   10077
     * >>>>>>> Versioning: Enabling and modifying Item Rest Tests as required & implement delete item. Work In Progress - Some tests are still ignored.
     * <p>
     * For this test we will be recommissioning 10072, meaning that 10073, 10074, 10075, 10076 and 10077
     * should also be recommissioned.
     */
    @Test
    public final void recommissionItemWithChildren()
    {
        final AssetLightDto asset = NewObjects.createAssetAndModel();
        final Long assetId = asset.getId();

        // We must decommission some items before we can recommission them
        final Long itemId = decommissionItem(assetId);

        BusinessProcesses.checkAssetOut(assetId);

        // Now recommission the same item
        TestUtils.putResponse(String.format(RECOMMISSION_ITEM, assetId, itemId), "", HttpStatus.OK);

        BusinessProcesses.checkAssetIn(assetId);

        // Retrieve the item and check the flag has been set to false
        final LazyItemDto item = TestUtils.getResponse(String.format(SPECIFIC_ITEM, assetId, itemId), HttpStatus.OK, LazyItemDto.class);
        assertFalse(item.getDecommissioned());

        // Check that the child items were also recommissioned
        checkChildren(item.getItems(), assetId, false);
    }

    private void checkChildren(final List<LinkVersionedResource> subChildren, final Long assetId, final Boolean decommissioned)
    {
        if (subChildren != null)
        {
            subChildren
                    .stream()
                    .forEach(subChild ->
                    {
                        final LazyItemDto child = TestUtils
                                .getResponse(String.format(SPECIFIC_ITEM, assetId, subChild.getId()), HttpStatus.OK, LazyItemDto.class);
                        assertEquals(decommissioned, child.getDecommissioned());
                        checkChildren(child.getItems(), assetId, decommissioned);
                    });
        }
    }
}
