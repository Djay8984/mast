package com.baesystems.ai.lr.utils;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.datatype.IDataTypeFactory;

public final class DatabaseUtils
{
    private DatabaseUtils()
    {
        // Prevents instantiation
    }

    private static IDatabaseConnection establishConnection(final String dbPrefix)
            throws SQLException, ClassNotFoundException, DatabaseUnitException, IOException, InstantiationException, IllegalAccessException
    {
        final Properties props = new Properties();
        props.load(DatabaseUtils.class.getClassLoader().getResourceAsStream("properties/jdbc.properties"));

        final String driver = props.getProperty("jdbc_" + dbPrefix + "_driverClassName");
        final String connection = new StringBuilder().append(props.getProperty("jdbc_" + dbPrefix + "_url"))
                .append(props.getProperty("jdbc_" + dbPrefix + "_schema"))
                .toString();
        final String user = props.getProperty("jdbc_" + dbPrefix + "_username");
        final String password = props.getProperty("jdbc_" + dbPrefix + "_password");
        final String dbunitFactory = props.getProperty("jdbc_" + dbPrefix + "_dbunitFactory");
        Class.forName(driver);
        final IDataTypeFactory factory = (IDataTypeFactory) Class.forName(dbunitFactory).newInstance();

        final DatabaseConnection iDbConn = new DatabaseConnection(DriverManager.getConnection(connection, user, password));

        final DatabaseConfig dbConfig = iDbConn.getConfig();
        dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, factory);
        return iDbConn;
    }

    public static IDataSet getDataSet(final String prefix)
            throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException, InstantiationException, IllegalAccessException
    {
        final IDatabaseConnection iDbConn = establishConnection(prefix);
        return iDbConn.createDataSet();
    }

    public static void refreshIncrementor(final String dataset, final String tableName)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final IDatabaseConnection iDbConn = establishConnection(dataset);
        iDbConn.getConnection().createStatement().execute("ALTER TABLE " + tableName + " auto_increment 1");
    }

    /**
     * Please document and justify when this is used
     * todo remove functionality when test data is isolated
     */
    public static void runRawQuery(final String dataset, final String query)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final IDatabaseConnection iDbConn = establishConnection(dataset);
        iDbConn.getConnection().createStatement().execute(query);
    }

    public static ResultSet getResultSetForQuery(final String dataset, final String query)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final IDatabaseConnection connection = establishConnection(dataset);
        return connection.getConnection().createStatement().executeQuery(query);
    }
}
