package com.baesystems.ai.lr.categories;

import org.dbunit.DatabaseUnitException;

/**
 * Interface used to mark a test/test class as being an Integration Test. Allows Maven to easily distinguish between
 * different test types when executing the surefire (UT) and failsafe (IT) plugins.
 * 
 * Usage: '@Category(IntegrationTest.class)'
 * 
 */
public interface PostIntegrationTest
{
    void setUp() throws DatabaseUnitException; 
}
