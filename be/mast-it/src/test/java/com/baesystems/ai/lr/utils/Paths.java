package com.baesystems.ai.lr.utils;

public abstract class Paths
{
    public static final String BASE_PATH = "mast/api/v2";

    /* Asset */
    public static final String BASE_ASSET = urlBuilder(BASE_PATH, "/asset");
    public static final String SPECIFIC_ASSET = urlBuilder(BASE_ASSET, "/%d");
    public static final String SPECIFIC_ASSET_VERSION = urlBuilder(SPECIFIC_ASSET, "?version=%d");
    public static final String CHECKOUT_ASSET = urlBuilder(SPECIFIC_ASSET, "/check-out");
    public static final String CHECKIN_ASSET = urlBuilder(SPECIFIC_ASSET, "/check-in");

    /* Asset Model */
    public static final String BASE_ASSET_MODEL = urlBuilder(BASE_PATH, "/asset-model");
    public static final String ASSET_MODEL = urlBuilder(BASE_ASSET_MODEL, "/%d");

    /* Item */
    public static final String BASE_ITEM = urlBuilder(SPECIFIC_ASSET, "/item");
    public static final String SPECIFIC_ITEM = urlBuilder(BASE_ITEM, "/%d");
    public static final String SPECIFIC_ITEM_INCLUDE_DRAFT = urlBuilder(SPECIFIC_ITEM, "?includeDraft=%s");
    public static final String ROOT_ITEM = urlBuilder(SPECIFIC_ASSET, "/root-item");
    public static final String ITEM_RELATIONSHIP_BASE = urlBuilder(SPECIFIC_ITEM, "/relationship");
    public static final String SPECIFIC_ITEM_RELATIONSHIP = urlBuilder(ITEM_RELATIONSHIP_BASE, "/%d");
    public static final String ITEM_QUERY = urlBuilder(BASE_ITEM, "/query");
    public static final String VALIDATE_DELETE_ITEM = urlBuilder(SPECIFIC_ITEM, "/validate");
    public static final String DECOMMISSION_ITEM = urlBuilder(SPECIFIC_ITEM, "/decommission");
    public static final String RECOMMISSION_ITEM = urlBuilder(SPECIFIC_ITEM, "/recommission");

    /* Attribute */
    public static final String BASE_ATTRIBUTE = urlBuilder(SPECIFIC_ITEM, "/attribute");
    public static final String SPECIFIC_ATTRIBUTE = urlBuilder(BASE_ATTRIBUTE, "/%d");

    /* Job */
    public static final String BASE_JOB = urlBuilder(BASE_PATH, "/job");
    public static final String SPECIFIC_JOB = urlBuilder(BASE_JOB, "/%d");

    /* Report */
    public static final String BASE_REPORT = urlBuilder(SPECIFIC_JOB, "/report");
    public static final String SPECIFIC_REPORT = urlBuilder(BASE_REPORT, "/%d");

    /* Service */
    public static final String BASE_ASSET_SERVICE = urlBuilder(SPECIFIC_ASSET, "/service");
    public static final String SPECIFIC_ASSET_SERVICE = urlBuilder(BASE_ASSET_SERVICE, "/%d");

    /* Misc. */
    public static final String ASSETS_CHECKED_OUT_BY_USER = urlBuilder(BASE_PATH, "/checked-out");

    /* Codicils */
    public static final String BASE_COC = urlBuilder(SPECIFIC_ASSET, "/coc");

    public static String urlBuilder(final String... parts)
    {
        final StringBuilder sB = new StringBuilder();
        for (final String part : parts)
        {
            sB.append(part);
        }
        return sB.toString();
    }
}
