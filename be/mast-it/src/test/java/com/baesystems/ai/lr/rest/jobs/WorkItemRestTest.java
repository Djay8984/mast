package com.baesystems.ai.lr.rest.jobs;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.util.Assert.notEmpty;
import static org.springframework.util.Assert.notNull;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CreditStatus;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.WorkItemPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class WorkItemRestTest extends BaseRestTest
{
    private static final String BASE_TASK = urlBuilder(BASE_PATH, "/task");
    private static final String QUERY_TASK = urlBuilder(BASE_TASK, "/query");
    private static final String WIP_BASE_TASK = urlBuilder(BASE_PATH, "/wip-task");
    private static final String WIP_QUERY_TASK = urlBuilder(WIP_BASE_TASK, "/query");
    private static final String WIP_TASK_PATH = urlBuilder(BASE_PATH, "/job/%d/wip-task");
    private static final String DELETE_SERVICE_PATH = urlBuilder(BASE_PATH, "/asset/%d/service?serviceIdList=%s");

    private static final String MAST_ASSET_SCHEDULED_SERVICE = "MAST_ASSET_ScheduledService";
    private static final String MAST_ASSET_WORK_ITEM = "MAST_ASSET_WorkItem";

    private static final Long WORK_ITEM_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ITEM_86_ID = 86L;
    private static final Long ITEM_88_ID = 88L;
    private static final Long SURVEY_1_ID = 1L;
    private static final Long SCHEDULED_SERVICE_101_ID = 101L;
    private static final Long SCHEDULED_SERVICE_103_ID = 103L;
    private static final Long TASK_1 = 1L;
    private static final Long JOB_1 = 1L;

    private WorkItemLightListDto taskListDto;

    private WorkItemPage workItemPage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        final WorkItemLightDto task = DataUtils.getWorkItemLight(TASK_1, Boolean.TRUE);
        this.taskListDto = new WorkItemLightListDto();
        this.taskListDto.setTasks(Collections.singletonList(task));
    }

    private void initialiseWorkItemPage(final Long... ids)
    {
        this.workItemPage = new WorkItemPage();

        WorkItemLightDto tempWorkItem;
        for (final Long id : ids)
        {
            try
            {
                tempWorkItem = DataUtils.getWorkItemLight(id, false);
                this.workItemPage.getContent().add(tempWorkItem);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    private void initialiseWIPWorkItemPage(final Long... ids)
    {
        this.workItemPage = new WorkItemPage();

        WorkItemLightDto tempWorkItem;
        for (final Long id : ids)
        {
            try
            {
                tempWorkItem = DataUtils.getWorkItemLight(id, true);
                this.workItemPage.getContent().add(tempWorkItem);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }
        }
    }

    /**
     * Test that a valid GET request for ../task results in a positive response. Expecting work items 1 to 4.
     *
     * @throws IntegrationTestException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    public void getAllTasks() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemPage results = TestUtils.getResponse(BASE_TASK, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    /**
     * Test that a valid GET request for ../wip-task results in a positive response. Expecting work items 1 to 4.
     *
     * @throws IntegrationTestException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    public void getAllWIPTasks() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemPage results = TestUtils.getResponse(WIP_BASE_TASK, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedTasksByItemId() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createQuery(Arrays.asList(ITEM_86_ID), null);

        final WorkItemPage results = TestUtils.postResponse(QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedTasksByItemAndServiceId()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createQuery(Arrays.asList(ITEM_86_ID), Arrays.asList(SCHEDULED_SERVICE_101_ID));

        final WorkItemPage results = TestUtils.postResponse(QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedTasksByServiceId() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            IntegrationTestException
    {
        initialiseWorkItemPage(WORK_ITEM_1_ID);

        final WorkItemQueryDto query = createQuery(null, Arrays.asList(SCHEDULED_SERVICE_101_ID));
        final WorkItemPage results = TestUtils.postResponse(QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedWIPTasksByItemId() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createWipQuery(Arrays.asList(ITEM_88_ID), null);

        final WorkItemPage results = TestUtils.postResponse(WIP_QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedWIPTasksByItemAndServiceId()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createWipQuery(Arrays.asList(ITEM_88_ID), Arrays.asList(SURVEY_1_ID));

        final WorkItemPage results = TestUtils.postResponse(WIP_QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    @Test
    public void getQueriedWIPTasksByServiceId() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException,
            IntegrationTestException
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);

        final WorkItemQueryDto query = createWipQuery(null, Arrays.asList(SURVEY_1_ID));
        final WorkItemPage results = TestUtils.postResponse(WIP_QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        TestUtils.compareFields(results, this.workItemPage, true);
    }

    private WorkItemQueryDto createQuery(final List<Long> itemIds, final List<Long> scheduledServiceIds)
    {
        final WorkItemQueryDto query = new WorkItemQueryDto();
        query.setItemId(itemIds);
        query.setScheduledServiceId(scheduledServiceIds);

        return query;
    }

    private WorkItemQueryDto createWipQuery(final List<Long> itemIds, final List<Long> surveyIds)
    {
        final WorkItemQueryDto query = new WorkItemQueryDto();
        query.setItemId(itemIds);
        query.setSurveyId(surveyIds);

        return query;
    }

    @Test
    public void updateTasks()
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createWipQuery(null, Arrays.asList(SURVEY_1_ID));
        final WorkItemPage originalTasks = TestUtils.postResponse(WIP_QUERY_TASK, query, HttpStatus.OK).jsonPath().getObject("",
                WorkItemPage.class);
        final WorkItemLightDto task = originalTasks.getContent().get(0);

        final LinkResource originalStatus = task.getCreditStatus();

        // Change the crediting status on the task
        task.setCreditStatus(new LinkResource(CreditStatus.CONFIRMED.getValue()));

        final WorkItemLightListDto taskList = new WorkItemLightListDto();
        taskList.setTasks(new ArrayList<WorkItemLightDto>());
        taskList.getTasks().addAll(originalTasks.getContent());

        final WorkItemLightListDto updatedTasks = TestUtils.putResponse(String.format(WIP_TASK_PATH, JOB_1), taskList, HttpStatus.OK, WorkItemLightListDto.class);

        notNull(updatedTasks);
        notNull(updatedTasks.getTasks());
        notEmpty(updatedTasks.getTasks());
        assertEquals(new LinkResource(CreditStatus.CONFIRMED.getValue()), updatedTasks.getTasks().get(0).getCreditStatus());

        // Revert the crediting status change
        updatedTasks.getTasks().get(0).setCreditStatus(originalStatus);
        TestUtils.putResponse(String.format(WIP_TASK_PATH, JOB_1), updatedTasks, HttpStatus.OK, WorkItemLightListDto.class);
    }

    @Test
    public void updateWIPTasksFailsWhenOldTokenIsUsed() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseWIPWorkItemPage(WORK_ITEM_1_ID);
        final WorkItemQueryDto query = createWipQuery(Arrays.asList(ITEM_88_ID), null);
        final WorkItemPage originalTasks = TestUtils.postResponse(WIP_QUERY_TASK, query, HttpStatus.OK).jsonPath().getObject("",
                WorkItemPage.class);
        final WorkItemLightDto task = originalTasks.getContent().get(0);

        final String originalStalenessHash = task.getStalenessHash();

        final WorkItemLightListDto taskList = new WorkItemLightListDto();
        taskList.setTasks(new ArrayList<WorkItemLightDto>());
        taskList.getTasks().addAll(originalTasks.getContent());

        // Make a small change
        task.setDescription(task.getDescription() + "X");
        final WorkItemLightListDto updatedTasks = TestUtils.putResponse(format(WIP_TASK_PATH, JOB_1), taskList, HttpStatus.OK).jsonPath().getObject("", WorkItemLightListDto.class);

        // Make another small change, but use the hash from the original GET
        updatedTasks.getTasks().get(0).setDescription(task.getDescription() + "X");
        updatedTasks.getTasks().get(0).setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(WIP_TASK_PATH, JOB_1), updatedTasks, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.TASK.getTypeString()), error.getMessage());
    }

    @Test
    public void deleteScheduledServiceDeletesTasks() throws DatabaseUnitException
    {
        // Get the Tasks associated with Service 103 and ensure something is returned
        final WorkItemQueryDto query = createQuery(null, Arrays.asList(SCHEDULED_SERVICE_103_ID));
        final WorkItemPage results = TestUtils.postResponse(QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        assertTrue(!results.getContent().isEmpty());

        final List<Long> taskIds = new ArrayList<Long>();
        for (final WorkItemLightDto task : results.getContent())
        {
            taskIds.add(task.getId());
        }

        // (Soft) Delete Service 103
        TestUtils.deleteResponse(format(DELETE_SERVICE_PATH, ASSET_1_ID, SCHEDULED_SERVICE_103_ID.toString().replaceAll("[\\[\\] ]", "")),
                HttpStatus.OK);

        // Confirm that the Service is (soft) deleted and that all associated Tasks are also (soft) deleted
        assertTrue(DataUtils.isDeleted(MAST_ASSET_SCHEDULED_SERVICE, SCHEDULED_SERVICE_103_ID));

        for (final Long taskId : taskIds)
        {
            assertTrue(DataUtils.isDeleted(MAST_ASSET_WORK_ITEM, taskId));
        }

        // Double check that no Tasks are returned when querying against this Service
        final WorkItemPage resultsFollowingDelete = TestUtils.postResponse(QUERY_TASK, query, HttpStatus.OK,
                WorkItemPage.class);

        assertTrue(resultsFollowingDelete.getContent().isEmpty());
    }
}
