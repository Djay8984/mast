package com.baesystems.ai.lr.utils;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.NamedLinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.BuilderDto;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.customers.OrganisationDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.DefectItemLinkDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyLightDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetTypeDto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment2Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipment4Dto;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.CertificateActionDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.CodicilTypeDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectDetailDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductGroupExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.ReferenceDataType;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

public final class DataUtils
{
    private static IDataSet mastDataSet;
    private static IDataSet ihsDataSet;
    private static boolean inserted;

    private static final Long NUMERIC_TRUE = 1L;

    private DataUtils()
    {
        // Prevents instantiation
    }

    public static void loadTestData()
            throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException, InstantiationException, IllegalAccessException
    {
        if (!inserted)
        {
            mastDataSet = DatabaseUtils.getDataSet("mast");
            ihsDataSet = DatabaseUtils.getDataSet("ihs");
            inserted = true;
        }
    }

    public static ReportDto getReport(final Long targetId) throws DatabaseUnitException
    {
        final ReportDto report = new ReportDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_Report");
        final int rowNum = getRowNum(table, targetId);

        report.setId(((Number) table.getValue(rowNum, "id")).longValue());

        report.setReportVersion(checkLongNotNull((Number) table.getValue(rowNum, "report_version")));

        report.setReportType(makeLinkResource(table, rowNum, "type_id"));
        report.setJob(makeLinkResource(table, rowNum, "job_id"));
        // report.setCertificateSnapshot((String) table.getValue(rowNum, "certificate_snapshot"));

        return report;
    }

    public static ReportLightDto getReportVersion(final Long targetId) throws DatabaseUnitException
    {
        final ReportLightDto reportVersion = new ReportLightDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_Report");
        final int rowNum = getRowNum(table, targetId);

        reportVersion.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        reportVersion.setReportVersion(checkLongNotNull((Number) table.getValue(rowNum, "report_version")));

        return reportVersion;
    }

    public static SupplementaryInformationDto getAttachment(final Long targetId) throws DatabaseUnitException
    {
        final SupplementaryInformationDto attachment = new SupplementaryInformationDto();
        final ITable table = mastDataSet.getTable("MAST_XXX_SupplementaryInformation");
        final int rowNum = getRowNum(table, targetId);

        attachment.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        attachment.setTitle((String) table.getValue(rowNum, "title"));
        attachment.setNote((String) table.getValue(rowNum, "note"));
        attachment.setAttachmentUrl((String) table.getValue(rowNum, "attachment_url"));
        attachment.setAssetLocationViewpoint((String) table.getValue(rowNum, "asset_location_viewpoint"));
        attachment.setAttachmentType(makeLinkResource(table, rowNum, "attachment_type_id"));
        attachment.setConfidentialityType(makeLinkResource(table, rowNum, "confidentiality_type_id"));

        return attachment;
    }

    public static PartyDto getCustomer(final Long targetId) throws DatabaseUnitException
    {
        final PartyDto customer = new PartyDto();

        final ITable table = mastDataSet.getTable("MAST_CDH_Party");
        final int rowNum = getRowNum(table, targetId);

        customer.setId(((Number) table.getValue(rowNum, "id")).longValue());
        customer.setName((String) table.getValue(rowNum, "name"));

        return customer;
    }

    public static OrganisationDto getOrganisation(final Long targetId) throws DatabaseUnitException
    {
        final OrganisationDto customer = new OrganisationDto();

        final ITable orgTable = mastDataSet.getTable("MAST_CDH_Organisation");
        final ITable table = mastDataSet.getTable("MAST_CDH_Party");
        final int rowNum = getRowNum(table, targetId);

        customer.setId(((Number) table.getValue(rowNum, "id")).longValue());
        customer.setName((String) table.getValue(rowNum, "name"));

        final int orgRowNum = getRowNum(orgTable, customer.getId(), "party_id");
        customer.setApprovalStatus((Boolean) orgTable.getValue(orgRowNum, "lr_approved_flag"));

        return customer;
    }

    public static PartyLightDto getParty(final Long targetId) throws DatabaseUnitException
    {
        final PartyLightDto customer = new PartyLightDto();

        final ITable table = mastDataSet.getTable("MAST_CDH_Party");
        final int rowNum = getRowNum(table, targetId);

        customer.setId(((Number) table.getValue(rowNum, "id")).longValue());
        customer.setName((String) table.getValue(rowNum, "name"));
        return customer;
    }

    public static BuilderDto getBuilder(final Long targetId) throws DatabaseUnitException
    {
        final BuilderDto builder = new BuilderDto();

        final ITable orgTable = mastDataSet.getTable("MAST_CDH_Organisation");
        final ITable table = mastDataSet.getTable("MAST_CDH_Party");
        final int rowNum = getRowNum(table, targetId);

        builder.setId(((Number) table.getValue(rowNum, "id")).longValue());
        builder.setName((String) table.getValue(rowNum, "name"));

        final int orgRowNum = getRowNum(orgTable, builder.getId(), "party_id");
        builder.setApprovalStatus((Boolean) orgTable.getValue(orgRowNum, "lr_approved_flag"));

        return builder;
    }

    public static FlagStateDto getFlagState(final Long targetId) throws DatabaseUnitException
    {
        final FlagStateDto flagState = new FlagStateDto();

        final ITable table = mastDataSet.getTable("MAST_REF_FlagAdministration");
        final int rowNum = getRowNum(table, targetId);

        flagState.setId(((Number) table.getValue(rowNum, "id")).longValue());
        flagState.setName((String) table.getValue(rowNum, "name"));
        flagState.setFlagCode((String) table.getValue(rowNum, "flag_code"));

        return flagState;
    }

    public static PortOfRegistryDto getPortOfRegistry(final Long targetId) throws DatabaseUnitException
    {
        final PortOfRegistryDto portOfRegistry = new PortOfRegistryDto();

        final ITable table = mastDataSet.getTable("MAST_REF_Port");
        final int rowNum = getRowNum(table, targetId);

        portOfRegistry.setId(((Number) table.getValue(rowNum, "id")).longValue());
        portOfRegistry.setName((String) table.getValue(rowNum, "name"));

        return portOfRegistry;
    }

    // Flag Port Administration

    public static IhsAssetDetailsDto getIhsAsset(final Long targetId) throws DatabaseUnitException
    {
        final IhsAssetDto ihsAsset = new IhsAssetDto();
        final IhsGroupFleetDto ihsGroupFleet = new IhsGroupFleetDto();
        final IhsAssetDetailsDto ihsAssetDetails = new IhsAssetDetailsDto();
        final IhsHistDto ihsHist = new IhsHistDto();

        final ITable table = ihsDataSet.getTable("ABSD_SHIP_SEARCH");
        final ITable grpFleetTable = ihsDataSet.getTable("ABSD_GROUP_FLEET");
        final ITable histTable = ihsDataSet.getTable("ABSD_HIST");

        final int rowNum = getRowNum(table, targetId, "LRNO");
        final int grpFleetrowNum = getRowNum(grpFleetTable, targetId, "LRNO");
        final int histRowNum = getRowNum(histTable, targetId, "LRNO");

        ihsAsset.setId(new Long(table.getValue(rowNum, "LRNO").toString()));
        ihsAsset.setYardNumber((String) table.getValue(rowNum, "YARDNO"));
        ihsAsset.setBuilder((String) table.getValue(rowNum, "SHIPBUILDER"));
        ihsAsset.setBuilderCode((String) table.getValue(rowNum, "BUILDERCODE"));
        ihsAsset.setName((String) table.getValue(rowNum, "VESSELNAME"));
        ihsAsset.setDateOfBuild((String) table.getValue(rowNum, "DATEOFBUILD"));
        ihsAsset.setPortName((String) table.getValue(rowNum, "PORTNAME"));
        ihsAsset.setShipManager((String) table.getValue(rowNum, "SHIPMANAGER"));
        ihsAsset.setOfficialNo((String) table.getValue(rowNum, "OFFICIALNO"));
        ihsAsset.setOperator((String) table.getValue(rowNum, "OPERATOR"));
        ihsAsset.setCallSign((String) table.getValue(rowNum, "CALLSIGN"));
        ihsAsset.setDocCompany((String) table.getValue(rowNum, "DOCCOMPANY"));
        ihsAsset.setFlag((String) table.getValue(rowNum, "FLAG"));
        ihsAsset.setOperatorCode((String) table.getValue(rowNum, "OPERATORCODE"));
        ihsAsset.setShipManagerCode((String) table.getValue(rowNum, "SHIPMANAGERCODE"));
        ihsAsset.setTechManagerCode((String) table.getValue(rowNum, "TECHMANCODE"));
        ihsAsset.setTechManager((String) table.getValue(rowNum, "TECHMANAGER"));
        ihsAsset.setDocCode((String) table.getValue(rowNum, "DOCCODE"));
        ihsAsset.setCountryOfBuild((String) table.getValue(rowNum, "COB"));

        ihsAsset.setId(new Long(grpFleetTable.getValue(grpFleetrowNum, "LRNO").toString()));
        ihsGroupFleet.setGroupOwner((String) grpFleetTable.getValue(grpFleetrowNum, "GROUP_OWNER"));
        ihsGroupFleet.setRegdOwner((String) grpFleetTable.getValue(grpFleetrowNum, "REGD_OWNER"));

        ihsHist.setId(new Long(histTable.getValue(histRowNum, "LRNO").toString()));
        ihsHist.setKeelLayingDate((String) histTable.getValue(histRowNum, "A02_EFD"));

        ihsAssetDetails.setIhsAsset(ihsAsset);
        ihsAssetDetails.setIhsGroupFleet(ihsGroupFleet);
        ihsAssetDetails.setIhsHist(ihsHist);

        return ihsAssetDetails;
    }

    public static AssetDto getAsset(final Long targetId) throws DatabaseUnitException
    {
        final AssetDto asset = new AssetDto();

        populateAsset(targetId, asset);

        return asset;
    }

    private static <T extends AssetLightDto> void populateAsset(final Long targetId, final T asset) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset");
        final int rowNum = getRowNum(table, targetId);
        final Number publishedVersion = (Number) table.getValue(rowNum, "published_version_id");
        populateAsset(targetId, publishedVersion.longValue(), asset);
    }

    private static <T extends AssetLightDto> void populateAsset(final Long targetId, final Long versionId, final T asset) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_VersionedAsset");
        final int rowNum = getRowNum(table, targetId, "id", versionId, "asset_version_id");

        asset.setId(((Number) table.getValue(rowNum, "id")).longValue());
        asset.setAssetType(makeLinkResource(table, rowNum, "asset_type_id"));
        asset.setClassStatus(makeLinkResource(table, rowNum, "class_status_id"));
        asset.setClassDepartment(makeLinkResource(table, rowNum, "class_department_id"));
        asset.setLinkedAsset(makeLinkResource(table, rowNum, "linked_asset_id"));
        asset.setBuildDate((Date) table.getValue(rowNum, "build_date"));
        asset.setEffectiveDate((Date) table.getValue(rowNum, "effective_date"));
        asset.setGrossTonnage((Double) table.getValue(rowNum, "gross_tonnage"));
        asset.setDeadWeight((Double) table.getValue(rowNum, "dead_weight"));
        asset.setBuilder((String) table.getValue(rowNum, "builder"));
        asset.setName((String) table.getValue(rowNum, "name"));
        asset.setClassNotation((String) table.getValue(rowNum, "hull_class_notation"));
        asset.setIsLead((Boolean) table.getValue(rowNum, "lead_asset_flag"));
        asset.setAssetCategory(makeLinkResource(table, rowNum, "asset_category_id"));
        asset.setIhsAsset(makeLinkResource(table, rowNum, "imo_number"));
        asset.setRuleSet(makeLinkResource(table, rowNum, "society_ruleset_id"));
        asset.setPreviousRuleSet(makeLinkResource(table, rowNum, "previous_society_ruleset_id"));
        asset.setHarmonisationDate((Date) table.getValue(rowNum, "harmonisation_date"));
        asset.setCoClassificationSociety(makeLinkResource(table, rowNum, "co_classification_society_id"));
        final Number imo = (Number) table.getValue(rowNum, "lead_imo");
        asset.setLeadImo(imo == null ? null : imo.longValue());
        asset.setKeelLayingDate((Date) table.getValue(rowNum, "keel_laying_date"));
        asset.setYardNumber((String) table.getValue(rowNum, "primary_yard_number"));
        asset.setRegisteredPort(makeLinkResource(table, rowNum, "registered_port_id"));
        asset.setAssetLifecycleStatus(makeLinkResource(table, rowNum, "asset_lifecycle_status_id"));
        asset.setHullIndicator(((Number) table.getValue(rowNum, "hull_indicator")).intValue());
        asset.setProductRuleSet(makeLinkResource(table, rowNum, "product_ruleset_id"));
    }

    public static CaseWithAssetDetailsDto getCaseWithLinks(final Long targetId) throws DatabaseUnitException
    {
        final CaseWithAssetDetailsDto caseDto = new CaseWithAssetDetailsDto();

        final ITable table = mastDataSet.getTable("MAST_CASE_Case");
        final int rowNum = getRowNum(table, targetId);

        caseDto.setId(((Number) table.getValue(rowNum, "id")).longValue());
        caseDto.setAsset(makeLinkResource(table, rowNum, "asset_id"));
        caseDto.setCaseType(makeLinkResource(table, rowNum, "case_type_id"));
        caseDto.setCaseStatus(makeLinkResource(table, rowNum, "case_status_id"));
        caseDto.setPreEicInspectionStatus(makeLinkResource(table, rowNum, "pre_eic_inspection_status_id"));
        caseDto.setRiskAssessmentStatus(makeLinkResource(table, rowNum, "risk_assessment_status_id"));
        caseDto.setTocAcceptanceDate((Date) table.getValue(rowNum, "toc_acceptance_date"));
        caseDto.setFlagState(makeLinkResource(table, rowNum, "flag_state_id"));
        return caseDto;
    }

    public static CaseDto getBasicCaseWithLinks(final Long targetId) throws DatabaseUnitException
    {
        final CaseDto caseDto = new CaseDto();

        final ITable table = mastDataSet.getTable("MAST_CASE_Case");
        final int rowNum = getRowNum(table, targetId);
        caseDto.setId(((Number) table.getValue(rowNum, "id")).longValue());
        caseDto.setAsset(makeLinkResource(table, rowNum, "asset_id"));
        caseDto.setCaseType(makeLinkResource(table, rowNum, "case_type_id"));
        caseDto.setCaseStatus(makeLinkResource(table, rowNum, "case_status_id"));
        caseDto.setPreEicInspectionStatus(makeLinkResource(table, rowNum, "pre_eic_inspection_status_id"));
        caseDto.setRiskAssessmentStatus(makeLinkResource(table, rowNum, "risk_assessment_status_id"));
        caseDto.setTocAcceptanceDate((Date) table.getValue(rowNum, "toc_acceptance_date"));
        return caseDto;
    }

    public static LazyItemDto getLazyItem(final Long targetId, final Boolean draft) throws DatabaseUnitException
    {
        final LazyItemDto item = new LazyItemDto();
        final ITable table = mastDataSet.getTable(draft ? "MAST_ASSET_DraftAssetItem" : "MAST_ASSET_VersionedAssetItem");
        final int rowNum = getRowNum(table, targetId);

        item.setId(((Number) table.getValue(rowNum, "id")).longValue());
        item.setName((String) table.getValue(rowNum, "name"));
        item.setItemType(makeLinkResource(table, rowNum, "item_type_id"));
        item.setReviewed((Boolean) table.getValue(rowNum, "reviewed"));
        item.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));
        item.setDecommissioned((Boolean) table.getValue(rowNum, "decommissioned"));

        return item;
    }

    public static ItemLightDto getItemLight(final Long targetId) throws DatabaseUnitException
    {


        final ITable table = mastDataSet.getTable("MAST_ASSET_VersionedAssetItem");
        final int rowNum = getRowNum(table, targetId);

        return populateItemLightDto(table, rowNum);

    }

    public static ItemLightDto getItemLight(final Long targetId, final Long versionId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_VersionedAssetItem");
        final int rowNum = getRowNum(table, targetId, versionId);

        return populateItemLightDto(table,  rowNum);
    }

    private static ItemLightDto populateItemLightDto(final ITable table, final int rowNum) throws DataSetException
    {
        final ItemLightDto item = new ItemLightDto();

        item.setId(((Number) table.getValue(rowNum, "id")).longValue());
        // asset_id, parent_item_id
        item.setName((String) table.getValue(rowNum, "name"));
        item.setItemType(makeLinkResource(table, rowNum, "item_type_id"));
        item.setReviewed((Boolean) table.getValue(rowNum, "reviewed"));
        item.setDecommissioned((Boolean) table.getValue(rowNum, "decommissioned"));

        final Number displayOrder = (Number) table.getValue(rowNum, "display_order");
        item.setDisplayOrder(displayOrder != null ? displayOrder.intValue() : null);

        return item;
    }

    public static ReferenceDataDto getItemRelationship(final Long targetId) throws DatabaseUnitException
    {
        final ReferenceDataDto itemRelationship = new ReferenceDataDto();

        final ITable table = mastDataSet.getTable("MAST_ASSET_AssetItemRelationship");
        final int rowNum = getRowNum(table, targetId);

        itemRelationship.setId(((Number) table.getValue(rowNum, "id")).longValue());

        return itemRelationship;
    }

    public static AttributeDto getAttribute(final Long targetId, final String tableName) throws DatabaseUnitException
    {
        final AttributeDto attribute = new AttributeDto();

        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, targetId);

        attribute.setId(((Number) table.getValue(rowNum, "id")).longValue());
        attribute.setAttributeType(makeLinkResource(table, rowNum, "attribute_type_id"));
        attribute.setValue((String) table.getValue(rowNum, "value"));

        return attribute;
    }

    public static AttributeDto getAttribute(final Long targetId) throws DatabaseUnitException
    {
        return getAttribute(targetId, "MAST_ASSET_AssetItemAttribute");
    }

    public static AttributeDto getDraftAttribute(final Long targetId) throws DatabaseUnitException
    {
        return getAttribute(targetId, "MAST_ASSET_DraftAssetAttribute");
    }

    public static List<AttributeDto> getAllAttributes() throws DatabaseUnitException
    {
        final List<AttributeDto> attributeDtoList = new ArrayList<>();
        final ITable table = mastDataSet.getTable("MAST_ASSET_AssetItemAttribute");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final AttributeDto attribute = new AttributeDto();
            attribute.setId(((Number) table.getValue(counter, "id")).longValue());
            attribute.setAttributeType(makeLinkResource(table, counter, "attribute_type_id"));
            attribute.setValue((String) table.getValue(counter, "value"));
            attributeDtoList.add(attribute);
        }
        return attributeDtoList;
    }

    public static List<AttributeDto> getAttributesForVersionedItem(final Long itemId, final Long versionId)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final String query = "select * from MAST_ASSET_AssetItemAttribute WHERE"
                + " item_id = " + itemId + " AND asset_version_id = " + versionId;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        final List<AttributeDto> attributes = new ArrayList<>();
        while (resultSet.next())
        {
            final AttributeDto attribute = new AttributeDto();
            attribute.setId(resultSet.getLong("id"));
            attribute.setItem(new LinkResource(resultSet.getLong("item_id")));
            attribute.setAttributeType(new LinkResource(resultSet.getLong("attribute_type_id")));
            attribute.setValue(resultSet.getString("value"));
            attributes.add(attribute);
        }
        return attributes;
    }

    public static List<ReferenceDataDto> getAttributeDataTypes() throws DatabaseUnitException
    {
        final List<ReferenceDataDto> attributeDataTypeList = new ArrayList<>();
        final ITable table = mastDataSet.getTable("MAST_REF_AttributeDataType");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final ReferenceDataDto refData = new ReferenceDataDto();
            refData.setId(((Number) table.getValue(counter, "id")).longValue());
            refData.setDescription((String) table.getValue(counter, "description"));
            attributeDataTypeList.add(refData);
        }
        return attributeDataTypeList;
    }

    public static LrEmployeeDto getLrEmployee(final Long targetId) throws DatabaseUnitException
    {
        final LrEmployeeDto lrEmployee = new LrEmployeeDto();

        final ITable table = mastDataSet.getTable("MAST_LRP_Employee");
        final int rowNum = getRowNum(table, targetId);

        lrEmployee.setId(((Number) table.getValue(rowNum, "id")).longValue());
        lrEmployee.setOneWorldNumber((String) table.getValue(rowNum, "oneworld_number"));
        lrEmployee.setFirstName((String) table.getValue(rowNum, "first_name"));
        lrEmployee.setLastName((String) table.getValue(rowNum, "last_name"));
        lrEmployee.setEmailAddress((String) table.getValue(rowNum, "email_address"));

        return lrEmployee;
    }

    public static OfficeLinkDto getJobOffice(final Long targetId) throws DatabaseUnitException
    {
        final OfficeLinkDto office = new OfficeLinkDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_JobOffice");
        final int rowNum = getRowNum(table, targetId);

        office.setId(((Number) table.getValue(rowNum, "id")).longValue());
        office.setOffice(makeLinkResource(table, rowNum, "office_id"));
        office.setOfficeRole(makeLinkResource(table, rowNum, "office_role_id"));

        return office;
    }

    public static EmployeeLinkDto getJobResource(final Long targetId) throws DatabaseUnitException
    {
        final EmployeeLinkDto employee = new EmployeeLinkDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_JobResource");
        final int rowNum = getRowNum(table, targetId);

        employee.setId(((Number) table.getValue(rowNum, "id")).longValue());
        employee.setLrEmployee(makeLrEmployee(table, rowNum, "lr_employee_id"));
        employee.setEmployeeRole(makeLinkResource(table, rowNum, "employee_role_id"));

        return employee;
    }

    public static OfficeDto getOffice(final Long targetId) throws DatabaseUnitException
    {
        final OfficeDto office = new OfficeDto();

        final ITable table = mastDataSet.getTable("MAST_REF_Office");
        final int rowNum = getRowNum(table, targetId);

        office.setId(((Number) table.getValue(rowNum, "id")).longValue());
        office.setName((String) table.getValue(rowNum, "name"));

        return office;
    }

    public static SocietyRulesetDto getSocietyRuleset(final Long targetId) throws DatabaseUnitException
    {
        final SocietyRulesetDto societyRuleset = new SocietyRulesetDto();

        final ITable table = mastDataSet.getTable("MAST_REF_Ruleset2");
        final int rowNum = getRowNum(table, targetId);

        societyRuleset.setId(((Number) table.getValue(rowNum, "id")).longValue());
        societyRuleset.setName((String) table.getValue(rowNum, "name"));
        societyRuleset.setCategory((String) table.getValue(rowNum, "category"));
        societyRuleset.setIsLrRuleset((Boolean) table.getValue(rowNum, "is_lr_ruleset"));

        return societyRuleset;
    }

    public static SurveyorDto getSurveyor(final Long targetId) throws DatabaseUnitException
    {
        final SurveyorDto surveyor = new SurveyorDto();

        final ITable table = mastDataSet.getTable("MAST_LRP_Employee");
        final int rowNum = getRowNum(table, targetId);

        surveyor.setId(((Number) table.getValue(rowNum, "id")).longValue());

        return surveyor;
    }

    public static CustomerLinkDto getAssetCustomer(final Long targetId) throws DatabaseUnitException
    {
        final CustomerLinkDto assetCustomer = new CustomerLinkDto();

        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset_Party");
        final int rowNum = getRowNum(table, targetId);

        assetCustomer.setId(((Number) table.getValue(rowNum, "id")).longValue());
        assetCustomer.setCustomer(new PartyDto());
        assetCustomer.getCustomer().setId(((Number) table.getValue(rowNum, "party_id")).longValue());
        assetCustomer.setRelationship(makeLinkResource(table, rowNum, "party_role_id"));

        return assetCustomer;
    }

    public static CustomerHasFunctionDto getCustomerHasFunction(final Long targetId) throws DatabaseUnitException
    {
        final CustomerHasFunctionDto customerHasFunction = new CustomerHasFunctionDto();

        final ITable table = mastDataSet.getTable("MAST_ASSET_Party_PartyRole");
        final int rowNum = getRowNum(table, targetId);

        customerHasFunction.setId(((Number) table.getValue(rowNum, "id")).longValue());
        customerHasFunction.setCustomerFunction(makeLinkResource(table, rowNum, "customer_function_type_id"));

        return customerHasFunction;
    }

    public static ItemTypeDto getItemType(final Long targetId) throws DatabaseUnitException
    {
        final ItemTypeDto itemType = new ItemTypeDto();

        final ITable table = mastDataSet.getTable("MAST_REF_AssetItemType");
        final int rowNum = getRowNum(table, targetId);

        itemType.setId(((Number) table.getValue(rowNum, "id")).longValue());
        itemType.setName((String) table.getValue(rowNum, "name"));
        itemType.setDescription((String) table.getValue(rowNum, "description"));
        itemType.setStartDate((Date) table.getValue(rowNum, "start_date"));
        itemType.setEndDate((Date) table.getValue(rowNum, "end_date"));
        itemType.setSection((String) table.getValue(rowNum, "section"));
        itemType.setDisplayOrder(checkLongNotNull((Number) table.getValue(rowNum, "display_order")));

        return itemType;
    }

    public static CaseStatusDto getCaseStatus(final Long targetId) throws DatabaseUnitException
    {
        final CaseStatusDto caseStatus = new CaseStatusDto();

        final ITable table = mastDataSet.getTable("MAST_REF_CaseStatus");
        final int rowNum = getRowNum(table, targetId);

        caseStatus.setId(((Number) table.getValue(rowNum, "id")).longValue());
        caseStatus.setName((String) table.getValue(rowNum, "name"));
        caseStatus.setCanSaveWithErrors((Boolean) table.getValue(rowNum, "can_save_with_errors"));

        return caseStatus;
    }

    public static IhsAssetTypeDto getIhsAssetType(final Long targetId) throws DatabaseUnitException
    {
        final IhsAssetTypeDto ihsAssetType = new IhsAssetTypeDto();

        final ITable table = ihsDataSet.getTable("mast_ref_ihs_asset_type");
        final int rowNum = getRowNum(table, targetId);

        ihsAssetType.setStat5Code((String) table.getValue(rowNum, "STAT5CODE"));
        ihsAssetType.setStatDeCode((String) table.getValue(rowNum, "STATDECODE"));

        return ihsAssetType;
    }

    public static AssetCategoryDto getAssetCategory(final Long targetId) throws DatabaseUnitException
    {
        final AssetCategoryDto assetCategory = new AssetCategoryDto();

        final ITable table = mastDataSet.getTable("MAST_REF_AssetCategory");
        final int rowNum = getRowNum(table, targetId);

        assetCategory.setId(((Number) table.getValue(rowNum, "id")).longValue());
        assetCategory.setName((String) table.getValue(rowNum, "name"));
        assetCategory.setBusinessProcess(makeLinkResource(table, rowNum, "business_process_id"));

        return assetCategory;
    }

    public static ProductDto getProduct(final Long targetId) throws DatabaseUnitException
    {
        final ProductDto product = new ProductDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ProductCatalogue");
        final int rowNum = getRowNum(table, targetId);

        product.setProductCatalogueId(((Number) table.getValue(rowNum, "id")).longValue());
        product.setDescription((String) table.getValue(rowNum, "description"));
        product.setName((String) table.getValue(rowNum, "name"));
        product.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));

        return product;
    }

    public static ProductCatalogueDto getProductCatalogue(final Long targetId) throws DatabaseUnitException
    {
        final ProductCatalogueDto productCatalogue = new ProductCatalogueDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ProductCatalogue");
        final int rowNum = getRowNum(table, targetId);

        productCatalogue.setId(((Number) table.getValue(rowNum, "id")).longValue());
        productCatalogue.setDescription((String) table.getValue(rowNum, "description"));
        productCatalogue.setName((String) table.getValue(rowNum, "name"));
        productCatalogue.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));
        productCatalogue.setProductGroup(makeLinkResource(table, rowNum, "product_group_id"));

        return productCatalogue;
    }

    public static ProductCatalogueExtendedDto getExtendedProductCatalogue(final Long targetId) throws DatabaseUnitException
    {
        final ProductCatalogueExtendedDto extendedProductCatalogue = new ProductCatalogueExtendedDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ProductCatalogue");
        final int rowNum = getRowNum(table, targetId);

        extendedProductCatalogue.setId(((Number) table.getValue(rowNum, "id")).longValue());
        extendedProductCatalogue.setName((String) table.getValue(rowNum, "name"));
        extendedProductCatalogue.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));

        return extendedProductCatalogue;
    }

    public static ProductTypeExtendedDto getExtendedProductType(final Long targetId) throws DatabaseUnitException
    {
        final ProductTypeExtendedDto extendedProductType = new ProductTypeExtendedDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ProductFamily");
        final int rowNum = getRowNum(table, targetId);

        extendedProductType.setId(((Number) table.getValue(rowNum, "id")).longValue());
        extendedProductType.setName((String) table.getValue(rowNum, "name"));

        return extendedProductType;
    }

    public static ProductGroupExtendedDto getExtendedProductGroup(final Long targetId) throws DatabaseUnitException
    {
        final ProductGroupExtendedDto extendedProductGroup = new ProductGroupExtendedDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ProductGroup");
        final int rowNum = getRowNum(table, targetId);

        extendedProductGroup.setId(((Number) table.getValue(rowNum, "id")).longValue());
        extendedProductGroup.setName((String) table.getValue(rowNum, "name"));
        extendedProductGroup.setProductType(makeLinkResource(table, rowNum, "product_family_id"));

        return extendedProductGroup;
    }

    public static ScheduledServiceDto getServiceFromCatalogue(final Long targetId) throws DatabaseUnitException
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ServiceCatalogue");
        final int rowNum = getRowNum(table, targetId);

        service.setServiceCatalogueId(((Number) table.getValue(rowNum, "id")).longValue());

        return service;
    }

    public static CoCDto getCoC(final Long targetId, final Boolean wip) throws DatabaseUnitException
    {
        final CoCDto coCDto = new CoCDto();

        final ITable table = mastDataSet.getTable(wip ? "MAST_JOB_CodicilWIP" : "MAST_ASSET_Codicil");
        final int rowNum = getRowNum(table, targetId);

        coCDto.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        coCDto.setAssetItem(makeLinkResourceNotBigInteger(table, rowNum, "asset_item_id"));
        coCDto.setCategory(makeLinkResource(table, rowNum, "category_id"));
        coCDto.setConfidentialityType(makeLinkResourceNotBigInteger(table, rowNum, "confidentiality_type_id"));
        coCDto.setDescription((String) table.getValue(rowNum, "narrative"));
        coCDto.setRequireApproval((Boolean) table.getValue(rowNum, "require_approval"));
        coCDto.setStatus(makeLinkResource(table, rowNum, "status_id"));
        coCDto.setSurveyorGuidance((String) table.getValue(rowNum, "surveyor_guidance"));
        coCDto.setTemplate(makeLinkResource(table, rowNum, "template_id"));
        coCDto.setTitle((String) table.getValue(rowNum, "title"));
        coCDto.setDueDate(new Date(((Date) table.getValue(rowNum, "due_date")).getTime()));
        coCDto.setDefect(makeLinkResource(table, rowNum, "defect_id"));
        coCDto.setInheritedFlag((Boolean) table.getValue(rowNum, "inherited_flag"));
        coCDto.setActionTaken((String) table.getValue(rowNum, "action_taken"));
        coCDto.setJob(makeLinkResource(table, rowNum, "job_id"));
        coCDto.setAsset(makeLinkResource(table, rowNum, "asset_id"));
        coCDto.setImposedDate(new Date(((Date) table.getValue(rowNum, "imposed_date")).getTime()));
        if (wip)
        {
            coCDto.setParent(makeLinkResource(table, rowNum, "parent_id"));
        }

        final Integer employeeId = ((Integer) table.getValue(rowNum, "employee_id"));
        if (employeeId != null)
        {
            final LrEmployeeDto employeeDto = new LrEmployeeDto();
            employeeDto.setId(employeeId.longValue());
            coCDto.setEmployee(employeeDto);
        }

        if (wip)
        {
            coCDto.setJobScopeConfirmed((Boolean) table.getValue(rowNum, "job_scope_confirmed"));
            coCDto.setParent(makeLinkResource(table, rowNum, "parent_id"));
        }

        return coCDto;
    }

    public static AssetNoteDto getAssetNote(final Long targetId, final Boolean wip) throws DatabaseUnitException
    {
        final AssetNoteDto assetNoteDto = new AssetNoteDto();

        final ITable table = mastDataSet.getTable(wip ? "MAST_JOB_CodicilWIP" : "MAST_ASSET_Codicil");
        final int rowNum = getRowNum(table, targetId);
        assetNoteDto.setId(((Number) table.getValue(rowNum, "id")).longValue());
        assetNoteDto.setDueDate((Date) table.getValue(rowNum, "due_date"));
        assetNoteDto.setImposedDate((Date) table.getValue(rowNum, "imposed_date"));
        assetNoteDto.setDescription((String) table.getValue(rowNum, "narrative"));
        assetNoteDto.setTemplate(makeLinkResource(table, rowNum, "template_id"));
        assetNoteDto.setTitle((String) table.getValue(rowNum, "title"));
        assetNoteDto.setCategory(makeLinkResource(table, rowNum, "category_id"));
        assetNoteDto.setSurveyorGuidance((String) table.getValue(rowNum, "surveyor_guidance"));

        if (wip)
        {
            assetNoteDto.setJobScopeConfirmed((Boolean) table.getValue(rowNum, "job_scope_confirmed"));
        }

        return assetNoteDto;
    }

    public static ActionableItemDto getActionableItem(final Long actionableItemId, final Boolean wip) throws DatabaseUnitException
    {
        final ActionableItemDto actionableItemDto = new ActionableItemDto();
        final ITable table = mastDataSet.getTable(wip ? "MAST_JOB_CodicilWIP" : "MAST_ASSET_Codicil");
        final int rowNum = getRowNum(table, actionableItemId);
        actionableItemDto.setId(((Integer) table.getValue(rowNum, "id")).longValue());

        final Integer itemId = ((Integer) table.getValue(rowNum, "asset_item_id"));
        if (itemId != null)
        {
            // Until versioning story
            actionableItemDto.setAssetItem(new ItemLightDto());
            actionableItemDto.getAssetItem().setId(itemId.longValue());
        }

        actionableItemDto.setCategory(makeLinkResource(table, rowNum, "category_id"));
        actionableItemDto.setConfidentialityType(makeLinkResourceNotBigInteger(table, rowNum, "confidentiality_type_id"));
        actionableItemDto.setDescription((String) table.getValue(rowNum, "narrative"));
        actionableItemDto.setRequireApproval((Boolean) table.getValue(rowNum, "require_approval"));
        actionableItemDto.setStatus(makeLinkResourceNotBigInteger(table, rowNum, "status_id"));
        actionableItemDto.setSurveyorGuidance((String) table.getValue(rowNum, "surveyor_guidance"));
        actionableItemDto.setTemplate(makeLinkResource(table, rowNum, "template_id"));
        actionableItemDto.setTitle((String) table.getValue(rowNum, "title"));

        if (wip)
        {
            actionableItemDto.setJobScopeConfirmed((Boolean) table.getValue(rowNum, "job_scope_confirmed"));
        }

        return actionableItemDto;
    }

    public static CaseMilestoneDto getCaseMilestone(final Long targetId) throws DatabaseUnitException
    {
        final CaseMilestoneDto caseMilestoneDto = new CaseMilestoneDto();

        final ITable table = mastDataSet.getTable("MAST_CASE_CaseMilestone");
        final int rowNum = getRowNum(table, targetId);
        caseMilestoneDto.setId(((Number) table.getValue(rowNum, "id")).longValue());
        caseMilestoneDto.setCompletionDate(null);
        caseMilestoneDto.setDueDate((Date) table.getValue(rowNum, "due_date"));
        caseMilestoneDto.setInScope(true);
        caseMilestoneDto.setMilestone(makeLinkResource(table, rowNum, "milestone_id"));
        caseMilestoneDto.setMilestoneStatus(makeLinkResource(table, rowNum, "milestone_status_id"));
        caseMilestoneDto.setaCase(makeLinkResource(table, rowNum, "case_id"));

        return caseMilestoneDto;
    }

    public static ServiceCatalogueDto getServiceCatalogue(final Long targetId) throws DatabaseUnitException
    {
        final ServiceCatalogueDto serviceCatalogue = new ServiceCatalogueDto();

        final ITable table = mastDataSet.getTable("MAST_REF_ServiceCatalogue");
        final int rowNum = getRowNum(table, targetId);

        serviceCatalogue.setId(((Number) table.getValue(rowNum, "id")).longValue());
        serviceCatalogue.setName((String) table.getValue(rowNum, "name"));
        serviceCatalogue.setDescription((String) table.getValue(rowNum, "description"));
        serviceCatalogue.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));
        serviceCatalogue.setCyclePeriodicity((Integer) table.getValue(rowNum, "cycle_periodicity"));
        serviceCatalogue.setLowerRangeDateOffsetMonths((Integer) table.getValue(rowNum, "lower_range_date_offset_months"));
        serviceCatalogue.setUpperRangeDateOffsetMonths((Integer) table.getValue(rowNum, "upper_range_date_offset_months"));
        serviceCatalogue.setCode((String) table.getValue(rowNum, "code"));
        serviceCatalogue.setServiceType(makeLinkResource(table, rowNum, "service_type_id"));
        serviceCatalogue.setProductCatalogue(getProductCatalogue(((Number) table.getValue(rowNum, "product_catalogue_id")).longValue()));
        return serviceCatalogue;
    }

    public static Entry<Long, Long> getProductCatalogueAndSchedulingRegimeForService(final Long targetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_REF_ServiceCatalogue");
        final int rowNum = getRowNum(table, targetId);

        return new AbstractMap.SimpleEntry<Long, Long>(
                ((Number) table.getValue(rowNum, "product_catalogue_id")).longValue(),
                ((Number) table.getValue(rowNum, "scheduling_regime_id")).longValue());
    }

    public static SurveyDto getSurvey(final Long targetId) throws DatabaseUnitException
    {
        final SurveyDto survey = new SurveyDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_JobServiceInstance");
        final int rowNum = getRowNum(table, targetId);

        survey.setId(((Number) table.getValue(rowNum, "id")).longValue());
        survey.setName((String) table.getValue(rowNum, "name"));
        survey.setJob(makeLinkResource(table, rowNum, "job_id"));
        survey.setServiceCatalogue(makeLinkResource(table, rowNum, "service_catalogue_id"));
        survey.setSurveyStatus(makeLinkResource(table, rowNum, "service_credit_status_id"));
        survey.setDateOfCrediting((Date) table.getValue(rowNum, "date_of_crediting"));
        survey.setScheduleDatesUpdated((Boolean) table.getValue(rowNum, "schedule_dates_updated"));

        if (table.getValue(rowNum, "credited_by") != null)
        {
            survey.setCreditedBy(makeLinkResource(table, rowNum, "credited_by"));
        }

        survey.setJobScopeConfirmed((Boolean) table.getValue(rowNum, "job_scope_confirmed"));

        return survey;
    }

    public static JobDto getJob(final Long targetId) throws DatabaseUnitException
    {
        final JobDto job = new JobDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_Job");
        final int rowNum = getRowNum(table, targetId);

        job.setId(((Number) table.getValue(rowNum, "id")).longValue());
        job.setNote((String) table.getValue(rowNum, "notes"));
        job.setRequestByTelephoneIndicator((Boolean) table.getValue(rowNum, "request_by_telephone_flag"));
        job.setRequestedAttendanceDate((Date) table.getValue(rowNum, "requested_attendance_date"));
        job.setStatusReason((String) table.getValue(rowNum, "status_reason"));
        job.setWorkOrderNumber(checkLongNotNull((Number) table.getValue(rowNum, "work_order_number")));
        job.setWrittenServiceRequestReceivedDate((Date) table.getValue(rowNum, "written_service_request_received_date"));
        job.setWrittenServiceResponseSentDate((Date) table.getValue(rowNum, "written_service_response_sent_date"));

        final Long assetId = ((Number) table.getValue(rowNum, "asset_id")).longValue();
        final Long assetVersion = ((Number) table.getValue(rowNum, "asset_version_id")).longValue();
        final LinkVersionedResource linkVersionedResource = new LinkVersionedResource();
        linkVersionedResource.setId(assetId);
        linkVersionedResource.setVersion(assetVersion);

        job.setAsset(linkVersionedResource);
        job.setJobStatus(makeLinkResource(table, rowNum, "status_id"));
        job.setEtaDate((Date) table.getValue(rowNum, "estimated_start_date"));
        job.setEtdDate((Date) table.getValue(rowNum, "estimated_end_date"));
        job.setaCase(makeLinkResource(table, rowNum, "case_id"));
        job.setLocation((String) table.getValue(rowNum, "location"));
        job.setDescription((String) table.getValue(rowNum, "narrative"));
        job.setJobCategory(makeLinkResource(table, rowNum, "job_category_id"));
        job.setJobTeam(makeLinkResource(table, rowNum, "job_team_id"));
        job.setZeroVisitJob((Boolean) table.getValue(rowNum, "zero_visit_job"));
        job.setProposedFlagState(makeLinkResource(table, rowNum, "proposed_flag_state_id"));
        job.setClassGroupJob((Boolean) table.getValue(rowNum, "class_group_job"));

        return job;
    }

    public static FollowUpActionDto getFUA(final Long targetId) throws DatabaseUnitException
    {
        final FollowUpActionDto fua = new FollowUpActionDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_FollowUpAction");
        final int rowNum = getRowNum(table, targetId);

        fua.setId(((Number) table.getValue(rowNum, "id")).longValue());
        fua.setTitle((String) table.getValue(rowNum, "title"));
        fua.setDescription((String) table.getValue(rowNum, "description"));
        fua.setRaisedBy(new LinkResource());
        fua.getRaisedBy().setId(((Number) table.getValue(rowNum, "raised_by_id")).longValue());
        if (table.getValue(rowNum, "completed_by_id") != null)
        {
            fua.setCompletedBy(new LinkResource());
            fua.getCompletedBy().setId(((Number) table.getValue(rowNum, "completed_by_id")).longValue());
        }
        fua.setAddedManually((Boolean) table.getValue(rowNum, "added_manually"));
        fua.setFollowUpActionStatus(new LinkResource());
        fua.getFollowUpActionStatus().setId(((Number) table.getValue(rowNum, "follow_up_action_status_id")).longValue());
        fua.setCompletionDate((Date) table.getValue(rowNum, "completion_date"));
        fua.setTargetDate((Date) table.getValue(rowNum, "target_date"));
        fua.setAsset(new LinkResource());
        fua.getAsset().setId(((Number) table.getValue(rowNum, "asset_id")).longValue());
        fua.setReport(getReport(((Number) table.getValue(rowNum, "report_id")).longValue()));
        fua.setSurvey(new LinkResource());
        fua.getAsset().setId(((Number) table.getValue(rowNum, "asset_id")).longValue());

        return fua;
    }

    public static CodicilTypeDto getCodicilType(final Long targetId) throws DatabaseUnitException
    {
        final CodicilTypeDto codicilType = new CodicilTypeDto();

        final ITable table = mastDataSet.getTable("MAST_REF_CodicilType");
        final int rowNum = getRowNum(table, targetId);

        codicilType.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        codicilType.setName((String) table.getValue(rowNum, "name"));
        codicilType.setCode((String) table.getValue(rowNum, "code"));

        return codicilType;
    }

    public static ScheduledServiceDto getService(final Long targetId) throws DatabaseUnitException
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();

        final ITable table = mastDataSet.getTable("MAST_ASSET_ScheduledService");
        final int rowNum = getRowNum(table, targetId);

        service.setId(((Number) table.getValue(rowNum, "id")).longValue());
        service.setAssignedDate((Date) table.getValue(rowNum, "assigned_date"));
        service.setCyclePeriodicity((Integer) table.getValue(rowNum, "cycle_periodicity"));
        service.setDueDate((Date) table.getValue(rowNum, "due_date"));
        service.setLowerRangeDate((Date) table.getValue(rowNum, "lower_range_date"));
        service.setServiceCatalogueId(((Number) table.getValue(rowNum, "service_catalogue_id")).longValue());
        service.setServiceStatus(makeLinkResource(table, rowNum, "service_status_id"));
        service.setUpperRangeDate((Date) table.getValue(rowNum, "upper_range_date"));
        service.setProvisionalDates((Boolean) table.getValue(rowNum, "provisional_date_flag"));

        return service;
    }

    public static DefectDetailDto getDefectDetail(final Long targetId) throws DatabaseUnitException
    {
        final DefectDetailDto defectDetail = new DefectDetailDto();

        final ITable table = mastDataSet.getTable("MAST_REF_DefectDescriptor");
        final int rowNum = getRowNum(table, targetId);

        defectDetail.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defectDetail.setName((String) table.getValue(rowNum, "name"));

        return defectDetail;
    }

    public static DefectValueDto getDefectValue(final Long targetId) throws DatabaseUnitException
    {
        final DefectValueDto defectValue = new DefectValueDto();

        final ITable table = mastDataSet.getTable("MAST_REF_DefectValue");
        final int rowNum = getRowNum(table, targetId);

        defectValue.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defectValue.setName((String) table.getValue(rowNum, "name"));
        defectValue.setSeverity(makeReferenceDataDto(table, rowNum, "severity_id"));

        return defectValue;
    }

    public static DefectCategoryDto getDefectCategory(final Long targetId) throws DatabaseUnitException
    {
        final DefectCategoryDto defectCategory = new DefectCategoryDto();

        final ITable table = mastDataSet.getTable("MAST_REF_DefectCategory");
        final int rowNum = getRowNum(table, targetId);

        defectCategory.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defectCategory.setCategoryLetter((String) table.getValue(rowNum, "category_letter"));
        defectCategory.setName((String) table.getValue(rowNum, "name"));
        defectCategory.setParent(makeLinkResource(table, rowNum, "parent_id"));

        return defectCategory;
    }

    public static DefectDto getDefect(final Long targetId) throws DatabaseUnitException
    {
        return (DefectDto) populateDefect(new DefectDto(), targetId);
    }

    public static DefectDto getWIPDefect(final Long targetId) throws DatabaseUnitException
    {
        return (DefectDto) populateWIPDefect(new DefectDto(), targetId);
    }

    public static DeficiencyDto getDeficiency(final Long targetId) throws DatabaseUnitException
    {
        return (DeficiencyDto) populateDeficiency(new DeficiencyDto(), targetId, false);
    }

    public static DeficiencyDto getWipDeficiency(final Long targetId) throws DatabaseUnitException
    {
        return (DeficiencyDto) populateDeficiency(new DeficiencyDto(), targetId, true);
    }

    public static DefectLightDto getDefectLight(final Long targetId) throws DatabaseUnitException
    {
        return populateDefect(new DefectLightDto(), targetId);
    }

    public static DefectLightDto getWIPDefectLight(final Long targetId) throws DatabaseUnitException
    {
        return populateWIPDefect(new DefectLightDto(), targetId);
    }

    private static <T extends DefectLightDto> DefectLightDto populateDefect(final T defect, final Long targetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_FAULT_Fault");
        final int rowNum = getRowNum(table, targetId);

        defect.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defect.setAsset(makeLinkResource(table, rowNum, "asset_id"));
        defect.setDefectCategory(getDefectCategory(((Number) table.getValue(rowNum, "defect_category_id")).longValue()));
        defect.setSequenceNumber((Integer) table.getValue(rowNum, "internal_sequence_number"));
        defect.setDefectStatus(makeLinkResource(table, rowNum, "defect_status_id"));
        defect.setFirstFrameNumber((Integer) table.getValue(rowNum, "first_frame_number"));
        defect.setIncidentDate((Date) table.getValue(rowNum, "incident_date"));
        defect.setIncidentDescription((String) table.getValue(rowNum, "description"));
        defect.setLastFrameNumber((Integer) table.getValue(rowNum, "last_frame_number"));
        defect.setTitle((String) table.getValue(rowNum, "title"));
        defect.setConfidentialityType(makeLinkResource(table, rowNum, "confidentiality_type_id"));

        final ArrayList<DefectItemDto> defectItemDtos = new ArrayList<DefectItemDto>();
        final List<Long> defectItemIds = getRowsWhere("MAST_DEFECT_Defect_AssetItem", "defect_id", targetId, "id");
        for (final Long defectItemId : defectItemIds)
        {
            final DefectItemDto defectItem = new DefectItemDto();
            populateDefectItem(defectItem, defectItemId, "MAST_DEFECT_Defect_AssetItem");
            defectItemDtos.add(defectItem);

        }
        defect.setItems(defectItemDtos);

        final ArrayList<DefectDefectValueDto> defectDefectValueDtos = new ArrayList<DefectDefectValueDto>();
        final List<Long> defectValueIds = getRowsWhere("MAST_DEFECT_Defect_DefectValue", "defect_id", targetId, "id");
        for (final Long defectValueId : defectValueIds)
        {
            final DefectDefectValueDto defectDefectValue = new DefectDefectValueDto();
            populateDefectValue(defectDefectValue, defectValueId, "MAST_DEFECT_Defect_DefectValue");
            defectDefectValueDtos.add(defectDefectValue);

        }
        defect.setValues(defectDefectValueDtos);

        return defect;
    }

    private static <T extends DefectLightDto> DefectLightDto populateWIPDefect(final T defect, final Long targetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_FAULT_WIP_Fault");
        final int rowNum = getRowNum(table, targetId);

        defect.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defect.setJob(makeLinkResource(table, rowNum, "job_id"));
        defect.setParent(makeLinkResource(table, rowNum, "parent_id"));
        defect.setDefectCategory(getDefectCategory(((Number) table.getValue(rowNum, "defect_category_id")).longValue()));
        defect.setSequenceNumber((Integer) table.getValue(rowNum, "internal_sequence_number"));
        defect.setDefectStatus(makeLinkResource(table, rowNum, "defect_status_id"));
        defect.setFirstFrameNumber((Integer) table.getValue(rowNum, "first_frame_number"));
        defect.setIncidentDate((Date) table.getValue(rowNum, "incident_date"));
        defect.setIncidentDescription((String) table.getValue(rowNum, "description"));
        defect.setLastFrameNumber((Integer) table.getValue(rowNum, "last_frame_number"));
        defect.setTitle((String) table.getValue(rowNum, "title"));
        defect.setConfidentialityType(makeLinkResource(table, rowNum, "confidentiality_type_id"));

        final ArrayList<DefectItemDto> defectItemDtos = new ArrayList<DefectItemDto>();
        final List<Long> defectItemIds = getRowsWhere("MAST_DEFECT_WIP_Defect_AssetItem", "defect_id", targetId, "id");
        for (final Long defectItemId : defectItemIds)
        {
            final DefectItemDto defectItem = new DefectItemDto();
            populateDefectItem(defectItem, defectItemId, "MAST_DEFECT_WIP_Defect_AssetItem");
            defectItemDtos.add(defectItem);

        }
        defect.setItems(defectItemDtos);

        final ArrayList<DefectDefectValueDto> defectDefectValueDtos = new ArrayList<DefectDefectValueDto>();
        final List<Long> defectValueIds = getRowsWhere("MAST_DEFECT_WIP_Defect_DefectValue", "defect_id", targetId, "id");
        for (final Long defectValueId : defectValueIds)
        {
            final DefectDefectValueDto defectDefectValue = new DefectDefectValueDto();
            populateDefectValue(defectDefectValue, defectValueId, "MAST_DEFECT_WIP_Defect_DefectValue");
            defectDefectValueDtos.add(defectDefectValue);

        }
        defect.setValues(defectDefectValueDtos);

        return defect;
    }

    private static <T extends DeficiencyLightDto> DeficiencyLightDto populateDeficiency(final T deficiency, final Long targetId, boolean wip) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(wip ? "MAST_FAULT_WIP_Fault" : "MAST_FAULT_Fault");
        final int rowNum = getRowNum(table, targetId);

        deficiency.setId(((Number) table.getValue(rowNum, "id")).longValue());
        deficiency.setSequenceNumber((Integer) table.getValue(rowNum, "internal_sequence_number"));
        deficiency.setFirstFrameNumber((Integer) table.getValue(rowNum, "first_frame_number"));
        deficiency.setIncidentDate((Date) table.getValue(rowNum, "incident_date"));
        deficiency.setIncidentDescription((String) table.getValue(rowNum, "description"));
        deficiency.setLastFrameNumber((Integer) table.getValue(rowNum, "last_frame_number"));
        deficiency.setTitle((String) table.getValue(rowNum, "title"));
        deficiency.setConfidentialityType(makeLinkResource(table, rowNum, "confidentiality_type_id"));
        if (wip)
        {
            deficiency.setJob(makeLinkResource(table, rowNum, "job_id"));
        }
        else
        {
            deficiency.setAsset(makeLinkResource(table, rowNum, "asset_id"));
        }


        return deficiency;
    }

    private static DefectItemDto populateDefectItem(final DefectItemDto defectItem, final Long targetId, final String tableName)
            throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, targetId);
        defectItem.setId(((Number) table.getValue(rowNum, "id")).longValue());
        // until defect versioning

        final Integer itemId = ((Integer) table.getValue(rowNum, "item_id"));
        if (itemId != null)
        {
            defectItem.setItem(new ItemLightDto());
            defectItem.getItem().setId(itemId.longValue());
        }
        return defectItem;
    }

    private static DefectDefectValueDto populateDefectValue(final DefectDefectValueDto defectValue, final Long targetId, final String tableName)
            throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, targetId);
        defectValue.setId(((Number) table.getValue(rowNum, "id")).longValue());
        defectValue.setOtherDetails((String) table.getValue(rowNum, "other"));
        defectValue.setDefectValue(getDefectValue(((Number) table.getValue(rowNum, "defect_value_id")).longValue()));
        return defectValue;
    }

    public static CertificateActionDto getCertificateActionTaken(final Long targetId) throws DatabaseUnitException
    {
        final CertificateActionDto certificateAction = new CertificateActionDto();

        final ITable table = mastDataSet.getTable("MAST_REF_CertificateActionTaken");
        final int rowNum = getRowNum(table, targetId);

        certificateAction.setId(((Number) table.getValue(rowNum, "id")).longValue());
        certificateAction.setName((String) table.getValue(rowNum, "name"));
        certificateAction.setDescription((String) table.getValue(rowNum, "description"));
        certificateAction.setIssueDateMutable((Boolean) table.getValue(rowNum, "issue_date_mutable"));

        return certificateAction;
    }

    public static RepairDto getRepair(final Long repairId, final Boolean wip) throws DatabaseUnitException
    {
        final RepairDto repair = new RepairDto();
        final ITable table = mastDataSet.getTable(wip ? "MAST_DEFECT_WIP_Repair" : "MAST_DEFECT_Repair");
        final int rowNum = getRowNum(table, repairId);

        repair.setId(((Number) table.getValue(rowNum, "id")).longValue());
        repair.setDescription((String) table.getValue(rowNum, "description"));
        repair.setPromptThoroughRepair((Boolean) table.getValue(rowNum, "prompt_thorough_flag"));
        repair.setNarrative((String) table.getValue(rowNum, "narrative"));
        repair.setRepairTypeDescription((String) table.getValue(rowNum, "repair_type_description"));
        repair.setDefect(makeLinkResource(table, rowNum, "defect_id"));
        repair.setRepairAction(makeLinkResource(table, rowNum, "action_type_id"));
        repair.setConfirmed((Boolean) table.getValue(rowNum, "confirmed"));
        repair.setCodicil(makeLinkResource(table, rowNum, "codicil_id"));

        final ArrayList<LinkResource> repairTypeLinkResources = new ArrayList<LinkResource>();
        final List<Long> repairTypeIds = getRowsWhere(wip ? "MAST_DEFECT_WIP_Repair_RepairType" : "MAST_DEFECT_Repair_RepairType", "repair_id",
                repairId, "repair_type_id");
        for (final Long repairTypeId : repairTypeIds)
        {
            final LinkResource repairTypeLinkResource = new LinkResource();
            final ITable repairTypeTable = mastDataSet.getTable("MAST_REF_RepairType");
            final int repairTypeRowNum = getRowNum(repairTypeTable, repairTypeId);
            repairTypeLinkResource.setId(((Number) repairTypeTable.getValue(repairTypeRowNum, "id")).longValue());
            repairTypeLinkResources.add(repairTypeLinkResource);

        }
        repair.setRepairTypes(repairTypeLinkResources);

        final ArrayList<RepairItemDto> repairItemDtos = new ArrayList<RepairItemDto>();
        final List<Long> repairItemIds = getRowsWhere(wip ? "MAST_DEFECT_WIP_Repair_AssetItem" : "MAST_DEFECT_Repair_AssetItem", "repair_id",
                repairId, "id");
        for (final Long repairItemId : repairItemIds)
        {
            final RepairItemDto repairItem = new RepairItemDto();
            populateRepairItem(repairItem, repairItemId, wip ? "MAST_DEFECT_WIP_Repair_AssetItem" : "MAST_DEFECT_Repair_AssetItem");
            repairItemDtos.add(repairItem);

        }
        repair.setRepairs(repairItemDtos);

        repair.setMaterialsUsed(new ArrayList<com.baesystems.ai.lr.dto.LinkResource>(0));

        return repair;
    }

    private static RepairItemDto populateRepairItem(final RepairItemDto repairItem, final Long targetId, final String tableName)
            throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, targetId);
        repairItem.setId(((Number) table.getValue(rowNum, "id")).longValue());
        final DefectItemLinkDto defectItemLink = new DefectItemLinkDto();
        repairItem.setItem(defectItemLink);
        return repairItem;
    }

    public static AssetTypeDto getAssetType(final Long targetId) throws DatabaseUnitException
    {
        final AssetTypeDto assetType = new AssetTypeDto();
        final ITable table = mastDataSet.getTable("MAST_REF_AssetType");
        final int rowNum = getRowNum(table, targetId);

        assetType.setId(((Number) table.getValue(rowNum, "id")).longValue());
        assetType.setCode((String) table.getValue(rowNum, "code"));
        assetType.setName((String) table.getValue(rowNum, "name"));
        assetType.setParentId(checkLongNotNull((Number) table.getValue(rowNum, "parent_id")));
        assetType.setLevelIndication((Integer) table.getValue(rowNum, "level_indication"));

        return assetType;
    }

    public static CertificateTypeDto getCertificateType(final Long targetId) throws DatabaseUnitException
    {
        final CertificateTypeDto certificateType = new CertificateTypeDto();

        final ITable table = mastDataSet.getTable("MAST_REF_CertificateType");
        final int rowNum = getRowNum(table, targetId);

        certificateType.setId(((Number) table.getValue(rowNum, "id")).longValue());
        certificateType.setDescription((String) table.getValue(rowNum, "description"));
        certificateType.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));
        certificateType.setFormNumber((String) table.getValue(rowNum, "form_number"));
        certificateType.setName((String) table.getValue(rowNum, "name"));
        certificateType.setServiceCatalogue(new LinkResource(((Number) table.getValue(rowNum, "service_catalogue_id")).longValue()));
        certificateType.setVersion((Double) table.getValue(rowNum, "version"));

        return certificateType;
    }

    public static CertificateDto getWipCertificate(final Long certificateId) throws DatabaseUnitException
    {
        final CertificateDto certificate = new CertificateDto();
        final ITable table = mastDataSet.getTable("MAST_JOB_CertificateWIP");
        final int rowNum = getRowNum(table, certificateId);

        certificate.setId(((Number) table.getValue(rowNum, "id")).longValue());
        certificate.setCertificateNumber((String) table.getValue(rowNum, "certificate_number"));
        certificate.setCertificateType(makeLinkResource(table, rowNum, "certificate_type_id"));
        certificate.setExpiryDate((Date) table.getValue(rowNum, "expiry_date"));
        certificate.setExtendedDate((Date) table.getValue(rowNum, "extended_date"));
        certificate.setIssueDate((Date) table.getValue(rowNum, "issue_date"));
        certificate.setOffice(makeLinkResource(table, rowNum, "lr_office_id"));
        certificate.setEmployee(makeLinkResource(table, rowNum, "lr_employee_id"));
        certificate.setJob(makeLinkResource(table, rowNum, "job_id"));
        certificate.setParent(makeLinkResource(table, rowNum, "parent_id"));
        certificate.setCertificateStatus(makeLinkResource(table, rowNum, "certificate_status_id"));
        certificate.setCertificateAction(makeLinkResource(table, rowNum, "certificate_action_id"));
        return certificate;
    }

    public static ReferenceDataDto getReferenceData(final ReferenceDataType type, final Long targetId) throws DatabaseUnitException
    {
        final ReferenceDataDto refData = new ReferenceDataDto();

        final ITable table = mastDataSet.getTable(type.getTable());
        final int rowNum = getRowNum(table, targetId);

        refData.setId(((Number) table.getValue(rowNum, "id")).longValue());
        refData.setName((String) table.getValue(rowNum, "name"));
        // Ignore "description" for now because the BE doesn't populate this field.

        return refData;
    }

    public static AttributeTypeDto getAttributeType(final Long typeId) throws DatabaseUnitException
    {
        final AttributeTypeDto typeDto = new AttributeTypeDto();
        final ITable table = mastDataSet.getTable("MAST_REF_AssetAttributeType");
        final int rowNum = getRowNum(table, typeId);
        typeDto.setId(((Number) table.getValue(rowNum, "id")).longValue());
        typeDto.setItemType(getItemType(((Number) table.getValue(rowNum, "item_type_id")).longValue()));
        typeDto.setValueType(makeReferenceDataDto(table, rowNum, "value_type_id"));
        typeDto.setDescriptionOrder(checkLongNotNull((Number) table.getValue(rowNum, "description_order")));
        typeDto.setStartDate((Date) table.getValue(rowNum, "start_date"));
        typeDto.setEndDate((Date) table.getValue(rowNum, "end_date"));
        typeDto.setNamingOrder(checkLongNotNull((Number) table.getValue(rowNum, "naming_order")));
        typeDto.setNamingDecoration((String) table.getValue(rowNum, "naming_decoration"));
        typeDto.setTransferable((Boolean) table.getValue(rowNum, "transferable"));
        typeDto.setMaxOccurs((Integer) table.getValue(rowNum, "max_occurance"));
        typeDto.setMinOccurs((Integer) table.getValue(rowNum, "min_occurance"));
        typeDto.setName((String) table.getValue(rowNum, "name"));
        return typeDto;

    }

    public static DraftItemDto getDraftItem(final Long itemId) throws DatabaseUnitException
    {
        final DraftItemDto item = new DraftItemDto();

        final ITable table = mastDataSet.getTable("MAST_ASSET_DraftAssetItem");
        final int rowNum = getRowNum(table, itemId);

        item.setId(((Number) table.getValue(rowNum, "id")).longValue());
        item.setDisplayOrder((Integer) table.getValue(rowNum, "display_order"));
        item.setItemTypeId(((Number) table.getValue(rowNum, "item_type_id")).longValue());
        item.setName((String) table.getValue(rowNum, "name"));

        return item;
    }

    public static WorkItemLightDto getWorkItemLight(final Long workItemId, final Boolean wip) throws DatabaseUnitException
    {
        final WorkItemLightDto workItemLight = new WorkItemLightDto();

        final ITable table = mastDataSet.getTable(wip ? "MAST_JOB_WorkItem" : "MAST_ASSET_WorkItem");
        final int rowNum = getRowNum(table, workItemId);

        workItemLight.setId(((Number) table.getValue(rowNum, "id")).longValue());
        workItemLight.setName((String) table.getValue(rowNum, "name"));
        workItemLight.setTaskNumber((String) table.getValue(rowNum, "task_number"));
        workItemLight.setAssetItem(makeLinkResource(table, rowNum, "asset_item_id"));
        workItemLight.setReferenceCode((String) table.getValue(rowNum, "reference_code"));
        workItemLight.setWorkItemType(makeLinkResource(table, rowNum, "work_item_type_id"));
        workItemLight.setCreditStatus(makeLinkResource(table, rowNum, "credit_status_id"));
        workItemLight.setDueDate((Date) table.getValue(rowNum, "due_date"));
        workItemLight.setAssignedDate((Date) table.getValue(rowNum, "assigned_date"));
        workItemLight.setServiceCode((String) table.getValue(rowNum, "service_code"));

        if (wip)
        {
            workItemLight.setSurvey(makeLinkResource(table, rowNum, "job_service_instance_id"));
            workItemLight.setParent(makeLinkResource(table, rowNum, "parent_id"));
        }
        else
        {
            workItemLight.setScheduledService(makeLinkResource(table, rowNum, "scheduled_service_id"));
        }

        return workItemLight;
    }

    public static WorkItemAttributeDto getWorkItemAttribute(final Long workItemAttributeId) throws DatabaseUnitException
    {
        final WorkItemAttributeDto workItemAttribute = new WorkItemAttributeDto();

        final ITable table = mastDataSet.getTable("MAST_JOB_WorkItemAttribute");
        final int rowNum = getRowNum(table, workItemAttributeId);

        workItemAttribute.setId(((Number) table.getValue(rowNum, "id")).longValue());

        return workItemAttribute;
    }

    public static Long checkLongNotNull(final Number longToCheck)
    {
        Long newLong = null;
        if (longToCheck != null)
        {
            newLong = longToCheck.longValue();
        }
        return newLong;
    }

    private static int getRowNum(final ITable table, final Long targetId) throws DatabaseUnitException
    {
        return getRowNum(table, targetId, "id");
    }

    private static int getRowNum(final ITable table, final Long targetId, final Long versionId) throws DatabaseUnitException
    {
        return getRowNum(table, targetId, versionId, "id", "asset_version_id");
    }

    private static int getRowNum(final ITable table, final Long targetId, final String idColumn) throws DatabaseUnitException
    {
        final Integer intId = Integer.valueOf((int) targetId.longValue());

        int retVal = -1;
        for (int count = 0; count < table.getRowCount(); count++)
        {
            final Integer intId2 = new Integer(table.getValue(count, idColumn).toString());
            if (intId2.equals(intId))
            {
                retVal = count;
                break;
            }
        }

        return retVal;
    }

    private static int getRowNum(final ITable table, final Long targetId, final Long versionId, final String idColumn, final String versionColumn)
            throws DatabaseUnitException
    {
        final Integer intId = Integer.valueOf((int) targetId.longValue());

        int retVal = -1;
        for (int count = 0; count < table.getRowCount(); count++)
        {
            final Integer intId2 = new Integer(table.getValue(count, idColumn).toString());
            final Integer versionId2 = new Integer(table.getValue(count, versionColumn).toString());
            if (intId2.equals(intId) && versionId2.equals(versionId2))
            {
                retVal = count;
                break;
            }
        }

        return retVal;
    }

    private static int getRowNum(final ITable table, final Long targetId1, final String idColumn1, final Long targetId2, final String idColumn2)
            throws DatabaseUnitException
    {
        final Integer intId1 = (int) targetId1.longValue();
        final Integer intId2 = (int) targetId2.longValue();

        int retVal = -1;
        for (int count = 0; count < table.getRowCount(); count++)
        {
            final Integer intId1A = new Integer(table.getValue(count, idColumn1).toString());
            final Integer intId2A = new Integer(table.getValue(count, idColumn2).toString());

            if (intId1A.equals(intId1) && intId2A.equals(intId2))
            {
                retVal = count;
                break;
            }
        }
        return retVal;
    }

    private static List<Integer> getRowNums(final ITable table, final Long target, final String column) throws DatabaseUnitException
    {
        final Integer intTarget = Integer.valueOf((int) target.longValue());

        final List<Integer> retVal = new ArrayList<Integer>();
        for (int count = 0; count < table.getRowCount(); count++)
        {
            final Integer intTarget2 = new Integer(table.getValue(count, column).toString());
            if (intTarget2.equals(intTarget))
            {
                retVal.add(count);
            }
        }

        return retVal;
    }

    private static LinkResource makeLinkResource(final ITable table, final int rowNum, final String column)
            throws DataSetException
    {
        LinkResource retVal = null;
        final Number resourceId = ((Number) table.getValue(rowNum, column));

        if (resourceId != null)
        {
            retVal = new LinkResource(resourceId.longValue());
        }

        return retVal;
    }

    private static NamedLinkResource makeLrEmployee(final ITable table, final int rowNum, final String column)
            throws DatabaseUnitException
    {
        NamedLinkResource retVal = null;
        final Long resourceId = ((Long) table.getValue(rowNum, column));

        if (resourceId != null)
        {
            final ITable employeeTable = mastDataSet.getTable("MAST_LRP_Employee");
            final int employeeRowNum = getRowNum(employeeTable, resourceId);

            final String employeeName = ((String) employeeTable.getValue(employeeRowNum, "first_name")) + " "
                    + ((String) employeeTable.getValue(employeeRowNum, "last_name"));

            retVal = new NamedLinkResource();
            retVal.setId(resourceId);
            retVal.setName(employeeName);
        }

        return retVal;
    }

    // todo remove once the Big Int Vs Int stuff is merged
    private static LinkResource makeLinkResourceNotBigInteger(final ITable table, final int rowNum, final String column)
            throws DataSetException
    {
        LinkResource retVal = null;
        final Integer resourceId = ((Integer) table.getValue(rowNum, column));

        if (resourceId != null)
        {
            retVal = new LinkResource(resourceId.longValue());
        }

        return retVal;
    }

    // Exists for legacy purposes with pre-existing "get" methods.
    private static ReferenceDataDto makeReferenceDataDto(final ITable table, final int rowNum, final String column)
            throws DataSetException
    {
        ReferenceDataDto retVal = null;
        final Number resourceId = ((Number) table.getValue(rowNum, column));

        if (resourceId != null)
        {
            retVal = new ReferenceDataDto();
            retVal.setId(resourceId.longValue());
        }

        return retVal;
    }

    /**
     * Returns the number of rows in a given table.
     *
     * @param tableName, the name of the table.
     * @return The total number of rows
     */
    public static Long getRowCount(final String tableName) throws DataSetException
    {
        final ITable table = mastDataSet.getTable(tableName);
        return new Long(table.getRowCount());
    }

    /**
     * Used to extract data from link tables returns a list of id from the requested column where the value in the query
     * column matches the given value.
     *
     * @param tableName, the name of the table.
     * @param idColumn, the name of the column to search in.
     * @param targetId, the required id
     * @param getColumnName, the name of the column to return values from
     * @return A list of ids that fulfil the query.
     */
    public static java.util.List<Long> getRowsWhere(final String tableName, final String idColumn, final Long targetId, final String getColumnName)
            throws DatabaseUnitException
    {
        final Integer intId = Integer.valueOf((int) targetId.longValue());
        final ITable table = mastDataSet.getTable(tableName);

        final java.util.List<Long> retVal = new ArrayList<Long>();
        for (int count = 0; count < table.getRowCount(); count++)
        {
            final Integer intId2 = new Integer(table.getValue(count, idColumn).toString());
            if (intId2.equals(intId))
            {
                retVal.add(((Number) table.getValue(count, getColumnName)).longValue());
            }
        }

        return retVal;
    }

    public static Pair<String, Date> getAudit(final String tableName, final Long id) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, id);
        return new ImmutablePair<>((String) table.getValue(rowNum, "last_update_user_id"), (Date) table.getValue(rowNum, "last_update_date"));
    }

    public static CodicilTemplateDto getCodicilTemplate(final Long codicilTemplateId) throws DatabaseUnitException
    {
        final CodicilTemplateDto codicilTemplateDto = new CodicilTemplateDto();
        final ITable table = mastDataSet.getTable("MAST_REF_CodicilTemplate");
        final int rowNum = getRowNum(table, codicilTemplateId);
        codicilTemplateDto.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        codicilTemplateDto.setCategory(makeReferenceDataDto(table, rowNum, "category_id"));
        codicilTemplateDto.setConfidentialityType(makeReferenceDataDto(table, rowNum, "confidentiality_type_id"));
        codicilTemplateDto.setDescription((String) table.getValue(rowNum, "narrative"));
        codicilTemplateDto.setSurveyorGuidance((String) table.getValue(rowNum, "surveyor_guidance"));
        codicilTemplateDto.setEditableBySurveyor((Boolean) table.getValue(rowNum, "editable_by_surveyor"));
        codicilTemplateDto.setTitle((String) table.getValue(rowNum, "title"));
        final Object possibleItemTypeId = table.getValue(rowNum, "item_type_id");
        if (possibleItemTypeId != null)
        {
            codicilTemplateDto.setItemType(new ItemTypeDto());
            codicilTemplateDto.getItemType().setId(((Integer) possibleItemTypeId).longValue());
        }
        return codicilTemplateDto;
    }

    public static Integer countDraftAttributesForDraftItem(final Long targetId) throws DatabaseUnitException
    {

        final ITable table = mastDataSet.getTable("MAST_ASSET_DraftAssetAttribute");
        final List<Integer> rowNums = getRowNums(table, targetId, "item_id");

        return rowNums.size();
    }

    public static Integer countExpectedAttributesForItemType(final Long targetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_REF_AssetAttributeType");
        final List<Integer> rowNums = getRowNums(table, targetId, "item_type_id");
        Integer total = 0;
        Integer minOccurs;

        for (final Integer rowNum : rowNums)
        {
            minOccurs = (Integer) table.getValue(rowNum, "min_occurance");

            total += ((Boolean) table.getValue(rowNum, "is_default")) && minOccurs == 0 ? 1 : minOccurs;
        }

        return total;
    }

    public static Boolean isDeleted(final String tableName, final Long id) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable(tableName);
        final int rowNum = getRowNum(table, id);
        final Boolean deleted = (Boolean) table.getValue(rowNum, "deleted");
        return deleted == null ? false : (Boolean) table.getValue(rowNum, "deleted");
    }

    public static Boolean isVersionedItemDeleted(final Long id, final Long version)
            throws DatabaseUnitException, ClassNotFoundException, IOException, InstantiationException, SQLException, IllegalAccessException
    {
        final String query = "select deleted from MAST_ASSET_VersionedAssetItem WHERE id = " + id + " and asset_version_id = " + version;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        resultSet.next();
        return resultSet.getLong("deleted") == NUMERIC_TRUE ? true : false;
    }

    public static Boolean isVersionedItemRelationshipDeleted(final Long fromItemId, final Long fromVersionId, final Long toItemId, final Long toVersionId)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final String query = "select deleted from MAST_ASSET_AssetItemRelationship WHERE"
                + " from_item_id = " + fromItemId + " AND from_version_id = " + fromVersionId
                + " AND to_item_id = " + toItemId + " AND to_version_id = " + toVersionId;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        resultSet.next();
        return resultSet.getLong("deleted") == NUMERIC_TRUE ? true : false;
    }

    public static Long getMostRecentVersionIdForItem(final Long itemId)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final String query = "select max(asset_version_id) as version from MAST_ASSET_VersionedAssetItem where id = " + itemId;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        resultSet.next();
        return resultSet.getLong("version");
    }


    public static Boolean attributeIsDeleted(final Long itemId, final Long versionId)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final String query = "select deleted from MAST_ASSET_AssetItemAttribute WHERE"
                + " item_id = " + itemId + " AND asset_version_id = " + versionId;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        resultSet.next();
        return resultSet.getLong("deleted") == NUMERIC_TRUE ? true : false;
    }

    public static ReferenceDataVersionDto getReferenceDataVersion() throws DatabaseUnitException
    {
        final ReferenceDataVersionDto referenceDataVersionDto = new ReferenceDataVersionDto();
        final ITable table = mastDataSet.getTable("MAST_REF_ReferenceDataVersion");
        final int rowNum = getRowNum(table, 1L);
        referenceDataVersionDto.setId(((Integer) table.getValue(rowNum, "id")).longValue());
        referenceDataVersionDto.setVersion((Integer) table.getValue(rowNum, "version"));
        return referenceDataVersionDto;
    }

    public static List<Long> getIdList(final String tableName) throws NumberFormatException, DataSetException
    {
        final ITable table = mastDataSet.getTable(tableName);

        final List<Long> returnValue = new ArrayList<Long>();
        for (int count = 0; count < table.getRowCount(); count++)
        {
            returnValue.add(new Long(table.getValue(count, "id").toString()));
        }

        return returnValue;
    }

    /**
     * Clear out some tasks that will have been added indirectly during the prior integration tests
     */
    public static void purgeRogueTasks(final List<Long> taskIds)
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        final StringBuffer stringBuffer = new StringBuffer();
        taskIds.forEach(taskId -> stringBuffer.append(taskId.toString()).append(","));
        final String ids = stringBuffer.substring(0, stringBuffer.length() - 1); // trim final ","
        final String query = "update MAST_ASSET_WorkItem set deleted = 1 where id in (" + ids + ")";
        DatabaseUtils.runRawQuery("mast", query);
    }

    public static Long getAssetPublishedVersion(final Long assetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset");
        final int rowNum = getRowNum(table, assetId);
        final Number value = (Number) table.getValue(rowNum, "published_version_id");
        return value != null ? value.longValue() : null;
    }

    public static Long getAssetDraftVersion(final Long assetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset");
        final int rowNum = getRowNum(table, assetId);
        final Number value = (Number) table.getValue(rowNum, "draft_version_id");
        return value != null ? value.longValue() : null;
    }

    public static String getCheckedOutBy(final Long assetId) throws DatabaseUnitException
    {
        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset");
        final int rowNum = getRowNum(table, assetId);
        return (String) table.getValue(rowNum, "checked_out_by");
    }

    public static Long getMaxVersionForAsset(final Long assetId)
            throws DatabaseUnitException, ClassNotFoundException, IOException, InstantiationException, SQLException, IllegalAccessException
    {
        final String query = "select MAX(asset_version_id) from MAST_ASSET_VersionedAsset WHERE id = " + assetId;
        final ResultSet resultSet = DatabaseUtils.getResultSetForQuery("mast", query);
        resultSet.next();
        return resultSet.getLong("MAX(asset_version_id)");
    }

    // For Ihs Equipment
    public static IhsEquipmentDetailsDto getEquipmentDetails(final Long imoNumber) throws DatabaseUnitException
    {
        final IhsEquipmentDetailsDto ihsEquipmentDetailsDto = new IhsEquipmentDetailsDto();

        final IhsEquipment2Dto ihsEquipment2Dto = new IhsEquipment2Dto();
        final IhsEquipment4Dto ihsEquipment4Dto = new IhsEquipment4Dto();

        final ITable ihsEquipment2DtoTable = ihsDataSet.getTable("ABSD_LREQ2");
        final ITable ihsEquipment4DtoTable = ihsDataSet.getTable("ABSD_LREQ4");

        final int ihsEquipment2DtoTablerowNum = getRowNum(ihsEquipment2DtoTable, imoNumber, "LRNO");
        final int ihsEquipment4DtoTablerowNum = getRowNum(ihsEquipment4DtoTable, imoNumber, "LRNO");

        ihsEquipment2Dto.setId(new Long(ihsEquipment2DtoTable.getValue(ihsEquipment2DtoTablerowNum, "LRNO").toString()));
        ihsEquipment2Dto.setCableDiameter((String) ihsEquipment2DtoTable.getValue(ihsEquipment2DtoTablerowNum, "F08_CABDIAM_IMP"));
        ihsEquipment2Dto.setCableGrade((String) ihsEquipment2DtoTable.getValue(ihsEquipment2DtoTablerowNum, "F08_CABGRAD"));
        final BigDecimal cableLength = (BigDecimal) ihsEquipment2DtoTable.getValue(ihsEquipment2DtoTablerowNum, "F08_CABLEN_MET");
        ihsEquipment2Dto.setCableLength(cableLength.doubleValue());

        ihsEquipment4Dto.setId(new Long(ihsEquipment4DtoTable.getValue(ihsEquipment4DtoTablerowNum, "LRNO").toString()));
        ihsEquipment4Dto.setEquipmentLetter((String) ihsEquipment4DtoTable.getValue(ihsEquipment4DtoTablerowNum, "F08_EQIP_LET"));

        ihsEquipmentDetailsDto.setEquipment2Dto(ihsEquipment2Dto);
        ihsEquipmentDetailsDto.setEquipment4Dto(ihsEquipment4Dto);

        return ihsEquipmentDetailsDto;
    }

    // For Ihs Principal Dimensions
    public static IhsPrincipalDimensionsDto getPrincipalDimensions(final Long imoNumber) throws DatabaseUnitException
    {
        final IhsPrincipalDimensionsDto ihsPrincipalDimensionsDto = new IhsPrincipalDimensionsDto();

        final ITable ihsAssetDtoTable = ihsDataSet.getTable("ABSD_SHIP_SEARCH");
        final ITable ihsHibrDtoTable = ihsDataSet.getTable("ABSD_HIBR");
        final ITable ihsHideDtoTable = ihsDataSet.getTable("ABSD_HIDE");
        final ITable ihsHidrDtoTable = ihsDataSet.getTable("ABSD_HIDR");
        final ITable ihsHileDtoTable = ihsDataSet.getTable("vwABSD_HILE");
        final ITable ihsHiprDtoTable = ihsDataSet.getTable("ABSD_HIPR");
        final ITable ihsHitlDtoTable = ihsDataSet.getTable("ABSD_HITL");
        final ITable ihsSsch2DtoTable = ihsDataSet.getTable("ABSD_SSCH2_COLLATED");
        final ITable ihsStdeDtoTable = ihsDataSet.getTable("SUPPLEMENTAL_ABSD_STDE");

        final int ihsAssetDtoTablerowNum = getRowNum(ihsAssetDtoTable, imoNumber, "LRNO");
        final int ihsHibrDtoTablerowNum = getRowNum(ihsHibrDtoTable, imoNumber, "LRNO");
        final int ihsHideDtoTablerowNum = getRowNum(ihsHideDtoTable, imoNumber, "LRNO");
        final int ihsHidrDtoTablerowNum = getRowNum(ihsHidrDtoTable, imoNumber, "LRNO");
        final int ihsHileDtoTablerowNum = getRowNum(ihsHileDtoTable, imoNumber, "LRNO");
        final int ihsHiprDtoTablerowNum = getRowNum(ihsHiprDtoTable, imoNumber, "LRNO");
        final int ihsHitlDtoTablerowNum = getRowNum(ihsHitlDtoTable, imoNumber, "LRNO");
        final int ihsSsch2DtoTablerowNum = getRowNum(ihsSsch2DtoTable, imoNumber, "LRNO");
        final int ihsStdeDtoTablerowNum = getRowNum(ihsStdeDtoTable, imoNumber, "LRNO");

        if (ihsAssetDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setId(new Long(ihsAssetDtoTable.getValue(ihsAssetDtoTablerowNum, "LRNO").toString()));
            final BigDecimal lengthOverall = (BigDecimal) ihsAssetDtoTable.getValue(ihsAssetDtoTablerowNum, "LOA");
            if (lengthOverall != null)
            {
                ihsPrincipalDimensionsDto.setLengthOverall(lengthOverall.doubleValue());
            }
        }

        if (ihsHibrDtoTablerowNum > 0)
        {
            final BigDecimal breadthExtreme = (BigDecimal) ihsHibrDtoTable.getValue(ihsHibrDtoTablerowNum, "C09_EX");
            final BigDecimal breadthMoulded = (BigDecimal) ihsHibrDtoTable.getValue(ihsHibrDtoTablerowNum, "C09_MLD");
            if (breadthExtreme != null)
            {
                ihsPrincipalDimensionsDto.setBreadthExtreme(breadthExtreme.doubleValue());
            }
            if (breadthMoulded != null)
            {
                ihsPrincipalDimensionsDto.setBreadthMoulded(breadthMoulded.doubleValue());
            }
        }

        if (ihsHideDtoTablerowNum > 0)
        {
            final BigDecimal depthMoulded = (BigDecimal) ihsHideDtoTable.getValue(ihsHideDtoTablerowNum, "C10_MLD");
            if (depthMoulded != null)
            {
                ihsPrincipalDimensionsDto.setDepthMoulded(depthMoulded.doubleValue());
            }
        }

        if (ihsHidrDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setDeadWeight((Integer) ihsHidrDtoTable.getValue(ihsHidrDtoTablerowNum, "C07_DWTL"));
            final BigDecimal draughtMax = (BigDecimal) ihsHidrDtoTable.getValue(ihsHidrDtoTablerowNum, "C07_DL");
            if (draughtMax != null)
            {
                ihsPrincipalDimensionsDto.setDraughtMax(draughtMax.doubleValue());
            }
        }

        if (ihsHileDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setLengthBtwPerpendiculars((String) ihsHileDtoTable.getValue(ihsHileDtoTablerowNum, "C02_LBP"));
        }

        if (ihsHiprDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setPropulsion((String) ihsHiprDtoTable.getValue(ihsHiprDtoTablerowNum, "D05_PRTYP"));
        }

        if (ihsHitlDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setNetTonnage((Integer) ihsHitlDtoTable.getValue(ihsHitlDtoTablerowNum, "B07_NET"));
        }

        if (ihsSsch2DtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setGrossTonnage((Integer) ihsSsch2DtoTable.getValue(ihsSsch2DtoTablerowNum, "GROSS"));
        }

        if (ihsStdeDtoTablerowNum > 0)
        {
            ihsPrincipalDimensionsDto.setDecks((String) ihsStdeDtoTable.getValue(ihsStdeDtoTablerowNum, "C05_NO_DECKS"));
        }

        return ihsPrincipalDimensionsDto;
    }
}
