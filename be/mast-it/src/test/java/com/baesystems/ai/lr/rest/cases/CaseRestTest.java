package com.baesystems.ai.lr.rest.cases;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.QueryRelationshipType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.CaseLightPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.path.json.JsonPath;

/**
 * Class to test the REST interface for the Cases functionality. See test/resources/insert_test_data.sql for the data
 * used by these tests.
 */
@Category(IntegrationTest.class)
public class CaseRestTest extends BaseRestTest
{
    /**
     * API Rest paths
     */
    public static final String BASE_CASE = new StringBuffer().append(BASE_PATH).append("/case").toString();
    public static final String CASE_QUERY = new StringBuffer().append(BASE_CASE).append("/query").toString();
    public static final String CASE_ABSTRACT_QUERY = new StringBuffer().append(BASE_CASE).append("/abstract-query").toString();
    public static final String CASE = new StringBuffer().append(BASE_CASE).append("/%d").toString();
    public static final String VERIFY_CASE = new StringBuffer().append(BASE_PATH)
            .append("/verify-case?imoNumber=%d&builder=%d&yard=%s&businessProcess=%d").toString();

    private static final Long CASE_1_ID = 1L;
    private static final Long CASE_2_ID = 2L;
    private static final Long CASE_3_ID = 3L;
    private static final Long CASE_5_ID = 5L;
    private static final Long CASE_6_ID = 6L;
    private static final Long CASE_7_ID = 7L;
    private static final Long CASE_8_ID = 8L;
    private static final Long VALID_CASE_ID_WITH_INVALID_ASSETS = 5L;
    private static final Long CANCELLED_CASE_ID = 7L;
    private static final Long DELETABLE_CASE_ID = 6L;
    private static final Long UNDELETABLE_CASE_ID = 1L;
    private static final Long INVALID_CASE_ID = 999L;
    private static final Long VALID_IMO_NUMBER = 1000019L;
    private static final Long VALID_NEW_IMO_NUMBER = 1000174L;
    private static final Long VALID_NEW_IMO_NUMBER2 = 1000186L;
    private static final Long VALID_NEW_IMO_NUMBER3 = 1000162L;
    private static final Long INVALID_IMO_NUMBER = 8888888L;
    private static final Long BUILDER = 1L;
    private static final String YARD_NUMBER = "y100";
    private static final Long BUSINESS_PROCESS = 1L;
    private static final Long STATUS_FOR_ERROR_CHECKING = 2L;
    private static final Long SURVEYOR_1_ID = 1L;
    private static final Long SURVEYOR_TYPE_1_ID = 1L;
    private static final Long OFFICE_2_ID = 2L;
    private static final Long OFFICE_ROLE_1_ID = 1L;
    private static final Long CLOSED_ID = 5L;
    private static final Long VALID_IDS_ASSET_ID = 1L;
    private static final Long CASE_TYPE_1_ID = 1L;

    private CaseWithAssetDetailsDto validCase;
    private CaseDto basicValidCase;
    private CaseLightPage casePage;
    private CaseValidationDto caseValidationEmpty;
    private CaseValidationDto caseValidationIdsAsset1Case1;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.validCase = DataUtils.getCaseWithLinks(CASE_1_ID);
        this.basicValidCase = DataUtils.getBasicCaseWithLinks(CASE_1_ID);

        this.casePage = new CaseLightPage();

        this.caseValidationEmpty = new CaseValidationDto();

        this.setupPopulatedCaseValidation();
    }

    private void setupPopulatedCaseValidation()
    {
        this.caseValidationIdsAsset1Case1 = new CaseValidationDto();
        final List<LinkResource> caseLinkResources = new ArrayList<LinkResource>();
        final LinkResource case1LinkResource = new LinkResource();
        case1LinkResource.setId(CASE_1_ID);
        caseLinkResources.add(case1LinkResource);
        this.caseValidationIdsAsset1Case1.setCases(caseLinkResources);
        final LinkResource idsAssetLinkResource = new LinkResource();
        idsAssetLinkResource.setId(VALID_IDS_ASSET_ID);
        this.caseValidationIdsAsset1Case1.setIdsAsset(idsAssetLinkResource);
    }

    /**
     * Test that a valid GET request for ../case/{caseId} results in a positive response. Expecting JSON containing Case
     * ID.
     */
    @Test
    public final void getCaseUriCaseFound()
    {
        TestUtils.singleItemFound(format(CASE, CASE_1_ID.longValue()), this.validCase, true);
    }

    /**
     * Test that a valid GET request for ../case/{caseId} results in a positive response. Expecting a blank body because
     * no Case with this ID was found.
     */
    @Test
    public final void getCaseUriCaseNotFound()
    {
        TestUtils.singleItemNotFound(format(CASE, INVALID_CASE_ID.longValue()));
    }

    /**
     * Test that a valid GET request for ../case results in a positive response. Expecting JSON to contain a list, even
     * if that list has nothing in it.
     */
    @Test
    public final void getCasesUri()
    {
        this.casePage.getContent().add(this.basicValidCase);
        TestUtils.listItemsFound(BASE_CASE, this.casePage, true, false);
    }

    /**
     * Test get case list filtered by open and closed status.
     */
    @Test
    public final void getCasesUriWithFilter()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CaseQueryDto caseQuery = new CaseQueryDto();
        final List<Long> caseStatusList = new ArrayList<Long>();

        caseStatusList.add(CLOSED_ID);

        caseQuery.setCaseStatusId(caseStatusList);

        final CaseLightPage results = TestUtils.postResponse(CASE_QUERY, caseQuery, HttpStatus.OK,
                CaseLightPage.class);

        TestUtils.compareFields(results, this.casePage, true);
    }

    /**
     * Test to verify a case with valid IMO number. Currently doesn't validate the contents of the returned JSON object.
     */
    @Test
    public final void verifyCaseTest()
    {
        TestUtils.singleItemFound(format(VERIFY_CASE, VALID_IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS), this.caseValidationIdsAsset1Case1,
                true);
    }

    /**
     * Test to verify a case with invalid IMO number. Currently doesn't validate the contents of the returned JSON
     * object.
     */
    @Test
    public final void verifyCaseTestInvalidIMONumber()
    {
        TestUtils.singleItemFound(format(VERIFY_CASE, INVALID_IMO_NUMBER, BUILDER, YARD_NUMBER, BUSINESS_PROCESS), this.caseValidationEmpty, true);
    }

    /**
     * Test deleting a case
     */
    @Test
    @Ignore // this is deleting an asset which causes problems later
    public final void deleteCase()
    {
        final String result1 = TestUtils.deleteResponse(String.format(CASE, DELETABLE_CASE_ID.longValue()), HttpStatus.OK).body().asString();
        assertEquals("true", result1);

        TestUtils.deleteResponse(String.format(CASE, DELETABLE_CASE_ID.longValue()), HttpStatus.NOT_FOUND);

        final String result3 = TestUtils.deleteResponse(String.format(CASE, UNDELETABLE_CASE_ID.longValue()), HttpStatus.OK).body().asString();
        assertEquals("false", result3);
    }

    // Test is no longer correct because the asset model is now valid @Test
    public final void createInvalidCase()
    {
        final CaseDto template = TestUtils.getResponse(BASE_PATH + "/case/" + VALID_CASE_ID_WITH_INVALID_ASSETS, HttpStatus.OK)
                .getBody()
                .as(CaseDto.class);

        this.cleanCaseTemplate(template);

        TestUtils.postResponse(BASE_PATH + "/case", template, HttpStatus.CONFLICT);
    }

    /**
     * Tests a successful creation of case.
     */
    @Test
    @Ignore
    // Will fix test once all validation is complete.
    public final void createCase()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + VALID_CASE_ID_WITH_INVALID_ASSETS, HttpStatus.OK)
                .getBody()
                .as(CaseWithAssetDetailsDto.class);
        this.cleanCaseTemplate(template);

        AssetDto asset = TestUtils.getResponse(BASE_PATH + "/asset/" + template.getAsset().getId(), HttpStatus.OK)
                .getBody()
                .as(AssetDto.class);

        asset.getIhsAsset().setId(VALID_NEW_IMO_NUMBER3);
        asset.getIhsAsset().setInternalId(null);
        asset.setId(null);
        asset.setBuilder(null);
        asset.setAssetModel(null);

        asset = TestUtils.postResponse(BASE_PATH + "/asset", asset, HttpStatus.OK, AssetDto.class);

        template.setAsset(new LinkResource(asset.getId()));
        template.setCreatedOn(null);

        final JsonPath value = TestUtils.postResponse(BASE_PATH + "/case", template, HttpStatus.OK).jsonPath();

        final Long id = value.getLong("id");

        final String createdOn = value.getString("createdOn");
        assertFalse(createdOn == null);
        assertFalse(createdOn.isEmpty());

        TestUtils.deleteResponse(String.format(CASE, id), HttpStatus.OK);
    }

    @Test
    public final void testUpdateCaseFailsWhenStaleTokenIsDifferent()
    {
        final CaseWithAssetDetailsDto existingCase = TestUtils.getResponse(format(CASE, CASE_1_ID), HttpStatus.OK)
                .getBody()
                .as(CaseWithAssetDetailsDto.class);

        existingCase.setClassNotation(existingCase.getClassNotation() + "X");
        existingCase.setStalenessHash("Incorrect Hash Data");

        final ErrorMessageDto error = TestUtils
                .putResponse(format(CASE, CASE_1_ID), existingCase, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.CASE.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    /**
     * Tests creating a case with a valid lead asset.
     */
    @Test
    @Ignore
    public final void createCaseValidLeadAsset()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + VALID_CASE_ID_WITH_INVALID_ASSETS, HttpStatus.OK)
                .getBody()
                .as(CaseWithAssetDetailsDto.class);
        this.cleanCaseTemplate(template);

        AssetDto asset = TestUtils.getResponse(BASE_PATH + "/asset/" + template.getAsset().getId(), HttpStatus.OK)
                .getBody()
                .as(AssetDto.class);

        asset.getIhsAsset().setId(VALID_NEW_IMO_NUMBER2);
        asset.setIsLead(false);
        asset.setLeadImo(VALID_IMO_NUMBER);
        asset.getIhsAsset().setInternalId(null);
        asset.setId(null);
        asset.setBuilder(null);
        asset.setAssetModel(null);

        asset = TestUtils.postResponse(BASE_PATH + "/asset", asset, HttpStatus.OK, AssetDto.class);

        template.setAsset(new LinkResource(asset.getId()));
        template.setSurveyors(new ArrayList<CaseSurveyorWithLinksDto>());
        template.getSurveyors().add(new CaseSurveyorWithLinksDto());
        template.getSurveyors().get(0).setSurveyor(new LinkResource(SURVEYOR_1_ID));
        template.getSurveyors().get(0).setEmployeeRole(new LinkResource(SURVEYOR_TYPE_1_ID));
        template.setOffices(new ArrayList<OfficeLinkDto>());
        template.getOffices().add(new OfficeLinkDto());
        template.getOffices().get(0).setOffice(new LinkResource(OFFICE_2_ID));
        template.getOffices().get(0).setOfficeRole(new LinkResource(OFFICE_ROLE_1_ID));
        template.getCaseStatus().setId(STATUS_FOR_ERROR_CHECKING);
        template.setCreatedOn(null);

        final JsonPath value = TestUtils.postResponse(BASE_PATH + "/case", template, HttpStatus.OK).jsonPath();

        final Long id = value.getLong("id");

        final String createdOn = value.getString("createdOn");
        assertFalse(createdOn == null);
        assertFalse(createdOn.isEmpty());

        TestUtils.deleteResponse(String.format(CASE, id), HttpStatus.OK);
    }

    /**
     * Tests fails to create case because the imo number of the lead asset does not exist.
     */
    @Test
    @Ignore
    public final void createCaseInvalidLeadAsset()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + VALID_CASE_ID_WITH_INVALID_ASSETS, HttpStatus.OK)
                .getBody()
                .as(CaseWithAssetDetailsDto.class);
        this.cleanCaseTemplate(template);

        AssetDto asset = TestUtils.getResponse(BASE_PATH + "/asset/" + template.getAsset().getId(), HttpStatus.OK)
                .getBody()
                .as(AssetDto.class);

        asset.getIhsAsset().setId(VALID_NEW_IMO_NUMBER);
        asset.setIsLead(false);
        asset.setLeadImo(INVALID_IMO_NUMBER);
        asset.getIhsAsset().setInternalId(null);
        asset.setAssetModel(null);

        asset.setId(null);
        asset.setBuilder(null);

        asset = TestUtils.postResponse(BASE_PATH + "/asset", asset, HttpStatus.OK, AssetDto.class);

        template.setAsset(new LinkResource(asset.getId()));
        template.getCaseStatus().setId(STATUS_FOR_ERROR_CHECKING);
        template.setCreatedOn(null);

        TestUtils.postResponse(BASE_PATH + "/case", template, HttpStatus.NOT_FOUND);
    }

    /**
     * Tests fails to create case because the imo number of the lead asset does not exist.
     */
    @Test
    public final void createCaseInvalidLeadAssetInUncommitedState()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + VALID_CASE_ID_WITH_INVALID_ASSETS, HttpStatus.OK)
                .getBody()
                .as(CaseWithAssetDetailsDto.class);
        this.cleanCaseTemplate(template);

        final AssetDto asset = TestUtils.getResponse(BASE_PATH + "/asset/" + template.getAsset().getId(), HttpStatus.OK)
                .getBody()
                .as(AssetDto.class);

        asset.getIhsAsset().setId(INVALID_IMO_NUMBER);
        asset.setIsLead(false);
        asset.setLeadImo(VALID_NEW_IMO_NUMBER);
        asset.getIhsAsset().setInternalId(null);
        asset.setId(null);
        asset.setBuilder(null);
        asset.setAssetModel(null);

        TestUtils.postResponse(BASE_PATH + "/asset", asset, HttpStatus.NOT_FOUND);
    }

    /**
     * Tests the creation of a new case with an existing asset already associated to a cancelled case.
     */
    @Test
    @Ignore
    public final void createCaseWithExistingAsset()
    {
        final CaseWithAssetDetailsDto template = TestUtils.getResponse(BASE_PATH + "/case/" + CANCELLED_CASE_ID, HttpStatus.OK).getBody()
                .as(CaseWithAssetDetailsDto.class);
        this.cleanCaseTemplate(template);

        template.setCreatedOn(null);

        final JsonPath value = TestUtils.postResponse(BASE_PATH + "/case", template, HttpStatus.OK).jsonPath();

        final Long id = value.getLong("id");

        final String createdOn = value.getString("createdOn");
        assertFalse(createdOn == null);
        assertFalse(createdOn.isEmpty());

        TestUtils.deleteResponse(String.format(CASE, id), HttpStatus.OK);
    }

    /**
     * Test the sorting of the paginated case query for asset name (Story - DB.15)
     */
    @Test
    public void querySortingTest()
    {
        final AbstractQueryDto exclude6 = new AbstractQueryDto();

        // exclude case 6 because it gets deleted and run order is not known.
        exclude6.setField("id");
        exclude6.setValue(CASE_6_ID);
        exclude6.setRelationship(QueryRelationshipType.NE);

        final List<Long> expected = Arrays.asList(CASE_7_ID, CASE_5_ID, CASE_2_ID, CASE_3_ID, CASE_1_ID, CASE_8_ID);
        final Iterator<Long> expectedIter = expected.iterator();

        final CaseLightPage result = TestUtils.postResponse(CASE_ABSTRACT_QUERY + "?sort=asset.publishedVersion.name", exclude6, HttpStatus.OK,
                CaseLightPage.class);

        assertNotNull(result);
        assertEquals(expected.size(), result.getContent().size());
        result.getContent().stream().forEach(value -> assertEquals(expectedIter.next(), value.getId()));
    }

    /**
     * Test filtering by surveyors (Story - DB.16)
     */
    @Test
    public void queryFilterBySurveyorRoleTest()
    {
        final AbstractQueryDto exclude6 = new AbstractQueryDto();

        // exclude case 6 because it gets deleted and run order is not known.
        exclude6.setField("id");
        exclude6.setValue(CASE_6_ID);
        exclude6.setRelationship(QueryRelationshipType.NE);

        final AbstractQueryDto query = new AbstractQueryDto();

        query.setJoinField("surveyors");
        query.setField("employeeRole.id");
        query.setValue(SURVEYOR_TYPE_1_ID);
        query.setRelationship(QueryRelationshipType.EQ);

        final AbstractQueryDto mainQuery = new AbstractQueryDto();

        mainQuery.setAnd(new ArrayList<AbstractQueryDto>());
        mainQuery.getAnd().add(exclude6);
        mainQuery.getAnd().add(query);

        final List<Long> expected = Arrays.asList(CASE_7_ID, CASE_5_ID, CASE_2_ID, CASE_3_ID, CASE_8_ID);
        final Iterator<Long> expectedIter = expected.iterator();

        final CaseLightPage result = TestUtils.postResponse(CASE_ABSTRACT_QUERY + "?sort=asset.publishedVersion.name", mainQuery, HttpStatus.OK,
                CaseLightPage.class);

        assertNotNull(result);
        assertEquals(expected.size(), result.getContent().size());
        result.getContent().stream().forEach(value -> assertEquals(expectedIter.next(), value.getId()));
    }

    /**
     * Test filtering by date, type and status (Story - DB.15, Story - DB.17, Story - DB.18)
     */
    @Test
    public void queryFilterByTypeStatusAndTest()
    {
        final AbstractQueryDto exclude6 = new AbstractQueryDto();

        // exclude case 6 because it gets deleted and run order is not known.
        exclude6.setField("id");
        exclude6.setValue(CASE_6_ID);
        exclude6.setRelationship(QueryRelationshipType.NE);

        final AbstractQueryDto queryLowerDate = new AbstractQueryDto();

        queryLowerDate.setField("createdOn");
        queryLowerDate.setValue("2015-08-01");
        queryLowerDate.setRelationship(QueryRelationshipType.GE);

        final AbstractQueryDto queryUpperDate = new AbstractQueryDto();

        queryUpperDate.setField("createdOn");
        queryUpperDate.setValue("2015-10-01");
        queryUpperDate.setRelationship(QueryRelationshipType.LE);

        final AbstractQueryDto queryStatus = new AbstractQueryDto();

        queryStatus.setField("caseStatus.id");
        queryStatus.setValue(STATUS_FOR_ERROR_CHECKING);
        queryStatus.setRelationship(QueryRelationshipType.EQ);

        final AbstractQueryDto queryType = new AbstractQueryDto();

        queryType.setField("caseType.id");
        queryType.setValue(CASE_TYPE_1_ID);
        queryType.setRelationship(QueryRelationshipType.EQ);

        final AbstractQueryDto mainQuery = new AbstractQueryDto();

        mainQuery.setAnd(new ArrayList<AbstractQueryDto>());
        mainQuery.getAnd().add(exclude6);
        mainQuery.getAnd().add(queryLowerDate);
        mainQuery.getAnd().add(queryUpperDate);
        mainQuery.getAnd().add(queryStatus);
        mainQuery.getAnd().add(queryType);

        final List<Long> expected = Arrays.asList(CASE_2_ID);
        final Iterator<Long> expectedIter = expected.iterator();

        final CaseLightPage result = TestUtils.postResponse(CASE_ABSTRACT_QUERY + "?sort=asset.publishedVersion.name", mainQuery, HttpStatus.OK,
                CaseLightPage.class);

        assertNotNull(result);
        assertEquals(expected.size(), result.getContent().size());
        result.getContent().stream().forEach(value -> assertEquals(expectedIter.next(), value.getId()));
    }

    /**
     * Cleans a case by deleting the id and internal id's.
     *
     * @param aCase
     */
    private void cleanCaseTemplate(final CaseDto aCase)
    {
        aCase.setId(null);
        aCase.setInternalId(null);
    }
}
