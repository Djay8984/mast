package com.baesystems.ai.lr.rest.services;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductGroupExtendedDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.enums.ReferenceDataType;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class ProductCatalogueRestTest extends BaseRestTest
{
    public static final String BASE_PRODUCT = new StringBuffer().append(BASE_PATH).append("/asset/%d/product").toString();
    public static final String PRODUCT = new StringBuffer().append(BASE_PRODUCT).append("/%d").toString();
    public static final String SELECTED_PRODUCT_FOR_ASSET = new StringBuffer().append(BASE_PATH).append("/asset/%d/product").toString();

    /**
     * Test data
     */
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long ASSET_3_ID = 3L;
    private static final Long PRODUCT_CATALOGUE_1_ID = 1L;
    private static final Long PRODUCT_CATALOGUE_2_ID = 2L;
    private static final Long PRODUCT_CATALOGUE_5_ID = 5L;
    private static final Long PRODUCT_CATALOGUE_6_ID = 6L;
    private static final Long PRODUCT_CATALOGUE_14_ID = 14L;
    private static final Long RULE_SET_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_7_ID = 7L;
    private static final Long PRODUCT_TYPE_1_ID = 1L;
    private static final Long PRODUCT_GROUP_1_ID = 1L;
    private static final Long INVALID_ID = 9999999L;
    private static final Long NEGATIVE = -1L;
    private static final Long ZERO = 0L;

    private static final Long PRODUCT_ID_2 = 2L;
    private static final Long PRODUCT_ID_4 = 4L;
    private static final Long SCHEDULE_REGIME_1 = 1L;

    // Needs to be public for Theory to work.
    @DataPoints
    public static final String[] BAD_REQUESTS = {String.format(BASE_PRODUCT, ZERO.longValue()),
                                                 BASE_PATH + "/asset" + "/SomeLetters" + "/product",
                                                 BASE_PATH + "/asset" + "/SomeLetters" + "/product",
                                                 String.format(SELECTED_PRODUCT_FOR_ASSET, ZERO.longValue()),
                                                 String.format(SELECTED_PRODUCT_FOR_ASSET, NEGATIVE.longValue())};

    private ProductCatalogueExtendedDto extendedProductCatalogue14;
    private ProductCatalogueExtendedDto extendedProductCatalogue2;
    private ProductDto product1;
    private ProductDto product5;
    private ProductTypeExtendedDto productType;
    private ProductGroupExtendedDto productGroup;
    private ProductDto[] selectedProducts;
    private ReferenceDataDto ruleSet1;
    private ServiceCatalogueLightDto serviceCatalogue2;
    private ServiceCatalogueLightDto serviceCatalogue7;
    private ProductListDto putExistingProductList;
    private ProductDto existingProduct2;
    private LinkResource schedulingRegimeOne;
    private ProductDto existingProduct4;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        product1 = DataUtils.getProduct(PRODUCT_CATALOGUE_1_ID);
        product5 = DataUtils.getProduct(PRODUCT_CATALOGUE_5_ID);

        selectedProducts = new ProductDto[]{product1, product5};

        putExistingProductList = new ProductListDto();
        final List<ProductDto> productList = new ArrayList<>();

        schedulingRegimeOne = new LinkResource(SCHEDULE_REGIME_1);

        existingProduct2 = new ProductDto();
        existingProduct2.setId(PRODUCT_ID_2);
        existingProduct2.setProductCatalogueId(PRODUCT_CATALOGUE_2_ID);
        existingProduct2.setSchedulingRegime(schedulingRegimeOne);
        productList.add(existingProduct2);

        existingProduct4 = new ProductDto();
        existingProduct4.setId(PRODUCT_ID_4);
        existingProduct4.setProductCatalogueId(PRODUCT_CATALOGUE_1_ID);
        existingProduct4.setSchedulingRegime(schedulingRegimeOne);
        productList.add(existingProduct4);

        putExistingProductList.setProductList(productList);

        createProductTypeExtendedDto();
    }

    private void createProductTypeExtendedDto() throws DatabaseUnitException
    {
        ruleSet1 = DataUtils.getReferenceData(ReferenceDataType.RULE_SET, RULE_SET_1_ID);
        serviceCatalogue2 = DataUtils.getServiceCatalogue(SERVICE_CATALOGUE_1_ID);
        serviceCatalogue7 = DataUtils.getServiceCatalogue(SERVICE_CATALOGUE_7_ID);
        extendedProductCatalogue14 = DataUtils.getExtendedProductCatalogue(PRODUCT_CATALOGUE_14_ID);
        extendedProductCatalogue2 = DataUtils.getExtendedProductCatalogue(PRODUCT_CATALOGUE_6_ID);
        productType = DataUtils.getExtendedProductType(PRODUCT_TYPE_1_ID);
        productGroup = DataUtils.getExtendedProductGroup(PRODUCT_GROUP_1_ID);

        extendedProductCatalogue14.setRuleSets(Arrays.asList(new ReferenceDataDto[]{ruleSet1}));
        extendedProductCatalogue2.setRuleSets(Arrays.asList(new ReferenceDataDto[]{ruleSet1}));
        extendedProductCatalogue14.setServiceCatalogues(Arrays.asList(new ServiceCatalogueLightDto[]{serviceCatalogue2}));
        extendedProductCatalogue2.setServiceCatalogues(Arrays.asList(new ServiceCatalogueLightDto[]{serviceCatalogue7}));
        productGroup.setProductCatalogues(Arrays.asList(new ProductCatalogueExtendedDto[]{extendedProductCatalogue14}));
        productType.setProductGroups(Arrays.asList(new ProductGroupExtendedDto[]{productGroup}));
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/selected-product results in a positive response. Expecting
     * error for record not found.
     */
    @Test
    public final void selectedProductForAssetUriAssetNotFound()
    {
        TestUtils.singleItemNotFound(format(SELECTED_PRODUCT_FOR_ASSET, INVALID_ID.longValue()));
    }

    /**
     * Test that a valid GET request for ../asset/{asset_id}/selected-product results in a positive response. Expecting
     * list of JSONs containing product 3.
     */
    @Test
    public final void selectedProductForAssetUri()
    {
        TestUtils.referenceDataListFound(format(SELECTED_PRODUCT_FOR_ASSET, ASSET_1_ID.longValue()), selectedProducts, true);
    }

    @Test
    public final void productUriProductPut()
    {
        // Create a new put request including the original products and a brand new one.
        final String url = format(SELECTED_PRODUCT_FOR_ASSET, ASSET_2_ID.longValue());

        final ProductListDto putWithNewProductList = new ProductListDto();
        final List<ProductDto> productList = new ArrayList<>();

        final List<ProductDto> existingProductList = Arrays.asList(TestUtils.getResponse(url).getBody().jsonPath().getObject("",
                ProductDto[].class));

        productList.addAll(existingProductList); // Get the existing products in the list

        final ProductDto newProduct = new ProductDto();
        newProduct.setProductCatalogueId(PRODUCT_CATALOGUE_5_ID);
        newProduct.setSchedulingRegime(schedulingRegimeOne);
        productList.add(newProduct);

        putWithNewProductList.setProductList(productList);

        // This put creates the new product
        final ProductDto[] updatedProducts = TestUtils.putResponse(url, putWithNewProductList, HttpStatus.OK).getBody().jsonPath().getObject("",
                ProductDto[].class);

        try
        {
            Assert.assertEquals(existingProductList.size() + 1, updatedProducts.length);
            Assert.assertEquals(existingProduct2, updatedProducts[0]);
            Assert.assertEquals(existingProduct4, updatedProducts[1]);
            Assert.assertNotNull(updatedProducts[2].getId());
        }

        finally
        {
            putWithNewProductList.setProductList(existingProductList);
            // This put removes that new product, putting it back to how it was.
            final ProductDto[] revertedProducts = TestUtils.putResponse(url, putWithNewProductList, HttpStatus.OK).getBody().jsonPath().getObject("",
                    ProductDto[].class);

            Assert.assertEquals(existingProductList.size(), revertedProducts.length);
            Assert.assertEquals(existingProduct2, revertedProducts[0]);
            Assert.assertEquals(existingProduct4, revertedProducts[1]);
        }
    }

    @Test
    @Ignore("Test fails with new reference data set. Re-introduce once the data set is stable enough for testing.")
    public final void checkProductRemovalFailsIfItHasAssociatedServices()
    {
        final String url = format(SELECTED_PRODUCT_FOR_ASSET, ASSET_3_ID.longValue());
        final String body = "{\"productList\": []}";
        TestUtils.putResponse(url, body, HttpStatus.CONFLICT);
    }

    /**
     * Test that a valid PUT request for ../asset/{asset_id}/product results in a positive response. Expecting error for
     * record not found.
     */
    @Test
    public final void productUriProductPutNotFound()
    {
        TestUtils.putResponse(format(SELECTED_PRODUCT_FOR_ASSET, INVALID_ID.longValue()), putExistingProductList,
                HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid GET request for ../product/{product_id} results in a positive response. Expecting an error for
     * a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual feedback
    public final void ruleSetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }
}
