package com.baesystems.ai.lr.rest.employees;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.pages.EmployeePage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class EmployeeRestTest extends BaseRestTest
{
    public static final String EMPLOYEE = new StringBuffer().append(BASE_PATH).append("/employee").toString();
    public static final String EMPLOYEE_BY_ID = new StringBuffer().append(EMPLOYEE).append("/%d").toString();
    public static final String EMPLOYEE_BY_NAME = new StringBuffer().append(EMPLOYEE).append("?search=%s").toString();

    private static final Long EMPLOYEE_1_ID = 1L;
    private static final Long INVALID_ID = 999999L;

    private EmployeePage employeePage;
    private LrEmployeeDto employeeDto1;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        employeePage = new EmployeePage();
        employeeDto1 = DataUtils.getLrEmployee(EMPLOYEE_1_ID);
        employeePage.getContent().add(employeeDto1);
    }

    /**
     * Test that a valid GET request for ../employee/{id} results in a positive response.
     */
    @Test
    public final void testGetEmployeeByID()
    {
        TestUtils.singleItemFound(String.format(EMPLOYEE_BY_ID, EMPLOYEE_1_ID), employeeDto1, true);
    }

    /**
     * Test that an invalid GET request for ../employee/{id} results in a 404 response.
     */
    @Test
    public final void testGetEmployeeByIDFails()
    {
        TestUtils.singleItemNotFound(String.format(EMPLOYEE_BY_ID, INVALID_ID));
    }

    /**
     * Test that a valid GET request for ../employee?search=X results in a positive response.
     */
    @Test
    public final void testSearchEmployeeByName()
    {
        TestUtils.listItemsFound(String.format(EMPLOYEE_BY_NAME, employeeDto1.getFirstName(), employeeDto1.getLastName()), employeePage, true, true);
    }

    /**
     * Test that a invalid GET request for ../employee?search=X results in a positive, empty response.
     */
    @Test
    public final void testSearchEmployeeByNameNotFound()
    {
        final String invalidFirstName = "gfufbfe";
        TestUtils.listItemsNotFound(String.format(EMPLOYEE_BY_NAME, invalidFirstName, employeeDto1.getLastName()));
    }

}
