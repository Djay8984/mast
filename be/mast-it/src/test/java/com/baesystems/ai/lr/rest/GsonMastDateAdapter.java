package com.baesystems.ai.lr.rest;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.baesystems.ai.lr.utils.DateHelper;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonMastDateAdapter implements JsonSerializer<Date>, JsonDeserializer<Date>
{

    private final SimpleDateFormat dateFormat;

    public GsonMastDateAdapter()
    {
        dateFormat = DateHelper.mastFormatter();
    }

    @Override
    public synchronized JsonElement serialize(final Date date, final Type type, final JsonSerializationContext jsonSerializationContext)
    {
        return new JsonPrimitive(dateFormat.format(date));
    }

    @Override
    public synchronized Date deserialize(final JsonElement jsonElement, final Type type,
            final JsonDeserializationContext jsonDeserializationContext)
    {
        try
        {
            return dateFormat.parse(jsonElement.getAsString());
        }
        catch (final ParseException exception)
        {
            throw new JsonParseException(exception);
        }
    }
}
