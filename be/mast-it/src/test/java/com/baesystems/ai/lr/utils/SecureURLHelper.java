package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.baesystems.ai.lr.constants.MastHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class SecureURLHelper
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureURLHelper.class);

    private Header customHeader;
    private volatile Map<String, String> cookies;

    public static final String USER = "Allan Hyde";
    public static final String GROUP = "ADMIN";

    // When the BE is https only
    // public static final String KEYSTORE = System.getProperty("container.keystore", "keystore.jks");
    // public static final String KEYSTORE_PASSWORD = System.getProperty("container.keystore.password", "nalle123");

    public SecureURLHelper()
    {
    }

    public Response invokeRequest(final String checkSum, final RequestCallback callback)
    {
        return invokeRequest(callback, createHeaders(checkSum));
    }

    public Response invokeRequest(final RequestCallback callback, Headers headers)
    {
        // RestAssured.keystore(KEYSTORE, KEYSTORE_PASSWORD);
        RequestSpecification spec = RestAssured.with().headers(headers);

        if (cookies != null)
        {
            spec = spec.cookies(cookies);
            LOGGER.warn("invokeRequest - missing cookies will request a new authentication");
        }

        final Response response = callback.doRequest(spec);
        if (!response.getCookies().isEmpty())
        {
            cookies = response.getCookies();
        }
        return response;
    }

    /**
     * Generates a group of security headers for authentication and authorisation.
     *
     * @param checkSum
     * @return List of headers
     */
    private Headers createHeaders(final String checkSum)
    {
        final List<Header> headers = getDefaultHeaders(checkSum);
        if (customHeader != null)
        {
            for (final Iterator<Header> iterator = headers.iterator(); iterator.hasNext();)
            {
                final Header header = iterator.next();
                if (header.getName().equals(customHeader.getName()))
                {
                    iterator.remove();
                }
            }
            headers.add(new Header(customHeader.getName(), customHeader.getValue()));
        }
        return new Headers(headers);
    }

    private List<Header> getDefaultHeaders(final String checkSum)
    {
        // One service is dependant on the username being a valid employee but this is will be changed in future
        // Need this now to allow the corresponding integration tests to pass.
        final Header userHeader = new Header(MastHeaders.USER, USER);

        // Groups
        final Header groupsHeader = new Header(MastHeaders.GROUPS, GROUP);

        // Client Sign
        final Header signHeader = new Header(MastHeaders.IDS_CLIENT_SIGN, checkSum);

        // Client Identity
        final Header identityHeader = new Header(MastHeaders.IDS_CLIENT_IDENTITY, SignUtils.getClientID());

        final Header contentTypeHeader = new Header("Content-Type", "application/json");

        return new ArrayList<>(Arrays.asList(userHeader, groupsHeader, signHeader, identityHeader, contentTypeHeader));
    }

    public interface RequestCallback
    {
        Response doRequest(RequestSpecification spec);
    }

    public void setCustomHeader(final Header customHeader)
    {
        this.customHeader = customHeader;
    }

    public void flushCookies()
    {
        cookies = null;
    }
}
