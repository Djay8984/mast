package com.baesystems.ai.lr.rest.services;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.pages.ProductList;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class ProductServiceRestTest extends BaseRestTest
{
    private static final String SELECTED_PRODUCT_FOR_ASSET = urlBuilder(BASE_PATH, "/asset/%d/product");

    private static final Long ASSET_1_ID = 1L;

    @Test
    public void failAProductUpdateIfSuppliedHashIsWrong()
    {
        final List<ProductDto> currentProductList = TestUtils
                .getResponse(String.format(SELECTED_PRODUCT_FOR_ASSET, ASSET_1_ID), HttpStatus.OK, ProductList.class);

        // get one of the products, and alter the staleness value to cause an error on updating.
        currentProductList.get(0).setStalenessHash("Invalid Hash");

        final ProductListDto putWithAlteredProductList = new ProductListDto();
        putWithAlteredProductList.setProductList(currentProductList);

        // watch it fail
        final ErrorMessageDto error = TestUtils
                .putResponse(format(SELECTED_PRODUCT_FOR_ASSET, ASSET_1_ID),
                        putWithAlteredProductList, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.PRODUCT.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

}
