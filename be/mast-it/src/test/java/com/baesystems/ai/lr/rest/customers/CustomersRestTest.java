package com.baesystems.ai.lr.rest.customers;

import static java.lang.String.format;

import java.util.ArrayList;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.CustomerHasFunctionDto;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.pages.AssetCustomerPage;
import com.baesystems.ai.lr.pages.CustomerPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class CustomersRestTest extends BaseRestTest
{
    /**
     * API Rest paths
     */
    public static final String BASE_CUSTOMER = new StringBuffer().append(BASE_PATH).append("/customer").toString();
    public static final String CUSTOMER = new StringBuffer().append(BASE_CUSTOMER).append("/%d").toString();
    public static final String CUSTOMER_SEARCH = new StringBuffer().append(CustomersRestTest.BASE_CUSTOMER).append("?search=%s&sort=%s").toString();
    public static final String ASSET_CUSTOMER = new StringBuffer().append(BASE_ASSET).append("/%d/customer").toString();

    /**
     * Test data
     */
    private static final Long CUSTOMER_2_ID = 2L;
    private static final Long FUNCTION_2_ID = 2L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long INVALID_ID = 8888L;

    private PartyDto customer2;
    private CustomerLinkDto assetparty2;
    private PartyLightDto party;
    private CustomerPage customerPage;
    private AssetCustomerPage assetCustomerPage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        this.customer2 = DataUtils.getCustomer(CUSTOMER_2_ID);
        this.party = DataUtils.getParty(CUSTOMER_2_ID);

        this.customerPage = new CustomerPage();
        this.customerPage.getContent().add(this.party);

        this.assetparty2 = new CustomerLinkDto();

        this.assetparty2.setId(CUSTOMER_2_ID);
        this.assetparty2.setCustomer(this.customer2);
        this.assetparty2.setFunctions(new ArrayList<CustomerHasFunctionDto>());
        this.assetparty2.getFunctions().add(new CustomerHasFunctionDto());
        this.assetparty2.getFunctions().get(0).setCustomerFunction(new LinkResource(FUNCTION_2_ID));
        this.assetparty2.getFunctions().get(0).setId(FUNCTION_2_ID);

        this.assetCustomerPage = new AssetCustomerPage();
        this.assetCustomerPage.getContent().add(this.assetparty2);
    }

    @Test
    public final void testGetAssetCustomers()
    {
        TestUtils.listItemsFound(format(ASSET_CUSTOMER, ASSET_2_ID.longValue()), this.assetCustomerPage, true, false);
    }

    @Test
    public final void testGetInvalidAssetCustomer()
    {
        TestUtils.singleItemNotFound(format(ASSET_CUSTOMER, INVALID_ID.longValue()));
    }
}
