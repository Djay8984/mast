package com.baesystems.ai.lr.rest.references;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.references.ReferenceDataMapDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class AssetModelTemplateRestTest extends BaseRestTest
{
    private static final String BASE_ASSET_MODEL_TEMPLATE_PATH = urlBuilder(BASE_PATH, "/reference-data/asset/asset-model-template");

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
    }

    /**
     * Tests that the Asset Model Template is returned with the correct JSON schema. Currently does not check content.
     */
    @Test
    public final void getAssetModelTemplate()
    {
        TestUtils.getResponse(BASE_ASSET_MODEL_TEMPLATE_PATH, HttpStatus.OK).jsonPath().getObject("", ReferenceDataMapDto.class);
    }
}
