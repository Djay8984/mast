package com.baesystems.ai.lr.rest.rules;

import static java.lang.String.format;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.dbunit.DatabaseUnitException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPageResourceDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.assets.DraftItemRestTest;
import com.baesystems.ai.lr.rest.services.ServiceRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class TaskRulesRestTest extends BaseRestTest
{
    public static final String MOCK_DROOLS_TASK_PATH = urlBuilder("mock-drools/taskGeneration/");
    public static final String TASK_QUERY = urlBuilder(BASE_PATH, "/task/query");

    private static final Long ASSET_1_ID = 1L;

    // See @TaskForServiceSuccessResponse-001.json and TaskForAssetSuccessResponse-001.json
    private static final Long ITEM_87 = 87L; // An Item for Asset 1, that is returned in the Mock Response Json
    private static final Long ITEM_61462 = 61462L;
    private static final Long SERVICE_101 = 101L; // A Service Id, that is returned in the Mock Response Json

    /**
     * Test that the mock rules engine responds. This doesn't really test anything not already covered, but should make
     * it easier to debug the other tests if they fail due to the mock not responding.
     */
    @Test
    public final void testMockDroolsResponse()
    {
        final StringBuilder url = new StringBuilder();
        url.append(MOCK_DROOLS_TASK_PATH);
        url.append("tasksForAsset?assetId=");
        url.append(ASSET_1_ID);
        assertNotNull(TestUtils.getResponse(url.toString(), HttpStatus.OK).getBody());
    }

    @Test
    @Ignore //ITEM_61462 no longer valid
    public final void updateAssetAndSyncTasks()
            throws IllegalAccessException, DatabaseUnitException, IOException, InstantiationException, SQLException, ClassNotFoundException
    {
        clearOutExistingTasks(ITEM_61462);

        TestUtils.postResponse(format(DraftItemRestTest.CHECK_OUT, ASSET_1_ID), "", HttpStatus.OK);
        TestUtils.postResponse(format(DraftItemRestTest.CHECK_IN, ASSET_1_ID), "", HttpStatus.OK);

        checkTasksHaveBeenCreated(ITEM_61462);
    }

    @Test
    public void updateServiceAndSyncTasks()
    {
        clearOutExistingTasks(ITEM_87);

        // Get Scheduled Service 101, the is no single GET API hence the below.
        ScheduledServiceDto scheduledServiceDto101 = null;
        final ScheduledServiceDto[] scheduledServices = TestUtils.getResponse(
                format(ServiceRestTest.BASE_ASSET_SERVICE, ASSET_1_ID, HttpStatus.OK)).as(ScheduledServiceDto[].class);

        for (final ScheduledServiceDto scheduledServiceDto : scheduledServices)
        {
            if (SERVICE_101.equals(scheduledServiceDto.getId()))
            {
                scheduledServiceDto101 = scheduledServiceDto;
                break;
            }
        }
        assertNotNull(scheduledServiceDto101);

        TestUtils.putResponse(format(ServiceRestTest.SPECIFIC_ASSET_SERVICE, ASSET_1_ID, SERVICE_101), scheduledServiceDto101, HttpStatus.OK);
        checkTasksHaveBeenCreated(ITEM_87);
    }

    /************************************
     * Helpers.....
     ************************************/

    private void clearOutExistingTasks(final Long itemId)
    {
        // Clear out any existing tasks against the provided item, in case any have been indirectly added by prior tests
        final WorkItemPageResourceDto pageResourceDto = TestUtils.postResponse(TASK_QUERY, getQueryDto(itemId), HttpStatus.OK,
                WorkItemPageResourceDto.class);

        final List<WorkItemLightDto> beforeTasks = pageResourceDto.getContent();
        if (beforeTasks != null && !beforeTasks.isEmpty())
        {
            final List<Long> ids = beforeTasks.stream().map(WorkItemLightDto::getId).collect(Collectors.toList());
            try
            {
                DataUtils.purgeRogueTasks(ids);
            }
            catch (final IllegalAccessException | ClassNotFoundException | SQLException | InstantiationException | IOException
                    | DatabaseUnitException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    private void checkTasksHaveBeenCreated(final Long itemId)
    {
        final WorkItemPageResourceDto afterPageResourceDto = TestUtils.postResponse(TASK_QUERY, getQueryDto(itemId), HttpStatus.OK,
                WorkItemPageResourceDto.class);

        final List<WorkItemLightDto> afterTasks = afterPageResourceDto.getContent();

        assertNotNull(afterTasks);
        assertTrue(!afterTasks.isEmpty());
    }

    private WorkItemQueryDto getQueryDto(final Long itemId)
    {
        final WorkItemQueryDto taskQuery = new WorkItemQueryDto();
        taskQuery.setItemId(Collections.singletonList(itemId));
        return taskQuery;
    }
}
