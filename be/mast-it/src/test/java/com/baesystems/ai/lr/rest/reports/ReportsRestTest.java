package com.baesystems.ai.lr.rest.reports;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.time.DateUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.pages.ReportWithContentDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.codicils.ActionableItemRestTest;
import com.baesystems.ai.lr.rest.codicils.AssetNoteRestTest;
import com.baesystems.ai.lr.rest.codicils.CoCRestTest;
import com.baesystems.ai.lr.rest.defects.DefectRestTest;
import com.baesystems.ai.lr.rest.jobs.JobRestTestHelper;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
public class ReportsRestTest extends BaseRestTest
{
    private static final String BASE_JOB = new StringBuffer().append(BASE_PATH).append("/job").toString();
    private static final String SPECIFIC_JOB = new StringBuffer().append(BASE_PATH).append("/job/%d").toString();
    private static final String SURVEY = new StringBuffer().append(SPECIFIC_JOB).append("/survey").toString();
    private static final String BASE_REPORT = new StringBuffer().append(SPECIFIC_JOB).append("/report").toString();
    private static final String SPECIFIC_REPORT = new StringBuffer(BASE_REPORT).append("/%d").toString();
    private static final String POST_REPORT = new StringBuffer(SPECIFIC_JOB).append("/report").toString();
    private static final String BASE_DEFECT = new StringBuilder(BASE_PATH).append("/defect").toString();
    private static final String BASE_REPAIR = new StringBuilder(BASE_DEFECT).append("/%d/repair").toString();

    private static final Long CASE_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long JOB_STATUS_1_ID = 1L;

    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long REPORT_1_ID = 1L;
    private static final Long EMPLOYEE_1_ID = 1L;
    private static final Long SERVCIE_CATALOGUE_1_ID = 1L;
    private static final Long NON_EXISTENT_JOB_888777_ID = 888777L;
    private static final Long NON_EXISTENT_REPORT_999888_ID = 999888L;

    private static final Long CONFIDENTIALITY_3 = 3L;

    private static final Long DEFECT_CATEGORY = 3L;

    private static final Long ONE = 1L;

    private static Date date;

    private static final int FUTURE_IMPOSED = 1;

    private ReportDto reportDto;
    private ReportLightDto[] reportVersions;

    private ReportContentDto correctContent;

    @Autowired
    private final JobRestTestHelper jobRestTestHelper = new JobRestTestHelper();

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        date = DateUtils.ceiling(new Date(), Calendar.DATE);
        this.reportDto = DataUtils.getReport(REPORT_1_ID);
        this.reportVersions = new ReportLightDto[]{DataUtils.getReportVersion(REPORT_1_ID)};
    }

    /**
     * Test that a valid GET request for ../report/{reportId} results in a positive response. Expecting report 1.
     */
    @Ignore
    public void getSingleReport()
    {
        TestUtils.singleItemFound(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), this.reportDto, true);
    }

    /**
     * Test a GET request for ../report/{reportId} results in an appropriate error when the report ID doesn't exist
     */
    @Test
    public void getReportWithInvalidReportId()
    {
        TestUtils.singleItemNotFound(format(SPECIFIC_REPORT, JOB_1_ID, NON_EXISTENT_REPORT_999888_ID));
    }

    /**
     * Test a GET request for ../report/{reportId} results in an appropriate error when the report ID relates to a job
     * different from the job id supplied. Check the error message is correct too.
     */
    @Test
    public void getReportWithIncorrectJobId()
    {
        final ErrorMessageDto error = TestUtils.getResponse(format(SPECIFIC_REPORT, NON_EXISTENT_JOB_888777_ID, REPORT_1_ID), HttpStatus.NOT_FOUND,
                ErrorMessageDto.class);
        assertEquals(
                format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Report", REPORT_1_ID, "Job", NON_EXISTENT_JOB_888777_ID),
                error.getMessage());
    }

    @Test
    public final void getReportVersions()
    {
        // Technically not Ref Data, but the output is essentially the same
        TestUtils.referenceDataListFound(format(BASE_REPORT, JOB_1_ID), this.reportVersions, true);
    }

    /**
     * Test a POST to ../report/{reportId} results in an appropriate error when the report ID relates to a job different
     * from the job id supplied. We also need to check that the report hasn't actually been updated even if we do get
     * the error message, since the error message might have come from the GET request we do after having performed the
     * PUT.
     */
    @Ignore
    public final void updateReportWithIncorrectJobId()
    {
        // get the current report for report 1 from using the correct job ID (1)...
        final ReportDto originalReportDto = TestUtils.getResponse(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), HttpStatus.OK, ReportDto.class);
        // ...make a minor change...
        final String originalClassRecommendation = originalReportDto.getClassRecommendation();
        final String newClassRecommendation = originalClassRecommendation + "TESTUPDATE";
        final ReportDto reportDtoToPut = originalReportDto;
        reportDtoToPut.setClassRecommendation(newClassRecommendation);
        // ...PUT the changed DTO to the endpoint using the incorrect job ID...
        final ErrorMessageDto error = TestUtils
                .putResponse(format(SPECIFIC_REPORT, JOB_2_ID, REPORT_1_ID), reportDtoToPut, HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        final ReportDto reportDtoAfterAttemptedUpdate = TestUtils.getResponse(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), HttpStatus.OK,
                ReportDto.class);
        // ...and check we get an error message and that the report has not been updated.
        assertEquals(format(ExceptionMessagesUtils.SPECIFIC_COMBINATION_NOT_FOUND_ERROR_MSG, "Report", REPORT_1_ID, "Job", JOB_2_ID),
                error.getMessage());
        final String latestClassRecommendation = reportDtoAfterAttemptedUpdate.getClassRecommendation();
        assertEquals(originalClassRecommendation, latestClassRecommendation); // check
                                                                              // that
                                                                              // no
                                                                              // update
                                                                              // has
                                                                              // happened.
    }

    @Test
    public final void unableToCreateFSR()
    {
        final LinkResource job = this.createNewJob();
        final ReportLightDto newReport = createReport(ReportType.FSR, job, null);

        // post uncommitted FAR
        final ErrorMessageDto message = TestUtils.postResponse(format(POST_REPORT, job.getId()), newReport, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertNotNull("exception", message);
    }

    @Test
    public final void testFarLifecyle() throws UnsupportedEncodingException, Exception
    {
        final LinkResource job = this.createNewJob();

        this.correctContent = new ReportContentDto();

        testLifecyle(ReportType.FAR, job);
    }

    @Test
    public final void testFullLifecyle() throws Exception
    {
        final LinkResource job = this.createNewJob();
        final ReportLightDto newReport = createReport(ReportType.DSR, job, null);

        // post uncommitted DSR should FAIL, as no
        final ErrorMessageDto error = TestUtils.postResponse(format(POST_REPORT, job.getId()), newReport, HttpStatus.CONFLICT).jsonPath()
                .getObject("", ErrorMessageDto.class);
        assertNotNull(error);

        // create report content (Asset note, Coc, Certificate and survey) before creating a FAR
        this.correctContent = createContent(job);

        testLifecyle(ReportType.FAR, job);
        testLifecyle(ReportType.DSR, job);
        testLifecyle(ReportType.FSR, job);
    }

    @Test
    public void updateReportFailsWhenOldTokenIsUsed()
    {
        final ReportDto originalReportDto = TestUtils.getResponse(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), HttpStatus.OK)
                .jsonPath()
                .getObject("", ReportDto.class);

        final String orginalStalenessHash = originalReportDto.getStalenessHash();

        originalReportDto.setNotes(originalReportDto.getNotes() + "X");

        // Perform a successful update
        final ReportDto reportDtoAfterUpdate = TestUtils
                .putResponse(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), originalReportDto, HttpStatus.OK)
                .jsonPath()
                .getObject("", ReportDto.class);

        // Perform an update with an old hash, and expect a fail.
        reportDtoAfterUpdate.setStalenessHash(orginalStalenessHash);
        reportDtoAfterUpdate.setNotes(reportDtoAfterUpdate.getNotes() + "X");

        final ErrorMessageDto error = TestUtils
                .putResponse(format(SPECIFIC_REPORT, JOB_1_ID, REPORT_1_ID), reportDtoAfterUpdate, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.REPORT.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    private ReportWithContentDto testLifecyle(final ReportType reportType, final LinkResource job) throws UnsupportedEncodingException, Exception
    {
        final ReportLightDto newReport = createReport(reportType, job, null);

        // post report
        final ReportWithContentDto version1 = TestUtils.postResponse(format(POST_REPORT, job.getId()), newReport, HttpStatus.OK,
                ReportWithContentDto.class);

        assertNotNull("id should not be null", version1.getId());
        assertNotNull("report version should not be null", version1.getReportVersion());

        final ReportContentDto content = version1.getContent();

        final String[] ignoredFields = {"wipAssetNotes[]dueDate", "wipAssetNotes[]actionTakenDate", "wipAssetNotes[]stalenessHash",
                                        "wipCocs[]dueDate", "wipCocs[]actionTakenDate", "wipCocs[]stalenessHash",
                                        "wipActionableItems[]dueDate", "wipActionableItems[]actionTakenDate", "wipActionableItems[]stalenessHash",
                                        "surveys[]dueDate", "surveys[]actionTakenDate", "surveys[]stalenessHash",
                                        "tasks[]dueDate", "tasks[]actionTakenDate"};

        TestUtils.compareFields(content, this.correctContent, true, ignoredFields);

        // post version 2
        final ReportWithContentDto version2 = TestUtils.postResponse(format(POST_REPORT, job.getId()), newReport, HttpStatus.OK,
                ReportWithContentDto.class);

        assertNotNull("id should not be null", version2.getId());
        assertNotNull("report version should not be null", version2.getReportVersion());

        assertEquals((Long) (version1.getReportVersion() + 1L), version2.getReportVersion());

        return version2;
    }

    private ReportContentDto createContent(final LinkResource job)
    {
        final ReportContentDto reportContent = new ReportContentDto();
        reportContent.setWipAssetNotes(new ArrayList<>(singletonList(createAssetNote(job, ASSET_2_ID, UUID.randomUUID().toString()))));
        reportContent.setWipCocs(new ArrayList<>(singletonList(createCoC(job, ASSET_2_ID, UUID.randomUUID().toString()))));
        reportContent.setWipActionableItems(new ArrayList<>(singletonList(createActionableItem(job, ASSET_2_ID, UUID.randomUUID().toString()))));
        reportContent.setSurveys(new ArrayList<>(singletonList(createSurvey(job, SERVCIE_CATALOGUE_1_ID, UUID.randomUUID().toString()))));

        return reportContent;
    }

    private ReportLightDto createReport(final ReportType reportTypeValue, final LinkResource jobReference,
            final Long version)
    {
        final ReportLightDto newReport = new ReportLightDto();
        newReport.setId(null);
        newReport.setJob(jobReference);
        newReport.setReportVersion(version);
        newReport.setReportType(new LinkResource(reportTypeValue.value()));
        newReport.setIssuedBy(new LinkResource(EMPLOYEE_1_ID));
        newReport.setReferenceCode("REFERENCE CODE");
        newReport.setClassRecommendation("Report Class Recomendation is " + UUID.randomUUID().toString());

        return newReport;
    }

    private AssetNoteDto createAssetNote(final LinkResource job, final Long assetId, final String description)
    {
        final AssetNoteDto note = new AssetNoteDto();
        setAssetNoteMandatoryFields(note);
        note.setActionTaken("actionTaken");
        note.setAffectedItems("affectedItems");
        note.setDescription(description);
        note.setDueDate(date);
        note.setJob(job);
        note.setEmployee(makeLinkResource(1L));
        note.setStatus(makeLinkResource(1L));
        note.setInheritedFlag(false);
        note.setAsset(makeLinkResource(assetId));
        note.setTemplate(makeLinkResource(1L));
        note.setJobScopeConfirmed(Boolean.TRUE);
        note.setActionTakenDate(new Date());

        final AssetNoteDto body = TestUtils.postResponse(format(AssetNoteRestTest.JOB_WIP_ASSET_NOTE, job.getId()), note, HttpStatus.OK,
                AssetNoteDto.class);
        return body;
    }

    private CoCDto createCoC(final LinkResource job, final Long assetId, final String description)
    {
        final CoCDto coc = new CoCDto();
        coc.setActionTaken("cocActionTaken");
        coc.setAffectedItems("cocAffectedItems");
        coc.setTemplate(makeLinkResource(1L));
        coc.setDescription(description);
        coc.setDueDate(date);
        coc.setImposedDate(date);
        coc.setEmployee(makeEmployee(1L));
        coc.setStatus(makeLinkResource(1L));
        coc.setAsset(makeLinkResource(assetId));
        coc.setJob(job);
        coc.setDefect(null);
        coc.setInheritedFlag(false);
        coc.setRequireApproval(Boolean.FALSE);
        coc.setJobScopeConfirmed(Boolean.TRUE);
        coc.setActionTakenDate(new Date());

        coc.setTitle("! NULL && 30 >= length >= 1.");
        coc.setDescription("! NULL && 2000 >= length >= 1");
        coc.setConfidentialityType(new LinkResource(ONE));
        coc.setRequireApproval(false);
        coc.setCategory(new LinkResource(ONE));

        final CoCDto body = TestUtils.postResponse(format(CoCRestTest.JOB_WIP_COC, job.getId()), coc, HttpStatus.OK, CoCDto.class);
        return body;
    }

    private ActionableItemDto createActionableItem(final LinkResource job, final Long assetId,
            final String description)
    {
        final ActionableItemDto ai = new ActionableItemDto();
        ai.setDescription(description);
        ai.setDueDate(date);
        ai.setImposedDate(date);
        ai.setEmployee(new LinkResource(1L));
        ai.setStatus(new LinkResource(1L));
        ai.setAsset(makeLinkResource(assetId));
        ai.setJob(job);
        ai.setDefect(null);
        ai.setRequireApproval(Boolean.FALSE);
        ai.setJobScopeConfirmed(Boolean.TRUE);
        ai.setActionTakenDate(new Date());

        ai.setTitle("! NULL && 30 >= length >= 1.");
        ai.setDescription("! NULL && 2000 >= length >= 1");
        ai.setConfidentialityType(new LinkResource(ONE));
        ai.setRequireApproval(false);
        ai.setCategory(new LinkResource(ONE));

        final ActionableItemDto body = TestUtils.postResponse(format(ActionableItemRestTest.WIP_ACTIONABLE_ITEM, job.getId()), ai, HttpStatus.OK,
                ActionableItemDto.class);
        return body;
    }

    private DefectDto createDefect(final LinkResource job, final Long assetId, final String description)
    {
        final DefectDto defect = new DefectDto();

        defect.setJobs(new ArrayList<JobDefectDto>());
        defect.getJobs().add(new JobDefectDto());
        defect.getJobs().get(0).setJob(job);
        defect.setAsset(makeLinkResource(assetId));
        defect.setTitle(description);
        defect.setDefectStatus(makeLinkResource(1L));
        defect.setDefectCategory(makeDefectCategory(DEFECT_CATEGORY));
        defect.setConfidentialityType(makeLinkResource(CONFIDENTIALITY_3));

        final DefectDto body = TestUtils.postResponse(DefectRestTest.BASE_DEFECT, defect, HttpStatus.OK, DefectDto.class);
        return body;
    }

    private RepairDto createRepair(final Long defectId)
    {
        final RepairDto repair = new RepairDto();

        repair.setRepairAction(makeLinkResource(1L));
        repair.setDefect(makeLinkResource(defectId));
        repair.setConfirmed(false);
        repair.setPromptThoroughRepair(false);
        repair.setMaterialsUsed(new ArrayList<LinkResource>());
        repair.setRepairTypes(new ArrayList<LinkResource>());
        repair.setRepairs(new ArrayList<RepairItemDto>());
        repair.setDescription("Some description for this repair.");

        final RepairDto body = TestUtils.postResponse(format(BASE_REPAIR, defectId), repair, HttpStatus.OK, RepairDto.class);
        return body;
    }

    private SurveyDto createSurvey(final LinkResource job, final Long scId, final String description)
    {
        final SurveyDto survey = new SurveyDto();
        survey.setJob(job);
        survey.setServiceCatalogue(new LinkResource(scId));
        survey.setNarrative(description);
        survey.setJobScopeConfirmed(true);
        survey.setScheduleDatesUpdated(true);
        survey.setActionTakenDate(new Date());

        final SurveyDto body = TestUtils.postResponse(format(SURVEY, job.getId()), survey, HttpStatus.OK, SurveyDto.class);
        return body;
    }

    private LinkResource createNewJob()
    {
        // create a new job so tests can be re-ran
        final JobDto job = this.jobRestTestHelper.createSkeletonJob(ASSET_2_ID, CASE_1_ID, JOB_STATUS_1_ID);
        final JobDto newJob = TestUtils.postResponse(BASE_JOB, job, HttpStatus.OK, JobDto.class);

        // report template
        return new LinkResource(newJob.getId());
    }

    private LinkResource makeLinkResource(final Long subId)
    {
        final LinkResource resource = new LinkResource(subId);

        return resource;
    }

    private LrEmployeeDto makeEmployee(final Long subId)
    {
        final LrEmployeeDto resource = new LrEmployeeDto();
        resource.setId(subId);

        return resource;
    }

    private DefectCategoryDto makeDefectCategory(final Long subId)
    {
        final DefectCategoryDto resource = new DefectCategoryDto();
        resource.setId(subId);

        return resource;
    }

    private void setAssetNoteMandatoryFields(final AssetNoteDto assetNoteDto)
    {
        assetNoteDto.setCategory(new LinkResource(ONE));
        assetNoteDto.setConfidentialityType(new LinkResource(ONE));
        assetNoteDto.setTitle("This is a test title.");
        assetNoteDto.setImposedDate(DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DATE), FUTURE_IMPOSED));
    }
}
