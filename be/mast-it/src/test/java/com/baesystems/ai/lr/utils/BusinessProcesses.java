package com.baesystems.ai.lr.utils;

import java.util.Collections;
import java.util.stream.Stream;

import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.assets.DraftItemRestTest;

public final class BusinessProcesses extends BaseRestTest
{
    public static void checkAssetOut(final Long assetId)
    {
        if (!isCheckedOut(assetId))
        {
            TestUtils.postResponse(String.format(CHECKOUT_ASSET, assetId), "", HttpStatus.OK);
        }
    }

    public static void checkAssetIn(final Long assetId)
    {
        if (isCheckedOut(assetId))
        {
            TestUtils.postResponse(String.format(CHECKIN_ASSET, assetId), "", HttpStatus.OK);
        }
    }

    private static boolean isCheckedOut(final Long assetId)
    {
        final AssetMetaDto[] assetMetaDtos = TestUtils.getResponse(ASSETS_CHECKED_OUT_BY_USER).as(AssetMetaDto[].class);

        return Stream
                .of(assetMetaDtos)
                .map(AssetMetaDto::getId)
                .anyMatch(id -> id.equals(assetId));
    }

    public static LazyItemDto copyFullAssetModel(final Long sourceAssetId, final AssetDto targetAsset)
    {
        final Long targetAssetId = targetAsset.getId();

        final LazyItemDto existingRootItem = TestUtils.getResponse(String.format(ROOT_ITEM, sourceAssetId), HttpStatus.OK).as(LazyItemDto.class);
        final LinkVersionedResource itemToCopy = existingRootItem.getItems().get(0); // First item (not root)

        BusinessProcesses.checkAssetOut(targetAssetId);

        final DraftItemDto draftItemDto = new DraftItemDto();
        draftItemDto.setFromAssetId(sourceAssetId);
        draftItemDto.setFromId(itemToCopy.getId());
        draftItemDto.setToAssetId(targetAssetId);
        draftItemDto.setToParentId(targetAsset.getAssetModel().getItems().get(0).getId()); // Auto-created root item

        final DraftItemListDto draftItemListDto = new DraftItemListDto();
        draftItemListDto.setDraftItemDtoList(Collections.singletonList(draftItemDto));
        draftItemListDto.setCopyAttributes(true);

        final LazyItemDto rootItem = TestUtils.postResponse(String.format(DraftItemRestTest.BASE_DRAFT_ITEM, targetAssetId), draftItemListDto,
                HttpStatus.OK,
                LazyItemDto.class);

        BusinessProcesses.checkAssetIn(targetAssetId);

        // Return the root item on the new Asset, which now references a new child
        return rootItem;
    }
}
