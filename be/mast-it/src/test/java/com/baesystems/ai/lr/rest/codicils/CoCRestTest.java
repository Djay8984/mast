package com.baesystems.ai.lr.rest.codicils;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.CoCPage;
import com.baesystems.ai.lr.pages.SurveyPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DtoUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.utils.TestUtils;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class CoCRestTest extends BaseRestTest
{
    private static final SimpleDateFormat DATE_FORMATTER = DateHelper.mastFormatter();

    /**
     * API Rest Paths
     */
    private static final String ASSET_COC = urlBuilder(SPECIFIC_ASSET, "/coc");
    private static final String BASE_JOB = urlBuilder(BASE_PATH, "/job/%d");
    public static final String JOB_WIP_COC = urlBuilder(BASE_JOB, "/wip-coc");
    private static final String ASSET_COC_WITH_STATUS = urlBuilder(ASSET_COC, "?statusId=%d");
    private static final String COC_QUERY = urlBuilder(ASSET_COC, "/query");
    private static final String DELETE_ASSET_COC = urlBuilder(ASSET_COC, "/%d");
    public static final String JOB_WIP_COC_ITEM = urlBuilder(JOB_WIP_COC, "/%d");
    private static final String WIP_COC_QUERY = urlBuilder(JOB_WIP_COC, "/query");
    private static final String WIP_COC_REPAIR = urlBuilder(JOB_WIP_COC_ITEM, "/wip-repair");
    public static final String DEFECT_COC = urlBuilder(SPECIFIC_ASSET, "/defect/%d/coc");
    private static final String JOB_SURVEY = urlBuilder(BASE_PATH, "/job/%d/survey");
    private static final String SPECIFIC_JOB_SURVEY = urlBuilder(JOB_SURVEY, "/%d");
    private static final String BASE_JOB_WIP_DEFECT_WIP_COC = urlBuilder(BASE_JOB, "/wip-defect/%d/wip-coc");

    /**
     * Test Data
     */
    private static final Long NEGATIVE = -1L;
    private static final Long ZERO = 0L;
    private static final Long INVALID_ID = 8888L;
    private static final Long COC_1_ID = 1L;
    private static final Long COC_21_ID = 21L;
    private static final Long WIP_COC_1_ID = 1L;
    private static final Long WIP_COC_44_ID = 44L;
    private static final Long WIP_COC_45_ID = 45L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long JOB_3_ID = 3L;
    private static final Long STATUS_1_ID = 1L;
    private static final Long STATUS_2_ID = 2L;
    private static final Long INVALID_COC_ID = 9999L;
    private static final Long NOT_ASSOCIATED_COC_ID = 4L;
    private static final Long DELETE_THIS_COC_ID = 11L;
    private static final Long VALID_DEFECT_ID_FOR_COCS = 6L;
    private static final Long INVALID_DEFECT_ID_FOR_COCS = 9999L;
    private static final Long WIP_COC_ID_FOR_JOB_AND_WIP_DEFECT = 25L;
    private static final Long INVALID_WIP_DEFECT_ID = 999L;
    private static final Long VALID_JOB_ID_FOR_WIP_DEFECT = 1L;
    private static final Long VALID_WIP_DEFECT_ID_4_FOR_JOB_1 = 4L;
    private static final Long VALID_WIP_DEFECT_ID_6_FOR_JOB_1 = 6L;
    private static final Long WIP_DEFECT_ID_WITH_NO_WIP_COC = 5L;
    private static final Long WIP_DEFECT_11_ID = 11L;
    private static final Long INVALID_ASSET_ITEM_ID = 9L;
    private static final Long TEMPLATE_ID = 183L;
    private static final int FUTURE_DUE = 9;
    private static final int FUTURE_IMPOSED = 1;
    private static final Long EXISTING_ASSET_NOTE_ID = 6L;
    private static final Long PERMANENT_REPAIR_ACTION = 1L;
    private static final Long REPAIR_TYPE_1 = 1L;
    private static final Long CONFIDENTIALITY_RETURN_NOTHING = -1L;
    private static final Long STATUS_RETURN_NOTHING = -1L;
    private static final Long ONE = 1L;
    private static final Long AMSC_SERVICE_CAT_ID = 2L;

    // Query
    private static final Long ASSET_2 = 2L;
    private static final Long COC_3 = 3L;
    private static final Long WIP_COC_20 = 20L;
    private static final Long ITEM_TYPE_2 = 2L;
    private static final Long ITEM_TYPE_4 = 4L;
    private static final Long ITEM_TYPE_327 = 327L;
    private static final Long CATEGORY_ID_1 = 1L;
    private static final Date TODAY = new Date();
    private static final Date A_LONG_TIME_AGO = new Date(0);
    private static final Long CONFIDENTIALITY_1 = 1L;
    private static final Long COC_DELETED_STATUS_ID = CodicilStatusType.COC_DELETED.getValue();
    private static final Long HULL_CODICIL_CATEGORY_ID = 7L;
    private static final Long MACHINERY_CODICIL_CATEGORY_ID = 8L;

    private static final Integer QUERY_TEST_SIZE = 12;

    private CoCDto coCDto1;
    private CoCDto wipCoCDto1;
    private CoCPage coCPage;

    private static final SimpleDateFormat YYYY_MM_DD_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    // Needs to be public for Theory to work.
    @DataPoints
    public static final String[] BAD_REQUESTS = {String.format(ASSET_COC, NEGATIVE), String.format(ASSET_COC, ZERO),
                                                 BASE_PATH + "/asset" + "/SomeLetters" + "/coc", BASE_PATH + "/asset" + "/SomeLetters",
                                                 String.format(JOB_WIP_COC, NEGATIVE), String.format(JOB_WIP_COC, ZERO),
                                                 BASE_PATH + "/job" + "/SomeLetters" + "/wip-coc", BASE_PATH + "/job" + "/SomeLetters"};

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        this.coCDto1 = DataUtils.getCoC(COC_1_ID, false);
        this.wipCoCDto1 = DataUtils.getCoC(COC_1_ID, true);
    }

    private void initialiseCoCPage(final Boolean wip, final Long... ids)
    {
        this.coCPage = new CoCPage();

        CoCDto tempCoC;
        for (final Long id : ids)
        {
            try
            {
                tempCoC = DataUtils.getCoC(id, wip);
                this.coCPage.getContent().add(tempCoC);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    @DataPoints("NON_WIP_QUERIES")
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints("WIP_QUERIES")
    public static final QueryAndResult[] WIP_QUERIES = new QueryAndResult[QUERY_TEST_SIZE - 1];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        // Search description
        QUERIES[index] = new QueryAndResult("searchString", "Desc 3", new Long[]{COC_3});
        ++index;
        // Search title
        QUERIES[index] = new QueryAndResult("searchString", "3title", new Long[]{COC_3});
        ++index;
        // Search Surveyor guidance
        QUERIES[index] = new QueryAndResult("searchString", "sur3", new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1),
                new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_2_ID), new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_4),
                new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1),
                new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{COC_3});
        ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList",
                Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING),
                new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327), new Long[]{});

        index = 0;
        // Search description
        WIP_QUERIES[index] = new QueryAndResult("searchString", "*COC*", new Long[]{WIP_COC_20});
        ++index;
        // Search title
        WIP_QUERIES[index] = new QueryAndResult("searchString", "WIPC*", new Long[]{WIP_COC_20});
        ++index;
        // Search Surveyor guidance
        WIP_QUERIES[index] = new QueryAndResult("searchString", "SurvW*", new Long[]{WIP_COC_20});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1),
                new Long[]{WIP_COC_20});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_2_ID),
                new Long[]{WIP_COC_20});
        ++index;
        // WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_2),
        // new Long[]{WIP_COC_20});
        // ++index;
        WIP_QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1),
                new Long[]{WIP_COC_20});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{WIP_COC_20});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{WIP_COC_20});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("confidentialityList",
                Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING),
                new Long[]{});
        ++index;
        WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327),
                new Long[]{});
    }

    /**
     * Test that a valid GET request for ../asset/x/coc/y results in a positive response. Expecting JSON containing coc
     * 1.
     */
    @Test
    public final void testGetCoC()
    {
        TestUtils.singleItemFound(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), this.coCDto1, true);
    }

    /**
     * Confirms that one of the known associated WIP elements is in the dto.
     */
    @Test
    public final void testGetCoCConfirmWIPChildrenPresent()
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        boolean containsExpectedChild = false;
        for (final LinkResource link : coCDto.getChildWIPs())
        {
            if (link.getId().equals(WIP_COC_1_ID))
            {
                containsExpectedChild = true;
                break;
            }
        }

        Assert.assertTrue(containsExpectedChild);
    }

    /**
     * Test that an invalid GET request for ../asset/x/coc/y results in a positive response. Expecting not found
     * exception for a non-existent coc
     */
    @Test
    public final void testGetCoCFails()
    {
        TestUtils.singleItemNotFound(format(ASSET_COC.concat("/%d"), ASSET_1_ID, INVALID_ID));
    }

    /**
     * Test that a valid GET request for ../job/x/wip-coc/y results in a positive response. Expecting JSON containing
     * wip-coc 1.
     */
    @Test
    public final void testGetWIPCoC()
    {
        TestUtils.singleItemFound(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), this.wipCoCDto1, true);
    }

    /**
     * Test that an invalid GET request for ../job/x/wip-coc/y results in a positive response. Expecting not found
     * exception for a non-existent coc
     */
    @Test
    public final void testGetWIPCoCFails()
    {
        TestUtils.singleItemNotFound(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, INVALID_ID));
    }

    /**
     * Test that a valid POST request for ../asset/x/coc results in a positive response. Expecting JSON containing newly
     * saved coc.
     */
    @Test
    public final void testCreateCoC()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "stalenessHash"};
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(false);

        final CoCDto body = TestUtils.postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        TestUtils.compareFields(body, coCDto, false, ignoredFields);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in a complaint about an old imposed date
     */
    @Test
    public final void testCreateCoCFailsWithAnHistoricalImposedDate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);
        coCDto.setId(null);
        final Date imposedDate = DateUtils.addMonths(new Date(), -1);
        coCDto.setImposedDate(imposedDate);

        final String errorMessage = TestUtils
                .postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.BAD_REQUEST).jsonPath().get("message");
        assertEquals("Imposed date is in the past: " + YYYY_MM_DD_FORMAT.format(imposedDate) + ".", errorMessage);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in a complaint about a due date being before an
     * imposed date.
     */
    @Test
    public final void testCreateCoCFailsWithADueDateBeforeAnImposedDate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);
        coCDto.setId(null);
        final Date today = new Date();
        coCDto.setImposedDate(DateUtils.addMonths(today, 1));
        final Date dueDate = DateUtils.addDays(today, 1);
        coCDto.setDueDate(dueDate);

        final String errorMessage = TestUtils
                .postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.BAD_REQUEST).jsonPath().get("message");
        assertEquals("Due date is before imposed date: " + YYYY_MM_DD_FORMAT.format(dueDate), errorMessage);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in an error where the coc data does not match the
     * data in the template it is using.
     */
    @Test
    public final void testCreateCoCFailsAsTemplateDataDoesNotMatchCocData()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final LinkResource template = new LinkResource(TEMPLATE_ID);
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(false);
        coCDto.setTemplate(template);
        final String errorMessage = TestUtils.postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.CONFLICT)
                .jsonPath().get("message");
        assertEquals("The CoC entered does not match the template it is linked to.", errorMessage);
    }

    /**
     * Test that an valid POST request for ../asset/x/coc succeeds, where the passed CoC data must match its associated
     * template data.
     */
    @Test
    @Ignore("There are no CoC Templates in the current set of Reference Data")
    public final void testCreateCoCSucceedsWithMatchingTemplateData()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final LinkResource template = new LinkResource(TEMPLATE_ID);

        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(false);
        // Manually set the dto to contain the same information as the template
        // defined in the ref data.
        coCDto.setTemplate(template);
        coCDto.setTitle("Template1");
        coCDto.setDescription("This is the narrative");
        coCDto.setSurveyorGuidance("Notes for surveyor");
        coCDto.setEditableBySurveyor(Boolean.TRUE);
        final LinkResource assetItem = new LinkResource(INVALID_ASSET_ITEM_ID);
        coCDto.setAssetItem(assetItem);

        TestUtils.postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.OK);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc succeeds, where the item type id on the template being used
     * does not match the type on the CoC's asset item.
     */
    @Test
    @Ignore("There are no CoC Templates in the current set of Reference Data")
    public final void testCreateCoCFailsOnAnInvalidTemplateItemTypeId()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final LinkResource template = new LinkResource(TEMPLATE_ID);
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(false);

        // Manually set the dto to contain the same information as the template
        // defined in the ref data.
        coCDto.setTemplate(template);
        coCDto.setTitle("Template1");
        coCDto.setDescription("This is the narrative");
        coCDto.setSurveyorGuidance("Notes for surveyor");
        coCDto.setEditableBySurveyor(Boolean.TRUE);

        // Set an asset item that doesn't fit with the template's type id.
        final LinkResource assetItem = new LinkResource(ONE);
        coCDto.setAssetItem(assetItem);

        final String errorMessage = TestUtils.postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.CONFLICT)
                .jsonPath().get("message");
        assertEquals("Entity with id 1 is not of type 421.", errorMessage);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in a positive response. Expecting a bad request
     * exception.
     */
    @Test
    public final void testCreateCoCFailsWithIDPopulated()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        final String errorMessage = TestUtils
                .postResponse(format(ASSET_COC, ASSET_1_ID), coCDto, HttpStatus.BAD_REQUEST).jsonPath().get("message");
        assertEquals("Field ID is expected to be null in the creation of new entities: 1", errorMessage);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in a positive response. Expecting not found
     * exception, for an invalid assetId
     */
    @Test
    public final void testCreateCoCFailsAssetNotFound()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);
        final String errorMessage = TestUtils.postResponse(format(ASSET_COC, INVALID_ID), coCDto, HttpStatus.NOT_FOUND)
                .jsonPath().get("message");
        assertEquals("Entity not found for identifier: 8888.", errorMessage);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-coc fails when the parent id doesn't exist.
     */
    @Test
    public final void testCreateWIPCoCFailsWhenParentIdNotInDatabase()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final LinkResource parent = new LinkResource(INVALID_COC_ID);
        coCDto.setParent(parent);
        final String errorMessage = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.NOT_FOUND)
                .jsonPath().get("message");
        assertEquals(String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.",
                INVALID_COC_ID), errorMessage);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-coc fails when the parent is not of the same codicil type.
     */
    @Test
    public final void testCreateWIPCoCFailsWhenParentTypeDifferent()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final LinkResource parent = new LinkResource(EXISTING_ASSET_NOTE_ID);
        coCDto.setParent(parent);
        final String errorMessage = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.NOT_FOUND)
                .jsonPath().get("message");
        assertEquals(String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.",
                EXISTING_ASSET_NOTE_ID), errorMessage);
    }

    /**
     * Test that a valid POST request for ../job/x/wip-coc results in a positive response. Expecting JSON containing
     * newly saved wip-coc. This is a non-defect CoC with no parent CoC so should create a survey. See comments on
     * LED-2484 - surveys should be created for non-defect or inherited CoCs only.
     */
    @Test
    public final void testCreateWIPCoC()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {

        final String[] ignoredFields = {"id", "stalenessHash"};
        final String cocTitle = "New WIP CoC for survey creation";
        // check we haven't got a survey with this title before we create the
        // CoC
        assertNull(checkSingleCoCSurveyExistence(JOB_1_ID, cocTitle));
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        coCDto.setParent(null);
        coCDto.setTitle(cocTitle);
        final CoCDto body = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        TestUtils.compareFields(body, coCDto, false, ignoredFields);
        final SurveyDto survey = checkSingleCoCSurveyExistence(JOB_1_ID, cocTitle);
        assertNotNull(survey);
        // clear it up
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY, survey.getJob().getId(), survey.getId()),
                HttpStatus.OK);
    }

    /**
     * Test that a POST request for ../job/x/wip-coc with null job scope confirmed results in an error.
     */
    @Test
    public final void testCreateWIPCoCFailsNullJobScopeConfirmed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        coCDto.setJobScopeConfirmed(null);
        final ErrorMessageDto error = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    /**
     * Test that a valid POST request for ../job/x/wip-coc results in a positive response. Expecting JSON containing
     * newly saved wip-coc. As this CoC has a parent set we shouldn't be creating a survey.
     */
    @Test
    public final void testCreateWIPCoCSuppressingSurveyCreation()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {

        final String[] ignoredFields = {"id", "stalenessHash"};
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final String cocTitle = "New WIP CoC for suppressed survey";
        coCDto.setTitle(cocTitle);
        coCDto.setParent(new LinkResource(COC_1_ID));
        // check we haven't got a survey with this title before we create the
        // CoC
        assertNull(checkSingleCoCSurveyExistence(JOB_1_ID, cocTitle));
        final CoCDto body = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        TestUtils.compareFields(body, coCDto, false, ignoredFields);
        assertNull(checkSingleCoCSurveyExistence(JOB_1_ID, cocTitle));
    }

    /**
     * Utility method to test that a survey exists or doesn't exist, as required, against the given defect's job
     *
     * @param wipDefect
     * @param shouldExist
     */
    private SurveyDto checkSingleCoCSurveyExistence(final Long jobId, final String cocName)
    {
        final SurveyPage surveyPage = TestUtils.getResponse(String.format(JOB_SURVEY, jobId), HttpStatus.OK, SurveyPage.class);

        SurveyDto surveyFound = null;

        for (final SurveyDto survey : surveyPage.getContent())
        {
            if (cocName.equals(survey.getName()) && survey.getServiceCatalogue().getId().equals(AMSC_SERVICE_CAT_ID))
            {
                // there should only be one.
                assertNull(surveyFound);
                surveyFound = survey;
            }
        }

        return surveyFound;
    }

    /**
     * Tests an invalid POST request for ../job/x/wip-coc results in a positive response. Expecting JSON Bad Request
     * Exception.
     */
    @Test
    public final void testCreateWIPCoCFailsWithIDPopulated()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = TestUtils
                .getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../asset/x/coc results in a positive response. Expecting not found
     * exception, for an invalid assetId
     */
    @Test
    public final void testCreateWIPCoCFailsJobNotFound()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        TestUtils.postResponse(format(JOB_WIP_COC, INVALID_ID), coCDto, HttpStatus.NOT_FOUND);
    }

    /**
     * Tests valid GET ../asset/x/coc returns a List of valid CoC including Coc1 and CoC2
     */
    @Test
    public final void testGetCoCForAsset()
    {
        initialiseCoCPage(false, COC_1_ID);
        TestUtils.listItemsFound(String.format(ASSET_COC, ASSET_1_ID), this.coCPage, true, false);
    }

    /**
     * Tests valid GET ../asset/x/coc with Status parameter returns a List of valid CoC including Coc1
     */
    @Test
    public final void testGetCoCForAssetWithStatusFilterFound()
    {
        initialiseCoCPage(false, COC_1_ID);
        TestUtils.listItemsFound(String.format(ASSET_COC_WITH_STATUS, ASSET_1_ID, STATUS_1_ID), this.coCPage, true, false);
    }

    /**
     * Tests valid GET ../asset/x/coc with Status parameter and returns an empty list
     */
    @Test
    public final void testGetCoCForAssetWithStatusFilterNonFound()
    {
        TestUtils.listItemsNotFound(String.format(ASSET_COC_WITH_STATUS, ASSET_1_ID, STATUS_2_ID));
    }

    /**
     * Tests valid GET ../job/x/wip-coc returns a List of valid wip-CoCs including wipCoc1 and wipCoC2
     */
    @Test
    public final void testGetWIPCoCForJob()
    {
        initialiseCoCPage(true, WIP_COC_1_ID);
        TestUtils.listItemsFound(String.format(JOB_WIP_COC, JOB_1_ID), this.coCPage, true, false);
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-coc returns a valid WIP CoC.
     */
    @Test
    public final void testGetWIPCoCForJobAndWIPDefect()
    {
        initialiseCoCPage(true, WIP_COC_ID_FOR_JOB_AND_WIP_DEFECT);
        TestUtils.listItemsFound(String.format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1),
                this.coCPage,
                true,
                false);
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-coc returns an empty WIP-CoC list.
     */
    @Test
    public final void testGetWIPCoCForJobAndWIPDefectReturnsEmpty()
    {
        TestUtils.listItemsNotFound(
                String.format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, WIP_DEFECT_ID_WITH_NO_WIP_COC));
    }

    /**
     * Test invalid GET ../job/{x}/wip-defect/{y}/wip-coc.
     */
    @Test
    public final void testGetWIPCoCForJobAndWIPDefectFails()
    {
        TestUtils.getResponse(
                String.format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, INVALID_WIP_DEFECT_ID),
                HttpStatus.NOT_FOUND);
    }

    /**
     * Test valid POST ../job/{x}/wip-defect/{y}/wip-coc. This is an inherited defect CoC so should create a survey. See
     * comments on LED-2484 - surveys should be created for non-defect or inherited CoCs only.
     */
    @Test
    public final void testCreateInheritedWIPCoCForJobAndWIPDefect() throws DatabaseUnitException,
            IntegrationTestException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        final String[] ignoredFields = {"id", "dueStatus", "stalenessHash"};
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final String title = "Inherited Non Defect CoC";
        coCDto.setTitle(title);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setDefect(defect);
        coCDto.setInheritedFlag(Boolean.TRUE);
        assertNull(checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title));
        // get the version of the CoC from the response.
        final CoCDto responseBody = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                        HttpStatus.OK, CoCDto.class);
        final Long newId = responseBody.getId();
        // get the version of the CoC from the DB.
        final CoCDto newCoCFromDb = DataUtils.getCoC(newId, true);
        // both flavours of the DTO should match what we originally passed up.
        TestUtils.compareFields(responseBody, coCDto, false, ignoredFields);
        TestUtils.compareFields(newCoCFromDb, coCDto, false, ignoredFields);

        final SurveyDto survey = checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title);
        assertNotNull(survey);

        // clean up the data
        TestUtils.deleteResponse(String.format(SPECIFIC_JOB_SURVEY, survey.getJob().getId(), survey.getId()),
                HttpStatus.OK);
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, VALID_JOB_ID_FOR_WIP_DEFECT, newCoCFromDb.getId()),
                HttpStatus.OK);
    }

    /**
     * Test valid POST ../job/{x}/wip-defect/{y}/wip-coc. This is an inherited defect CoC but should not create a survey
     * as it has a parent. See comments on LRD-2484 - surveys should be created for non-defect or inherited CoCs only.
     */
    @Test
    public final void testCreateInheritedWIPCoCForJobAndWIPDefectSuppressingSurvey() throws DatabaseUnitException,
            IntegrationTestException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        final String[] ignoredFields = {"id", "dueStatus", "stalenessHash"};
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final String title = "Inherited Non Defect CoC for suppressed survey";
        coCDto.setTitle(title);
        coCDto.setParent(new LinkResource(COC_1_ID));
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setDefect(defect);
        coCDto.setInheritedFlag(Boolean.TRUE);
        assertNull(checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title));
        // get the version of the CoC from the response.
        final CoCDto responseBody = TestUtils
                .postResponse(
                        format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1),
                        coCDto,
                        HttpStatus.OK, CoCDto.class);
        final Long newId = responseBody.getId();
        // get the version of the CoC from the DB.
        final CoCDto newCoCFromDb = DataUtils.getCoC(newId, true);
        // both flavours of the DTO should match what we originally passed up.
        TestUtils.compareFields(responseBody, coCDto, false, ignoredFields);
        TestUtils.compareFields(newCoCFromDb, coCDto, false, ignoredFields);

        assertNull(checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title));

        // clean up the data
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, VALID_JOB_ID_FOR_WIP_DEFECT, newCoCFromDb.getId()),
                HttpStatus.OK);
    }

    /**
     * Test valid POST ../job/{x}/wip-defect/{y}/wip-coc. This is an non-inherited defect CoC so should not create a
     * survey. See comments on LED-2484 - surveys should be created for non-defect or inherited CoCs only.
     */
    @Test
    public final void testCreateNonInheritedWIPCoCForJobAndWIPDefect() throws DatabaseUnitException,
            IntegrationTestException, NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {

        final String[] ignoredFields = {"id", "dueStatus", "stalenessHash"};
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final String title = "Non-Inherited Non Defect CoC";
        coCDto.setTitle(title);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setDefect(defect);
        coCDto.setInheritedFlag(Boolean.FALSE);
        assertNull(checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title));
        // get the version of the CoC from the response.
        final CoCDto responseBody = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                        HttpStatus.OK, CoCDto.class);
        final Long newId = responseBody.getId();
        // get the version of the CoC from the DB.
        final CoCDto newCoCFromDb = DataUtils.getCoC(newId, true);
        // both flavours of the DTO should match what we originally passed up.
        TestUtils.compareFields(responseBody, coCDto, false, ignoredFields);
        TestUtils.compareFields(newCoCFromDb, coCDto, false, ignoredFields);

        assertNull(checkSingleCoCSurveyExistence(VALID_JOB_ID_FOR_WIP_DEFECT, title));

        // clean up the data
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, VALID_JOB_ID_FOR_WIP_DEFECT, newCoCFromDb.getId()),
                HttpStatus.OK);
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-coc due to mismatched defect IDs.
     */
    @Test
    public final void testCreateWIPCoCForJobAndWIPDefectDefectIdMismatch()
    {
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_6_FOR_JOB_1);
        coCDto.setDefect(defect);
        // get the version of the CoC from the response.
        final ErrorMessageDto error = TestUtils.postResponse(
                format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.BODY_ID_MISMATCH, "WIP defect", VALID_WIP_DEFECT_ID_6_FOR_JOB_1, VALID_WIP_DEFECT_ID_4_FOR_JOB_1),
                error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-coc due to defect not under given job
     */
    @Test
    public final void testCreateWIPCoCForJobAndWIPDefectWrongJob()
    {
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setDefect(defect);
        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_2_ID, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                HttpStatus.NOT_FOUND, ErrorMessageDto.class);
        assertEquals(ExceptionMessagesUtils.GENERAL_NOT_FOUND_ERROR_MSG, error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-coc due to null job scope confirmed
     */
    @Test
    public final void testCreateWIPCoCForJobAndWIPDefectNullJobScopeConfirmed()
    {
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        coCDto.setJobScopeConfirmed(null);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setDefect(defect);
        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_1_ID, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    /**
     * Test invalid POST ../job/{x}/wip-defect/{y}/wip-coc due to ID included in body
     */
    @Test
    public final void testCreateWIPCoCForJobAndWIPDefectInvalidId()
    {
        final CoCDto coCDto = getCoCDtoWithNoIdAndDueDates(true);
        final LinkResource defect = new LinkResource(VALID_WIP_DEFECT_ID_4_FOR_JOB_1);
        coCDto.setId(ONE);
        coCDto.setDefect(defect);
        // get the version of the CoC from the response.
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_4_FOR_JOB_1), coCDto,
                        HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.ID_NOT_NULL, ONE), error.getMessage());
    }

    /**
     * Tests valid PUT ../asset/x/coc/y updates coc and returns the updated value
     */
    @Test
    public final void testUpdateCoC()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        final String newDescription = "new Description";
        originalCoCDto.setDescription(newDescription);

        final CoCDto updatedCto = TestUtils.putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto, HttpStatus.OK,
                CoCDto.class);

        assertEquals(originalCoCDto.getId(), updatedCto.getId());
        assertEquals(updatedCto.getDescription(), newDescription);
    }

    @Test
    public final void testUpdateCoCFailsWhenStaleTokenIsDifferent()
    {
        final CoCDto originalCoCDto = TestUtils.getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK)
                .getBody()
                .as(CoCDto.class);

        originalCoCDto.setDescription(originalCoCDto.getDescription() + "X");
        originalCoCDto.setStalenessHash("Incorrect Hash Data");

        final ErrorMessageDto error = TestUtils
                .putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.COC.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public final void testUpdateCoCFailsWhenOldTokenIsUsed()
    {
        final CoCDto originalCoCDto = TestUtils.getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK)
                .getBody()
                .as(CoCDto.class);

        final String originalStalenessHash = originalCoCDto.getStalenessHash();

        // Make a small change
        originalCoCDto.setDescription(originalCoCDto.getDescription() + "X");

        final CoCDto updatedCto = TestUtils.putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto, HttpStatus.OK,
                CoCDto.class);

        // Make another small change, but use the hash from the original GET
        updatedCto.setDescription(updatedCto.getDescription() + "X");
        updatedCto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), updatedCto, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.COC.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    /**
     * Tests invalid PUT ../asset/x/coc/y fails to update CoC when we attempt to change immutable field title
     */
    @Test
    public final void testUpdateCoCFailsChangedTitle()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        final String originalTitle = originalCoCDto.getTitle();
        final String newTitle = "New Title";
        originalCoCDto.setTitle(newTitle);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(MessageFormat.format("com.baesystems.ai.lr.utils.exception.ComparisonException: " + DtoUtils.FIELD_MISMATCH_ERROR + "\n",
                "title", newTitle, originalTitle), error.getMessage());
    }

    /**
     * Tests invalid PUT ../asset/x/coc/y fails to update CoC when we attempt to change immutable field due date
     */
    @Test
    public final void testUpdateCoCFailsChangedImposedDate()
    {
        final CoCDto originalCoCDto = TestUtils.getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK, CoCDto.class);

        final Date originalImposedDate = originalCoCDto.getImposedDate();

        final Calendar calander = Calendar.getInstance();
        calander.setTime(originalImposedDate);
        calander.add(Calendar.DATE, 1);

        final Date newImposedDate = calander.getTime();
        originalCoCDto.setImposedDate(newImposedDate);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals("Attempting to modify imposed date. Original: " + DATE_FORMATTER.format(originalImposedDate) + " Modified: "
                + DATE_FORMATTER.format(newImposedDate), error.getMessage());
    }

    /**
     * Tests an invalid PUT ../asset/x/coc/y where the child WIPs for an existing element are attempted to be removed.
     */
    @Test
    public final void testInvalidUpdateChildEntitiesDoesNotBreak()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        originalCoCDto.setChildWIPs(null);

        TestUtils.putResponse(format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID), originalCoCDto,
                HttpStatus.BAD_REQUEST);
    }

    /**
     * Tests valid PUT ../job/x/wip-coc/y updates wip-coc and returns the updated value
     */
    @Test
    public final void testUpdateWIPCoC()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        final String newDescription = "new Description";
        originalCoCDto.setDescription(newDescription);

        final CoCDto updatedCto = TestUtils.putResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), originalCoCDto, HttpStatus.OK,
                CoCDto.class);

        assertEquals(originalCoCDto.getId(), updatedCto.getId());
        assertEquals(updatedCto.getDescription(), newDescription);
    }

    @Test
    public final void testUpdateWIPCoCFailsWhenOldTokenIsUsed()
    {
        final CoCDto originalCoCDto = TestUtils.getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK)
                .getBody()
                .as(CoCDto.class);

        final String originalStalenessHash = originalCoCDto.getStalenessHash();

        // Make a small change
        originalCoCDto.setDescription(originalCoCDto.getDescription() + "X");

        final CoCDto updatedCto = TestUtils.putResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), originalCoCDto, HttpStatus.OK)
                .jsonPath().getObject("", CoCDto.class);

        // Make another small change, but use the hash from the original GET
        updatedCto.setDescription(updatedCto.getDescription() + "X");
        updatedCto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), updatedCto, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.COC.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    @Test
    public final void testUpdateWIPCoCForWIPDefectFailsWhenOldTokenIsUsed()
    {
        final CoCPage wipCoCs = TestUtils.getResponse(
                format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_1_ID, VALID_WIP_DEFECT_ID_4_FOR_JOB_1, WIP_COC_ID_FOR_JOB_AND_WIP_DEFECT), HttpStatus.OK,
                CoCPage.class);
        final CoCDto originalCoCDto = wipCoCs.getContent().get(0);

        final String originalStalenessHash = originalCoCDto.getStalenessHash();

        // Make a small change
        originalCoCDto.setDescription(originalCoCDto.getDescription() + "-ForWipDefect-");

        final CoCDto updatedCto = TestUtils.putResponse(
                format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_1_ID, VALID_WIP_DEFECT_ID_4_FOR_JOB_1, WIP_COC_ID_FOR_JOB_AND_WIP_DEFECT), originalCoCDto,
                HttpStatus.OK, CoCDto.class);

        // Make another small change, but use the hash from the original GET
        updatedCto.setDescription(updatedCto.getDescription() + "X");
        updatedCto.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_1_ID, VALID_WIP_DEFECT_ID_4_FOR_JOB_1, WIP_COC_ID_FOR_JOB_AND_WIP_DEFECT),
                        updatedCto, HttpStatus.CONFLICT, ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.COC.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());
    }

    /**
     * Tests invalid PUT ../job/x/wip-coc/y fails when job scope confirmed is null
     */
    @Test
    public final void testUpdateWIPCoCFailsNullJobScopeConfirmed()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        originalCoCDto.setJobScopeConfirmed(null);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), originalCoCDto, HttpStatus.BAD_REQUEST,
                        ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    /**
     * Tests valid PUT ../job/x/wip-coc/y updates wip-coc and returns the updated value
     */
    @Test
    public final void testUpdateWIPCoCWithDeletedStatus()
    {
        final CoCDto originalCoCDto = TestUtils
                .getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK).getBody()
                .as(CoCDto.class);

        final String newDescription = "new Description";
        originalCoCDto.setDescription(newDescription);

        originalCoCDto.setStatus(new LinkResource(COC_DELETED_STATUS_ID));
        final LinkResource category = new LinkResource(MACHINERY_CODICIL_CATEGORY_ID);
        originalCoCDto.setCategory(category);

        final CoCDto updatedCto = TestUtils.putResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), originalCoCDto, HttpStatus.OK,
                CoCDto.class);

        assertEquals(originalCoCDto.getId(), updatedCto.getId());
        assertEquals(updatedCto.getDescription(), newDescription);
    }

    @Test
    public final void testDeleteCoCThatDoesNotExist()
    {
        final String url = format(DELETE_ASSET_COC, ASSET_1_ID, INVALID_COC_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteCoCNotAssociatedWithThepPassedAsset()
    {
        final String url = format(DELETE_ASSET_COC, ASSET_2_ID, NOT_ASSOCIATED_COC_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteCoC()
    {
        final CoCDto deletedCoC = TestUtils.getResponse(format(DELETE_ASSET_COC, ASSET_1_ID, DELETE_THIS_COC_ID), HttpStatus.OK, CoCDto.class);

        final String url = format(DELETE_ASSET_COC, ASSET_1_ID, DELETE_THIS_COC_ID);
        TestUtils.deleteResponse(url, HttpStatus.OK);

        // Confirm that the CoC is no longer returned.
        TestUtils.getResponse(format(DELETE_ASSET_COC, ASSET_1_ID, DELETE_THIS_COC_ID), HttpStatus.NOT_FOUND);

        // Replace the deleted CoC to avoid interfering with other tests.
        deletedCoC.setId(null);
        deletedCoC.setImposedDate(new Date());
        deletedCoC.setDueDate(new Date());
        TestUtils.postResponse(format(ASSET_COC, ASSET_1_ID), deletedCoC, HttpStatus.OK);
    }

    /**
     * Test that an invalid GET request for ../asset/{assetId}/coc results in a positive response. Expecting an error
     * for a bad request.
     */
    @Theory
    // loops all bad requests in the array BAD_REQUESTS and gives individual
    // feedback
    public final void ruleSetUriBadRequests(final String badRequest)
    {
        TestUtils.singleItemBadRequest(badRequest);
    }

    /**
     * Test /asset/x/coc/query with various parameters
     */
    @Theory
    public final void queries(@FromDataPoints("NON_WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseCoCPage(false, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final CoCPage results = TestUtils.postResponse(format(COC_QUERY, ASSET_2), query, HttpStatus.OK,
                CoCPage.class);

        TestUtils.compareFields(results, this.coCPage, true);
    }

    /**
     * Test /job/x/coc/query with various parameters
     */
    @Theory
    public final void wipQueries(@FromDataPoints("WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        initialiseCoCPage(true, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final CoCPage results = TestUtils.postResponse(format(WIP_COC_QUERY, JOB_1_ID), query, HttpStatus.OK,
                CoCPage.class);

        TestUtils.compareFields(results, this.coCPage, true);
    }

    @Test
    public final void testWIPDeleteCoCThatDoesNotExist()
    {
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, JOB_1_ID, INVALID_COC_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testWIPDeleteCoCNotAssociatedWithThePassedJob()
    {
        // Create a wip coc
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final CoCDto body = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        // deliberately try to delete the WIP CoC passing the wrong job id
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, JOB_2_ID, body.getId()), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testWIPDeleteCoC()
    {
        // Create a wip coc to delete
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final CoCDto body = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        // Delete it
        TestUtils.deleteResponse(format(JOB_WIP_COC_ITEM, JOB_1_ID, body.getId()), HttpStatus.OK);

    }

    @Test
    public final void testCreateRepairOnWIPCoC()
    {
        // Create a wip coc just for this test
        final CoCDto coCDto = createNewCocDtoFromExistingDto();
        final CoCDto body = TestUtils.postResponse(format(JOB_WIP_COC, JOB_1_ID), coCDto, HttpStatus.OK, CoCDto.class);

        final RepairDto repairToAdd = new RepairDto();
        repairToAdd.setCodicil(makeLinkResource(body.getId()));
        repairToAdd.setConfirmed(false);
        repairToAdd.setDescription("New WIP Repair");
        repairToAdd.setMaterialsUsed(new ArrayList<LinkResource>());
        repairToAdd.setNarrative("New WIP Repair Narrative.");
        repairToAdd.setPromptThoroughRepair(false);
        repairToAdd.setRepairAction(makeLinkResource(PERMANENT_REPAIR_ACTION));
        repairToAdd.setRepairs(new ArrayList<RepairItemDto>());
        repairToAdd.setRepairTypeDescription("WIP Repair Type Description.");
        repairToAdd.setRepairTypes(new ArrayList<LinkResource>());
        repairToAdd.getRepairTypes().add(makeLinkResource(REPAIR_TYPE_1));
        repairToAdd.setUpdatedDate(new Date());

        final RepairDto newlyReturnedDto = TestUtils.postResponse(format(WIP_COC_REPAIR, JOB_1_ID, body.getId()), repairToAdd, HttpStatus.OK,
                RepairDto.class);

        Assert.assertNotNull(newlyReturnedDto.getId());
    }

    public final void testGetCoCForDefect()
    {
        initialiseCoCPage(false, COC_21_ID);
        TestUtils.listItemsFound(String.format(DEFECT_COC, ASSET_2_ID, VALID_DEFECT_ID_FOR_COCS), this.coCPage, true, false);
    }

    @Test
    public final void testGetDefectCoCFails()
    {
        TestUtils.singleItemNotFound(format(DEFECT_COC, ASSET_2_ID, INVALID_DEFECT_ID_FOR_COCS));
    }

    @Test
    public final void testUpdateWIPCoCOnWIPDefectSucceeds() throws DatabaseUnitException, InvocationTargetException,
            NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        final String[] ignoredFields = {"id", "dueStatus", "stalenessHash"};

        final CoCDto updateThisCoC = TestUtils.getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_3_ID, WIP_COC_45_ID), HttpStatus.OK, CoCDto.class);

        updateThisCoC.setDescription("This description change should be allowed");
        updateThisCoC.setDueDate(DateUtils.truncate(new Date(), Calendar.DATE));
        updateThisCoC.setTitle("Allow this change");

        final CoCDto result = TestUtils.putResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_3_ID, WIP_DEFECT_11_ID), updateThisCoC, HttpStatus.OK,
                CoCDto.class);

        TestUtils.compareFields(result, updateThisCoC, false, ignoredFields);
    }

    @Test
    public final void testUpdateWIPCoCOnWIPDefectNullJobScopeConfirmed() throws DatabaseUnitException
    {

        final CoCDto cocDto = DataUtils.getCoC(WIP_COC_45_ID, true);
        cocDto.setJobScopeConfirmed(null);
        final ErrorMessageDto error = TestUtils
                .putResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_3_ID, WIP_DEFECT_11_ID), cocDto, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    @Test
    public final void testUpdateWIPCoCOnWIPDefectWithParentFails() throws DatabaseUnitException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        final CoCDto updateThisCoC = TestUtils.getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_3_ID, WIP_COC_44_ID), HttpStatus.OK)
                .getBody().as(CoCDto.class);

        updateThisCoC.setDescription("This description change should be allowed but will fail due to title change");
        updateThisCoC.setDueDate(DateUtils.truncate(new Date(), Calendar.DATE));
        updateThisCoC.setTitle("Do not allow this change");

        TestUtils.putResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_3_ID, WIP_DEFECT_11_ID), updateThisCoC,
                HttpStatus.BAD_REQUEST);
    }

    @Test
    public final void testUpdateWIPCoCOnWIPDefectWithParentSucceeds() throws DatabaseUnitException,
            InvocationTargetException, NoSuchMethodException, IntegrationTestException, IllegalAccessException
    {
        final String[] ignoredFields = {"id", "dueStatus", "stalenessHash"};

        final CoCDto updateThisCoC = TestUtils.getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_3_ID, WIP_COC_44_ID), HttpStatus.OK, CoCDto.class);

        updateThisCoC.setDescription("This description change should be allowed");
        updateThisCoC.setDueDate(DateUtils.truncate(new Date(), Calendar.DATE));

        final CoCDto result = TestUtils.putResponse(format(BASE_JOB_WIP_DEFECT_WIP_COC, JOB_3_ID, WIP_DEFECT_11_ID), updateThisCoC, HttpStatus.OK,
                CoCDto.class);

        TestUtils.compareFields(result, updateThisCoC, false, ignoredFields);
    }

    private CoCDto createNewCocDtoFromExistingDto()
    {
        final CoCDto coCDto = TestUtils.getResponse(format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_1_ID), HttpStatus.OK, CoCDto.class);
        coCDto.setId(null);
        coCDto.setRequireApproval(Boolean.FALSE);

        coCDto.setTitle("! NULL && 50 >= length >= 1.");
        coCDto.setDescription("! NULL && 2000 >= length >= 1");
        coCDto.setConfidentialityType(new LinkResource(ONE));
        coCDto.setRequireApproval(false);
        coCDto.setCategory(new LinkResource(HULL_CODICIL_CATEGORY_ID));
        final LinkResource parent = new LinkResource(COC_1_ID);
        coCDto.setParent(parent);

        return coCDto;
    }

    private LinkResource makeLinkResource(final Long id)
    {
        final LinkResource linkResource = new LinkResource(id);
        return linkResource;
    }

    private CoCDto getCoCDtoWithNoIdAndDueDates(final boolean wip)
    {
        final String url = wip ? format(JOB_WIP_COC.concat("/%d"), JOB_1_ID, WIP_COC_20)
                : format(ASSET_COC.concat("/%d"), ASSET_1_ID, COC_1_ID);
        final CoCDto coCDto = TestUtils.getResponse(url, HttpStatus.OK).getBody().as(CoCDto.class);
        coCDto.setId(null);
        coCDto.setImposedDate(DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DATE), FUTURE_IMPOSED));
        coCDto.setDueDate(DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DATE), FUTURE_DUE));
        return coCDto;
    }
}
