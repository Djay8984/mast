package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueryAndResult
{
    private final Long[] result;

    private final List<String> queryField;

    private final List<Object> queryValue;

    public QueryAndResult(final String queryField, final Object queryValue, final Long[] result)
    {
        this.result = result;
        this.queryField = new ArrayList<String>();
        this.queryValue = new ArrayList<Object>();
        this.queryField.add(queryField);
        this.queryValue.add(queryValue);
    }

    public QueryAndResult(final String[] queryField, final Object[] queryValue, final Long[] result)
    {
        this.result = result;
        this.queryField = Arrays.asList(queryField);
        this.queryValue = Arrays.asList(queryValue);
    }

    public Long[] getResult()
    {
        return result;
    }

    public List<String> getQueryField()
    {
        return queryField;
    }

    public List<Object> getQueryValue()
    {
        return queryValue;
    }
}
