package com.baesystems.ai.lr.rest.assets;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.TestUtils;
import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

@Category(IntegrationTest.class)
public class ItemVersioningRestTest extends BaseRestTest
{
    private static final String ITEM_URL = urlBuilder(BASE_PATH, "/asset/%d/item/%d");
    private static final Long ASSET_1_ID = 1L;
    private static final Long ITEM_87_ID = 87L;
    private static final Long ASSET_1_PUBLISHED_VERSION_ID = 2L;
    private static final Long ASSET_1_OLD_VERSION_ID = 1L;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
    }

    @Test
    public void checkItemRelationshipForPublishedVersion()
    {
        final LazyItemDto itemDto = TestUtils.getResponse(String.format(ITEM_URL, ASSET_1_ID, ITEM_87_ID), HttpStatus.OK).as(LazyItemDto.class);
        itemDto.getItems().forEach(relationshipLink -> checkItemRelationship(relationshipLink, ASSET_1_PUBLISHED_VERSION_ID));
    }

    @Test
    public void checkItemRelationshipForHistoricVersion()
    {
        final LazyItemDto itemDto = TestUtils
                .getResponse(String.format(ITEM_URL.concat("?versionId=%d"), ASSET_1_ID, ITEM_87_ID, ASSET_1_OLD_VERSION_ID), HttpStatus.OK)
                .as(LazyItemDto.class);
        itemDto.getItems().forEach(relationshipLink -> checkItemRelationship(relationshipLink, ASSET_1_OLD_VERSION_ID));
    }

    @Test
    public void checkDraftItemsAreNotVisibleForOtherUsers()
    {
        final ErrorMessageDto errorMessage =
                TestUtils.getResponse(String.format(ITEM_URL.concat("?includeDraft=true"), ASSET_1_ID, ITEM_87_ID),
                        HttpStatus.BAD_REQUEST).as(ErrorMessageDto.class);

        Assert.assertEquals(String.format(ExceptionMessagesUtils.ASSET_IS_NOT_CHECKED_OUT, ASSET_1_ID),
                errorMessage.getMessage());
    }

    private void checkItemRelationship(final LinkVersionedResource relationshipLink, final Long version)
    {
        Assert.assertEquals(version, relationshipLink.getVersion());
    }
}
