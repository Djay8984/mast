package com.baesystems.ai.lr.rest.jobs;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.utils.CollectionUtils;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.DefectItemLinkDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairItemDto;
import com.baesystems.ai.lr.dto.jobs.JobBundleDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.pages.SupplementaryInformationPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.rest.attachments.AttachmentsRestTest;
import com.baesystems.ai.lr.utils.TestUtils;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Category(IntegrationTest.class)
public class JobBundleRestTest extends BaseRestTest
{
    private static final String JOB_BUNDLE = urlBuilder(BASE_PATH, "/job-bundle/%d");
    private static final Long JOB_1_ID = 1L;
    private static final String PLACE_HOLDER = "Place Holder";
    private static final Long ITEM_87_ID = 87L;

    private MapperFacade mapper;
    private JobBundleDto originalBundle;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
        this.originalBundle = getBundle();
    }

    @Test
    public void testGetJobBundle()
    {
        final JobBundleDto jobBundleDto = getBundle();

        Assert.notNull(jobBundleDto.getJob());
        Assert.notEmpty(jobBundleDto.getReport());
        Assert.notEmpty(jobBundleDto.getSurvey());
        Assert.notEmpty(jobBundleDto.getWipTask());
        Assert.notEmpty(jobBundleDto.getWipActionableItem());
        Assert.notEmpty(jobBundleDto.getWipCoC());
        Assert.notEmpty(jobBundleDto.getWipAssetNote());
        Assert.notEmpty(jobBundleDto.getWipDefect());
        Assert.notEmpty(jobBundleDto.getWipRepair());

        Assert.notNull(jobBundleDto.getAsset());
        Assert.notNull(jobBundleDto.getAssetModel());
        Assert.notEmpty(jobBundleDto.getActionableItem());
        Assert.notEmpty(jobBundleDto.getCoC());
        Assert.notEmpty(jobBundleDto.getAssetNote());
        Assert.notEmpty(jobBundleDto.getDefect());
        Assert.notEmpty(jobBundleDto.getRepair());
        Assert.notEmpty(jobBundleDto.getService());
        Assert.notEmpty(jobBundleDto.getTask());

        // uncomment in R3
        // Assert.notEmpty(jobBundleDto.getAttachment());
    }

    /**
     * Get bundle, change job description
     */
    @Test
    public void testUpdateJob()
    {
        final String originalValue = this.originalBundle.getJob().getDescription();
        this.originalBundle.getJob().setDescription(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setJob(this.originalBundle.getJob());

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getJob().getDescription());

        // Reset data
        ourBundle.getJob().setDescription(originalValue);
        ourBundle.getJob().setStalenessHash(updatedBundle.getJob().getStalenessHash());
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change wip asset note description
     */
    @Test
    public void testUpdateWipAssetNote()
    {
        final AssetNoteDto assetNoteDto = this.originalBundle.getWipAssetNote().get(0);
        final String originalValue = assetNoteDto.getDescription();
        assetNoteDto.setDescription(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipAssetNote(Collections.singletonList(assetNoteDto));

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getWipAssetNote().get(0).getDescription());

        // Reset data
        assetNoteDto.setDescription(originalValue);
        assetNoteDto.setStalenessHash(updatedBundle.getWipAssetNote().get(0).getStalenessHash());
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change multiple wip asset note descriptions
     */
    @Test
    public void testUpdateMultipleWipAssetNotes()
    {
        final AssetNoteDto assetNoteDto1 = this.originalBundle.getWipAssetNote().get(0);
        final AssetNoteDto assetNoteDto2 = this.originalBundle.getWipAssetNote().get(1);
        final String originalValue1 = assetNoteDto1.getDescription();
        final String originalValue2 = assetNoteDto2.getDescription();

        assetNoteDto1.setDescription(PLACE_HOLDER);
        assetNoteDto2.setDescription(PLACE_HOLDER + "2");

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipAssetNote(Arrays.asList(assetNoteDto1, assetNoteDto2));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEquals(PLACE_HOLDER, newBundle.getWipAssetNote().get(0).getDescription());
        assertEquals(PLACE_HOLDER + "2", newBundle.getWipAssetNote().get(1).getDescription());

        // Reset data
        assetNoteDto1.setDescription(originalValue1);
        assetNoteDto1.setStalenessHash(newBundle.getWipAssetNote().get(0).getStalenessHash());

        assetNoteDto2.setDescription(originalValue2);
        assetNoteDto2.setStalenessHash(newBundle.getWipAssetNote().get(1).getStalenessHash());

        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, add a new wip asset note
     */
    @Test
    public void testCreateAndDeleteWipAssetNote()
    {
        // Create new AssetNote Dto
        final AssetNoteDto newAssetNoteDto = this.mapper.map(this.originalBundle.getWipAssetNote().get(0),
                AssetNoteDto.class);
        newAssetNoteDto.setId(null);

        // Add to minimal bundle
        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipAssetNote(Collections.singletonList(newAssetNoteDto));

        // Put bundle and confirm the Asset note has been saved
        final JobBundleDto result = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipAssetNote(), result.getWipAssetNote());

        // Get the saved asset note, delete it and put the Bundle
        final AssetNoteDto savedAssetNote = getLast(result.getWipAssetNote());
        savedAssetNote.setDeleted(true);
        ourBundle.setWipAssetNote(Collections.singletonList(savedAssetNote));
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change wip actionable item description
     */
    @Test
    public void testUpdateWipActionableItem()
    {
        final ActionableItemDto actionableItemDto = this.originalBundle.getWipActionableItem().get(1);
        final String originalValue = actionableItemDto.getDescription();
        actionableItemDto.setDescription(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipActionableItem(Collections.singletonList(actionableItemDto));

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getWipActionableItem().get(1).getDescription());

        // Reset data
        actionableItemDto.setDescription(originalValue);
        actionableItemDto.setStalenessHash(updatedBundle.getWipActionableItem().get(1).getStalenessHash());
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, create new Wip Actionable Item
     */
    @Test
    public void testCreateAndDeleteWipActionableItem()
    {
        // Create new ActionableItem Dto
        final ActionableItemDto actionableItemDto = this.mapper.map(this.originalBundle.getWipActionableItem().get(0),
                ActionableItemDto.class);
        actionableItemDto.setId(null);
        actionableItemDto.setImposedDate(new Date());
        actionableItemDto.setDueDate(actionableItemDto.getImposedDate());

        // Add to minimal bundle
        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipActionableItem(Collections.singletonList(actionableItemDto));

        // Put bundle and confirm the Actionable Item has been saved
        final JobBundleDto result = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipActionableItem(), result.getWipActionableItem());

        // Get the saved asset note, delete it and put the Bundle
        final ActionableItemDto savedActionableItem = getLast(result.getWipActionableItem());
        savedActionableItem.setDeleted(true);
        ourBundle.setWipActionableItem(Collections.singletonList(savedActionableItem));
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change wip coc description
     */
    @Test
    public void testUpdateWipCoC()
    {
        final CoCDto coCDto = this.originalBundle.getWipCoC().get(0);
        final String originalValue = coCDto.getDescription();
        coCDto.setDescription(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipCoC(Collections.singletonList(coCDto));

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getWipCoC().get(0).getDescription());

        // Reset data
        coCDto.setDescription(originalValue);
        coCDto.setStalenessHash(updatedBundle.getWipCoC().get(0).getStalenessHash());
        putBundle(ourBundle);
        checkState(false);
    }

    /**
     * Get bundle, create new WipCoC
     */
    @Test
    public void testCreateAndDeleteWipCoC()
    {
        final CoCDto newCoC = this.mapper.map(this.originalBundle.getWipCoC().get(0), CoCDto.class);
        newCoC.setId(null);
        newCoC.setConfidentialityType(new LinkResource(1L));
        newCoC.setTitle("stupid title");

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipCoC(Collections.singletonList(newCoC));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipCoC(), newBundle.getWipCoC());

        getLast(newBundle.getWipCoC()).setDeleted(true);
        ourBundle.setWipCoC(Collections.singletonList(getLast(newBundle.getWipCoC())));
        putBundle(ourBundle);
        checkState(false);
    }

    /**
     * Get bundle, change wip defect description
     */
    @Test
    public void testUpdateWipDefect()
    {
        final DefectDto defectDto = this.originalBundle.getWipDefect().get(0);
        final String originalValue = defectDto.getIncidentDescription();
        defectDto.setIncidentDescription(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipDefect(Collections.singletonList(defectDto));

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getWipDefect().get(0).getIncidentDescription());

        defectDto.setIncidentDescription(originalValue);
        defectDto.setStalenessHash(updatedBundle.getWipDefect().get(0).getStalenessHash());
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, create new Wip Defect
     */
    @Test
    @Ignore
    public void testCreateAndThenDeleteWipDefect()
    {
        final DefectDto newDefectDto = this.mapper.map(this.originalBundle.getWipDefect().get(0), DefectDto.class);
        newDefectDto.setConfidentialityType(new LinkResource());
        newDefectDto.getConfidentialityType().setId(1L);
        newDefectDto.setTitle("title");
        newDefectDto.setId(null);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipDefect(Collections.singletonList(newDefectDto));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipDefect(), newBundle.getWipDefect());

        // Delete the newly created Wip defect and the survey that it will have
        // created.
        getLast(newBundle.getWipDefect()).setDeleted(true);
        getLast(newBundle.getSurvey()).setDeleted(true);

        ourBundle.setWipDefect(Collections.singletonList(getLast(newBundle.getWipDefect())));
        ourBundle.setSurvey(Collections.singletonList(getLast(newBundle.getSurvey())));
        putBundle(ourBundle);
        checkState();
    }

    @Test
    @Ignore
    public void testCreateAndThenDeleteWipRepair()
    {
        // DEFECT
        final DefectDto newDefectDto = this.mapper.map(this.originalBundle.getWipDefect().get(0), DefectDto.class);
        newDefectDto.setConfidentialityType(new LinkResource());
        newDefectDto.getConfidentialityType().setId(1L);
        newDefectDto.setTitle("title");
        newDefectDto.setInternalId("1");
        newDefectDto.setId(null);
        newDefectDto.setItems(new ArrayList<>());

        // DEFECT-ITEM
        final DefectItemDto defectItem = new DefectItemDto();
        defectItem.setItem(new ItemLightDto());
        defectItem.getItem().setId(ITEM_87_ID);
        newDefectDto.getItems().add(defectItem);

        // REPAIR
        final RepairDto newRepairDto = this.mapper.map(this.originalBundle.getWipRepair().get(0), RepairDto.class);
        newRepairDto.setDefect(new LinkResource());
        newRepairDto.getDefect().setInternalId(newDefectDto.getInternalId());
        newRepairDto.setId(null);
        newRepairDto.setRepairs(new ArrayList<>());

        // REPAIR-ITEM
        final RepairItemDto repairItem = new RepairItemDto();
        repairItem.setInternalId("77");
        repairItem.setItem(new DefectItemLinkDto());
        repairItem.getItem().setItem(new LinkResource());
        repairItem.getItem().getItem().setId(ITEM_87_ID);
        newRepairDto.getRepairs().add(repairItem);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipDefect(Collections.singletonList(newDefectDto));
        ourBundle.setWipRepair(Collections.singletonList(newRepairDto));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipDefect(), newBundle.getWipDefect());
        assertEntityHasBeenCreated(this.originalBundle.getWipRepair(), newBundle.getWipRepair());
        for (final RepairDto repair : this.originalBundle.getWipRepair())
        {
            for (final RepairDto newRepair : CollectionUtils.nullSafeCollection(newBundle.getWipRepair()))
            {
                if (!repair.getRepairs().isEmpty())
                {
                    assertEntityHasBeenCreated(repair.getRepairs(), newRepair.getRepairs());
                }
            }
        }

        getLast(newBundle.getWipDefect()).setDeleted(true);
        getLast(newBundle.getSurvey()).setDeleted(true);
        ourBundle.setWipDefect(Collections.singletonList(getLast(newBundle.getWipDefect())));
        ourBundle.setSurvey(Collections.singletonList(getLast(newBundle.getSurvey())));
        ourBundle.setWipRepair(null);
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change survey name
     */
    @Test
    public void testUpdateSurvey()
    {

        final SurveyDto surveyDto = this.originalBundle.getSurvey().get(0);
        final String originalValue = surveyDto.getNarrative();
        surveyDto.setNarrative(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setSurvey(Collections.singletonList(surveyDto));

        final SurveyDto updatedSurvey = putBundle(ourBundle).getSurvey().get(0);
        assertEquals(PLACE_HOLDER, updatedSurvey.getNarrative());

        // Important to push our original sparse bundle rather than the result of the updating PUT because the second
        // case returns _all_ the bundle data. Passing the full data set through has the side effect of creating more
        // surveys (as wip defects are updated).

        // However, the hash still needs to be updated.

        final SurveyDto surveyToReset = ourBundle.getSurvey().get(0);
        surveyToReset.setNarrative(originalValue);
        surveyToReset.setStalenessHash(updatedSurvey.getStalenessHash());

        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, create new WipTask
     */
    @Test
    public void testCreateWipTask()
    {
        final WorkItemLightDto newTask = this.mapper.map(this.originalBundle.getWipTask().get(0),
                WorkItemLightDto.class);
        newTask.setId(null);
        newTask.setSurvey(new LinkResource(1L));
        newTask.setName("stupid name");

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setWipTask(Collections.singletonList(newTask));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getWipTask(), newBundle.getWipTask());

        getLast(newBundle.getWipTask()).setDeleted(true);
        ourBundle.setWipTask(Collections.singletonList(getLast(newBundle.getWipTask())));
        putBundle(ourBundle);
        checkState(false);
    }

    /**
     * Get bundle, change task name
     */
    @Test
    public void testUpdateWipTask()
    {
        final WorkItemLightDto taskDto = this.originalBundle.getWipTask().get(0);
        final String originalValue = taskDto.getName();
        taskDto.setName(PLACE_HOLDER);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setWipTask(Collections.singletonList(taskDto));

        final JobBundleDto updatedBundle = putBundle(ourBundle);

        assertEquals(PLACE_HOLDER, updatedBundle.getWipTask().get(0).getName());

        // Reset data
        taskDto.setName(originalValue);
        taskDto.setStalenessHash(updatedBundle.getWipTask().get(0).getStalenessHash());

        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, change survey name
     */
    @Test
    public void testCreateAndThenDeleteSurvey()
    {
        final SurveyDto newSurveyDto = this.mapper.map(this.originalBundle.getSurvey().get(0), SurveyDto.class);
        newSurveyDto.setId(null);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setSurvey(Collections.singletonList(newSurveyDto));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEntityHasBeenCreated(this.originalBundle.getSurvey(), newBundle.getSurvey());

        // Housekeep
        getLast(newBundle.getSurvey()).setDeleted(true);
        ourBundle.setSurvey(Collections.singletonList(getLast(newBundle.getSurvey())));
        putBundle(ourBundle);
        checkState();
    }

    /**
     * Get bundle, copy a report and set id of one of the reports to null and the type to DAR so that a new DAR will be
     * created
     */
    @Test
    public void testCreateReport()
    {
        final ReportDto report = this.mapper.map(this.originalBundle.getReport().get(0), ReportDto.class);
        final int originalSize = this.originalBundle.getReport().size();

        report.getReportType().setId(ReportType.DAR.value());
        report.setId(null);

        final JobBundleDto ourBundle = new JobBundleDto();
        ourBundle.setId(JOB_1_ID);
        ourBundle.setReport(Collections.singletonList(report));

        final JobBundleDto newBundle = putBundle(ourBundle);
        assertEquals(originalSize + 1, newBundle.getReport().size());

        checkState(false); // follow up actions is called in the route so
                           // surveys change.
    }

    @Test
    public void testCreateAttachmentForJob()
    {
        final SupplementaryInformationPage attachments = TestUtils
                .getResponse(String.format(AttachmentsRestTest.BASE_JOB_ATTACHMENT, JOB_1_ID), HttpStatus.OK)
                .as(SupplementaryInformationPage.class);

        final int count = attachments.getContent().size();

        final SupplementaryInformationDto newAttachment = attachments.getContent().get(0);
        newAttachment.setId(null);

        final JobBundleDto jobBundleDto = new JobBundleDto();
        jobBundleDto.setAttachment(new ArrayList<>(Collections.singletonList(newAttachment)));

        putBundle(jobBundleDto);

        final SupplementaryInformationPage newAttachments = TestUtils
                .getResponse(String.format(AttachmentsRestTest.BASE_JOB_ATTACHMENT, JOB_1_ID), HttpStatus.OK)
                .as(SupplementaryInformationPage.class);

        assertEquals(count + 1, newAttachments.getContent().size());
        checkState();
    }

    /*
     * Helpers
     */
    private static JobBundleDto getBundle()
    {
        return TestUtils.getResponse(String.format(JOB_BUNDLE, JOB_1_ID), HttpStatus.OK).getBody()
                .as(JobBundleDto.class);
    }

    private static JobBundleDto putBundle(final JobBundleDto jobBundleDto)
    {
        return TestUtils
                .putResponse(String.format(JOB_BUNDLE, JOB_1_ID), jobBundleDto, HttpStatus.OK, JobBundleDto.class);
    }

    private static void assertEntityHasBeenCreated(final List oldList, final List newList)
    {
        assertEquals(oldList.size() + 1, newList.size());
    }

    private <T> T getLast(final List<T> list)
    {
        return list.get(list.size() - 1);
    }

    private void checkState()
    {
        checkState(true);
    }

    private void checkState(final boolean includeSurveys)
    {
        final JobBundleDto afterBundle = getBundle();
        assertEquals(this.originalBundle.getWipActionableItem().size(), afterBundle.getWipActionableItem().size());
        assertEquals(this.originalBundle.getWipAssetNote().size(), afterBundle.getWipAssetNote().size());
        assertEquals(this.originalBundle.getWipCoC().size(), afterBundle.getWipCoC().size());
        assertEquals(this.originalBundle.getWipDefect().size(), afterBundle.getWipDefect().size());
        assertEquals(this.originalBundle.getWipRepair().size(), afterBundle.getWipRepair().size());
        if (includeSurveys)
        {
            assertEquals(this.originalBundle.getSurvey().size(), afterBundle.getSurvey().size());
            assertEquals(this.originalBundle.getWipTask().size(), afterBundle.getWipTask().size());
        }
    }
}
