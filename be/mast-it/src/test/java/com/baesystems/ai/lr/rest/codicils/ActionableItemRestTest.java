package com.baesystems.ai.lr.rest.codicils;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dbunit.DatabaseUnitException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.BatchActionActionableItemDto;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.ExceptionType;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exceptions.IntegrationTestException;
import com.baesystems.ai.lr.pages.ActionableItemPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DateHelperImpl;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.QueryAndResult;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Response;

@Category(IntegrationTest.class)
@RunWith(Theories.class)
public class ActionableItemRestTest extends BaseRestTest
{
    private static final String BASE_ASSET = urlBuilder(BASE_PATH, "/asset/%d");
    private static final String BASE_ACTIONABLE_ITEM = urlBuilder(BASE_ASSET, "/actionable-item");
    private static final String ACTIONABLE_ITEM_ON_ITEM = urlBuilder(BASE_ASSET, "/item/%d/actionable-item");
    private static final String ACTIONABLE_ITEM = urlBuilder(BASE_ACTIONABLE_ITEM, "/%d");
    private static final String NO_PARAM_TEMPLATE_ASSET_COUNT = urlBuilder(BASE_PATH, "/asset/actionable-item/count");
    private static final String TEMPLATE_ASSET_COUNT = urlBuilder(NO_PARAM_TEMPLATE_ASSET_COUNT, "?template=%s");
    private static final String ACTIONABLE_ITEM_QUERY = urlBuilder(BASE_ACTIONABLE_ITEM, "/query");
    private static final String BASE_JOB = urlBuilder(BASE_PATH, "/job/%d");
    public static final String WIP_ACTIONABLE_ITEM = urlBuilder(BASE_JOB, "/wip-actionable-item");
    private static final String WIP_ACTIONABLE_ITEM_WITH_ID = urlBuilder(WIP_ACTIONABLE_ITEM, "/%d");
    private static final String WIP_ACTIONABLE_ITEM_QUERY = urlBuilder(WIP_ACTIONABLE_ITEM, "/query");
    private static final String BASE_JOB_WIP_DEFECT_WIP_AI = urlBuilder(BASE_JOB, "/wip-defect/%d/wip-actionable-item");
    private static final String BATCH_ACTION_ACTIONABLE_ITEM = urlBuilder(BASE_PATH, "/batch-action/actionable-item");

    private static final Long ACTIONABLE_ITEM_4_ID = 4L;
    private static final Long DELETABLE_ACTIONABLE_ITEM_ID = 15L;
    private static final Long DELETED_ACTIONABLE_ITEM_ID = 16L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_3_ID = 3L;
    private static final Long ASSET_5_ID = 5L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_87_ID = 87L;
    private static final Long ITEM_88_ID = 88L;
    private static final Long CATEGORY_2_ID = 2L;
    private static final Long ASSET_2_ID = 2L;
    private static final Long STATUS_1_ID = 1L;
    private static final Long STATUS_2_ID = 2L;
    private static final Long STATUS_4_ID = 4L;
    private static final Long INVALID_ACTIONABLE_ITEM_ID = 999L;
    private static final Long INVALID_ASSET_ID = 999L;
    private static final Long TEMPLATE_39_ID = 39L;
    private static final Long TEMPLATE_40_ID = 40L;
    private static final Long TEMPLATE_41_ID = 41L;
    private static final Integer TEMPLATE_39_COUNT = 1;
    private static final Integer TEMPLATE_40_COUNT = 0;
    private static final Integer TEMPLATE_41_COUNT = 1;
    private static final Long JOB_1_ID = 1L;
    private static final Long JOB_2_ID = 2L;
    private static final Long INVALID_JOB_1_ID = 9999L;
    private static final Long EXISTING_CODICIL_OF_A_DIFFERENT_TYPE_ID = 2L;
    private static final Long WIP_AI_ID_FOR_JOB_AND_WIP_DEFECT = 26L;
    private static final Long INVALID_WIP_DEFECT_ID = 999L;
    private static final Long VALID_JOB_ID_FOR_WIP_DEFECT = 1L;
    private static final Long VALID_WIP_DEFECT_ID_FOR_JOB = 6L;
    private static final Long WIP_DEFECT_ID_WITH_NO_WIP_AI = 4L;
    private static final int MINUS_2 = -2;

    private static final String SYSTEM_GROUP = "SYSTEM GROUP";
    private static final Long ITEM_TYPE_ID = 25L;

    // Query
    private static final Long ASSET_2 = 2L;
    private static final Long ACTIONABLE_ITEM_13 = 13L;
    private static final Long WIP_ACTIONABLE_ITEM_21 = 21L;
    private static final Long WIP_ACTIONABLE_ITEM_26 = 26L;
    private static final Long ITEM_TYPE_2 = 2L;
    private static final Long ITEM_TYPE_4 = 4L;
    private static final Long ITEM_TYPE_327 = 327L;
    private static final Long CATEGORY_ID_1 = 1L;
    private static final Date TODAY = new Date();
    private static final Date A_LONG_TIME_AGO = new Date(0);
    private static final Long CONFIDENTIALITY_1 = 1L;
    private static final Long CONFIDENTIALITY_RETURN_NOTHING = -1L;
    private static final Long STATUS_RETURN_NOTHING = -1L;

    private static final Integer QUERY_TEST_SIZE = 12;

    private static final Long ITEM_TYPE_392_ID = 392L;
    private static final Long INVALID_ITEM_TYPE_ID = 99999L;

    private final DateHelper dateUtils = new DateHelperImpl();

    @DataPoints
    public static final Object[][] THEORY_ARGUMENTS = {{"title", "wrong title"},
                                                       {"description", "Nothing"},
                                                       {"surveyorGuidance", "Nothing"},
                                                       {"surveyorGuidance", null}};

    private ActionableItemDto actionableItemDto;
    private ActionableItemDto actionableItemForPost;
    private ActionableItemPage actionableItemPage;
    private CodicilTemplateDto template;

    @DataPoints("NON_WIP_QUERIES")
    public static final QueryAndResult[] QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @DataPoints("WIP_QUERIES")
    public static final QueryAndResult[] WIP_QUERIES = new QueryAndResult[QUERY_TEST_SIZE];

    @BeforeClass
    public static void addQueries() throws DatabaseUnitException
    {
        int index = 0;
        // Search description
        QUERIES[index] = new QueryAndResult("searchString", "Desc 3", new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        // Search title
        QUERIES[index] = new QueryAndResult("searchString", "3title", new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        // Search Surveyor guidance
        QUERIES[index] = new QueryAndResult("searchString", "sur3", new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1), new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_1_ID), new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_4), new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1), new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{ACTIONABLE_ITEM_13});
        ++index;
        QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING), new Long[]{});
        ++index;
        QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327), new Long[]{});

        index = 0;

        // todo all the wip stuff is based on bad data, re-do when defect-versioning
        // // Search description
        // WIP_QUERIES[index] = new QueryAndResult("searchString", "*ITEM*", new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // // Search title
        // WIP_QUERIES[index] = new QueryAndResult("searchString", "WIPA*", new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // // Search Surveyor guidance
        // WIP_QUERIES[index] = new QueryAndResult("searchString", "SurvW*", new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("categoryList", Collections.singletonList(CATEGORY_ID_1), new
        // Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_2_ID), new
        // Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(339L), new
        // Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("confidentialityList", Collections.singletonList(CONFIDENTIALITY_1),
        // new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("dueDateMin", A_LONG_TIME_AGO, new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("dueDateMax", TODAY, new Long[]{WIP_ACTIONABLE_ITEM_21});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("confidentialityList",
        // Collections.singletonList(CONFIDENTIALITY_RETURN_NOTHING), new Long[]{});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("statusList", Collections.singletonList(STATUS_RETURN_NOTHING), new
        // Long[]{});
        // ++index;
        // WIP_QUERIES[index] = new QueryAndResult("itemTypeList", Collections.singletonList(ITEM_TYPE_327), new
        // Long[]{});
    }

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();
        this.actionableItemDto = DataUtils.getActionableItem(ACTIONABLE_ITEM_4_ID, false);
        this.actionableItemForPost = DataUtils.getActionableItem(ACTIONABLE_ITEM_4_ID, false);
        this.template = DataUtils.getCodicilTemplate(TEMPLATE_39_ID);

        this.actionableItemForPost.setId(null);
        this.actionableItemForPost.setImposedDate(this.createImposedDate().getTime());
        this.actionableItemForPost.setDueDate(this.createDueDate(this.createImposedDate()).getTime());
        this.actionableItemForPost.setAssetItem(null);
        this.actionableItemForPost.setChildWIPs(new ArrayList<LinkResource>());
    }

    private Calendar createDueDate(final Calendar imposedDate)
    {
        final Calendar dueDate = Calendar.getInstance();
        dueDate.setTime(imposedDate.getTime());
        dueDate.add(Calendar.YEAR, 1);
        return dueDate;
    }

    private Calendar createImposedDate()
    {
        final Calendar imposedDate = Calendar.getInstance();
        imposedDate.setTime(new Date());
        imposedDate.set(Calendar.HOUR_OF_DAY, 0);
        imposedDate.set(Calendar.MINUTE, 0);
        imposedDate.set(Calendar.SECOND, 0);
        imposedDate.set(Calendar.MILLISECOND, 0);
        imposedDate.add(Calendar.DAY_OF_MONTH, 1);
        return imposedDate;
    }

    private void initialiseActionableItemPage(final Boolean wip, final Long... ids)
    {
        this.actionableItemPage = new ActionableItemPage();

        ActionableItemDto tempAssetNoteDto;
        for (final Long id : ids)
        {
            try
            {
                tempAssetNoteDto = DataUtils.getActionableItem(id, wip);
                this.actionableItemPage.getContent().add(tempAssetNoteDto);
            }
            catch (final DatabaseUnitException dse)
            {
                dse.printStackTrace();
            }

        }
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item/y results in a positive response. Expecting JSON
     * containing actionable item 1.
     */
    @Test
    public final void testGetActionableItem()
    {
        TestUtils.singleItemFound(format(ACTIONABLE_ITEM, ASSET_2_ID, ACTIONABLE_ITEM_4_ID), this.actionableItemDto, true);
    }

    /**
     * Confirms that one of the known associated WIP elements is in the dto.
     */
    @Test
    public final void testGetActionableItemConfirmWIPChildrenPresent()
    {
        final ActionableItemDto dto = TestUtils.getResponse(format(ACTIONABLE_ITEM, ASSET_2_ID, ACTIONABLE_ITEM_4_ID), HttpStatus.OK)
                .getBody()
                .as(ActionableItemDto.class);

        boolean containsExpectedChild = false;
        for (final LinkResource link : dto.getChildWIPs())
        {
            if (link.getId().equals(WIP_ACTIONABLE_ITEM_26))
            {
                containsExpectedChild = true;
                break;
            }
        }

        Assert.assertTrue(containsExpectedChild);
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item/y returns a 404 for an actionable item in the
     * database with a deleted flag set.
     */
    @Test
    public final void testGetActionableItemForADeletedItem()
    {
        TestUtils.getResponse(format(ACTIONABLE_ITEM, ASSET_2_ID, DELETED_ACTIONABLE_ITEM_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item/y results in a negative response. Expecting a 404
     */
    @Test
    public final void testGetActionableItemNotFound()
    {
        TestUtils.getResponse(format(ACTIONABLE_ITEM, ASSET_2_ID, INVALID_ACTIONABLE_ITEM_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item results in a positive response with a list
     * containing AI 1
     */
    @Test
    public final void testGetActionableItemsForAsset()
    {
        this.initialiseActionableItemPage(false, ACTIONABLE_ITEM_4_ID);
        TestUtils.listItemsFound(format(BASE_ACTIONABLE_ITEM, ASSET_2_ID), this.actionableItemPage, true, false);
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item results in a positive response with a list that does
     * not contain a known deleted item.
     */
    @Test
    public final void testGetActionableItemsDoesNotIncludeDeletedItems()
    {
        this.initialiseActionableItemPage(false, ACTIONABLE_ITEM_4_ID);
        final Response response = TestUtils.getResponse(format(BASE_ACTIONABLE_ITEM, ASSET_2_ID), HttpStatus.OK);

        final ActionableItemPage page = response.getBody().as(ActionableItemPage.class);
        assertNotNull(page);
        assertNotNull(page.getContent());
        for (final ActionableItemDto dto : page.getContent())
        {
            if (dto.getId().equals(DELETED_ACTIONABLE_ITEM_ID))
            {
                Assert.fail("Getting actionable items returned the deleted actionable item id : " + DELETED_ACTIONABLE_ITEM_ID);
            }
        }
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item?status=z results in a positive response with a list
     * containing AI 1
     */
    @Test
    public final void testGetActionableItemForAssetWithStatus()
    {
        this.initialiseActionableItemPage(false, ACTIONABLE_ITEM_4_ID);
        TestUtils.listItemsFound(format(BASE_ACTIONABLE_ITEM.concat("?statusId=%s"), ASSET_2_ID, STATUS_4_ID), this.actionableItemPage, true, false);
    }

    /**
     * Test that a valid GET request for ../asset/x/actionable-item results in an empty response
     */
    @Test
    public final void testGetActionableItemForAssetNonFound()
    {
        this.initialiseActionableItemPage(false, ACTIONABLE_ITEM_4_ID);
        TestUtils.listItemsNotFound(format(BASE_ACTIONABLE_ITEM, INVALID_ASSET_ID));
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/wip-actionable-item returns a valid WIP Actionable Item.
     */
    @Test
    public final void testGetWIPActionableItemForJobAndWIPDefect()
    {
        this.initialiseActionableItemPage(true, WIP_AI_ID_FOR_JOB_AND_WIP_DEFECT);
        TestUtils.listItemsFound(String.format(BASE_JOB_WIP_DEFECT_WIP_AI, VALID_JOB_ID_FOR_WIP_DEFECT, VALID_WIP_DEFECT_ID_FOR_JOB),
                this.actionableItemPage, true, false);
    }

    /**
     * Test valid GET ../job/{x}/wip-defect/{y}/wip-actionable-item returns an empty WIP Actionable Item.
     */
    @Test
    public final void testGetWIPActionableItemForJobAndWIPDefectReturnsEmpty()
    {
        TestUtils.listItemsNotFound(String.format(BASE_JOB_WIP_DEFECT_WIP_AI, VALID_JOB_ID_FOR_WIP_DEFECT, WIP_DEFECT_ID_WITH_NO_WIP_AI));
    }

    /**
     * Test invalid GET ../job/{x}/wip-defect/{y}/wip-actionable-item.
     */
    @Test
    public final void testGetWIPActionableItemForJobAndWIPDefectFails()
    {
        TestUtils.getResponse(String.format(BASE_JOB_WIP_DEFECT_WIP_AI, VALID_JOB_ID_FOR_WIP_DEFECT, INVALID_WIP_DEFECT_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test that a valid POST request for ../asset/x/actionable-item results in a positive response. Expecting JSON
     * containing new actionable-item .
     */
    @Test
    public final void testCreateActionableItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "asset", "dueStatus"};

        final ActionableItemDto body = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.compareFields(body, this.actionableItemForPost, true, ignoredFields);
    }

    /**
     * Test that a POST request for ../asset/x/actionable-item without a due date fails via the asset route as it is
     * mandatory when created in this manner.
     */
    @Test
    public final void testCreateActionableItemWithoutDueDate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.actionableItemForPost.setDueDate(null);
        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"), error.getMessage());
    }

    /**
     * Test that a valid POST request for ../batch-action/x/actionable-item without a due date fails via the asset route
     * as it is mandatory when created in this manner.
     */
    @Test
    public final void testCreateActionableItemWithoutDueDateViaBatchAction()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.getActionableItemDto().setDueDate(null);
        final List<Long> outgoingAssetIds = new ArrayList<>(dto.getAssetIds());
        final BatchActionActionableItemDto result = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.OK,
                BatchActionActionableItemDto.class);
        final Map<Long, List<ActionableItemDto>> actionableItemDtoMap = result.getSavedActionableItemDtos();
        assertNotNull(actionableItemDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(actionableItemDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(outgoingAssetIds.contains(savedId));
        }

        for (final Long outgoingId : outgoingAssetIds)
        {
            assertTrue(savedAssetIds.contains(outgoingId));
        }
    }

    /**
     * Test that a valid POST request for ../asset/x/actionable-item results in a positive response. Expecting JSON
     * containing new actionable-item .
     */
    @Test
    public final void testCreateActionableItemDueDateEqualsImposedDate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "asset", "dueStatus", "statenessHash"};

        this.actionableItemForPost.setDueDate(this.actionableItemForPost.getImposedDate());

        final ActionableItemDto body = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.compareFields(body, this.actionableItemForPost, true, ignoredFields);
    }

    /**
     * Test that a valid POST request for ../asset/x/actionable-item results in a positive response. Expecting JSON
     * containing new actionable-item .
     */
    @Test
    public final void testCreateActionableItemSetStatus()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "asset", "dueStatus", "stalenessHash"};

        final LinkResource status = this.actionableItemForPost.getStatus();

        this.actionableItemForPost.setStatus(status);

        final ActionableItemDto body = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.compareFields(body, this.actionableItemForPost, false, ignoredFields);
    }

    /**
     * Test that an invalid POST request for ../asset/x/actionable-item results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testCreateActionableItemWithId()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.actionableItemForPost.setId(1L);

        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        assertEquals("Field ID is expected to be null in the creation of new entities: 1", error.getMessage());
    }

    /**
     * Test that an invalid POST request for ../asset/x/actionable-item results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testCreateActionableItemImposedDateDayBeforeYesterday()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.DAY_OF_MONTH, MINUS_2);
        this.actionableItemForPost.setImposedDate(date.getTime());
        this.actionableItemForPost.getStatus().setId(STATUS_1_ID);

        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        assertEquals("The imposed date cannot be before today.", error.getMessage());
    }

    /**
     * Test that an invalid POST request for ../asset/x/actionable-item results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testCreateActionableItemNullManditoryFields()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final List<String> expectedErrors = new ArrayList<String>();

        expectedErrors.add("Validation failed with errors");
        expectedErrors.add(" category may not be null");
        expectedErrors.add(" description may not be null");
        expectedErrors.add(" requireApproval may not be null");
        expectedErrors.add(" title may not be null");
        expectedErrors.add(" confidentialityType may not be null");
        expectedErrors.add(" imposedDate may not be null");

        this.actionableItemForPost.setTitle(null);
        this.actionableItemForPost.setCategory(null);
        this.actionableItemForPost.setDescription(null);
        this.actionableItemForPost.setImposedDate(null);
        this.actionableItemForPost.setDueDate(null);
        this.actionableItemForPost.setConfidentialityType(null);
        this.actionableItemForPost.setRequireApproval(null);

        final ErrorMessageDto error = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        final List<String> errors = Arrays.asList(error.getMessage().split("[:,\\.]"));

        assertTrue(expectedErrors.containsAll(errors));
        assertTrue(errors.containsAll(expectedErrors));
    }

    /**
     * Test that an invalid POST request for ../asset/x/actionable-item results in a positive response. Expecting not
     * found exception, for an invalid assetId
     */
    @Test
    public final void testCreateActionableItemAssetNotFound()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ErrorMessageDto error = TestUtils
                .postResponse(format(BASE_ACTIONABLE_ITEM, INVALID_ASSET_ID), this.actionableItemForPost, HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);

        assertEquals("Entity not found for identifier: 999.", error.getMessage());
    }

    /**
     * Check that query parameter 'template' is considered mandatory by not passing it in at all.
     */
    @Test
    public final void countAssetsMissingTemplateIdParameter()
    {
        final ErrorMessageDto error = TestUtils.getResponse(NO_PARAM_TEMPLATE_ASSET_COUNT, HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Query parameter template cannot be null.", error.getMessage());
    }

    /**
     * Path: /asset/actionable-item/count?template= <br>
     * Check that query parameter 'template' is considered mandatory by including it but with no value.
     */
    @Test
    public final void countAssetsForEmptyTemplateId()
    {
        final ErrorMessageDto error = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, ""), HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Could not bind parameter  to number.", error.getMessage());
    }

    /**
     * Path: /asset/actionable-item/count?template=abc <br>
     * Check that giving query parameter 'template' a non-number value will result in a friendly error message.
     */
    @Test
    public final void countAssetsForNonNumberTemplateId()
    {
        final ErrorMessageDto error = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "abc"), HttpStatus.BAD_REQUEST, ErrorMessageDto.class);

        assertEquals("Could not bind parameter abc to number.", error.getMessage());
    }

    /**
     * Path: /asset/actionable-item/count?template=39 <br>
     * Check that a single valid template ID will result in a positive response.
     */
    @Test
    public final void countAssetsForValidSingleTemplateId()
    {
        final TemplateAssetCountDto responseDto = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "39"), HttpStatus.OK,
                TemplateAssetCountDto.class);

        final Map<Long, Integer> counts = responseDto.getCounts();

        assertTrue(counts.containsKey(TEMPLATE_39_ID));
        assertEquals(TEMPLATE_39_COUNT, counts.get(TEMPLATE_39_ID));
    }

    /**
     * Path: /asset/actionable-item/count?template=39,40,41 <br>
     * Check that multiple valid template IDs will result in a positive response.
     */
    @Test
    public final void countAssetsForValidMultipleTemplateId()
    {
        final TemplateAssetCountDto responseDto = TestUtils.getResponse(format(TEMPLATE_ASSET_COUNT, "39,40,41"), HttpStatus.OK,
                TemplateAssetCountDto.class);

        final Map<Long, Integer> counts = responseDto.getCounts();

        assertTrue(counts.containsKey(TEMPLATE_39_ID));
        assertEquals(TEMPLATE_39_COUNT, counts.get(TEMPLATE_39_ID));

        assertTrue(counts.containsKey(TEMPLATE_40_ID));
        assertEquals(TEMPLATE_40_COUNT, counts.get(TEMPLATE_40_ID));

        assertTrue(counts.containsKey(TEMPLATE_41_ID));
        assertEquals(TEMPLATE_41_COUNT, counts.get(TEMPLATE_41_ID));
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting JSON
     * containing updated actionable-item.
     */
    @Test
    public final void testUpdateActionableItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final ActionableItemDto body = TestUtils
                .putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.OK, ActionableItemDto.class);

        TestUtils.compareFields(body, newAi, false);
    }

    @Test
    public final void testUpdateActionableItemFailsWhenAnOldTokenIsUsed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final String originalStalenessHash = newAi.getStalenessHash();

        // Make a small change
        newAi.setSurveyorGuidance(newAi.getSurveyorGuidance() + "X");

        final ActionableItemDto updatedAI = TestUtils
                .putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.OK, ActionableItemDto.class);

        // Make another small change, but use the hash from the original GET
        updatedAI.setDescription(updatedAI.getDescription() + "X");
        updatedAI.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, updatedAI.getId()), updatedAI, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ACTIONABLE_ITEM.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    /**
     * Test that an invalid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testUpdateActionableItemCombinationNotFound()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_2_ID, newAi.getId()), newAi, HttpStatus.NOT_FOUND,
                ErrorMessageDto.class);

        assertEquals("Actionable item with id " + newAi.getId() + " does not exist for Asset " + ASSET_2_ID + ".", error.getMessage());
    }

    /**
     * Test that an invalid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testUpdateActionableItemCombinationNotFound2()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        newAi.setId(newAi.getId() + 1L);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.NOT_FOUND,
                ErrorMessageDto.class);

        assertEquals("Actionable item with id " + newAi.getId() + " does not exist for Asset " + ASSET_1_ID + ".", error.getMessage());
    }

    /**
     * Test that a PUT request for ../asset/x/actionable-item/y results in a positive response, despite the imposed date
     * being historical.
     */
    @Test
    public final void testUpdateActionableItemImposedDateYesterdayStillWorks()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.add(Calendar.DAY_OF_MONTH, -1);

        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        newAi.setImposedDate(date.getTime());

        TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.OK);
    }

    /**
     * Test that an invalid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting a bad
     * request exception.
     */
    @Test
    public final void testUpdateActionableItemNullManditoryFields()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final List<String> expectedErrors = new ArrayList<String>();

        expectedErrors.add("Validation failed with errors");
        expectedErrors.add(" category may not be null");
        expectedErrors.add(" description may not be null");
        expectedErrors.add(" requireApproval may not be null");
        expectedErrors.add(" title may not be null");
        expectedErrors.add(" confidentialityType may not be null");
        expectedErrors.add(" imposedDate may not be null");

        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        newAi.setTitle(null);
        newAi.setCategory(null);
        newAi.setDescription(null);
        newAi.setImposedDate(null);
        newAi.setDueDate(null);
        newAi.setConfidentialityType(null);
        newAi.setRequireApproval(null);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        final List<String> errors = Arrays.asList(error.getMessage().split("[:,\\.]"));

        assertTrue(expectedErrors.containsAll(errors));
        assertTrue(errors.containsAll(expectedErrors));
    }

    /**
     * Test that an invalid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting a bad
     * request exception for null due date which is validated separately from other null fields.
     */
    @Test
    public final void testUpdateActionableItemNullDueDate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        newAi.setDueDate(null);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);

        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"), error.getMessage());
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting JSON
     * containing updated actionable-item.
     */
    @Test
    public final void testUpdateActionableItemWithTemplate()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, DatabaseUnitException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        this.enforceTemplate(newAi);

        final ActionableItemDto body = TestUtils
                .putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.OK, ActionableItemDto.class);

        TestUtils.compareFields(body, newAi, false, new String[]{"stalenessHash"});
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting bad
     * request.
     */
    @Theory
    @Ignore
    public final void testUpdateActionableItemWithTemplateDoesNotMatch(final Object[] theoryArgument)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, DatabaseUnitException,
            NoSuchFieldException, SecurityException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        this.enforceTemplate(newAi);

        final Field field = newAi.getClass().getDeclaredField((String) theoryArgument[0]);
        final boolean isAccessible = field.isAccessible();

        field.setAccessible(true);
        field.set(newAi, theoryArgument[1]);
        field.setAccessible(isAccessible);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals("The actionable item entered does not match the template it is linked to.", error.getMessage());
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting bad
     * request.
     */
    @Test
    public final void testUpdateActionableItemWithTemplateDoesNotMatchCategory()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, DatabaseUnitException,
            NoSuchFieldException, SecurityException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        this.enforceTemplate(newAi);

        newAi.setCategory(new LinkResource(CATEGORY_2_ID));

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals("The actionable item entered does not match the template it is linked to.", error.getMessage());
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting JSON
     * with updated actionable item.
     */
    @Test
    @Ignore("This test relies on a template with an item type to validate against. No such template exists any more.")
    public final void testUpdateActionableItemWithTemplateDoesNotMatchItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, DatabaseUnitException,
            NoSuchFieldException, SecurityException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        this.enforceTemplate(newAi);

        newAi.setAssetItem(new ItemLightDto());
        newAi.getAssetItem().setId(ITEM_88_ID);

        final ErrorMessageDto error = TestUtils.putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.CONFLICT,
                ErrorMessageDto.class);

        assertEquals("Entity with id " + ITEM_88_ID + " is not of type " + this.template.getItemType().getId() + ".", error.getMessage());
    }

    /**
     * Test that a valid PUT request for ../asset/x/actionable-item/y results in a positive response. Expecting bad
     * request.
     */
    @Test
    @Ignore
    public final void testUpdateActionableItemWithTemplateMatchsItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException, DatabaseUnitException,
            NoSuchFieldException, SecurityException
    {
        final ActionableItemDto newAi = TestUtils.postResponse(format(BASE_ACTIONABLE_ITEM, ASSET_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        this.enforceTemplate(newAi);

        final LinkResource itemType = new LinkResource(ITEM_TYPE_ID);

        final ItemLightDto assetItem = new ItemLightDto();
        assetItem.setId(ITEM_87_ID);
        assetItem.setItemType(itemType);
        assetItem.setName(SYSTEM_GROUP);
        assetItem.setReviewed(false);
        assetItem.setDisplayOrder(0);

        newAi.setAssetItem(assetItem);

        final ActionableItemDto body = TestUtils
                .putResponse(format(ACTIONABLE_ITEM, ASSET_1_ID, newAi.getId()), newAi, HttpStatus.OK, ActionableItemDto.class);

        TestUtils.compareFields(body, newAi, false, new String[]{"stalenessHash"});
    }

    /**
     * Test that a valid POST request for ../asset/x/item/y/actionable-item results in a positive response. Expecting
     * JSON containing new actionable-item .
     */
    @Test
    public final void testCreateActionableItemOnItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final String[] ignoredFields = {"id", "sequenceNumber", "asset", "dueStatus", "stalenessHash"};

        this.actionableItemForPost.setAssetItem(new ItemLightDto());
        this.actionableItemForPost.getAssetItem().setId(ITEM_87_ID);

        final ActionableItemDto body = TestUtils
                .postResponse(format(ACTIONABLE_ITEM_ON_ITEM, ASSET_1_ID, ITEM_87_ID), this.actionableItemForPost, HttpStatus.OK,
                        ActionableItemDto.class);

        TestUtils.compareFields(body, this.actionableItemForPost, false, ignoredFields);
    }

    /**
     * Test that a valid POST request for ../asset/x/item/y/actionable-item but with a null due date results in an
     * error.
     */
    @Test
    public final void testCreateActionableItemOnItemNullDueDate()
    {
        this.actionableItemForPost.setAssetItem(new ItemLightDto());
        this.actionableItemForPost.getAssetItem().setId(ITEM_87_ID);
        this.actionableItemForPost.setDueDate(null);

        final ErrorMessageDto error = TestUtils
                .postResponse(format(ACTIONABLE_ITEM_ON_ITEM, ASSET_1_ID, ITEM_87_ID), this.actionableItemForPost, HttpStatus.BAD_REQUEST,
                        ErrorMessageDto.class);

        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"), error.getMessage());
    }

    /**
     * Test that an invalid POST request for ../asset/x/item/y/actionable-item results in a positive response. Expecting
     * a not found exception.
     */
    @Test
    public final void testCreateActionableItemOnItemNotPartOfAsset()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ErrorMessageDto error = TestUtils
                .postResponse(format(ACTIONABLE_ITEM_ON_ITEM, ASSET_1_ID, ITEM_1_ID), this.actionableItemForPost, HttpStatus.NOT_FOUND,
                        ErrorMessageDto.class);

        assertEquals("Item with identifier " + ASSET_1_ID + " not found for asset " + ITEM_1_ID, error.getMessage());
    }

    /**
     * Test that an invalid POST request for ../asset/x/item/y/actionable-item results in a positive response. Expecting
     * a bad request exception.
     */
    @Test
    public final void testCreateActionableItemOnItemIdsDoNotMatch()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.actionableItemForPost.setAssetItem(new ItemLightDto());
        this.actionableItemForPost.getAssetItem().setId(ITEM_87_ID);

        final ErrorMessageDto error = TestUtils
                .postResponse(format(ACTIONABLE_ITEM_ON_ITEM, ASSET_1_ID, ITEM_1_ID), this.actionableItemForPost, HttpStatus.BAD_REQUEST,
                        ErrorMessageDto.class);

        assertEquals("The  ID in the request body " + ITEM_87_ID + ", does not match the resource id " + ITEM_1_ID, error.getMessage());
    }

    private void enforceTemplate(final ActionableItemDto newAi) throws DatabaseUnitException
    {
        newAi.setTemplate(new LinkResource(this.template.getId()));

        newAi.setTitle(this.template.getTitle());
        newAi.setDescription(this.template.getDescription());
        newAi.setSurveyorGuidance(this.template.getSurveyorGuidance());
        newAi.setConfidentialityType(this.refDataToLink(this.template.getConfidentialityType()));
        newAi.setEditableBySurveyor(this.template.getEditableBySurveyor());
        newAi.setCategory(this.refDataToLink(this.template.getCategory()));
        newAi.setRequireApproval(Boolean.TRUE);
    }

    private LinkResource refDataToLink(final ReferenceDataDto refData)
    {
        final LinkResource link = new LinkResource(refData.getId());

        return link;
    }

    @Test
    public final void testDeleteActionableItemThatDoesNotExist()
    {
        final String url = format(ACTIONABLE_ITEM, ASSET_2_ID, INVALID_ACTIONABLE_ITEM_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteActionableItemNotAssociatedWithThePassedAsset()
    {
        final String url = format(ACTIONABLE_ITEM, ASSET_1_ID, ACTIONABLE_ITEM_4_ID);
        TestUtils.deleteResponse(url, HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteActionableItem()
    {
        final String url = format(ACTIONABLE_ITEM, ASSET_2_ID, DELETABLE_ACTIONABLE_ITEM_ID);
        TestUtils.deleteResponse(url, HttpStatus.OK);

        // Confirm that the actionable item is no longer returned.
        TestUtils.getResponse(format(ACTIONABLE_ITEM, ASSET_2_ID, DELETABLE_ACTIONABLE_ITEM_ID), HttpStatus.NOT_FOUND);
    }

    /**
     * Test /asset/x/actionable-item/query with various parameters
     */
    @Theory
    public final void queries(@FromDataPoints("NON_WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.initialiseActionableItemPage(false, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final ActionableItemPage results = TestUtils.postResponse(format(ACTIONABLE_ITEM_QUERY, ASSET_2), query, HttpStatus.OK,
                ActionableItemPage.class);

        TestUtils.compareFields(results, this.actionableItemPage, true);
    }

    @Ignore
    @Theory
    public final void wipQueries(@FromDataPoints("WIP_QUERIES") final QueryAndResult queryAndResult)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        System.out.println(queryAndResult.getQueryField());
        System.out.println(queryAndResult.getQueryValue());
        System.out.println(Arrays.toString(queryAndResult.getResult()));

        this.initialiseActionableItemPage(true, queryAndResult.getResult());

        final CodicilDefectQueryDto query = new CodicilDefectQueryDto();

        int index = 0;

        for (final String fieldName : queryAndResult.getQueryField())
        {
            final Field field = query.getClass().getDeclaredField(fieldName);
            final boolean isAccessible = field.isAccessible();

            field.setAccessible(true);
            field.set(query, queryAndResult.getQueryValue().get(index));
            field.setAccessible(isAccessible);

            ++index;
        }

        final ActionableItemPage results = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM_QUERY, JOB_1_ID), query, HttpStatus.OK).jsonPath()
                .getObject("",
                        ActionableItemPage.class);

        TestUtils.compareFields(results, this.actionableItemPage, true);
    }

    /**
     * Test that a valid POST request for ../job/x/wip-actionable-item results in a positive response. Expecting JSON
     * containing new WIP actionable item.
     */
    @Test
    public final void testCreateWIPActionableItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        final ActionableItemDto body = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        Assert.assertNotNull(body.getId());

        final String[] ignoredFields = {"id", "childWIPs", "sequenceNumber", "dueStatus", "stalenessHash"};

        TestUtils.compareFields(body, this.actionableItemForPost, true, ignoredFields);

        // Now perform a GET to make sure the item has been persisted.
        final ActionableItemDto retrievedAI = TestUtils.getResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, body.getId()), HttpStatus.OK,
                ActionableItemDto.class);
        TestUtils.compareFields(retrievedAI, body, true);
    }

    /**
     * Test that a valid POST request for ../job/x/wip-actionable-item with a null due date results in an error respone.
     */
    @Test
    public final void testCreateWIPActionableItemNullDueDate()
    {
        this.setWipFieldsOnActionableItem();
        this.actionableItemForPost.setDueDate(null);
        final ErrorMessageDto error = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"), error.getMessage());
    }

    private void setWipFieldsOnActionableItem()
    {
        final LinkResource job = new LinkResource(JOB_1_ID);
        this.actionableItemForPost.setJob(job);
        this.actionableItemForPost.setJobScopeConfirmed(false);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-actionable-item where the job id does not exist.
     */
    @Test
    public final void testCreateWIPActionableItemFailsWithInvalidJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, INVALID_JOB_1_ID), this.actionableItemForPost, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-actionable-item where the item already has an id.
     */
    @Test
    public final void testCreateWIPActionableItemFailsWithAnItemThatAlreadyHasAnId()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto dodgyDto = new ActionableItemDto();
        dodgyDto.setId(ACTIONABLE_ITEM_4_ID);
        TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), dodgyDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-actionable-item where the job scope is not confirmed.
     */
    @Test
    public final void testCreateWIPActionableItemFailsNullJobScopeConfirmed()
    {
        this.setWipFieldsOnActionableItem();
        this.actionableItemForPost.setJobScopeConfirmed(null);
        final ErrorMessageDto error = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());

    }

    /**
     * Test that an invalid POST request for ../job/x/wip-actionable-item fails when the parent id doesn't exist.
     */
    @Test
    public final void testCreateWIPActionableItemFailsWhenParentIdNotInDatabase()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto actionableItemDtoForTest = this.createActionableItemForWIPPost();
        final LinkResource parent = new LinkResource(INVALID_ACTIONABLE_ITEM_ID);
        actionableItemDtoForTest.setParent(parent);

        final String errorMessage = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), actionableItemDtoForTest, HttpStatus.NOT_FOUND)
                .jsonPath().get("message");
        assertEquals(String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.", INVALID_ACTIONABLE_ITEM_ID),
                errorMessage);
    }

    private ActionableItemDto createActionableItemForWIPPost()
    {
        final ActionableItemDto result = new ActionableItemDto();

        result.setCategory(this.actionableItemDto.getCategory());
        result.setConfidentialityType(this.actionableItemDto.getConfidentialityType());
        result.setDefect(this.actionableItemDto.getDefect());
        result.setDescription(this.actionableItemDto.getDescription());
        result.setEditableBySurveyor(this.actionableItemDto.getEditableBySurveyor());
        result.setEmployee(this.actionableItemDto.getEmployee());
        result.setInternalId(this.actionableItemDto.getInternalId());
        result.setRequireApproval(this.actionableItemDto.getRequireApproval());
        result.setStatus(new LinkResource(CodicilStatusType.AI_OPEN.getValue()));
        result.setSurveyorGuidance(this.actionableItemDto.getSurveyorGuidance());
        result.setTemplate(this.actionableItemDto.getTemplate());
        result.setTitle(this.actionableItemDto.getTitle());

        result.setImposedDate(this.createImposedDate().getTime());
        result.setDueDate(this.createDueDate(this.createImposedDate()).getTime());
        result.setJobScopeConfirmed(Boolean.TRUE);

        final LinkResource job = new LinkResource(JOB_1_ID);
        result.setJob(job);

        return result;
    }

    /**
     * Test that an invalid POST request for ../job/x/wip-actionable-item fails when the parent is not of the same
     * codicil type.
     */
    @Test
    public final void testCreateWIPActionableItemFailsWhenParentTypeDifferent()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        final ActionableItemDto actionableItemDtoForTest = this.createActionableItemForWIPPost();
        final LinkResource parent = new LinkResource(EXISTING_CODICIL_OF_A_DIFFERENT_TYPE_ID);
        actionableItemDtoForTest.setParent(parent);

        final String errorMessage = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), actionableItemDtoForTest, HttpStatus.NOT_FOUND)
                .jsonPath().get("message");
        assertEquals(String.format("The original codicil id relating to this new WIP codicil is invalid. ID : %s.",
                EXISTING_CODICIL_OF_A_DIFFERENT_TYPE_ID),
                errorMessage);
    }

    /**
     * Test that a valid PUT request for ../job/x/wip-actionable-item/y results in a positive response. Expecting JSON
     * containing new WIP actionable item.
     */
    @Test
    public final void testUpdateWIPActionableItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        // Create a new item for update testing to avoid clashing with other test data.
        final ActionableItemDto newDto = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final Long newItemId = newDto.getId();
        Assert.assertNotNull(newItemId);

        final String title = "New title";
        final String description = "New description";

        newDto.setTitle(title);
        newDto.setDescription(description);

        final ActionableItemDto updatedBody = TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, newItemId), newDto, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.compareFields(updatedBody, newDto, false, new String[]{"stalenessHash"});
    }

    @Test
    public final void testUpdateWIPActionableItemFailsWhenAnOldTokenIsUsed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();

        final ActionableItemDto newAi = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK)
                .jsonPath().getObject("", ActionableItemDto.class);

        final String originalStalenessHash = newAi.getStalenessHash();

        // Make a small change
        newAi.setSurveyorGuidance(newAi.getSurveyorGuidance() + "X");

        final ActionableItemDto updatedAI = TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, newAi.getId()), newAi, HttpStatus.OK)
                .jsonPath().getObject("", ActionableItemDto.class);

        // Make another small change, but use the hash from the original GET
        updatedAI.setDescription(updatedAI.getDescription() + "X");
        updatedAI.setStalenessHash(originalStalenessHash);

        final ErrorMessageDto error = TestUtils
                .putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, updatedAI.getId()), updatedAI, HttpStatus.CONFLICT)
                .jsonPath().getObject("", ErrorMessageDto.class);

        assertEquals(String.format(ExceptionMessagesUtils.UPDATING_STALE_OBJECT, StaleCheckType.ACTIONABLE_ITEM.getTypeString()), error.getMessage());
        assertEquals(ExceptionType.STALE_ENTITY_UPDATED.getExceptionCode(), error.getExceptionCode());

    }

    /**
     * Test that a valid PUT request for ../job/x/wip-actionable-item/y but with a null due date results in an errore
     * response.
     */
    @Test
    public final void testUpdateWIPActionableItemNullDueDate()
    {
        this.setWipFieldsOnActionableItem();
        // Create a new item for update testing to avoid clashing with other test data.
        final ActionableItemDto newDto = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final Long newItemId = newDto.getId();
        Assert.assertNotNull(newItemId);
        newDto.setDueDate(null);

        final ErrorMessageDto error = TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, newItemId), newDto, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Due Date"), error.getMessage());

    }

    /**
     * Test that an invalid PUT request for ../job/x/wip-actionable-item/X where the job id does not exist.
     */
    @Test
    public final void testUpdateWIPActionableItemFailsWithInvalidJob()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, INVALID_JOB_1_ID, ACTIONABLE_ITEM_4_ID), this.actionableItemForPost,
                HttpStatus.BAD_REQUEST);
    }

    /**
     * Test that an invalid PUT request for ../job/x/wip-actionable-item/X where the item does not exist.
     */
    @Test
    public final void testUpdateWIPActionableItemFailsWithInvalidItem()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, INVALID_ACTIONABLE_ITEM_ID), this.actionableItemForPost,
                HttpStatus.NOT_FOUND);
    }

    /**
     * Test that an invalid PUT request for ../job/x/wip-actionable-item/X returns an error when job scope confirmed is
     * null
     */
    @Test
    public final void testUpdateWIPActionableItemFailsNullJobScopeConfirmed()
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IntegrationTestException
    {
        this.setWipFieldsOnActionableItem();
        // Create a new item for update testing to avoid clashing with other test data.
        final ActionableItemDto newDto = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        final Long newItemId = newDto.getId();
        Assert.assertNotNull(newItemId);
        newDto.setJobScopeConfirmed(null);
        final ErrorMessageDto error = TestUtils.putResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, newItemId), newDto, HttpStatus.BAD_REQUEST,
                ErrorMessageDto.class);
        assertEquals(format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Job Scope Confirmed"), error.getMessage());
    }

    @Test
    public final void testDeleteWIPActionableItemThatDoesNotExist()
    {
        TestUtils.deleteResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, INVALID_ACTIONABLE_ITEM_ID), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteWIPActionableItemNotAssociatedWithThePassedJob()
    {
        this.setWipFieldsOnActionableItem();
        final ActionableItemDto body = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.deleteResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_2_ID, body.getId()), HttpStatus.NOT_FOUND);
    }

    @Test
    public final void testDeleteWIPActionableItem()
    {
        this.setWipFieldsOnActionableItem();
        final ActionableItemDto body = TestUtils.postResponse(format(WIP_ACTIONABLE_ITEM, JOB_1_ID), this.actionableItemForPost, HttpStatus.OK,
                ActionableItemDto.class);

        TestUtils.deleteResponse(format(WIP_ACTIONABLE_ITEM_WITH_ID, JOB_1_ID, body.getId()), HttpStatus.OK);
    }

    @Test
    public final void testSaveValidBatchActionAssetNote()
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        final List<Long> outgoingAssetIds = new ArrayList<>(dto.getAssetIds());
        final BatchActionActionableItemDto result = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.OK,
                BatchActionActionableItemDto.class);
        final Map<Long, List<ActionableItemDto>> actionableItemDtoMap = result.getSavedActionableItemDtos();
        assertNotNull(actionableItemDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(actionableItemDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(outgoingAssetIds.contains(savedId));
        }

        for (final Long outgoingId : outgoingAssetIds)
        {
            assertTrue(savedAssetIds.contains(outgoingId));
        }
    }

    @Test
    public final void testSaveValidBatchActionActionableItemFiltersNull()
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, null, ASSET_2_ID));
        final List<Long> outgoingAssetIds = new ArrayList<>(dto.getAssetIds());
        final BatchActionActionableItemDto result = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.OK,
                BatchActionActionableItemDto.class);

        final Map<Long, List<ActionableItemDto>> actionableItemDtoMap = result.getSavedActionableItemDtos();
        assertNotNull(actionableItemDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(actionableItemDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(outgoingAssetIds.contains(savedId));
        }

        for (final Long outgoingId : outgoingAssetIds)
        {
            if (outgoingId != null)
            {
                assertTrue(savedAssetIds.contains(outgoingId));
            }
            else
            {
                assertTrue(!savedAssetIds.contains(outgoingId));
            }
        }
    }

    @Test
    public final void testSaveValidBatchActionActionableItemRemovesDuplicateAssetIds()
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_1_ID));
        final BatchActionActionableItemDto result = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.OK,
                BatchActionActionableItemDto.class);
        final Map<Long, List<ActionableItemDto>> actionableItemDtoMap = result.getSavedActionableItemDtos();
        assertNotNull(actionableItemDtoMap);
        Assert.assertNull(result.getRejectedAssetIds());

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(actionableItemDtoMap);

        assertTrue(savedAssetIds.size() == 1);
        assertTrue(savedAssetIds.contains(ASSET_1_ID));
    }

    @Test
    public final void testSaveValidBatchActionActionableItemFailsAllWithSingleInvalidAssetId()
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_2_ID, ASSET_3_ID, INVALID_ASSET_ID));
        TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.NOT_FOUND);
    }

    // Story 7.2 AC 5
    @Test
    public final void testSaveBatchActionFailsWithHistoricalImposed() throws MastBusinessException
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.getActionableItemDto().getStatus().setId(CodicilStatusType.AI_INACTIVE.getValue());
        dto.getActionableItemDto().setImposedDate(this.dateUtils.adjustDate(new Date(), MINUS_2, Calendar.DAY_OF_MONTH));
        final String errorMessage = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.BAD_REQUEST)
                .jsonPath().get("message");
        assertTrue(errorMessage.startsWith("The imposed date cannot be before today"));
    }

    @Test
    @Ignore
    public final void testSaveValidBatchActionActionableItemWithItemType() throws DatabaseUnitException
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.setItemTypeId(ITEM_TYPE_392_ID);
        dto.setAssetIds(Arrays.asList(ASSET_5_ID, ASSET_3_ID, ASSET_1_ID));
        final List<Long> expectedSavedAssetIds = Arrays.asList(ASSET_5_ID, ASSET_3_ID);
        final List<Long> expectedRejectedAssetIds = Arrays.asList(ASSET_1_ID);

        final BatchActionActionableItemDto result = TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.OK,
                BatchActionActionableItemDto.class);

        final Map<Long, List<ActionableItemDto>> actionableItemDtoMap = result.getSavedActionableItemDtos();
        final List<Long> rejectedAssetIds = result.getRejectedAssetIds();
        assertNotNull(actionableItemDtoMap);
        assertNotNull(rejectedAssetIds);

        assertEquals(actionableItemDtoMap.size(), expectedSavedAssetIds.size());
        assertEquals(rejectedAssetIds.size(), expectedRejectedAssetIds.size());
        assertTrue(rejectedAssetIds.contains(ASSET_1_ID));

        final List<Long> savedAssetIds = this.getAssetIdsFromDtoMap(actionableItemDtoMap);

        for (final Long savedId : savedAssetIds)
        {
            assertTrue(expectedSavedAssetIds.contains(savedId));
        }

        for (final Long expectedId : expectedSavedAssetIds)
        {
            assertTrue(savedAssetIds.contains(expectedId));
        }

        for (final Long assetId : actionableItemDtoMap.keySet())
        {
            final List<ActionableItemDto> inspectThisActionableItemDtoList = actionableItemDtoMap.get(assetId);
            for (final ActionableItemDto inspectThisActionableItemDto : inspectThisActionableItemDtoList)
            {
                final ItemLightDto itemDto = DataUtils.getItemLight(inspectThisActionableItemDto.getAssetItem().getId());
                assertEquals(itemDto.getItemType().getId(), ITEM_TYPE_392_ID);
            }
        }
    }

    @Test
    public final void testBatchActionAssetNoteInvalidTypeId()
    {
        final BatchActionActionableItemDto dto = this.createValidBatchActionActionableItemDto();
        dto.setItemTypeId(INVALID_ITEM_TYPE_ID);
        TestUtils.postResponse(BATCH_ACTION_ACTIONABLE_ITEM, dto, HttpStatus.NOT_FOUND);
    }

    private List<Long> getAssetIdsFromDtoMap(final Map<Long, List<ActionableItemDto>> actionableItemDtoMap)
    {
        final List<Long> extractedAssetIds = new ArrayList<>();
        for (final Long assetId : actionableItemDtoMap.keySet())
        {
            actionableItemDtoMap.get(assetId).stream()
                    .forEach(localDto -> extractedAssetIds.add(localDto.getAsset().getId()));
        }
        return extractedAssetIds;
    }

    private BatchActionActionableItemDto createValidBatchActionActionableItemDto()
    {
        final BatchActionActionableItemDto dto = new BatchActionActionableItemDto();
        final ActionableItemDto aiDto = this.createActionableItemForWIPPost();
        aiDto.setDescription("This is a batch action Actionable Item");
        aiDto.setJobScopeConfirmed(false);
        dto.setActionableItemDto(aiDto);
        dto.setAssetIds(Arrays.asList(ASSET_1_ID, ASSET_2_ID, ASSET_3_ID));
        return dto;
    }
}
