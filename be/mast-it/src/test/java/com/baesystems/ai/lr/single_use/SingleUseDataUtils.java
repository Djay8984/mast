package com.baesystems.ai.lr.single_use;

import com.baesystems.ai.lr.utils.DatabaseUtils;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class SingleUseDataUtils
{
    private static IDataSet mastDataSet;
    private static boolean inserted;

    private SingleUseDataUtils()
    {
    }

    static void loadTestData()
            throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException, InstantiationException, IllegalAccessException
    {
        if (!inserted)
        {
            mastDataSet = DatabaseUtils.getDataSet("mast");
            inserted = true;
        }
    }

    static Map<Long, ItemRelationshipRulesTest.Asset> getAllAssets() throws DatabaseUnitException
    {
        final Map<Long, ItemRelationshipRulesTest.Asset> assetList = new HashMap<>();
        final ITable table = mastDataSet.getTable("MAST_ASSET_Asset");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final ItemRelationshipRulesTest.Asset asset = new ItemRelationshipRulesTest.Asset();
            asset.setId(toLong(table, counter, "id"));
            asset.setCategoryId(toLong(table, counter, "asset_category_id"));
            assetList.put(asset.getId(), asset);
        }
        return assetList;
    }

    static Map<Long, ItemRelationshipRulesTest.Item> getAllItems() throws DatabaseUnitException
    {
        final Map<Long, ItemRelationshipRulesTest.Item> itemsList = new HashMap<>();
        final ITable table = mastDataSet.getTable("MAST_ASSET_AssetItem");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final ItemRelationshipRulesTest.Item item = new ItemRelationshipRulesTest.Item();
            item.setId(toLong(table, counter, "id"));
            item.setItemTypeId(toLong(table, counter, "item_type_id"));
            item.setAssetId(toLong(table, counter, "asset_id"));
            itemsList.put(item.getId(), item);
        }
        return itemsList;
    }

    static List<ItemRelationshipRulesTest.ItemRelationship> getAllItemRelationships() throws DatabaseUnitException
    {
        final List<ItemRelationshipRulesTest.ItemRelationship> relationshipList = new ArrayList<>();
        final ITable table = mastDataSet.getTable("MAST_ASSET_AssetItemRelationship");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final ItemRelationshipRulesTest.ItemRelationship relationship = new ItemRelationshipRulesTest.ItemRelationship();
            relationship.setFromId(toLong(table, counter, "from_item_id"));
            relationship.setToId(toLong(table, counter, "to_item_id"));
            relationship.setRelationshipTypeId(toLong(table, counter, "relationship_type_id"));
            relationshipList.add(relationship);
        }
        return relationshipList;
    }

    static List<ItemRelationshipRulesTest.ItemTypeRelationship> getAllItemTypeRelationships() throws DataSetException
    {
        final List<ItemRelationshipRulesTest.ItemTypeRelationship> typeRelationshipList = new ArrayList<>();
        final ITable table = mastDataSet.getTable("MAST_REF_AssetItemTypeRelationship");
        final int numberOfRows = table.getRowCount();
        for (int counter = 0; counter < numberOfRows; counter++)
        {
            final ItemRelationshipRulesTest.ItemTypeRelationship typeRelationship = new ItemRelationshipRulesTest.ItemTypeRelationship();
            typeRelationship.setFromTypeId(toLong(table, counter, "from_item_type_id"));
            typeRelationship.setToTypeId(toLong(table, counter, "to_item_type_id"));
            typeRelationship.setAssetCategoryId(toLong(table, counter, "asset_category_id"));
            typeRelationship.setRelationshipTypeId(toLong(table, counter, "relationship_type_id"));
            typeRelationshipList.add(typeRelationship);
        }
        return typeRelationshipList;
    }

    private static Long toLong(final ITable table, final int counter, final String column) throws DataSetException
    {
        final Object val = table.getValue(counter, column);
        return val != null ? ((Number) val).longValue() : null;
    }
}
