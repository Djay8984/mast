package com.baesystems.ai.lr.pages;

import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;

public class ReportWithContentDto extends ReportLightDto
{
    private static final long serialVersionUID = -638774888529095928L;

    private ReportContentDto content;

    public void setContent(final ReportContentDto content)
    {
        this.content = content;
    }

    public ReportContentDto getContent()
    {
        return content;
    }
}
