package com.baesystems.ai.lr.rest.flagports;

import static java.lang.String.format;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.pages.PortOfRegistryPage;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.DataUtils;
import com.baesystems.ai.lr.utils.TestUtils;

/**
 * Class to test the REST interface for the Port of Registry functionality. See test/resources/insert_test_data.sql for
 * the data used by these tests.
 */
@Category(IntegrationTest.class)
public class PortRestTest extends BaseRestTest
{
    private static final String BASE_PORT_OF_REGISTRY_PATH = new StringBuffer(BASE_PATH).append("/port-of-registry").toString();
    private static final String SINGLE_PORT_OF_REGISTRY_PATH = new StringBuffer(BASE_PORT_OF_REGISTRY_PATH).append("/%d").toString();
    private static final String PORT_OF_REGISTRY_FS_SEARCH_PATH = new StringBuffer(BASE_PORT_OF_REGISTRY_PATH).append("?flagState=%d").toString();

    private static final Long VALID_PORT_OF_REGISTRY = 1L;
    private static final Long PORT_OF_REGISTRY_252_ID = 252L;
    private static final Long PORT_OF_REGISTRY_454_ID = 454L;
    private static final Long PORT_OF_REGISTRY_5719_ID = 5719L;
    private static final Long FLAG_STATE_5_ID = 5L;
    private static final Long INVALID_PORT_ID = 10000L;

    private PortOfRegistryDto portOfRegistry;
    private PortOfRegistryPage portPage;

    @Override
    @Before
    public void setUp() throws DatabaseUnitException
    {
        super.setUp();

        portOfRegistry = DataUtils.getPortOfRegistry(VALID_PORT_OF_REGISTRY);
    }

    private void initialisePortPage(final Long... ids)
    {
        portPage = new PortOfRegistryPage();

        PortOfRegistryDto tempPort;
        for (final Long id : ids)
        {
            tempPort = new PortOfRegistryDto();
            tempPort.setId(id);
            portPage.getContent().add(tempPort);
        }
    }

    @Test
    public final void getPorts()
    {
        initialisePortPage(VALID_PORT_OF_REGISTRY);
        TestUtils.listItemsFound(BASE_PORT_OF_REGISTRY_PATH, portPage, true, false);
    }

    @Test
    public final void getPortsWithFlagState()
    {
        initialisePortPage(PORT_OF_REGISTRY_252_ID, PORT_OF_REGISTRY_454_ID, PORT_OF_REGISTRY_5719_ID);
        TestUtils.listItemsFound(format(PORT_OF_REGISTRY_FS_SEARCH_PATH, FLAG_STATE_5_ID), portPage, true, false);
    }

    @Test
    public final void getPortOfRegistry() throws DataSetException
    {
        TestUtils.singleItemFound(format(SINGLE_PORT_OF_REGISTRY_PATH, VALID_PORT_OF_REGISTRY.longValue()), portOfRegistry, true);
    }

    @Test
    public final void getInvalidPortOfRegistry()
    {
        TestUtils.singleItemNotFound(format(SINGLE_PORT_OF_REGISTRY_PATH, INVALID_PORT_ID.longValue()));
    }
}
