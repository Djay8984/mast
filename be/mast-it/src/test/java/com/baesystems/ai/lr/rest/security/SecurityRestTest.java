package com.baesystems.ai.lr.rest.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.baesystems.ai.lr.constants.MastHeaders;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.SignUtils;
import com.jayway.restassured.response.Headers;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpStatus;

import com.baesystems.ai.lr.categories.IntegrationTest;
import com.baesystems.ai.lr.dto.security.UserDto;
import com.baesystems.ai.lr.rest.BaseRestTest;
import com.baesystems.ai.lr.utils.SecureURLHelper;
import com.baesystems.ai.lr.utils.TestUtils;
import com.jayway.restassured.response.Header;

import java.util.Arrays;
import java.util.Random;

@Category(IntegrationTest.class)
public class SecurityRestTest extends BaseRestTest
{
    public static final String CURRENT_USER_PATH = urlBuilder(BASE_PATH, "/current-user");

    private static final String TRUSTED_SYSTEMS_BASE_PATH = "/mast/trusted-system/api/v2";
    private static final String TRUSTED_SYSTEMS_ASSET = urlBuilder(TRUSTED_SYSTEMS_BASE_PATH, "/asset");

    private static final String CDF_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----" + '\n'
            + "MIICXQIBAAKBgQDt7f62voVghlY+EiN53QyEoCPaq8JM7l6/YWTuMEbVc6xi30kA" + '\n'
            + "xp1XF9jH2XbtDNZBrF9mz4PqIn8jwAiSs6rZddi3fbIc68560GGSFeCzXqbIsFDA" + '\n'
            + "pvFmwuSlUNspVYTtaieT+VUQ5lGH4MAl6S2KujpPdItsnN6BKr87QNFz/QIDAQAB" + '\n'
            + "AoGAKTwb7tPqDaZ6f9to5o/WwY13aUhnLioJpaVddqJLRGb9s0z7O/UwA44QdeJM" + '\n'
            + "aBzXbR5QkmLYe9Vqr2lykuosa5oalfAmjHFVVf2RlQ78GuEuoAOFVdNYbgFpDV+p" + '\n'
            + "acrJ7BvYxuqb1k4mW0xrcXlQcoDBsSSvGH/BiyjWlmia+4kCQQD93kituxCO4U1v" + '\n'
            + "d6w5fGJ/x8F15ankVbGeiPzaSWVPl3hwW0pF0O2chrTFgW2nlcBBo9wChCDASnTp" + '\n'
            + "RPnvhK/jAkEA7+1zCHt5orZ3pbZ+ktfDvw6aoZKAizOARMpl3cf2U3gB8OXI+KY9" + '\n'
            + "1vZ5EUQWIyq0lLZS2GVkrjyezAJRkGDSnwJBAPo5IkG64/MwtvZramGu7BcueM0P" + '\n'
            + "9YiNvmiYdUFinmz4y0lTcb2m1M03KO6TR9WqkLkiHTSNrwpZWSqjRgaDLikCQEMZ" + '\n'
            + "/LcdmWo5cAMHpCS7dYKEuhiAGbVUxQX4OT8qwqibgqd2DJwGGnt5WWjLywkw02pY" + '\n'
            + "B5l/2gWk7KCP8XHYsm8CQQDkzz6ViCwmlwabDYUXp91dkWH2rrWs+BK3Wkt/W45C" + '\n'
            + "3UYfeXSxR8+FVQ+zLg7SI7T7pg4TLQa6cSQ/0IN+tPhs" + '\n'
            + "-----END RSA PRIVATE KEY-----";

    private static final Long ASSET_1 = 1L;

    @Test
    public final void testGetCurrentUser()
    {
        TestUtils.flushCookies();
        TestUtils.addCustomHeader(new Header("user", SecureURLHelper.USER));
        final UserDto currentUser = TestUtils.getResponse(CURRENT_USER_PATH, HttpStatus.OK).getBody().as(UserDto.class);
        assertNotNull(currentUser);
        assertEquals(SecureURLHelper.USER, currentUser.getUserName());
        assertNotNull(currentUser.getGroupList());
        assertEquals(1, currentUser.getGroupList().size());
        assertEquals(SecureURLHelper.GROUP, currentUser.getGroupList().get(0));
    }

    /**
     * POST to the /external-systems/api/v2/asset, signed with cdf key, expect to be able to successfully post
     */
    @Test
    public void postUsingExternalSystem()
    {
        TestUtils.flushCookies();

        final AssetLightDto asset = getAsset();

        TestUtils.flushCookies();

        final String bodyAsString = TestUtils.convertBodyToString(asset);

        final String checkSum = SignUtils.generateChecksum("POST", TRUSTED_SYSTEMS_ASSET, bodyAsString, CDF_PRIVATE_KEY);

        final Header signHeader = new Header(MastHeaders.IDS_CLIENT_SIGN, checkSum);
        final Header versionHeader = new Header(MastHeaders.IDS_CLIENT_IDENTITY, "cdf");
        final Header contentTypeHeader = new Header("Content-Type", "application/json");
        final Headers headers = new Headers(Arrays.asList(signHeader, versionHeader, contentTypeHeader));

        TestUtils.postResponse(TRUSTED_SYSTEMS_ASSET, bodyAsString, HttpStatus.OK, headers);
    }

    /**
     * POST to the /api/v2/asset, signed with cdf key, expect to get unauthorized 401
     */
    @Test
    public void postUsingExternalSystemToMastClientAPI()
    {
        TestUtils.flushCookies();

        final AssetLightDto asset = getAsset();

        TestUtils.flushCookies();

        final String bodyAsString = TestUtils.convertBodyToString(asset);

        final String checkSum = SignUtils.generateChecksum("POST", BASE_ASSET, bodyAsString, CDF_PRIVATE_KEY);

        final Header signHeader = new Header(MastHeaders.IDS_CLIENT_SIGN, checkSum);
        final Header versionHeader = new Header(MastHeaders.IDS_CLIENT_IDENTITY, "cdf");
        final Header contentTypeHeader = new Header("Content-Type", "application/json");
        final Headers headers = new Headers(Arrays.asList(signHeader, versionHeader, contentTypeHeader));

        TestUtils.postResponse(BASE_ASSET, bodyAsString, HttpStatus.FOUND, headers);
    }

    private AssetLightDto getAsset()
    {

        final AssetLightDto existingAsset = TestUtils.getResponse(String.format(SPECIFIC_ASSET, ASSET_1), HttpStatus.OK)
                .as(AssetLightDto.class);
        existingAsset.setId(null);
        existingAsset.setBuilder(existingAsset.getBuilder().concat(Integer.toString(new Random().nextInt())));
        existingAsset.setIhsAsset(null);

        CollectionUtils.nullSafeStream(existingAsset.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });
        CollectionUtils.nullSafeStream(existingAsset.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));
        return existingAsset;
    }
}
