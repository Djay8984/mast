package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dto.references.CodicilStatusDto;
import com.baesystems.ai.lr.dto.references.CodicilTemplateDto;
import com.baesystems.ai.lr.dto.references.CodicilTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "CodicilReferenceDataService")
@Loggable(Loggable.DEBUG)
public class CodicilReferenceDataServiceImpl implements CodicilReferenceDataService
{
    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<CodicilTemplateDto> getCodicilTemplates()
    {
        return this.codicilReferenceDataDao.getCodicilTemplates();
    }

    @Override
    public List<ReferenceDataDto> getCodicilCategories()
    {
        return this.codicilReferenceDataDao.getCodicilCategories();
    }

    @Override
    public List<ReferenceDataDto> getTemplateStatuses()
    {
        return this.codicilReferenceDataDao.getTemplateStatuses();
    }

    @Override
    public List<CodicilTypeDto> getCodicilTypes()
    {
        return this.codicilReferenceDataDao.getCodicilTypes();
    }

    @Override
    public CodicilTypeDto getCodicilType(final Long codicilTypeId) throws RecordNotFoundException
    {
        final CodicilTypeDto codicilType = this.codicilReferenceDataDao.getCodicilType(codicilTypeId);
        this.serviceHelper.verifyModel(codicilType, codicilTypeId);

        return codicilType;
    }

    @Override
    public List<CodicilStatusDto> getCodicilStatuses()
    {
        return this.codicilReferenceDataDao.getCodicilStatuses();
    }

    @Override
    public ReferenceDataDto getCodicilStatus(final Long codicilStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto codicilStatus = this.codicilReferenceDataDao.getCodicilStatus(codicilStatusId);
        this.serviceHelper.verifyModel(codicilStatus, codicilStatusId);

        return codicilStatus;
    }
}
