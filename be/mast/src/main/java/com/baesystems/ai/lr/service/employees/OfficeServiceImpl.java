package com.baesystems.ai.lr.service.employees;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.employees.OfficeDao;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "OfficeService")
@Loggable(Loggable.DEBUG)
public class OfficeServiceImpl implements OfficeService
{
    @Autowired
    private OfficeDao officeDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final List<OfficeDto> getOffices()
    {
        return this.officeDao.findAll();
    }

    @Override
    public final OfficeDto getOffice(final Long officeId) throws RecordNotFoundException
    {
        final OfficeDto office = this.officeDao.getOffice(officeId);
        this.serviceHelper.verifyModel(office, officeId);

        return office;
    }

    @Override
    public OfficeDto getOfficeByCode(final String code) throws RecordNotFoundException
    {
        final OfficeDto office = this.officeDao.getOfficeByCode(code);
        this.serviceHelper.verifyModel(office, null);
        return office;
    }

    @Override
    public String getOfficeName(final Long officeId)
    {
        return this.officeDao.getOfficeName(officeId);
    }
}
