package com.baesystems.ai.lr.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.exception.MastSystemException;

@Service(value = "ReportContentProcessor")
public class ReportContentProcessor implements Processor
{
    @Autowired
    @Qualifier("reportContentJackson")
    private JacksonDataFormat dataFormatter;

    @Override
    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public void process(final Exchange exchange) throws IOException
    {
        final ReportDto reportBody = exchange.getIn().getBody(ReportDto.class);
        if (reportBody != null && reportBody.getContent() != null)
        {
            final byte[] reportContentBytes = reportBody.getContent().getBytes(Charset.forName("UTF-8"));

            try (final InputStream contentStream = new ByteArrayInputStream(reportContentBytes))
            {
                try
                {
                    final ReportContentDto reportContentDto = (ReportContentDto) dataFormatter.unmarshal(exchange, contentStream);

                    final Message outMessage = exchange.getOut();
                    // Proper content generated, set back on the OUT message
                    outMessage.setBody(reportContentDto, ReportContentDto.class);

                    // Copy headers and attachments from IN to OUT to propagate them
                    outMessage.setHeaders(exchange.getIn().getHeaders());
                    outMessage.setAttachments(exchange.getIn().getAttachments());
                }
                catch (final Exception exception)
                {
                    throw new MastSystemException("Error unmarshalling report content", exception);
                }
            }
        }
    }
}
