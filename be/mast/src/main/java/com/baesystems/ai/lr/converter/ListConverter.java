package com.baesystems.ai.lr.converter;

import java.util.Arrays;
import java.util.List;

import org.apache.camel.Converter;
import org.apache.camel.TypeConverters;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public final class ListConverter implements TypeConverters
{
    private ListConverter()
    {
    }

    @Converter
    public static List<Long> toList(final String value) throws BadRequestException
    {
        final String[] vals = value.split(",");
        final Long[] longVals = new Long[vals.length];

        for (int i = 0; i < vals.length; i++)
        {
            try
            {
                longVals[i] = Long.parseLong(vals[i]);
            }
            catch (final NumberFormatException exception)
            {
                final String message = String.format(ExceptionMessagesUtils.DETAILED_BINDING_ERROR_MSG, value, "number");
                throw new BadRequestException(message, exception);
            }
        }

        return Arrays.asList(longVals);
    }
}
