package com.baesystems.ai.lr.service.codicils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.BaseBatchActionDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.BatchActionActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.BatchActionAssetNoteDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.jcabi.aspects.Loggable;

@Service(value = "BatchActionService")
@Loggable(Loggable.DEBUG)
public class BatchActionServiceImpl implements BatchActionService
{

    @Autowired
    private AssetNoteService assetNoteService;

    @Autowired
    private ActionableItemService actionableItemService;

    @Autowired
    private ItemDao itemDao;

    /**
     * Saves an AssetNote against a list of Assets or Asset with a specified ItemType. A single AssetNoteDto is
     * specified within BatchActionAssetNoteDto together with a List of Assets IDs and optionally a single ItemType ID.
     * <p>
     * If the Item type ID is not specified i.e. is null, then a unique AssetNote is saved against each Asset ID and
     * added to the BatchActionAssetNoteDto.savedAssetNoteDtos Map.
     * <p>
     * If the ItemTypeId is NOT null then for each AssetID, all items of type ItemTypeId are obtained. If a particular
     * Asset does not contain Items of specified ItemType then the asset ID is added to
     * BatchActionAssetNoteDto.rejectedAssetIds List. If the asset DOES have matching items then an AssetNote is saved
     * with the Asset and Item fields populated, and added to the BatchActionAssetNoteDto.savedAssetNoteDtos Map.
     * <p>
     * The List of AssetIds is assumed to be 'dirty' so is first cleaned to remove null or duplicate entries. This
     * method also assumes the required validation had been carried out on the BatchActionAssetNoteDto.
     * <p>
     * If one AssetNote save fails then the whole batch will fail.
     *
     * @param batchActionAssetNoteDto containing the List of AssetIds, AssetNoteDto and optional ItemTypeId.
     * @return the BatchActionAssetNoteDto populated with savedAssetNoteDtos and (possibly) rejectedAssetIds. None of
     *         the 'input' fields (those populated by the FE) are modified.
     * @throws RecordNotFoundException if cannot save any single AssetNote.
     */
    @Override
    public BatchActionAssetNoteDto saveBatchActionAssetNote(final BatchActionAssetNoteDto batchActionAssetNoteDto)
            throws RecordNotFoundException
    {
        final AssetNoteDto assetNoteDto = batchActionAssetNoteDto.getAssetNoteDto();
        final List<Long> cleanedAssetIds = cleanIds(batchActionAssetNoteDto);

        batchActionAssetNoteDto.setSavedAssetNoteDtos(new HashMap<>());
        assetNoteDto.setAsset(new LinkResource());
        if (batchActionAssetNoteDto.getItemTypeId() == null)
        {
            // Batch-Action against Asset only
            for (final Long assetId : cleanedAssetIds)
            {
                assetNoteDto.getAsset().setId(assetId);
                final AssetNoteDto savedAssetNote = this.assetNoteService.saveAssetNote(assetNoteDto);
                batchActionAssetNoteDto.getSavedAssetNoteDtos().put(assetId, Arrays.asList(savedAssetNote));
            }
        }
        else
        {
            // Batch Action against Item Type within the specified List of Assets
            final List<Long> itemTypeId = Arrays.asList(batchActionAssetNoteDto.getItemTypeId());
            batchActionAssetNoteDto.setRejectedAssetIds(new ArrayList<>());
            batchActionAssetNoteDto.getAssetNoteDto().setAssetItem(new ItemLightDto());
            for (final Long assetId : cleanedAssetIds)
            {
                final List<Long> itemIds = this.itemDao.getItemIdsFromAssetAndType(assetId, itemTypeId);
                if (itemIds == null || itemIds.isEmpty())
                {
                    batchActionAssetNoteDto.getRejectedAssetIds().add(assetId);
                }
                else
                {
                    assetNoteDto.getAsset().setId(assetId);
                    final List<AssetNoteDto> savedAssetNoteDtos = new ArrayList<>();
                    for (final Long itemId : itemIds)
                    {
                        assetNoteDto.getAssetItem().setId(itemId);
                        final AssetNoteDto savedAssetNote = this.assetNoteService.saveAssetNote(assetNoteDto);

                        savedAssetNoteDtos.add(savedAssetNote);
                    }
                    batchActionAssetNoteDto.getSavedAssetNoteDtos().put(assetId, savedAssetNoteDtos);
                }
            }
        }

        return batchActionAssetNoteDto;
    }

    /**
     * This method functions in the same manner as saveBatchActionAssetNote()
     *
     * @param batchActionActionableItemDto
     * @return
     */
    @Override
    public BatchActionActionableItemDto saveBatchActionActionableItem(final BatchActionActionableItemDto batchActionActionableItemDto)
    {
        final ActionableItemDto actionableItemDto = batchActionActionableItemDto.getActionableItemDto();
        final List<Long> cleanedAssetIds = cleanIds(batchActionActionableItemDto);

        batchActionActionableItemDto.setSavedActionableItemDtos(new HashMap<>());
        actionableItemDto.setAsset(new LinkResource());
        if (batchActionActionableItemDto.getItemTypeId() == null)
        {
            // Batch-Action against Asset only
            for (final Long assetId : cleanedAssetIds)
            {
                actionableItemDto.getAsset().setId(assetId);
                final ActionableItemDto savedActionableItem = this.actionableItemService.createActionableItem(assetId, actionableItemDto);
                batchActionActionableItemDto.getSavedActionableItemDtos().put(assetId, Arrays.asList(savedActionableItem));
            }
        }
        else
        {
            // Batch Action against Item Type within the specified List of Assets
            final List<Long> itemTypeId = Arrays.asList(batchActionActionableItemDto.getItemTypeId());
            batchActionActionableItemDto.setRejectedAssetIds(new ArrayList<>());
            batchActionActionableItemDto.getActionableItemDto().setAssetItem(new ItemLightDto());
            for (final Long assetId : cleanedAssetIds)
            {
                final List<Long> itemIds = itemDao.getItemIdsFromAssetAndType(assetId, itemTypeId);
                if (itemIds == null || itemIds.isEmpty())
                {
                    batchActionActionableItemDto.getRejectedAssetIds().add(assetId);
                }
                else
                {
                    actionableItemDto.getAsset().setId(assetId);
                    final List<ActionableItemDto> savedActionableItemDtos = new ArrayList<>();
                    for (final Long itemId : itemIds)
                    {
                        actionableItemDto.getAssetItem().setId(itemId);
                        savedActionableItemDtos.add(this.actionableItemService.createActionableItem(assetId, actionableItemDto));
                    }
                    batchActionActionableItemDto.getSavedActionableItemDtos().put(assetId, savedActionableItemDtos);
                }
            }
        }

        return batchActionActionableItemDto;
    }

    /**
     * Removes null and duplicate values from the AssetId List in a BaseBatchActionDto.
     *
     * @param baseBatchActionDto
     * @return
     */
    private List<Long> cleanIds(final BaseBatchActionDto baseBatchActionDto)
    {
        return baseBatchActionDto.getAssetIds().stream()
                .filter(id -> id != null)
                .distinct()
                .collect(Collectors.toList());
    }
}
