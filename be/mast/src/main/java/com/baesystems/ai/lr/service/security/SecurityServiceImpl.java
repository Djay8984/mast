package com.baesystems.ai.lr.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.employees.LrEmployeeDao;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.security.UserDto;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "SecurityService")
@Loggable(Loggable.DEBUG)
public class SecurityServiceImpl implements SecurityService
{
    @Autowired
    private ServiceUtils serviceUtils;

    @Autowired
    private LrEmployeeDao lrEmployeeDao;

    /**
     * Gets information about the authenticated user including a list of the groups assigned.
     *
     * @return {@link UserDto} user information
     */
    @Override
    public UserDto getUserInfo()
    {
        final UserDto userDto = new UserDto();
        userDto.setUserName(this.serviceUtils.getSecurityContextPrincipal());
        userDto.setGroupList(this.serviceUtils.getUserGroups());

        // Find matching internal employee
        final LrEmployeeDto employee = this.lrEmployeeDao.getLrEmployee(userDto.getUserName());

        if (employee != null)
        {
            userDto.setId(employee.getId());
            userDto.setEmailAddress(employee.getEmailAddress());
            userDto.setDeleted(employee.getDeleted());
        }
        return userDto;
    }
}
