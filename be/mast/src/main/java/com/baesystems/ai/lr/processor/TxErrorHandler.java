package com.baesystems.ai.lr.processor;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.processor.RedeliveryPolicy;
import org.apache.camel.processor.exceptionpolicy.ExceptionPolicyStrategy;
import org.apache.camel.spring.spi.TransactionErrorHandler;
import org.apache.camel.util.CamelLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

@SuppressWarnings("PMD")
public class TxErrorHandler extends TransactionErrorHandler
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TxErrorHandler.class);
    private static final String COUNTER_REF = "TxErrorHandlerBuilder-COUNTER";

    private List<Processor> processors;

    public TxErrorHandler(final CamelContext camelContext, final Processor output, final CamelLogger logger, final Processor redeliveryProcessor,
            final RedeliveryPolicy redeliveryPolicy, final ExceptionPolicyStrategy exceptionPolicyStrategy,
            final TransactionTemplate transactionTemplate,
            final Predicate retryWhile, final ScheduledExecutorService executorService, final LoggingLevel rollbackLoggingLevel)
    {
        super(camelContext, output, logger, redeliveryProcessor, redeliveryPolicy, exceptionPolicyStrategy, transactionTemplate, retryWhile,
                executorService,
                rollbackLoggingLevel);
    }

    @Override
    public boolean isDeadLetterChannel()
    {
        return true;
    }

    public void setProcessors(final List<Processor> processors)
    {
        this.processors = processors;
    }

    @Override
    protected void prepareExchangeAfterFailure(final Exchange exchange, final RedeliveryData data, final boolean isDeadLetterChannel,
            final boolean shouldHandle,
            final boolean shouldContinue)
    {
        super.prepareExchangeAfterFailure(exchange, data, isDeadLetterChannel, shouldHandle, shouldContinue);

        if (exchange.getProperty(Exchange.ERRORHANDLER_HANDLED, Boolean.class))
        {
            exchange.setProperty(Exchange.ERRORHANDLER_HANDLED, Boolean.FALSE);
            try
            {
                processDeadLetter(exchange);
            }
            catch (final Exception exception)
            {
                LOGGER.error("prepareExchangeAfterFailure", exception);
            }
        }
    }

    @Override
    protected void processInTransaction(final Exchange exchange) throws Exception
    {
        AtomicInteger counter = (AtomicInteger) exchange.getProperty(COUNTER_REF);
        if (counter == null)
        {
            counter = new AtomicInteger();
            exchange.setProperty(COUNTER_REF, counter);
        }

        counter.incrementAndGet();
        try
        {
            super.processInTransaction(exchange);
        }

        finally
        {
            if (counter.decrementAndGet() == 0 && exchange.isFailed())
            {
                try
                {
                    counter.incrementAndGet();

                    processDeadLetter(exchange);
                }
                finally
                {
                    counter.decrementAndGet();
                }
            }
        }
    }

    private void processDeadLetter(final Exchange exchange) throws Exception
    {
        if (processors != null)
        {
            for (final Processor processor : processors)
            {
                processor.process(exchange);
            }
        }
    }
}
