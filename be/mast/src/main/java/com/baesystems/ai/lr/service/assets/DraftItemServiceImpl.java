package com.baesystems.ai.lr.service.assets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.DraftItemPersistDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.utils.VersioningHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "DraftItemService")
@SuppressWarnings("PMD.TooManyMethods")
@Loggable(Loggable.DEBUG)
public class DraftItemServiceImpl implements DraftItemService
{
    @Autowired
    private ItemDao itemDao;

    @Autowired
    private DraftItemPersistDao draftItemPersistDao;

    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    //@Autowired
    //private ServiceHelper serviceHelper;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private VersioningHelper versioningHelper;

    @Autowired
    private AssetVersionUpdateService assetVersionUpdateService;

    /**
     * This is the main point of entry for the adding of new items to a draft item hierarchy. The items can be added
     * through three distinct origins processes. The specific process required can be inferred through the validated
     * request structure
     * <p>
     * If itemTypeId is supplied on the itemDto, this implies that the item is to be created from reference data. The
     * DTO is validated such that itemTypeId and fromId is exclusive.
     * <p>
     * If fromId is instead populated this implies that the we wish to copy the item model from another asset to the
     * target draft item model. In this case, if there is no itemRelationshipDto supplied this means we do a full
     * recursive copy down the tree. If itemRelationshipDto is supplied this implies an explicit copy of a subset of
     * items from a another asset model. The items subset of items and their structure is contained in the
     * itemRelationshipDto
     *
     * @param assetId          the assetId of the draft item model
     * @param draftItemListDto the request payload
     * @return the parent draft item id that is being appended to
     */
    @Override
    public Long appendDraftItems(final Long assetId, final DraftItemListDto draftItemListDto) throws MastBusinessException, BadRequestException
    {
        if (draftItemListDto.getDraftItemDtoList() == null || draftItemListDto.getDraftItemDtoList().isEmpty())
        {
            throw new BadRequestException("Draft Item List is null or empty");
        }

        final DraftItemDto draftItemDto = draftItemListDto.getDraftItemDtoList().get(0);
        final Integer nextDisplayOrder = this.itemDao.getNextChildDisplayOrder(draftItemDto.getToParentId());

        if (draftItemDto.getItemTypeId() != null) // From reference Data
        {
            appendDraftItemFromReferenceData(assetId, draftItemListDto, nextDisplayOrder);
        }
        else if (draftItemDto.getItemRelationshipDto() != null) // Explicit, non-recursive copy
        {
            appendSpecificCopiedItems(assetId, draftItemListDto, nextDisplayOrder);
        }
        else
        // Full recursive copy
        {
            appendCopiedAssetModel(assetId, draftItemListDto, nextDisplayOrder);
        }

        return draftItemDto.getToParentId();
    }

    private void appendCopiedAssetModel(final Long assetId, final DraftItemListDto draftItemListDto, final Integer nextDisplayOrder)
            throws MastBusinessException
    {
        int nextDisplayOrderInt = nextDisplayOrder.intValue();
        for (final DraftItemDto draftItem : draftItemListDto.getDraftItemDtoList())
        {
            this.draftItemPersistDao.appendCopiedAssetModel(assetId, draftItem, draftItemListDto.getCopyAttributes(), nextDisplayOrderInt);
            nextDisplayOrderInt++;
        }
    }

    private void appendSpecificCopiedItems(final Long assetId, final DraftItemListDto draftItemListDto, final Integer nextDisplayOrder)
            throws MastBusinessException
    {
        int nextDisplayOrderInt = nextDisplayOrder.intValue();
        for (final DraftItemDto draftItem : draftItemListDto.getDraftItemDtoList())
        {
            if (draftItem.getItemRelationshipDto() != null)
            {
                this.draftItemPersistDao.selectiveAppendCopiedAssetModel(assetId, draftItem, draftItemListDto.getCopyAttributes(),
                        nextDisplayOrderInt);
                nextDisplayOrderInt++;
            }
        }
    }

    /**
     * Method to add a list of draft items from reference data. Any that do not have type ids (those that are copied
     * from existing items) are ignored.
     */
    //NOPMD
    private void appendDraftItemFromReferenceData(final Long assetId, final DraftItemListDto draftItemListDto, final Integer nextDisplayOrder) //NOPMD
    {
        int nextDisplayOrderInt = nextDisplayOrder;  //NOPMD

        for (final DraftItemDto draftItem : draftItemListDto.getDraftItemDtoList())
        {
            draftItem.setId(draftItem.getId()); // Pointless statement to suppress PMD
            if (draftItem.getItemTypeId() != null)
            {
                final ItemLightDto newItem = this.draftItemPersistDao.appendDraftItemFromReferenceData(assetId, draftItem,
                        this.itemTypeDao.getItemTypeName(draftItem.getItemTypeId()), nextDisplayOrderInt);
                this.createDefaultAndMandatoryAttributes(newItem);
                nextDisplayOrderInt++;
            }
        }
    } //NOPMD

    private void createDefaultAndMandatoryAttributes(final ItemLightDto item)
    {
        final List<AttributeTypeDto> attributeTypes = this.assetReferenceDataDao
                .getDefaultAndMandatoryAttributeTypesForItemType(item.getItemType().getId());

        for (final AttributeTypeDto attributeType : attributeTypes)
        {
            this.draftItemPersistDao.createDraftAttribute(attributeType.getId(), item.getId(), item.getAssetId(), item.getAssetVersionId());

            for (int added = 1; added < attributeType.getMinOccurs(); ++added)
            {
                this.draftItemPersistDao.createDraftAttribute(attributeType.getId(), item.getId(), item.getAssetId(), item.getAssetVersionId());
            }
        }
    }

    @Override
    public void deleteDraftItem(final Long draftItemId)
    {
        this.draftItemPersistDao.deleteDraftItem(draftItemId);
    }

    @Override
    public void updateDisplayOrders(final DraftItemDto newItem) throws MastBusinessException
    {
        //Kind of feels ugly using the DraftItemDto in this regard, in this context it is being used as a DTO with ID = the item ID
        //It's properties have different meanings when used in the "draft item creation" processes
        final Long assetId = newItem.getFromAssetId();
        final Long itemId = newItem.getId();
        final VersionedItemPK itemPK = this.versioningHelper.getVersionedItemPK(itemId, assetId, null, true);

        if (!this.itemDao.itemExists(itemPK)) // If not draft
        {
            final Long draftVersion = this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
            itemPK.setAssetVersionId(draftVersion);
        }
        this.draftItemPersistDao.updateDisplayOrders(itemPK, newItem.getDisplayOrder());

    }

}
