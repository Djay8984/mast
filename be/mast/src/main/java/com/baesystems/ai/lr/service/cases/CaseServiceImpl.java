package com.baesystems.ai.lr.service.cases;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.dto.cases.CaseValidationDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.CaseQueryDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.enums.MilestoneStatusType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.CaseReferenceDataService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CaseService")
@Loggable(Loggable.DEBUG)
public class CaseServiceImpl implements CaseService
{
    private static final Long ZERO = 0L;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private CaseDao caseDao;

    @Autowired
    private CaseMilestoneDao caseMilestoneDao;

    @Autowired
    private ValidationDataService validationDataService;

    @Autowired
    private CaseReferenceDataDao caseReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private CaseReferenceDataService caseReferenceDataService;

    @Autowired
    private CaseMilestoneService caseMilestoneService;

    @Override
    public final PageResource<CaseDto> getCases(final Integer page, final Integer size, final String sort, final String order,
            final CaseQueryDto query) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CaseQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(query, CaseQueryDto.class);
        final PageResource<CaseDto> cases = this.caseDao.getCases(pageSpecification, cleanDto);

        return cases;
    }

    @Override
    public final PageResource<CaseDto> getCasesAbstract(final Integer page, final Integer size, final String sort, final String order,
            final AbstractQueryDto query) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final PageResource<CaseDto> cases = this.caseDao.getCases(pageSpecification, query);

        return cases;
    }

    @Override
    public final CaseWithAssetDetailsDto getCase(final Long caseId) throws RecordNotFoundException
    {
        final CaseWithAssetDetailsDto aCase = this.caseDao.getCase(caseId);
        this.serviceHelper.verifyModel(aCase, caseId);

        if (this.caseReferenceDataService.getCaseStatus(aCase.getCaseStatus().getId()).getSynchroniseAsset())
        {
            mapAssetToCase(aCase);
        }

        return aCase;
    }

    @Override
    @Transactional
    public final CaseWithAssetDetailsDto saveCase(final CaseWithAssetDetailsDto caseDto) throws MastBusinessException, RecordNotFoundException
    {
        CaseWithAssetDetailsDto aCase = this.caseDao.createCase(caseDto);
        if (aCase != null)
        {
            milestoneCreation(aCase);
            aCase = this.caseDao.updateCase(aCase);
        }
        processAssetUpdate(aCase);

        return aCase;
    }

    @Override
    @Transactional
    public final CaseWithAssetDetailsDto updateCase(final Long caseId, final CaseWithAssetDetailsDto caseDto)
            throws MastBusinessException, RecordNotFoundException
    {
        // Overwrite any milestones entered with the existing milestones, so that they cannot be changed accidently and
        // corrupt the data.
        final CaseMilestoneListDto originalMilestones = this.caseMilestoneDao.getMilestonesForCase(caseDto.getId());
        caseDto.setMilestones(originalMilestones == null ? new ArrayList<CaseMilestoneDto>() : originalMilestones.getCaseMilestoneList());

        final CaseWithAssetDetailsDto existingCase = this.caseDao.findOneWithLock(caseId, CaseWithAssetDetailsDto.class, StaleCheckType.CASE);

        StaleCheckUtils.checkNotStale(existingCase, caseDto, StaleCheckType.CASE);

        CaseWithAssetDetailsDto aCase = this.caseDao.updateCase(caseDto);

        if (aCase != null && aCase.getMilestones().isEmpty())
        {
            milestoneCreation(aCase);
            aCase = this.caseDao.updateCase(aCase);
        }
        processAssetUpdate(aCase);

        return aCase;
    }

    /**
     * @param caseStatusId The status id from case.caseStatus.id
     * @return true if the case is can be deleted false if not.
     */
    @Override
    public final Boolean getCanDeleteCase(final Long caseStatusId)
    {
        return this.caseDao.getCanDeleteCase(caseStatusId);
    }

    /**
     * @param caseId The id of the case to delete
     */
    @Override
    public void deleteCase(final Long caseId)
    {
        this.caseDao.deleteCase(caseId);
    }

    @Override
    public final CaseValidationDto verifyCase(final Long imoNumber, final String builder, final String yardNumber, final Long businessProcess)
    {
        return this.validationDataService.verifyCase(imoNumber, builder, yardNumber, businessProcess);
    }

    private void createMilestonesForCase(final CaseWithAssetDetailsDto caseDto) throws MastBusinessException
    {
        if (caseDto.getCaseType() != null)
        {
            final List<MilestoneDto> milestones = this.caseReferenceDataDao.getMilestonesForCaseType(caseDto.getCaseType().getId());
            final List<CaseMilestoneDto> caseMilestones = new ArrayList<CaseMilestoneDto>();
            final LinkResource aCase = new LinkResource(caseDto.getId());
            final LinkResource defaultMilestoneStatus = new LinkResource(MilestoneStatusType.OPEN.value());

            for (final MilestoneDto milestone : milestones)
            {
                final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
                final LinkResource milestoneLink = new LinkResource(milestone.getId());
                caseMilestone.setaCase(aCase);
                caseMilestone.setMilestone(milestoneLink);

                caseMilestone.setMilestoneStatus(defaultMilestoneStatus);
                caseMilestone.setInScope(true);
                caseMilestones.add(caseMilestone);
            }

            this.caseMilestoneService.updateMilestoneDates(caseDto.getId(), caseMilestones, caseDto.getCaseAcceptanceDate(),
                    caseDto.getEstimatedBuildDate());

            caseDto.setMilestones(caseMilestones);
        }
    }

    private void milestoneCreation(final CaseWithAssetDetailsDto aCase) throws MastBusinessException
    {
        final CaseStatusDto caseStatus = this.caseReferenceDataDao.getCaseStatus(aCase.getCaseStatus().getId());
        if (caseStatus != null && CaseStatusType.getCaseStatusForOpen().contains(caseStatus.getId())
                && (aCase.getMilestones() == null || aCase.getMilestones().isEmpty()))
        {
            createMilestonesForCase(aCase);
        }
    }

    /**
     * Test if an asset has any other liked cases. If not it can be deleted with the current case.
     *
     * @param assetId The id of the asset in question, from case.asset.id.
     */
    @Override
    public final Boolean isAssetDeletable(final Long assetId)
    {
        final Long linkedCases = this.caseDao.countCasesForAsset(assetId);

        Boolean deletable = false;
        if (ZERO.equals(linkedCases))
        {
            deletable = true;
        }

        return deletable;
    }

    /**
     * Get all of the Cases for the given Asset where the Case Status is 'Open'.
     *
     * @param assetId
     * @return List of {@link CaseDto}
     */
    @Override
    public List<CaseDto> getCasesForAsset(final Long assetId)
    {
        return this.caseDao.getCasesForAsset(assetId, CaseStatusType.getCaseStatusForOpen());
    }

    private void mapAssetToCase(final CaseWithAssetDetailsDto caseDto)
    {
        if (caseDto.getAsset() != null)
        {
            final Long publishedVersion = this.assetDao.getPublishedVersion(caseDto.getAsset().getId());
            final AssetLightDto asset = this.assetDao.getAsset(caseDto.getAsset().getId(), publishedVersion, AssetLightDto.class);

            caseDto.setAssetName(asset.getName());
            caseDto.setImoNumber(asset.getIhsAsset() == null ? null : asset.getIhsAsset().getId());
            caseDto.setGrossTonnage(asset.getGrossTonnage());
            caseDto.setAssetLifecycleStatus(asset.getAssetLifecycleStatus());
            caseDto.setAssetType(asset.getAssetType());
            caseDto.setClassStatus(asset.getClassStatus());
            caseDto.setFlagState(asset.getFlagState());
            caseDto.setRegisteredPort(asset.getRegisteredPort());
            caseDto.setRuleSet(asset.getRuleSet());
            caseDto.setClassMaintenanceStatus(asset.getClassMaintenanceStatus());
            caseDto.setClassNotation(asset.getClassNotation());
            caseDto.setCoClassificationSociety(asset.getCoClassificationSociety());
            caseDto.setBuildDate(asset.getBuildDate());
            caseDto.setEstimatedBuildDate(asset.getEstimatedBuildDate());
            caseDto.setKeelLayingDate(asset.getKeelLayingDate());
            caseDto.setDateOfRegistry(asset.getDateOfRegistry());
        }
    }

    private void processAssetUpdate(final CaseWithAssetDetailsDto caseDto) throws RecordNotFoundException
    {
        if (caseDto != null)
        {
            final Boolean syncAsset = this.caseReferenceDataService.getCaseStatus(caseDto.getCaseStatus().getId()).getSynchroniseAsset();
            if (syncAsset == null ? false : syncAsset)
            {
                final AssetLightDto asset = mapCaseToAsset(caseDto);
                this.assetDao.updateAsset(asset);
            }
        }
    }

    private AssetLightDto mapCaseToAsset(final CaseWithAssetDetailsDto caseDto)
    {
        AssetLightDto asset = null;

        if (caseDto.getAsset() != null)
        {
            final Long publishedVersion = this.assetDao.getPublishedVersion(caseDto.getAsset().getId());
            asset = this.assetDao.getAsset(caseDto.getAsset().getId(), publishedVersion, AssetLightDto.class);

            if (asset.getIhsAsset() != null)
            {
                asset.getIhsAsset().setId(caseDto.getImoNumber());
            }

            asset.setName(caseDto.getAssetName());
            asset.setGrossTonnage(caseDto.getGrossTonnage());
            asset.setAssetLifecycleStatus(caseDto.getAssetLifecycleStatus());
            asset.setAssetType(caseDto.getAssetType());
            asset.setClassStatus(caseDto.getClassStatus());
            asset.setFlagState(caseDto.getFlagState());
            asset.setRegisteredPort(caseDto.getRegisteredPort());
            asset.setRuleSet(caseDto.getRuleSet());
            asset.setClassMaintenanceStatus(caseDto.getClassMaintenanceStatus());
            asset.setClassNotation(caseDto.getClassNotation());
            asset.setCoClassificationSociety(caseDto.getCoClassificationSociety());
            asset.setBuildDate(caseDto.getBuildDate());
            asset.setEstimatedBuildDate(caseDto.getEstimatedBuildDate());
            asset.setKeelLayingDate(caseDto.getKeelLayingDate());
            asset.setDateOfRegistry(caseDto.getDateOfRegistry());
            asset.setHarmonisationDate(asset.getBuildDate());
        }

        return asset;
    }
}
