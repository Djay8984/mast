package com.baesystems.ai.lr.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.camel.Exchange;
import org.apache.camel.swagger.servlet.RestSwaggerServlet;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baesystems.ai.lr.dto.base.Dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import io.swagger.converter.ModelConverters;
import io.swagger.models.Model;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.SerializableParameter;
import io.swagger.parser.SwaggerParser;

public class MastSwaggerServlet extends RestSwaggerServlet
{
    private static final long serialVersionUID = -7056529634174293359L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MastSwaggerServlet.class);

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        try (ByteArrayOutputStream result = new ByteArrayOutputStream())
        {

            // copy request to dummy output stream
            super.doGet(request, new HttpServletResponseFilter(response, new ResponseServletOutputStream(result)));
            result.flush();

            // parse the model
            final byte[] results = result.toByteArray();
            final SwaggerParser parser = new SwaggerParser();
            final Swagger swagger = parser.parse(new String(results, "utf-8"));
            enumRemoval(swagger);

            // Merge all Dto's in to the Model
            final Set<Class<? extends Dto>> modules = getDtoClasses();
            for (final Class<?> dto : modules)
            {
                final Map<String, Model> newDefinitions = ModelConverters.getInstance().readAll(dto);
                swagger.getDefinitions().putAll(newDefinitions);
            }

            // Re-output the Swagger JSON Schema
            final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            final byte[] bytes = mapper.writeValueAsBytes(swagger);

            response.addHeader(Exchange.CONTENT_LENGTH, Integer.toString(bytes.length));
            response.getOutputStream().write(bytes);
        }
    }

    protected Set<Class<? extends Dto>> getDtoClasses()
    {

        final Reflections reflections = new Reflections(new ConfigurationBuilder()
                .addUrls(ClasspathHelper.forClass(Dto.class))
                .setScanners(new SubTypesScanner()));

        final Class<Dto> classType = Dto.class;
        return reflections.getSubTypesOf(classType);
    }

    public void enumRemoval(final Swagger swaggerActual)
    {
        for (final Map.Entry<String, Path> entry : swaggerActual.getPaths().entrySet())
        {
            final Path path = entry.getValue();

            for (final Operation operation : path.getOperations())
            {
                final List<Parameter> parameters = operation.getParameters();
                for (final Parameter param : parameters)
                {
                    if (param instanceof SerializableParameter)
                    {
                        final SerializableParameter queryParam = (SerializableParameter) param;
                        if (queryParam.getEnum() != null && queryParam.getEnum().isEmpty())
                        {
                            queryParam.setEnum(null);
                        }
                    }
                }
            }
        }
    }

    private static class ResponseServletOutputStream extends ServletOutputStream
    {
        private final ByteArrayOutputStream result;

        public ResponseServletOutputStream(final ByteArrayOutputStream result)
        {
            super();
            this.result = result;
        }

        @Override
        public void write(final int byteValue) throws IOException
        {
            result.write(byteValue);
        }

        @Override
        public void write(final byte[] bytes, final int off, final int len) throws IOException
        {
            result.write(bytes, off, len);
        }

        @Override
        public boolean isReady()
        {
            return true;
        }

        @Override
        public void setWriteListener(final WriteListener writeListener)
        {
            try
            {
                writeListener.onWritePossible();
            }
            catch (final IOException exception)
            {
                LOGGER.error("setWriteListener", exception);
            }
        }
    }

    public static class HttpServletResponseFilter extends HttpServletResponseWrapper
    {
        private final ServletOutputStream stream;

        public HttpServletResponseFilter(final HttpServletResponse response, final ServletOutputStream stream)
        {
            super(response);
            this.stream = stream;

        }

        /**
         * Return the underlying target stream (never {@code null}).
         */
        public final OutputStream getTargetStream()
        {
            return this.stream;
        }

        public void write(final int byteValue) throws IOException
        {
            this.stream.write(byteValue);
        }

        public void flush() throws IOException
        {
            this.stream.flush();
        }

        public void close() throws IOException
        {
            this.stream.close();
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException
        {
            return stream;
        }
    }

}
