package com.baesystems.ai.lr.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MastSystemException;

@Component
public class DateHelperImpl implements DateHelper
{
    /**
     * Change the given date by a number of days/months etc.
     *
     * @param startDate - Date to adjust.
     * @param increment - Number of increment type to adjust the date by. Can be negative.
     * @param incrementType - Calendar Field Value e.g. {@link java.util.Calendar.DAY_OF_MONTH}.
     * @return Date
     * @throws MastBusinessException
     */
    @Override
    public Date adjustDate(final Date startDate, final Integer increment, final int incrementType) throws MastBusinessException
    {
        if (startDate == null || increment == null)
        {
            throw new MastBusinessException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Initial date and increment"));
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(incrementType, increment);
        return calendar.getTime();
    }

    @Override
    public DateDto createDateDto(final Date date)
    {
        final DateDto dateDto = new DateDto();
        dateDto.setDate(date);
        return dateDto;
    }

    /**
     * Returns true if the given date is after the start of the current date minus one day (to allow for differences in
     * time zones).
     */
    @Override
    public Boolean isTodayOrAfter(final Date date)
    {
        Date today = null;

        try
        {
            today = this.adjustDate(new Date(), -1, Calendar.DAY_OF_MONTH);
        }
        catch (final MastBusinessException exception)
        {
            // This should never happen.
            throw new MastSystemException(exception);
        }

        return date != null && (DateUtils.isSameDay(date, today) || today.before(date));
    }

    /**
     * Returns true if the given date is before the end of the current date plus one day (to allow for differences in
     * time zones).
     */
    @Override
    public Boolean isTodayOrBefore(final Date date)
    {
        Date today = null;

        try
        {
            today = this.adjustDate(new Date(), 1, Calendar.DAY_OF_MONTH);
        }
        catch (final MastBusinessException exception)
        {
            // This should never happen.
            throw new MastSystemException(exception);
        }

        return date != null && (DateUtils.isSameDay(date, today) || today.after(date));
    }

    @Override
    public Boolean isTodayOrAfterStrict(final Date date)
    {
        final Date today = new Date();

        return date != null && (DateUtils.isSameDay(date, today) || today.before(date));
    }

    @Override
    public Boolean isTodayOrBeforeStrict(final Date date)
    {
        final Date today = new Date();

        return date != null && (DateUtils.isSameDay(date, today) || today.after(date));
    }

    @Override
    public Boolean isTodayOrAfterStrict(final Date date, final Date today)
    {
        return date != null && (DateUtils.isSameDay(date, today) || today.before(date));
    }

    @Override
    public Boolean isTodayOrBeforeStrict(final Date date, final Date today)
    {
        return date != null && (DateUtils.isSameDay(date, today) || today.after(date));
    }
}
