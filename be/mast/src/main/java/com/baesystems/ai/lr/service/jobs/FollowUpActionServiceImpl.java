package com.baesystems.ai.lr.service.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.FollowUpActionDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.references.jobs.FollowUpActionTemplateDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FollowUpActionTemplateDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.enums.FollowUpActionStatus;
import com.baesystems.ai.lr.enums.FollowUpActionTemplates;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;

@Service(value = "FollowUpActionService")
public class FollowUpActionServiceImpl implements FollowUpActionService
{
    @Autowired
    private FollowUpActionDao followUpActionDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private FollowUpActionTemplateDao followUpActionTemplateDao;

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private LrEmployeeRepository employeeRepository;

    @Autowired
    private ServiceUtils serviceUtils;

    @Override
    public PageResource<FollowUpActionDto> getFollowUpActionsForJob(final Integer page, final Integer size, final String sort, final String order,
            final Long jobId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.followUpActionDao.getFollowUpActionsForJob(pageSpecification, jobId);
    }

    @Override
    public void generateFollowUpActions(final ReportDto report)
    {
        if (ReportType.FSR.value().equals(report.getReportType().getId()))
        {
            final JobDto thisJob = this.jobDao.getJob(report.getJob().getId());
            if (thisJob.getaCase() != null)
            {
                final FollowUpActionDto newFUA = new FollowUpActionDto();
                final FollowUpActionTemplateDto eicTemplate = this.followUpActionTemplateDao.getTemplate(FollowUpActionTemplates.EIC.value());
                final ReportDto reportDto = this.reportDao.getReport(report.getId());
                final LrEmployeeDO raisedBy = this.employeeRepository.findByFullName(this.serviceUtils.getSecurityContextPrincipal());
                newFUA.setTitle(eicTemplate.getTitle());
                newFUA.setDescription(eicTemplate.getDescription());
                newFUA.setRaisedBy(new LinkResource());
                newFUA.getRaisedBy().setId(raisedBy.getId());
                newFUA.setAsset(new LinkResource(thisJob.getAsset().getId()));
                newFUA.setFollowUpActionStatus(new LinkResource());
                newFUA.getFollowUpActionStatus().setId(FollowUpActionStatus.INCOMPLETE.value());
                newFUA.setAddedManually(false);
                newFUA.setReport(reportDto);
                this.followUpActionDao.saveFollowUpAction(newFUA, report);
            }
        }
    }

    /**
     * For a FollowUpAction object the only field that can be modified is the status. This method looks like a standard
     * object update method but in fact only updates a single field. If using a merge then all the fields would have to
     * be validated in case they had changed.
     *
     * @param followUpActionDto the FollowUpActionDto to update (its status)
     * @return
     * @throws StaleObjectException
     */
    @Override
    public FollowUpActionDto updateFollowUpAction(final FollowUpActionDto followUpActionDto) throws StaleObjectException
    {
        // Note: BaseDto and the later cast are there because FindBugs goes loopy complaining about unchecked casts, and
        // I've no idea why.

        final BaseDto existingDto = this.followUpActionDao.findOneWithLock(followUpActionDto.getId(),
                BaseDto.class, StaleCheckType.FOLLOW_UP_ACTION);

        StaleCheckUtils.checkNotStale((StaleObject) existingDto, followUpActionDto, StaleCheckType.FOLLOW_UP_ACTION);

        return this.followUpActionDao.updateFollowUpActionStatus(followUpActionDto.getId(), followUpActionDto.getFollowUpActionStatus());
    }
}
