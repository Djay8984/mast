package com.baesystems.ai.lr.service.cases;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.enums.MilestoneDueDateReferenceType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CaseMilestoneService")
@Loggable(Loggable.DEBUG)
public class CaseMilestoneServiceImpl implements CaseMilestoneService
{
    @Autowired
    private CaseMilestoneDao caseMilestoneDao;

    @Autowired
    private CaseReferenceDataDao caseReferenceDataDao;

    @Autowired
    private CaseDao caseDao;

    @Autowired
    private DateHelper dateUtils;

    /**
     * Get all the milestones for a case with a given id
     */
    @Override
    public CaseMilestoneListDto getMilestonesForCase(final Long caseId) throws BadRequestException
    {
        final CaseMilestoneListDto result = this.caseMilestoneDao.getMilestonesForCase(caseId);

        return result;
    }

    /**
     * update the case milestones for the case with caseId to those in the milestones list
     */
    @Override
    public void updateMilestonesForCase(final Long caseId, final CaseMilestoneListDto milestones) throws MastBusinessException
    {
        final List<CaseMilestoneDto> milestoneListToUpdate = milestones == null ? new ArrayList<CaseMilestoneDto>()
                : milestones.getCaseMilestoneList(); // extract the list form the object
        final CaseWithAssetDetailsDto caseDto = this.caseDao.getCase(caseId);
        final CaseMilestoneListDto existingMilestoneList = this.caseMilestoneDao.getMilestonesForCase(caseId);

        // All milestones (even those not in the list may need updating) so any milestones that are in the database but
        // not in the list are copied into it so that the date updater can work on a single list.
        if (existingMilestoneList != null && existingMilestoneList.getCaseMilestoneList() != null)
        {
            existingMilestoneList.getCaseMilestoneList().stream()
                    .filter(caseMilestone -> !milestoneListToUpdate.contains(caseMilestone))
                    .forEach(caseMilestone -> milestoneListToUpdate.add(caseMilestone));
        }

        this.updateMilestoneDates(caseId, milestoneListToUpdate, caseDto.getCaseAcceptanceDate(), caseDto.getEstimatedBuildDate());

        for (final CaseMilestoneDto caseMilestone : milestoneListToUpdate)
        {
            final CaseMilestoneDto existingDto = this.caseMilestoneDao.findOneWithLock(caseMilestone.getId(), CaseMilestoneDto.class,
                    StaleCheckType.CASE_MILESTONE);

            StaleCheckUtils.checkNotStale(existingDto, caseMilestone, StaleCheckType.CASE_MILESTONE);

            // save the updates
            this.caseMilestoneDao.updateMilestone(caseId, caseMilestone);
        }
    }

    /**
     * Update the due dates on a list of milestones for a given case. Only milestones in the list will be updated, their
     * dependents will not be updated unless they are in the list. The dates used should come from the case table (not
     * the asset)
     */
    @Override
    public void updateMilestoneDates(final Long caseId, final List<CaseMilestoneDto> caseMilestones, final Date caseAcceptenceDate,
            final Date estimatedBuildDate) throws MastBusinessException
    {
        for (final CaseMilestoneDto caseMilestone : caseMilestones)
        {
            if (caseMilestone.getCompletionDate() != null)
            { // if the milestone is complete then do not update the date any more.
                continue;
            }

            updateMilestoneDueDate(caseMilestones, caseMilestone, caseAcceptenceDate, estimatedBuildDate);
        }
    }

    /**
     * Method to updata the due dates of milestones depending on the type of date dependency in the ref data.
     */
    private void updateMilestoneDueDate(final List<CaseMilestoneDto> caseMilestones, final CaseMilestoneDto caseMilestone,
            final Date caseAcceptenceDate, final Date estimatedBuildDate) throws MastBusinessException
    {
        final MilestoneDto milestone = this.caseReferenceDataDao.getMilestone(caseMilestone.getMilestone().getId());

        if (milestone == null)
        {
            throw new MastSystemException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "milestone"));
        }

        final Integer offsetDays = milestone.getWorkingDaysOffset();

        if (MilestoneDueDateReferenceType.CASE_ACCEPTANCE_DATE.getValue().equals(milestone.getDueDateReference().getId()))
        {
            if (caseAcceptenceDate != null)
            {
                caseMilestone.setDueDate(this.dateUtils.adjustDate(caseAcceptenceDate, offsetDays, Calendar.DAY_OF_MONTH));
            }
        }
        else if (MilestoneDueDateReferenceType.ESTIMATED_BUILD_DATE.getValue().equals(milestone.getDueDateReference().getId()))
        {
            if (estimatedBuildDate != null)
            {
                caseMilestone.setDueDate(this.dateUtils.adjustDate(estimatedBuildDate, offsetDays, Calendar.DAY_OF_MONTH));
            }
        }
        else if (MilestoneDueDateReferenceType.MILESTONE_COMPLETION.getValue().equals(milestone.getDueDateReference().getId()))
        {
            updatedateDependentMilestone(caseMilestones, caseMilestone, milestone, offsetDays);
        }
    }

    /**
     * Method for updating the due date of a milestone when it depends on the completion date of another.
     */
    private void updatedateDependentMilestone(final List<CaseMilestoneDto> caseMilestones, final CaseMilestoneDto caseMilestone,
            final MilestoneDto milestone, final Integer offsetDays) throws MastBusinessException
    {
        final Long dateDependentPredecessorId = this.caseReferenceDataDao.getDateDependentParentMilestoneId(milestone.getId());

        // find the predecessor milestone in the list. This method needs the one from the input list not the database in
        // case it has been changed and not saved yet. All milestones updated or not should be in this list
        final List<CaseMilestoneDto> dateDependentPredecessorList = caseMilestones.stream()
                .filter(caseMilestoneStream -> dateDependentPredecessorId.equals(caseMilestoneStream.getMilestone().getId()))
                .collect(Collectors.toList());

        if (!dateDependentPredecessorList.isEmpty())
        { // of the date dependent predecessor is not found then the calculation must be skipped.
            final CaseMilestoneDto dateDependentPredecessor = dateDependentPredecessorList.get(0);

            if (dateDependentPredecessor.getCompletionDate() != null)
            {
                caseMilestone
                        .setDueDate(this.dateUtils.adjustDate(dateDependentPredecessor.getCompletionDate(), offsetDays, Calendar.DAY_OF_MONTH));
            }
        }
    }
}
