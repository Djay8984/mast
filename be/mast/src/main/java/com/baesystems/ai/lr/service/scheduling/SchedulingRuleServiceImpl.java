package com.baesystems.ai.lr.service.scheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.ProductType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.jcabi.aspects.Loggable;

@Service(value = "SchedulingRuleService")
@Loggable(Loggable.DEBUG)
public class SchedulingRuleServiceImpl implements SchedulingRuleService
{
    @Autowired
    private SchedulingHelper schedulingHelper;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private AssetDao assetDao;

    /**
     * Determine a new service schedule for the given Asset, updating existing active services where necessary and
     * creating additional services. All services and schedule dates are determined by the Scheduling Rule Engine, using
     * the exposed nextServices service method.
     *
     * @param completedService
     * @param harmonisationDate
     * @param provisionalDates - This should be true if triggered from FAR, otherwise false
     * @throws RecordNotFoundException
     */
    @Override
    public void scheduleServicesFollowingServiceCompletion(final ScheduledServiceDto completedService, final Date harmonisationDate,
            final Boolean provisionalDates) throws RecordNotFoundException
    {
        final Long assetId = completedService.getAsset().getId();

        final List<ScheduledServiceDto> ruleGeneratedServices = this.schedulingHelper.getNextServicesFromCompletedService(assetId,
                completedService.getId(), harmonisationDate, completedService.getCompletionDate());

        this.scheduleServices(assetId, provisionalDates, ruleGeneratedServices.toArray(new ScheduledServiceDto[ruleGeneratedServices.size()]));
    }

    /**
     * Determine a new service schedule for the given Service, updating the existing service as necessary. All services
     * and schedule dates are determined by the Scheduling Rule Engine, using the exposed manualEntryService service
     * method.
     *
     * @param createdService
     * @param harmonisationDate
     * @param provisionalDates - This should be true if triggered from FAR, otherwise false
     * @throws RecordNotFoundException
     */
    @Override
    public void scheduleServicesFollowingServiceCreation(final ScheduledServiceDto createdService, final Date harmonisationDate,
            final Boolean provisionalDates) throws RecordNotFoundException
    {
        final Long assetId = createdService.getAsset().getId();

        final ScheduledServiceDto ruleGeneratedService = this.schedulingHelper.getServiceScheduleForSingleService(assetId, createdService.getId(),
                harmonisationDate);

        if (ruleGeneratedService != null)
        {
            this.scheduleServices(assetId, provisionalDates, ruleGeneratedService);
        }
    }

    /**
     * Determine a new service schedule for the given Asset, updating existing active services where necessary and
     * creating additional services. Primarily this should be used when updating the asset harmonisation date. All
     * services and schedule dates are determined by the Scheduling Rule Engine.
     * </p>
     *
     * This differs to the other scheduling methods in that every existing service will trigger a call to the rule
     * engine. We examine the Parent Service for each existing Scheduled Service for the asset.
     * </p>
     *
     * If a parent exists, re-run the 'nextServices' method on the rule engine for the parent Service, with the parent
     * completion date. This is to avoid creating unnecessary services or adjusting the schedule for next open services
     * incorrectly.
     * </p>
     *
     * If there is no parent, just run the 'manualEntryService' method on the rule engine for the existing service.
     * </p>
     *
     * @param assetId
     * @param provisionalDates - This should be true if triggered from FAR, otherwise false
     * @throws RecordNotFoundException
     */
    @Override
    public void scheduleServicesFollowingAssetUpdate(final Long assetId, final Boolean provisionalDates)
            throws RecordNotFoundException
    {
        final Date harmonisationDate = this.assetDao.getAssetHarmonisationDate(assetId);

        final List<ScheduledServiceDto> existingAssetServices = this.serviceDao.getServicesForAssetByStatus(assetId,
                ServiceCreditStatusType.getOpen());

        final List<ScheduledServiceDto> ruleGeneratedServices = new ArrayList<ScheduledServiceDto>();

        for (final ScheduledServiceDto existingService : existingAssetServices)
        {
            if (existingService.getParentService() != null)
            {
                final ScheduledServiceDto parentService = this.serviceDao.getService(existingService.getParentService().getId());

                final List<ScheduledServiceDto> services = this.schedulingHelper.getNextServicesFromCompletedService(assetId,
                        parentService.getId(), harmonisationDate, parentService.getCompletionDate());

                if (services != null)
                {
                    ruleGeneratedServices.addAll(services);
                }
            }
            else
            {
                final ScheduledServiceDto service = this.schedulingHelper.getServiceScheduleForSingleService(assetId,
                        existingService.getId(), harmonisationDate);

                if (service != null)
                {
                    ruleGeneratedServices.add(service);
                }
            }
        }

        this.scheduleServices(assetId, provisionalDates, ruleGeneratedServices.toArray(new ScheduledServiceDto[ruleGeneratedServices.size()]));
    }

    /**
     * Compares a list of services generated by the Scheduling Rule Engine with the services that already exist for the
     * given Asset. Existing active services have their schedule dates updated, and any suggested service codes which do
     * not yet exist against the Asset will be created. Once the services are added it checks that the asset has all
     * necessary products and adds any that are needed.
     *
     * @param assetId
     * @param provisionalDates - This should be true if triggered from FAR, otherwise false
     * @param ruleGeneratedServices
     */
    private void scheduleServices(final Long assetId, final Boolean provisionalDates, final ScheduledServiceDto... ruleGeneratedServices)
    {
        if (ruleGeneratedServices != null && ruleGeneratedServices.length > 0)
        {
            final List<ScheduledServiceDto> existingAssetServices = this.serviceDao.getServicesForAssetByStatus(assetId,
                    ServiceCreditStatusType.getOpen());

            final List<ScheduledServiceDto> servicesToSave = this.schedulingHelper.generateNewServiceSchedule(assetId, provisionalDates,
                    existingAssetServices, ruleGeneratedServices);

            this.serviceDao.saveOrUpdateServices(servicesToSave);

            // add any more products that might be needed for the newly added services
            if (!servicesToSave.isEmpty())
            {
                this.refreshProductsAfterScheduleGeneration(assetId, servicesToSave);
            }
        }
    }

    /**
     * Method to add required products for new services.
     * <p>
     * When the rules engine runs it may create new services. This method is to be run after the rules engine to ensure
     * that all of the necessary products are in place for the services created. If a service has been created that
     * requires a product catalogue that is not selected for the asset then this method will create one.
     * <p>
     * The method extracts a list of service catalogues from the scheduled services added and gets and all of the
     * existing products against the asset, and compares the lists looking to see which product catalogue and scheduling
     * regime combinations are missing and then adds them.
     */
    private void refreshProductsAfterScheduleGeneration(final Long assetId, final List<ScheduledServiceDto> newServices)
    {
        // get a distinct list of new service catalogue IDs
        final Set<Long> requiredServiceCatalogueIds = new HashSet<Long>();
        for (final ScheduledServiceDto service : newServices)
        {
            requiredServiceCatalogueIds.add(service.getServiceCatalogueId());
        }

        final List<ProductDto> activeList = this.productDao.getProducts(assetId);

        for (final Long serviceCatalogueId : requiredServiceCatalogueIds)
        {
            final ServiceCatalogueDto required = this.serviceDao.getServiceCatalogue(serviceCatalogueId);
            // go through all the required products by looking at the service catalogues
            final Long requiredProductCatalogueId = required.getProductCatalogue().getId();
            final LinkResource requiredSchedulingRegime = required.getSchedulingRegime();

            boolean create = true;

            for (final ProductDto active : activeList)
            { // go through the list of active products and see if any match
                if (required.getProductCatalogue().getId().equals(active.getProductCatalogueId())
                        && (Objects.equals(required.getSchedulingRegime(), active.getSchedulingRegime()) || this.productDao
                                .isProductCatalogueOfType(required.getProductCatalogue().getId(), ProductType.CLASSIFICATION.getValue())))
                { // if both the product catalogue and scheduling regime match then nothing needs to be created.
                  // Note that the scheduling regime can be null (for classification products) and null = null
                  // counts as a match.
                    create = false;
                    break; // once found skip the rest.
                }
            }

            if (create)
            { // if no match was found create a new product/
                this.productDao.createProduct(assetId, requiredProductCatalogueId,
                        requiredSchedulingRegime == null ? null : requiredSchedulingRegime.getId());

                // make a dummy product with the product catalogue id and scheduling regime of the new one and add
                // it to the active list to prevent duplication in case two new services require the same product
                final ProductDto newProduct = new ProductDto();
                newProduct.setProductCatalogueId(requiredProductCatalogueId);
                newProduct.setSchedulingRegime(requiredSchedulingRegime);

                activeList.add(newProduct);
            }
        }

    }
}
