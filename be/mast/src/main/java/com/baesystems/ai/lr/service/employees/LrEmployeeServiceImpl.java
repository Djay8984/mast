package com.baesystems.ai.lr.service.employees;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.employees.LrEmployeeDao;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "LrEmployeeService")
@Loggable(Loggable.DEBUG)
public class LrEmployeeServiceImpl implements LrEmployeeService
{
    @Autowired
    private LrEmployeeDao lrEmployeeDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final LrEmployeeDto getLrEmployee(final Long employeeId) throws RecordNotFoundException
    {
        final LrEmployeeDto employee = this.lrEmployeeDao.getLrEmployee(employeeId);
        this.serviceHelper.verifyModel(employee, employeeId);

        return employee;
    }

    @Override
    public final LrEmployeeDto getLrEmployeeByFullName(final String userFullName) throws RecordNotFoundException
    {
        final LrEmployeeDto employee = this.lrEmployeeDao.getLrEmployee(userFullName);

        if (null == employee)
        {
            throw new RecordNotFoundException("Employee with name [" + userFullName + "] not found.");
        }

        return employee;
    }

    @Override
    public PageResource<LrEmployeeDto> searchEmployees(final Integer page, final Integer size, final String sort, final String order,
            final String searchString, final Long officeId) throws BadPageRequestException
    {
        final Pageable pageSpecification = serviceHelper.createPageable(page, size, sort, order);

        final String search = searchString != null ? QueryDtoUtils.escapeSpecialCharsAndConvertWildcards(searchString) : null;

        return lrEmployeeDao.searchEmployees(pageSpecification, search, officeId);
    }

    @Override
    public String getEmployeeFullName(final Long employeeId)
    {
        return this.lrEmployeeDao.getEmployeeFullName(employeeId);
    }

    @Override
    public List<LrEmployeeDto> getEmployees()
    {
        return this.lrEmployeeDao.getEmployees();
    }
}
