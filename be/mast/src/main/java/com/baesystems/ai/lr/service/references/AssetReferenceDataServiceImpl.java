package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dto.references.AllowedAssetAttributeValueDto;
import com.baesystems.ai.lr.dto.references.AssetCategoryDto;
import com.baesystems.ai.lr.dto.references.AssetTypeDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ClassDepartmentDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SocietyRulesetDto;
import com.baesystems.ai.lr.dto.validation.AssetModelTemplateDto;
import com.baesystems.ai.lr.dto.validation.ItemRuleDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "AssetReferenceDataService")
@Loggable(Loggable.DEBUG)
public class AssetReferenceDataServiceImpl implements AssetReferenceDataService
{
    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private ValidationDataService validationDataService;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Override
    public List<AssetCategoryDto> getAssetCategories()
    {
        return this.assetReferenceDataDao.getAssetCategories();
    }

    @Override
    public AssetCategoryDto getAssetCategory(final Long assetCategoryId) throws RecordNotFoundException
    {
        final AssetCategoryDto assetCategory = this.assetReferenceDataDao.getAssetCategory(assetCategoryId);
        this.serviceHelper.verifyModel(assetCategory, assetCategoryId);

        return assetCategory;
    }

    @Override
    public List<AssetTypeDto> getAssetTypes()
    {
        return this.assetReferenceDataDao.getAssetTypes();
    }

    @Override
    public AssetTypeDto getAssetType(final Long assetTypeId) throws RecordNotFoundException
    {
        final AssetTypeDto assetType = this.assetReferenceDataDao.getAssetType(assetTypeId);
        this.serviceHelper.verifyModel(assetType, assetTypeId);

        return assetType;
    }

    @Override
    public List<ReferenceDataDto> getCustomerFunctions()
    {
        return this.assetReferenceDataDao.getCustomerFunctions();
    }

    @Override
    public ReferenceDataDto getCustomerFunction(final Long customerFunctionId) throws RecordNotFoundException
    {
        final ReferenceDataDto customerFunction = this.assetReferenceDataDao.getCustomerFunction(customerFunctionId);
        this.serviceHelper.verifyModel(customerFunction, customerFunctionId);

        return customerFunction;
    }

    @Override
    public List<ReferenceDataDto> getCustomerRelationships()
    {
        return this.assetReferenceDataDao.getCustomerRelationships();

    }

    @Override
    public ReferenceDataDto getCustomerRelationship(final Long customerRelationshipId) throws RecordNotFoundException
    {
        final ReferenceDataDto customerRelationship = this.assetReferenceDataDao.getCustomerRelationship(customerRelationshipId);
        this.serviceHelper.verifyModel(customerRelationship, customerRelationshipId);

        return customerRelationship;
    }

    @Override
    public List<AttributeTypeDto> getAttributeTypes()
    {
        return this.assetReferenceDataDao.getAttributeTypes();
    }

    @Override
    public AttributeTypeDto getAttributeType(final Long attributeTypeId) throws RecordNotFoundException
    {
        final AttributeTypeDto attributeType = this.assetReferenceDataDao.getAttributeType(attributeTypeId);
        this.serviceHelper.verifyModel(attributeType, attributeTypeId);

        return attributeType;
    }

    @Override
    public List<ReferenceDataDto> getAttributeTypeValues()
    {
        return this.assetReferenceDataDao.getAttributeTypeValues();
    }

    @Override
    public List<AllowedAssetAttributeValueDto> getAllowedAssetAttributeValues()
    {
        return this.assetReferenceDataDao.getAllowedAssetAttributeValues();
    }

    /**
     * Gets the a list of all item Types.
     *
     * @return {@link ItemTypeDto}, Item type.
     */
    @Override
    public final List<ItemTypeDto> getItemTypes()
    {
        return this.itemTypeDao.getItemTypes();
    }

    /**
     * Gets the item Type, for an item.
     *
     * @param itemTypeId, item identifier.
     * @return {@link ItemTypeDto}, Item type.
     * @throws RecordNotFoundException
     */
    @Override
    public final ItemTypeDto getItemType(final Long itemTypeId) throws RecordNotFoundException
    {
        final ItemTypeDto itemType = this.itemTypeDao.getItemType(itemTypeId);
        this.serviceHelper.verifyModel(itemType, itemTypeId);

        return itemType;
    }

    /**
     * Creates a model template AssetModelTemplateDto which includes the list of all item and attribute type
     * relationship rules for every existing item type.
     *
     * @return List of item (ItemRuleDto) and attribute (ttributeRuleDto) rules for each item type.
     */
    @Override
    public AssetModelTemplateDto getAssetModelTemplate()
    {
        return this.validationDataService.getAssetModelTemplate();
    }

    /**
     * Gets an IACS Society, {@link IacsSocietyDto}, by its identifier.
     *
     * @param IacsSocietyId, asset identifier.
     * @return An asset, {@link IacsSocietyDto}.
     * @throws RecordNotFoundException, if IACS Society not found.
     */
    @Override
    public final ReferenceDataDto getIacsSociety(final Long id) throws RecordNotFoundException
    {
        final ReferenceDataDto iacsSociety = this.assetReferenceDataDao.getIacsSociety(id);
        this.serviceHelper.verifyModel(iacsSociety, id);

        return iacsSociety;
    }

    /**
     * Lists all IACS Societies in the database.
     *
     * @return A list of all IACS Societies {@Link IacsSocietyDto}
     **/
    @Override
    public final List<ReferenceDataDto> getIacsSocieties()
    {
        return this.assetReferenceDataDao.getIacsSocieties();
    }

    /**
     * Gets a Rule Set, {@link SocietyRulesetDto}, by its identifier.
     *
     * @param Id, Rule Set identifier.
     * @return An asset, {@link SocietyRulesetDto}.
     * @throws RecordNotFoundException, if Rule Set not found.
     */
    @Override
    public final SocietyRulesetDto getRuleSet(final Long id) throws RecordNotFoundException
    {
        final SocietyRulesetDto ruleSet = this.assetReferenceDataDao.getRuleSet(id);
        this.serviceHelper.verifyModel(ruleSet, id);

        return ruleSet;
    }

    /**
     * Gets a Rule Set, {@link ReferenceDataDto}, by its identifier.
     *
     * @param Id, Rule Set identifier.
     * @return {@link ReferenceDataDto}.
     */
    @Override
    public final ReferenceDataDto getProductRuleSet(final Long id)
    {
        return this.assetReferenceDataDao.getProductRuleSet(id);
    }

    /**
     * Lists all Rule Sets in the database.
     *
     * @return A list of all Rule Sets {@Link SocietyRulesetDto}
     **/
    @Override
    public final List<SocietyRulesetDto> getRuleSets(final String category, final Boolean isLrRuleset)
    {
        return this.assetReferenceDataDao.getRuleSets(category, isLrRuleset);
    }

    @Override
    public List<ReferenceDataDto> getProductRuleSets()
    {
        return this.assetReferenceDataDao.getProductRuleSets();
    }

    @Override
    public final List<ReferenceDataDto> getAssetLifecycleStatuses()
    {
        return this.assetReferenceDataDao.getAssetLifecycleStatuses();
    }

    @Override
    public final ReferenceDataDto getAssetLifecycleStatus(final Long lifecycleStatusId)
    {
        return this.assetReferenceDataDao.getAssetLifecycleStatus(lifecycleStatusId);
    }

    @Override
    public final List<ReferenceDataDto> getClassStatuses()
    {
        return this.assetReferenceDataDao.getClassStatuses();
    }

    @Override
    public final ReferenceDataDto getClassStatus(final Long classStatusId)
    {
        return this.assetReferenceDataDao.getClassStatus(classStatusId);
    }

    @Override
    public final List<ItemRuleDto> getItemTypeRelationships()
    {
        return this.itemTypeDao.getItemTypeRelationships();
    }

    @Override
    public List<ReferenceDataDto> getItemRelationshipTypes()
    {
        return this.assetReferenceDataDao.getItemRelationshipTypes();
    }

    @Override
    public List<ReferenceDataDto> getCountries()
    {
        return this.assetReferenceDataDao.getCountries();
    }

    @Override
    public List<ReferenceDataDto> getClassMaintenanceStatuses()
    {
        return this.assetReferenceDataDao.getClassMaintenanceStatuses();
    }

    @Override
    public List<ClassDepartmentDto> getClassDepartments()
    {
        return this.assetReferenceDataDao.getClassDepartments();
    }

    @Override
    public final ClassDepartmentDto getClassDepartment(final Long classDepartmentId)
    {
        return this.assetReferenceDataDao.getClassDepartment(classDepartmentId);
    }

    @Override
    public List<ReferenceDataDto> getActionsTaken()
    {
        return this.assetReferenceDataDao.getActionsTaken();
    }

    @Override
    public List<ReferenceDataDto> getClassNotations()
    {
        return this.assetReferenceDataDao.getClassNotations();
    }

    @Override
    public List<ReferenceDataDto> getMigrationStatuses()
    {
        return this.assetReferenceDataDao.getMigrationStatuses();
    }
}
