package com.baesystems.ai.lr.processor;

import java.util.List;

import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.DefaultErrorHandlerBuilder;
import org.apache.camel.spi.RouteContext;
import org.apache.camel.spring.spi.TransactionErrorHandler;
import org.apache.camel.util.CamelLogger;
import org.apache.camel.util.ObjectHelper;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * A transactional error handler that supports leveraging Spring TransactionManager.
 *
 * @version
 */
public class TxErrorHandlerBuilder extends DefaultErrorHandlerBuilder
{
    private static final LoggingLevel ROLLBACK_LOG_LEVEL = LoggingLevel.WARN;

    private TransactionTemplate transactionTemplate;
    private List<Processor> processors;

    public void setProcessors(final List<Processor> processors)
    {
        this.processors = processors;
    }

    public void setTransactionTemplate(final TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public boolean supportTransacted()
    {
        return true;
    }

    @Override
    public Processor createErrorHandler(final RouteContext routeContext, final Processor processor)
    {
        return this.createErrorHandler(routeContext, processor, transactionTemplate);
    }

    public TxErrorHandler createErrorHandler(final RouteContext routeContext, final Processor processor,
            final TransactionTemplate localTransactionTemplate)
    {
        ObjectHelper.notNull(localTransactionTemplate, "transactionTemplate", this);

        final TxErrorHandler answer = new TxErrorHandler(routeContext.getCamelContext(), processor,
                getLogger(), getOnRedelivery(), getRedeliveryPolicy(), getExceptionPolicyStrategy(), localTransactionTemplate,
                getRetryWhilePolicy(routeContext.getCamelContext()), getExecutorService(routeContext.getCamelContext()), ROLLBACK_LOG_LEVEL);

        answer.setProcessors(processors);

        // configure error handler before we can use it
        configure(routeContext, answer);
        return answer;
    }

    @Override
    protected CamelLogger createLogger()
    {
        return new CamelLogger(LoggerFactory.getLogger(TransactionErrorHandler.class), LoggingLevel.ERROR);
    }

    @Override
    public String toString()
    {
        return "TxErrorHandlerBuilder";
    }

}
