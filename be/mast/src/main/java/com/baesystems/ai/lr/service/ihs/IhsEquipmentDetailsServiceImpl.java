package com.baesystems.ai.lr.service.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.ihs.IhsEquipmentDetailsDao;
import com.baesystems.ai.lr.dto.ihs.IhsEquipmentDetailsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "IhsEquipmentService")
@Loggable(Loggable.DEBUG)
public class IhsEquipmentDetailsServiceImpl implements IhsEquipmentDetailsService
{
    @Autowired
    private IhsEquipmentDetailsDao ihsEquipmentDetails;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public IhsEquipmentDetailsDto getEquipmentDetails(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsEquipmentDetailsDto equipDetailsDto = ihsEquipmentDetails.getEquipmentDetails(imoNumber);
        serviceHelper.verifyModel(equipDetailsDto, imoNumber);
        serviceHelper.verifyModel(equipDetailsDto.getEquipment2Dto(), imoNumber);
        serviceHelper.verifyModel(equipDetailsDto.getEquipment4Dto(), imoNumber);

        return equipDetailsDto;
    }
}
