package com.baesystems.ai.lr.service.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.references.ProductTypeExtendedDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ProductService")
@Loggable(Loggable.DEBUG)
public class ProductServiceImpl implements ProductService
{
    @Autowired
    private ProductDao productDao;

    @Autowired
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final List<ProductCatalogueDto> getProductCatalogues()
    {
        return this.serviceReferenceDataDao.getProductCatalogues();
    }

    /**
     * Get all of the Products that have been associated with the given Asset.
     *
     * @param assetId
     * @return List of {@link ProductDto}
     * @throws RecordNotFoundException
     */
    @Override
    public final List<ProductDto> getProducts(final Long assetId) throws RecordNotFoundException
    {
        // Verify that the asset exists first to avoid the wrong error being displayed if it doesn't.
        this.serviceHelper.verifyModel(this.assetDao.getAsset(assetId, null, AssetLightDto.class), assetId);
        return this.productDao.getProducts(assetId);
    }

    @Override
    @Transactional
    public final List<ProductDto> updateProducts(final Long assetId, final ProductListDto selectedProductList)
            throws RecordNotFoundException, StaleObjectException
    {
        final List<ProductDto> selectedProducts = selectedProductList.getProductList();

        // A set of the products being passed in.
        final Set<ProductDto> productBeingUpdated = selectedProducts.stream()
                .filter(product -> product.getId() != null)
                .collect(Collectors.toSet());

        // A set of the product ids being passed in.
        final Set<Long> productIdsBeingUpdated = productBeingUpdated.stream()
                .map(product -> product.getId())
                .collect(Collectors.toSet());

        // go through existing entries and see which need to be set to deleted.
        final List<ProductDto> existingProducts = this.productDao.getProducts(assetId);

        // delete all products that still exist in the database but are not in the passed selected product list.
        existingProducts.stream().map(product -> product.getId())
                .filter(productId -> !productIdsBeingUpdated.contains(productId))
                .forEach(productId -> this.productDao.deleteProduct(productId));

        // Get all products from the passed list to be updated.
        final List<ProductDto> productsToUpdate = selectedProducts
                .stream()
                .filter(product -> existingProducts.contains(product))
                .collect(Collectors.toList());

        // Update all products that exist in the database that have been passed in the selected product list.
        // Currently, this should only update the scheduling regime.
        // Note that each update performs a staleness check in case someone has updated the product with different data.

        // Have to use a for loop as Java streams don't work seamlessly with unchecked exceptions.
        for (final ProductDto productToUpdate : productsToUpdate)
        {
            final ProductDto existingProductDto = this.productDao.findOneWithLock(productToUpdate.getId(), ProductDto.class, StaleCheckType.PRODUCT);
            StaleCheckUtils.checkNotStale(existingProductDto, productToUpdate, StaleCheckType.PRODUCT);
            this.productDao.updateProductSchedulingRegime(productToUpdate.getId(),
                    productToUpdate.getSchedulingRegime() == null ? null : productToUpdate.getSchedulingRegime().getId());
        }

        // Create a list of new products to be added (ie those with null ids that don't have an equivalent in the
        // existing list from the database)
        final List<ProductDto> newProductList = selectedProducts.stream()
                .filter(product -> product.getId() == null)
                .filter(doesNotAlreadyExist(productBeingUpdated))
                .collect(Collectors.toList());

        final List<ProductDto> productsToAdd = new ArrayList<ProductDto>();

        // copy products into a new list taking only the first instance if two are equivalent
        for (final ProductDto product : newProductList)
        {
            if (isNew(product, productsToAdd))
            {
                productsToAdd.add(product);
            }
        }

        // Create a new product for each entry in the list
        productsToAdd.stream()
                .forEach(product -> this.productDao.createProduct(assetId, product.getProductCatalogueId(),
                        product.getSchedulingRegime() == null ? null : product.getSchedulingRegime().getId()));

        return this.productDao.getProducts(assetId);
    }

    @Override
    public final List<ProductTypeExtendedDto> getProductModel()
    {
        return this.serviceReferenceDataDao.getProductModel();
    }

    /**
     * @return true if the product is not in the list
     */
    private Predicate<ProductDto> doesNotAlreadyExist(final Set<ProductDto> existingProducts)
    {
        return product ->
        {
            return isNew(product, existingProducts);
        };
    }

    /**
     * @return true if an equivalent product is not in the list
     */
    private boolean isNew(final ProductDto product, final Collection<ProductDto> existingProducts)
    {
        return !existingProducts.stream().anyMatch(testProduct -> productsAreTheSame(testProduct, product));
    }

    /**
     * tests if two products are equivalent in that they have the same product catalogue and scheduling regime
     */
    private boolean productsAreTheSame(final ProductDto existingProduct, final ProductDto product)
    {
        return existingProduct.getProductCatalogueId().equals(product.getProductCatalogueId())
                && Objects.equals(existingProduct.getSchedulingRegime(), product.getSchedulingRegime());
    }
}
