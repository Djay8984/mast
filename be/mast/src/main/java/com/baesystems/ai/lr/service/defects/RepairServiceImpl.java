package com.baesystems.ai.lr.service.defects;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.references.RepairReferenceDataService;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "RepairService")
@Loggable(Loggable.DEBUG)
public class RepairServiceImpl implements RepairService
{
    @Autowired
    private RepairDao repairDao;

    @Autowired
    private WIPRepairDao wipRepairDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private RepairReferenceDataService repairReferenceDataService;

    @Override
    public RepairDto getRepair(final Long repairId) throws RecordNotFoundException, BadRequestException
    {
        final RepairDto repair = this.repairDao.getRepair(repairId);
        this.serviceHelper.verifyModel(repair, repairId);
        return repair;
    }

    @Override
    public PageResource<RepairDto> getDefectRepairs(final Long defectId, final Integer page, final Integer size, final String sort,
            final String order) throws BadPageRequestException, BadRequestException
    {
        final Pageable pageable = this.serviceHelper.createPageable(page, size, sort, order);
        return this.repairDao.getRepairsForDefect(pageable, defectId);
    }

    @Override
    public RepairDto createRepair(final Long defectId, final RepairDto repairDto)
            throws BadRequestException
    {
        final LinkResource defect = new LinkResource(defectId);
        repairDto.setDefect(defect);

        return this.repairDao.saveRepair(repairDto);
    }

    @Override
    public RepairDto createWIPRepair(final Long defectId, final RepairDto repairDto) throws BadRequestException
    {
        repairDto.setDefect(new LinkResource(defectId));

        return this.wipRepairDao.saveRepair(repairDto);
    }

    @Override
    public RepairDto updateRepair(final RepairDto repairDto) throws BadRequestException, StaleObjectException
    {
        final RepairDto existingDto = this.repairDao.findOneWithLock(repairDto.getId(), RepairDto.class, StaleCheckType.REPAIR);
        StaleCheckUtils.checkNotStale(existingDto, repairDto, StaleCheckType.REPAIR);

        return this.repairDao.saveRepair(repairDto);
    }

    @Override
    public RepairDto updateWIPRepair(final RepairDto repairDto) throws BadRequestException, StaleObjectException
    {
        final RepairDto existingDto = this.wipRepairDao.findOneWithLock(repairDto.getId(), RepairDto.class, StaleCheckType.REPAIR);
        StaleCheckUtils.checkNotStale(existingDto, repairDto, StaleCheckType.REPAIR);

        return this.wipRepairDao.saveRepair(repairDto);
    }

    @Override
    public Boolean getAreAllItemsPermanentlyRepaired(final Long defectId)
    {
        return this.repairDao.getAreAllItemsPermanentlyRepaired(defectId);
    }

    @Override
    public Boolean getAreAllItemsPermanentlyWIPRepaired(final Long wipDefectId)
    {
        return this.wipRepairDao.getAreAllItemsPermanentlyRepaired(wipDefectId);
    }

    /**
     * @param defectId, the id of the defect.
     * @return true if the defect has at least one confirmed permanent repair.
     */
    @Override
    public Boolean getDefectHasPermanentRepair(final Long defectId)
    {
        return this.repairDao.getDefectHasPermanentRepair(defectId);
    }

    @Override
    public Boolean getWIPDefectHasPermanentRepair(final Long wipDefectId)
    {
        return this.wipRepairDao.getDefectHasPermanentRepair(wipDefectId);
    }

    @Override
    public PageResource<RepairDto> getDefectRepairsForCoC(final Long cocId, final Integer page, final Integer size, final String sort,
            final String order) throws BadPageRequestException, BadRequestException
    {
        final Pageable pageable = this.serviceHelper.createPageable(page, size, sort, order);
        return this.repairDao.getRepairsForCoC(cocId, pageable);
    }

    @Override
    public PageResource<RepairDto> getDefectRepairsForWIPCoC(final Long cocId, final Integer page, final Integer size, final String sort,
            final String order) throws BadPageRequestException, BadRequestException
    {
        final Pageable pageable = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipRepairDao.getRepairsForCoC(cocId, pageable);
    }

    @Override
    public RepairDto createWIPCocRepair(final Long jobId, final RepairDto repairDto)
            throws BadRequestException, RecordNotFoundException
    {
        return wipRepairDao.saveRepair(repairDto);
    }

    @Override
    public PageResource<RepairDto> getWIPRepairsForDefect(final Integer page, final Integer size, final String sort, final String order,
            final Long defectId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipRepairDao.getRepairsForDefect(pageSpecification, defectId);
    }

    @Override
    public List<RepairDto> getWIPRepairsForWIPDefectsAndWipCoCs(final List<Long> wipDefectIds, final List<Long> wipCoCIds)
    {
        return this.wipRepairDao.getWIPRepairsForWIPDefectsAndWipCoCs(wipDefectIds, wipCoCIds);
    }

    @Override
    public RepairDto getWIPRepair(final Long repairId) throws RecordNotFoundException, BadRequestException
    {
        final RepairDto repair = this.wipRepairDao.getRepair(repairId);
        this.serviceHelper.verifyModel(repair, repairId);
        return repair;
    }

    @Override
    public List<RepairDto> getRepairsForAsset(final Long assetId)
    {
        return this.repairDao.findRepairsForAsset(assetId);
    }

    @Override
    public Boolean getRepairIsPermanentAndConfirmed(final Long repairId) throws RecordNotFoundException
    {

        Boolean isPermanentAndConfirmed = Boolean.FALSE;
        final RepairDto repairDto = this.wipRepairDao.getRepair(repairId);
        if (repairDto.getConfirmed())
        {
            final ReferenceDataDto repairAction = this.repairReferenceDataService.getRepairAction(repairDto.getRepairAction().getId());

            if (RepairActionType.PERMANENT.value().equals(repairAction.getId()))
            {
                isPermanentAndConfirmed = Boolean.TRUE;
            }
        }
        return isPermanentAndConfirmed;
    }

    @Override
    public Boolean getWIPCocHasPermanentRepair(final Long wipCocId)
    {
        return wipRepairDao.getCocHasPermanentRepair(wipCocId);
    }
}
