package com.baesystems.ai.lr.service.assets;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.dto.assets.MultiAssetDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.AssetQueryDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.enums.AssetLifecycleStatus;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "AssetService")
@Loggable(Loggable.DEBUG)
public class AssetServiceImpl implements AssetService
{
    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private JobDao jobDao;

    /**
     * Gets all assets that match the query parameters specified in {@link AssetQueryDto} query. Nulls are equivalent to
     * sql.
     * <p>
     * If no query parameters are given all assets are returned without hql query as this is more efficient.
     *
     * @param {@link AssetQueryDto} query object containing query parameters.
     * @param page
     * @param size
     * @param sort
     * @param order
     * @return List of {@link AssetLightDto}.
     * @throws BadRequestException
     * @throws BadPageRequestException
     */
    @Override
    public final PageResource<AssetLightDto> getAssets(final Integer page, final Integer size, final String sort, final String order,
            final AssetQueryDto query)
                    throws BadRequestException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        PageResource<AssetLightDto> result = null;

        if (query == null)
        {
            result = this.assetDao.findAll(pageSpecification);
        }
        else
        {
            final AssetQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(query, AssetQueryDto.class);
            result = this.assetDao.findAll(pageSpecification, cleanDto);
        }

        return result;
    }

    /**
     * Gets all assets that match the query parameters specified in {@link AbstractQueryDto} query. Nulls are equivalent
     * to sql.
     *
     * If no query parameters are given all assets are returned without hql query as this is more efficient.
     *
     * @param {@link AbstractQueryDto} query object containing query parameters.
     * @param page
     * @param size
     * @param sort
     * @param order
     * @return List of {@link AssetLightDto}.
     * @throws BadRequestException
     * @throws BadPageRequestException
     */
    @Override
    public final PageResource<AssetLightDto> getAssetsAbstract(final Integer page, final Integer size, final String sort, final String order,
            final AbstractQueryDto query) throws BadRequestException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        PageResource<AssetLightDto> result = null;

        if (query == null)
        {
            result = this.assetDao.findAll(pageSpecification);
        }
        else
        {
            result = this.assetDao.findAll(pageSpecification, query);
        }

        return result;
    }

    /**
     * Gets all assets that match the query parameters specified in {@link AbstractQueryDto} query. Nulls are equivalent
     * to sql.
     *
     * If no query parameters are given all assets are returned without hql query as this is more efficient.
     *
     * @param {@link AbstractQueryDto} query object containing query parameters.
     * @param page
     * @param size
     * @param sort
     * @param order
     * @return List of {@link AssetLightDto}.
     * @throws BadRequestException
     * @throws BadPageRequestException
     */
    @Override
    public final PageResource<MultiAssetDto> getAssetsFromBothDataBasesAbstract(final Integer page, final Integer size, final String sort,
            final String order, final AbstractQueryDto query) throws BadRequestException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        PageResource<MultiAssetDto> result = null;
        AbstractQueryDto localQuery = query;

        if (query == null)
        {
            localQuery = new AbstractQueryDto();
        }

        result = this.assetDao.findAllFromBothDatabases(pageSpecification, localQuery);
        return result;
    }

    /**
     * Gets an asset, {@link AssetLightDto}, by its identifier.
     *
     * @param assetId, asset identifier.
     * @return An asset, {@link AssetLightDto}.
     * @throws RecordNotFoundException, if asset not found.
     */
    private AssetLightDto getVersionedAsset(final Long assetId, final Long versionId) throws RecordNotFoundException
    {

        final AssetLightDto asset = this.assetDao.getAsset(assetId, versionId, AssetLightDto.class);
        this.serviceHelper.verifyModel(asset, assetId);
        return asset;
    }

    /**
     * Gets a specified asset. versionId overrides all other parameters, and if specified the requested asset version is
     * returned. getPublished overrides getDraft and if not overridden returns the published version. getDraft if
     * specified and not overridden returns the draft version. If none of the above parameters are specified the
     * published version is returned.
     *
     * @param assetId
     * @param versionId - may be set to null
     * @param getPublished - may be set to null
     * @param getDraft - may be set to null
     * @return
     */
    @Override
    public AssetLightDto getAsset(final Long assetId, final Long versionId, final Long jobId, final Boolean getPublished, final Boolean getDraft)
            throws RecordNotFoundException
    {
        AssetLightDto asset;
        if (versionId != null)
        {
            asset = this.getVersionedAsset(assetId, versionId);
        }
        else if (jobId != null)
        {
            asset = this.getVersionedAsset(assetId, this.jobDao.getVersionForAssetAndJob(assetId, jobId));
        }
        else if (getPublished != null && getPublished)
        {
            asset = this.getVersionedAsset(assetId, null);
        }
        else if (getDraft != null && getDraft)
        {
            asset = this.getVersionedAsset(assetId, this.assetDao.getDraftVersion(assetId));
        }
        else
        {
            asset = this.getVersionedAsset(assetId, null);
        }

        return asset;
    }

    /**
     * Gets all the items, including its attributes and child items, for an asset.
     *
     * @param assetId, asset identifier.
     * @return {@link AssetModelDto}, Item hierarchy for an asset.
     * @throws RecordNotFoundException
     */
    @Override
    public final AssetModelDto getAssetModel(final Long assetId, final Long versionId) throws RecordNotFoundException
    {
        final AssetModelDto assetModel = this.itemDao.getAssetModel(assetId, versionId);
        this.serviceHelper.verifyModel(assetModel, assetId);

        return assetModel;
    }

    @Override
    public AssetDto createAsset(final AssetLightDto assetLightDto) throws MastBusinessException
    {
        final ItemTypeDto rootItemType = this.itemTypeDao.getRootItemTypeForAssetCategory(assetLightDto.getAssetCategory().getId().intValue());
        if (rootItemType == null)
        {
            throw new MastBusinessException(
                    String.format("Cannot create a root item for Asset Category: %d", assetLightDto.getAssetCategory().getId()));
        }

        assetLightDto.setAssetLifecycleStatus(new LinkResource(AssetLifecycleStatus.IN_SERVICE.getValue()));
        if (assetLightDto.getHarmonisationDate() == null)
        {
            assetLightDto.setHarmonisationDate(assetLightDto.getBuildDate());
        }

        if (assetLightDto.getHullIndicator() == null)
        {
            assetLightDto.setHullIndicator(1);
        }

        // Create Asset
        final AssetMetaDto assetMetaDto = this.assetDao.createAssetMeta();

        // Save the new ID on the inbound Asset DTO
        assetLightDto.setId(assetMetaDto.getId());
        assetLightDto.setAssetVersionId(1L);

        // Create the Versioned Asset
        final AssetLightDto newLightAssetDto = this.assetDao.createAsset(assetLightDto);

        // Updated the Assets Published version to the new Versioned Asset
        assetMetaDto.setPublishedVersionId(1L);
        this.assetDao.updateAssetMeta(assetMetaDto);

        // Create Root Item and Versioned Item
        final Long itemId = this.createRootItem(assetLightDto.getId(), rootItemType);

        // Set the Root item on the Asset
        this.assetDao.setRootItemOnAsset(newLightAssetDto, itemId);

        // Retrieve and return a fresh full DTO rather than juggle the existing light dtos.
        return this.assetDao.getAsset(newLightAssetDto.getId(), newLightAssetDto.getAssetVersionId(), AssetDto.class);
    }

    public Long createRootItem(final Long assetId, final ItemTypeDto itemType)
    {
        // Create Root Item
        final ItemMetaDto itemMetaDto = this.itemPersistDao.createItemMeta(assetId);

        // Create the root item separately
        final ItemLightDto rootItemForAsset = new ItemLightDto();
        rootItemForAsset.setId(itemMetaDto.getId());
        rootItemForAsset.setAssetId(assetId);
        rootItemForAsset.setAssetVersionId(1L);

        rootItemForAsset.setItemType(new LinkResource(itemType.getId()));
        rootItemForAsset.setName(itemType.getName());
        rootItemForAsset.setReviewed(Boolean.FALSE);
        rootItemForAsset.setDisplayOrder(1);
        this.itemPersistDao.createAssetRootItem(assetId, rootItemForAsset);

        return itemMetaDto.getId();

    }

    @Override
    public AssetLightDto updateAsset(final AssetLightDto assetLightDto) throws RecordNotFoundException, StaleObjectException
    {
        final AssetLightDto existingDto = this.assetDao
                .findOneWithLock(assetLightDto.getId(), assetLightDto.getAssetVersionId(), AssetLightDto.class, StaleCheckType.ASSET);

        StaleCheckUtils.checkNotStale(existingDto, assetLightDto, StaleCheckType.ASSET);

        return this.assetDao.updateAsset(assetLightDto);
    }

    @Override
    public void deleteAsset(final Long assetId)
    {
        // can't delete assets with items due to fk constraints on item relationship.

        // todo dont think this is right, should be elaborated as part of case-versioning story
        // for now just default to published
        final Long publishedVersion = this.assetDao.getPublishedVersion(assetId);
        this.assetDao.deleteAsset(new VersionedAssetPK(assetId, publishedVersion));
    }

    /**
     * Compares given harmonisation date and asset harmonisation date.
     *
     * @param assetId Asset identifier.
     * @param harmonisationDate Harmonisation date
     * @return TRUE if dates are different, FALSE otherwise.
     * @throws RecordNotFoundException If there is no asset for given asset id.
     */
    @Override
    public Boolean isHarmonisationDateDifferent(final Long assetId, final Date harmonisationDate) throws RecordNotFoundException
    {
        final Long draftVersionId = this.assetDao.getDraftVersion(assetId);
        final AssetLightDto asset = this.getVersionedAsset(assetId, draftVersionId);

        Boolean equalDates;

        // If both dates are null
        if (asset.getHarmonisationDate() == null && harmonisationDate == null)
        {
            equalDates = Boolean.TRUE;
        }
        // If at least one date is null
        else if (asset.getHarmonisationDate() == null && harmonisationDate != null
                || asset.getHarmonisationDate() != null && harmonisationDate == null)
        {
            equalDates = Boolean.FALSE;
        }
        // Both dates are not null
        else
        {
            equalDates = new Date(asset.getHarmonisationDate().getTime()).equals(harmonisationDate);
        }

        return !equalDates;
    }

    @Override
    public void updateHarmonisationDate(final Long jobId, final Date harmonisationDate) throws RecordNotFoundException
    {
        if (harmonisationDate != null)
        {
            final Long assetId = this.jobDao.getAssetIdForJob(jobId);
            final Long assetVersionId = this.jobDao.getJob(jobId).getAsset().getVersion();
            final AssetLightDto assetDto = this.assetDao.getAsset(assetId, assetVersionId, AssetLightDto.class);
            assetDto.setHarmonisationDate(harmonisationDate);
            this.assetDao.updateAsset(assetDto);
        }
    }
}
