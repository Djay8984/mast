package com.baesystems.ai.lr.processor;

import java.io.IOException;
import java.util.Date;

import com.baesystems.ai.lr.utils.DateHelper;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MastDateSerializer extends JsonSerializer<Date>
{
    @Override
    public void serialize(final Date date, final JsonGenerator jsonGenerator, final SerializerProvider serializers)
            throws IOException, JsonProcessingException
    {
        final String stringDate = DateHelper.mastFormatter().format(date);
        jsonGenerator.writeString(stringDate);
    }
}
