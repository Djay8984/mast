package com.baesystems.ai.lr.utils;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.codicils.CodicilDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.DueStatusType;

@Service(value = "DueStatusService")
public class DueStatusServiceImpl implements DueStatusService
{
    @Autowired
    private DateHelper dateHelper;

    @Override
    public <T extends DueStatusUpdateableDto> void calculateDueStatus(final T object)
    {
        calculateDueStatus(object, new Date());
    }

    @Override
    public <T extends DueStatusUpdateableDto> void calculateDueStatuses(final List<T> objects)
    {
        if (objects != null)
        {
            final Date currentDate = new Date();

            for (final T object : objects)
            {
                calculateDueStatus(object, currentDate);
            }
        }
    }

    private <T extends DueStatusUpdateableDto> void calculateDueStatus(final T object, final Date currentDate)
    {
        if (object != null && currentDate != null && object.getDueDate() != null)
        {
            if (this.dateHelper.isTodayOrAfterStrict(object.getDueDate(), currentDate))
            {
                object.setDueStatus(new LinkResource(DueStatusType.NOT_DUE.getValue()));
            }
            else
            {
                object.setDueStatus(new LinkResource(DueStatusType.OVERDUE.getValue()));
            }
        }
    }

    @Override
    public void calculateServiceDueStatus(final ScheduledServiceDto service)
    {
        updateDueStatus(service, new Date());
    }

    @Override
    public void calculateServiceDueStatuses(final List<ScheduledServiceDto> services)
    {
        if (services != null)
        {
            for (final ScheduledServiceDto service : services)
            {
                updateDueStatus(service, new Date());
            }
        }
    }

    private void updateDueStatus(final ScheduledServiceDto service, final Date currentDate)
    {
        if (currentDate != null && service != null && service.getLowerRangeDate() != null && service.getUpperRangeDate() != null)
        {
            if (!this.dateHelper.isTodayOrAfterStrict(service.getLowerRangeDate(), currentDate)
                    && !this.dateHelper.isTodayOrBeforeStrict(service.getUpperRangeDate(), currentDate))
            {
                service.setDueStatus(new LinkResource(DueStatusType.DUE.getValue()));
            }
            else if (this.dateHelper.isTodayOrAfterStrict(service.getLowerRangeDate(), currentDate))
            {
                service.setDueStatus(new LinkResource(DueStatusType.NOT_DUE.getValue()));
            }
            else if (this.dateHelper.isTodayOrBeforeStrict(service.getUpperRangeDate(), currentDate))
            {
                service.setDueStatus(new LinkResource(DueStatusType.OVERDUE.getValue()));
            }
        }
    }

    @Override
    public <T extends CodicilDto> void updateCodicilStatus(final T codicil)
    {
        if (codicil.getImposedDate() != null && codicil.getStatus() != null)
        {
            this.setCodicilStatus(codicil);
        }
    }

    private <T extends CodicilDto> void setCodicilStatus(final T codicil)
    {
        if (CodicilStatusType.getOpenAndInactiveForAssetNote().contains(codicil.getStatus().getId()))
        {
            if (dateHelper.isTodayOrBefore(codicil.getImposedDate()))
            {
                codicil.getStatus().setId(CodicilStatusType.AN_OPEN.getValue());
            }
            else
            {
                codicil.getStatus().setId(CodicilStatusType.AN_INACTIVE.getValue());
            }
        }
        else if (CodicilStatusType.getOpenAndInactiveForActionableItem().contains(codicil.getStatus().getId()))
        {
            if (dateHelper.isTodayOrBefore(codicil.getImposedDate()))
            {
                codicil.getStatus().setId(CodicilStatusType.AI_OPEN.getValue());
            }
            else
            {
                codicil.getStatus().setId(CodicilStatusType.AI_INACTIVE.getValue());
            }
        }
    }
}
