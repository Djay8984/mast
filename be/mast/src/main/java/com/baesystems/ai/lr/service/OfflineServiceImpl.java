package com.baesystems.ai.lr.service;

import static com.baesystems.ai.lr.utils.CollectionUtils.nullIfEmpty;
import static com.baesystems.ai.lr.utils.CollectionUtils.nullSafeStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.jobs.JobBundleDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.assets.AssetService;
import com.baesystems.ai.lr.service.codicils.ActionableItemService;
import com.baesystems.ai.lr.service.codicils.AssetNoteService;
import com.baesystems.ai.lr.service.codicils.CoCService;
import com.baesystems.ai.lr.service.defects.DefectService;
import com.baesystems.ai.lr.service.defects.RepairService;
import com.baesystems.ai.lr.service.jobs.JobService;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.service.reports.ReportService;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.baesystems.ai.lr.service.tasks.WorkItemService;
import com.jcabi.aspects.Loggable;

@Service("OfflineService")
@Loggable(Loggable.DEBUG)
public class OfflineServiceImpl implements OfflineService
{
    @Autowired
    private JobService jobService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private ActionableItemService actionableItemService;

    @Autowired
    private CoCService coCService;

    @Autowired
    private AssetNoteService assetNoteService;

    @Autowired
    private DefectService defectService;

    @Autowired
    private RepairService repairService;

    @Autowired
    private AssetService assetService;

    @Autowired
    private WorkItemService workItemService;

    @Autowired
    private ServiceService serviceService;

    // @Autowired
    // private AttachmentService attachmentService;

    private final MapperFacade mapper;

    public OfflineServiceImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(ReportDto.class, ReportLightDto.class).byDefault().toClassMap());

        this.mapper = factory.getMapperFacade();
    }

    // todo replace with pure camel config
    @Override
    public JobBundleDto getJobBundle(final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final JobBundleDto jobBundle = new JobBundleDto();

        jobBundle.setId(jobId);

        // Job Stuff
        jobBundle.setJob(this.jobService.getJob(jobId));

        jobBundle.setReport(this.mapper.mapAsList(this.reportService.getReports(jobId), ReportDto.class));

        jobBundle.setSurvey(this.surveyService.getSurveys(null, null, null, null, jobId).getContent());

        jobBundle.setWipTask(this.workItemService.getWIPTasksForJob(jobId));

        jobBundle.setWipActionableItem(this.actionableItemService.getWIPActionableItemsForJobId(null, null, null, null, jobId, null).getContent());

        jobBundle.setWipCoC(this.coCService.getWIPCoCsForJob(null, null, null, null, jobId, null).getContent());

        jobBundle.setWipAssetNote(this.assetNoteService.getWIPAssetNotesForJob(null, null, null, null, jobId).getContent());

        jobBundle.setWipDefect(this.defectService.getWIPDefectsForJob(jobId, null, null, null, null).getContent());

        final List<Long> wipDefectIds = nullSafeStream(jobBundle.getWipDefect())
                .map(DefectDto::getId)
                .collect(Collectors.toList());

        final List<Long> wipCoCIds = nullSafeStream(jobBundle.getWipCoC())
                .map(CoCDto::getId)
                .collect(Collectors.toList());

        jobBundle.setWipRepair(this.repairService.getWIPRepairsForWIPDefectsAndWipCoCs(
                nullIfEmpty(wipDefectIds), nullIfEmpty(wipCoCIds)));

        final Long assetId = jobBundle.getJob().getAsset().getId();
        final Long assetVersionId = jobBundle.getJob().getAsset().getVersion();

        // Asset stuff
        jobBundle.setAsset(this.assetService.getAsset(assetId, assetVersionId, null, null, null));

        jobBundle.setAssetModel(this.assetService.getAssetModel(assetId, assetVersionId));

        jobBundle.setActionableItem(this.actionableItemService.getActionableItemsForAssets(null, null, null, null, assetId, null).getContent());

        jobBundle.setCoC(this.coCService.getCoCsByAsset(null, null, null, null, assetId, null).getContent());

        jobBundle.setAssetNote(this.assetNoteService.getAssetNotesByAsset(null, null, null, null, assetId, null).getContent());

        jobBundle.setDefect(this.defectService.getDefectsForAsset(null, null, null, null, assetId, null).getContent());

        jobBundle.setRepair(this.repairService.getRepairsForAsset(assetId));

        jobBundle.setService(this.serviceService.getServicesForAsset(assetId, null, null, null, null));

        jobBundle.setTask(this.workItemService.getTasksForAsset(assetId));

        // uncomment in R3
        // setAttachments(jobBundle);

        jobBundle.setAttachment(new ArrayList<>());

        return jobBundle;
    }

    /**
     * After the persistence of a new wipDefect, we need to iterate over the containing job bundle and update any
     * associated entities to the defect id. This is because these entities will not yet have been persisted and are
     * using a transient ID (in form of the internalId).
     *
     * @param jobBundleDto The Job Bundle
     * @param temporalDefectId The Temporary (offline) ID assigned to the wip defect by the FE
     * @param persistedDefect The defect that has just been persisted
     * @return jobBundleDto
     */
    @Override
    public JobBundleDto updateAnyRelatedWipEntitiesForNewWipDefect(final JobBundleDto jobBundleDto, final Long temporalDefectId,
            final DefectDto persistedDefect)
    {
        final Long actualId = persistedDefect.getId();
        if (temporalDefectId != null && actualId != null && jobBundleDto != null)
        {
            nullSafeStream(jobBundleDto.getWipRepair())
                    .filter(repairDto -> this.isAssociated(repairDto.getDefect(), temporalDefectId))
                    .forEach(repairDto -> this.updateWIPRepair(repairDto, persistedDefect));

            nullSafeStream(jobBundleDto.getWipCoC())
                    .filter(wipCoC -> this.isAssociated(wipCoC.getDefect(), temporalDefectId))
                    .forEach(wipCoC -> wipCoC.getDefect().setId(actualId));

            nullSafeStream(jobBundleDto.getWipActionableItem())
                    .filter(wipActionableItem -> this.isAssociated(wipActionableItem.getDefect(), temporalDefectId))
                    .forEach(wipActionableItem -> wipActionableItem.getDefect().setId(actualId));
        }
        return jobBundleDto;
    }

    private void updateWIPRepair(final RepairDto repair, final DefectDto persistedDefect)
    {
        repair.getDefect().setId(persistedDefect.getId());
        for (final DefectItemDto defectItem : persistedDefect.getItems() == null
                ? Collections.<DefectItemDto>emptyList() : persistedDefect.getItems())
        {
            nullSafeStream(repair.getRepairs())
                    .filter(wipRepairItem -> wipRepairItem.getItem().getItem().getId().equals(defectItem.getItem().getId()))
                    .forEach(wipRepairItem -> wipRepairItem.getItem().setId(defectItem.getId()));
        }
    }

    /**
     * After the persistence of a new wipCoc, we need to iterate over the containing job bundle and update any
     * associated entities to the coc id. This is because these entities will not yet have been persisted and are using
     * a transient ID (in form of the internalId).
     *
     * @param jobBundleDto The Job Bundle
     * @param temporalCoCId The Temporary (offline) ID assigned to the wip coc by the FE
     * @param actualId The actual ID which was then assigned to the coc when persisted
     * @return jobBundleDto
     */
    @Override
    public JobBundleDto updateAnyRelatedWipEntitiesForNewWipCoC(final JobBundleDto jobBundleDto, final Long temporalCoCId, final Long actualId)
    {
        if (temporalCoCId != null && actualId != null && jobBundleDto != null)
        {
            nullSafeStream(jobBundleDto.getWipRepair())
                    .filter(repairDto -> this.isAssociated(repairDto.getCodicil(), temporalCoCId))
                    .forEach(repairDto -> repairDto.getCodicil().setId(actualId));
        }
        return jobBundleDto;
    }

    /**
     * After the persistence of a new Survey, we need to iterate over the containing job bundle and update any
     * associated entities to the Survey ID. This is because these entities will not yet have been persisted and are
     * using a transient ID (in form of the internalId).
     *
     * @param jobBundleDto The Job Bundle
     * @param temporalSurveyId The Temporary (offline) ID assigned to the Survey by the FE
     * @param actualId The actual ID which was then assigned to the Survey when persisted
     * @return jobBundleDto
     */
    @Override
    public JobBundleDto updateAnyRelatedWipEntitiesForNewSurvey(final JobBundleDto jobBundleDto, final Long temporalSurveyId, final Long actualId)
    {
        if (temporalSurveyId != null && actualId != null && jobBundleDto != null)
        {
            nullSafeStream(jobBundleDto.getTask())
                    .filter(taskDto -> this.isAssociated(taskDto.getSurvey(), temporalSurveyId))
                    .forEach(taskDto -> taskDto.getSurvey().setId(actualId));
        }

        return jobBundleDto;
    }

    private boolean isAssociated(final BaseDto link, final Long temporalId)
    {
        return link != null && link.getId() == null && link.getInternalId().equals(temporalId.toString());
    }

    // /**
    // * Builds and sets attachments attachments/notes for job and asset and all related entities.
    // */
    // private void setAttachments(final JobBundleDto jobBundle) //NOPMD
    // {
    // final AttachmentLinkQueryDto attachmentLinkQueryDto = new AttachmentLinkQueryDto();
    //
    // //Job
    // attachmentLinkQueryDto.setWipDefect(jobBundle.getWipDefect().stream().map(DefectLightDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setJob(Collections.singletonList(jobBundle.getJob().getId()));
    //
    // attachmentLinkQueryDto.setWipCoC(jobBundle.getWipCoC().stream().map(CoCDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setWipAssetNote(jobBundle.getWipAssetNote().stream().map(AssetNoteDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto
    // .setWipActionableItem(jobBundle.getWipActionableItem().stream().map(ActionableItemDto::getId).collect(Collectors.toList()));
    //
    // //Asset
    // attachmentLinkQueryDto.setDefect(jobBundle.getDefect().stream().map(DefectLightDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setAsset(Collections.singletonList(jobBundle.getAsset().getId()));
    //
    // attachmentLinkQueryDto.setCoC(jobBundle.getCoC().stream().map(CoCDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setItem(jobBundle.getAssetModel().getItems().stream().map(ItemDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setAssetNote(jobBundle.getAssetNote().stream().map(AssetNoteDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setRepair(jobBundle.getRepair().stream().map(RepairDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setReport(jobBundle.getReport().stream().map(ReportLightDto::getId).collect(Collectors.toList()));
    //
    // attachmentLinkQueryDto.setActionableItem(jobBundle.getActionableItem().stream().map(ActionableItemDto::getId).collect(Collectors.toList()));
    //
    // final List<SupplementaryInformationDto> attachments =
    // this.attachmentService.getAttachmentsByQuery(attachmentLinkQueryDto);
    //
    // jobBundle.setAttachment(attachments);
    // }
}
