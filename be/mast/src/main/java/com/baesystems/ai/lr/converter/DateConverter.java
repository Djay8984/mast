package com.baesystems.ai.lr.converter;

import java.text.ParseException;
import java.util.Date;

import org.apache.camel.Converter;
import org.apache.camel.TypeConverters;

import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

public final class DateConverter implements TypeConverters
{
    private DateConverter()
    {
    }

    @Converter
    public static Date toDate(final String value) throws BadRequestException
    {
        try
        {
            return value == null ? null : DateHelper.mastFormatter().parse(value);
        }
        catch (final ParseException exception)
        {
            final String message = String.format(ExceptionMessagesUtils.DETAILED_BINDING_ERROR_MSG, value, "date");
            throw new BadRequestException(message, exception);
        }
    }
}
