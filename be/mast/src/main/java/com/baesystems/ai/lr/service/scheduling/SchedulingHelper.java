package com.baesystems.ai.lr.service.scheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.scheduling.SRECertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.scheduling.SREServiceDto;
import com.baesystems.ai.lr.dto.scheduling.SREUncertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.ServiceStatus;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.jcabi.aspects.Loggable;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Component(value = "SchedulingHelper")
@Loggable(Loggable.DEBUG)
public class SchedulingHelper
{
    @Autowired
    private SREHandler sreHandler;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private AssetDao assetDao;

    private final MapperFacade mapper;

    public SchedulingHelper()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(ServiceCatalogueLightDto.class, ScheduledServiceDto.class)
                .fieldAToB("id", "serviceCatalogueId")
                .exclude("internalId")
                .byDefault().toClassMap());
        this.mapper = factory.getMapperFacade();
    }

    private void mapSchedule(final SREServiceDto sreService, final ScheduledServiceDto scheduledService)
    {
        scheduledService.setAssignedDate(sreService.getAssignedDate());
        scheduledService.setDueDate(sreService.getDueDate());
        scheduledService.setLowerRangeDate(sreService.getLowerRangeDate());
        scheduledService.setUpperRangeDate(sreService.getUpperRangeDate());
    }

    private void mapSchedule(final ScheduledServiceDto fromService, final ScheduledServiceDto toService)
    {
        toService.setAssignedDate(fromService.getAssignedDate());
        toService.setDueDate(fromService.getDueDate());
        toService.setLowerRangeDate(fromService.getLowerRangeDate());
        toService.setUpperRangeDate(fromService.getUpperRangeDate());
    }

    private void mapNewService(final ScheduledServiceDto service, final Long assetId)
    {
        service.setAsset(new LinkResource(assetId));
        service.setServiceStatus(new LinkResource(ServiceStatus.NOT_STARTED.getValue()));
        service.setServiceCreditStatus(new LinkResource(ServiceCreditStatusType.NOT_STARTED.value()));
    }

    private void updateAssetHullIndicator(final Long assetId, final Integer hullIndicator)
    {
        if (hullIndicator != null)
        {
            this.assetDao.updateHullIndicator(assetId, hullIndicator);
        }
    }

    /**
     * Call the Scheduling Rule Engine API to get the scheduling dates for a single service. Convert the result into a
     * ScheduledService with the schedule information.
     *
     * @param assetId
     * @param creditedServiceId
     * @param harmonisationDate
     * @return {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    public ScheduledServiceDto getServiceScheduleForSingleService(final Long assetId, final Long creditedServiceId, final Date harmonisationDate)
            throws RecordNotFoundException
    {
        final SREUncertifiedSuccessResponseDto response = this.sreHandler.getManualEntryService(creditedServiceId, harmonisationDate);

        this.updateAssetHullIndicator(assetId, response.getHullIndicator());

        return response.getService() == null ? null : this.processService(response.getService(), assetId);
    }

    /**
     * Given a completed Service, call the Scheduling Rule Engine API to get a list of the next services which need to
     * be scheduled. Convert the result into a list of Scheduled Services with schedule information.
     *
     * @param assetId
     * @param creditedServiceId
     * @param harmonisationDate
     * @param completionDate
     * @return List of {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    public List<ScheduledServiceDto> getNextServicesFromCompletedService(final Long assetId, final Long creditedServiceId,
            final Date harmonisationDate, final Date completionDate) throws RecordNotFoundException
    {
        final SRECertifiedSuccessResponseDto response = this.sreHandler.getNextServices(creditedServiceId, harmonisationDate, completionDate);

        this.updateAssetHullIndicator(assetId, response.getHullIndicator());

        List<ScheduledServiceDto> nextServices = null;

        if (response.getServices() != null)
        {
            nextServices = this.processServiceList(response.getServices(), assetId);

            // Always set the Parent Service when returning from the 'nextServices' method on the rule engine.
            for (final ScheduledServiceDto nextService : nextServices)
            {
                nextService.setParentService(new LinkResource(creditedServiceId));
            }
        }

        return nextServices;
    }

    /**
     * Generate a Scheduled Service using the schedule dates and service catalogue outlined in the given Scheduling Rule
     * Engine Service.
     *
     * @param sreService
     * @param assetId
     * @return Single {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    public ScheduledServiceDto processService(final SREServiceDto sreService, final Long assetId) throws RecordNotFoundException
    {
        final ServiceCatalogueLightDto serviceCatalogue = this.serviceService
                .getServiceCatalogue(sreService.getServiceCatalogueIdentifier().longValue());

        final ScheduledServiceDto scheduledService = mapper.map(serviceCatalogue, ScheduledServiceDto.class);
        this.mapNewService(scheduledService, assetId);
        this.mapSchedule(sreService, scheduledService);

        return scheduledService;
    }

    /**
     * Generate a list of Scheduled Services using the schedule dates and service catalogues outlined in the given list
     * of Scheduling Rule Engine Services.
     *
     * @param sreServices as list of {@link SREServiceDto}
     * @param assetId
     * @return List of {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    public List<ScheduledServiceDto> processServiceList(final List<SREServiceDto> sreServices, final Long assetId) throws RecordNotFoundException
    {
        final List<ScheduledServiceDto> scheduledServices = new ArrayList<ScheduledServiceDto>();

        for (final SREServiceDto sreService : sreServices)
        {
            scheduledServices.add(this.processService(sreService, assetId));
        }

        return scheduledServices;
    }

    /**
     * Create a list of Scheduled Services that comprises a combination of active services which already exist for the
     * asset and any rule generated services outside of those service catalogue IDs. The aim is to limit the list to
     * only include unique service catalogue IDs (or unique combinations of service catalogue ids and item ids for
     * multiple services) and for existing services, to update their schedule to match the corresponding rule generated
     * dates.
     *
     * @param assetId
     * @param provisionalDates
     * @param ruleGeneratedServices
     * @param existingServicesForAsset
     * @return A list of {@link ScheduledServiceDto} that has schedule dates populated and is ready to save.
     */
    public List<ScheduledServiceDto> generateNewServiceSchedule(final Long assetId, final Boolean provisionalDates,
            final List<ScheduledServiceDto> existingServicesForAsset, final ScheduledServiceDto... ruleGeneratedServices)
    {
        final List<ScheduledServiceDto> servicesWithNewSchedule = new ArrayList<ScheduledServiceDto>();

        // Spin through the rule generated services and existing services to see if we have any matching catalogue IDs.
        for (final ScheduledServiceDto ruleGeneratedService : ruleGeneratedServices)
        {
            boolean serviceAlreadyPresent = false;

            for (final ScheduledServiceDto existingAssetService : existingServicesForAsset)
            {
                if (!serviceAlreadyPresent && existingAssetService.getServiceCatalogueId().equals(ruleGeneratedService.getServiceCatalogueId()))
                {
                    // A service with this catalogue ID already exists for this asset.
                    this.mapSchedule(ruleGeneratedService, existingAssetService);

                    if (ruleGeneratedService.getParentService() != null)
                    {
                        existingAssetService.setParentService(ruleGeneratedService.getParentService());
                    }

                    existingAssetService.setProvisionalDates(provisionalDates);
                    servicesWithNewSchedule.add(existingAssetService);

                    serviceAlreadyPresent = true;
                }
            }

            if (!serviceAlreadyPresent)
            {
                // No existing service found that matches this catalogue ID.
                ruleGeneratedService.setProvisionalDates(provisionalDates);
                servicesWithNewSchedule.add(ruleGeneratedService);
            }
        }

        return servicesWithNewSchedule;
    }
}
