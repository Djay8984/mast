package com.baesystems.ai.lr.service.assets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.VersioningHelper;
import com.jcabi.aspects.Loggable;

@Service("AttributeService")
@Loggable(Loggable.DEBUG)
public class AttributeServiceImpl implements AttributeService
{
    @Autowired
    private AttributeDao attributeDao;

    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private ItemService itemService;

    @Autowired
    private VersioningHelper versionHelper;

    @Autowired
    private AssetVersionUpdateService assetVersionUpdateService;

    @Override
    public AttributeDto updateAttribute(final Long itemId, final AttributeDto attributeDto) throws RecordNotFoundException
    {
        final AttributeDto existingAttribute = this.attributeDao.getAttribute(attributeDto.getId());
        if (!existingAttribute.getValue().equals(attributeDto.getValue())) // should never be null
        {
            this.itemPersistDao.setItemAndAllAncestorsToRequireAReview(itemId);
        }
        final AttributeDto attribute = this.attributeDao.updateAttribute(itemId, attributeDto);
        this.serviceHelper.verifyModel(attribute, attribute.getId());

        return attribute;
    }

    @Override
    public ItemWithAttributesDto updateAttributes(final Long assetId, final Long itemId, final ItemWithAttributesDto itemWithAttributesDto)
            throws RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final List<AttributeDto> responseAttributes = new ArrayList<>();

        // Used to determine whether any changes have occurred, to then update the item name and review flags.
        boolean requiresReview = false;

        final Map<Long, AttributeDto> existingAttributeMap = createExistingAttributeMap(itemId);

        final List<AttributeDto> attributes = itemWithAttributesDto.getAttributes();

        for (final AttributeDto attributeDto : attributes)
        {
            if (attributeDto.getId() == null)
            {
                // the attribute is new
                responseAttributes.add(createAttribute(assetId, itemId, attributeDto));
                requiresReview = true;
            }
            else
            {
                // the attribute exists and is to be updated
                final Long attributeId = attributeDto.getId();
                final AttributeDto existingAttributeDto = existingAttributeMap.get(attributeId);

                // the value has changed, so update it
                if (!existingAttributeDto.getValue().equals(attributeDto.getValue()))
                {
                    // return the dto with the new id set
                    responseAttributes.add(this.attributeDao.updateAttribute(itemId, attributeDto));
                    requiresReview = true;
                }
                else
                {
                    // Return the unaltered dto
                    responseAttributes.add(attributeDto);
                }
                existingAttributeMap.remove(attributeId);
            }
        }

        // Remove all attributes no longer present in the passed list.
        for (final AttributeDto attributeToBeDeleted : existingAttributeMap.values())
        {
            deleteAttribute(assetId, itemId, attributeToBeDeleted);
            requiresReview = true;
        }

        // Only consider updating the item name if attributes have changed.
        if (requiresReview)
        {
            final String newItemName = this.itemService.updateItemName(itemWithAttributesDto);
            if (newItemName != null)
            {
                itemWithAttributesDto.setName(newItemName);
            }

            this.itemPersistDao.setItemAndAllAncestorsToRequireAReview(itemId);
        }

        itemWithAttributesDto.setAttributes(responseAttributes);
        return itemWithAttributesDto;
    }

    private Map<Long, AttributeDto> createExistingAttributeMap(final Long itemId) throws BadPageRequestException
    {
        final List<AttributeDto> existingAttributesForThisItem = this.attributeDao.findAttributesByItem(itemId);
        final Map<Long, AttributeDto> existingAttributeMap = new HashMap<>();
        for (final AttributeDto existingAttributeDto : existingAttributesForThisItem)
        {
            existingAttributeMap.put(existingAttributeDto.getId(), existingAttributeDto);
        }
        return existingAttributeMap;
    }

    /**
     * Creates an Attribute.
     * <p>
     * This implementation contains logic to prevent the creation of an attribute under certain conditions.
     * <p>
     * An attribute is considered equivalent by attributeType and Item. The number of equivalent attributes should be
     * exceed the 'max_occur' value of the corresponding attribute_type table.
     *
     * @param itemId, the ID of the item for which the attribute is to be associated.
     * @param attributeDto, the DTO representation of the attribute entity
     * @return An AttributeDto generated from the persisted entity, {@link AttributeDto} .
     * @throws RecordNotFoundException, MastBusinessException.
     */
    @SuppressWarnings("CPD-START")
    @Override
    public AttributeDto createAttribute(final Long assetId, final Long itemId, final AttributeDto attributeDto)
            throws RecordNotFoundException, MastBusinessException
    {
        final AttributeTypeDto attributeTypeDto = this.assetReferenceDataDao.getAttributeType(attributeDto.getAttributeType().getId());
        final VersionedItemPK itemPK = this.versionHelper.getVersionedItemPK(itemId, assetId, null, true);

        if (!this.itemDao.itemExists(itemPK)) // If not draft
        {
            this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
        }

        if (attributeTypeDto != null)
        {

            final Integer numberOfExistingInstances = this.attributeDao
                    .countAttributesByItemAndAttributeType(itemId, attributeDto.getAttributeType().getId(), itemPK.getAssetVersionId());
            final Integer max = attributeTypeDto.getMaxOccurs();

            final boolean unlimited = max == 0;
            final boolean canCreate = unlimited || numberOfExistingInstances < max;

            if (!canCreate)
            {
                throw new MastBusinessException(
                        String.format("Unable to create attribute due to maximum instance rules. Existing: %d, Min %d", numberOfExistingInstances,
                                max));
            }
        }

        return this.attributeDao.createAttribute(attributeDto, itemPK);
    }

    /**
     * Deletes an Attribute.
     * <p>
     * This implementation contains logic to prevent the deletion of an attribute under certain conditions.
     * <p>
     * An attribute is considered equivalent by attributeType and Item. The number of equivalent attributes should
     * always equal or exceed the 'min_occur' value of the corresponding attribute_type table.
     *
     * @param itemId, the ID of the item for which the attribute is associated.
     * @param attributeId, the ID of the attribute to be deleted
     * @throws RecordNotFoundException, MastBusinessException.
     */
    @Override
    public void deleteAttribute(final Long assetId, final Long itemId, final Long attributeId) throws RecordNotFoundException, MastBusinessException
    {
        final AttributeDto attributeDto = this.attributeDao.getAttribute(attributeId);
        deleteAttribute(assetId, itemId, attributeDto);
    }

    private void deleteAttribute(final Long assetId, final Long itemId, final AttributeDto attributeDto)
            throws RecordNotFoundException, MastBusinessException
    {
        Long attributeId = attributeDto.getId();
        final AttributeTypeDto attributeTypeDto = this.assetReferenceDataDao.getAttributeType(attributeDto.getAttributeType().getId());
        final VersionedItemPK itemPK = this.versionHelper.getVersionedItemPK(itemId, assetId, null, true);

        if (!this.itemDao.itemExists(itemPK)) // If not draft
        {
            final Long draftVersionId = this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
            final LazyItemDto upversionedItem = this.itemDao.getItem(new VersionedItemPK(itemId, assetId, draftVersionId));
            attributeId = getAttributeId(attributeDto, upversionedItem.getAttributes());
        }

        if (attributeTypeDto != null)
        {
            final Integer numberOfExistingInstances = this.attributeDao
                    .countAttributesByItemAndAttributeType(itemId, attributeDto.getAttributeType().getId(), itemPK.getAssetVersionId());
            final Integer min = attributeTypeDto.getMinOccurs();

            if (numberOfExistingInstances - 1 < min)
            {
                throw new MastBusinessException(
                        String.format("Unable to delete attribute due to minimum instance rules. Existing: %d, Min: %d", numberOfExistingInstances,
                                min));
            }
        }
        this.attributeDao.deleteAttribute(attributeId);
    }

    /**
     * Find the the corresponding attribute from a list a up-versioned attributes
     * @param originalAttributeDto the original attribute
     * @param attributes a list of up-versioned attributes
     * @return the ID of the corresponding attribute
     */
    private Long getAttributeId(final AttributeDto originalAttributeDto, final List<AttributeDto> attributes)
    {
        return CollectionUtils.nullSafeStream(attributes)
                .filter(attributeDto -> Objects.equals(attributeDto.getAttributeType(), originalAttributeDto.getAttributeType()))
                .filter(attributeDto -> Objects.equals(attributeDto.getItem(), originalAttributeDto.getItem()))
                .filter(attributeDto -> Objects.equals(attributeDto.getValue(), originalAttributeDto.getValue()))
                .findFirst().get().getId();
    }

    @SuppressWarnings("CPD-END")
    @Override
    public PageResource<AttributeDto> getAttributes(final Integer page, final Integer size, final String sort, final String order, final Long itemId)
            throws RecordNotFoundException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.attributeDao.findAttributesByItem(itemId, pageSpecification);
    }

    @Override
    public AttributeDto getAttribute(final Long attributeId)
    {
        return this.attributeDao.getAttribute(attributeId);
    }
}
