package com.baesystems.ai.lr.service.tasks;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "WorkItemService")
@Loggable(Loggable.DEBUG)
public class WorkItemServiceImpl implements WorkItemService
{
    @Autowired
    private WorkItemDao workItemDao;

    @Autowired
    private WIPWorkItemDao wipWorkItemDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private TaskRuleService taskRuleService;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public PageResource<WorkItemLightDto> getTasks(final Integer page, final Integer size, final String sort, final String order,
            final WorkItemQueryDto query) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final WorkItemQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(query, WorkItemQueryDto.class);
        return this.workItemDao.getTasks(pageSpecification, cleanDto);
    }

    @Override
    public PageResource<WorkItemLightDto> getWIPTasks(final Integer page, final Integer size, final String sort, final String order,
            final WorkItemQueryDto query) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final WorkItemQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(query, WorkItemQueryDto.class);
        return this.wipWorkItemDao.getTasks(pageSpecification, cleanDto);
    }

    @Override
    public List<WorkItemLightDto> getTasksForAsset(final Long assetId)
    {
        return this.workItemDao.getLightTasksForAsset(assetId);
    }

    @Override
    public List<WorkItemLightDto> getWIPTasksForJob(final Long jobId)
    {
        return this.wipWorkItemDao.getLightTasksForJob(jobId);
    }

    @Override
    public WorkItemLightListDto updateWIPTasks(final WorkItemLightListDto tasks) throws RecordNotFoundException, StaleObjectException
    {
        final List<WorkItemLightDto> updatedTaskList = new ArrayList<WorkItemLightDto>();

        if (tasks.getTasks() != null)
        {
            for (final WorkItemLightDto task : tasks.getTasks())
            {
                updatedTaskList.add(this.updateWIPTask(task));
            }
        }

        final WorkItemLightListDto updatedTasks = new WorkItemLightListDto();
        updatedTasks.setTasks(updatedTaskList);

        return updatedTasks;
    }

    @Override
    public WorkItemLightDto updateWIPTask(final WorkItemLightDto task) throws RecordNotFoundException, StaleObjectException
    {
        final WorkItemLightDto existingTaskDto = this.wipWorkItemDao.findOneWithLock(task.getId(), WorkItemLightDto.class, StaleCheckType.TASK);

        StaleCheckUtils.checkNotStale(existingTaskDto, task, StaleCheckType.TASK);

        this.serviceHelper.verifyModel(task, task.getId());
        return this.wipWorkItemDao.updateTask(task);
    }

    @Override
    public WorkItemLightDto createWIPTask(final WorkItemLightDto task) throws RecordNotFoundException
    {
        return this.wipWorkItemDao.createOrUpdateTask(task);
    }

    @Override
    public void generateWIPTasks(final SurveyDto survey) throws RecordNotFoundException, BadPageRequestException
    {
        if (survey.getScheduledService() != null)
        {
            final List<WorkItemLightDto> tasks = this.workItemDao.getTasksForService(survey.getScheduledService().getId());
            for (final WorkItemLightDto task : tasks)
            {
                task.setSurvey(new LinkResource(survey.getId()));
                task.setParent(new LinkResource(task.getId()));
                task.setId(null);
                this.createWIPTask(task);
            }
        }
    }

    /**
     * Retrieves all Scheduled Services for the asset associated with the provided Job and call the Task Rules Engine
     * for each service.
     * <p>
     * It may be the case that this is equivalent to simply calling the asset centric task Rules Engine endpoint, but at
     * the risk of the Rules Engine implementation being unseen-ly different we play it safe and explicitly call for
     * each service.
     *
     * @param jobId the JobId Id
     */
    @Override
    public void updateTasksFollowingFARorFSR(final Long jobId)
    {
        final Long assetId = this.jobDao.getAssetIdForJob(jobId);
        final List<Long> schedServiceIds = CollectionUtils
                .toIDList(this.serviceDao.getServicesForAsset(assetId, ServiceCreditStatusType.getOpen(), null, null, null));
        schedServiceIds.forEach(serviceId -> this.taskRuleService.syncTasksForScheduledService(assetId, serviceId));
    }
}
