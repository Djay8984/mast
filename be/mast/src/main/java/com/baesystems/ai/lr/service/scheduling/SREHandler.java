package com.baesystems.ai.lr.service.scheduling;

import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_COMMUNICATION_FAILURE;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_ERROR;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_INVALID_RESPONSE;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.baesystems.ai.lr.dto.scheduling.SREBaseResponseDto;
import com.baesystems.ai.lr.dto.scheduling.SRECertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.scheduling.SREUncertifiedSuccessResponseDto;
import com.baesystems.ai.lr.enums.RuleEngineResponseStatus;
import com.baesystems.ai.lr.enums.SREServiceMethod;
import com.baesystems.ai.lr.enums.SREServiceQueryParameter;
import com.baesystems.ai.lr.exception.RuleEngineException;
import com.baesystems.ai.lr.utils.DateHelper;

@Component
public class SREHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(SREHandler.class);

    // Format expected by the Rule Engine
    private final SimpleDateFormat dateFormat = DateHelper.mastFormatter();

    private final String sreServiceUrl;

    public SREHandler()
    {
        sreServiceUrl = System.getProperty("sre.service.url");
    }

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Construct the full target URL for the Scheduling Rule Engine API including query parameters.
     *
     * @param sreMethod
     * @param creditedServiceId
     * @param harmonisationDate
     * @param completionDate
     * @return Target URL as {@link URI}
     */
    private URI buildUrl(final SREServiceMethod sreMethod, final Long creditedServiceId, final Date harmonisationDate, final Date completionDate)
    {
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(sreServiceUrl).path(sreMethod.getMethodName());

        if (creditedServiceId != null)
        {
            builder.queryParam(SREServiceQueryParameter.CREDITED_SERVICE_ID.getParameterName(), creditedServiceId.toString());
        }

        if (harmonisationDate != null)
        {
            builder.queryParam(SREServiceQueryParameter.ASSET_HARMONISATION_DATE.getParameterName(), this.dateFormat.format(harmonisationDate));
        }

        if (completionDate != null)
        {
            builder.queryParam(SREServiceQueryParameter.COMPLETION_DATE.getParameterName(), this.dateFormat.format(completionDate));
        }

        return builder.build().toUri();
    }

    /**
     * Call the Scheduling Rule Engine API.
     *
     * @param targetUrl
     * @return Successful {@link SREBaseResponseDto}. </br>
     *         Unsuccessful responses will throw a {@link RuleEngineException}.
     */
    private <T extends SREBaseResponseDto> T callSchedulingRuleEngine(final URI targetUrl, final Class<T> responseType)
    {
        LOG.debug(String.format("Calling Scheduling Rule Engine. Target URL: %s", targetUrl));

        try
        {
            final ResponseEntity<T> entity = this.restTemplate.getForEntity(targetUrl, responseType);
            if (!HttpStatus.OK.equals(entity.getStatusCode()) || RuleEngineResponseStatus.FAILURE.name().equals(entity.getBody().getStatus()))
            {
                final String failureMessage = entity.getBody().getMessage();
                throw new RuleEngineException(String.format(RULE_ENGINE_ERROR, "Scheduling", failureMessage));
            }

            final T responseBody = entity.getBody();

            LOG.debug(String.format("Scheduling Rule Engine returned response body: %s", responseBody));

            return responseBody;
        }
        catch (final ClassCastException classCastEx)
        {
            throw new RuleEngineException(String.format(RULE_ENGINE_INVALID_RESPONSE, "Scheduling"), classCastEx);
        }
        catch (final HttpClientErrorException exception)
        {
            throw new RuleEngineException(String.format(RULE_ENGINE_COMMUNICATION_FAILURE, "Scheduling"), exception);
        }
    }

    /**
     * Call the Scheduling Rule Engine API to get a list of the next services which need to be scheduled based on the
     * completion of another service and its harmonisation date. Expect a Certified Success Response. </br>
     * <p>
     * ../scheduling/nextServices?creditedServiceId=x&completionDate=y&assetHarmonisationDate=z
     *
     * @param creditedServiceId
     * @param harmonisationDate
     * @param completionDate
     * @return Response as {@link SRECertifiedSuccessResponseDto}
     */
    public SRECertifiedSuccessResponseDto getNextServices(final Long creditedServiceId, final Date harmonisationDate, final Date completionDate)
    {
        final URI targetUrl = this.buildUrl(SREServiceMethod.NEXT_SERVICES, creditedServiceId, harmonisationDate, completionDate);
        return this.callSchedulingRuleEngine(targetUrl, SRECertifiedSuccessResponseDto.class);
    }

    /**
     * Call the Scheduling Rule Engine API to get the scheduling dates for a single service based on the service ID and
     * harmonisation date. Expect an Uncertified Success Response. </br>
     * <p>
     * e.g. .../scheduling/manualEntryService?creditedServiceId=x&assetHarmonisationDate=z
     *
     * @param creditedServiceId
     * @param harmonisationDate
     * @return Response as {@link SREUncertifiedSuccessResponseDto}
     */
    public SREUncertifiedSuccessResponseDto getManualEntryService(final Long creditedServiceId, final Date harmonisationDate)
    {
        final URI targetUrl = this.buildUrl(SREServiceMethod.MANUAL_ENTRY_SERVICE, creditedServiceId, harmonisationDate, null);
        return this.callSchedulingRuleEngine(targetUrl, SREUncertifiedSuccessResponseDto.class);
    }
}
