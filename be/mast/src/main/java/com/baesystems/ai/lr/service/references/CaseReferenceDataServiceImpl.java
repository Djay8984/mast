package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.references.CaseReferenceDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "CaseReferenceDataService")
@Loggable(Loggable.DEBUG)
public class CaseReferenceDataServiceImpl implements CaseReferenceDataService
{

    @Autowired
    private CaseReferenceDataDao caseReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<ReferenceDataDto> getRiskAssessmentStatuses()
    {
        return this.caseReferenceDataDao.getRiskAssessmentStatuses();
    }

    @Override
    public ReferenceDataDto getRiskAssessmentStatus(final Long riskAssessmentStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto riskAssessmentStatus = this.caseReferenceDataDao.getRiskAssessmentStatus(riskAssessmentStatusId);
        this.serviceHelper.verifyModel(riskAssessmentStatus, riskAssessmentStatusId);

        return riskAssessmentStatus;
    }

    @Override
    public List<ReferenceDataDto> getPreEicInspectionStatuses()
    {
        return this.caseReferenceDataDao.getPreEicInspectionStatuses();
    }

    @Override
    public ReferenceDataDto getPreEicInspectionStatus(final Long preEicInspectionStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto preEicStatus = this.caseReferenceDataDao.getPreEicInspectionStatus(preEicInspectionStatusId);
        this.serviceHelper.verifyModel(preEicStatus, preEicInspectionStatusId);

        return preEicStatus;
    }

    @Override
    public List<ReferenceDataDto> getCaseTypes()
    {
        return this.caseReferenceDataDao.getCaseTypes();
    }

    @Override
    public ReferenceDataDto getCaseType(final Long caseTypeId) throws RecordNotFoundException
    {
        final ReferenceDataDto caseType = this.caseReferenceDataDao.getCaseType(caseTypeId);
        this.serviceHelper.verifyModel(caseType, caseTypeId);

        return caseType;
    }

    @Override
    public List<CaseStatusDto> getCaseStatuses()
    {
        return this.caseReferenceDataDao.getCaseStatuses();
    }

    @Override
    public CaseStatusDto getCaseStatus(final Long caseStatusId) throws RecordNotFoundException
    {
        final CaseStatusDto caseStatus = this.caseReferenceDataDao.getCaseStatus(caseStatusId);
        this.serviceHelper.verifyModel(caseStatus, caseStatusId);

        return caseStatus;
    }

    @Override
    public CaseReferenceDto getCaseReferenceData()
    {
        final CaseReferenceDto caseReferenceDto = new CaseReferenceDto();
        caseReferenceDto.setCaseStatuses(this.getCaseStatuses());

        return caseReferenceDto;
    }

    /**
     * Gets an Office Role, {@link OfficeRoleDto}, by its identifier.
     *
     * @param OfficeRoleId, Office Role identifier.
     * @return An Office Role, {@link OfficeRoleDto}.
     * @throws RecordNotFoundException, if Office Role is not found.
     */
    @Override
    public final ReferenceDataDto getOfficeRole(final Long id) throws RecordNotFoundException
    {
        final ReferenceDataDto officeRole = this.caseReferenceDataDao.getOfficeRole(id);
        this.serviceHelper.verifyModel(officeRole, id);

        return officeRole;
    }

    /**
     * Lists all Office Roles in the database.
     *
     * @return A list of all Office Roles {@Link OfficeRoleDto}
     **/
    @Override
    public final List<ReferenceDataDto> getOfficeRoles()
    {
        return this.caseReferenceDataDao.getOfficeRoles();
    }

    /**
     * Lists all milestones in the database.
     *
     * @return A list of all milestones {@Link MilestoneDto}
     */
    @Override
    public final List<MilestoneDto> getMilestones()
    {
        return this.caseReferenceDataDao.getMilestones();
    }

    @Override
    public final List<ReferenceDataDto> getMilestoneStatuses()
    {
        return this.caseReferenceDataDao.getMilestoneStatuses();
    }

    /**
     * List all the status available from a given case status so that transitions can be validated.
     */
    @Override
    public final List<Long> getValidNewCaseStatuses(final Long initialStatusId)
    {
        return this.caseReferenceDataDao.getValidNewCaseStatuses(initialStatusId);
    }

    @Override
    public final List<ReferenceDataDto> getMilestoneDueDateReferences()
    {
        return this.caseReferenceDataDao.getMilestoneDueDataReferences();
    }
}
