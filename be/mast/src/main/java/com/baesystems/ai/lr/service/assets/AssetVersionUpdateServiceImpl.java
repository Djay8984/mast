package com.baesystems.ai.lr.service.assets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.AssetOfficeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.assets.VersionedAssetPKGenerator;
import com.baesystems.ai.lr.dao.customers.PartyDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.AssetCustomerFunctionCustomerFunctionTypeDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedAssetPK;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.AssetCustomerFunctionDao;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.ComparisonUtils;
import com.baesystems.ai.lr.utils.ServiceUtils;

@Service(value = "AssetVersionUpdateService")
@SuppressWarnings("PMD.TooManyMethods")
public class AssetVersionUpdateServiceImpl implements AssetVersionUpdateService
{

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private AssetService assetService;

    @Autowired
    private ServiceUtils serviceUtils;

    @Autowired
    private AssetOfficeDao assetOfficeDao;

    @Autowired
    private PartyDao partyDao;

    @Autowired
    private AssetCustomerFunctionDao assetCustomerFunctionDao;

    @Autowired
    private VersionedAssetPKGenerator versionedAssetPKGenerator;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    private static final Long NO_VERSION_ID = -1L;

    /**
     * Check out the asset, this occurs as a pre-requisite when a user is going to make subsequent versionable changes
     * to the Asset This process creates a new version and designates that version as in draft state, this also locks
     * down the Asset so that other users may not check the Asset out
     *
     * @param assetId Asset ID
     * @param jobVersionId Job Version ID
     */
    @Override
    @Transactional
    public void checkOutAsset(final Long assetId, final Long jobVersionId) throws BadRequestException
    {
        final AssetMetaDto assetMetaDto = this.assetDao.getAssetMeta(assetId);
        final String userId = this.serviceUtils.getSecurityContextPrincipal();
        assetMetaDto.setCheckedOutBy(userId);
        assetMetaDto.setDraftVersionId(jobVersionId != null
                ? jobVersionId
                : this.upVersionAsset(assetId).getAssetVersionId());
        this.assetDao.updateAssetMeta(assetMetaDto);
    }

    /**
     * Once the User has finished making changes to the Asset, they check it back in. If no versionable changes have
     * been made the draft version is discarded. If a draft is to be discarded then the draftID is returned otherwise a
     * NO_VERSION Flag.
     *
     * @param assetId Asset ID
     * @param jobVersionId Job version ID
     */
    @Override
    @Transactional
    public Long checkInAsset(final Long assetId, final Long jobVersionId) throws BadRequestException
    {
        Long version = NO_VERSION_ID;
        final Long draftVersionId = this.assetDao.getDraftVersion(assetId);
        if (this.hasAVersionableChangeOccurred(assetId, draftVersionId))
        {
            this.promoteDraftVersion(assetId);
        }
        else if (jobVersionId == null)
        {
            version = this.discardDraftVersion(assetId);
        }
        return version;
    }

    /**
     * Discarding of an Asset version is going to be implemented as a hard delete.
     *
     * @param assetId Asset ID
     */
    private Long discardDraftVersion(final Long assetId)
    {
        final AssetMetaDto assetMeta = this.assetDao.getAssetMeta(assetId);
        final Long versionId = assetMeta.getDraftVersionId();

        assetMeta.setDraftVersionId(null);
        assetMeta.setCheckedOutBy(null);
        this.assetDao.updateAssetMeta(assetMeta);

        this.assetOfficeDao.hardDeleteAssetVersionedEntities(assetId, versionId);

        final List<AssetCustomerDO> assetCustomers = this.partyDao.getAssetCustomersForVersionedAsset(assetId, versionId);
        if (assetCustomers != null)
        {
            assetCustomers.stream().forEach(customer ->
            {
                final List<AssetCustomerFunctionCustomerFunctionTypeDO> functions = customer.getFunctions();
                if (functions != null)
                {
                    functions.stream().forEach(function -> this.assetCustomerFunctionDao.hardDelete(function.getId()));
                }
            });
            this.partyDao.hardDeleteAssetVersionedEntities(assetId, versionId);
        }
        return versionId;
    }

    /**
     * Hard delete the VersionedAsset. It is intended that this method is called outside any transaction calling
     * discardDraftVersion().
     *
     * @param assetId
     * @param versionId
     */
    @Override
    @Transactional
    public void hardDeleteDraftAsset(final Long assetId, final Long versionId)
    {
        if (versionId != null && !versionId.equals(NO_VERSION_ID))
        {
            this.assetDao.hardDeleteAssetVersionedEntities(assetId, versionId);
        }
    }

    /**
     * The promotion of a draft version to published is a simple update to the Asset(meta) entity
     *
     * @param assetId Asset ID
     */
    private void promoteDraftVersion(final Long assetId)
    {
        final AssetMetaDto assetMeta = this.assetDao.getAssetMeta(assetId);
        this.itemPersistDao.publishItems(assetId, assetMeta.getDraftVersionId());
        assetMeta.setPublishedVersionId(assetMeta.getDraftVersionId());
        assetMeta.setDraftVersionId(null);
        assetMeta.setCheckedOutBy(null);
        this.assetDao.updateAssetMeta(assetMeta);
    }

    /**
     * Compares the specified Asset version with that of the published Asset, to determine if a versionable change was
     * made
     *
     * @param assetId Asset ID
     * @param versionId The specific version
     * @return Whether or not a versionable change has been made
     */
    @Override
    @Transactional
    public boolean hasAVersionableChangeOccurred(final Long assetId, final Long versionId)
    {
        final String[] ignoredFields = {"customers", "assetVersionId", "parentPublishVersionId", "offices", "stalenessHash"};
        final Long publishedVersionId = this.assetDao.getPublishedVersion(assetId);
        final AssetLightDto specificVersion = this.assetDao.getAsset(assetId, versionId, AssetLightDto.class);
        final AssetLightDto publishedVersion = this.assetDao.getAsset(assetId, publishedVersionId, AssetLightDto.class);
        final Integer draftItemCount = this.itemDao.countItemsOfSpecificVersion(assetId, versionId);

        return draftItemCount > 0 || !ComparisonUtils.objectsMatch(specificVersion, publishedVersion, ignoredFields);
    }

    @Override
    @Transactional
    public List<AssetMetaDto> getCheckedOutAssets(final Boolean all)
    {
        String userName = null;
        if (!Boolean.TRUE.equals(all))
        {
            userName = this.serviceUtils.getSecurityContextPrincipal();
        }
        return this.assetDao.getCheckedOutAssets(userName);
    }

    @Override
    @Transactional
    public Long upVersionSingleItem(final Long assetId, final Long itemId, final Boolean linkFromDraftVersion)
    {
        final Long draftVersionId = this.itemPersistDao.upVersionItem(itemId, assetId).getAssetVersionId();
        this.itemPersistDao.upversionInverseRelationships(assetId, itemId, draftVersionId, linkFromDraftVersion);
        return draftVersionId;
    }

    /**
     * Main entry point for creating a new Asset Version, copied from the Published version. This is done either on
     * Check-Out, or when a Job is raised
     *
     * @param assetId Asset ID
     * @return The new asset version copy
     */
    private AssetLightDto upVersionAsset(final Long assetId)
    {
        final AssetLightDto publishedAssetCopy = this.assetDao.getAsset(assetId, null, AssetLightDto.class);

        final VersionedAssetPK newVersionPK = this.versionedAssetPKGenerator.getNextPK(assetId);
        final Long newVersionId = newVersionPK.getAssetVersionId();
        publishedAssetCopy.setParentPublishVersionId(publishedAssetCopy.getAssetVersionId());
        publishedAssetCopy.setAssetVersionId(newVersionId);
        this.prepareDirectBridgeEntities(publishedAssetCopy);
        return this.assetDao.updateAsset(publishedAssetCopy);
    }

    /**
     * Prepare existing bridge entities prior to upVersioning the Asset, such that new versions are created alongside
     * the new Asset version
     *
     * @param assetLightDto the Current Asset to be upVersioned, this should be the published version
     */
    private void prepareDirectBridgeEntities(final AssetLightDto assetLightDto)
    {
        CollectionUtils.nullSafeStream(assetLightDto.getCustomers()).forEach(customerLinkDto ->
        {
            customerLinkDto.setId(null);
            CollectionUtils.nullSafeStream(customerLinkDto.getFunctions()).forEach(function -> function.setId(null));
        });
        CollectionUtils.nullSafeStream(assetLightDto.getOffices()).forEach(officeLinkDto -> officeLinkDto.setId(null));
    }

    /**
     * Create a Job and take an associated asset version POST /job
     *
     * @param jobDto
     * @throws RecordNotFoundException
     */
    @Override
    @Transactional
    public void createJobVersionedAsset(final JobDto jobDto)
    {
        final Long assetId = jobDto.getAsset().getId();
        final AssetLightDto jobVersionedAsset = this.upVersionAsset(assetId);
        jobDto.getAsset().setVersion(jobVersionedAsset.getAssetVersionId());
    }

    /**
     * Updates an asset header, PUT /asset/x, This can only happen when the asset is checked out and therefore is in
     * draft version state
     *
     * @param assetLightDto
     * @return an AssetLightDto representation of the updated asset
     * @throws RecordNotFoundException
     */
    @Override
    @Transactional
    public AssetLightDto updateAsset(final AssetLightDto assetLightDto) throws RecordNotFoundException, BadRequestException, StaleObjectException
    {
        // Double check that the Draft Version is the version being updated.
        assetLightDto.setAssetVersionId(this.assetDao.getDraftVersion(assetLightDto.getId()));
        return this.assetService.updateAsset(assetLightDto);
    }
}
