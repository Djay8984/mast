package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.defects.RepairReferenceDataDao;
import com.baesystems.ai.lr.dto.references.MaterialDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.RepairReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "RepairReferenceDataService")
@Loggable(Loggable.DEBUG)
public class RepairReferenceDataServiceImpl implements RepairReferenceDataService
{
    @Autowired
    private RepairReferenceDataDao defectReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<MaterialDto> getMaterials()
    {
        return defectReferenceDataDao.getMaterials();
    }

    @Override
    public List<ReferenceDataDto> getMaterialTypes()
    {
        return defectReferenceDataDao.getMaterialTypes();
    }

    @Override
    public List<ReferenceDataDto> getRepairTypes()
    {
        return defectReferenceDataDao.getRepairTypes();
    }

    @Override
    public List<ReferenceDataDto> getRepairActions()
    {
        return defectReferenceDataDao.getRepairActions();
    }

    @Override
    public ReferenceDataDto getRepairAction(final Long repairActionId) throws RecordNotFoundException
    {
        final ReferenceDataDto repairAction = this.defectReferenceDataDao.getRepairAction(repairActionId);
        this.serviceHelper.verifyModel(repairAction, repairActionId);

        return repairAction;
    }

    @Override
    public RepairReferenceDataDto getReferenceData()
    {
        return defectReferenceDataDao.getReferenceData();
    }

}
