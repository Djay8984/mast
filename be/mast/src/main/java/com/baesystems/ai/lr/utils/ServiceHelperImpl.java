package com.baesystems.ai.lr.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

@Component
public class ServiceHelperImpl implements ServiceHelper
{
    private static final Integer DEFAULT_PAGE = 0;
    private static final Integer DEFAULT_SIZE = Integer.MAX_VALUE;

    /**
     * Creates page specification to be used on database queries.
     *
     * @param page Number of the page.
     * @param size Number of elements on the page.
     * @param sort Sort result.
     * @param order The direction to sort in ASC or DESC.
     * @return Page specification {@link Pageable}
     * @throws BadPageRequestException
     */
    @Override
    public Pageable createPageable(final Integer page, final Integer size, final String sort, final String order)
            throws BadPageRequestException
    {
        final Integer pageSize = this.setPageSize(size);
        final Integer pageNum = this.setPageNum(page);
        Pageable pageSpecification = null;

        if (sort == null)
        {
            pageSpecification = new PageRequest(pageNum, pageSize);
        }
        else
        {
            final Iterator<String> directions = Arrays.asList(order == null ? new String[0] : StringUtils.split(order, ",")).iterator();
            try
            {
                final List<Order> orders = Arrays.stream(StringUtils.split(sort, ","))
                        .map(property -> new Order(directions.hasNext() ? Direction.fromString(directions.next()) : Sort.DEFAULT_DIRECTION, property))
                        .collect(Collectors.toList());
                pageSpecification = new PageRequest(pageNum, pageSize, new Sort(orders));
            }
            catch (final IllegalArgumentException exception)
            { // if the direction is not ASC or DESC
                throw new BadPageRequestException(exception.getMessage(), exception);
            }
        }

        return pageSpecification;
    }

    private Integer setPageSize(final Integer sizeIn) throws BadPageRequestException
    {
        final Integer retVal = sizeIn == null ? DEFAULT_SIZE : sizeIn;
        if (retVal <= 0)
        {
            throw new BadPageRequestException(ExceptionMessagesUtils.INVALID_PAGE_SIZE);
        }
        return retVal;
    }

    private Integer setPageNum(final Integer page) throws BadPageRequestException
    {
        final Integer retVal = page == null ? DEFAULT_PAGE : page;
        if (retVal < 0)
        {
            throw new BadPageRequestException(ExceptionMessagesUtils.INVALID_PAGE_INDEX);
        }
        return retVal;
    }

    /**
     * Verifies if an entity exists (null) and creates an {@link RecordNotFoundException} with related error message.
     *
     * @param model
     * @param id
     * @throws RecordNotFoundException
     */
    @Override
    public void verifyModel(final Object model, final Long id) throws RecordNotFoundException
    {
        if (model == null)
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, id));
        }
    }
}
