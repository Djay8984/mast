package com.baesystems.ai.lr.service.jobs;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.jobs.FollowUpActionDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.FollowUpActionStatus;
import com.baesystems.ai.lr.enums.JobStatusType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.assets.AssetVersionUpdateService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "JobService")
@Loggable(Loggable.DEBUG)
public class JobServiceImpl implements JobService
{
    @Autowired
    private JobDao jobDao;

    @Autowired
    private WIPAssetNoteDao wipAssetNoteDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private FollowUpActionDao followUpActionDao;

    @Autowired
    private AssetVersionUpdateService assetVersionUpdateService;

    @Autowired
    private ServiceUtils serviceUtils;

    public static final List<Long> AN_SCOPE_CONFIRMED_APPLICABLE_STATUSES = Collections
            .unmodifiableList(Arrays.asList(CodicilStatusType.AN_OPEN.getValue()));

    public static final List<Long> AI_SCOPE_CONFIRMED_APPLICABLE_STATUSES = Collections
            .unmodifiableList(Arrays.asList(CodicilStatusType.AI_OPEN.getValue()));

    public static final List<Long> COC_SCOPE_CONFIRMED_APPLICABLE_STATUSES = Collections
            .unmodifiableList(Arrays.asList(CodicilStatusType.COC_OPEN.getValue()));

    public static final List<Long> SURVEY_SCOPE_CONFIRMED_APPLICABLE_STATUSES = Collections
            .unmodifiableList(Arrays.asList(ServiceCreditStatusType.NOT_STARTED.value(),
                    ServiceCreditStatusType.PART_HELD.value()));

    @Override
    public PageResource<JobDto> getJobs(final Integer page, final Integer size, final String sort, final String order,
            final Long employeeId, final Long teamId, final JobQueryDto query) throws BadPageRequestException
    {
        PageResource<JobDto> jobs = null;
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final JobQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(query, JobQueryDto.class);
        if (employeeId == null && teamId == null)
        {
            jobs = this.jobDao.getJobs(pageSpecification, cleanDto);
        }
        else if (employeeId != null)
        {
            jobs = this.jobDao.getJobsForEmployee(pageSpecification, employeeId, query);

        }
        else if (teamId != null)
        {
            // Team jobs
            jobs = this.jobDao.getJobsForTeam(pageSpecification, employeeId, cleanDto);
        }

        return jobs;
    }

    @Override
    public PageResource<JobDto> getJobsForAsset(final Integer page, final Integer size, final String sort, final String order,
            final Long assetId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.jobDao.getJobsForAsset(pageSpecification, assetId);
    }

    @Override
    public JobDto getJob(final Long jobId) throws RecordNotFoundException
    {
        final JobDto job = this.jobDao.getJob(jobId);
        this.serviceHelper.verifyModel(job, jobId);

        return job;
    }

    @Override
    public JobDto createJob(final JobDto jobDto) throws RecordNotFoundException
    {
        this.assetVersionUpdateService.createJobVersionedAsset(jobDto);
        return this.jobDao.createJob(jobDto);
    }

    @Override
    public JobDto updateJob(final Long jobId, final JobDto jobDto)
            throws RecordNotFoundException, BadPageRequestException, MastBusinessException
    {
        final JobDto existingJobDto = this.jobDao.findOneWithLock(jobId, JobDto.class, StaleCheckType.JOB);
        StaleCheckUtils.checkNotStale(existingJobDto, jobDto, StaleCheckType.JOB);

        JobDto updatedJobDto = null;

        if (jobDto != null && JobStatusType.CLOSED.getValue().equals(jobDto.getJobStatus().getId()))
        {
            updatedJobDto = this.closeJob(jobId, jobDto.getCompletedOn());
        }
        else
        {
            final JobDto originalJobDto = this.jobDao.getJob(jobId);
            this.serviceHelper.verifyModel(originalJobDto, jobId);

            // The job is updated before the Codicils and Surveys because if done after, any changes to the surveys
            // are over-written by the job update.
            updatedJobDto = this.jobDao.updateJob(jobDto);

            // Check original scopeConfirmed and act ONLY if transition FALSE -> TRUE, no action if NULL or TRUE.
            if (originalJobDto.getScopeConfirmed() != null && !originalJobDto.getScopeConfirmed() && jobDto.getScopeConfirmed())
            {
                this.wipAssetNoteDao.updateScopeConfirmed(jobId, jobDto.getScopeConfirmed(), AN_SCOPE_CONFIRMED_APPLICABLE_STATUSES);
                this.wipActionableItemDao.updateScopeConfirmed(jobId, jobDto.getScopeConfirmed(), AI_SCOPE_CONFIRMED_APPLICABLE_STATUSES);
                this.wipCoCDao.updateScopeConfirmed(jobId, jobDto.getScopeConfirmed(), COC_SCOPE_CONFIRMED_APPLICABLE_STATUSES);
                this.surveyDao.updateScopeConfirmed(jobId, jobDto.getScopeConfirmed(), SURVEY_SCOPE_CONFIRMED_APPLICABLE_STATUSES);
            }
        }
        return updatedJobDto;
    }

    @Override
    public PageResource<JobDto> getJobsAbstract(final Integer page, final Integer size, final String sort, final String order,
            final AbstractQueryDto query) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.jobDao.getJobs(pageSpecification, query);
    }

    private JobDto closeJob(final Long jobId, final Date completionDate) throws BadPageRequestException, MastBusinessException
    {
        JobDto updatedJobDto = null;
        final Pageable pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        final PageResource<FollowUpActionDto> followUpActionDtoPageResource = this.followUpActionDao.getFollowUpActionsForJob(pageSpecification,
                jobId);
        if (followUpActionDtoPageResource != null)
        {
            final long incompleteFUACount = followUpActionDtoPageResource.getContent().stream()
                    .filter(dto -> FollowUpActionStatus.INCOMPLETE.value().equals(dto.getFollowUpActionStatus().getId()))
                    .count();
            if (incompleteFUACount > 0)
            {
                throw new MastBusinessException(String.format(ExceptionMessagesUtils.CANNOT_CLOSE_JOB_OPEN_FUA, jobId));
            }
            else
            {
                this.jobDao.updateJobStatus(jobId, JobStatusType.CLOSED.getValue());
                this.jobDao.updateCompletedFields(jobId, this.serviceUtils.getSecurityContextPrincipal(), completionDate);

                updatedJobDto = this.jobDao.getJob(jobId);
            }
        }
        return updatedJobDto;
    }
}
