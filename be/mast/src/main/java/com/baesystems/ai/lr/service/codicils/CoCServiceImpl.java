package com.baesystems.ai.lr.service.codicils;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CoCService")
@Loggable(Loggable.DEBUG)
public class CoCServiceImpl implements CoCService
{
    @Autowired
    private CoCDao coCDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private SurveyService surveyService;

    @Override
    public CoCDto getCoC(final Long cocId, final boolean wip) throws RecordNotFoundException
    {
        final CoCDto coCDto = (wip ? this.wipCoCDao : this.coCDao).getCoC(cocId);
        this.serviceHelper.verifyModel(coCDto, cocId);
        return coCDto;
    }

    @Override
    public PageResource<CoCDto> getCoCsByAsset(final Integer page, final Integer size, final String sort, final String order,
            final Long assetId, final Long statusId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.coCDao.getCoCsForAsset(pageSpecification, assetId, statusId);
    }

    @Override
    public CoCDto saveCoC(final Long assetId, final CoCDto cocDto) throws RecordNotFoundException
    {
        final CoCDto result = this.coCDao.saveCoC(cocDto, assetId);
        this.serviceHelper.verifyModel(result, cocDto.getId());
        return result;
    }

    @Override
    public PageResource<CoCDto> getWIPCoCsForJob(final Integer page, final Integer size, final String sort, final String order,
            final Long jobId, final Long defectId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipCoCDao.getCoCsForJob(pageSpecification, jobId, defectId);
    }

    @Override
    public CoCDto updateCoC(final CoCDto coCDto) throws RecordNotFoundException, StaleObjectException
    {
        final CoCDto existingDto = this.coCDao.findOneWithLock(coCDto.getId(), CoCDto.class, StaleCheckType.COC);
        StaleCheckUtils.checkNotStale(existingDto, coCDto, StaleCheckType.COC);

        final CoCDto result = this.coCDao.updateCoC(coCDto);

        this.serviceHelper.verifyModel(result, coCDto.getId());
        return result;
    }

    @Override
    public CoCDto updateWIPCoC(final CoCDto coCDto) throws RecordNotFoundException, StaleObjectException
    {
        final CoCDto existingDto = this.wipCoCDao.findOneWithLock(coCDto.getId(), CoCDto.class, StaleCheckType.COC);
        StaleCheckUtils.checkNotStale(existingDto, coCDto, StaleCheckType.COC);

        final CoCDto result = this.wipCoCDao.updateCoC(coCDto);

        this.serviceHelper.verifyModel(result, coCDto.getId());

        final Long jobId = coCDto.getJob().getId();
        final Long wipCoCId = coCDto.getId();

        if (wipCoCHasChanged(coCDto, result))
        {
            this.surveyService.createAssetManagementSurveyForWipCoC(jobId, wipCoCId);
        }

        return result;
    }

    /*
     * Used to check if the CoC has actually changed during an update, comparison on 'approved' is omitted as every
     * update sets this to true, apparently.
     */
    private boolean wipCoCHasChanged(final CoCDto cocOne, final CoCDto cocTwo)
    {
        return !(Objects.equals(cocOne.getId(), cocTwo.getId())
                && Objects.equals(cocOne.getTemplate(), cocTwo.getTemplate())
                && Objects.equals(cocOne.getImposedDate(), cocTwo.getImposedDate())
                && Objects.equals(cocOne.getDueDate(), cocTwo.getDueDate())
                && Objects.equals(cocOne.getDescription(), cocTwo.getDescription())
                && Objects.equals(cocOne.getAffectedItems(), cocTwo.getAffectedItems())
                && Objects.equals(cocOne.getStatus(), cocTwo.getStatus())
                && Objects.equals(cocOne.getAssetItem(), cocTwo.getAssetItem())
                && Objects.equals(cocOne.getJob(), cocTwo.getJob())
                && Objects.equals(cocOne.getAsset(), cocTwo.getAsset())
                && Objects.equals(cocOne.getDefect(), cocTwo.getDefect())
                && Objects.equals(cocOne.getChildWIPs(), cocTwo.getChildWIPs())
                && Objects.equals(cocOne.getInheritedFlag(), cocTwo.getInheritedFlag())
                && Objects.equals(cocOne.getActionTaken(), cocTwo.getActionTaken())
                && Objects.equals(cocOne.getEmployee(), cocTwo.getEmployee())
                && Objects.equals(cocOne.getParent(), cocTwo.getParent())
                && Objects.equals(cocOne.getEditableBySurveyor(), cocTwo.getEditableBySurveyor())
                && Objects.equals(cocOne.getConfidentialityType(), cocTwo.getConfidentialityType())
                && Objects.equals(cocOne.getCategory(), cocTwo.getCategory())
                && Objects.equals(cocOne.getSurveyorGuidance(), cocTwo.getSurveyorGuidance())
                && Objects.equals(cocOne.getRequireApproval(), cocTwo.getRequireApproval())
                && Objects.equals(cocOne.getJobScopeConfirmed(), cocTwo.getJobScopeConfirmed())
                && Objects.equals(cocOne.getDateOfCrediting(), cocTwo.getDateOfCrediting())
                && Objects.equals(cocOne.getCreditedBy(), cocTwo.getCreditedBy()));
    }

    @Override
    public Boolean deleteCoC(final Long coCId) throws RecordNotFoundException
    {
        this.coCDao.deleteCoC(coCId);
        return !this.coCDao.exists(coCId);
    }

    @Override
    public Boolean deleteWIPCoC(final Long coCId) throws RecordNotFoundException
    {
        this.wipCoCDao.deleteCoC(coCId);
        return !this.wipCoCDao.exists(coCId);
    }

    /**
     * When we are creating a WIP CoC by bringing it into job scope from a non-wip CoC then we shouldn't be creating an
     * AMSC survey. We identify this situation based on whether or not the WIP CoC has a parent - and if this is the
     * case don't create a survey. We also should only be creating surveys for inherited/non-defect CoCs - so if it's
     * non-inherited and for a defect then don't create a survey then either.
     */
    @Override
    public CoCDto saveWIPCoC(final Long jobId, final CoCDto coCDto) throws RecordNotFoundException
    {
        final CoCDto result = this.wipCoCDao.saveCoC(jobId, coCDto);
        this.serviceHelper.verifyModel(result, coCDto.getId());
        final Boolean inheritedFlag = coCDto.getInheritedFlag();

        final boolean isNonInheritedDefectCoc = (inheritedFlag == null || !inheritedFlag.booleanValue()) && coCDto.getDefect() != null;
        if (coCDto.getParent() == null && !isNonInheritedDefectCoc)
        {
            this.surveyService.createAssetManagementSurveyForWipCoC(jobId, result.getId());
        }

        return result;
    }

    @Override
    public PageResource<CoCDto> getCoCsByQuery(final Long assetId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.coCDao.findAllForAsset(pageSpecification, assetId, cleanDto);
    }

    @Override
    public PageResource<CoCDto> getWIPCoCsByQuery(final Long jobId, final Integer page, final Integer size, final String sort, final String order,
            final CodicilDefectQueryDto queryDto)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.wipCoCDao.findAllForJob(pageSpecification, jobId, cleanDto);
    }

    @Override
    public PageResource<CoCDto> getDefectCocs(final Long assetId, final Long defectId, final Integer page, final Integer size, final String sort,
            final String order, final Long statusId) throws BadPageRequestException
    {
        final Pageable pageable = this.serviceHelper.createPageable(page, size, sort, order);
        return this.coCDao.getCoCsForDefect(pageable, assetId, defectId, statusId);
    }
}
