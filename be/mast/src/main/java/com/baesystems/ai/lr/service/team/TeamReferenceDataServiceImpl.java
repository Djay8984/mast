package com.baesystems.ai.lr.service.team;

import com.baesystems.ai.lr.dao.teams.TeamDao;
import com.baesystems.ai.lr.dao.teams.TeamEmployeeDao;
import com.baesystems.ai.lr.dao.teams.TeamOfficeDao;
import com.baesystems.ai.lr.dto.references.TeamDto;
import com.baesystems.ai.lr.dto.references.TeamEmployeeDto;
import com.baesystems.ai.lr.dto.references.TeamOfficeDto;
import com.jcabi.aspects.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "TeamService")
@Loggable(Loggable.DEBUG)
public class TeamReferenceDataServiceImpl implements TeamReferenceDataService
{
    @Autowired
    private TeamDao teamDao;

    @Autowired
    private TeamOfficeDao teamOfficeDao;

    @Autowired
    private TeamEmployeeDao teamEmployeeDao;

    @Override
    public List<TeamDto> getTeams()
    {
        return this.teamDao.findAll();
    }

    @Override
    public List<TeamEmployeeDto> getTeamEmployees()
    {
        return this.teamEmployeeDao.findAll();
    }

    @Override
    public List<TeamOfficeDto> getTeamOffices()
    {
        return this.teamOfficeDao.findAll();
    }
}
