package com.baesystems.ai.lr.service.tasks;

import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_ERROR;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_INVALID_RESPONSE;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.RULE_ENGINE_COMMUNICATION_FAILURE;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.baesystems.ai.lr.dto.tasks.TREBaseResponseDto;
import com.baesystems.ai.lr.dto.tasks.TREMultiServiceSuccessResponseDto;
import com.baesystems.ai.lr.dto.tasks.TRESingleServiceSuccessResponseDto;
import com.baesystems.ai.lr.enums.RuleEngineResponseStatus;
import com.baesystems.ai.lr.enums.TREServiceMethod;
import com.baesystems.ai.lr.enums.TREServiceQueryParameter;
import com.baesystems.ai.lr.exception.RuleEngineException;

@Component
public class TREHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(TREHandler.class);

    private final String treServiceUrl;

    public TREHandler()
    {
        treServiceUrl = System.getProperty("tre.service.url");
    }

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Construct the full target URL for the Task Rule Engine API including query parameters.
     *
     * @param treMethod
     * @param assetId
     * @param scheduledServiceId
     * @return Target URL as {@link URI}
     */
    private URI buildUrl(final TREServiceMethod treMethod, final Long assetId, final Long scheduledServiceId)
    {
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(treServiceUrl).path(treMethod.getMethodName());

        if (assetId != null)
        {
            builder.queryParam(TREServiceQueryParameter.ASSET_ID.getParameterName(), assetId.toString());
        }

        if (scheduledServiceId != null)
        {
            builder.queryParam(TREServiceQueryParameter.SERVICE_ID.getParameterName(), scheduledServiceId.toString());
        }

        return builder.build().toUri();
    }

    /**
     * Call the Task Rule Engine API.
     *
     * @param targetUrl
     * @return Successful {@link TREBaseResponseDto}. </br>
     *         Unsuccessful responses will throw a {@link RuleEngineException}.
     */
    private <T extends TREBaseResponseDto> T callTaskRuleEngine(final URI targetUrl, final Class<T> responseType)
    {
        LOG.debug(String.format("Calling Task Rule Engine. Target URL: %s", targetUrl));

        try
        {
            final ResponseEntity<T> entity = this.restTemplate.getForEntity(targetUrl, responseType);
            if (!HttpStatus.OK.equals(entity.getStatusCode()) || RuleEngineResponseStatus.FAILURE.name().equals(entity.getBody().getStatus()))
            {
                final String failureMessage = entity.getBody().getMessage();
                throw new RuleEngineException(String.format(RULE_ENGINE_ERROR, "Task", failureMessage));
            }

            final T responseBody = entity.getBody();

            LOG.debug(String.format("Task Rule Engine returned response body: %s", responseBody));

            return responseBody;
        }
        catch (final ClassCastException classCastEx)
        {
            throw new RuleEngineException(String.format(RULE_ENGINE_INVALID_RESPONSE, "Task"), classCastEx);
        }
        catch (final HttpClientErrorException exception)
        {
            throw new RuleEngineException(String.format(RULE_ENGINE_COMMUNICATION_FAILURE, "Task"), exception);
        }
    }

    /**
     * Call the Task Rule Engine API to get a list of the Tasks which need to be created based on the given asset and
     * service. Expect a Success Response. </br>
     * <p>
     * ../taskGeneration/tasksForAssetService?assetId=x&assetServiceId=y
     *
     * @param assetId
     * @param scheduledServiceId
     * @return Response as {@link TRESingleServiceSuccessResponseDto}
     */
    public TRESingleServiceSuccessResponseDto getTasksForService(final Long assetId, final Long scheduledServiceId)
    {
        final URI targetUrl = this.buildUrl(TREServiceMethod.TASKS_FOR_ASSET_SERVICE, assetId, scheduledServiceId);
        return this.callTaskRuleEngine(targetUrl, TRESingleServiceSuccessResponseDto.class);
    }

    /**
     * Call the Task Rule Engine API to get a list of the Tasks which need to be created based on the given asset.
     * Expect a Success Response. </br>
     * <p>
     * ../taskGeneration/tasksForAsset?assetId=x
     *
     * @param assetId
     * @return Response as {@link TREMultiServiceSuccessResponseDto}
     */
    public TREMultiServiceSuccessResponseDto getTasksForAsset(final Long assetId)
    {
        final URI targetUrl = this.buildUrl(TREServiceMethod.TASKS_FOR_ASSET, assetId, null);
        return this.callTaskRuleEngine(targetUrl, TREMultiServiceSuccessResponseDto.class);
    }
}
