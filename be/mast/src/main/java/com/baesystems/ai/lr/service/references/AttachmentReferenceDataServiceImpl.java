package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.attachments.AttachmentReferenceDataDao;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "AttachmentReferenceDataService")
@Loggable(Loggable.DEBUG)
public class AttachmentReferenceDataServiceImpl implements AttachmentReferenceDataService
{
    @Autowired
    private AttachmentReferenceDataDao attachmentReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<ReferenceDataDto> getAttachmentTypes()
    {
        return this.attachmentReferenceDataDao.getAttachmentTypes();
    }

    @Override
    public ReferenceDataDto getAttachmentType(final Long attachmentTypeId) throws RecordNotFoundException
    {
        final ReferenceDataDto type = this.attachmentReferenceDataDao.getAttachmentType(attachmentTypeId);
        this.serviceHelper.verifyModel(type, attachmentTypeId);
        return type;
    }

    @Override
    public List<ReferenceDataDto> getConfidentialityTypes()
    {
        return this.attachmentReferenceDataDao.getConfidentialityTypes();
    }

    @Override
    public ReferenceDataDto getConfidentialityType(final Long confidentialityTypeId) throws RecordNotFoundException
    {
        final ReferenceDataDto type = this.attachmentReferenceDataDao.getConfidentialityType(confidentialityTypeId);
        this.serviceHelper.verifyModel(type, confidentialityTypeId);
        return type;
    }
}
