package com.baesystems.ai.lr.service.tasks;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.utils.VersioningHelper;
import org.apache.commons.codec.binary.Base32;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.tasks.TREAssetItemDto;
import com.baesystems.ai.lr.dto.tasks.TREMultiServiceSuccessResponseDto;
import com.baesystems.ai.lr.dto.tasks.TREServiceDto;
import com.baesystems.ai.lr.dto.tasks.TRESingleServiceSuccessResponseDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.CreditStatus;
import com.baesystems.ai.lr.service.references.JobReferenceDataService;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@Component(value = "TaskHelper")
public class TaskHelper
{
    private static final Logger LOG = LoggerFactory.getLogger(TaskHelper.class);

    private static final int TASK_NUMBER_SUBSTRING_MAX_LENGTH = 6;

    private static Base32 base32encoder = new Base32();

    @Autowired
    private TREHandler treHandler;

    @Autowired
    private JobReferenceDataService jobReferenceDataService;

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private VersioningHelper versioningHelper;

    /**
     * Given an Asset ID and Scheduled Service ID, call the Task Rule Engine API to get a list of the Tasks which should
     * be generated.
     *
     * @param assetId
     * @param scheduledServiceId
     * @return List of {@link WorkItemLightDto}
     */
    public List<WorkItemLightDto> getTaskListForService(final Long assetId, final Long scheduledServiceId)
    {
        final TRESingleServiceSuccessResponseDto response = this.treHandler.getTasksForService(assetId, scheduledServiceId);

        return response.getService() == null ? null : this.processTasks(response.getService().getAssetItems(), scheduledServiceId);
    }

    /**
     * Given an Asset ID, call the Task Rule Engine API to get a list of the Tasks which should be generated.
     *
     * @param assetId
     * @return List of {@link WorkItemLightDto}
     */
    public List<WorkItemLightDto> getTaskListForAsset(final Long assetId)
    {
        final TREMultiServiceSuccessResponseDto response = this.treHandler.getTasksForAsset(assetId);

        return response.getServices() == null ? null : this.processTasks(response.getServices());
    }

    /**
     * Generate a list of Tasks using the response data from the Task Rule Engine Service.
     *
     * @param List of {@link TREServiceDto}
     * @return List of {@link WorkItemLightDto}
     */
    public List<WorkItemLightDto> processTasks(final List<TREServiceDto> treServices)
    {
        final List<WorkItemLightDto> tasks = new ArrayList<WorkItemLightDto>();

        for (final TREServiceDto treService : treServices)
        {
            tasks.addAll(this.processTasks(treService.getAssetItems(), treService.getServiceIdentifier()));
        }

        return tasks;
    }

    /**
     * Generate a list of Tasks using the response data from the Task Rule Engine Service.
     *
     * @param List of {@link TREAssetItemDto}
     * @param scheduledServiceId
     * @return List of {@link WorkItemLightDto}
     */
    public List<WorkItemLightDto> processTasks(final List<TREAssetItemDto> treAssetItems, final Long scheduledServiceId)
    {
        final List<WorkItemLightDto> tasks = new ArrayList<WorkItemLightDto>();

        for (final TREAssetItemDto treAssetItem : CollectionUtils.nullSafeCollection(treAssetItems))
        {
            if (verifyAssetItem(treAssetItem) && treAssetItem.getTasks() != null)
            {
                tasks.addAll(generateTasksForAssetItem(treAssetItem, scheduledServiceId));
            }
        }

        return tasks;
    }

    /**
     * Populate a list of Tasks (Work Items) based on the data held within the Asset Item object returned by the Task
     * Rule Engine. This Asset Item contains a list of codes, and each of those codes represents a Task that should be
     * associated with the Asset Item, the Scheduled Service, and the matching Work Item Action from reference data.
     *
     * @param treAssetItem
     * @param scheduledServiceId
     * @return List of {@link WorkItemLightDto}
     */
    private List<WorkItemLightDto> generateTasksForAssetItem(final TREAssetItemDto treAssetItem, final Long scheduledServiceId)
    {
        final List<WorkItemLightDto> tasks = new ArrayList<WorkItemLightDto>();

        for (final String treTaskCode : treAssetItem.getTasks())
        {
            final WorkItemActionDto workItemAction = this.findWorkItemActionFromReferenceCode(treTaskCode);

            //Rules provided a task code that is not in the Mast Reference Data
            if (workItemAction == null)
            {
                LOG.warn(String.format(ExceptionMessagesUtils.RULES_TASK_UNKNOWN_TASK_CODE, treTaskCode));
            }
            else
            {
                final Long assetItemId = treAssetItem.getAssetItemIdentifier().longValue();
                final String serviceCode = this.serviceDao.getServiceCodeFromScheduledService(scheduledServiceId);

                final WorkItemLightDto task = new WorkItemLightDto();
                task.setAssetItem(new LinkResource(assetItemId));
                task.setWorkItemAction(workItemAction);
                task.setScheduledService(new LinkResource(scheduledServiceId));
                task.setCreditStatus(new LinkResource(CreditStatus.UNCREDITED.getValue()));
                task.setTaskNumber(this.generateTaskNumber(assetItemId, workItemAction.getId()));
                task.setReferenceCode(treTaskCode);
                task.setServiceCode(serviceCode);
                tasks.add(task);
            }
        }
        return tasks;
    }

    private boolean verifyAssetItem(final TREAssetItemDto treAssetItem)
    {
        boolean canBeAdded = true;
        if (treAssetItem.getAssetItemIdentifier() != null)
        {
            final Long assetItemId = treAssetItem.getAssetItemIdentifier().longValue();
            if (!this.itemDao.itemExists(assetItemId))
            {
                canBeAdded = false;
                LOG.warn(String.format(ExceptionMessagesUtils.RULES_TASK_UNKNOWN_ASSET_ITEM, assetItemId));
            }
        }
        return canBeAdded;
    }

    private WorkItemActionDto findWorkItemActionFromReferenceCode(final String referenceCode)
    {
        return this.jobReferenceDataService.findWorkItemActionFromReferenceCode(referenceCode);
    }

    /**
     * Generate a Task Number in the following format: </br>
     * Item Type ID (from reference data) – 6 alphanumeric string – Task Type ID (reference data)</br>
     * e.g. 000001-BKL72R-000003</br>
     * Where the middle string is a 6 char base32 encoding of the Item ID using RFC-4648 base32 alphabet
     *
     * @param assetItemId
     * @param workItemActionId
     * @return Task Number string
     */
    public String generateTaskNumber(final Long assetItemId, final Long workItemActionId)
    {
        final StringBuilder itemTypeID = new StringBuilder("000000");
        StringBuilder base32AlphaNumeric = new StringBuilder("000000");
        final StringBuilder taskTypeId = new StringBuilder("000000");

        if (assetItemId != null)
        {
            generateItemTypeIDSubString(assetItemId, itemTypeID);

            final StringBuilder tempBuffer = new StringBuilder(
                    base32encoder.encodeAsString(assetItemId.toString().getBytes(Charset.defaultCharset())));
            String filtered = tempBuffer.toString().replace("=", "");
            if (filtered.length() > TASK_NUMBER_SUBSTRING_MAX_LENGTH)
            {
                filtered = filtered.substring(0, TASK_NUMBER_SUBSTRING_MAX_LENGTH);
            }
            base32AlphaNumeric = new StringBuilder(filtered);
        }

        if (workItemActionId != null)
        {
            taskTypeId.replace(TASK_NUMBER_SUBSTRING_MAX_LENGTH - workItemActionId.toString().length(), TASK_NUMBER_SUBSTRING_MAX_LENGTH,
                    workItemActionId.toString());
        }

        return itemTypeID.toString() + "-" + base32AlphaNumeric.toString() + "-" + taskTypeId.toString();
    }

    private void generateItemTypeIDSubString(final Long assetItemId, final StringBuilder itemTypeStringBuilder)
    {
        final ItemMetaDto itemMetaDto = this.itemDao.getItemMeta(assetItemId);

        final LazyItemDto itemDto = itemDao.getItem(this.versioningHelper.getVersionedItemPK(itemMetaDto.getId(), itemMetaDto.getAsset().getId()));

        if (itemDto.getId() != null)
        {
            final LinkResource itemType = itemDto.getItemType();
            if (itemType != null)
            {
                final Long itemTypeId = itemType.getId();
                itemTypeStringBuilder.replace(TASK_NUMBER_SUBSTRING_MAX_LENGTH - itemTypeId.toString().length(), TASK_NUMBER_SUBSTRING_MAX_LENGTH,
                        itemTypeId.toString());
            }
        }
    }
}
