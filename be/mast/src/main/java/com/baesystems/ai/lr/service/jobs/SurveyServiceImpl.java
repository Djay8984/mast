package com.baesystems.ai.lr.service.jobs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.enums.SurveyType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.employees.LrEmployeeService;
import com.baesystems.ai.lr.service.references.RepairReferenceDataService;
import com.baesystems.ai.lr.service.tasks.WorkItemService;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "SurveyService")
@SuppressWarnings("PMD.TooManyMethods")
@Loggable(Loggable.DEBUG)
public class SurveyServiceImpl implements SurveyService
{
    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private LrEmployeeService lrEmployeeService;

    @Autowired
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Autowired
    private RepairReferenceDataService repairReferenceDataService;

    @Autowired
    private WorkItemService workItemService;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private WIPCoCDao wipCocDao;

    @Autowired
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Autowired
    private ServiceUtils serviceUtils;

    private static final String REPAIR_CODE_SUFFIX = "RPS";

    /**
     * Get all of the Services that have been associated with the given Job. These are returned as Surveys, which can
     * represent scheduled services and one off services.
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param jobId
     * @return Paginated list of {@link SurveyDto}
     * @throws RecordNotFoundException
     * @throws BadPageRequestException
     */
    @Override
    public final PageResource<SurveyDto> getSurveys(final Integer page, final Integer size, final String sort, final String order,
            final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.surveyDao.getSurveys(pageSpecification, jobId);
    }

    @Override
    public SurveyDto updateSurvey(final Long surveyId, final SurveyDto surveyDto) throws RecordNotFoundException, StaleObjectException
    {
        this.serviceHelper.verifyModel(surveyDto, surveyId);

        final SurveyDto existingSurvey = this.surveyDao.findOneWithLock(surveyDto.getId(), SurveyDto.class, StaleCheckType.SURVEY);

        StaleCheckUtils.checkNotStale(existingSurvey, surveyDto, StaleCheckType.SURVEY);

        surveyDto.setApproved(false);

        return this.surveyDao.updateSurvey(surveyDto);
    }

    @Override
    public SurveyListDto updateSurveys(final SurveyListDto surveys) throws RecordNotFoundException, BadPageRequestException, StaleObjectException
    {
        final List<SurveyDto> updatedSurveyList = new ArrayList<SurveyDto>();
        for (final SurveyDto survey : surveys.getSurveys())
        {
            if (survey.getId() == null)
            {
                // create a new survey!
                final SurveyDto createdSurvey = createSurvey(survey.getJob().getId(), survey);
                workItemService.generateWIPTasks(createdSurvey);
                updatedSurveyList.add(createdSurvey);
            }
            else
            {
                // update a survey!
                updatedSurveyList.add(updateSurvey(survey.getId(), survey));
            }
        }

        final SurveyListDto updatedSurveys = new SurveyListDto();
        updatedSurveys.setSurveys(updatedSurveyList);

        return updatedSurveys;
    }

    @Override
    public Boolean deleteSurvey(final Long surveyId)
    {
        this.surveyDao.deleteSurvey(surveyId);
        return !this.surveyDao.surveyExists(surveyId);
    }

    /**
     * Used to populate the details of a new Survey that will be automatically generated.
     *
     * @param jobId
     * @param serviceCatalogue
     * @param surveyName
     * @param lrEmployee
     * @return New {@link SurveyDto} with details completed
     */
    private SurveyDto populateNewSurvey(final Long jobId, final ServiceCatalogueDto serviceCatalogue, final String surveyName,
            final LrEmployeeDto lrEmployee)
    {
        final SurveyDto surveyDto = new SurveyDto();

        surveyDto.setId(null);
        surveyDto.setAutoGenerated(Boolean.TRUE);
        surveyDto.setJob(new LinkResource(jobId));
        surveyDto.setName(surveyName);
        surveyDto.setScheduledService(null);
        surveyDto.setServiceCatalogue(new LinkResource(serviceCatalogue.getId()));
        surveyDto.setSurveyStatus(
                new LinkResource(this.serviceReferenceDataDao.getServiceCreditStatus(ServiceCreditStatusType.COMPLETE.value()).getId()));
        surveyDto.setDateOfCrediting(new Date());
        surveyDto.setCreditedBy(new LinkResource(lrEmployee.getId()));
        surveyDto.setScheduleDatesUpdated(Boolean.FALSE);
        surveyDto.setApproved(Boolean.TRUE);
        surveyDto.setJobScopeConfirmed(Boolean.TRUE);

        return surveyDto;
    }

    @Override
    public void createWIPCoCSurveyForPermanentRepairs(final Long jobId, final RepairDto repairDto)
            throws RecordNotFoundException
    {
        final ReferenceDataDto repairAction = this.repairReferenceDataService.getRepairAction(repairDto.getRepairAction().getId());

        // Only create a survey if the repair is permanent.
        if (RepairActionType.PERMANENT.value().equals(repairAction.getId()))
        {
            createWIPCocSurvey(jobId, repairDto.getCodicil().getId());
        }
    }

    private void createWIPCocSurvey(final Long jobId, final Long wipCoCId) throws RecordNotFoundException
    {
        final LrEmployeeDto lrEmployee = this.lrEmployeeService.getLrEmployeeByFullName(this.serviceUtils.getSecurityContextPrincipal());
        final CoCDto wipCoC = this.wipCocDao.getCoC(wipCoCId);

        final ReferenceDataDto codicilCategory = this.codicilReferenceDataDao.getCodicilCategory(wipCoC.getCategory().getId());
        final String lookUpCode = codicilCategory.getName().toUpperCase(Locale.getDefault()).charAt(0) + REPAIR_CODE_SUFFIX;
        final ServiceCatalogueDto serviceDto = this.serviceDao.getServiceCatalogueByCodeAndSurveyType(lookUpCode, SurveyType.REPAIR.name());
        
        if (serviceDto != null)
        {
            final SurveyDto surveyDto = populateNewSurvey(jobId, serviceDto, serviceDto.getName(), lrEmployee);
            surveyDto.setJobScopeConfirmed(wipCoC.getJobScopeConfirmed());
            this.surveyDao.createSurvey(surveyDto);
        }
    }

    /**
     * Create a new Survey.
     *
     * @param jobId
     * @param surveyDto
     * @return New {@link SurveyDto}
     */
    @Override
    public SurveyDto createSurvey(final Long jobId, final SurveyDto surveyDto)
    {
        if (surveyDto.getJob() == null)
        {
            surveyDto.setJob(new LinkResource());
        }
        surveyDto.getJob().setId(jobId);

        // The approved flag is used to indicate that a final report (FSR) that includes these items was submitted.
        // So for a new WIP survey, this will always be false.
        surveyDto.setApproved(Boolean.FALSE);

        return this.surveyDao.createSurvey(surveyDto);
    }

    @Override
    public void createAssetManagementSurveyForWipCoC(final Long jobId, final Long wipCoCId) throws RecordNotFoundException
    {
        final ServiceCatalogueDto serviceDto = this.serviceDao.getUniqueServiceCatalogueByCode(ServiceCatalogueCodes.AMSC);

        if (serviceDto != null)
        {
            final LrEmployeeDto lrEmployee = this.lrEmployeeService.getLrEmployeeByFullName(this.serviceUtils.getSecurityContextPrincipal());
            final CoCDto wipCoC = this.wipCocDao.getCoC(wipCoCId);

            final SurveyDto surveyDto = populateNewSurvey(jobId, serviceDto, wipCoC.getTitle(), lrEmployee);
            surveyDto.setJobScopeConfirmed(wipCoC.getJobScopeConfirmed());
            this.surveyDao.createSurvey(surveyDto);
        }
    }

    @Override
    public SurveyDto getSurvey(final Long surveyId) throws RecordNotFoundException
    {
        final SurveyDto surveyDto = this.surveyDao.getSurvey(surveyId);
        this.serviceHelper.verifyModel(surveyDto, surveyId);

        return surveyDto;
    }
}
