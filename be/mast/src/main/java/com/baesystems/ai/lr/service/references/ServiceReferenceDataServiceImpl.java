package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dto.references.ProductGroupDto;
import com.baesystems.ai.lr.dto.references.ProductTypeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.SchedulingTypeDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import com.baesystems.ai.lr.dto.references.ServiceGroupDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "ServiceReferenceDataService")
@Loggable(Loggable.DEBUG)
public class ServiceReferenceDataServiceImpl implements ServiceReferenceDataService
{
    @Autowired
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<ReferenceDataDto> getServiceStatuses()
    {
        return this.serviceReferenceDataDao.getServiceStatuses();
    }

    @Override
    public List<ServiceCatalogueDto> getServiceCatalogues()
    {
        return this.serviceReferenceDataDao.getServiceCatalogues();
    }

    @Override
    public ReferenceDataDto getServiceStatus(final Long serviceStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto serviceStatus = this.serviceReferenceDataDao.getServiceStatus(serviceStatusId);
        this.serviceHelper.verifyModel(serviceStatus, serviceStatusId);

        return serviceStatus;
    }

    @Override
    public List<ReferenceDataDto> getServiceTypes()
    {
        return this.serviceReferenceDataDao.getServiceTypes();
    }

    @Override
    public List<ProductTypeDto> getProductTypes()
    {
        return this.serviceReferenceDataDao.getProductTypes();
    }

    @Override
    public List<ProductGroupDto> getProductGroups()
    {
        return this.serviceReferenceDataDao.getProductGroups();
    }

    @Override
    public List<ReferenceDataDto> getSchedulingRegimes()
    {
        return this.serviceReferenceDataDao.getSchedulingRegimes();
    }

    @Override
    public List<SchedulingTypeDto> getSchedulingTypes()
    {
        return this.serviceReferenceDataDao.getSchedulingTypes();
    }

    @Override
    public List<ServiceGroupDto> getServiceGroups()
    {
        return this.serviceReferenceDataDao.getServiceGroups();
    }

    @Override
    public List<ReferenceDataDto> getServiceRulesets()
    {
        return this.serviceReferenceDataDao.getServiceRulesets();
    }

    @Override
    public List<ReferenceDataDto> getAssignedSchedulingTypes()
    {
        return this.serviceReferenceDataDao.getAssignedSchedulingTypes();
    }

    @Override
    public List<ReferenceDataDto> getAssociatedSchedulingTypes()
    {
        return this.serviceReferenceDataDao.getAssociatedSchedulingTypes();
    }

    @Override
    public List<ReferenceDataDto> getHarmonisationTypes()
    {
        return this.serviceReferenceDataDao.getHarmonisationTypes();
    }

    @Override
    public List<ReferenceDataDto> getSchedulingDueTypes()
    {
        return this.serviceReferenceDataDao.getSchedulingDueTypes();
    }

    @Override
    public List<ServiceCreditStatusDto> getServiceCreditStatuses()
    {
        return this.serviceReferenceDataDao.getServiceCreditStatuses();
    }
}
