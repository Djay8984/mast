package com.baesystems.ai.lr.service.defects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.defects.DeficiencyDao;
import com.baesystems.ai.lr.dao.defects.WIPDeficiencyDao;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "DeficiencyService")
@Loggable(Loggable.DEBUG)
public class DeficiencyServiceImpl implements DeficiencyService
{
    @Autowired
    private DeficiencyDao deficiencyDao;

    @Autowired
    private WIPDeficiencyDao wipDeficiencyDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public PageResource<DeficiencyDto> getDeficienciesByQuery(final Long assetId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.deficiencyDao.findAllForAsset(pageSpecification, assetId, cleanDto);
    }

    @Override
    public PageResource<DeficiencyDto> getWIPDeficienciesByQuery(final Long jobId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.wipDeficiencyDao.findAllForJob(pageSpecification, jobId, cleanDto);
    }
}
