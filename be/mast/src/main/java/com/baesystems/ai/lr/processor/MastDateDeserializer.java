package com.baesystems.ai.lr.processor;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import com.baesystems.ai.lr.utils.DateHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class MastDateDeserializer extends JsonDeserializer<Date>
{
    @Override
    @SuppressWarnings("PMD.PreserveStackTrace")
    public Date deserialize(final JsonParser jasonParser, final DeserializationContext ctxt) throws IOException, JsonProcessingException
    {
        Date date = null;
        final ObjectCodec objectCodec = jasonParser.getCodec();
        final JsonNode node = objectCodec.readTree(jasonParser);
        final String stringDate = node.asText();
        try
        {
            date = DateHelper.mastFormatter().parse(stringDate);
        }
        catch (final ParseException exception)
        { // can't throw a parseException because the super implementation doesn't so re-throw as JsonParseException
            throw new JsonParseException("Error deserializing date", null, exception);
        }

        return date;
    }
}
