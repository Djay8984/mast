package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.jobs.CertificateReferenceDataDao;
import com.baesystems.ai.lr.dao.references.jobs.JobReferenceDataDao;
import com.baesystems.ai.lr.dto.references.AllowedWorkItemAttributeValueDto;
import com.baesystems.ai.lr.dto.references.CertificateTypeDto;
import com.baesystems.ai.lr.dto.references.ClassRecommendationDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "JobReferenceDataService")
@Loggable(Loggable.DEBUG)
public class JobReferenceDataServiceImpl implements JobReferenceDataService
{
    @Autowired
    private JobReferenceDataDao jobReferenceDataDao;

    @Autowired
    private CertificateReferenceDataDao certificateReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<CertificateTypeDto> getCertificateTypes()
    {
        return this.certificateReferenceDataDao.getCertificateTypes();
    }

    @Override
    public List<ReferenceDataDto> getCertificateStatuses()
    {
        return this.certificateReferenceDataDao.getCertificateStatuses();
    }

    @Override
    public List<ReferenceDataDto> getCertificateActions()
    {
        return this.certificateReferenceDataDao.getCertificateActions();
    }

    @Override
    public CertificateTypeDto getCertificateType(final Long certificateTypeId) throws RecordNotFoundException
    {
        final CertificateTypeDto certificateType = this.certificateReferenceDataDao.getCertificateType(certificateTypeId);
        this.serviceHelper.verifyModel(certificateType, certificateTypeId);
        return certificateType;
    }

    @Override
    public ReferenceDataDto getCertificateStatus(final Long certificateStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto certificateStatus = this.certificateReferenceDataDao.getCertificateStatus(certificateStatusId);
        this.serviceHelper.verifyModel(certificateStatus, certificateStatusId);
        return certificateStatus;
    }

    @Override
    public ReferenceDataDto getCertificateAction(final Long certificateActionId) throws RecordNotFoundException
    {
        final ReferenceDataDto certificateAction = this.certificateReferenceDataDao.getCertificateAction(certificateActionId);
        this.serviceHelper.verifyModel(certificateAction, certificateActionId);
        return certificateAction;
    }

    @Override
    public List<ReferenceDataDto> getJobCategories()
    {
        return this.jobReferenceDataDao.getJobCategories();
    }

    @Override
    public ReferenceDataDto getJobCategory(final Long categoryId) throws RecordNotFoundException
    {
        final ReferenceDataDto jobCategory = this.jobReferenceDataDao.getJobCategory(categoryId);
        this.serviceHelper.verifyModel(jobCategory, categoryId);
        return jobCategory;
    }

    @Override
    public List<ReferenceDataDto> getJobStatuses()
    {
        return this.jobReferenceDataDao.getJobStatuses();
    }

    @Override
    public List<ReferenceDataDto> getCreditStatuses()
    {
        return this.jobReferenceDataDao.getCreditStatuses();
    }

    @Override
    public List<ReferenceDataDto> getEndorsementTypes()
    {
        return this.jobReferenceDataDao.getEndorsementTypes();
    }

    @Override
    public List<ReferenceDataDto> getWorkItemTypes()
    {
        return this.jobReferenceDataDao.getWorkItemTypes();
    }

    @Override
    public List<WorkItemActionDto> getWorkItemActions()
    {
        return this.jobReferenceDataDao.getWorkItemActions();
    }

    @Override
    public List<ReferenceDataDto> getResolutionStatuses()
    {
        return this.jobReferenceDataDao.getResolutionStatuses();
    }

    @Override
    public List<ReferenceDataDto> getReportTypes()
    {
        return this.jobReferenceDataDao.getReportTypes();
    }

    @Override
    public List<ClassRecommendationDto> getClassRecommendations()
    {
        return this.jobReferenceDataDao.getClassRecommendations();
    }

    @Override
    public List<AllowedWorkItemAttributeValueDto> getAllowedWorkItemAttributeValues()
    {
        return this.jobReferenceDataDao.getAllowedWorkItemAttributeValues();
    }

    @Override
    public WorkItemActionDto findWorkItemActionFromReferenceCode(final String referenceCode)
    {
        return this.jobReferenceDataDao.findWorkItemActionFromReferenceCode(referenceCode);
    }
}
