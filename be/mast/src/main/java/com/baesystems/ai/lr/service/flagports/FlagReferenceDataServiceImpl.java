package com.baesystems.ai.lr.service.flagports;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.flagports.FlagReferenceDataDao;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FlagStateDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "FlagService")
@Loggable(Loggable.DEBUG)
public class FlagReferenceDataServiceImpl implements FlagReferenceDataService
{
    @Autowired
    private FlagReferenceDataDao flagDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final FlagStateDto getFlagState(final Long flagStateId) throws RecordNotFoundException
    {
        final FlagStateDto flagState = this.flagDao.getFlagState(flagStateId);
        this.serviceHelper.verifyModel(flagState, flagStateId);

        return flagState;
    }

    @Override
    public final PageResource<FlagStateDto> getFlagStates(final Integer page, final Integer size, final String sort, final String order,
            final String searchString)
            throws BadRequestException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);

        PageResource<FlagStateDto> result = null;

        if (searchString == null)
        {
            result = this.flagDao.findAll(pageSpecification);
        }
        else
        {
            final String search = QueryDtoUtils.escapeSpecialCharsAndConvertWildcards(searchString);
            result = this.flagDao.findAll(pageSpecification, search);
        }

        return result;
    }

    @Override
    public final List<FlagStateDto> getFlags()
    {
        return this.flagDao.getFlags();
    }
}
