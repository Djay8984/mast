package com.baesystems.ai.lr.service.codicils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.AssetLifecycleStatus;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "AssetNoteService")
@Loggable(Loggable.DEBUG)
public class AssetNoteServiceImpl implements AssetNoteService
{
    public static final Logger LOG = LoggerFactory.getLogger(AssetNoteServiceImpl.class);

    @Autowired
    private AssetNoteDao assetNoteDao;

    @Autowired
    private WIPAssetNoteDao wipAssetNoteDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public AssetNoteDto getAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        final AssetNoteDto assetNoteDto = this.assetNoteDao.getAssetNote(assetNoteId);
        this.serviceHelper.verifyModel(assetNoteDto, assetNoteId);
        return assetNoteDto;
    }

    @Override
    public AssetNoteDto getWIPAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        final AssetNoteDto assetNoteDto = this.wipAssetNoteDao.getAssetNote(assetNoteId);
        this.serviceHelper.verifyModel(assetNoteDto, assetNoteId);
        return assetNoteDto;
    }

    @Override
    public PageResource<AssetNoteDto> getAssetNotesByAsset(final Integer page, final Integer size, final String sort, final String order,
            final Long assetId, final Long statusId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.assetNoteDao.getAssetNotesByAsset(pageSpecification, assetId, statusId);
    }

    @Override
    public AssetNoteDto saveAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException
    {
        final AssetNoteDto result = this.assetNoteDao.saveAssetNote(assetNoteDto);
        this.serviceHelper.verifyModel(result, assetNoteDto.getId());
        return result;
    }

    @Override
    public AssetNoteDto saveWIPAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException
    {
        final AssetNoteDto result = this.wipAssetNoteDao.saveAssetNote(assetNoteDto);
        this.serviceHelper.verifyModel(result, assetNoteDto.getId());
        return result;
    }

    @Override
    public PageResource<AssetNoteDto> getWIPAssetNotesForJob(final Integer page, final Integer size, final String sort, final String order,
            final Long jobId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipAssetNoteDao.getAssetNotesForJob(pageSpecification, jobId);
    }

    @Override
    public AssetNoteDto updateAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException, StaleObjectException
    {
        final AssetNoteDto existingAssetNoteDto = this.assetNoteDao.findOneWithLock(assetNoteDto.getId(), AssetNoteDto.class,
                StaleCheckType.ASSET_NOTE);
        StaleCheckUtils.checkNotStale(existingAssetNoteDto, assetNoteDto, StaleCheckType.ASSET_NOTE);

        final AssetNoteDto result = this.assetNoteDao.updateAssetNote(assetNoteDto);
        this.serviceHelper.verifyModel(result, assetNoteDto.getId());

        return assetNoteDto;
    }

    @Override
    public AssetNoteDto updateWIPAssetNote(final AssetNoteDto assetNoteDto) throws RecordNotFoundException, StaleObjectException
    {
        final AssetNoteDto existingAssetNoteDto = this.wipAssetNoteDao.findOneWithLock(assetNoteDto.getId(), AssetNoteDto.class,
                StaleCheckType.ASSET_NOTE);
        StaleCheckUtils.checkNotStale(existingAssetNoteDto, assetNoteDto, StaleCheckType.ASSET_NOTE);

        final AssetNoteDto result = this.wipAssetNoteDao.updateAssetNote(assetNoteDto);
        this.serviceHelper.verifyModel(result, assetNoteDto.getId());
        return assetNoteDto;
    }

    @Override
    public Boolean deleteAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        this.assetNoteDao.deleteAssetNote(assetNoteId);
        return !this.assetNoteDao.exists(assetNoteId);
    }

    @Override
    public Boolean deleteWIPAssetNote(final Long assetNoteId) throws RecordNotFoundException
    {
        this.wipAssetNoteDao.deleteAssetNote(assetNoteId);
        return !this.wipAssetNoteDao.exists(assetNoteId);
    }

    @Override
    public TemplateAssetCountDto countAssetsWithAssetNotesForTemplate(final List<Long> templateIds)
    {
        return this.assetNoteDao.countAssetsWithAssetNotesForTemplate(templateIds, AssetLifecycleStatus.getStatusesForTemplateCount());
    }

    @Override
    public PageResource<AssetNoteDto> getAssetNotesByQuery(final Long assetId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.assetNoteDao.findAllForAsset(pageSpecification, assetId, cleanDto);
    }

    @Override
    public PageResource<AssetNoteDto> getWIPAssetNotesByQuery(final Long jobId, final Integer page, final Integer size, final String sort,
            final String order,
            final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.wipAssetNoteDao.findAllForJob(pageSpecification, jobId, cleanDto);
    }
}
