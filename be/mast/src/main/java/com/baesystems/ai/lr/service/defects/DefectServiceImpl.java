package com.baesystems.ai.lr.service.defects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "DefectService")
@Loggable(Loggable.DEBUG)
public class DefectServiceImpl implements DefectService
{
    @Autowired
    private DefectDao defectDao;

    @Autowired
    private WIPDefectDao wipDefectDao;

    @Autowired
    private CoCDao coCDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private ActionableItemDao actionableItemDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private RepairDao repairDao;

    @Autowired
    private WIPRepairDao wipRepairDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public DefectDto getDefect(final Long defectId) throws RecordNotFoundException
    {
        final DefectDto defect = this.defectDao.getDefect(defectId);
        this.serviceHelper.verifyModel(defect, defectId);
        setRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    @Override
    public DefectDto getWIPDefect(final Long defectId) throws RecordNotFoundException
    {
        final DefectDto defect = this.wipDefectDao.getDefect(defectId);
        this.serviceHelper.verifyModel(defect, defectId);
        setWIPRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    @Override
    public DefectLightDto getDefectLight(final Long defectId) throws RecordNotFoundException
    {
        final DefectLightDto defect = this.defectDao.getDefectLight(defectId);
        this.serviceHelper.verifyModel(defect, defectId);
        setRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    @Override
    public DefectDto createDefect(final DefectDto defectDto) throws RecordNotFoundException
    {
        final DefectDto defect = this.defectDao.createDefect(defectDto);
        setRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    @Override
    public DefectDto createWIPDefect(final DefectDto defectDto) throws RecordNotFoundException
    {
        final DefectDto defect = this.wipDefectDao.createDefect(defectDto);
        setWIPRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    @Override
    public DefectDto updateDefect(final Long defectId, final DefectDto defectDto) throws RecordNotFoundException
    {
        this.serviceHelper.verifyModel(this.defectDao.getDefect(defectId), defectId);
        final DefectDto defect = this.defectDao.updateDefect(defectDto);
        setRepairAndCourseOfActionCounts(defect);
        return defect;
    }

    /**
     * Updates the DefectDO corresponding to the specified DefectDto
     *
     * @param defectDto used to update the corresponding DefectDO
     * @param wip TRUE if the specified DefectDto is WIP
     * @return a DefectDto corresponding to the updated DefectDO
     * @throws RecordNotFoundException if the DefectDO corresponding to the specified DefectDto could not be found.
     * @throws StaleObjectException
     */
    @Override
    public DefectDto updateDefect(final DefectDto defectDto, final boolean wip) throws RecordNotFoundException, StaleObjectException
    {
        final DefectDto existingDefectDto = this.wipDefectDao.findOneWithLock(defectDto.getId(), DefectDto.class, StaleCheckType.DEFECT);
        StaleCheckUtils.checkNotStale(existingDefectDto, defectDto, StaleCheckType.DEFECT);


        final DefectDto result = wip ? this.wipDefectDao.updateDefect(defectDto) : this.defectDao.updateDefect(defectDto);
        this.serviceHelper.verifyModel(result, defectDto.getId());
        return result;
    }

    @Override
    public Boolean closeDefect(final Long defectId) throws BadRequestException
    {
        return this.defectDao.closeDefect(defectId);
    }

    @Override
    public Boolean closeWIPDefect(final Long wipDefectId) throws BadRequestException
    {
        return this.wipDefectDao.closeDefect(wipDefectId);
    }

    /**
     * Delete the specified Defect and its associated CoCs, repairs and attachments Story 10.46, AC 3
     *
     * @param defectId the id of the defect to be deleted
     * @return true if defect and its associated entities are deleted.
     * @throws RecordNotFoundException if the specified defect Id could not be found.
     * @throws MastBusinessException if the defect or one of its associated entities could not be deleted.
     */
    @Override
    public Boolean deleteWIPDefect(final Long defectId) throws RecordNotFoundException, MastBusinessException
    {

        this.wipDefectDao.deleteDefect(defectId);
        final Boolean deleted = !this.wipDefectDao.exists(defectId);

        if (!deleted)
        {
            throw new MastBusinessException(String.format("Delete WIPDefect ID %d failed", defectId));
        }
        return deleted;
    }

    /**
     * Get the defects for a given asset and in a given status (optional).
     *
     * @return Paginated list of defects with repair counts populated for all temporary and permanent repairs.
     */
    @Override
    public PageResource<DefectDto> getDefectsForAsset(final Integer page, final Integer size, final String sort, final String order,
            final Long assetId, final Long statusId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final PageResource<DefectDto> defects = this.defectDao.getDefectsForAsset(pageSpecification, assetId, statusId);

        for (final DefectLightDto defect : defects.getContent())
        {
            setRepairAndCourseOfActionCounts(defect);
        }

        return defects;
    }

    @Override
    public PageResource<DefectDto> getWIPDefectsForJob(final Long jobId, final Integer page, final Integer size, final String sort,
            final String order) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final PageResource<DefectDto> defects = this.wipDefectDao.findAllForJob(pageSpecification, jobId, null);

        for (final DefectDto defect : defects.getContent())
        {
            setWIPRepairAndCourseOfActionCounts(defect);
        }

        return defects;
    }

    // todo move these to Orika Mapper on Defect Dao
    private <T extends DefectLightDto> void setRepairAndCourseOfActionCounts(final T defect)
    {
        defect.setRepairCount(this.repairDao.countRepairsForDefect(defect.getId()));
        defect.setCourseOfActionCount(defect.getRepairCount()
                + this.coCDao.countCoCsForDefect(defect.getId())
                + this.actionableItemDao.countActionableItemsForDefect(defect.getId()));
    }

    private <T extends DefectLightDto> void setWIPRepairAndCourseOfActionCounts(final T defect)
    {
        defect.setRepairCount(this.wipRepairDao.countRepairsForDefect(defect.getId()));
        defect.setCourseOfActionCount(defect.getRepairCount() + this.wipCoCDao.countCoCsForDefect(defect.getId())
                + this.wipActionableItemDao.countActionableItemsForDefect(defect.getId()));
    }

    @Override
    public PageResource<DefectDto> getDefectsByQuery(final Long assetId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);

        final PageResource<DefectDto> defects = this.defectDao.findAllForAsset(pageSpecification, assetId, cleanDto);

        for (final DefectLightDto defect : defects.getContent())
        {
            setRepairAndCourseOfActionCounts(defect);
        }
        return defects;
    }

    @Override
    public PageResource<DefectDto> getWIPDefectsByQuery(final Long jobId, final Integer page, final Integer size, final String sort,
            final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.wipDefectDao.findAllForJob(pageSpecification, jobId, cleanDto);
    }
}
