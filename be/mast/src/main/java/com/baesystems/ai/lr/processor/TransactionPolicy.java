package com.baesystems.ai.lr.processor;

import org.apache.camel.Processor;
import org.apache.camel.model.ProcessorDefinition;
import org.apache.camel.spi.RouteContext;
import org.apache.camel.spi.TransactedPolicy;
import org.apache.camel.util.ObjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Wraps the processor in a Spring transaction
 *
 * @version
 */
public class TransactionPolicy implements TransactedPolicy
{
    private static final Logger LOG = LoggerFactory.getLogger(TransactionPolicy.class);
    private TransactionTemplate transactionTemplate;
    private String propagationBehaviorName;
    private String isolationLevelName;
    private PlatformTransactionManager transactionManager;
    private boolean readOnly;

    private TxErrorHandlerBuilder builder;

    /**
     * Default constructor for easy spring configuration.
     */
    public TransactionPolicy()
    {
        // default constructor for easy spring configuration.
    }

    public TransactionPolicy(final TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    public TransactionPolicy(final PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public void setBuilder(final TxErrorHandlerBuilder builder)
    {
        this.builder = builder;
    }

    public void setTransactionTemplate(final TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    public void setTransactionManager(final PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public void setPropagationBehaviorName(final String propagationBehaviorName)
    {
        this.propagationBehaviorName = propagationBehaviorName;
    }

    public void setIsolationLevelName(final String isolationLevelName)
    {
        this.isolationLevelName = isolationLevelName;
    }

    public void setReadOnly(final boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    @Override
    public void beforeWrap(final RouteContext routeContext, final ProcessorDefinition<?> definition)
    {
        // not required
    }

    @Override
    public Processor wrap(final RouteContext routeContext, final Processor processor)
    {
        // already a TX error handler then we are good to go
        LOG.debug("The ErrorHandlerBuilder configured is already a TransactionErrorHandlerBuilder: {}", this.builder);
        final TxErrorHandler answer = this.createTransactionErrorHandler(routeContext, processor, this.builder);
        answer.setExceptionPolicy(this.builder.getExceptionPolicyStrategy());
        // configure our answer based on the existing error handler
        this.builder.configure(routeContext, answer);

        // return with wrapped transacted error handler
        return answer;
    }

    protected TxErrorHandler createTransactionErrorHandler(final RouteContext routeContext, final Processor processor,
            final TxErrorHandlerBuilder errorBuilder)
    {
        TxErrorHandler answer;
        try
        {
            answer = errorBuilder.createErrorHandler(routeContext, processor, this.getTransactionTemplate());
        }
        // need to catch Exception here to wrap it
        catch (final Exception exception) // NOPMD
        {
            throw ObjectHelper.wrapRuntimeCamelException(exception);
        }
        return answer;
    }

    public TransactionTemplate getTransactionTemplate()
    {
        if (this.transactionTemplate == null)
        {
            ObjectHelper.notNull(this.transactionManager, "transactionManager");
            this.transactionTemplate = new TransactionTemplate(this.transactionManager);
            if (this.propagationBehaviorName != null)
            {
                this.transactionTemplate.setPropagationBehaviorName(this.propagationBehaviorName);
            }
            if (this.isolationLevelName != null)
            {
                this.transactionTemplate.setIsolationLevelName(this.isolationLevelName);
            }

            this.transactionTemplate.setReadOnly(this.readOnly);
        }
        return this.transactionTemplate;
    }

}
