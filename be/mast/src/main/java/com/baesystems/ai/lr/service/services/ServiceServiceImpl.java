package com.baesystems.ai.lr.service.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.DateDto;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.scheduling.SchedulingRuleService;
import com.baesystems.ai.lr.utils.CollectionUtils;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ServiceService")
@Loggable(Loggable.DEBUG)
public class ServiceServiceImpl implements ServiceService
{
    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private WIPCertificateDao wipCertificateDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private SchedulingRuleService schedulingRuleService;

    @Autowired
    private DateHelper dateHelper;

    @Override
    public final List<ReferenceDataDto> getSchedulingTypesForServiceCatalogue(final Long serviceCatalogueId) throws RecordNotFoundException
    {
        final ServiceCatalogueLightDto service = this.serviceDao.getServiceCatalogue(serviceCatalogueId);
        this.serviceHelper.verifyModel(service, serviceCatalogueId);

        return this.serviceDao.getSchedulingTypesForServiceCatalogue(serviceCatalogueId);
    }

    /**
     * Get all of the Services that have been associated with the given Asset. Optionally filter these by the given
     * Service Credit Statuses.
     *
     * @param assetId
     * @param statusIds A list of Service Credit Status IDs
     * @param productFamilyIds A list of Product Family IDs
     * @param dueDateMin minimum due date
     * @param dueDateMax maximum due date
     * @return List of {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    @Override
    public final List<ScheduledServiceDto> getServicesForAsset(final Long assetId, final List<Long> statusIds, final List<Long> productFamilyIds,
            final Date dueDateMin, final Date dueDateMax) throws RecordNotFoundException
    {
        final Long publishedVersion = this.assetDao.getPublishedVersion(assetId);
        final AssetLightDto asset = this.assetDao.getAsset(assetId, publishedVersion, AssetLightDto.class);
        this.serviceHelper.verifyModel(asset, assetId);

        return this.serviceDao.getServicesForAsset(assetId, statusIds, productFamilyIds, dueDateMin, dueDateMax);
    }

    /**
     * Get a Service by its identifier, and its related Asset identifier. Will return null if the given IDs do not
     * represent an existing relationship.
     *
     * @param serviceId
     * @param productId
     * @param assetId
     * @return {@link ScheduledServiceDto}
     * @throws RecordNotFoundException
     */
    @Override
    public final ScheduledServiceDto getService(final Long serviceId, final Long assetId) throws RecordNotFoundException
    {
        final ScheduledServiceDto service = this.serviceDao.getService(serviceId, assetId);
        this.serviceHelper.verifyModel(service, serviceId);

        return service;
    }

    @Override
    public final ServiceCatalogueLightDto getServiceCatalogue(final Long serviceCatalogueId) throws RecordNotFoundException
    {
        final ServiceCatalogueLightDto serviceCatalogue = this.serviceDao.getServiceCatalogue(serviceCatalogueId);
        this.serviceHelper.verifyModel(serviceCatalogue, serviceCatalogueId);

        return serviceCatalogue;
    }

    @Override
    public final ScheduledServiceDto updateService(final Long serviceId, final ScheduledServiceDto serviceDto)
            throws RecordNotFoundException, MastBusinessException
    {
        final ScheduledServiceDto oldServiceDto = this.serviceDao.findOneWithLock(serviceId, ScheduledServiceDto.class, StaleCheckType.SERVICE);

        this.serviceHelper.verifyModel(oldServiceDto, serviceId);

        StaleCheckUtils.checkNotStale(oldServiceDto, serviceDto, StaleCheckType.SERVICE);

        return this.serviceDao.updateService(serviceDto);
    }

    /**
     * Method to add a new service on a post request, provided that the new service from the body and the product id are
     * validated this method will create a new row in the database for the service and return the new state of selected
     * service for the product to which it has been added.
     *
     * @param productId The id of the product to which the service will be added.
     * @param newService {@link ScheduledServiceDto} containing the details of the new service to be added to the
     *        product.
     */
    @Override
    public ScheduledServiceListDto saveServices(final ScheduledServiceListDto newServices) throws MastBusinessException, RecordNotFoundException
    {
        final ScheduledServiceListDto newServiceList = new ScheduledServiceListDto();
        newServiceList.setScheduledServices(new ArrayList<ScheduledServiceDto>());

        for (final ScheduledServiceDto newService : newServices.getScheduledServices())
        {
            // If this is to be a service update rather than an insert, check for staleness.
            if (newService.getId() != null)
            {
                final ScheduledServiceDto existingDto = this.serviceDao.getService(newService.getId());
                StaleCheckUtils.checkNotStale(existingDto, newService, StaleCheckType.SERVICE);
            }

            newServiceList.getScheduledServices().add(this.serviceDao.saveService(newService));
        }

        return newServiceList;
    }

    /**
     * Method to run the scheduling rules on newly created services. This needs to be run outside of the original
     * transaction so that the services exist for the rules engine to look at.
     */
    @Override
    public void runSchedulingRulesOnNewServices(final ScheduledServiceListDto newServices) throws MastBusinessException, RecordNotFoundException
    {
        for (final ScheduledServiceDto newService : newServices.getScheduledServices())
        {
            this.saveServiceScheduleDates(newService);
        }
    }

    /**
     * Method to run the scheduling rules on updated services. This needs to be run outside of the original transaction
     * so that the services exist for the rules engine to look at.
     */
    @Override
    public void runSchedulingRulesAfterUpdatingService(final ScheduledServiceDto newService) throws MastBusinessException, RecordNotFoundException
    {
        this.saveServiceScheduleDates(newService);
    }

    private void saveServiceScheduleDates(final ScheduledServiceDto service) throws MastBusinessException, RecordNotFoundException
    {
        // Find the product in the product table
        final Date harmonisationDate = this.assetDao.getAssetHarmonisationDate(service.getAsset().getId());

        // Calculate the schedule. This will call the rules engine.
        this.schedulingRuleService.scheduleServicesFollowingServiceCreation(service, harmonisationDate, service.getProvisionalDates());
    }

    /**
     * Soft delete a list of services given their ids.
     */
    @Override
    public final void deleteServices(final List<Long> idList)
    {
        idList.stream().forEach(serviceId -> this.serviceDao.deleteService(serviceId));
    }

    @Override
    public final DateDto getHarmonisationDate(final Long assetId) throws RecordNotFoundException
    {
        // Verify Asset exists here to prevent making two DB requests.
        final Long publishedVersion = this.assetDao.getPublishedVersion(assetId);
        final AssetLightDto asset = this.assetDao.getAsset(assetId, publishedVersion, AssetLightDto.class);
        this.serviceHelper.verifyModel(asset, assetId);

        return this.dateHelper.createDateDto(asset.getHarmonisationDate());
    }

    /**
     * The SREServiceMethod.nextServices API on the Scheduling Rules must only be called under the following
     * circumstances:
     * </p>
     * - There is a Scheduled Service to update </br>
     * - The Scheduled Service has been completed (i.e. has a Completion Date set, which happens when fully
     * credited) </br>
     * - The schedule has been updated on the Survey or the dates are to be set as not provisional because the FSR is
     * submitted. </br>
     */
    @Override
    public void updateServiceScheduleFollowingFARorFSR(final List<SurveyDto> surveys, final Boolean provisionalDates)
            throws MastBusinessException, RecordNotFoundException
    {
        if (surveys != null)
        {
            // Retrieve a fresh copy of the Surveys to ensure we have all newly created, linked Scheduled Services.
            final List<SurveyDto> freshSurveys = this.surveyDao.getSurveys(CollectionUtils.toIDList(surveys));

            for (final SurveyDto survey : freshSurveys)
            {
                if (survey.getScheduledService() != null
                        && survey.getScheduledService().getCompletionDate() != null
                        && (!survey.getScheduleDatesUpdated() || !provisionalDates))
                {
                    final ScheduledServiceDto service = survey.getScheduledService();
                    final Date harmonisationDate = this.assetDao.getAssetHarmonisationDate(service.getAsset().getId());

                    // Run Scheduling Rules on the Service.
                    this.schedulingRuleService.scheduleServicesFollowingServiceCompletion(service, harmonisationDate, provisionalDates);

                    // Prevent the Service schedule dates from being updated again for this Survey.
                    this.surveyDao.updateScheduleDatesFlag(survey.getId(), true);
                }
            }
        }
    }

    @Override
    public List<ScheduledServiceDto> updateServiceCertStatusFollowingFAR(final Long jobId)
    {
        final List<CertificateDto> certificates = this.wipCertificateDao.getCertificatesForJob(jobId);
        final List<ScheduledServiceDto> updatedServices = new ArrayList<ScheduledServiceDto>();

        if (certificates != null)
        {
            for (final CertificateDto certificate : certificates)
            {
                if (certificate.getJob() != null && certificate.getCertificateStatus() != null)
                {
                    final List<SurveyDto> surveys = this.surveyDao.getSurveys(certificate.getJob().getId());
                    updatedServices.addAll(this.updateCertStatusForServices(surveys, certificate.getCertificateStatus()));
                }
            }
        }

        return updatedServices;
    }

    private List<ScheduledServiceDto> updateCertStatusForServices(final List<SurveyDto> surveys, final LinkResource certificateStatus)
    {
        final List<ScheduledServiceDto> updatedServices = new ArrayList<ScheduledServiceDto>();

        if (surveys != null)
        {
            for (final SurveyDto survey : surveys)
            {
                if (survey.getScheduledService() != null)
                {
                    final ScheduledServiceDto service = this.serviceDao.getService(survey.getScheduledService().getId());
                    service.setServiceCreditStatus(new LinkResource(certificateStatus.getId()));
                    updatedServices.add(this.serviceDao.updateService(service));
                }
            }
        }

        return updatedServices;
    }
}
