package com.baesystems.ai.lr.service.jobs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.jobs.CertificateDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "CertificateService")
@Loggable(Loggable.DEBUG)
public class CertificateServiceImpl implements CertificateService
{
    @Autowired
    private WIPCertificateDao wipCertificateDao;

    @Autowired
    private CertificateDao certificateDao;

    @Autowired
    private DateHelper dateUtils;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public PageResource<CertificateDto> getCertificatesForJob(final Integer page, final Integer size, final String sort, final String order,
            final Long jobId) throws MastBusinessException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipCertificateDao.getCertificatesForJob(pageSpecification, jobId);
    }

    @Override
    public CertificateDto createCertificate(final CertificateDto certificateDto, final Long jobId) throws MastBusinessException
    {
        fillOutCertificateFields(certificateDto, jobId);

        return this.wipCertificateDao.saveCertificate(certificateDto);
    }

    @Override
    public CertificateDto updateCertificate(final CertificateDto certificateDto, final Long jobId, final Long certificateId)
            throws MastBusinessException
    {
        fillOutCertificateFields(certificateDto, jobId);

        final CertificateDto existingCertificateDto = this.wipCertificateDao.findOneWithLock(certificateId, CertificateDto.class, StaleCheckType.CERTIFICATE);
        StaleCheckUtils.checkNotStale(existingCertificateDto, certificateDto, StaleCheckType.CERTIFICATE);

        return this.wipCertificateDao.saveCertificate(certificateDto);
    }

    @Override
    public void updateCertificates(final Long jobId, final boolean approved) throws RecordNotFoundException
    {
        final List<CertificateDto> certificates = this.wipCertificateDao.getCertificatesForJob(jobId);

        for (final CertificateDto cert : certificates)
        {
            final Long wipId = cert.getId();
            final Long certId = cert.getParent() == null ? null : cert.getParent().getId();
            cert.setId(certId);

            cert.setApproved(approved);
            final CertificateDto result = this.certificateDao.saveCertificate(cert);

            if (certId == null)
            {
                cert.setId(wipId);
                cert.setParent(new LinkResource(result.getId()));

                this.wipCertificateDao.saveCertificate(cert);
            }
        }
    }

    private void fillOutCertificateFields(final CertificateDto certificateDto, final Long jobId) throws MastBusinessException
    {
        certificateDto.setJob(new LinkResource(jobId));

        final Date issueDate = certificateDto.getIssueDate();

        if (issueDate != null)
        {
            // The null checks on Certificate Type are done at the dto validation level so there is no need for checks
            // here.
            Date expiryDate = this.dateUtils.adjustDate(issueDate,
                    this.certificateDao.getCyclePeriodicityOfCertificateType(certificateDto.getCertificateType().getId()), Calendar.MONTH);
            expiryDate = this.dateUtils.adjustDate(expiryDate, -1, Calendar.DAY_OF_MONTH);
            certificateDto.setExpiryDate(expiryDate);
        }

        // by this point the job is know to exist
        certificateDto.setCertificateNumber(jobId.toString());
        certificateDto.setApproved(Boolean.TRUE);
    }
}
