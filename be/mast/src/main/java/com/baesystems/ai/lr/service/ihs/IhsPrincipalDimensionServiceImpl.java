package com.baesystems.ai.lr.service.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.ihs.IhsPrincipalDimensionsDao;
import com.baesystems.ai.lr.dto.ihs.IhsPrincipalDimensionsDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "IhsPrincipalDimensionService")
@Loggable(Loggable.DEBUG)
public class IhsPrincipalDimensionServiceImpl implements IhsPrincipalDimensionService
{
    @Autowired
    private IhsPrincipalDimensionsDao ihsPrincipalDimensionsDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public IhsPrincipalDimensionsDto getPrincipalDimensions(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsPrincipalDimensionsDto principalDimension = ihsPrincipalDimensionsDao.getPrincipalDimensions(imoNumber);
        this.serviceHelper.verifyModel(principalDimension, imoNumber);

        return principalDimension;
    }
}
