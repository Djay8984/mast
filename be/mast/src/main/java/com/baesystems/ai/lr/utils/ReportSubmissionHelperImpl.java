package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Component
public class ReportSubmissionHelperImpl implements ReportSubmissionHelper
{
    private final MapperFacade mapperForExistingDefects;

    private final MapperFacade mapperForNewDefects;

    private final MapperFacade generalWipToNonWipMapper;

    public ReportSubmissionHelperImpl()
    {
        DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(DefectDto.class, DefectDto.class)
                .exclude("id")
                .exclude("asset")
                .exclude("jobs")
                .exclude("items")
                .exclude("values")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectDefectValueDto.class, DefectDefectValueDto.class)
                .exclude("id")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectItemDto.class, DefectItemDto.class)
                .exclude("id")
                .byDefault().toClassMap());
        this.mapperForExistingDefects = factory.getMapperFacade();

        factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(DefectDto.class, DefectDto.class)
                .exclude("id")
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectDefectValueDto.class, DefectDefectValueDto.class)
                .exclude("id")
                .exclude("defect")
                .exclude("parent") // this should be null anyway.
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(DefectItemDto.class, DefectItemDto.class)
                .exclude("id")
                .exclude("defect")
                .exclude("parent") // this should be null anyway.
                .byDefault().toClassMap());
        this.mapperForNewDefects = factory.getMapperFacade();

        factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(SurveyDto.class, ScheduledServiceDto.class)
                .field("scheduledService.id", "id")
                .field("serviceCatalogue.id", "serviceCatalogueId")
                .field("surveyStatus", "serviceCreditStatus")
                .field("dateOfCrediting", "completionDate")
                .customize(new AlwaysActiveMapper())
                .byDefault().toClassMap());
        factory.registerClassMap(factory.classMap(WorkItemDto.class, WorkItemLightDto.class)
                .exclude("codicil")
                .exclude("survey")
                .exclude("conditionalParent")
                .exclude("workItem")
                .fieldAToB("parent.id", "id")
                .byDefault().toClassMap());
        this.generalWipToNonWipMapper = factory.getMapperFacade();

        factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(WorkItemDto.class, WorkItemLightDto.class).byDefault().toClassMap());
    }

    private static class AlwaysActiveMapper extends CustomMapper<SurveyDto, ScheduledServiceDto>
    {
        @Override
        public void mapAtoB(final SurveyDto surveyDto, final ScheduledServiceDto scheduledServiceDto, final MappingContext context)
        {
            // always initialise the active flag on a Scheduled Service to true
            scheduledServiceDto.setActive(Boolean.TRUE);
        }
    }

    /**
     * Method for mapping wipTasks (WorkItemDto) to tasks for saving/updating (WorkItemLightDto). The method
     * automatically maps from the wip versions to the parents of entities that are joined to the task (ie codicils and
     * surveys)
     */
    @Override
    public WorkItemLightDto mapWipTaskToTask(final WorkItemDto wipTask, final Long parentCodicilId)
    {
        final WorkItemLightDto task = this.generalWipToNonWipMapper.map(wipTask, WorkItemLightDto.class);
        task.setScheduledService(wipTask.getSurvey() == null ? null
                : wipTask.getSurvey().getScheduledService() == null ? null : new LinkResource(wipTask.getSurvey().getScheduledService().getId()));
        task.setCodicil(parentCodicilId == null ? null : new LinkResource(parentCodicilId));

        return task;
    }

    /**
     * Method to merge two defects largely overwriting the properties of the non-wip-defect with those of the
     * wip-defect.<br>
     * First the trivial fields like title etc. are copied from the wip-defect on to the non-wip-defect.<br>
     * Then if the current defect is already linked to the job the wip defect comes from then the list of jobs is left
     * alone, if not then the job that the wip-defect is attached to is added to the list.<br>
     * Finally the join tables for the items and values are merged retaining the ids of the parents and merging
     * (overwriting the old values on the non-wip-defect with those on the wip-defect) where possible. If there are now
     * links on the wip-defect (parent=null) then they are created on the non-wip-defect. If there are old links on the
     * non-wip-defect that are not on the wip-defect then they are deleted.
     */
    @Override
    public void mergeDefects(final DefectDto wipDefect, final DefectDto defect)
    {
        // map the trivial fields overwriting the ones on the defect with their wip counterparts.
        this.mapperForExistingDefects.map(wipDefect, defect);

        // link this job
        if (defect.getJobs() == null)
        { // if the defect has no jobs associated already create a list and add this job
            defect.setJobs(new ArrayList<>());
            addJob(defect.getJobs(), wipDefect.getJob().getId());
        }
        else if (!defect.getJobs().stream().anyMatch(link -> link.getJob().getId().equals(wipDefect.getJob().getId())))
        { // if the defect has a list of jobs, but it does not contain this one then add this one
            addJob(defect.getJobs(), wipDefect.getJob().getId());
        }

        // map the item links
        if (defect.getItems() == null)
        { // if the defect has no items already create a new list
            defect.setItems(new ArrayList<>());
        }
        defect.setItems(mergeItemLists(wipDefect.getItems(), defect.getItems()));

        // map the defect values
        if (defect.getValues() == null)
        { // if the defect has no values already create a new list
            defect.setValues(new ArrayList<>());
        }
        defect.setValues(mergeValueLists(wipDefect.getValues(), defect.getValues()));
    }

    /**
     * Creates a new job link and adds it to the list of jobs it is passed.
     */
    @Override
    public void addJob(final List<JobDefectDto> jobLinks, final Long jobId)
    {
        final JobDefectDto jobLink = new JobDefectDto();
        jobLink.setJob(new LinkResource(jobId));

        jobLinks.add(jobLink);
    }

    /**
     * Method for merging two lists of defect items links.
     * <p>
     * If the links exist in the first list, but no the second then they will not appear in the output.<br>
     * If the links exist in the second list, but not the first then they will appear in the output, but their ids will
     * be null so that new links are created<br>
     * If the links appear in both list then they will appear in the output with the id they had in the second list, but
     * the value they had in the first (mapped by the parent id from the first list). This means that the values of the
     * existing links will be overwritten.
     *
     * @param wipDefectItemList
     * @param defectItemList
     * @return A new list of links between the defect and asset items.
     */
    @Override
    public List<DefectItemDto> mergeItemLists(final List<DefectItemDto> wipDefectItemList, final List<DefectItemDto> defectItemList)
    {
        List<DefectItemDto> defectItems = new ArrayList<DefectItemDto>();

        if (wipDefectItemList != null)
        {
            // create a map that maps the ids of the existing links that will be retained to the wip links that will
            // overwrite them.
            final Map<Long, DefectItemDto> retainedDefectItemLinkIdToWipMap = wipDefectItemList.stream()
                    .filter(wipDefectItem -> wipDefectItem.getParent() != null)
                    .collect(Collectors.toMap(wipDefectItem -> wipDefectItem.getParent().getId(), Function.identity()));

            // create the new list of links starting by mapping in the new values of the links that are being retained.
            defectItems = defectItemList.stream()
                    .filter(defectItem -> retainedDefectItemLinkIdToWipMap.keySet().contains(defectItem.getId()))
                    .peek(defectItem -> this.mapperForExistingDefects.map(retainedDefectItemLinkIdToWipMap.get(defectItem.getId()), defectItem))
                    .collect(Collectors.toList());

            // create a list of new links that need to be created
            final List<DefectItemDto> newDefectItemLinks = wipDefectItemList.stream()
                    .filter(wipDefectItem -> wipDefectItem.getParent() == null)
                    .peek(wipDefectItem -> wipDefectItem.setId(null))
                    .collect(Collectors.toList());

            // add to the list the links that will be created.
            defectItems.addAll(newDefectItemLinks);
        }

        return defectItems;
    }

    /**
     * Method for merging two lists of defect defect-value links.
     * <p>
     * If the links exist in the first list, but no the second then they will not appear in the output.<br>
     * If the links exist in the second list, but not the first then they will appear in the output, but their ids will
     * be null so that new links are created<br>
     * If the links appear in both list then they will appear in the output with the id they had in the second list, but
     * the value they had in the first (mapped by the parent id from the first list). This means that the values of the
     * existing links will be overwritten.
     *
     * @param wipDefectValueList
     * @param defectValueList
     * @return A new list of links between the defect and defect-values.
     */
    @Override
    public List<DefectDefectValueDto> mergeValueLists(final List<DefectDefectValueDto> wipDefectValueList,
            final List<DefectDefectValueDto> defectValueList)
    {
        List<DefectDefectValueDto> defectValues = new ArrayList<DefectDefectValueDto>();

        if (wipDefectValueList != null)
        {
            // create a map that maps the ids of the existing links that will be retained to the wip links that will
            // overwrite them.
            final Map<Long, DefectDefectValueDto> retainedDefectValueLinkIdToWipMap = wipDefectValueList.stream()
                    .filter(wipDefectValue -> wipDefectValue.getParent() != null)
                    .collect(Collectors.toMap(wipDefectValue -> wipDefectValue.getParent().getId(), Function.identity()));

            // create the new list of links starting by mapping in the new values of the links that are being retained.
            defectValues = defectValueList.stream()
                    .filter(defectValue -> retainedDefectValueLinkIdToWipMap.keySet().contains(defectValue.getId()))
                    .peek(defectValue -> this.mapperForExistingDefects.map(retainedDefectValueLinkIdToWipMap.get(defectValue.getId()), defectValue))
                    .collect(Collectors.toList());

            // create a list of new links that need to be created
            final List<DefectDefectValueDto> newDefectValueLinks = wipDefectValueList.stream()
                    .filter(wipDefectValue -> wipDefectValue.getParent() == null)
                    .peek(wipDefectValue -> wipDefectValue.setId(null))
                    .collect(Collectors.toList());

            // add to the list the links that will be created.
            defectValues.addAll(newDefectValueLinks);
        }

        return defectValues;
    }

    @Override
    public <T extends BaseDto> void generalWipToNonWipMap(final T wip, final T nonWip)
    {
        this.generalWipToNonWipMapper.map(wip, nonWip);
    }

    @Override
    public ScheduledServiceDto serviceWipToNonWipMap(final SurveyDto survey)
    {
        return this.generalWipToNonWipMapper.map(survey, ScheduledServiceDto.class);
    }

    @Override
    public DefectDto mapNewDefect(final DefectDto wipDefect)
    {
        return this.mapperForNewDefects.map(wipDefect, DefectDto.class);
    }
}
