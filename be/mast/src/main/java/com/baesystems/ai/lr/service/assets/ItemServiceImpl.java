package com.baesystems.ai.lr.service.assets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.VersioningHelper;
import com.jcabi.aspects.Loggable;

@Service("ItemService")
@Loggable(Loggable.DEBUG)
public class ItemServiceImpl implements ItemService
{
    @Autowired
    private ItemDao itemDao;

    @Autowired
    private ItemPersistDao itemPersistDao;

    @Autowired
    private AttributeDao attributeDao;

    @Autowired
    private AssetReferenceDataDao assetReferenceDataDao;

    @Autowired
    private ItemTypeDao itemTypeDao;

    @Autowired
    private AssetDao assetDao;

    @Autowired
    private AssetVersionUpdateService assetVersionUpdateService;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private VersioningHelper versioningHelper;

    @Override
    public PageResource<LazyItemDto> getItems(final Integer page, final Integer size, final String sort, final String order, final Long assetId,
            final Long versionId, final Boolean allowDrafts, final ItemQueryDto itemQueryDto) throws RecordNotFoundException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final ItemQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(itemQueryDto, ItemQueryDto.class);
        return this.itemDao.findItemsByAsset(assetId, versionId, allowDrafts, pageSpecification, cleanDto);
    }

    @Override
    public LazyItemDto getItem(final Long itemId, final Long assetId, final Long assetVersion, final Boolean allowDraft)
            throws RecordNotFoundException
    {
        final VersionedItemPK versionedItemPK = this.versioningHelper.getVersionedItemPK(itemId, assetId, assetVersion, allowDraft);
        return this.itemDao.getItem(versionedItemPK);
    }

    @Override
    public LazyItemDto getRootItem(final Long assetId, final Long versionId, final Boolean allowDraft) throws RecordNotFoundException
    {
        final LazyItemDto rootItem = this.itemDao.getRootItem(assetId, versionId, allowDraft);
        this.serviceHelper.verifyModel(rootItem, assetId);
        return rootItem;
    }

    /**
     * Updates the name of the Item associated with the given Attribute. The Item name is derived from the Attribute
     * Type names of all Attributes linked to the Item, as well as the Item Type name.
     *
     * @param {@link AttributeDto}
     */
    @Override
    public void updateItemName(final AttributeDto attribute, final Long itemId)
    {
        if (attribute.getAttributeType() != null && itemId != null)
        {
            final AttributeTypeDto attributeType = this.assetReferenceDataDao.getAttributeType(attribute.getAttributeType().getId());

            if (attributeType.getNamingOrder() != null)
            {
                updateItemNameUsingRefData(itemId);
            }
        }
    }

    /**
     * Update the item name using a value generated from the attribute types it holds.
     */
    @Override
    public String updateItemName(final ItemWithAttributesDto itemWithAttributes)
    {
        String newItemName = null;

        final List<AttributeDto> attributes = itemWithAttributes.getAttributes();
        if (CollectionUtils.isNotEmpty(attributes) && isCandidateForRenaming(attributes))
        {
            final Long itemId = itemWithAttributes.getId();

            newItemName = updateItemNameUsingRefData(itemId);
        }

        return newItemName;
    }

    /**
     * If all attributes' types have no naming order, then no renaming is required.
     *
     * @param attributes
     * @return
     */
    private boolean isCandidateForRenaming(final List<AttributeDto> attributes)
    {
        // Minimises the same attribute type being looked up more than once.
        final Set<Long> attributeTypeIdsChecked = new HashSet<>();

        boolean result = false;

        for (final AttributeDto attribute : attributes)
        {
            final Long attributeTypeId = attribute.getAttributeType().getId();

            if (!attributeTypeIdsChecked.contains(attributeTypeId))
            {
                final AttributeTypeDto attributeType = this.assetReferenceDataDao.getAttributeType(attributeTypeId);

                if (attributeType.getNamingOrder() != null)
                {
                    result = true;
                    break;
                }

                attributeTypeIdsChecked.add(attributeTypeId);
            }
        }

        return result;
    }

    /**
     * Get attribute type data for an item, and use it to construct a new item name, then persist it.
     *
     * @param itemId
     */
    private String updateItemNameUsingRefData(final Long itemId)
    {
        // Get attribute types relating to this item's attributes
        final List<AttributeDto> attributes = getAttributesWithNamingOrderForItem(itemId);

        // Get the item type for this item
        final Long itemTypeId = this.itemDao.getItemType(itemId);
        final ItemTypeDto itemType = this.itemTypeDao.getItemType(itemTypeId);

        // Generate new Item name
        final String itemName = generateItemName(attributes, itemType);

        // Update Item with new name
        this.itemPersistDao.updateItemName(itemId, itemName);

        return itemName;
    }

    /**
     * Get the the Attributes that are linked to the given Item. Attribute are returned in Attribute Type Naming Order.
     * Attributes with Attribute Types without a Naming Order are not included.
     *
     * @param itemId
     * @return list of {@link AttributeDto}
     */
    private List<AttributeDto> getAttributesWithNamingOrderForItem(final Long itemId)
    {
        return this.attributeDao.findAttributesWithNamingOrderForItem(itemId);
    }

    /**
     * Create an Item name from a list of Attributes and an Item Type Name. The Attribute value will be appended in the
     * order that they are provided.
     *
     * @param list of {@link AttributeDto}
     * @param the item type name
     * @return new Item name
     */
    private String generateItemName(final List<AttributeDto> attributes, final ItemTypeDto itemType)
    {
        String newItemName;

        if (attributes != null)
        {
            newItemName = attributes
                    .stream()
                    .map(attribute -> attribute.getValue())
                    .collect(Collectors.joining(" ", "", itemType.getName()));
        }
        else
        {
            newItemName = itemType.getName();
        }

        return newItemName.trim();
    }

    @Override
    public void setItemReviewed(final Long itemId)
    {
        // final LazyItemDto lazyItemDto = this.itemDao.getItem(itemId);
        final LazyItemDto lazyItemDto = new LazyItemDto();

        final boolean reviewedInTheDatabase = lazyItemDto.getReviewed() == null || lazyItemDto.getReviewed().booleanValue();

        // If the reviewed flag has not changed, update nothing.
        if (!reviewedInTheDatabase)
        {
            this.itemPersistDao.setItemAndAllDescendantsToSuccessfullyReviewed(itemId);
        }
        else
        {
            this.itemPersistDao.setItemAndAllAncestorsToRequireAReview(itemId);
        }
    }

    /**
     * Update the decommissioned flag for the given item and all of its child items. This change must be applied to
     * versioned items, so first check whether we need to create a new version by comparing the version on the item with
     * the version on the asset. If they match, we know that we are in sync and can update the item. If they do not
     * match, we need to create a new version of the item and update that instead.
     *
     * @param assetId
     * @param itemId
     * @param flagState
     */
    @Override
    public void setItemDecommissionState(final Long assetId, final Long itemId, final Boolean flagState)
    {
        final Long versionId = this.assetDao.getDraftVersion(assetId);
        final VersionedItemPK versionedItemPK = new VersionedItemPK(itemId, assetId, versionId);

        final LazyItemDto item = this.itemDao.getItemWithoutRelationships(versionedItemPK);

        final List<Long> itemIdsToUpdate = new ArrayList<Long>();

        // UpVersion the top level item if necessary and add to the list for update
        if (!versionId.equals(item.getAssetVersionId()))
        {
            this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
        }
        itemIdsToUpdate.add(itemId);

        // UpVersion the child items if necessary and add to the list for update
        for (final ItemDto childItem : this.itemDao.getItemChildList(versionedItemPK))
        {
            if (!versionId.equals(childItem.getAssetVersionId()))
            {
                this.assetVersionUpdateService.upVersionSingleItem(assetId, childItem.getId(), true);
            }

            itemIdsToUpdate.add(childItem.getId());
        }

        // Update the flag for all items in the list
        this.itemPersistDao.updateDecommissionedFlag(flagState, assetId, itemIdsToUpdate, versionId);
    }

    @Override
    public void deleteItem(final Long itemId, final Long assetId)
    {
        this.itemPersistDao.deleteItem(itemId, assetId);
    }

    @Override
    public ItemRelationshipDto createItemRelationship(final Long assetId, final Long itemId, final ItemRelationshipDto itemRelationshipDto)
    {
        return this.itemPersistDao.createItemRelationship(assetId, itemId, itemRelationshipDto);
    }

    @Override
    public void deleteItemRelationship(final Long relationshipId)
    {
        final ItemRelationshipDO passedRelationship = this.itemDao.getItemRelationship(relationshipId);

        final VersionedItemDO fromItem = passedRelationship.getFromItem();
        final VersionedItemDO toItem = passedRelationship.getToItem();

        final Long fromDraftVersion = this.assetDao.getDraftVersion(fromItem.getAssetId());
        final Long toDraftVersion = this.assetDao.getDraftVersion(toItem.getAssetId());

        final Optional<Long> possibleRelationshipToDelete = this.itemDao.getItemRelationshipId(fromItem.getAssetId(), fromItem.getId(),
                fromDraftVersion, toItem.getId(), toDraftVersion);

        Long relationshipIdToDelete;

        if (possibleRelationshipToDelete.isPresent())
        {
            relationshipIdToDelete = possibleRelationshipToDelete.get();
        }
        else
        {
            // Check whether draft versions exist of both items, creating them if necessary.

            getOrCreateDraftItemVersion(fromDraftVersion, fromItem.getAssetId(), fromItem.getId());
            final Long toAssetVersionId = getOrCreateDraftItemVersion(toDraftVersion, toItem.getAssetId(), toItem.getId());

            // Create the relationship to then soft delete it

            final ItemRelationshipDto itemRelationshipDto = new ItemRelationshipDto();
            final VersionedItemPK versionedToItemPK = new VersionedItemPK(toItem.getId(), toItem.getAssetId(), toAssetVersionId);
            final ItemLightDto toItemLight = this.itemDao.getItemLight(versionedToItemPK);
            itemRelationshipDto.setToItem(toItemLight);
            itemRelationshipDto.setType(new LinkResource(RelationshipType.IS_RELATED_TO.getValue()));

            final ItemRelationshipDto newRelationship = this.itemPersistDao.createItemRelationship(fromItem.getAssetId(), fromItem.getId(),
                    itemRelationshipDto);
            relationshipIdToDelete = newRelationship.getId();
        }

        this.itemPersistDao.deleteItemRelationship(relationshipIdToDelete);
    }

    private Long getOrCreateDraftItemVersion(final Long assetDraftVersion, final Long assetId, final Long itemId)
    {
        final VersionedItemPK versionedItemPK = new VersionedItemPK(itemId, assetId, assetDraftVersion);
        final ItemLightDto possibleDraftItem = this.itemDao.getItemLight(versionedItemPK);

        Long updatedVersionId;

        if (possibleDraftItem == null)
        {
            updatedVersionId = this.assetVersionUpdateService.upVersionSingleItem(assetId, itemId, false);
        }
        else
        {
            updatedVersionId = assetDraftVersion;
        }

        return updatedVersionId;
    }
}
