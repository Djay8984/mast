package com.baesystems.ai.lr.service.attachments;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.baesystems.ai.lr.utils.QueryDtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.attachments.AttachmentDao;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "AttachmentService")
@Loggable(Loggable.DEBUG)
public class AttachmentServiceImpl implements AttachmentService
{
    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public SupplementaryInformationDto saveAttachment(final Long entityId, final String attachmentTargetTypeString,
            final SupplementaryInformationDto supplementaryInformationDto)
                    throws RecordNotFoundException, MastBusinessException
    {
        try
        {
            final AttachmentTargetType attachmentTargetType = AttachmentTargetType.valueOf(attachmentTargetTypeString);
            final SupplementaryInformationDto supplementaryInformation = this.attachmentDao.saveAttachment(
                    attachmentTargetType,
                    entityId,
                    supplementaryInformationDto);
            this.serviceHelper.verifyModel(supplementaryInformation, supplementaryInformation.getId());
            return supplementaryInformation;
        }
        catch (final IllegalArgumentException exception)
        {
            throw new MastBusinessException("Illegal Target Type for Attachment", exception);
        }
    }

    @Override
    public SupplementaryInformationDto getAttachment(final Long supplementaryInformationId) throws RecordNotFoundException
    {
        final SupplementaryInformationDto supplementaryInformation = this.attachmentDao
                .getAttachment(supplementaryInformationId);
        this.serviceHelper.verifyModel(supplementaryInformation, supplementaryInformationId);
        return supplementaryInformation;
    }

    @Override
    public Boolean deleteAttachment(final Long supplementaryInformationId)
    {
        this.attachmentDao.deleteAttachment(supplementaryInformationId);
        return !this.attachmentDao.exists(supplementaryInformationId);
    }

    @Override
    public PageResource<SupplementaryInformationDto> getAttachments(final Integer page, final Integer size, final String sort,
            final String order, final String target, final Long targetId) throws BadPageRequestException, MastBusinessException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        try
        {
            final AttachmentTargetType targetType = AttachmentTargetType.valueOf(target);
            return this.attachmentDao.findAttachments(pageSpecification, targetType, targetId);
        }
        catch (final IllegalArgumentException exception)
        {
            throw new MastBusinessException("Illegal Target Type for Attachment", exception);
        }
    }

    @Override
    public SupplementaryInformationDto updateAttachment(final SupplementaryInformationDto attachmentDto) throws RecordNotFoundException
    {
        final SupplementaryInformationDto result = this.attachmentDao.updateAttachment(attachmentDto);
        this.serviceHelper.verifyModel(result, attachmentDto.getId());
        return result;
    }

    @Override
    public List<SupplementaryInformationDto> getAttachmentsByQuery(final AttachmentLinkQueryDto attachmentLinkQueryDto)
    {
        final AttachmentLinkQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(attachmentLinkQueryDto, AttachmentLinkQueryDto.class);
        return this.attachmentDao.findAttachmentsByQuery(cleanDto);
    }

    @Override
    public void migrateAttachmentsFromWipEntities(final Map<Long, Long> wipToNonwipIdMap, final AttachmentTargetType typeFrom,
            final AttachmentTargetType typeTo)
                    throws BadPageRequestException
    {
        for (final Entry<Long, Long> wipToNonwipIdEntry : wipToNonwipIdMap.entrySet())
        {
            final PageResource<SupplementaryInformationDto> attachmentsOnWipObject = this.attachmentDao
                    .findAttachments(this.serviceHelper.createPageable(0, null, null, null), typeFrom, wipToNonwipIdEntry.getKey());

            for (final SupplementaryInformationDto attachment : attachmentsOnWipObject.getContent())
            {
                attachment.setId(null);
                this.attachmentDao.saveAttachment(typeTo, wipToNonwipIdEntry.getValue(), attachment);
            }
        }
    }
}
