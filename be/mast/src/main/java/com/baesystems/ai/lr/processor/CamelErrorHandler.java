package com.baesystems.ai.lr.processor;

import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.DETAILED_BINDING_ERROR_MSG;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.GENERAL_BINDING_ERROR_MSG;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.INTERNAL_SERVER_ERROR_MSG;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.INVALID_FIELD;
import static com.baesystems.ai.lr.utils.ExceptionMessagesUtils.STALE_DATA_TRYING_TO_BE_PERSISTED;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.RuntimeCamelException;
import org.apache.camel.component.bean.ParameterBindingException;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.transaction.HeuristicCompletionException;

import com.baesystems.ai.lr.dto.validation.ErrorMessageDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MastException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

public class CamelErrorHandler implements Processor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CamelErrorHandler.class);
    private static final  String DELIMITER = " : ";

    @Override
    public void process(final Exchange exchange)
    {
        final Throwable exception = getException(exchange);
        if (exception != null)
        {
            processException(exchange, exception);
        }
    }

    private void processException(final Exchange exchange, final Throwable exception)
    {
        ErrorMessageDto errorMessage;
        LOGGER.error("process", exception);
        Integer errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();



        if (exception instanceof ParameterBindingException)
        {
            final ParameterBindingException parameterBindingException = (ParameterBindingException) exception;

            errorCode = HttpStatus.BAD_REQUEST.value();
            errorMessage = new ErrorMessageDto(GENERAL_BINDING_ERROR_MSG
                    + DELIMITER
                    + String.format(DETAILED_BINDING_ERROR_MSG,
                    parameterBindingException.getParameterValue(),
                    parameterBindingException.getParameterType().getSimpleName()));
        }
        else if (exception instanceof InvalidFormatException)
        {
            final InvalidFormatException invalidFormatException = (InvalidFormatException) exception;
            errorCode = HttpStatus.BAD_REQUEST.value();
            errorMessage = new ErrorMessageDto(GENERAL_BINDING_ERROR_MSG
                    + DELIMITER
                    + String.format(DETAILED_BINDING_ERROR_MSG,
                    invalidFormatException.getValue(),
                    invalidFormatException.getTargetType().getSimpleName()));
        }
        else if (exception instanceof MastBusinessException)
        {
            errorCode = ((MastBusinessException) exception).getErrorCode();
            final int exceptionCode = ((MastBusinessException) exception).getType().getExceptionCode();
            errorMessage = new ErrorMessageDto(exception.getMessage(), exceptionCode);
        }
        else if (exception instanceof MastException)
        {
            errorCode = ((MastException) exception).getErrorCode();
            errorMessage = new ErrorMessageDto(exception.getMessage());
        }
        else if (exception instanceof PropertyReferenceException)
        {
            errorCode = HttpStatus.BAD_REQUEST.value();
            errorMessage = new ErrorMessageDto(String.format(INVALID_FIELD, ((PropertyReferenceException) exception).getPropertyName()));
        }
        else if (isStaleState(exception))
        {
            errorCode = HttpStatus.CONFLICT.value();
            errorMessage = new ErrorMessageDto(INTERNAL_SERVER_ERROR_MSG
                    + DELIMITER
                    + STALE_DATA_TRYING_TO_BE_PERSISTED);
        }
        // Catch the exception that is thrown when an invalid sort is included with a search.
        else
        {
            errorMessage = new ErrorMessageDto(INTERNAL_SERVER_ERROR_MSG
                    + DELIMITER
                    + exception.getMessage());
        }

        final StringBuilder details = new StringBuilder(exchange.getProperty(Exchange.FAILURE_ROUTE_ID, "", String.class));
        final Throwable cause = exception.getCause();
        if (cause != null)
        {
            details.append(DELIMITER);
            details.append(exception.getCause().toString());
        }
        errorMessage.setDetailedMessage(details.toString());

        // translated so clear
        exchange.setException(null);
        exchange.setProperty(Exchange.EXCEPTION_CAUGHT, null);

        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(errorMessage);
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, errorCode);
    }

    /**
     * This method determines whether the passed exception has been caused by hibernate attempting to persist stale
     * data, where some other process is trying to update the same data on another thread.
     *
     * @param exception to be checked
     * @return whether the exception was caused by trying to persist stale data with hibernate
     */
    private static boolean isStaleState(final Throwable exception)
    {
        return exception instanceof HeuristicCompletionException
                && exception.getCause() instanceof ObjectOptimisticLockingFailureException
                && exception.getCause().getCause() instanceof StaleStateException;
    }

    private Throwable getException(final Exchange exchange)
    {
        Throwable exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
        if (exception == null)
        {
            exception = exchange.getException();
        }

        if (exception != null)
        {
            while (exception.getCause() != null && exception != exception.getCause() && (exception instanceof CamelException
                    || exception instanceof RuntimeCamelException))
            {
                exception = exception.getCause();
            }
        }
        return exception;
    }

}
