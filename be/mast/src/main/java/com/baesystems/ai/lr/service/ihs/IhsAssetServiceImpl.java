package com.baesystems.ai.lr.service.ihs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dao.ihs.IhsGroupFleetDao;
import com.baesystems.ai.lr.dao.ihs.IhsHistDao;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "IhsAssetService")
@Loggable(Loggable.DEBUG)
public class IhsAssetServiceImpl implements IhsAssetService
{
    @Autowired
    private IhsAssetDao ihsAssetDao;

    @Autowired
    private IhsGroupFleetDao ihsGroupFleetDao;

    @Autowired
    private IhsHistDao ihsHistDao;

    @Autowired
    private ServiceHelper serviceHelper;

    /**
     * Gets the IHS asset, {@link IhsAssetDetailsDto}.
     *
     * @param imoNumber, IMO number.
     * @return IHS asset, {@link IhsAssetDetailsDto}.
     * @throws RecordNotFoundException, if asset not found.
     */
    @Override
    public final IhsAssetDetailsDto getIhsAsset(final Long imoNumber) throws RecordNotFoundException
    {
        final IhsAssetDto ihsAsset = this.ihsAssetDao.getIhsAsset(imoNumber);
        final IhsGroupFleetDto ihsGroupFleet = this.ihsGroupFleetDao.getIhsGroupFleet(imoNumber);
        final IhsHistDto ihsHist = this.ihsHistDao.getIhsHist(imoNumber);

        final IhsAssetDetailsDto ihsAssetDetails = new IhsAssetDetailsDto();

        this.serviceHelper.verifyModel(ihsAsset, imoNumber);
        this.serviceHelper.verifyModel(ihsGroupFleet, imoNumber);
        this.serviceHelper.verifyModel(ihsHist, imoNumber);

        ihsAssetDetails.setIhsAsset(ihsAsset);
        ihsAssetDetails.setIhsGroupFleet(ihsGroupFleet);
        ihsAssetDetails.setIhsHist(ihsHist);

        return ihsAssetDetails;
    }
}
