package com.baesystems.ai.lr.service.reports;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.DueStatusService;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ReportService")
@Loggable(Loggable.DEBUG)
public class ReportServiceImpl implements ReportService
{
    @Autowired
    private ReportDao reportDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    private WIPAssetNoteDao wipAssetNoteDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private WIPWorkItemDao wipWorkItemDao;

    @Autowired
    @Qualifier("reportContentJackson")
    private JacksonDataFormat dataFormatter;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private ServiceUtils serviceUtils;

    @Autowired
    private DueStatusService dueStatusService;

    @Override
    public ReportDto getReport(final Long reportId) throws RecordNotFoundException, BadRequestException
    {
        final ReportDto report = this.reportDao.getReport(reportId);
        this.serviceHelper.verifyModel(report, reportId);

        return report;
    }

    @Override
    public List<ReportLightDto> getReports(final Long jobId) throws RecordNotFoundException
    {
        this.serviceHelper.verifyModel(this.jobDao.getJob(jobId), jobId);
        return this.reportDao.getReports(jobId);
    }

    @Override
    public ReportDto saveReport(final ReportDto reportDto) throws BadRequestException
    {
        String content = null;

        final Long jobId = reportDto.getJob().getId();
        final Long typeId = reportDto.getReportType().getId();

        reportDto.setReportVersion(this.reportDao.getNextReportVersion(jobId, typeId));
        // Update to latest Report Snapshot to push FAR
        // Otherwise migrate the "Report"
        if (!ReportType.isAfter(reportDto.getReportType().getId(), ReportType.FAR))
        {
            // if the front end has passed content then keep this content otherwise generate it from the database.
            content = reportDto.getContent() == null ? getReportContent(jobId, reportDto) : reportDto.getContent();
        }
        else
        {
            getDatePopulatedReport(reportDto, reportDto.getJob().getId(), reportDto.getReportType().getId());
            content = getParentReportContent(jobId, reportDto.getReportType().getId());
        }
        reportDto.setContent(content);

        return this.reportDao.saveReport(reportDto);
    }

    @Override
    public ReportDto updateReport(final ReportDto updatedReport) throws BadRequestException, StaleObjectException
    {
        final ReportDto existingReportDto = this.reportDao.findOneWithLock(updatedReport.getId(), ReportDto.class, StaleCheckType.REPORT);
        StaleCheckUtils.checkNotStale(existingReportDto, updatedReport, StaleCheckType.REPORT);

        // keep the old version and content.
        updatedReport.setContent(existingReportDto.getContent());
        updatedReport.setReportVersion(existingReportDto.getReportVersion());

        return this.reportDao.saveReport(updatedReport);
    }

    @Override
    public Long getNextReportVersion(final Long jobId, final Long reportTypeId)
    {
        return this.reportDao.getNextReportVersion(jobId, reportTypeId);
    }

    @Override
    public ReportDto getLatestReport(final Long jobId, final Long typeId)
    {
        final Long version = this.reportDao.getNextReportVersion(jobId, typeId);
        ReportDto result = null;
        if (version != null && version > 1)
        {
            result = this.reportDao.getReport(jobId, typeId, version - 1);
        }
        return result;
    }

    private void getDatePopulatedReport(final ReportDto report, final Long jobId, final Long reportTypeId) throws BadRequestException
    {
        final ReportDto parent = getLatestReport(jobId, ReportType.getTypeBeforeCurrent(reportTypeId));
        if (parent != null)
        {
            report.setIssueDate(parent.getIssueDate());
        }
    }

    private ReportContentDto populateReportContent(final Long jobId, final ReportDto report)
    {
        final ReportContentDto reportContent = new ReportContentDto();

        List<AssetNoteDto> assetNotes;
        List<CoCDto> cocs;
        List<ActionableItemDto> actionableItems;
        List<SurveyDto> surveys;
        List<WorkItemDto> tasks;

        if (ReportType.DAR.value().equals(report.getReportType().getId()))
        { // if the report is a DAR then the content should only be that updated by the user on the issue date
            final String userName = this.serviceUtils.getSecurityContextPrincipal();
            assetNotes = this.wipAssetNoteDao.getAssetNotesForJob(jobId, userName, report.getIssueDate());
            cocs = this.wipCoCDao.getCoCsForJob(jobId, userName, report.getIssueDate());
            actionableItems = this.wipActionableItemDao.getActionableItemsForJob(jobId, userName, report.getIssueDate());
            surveys = this.surveyDao.getSurveys(jobId, userName, report.getIssueDate());
            tasks = this.wipWorkItemDao.getTasksForJob(jobId, userName, report.getIssueDate());
        }
        else
        { // for non DARs the content is the contents of the data base at the point of submission
            assetNotes = this.wipAssetNoteDao.getAssetNotesForReportContent(jobId);
            cocs = this.wipCoCDao.getCoCsForReportContent(jobId);
            actionableItems = this.wipActionableItemDao.getActionableItemsForReportContent(jobId);
            surveys = this.surveyDao.getSurveysForReportContent(jobId);
            tasks = this.wipWorkItemDao.getTasksForReportContent(jobId);
        }

        this.dueStatusService.calculateDueStatuses(assetNotes);
        this.dueStatusService.calculateDueStatuses(cocs);
        this.dueStatusService.calculateDueStatuses(actionableItems);
        // dueStatusService.calculateDueStatuses(surveys);
        // dueStatusService.calculateDueStatuses(tasks);

        reportContent.setWipAssetNotes(assetNotes);
        reportContent.setWipCocs(cocs);
        reportContent.setWipActionableItems(actionableItems);
        reportContent.setSurveys(surveys);
        reportContent.setTasks(tasks);

        return reportContent;
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private String getReportContent(final Long jobId, final ReportDto report)
    {
        final ReportContentDto reportContent = populateReportContent(jobId, report);

        try (final ByteArrayOutputStream content = new ByteArrayOutputStream())
        {
            try
            {
                this.dataFormatter.marshal(null, reportContent, content);
                return content.toString("UTF-8");
            }
            catch (final Exception exception)
            {
                throw new MastSystemException("Error Generating report content", exception);
            }
        }
        catch (final IOException ioException)
        {
            throw new MastSystemException("Error Generating report content", ioException);
        }
    }

    private String getParentReportContent(final Long jobId, final Long reportTypeId) throws BadRequestException
    {
        final ReportDto parent = getLatestReport(jobId, ReportType.getTypeBeforeCurrent(reportTypeId));

        if (parent == null)
        {
            // will be handled earlier via validation routines
            throw new BadRequestException("Can not determine parent report");
        }

        return parent.getContent();
    }
}
