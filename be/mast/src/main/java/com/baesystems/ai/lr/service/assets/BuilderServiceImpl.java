package com.baesystems.ai.lr.service.assets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.assets.BuilderDao;
import com.baesystems.ai.lr.dto.assets.BuilderDto;
import com.jcabi.aspects.Loggable;

@Service(value = "BuilderService")
@Loggable(Loggable.DEBUG)
public class BuilderServiceImpl implements BuilderService
{
    @Autowired
    private BuilderDao builderDao;

    @Override
    public final List<BuilderDto> getBuilders()
    {
        return this.builderDao.findAll();
    }
}
