package com.baesystems.ai.lr.service.customers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.customers.PartyDao;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;
import com.baesystems.ai.lr.dto.customers.PartyLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "CustomerService")
@Loggable(Loggable.DEBUG)
public class CustomerServiceImpl implements CustomerService
{
    @Autowired
    private PartyDao customerDao;

    @Autowired
    private ServiceHelper serviceHelper;

    /**
     * Gets a customer, {@link PartyDto}, by its identifier.
     *
     * @param customerId, customer identifier.
     * @return A customer, {@link PartyDto}.
     * @throws RecordNotFoundException, if customer not found.
     */
    @Override
    public final PartyDto getCustomer(final Long customerId) throws RecordNotFoundException
    {
        final PartyDto customer = this.customerDao.getCustomer(customerId);
        this.serviceHelper.verifyModel(customer, customerId);

        return customer;
    }

    @Override
    public final List<PartyLightDto> getCustomers()
    {
        return this.customerDao.findAll();
    }

    @Override
    public void saveCustomer(final PartyDto customer)
    {
        this.customerDao.saveCustomer(customer);
    }

    @Override
    public final PageResource<CustomerLinkDto> getAssetCustomers(final Integer page, final Integer size, final String sort, final String order,
            final Long assetId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);

        return customerDao.getAssetCustomers(pageSpecification, assetId);
    }
}
