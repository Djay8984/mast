package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.ReferenceDataMetaDao;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataVersionDto;
import com.jcabi.aspects.Loggable;

@Service("ReferenceDataMetaService")
@Loggable(Loggable.DEBUG)
public class ReferenceDataMetaServiceImpl implements ReferenceDataMetaService
{
    @Autowired
    private ReferenceDataMetaDao referenceDataMetaDao;

    @Override
    public ReferenceDataVersionDto getTopLevelReferenceDataVersion()
    {
        return this.referenceDataMetaDao.getTopLevelReferenceDataVersion();
    }

    @Override
    public List<ReferenceDataDto> getDueStatuses()
    {
        return this.referenceDataMetaDao.getDueStatuses();
    }
}
