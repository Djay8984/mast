package com.baesystems.ai.lr.service.codicils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.TemplateAssetCountDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.AssetLifecycleStatus;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.StaleCheckUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "ActionableItemService")
@Loggable(Loggable.DEBUG)
public class ActionableItemServiceImpl implements ActionableItemService
{
    @Autowired
    private ActionableItemDao actionableItemDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Autowired
    private DateHelper dateHelper;

    @Override
    public ActionableItemDto getActionableItem(final Long id) throws RecordNotFoundException
    {
        final ActionableItemDto actionableItemDto = this.actionableItemDao.getActionableItem(id);
        this.serviceHelper.verifyModel(actionableItemDto, id);
        return actionableItemDto;
    }

    @Override
    public PageResource<ActionableItemDto> getActionableItemsForAssets(final Integer page, final Integer size, final String sort,
            final String order, final Long assetId, final Long statusId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.actionableItemDao.getActionableItemsForAssets(pageSpecification, assetId, statusId);
    }

    @Override
    public ActionableItemDto createActionableItem(final Long assetId, final Long itemId, final ActionableItemDto actionableItem)
    {
        actionableItem.setAssetItem(new ItemLightDto());
        actionableItem.getAssetItem().setId(itemId);
        return this.createActionableItem(assetId, actionableItem);
    }

    @Override
    public ActionableItemDto createActionableItem(final Long assetId, final ActionableItemDto actionableItem)
    {
        setStatus(actionableItem);
        return this.actionableItemDao.createActionableItem(assetId, actionableItem);
    }

    @Override
    public TemplateAssetCountDto countAssetsWithActionableItemsForTemplate(final List<Long> templateIds)
    {
        return this.actionableItemDao.countAssetsWithActionableItemsForTemplate(templateIds, AssetLifecycleStatus.getStatusesForTemplateCount());
    }

    @Override
    public ActionableItemDto updateActionableItem(final Long assetId, final ActionableItemDto actionableItem) throws StaleObjectException
    {
        final ActionableItemDto existingActionableItemDto = this.actionableItemDao.findOneWithLock(actionableItem.getId(), ActionableItemDto.class,
                StaleCheckType.ACTIONABLE_ITEM);

        setStatus(actionableItem);

        StaleCheckUtils.checkNotStale(existingActionableItemDto, actionableItem, StaleCheckType.ACTIONABLE_ITEM);

        return this.actionableItemDao.updateActionableItem(assetId, actionableItem);
    }

    @Override
    public Boolean deleteActionableItem(final Long actionableItemId) throws RecordNotFoundException
    {
        this.actionableItemDao.deleteActionableItem(actionableItemId);
        return !this.actionableItemDao.actionableItemExists(actionableItemId);
    }

    @Override
    public PageResource<ActionableItemDto> getActionableItemsByQuery(final Long assetId, final Integer page, final Integer size,
            final String sort, final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.actionableItemDao.findAllForAsset(pageSpecification, assetId, cleanDto);
    }

    @Override
    public PageResource<ActionableItemDto> getWIPActionableItemsByQuery(final Long jobId, final Integer page, final Integer size,
            final String sort, final String order, final CodicilDefectQueryDto queryDto) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        final CodicilDefectQueryDto cleanDto = QueryDtoUtils.sanitiseQueryDto(queryDto, CodicilDefectQueryDto.class);
        return this.wipActionableItemDao.findAllForJob(pageSpecification, jobId, cleanDto);
    }

    private void setStatus(final ActionableItemDto actionableItem)
    {
        if (actionableItem.getStatus() == null)
        {
            actionableItem.setStatus(new LinkResource(
                    this.dateHelper.isTodayOrBeforeStrict(actionableItem.getImposedDate()) ? CodicilStatusType.AI_OPEN.getValue()
                            : CodicilStatusType.AI_INACTIVE.getValue()));
        }
    }

    @Override
    public PageResource<ActionableItemDto> getWIPActionableItemsForJobId(final Integer page, final Integer size, final String sort,
            final String order, final Long jobId, final Long statusId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipActionableItemDao.getActionableItemsForJob(pageSpecification, jobId, statusId);
    }

    @Override
    public ActionableItemDto getWIPActionableItem(final Long id) throws RecordNotFoundException
    {
        final ActionableItemDto actionableItemDto = this.wipActionableItemDao.getActionableItem(id);
        this.serviceHelper.verifyModel(actionableItemDto, id);
        return actionableItemDto;
    }

    @Override
    public ActionableItemDto createWIPActionableItem(final ActionableItemDto actionableItem)
    {
        setStatus(actionableItem);
        return this.wipActionableItemDao.createActionableItem(actionableItem);
    }

    @Override
    public ActionableItemDto updateWIPActionableItem(final ActionableItemDto actionableItem) throws StaleObjectException
    {
        final ActionableItemDto existingActionableItemDto = this.wipActionableItemDao.findOneWithLock(actionableItem.getId(), ActionableItemDto.class,
                StaleCheckType.ACTIONABLE_ITEM);

        setStatus(actionableItem);

        StaleCheckUtils.checkNotStale(existingActionableItemDto, actionableItem, StaleCheckType.ACTIONABLE_ITEM);

        return this.wipActionableItemDao.updateActionableItem(actionableItem);
    }

    @Override
    public Boolean deleteWIPActionableItem(final Long actionableItemId)
    {
        this.wipActionableItemDao.deleteActionableItem(actionableItemId);
        return !this.wipActionableItemDao.actionableItemExists(actionableItemId);
    }

    @Override
    public PageResource<ActionableItemDto> getWIPActionableItemsForJobWIPDefect(final Integer page, final Integer size, final String sort,
            final String order, final Long jobId,
            final Long defectId) throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        return this.wipActionableItemDao.getActionableItemForJob(pageSpecification, jobId, defectId);
    }

    @Override
    public List<ActionableItemDto> getActionableItemsForAService(final Long serviceId) throws BadPageRequestException
    {
        return this.actionableItemDao.getActionableItemsForAService(serviceId);
    }
}
