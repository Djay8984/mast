package com.baesystems.ai.lr.service.ihs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.ihs.IhsContactDetailsDao;
import com.baesystems.ai.lr.dto.ihs.CompanyContactDto;
import com.baesystems.ai.lr.dto.ihs.IhsPersonnelContactDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.jcabi.aspects.Loggable;

@Service(value = "IhsAddressService")
@Loggable(Loggable.DEBUG)
public class IhsAddressServiceImpl implements IhsAddressService
{
    @Autowired
    private IhsContactDetailsDao ihsContactDetailsDao;

    @Override
    public List<CompanyContactDto> getAddress(final Long imoNumber) throws RecordNotFoundException
    {
        final List<CompanyContactDto> addresses = ihsContactDetailsDao.getAddress(imoNumber);
        if (addresses == null || addresses.isEmpty())
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, imoNumber));
        }
        return addresses;
    }

    @Override
    public List<IhsPersonnelContactDto> getPersonnelAddress(final Long imoNumber) throws RecordNotFoundException
    {
        final List<IhsPersonnelContactDto> addresses = ihsContactDetailsDao.getPersonnelAddress(imoNumber);
        if (addresses == null || addresses.isEmpty())
        {
            throw new RecordNotFoundException(String.format(ExceptionMessagesUtils.NOT_FOUND_ERROR_MSG, imoNumber));
        }
        return addresses;
    }
}
