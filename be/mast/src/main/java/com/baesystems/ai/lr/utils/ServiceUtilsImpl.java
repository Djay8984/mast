package com.baesystems.ai.lr.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.security.rbac.authentication.UserPrincipal;

@Component
public class ServiceUtilsImpl implements ServiceUtils
{
    /**
     * Gets the current user username.
     *
     * @return {@link String} username
     */
    @Override
    public final String getSecurityContextPrincipal()
    {
        String username = null;
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated())
        {
            final UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            username = userPrincipal.getDisplayName();

            if (userPrincipal.getDisplayName() == null)
            {
                username = userPrincipal.getUserName();
            }
        }
        return username;
    }

    /**
     * Gets the current list of groups assigned to the user.
     *
     * @return List of {@link String} groups.
     */
    @Override
    public List<String> getUserGroups()
    {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        @SuppressWarnings("unchecked")
        final Collection<GrantedAuthority> groups = (Collection<GrantedAuthority>) authentication.getAuthorities();

        final List<String> groupList = new ArrayList<String>();

        if (groups != null)
        {
            for (final GrantedAuthority auth : groups)
            {
                groupList.add(auth.getAuthority());
            }
        }

        return groupList;
    }
}
