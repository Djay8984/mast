package com.baesystems.ai.lr.service.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCodicilDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.attachments.AttachmentService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ReportSubmissionHelper;

@Service("ReportSubmissionService")
public class ReportSubmissionServiceImpl implements ReportSubmissionService
{
    @Autowired
    private SurveyDao surveyDao;

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private WIPWorkItemDao wipWorkItemDao;

    @Autowired
    private WorkItemDao workItemDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private DefectDao defectDao;

    @Autowired
    private WIPDefectDao wipDefectDao;

    @Autowired
    private JobDao jobDao;

    @Autowired
    private WIPCodicilDao wipCodicilDao;

    @Autowired
    private CoCDao coCDao;

    @Autowired
    private WIPCoCDao wipCoCDao;

    @Autowired
    private AssetNoteDao assetNoteDao;

    @Autowired
    private WIPAssetNoteDao wipAssetNoteDao;

    @Autowired
    private ActionableItemDao actionableItemDao;

    @Autowired
    private WIPActionableItemDao wipActionableItemDao;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private ReportSubmissionHelper reportSubmissionHelper;

    private final MapperFacade plainMapper;

    public ReportSubmissionServiceImpl()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.registerClassMap(factory.classMap(WorkItemDto.class, WorkItemLightDto.class).byDefault().toClassMap());
        this.plainMapper = factory.getMapperFacade();
    }

    /**
     * Method to copy actionable items from the wip version to the non wip-table. If the wip version has a parent id
     * then the parent is overwritten, if not a new one is created and the parent is set on the wip-version. The defect
     * id of the non-wip defect is also calculated from the defect id of the wip defect to which the wip actionable item
     * is linked.
     */
    @Override
    public void updateActionableItemsFollowingReport(final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        // get the wip actionable items
        final List<ActionableItemDto> actionableItems = this.wipActionableItemDao.getActionableItemsForJob(jobId);
        final Long assetId = this.jobDao.getAssetIdForJob(jobId);

        for (final ActionableItemDto wipActionableItem : actionableItems)
        {
            final ActionableItemDto actionableItem = this.plainMapper.map(wipActionableItem, ActionableItemDto.class);
            final Long wipId = wipActionableItem.getId();
            // get the id of the parent if there is one
            final Long actionableItemId = wipActionableItem.getParent() == null ? null : wipActionableItem.getParent().getId();
            actionableItem.setId(actionableItemId);
            // change the defect id to the one that exists in the non-wip table.
            actionableItem.setDefect(wipActionableItem.getDefect() == null ? null
                    : new LinkResource(this.wipDefectDao.getParentDefectId(wipActionableItem.getDefect().getId())));
            final ActionableItemDto result = this.actionableItemDao.createActionableItem(assetId, actionableItem);

            wipToNonWipIdMap.put(wipId, result.getId());

            if (actionableItemId == null)
            { // if a new parent had to be created then recored its id on the wip version
                wipActionableItem.setParent(new LinkResource(result.getId()));
                this.wipActionableItemDao.updateActionableItem(wipActionableItem);
            }
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.WIP_ACTIONABLE_ITEM,
                AttachmentTargetType.ACTIONABLE_ITEM);
    }

    /**
     * Method to copy asset notes from the wip version to the non wip-table. If the wip version has a parent id then the
     * parent is overwritten, if not a new one is created and the parent is set on the wip-version.
     */
    @Override
    public void updateAssetNotesFollowingReport(final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        final List<AssetNoteDto> notes = this.wipAssetNoteDao.getAssetNotesForJob(jobId);

        for (final AssetNoteDto note : notes)
        {
            final Long wipId = note.getId();
            // get the id of the parent if there is one
            final Long noteId = note.getParent() == null ? null : note.getParent().getId();
            note.setId(noteId);
            final AssetNoteDto result = this.assetNoteDao.saveAssetNote(note);

            wipToNonWipIdMap.put(wipId, result.getId());

            if (noteId == null)
            { // if a new parent had to be created then recored its id on the wip version
                note.setId(wipId);
                note.setParent(new LinkResource(result.getId()));

                this.wipAssetNoteDao.saveAssetNote(note);
            }
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.WIP_ASSET_NOTE,
                AttachmentTargetType.ASSET_NOTE);
    }

    /**
     * Method to copy cocs from the wip version to the non wip-table. If the wip version has a parent id then the parent
     * is overwritten, if not a new one is created and the parent is set on the wip-version. The defect id of the
     * non-wip defect is also calculated from the defect id of the wip defect to which the wip actionable item is
     * linked.
     */
    @Override
    public void updateCoCsFollowingReport(final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        final List<CoCDto> cocs = this.wipCoCDao.getCoCsForJob(jobId);
        final Long assetId = this.jobDao.getAssetIdForJob(jobId);

        for (final CoCDto wipCoC : cocs)
        {
            final CoCDto coc = this.plainMapper.map(wipCoC, CoCDto.class);
            final Long wipId = wipCoC.getId();
            // get the id of the parent if there is one
            final Long cocId = wipCoC.getParent() == null ? null : wipCoC.getParent().getId();
            coc.setId(cocId);
            // get the id of the parent of the wip defect the wip coc is linked to.
            coc.setDefect(wipCoC.getDefect() == null ? null : new LinkResource(this.wipDefectDao.getParentDefectId(wipCoC.getDefect().getId())));
            final CoCDto result = this.coCDao.saveCoC(coc, assetId);

            wipToNonWipIdMap.put(wipId, result.getId());

            if (cocId == null)
            { // if a new parent had to be created then recored its id on the wip version
                wipCoC.setParent(new LinkResource(result.getId()));
                this.wipCoCDao.updateCoC(wipCoC);
            }
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.WIP_COC, AttachmentTargetType.COC);
    }

    /**
     * Method to update tasks following the submission of a FAR, mapping the wip tasks on the job back to the asset task
     * table.
     * <p>
     * This method must be called after the updateSurveysFollowingReport method an all of the update codicil methods as
     * both are referenced on the task and the parents need to be in place and their ids recorded on the wip objects so
     * that they can be mapped when the tasks are mapped.
     */
    @Override
    public void updateTasksFollowingReport(final Long jobId) throws BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        final List<WorkItemDto> wipTasks = this.wipWorkItemDao.getTasksForJob(jobId);

        for (final WorkItemDto wipTask : wipTasks)
        {
            final Long wipId = wipTask.getId();
            // get the id of the parent if there is one
            final Long taskId = wipTask.getParent() == null ? null : wipTask.getParent().getId();
            WorkItemLightDto task = mapWipTaskToTask(wipTask);

            task = this.workItemDao.createOrUpdateTask(task);

            wipToNonWipIdMap.put(wipId, task.getId());

            if (taskId == null)
            { // if a new parent had to be created then recored its id on the wip version
                wipTask.setParent(new LinkResource(task.getId()));

                this.wipWorkItemDao.updateTask(this.plainMapper.map(wipTask, WorkItemLightDto.class));
            }
        }

        // once all of the tasks have been saved update any relationships (because there are two field this cannot be
        // done above using the hierarchy because it introduces the potential for an infinite loop)
        for (final WorkItemDto wipTask : wipTasks)
        {
            if (wipTask.getConditionalParent() != null || wipTask.getWorkItem() != null)
            { // skip any tasks that don't have relationships
              // copy from the wip task again to avoid looking up.
                final WorkItemLightDto task = mapWipTaskToTask(wipTask);

                if (wipTask.getConditionalParent() != null)
                { // map the conditional parent id to the id of the equivalent task in the asset table
                    final WorkItemLightDto conditionalPatent = new WorkItemLightDto();
                    conditionalPatent.setId(wipToNonWipIdMap.get(wipTask.getConditionalParent().getId()));
                    task.setConditionalParent(conditionalPatent);
                }

                if (wipTask.getWorkItem() != null)
                { // map the work item id to the id of the equivalent task in the asset table
                    final WorkItemLightDto child = new WorkItemLightDto();
                    child.setId(wipToNonWipIdMap.get(wipTask.getWorkItem().getId()));
                    task.setWorkItem(child);
                }

                // update the existing tasks (by this point it will have already been created and the id will be set
                // from the updated wip version). The create method is used here as it has the same effect wehen the id
                // is present and there is no update
                this.workItemDao.createOrUpdateTask(task);
            }
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.WIP_TASK,
                AttachmentTargetType.TASK);
    }

    /**
     * Method to update services following the submission of a FAR, mapping the surveys on the job back to the asset
     * service table.
     * <p>
     * This method needs to be called before the update tasks method as the tasks reference services.
     */
    @Override
    public void updateSurveysFollowingReport(final Long jobId, final Boolean provisionalDates) throws RecordNotFoundException, BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        // get all the surveys for the job
        final List<SurveyDto> surveys = this.surveyDao.getSurveys(jobId);
        final Long assetId = this.jobDao.getAssetIdForJob(jobId);

        for (final SurveyDto survey : surveys)
        {
            ScheduledServiceDto service;

            if (survey.getScheduledService() == null)
            { // if the survey has no parent service then create a new service
                service = this.reportSubmissionHelper.serviceWipToNonWipMap(survey);

                // some of the mandatory fields on the service are not in the survey so these will be populated with the
                // defaults from the service catalogue
                final ServiceCatalogueDto serviceCatalogue = this.serviceDao.getServiceCatalogue(service.getServiceCatalogueId());

                // if the service catalogue cannot be found (this should not be possible)
                if (serviceCatalogue == null)
                {
                    throw new MastSystemException(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Service catalogue"));
                }

                service.setProvisionalDates(provisionalDates);
                service.setCyclePeriodicity(serviceCatalogue.getCyclePeriodicity());

                // get the product catalogue id of the new service
                final Long productCatalogueId = serviceCatalogue.getProductCatalogue() == null ? null
                        : serviceCatalogue.getProductCatalogue().getId();
                final Long schedulingRegimeId = serviceCatalogue.getSchedulingRegime() == null ? null
                        : serviceCatalogue.getSchedulingRegime().getId();
                service.setAsset(new LinkResource(assetId));

                // if this product does not exist then create it
                if (!this.productDao.isProductCatalogueSelectedForAsset(assetId, productCatalogueId, schedulingRegimeId))
                {
                    this.productDao.createProduct(assetId, productCatalogueId, schedulingRegimeId);
                }

                // save the new service
                service = this.serviceDao.saveService(service);

                // update the survey so that it references the new service.
                survey.setScheduledService(service);

                this.surveyDao.updateSurvey(survey);
            }
            else
            {
                // get the parent service
                service = survey.getScheduledService();

                // it should not be possible to change the service catalogue
                if (!service.getServiceCatalogueId().equals(survey.getServiceCatalogue().getId()))
                {
                    throw new MastSystemException(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "service catalogue"));
                }

                // overwrite the values on the service with those on the survey
                this.reportSubmissionHelper.generalWipToNonWipMap(survey, service);

                service.setProvisionalDates(provisionalDates);

                // set the asset just in case
                service.setAsset(new LinkResource(assetId));

                // update the service
                service = this.serviceDao.saveService(service);
            }

            wipToNonWipIdMap.put(survey.getId(), service.getId());
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.SURVEY,
                AttachmentTargetType.SERVICE);
    }

    /**
     * Method to update defects following the submission of a FAR, mapping the wip defects on the job back to the asset
     * table.
     * <p>
     * This method must be called before the update cocs method as the cocs references defects.
     */
    @Override
    public void updateDefectsFollowingReport(final Long jobId) throws RecordNotFoundException, BadPageRequestException
    {
        final Map<Long, Long> wipToNonWipIdMap = new HashMap<Long, Long>();
        final List<DefectDto> wipDefects = this.wipDefectDao.getDefectsForJob(jobId);
        final Long assetId = this.jobDao.getAssetIdForJob(jobId);

        for (final DefectDto wipDefect : wipDefects)
        { // loop over all the defects attached to the job
            final Long wipId = wipDefect.getId();
            final Long defectId = wipDefect.getParent() == null ? null : wipDefect.getParent().getId();

            // if there defect on the job has a parent defect then get that, if not create a new on from the wip-defect.
            final DefectDto defect = defectId == null ? this.reportSubmissionHelper.mapNewDefect(wipDefect)
                    : this.defectDao.getDefect(defectId);

            if (defectId != null)
            { // if there is an existing defect merge the fields with the wip-defect's
                this.reportSubmissionHelper.mergeDefects(wipDefect, defect);
            }
            else
            { // if not then the asset id on the defect will need to be set to the asset id for this job and this job
              // will have to be added to the list of jobs
                defect.setAsset(new LinkResource(assetId));
                defect.setJobs(new ArrayList<>());
                this.reportSubmissionHelper.addJob(defect.getJobs(), wipDefect.getJob().getId());
            }

            // create/update the defect
            final DefectDto result = this.defectDao.createDefect(defect);

            wipToNonWipIdMap.put(wipId, result.getId());

            if (defectId == null)
            { // if a new defect was created then update the wip-defect with the id of its parent defect.
                wipDefect.setId(wipId);
                wipDefect.setParent(new LinkResource(result.getId()));

                wipDefectDao.updateDefect(wipDefect);
            }
        }

        this.attachmentService.migrateAttachmentsFromWipEntities(wipToNonWipIdMap, AttachmentTargetType.WIP_DEFECT,
                AttachmentTargetType.DEFECT);
    }

    private WorkItemLightDto mapWipTaskToTask(final WorkItemDto wipTask)
    {
        return this.reportSubmissionHelper.mapWipTaskToTask(wipTask,
                wipTask.getCodicil() == null ? null : this.wipCodicilDao.getParentId(wipTask.getCodicil().getId()));
    }
}
