package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.tasks.TaskReferenceDataDao;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.jcabi.aspects.Loggable;

@Service(value = "TaskReferenceDataService")
@Loggable(Loggable.DEBUG)
public class TaskReferenceDataServiceImpl implements TaskReferenceDataService
{
    @Autowired
    private TaskReferenceDataDao taskReferenceDataDao;

    @Override
    public List<ReferenceDataDto> getPostponementTypes()
    {
        return this.taskReferenceDataDao.getPostponementTypes();
    }
}
