package com.baesystems.ai.lr.service.jobs;

/**
 * The purpose of this class is to hold known Service Catalogue Code constants
 */
public final class ServiceCatalogueCodes
{
    private ServiceCatalogueCodes()
    {
    }

    public static final String AMSC = "AMSC";
}
