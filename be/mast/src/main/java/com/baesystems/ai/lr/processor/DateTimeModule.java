package com.baesystems.ai.lr.processor;

import java.util.Date;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class DateTimeModule extends SimpleModule
{
    private static final long serialVersionUID = -727633465747252611L;

    public DateTimeModule()
    {
        super();
        addSerializer(Date.class, new MastDateSerializer());
        addDeserializer(Date.class, new MastDateDeserializer());
    }
}
