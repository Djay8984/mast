package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.employees.EmployeeReferenceDataDao;
import com.baesystems.ai.lr.dto.references.EmployeeOfficeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.jcabi.aspects.Loggable;

@Service(value = "EmployeeReferenceDataService")
@Loggable(Loggable.DEBUG)
public class EmployeeReferenceDataServiceImpl implements EmployeeReferenceDataService
{
    @Autowired
    private EmployeeReferenceDataDao employeeReferenceDataDao;

    @Override
    public List<ReferenceDataDto> getEmployeeRoles()
    {
        return this.employeeReferenceDataDao.getEmployeeRoles();
    }

    @Override
    public ReferenceDataDto getEmployeeRole(final Long roleId)
    {
        return this.employeeReferenceDataDao.getEmployeeRole(roleId);
    }

    @Override
    public List<EmployeeOfficeDto> getEmployeeOffices()
    {
        return this.employeeReferenceDataDao.getEmployeeOffices();
    }
}
