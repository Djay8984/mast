package com.baesystems.ai.lr.service.spellcheck;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MissingDictionaryException;
import com.jcabi.aspects.Loggable;

@Service(value = "DictionaryService")
@Loggable(Loggable.DEBUG)
public class DictionaryServiceImpl implements DictionaryService
{
    public static final String DICTIONARY_FILENAME = "dictionary.zip";

    @Override
    public byte[] getDictionary(final String dictionaryPath) throws MastBusinessException
    {
        try
        {
            return Files.readAllBytes(new File(dictionaryPath + File.separator + DICTIONARY_FILENAME).toPath());
        }
        catch (final IOException ioException)
        {
            throw new MissingDictionaryException(dictionaryPath, ioException);
        }
    }
}
