package com.baesystems.ai.lr.service.employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.employees.SurveyorDao;
import com.baesystems.ai.lr.dto.employees.SurveyorDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "SurveyorService")
@Loggable(Loggable.DEBUG)
public class SurveyorServiceImpl implements SurveyorService
{
    @Autowired
    private SurveyorDao surveyorDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final PageResource<SurveyorDto> getSurveyors(final Integer page, final Integer size, final String sort, final String order,
            final String searchString, final Long officeId)
            throws BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);
        PageResource<SurveyorDto> results = null;

        if (searchString == null && officeId == null)
        {
            results = this.surveyorDao.getSurveyors(pageSpecification);
        }
        else
        {
            final String search = QueryDtoUtils.escapeSpecialCharsAndConvertWildcards(searchString);
            results = this.surveyorDao.getSurveyors(pageSpecification, search, officeId);
        }
        return results;
    }

    @Override
    public SurveyorDto getSurveyor(final Long surveyorId) throws RecordNotFoundException
    {
        final SurveyorDto surveyor = this.surveyorDao.getSurveyor(surveyorId);
        this.serviceHelper.verifyModel(surveyor, surveyorId);

        return surveyor;
    }
}
