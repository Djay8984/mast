package com.baesystems.ai.lr.service.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.jcabi.aspects.Loggable;

@Service(value = "TaskRuleService")
@Loggable(Loggable.DEBUG)
public class TaskRuleServiceImpl implements TaskRuleService
{
    @Autowired
    private WorkItemDao workItemDao;

    @Autowired
    private TaskHelper taskHelper;

    @Override
    public void syncTasksForAsset(final Long assetId)
    {
        final List<WorkItemLightDto> existingTasks = this.workItemDao.getLightTasksForAsset(assetId);
        final List<WorkItemLightDto> tasksFromRulesEngine = this.taskHelper.getTaskListForAsset(assetId);
        final List<WorkItemLightDto> resultingTasks = buildFinalTaskList(existingTasks, tasksFromRulesEngine);

        generateConditionalTasks(resultingTasks);

        resultingTasks.forEach(task -> this.workItemDao.createOrUpdateTask(task));
    }

    @Override
    public void syncTasksForScheduledService(final Long assetId, final Long scheduledServiceId)
    {
        final List<WorkItemLightDto> existingTasks = this.workItemDao.getTasksForService(scheduledServiceId);
        final List<WorkItemLightDto> tasksFromRulesEngine = this.taskHelper.getTaskListForService(assetId, scheduledServiceId);
        final List<WorkItemLightDto> resultingTasks = buildFinalTaskList(existingTasks, tasksFromRulesEngine);

        resultingTasks.forEach(task -> this.workItemDao.createOrUpdateTask(task));
    }

    /**
     * Generates conditional tasks for a given task list.
     *
     * @param resultingTasks updated list of tasks with conditional tasks
     */
    private void generateConditionalTasks(final List<WorkItemLightDto> resultingTasks)
    {
        // Identify the new work items and get its the conditional tasks
        final List<WorkItemLightDto> newTasks = getNonPersistedTaskList(resultingTasks);

        for (final WorkItemLightDto newTask : newTasks == null ? Collections.<WorkItemLightDto>emptyList() : newTasks)
        {
            if (newTask.getAssetItem() != null && newTask.getWorkItemAction() != null)
            {
                final WorkItemConditionalAttributeDto workItemConditionalAttribute = this.workItemDao.getWorkItemConditionalAttribute(
                        newTask.getServiceCode(), newTask.getAssetItem().getId(), newTask.getWorkItemAction().getId());

                // Create new work item attribute
                if (workItemConditionalAttribute != null)
                {
                    final WorkItemAttributeDto workItemAttribute = new WorkItemAttributeDto();

                    workItemAttribute.setAttributeDataType(workItemConditionalAttribute.getAttributeDataType());
                    workItemAttribute.setWorkItemConditionalAttribute(workItemConditionalAttribute);
                    workItemAttribute.setDescription(workItemConditionalAttribute.getDescription());
                    newTask.setAttributes(newTask.getAttributes() != null ? newTask.getAttributes() : new ArrayList<WorkItemAttributeDto>());
                    newTask.getAttributes().add(workItemAttribute);
                }
            }
        }
    }

    /**
     * Gets a list of not yet persisted work items from a list that can contain both existing and new work items.
     *
     * @param taskList List of merged tasks
     * @return List of new generated work items (tasks)
     */
    private List<WorkItemLightDto> getNonPersistedTaskList(final List<WorkItemLightDto> taskList)
    {
        return taskList.stream()
                .filter(task -> task.getId() == null)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<WorkItemLightDto> buildFinalTaskList(final List<WorkItemLightDto> existingTasks, final List<WorkItemLightDto> tasksFromRulesEngine)
    {
        final List<WorkItemLightDto> resultingTasks = new ArrayList<>();
        for (final WorkItemLightDto generatedTask : tasksFromRulesEngine)
        {
            boolean added = false;
            for (final WorkItemLightDto existingTask : existingTasks)
            {
                if (!added && exists(existingTask, generatedTask))
                {
                    // task already exists, update and add to the final list
                    this.mapTask(generatedTask, existingTask);
                    resultingTasks.add(existingTask);
                    added = true;
                }
            }
            // Add brand new Tasks
            if (!added)
            {
                resultingTasks.add(generatedTask);
            }
        }
        return resultingTasks;
    }

    /**
     * Add to this method any updates that need to be applied to existing Tasks from a corresponding "fresh" Rules
     * generated Task
     */
    @SuppressWarnings("unused")
    private void mapTask(final WorkItemLightDto generatedTask, final WorkItemLightDto existingTask)
    {
        // Nothing currently requiring update
    }

    private boolean exists(final WorkItemLightDto existingTask, final WorkItemLightDto generatedTask)
    {
        return Objects.equals(existingTask.getAssetItem().getId(), generatedTask.getAssetItem().getId())
                && Objects.equals(existingTask.getScheduledService().getId(), generatedTask.getScheduledService().getId())
                && Objects.equals(existingTask.getReferenceCode(), generatedTask.getReferenceCode());
    }
}
