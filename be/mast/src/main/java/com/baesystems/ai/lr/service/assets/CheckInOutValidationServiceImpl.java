package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dto.assets.AssetMetaDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ServiceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CheckInOutValidationService")
public class CheckInOutValidationServiceImpl implements CheckInOutValidationService
{
    @Autowired
    private AssetDao assetDao;

    @Autowired
    private ServiceUtils serviceUtils;

    @Override
    public void verifyEligibleForCheckout(final Long assetId) throws BadRequestException
    {
        final String userId = this.serviceUtils.getSecurityContextPrincipal();
        final AssetMetaDto assetMetaDto = this.assetDao.getAssetMeta(assetId);
        final String checkedOutBy = assetMetaDto.getCheckedOutBy();
        if (checkedOutBy != null && !checkedOutBy.equals(userId))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.ASSET_IS_CHECKED_OUT_BY_ANOTHER_USER, assetId, checkedOutBy));
        }
        if (checkedOutBy != null && checkedOutBy.equals(userId))
        {
            throw new BadRequestException(String.format(ExceptionMessagesUtils.ASSET_CHECKED_OUT_BY_SAME_USER, assetId, checkedOutBy));
        }
    }

    @Override
    public void verifyIsCheckedOut(final Long assetId) throws BadRequestException
    {
        final AssetMetaDto assetMetaDto = this.assetDao.getAssetMeta(assetId);
        final String checkedOutBy = assetMetaDto.getCheckedOutBy();
        if (checkedOutBy == null || !checkedOutBy.equals(this.serviceUtils.getSecurityContextPrincipal()))
        {
            throw new BadRequestException(checkedOutBy == null
                    ? String.format(ExceptionMessagesUtils.ASSET_IS_NOT_CHECKED_OUT, assetId)
                    : String.format(ExceptionMessagesUtils.ASSET_IS_CHECKED_OUT_BY_ANOTHER_USER, assetId, checkedOutBy));
        }
    }
}
