package com.baesystems.ai.lr.service.flagports;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.flagports.PortDao;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.PortOfRegistryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "PortService")
@Loggable(Loggable.DEBUG)
public class PortServiceImpl implements PortService
{
    @Autowired
    private PortDao portDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public final PortOfRegistryDto getPortOfRegistry(final Long portOfRegistryId) throws RecordNotFoundException
    {
        final PortOfRegistryDto portOfRegistry = this.portDao.getPortOfRegistry(portOfRegistryId);
        this.serviceHelper.verifyModel(portOfRegistry, portOfRegistryId);

        return portOfRegistry;
    }

    @Override
    public PageResource<PortOfRegistryDto> getPorts(final Integer page, final Integer size, final String sort, final String order,
            final Long flagState)
            throws BadRequestException, BadPageRequestException
    {
        final Pageable pageSpecification = this.serviceHelper.createPageable(page, size, sort, order);

        PageResource<PortOfRegistryDto> result = null;

        if (flagState == null)
        {
            result = this.portDao.findAll(pageSpecification);
        }
        else
        {
            result = this.portDao.findAll(pageSpecification, flagState);
        }

        return result;
    }

    @Override
    public List<PortOfRegistryDto> getPortList()
    {
        return this.portDao.getPorts();
    }
}
