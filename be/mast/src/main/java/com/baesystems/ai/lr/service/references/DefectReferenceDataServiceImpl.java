package com.baesystems.ai.lr.service.references;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baesystems.ai.lr.dao.references.defects.DefectReferenceDataDao;
import com.baesystems.ai.lr.domain.mast.entities.references.DefectCategoryDO;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.DefectDetailDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.jcabi.aspects.Loggable;

@Service(value = "DefectReferenceDataService")
@Loggable(Loggable.DEBUG)
public class DefectReferenceDataServiceImpl implements DefectReferenceDataService
{
    @Autowired
    private DefectReferenceDataDao defectReferenceDataDao;

    @Autowired
    private ServiceHelper serviceHelper;

    @Override
    public List<DefectCategoryDto> getDefectCategories()
    {
        return this.defectReferenceDataDao.getDefectCategories();
    }

    @Override
    public DefectCategoryDto getDefectCategory(final Long defectCategoryId) throws RecordNotFoundException
    {
        final DefectCategoryDto defectCategory = this.defectReferenceDataDao.getDefectCategory(defectCategoryId);
        this.serviceHelper.verifyModel(defectCategory, defectCategoryId);

        return defectCategory;
    }

    @Override
    public String getDefectCategoryLetter(final DefectCategoryDto defectCategoryDto) throws RecordNotFoundException
    {
        final DefectCategoryDO defectCategoryDO = this.defectReferenceDataDao.getDefectCategoryDO(defectCategoryDto.getId());
        this.serviceHelper.verifyModel(defectCategoryDO, defectCategoryDto.getId());

        return defectCategoryDO.getCategoryLetter() != null
                ? defectCategoryDO.getCategoryLetter()
                : defectCategoryDO.getParent().getCategoryLetter();
    }

    @Override
    public List<DefectDetailDto> getDefectDetails()
    {
        return this.defectReferenceDataDao.getDefectDetails();
    }

    @Override
    public List<ReferenceDataDto> getDefectStatuses()
    {
        return this.defectReferenceDataDao.getDefectStatuses();
    }

    @Override
    public ReferenceDataDto getDefectStatus(final Long defectStatusId) throws RecordNotFoundException
    {
        final ReferenceDataDto defectStatus = this.defectReferenceDataDao.getDefectStatus(defectStatusId);
        this.serviceHelper.verifyModel(defectStatus, defectStatusId);

        return defectStatus;
    }
}
