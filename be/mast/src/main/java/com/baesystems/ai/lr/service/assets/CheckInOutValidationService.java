package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.exception.BadRequestException;

public interface CheckInOutValidationService
{
    void verifyEligibleForCheckout(Long assetId) throws BadRequestException;
    void verifyIsCheckedOut(Long assetId) throws BadRequestException;
}
