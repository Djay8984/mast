package com.baesystems.ai.lr.service.cases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneListDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.enums.MilestoneDueDateReferenceType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;

@RunWith(MockitoJUnitRunner.class)
public class CaseMilestoneServiceImplTest
{
    @Mock
    private CaseMilestoneDao caseMilestoneDao;

    @Mock
    private CaseDao caseDao;

    @Mock
    private CaseReferenceDataDao caseReferenceDataDao;

    @Mock
    private DateHelper dateUtils;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @InjectMocks
    private final CaseMilestoneServiceImpl caseMilestoneServiceDatesSkipped = new CaseMilestoneServiceImpl()
    {
        @Override
        public void updateMilestoneDates(final Long caseId, final List<CaseMilestoneDto> caseMilestonesList, final Date caseAcceptenceDateArg,
                final Date estimatedBuildDateArg)
        {
            // skipped for mocks
        }
    };

    @InjectMocks
    private final CaseMilestoneServiceImpl caseMilestoneService = new CaseMilestoneServiceImpl();

    private static final int ONE = 1;
    private static final int ONE_HUNDRED = 100;
    private static final Long CASE_1_ID = 1L;
    private static final Long MILESTONE_1_ID = 1L;
    private static final Long MILESTONE_2_ID = 2L;
    private static final Long CASE_STATUS_1_ID = 1L;
    private static final Long MILESTONE_STATUS_COMPLETE_ID = 2L;
    private static final Integer TEN = 10;
    private static final Date COMPLETION_DATE = new Date();
    private static Date caseAcceptenceDate;
    private static Date estimatedBuildDate;
    private static final List<Long> CHILD_ID_LIST = new ArrayList<Long>();

    private CaseMilestoneListDto caseMilestoneList;
    private List<CaseMilestoneDto> caseMilestones;

    private Date adjustDate(final Date startDate, final Integer increment, final int incrementType)
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(incrementType, increment);
        return calendar.getTime();
    }

    @Before
    public void setUp() throws Exception
    {
        caseMilestones = new ArrayList<CaseMilestoneDto>();
        caseMilestones.add(new CaseMilestoneDto());
        caseMilestones.get(0).setId(MILESTONE_1_ID);
        caseMilestones.get(0).setMilestone(new LinkResource(MILESTONE_1_ID));
        caseMilestones.get(0).setaCase(new LinkResource(CASE_1_ID));

        caseMilestoneList = new CaseMilestoneListDto();
        caseMilestoneList.setCaseMilestoneList(caseMilestones);

        CHILD_ID_LIST.add(MILESTONE_1_ID);

        final Random random = new Random();

        caseAcceptenceDate = this.adjustDate(new Date(), -(random.nextInt(ONE_HUNDRED) + ONE), Calendar.DAY_OF_MONTH);
        estimatedBuildDate = this.adjustDate(new Date(), -(random.nextInt(ONE_HUNDRED) + ONE), Calendar.DAY_OF_MONTH);
    }

    @Test
    public void getMilestonesForCase() throws BadRequestException, MastBusinessException
    {
        when(this.caseMilestoneDao.getMilestonesForCase(CASE_1_ID)).thenReturn(new CaseMilestoneListDto());

        assertNotNull(this.caseMilestoneServiceDatesSkipped.getMilestonesForCase(CASE_1_ID));
    }

    @Test
    public void updateMilestonesForCase() throws BadRequestException, MastBusinessException
    {
        when(this.caseMilestoneDao.updateMilestone(CASE_1_ID, caseMilestones.get(0))).thenReturn(caseMilestones.get(0));
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(new CaseWithAssetDetailsDto());

        this.caseMilestoneServiceDatesSkipped.updateMilestonesForCase(CASE_1_ID, caseMilestoneList);
    }

    @Test
    public void updateMilestonesForCaseUpdateCase() throws BadRequestException, MastBusinessException
    {
        caseMilestones.get(0).setMilestoneStatus(new LinkResource(MILESTONE_STATUS_COMPLETE_ID));

        when(this.caseMilestoneDao.updateMilestone(CASE_1_ID, caseMilestones.get(0))).thenReturn(caseMilestones.get(0));
        doNothing().when(this.caseDao).updateCaseStatus(CASE_1_ID, CASE_STATUS_1_ID);
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(new CaseWithAssetDetailsDto());

        this.caseMilestoneServiceDatesSkipped.updateMilestonesForCase(CASE_1_ID, caseMilestoneList);
    }

    @Test
    public void updateMilestonesWithChildMilestonesForCase() throws BadRequestException, MastBusinessException
    {
        caseMilestones.get(0).setCompletionDate(new Date());

        when(this.caseMilestoneDao.updateMilestone(CASE_1_ID, caseMilestones.get(0))).thenReturn(caseMilestones.get(0));
        when(this.caseReferenceDataDao.getChildMilestoneIds(MILESTONE_1_ID)).thenReturn(CHILD_ID_LIST);
        when(this.caseMilestoneDao.getChildMilestones(CHILD_ID_LIST, CASE_1_ID)).thenReturn(caseMilestones);
        when(this.caseReferenceDataDao.getMilestonesWorkingDaysOffset(MILESTONE_1_ID)).thenReturn(TEN);
        when(this.dateUtils.adjustDate(caseMilestones.get(0).getCompletionDate(), TEN, Calendar.DAY_OF_MONTH)).thenReturn(new Date());
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(new CaseWithAssetDetailsDto());

        this.caseMilestoneServiceDatesSkipped.updateMilestonesForCase(CASE_1_ID, caseMilestoneList);
    }

    @Test
    public void updateMilestonesForCaseMergeExistingList() throws BadRequestException, MastBusinessException
    {
        final List<CaseMilestoneDto> existingList = new ArrayList<CaseMilestoneDto>();
        existingList.add(new CaseMilestoneDto());
        existingList.get(0).setId(MILESTONE_2_ID);
        existingList.addAll(caseMilestoneList.getCaseMilestoneList());

        final CaseMilestoneListDto existingListObject = new CaseMilestoneListDto();
        existingListObject.setCaseMilestoneList(existingList);

        when(this.caseMilestoneDao.updateMilestone(CASE_1_ID, caseMilestones.get(0))).thenReturn(caseMilestones.get(0));
        when(this.caseMilestoneDao.getMilestonesForCase(CASE_1_ID)).thenReturn(existingListObject);
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(new CaseWithAssetDetailsDto());

        this.caseMilestoneServiceDatesSkipped.updateMilestonesForCase(CASE_1_ID, caseMilestoneList);
    }

    @Test
    public void updateMilestonesForCaseNullTests() throws BadRequestException, MastBusinessException
    {
        final CaseMilestoneListDto existingListObject = new CaseMilestoneListDto();
        existingListObject.setCaseMilestoneList(null);

        when(this.caseMilestoneDao.updateMilestone(CASE_1_ID, caseMilestones.get(0))).thenReturn(caseMilestones.get(0));
        when(this.caseMilestoneDao.getMilestonesForCase(CASE_1_ID)).thenReturn(existingListObject);
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(new CaseWithAssetDetailsDto());

        this.caseMilestoneServiceDatesSkipped.updateMilestonesForCase(CASE_1_ID, null);
    }

    @Test
    public void updateMilestoneDatesComplete() throws MastBusinessException
    {
        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setCompletionDate(COMPLETION_DATE);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, estimatedBuildDate);

        // this is really to tests that this.caseMilestoneService.updateMilestoneDueDate is not called, but it is easer
        // to test the first mock in the method (this is guaranteed to be called if the method is called)
        verify(this.caseReferenceDataDao, never()).getMilestone(any(Long.class));
    }

    @Test
    public void updateMilestoneDatesCaseAcceptence() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.CASE_ACCEPTANCE_DATE.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);
        when(this.dateUtils.adjustDate(caseAcceptenceDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH))
                .thenReturn(this.adjustDate(caseAcceptenceDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH));

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, estimatedBuildDate);

        assertEquals(this.adjustDate(caseAcceptenceDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH), caseMilestone.getDueDate());
    }

    @Test
    public void updateMilestoneDatesCaseAcceptenceNull() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.CASE_ACCEPTANCE_DATE.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), null, estimatedBuildDate);

        assertNull(caseMilestone.getDueDate());
    }

    @Test
    public void updateMilestoneDatesEstimatedBuildDate() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.ESTIMATED_BUILD_DATE.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);
        when(this.dateUtils.adjustDate(estimatedBuildDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH))
                .thenReturn(this.adjustDate(estimatedBuildDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH));

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, estimatedBuildDate);

        assertEquals(this.adjustDate(estimatedBuildDate, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH), caseMilestone.getDueDate());
    }

    @Test
    public void updateMilestoneDatesEstimatedBuildDateNull() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.ESTIMATED_BUILD_DATE.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, null);

        assertNull(caseMilestone.getDueDate());
    }

    @Test
    public void updateMilestoneDatesMilestoneDate() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.MILESTONE_COMPLETION.getValue()));

        final CaseMilestoneDto caseMilestone1 = new CaseMilestoneDto();
        caseMilestone1.setId(MILESTONE_1_ID);
        caseMilestone1.setMilestone(new LinkResource(MILESTONE_1_ID));

        final CaseMilestoneDto caseMilestone2 = new CaseMilestoneDto();
        caseMilestone2.setId(MILESTONE_2_ID);
        caseMilestone2.setMilestone(new LinkResource(MILESTONE_2_ID));
        caseMilestone2.setCompletionDate(COMPLETION_DATE);

        final List<CaseMilestoneDto> milestoneList = new ArrayList<CaseMilestoneDto>();
        milestoneList.add(caseMilestone1);
        milestoneList.add(caseMilestone2);

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);
        when(this.dateUtils.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH))
                .thenReturn(this.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH));
        when(this.caseReferenceDataDao.getDateDependentParentMilestoneId(MILESTONE_1_ID)).thenReturn(MILESTONE_2_ID);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, milestoneList, caseAcceptenceDate, estimatedBuildDate);

        assertEquals(this.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH), milestoneList.get(0).getDueDate());
    }

    @Test
    public void updateMilestoneDatesMilestoneDateDependencyNotFound() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.MILESTONE_COMPLETION.getValue()));

        final CaseMilestoneDto caseMilestone1 = new CaseMilestoneDto();
        caseMilestone1.setId(MILESTONE_1_ID);
        caseMilestone1.setMilestone(new LinkResource(MILESTONE_1_ID));

        final List<CaseMilestoneDto> milestoneList = new ArrayList<CaseMilestoneDto>();
        milestoneList.add(caseMilestone1);

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);
        when(this.dateUtils.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH))
                .thenReturn(this.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH));
        when(this.caseReferenceDataDao.getDateDependentParentMilestoneId(MILESTONE_1_ID)).thenReturn(MILESTONE_2_ID);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, milestoneList, caseAcceptenceDate, estimatedBuildDate);

        assertNull(milestoneList.get(0).getDueDate());
    }

    @Test
    public void updateMilestoneDatesMilestoneDateNullCompletionDate() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.MILESTONE_COMPLETION.getValue()));

        final CaseMilestoneDto caseMilestone1 = new CaseMilestoneDto();
        caseMilestone1.setId(MILESTONE_1_ID);
        caseMilestone1.setMilestone(new LinkResource(MILESTONE_1_ID));

        final CaseMilestoneDto caseMilestone2 = new CaseMilestoneDto();
        caseMilestone2.setId(MILESTONE_2_ID);
        caseMilestone2.setMilestone(new LinkResource(MILESTONE_2_ID));

        final List<CaseMilestoneDto> milestoneList = new ArrayList<CaseMilestoneDto>();
        milestoneList.add(caseMilestone1);
        milestoneList.add(caseMilestone2);

        when(this.caseReferenceDataDao.getMilestone(any(Long.class))).thenReturn(milestone);
        when(this.dateUtils.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH))
                .thenReturn(this.adjustDate(COMPLETION_DATE, milestone.getWorkingDaysOffset(), Calendar.DAY_OF_MONTH));
        when(this.caseReferenceDataDao.getDateDependentParentMilestoneId(MILESTONE_1_ID)).thenReturn(MILESTONE_2_ID);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, milestoneList, caseAcceptenceDate, estimatedBuildDate);

        assertNull(milestoneList.get(0).getDueDate());
    }

    @Test
    public void updateMilestoneDatesManual() throws MastBusinessException
    {
        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.MANUAL.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(milestone);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, null);

        assertNull(caseMilestone.getDueDate());
    }

    @Test
    public void updateMilestoneDatesMilestoneNotFound() throws MastBusinessException
    {
        thrown.expect(MastSystemException.class);
        thrown.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "milestone"));

        final MilestoneDto milestone = new MilestoneDto();
        milestone.setId(MILESTONE_1_ID);
        milestone.setWorkingDaysOffset(TEN);
        milestone.setDueDateReference(new LinkResource(MilestoneDueDateReferenceType.MANUAL.getValue()));

        final CaseMilestoneDto caseMilestone = new CaseMilestoneDto();
        caseMilestone.setId(MILESTONE_1_ID);
        caseMilestone.setMilestone(new LinkResource(MILESTONE_1_ID));

        when(this.caseReferenceDataDao.getMilestone(MILESTONE_1_ID)).thenReturn(null);

        this.caseMilestoneService.updateMilestoneDates(CASE_1_ID, Collections.singletonList(caseMilestone), caseAcceptenceDate, estimatedBuildDate);
    }
}
