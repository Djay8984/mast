package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.baesystems.ai.lr.dto.paging.BasePageResource;
import com.baesystems.ai.lr.dto.paging.PageLinkDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.paging.PaginationDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;

public class PageResourceImplTest extends PageResourceImplBasedTest
{
    private static final Long NUMBER_OF_ELEMENTS = 25L;
    private static final Long PAGE_SIZE = 10L;

    private static final List<Object> EMPTY_LIST = new ArrayList<Object>();
    private static final List<Object> LIST = new ArrayList<Object>();
    private static final Page<Object> EMPTY_PAGE = new PageImpl<Object>(EMPTY_LIST);

    private static final PaginationDto PAGINATION_EMPTY = new PaginationDto();
    private static final PaginationDto PAGINATION = new PaginationDto();

    public static final Integer PAGE_0 = 0;
    public static final Integer PAGE_1 = 1;
    public static final Integer PAGE_2 = 2;

    @Mock
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @BeforeClass
    public static void startUp() throws BadPageRequestException
    {
        addElements(LIST, NUMBER_OF_ELEMENTS.intValue());
    }

    @Before
    public void setUp()
    {
        final PageLinkDto firstPageEmpty = new PageLinkDto();
        final PageLinkDto lastPageEmpty = new PageLinkDto();
        final PageLinkDto thisPageEmplty = new PageLinkDto();

        firstPageEmpty.setRel("first");
        firstPageEmpty.setHref("/mock?page=0&size=0");
        lastPageEmpty.setRel("last");
        lastPageEmpty.setHref("/mock?page=0&size=0");
        thisPageEmplty.setRel("self");
        thisPageEmplty.setHref("/mock?page=0&size=0");

        final List<PageLinkDto> linksEmpty = new ArrayList<PageLinkDto>();

        linksEmpty.add(firstPageEmpty);
        linksEmpty.add(lastPageEmpty);
        linksEmpty.add(thisPageEmplty);

        PAGINATION_EMPTY.setFirst(true);
        PAGINATION_EMPTY.setLast(true);
        PAGINATION_EMPTY.setLink(linksEmpty);
        PAGINATION_EMPTY.setNumber(0);
        PAGINATION_EMPTY.setNumberOfElements(0);
        PAGINATION_EMPTY.setSize(0);
        PAGINATION_EMPTY.setSort(null);
        PAGINATION_EMPTY.setTotalElements(0L);
        PAGINATION_EMPTY.setTotalPages(1);
    }

    private static void addElements(final List<Object> list, final int add)
    {
        for (int i = 0; i < add; ++i)
        {
            list.add(new Object());
        }
    }

    private void verifyPageLinks(final List<PageLinkDto> expected, final List<PageLinkDto> actual)
    {
        assertNotNull(actual);
        for (int i = 0; i < actual.size(); ++i)
        {
            assertNotNull(actual.get(i));
            assertEquals(expected.get(i).getHref(), actual.get(i).getHref());
            assertEquals(expected.get(i).getRel(), actual.get(i).getRel());
        }
    }

    private void verifyPagination(final PaginationDto expected, final PaginationDto actual)
    {
        assertNotNull(actual);
        assertEquals(expected.getFirst(), actual.getFirst());
        assertEquals(expected.getLast(), actual.getLast());
        verifyPageLinks(expected.getLink(), actual.getLink());
        assertEquals(expected.getNumber(), actual.getNumber());
        assertEquals(expected.getNumberOfElements(), actual.getNumberOfElements());
        assertEquals(expected.getSize(), actual.getSize());
        assertEquals(expected.getSort(), actual.getSort());
        assertEquals(expected.getTotalElements(), actual.getTotalElements());
        assertEquals(expected.getTotalPages(), actual.getTotalPages());
    }

    private void verifyPage(final PageResource<Object> pageResource, final PaginationDto pagination, final List<Object> content)
    {
        verifyPagination(pagination, pageResource.getPagination());
        assertEquals(content.size(), pageResource.getContent().size());
    }

    private Page<Object> setUpMultiPage(final Integer pageNumber, final Integer pageSize)
    {
        final Page<Object> page = new PageImpl<Object>(getSubList(LIST, pageNumber, pageSize), new PageRequest(pageNumber, pageSize),
                NUMBER_OF_ELEMENTS);

        final List<PageLinkDto> links = new ArrayList<PageLinkDto>();

        PAGINATION.setFirst(true);
        PAGINATION.setLast(true);

        if (page.hasPrevious())
        {
            final PageLinkDto previousPage = new PageLinkDto();

            previousPage.setRel("prev");
            previousPage.setHref("/mock?page=" + (page.getNumber() - 1) + "&size=" + pageSize);

            links.add(previousPage);
            PAGINATION.setFirst(false);
        }

        if (page.hasNext())
        {
            final PageLinkDto nextPage = new PageLinkDto();

            nextPage.setRel("next");
            nextPage.setHref("/mock?page=" + (page.getNumber() + 1) + "&size=" + pageSize);

            links.add(nextPage);
            PAGINATION.setLast(false);
        }

        final PageLinkDto firstPage = new PageLinkDto();
        final PageLinkDto lastPage = new PageLinkDto();
        final PageLinkDto thisPage = new PageLinkDto();

        firstPage.setRel("first");
        firstPage.setHref("/mock?page=" + 0 + "&size=" + pageSize);
        lastPage.setRel("last");

        lastPage.setHref("/mock?page=" + (page.getTotalPages() - 1) + "&size=" + pageSize);
        thisPage.setRel("self");
        thisPage.setHref("/mock?page=" + page.getNumber() + "&size=" + pageSize);

        links.add(firstPage);
        links.add(lastPage);
        links.add(thisPage);

        PAGINATION.setLink(links);
        PAGINATION.setNumber(page.getNumber());
        PAGINATION.setNumberOfElements(getSubList(LIST, pageNumber, pageSize).size());
        PAGINATION.setSize(pageSize);
        PAGINATION.setSort(null);
        PAGINATION.setTotalElements(NUMBER_OF_ELEMENTS);
        PAGINATION.setTotalPages(page.getTotalPages());

        return page;
    }

    private List<Object> getSubList(final List<Object> list, final Integer page, final Integer size)
    {
        Integer end = (page + 1) * size.intValue();
        if ((page + 1) * size.intValue() > NUMBER_OF_ELEMENTS.intValue())
        {
            end = NUMBER_OF_ELEMENTS.intValue();
        }
        return list.subList(page * size.intValue(), end);
    }

    @Test
    public void testGetPageResourceImplBasicEmpty()
    {
        final ObjectPageResourceDto result = pageFactory.createPageResource(EMPTY_PAGE, ObjectPageResourceDto.class);
        verifyPage(result, PAGINATION_EMPTY, EMPTY_LIST);
    }

    @Test
    public void testGetPageResourceImplBasic()
    {
        final ObjectPageResourceDto result = pageFactory.createPageResource(setUpMultiPage(0, NUMBER_OF_ELEMENTS.intValue()),
                ObjectPageResourceDto.class);
        verifyPage(result, PAGINATION, LIST);
    }

    @Test
    public final void testGetPageResourceImplMoreThanOne()
    {
        ObjectPageResourceDto pageResource = pageFactory.createPageResource(setUpMultiPage(PAGE_0, PAGE_SIZE.intValue()),
                ObjectPageResourceDto.class);

        verifyPage(pageResource, PAGINATION, getSubList(LIST, PAGE_0, PAGE_SIZE.intValue()));

        pageResource = pageFactory.createPageResource(setUpMultiPage(PAGE_0, PAGE_SIZE.intValue()),
                ObjectPageResourceDto.class);

        pageResource = pageFactory.createPageResource(setUpMultiPage(PAGE_1, PAGE_SIZE.intValue()), ObjectPageResourceDto.class);
        verifyPage(pageResource, PAGINATION, getSubList(LIST, PAGE_1, PAGE_SIZE.intValue()));

        pageResource = pageFactory.createPageResource(setUpMultiPage(PAGE_2, PAGE_SIZE.intValue()), ObjectPageResourceDto.class);
        verifyPage(pageResource, PAGINATION, getSubList(LIST, PAGE_2, PAGE_SIZE.intValue()));
    }

    public static final class ObjectPageResourceDto extends BasePageResource<Object>
    {
        private List<Object> content;

        @Override
        public List<Object> getContent()
        {
            return content;
        }

        @Override
        public void setContent(final List<Object> content)
        {
            this.content = content;
        }

    }
}
