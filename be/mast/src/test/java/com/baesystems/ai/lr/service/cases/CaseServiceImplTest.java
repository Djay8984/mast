package com.baesystems.ai.lr.service.cases;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.cases.CaseDao;
import com.baesystems.ai.lr.dao.cases.CaseMilestoneDao;
import com.baesystems.ai.lr.dao.references.cases.CaseReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetDto;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.cases.CaseDto;
import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.cases.CaseWithAssetDetailsDto;
import com.baesystems.ai.lr.dto.references.CaseStatusDto;
import com.baesystems.ai.lr.dto.references.MilestoneDto;
import com.baesystems.ai.lr.enums.CaseStatusType;
import com.baesystems.ai.lr.service.assets.AssetService;
import com.baesystems.ai.lr.service.references.CaseReferenceDataService;
import com.baesystems.ai.lr.service.validation.ValidationDataService;
import com.baesystems.ai.lr.service.validation.ValidationService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DateHelperImpl;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class CaseServiceImplTest
{
    private static final Long CASE_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long IMO_NUMBER = 1L;
    private static final Long CASE_TYPE_ID = 1L;
    private static final String YARD_NUMBER = "Yard 1";
    private static final String ITEM_NAME = "TestItem";
    private static final CaseStatusDto COMMITTED_CASE_STATUS = new CaseStatusDto();
    private static final LinkResource COMMITTED_CASE_STATUS_LINK = new LinkResource();
    private static final CaseStatusDto UNCOMMITTED_CASE_STATUS = new CaseStatusDto();
    private static final LinkResource UNCOMMITTED_CASE_STATUS_LINK = new LinkResource();
    private static final String COMMITTED_STATUS = CaseStatusType.POPULATING.name();
    private static final String UNCOMMITTED_STATUS = CaseStatusType.UNCOMMITTED.name();
    private static final LinkResource CASE_TYPE = new LinkResource();
    private static final MilestoneDto MILESTONE = new MilestoneDto();
    private static final List<MilestoneDto> MILESTONE_LIST = new ArrayList<MilestoneDto>();
    private final List<CaseMilestoneDto> expected = new ArrayList<CaseMilestoneDto>();

    private static final CaseWithAssetDetailsDto CASE_1 = new CaseWithAssetDetailsDto();
    private static final AssetDto ASSET_1 = new AssetDto();
    private static final ItemDto ITEM_1 = new ItemDto();
    private static final LazyItemDto LAZY_ITEM_1 = new LazyItemDto();
    private static final ItemMetaDto ITEM_1_META = new ItemMetaDto();

    @Mock
    private CaseDao caseDao;

    @Mock
    private ItemDao itemDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private AssetService assetService;

    @Mock
    private CaseMilestoneDao caseMilestoneDao;

    @Mock
    private CaseReferenceDataDao caseReferenceDataDao;

    @Mock
    private ValidationDataService validationDataService;

    @Mock
    private ValidationService validationService;

    @Mock
    private CaseReferenceDataService caseReferenceDataService;

    @Mock
    private final DateHelper dateHelper = new DateHelperImpl();

    @Mock
    private CaseMilestoneService caseMilestoneService;

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final CaseService caseService = new CaseServiceImpl();

    @Before
    public void setUp() throws Exception
    {
        ASSET_1.setId(ASSET_1_ID);
        ASSET_1.setYardNumber(YARD_NUMBER);
        ASSET_1.setIhsAsset(new LinkResource(IMO_NUMBER));
        ASSET_1.setBuilder("Builder Name");
        COMMITTED_CASE_STATUS.setName(COMMITTED_STATUS);
        COMMITTED_CASE_STATUS.setId(2L);
        COMMITTED_CASE_STATUS.setSynchroniseAsset(false);
        COMMITTED_CASE_STATUS_LINK.setId(COMMITTED_CASE_STATUS.getId());
        UNCOMMITTED_CASE_STATUS.setName(UNCOMMITTED_STATUS);
        UNCOMMITTED_CASE_STATUS.setId(1L);
        UNCOMMITTED_CASE_STATUS.setSynchroniseAsset(true);
        UNCOMMITTED_CASE_STATUS_LINK.setId(UNCOMMITTED_CASE_STATUS.getId());
        CASE_1.setMilestones(expected);
        MILESTONE.setWorkingDaysOffset(1);
        MILESTONE_LIST.add(MILESTONE);

        CASE_1.setId(CASE_1_ID);
        CASE_1.setAsset(new LinkResource(ASSET_1_ID));

        LAZY_ITEM_1.setId(ITEM_1_ID);
        LAZY_ITEM_1.setItemType(new LinkResource(ITEM_1_ID));
        LAZY_ITEM_1.setName(ITEM_NAME);
        LAZY_ITEM_1.setReviewed(false);

        ITEM_1.setId(ITEM_1_ID);
        ITEM_1.setItemType(new LinkResource(ITEM_1_ID));
        ITEM_1.setName(ITEM_NAME);
        ITEM_1.setReviewed(false);
    }

    @Test
    public void createMilestonesWhenCaseCommitted() throws Exception
    {
        CASE_1.setCaseStatus(COMMITTED_CASE_STATUS_LINK);
        CASE_1.setCaseType(CASE_TYPE);
        CASE_1.getCaseType().setId(CASE_TYPE_ID);
        CASE_1.setTocAcceptanceDate(new Date());
        CASE_1.setAsset(new LinkResource(ASSET_1_ID));

        when(this.itemDao.getRootItem(ASSET_1_ID, ASSET_1_ID, false)).thenReturn(LAZY_ITEM_1);
        when(this.itemDao.getItemWithFullHierarchy(ITEM_1_ID)).thenReturn(ITEM_1);
        when(this.caseReferenceDataService.getCaseStatus(CASE_1.getCaseStatus().getId())).thenReturn(UNCOMMITTED_CASE_STATUS);
        when(this.assetDao.updateAsset(ASSET_1)).thenReturn(ASSET_1);
        when(this.caseDao.updateCase(CASE_1)).thenReturn(CASE_1);
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(CASE_1);
        when(this.caseReferenceDataDao.getCaseStatus(CASE_1.getCaseStatus().getId())).thenReturn(COMMITTED_CASE_STATUS);
        when(this.caseReferenceDataDao.getMilestonesForCaseType(CASE_1.getCaseType().getId())).thenReturn(MILESTONE_LIST);
        when(this.assetDao.getPublishedVersion(ASSET_1_ID)).thenReturn(1L);
        when(this.assetDao.getAsset(ASSET_1_ID, 1L, AssetLightDto.class)).thenReturn(ASSET_1);

        doNothing().when(this.caseMilestoneService).updateMilestoneDates(any(Long.class), any(List.class), any(Date.class), any(Date.class));
        final CaseDto caseDto = this.caseService.updateCase(CASE_1_ID, CASE_1);
        assertFalse(caseDto.getMilestones().isEmpty());
    }

    @Test
    public void createMilestonesWhenCaseUncommitted() throws Exception
    {
        CASE_1.setCaseStatus(UNCOMMITTED_CASE_STATUS_LINK);
        CASE_1.setCaseType(CASE_TYPE);
        CASE_1.getCaseType().setId(CASE_TYPE_ID);
        CASE_1.setAsset(new LinkResource(ASSET_1_ID));

        final AssetLightDto asset = new AssetLightDto();
        asset.setIhsAsset(new LinkResource());

        when(this.caseReferenceDataService.getCaseStatus(CASE_1.getCaseStatus().getId())).thenReturn(UNCOMMITTED_CASE_STATUS);
        when(this.assetDao.updateAsset(ASSET_1)).thenReturn(ASSET_1);
        when(this.caseDao.updateCase(CASE_1)).thenReturn(CASE_1);
        when(this.caseDao.getCase(CASE_1_ID)).thenReturn(CASE_1);
        when(this.caseReferenceDataDao.getCaseStatus(CASE_1.getCaseStatus().getId())).thenReturn(UNCOMMITTED_CASE_STATUS);
        when(this.assetDao.getPublishedVersion(ASSET_1_ID)).thenReturn(1L);
        when(this.assetDao.getAsset(ASSET_1_ID, 1L, AssetLightDto.class)).thenReturn(ASSET_1);
        final CaseDto caseDto = this.caseService.updateCase(CASE_1_ID, CASE_1);
        assertTrue(caseDto.getMilestones().equals(expected));
    }
}
