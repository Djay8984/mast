package com.baesystems.ai.lr.service.scheduling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.scheduling.SRECertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.scheduling.SREServiceDto;
import com.baesystems.ai.lr.dto.scheduling.SREUncertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.services.ServiceService;

@RunWith(MockitoJUnitRunner.class)
public class SchedulingHelperTest
{
    private static final Long CREDITED_SERVICE_ID = 123L;
    private static final Date COMPLETION_DATE = new Date();
    private static final Date ASSET_HARMONISATION_DATE = new Date();
    private static final Long ASSET_ID = 100L;
    private static final Long SERVICE_CATALOGUE_ID_A = 300L;
    private static final Long SERVICE_CATALOGUE_ID_B = 400L;
    private static final Long SERVICE_CATALOGUE_ID_C = 500L;
    private static final Integer HULL_INDICATOR = 123;
    private static final Long PARENT_SERVICE_ID_C = 456L;
    private static final Long PARENT_SERVICE_ID_D = 789L;

    private SRECertifiedSuccessResponseDto certifiedSuccessResponse;
    private SREUncertifiedSuccessResponseDto uncertifiedSuccessResponse;

    private SREServiceDto sreServiceA;
    private SREServiceDto sreServiceB;
    private ScheduledServiceDto ruleGeneratedServiceA;
    private ScheduledServiceDto ruleGeneratedServiceB;
    private ScheduledServiceDto existingAssetServiceB;
    private ScheduledServiceDto existingAssetServiceC;
    private ServiceCatalogueLightDto serviceCatalogueA;
    private ServiceCatalogueLightDto serviceCatalogueB;
    private List<SREServiceDto> sreServices;
    private List<ScheduledServiceDto> ruleGeneratedServices;
    private List<ScheduledServiceDto> existingServicesForAsset;

    @Mock
    private SREHandler sreHandler;

    @Mock
    private ServiceService serviceService;

    @Mock
    private AssetDao assetDao;

    @InjectMocks
    private SchedulingHelper schedulingHelper = new SchedulingHelper();

    @Before
    public void setUp()
    {
        sreServiceA = createSreService(SERVICE_CATALOGUE_ID_A);
        sreServiceB = createSreService(SERVICE_CATALOGUE_ID_B);

        sreServices = new ArrayList<SREServiceDto>();
        sreServices.add(sreServiceA);
        sreServices.add(sreServiceB);

        ruleGeneratedServiceA = createScheduledService(SERVICE_CATALOGUE_ID_A, ASSET_ID, PARENT_SERVICE_ID_C);
        ruleGeneratedServiceB = createScheduledService(SERVICE_CATALOGUE_ID_B, ASSET_ID, PARENT_SERVICE_ID_C);
        ruleGeneratedServices = new ArrayList<ScheduledServiceDto>();
        ruleGeneratedServices.add(ruleGeneratedServiceA);
        ruleGeneratedServices.add(ruleGeneratedServiceB);

        existingAssetServiceB = createScheduledService(SERVICE_CATALOGUE_ID_B, ASSET_ID, PARENT_SERVICE_ID_D);
        existingAssetServiceC = createScheduledService(SERVICE_CATALOGUE_ID_C, ASSET_ID, PARENT_SERVICE_ID_D);
        existingServicesForAsset = new ArrayList<ScheduledServiceDto>();
        existingServicesForAsset.add(existingAssetServiceB);
        existingServicesForAsset.add(existingAssetServiceC);

        serviceCatalogueA = new ServiceCatalogueLightDto();
        serviceCatalogueA.setId(SERVICE_CATALOGUE_ID_A);
        serviceCatalogueB = new ServiceCatalogueLightDto();
        serviceCatalogueB.setId(SERVICE_CATALOGUE_ID_B);

        certifiedSuccessResponse = new SRECertifiedSuccessResponseDto();
        certifiedSuccessResponse.setServices(sreServices);
        certifiedSuccessResponse.setHullIndicator(HULL_INDICATOR);
        uncertifiedSuccessResponse = new SREUncertifiedSuccessResponseDto();
        uncertifiedSuccessResponse.setService(sreServiceA);
    }

    private SREServiceDto createSreService(final Long serviceCatalogueId)
    {
        final SREServiceDto sreService = new SREServiceDto();
        sreService.setServiceCatalogueIdentifier(serviceCatalogueId.intValue());

        // Any dates will do, we just need to assert that the dates we sent in match the dates that came out.
        sreService.setAssignedDate(new Date());
        sreService.setDueDate(new Date());
        sreService.setLowerRangeDate(new Date());
        sreService.setUpperRangeDate(new Date());

        return sreService;
    }

    private ScheduledServiceDto createScheduledService(final Long serviceCatalogueId, final Long assetId, final Long parentServiceId)
    {
        final ScheduledServiceDto scheduledService = new ScheduledServiceDto();
        scheduledService.setServiceCatalogueId(serviceCatalogueId);
        scheduledService.setAsset(new LinkResource(assetId));
        scheduledService.setParentService(new LinkResource(parentServiceId));

        return scheduledService;
    }

    private void assertSchedulesMatch(final SREServiceDto expected, final ScheduledServiceDto actual)
    {
        assertEquals(expected.getAssignedDate(), actual.getAssignedDate());
        assertEquals(expected.getDueDate(), actual.getDueDate());
        assertEquals(expected.getLowerRangeDate(), actual.getLowerRangeDate());
        assertEquals(expected.getUpperRangeDate(), actual.getUpperRangeDate());
    }

    private void assertSchedulesMatch(final ScheduledServiceDto expected, final ScheduledServiceDto actual)
    {
        assertEquals(expected.getAssignedDate(), actual.getAssignedDate());
        assertEquals(expected.getDueDate(), actual.getDueDate());
        assertEquals(expected.getLowerRangeDate(), actual.getLowerRangeDate());
        assertEquals(expected.getUpperRangeDate(), actual.getUpperRangeDate());
    }

    @Test
    public void processServiceList() throws RecordNotFoundException
    {
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_A)).thenReturn(serviceCatalogueA);
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_B)).thenReturn(serviceCatalogueB);

        final List<ScheduledServiceDto> scheduledServices = this.schedulingHelper.processServiceList(this.sreServices, ASSET_ID);

        assertTrue(scheduledServices != null);
        assertTrue(!scheduledServices.isEmpty());
        assertEquals(ASSET_ID, scheduledServices.get(0).getAsset().getId());
        assertSchedulesMatch(sreServices.get(0), scheduledServices.get(0));
        assertEquals(ASSET_ID, scheduledServices.get(1).getAsset().getId());
        assertSchedulesMatch(sreServices.get(1), scheduledServices.get(1));
    }

    @Test
    public void processService() throws RecordNotFoundException
    {
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_A)).thenReturn(serviceCatalogueA);

        final ScheduledServiceDto scheduledService = this.schedulingHelper.processService(sreServiceA, ASSET_ID);

        assertTrue(scheduledService != null);
        assertEquals(ASSET_ID, scheduledService.getAsset().getId());
        assertSchedulesMatch(sreServiceA, scheduledService);
    }

    @Test
    public void manualEntryService() throws RecordNotFoundException
    {
        when(this.sreHandler.getManualEntryService(CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE)).thenReturn(uncertifiedSuccessResponse);
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_A)).thenReturn(serviceCatalogueA);

        final ScheduledServiceDto scheduledService = this.schedulingHelper.getServiceScheduleForSingleService(ASSET_ID, CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE);

        assertTrue(scheduledService != null);
        assertSchedulesMatch(sreServiceA, scheduledService);
        assertTrue(scheduledService.getParentService() == null);
    }

    @Test
    public void nextServices() throws RecordNotFoundException
    {
        when(this.sreHandler.getNextServices(CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE, COMPLETION_DATE)).thenReturn(certifiedSuccessResponse);
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_A)).thenReturn(serviceCatalogueA);
        when(this.serviceService.getServiceCatalogue(SERVICE_CATALOGUE_ID_B)).thenReturn(serviceCatalogueB);

        final List<ScheduledServiceDto> scheduledServices = this.schedulingHelper.getNextServicesFromCompletedService(ASSET_ID, CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE,
                COMPLETION_DATE);

        assertTrue(scheduledServices != null);
        assertTrue(!scheduledServices.isEmpty());

        assertSchedulesMatch(sreServices.get(0), scheduledServices.get(0));
        assertSchedulesMatch(sreServices.get(1), scheduledServices.get(1));

        assertEquals(CREDITED_SERVICE_ID, scheduledServices.get(0).getParentService().getId());
        assertEquals(CREDITED_SERVICE_ID, scheduledServices.get(1).getParentService().getId());
    }

    @Test
    public void generateNewServiceSchedule()
    {
        final List<ScheduledServiceDto> scheduledServices = this.schedulingHelper.generateNewServiceSchedule(ASSET_ID, Boolean.FALSE,
                this.existingServicesForAsset, this.ruleGeneratedServices.toArray(new ScheduledServiceDto[ruleGeneratedServices.size()]));

        // Existing Service C is passed in to the helper method, but we do not expect it to be passed out because
        // there are no rule generated services with the same catalogue ID, so it is unaffected and will not need updating.
        final List<ScheduledServiceDto> expectedServices = new ArrayList<ScheduledServiceDto>();
        expectedServices.add(ruleGeneratedServiceA);
        expectedServices.add(existingAssetServiceB);

        assertTrue(scheduledServices != null);
        assertEquals(expectedServices.size(), scheduledServices.size());

        final ScheduledServiceDto firstExpectedService = expectedServices.get(0);
        final ScheduledServiceDto secondExpectedService = expectedServices.get(1);

        final ScheduledServiceDto firstScheduledService = scheduledServices.get(0);
        final ScheduledServiceDto secondScheduledService = scheduledServices.get(1);

        assertEquals(firstExpectedService.getServiceCatalogueId(), firstScheduledService.getServiceCatalogueId());
        assertEquals(secondExpectedService.getServiceCatalogueId(), secondScheduledService.getServiceCatalogueId());

        assertEquals(ASSET_ID, firstScheduledService.getAsset().getId());
        assertEquals(ASSET_ID, secondScheduledService.getAsset().getId());

        assertEquals(ruleGeneratedServiceA.getParentService().getId(), firstScheduledService.getParentService().getId());
        assertEquals(ruleGeneratedServiceB.getParentService().getId(), secondScheduledService.getParentService().getId());

        assertSchedulesMatch(firstExpectedService, firstScheduledService);
        assertSchedulesMatch(secondExpectedService, secondScheduledService);
    }
}
