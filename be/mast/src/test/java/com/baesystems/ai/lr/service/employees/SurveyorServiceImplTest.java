package com.baesystems.ai.lr.service.employees;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.employees.SurveyorDao;
import com.baesystems.ai.lr.dto.employees.SurveyorPageResourceDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class SurveyorServiceImplTest
{
    private static final String SORT = "id";
    private static final String ORDER = "ASC";
    private static final Integer PAGE = 1;
    private static final Integer SIZE = 1;

    private static final Long OFFICE_ID = 1L;
    private static final String SEARCH = "Office";

    private static final SurveyorPageResourceDto SURVEYOR_1 = new SurveyorPageResourceDto();

    private Pageable pageSpecification;

    @Mock
    private SurveyorDao surveyorDao;

    @Spy
    private ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final SurveyorService surveyorService = new SurveyorServiceImpl();

    @Before
    public void setUp() throws BadPageRequestException
    {
        pageSpecification = this.serviceHelper.createPageable(PAGE, SIZE, SORT, ORDER);
    }

    @Test
    public void simpleGetAll() throws BadRequestException, BadPageRequestException
    {
        when(this.surveyorDao.getSurveyors(pageSpecification)).thenReturn(SURVEYOR_1);
        assertNotNull(this.surveyorService.getSurveyors(PAGE, SIZE, SORT, ORDER, null, null));
    }

    @Test
    public void simpleGetWithSearch() throws BadRequestException, BadPageRequestException
    {
        when(this.surveyorDao.getSurveyors(pageSpecification, SEARCH, null)).thenReturn(SURVEYOR_1);
        assertNotNull(this.surveyorService.getSurveyors(PAGE, SIZE, SORT, ORDER, SEARCH, null));
    }

    @Test
    public void simpleGetWithOffice() throws BadRequestException, BadPageRequestException
    {
        when(this.surveyorDao.getSurveyors(pageSpecification, null, OFFICE_ID)).thenReturn(SURVEYOR_1);
        assertNotNull(this.surveyorService.getSurveyors(PAGE, SIZE, SORT, ORDER, null, OFFICE_ID));
    }

    @Test
    public void simpleGetWithAllParameters() throws BadRequestException, BadPageRequestException
    {
        when(this.surveyorDao.getSurveyors(pageSpecification, SEARCH, OFFICE_ID)).thenReturn(SURVEYOR_1);
        assertNotNull(this.surveyorService.getSurveyors(PAGE, SIZE, SORT, ORDER, SEARCH, OFFICE_ID));
    }
}
