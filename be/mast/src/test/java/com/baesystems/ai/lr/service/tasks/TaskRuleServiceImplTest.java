package com.baesystems.ai.lr.service.tasks;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

@RunWith(MockitoJUnitRunner.class)
public class TaskRuleServiceImplTest
{
    private static final Long ASSET_1 = 1L;
    private static final Long ITEM_1 = 1L;
    private static final Long ITEM_2 = 2L;
    private static final Long SERVICE_1 = 1L;
    private static final Long WORK_ITEM_ACTION_1 = 1L;
    private static final Long WORK_ITEM_ACTION_2 = 2L;
    private static final String SERVICE_CODE = "SC";

    @Mock
    private WorkItemDao workItemDao;

    @Mock
    private TaskHelper taskHelper;

    @InjectMocks
    private TaskRuleService taskRuleService = new TaskRuleServiceImpl();

    private List<WorkItemLightDto> existingTasks = new ArrayList<>();
    private List<WorkItemLightDto> droolsTasks = new ArrayList<>();

    private WorkItemLightDto item1A = new WorkItemLightDto();
    private WorkItemLightDto item1B;
    private WorkItemLightDto item2A = new WorkItemLightDto();

    private final MapperFacade mapper;

    public TaskRuleServiceImplTest()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    @Before
    public void prelim()
    {
        when(this.workItemDao.getLightTasksForAsset(ASSET_1)).thenReturn(existingTasks);
        when(this.workItemDao.getTasksForService(SERVICE_1)).thenReturn(existingTasks);
        when(this.taskHelper.getTaskListForAsset(ASSET_1)).thenReturn(droolsTasks);
        when(this.taskHelper.getTaskListForService(ASSET_1, SERVICE_1)).thenReturn(droolsTasks);

        this.item1A.setAssetItem(new LinkResource(ITEM_1));
        this.item1A.setScheduledService(new LinkResource(SERVICE_1));
        this.item1A.setWorkItemAction(new WorkItemActionDto());
        this.item1A.getWorkItemAction().setId(WORK_ITEM_ACTION_1);
        this.item1A.setServiceCode(SERVICE_CODE);

        this.item1B = this.mapper.map(item1A, WorkItemLightDto.class);

        this.item2A.setAssetItem(new LinkResource(ITEM_2));
        this.item2A.setScheduledService(new LinkResource(SERVICE_1));
        this.item2A.setWorkItemAction(new WorkItemActionDto());
        this.item2A.getWorkItemAction().setId(WORK_ITEM_ACTION_2);
    }

    /*
     * Non existing - create two supplied by drools
     */
    @Test
    public void testSyncTasksForAssetNonExisting()
    {
        droolsTasks.add(this.item1A);
        this.taskRuleService.syncTasksForAsset(ASSET_1);
        verify(this.workItemDao, times(1)).createOrUpdateTask(any(WorkItemLightDto.class));
    }

    /*
     * One already existing - update one
     */
    @Test
    public void testSyncTasksForAssetOneExistingRefreshDrools()
    {
        existingTasks.add(this.item1B);
        droolsTasks.add(this.item1A);
        this.taskRuleService.syncTasksForAsset(ASSET_1);
        verify(this.workItemDao, times(1)).createOrUpdateTask(any(WorkItemLightDto.class));
    }

    /*
     * One already existing, one from drools - update one and create one
     */
    @Test
    public void testSyncTasksForAssetMultiple()
    {
        existingTasks.add(this.item1A);
        droolsTasks.add(this.item1B);
        droolsTasks.add(this.item2A);
        this.taskRuleService.syncTasksForAsset(ASSET_1);
        verify(this.workItemDao, times(2)).createOrUpdateTask(any(WorkItemLightDto.class));
    }

    /*
     * Testing uniqueness/duplication
     */
    @Test
    public void testSyncTasksForService()
    {
        this.droolsTasks.add(item1A);
        this.taskRuleService.syncTasksForScheduledService(ASSET_1, SERVICE_1);
        verify(this.workItemDao, times(1)).createOrUpdateTask(any(WorkItemLightDto.class));
    }

    /*
     * Testing uniqueness/duplication
     */
    @Test
    public void testSyncTasksForServiceMultiple()
    {
        this.existingTasks.add(item1B);
        this.existingTasks.add(item2A);
        this.droolsTasks.add(item1A);
        this.droolsTasks.add(item1B);
        this.taskRuleService.syncTasksForScheduledService(ASSET_1, SERVICE_1);
        verify(this.workItemDao, times(2)).createOrUpdateTask(any(WorkItemLightDto.class));
    }

    /**
     * Testing generation of conditional tasks
     */
    @Test
    public void testConditionalTaskGeneration()
    {
        droolsTasks.add(this.item1A);

        final WorkItemConditionalAttributeDto workItemConditionalAttribute = new WorkItemConditionalAttributeDto();
        when(
                this.workItemDao.getWorkItemConditionalAttribute(item1A.getServiceCode(), item1A.getAssetItem().getId(), item1A.getWorkItemAction()
                        .getId())).thenReturn(workItemConditionalAttribute);

        this.taskRuleService.syncTasksForAsset(ASSET_1);
        verify(this.workItemDao, times(1)).createOrUpdateTask(any(WorkItemLightDto.class));
    }
}
