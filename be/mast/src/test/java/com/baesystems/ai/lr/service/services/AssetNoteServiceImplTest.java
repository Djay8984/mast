package com.baesystems.ai.lr.service.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.hibernate.PropertyValueException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.AssetNotePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.codicils.AssetNoteService;
import com.baesystems.ai.lr.service.codicils.AssetNoteServiceImpl;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.enums.StaleCheckType;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class AssetNoteServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long ONE = 1L;

    @Mock
    private AssetNoteDao assetNoteDao;

    @Mock
    private WIPAssetNoteDao wipAssetNoteDao;

    @InjectMocks
    private final AssetNoteService assetNoteService = new AssetNoteServiceImpl();

    @Mock
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    private static final Long ASSET_1_ID = 1L;

    private static final Long ASSET_NOTE_1_ID = 1L;
    private static final Long WIP_ASSET_NOTE_1_ID = 1L;
    private static final Long INVALID_ASSET_NOTE_ID = 999L;
    private static final Long WIP_INVALID_ASSET_NOTE_ID = 999L;

    private static final Long JOB_1_ID = 1L;
    private static final Long INVALID_JOB_ID = 999L;

    private static final Long STATUS_1_ID = 1L;

    private final PropertyValueException propertyValueException = new PropertyValueException("", "", "");
    private final AssetNoteDto expectedAssetNoteDto = new AssetNoteDto();
    private final AssetNoteDto expectedWipAssetNoteDto = new AssetNoteDto();
    private final AssetNoteDto invalidAssetNoteDto = new AssetNoteDto();
    private Pageable pageSpecification;
    private PageResource<AssetNoteDto> emptyPageResource;
    private CodicilDefectQueryDto codicilDefectQueryDto;

    @Before
    public void setUp() throws BadPageRequestException
    {
        this.pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        this.emptyPageResource = new AssetNotePageResourceDto();
        this.emptyPageResource.setContent(new ArrayList<AssetNoteDto>());
        this.codicilDefectQueryDto = new CodicilDefectQueryDto();
        this.codicilDefectQueryDto.setSearchString("Random String");

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(this.codicilDefectQueryDto, CodicilDefectQueryDto.class))
                .willReturn(this.codicilDefectQueryDto);
    }

    @Test
    public void testGetAssetNoteWithValidID() throws RecordNotFoundException
    {
        when(this.assetNoteDao.getAssetNote(ASSET_NOTE_1_ID)).thenReturn(this.expectedAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.getAssetNote(ASSET_NOTE_1_ID);
        assertEquals(this.expectedAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testGetWIPAssetNoteWithValidID() throws RecordNotFoundException
    {
        when(this.wipAssetNoteDao.getAssetNote(WIP_ASSET_NOTE_1_ID)).thenReturn(this.expectedWipAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.getWIPAssetNote(WIP_ASSET_NOTE_1_ID);
        assertEquals(this.expectedWipAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testGetAssetNoteWithInvalidID() throws RecordNotFoundException
    {
        when(this.assetNoteDao.getAssetNote(INVALID_ASSET_NOTE_ID)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.assetNoteService.getAssetNote(INVALID_ASSET_NOTE_ID);
    }

    @Test
    public void testGetWIPAssetNoteWithInvalidID() throws RecordNotFoundException
    {
        when(this.wipAssetNoteDao.getAssetNote(WIP_INVALID_ASSET_NOTE_ID)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.assetNoteService.getAssetNote(WIP_INVALID_ASSET_NOTE_ID);
    }

    @Test
    public void testGetAssetNoteByAssetWithValidAssetID() throws BadPageRequestException
    {
        final PageResource<AssetNoteDto> expectedPagedResource = getPageResource(false);
        when(this.assetNoteDao.getAssetNotesByAsset(this.pageSpecification, ASSET_1_ID, null)).thenReturn(expectedPagedResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService.getAssetNotesByAsset(null, null, null, null, ASSET_1_ID, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetAssetNoteByAssetWithInvalidAssetID() throws BadPageRequestException
    {
        when(this.assetNoteDao.getAssetNotesByAsset(this.pageSpecification, ASSET_1_ID, null)).thenReturn(this.emptyPageResource);
        final PageResource<AssetNoteDto> result = this.assetNoteService.getAssetNotesByAsset(null, null, null, null, ASSET_1_ID, null);
        assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void testGetAssetNoteByAssetWithStatus() throws BadPageRequestException
    {
        final PageResource<AssetNoteDto> expectedPagedResource = getPageResource(false);
        when(this.assetNoteDao.getAssetNotesByAsset(this.pageSpecification, ASSET_1_ID, STATUS_1_ID)).thenReturn(expectedPagedResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService
                .getAssetNotesByAsset(null, null, null, null, ASSET_1_ID, STATUS_1_ID);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetAssetNoteByAssetWithStatusNonFound() throws BadPageRequestException
    {
        when(this.assetNoteDao.getAssetNotesByAsset(this.pageSpecification, ASSET_1_ID, STATUS_1_ID)).thenReturn(this.emptyPageResource);
        final PageResource<AssetNoteDto> result = this.assetNoteService.getAssetNotesByAsset(null, null, null, null, ASSET_1_ID, STATUS_1_ID);
        assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void testSaveAssetNoteWithValidAssetIDValidAssetNote() throws RecordNotFoundException
    {
        this.expectedAssetNoteDto.setAsset(new LinkResource(ONE));
        when(this.assetNoteDao.saveAssetNote(this.expectedAssetNoteDto)).thenReturn(this.expectedAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.saveAssetNote(this.expectedAssetNoteDto);
        assertEquals(this.expectedAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testSaveWIPAssetNoteWithValidAssetIDValidAssetNote() throws RecordNotFoundException
    {
        this.expectedAssetNoteDto.setAsset(new LinkResource(ONE));
        when(this.wipAssetNoteDao.saveAssetNote(this.expectedAssetNoteDto)).thenReturn(this.expectedAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.saveWIPAssetNote(this.expectedAssetNoteDto);
        assertEquals(this.expectedAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testSaveAssetNoteWithInvalidAssetNote() throws RecordNotFoundException
    {
        doThrow(this.propertyValueException).when(this.assetNoteDao).saveAssetNote(this.invalidAssetNoteDto);
        this.thrown.expect(PropertyValueException.class);
        this.assetNoteService.saveAssetNote(this.invalidAssetNoteDto);
    }

    @Test
    public void testSaveWIPAssetNoteWithInvalidAssetNote() throws RecordNotFoundException
    {
        doThrow(this.propertyValueException).when(this.wipAssetNoteDao).saveAssetNote(this.invalidAssetNoteDto);
        this.thrown.expect(PropertyValueException.class);
        this.assetNoteService.saveWIPAssetNote(this.invalidAssetNoteDto);
    }

    @Test
    public void testGetWIPAssetNotesByJob() throws BadPageRequestException
    {
        final PageResource<AssetNoteDto> expectedPagedResource = getPageResource(true);
        when(this.wipAssetNoteDao.getAssetNotesForJob(this.pageSpecification, JOB_1_ID)).thenReturn(expectedPagedResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService.getWIPAssetNotesForJob(null, null, null, null, JOB_1_ID);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetWIPAssetNotesByInvalidJobID() throws RecordNotFoundException, BadPageRequestException
    {
        when(this.wipAssetNoteDao.getAssetNotesForJob(this.pageSpecification, INVALID_JOB_ID)).thenReturn(this.emptyPageResource);
        final PageResource<AssetNoteDto> result = this.assetNoteService.getWIPAssetNotesForJob(null, null, null, null, INVALID_JOB_ID);
        assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void testUpdateAssetNoteWithValidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.assetNoteDao.findOneWithLock(this.expectedAssetNoteDto.getId(), AssetNoteDto.class, StaleCheckType.ASSET_NOTE))
                .thenReturn(this.expectedAssetNoteDto);
        when(this.wipAssetNoteDao.updateAssetNote(this.expectedAssetNoteDto)).thenReturn(this.expectedAssetNoteDto);
        when(this.assetNoteDao.updateAssetNote(this.expectedAssetNoteDto)).thenReturn(this.expectedAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.updateAssetNote(this.expectedAssetNoteDto);
        assertEquals(this.expectedAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testUpdateWIPAssetNoteWithValidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.wipAssetNoteDao.updateAssetNote(this.expectedWipAssetNoteDto)).thenReturn(this.expectedWipAssetNoteDto);
        final AssetNoteDto assetNoteDto = this.assetNoteService.updateWIPAssetNote(this.expectedWipAssetNoteDto);
        assertEquals(this.expectedWipAssetNoteDto, assetNoteDto);
    }

    @Test
    public void testUpdateAssetNoteWithInvalidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.assetNoteDao.findOneWithLock(this.invalidAssetNoteDto.getId(), AssetNoteDto.class, StaleCheckType.ASSET_NOTE)).thenReturn(null);
        when(this.assetNoteDao.updateAssetNote(this.invalidAssetNoteDto)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.assetNoteService.updateAssetNote(this.invalidAssetNoteDto);
    }

    @Test
    public void testUpdateWIPAssetNoteWithInvalidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.wipAssetNoteDao.updateAssetNote(this.invalidAssetNoteDto)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.assetNoteService.updateWIPAssetNote(this.invalidAssetNoteDto);
    }

    @Test
    public void testQueryAssetNote() throws BadPageRequestException
    {
        final PageResource<AssetNoteDto> expectedPageResource = getPageResource(false);
        when(this.assetNoteDao.findAllForAsset(this.pageSpecification, ASSET_1_ID, this.codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService
                .getAssetNotesByQuery(ASSET_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryAssetNoteNonFound() throws BadPageRequestException
    {
        when(this.assetNoteDao.findAllForAsset(this.pageSpecification, ASSET_1_ID, this.codicilDefectQueryDto)).thenReturn(this.emptyPageResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService
                .getAssetNotesByQuery(ASSET_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(this.emptyPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWipAssetNote() throws BadPageRequestException
    {
        final PageResource<AssetNoteDto> expectedPageResource = getPageResource(true);
        when(this.wipAssetNoteDao.findAllForJob(this.pageSpecification, JOB_1_ID, this.codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService
                .getWIPAssetNotesByQuery(JOB_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWipAssetNoteNonFound() throws BadPageRequestException
    {
        when(this.wipAssetNoteDao.findAllForJob(this.pageSpecification, JOB_1_ID, this.codicilDefectQueryDto)).thenReturn(this.emptyPageResource);
        final PageResource<AssetNoteDto> dtoPageResource = this.assetNoteService
                .getWIPAssetNotesByQuery(JOB_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(this.emptyPageResource, dtoPageResource);
    }

    @Test
    public void testDeleteAssetNoteWithValidID() throws RecordNotFoundException
    {
        when(this.assetNoteDao.exists(ASSET_NOTE_1_ID)).thenReturn(false);
        final boolean result = this.assetNoteService.deleteAssetNote(ASSET_NOTE_1_ID);
        assert result;
    }

    @Test
    public void testDeleteAssetNoteWithInvalidID() throws RecordNotFoundException
    {
        when(this.assetNoteDao.exists(INVALID_ASSET_NOTE_ID)).thenReturn(true);
        final boolean result = this.assetNoteService.deleteAssetNote(INVALID_ASSET_NOTE_ID);
        assert !result;
    }

    private PageResource<AssetNoteDto> getPageResource(final boolean wip)
    {
        final ArrayList<AssetNoteDto> tempList = new ArrayList<>();
        tempList.add(wip ? this.expectedWipAssetNoteDto : this.expectedAssetNoteDto);
        final Page<AssetNoteDto> assetNoteDtoPage = new PageImpl<>(tempList);
        return this.pageFactory.createPageResource(assetNoteDtoPage, AssetNotePageResourceDto.class);
    }
}
