package com.baesystems.ai.lr.service.reports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.component.jackson.JacksonDataFormat;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.reports.ReportContentDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.utils.DueStatusService;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.utils.ServiceUtils;
import com.baesystems.ai.lr.enums.StaleCheckType;

@RunWith(MockitoJUnitRunner.class)
public class ReportServiceImplTest
{
    @Mock
    private ReportDao reportDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private WIPAssetNoteDao wipAssetNoteDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private SurveyDao surveyDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Mock
    private WIPWorkItemDao wipWorkItemDao;

    @Mock
    private ServiceUtils serviceUtils;

    @Mock
    private DueStatusService dueStatusService;

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final ReportService reportService = new ReportServiceImpl();

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private static final String USER = "user";

    private static final Long VALID_REPORT_ID = 1L;
    private static final Long VALID_JOB_ID = 1L;
    private static final Long INVALID_REPORT_ID = 999L;
    private static final Long INVALID_JOB_ID = 999L;
    private static final Long REPORT_VERSION = 2L;

    private static final Long DRAFT_REPORT_ID = 50L;

    private ReportDto report;
    private final JobDto job = new JobDto();
    private final ReportLightDto reportVersion = new ReportLightDto();

    private List<AssetNoteDto> assetNoteList;
    private List<CoCDto> cocList;
    private List<ActionableItemDto> actionableItemList;
    private List<SurveyDto> surveyList;
    private List<WorkItemDto> taskList;
    private ReportContentDto content;

    @Mock
    private JacksonDataFormat dataFormatter;

    @Before
    public void setup()
    {
        this.report = new ReportDto();
        this.report.setContent("{}");
        this.report.setId(VALID_REPORT_ID);
        this.report.setJob(new LinkResource(VALID_JOB_ID));
        this.report.setReportVersion(REPORT_VERSION);
        this.report.setReportType(new LinkResource());
        this.report.getReportType().setId(ReportType.FAR.value());
        this.report.setIssueDate(new Date());
    }

    private void setUpFinalSurveyReportContent()
    {
        Long id = 0L;

        this.assetNoteList = new ArrayList<>();
        this.taskList = new ArrayList<>();
        this.cocList = new ArrayList<>();
        this.surveyList = new ArrayList<>();
        this.actionableItemList = new ArrayList<>();

        this.assetNoteList.add(new AssetNoteDto());
        this.taskList.add(new WorkItemDto());
        this.cocList.add(new CoCDto());
        this.surveyList.add(new SurveyDto());
        this.actionableItemList.add(new ActionableItemDto());

        this.assetNoteList.get(0).setId(++id);
        this.taskList.get(0).setId(++id);
        this.cocList.get(0).setId(++id);
        this.surveyList.get(0).setId(++id);
        this.actionableItemList.get(0).setId(++id);
    }

    private String formatContent(final ReportContentDto finalSurveryReport) throws Exception
    {
        final ByteArrayOutputStream streamContent = new ByteArrayOutputStream();

        try
        {
            this.dataFormatter.marshal(null, finalSurveryReport, streamContent);
            return streamContent.toString("UTF-8");
        }
        catch (final Exception streamException)
        {
            throw new MastSystemException("Error Generating report content", streamException);
        }
        finally
        {
            streamContent.close();
        }
    }

    @Test
    public void getReport() throws RecordNotFoundException, BadRequestException
    {
        when(this.reportDao.getReport(VALID_REPORT_ID)).thenReturn(this.report);
        assertNotNull(this.reportService.getReport(VALID_REPORT_ID));
    }

    @Test
    public void getReportNotFound() throws RecordNotFoundException, BadRequestException
    {
        when(this.reportDao.getReport(INVALID_REPORT_ID)).thenReturn(null);
        this.exception.expect(RecordNotFoundException.class);
        this.reportService.getReport(INVALID_REPORT_ID);
    }

    @Test
    public void getReportVersions() throws RecordNotFoundException
    {
        final ArrayList<ReportLightDto> versions = new ArrayList<ReportLightDto>();
        versions.add(this.reportVersion);

        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.reportDao.getReports(VALID_JOB_ID)).thenReturn(versions);

        assertNotNull(this.reportService.getReports(VALID_JOB_ID));
    }

    @Test
    public void getReportVersionsNotFound() throws RecordNotFoundException
    {
        when(this.jobDao.getJob(INVALID_JOB_ID)).thenReturn(null);

        this.exception.expect(RecordNotFoundException.class);
        this.reportService.getReports(INVALID_JOB_ID);
    }

    @Test
    public void saveReport() throws Exception
    {
        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.FAR.value())).thenReturn(REPORT_VERSION);
        when(this.reportDao.saveReport(this.report)).thenReturn(newReport);
        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.wipAssetNoteDao.getAssetNotesForReportContent(VALID_JOB_ID)).thenReturn(this.assetNoteList);
        when(this.wipCoCDao.getCoCsForReportContent(VALID_JOB_ID)).thenReturn(this.cocList);
        when(this.wipActionableItemDao.getActionableItemsForReportContent(VALID_JOB_ID)).thenReturn(this.actionableItemList);
        when(this.surveyDao.getSurveysForReportContent(VALID_JOB_ID)).thenReturn(this.surveyList);
        when(this.wipWorkItemDao.getTasksForReportContent(VALID_JOB_ID)).thenReturn(this.taskList);
        when(this.reportDao.getReport(DRAFT_REPORT_ID)).thenReturn(this.report);
        doNothing().when(this.dataFormatter).marshal(null, this.content, new ByteArrayOutputStream());

        final ReportDto returnedReport = this.reportService.saveReport(this.report);

        assertNotNull(returnedReport);
        assertEquals(returnedReport.getReportVersion(), newReport.getReportVersion());
    }

    @Test
    public void saveReportDar() throws Exception
    {
        this.report.setReportType(new LinkResource(ReportType.DAR.value()));

        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.DAR.value())).thenReturn(REPORT_VERSION);
        when(this.reportDao.saveReport(this.report)).thenReturn(newReport);
        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.wipAssetNoteDao.getAssetNotesForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.assetNoteList);
        when(this.wipCoCDao.getCoCsForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.cocList);
        when(this.wipActionableItemDao.getActionableItemsForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.actionableItemList);
        when(this.surveyDao.getSurveys(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.surveyList);
        when(this.wipWorkItemDao.getTasksForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.taskList);
        when(this.reportDao.getReport(DRAFT_REPORT_ID)).thenReturn(this.report);
        when(this.serviceUtils.getSecurityContextPrincipal()).thenReturn(USER);
        doNothing().when(this.dataFormatter).marshal(null, this.content, new ByteArrayOutputStream());

        final ReportDto returnedReport = this.reportService.saveReport(this.report);

        assertNotNull(returnedReport);
        assertEquals(returnedReport.getReportVersion(), newReport.getReportVersion());
    }

    @Test
    public void saveReportDarWithNullContent() throws Exception
    {
        this.report.setReportType(new LinkResource(ReportType.DAR.value()));

        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));
        this.report.setContent((String) null);

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.DAR.value())).thenReturn(REPORT_VERSION);
        when(this.reportDao.saveReport(this.report)).thenReturn(newReport);
        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.wipAssetNoteDao.getAssetNotesForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.assetNoteList);
        when(this.wipCoCDao.getCoCsForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.cocList);
        when(this.wipActionableItemDao.getActionableItemsForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.actionableItemList);
        when(this.surveyDao.getSurveys(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.surveyList);
        when(this.wipWorkItemDao.getTasksForJob(VALID_JOB_ID, USER, this.report.getIssueDate())).thenReturn(this.taskList);
        when(this.reportDao.getReport(DRAFT_REPORT_ID)).thenReturn(this.report);
        when(this.serviceUtils.getSecurityContextPrincipal()).thenReturn(USER);
        doNothing().when(this.dataFormatter).marshal(null, this.content, new ByteArrayOutputStream());
        doNothing().when(this.dueStatusService).calculateDueStatuses(any((Class<List<DueStatusUpdateableDto>>) (Class<?>) List.class));

        final ReportDto returnedReport = this.reportService.saveReport(this.report);

        assertNotNull(returnedReport);
        assertEquals(returnedReport.getReportVersion(), newReport.getReportVersion());
    }

    @Test
    public void saveReportDsr() throws Exception
    {
        this.report.setReportType(new LinkResource(ReportType.DSR.value()));

        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        // this get next report will be for the type before DSR which is FAR
        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.FAR.value())).thenReturn(REPORT_VERSION);
        when(this.reportDao.getReport(VALID_JOB_ID, ReportType.FAR.value(), REPORT_VERSION - 1)).thenReturn(new ReportDto());
        when(this.reportDao.saveReport(this.report)).thenReturn(newReport);
        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.wipAssetNoteDao.getAssetNotesForReportContent(VALID_JOB_ID)).thenReturn(this.assetNoteList);
        when(this.wipCoCDao.getCoCsForReportContent(VALID_JOB_ID)).thenReturn(this.cocList);
        when(this.wipActionableItemDao.getActionableItemsForReportContent(VALID_JOB_ID)).thenReturn(this.actionableItemList);
        when(this.surveyDao.getSurveysForReportContent(VALID_JOB_ID)).thenReturn(this.surveyList);
        when(this.wipWorkItemDao.getTasksForReportContent(VALID_JOB_ID)).thenReturn(this.taskList);
        when(this.reportDao.getReport(DRAFT_REPORT_ID)).thenReturn(this.report);
        when(this.serviceUtils.getSecurityContextPrincipal()).thenReturn(USER);
        doNothing().when(this.dataFormatter).marshal(null, this.content, new ByteArrayOutputStream());

        final ReportDto returnedReport = this.reportService.saveReport(this.report);

        assertNotNull(returnedReport);
        assertEquals(returnedReport.getReportVersion(), newReport.getReportVersion());
    }

    @Test
    public void saveReportDsrNullVersion() throws Exception
    {
        this.report.setReportType(new LinkResource(ReportType.DSR.value()));

        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        this.exception.expect(BadRequestException.class);
        this.exception.expectMessage("Can not determine parent report");

        // this get next report will be for the type before DSR which is FAR
        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.FAR.value())).thenReturn(null);

        this.reportService.saveReport(this.report);
    }

    @Test
    public void saveReportDsrVersion0() throws Exception
    {
        this.report.setReportType(new LinkResource(ReportType.DSR.value()));

        setUpFinalSurveyReportContent();
        this.content = new ReportContentDto();
        this.content.setWipAssetNotes(this.assetNoteList);
        this.content.setTasks(this.taskList);
        this.content.setWipCocs(this.cocList);
        this.content.setWipActionableItems(this.actionableItemList);
        this.content.setSurveys(this.surveyList);

        this.report.setJob(new LinkResource(VALID_JOB_ID));

        final ReportDto newReport = new ReportDto();
        newReport.setReportVersion(REPORT_VERSION);

        this.exception.expect(BadRequestException.class);
        this.exception.expectMessage("Can not determine parent report");

        // this get next report will be for the type before DSR which is FAR
        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.FAR.value())).thenReturn(0L);

        this.reportService.saveReport(this.report);
    }

    @Test
    public void populateFinalServiceReportTest()
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        setUpFinalSurveyReportContent();

        when(this.jobDao.getJob(VALID_JOB_ID)).thenReturn(this.job);
        when(this.wipAssetNoteDao.getAssetNotesForReportContent(VALID_JOB_ID)).thenReturn(this.assetNoteList);
        when(this.wipCoCDao.getCoCsForReportContent(VALID_JOB_ID)).thenReturn(this.cocList);
        when(this.surveyDao.getSurveysForReportContent(VALID_JOB_ID)).thenReturn(this.surveyList);
        when(this.wipWorkItemDao.getTasksForReportContent(VALID_JOB_ID)).thenReturn(this.taskList);
        when(this.wipActionableItemDao.getActionableItemsForReportContent(VALID_JOB_ID)).thenReturn(this.actionableItemList);
        doNothing().when(this.dueStatusService).calculateDueStatuses(any((Class<List<DueStatusUpdateableDto>>) (Class<?>) List.class));

        final Method populateFinalSurveyReport = this.reportService.getClass().getDeclaredMethod("populateReportContent", java.lang.Long.class,
                ReportDto.class);
        populateFinalSurveyReport.setAccessible(true);
        final Object[] parameters = new Object[2];
        parameters[0] = VALID_JOB_ID;
        parameters[1] = this.report;
        final ReportContentDto output = (ReportContentDto) populateFinalSurveyReport.invoke(this.reportService, parameters);

        assertNotNull(output);
        assertNotNull(output.getWipAssetNotes());
        assertNotNull(output.getTasks());
        assertNotNull(output.getWipCocs());
        assertNotNull(output.getSurveys());
        assertNotNull(output.getWipActionableItems());
        assertEquals(1, output.getWipAssetNotes().size());
        assertEquals(1, output.getTasks().size());
        assertEquals(1, output.getWipCocs().size());
        assertEquals(1, output.getSurveys().size());
        assertEquals(1, output.getWipActionableItems().size());
        assertEquals(this.assetNoteList.get(0), output.getWipAssetNotes().get(0));
        assertEquals(this.taskList.get(0), output.getTasks().get(0));
        assertEquals(this.cocList.get(0), output.getWipCocs().get(0));
        assertEquals(this.surveyList.get(0), output.getSurveys().get(0));
        assertEquals(this.actionableItemList.get(0), output.getWipActionableItems().get(0));
    }

    @Test
    public void getReportForJobFAR() throws RecordNotFoundException, BadRequestException
    {
        when(this.reportDao.getReport(VALID_REPORT_ID)).thenReturn(this.report);
        assertNotNull(this.reportService.getReport(VALID_REPORT_ID));
    }

    @Test
    public void getNextReportVersion()
    {
        when(this.reportDao.getNextReportVersion(VALID_JOB_ID, ReportType.DAR.value())).thenReturn(REPORT_VERSION);

        assertEquals(REPORT_VERSION, this.reportService.getNextReportVersion(VALID_JOB_ID, ReportType.DAR.value()));
    }

    @Test
    public void updateReport() throws BadRequestException, StaleObjectException
    {
        when(this.reportDao.findOneWithLock(this.report.getId(), ReportDto.class, StaleCheckType.REPORT)).thenReturn(this.report);
        when(this.reportDao.saveReport(any(ReportDto.class))).thenReturn(this.report);

        this.reportService.updateReport(this.report);
    }
}
