package com.baesystems.ai.lr.service.assets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class AssetServiceImplTest
{
    @Mock
    private AssetDao assetDao;

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final AssetService assetService = new AssetServiceImpl();

    private static AssetLightDto asset;

    private static final Long ASSET_ID = 1L;

    @BeforeClass
    public static void setUpClass()
    {
        asset = new AssetLightDto();
    }

    @Test
    public void testHarmonisationDateComparisonNull() throws RecordNotFoundException
    {
        asset.setHarmonisationDate(new Date());
        when(this.assetDao.getDraftVersion(ASSET_ID)).thenReturn(1L);
        when(assetDao.getAsset(ASSET_ID, 1L, AssetLightDto.class)).thenReturn(asset);
        final Boolean result = this.assetService.isHarmonisationDateDifferent(ASSET_ID, null);
        assertTrue(result);
    }

    @Test
    public void testHarmonisationDateComparisonBothNull() throws RecordNotFoundException
    {
        asset.setHarmonisationDate(null);
        when(this.assetDao.getDraftVersion(ASSET_ID)).thenReturn(1L);
        when(assetDao.getAsset(ASSET_ID, 1L, AssetLightDto.class)).thenReturn(asset);
        final Boolean result = this.assetService.isHarmonisationDateDifferent(ASSET_ID, null);
        assertFalse(result);
    }

    @Test
    public void testHarmonisationDateComparisonEqual() throws RecordNotFoundException
    {
        final Date date = new Date();
        asset.setHarmonisationDate(date);
        when(this.assetDao.getDraftVersion(ASSET_ID)).thenReturn(1L);
        when(assetDao.getAsset(ASSET_ID, 1L, AssetLightDto.class)).thenReturn(asset);
        final Boolean result = this.assetService.isHarmonisationDateDifferent(ASSET_ID, date);
        assertFalse(result);
    }

    @Test
    public void testHarmonisationDateComparisonDifferent() throws RecordNotFoundException
    {
        final Date date1 = new Date();
        final Date date2 = new Date();
        date2.setTime(date1.getTime() + 1);

        asset.setHarmonisationDate(date1);
        when(this.assetDao.getDraftVersion(ASSET_ID)).thenReturn(1L);
        when(assetDao.getAsset(ASSET_ID, 1L, AssetLightDto.class)).thenReturn(asset);
        final Boolean result = this.assetService.isHarmonisationDateDifferent(ASSET_ID, date2);
        assertTrue(result);
    }
}
