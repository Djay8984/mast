package com.baesystems.ai.lr.service.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceListDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.scheduling.SchedulingRuleService;
import com.baesystems.ai.lr.service.tasks.TaskRuleService;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DateHelperImpl;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class ServiceServiceImplTest
{
    private static final Long PRODUCT_CATALOGUE_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long PRODUCT_RULESET_1_ID = 1L;
    private static final Long SERVICE_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long CERT_STATUS_1_ID = 1L;
    private static final Long INVALID_ID = 999L;

    private static final Integer CYCLE_PERIODICITY_1 = 12;
    private static final Integer LOWER_RANGE_OFFSET_DAYS_1 = 7;
    private static final Integer UPPER_RANGE_OFFSET_DAYS_1 = 14;

    private static final Integer CYCLE_PERIODICITY_2 = 36;
    private static final Integer LOWER_RANGE_OFFSET_DAYS_2 = 28;
    private static final Integer UPPER_RANGE_OFFSET_DAYS_2 = 14;

    private static final Integer OFFSET_DAYS = 365;

    private static final AssetLightDto ASSET_1 = new AssetLightDto();
    private static final ScheduledServiceDto SERVICE_1 = new ScheduledServiceDto();
    private static final ScheduledServiceDto SERVICE_2 = new ScheduledServiceDto();
    private static final ScheduledServiceListDto SERVICE_LIST = new ScheduledServiceListDto();
    private static final ScheduledServiceDto SERVICE_AFTER_RESCHEDULE = new ScheduledServiceDto();
    private static final ScheduledServiceDto SERVICE_WITH_UPDATED_CERT_STATUS = new ScheduledServiceDto();
    private static final ProductDto PRODUCT_1 = new ProductDto();
    private static final ServiceCatalogueLightDto SERVICE_CATALOGUE_1 = new ServiceCatalogueLightDto();
    private static final ServiceCatalogueLightDto SERVICE_CATALOGUE_2 = new ServiceCatalogueLightDto();
    private static final Date HARMONISATION_DATE = new Date();
    private static final LinkResource CERTIFICATE_STATUS = new LinkResource();

    private List<SurveyDto> surveys;
    private List<CertificateDto> certificates;

    private List<ScheduledServiceDto> serviceList;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private SurveyDao surveyDao;

    @Mock
    private WIPCertificateDao wipCertificateDao;

    @Mock
    private SchedulingRuleService schedulingRuleService;

    @Mock
    private TaskRuleService taskRuleService;

    @Spy
    private final DateHelper dateUtils = new DateHelperImpl();

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final ServiceService serviceService = new ServiceServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws MastBusinessException
    {
        this.setServiceSchedulingFactors(SERVICE_CATALOGUE_1, CYCLE_PERIODICITY_1, LOWER_RANGE_OFFSET_DAYS_1, UPPER_RANGE_OFFSET_DAYS_1);
        this.setServiceSchedulingFactors(SERVICE_CATALOGUE_2, CYCLE_PERIODICITY_2, LOWER_RANGE_OFFSET_DAYS_2, UPPER_RANGE_OFFSET_DAYS_2);

        ASSET_1.setProductRuleSet(new LinkResource(PRODUCT_RULESET_1_ID));

        PRODUCT_1.setProductCatalogueId(PRODUCT_CATALOGUE_1_ID);

        SERVICE_1.setId(SERVICE_1_ID);
        SERVICE_1.setAssignedDate(HARMONISATION_DATE);
        SERVICE_1.setDueDate(this.dateUtils.adjustDate(SERVICE_1.getAssignedDate(), SERVICE_CATALOGUE_1.getCyclePeriodicity(), Calendar.MONTH));
        SERVICE_1.setLowerRangeDate(
                this.dateUtils.adjustDate(SERVICE_1.getDueDate(), -SERVICE_CATALOGUE_1.getLowerRangeDateOffsetMonths(), Calendar.DAY_OF_MONTH));
        SERVICE_1.setUpperRangeDate(
                this.dateUtils.adjustDate(SERVICE_1.getDueDate(), SERVICE_CATALOGUE_1.getUpperRangeDateOffsetMonths(), Calendar.DAY_OF_MONTH));
        SERVICE_1.setAsset(new LinkResource(ASSET_1_ID));
        SERVICE_2.setAssignedDate(this.dateUtils.adjustDate(HARMONISATION_DATE, OFFSET_DAYS, Calendar.DAY_OF_MONTH));
        SERVICE_2.setDueDate(this.dateUtils.adjustDate(SERVICE_2.getAssignedDate(), SERVICE_CATALOGUE_2.getCyclePeriodicity(), Calendar.MONTH));
        SERVICE_2.setLowerRangeDate(
                this.dateUtils.adjustDate(SERVICE_2.getDueDate(), -SERVICE_CATALOGUE_2.getLowerRangeDateOffsetMonths(), Calendar.DAY_OF_MONTH));
        SERVICE_2.setUpperRangeDate(
                this.dateUtils.adjustDate(SERVICE_2.getDueDate(), SERVICE_CATALOGUE_2.getUpperRangeDateOffsetMonths(), Calendar.DAY_OF_MONTH));
        SERVICE_2.setAsset(new LinkResource(ASSET_1_ID));

        SERVICE_AFTER_RESCHEDULE.setId(SERVICE_1_ID);
        SERVICE_AFTER_RESCHEDULE.setAssignedDate(HARMONISATION_DATE);
        SERVICE_AFTER_RESCHEDULE
                .setDueDate(this.dateUtils.adjustDate(SERVICE_1.getDueDate(), SERVICE_CATALOGUE_1.getCyclePeriodicity(), Calendar.MONTH));
        SERVICE_AFTER_RESCHEDULE.setLowerRangeDate(
                this.dateUtils.adjustDate(SERVICE_AFTER_RESCHEDULE.getDueDate(), -SERVICE_CATALOGUE_1.getLowerRangeDateOffsetMonths(),
                        Calendar.DAY_OF_MONTH));
        SERVICE_AFTER_RESCHEDULE.setUpperRangeDate(
                this.dateUtils.adjustDate(SERVICE_AFTER_RESCHEDULE.getDueDate(), SERVICE_CATALOGUE_1.getUpperRangeDateOffsetMonths(),
                        Calendar.DAY_OF_MONTH));

        SERVICE_WITH_UPDATED_CERT_STATUS.setId(SERVICE_1_ID);

        this.serviceList = new ArrayList<ScheduledServiceDto>();
        this.serviceList.add(SERVICE_1);
        this.serviceList.add(SERVICE_2);

        this.surveys = new ArrayList<>();
        this.certificates = new ArrayList<CertificateDto>();

        CERTIFICATE_STATUS.setId(CERT_STATUS_1_ID);

        SERVICE_LIST.setScheduledServices(Collections.singletonList(SERVICE_1));
    }

    private void setServiceSchedulingFactors(final ServiceCatalogueLightDto serviceCatalogue, final Integer cyclePeriodicity,
            final Integer lowerRangeDateOffsetMonths, final Integer upperRangeDateOffsetMonths)
    {
        serviceCatalogue.setCyclePeriodicity(cyclePeriodicity);
        serviceCatalogue.setLowerRangeDateOffsetMonths(lowerRangeDateOffsetMonths);
        serviceCatalogue.setUpperRangeDateOffsetMonths(upperRangeDateOffsetMonths);
    }

    @Test
    public void saveService() throws MastBusinessException, RecordNotFoundException
    {
        when(this.serviceDao.saveService(SERVICE_1)).thenReturn(SERVICE_1);
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(SERVICE_1);

        this.serviceService.saveServices(SERVICE_LIST);
    }

    @Test
    public void getServicesForInvalidAsset() throws RecordNotFoundException
    {
        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(null);

        this.thrown.expect(RecordNotFoundException.class);

        this.serviceService.getServicesForAsset(INVALID_ID, null, null, null, null);
    }

    @Test
    public void getServicesForAsset() throws RecordNotFoundException
    {
        when(this.assetDao.getPublishedVersion(ASSET_1_ID)).thenReturn(1L);
        when(this.assetDao.getAsset(ASSET_1_ID, 1L, AssetLightDto.class)).thenReturn(ASSET_1);
        when(this.serviceDao.getServicesForAsset(ASSET_1_ID, null, null, null, null)).thenReturn(this.serviceList);

        final List<ScheduledServiceDto> services = this.serviceService.getServicesForAsset(ASSET_1_ID, null, null, null, null);

        assertNotNull(services);
        assertFalse(services.isEmpty());
        assertTrue(services.containsAll(this.serviceList));
        assertTrue(this.serviceList.containsAll(services));
    }

    @Test
    public void getServicesForAssetEmpty() throws RecordNotFoundException
    {
        when(this.assetDao.getPublishedVersion(ASSET_1_ID)).thenReturn(1L);
        when(this.assetDao.getAsset(ASSET_1_ID, ASSET_1_ID, AssetLightDto.class)).thenReturn(ASSET_1);
        when(this.serviceDao.getServicesForAsset(ASSET_1_ID, null, null, null, null)).thenReturn(Collections.<ScheduledServiceDto>emptyList());

        final List<ScheduledServiceDto> services = this.serviceService.getServicesForAsset(ASSET_1_ID, null, null, null, null);

        assertNotNull(services);
        assertTrue(services.isEmpty());
    }

    @Test
    public void updateScheduleFollowingFARNullSurveys() throws MastBusinessException, RecordNotFoundException
    {
        this.serviceService.updateServiceScheduleFollowingFARorFSR(null, true);
    }

    @Test
    public void updateScheduleFollowingFAREmptySurveys() throws MastBusinessException, RecordNotFoundException
    {
        this.serviceService.updateServiceScheduleFollowingFARorFSR(new ArrayList<SurveyDto>(), true);
    }

    @Test
    public void updateScheduleFollowingFARDatesAlreadyUpdated() throws MastBusinessException, RecordNotFoundException
    {
        final SurveyDto survey = new SurveyDto();
        survey.setScheduledService(SERVICE_1);
        this.surveys.add(survey);

        // This flag should prevent any update of the service dates for this survey
        survey.setScheduleDatesUpdated(true);

        this.serviceService.updateServiceScheduleFollowingFARorFSR(this.surveys, true);
    }

    @Test
    public void updateScheduleFollowingFARMultipleSurveys() throws MastBusinessException, RecordNotFoundException
    {
        final SurveyDto surveyAlreadyUpdated = new SurveyDto();
        surveyAlreadyUpdated.setScheduledService(SERVICE_2);

        final SurveyDto surveyNeedsUpdating = new SurveyDto();
        surveyNeedsUpdating.setScheduledService(SERVICE_1);

        this.surveys.add(surveyAlreadyUpdated);
        this.surveys.add(surveyNeedsUpdating);

        surveyAlreadyUpdated.setScheduleDatesUpdated(true);
        surveyNeedsUpdating.setScheduleDatesUpdated(false);

        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(SERVICE_1);
        when(this.assetDao.getAssetHarmonisationDate(ASSET_1_ID)).thenReturn(HARMONISATION_DATE);
        doNothing().when(this.schedulingRuleService).scheduleServicesFollowingServiceCompletion(SERVICE_1, HARMONISATION_DATE, null);
        doNothing().when(this.taskRuleService).syncTasksForScheduledService(ASSET_1_ID, SERVICE_1_ID);

        this.serviceService.updateServiceScheduleFollowingFARorFSR(this.surveys, true);
    }

    @Test
    public void updateServiceCertStatusFollowingFARNullCertificates()
    {
        final List<ScheduledServiceDto> updatedServices = this.serviceService.updateServiceCertStatusFollowingFAR(null);

        assertTrue(updatedServices.isEmpty());
    }

    @Test
    public void updateServiceCertStatusFollowingFAREmptyCertificates()
    {
        when(this.wipCertificateDao.getCertificatesForJob(JOB_1_ID)).thenReturn(new ArrayList<CertificateDto>());

        final List<ScheduledServiceDto> updatedServices = this.serviceService.updateServiceCertStatusFollowingFAR(JOB_1_ID);

        assertTrue(updatedServices.isEmpty());
    }

    @Test
    public void updateServiceCertStatusFollowingFARNullJob()
    {
        final CertificateDto certificate = new CertificateDto();
        certificate.setJob(null);
        this.certificates.add(certificate);

        when(this.wipCertificateDao.getCertificatesForJob(JOB_1_ID)).thenReturn(this.certificates);

        final List<ScheduledServiceDto> updatedServices = this.serviceService.updateServiceCertStatusFollowingFAR(JOB_1_ID);

        assertTrue(updatedServices.isEmpty());
    }

    @Test
    public void updateServiceCertStatusFollowingFARNoSurveys()
    {
        final CertificateDto certificate = new CertificateDto();
        certificate.setJob(new LinkResource(JOB_1_ID));
        this.certificates.add(certificate);

        when(this.wipCertificateDao.getCertificatesForJob(JOB_1_ID)).thenReturn(this.certificates);
        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(null);

        final List<ScheduledServiceDto> updatedServices = this.serviceService.updateServiceCertStatusFollowingFAR(JOB_1_ID);

        assertTrue(updatedServices.isEmpty());
    }

    @Test
    public void updateServiceCertStatusFollowingFARNoScheduleServices()
    {
        final CertificateDto certificate = new CertificateDto();
        certificate.setJob(new LinkResource(JOB_1_ID));
        certificate.setCertificateStatus(new LinkResource(CERTIFICATE_STATUS.getId()));
        this.certificates.add(certificate);

        final SurveyDto surveyWithScheduledService = new SurveyDto();
        surveyWithScheduledService.setServiceCatalogue(new LinkResource());
        surveyWithScheduledService.setScheduledService(SERVICE_1);

        final SurveyDto surveyWithNoScheduledService = new SurveyDto();
        surveyWithNoScheduledService.setServiceCatalogue(new LinkResource());

        this.surveys.add(surveyWithScheduledService);
        this.surveys.add(surveyWithNoScheduledService);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(this.surveys);
        when(this.serviceDao.updateService(SERVICE_1)).thenReturn(SERVICE_WITH_UPDATED_CERT_STATUS);
        when(this.serviceDao.getService(SERVICE_1_ID)).thenReturn(SERVICE_1);
        when(this.wipCertificateDao.getCertificatesForJob(JOB_1_ID)).thenReturn(this.certificates);

        final List<ScheduledServiceDto> updatedServices = this.serviceService.updateServiceCertStatusFollowingFAR(JOB_1_ID);

        assertEquals(1, updatedServices.size());
        assertEquals(SERVICE_1_ID, updatedServices.get(0).getId());
    }
}
