package com.baesystems.ai.lr.service.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ProductListDto;
import com.baesystems.ai.lr.dto.services.ProductDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.enums.StaleCheckType;

@RunWith(MockitoJUnitRunner.class)
@Ignore
public class ProductServiceImplTest
{
    private static final Long ASSET_1_ID = 1L;

    private static final Long PRODUCT_10_ID = 10L;
    private static final Long PRODUCT_20_ID = 20L;
    private static final Long PRODUCT_30_ID = 30L;

    private static final Long SCHEDULING_REGIME_100_ID = 100L;
    private static final Long SCHEDULING_REGIME_200_ID = 200L;

    private static final ProductDto PRODUCT_10 = new ProductDto();
    private static final ProductDto PRODUCT_20 = new ProductDto();
    private static final ProductDto PRODUCT_30 = new ProductDto();

    private static final Long PRODUCT_CATALOGUE_1_ID = 1L;
    private static final Long PRODUCT_CATALOGUE_2_ID = 2L;
    private static final Long PRODUCT_CATALOGUE_3_ID = 3L;
    private static final Long PRODUCT_CATALOGUE_4_ID = 4L;

    private static final ProductCatalogueDto PRODUCT_CATALOGUE_1 = new ProductCatalogueDto();
    private static final ProductCatalogueDto PRODUCT_CATALOGUE_2 = new ProductCatalogueDto();
    private static final ProductCatalogueDto PRODUCT_CATALOGUE_3 = new ProductCatalogueDto();
    private static final ProductCatalogueDto PRODUCT_CATALOGUE_4 = new ProductCatalogueDto();

    private static final LinkResource SCHEDULING_REGIME_100 = new LinkResource(SCHEDULING_REGIME_100_ID);
    private static final LinkResource SCHEDULING_REGIME_200 = new LinkResource(SCHEDULING_REGIME_200_ID);

    private static final ProductListDto PRODUCT_CATALOGUE_EMPTY = new ProductListDto();

    private static final List<ProductDto> EMPTY_PRODUCT_LIST = new ArrayList<ProductDto>();

    @Mock
    private ProductDao productDao;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Mock
    private AssetDao assetDao;

    @InjectMocks
    private final ProductService productService = new ProductServiceImpl();

    @Before
    public void setUp()
    {
        PRODUCT_10.setId(PRODUCT_10_ID);
        PRODUCT_20.setId(PRODUCT_20_ID);
        PRODUCT_30.setId(PRODUCT_30_ID);

        PRODUCT_CATALOGUE_1.setId(PRODUCT_CATALOGUE_1_ID);
        PRODUCT_CATALOGUE_2.setId(PRODUCT_CATALOGUE_2_ID);
        PRODUCT_CATALOGUE_3.setId(PRODUCT_CATALOGUE_3_ID);
        PRODUCT_CATALOGUE_4.setId(PRODUCT_CATALOGUE_4_ID);

        PRODUCT_CATALOGUE_EMPTY.setProductList(EMPTY_PRODUCT_LIST);
    }

    @Test
    public void addProductsToEmptyList() throws RecordNotFoundException, BadRequestException, StaleObjectException
    {
        final ProductDto newProduct10 = new ProductDto();
        newProduct10.setProductCatalogueId(PRODUCT_CATALOGUE_1_ID);
        newProduct10.setSchedulingRegime(SCHEDULING_REGIME_100);

        final ProductDto newProduct20 = new ProductDto();
        newProduct20.setProductCatalogueId(PRODUCT_CATALOGUE_2_ID);
        newProduct20.setSchedulingRegime(SCHEDULING_REGIME_200);

        final ProductListDto productListDto = new ProductListDto();
        productListDto.setProductList(Arrays.asList(newProduct10, newProduct20));

        // Note, this is called at the end of the test too, but as there is no benefit in asserting the mocked result no
        // more behaviour will be added here.
        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(EMPTY_PRODUCT_LIST);

        this.productService.updateProducts(ASSET_1_ID, productListDto);

        Mockito.verify(this.productDao, Mockito.atLeast(1)).getProducts(ASSET_1_ID);
        Mockito.verify(this.productDao).createProduct(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, SCHEDULING_REGIME_100_ID);
        Mockito.verify(this.productDao).createProduct(ASSET_1_ID, PRODUCT_CATALOGUE_2_ID, SCHEDULING_REGIME_200_ID);
        Mockito.verifyNoMoreInteractions(this.productDao);
    }

    @Test
    public void addProductsToExistingList() throws RecordNotFoundException, BadRequestException, StaleObjectException
    {
        final ProductDto newProduct10 = new ProductDto();
        newProduct10.setProductCatalogueId(PRODUCT_CATALOGUE_1_ID);
        newProduct10.setSchedulingRegime(SCHEDULING_REGIME_100);

        final ProductDto existingProduct20 = new ProductDto();
        existingProduct20.setId(PRODUCT_20_ID);
        existingProduct20.setProductCatalogueId(PRODUCT_CATALOGUE_2_ID);
        existingProduct20.setSchedulingRegime(SCHEDULING_REGIME_200);

        final ProductListDto productListDto = new ProductListDto();
        productListDto.setProductList(Arrays.asList(newProduct10, existingProduct20));

        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(Arrays.asList(existingProduct20));

        this.productService.updateProducts(ASSET_1_ID, productListDto);

        Mockito.verify(this.productDao, Mockito.atLeast(1)).getProducts(ASSET_1_ID);
        Mockito.verify(this.productDao).createProduct(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, SCHEDULING_REGIME_100_ID);
        Mockito.verify(this.productDao).updateProductSchedulingRegime(PRODUCT_20_ID, SCHEDULING_REGIME_200_ID);
        Mockito.verify(this.productDao).findOneWithLock(PRODUCT_20_ID, ProductDto.class, StaleCheckType.PRODUCT);
        Mockito.verifyNoMoreInteractions(this.productDao);

    }

    @Test
    public void addProductsToExistingListWithDelete() throws RecordNotFoundException, BadRequestException, StaleObjectException
    {
        final ProductDto newProduct10 = new ProductDto();
        newProduct10.setProductCatalogueId(PRODUCT_CATALOGUE_1_ID);
        newProduct10.setSchedulingRegime(SCHEDULING_REGIME_100);

        final ProductDto existingProduct20 = new ProductDto();
        existingProduct20.setId(PRODUCT_20_ID);
        existingProduct20.setProductCatalogueId(PRODUCT_CATALOGUE_2_ID);
        existingProduct20.setSchedulingRegime(SCHEDULING_REGIME_200);

        final ProductDto existingProduct30 = new ProductDto();
        existingProduct30.setId(PRODUCT_30_ID);
        existingProduct30.setProductCatalogueId(PRODUCT_CATALOGUE_3_ID);
        existingProduct30.setSchedulingRegime(SCHEDULING_REGIME_200);

        final ProductListDto productListDto = new ProductListDto();
        productListDto.setProductList(Arrays.asList(newProduct10, existingProduct20));

        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(Arrays.asList(existingProduct20, existingProduct30));

        this.productService.updateProducts(ASSET_1_ID, productListDto);

        Mockito.verify(this.productDao, Mockito.atLeast(1)).getProducts(ASSET_1_ID);
        Mockito.verify(this.productDao).createProduct(ASSET_1_ID, PRODUCT_CATALOGUE_1_ID, SCHEDULING_REGIME_100_ID);
        Mockito.verify(this.productDao).updateProductSchedulingRegime(PRODUCT_20_ID, SCHEDULING_REGIME_200_ID);
        Mockito.verify(this.productDao).deleteProduct(PRODUCT_30_ID);
        Mockito.verify(this.productDao).findOneWithLock(PRODUCT_20_ID, ProductDto.class, StaleCheckType.PRODUCT);
        Mockito.verifyNoMoreInteractions(this.productDao);
    }

    @Test
    public void deleteAllProducts() throws RecordNotFoundException, BadRequestException, StaleObjectException
    {
        final ProductDto existingProduct20 = new ProductDto();
        existingProduct20.setId(PRODUCT_20_ID);
        existingProduct20.setProductCatalogueId(PRODUCT_CATALOGUE_2_ID);
        existingProduct20.setSchedulingRegime(SCHEDULING_REGIME_200);

        final ProductDto existingProduct30 = new ProductDto();
        existingProduct30.setId(PRODUCT_30_ID);
        existingProduct30.setProductCatalogueId(PRODUCT_CATALOGUE_3_ID);
        existingProduct30.setSchedulingRegime(SCHEDULING_REGIME_200);

        when(this.productDao.getProducts(ASSET_1_ID)).thenReturn(Arrays.asList(existingProduct20, existingProduct30));

        this.productService.updateProducts(ASSET_1_ID, PRODUCT_CATALOGUE_EMPTY);

        Mockito.verify(this.productDao, Mockito.atLeast(1)).getProducts(ASSET_1_ID);
        Mockito.verify(this.productDao).deleteProduct(PRODUCT_20_ID);
        Mockito.verify(this.productDao).deleteProduct(PRODUCT_30_ID);
        Mockito.verifyNoMoreInteractions(this.productDao);
    }
}
