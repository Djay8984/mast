package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.baesystems.ai.lr.security.rbac.authentication.UserPrincipal;

public class ServiceUtilsImplTest
{

    private final ServiceUtilsImpl serviceUtils = new ServiceUtilsImpl();

    private static Authentication authentication;

    private static final String USERNAME = "username";
    private static final String GROUP = "group";

    @BeforeClass
    public static void setUp()
    {
        authentication = Mockito.mock(Authentication.class);
        final SecurityContext securityContext = Mockito.mock(SecurityContext.class);

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void getSecurityPrincipalTest()
    {

        final UserPrincipal userPrincipal = new UserPrincipal();
        userPrincipal.setUserName(USERNAME);
        Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);
        Mockito.when(authentication.getPrincipal()).thenReturn(userPrincipal);

        final String currentUsername = this.serviceUtils.getSecurityContextPrincipal();

        assertNotNull(currentUsername);
        assertEquals(USERNAME, currentUsername);
    }

    @Test
    public void getSecurityPrincipalNotAuthenticatedTest()
    {
        Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.FALSE);
        assertNull(this.serviceUtils.getSecurityContextPrincipal());
    }

    @Test
    public void testGetUserGroups()
    {
        final List<GrantedAuthority> groupList = new ArrayList<GrantedAuthority>();
        groupList.add(new SimpleGrantedAuthority(GROUP));

        Mockito.when(authentication.isAuthenticated()).thenReturn(Boolean.TRUE);
        BDDMockito.willReturn(groupList).given(authentication).getAuthorities();

        final List<String> result = this.serviceUtils.getUserGroups();
        assertNotNull(result);
        assertEquals(groupList.size(), result.size());
        assertEquals(GROUP, result.get(0));
    }
}
