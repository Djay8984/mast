package com.baesystems.ai.lr.service.defects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectLightDto;
import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class DefectServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long DEFECT_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long INVALID_ID = 999L;
    private static final Integer ZERO = 0;
    private static final Integer ONE = 1;
    private static final Integer TWO = 2;
    private static final Integer FIVE = 5;

    @Mock
    private DefectDao defectDao;

    @Mock
    private WIPDefectDao wipDefectDao;

    @Mock
    private RepairDao repairDao;

    @Mock
    private WIPRepairDao wipRepairDao;

    @Mock
    private CoCDao coCDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private ActionableItemDao actionableItemDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Spy
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final DefectService defectService = new DefectServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final DefectDto expectedDefectDto = new DefectDto();
    private Pageable pageSpecification;
    private CodicilDefectQueryDto codicilDefectQueryDto;

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedDefectDto.setId(DEFECT_1_ID);
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        codicilDefectQueryDto = new CodicilDefectQueryDto();
        codicilDefectQueryDto.setSearchString("Random String");

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(codicilDefectQueryDto, CodicilDefectQueryDto.class))
                .willReturn(codicilDefectQueryDto);

    }

    private PageResource<DefectDto> getPageResource(final DefectDto... defects)
    {
        final ArrayList<DefectDto> tempList = new ArrayList<>();
        for (final DefectDto defect : defects)
        {
            tempList.add(defect);
        }
        final Page<DefectDto> defectDtoPage = new PageImpl<>(tempList);
        return pageFactory.createPageResource(defectDtoPage, DefectPageResourceDto.class);
    }

    @Test
    public void getDefectFound() throws RecordNotFoundException
    {
        when(this.defectDao.getDefectLight(DEFECT_1_ID)).thenReturn(expectedDefectDto);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        final DefectLightDto defect = this.defectService.getDefectLight(DEFECT_1_ID);
        assertEquals(expectedDefectDto, defect);
    }

    @Test
    public void getDefectNotFound() throws RecordNotFoundException
    {
        when(this.defectDao.getDefect(INVALID_ID)).thenReturn(null);
        thrown.expect(RecordNotFoundException.class);
        this.defectService.getDefect(INVALID_ID);
    }

    @Test
    public void createDefectSimple() throws RecordNotFoundException
    {
        expectedDefectDto.setDefectCategory(new DefectCategoryDto());
        expectedDefectDto.getDefectCategory().setId(1L);

        when(defectDao.createDefect(expectedDefectDto)).thenReturn(expectedDefectDto);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final DefectDto defect = defectService.createDefect(expectedDefectDto);
        assertEquals(expectedDefectDto, defect);
    }

    @Test
    public void updateDefectSuccessful() throws RecordNotFoundException
    {
        when(this.defectDao.getDefect(DEFECT_1_ID)).thenReturn(expectedDefectDto);
        when(this.defectDao.updateDefect(expectedDefectDto)).thenReturn(expectedDefectDto);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final DefectDto defect = this.defectService.updateDefect(DEFECT_1_ID, expectedDefectDto);
        assertEquals(expectedDefectDto, defect);
    }

    @Test
    public void updateDefectInvalidModel() throws RecordNotFoundException
    {
        thrown.expect(RecordNotFoundException.class);
        this.defectService.updateDefect(DEFECT_1_ID, null);
    }

    @Test
    public void closeDefectSuccessful() throws BadRequestException
    {
        when(this.defectDao.closeDefect(DEFECT_1_ID)).thenReturn(true);
        assertTrue(this.defectService.closeDefect(DEFECT_1_ID));
    }

    @Test
    public void closeWIPDefectSuccessful() throws BadRequestException
    {
        when(this.wipDefectDao.closeDefect(DEFECT_1_ID)).thenReturn(true);
        assertTrue(this.defectService.closeWIPDefect(DEFECT_1_ID));
    }

    @Test
    public void getDefectsForAssetSuccessful() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPagedResource = getPageResource(expectedDefectDto);
        when(this.defectDao.getDefectsForAsset(pageSpecification, ASSET_1_ID, null)).thenReturn(expectedPagedResource);
        when(this.repairDao.countRepairsForDefect(any(Long.class))).thenReturn(ZERO);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final PageResource<DefectDto> dtoPageResource = this.defectService.getDefectsForAsset(null, null, null, null, ASSET_1_ID, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetDefectsForAssetInvalidAssetId() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPagedResource = getPageResource();
        when(this.defectDao.getDefectsForAsset(pageSpecification, INVALID_ID, null)).thenReturn(expectedPagedResource);
        when(this.repairDao.countRepairsForDefect(any(Long.class))).thenReturn(ZERO);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final PageResource<DefectDto> dtoPageResource = this.defectService.getDefectsForAsset(null, null, null, null, INVALID_ID, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetWIPDefectsForJobSuccessful() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPagedResource = getPageResource(expectedDefectDto);
        when(this.wipDefectDao.findAllForJob(pageSpecification, JOB_1_ID, null)).thenReturn(expectedPagedResource);
        when(this.wipRepairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.wipCoCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.wipActionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final PageResource<DefectDto> dtoPageResource = this.defectService.getWIPDefectsForJob(JOB_1_ID, null, null, null, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetWIPDefectsForJobInvalidJobId() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPagedResource = getPageResource();
        when(this.wipDefectDao.findAllForJob(pageSpecification, INVALID_ID, null)).thenReturn(expectedPagedResource);
        when(this.repairDao.countRepairsForDefect(any(Long.class))).thenReturn(ZERO);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(ZERO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(ZERO);

        final PageResource<DefectDto> dtoPageResource = this.defectService.getWIPDefectsForJob(INVALID_ID, null, null, null, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetDefectFoundTestCounts() throws RecordNotFoundException
    {
        when(this.defectDao.getDefectLight(DEFECT_1_ID)).thenReturn(expectedDefectDto);
        when(this.repairDao.countRepairsForDefect(DEFECT_1_ID)).thenReturn(ONE);
        when(this.coCDao.countCoCsForDefect(DEFECT_1_ID)).thenReturn(TWO);
        when(this.actionableItemDao.countActionableItemsForDefect(DEFECT_1_ID)).thenReturn(TWO);
        final DefectLightDto defect = this.defectService.getDefectLight(DEFECT_1_ID);
        assertEquals(ONE, defect.getRepairCount());
        assertEquals(FIVE, defect.getCourseOfActionCount());
    }

    @Test
    public void testQueryDefect() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPageResource = getPageResource();
        when(defectDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<DefectDto> dtoPageResource = this.defectService
                .getDefectsByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryDefectNonFound() throws BadPageRequestException
    {
        final PageResource<DefectDto> emptyPageResource = getEmptyPageResource();
        when(defectDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<DefectDto> dtoPageResource = this.defectService
                .getDefectsByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPDefect() throws BadPageRequestException
    {
        final PageResource<DefectDto> expectedPageResource = getPageResource();
        when(wipDefectDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<DefectDto> dtoPageResource = this.defectService
                .getWIPDefectsByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPDefectNonFound() throws BadPageRequestException
    {
        final PageResource<DefectDto> emptyPageResource = getEmptyPageResource();
        when(wipDefectDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<DefectDto> dtoPageResource = this.defectService
                .getWIPDefectsByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }

    private PageResource<DefectDto> getEmptyPageResource()
    {
        final Page<DefectDto> defectDtoPage = new PageImpl<>(new ArrayList<DefectDto>());
        return pageFactory.createPageResource(defectDtoPage, DefectPageResourceDto.class);
    }
}
