package com.baesystems.ai.lr.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;

public class QueryDtoUtilsTest
{
    private static final String TEST_STRING = "TestString";
    private static final String TEST_STRING_WITH_PERCENT_SIGNS = "%TestString%";
    private static final String TEST_STRING_WITH_PERCENT_SIGNS_ESCAPED = "\\%TestString\\%";
    private static final String TEST_STRING_WITH_WILDCARDS = "*TestString*";
    private static final String TEST_STRING_WITH_WILDCARDS_CONVERTED = "%TestString%";
    private static final String TEST_STRING_WITH_WILDCARD_MIX = "*Tes_String%";
    private static final String TEST_STRING_WITH_WILDCARD_MIX_CONVERTED = "%Tes\\_String\\%";

    @Test
    public void testQueryDtoCleanerReplacesEmptyList()
    {
        final CodicilDefectQueryDto orig = new CodicilDefectQueryDto();

        // These should remain unchanged
        orig.setCategoryList(Collections.singletonList(1L));
        orig.setDueDateMax(new Date());
        orig.setDueDateMin(null);
        orig.setConfidentialityList(Arrays.asList(1L, 2L));
        orig.setItemTypeList(null);
        orig.setSearchString(TEST_STRING);

        // This should be nulled
        orig.setStatusList(Collections.emptyList());

        final CodicilDefectQueryDto clean = QueryDtoUtils.sanitiseQueryDto(orig, CodicilDefectQueryDto.class);

        Assert.assertEquals(orig.getCategoryList(), clean.getCategoryList());
        Assert.assertEquals(orig.getDueDateMax(), clean.getDueDateMax());
        Assert.assertEquals(orig.getDueDateMin(), clean.getDueDateMin());
        Assert.assertEquals(orig.getConfidentialityList(), clean.getConfidentialityList());
        Assert.assertEquals(orig.getItemTypeList(), clean.getItemTypeList());
        Assert.assertEquals(orig.getSearchString(), clean.getSearchString());

        Assert.assertEquals(clean.getStatusList(), null);
    }

    @Test
    public void testQueryDtoCleanerEscapesPercentages()
    {
        final CodicilDefectQueryDto orig = new CodicilDefectQueryDto();
        orig.setSearchString(TEST_STRING_WITH_PERCENT_SIGNS);
        final CodicilDefectQueryDto clean = QueryDtoUtils.sanitiseQueryDto(orig, CodicilDefectQueryDto.class);
        Assert.assertEquals(clean.getSearchString(), TEST_STRING_WITH_PERCENT_SIGNS_ESCAPED);
    }

    @Test
    public void testQueryDtoCleanerConvertsWildcards()
    {
        final CodicilDefectQueryDto orig = new CodicilDefectQueryDto();
        orig.setSearchString(TEST_STRING_WITH_WILDCARDS);
        final CodicilDefectQueryDto clean = QueryDtoUtils.sanitiseQueryDto(orig, CodicilDefectQueryDto.class);
        Assert.assertEquals(clean.getSearchString(), TEST_STRING_WITH_WILDCARDS_CONVERTED);
    }

    @Test
    public void testQueryDtoCleanerConvertsPercentageAndWildcardMix()
    {
        final CodicilDefectQueryDto orig = new CodicilDefectQueryDto();
        orig.setSearchString(TEST_STRING_WITH_WILDCARD_MIX);
        final CodicilDefectQueryDto clean = QueryDtoUtils.sanitiseQueryDto(orig, CodicilDefectQueryDto.class);
        Assert.assertEquals(clean.getSearchString(), TEST_STRING_WITH_WILDCARD_MIX_CONVERTED);
    }
}
