package com.baesystems.ai.lr.service.assets;

import static org.junit.Assert.assertNull;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.baesystems.ai.lr.dao.assets.ItemDaoImpl;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.AttributePageResourceDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.DtoUtils;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.VersioningHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;

public class AttributeServiceImplTest extends PageResourceImplBasedTest
{
    @Mock
    private AttributeDao attributeDao;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @Mock
    private ItemPersistDao itemPersistDao;

    @Mock
    private ServiceHelper serviceHelper;

    @Mock
    private ItemService itemService;

    @Mock
    private VersioningHelper versioningHelper;

    @Mock
    private AssetVersionUpdateService assetVersionUpdateService;

    @Mock
    private ItemDaoImpl itemDao;

    @InjectMocks
    private final AttributeService attributeService = new AttributeServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long ATTRIBUTE_1_ID = 1L;
    private static final Long ATTRIBUTE_2_ID = 2L;
    private static final Long ITEM_1_ID = 2L;
    private static final Long ASSET_1 = 1L;
    private static final Long ATTRIBUTE_TYPE_ID = 3L;
    private static final String VALUE_1 = "VALUE_1";
    private static final String VALUE_2 = "VALUE_2";

    private static final Integer ZERO = 0;
    private static final Integer ONE = 1;
    private static final Integer TWO = 2;
    private static final Integer THREE = 3;
    private static final Integer FOUR = 4;
    private static final Integer FIVE = 5;
    private static final Integer TEN = 10;

    private AttributeDto attributeDto1;
    private AttributeDto attributeDto1Updated;
    private AttributeTypeDto attributeTypeDto;
    private PageResourceFactory pageFactory;
    private Pageable pageSpecification;
    private VersionedItemPK versionedItemPK = new VersionedItemPK();

    @Before
    public void setUp() throws Exception
    {
        attributeDto1 = new AttributeDto();
        attributeDto1.setId(ATTRIBUTE_1_ID);
        attributeDto1.setValue(VALUE_1);
        attributeDto1.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_ID));

        attributeDto1Updated = new AttributeDto();
        attributeDto1Updated.setId(ATTRIBUTE_1_ID);
        attributeDto1Updated.setValue(VALUE_2);
        attributeDto1Updated.setAttributeType(new LinkResource(ATTRIBUTE_TYPE_ID));

        attributeTypeDto = new AttributeTypeDto();
        attributeTypeDto.setId(ATTRIBUTE_TYPE_ID);

        pageFactory = new PageResourceFactoryImpl();
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        attributeTypeDto.setId(ATTRIBUTE_TYPE_ID);
    }

    @Test
    public void testUpdateAttributePasses() throws RecordNotFoundException
    {
        when(this.attributeDao.getAttribute(ATTRIBUTE_1_ID)).thenReturn(attributeDto1);
        when(this.attributeDao.updateAttribute(ITEM_1_ID, attributeDto1)).thenReturn(attributeDto1Updated);
        Assert.assertEquals(attributeDto1Updated, this.attributeService.updateAttribute(ITEM_1_ID, attributeDto1));
    }

    @Test
    public void testDeleteAttributePassesEx4Min1() throws Exception
    {
        testDelete(FOUR, ONE);
    }

    @Test
    public void testDeleteAttributeFailsEx2Min2() throws Exception
    {
        thrown.expect(MastBusinessException.class);
        testDelete(TWO, TWO);
    }

    private void testDelete(final Integer numberOfExistingAttributes, final Integer minAllowed) throws MastBusinessException, RecordNotFoundException
    {
        final Long draftVersion = 123L;

        final AttributeDto upversionedAttribute = DtoUtils.simpleCopy(attributeDto1);
        upversionedAttribute.setId(upversionedAttribute.getId() + 1);

        final LazyItemDto upversionedItem = new LazyItemDto();
        upversionedItem.setAttributes(Collections.singletonList(upversionedAttribute));

        when(this.versioningHelper.getVersionedItemPK(anyLong(), anyLong(), anyLong(), anyBoolean())).thenReturn(versionedItemPK);
        when(this.attributeDao.getAttribute(ATTRIBUTE_1_ID)).thenReturn(attributeDto1);
        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(attributeTypeDto);
        when(this.attributeDao.countAttributesByItemAndAttributeType(ITEM_1_ID, ATTRIBUTE_TYPE_ID, versionedItemPK.getAssetVersionId()))
                .thenReturn(numberOfExistingAttributes);
        when(this.assetVersionUpdateService.upVersionSingleItem(anyLong(), anyLong(), anyBoolean())).thenReturn(draftVersion);
        when(this.itemDao.getItem(any(VersionedItemPK.class))).thenReturn(upversionedItem);
        attributeTypeDto.setMinOccurs(minAllowed);
        this.attributeService.deleteAttribute(ASSET_1, ITEM_1_ID, ATTRIBUTE_1_ID);
    }

    @Test
    public void testCreateAttributePassesEx5Max10() throws Exception
    {
        testCreate(FIVE, TEN);
    }

    @Test
    public void testCreateAttributePassesEx1MaxUnlimited() throws Exception
    {
        testCreate(ONE, ZERO);
    }

    @Test
    public void testCreateAttributePassesEx2Max3() throws Exception
    {
        testCreate(TWO, THREE);
    }

    @Test
    public void testCreateAttributeFailsEx1Max1() throws Exception
    {
        thrown.expect(MastBusinessException.class);
        testCreate(ONE, ONE);
    }

    private void testCreate(final Integer numberOfExistingAttributes, final Integer maxAllowed) throws MastBusinessException, RecordNotFoundException
    {
        when(this.attributeDao.createAttribute(attributeDto1, versionedItemPK)).thenReturn(attributeDto1);
        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(attributeTypeDto);
        when(this.attributeDao.countAttributesByItemAndAttributeType(ITEM_1_ID, ATTRIBUTE_TYPE_ID, versionedItemPK.getAssetVersionId()))
                .thenReturn(numberOfExistingAttributes);
        when(this.versioningHelper.getVersionedItemPK(anyLong(), anyLong(), anyLong(), anyBoolean())).thenReturn(versionedItemPK);
        when(this.itemDao.getItemLight(versionedItemPK)).thenReturn(new ItemLightDto());
        attributeTypeDto.setMaxOccurs(maxAllowed);
        this.attributeService.createAttribute(ASSET_1, ITEM_1_ID, attributeDto1);
    }

    @Test
    public void testGetAttributesForItemPasses() throws Exception
    {
        final PageResource<AttributeDto> expectedPagedResource = getPageResource();
        when(this.attributeDao.findAttributesByItem(ITEM_1_ID, pageSpecification)).thenReturn(expectedPagedResource);
        final PageResource<AttributeDto> dtoPageResource = attributeService.getAttributes(null, null, null, null, ITEM_1_ID);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetAttributesForItemFails() throws Exception
    {
        final PageResource<AttributeDto> emptyPageResource = new AttributePageResourceDto();
        emptyPageResource.setContent(new ArrayList<>());
        when(this.attributeDao.findAttributesByItem(ITEM_1_ID, pageSpecification)).thenReturn(emptyPageResource);
        final PageResource<AttributeDto> dtoPageResource = attributeService.getAttributes(null, null, null, null, ITEM_1_ID);
        assertTrue(dtoPageResource.getContent().isEmpty());
    }

    private PageResource<AttributeDto> getPageResource()
    {
        final ArrayList<AttributeDto> tempList = new ArrayList<>();
        tempList.add(attributeDto1);
        final Page<AttributeDto> attributeDtoPage = new PageImpl<>(tempList);
        return pageFactory.createPageResource(attributeDtoPage, AttributePageResourceDto.class);
    }

    @Test
    public void testUpdateAttributesWhereNoChangesHaveBeenMade() throws RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final ItemWithAttributesDto itemWithAttributesDto = new ItemWithAttributesDto();
        itemWithAttributesDto.setId(ITEM_1_ID);
        attributeDto1.setValue("Attribute new value");
        final List<AttributeDto> attributes = Arrays.asList(attributeDto1);
        itemWithAttributesDto.setAttributes(attributes);

        when(this.attributeDao.findAttributesByItem(ITEM_1_ID)).thenReturn(attributes);
        final String newItemName = "New Item Name";
        when(this.itemService.updateItemName(itemWithAttributesDto)).thenReturn(newItemName);

        final ItemWithAttributesDto responseDto = this.attributeService.updateAttributes(ASSET_1, ITEM_1_ID, itemWithAttributesDto);

        assertNull(responseDto.getName());

        Mockito.verifyNoMoreInteractions(itemService, itemPersistDao, assetReferenceDataDao);
    }

    @Test
    public void testUpdateAttributesWhereAnAttributeHasBeenUpdated() throws RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final ItemWithAttributesDto itemDtoPassedIn = new ItemWithAttributesDto();
        itemDtoPassedIn.setId(ITEM_1_ID);

        final AttributeDto passedAttribute = new AttributeDto();
        passedAttribute.setId(ATTRIBUTE_1_ID);
        passedAttribute.setValue(VALUE_1);
        final LinkResource attributeType = new LinkResource(ATTRIBUTE_TYPE_ID);
        passedAttribute.setAttributeType(attributeType);
        final List<AttributeDto> passedAttributes = Arrays.asList(passedAttribute);
        itemDtoPassedIn.setAttributes(passedAttributes);

        final AttributeDto existingAttribute = new AttributeDto();
        existingAttribute.setId(ATTRIBUTE_1_ID);
        existingAttribute.setValue(VALUE_2);
        existingAttribute.setAttributeType(attributeType);

        final List<AttributeDto> existingAttributes = Arrays.asList(existingAttribute);

        when(this.attributeDao.updateAttribute(ITEM_1_ID, passedAttribute)).thenReturn(passedAttribute);
        when(this.attributeDao.findAttributesByItem(ITEM_1_ID)).thenReturn(existingAttributes);
        final String newItemName = "New Item Name";
        when(this.itemService.updateItemName(itemDtoPassedIn)).thenReturn(newItemName);

        final ItemWithAttributesDto responseDto = this.attributeService.updateAttributes(ASSET_1, ITEM_1_ID, itemDtoPassedIn);

        assertEquals(newItemName, responseDto.getName());

        verify(attributeDao).findAttributesByItem(ITEM_1_ID);
        verify(attributeDao).updateAttribute(ITEM_1_ID, passedAttribute);
        verify(itemPersistDao).setItemAndAllAncestorsToRequireAReview(ITEM_1_ID);
        verify(itemService).updateItemName(itemDtoPassedIn);
        verifyNoMoreInteractions(itemService, itemPersistDao, attributeDao, assetReferenceDataDao);
    }

    @Test
    public void testUpdateAttributesWhereAnAttributeHasBeenInserted() throws RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        when(this.versioningHelper.getVersionedItemPK(anyLong(), anyLong(), anyLong(), anyBoolean())).thenReturn(versionedItemPK);
        when(this.itemDao.getItemLight(versionedItemPK)).thenReturn(new ItemLightDto());

        final ItemWithAttributesDto itemDtoPassedIn = new ItemWithAttributesDto();
        itemDtoPassedIn.setId(ITEM_1_ID);

        final AttributeDto passedAttributeOne = new AttributeDto();
        passedAttributeOne.setId(ATTRIBUTE_1_ID);
        passedAttributeOne.setValue(VALUE_1);

        final AttributeDto passedAttributeTwo = new AttributeDto();
        passedAttributeTwo.setId(null);
        passedAttributeTwo.setValue(VALUE_1);

        final AttributeDto createdDto = new AttributeDto();
        createdDto.setId(ATTRIBUTE_2_ID);
        createdDto.setValue(VALUE_1);

        final LinkResource attributeType = new LinkResource(ATTRIBUTE_TYPE_ID);
        passedAttributeOne.setAttributeType(attributeType);
        passedAttributeTwo.setAttributeType(attributeType);
        createdDto.setAttributeType(attributeType);

        final List<AttributeDto> passedAttributes = Arrays.asList(passedAttributeOne, passedAttributeTwo);
        itemDtoPassedIn.setAttributes(passedAttributes);

        final AttributeDto existingAttribute = new AttributeDto();
        existingAttribute.setId(ATTRIBUTE_1_ID);
        existingAttribute.setValue(VALUE_1);
        existingAttribute.setAttributeType(attributeType);

        final List<AttributeDto> existingAttributes = Arrays.asList(existingAttribute);

        when(this.attributeDao.findAttributesByItem(ITEM_1_ID)).thenReturn(existingAttributes);

        final AttributeTypeDto localAttributeTypeDto = new AttributeTypeDto();
        localAttributeTypeDto.setId(ATTRIBUTE_TYPE_ID);
        localAttributeTypeDto.setMaxOccurs(ZERO);
        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(localAttributeTypeDto);

        when(this.attributeDao.createAttribute(passedAttributeTwo, versionedItemPK)).thenReturn(createdDto);

        final String newItemName = "New Item Name";
        when(this.itemService.updateItemName(itemDtoPassedIn)).thenReturn(newItemName);

        when(attributeDao.countAttributesByItemAndAttributeType(ASSET_1, ITEM_1_ID, ATTRIBUTE_TYPE_ID)).thenReturn(ZERO);

        final ItemWithAttributesDto responseDto = this.attributeService.updateAttributes(ASSET_1, ITEM_1_ID, itemDtoPassedIn);

        assertEquals(newItemName, responseDto.getName());
        Assert.assertArrayEquals(Arrays.asList(passedAttributeOne, createdDto).toArray(), responseDto.getAttributes().toArray());

        verify(attributeDao).findAttributesByItem(ITEM_1_ID);
        verify(attributeDao).createAttribute(passedAttributeTwo, versionedItemPK);
        verify(attributeDao).countAttributesByItemAndAttributeType(ITEM_1_ID, ATTRIBUTE_TYPE_ID, versionedItemPK.getAssetVersionId());
        verify(itemPersistDao).setItemAndAllAncestorsToRequireAReview(ITEM_1_ID);
        verify(itemService).updateItemName(itemDtoPassedIn);
        verify(assetReferenceDataDao).getAttributeType(ATTRIBUTE_TYPE_ID);

        verifyNoMoreInteractions(itemService, itemPersistDao, attributeDao, assetReferenceDataDao);
    }

    @Test
    public void testUpdateAttributesWhereAnAttributeHasBeenDeleted() throws RecordNotFoundException, MastBusinessException, BadPageRequestException
    {
        final ItemWithAttributesDto itemDtoPassedIn = new ItemWithAttributesDto();
        itemDtoPassedIn.setId(ITEM_1_ID);

        final AttributeDto passedAttributeOne = new AttributeDto();
        passedAttributeOne.setId(ATTRIBUTE_1_ID);
        passedAttributeOne.setValue(VALUE_1);

        final LinkResource attributeType = new LinkResource(ATTRIBUTE_TYPE_ID);
        passedAttributeOne.setAttributeType(attributeType);

        final List<AttributeDto> passedAttributes = Arrays.asList(passedAttributeOne);
        itemDtoPassedIn.setAttributes(passedAttributes);

        final AttributeDto existingAttributeOne = new AttributeDto();
        existingAttributeOne.setId(ATTRIBUTE_1_ID);
        existingAttributeOne.setValue(VALUE_1);
        existingAttributeOne.setAttributeType(attributeType);

        final AttributeDto existingAttributeTwo = new AttributeDto();
        existingAttributeTwo.setId(ATTRIBUTE_2_ID);
        existingAttributeTwo.setValue(VALUE_1);
        existingAttributeTwo.setAttributeType(attributeType);

        final AttributeDto upversionedAttribute = DtoUtils.simpleCopy(attributeDto1);
        upversionedAttribute.setId(upversionedAttribute.getId() + 1);

        final LazyItemDto upversionedItem = new LazyItemDto();
        upversionedItem.setAttributes(Collections.singletonList(upversionedAttribute));
        when(this.itemDao.getItem(any(VersionedItemPK.class))).thenReturn(upversionedItem);

        final List<AttributeDto> existingAttributes = Arrays.asList(existingAttributeOne, existingAttributeTwo);

        when(this.attributeDao.findAttributesByItem(ITEM_1_ID)).thenReturn(existingAttributes);

        final AttributeTypeDto localAttributeTypeDto = new AttributeTypeDto();
        localAttributeTypeDto.setId(ATTRIBUTE_TYPE_ID);
        localAttributeTypeDto.setMaxOccurs(ZERO);
        localAttributeTypeDto.setMinOccurs(ZERO);
        when(this.assetReferenceDataDao.getAttributeType(ATTRIBUTE_TYPE_ID)).thenReturn(localAttributeTypeDto);

        final String newItemName = "New Item Name";
        when(this.itemService.updateItemName(itemDtoPassedIn)).thenReturn(newItemName);

        when(attributeDao.countAttributesByItemAndAttributeType(ITEM_1_ID, ATTRIBUTE_TYPE_ID, versionedItemPK.getAssetVersionId())).thenReturn(ONE);
        when(this.versioningHelper.getVersionedItemPK(anyLong(), anyLong(), anyLong(), anyBoolean())).thenReturn(versionedItemPK);

        final ItemWithAttributesDto responseDto = this.attributeService.updateAttributes(ASSET_1, ITEM_1_ID, itemDtoPassedIn);

        assertEquals(newItemName, responseDto.getName());
        Assert.assertArrayEquals(Arrays.asList(passedAttributeOne).toArray(), responseDto.getAttributes().toArray());

        verify(attributeDao).findAttributesByItem(ITEM_1_ID);
        verify(itemPersistDao).setItemAndAllAncestorsToRequireAReview(ITEM_1_ID);
        verify(itemService).updateItemName(itemDtoPassedIn);
        verify(assetReferenceDataDao).getAttributeType(ATTRIBUTE_TYPE_ID);
        verify(attributeDao).countAttributesByItemAndAttributeType(ITEM_1_ID, ATTRIBUTE_TYPE_ID, versionedItemPK.getAssetVersionId());
        verify(attributeDao).deleteAttribute(ATTRIBUTE_2_ID);

        verifyNoMoreInteractions(itemService, itemPersistDao, attributeDao, assetReferenceDataDao);
    }
}
