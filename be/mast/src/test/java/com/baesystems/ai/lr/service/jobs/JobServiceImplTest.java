package com.baesystems.ai.lr.service.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.baesystems.ai.lr.service.assets.AssetVersionUpdateService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.employees.OfficeDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.jobs.JobPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.JobQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class JobServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long JOB_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long EMPLOYEE_1_ID = 1L;
    private static final Long OFFICE_1_ID = 1L;
    private static final Long INVALID_ID = 999L;
    private static final String OFFICE_NAME = "ABERDEEN";

    @Mock
    private JobDao jobDao;

    @Mock
    private OfficeDao officeDao;

    @Mock
    private WIPAssetNoteDao wipAssetNoteDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private SurveyDao surveyDao;

    @Mock
    private AssetVersionUpdateService assetVersionUpdateService;

    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @InjectMocks
    private final JobService jobService = new JobServiceImpl();

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final JobDto expectedJobDto = new JobDto();
    private Pageable pageSpecification;

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedJobDto.setId(JOB_1_ID);
        expectedJobDto.setScopeConfirmed(true);
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
    }

    private PageResource<JobDto> getPageResource()
    {
        final ArrayList<JobDto> tempList = new ArrayList<>();
        tempList.add(expectedJobDto);
        final Page<JobDto> jobDtoPage = new PageImpl<>(tempList);
        return pageFactory.createPageResource(jobDtoPage, JobPageResourceDto.class);
    }

    @Test
    public void getJobsForEmployeeAndOfficeSuccessful() throws BadPageRequestException
    {
        final PageResource<JobDto> expectedPagedResource = getPageResource();
        final JobQueryDto query = new JobQueryDto();
        query.setEmployeeId(Arrays.asList(EMPLOYEE_1_ID));
        query.setOfficeId(Arrays.asList(OFFICE_1_ID));

        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(query, JobQueryDto.class))
                .willReturn(query);

        when(this.jobDao.getJobs(pageSpecification, query)).thenReturn(expectedPagedResource);
        final PageResource<JobDto> dtoPageResource = this.jobService.getJobs(null, null, null, null, null, null, query);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void getJobsForEmployeeAndOfficeInvalidEmployeeIdAndOfficeId() throws BadPageRequestException
    {
        when(this.jobDao.getJobsForAsset(pageSpecification, INVALID_ID)).thenReturn(null);
        assertNull(this.jobService.getJobsForAsset(null, null, null, null, INVALID_ID));
    }

    @Test
    public void getJobsForAssetSuccessful() throws BadPageRequestException
    {
        final PageResource<JobDto> expectedPagedResource = getPageResource();
        when(this.jobDao.getJobsForAsset(pageSpecification, ASSET_1_ID)).thenReturn(expectedPagedResource);
        final PageResource<JobDto> dtoPageResource = this.jobService.getJobsForAsset(null, null, null, null, ASSET_1_ID);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void getJobsForAssetInvalidAssetId() throws BadPageRequestException
    {
        when(this.jobDao.getJobsForAsset(pageSpecification, INVALID_ID)).thenReturn(null);
        assertNull(this.jobService.getJobsForAsset(null, null, null, null, INVALID_ID));
    }

    @Test
    public void getJobFound() throws RecordNotFoundException
    {
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(expectedJobDto);
        when(this.officeDao.getOfficeName(OFFICE_1_ID)).thenReturn(OFFICE_NAME);
        final JobDto job = this.jobService.getJob(JOB_1_ID);
        assertEquals(expectedJobDto, job);
    }

    @Test
    public void getJobNotFound() throws RecordNotFoundException
    {
        when(this.jobDao.getJob(INVALID_ID)).thenReturn(null);
        thrown.expect(RecordNotFoundException.class);
        this.jobService.getJob(INVALID_ID);
    }

    @Test
    public void createJobSimple() throws RecordNotFoundException
    {
        when(this.jobDao.createJob(expectedJobDto)).thenReturn(expectedJobDto);
        when(this.jobDao.updateJob(expectedJobDto)).thenReturn(expectedJobDto);
        final JobDto job = this.jobService.createJob(expectedJobDto);
        assertEquals(expectedJobDto, job);
    }

    @Test
    public void updateJobSuccessful() throws RecordNotFoundException, BadPageRequestException, MastBusinessException
    {
        final List<AssetNoteDto> assetNoteDtos = Arrays.asList(new AssetNoteDto(), new AssetNoteDto(), new AssetNoteDto());
        assetNoteDtos.stream().forEach(anDto -> anDto.setJobScopeConfirmed(false));
        final List<ActionableItemDto> actionableItemDtos = Arrays.asList(new ActionableItemDto(), new ActionableItemDto(), new ActionableItemDto());
        actionableItemDtos.stream().forEach(aiDto -> aiDto.setJobScopeConfirmed(false));
        final List<CoCDto> coCDtos = Arrays.asList(new CoCDto(), new CoCDto(), new CoCDto());
        coCDtos.stream().forEach(cocDto -> cocDto.setJobScopeConfirmed(false));
        expectedJobDto.setJobStatus(new LinkResource());
        expectedJobDto.getJobStatus().setId(1L);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(expectedJobDto);
        when(this.jobDao.updateJob(expectedJobDto)).thenReturn(expectedJobDto);
        when(this.wipAssetNoteDao.getAssetNotesForJob(JOB_1_ID, JobServiceImpl.AN_SCOPE_CONFIRMED_APPLICABLE_STATUSES)).thenReturn(assetNoteDtos);
        when(this.wipActionableItemDao.getActionableItemsForJob(JOB_1_ID, JobServiceImpl.AI_SCOPE_CONFIRMED_APPLICABLE_STATUSES))
                .thenReturn(actionableItemDtos);
        when(this.wipCoCDao.getCoCsForJob(JOB_1_ID, JobServiceImpl.COC_SCOPE_CONFIRMED_APPLICABLE_STATUSES)).thenReturn(coCDtos);

        final JobDto job = this.jobService.updateJob(JOB_1_ID, expectedJobDto);
        assertEquals(expectedJobDto, job);
    }

    @Test
    public void updateJobInvalidModel() throws RecordNotFoundException, BadPageRequestException, MastBusinessException
    {
        thrown.expect(RecordNotFoundException.class);
        this.jobService.updateJob(JOB_1_ID, null);
    }

}
