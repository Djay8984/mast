package com.baesystems.ai.lr.service.tasks;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.baesystems.ai.lr.dto.tasks.TREMultiServiceSuccessResponseDto;
import com.baesystems.ai.lr.dto.tasks.TRESingleServiceSuccessResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class TREHandlerTest
{
    private static final String TASK_SERVICE_ENDPOINT = "http://localhost:9099/taskGeneration/";
    private static final String GET_TASKS_FOR_SERVICE_PATH = "tasksForAssetService?assetId={assetId}&assetServiceId={serviceId}";
    private static final String GET_TASKS_FOR_ASSET_PATH = "tasksForAsset?assetId={assetId}";

    private static final Long ASSET_ID = 123L;
    private static final Long SERVICE_ID = 456L;

    private static ObjectMapper objectMapper;

    private static URI targetUrlGetTasksForService;
    private static URI targetUrlGetTasksForAsset;

    private static TRESingleServiceSuccessResponseDto singleServiceSuccessResponse;
    private static TREMultiServiceSuccessResponseDto multiServiceSuccessResponse;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private TREHandler treHandler = new TREHandler();

    @BeforeClass
    public static void startUp()
    {
        System.setProperty("tre.service.url", TASK_SERVICE_ENDPOINT);
        objectMapper = new ObjectMapper();

        targetUrlGetTasksForService = constructGetTasksForServiceUri();
        targetUrlGetTasksForAsset = constructGetTasksForAssetUri();
    }

    @Before
    public void setUp() throws IOException
    {
        singleServiceSuccessResponse = objectMapper.readValue(
                this.getClass().getResourceAsStream("/tasks/TasksForServiceSuccessResponse-001.json"), TRESingleServiceSuccessResponseDto.class);

        multiServiceSuccessResponse = objectMapper.readValue(
                this.getClass().getResourceAsStream("/tasks/TasksForAssetSuccessResponse-001.json"), TREMultiServiceSuccessResponseDto.class);
    }

    private static URI constructGetTasksForServiceUri()
    {
        final Map<String, String> uriVariables = new HashMap<String, String>();
        uriVariables.put("assetId", ASSET_ID.toString());
        uriVariables.put("serviceId", SERVICE_ID.toString());

        final URI uri = UriComponentsBuilder.fromHttpUrl(TASK_SERVICE_ENDPOINT + GET_TASKS_FOR_SERVICE_PATH)
                .buildAndExpand(uriVariables).toUri();

        return uri;
    }

    private static URI constructGetTasksForAssetUri()
    {
        final Map<String, String> uriVariables = new HashMap<String, String>();
        uriVariables.put("assetId", ASSET_ID.toString());

        final URI uri = UriComponentsBuilder.fromHttpUrl(TASK_SERVICE_ENDPOINT + GET_TASKS_FOR_ASSET_PATH)
                .buildAndExpand(uriVariables).toUri();

        return uri;
    }

    @Test
    public void getTasksForService()
    {
        final ResponseEntity<TRESingleServiceSuccessResponseDto> baseResponseDto = new ResponseEntity<>(singleServiceSuccessResponse, HttpStatus.OK);

        when(this.restTemplate.getForEntity(targetUrlGetTasksForService, TRESingleServiceSuccessResponseDto.class)).thenReturn(baseResponseDto);

        final TRESingleServiceSuccessResponseDto response = treHandler.getTasksForService(ASSET_ID, SERVICE_ID);

        assertEquals(singleServiceSuccessResponse.getService(), response.getService());
    }

    @Test
    public void getTasksForAsset()
    {
        final ResponseEntity<TREMultiServiceSuccessResponseDto> baseResponseDto = new ResponseEntity<>(multiServiceSuccessResponse, HttpStatus.OK);

        when(this.restTemplate.getForEntity(targetUrlGetTasksForAsset, TREMultiServiceSuccessResponseDto.class)).thenReturn(baseResponseDto);

        final TREMultiServiceSuccessResponseDto response = treHandler.getTasksForAsset(ASSET_ID);

        assertEquals(multiServiceSuccessResponse.getServices(), response.getServices());
    }
}
