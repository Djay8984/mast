package com.baesystems.ai.lr.service.defects;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.defects.DeficiencyDao;
import com.baesystems.ai.lr.dao.defects.WIPDeficiencyDao;
import com.baesystems.ai.lr.dto.defects.DeficiencyDto;
import com.baesystems.ai.lr.dto.defects.DeficiencyPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class DeficiencyServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long DEFICIENCY_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;

    @Mock
    private DeficiencyDao deficiencyDao;

    @Mock
    private WIPDeficiencyDao wipDeficiencyDao;

    @Spy
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final DeficiencyService deficiencyService = new DeficiencyServiceImpl();

    private final DeficiencyDto expectedDeficiencyDto = new DeficiencyDto();
    private Pageable pageSpecification;
    private CodicilDefectQueryDto codicilDefectQueryDto;

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedDeficiencyDto.setId(DEFICIENCY_1_ID);
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        codicilDefectQueryDto = new CodicilDefectQueryDto();
        codicilDefectQueryDto.setSearchString("Random String");

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(codicilDefectQueryDto, CodicilDefectQueryDto.class))
                .willReturn(codicilDefectQueryDto);

    }

    private PageResource<DeficiencyDto> getPageResource(final DeficiencyDto... deficiencies)
    {
        final ArrayList<DeficiencyDto> tempList = new ArrayList<>();
        for (final DeficiencyDto deficiency : deficiencies)
        {
            tempList.add(deficiency);
        }
        final Page<DeficiencyDto> deficiencyDtoPage = new PageImpl<>(tempList);
        return pageFactory.createPageResource(deficiencyDtoPage, DeficiencyPageResourceDto.class);
    }

    private PageResource<DeficiencyDto> getEmptyPageResource()
    {
        final Page<DeficiencyDto> deficiencyDtoPage = new PageImpl<>(new ArrayList<DeficiencyDto>());
        return pageFactory.createPageResource(deficiencyDtoPage, DeficiencyPageResourceDto.class);
    }

    @Test
    public void testQueryDeficiency() throws BadPageRequestException
    {
        final PageResource<DeficiencyDto> expectedPageResource = getPageResource();
        when(deficiencyDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<DeficiencyDto> dtoPageResource = this.deficiencyService
                .getDeficienciesByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryDeficiencyNonFound() throws BadPageRequestException
    {
        final PageResource<DeficiencyDto> emptyPageResource = getEmptyPageResource();
        when(deficiencyDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<DeficiencyDto> dtoPageResource = this.deficiencyService
                .getDeficienciesByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPDeficiency() throws BadPageRequestException
    {
        final PageResource<DeficiencyDto> expectedPageResource = getPageResource();
        when(wipDeficiencyDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<DeficiencyDto> dtoPageResource = this.deficiencyService
                .getWIPDeficienciesByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPDefectNonFound() throws BadPageRequestException
    {
        final PageResource<DeficiencyDto> emptyPageResource = getEmptyPageResource();
        when(wipDeficiencyDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<DeficiencyDto> dtoPageResource = this.deficiencyService
                .getWIPDeficienciesByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }
}
