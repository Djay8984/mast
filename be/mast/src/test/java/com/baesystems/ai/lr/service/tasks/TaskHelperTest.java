package com.baesystems.ai.lr.service.tasks;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.assets.ItemMetaDto;
import com.baesystems.ai.lr.utils.VersioningHelper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.tasks.TREAssetItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.CreditStatus;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.references.JobReferenceDataService;

@RunWith(MockitoJUnitRunner.class)
public class TaskHelperTest
{
    private static final Long ASSET_ITEM_1_ID = 1L;
    private static final Long ASSET_ITEM_2_ID = 2L;
    private static final Long SERVICE_99_ID = 99L;
    private static final Long ANY_ID = 123L;
    private static final String TASK_NAME_A = "A";
    private static final String TASK_NAME_B = "B";
    private static final String TASK_NAME_C = "C";
    private static final String SERVICE_CODE = "GPCR";

    private static final Long ASSET_ITEM_1234_ID = 1234L;
    private static final Long WORK_ITEM_ACTION_1_ID = 1L;
    private static final Long WORK_ITEM_ACTION_999_ID = 999L;
    private static final Long WORK_ITEM_TYPE_1_ID = 1L;
    private static final Long WORK_ITEM_TYPE_634_ID = 634L;

    private TREAssetItemDto treAssetItem1;
    private TREAssetItemDto treAssetItem2;
    private List<TREAssetItemDto> treAssetItems;
    private WorkItemLightDto expectedTask1;
    private WorkItemLightDto expectedTask2;
    private WorkItemLightDto expectedTask3;
    private List<WorkItemLightDto> expectedTasks;
    private WorkItemActionDto expectedWorkItemActionA;
    private WorkItemActionDto expectedWorkItemActionB;
    private WorkItemActionDto expectedWorkItemActionC;

    @Mock
    private TREHandler treHandler;

    @Mock
    private JobReferenceDataService jobReferenceDataService;

    @Mock
    private ItemDao itemDao;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private VersioningHelper versioningHelper;

    @Mock
    private AssetDao assetDao;

    @InjectMocks
    private TaskHelper taskHelper = new TaskHelper();

    @Before
    public void setUp()
    {
        expectedWorkItemActionA = createWorkItemAction(TASK_NAME_A);
        expectedWorkItemActionB = createWorkItemAction(TASK_NAME_B);
        expectedWorkItemActionC = createWorkItemAction(TASK_NAME_C);

        treAssetItem1 = createTreAssetItem(ASSET_ITEM_1_ID, TASK_NAME_A, TASK_NAME_B);
        treAssetItem2 = createTreAssetItem(ASSET_ITEM_2_ID, TASK_NAME_C);

        treAssetItems = new ArrayList<TREAssetItemDto>();
        treAssetItems.add(treAssetItem1);
        treAssetItems.add(treAssetItem2);

        expectedTask1 = createTask(ASSET_ITEM_1_ID, expectedWorkItemActionA);
        expectedTask2 = createTask(ASSET_ITEM_1_ID, expectedWorkItemActionB);
        expectedTask3 = createTask(ASSET_ITEM_2_ID, expectedWorkItemActionC);

        expectedTasks = new ArrayList<WorkItemLightDto>();
        expectedTasks.add(expectedTask1);
        expectedTasks.add(expectedTask2);
        expectedTasks.add(expectedTask3);
    }

    private WorkItemActionDto createWorkItemAction(final String name)
    {
        final WorkItemActionDto workItemAction = new WorkItemActionDto();
        workItemAction.setId(ANY_ID);
        workItemAction.setName(name);

        return workItemAction;
    }

    private TREAssetItemDto createTreAssetItem(final Long assetItemId, String... taskNames)
    {
        final TREAssetItemDto treAssetItem = new TREAssetItemDto();
        treAssetItem.setAssetItemIdentifier(assetItemId.intValue());
        treAssetItem.setTasks(new ArrayList<String>());

        for (final String name : taskNames)
        {
            treAssetItem.getTasks().add(name);
        }

        return treAssetItem;
    }

    private WorkItemLightDto createTask(final Long assetItemId, final WorkItemActionDto workItemAction)
    {
        final WorkItemLightDto task = new WorkItemLightDto();
        task.setAssetItem(new LinkResource(assetItemId));
        task.setScheduledService(new LinkResource(SERVICE_99_ID));
        task.setCreditStatus(new LinkResource(CreditStatus.UNCREDITED.getValue()));
        task.setWorkItemAction(workItemAction);

        return task;
    }

    @Test
    public void processTasks() throws RecordNotFoundException
    {
        final ItemMetaDto itemMetaDto = new ItemMetaDto();
        itemMetaDto.setId(1L);
        itemMetaDto.setAsset(new LinkResource(1L));
        when(this.itemDao.itemExists(anyLong())).thenReturn(true);
        when(this.itemDao.getItemMeta(anyLong())).thenReturn(itemMetaDto);
        when(this.jobReferenceDataService.findWorkItemActionFromReferenceCode(TASK_NAME_A)).thenReturn(expectedWorkItemActionA);
        when(this.jobReferenceDataService.findWorkItemActionFromReferenceCode(TASK_NAME_B)).thenReturn(expectedWorkItemActionB);
        when(this.jobReferenceDataService.findWorkItemActionFromReferenceCode(TASK_NAME_C)).thenReturn(expectedWorkItemActionC);
        when(this.serviceDao.getServiceCodeFromScheduledService(ANY_ID)).thenReturn(SERVICE_CODE);
        when(this.versioningHelper.getVersionedItemPK(anyLong(), anyLong())).thenReturn(new VersionedItemPK());
        when(this.itemDao.getItem(any(VersionedItemPK.class))).thenReturn(new LazyItemDto());

        final List<WorkItemLightDto> tasks = this.taskHelper.processTasks(treAssetItems, SERVICE_99_ID);

        assertEquals(expectedTasks, tasks);
    }

    @Test
    @Ignore
    public void testGenerateSimpleTaskNumber()
    {
        final LazyItemDto itemDto = new LazyItemDto();
        final String expectedTaskNumber = "000001-GE-000001";
        itemDto.setItemType(new LinkResource());
        itemDto.getItemType().setId(WORK_ITEM_TYPE_1_ID);
        when(itemDao.getItem(any(VersionedItemPK.class))).thenReturn(itemDto);

        final String result = this.taskHelper.generateTaskNumber(ASSET_ITEM_1_ID, WORK_ITEM_ACTION_1_ID);

        assertEquals("Generated task number not as expected. Expected: " + expectedTaskNumber
                        + "Actual: " + result,
                expectedTaskNumber, result);
    }

    @Test
    @Ignore
    public void testGenerateLongTaskNumber()
    {
        final LazyItemDto itemDto = new LazyItemDto();
        final String expectedTaskNumber = "000634-GEZDGN-000999";
        itemDto.setItemType(new LinkResource());
        itemDto.getItemType().setId(WORK_ITEM_TYPE_634_ID);
        //        when(itemDao.getItem(ASSET_ITEM_1234_ID)).thenReturn(itemDto);

        final String result = this.taskHelper.generateTaskNumber(ASSET_ITEM_1234_ID, WORK_ITEM_ACTION_999_ID);

        assertEquals("Generated task number not as expected. Expected: " + expectedTaskNumber
                        + "Actual: " + result,
                expectedTaskNumber, result);
    }
}
