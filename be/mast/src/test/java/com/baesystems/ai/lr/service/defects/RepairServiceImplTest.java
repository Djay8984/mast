package com.baesystems.ai.lr.service.defects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.defects.RepairDao;
import com.baesystems.ai.lr.dao.defects.WIPRepairDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.defects.RepairPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.references.RepairReferenceDataService;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

public class RepairServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long VALID_DEFECT_REPAIR_ID = 1L;
    private static final Long VALID_WIP_COC_ID = 1L;
    private static final Long INVALID_DEFECT_REPAIR_ID = 999L;
    private static final Long VALID_DEFECT_ID = 1L;
    private static final Long VALID_WIP_DEFECT_ID = 1L;
    private static final Long REPAIR_ACTION_PERMANENT = 1L;
    private static final Long REPAIR_ACTION_TEMPORARY = 2L;

    @Mock
    private RepairDao repairDao;

    @Mock
    private WIPRepairDao wipRepairDao;

    @Mock
    private RepairReferenceDataService repairReferenceDataService;

    @InjectMocks
    private final RepairService repairService = new RepairServiceImpl();

    @Spy
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    private final RepairDto expectedRepairDto = new RepairDto();
    private Pageable pageSpecification;

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedRepairDto.setId(VALID_DEFECT_REPAIR_ID);
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
    }

    @Test
    public void testGetValidDefectRepair() throws BadPageRequestException, BadRequestException
    {
        final PageResource<RepairDto> expectedPagedResource = getPageResource(expectedRepairDto);
        when(this.repairDao.getRepairsForCoC(VALID_DEFECT_REPAIR_ID, pageSpecification)).thenReturn(expectedPagedResource);

        final PageResource<RepairDto> dtoPageResource = this.repairService.getDefectRepairsForCoC(VALID_DEFECT_REPAIR_ID, null, null, null, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetInvalidDefectRepair() throws BadPageRequestException, BadRequestException
    {
        when(this.repairDao.getRepairsForCoC(INVALID_DEFECT_REPAIR_ID, pageSpecification)).thenReturn(null);
        assertNull(this.repairService.getDefectRepairsForCoC(INVALID_DEFECT_REPAIR_ID, null, null, null, null));
    }

    @Test
    public void testGetValidWIPDefectRepair() throws BadPageRequestException, BadRequestException
    {
        final PageResource<RepairDto> expectedPagedResource = getPageResource(expectedRepairDto);
        when(this.wipRepairDao.getRepairsForCoC(VALID_DEFECT_REPAIR_ID, pageSpecification)).thenReturn(expectedPagedResource);

        final PageResource<RepairDto> dtoPageResource = this.repairService.getDefectRepairsForWIPCoC(VALID_DEFECT_REPAIR_ID, null, null, null, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetInvalidWIPDefectRepair() throws BadPageRequestException, BadRequestException
    {
        when(this.wipRepairDao.getRepairsForCoC(INVALID_DEFECT_REPAIR_ID, pageSpecification)).thenReturn(null);
        assertNull(this.repairService.getDefectRepairsForWIPCoC(INVALID_DEFECT_REPAIR_ID, null, null, null, null));
    }

    @Test
    public void testCreateWIPRepair() throws BadRequestException
    {
        when(this.wipRepairDao.saveRepair(expectedRepairDto)).thenReturn(expectedRepairDto);

        final RepairDto returnedRepairDto = repairService.createWIPRepair(1L, expectedRepairDto);
        assertEquals(expectedRepairDto, returnedRepairDto);
    }

    @Test
    public void testUpdateDefectWIPRepair() throws BadRequestException, StaleObjectException
    {
        final RepairDto updateThisRepairDto = new RepairDto();
        updateThisRepairDto.setDefect(new LinkResource(1L));
        updateThisRepairDto.setId(1L);

        final RepairDto expectedResultDto = new RepairDto();
        expectedResultDto.setDefect(new LinkResource(1L));
        expectedResultDto.setId(1L);

        when(this.wipRepairDao.saveRepair(updateThisRepairDto)).thenReturn(expectedResultDto);

        final RepairDto result = this.repairService.updateWIPRepair(updateThisRepairDto);
        assertEquals(expectedResultDto, result);
    }

    private PageResource<RepairDto> getPageResource(final RepairDto... defectRepairs)
    {
        final ArrayList<RepairDto> tempList = new ArrayList<>();
        for (final RepairDto repair : defectRepairs)
        {
            tempList.add(repair);
        }
        final Page<RepairDto> repairDtoPage = new PageImpl<>(tempList);
        return pageFactory.createPageResource(repairDtoPage, RepairPageResourceDto.class);
    }

    @Test
    public void testGetAreAllItemsPermanentlyRepairedTrue()
    {
        when(repairDao.getAreAllItemsPermanentlyRepaired(VALID_DEFECT_ID)).thenReturn(true);
        assertTrue(repairService.getAreAllItemsPermanentlyRepaired(VALID_DEFECT_ID));
    }

    @Test
    public void testGetAreAllItemsPermanentlyRepairedFalse()
    {
        when(repairDao.getAreAllItemsPermanentlyRepaired(VALID_DEFECT_ID)).thenReturn(false);
        assertFalse(repairService.getAreAllItemsPermanentlyRepaired(VALID_DEFECT_ID));
    }

    @Test
    public void testGetAreAllItemsPermanentlyWIPRepairedTrue()
    {
        when(wipRepairDao.getAreAllItemsPermanentlyRepaired(VALID_WIP_DEFECT_ID)).thenReturn(true);
        assertTrue(repairService.getAreAllItemsPermanentlyWIPRepaired(VALID_WIP_DEFECT_ID));
    }

    @Test
    public void testGetAreAllItemsPermanentlyWIPRepairedFalse()
    {
        when(wipRepairDao.getAreAllItemsPermanentlyRepaired(VALID_WIP_DEFECT_ID)).thenReturn(false);
        assertFalse(repairService.getAreAllItemsPermanentlyWIPRepaired(VALID_WIP_DEFECT_ID));

    }

    @Test
    public void testGetDefectHasPermanentRepairTrue()
    {
        when(repairDao.getDefectHasPermanentRepair(VALID_DEFECT_ID)).thenReturn(true);
        assertTrue(repairService.getDefectHasPermanentRepair(VALID_DEFECT_ID));
    }

    @Test
    public void testGetDefectHasPermanentRepairFalse()
    {
        when(repairDao.getDefectHasPermanentRepair(VALID_DEFECT_ID)).thenReturn(false);
        assertFalse(repairService.getDefectHasPermanentRepair(VALID_DEFECT_ID));
    }

    @Test
    public void testGetWIPDefectHasPermanentRepairTrue()
    {
        when(wipRepairDao.getDefectHasPermanentRepair(VALID_WIP_DEFECT_ID)).thenReturn(true);
        assertTrue(repairService.getWIPDefectHasPermanentRepair(VALID_WIP_DEFECT_ID));
    }

    @Test
    public void testGetWIPDefectHasPermanentRepairFalse()
    {
        when(wipRepairDao.getDefectHasPermanentRepair(VALID_WIP_DEFECT_ID)).thenReturn(false);
        assertFalse(repairService.getWIPDefectHasPermanentRepair(VALID_WIP_DEFECT_ID));
    }

    @Test
    public void testRepairIsPermanentAndConfirmed() throws RecordNotFoundException
    {
        mockRepairDtoWithActionTypeAndConfirmation(VALID_DEFECT_REPAIR_ID, Boolean.TRUE, RepairActionType.PERMANENT, REPAIR_ACTION_PERMANENT);
        assertTrue(repairService.getRepairIsPermanentAndConfirmed(VALID_DEFECT_REPAIR_ID));
    }

    @Test
    public void testRepairIsPermanentNotConfirmed() throws RecordNotFoundException
    {
        mockRepairDtoWithActionTypeAndConfirmation(VALID_DEFECT_REPAIR_ID, Boolean.FALSE, RepairActionType.PERMANENT, REPAIR_ACTION_PERMANENT);
        assertFalse(repairService.getRepairIsPermanentAndConfirmed(VALID_DEFECT_REPAIR_ID));
    }

    @Test
    public void testRepairIsTemporaryAndConfirmed() throws RecordNotFoundException
    {
        mockRepairDtoWithActionTypeAndConfirmation(VALID_DEFECT_REPAIR_ID, Boolean.TRUE, RepairActionType.TEMPORARY, REPAIR_ACTION_TEMPORARY);
        assertFalse(repairService.getRepairIsPermanentAndConfirmed(VALID_DEFECT_REPAIR_ID));
    }

    @Test
    public void testRepairIsTemporaryNotConfirmed() throws RecordNotFoundException
    {
        mockRepairDtoWithActionTypeAndConfirmation(VALID_DEFECT_REPAIR_ID, Boolean.FALSE, RepairActionType.TEMPORARY, REPAIR_ACTION_TEMPORARY);
        assertFalse(repairService.getRepairIsPermanentAndConfirmed(VALID_DEFECT_REPAIR_ID));
    }

    private void mockRepairDtoWithActionTypeAndConfirmation(Long repairId, Boolean confirmed, RepairActionType repairActionType, Long repairActionId)
            throws RecordNotFoundException
    {
        final RepairDto repairDto = new RepairDto();
        repairDto.setId(repairId);
        repairDto.setConfirmed(confirmed);
        repairDto.setRepairAction(new LinkResource(repairActionId));
        final ReferenceDataDto refDataDto = new ReferenceDataDto();
        refDataDto.setId(repairActionType.value());
        when(this.wipRepairDao.getRepair(repairId)).thenReturn(repairDto);
        when(this.repairReferenceDataService.getRepairAction(repairDto.getRepairAction().getId())).thenReturn(refDataDto);
    }
}
