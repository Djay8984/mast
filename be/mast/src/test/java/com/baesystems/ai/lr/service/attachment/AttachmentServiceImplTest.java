package com.baesystems.ai.lr.service.attachment;

import com.baesystems.ai.lr.dao.attachments.AttachmentDao;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationPageResourceDto;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.attachments.AttachmentService;
import com.baesystems.ai.lr.service.attachments.AttachmentServiceImpl;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AttachmentServiceImplTest
{
    private static final Long ID = 1L;

    @Mock
    private AttachmentDao attachmentDao;

    @Spy
    private ServiceHelperImpl serviceHelper;

    @InjectMocks
    private AttachmentService attachmentService = new AttachmentServiceImpl();

    private SupplementaryInformationPageResourceDto pageResourceDto = new SupplementaryInformationPageResourceDto();
    private SupplementaryInformationDto singleDto = new SupplementaryInformationDto();
    private List<SupplementaryInformationDto> listDtos = new ArrayList<>();

    @Test
    public void getAttachments() throws BadPageRequestException, MastBusinessException
    {
        final AttachmentTargetType type = AttachmentTargetType.ASSET;
        when(this.attachmentDao.findAttachments(any(Pageable.class), eq(type), eq(ID))).thenReturn(pageResourceDto);
        Assert.assertEquals(pageResourceDto, this.attachmentService.getAttachments(null, null, null, null, type.name(), ID));
    }

    @Test
    public void getAttachment() throws RecordNotFoundException
    {
        when(this.attachmentDao.getAttachment(eq(ID))).thenReturn(singleDto);
        Assert.assertEquals(singleDto, this.attachmentService.getAttachment(ID));
    }

    @Test
    public void deleteAttachment()
    {
        doNothing().when(this.attachmentDao).deleteAttachment(eq(ID));
        when(this.attachmentDao.exists(eq(ID))).thenReturn(false);
        Assert.assertTrue(this.attachmentService.deleteAttachment(ID));
    }

    @Test
    public void saveAttachment() throws MastBusinessException, RecordNotFoundException
    {
        when(this.attachmentDao.saveAttachment(AttachmentTargetType.ASSET, ID, singleDto)).thenReturn(singleDto);
        Assert.assertEquals(singleDto, this.attachmentService.saveAttachment(ID, "ASSET", singleDto));
    }

    @Test
    public void updateAttachment() throws RecordNotFoundException
    {
        when(this.attachmentDao.updateAttachment(singleDto)).thenReturn(singleDto);
        Assert.assertEquals(singleDto, this.attachmentService.updateAttachment(singleDto));
    }

    @Test
    public void getAttachmentsByQuery()
    {
        when(this.attachmentDao.findAttachmentsByQuery(any(AttachmentLinkQueryDto.class))).thenReturn(listDtos);
        Assert.assertEquals(listDtos, this.attachmentService.getAttachmentsByQuery(new AttachmentLinkQueryDto()));

    }
}
