package com.baesystems.ai.lr.service.tasks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.WorkItemQueryDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightListDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemPageResourceDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.jobs.JobService;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class WorkItemServiceImplTest
{
    @Mock
    private WorkItemDao workItemDao;

    @Mock
    private WIPWorkItemDao wipWorkItemDao;

    @Mock
    private ServiceHelper serviceHelper;

    @Mock
    private JobService jobService;

    @Mock
    private ItemDao itemDao;

    @Mock
    private SurveyService surveyService;

    @InjectMocks
    private final WorkItemService workItemService = new WorkItemServiceImpl();

    private static final Long SURVEY_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L; // child with task

    private WorkItemQueryDto workItemQueryDto;
    private WorkItemPageResourceDto workItemPageResourceDto;

    private List<WorkItemDto> expectedTaskList;
    private WorkItemDto expectedTask;
    private SurveyDto expectedSurvey;

    private WorkItemLightListDto expectedLightTaskListDto;
    private WorkItemLightDto expectedLightTask;

    @Before
    public void setUp() throws BadPageRequestException
    {
        this.workItemQueryDto = new WorkItemQueryDto();
        this.workItemPageResourceDto = new WorkItemPageResourceDto();

        this.expectedSurvey = new SurveyDto();
        this.expectedSurvey.setId(SURVEY_1_ID);

        this.expectedTask = new WorkItemDto();
        this.expectedTask.setSurvey(this.expectedSurvey);
        this.expectedTask.setAssetItem(new ItemLightDto());
        this.expectedTask.getAssetItem().setId(ITEM_2_ID);

        this.expectedTaskList = new ArrayList<>();
        this.expectedTaskList.add(this.expectedTask);

        this.expectedLightTask = new WorkItemLightDto();
        this.expectedLightTask.setSurvey(new LinkResource(SURVEY_1_ID));
        this.expectedLightTask.setAssetItem(new LinkResource(ITEM_2_ID));

        this.expectedLightTaskListDto = new WorkItemLightListDto();
        this.expectedLightTaskListDto.setTasks(new ArrayList<WorkItemLightDto>());
        this.expectedLightTaskListDto.getTasks().add(this.expectedLightTask);

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(this.workItemQueryDto, WorkItemQueryDto.class))
                .willReturn(this.workItemQueryDto);
    }

    @Test
    public void testGetTasks() throws BadPageRequestException, RecordNotFoundException
    {
        when(this.workItemDao.getTasks(any(Pageable.class), eq(this.workItemQueryDto))).thenReturn(this.workItemPageResourceDto);
        final PageResource<WorkItemLightDto> result = this.workItemService.getTasks(null, null, null, null, this.workItemQueryDto);
        Assert.assertNotNull(result);
        Assert.assertEquals(this.workItemPageResourceDto, result);
    }

    @Test
    public void testGetWIPTasks() throws BadPageRequestException, RecordNotFoundException
    {
        when(this.wipWorkItemDao.getTasks(any(Pageable.class), eq(this.workItemQueryDto))).thenReturn(this.workItemPageResourceDto);
        final PageResource<WorkItemLightDto> result = this.workItemService.getWIPTasks(null, null, null, null, this.workItemQueryDto);
        Assert.assertNotNull(result);
        Assert.assertEquals(this.workItemPageResourceDto, result);
    }

    @Test
    public void updateWIPTasks() throws RecordNotFoundException, StaleObjectException
    {
        when(this.wipWorkItemDao.updateTask(this.expectedLightTask)).thenReturn(this.expectedLightTask);
        final WorkItemLightListDto listOfUpdatedTasks = this.workItemService.updateWIPTasks(this.expectedLightTaskListDto);
        assertNotNull(listOfUpdatedTasks);
        assertTrue(listOfUpdatedTasks.getTasks().size() == 1);
        assertEquals(this.expectedLightTaskListDto.getTasks().get(0), listOfUpdatedTasks.getTasks().get(0));
    }
}
