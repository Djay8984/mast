package com.baesystems.ai.lr.service.jobs;

import static org.mockito.Mockito.when;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.jobs.FollowUpActionDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.references.jobs.FollowUpActionTemplateDao;
import com.baesystems.ai.lr.dao.reports.ReportDao;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.FollowUpActionDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.FollowUpActionTemplateDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.enums.ReportType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.enums.FollowUpActionTemplates;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.utils.ServiceUtils;

public class FollowUpActionServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long JOB_1_ID = 1L;
    private static final Long ASSET_1 = 1L;
    private static final Long REPORT_1_ID = 1L;
    private static final Long FUA_1_ID = 1L;
    private static final Long REPORT_FSR_TYPE_ID = ReportType.FSR.value();
    private static final Long CASE_1_ID = 1L;
    private static final Long EMPLOYEE_1_ID = 1L;
    private static final Long TEMPLATE_1_ID = 1L;
    private static final String TITLE_1 = "title 1";
    private static final String DESCRIPTION_1 = "desc 1";

    @Mock
    private FollowUpActionDao followUpActionDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private ReportDao reportDao;

    @Mock
    private FollowUpActionTemplateDao followUpActionTemplateDao;

    @Mock
    private LrEmployeeRepository employeeRepository;

    @Mock
    private ServiceUtils serviceUtils;

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @InjectMocks
    private final FollowUpActionService followUpActionService = new FollowUpActionServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final FollowUpActionDto expectedFollowUpActionDto = new FollowUpActionDto();
    private final ReportDto reportDto = new ReportDto();
    private final JobDto jobDto = new JobDto();
    private final LrEmployeeDO employee = new LrEmployeeDO();
    private final FollowUpActionTemplateDto template = new FollowUpActionTemplateDto();
    private PageResource<FollowUpActionDto> expectedPageResource;
    private Pageable pageSpecification;

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedFollowUpActionDto.setId(FUA_1_ID);
        expectedFollowUpActionDto.setReport(new ReportDto());
        expectedFollowUpActionDto.getReport().setId(REPORT_1_ID);
        expectedFollowUpActionDto.getReport().setJob(new LinkResource());
        expectedFollowUpActionDto.getReport().getJob().setId(JOB_1_ID);
        reportDto.setId(REPORT_1_ID);
        reportDto.setReportType(new LinkResource());
        reportDto.getReportType().setId(REPORT_FSR_TYPE_ID);
        reportDto.setJob(new LinkResource());
        reportDto.getJob().setId(JOB_1_ID);
        jobDto.setId(JOB_1_ID);
        jobDto.setaCase(new LinkResource());
        jobDto.getaCase().setId(CASE_1_ID);
        jobDto.setAsset(new LinkVersionedResource(ASSET_1, ASSET_1));
        employee.setId(EMPLOYEE_1_ID);
        template.setId(TEMPLATE_1_ID);
        template.setTitle(TITLE_1);
        template.setDescription(DESCRIPTION_1);

        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
    }

    @Test
    public void getFollowUpActionsForJobSuccess() throws BadPageRequestException
    {
        when(this.followUpActionDao.getFollowUpActionsForJob(pageSpecification, JOB_1_ID))
                .thenReturn(expectedPageResource);
        final PageResource<FollowUpActionDto> result = this.followUpActionService
                .getFollowUpActionsForJob(null, null, null, null, JOB_1_ID);
        Assert.assertEquals(expectedPageResource, result);
    }

    @Test
    public void generateFollowUpActionsSuccess()
    {
        when(this.followUpActionDao.saveFollowUpAction(expectedFollowUpActionDto, reportDto))
                .thenReturn(expectedFollowUpActionDto);
        when(this.jobDao.getJob(reportDto.getJob().getId())).thenReturn(jobDto);
        when(this.followUpActionTemplateDao.getTemplate(FollowUpActionTemplates.EIC.value()))
        .thenReturn(template);
        when(this.reportDao.getReport(reportDto.getId())).thenReturn(reportDto);
        when(this.employeeRepository.findByFullName(serviceUtils.getSecurityContextPrincipal()))
                .thenReturn(employee);
        this.followUpActionService.generateFollowUpActions(reportDto);
    }
}
