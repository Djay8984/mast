package com.baesystems.ai.lr.service.codicils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import com.baesystems.ai.lr.utils.QueryDtoUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class ActionableItemServiceImplTest
{
    @Mock
    private ActionableItemDao actionableItemDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    @Mock
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @InjectMocks
    private final ActionableItemService actionableItemService = new ActionableItemServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static final Long ACTIONABLE_ITEM_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long STATUS_1_ID = 1L;

    private ActionableItemDto actionableItemDto;
    private Pageable pageSpecification;
    private PageResource<ActionableItemDto> emptyPageResource;
    private PageResource<ActionableItemDto> expectedPageResource;
    private CodicilDefectQueryDto codicilDefectQueryDto;

    @Before
    public void setUp() throws Exception
    {
        actionableItemDto = new ActionableItemDto();
        actionableItemDto.setId(ACTIONABLE_ITEM_1_ID);
        pageSpecification = this.serviceHelper.createPageable(null, null, null, null);

        expectedPageResource = new ActionableItemPageResourceDto();
        expectedPageResource.setContent(new ArrayList<ActionableItemDto>());
        expectedPageResource.getContent().add(actionableItemDto);

        emptyPageResource = new ActionableItemPageResourceDto();
        emptyPageResource.setContent(new ArrayList<ActionableItemDto>());

        codicilDefectQueryDto = new CodicilDefectQueryDto();
        codicilDefectQueryDto.setSearchString("Random string");

        //Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(codicilDefectQueryDto, CodicilDefectQueryDto.class))
                .willReturn(codicilDefectQueryDto);
    }

    @Test
    public void testGetActionableItem() throws Exception
    {
        when(this.actionableItemDao.getActionableItem(ACTIONABLE_ITEM_1_ID)).thenReturn(actionableItemDto);
        final ActionableItemDto result = this.actionableItemService.getActionableItem(ACTIONABLE_ITEM_1_ID);
        Assert.assertEquals(actionableItemDto, result);
    }

    @Test
    public void testGetActionableItemNotFound() throws Exception
    {
        when(this.actionableItemDao.getActionableItem(ACTIONABLE_ITEM_1_ID)).thenReturn(null);
        thrown.expect(RecordNotFoundException.class);
        this.actionableItemService.getActionableItem(ACTIONABLE_ITEM_1_ID);
    }

    @Test
    public void testGetActionableItemsForAsset() throws Exception
    {
        innerGetActionableItemsForAsset(null);
    }

    @Test
    public void testGetActionableItemsForAssetWithStatus() throws Exception
    {
        innerGetActionableItemsForAsset(STATUS_1_ID);
    }

    private void innerGetActionableItemsForAsset(final Long statusId) throws Exception
    {
        when(this.actionableItemDao.getActionableItemsForAssets(pageSpecification, ASSET_1_ID, statusId))
                .thenReturn(expectedPageResource);
        final PageResource<ActionableItemDto> result = this.actionableItemService
                .getActionableItemsForAssets(null, null, null, null, ASSET_1_ID, statusId);
        Assert.assertEquals(expectedPageResource, result);
    }

    @Test
    public void testGetActionableItemsForAssetNonFound() throws Exception
    {
        innerGetActionableItemsForAssetNonFound(null);
    }

    @Test
    public void testGetActionableItemsForAssetWithStatusNonFound() throws Exception
    {
        innerGetActionableItemsForAssetNonFound(STATUS_1_ID);
    }

    @Test
    public void testQueryActionableItem() throws BadPageRequestException
    {
        when(actionableItemDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<ActionableItemDto> dtoPageResource = this.actionableItemService
                .getActionableItemsByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryActionableItemNonFound() throws BadPageRequestException
    {
        when(actionableItemDao.findAllForAsset(pageSpecification, ASSET_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<ActionableItemDto> dtoPageResource = this.actionableItemService
                .getActionableItemsByQuery(ASSET_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPActionableItem() throws BadPageRequestException
    {
        when(wipActionableItemDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<ActionableItemDto> dtoPageResource = this.actionableItemService
                .getWIPActionableItemsByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPActionableItemNonFound() throws BadPageRequestException
    {
        when(wipActionableItemDao.findAllForJob(pageSpecification, JOB_1_ID, codicilDefectQueryDto)).thenReturn(emptyPageResource);
        final PageResource<ActionableItemDto> dtoPageResource = this.actionableItemService
                .getWIPActionableItemsByQuery(JOB_1_ID, null, null, null, null, codicilDefectQueryDto);
        assertEquals(emptyPageResource, dtoPageResource);
    }

    private void innerGetActionableItemsForAssetNonFound(final Long statusId) throws Exception
    {
        when(this.actionableItemDao.getActionableItemsForAssets(pageSpecification, ASSET_1_ID, statusId))
                .thenReturn(emptyPageResource);
        final PageResource<ActionableItemDto> result = this.actionableItemService
                .getActionableItemsForAssets(null, null, null, null, ASSET_1_ID, statusId);
        Assert.assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void getWipActionableItems() throws BadPageRequestException
    {
        final Long jobId = 1L;
        final PageResource<ActionableItemDto> resource = new ActionableItemPageResourceDto();
        resource.setContent(new ArrayList<ActionableItemDto>());

        when(this.wipActionableItemDao.getActionableItemsForJob(Mockito.any(Pageable.class), Mockito.eq(jobId), Mockito.any(Long.class))).thenReturn(
                resource);
        final PageResource<ActionableItemDto> results = this.actionableItemService.getWIPActionableItemsForJobId(null, null, null, null, jobId, null);
        assertEquals(resource, results);
    }

    @Test
    public void deleteWipActionableItem()
    {
        // ensure this call passes through to the dao, and nothing more.
        when(this.wipActionableItemDao.actionableItemExists(ACTIONABLE_ITEM_1_ID)).thenReturn(false);
        assertTrue(this.actionableItemService.deleteWIPActionableItem(ACTIONABLE_ITEM_1_ID));
    }
}
