package com.baesystems.ai.lr.utils;

import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(ServletUriComponentsBuilder.class)
public abstract class PageResourceImplBasedTest
{
    private static final int DUMMY_PORT = 9080;

    private ServletUriComponentsBuilder mockUriComponentsBuilderFull;
    private ServletUriComponentsBuilder mockUriComponentsBuilderBase;
    private MockHttpServletRequest requestFull;
    private MockHttpServletRequest requestBase;

    @Before
    public void setUpComponentBuilder()
    {
        this.requestFull = new MockHttpServletRequest();
        this.requestFull.setScheme("http");
        this.requestFull.setServerName("localhost");
        this.requestFull.setServerPort(DUMMY_PORT);
        this.requestFull.setContextPath("/mast/mock");
        mockUriComponentsBuilderFull = ServletUriComponentsBuilder.fromContextPath(requestFull);

        this.requestBase = new MockHttpServletRequest();
        this.requestBase.setScheme("http");
        this.requestBase.setServerName("localhost");
        this.requestBase.setServerPort(DUMMY_PORT);
        this.requestBase.setContextPath("/mast");
        mockUriComponentsBuilderBase = ServletUriComponentsBuilder.fromContextPath(requestBase);

        PowerMockito.mockStatic(ServletUriComponentsBuilder.class);
        when(ServletUriComponentsBuilder.fromCurrentRequestUri()).thenReturn(mockUriComponentsBuilderFull);
        when(ServletUriComponentsBuilder.fromCurrentServletMapping()).thenReturn(mockUriComponentsBuilderBase);
    }

    public ServletUriComponentsBuilder getMockUriComponentsBuilderFull()
    {
        return mockUriComponentsBuilderFull;
    }

    public ServletUriComponentsBuilder getMockUriComponentsBuilderBase()
    {
        return mockUriComponentsBuilderBase;
    }

    public MockHttpServletRequest getRequestFull()
    {
        return requestFull;
    }

    public MockHttpServletRequest getRequestBase()
    {
        return requestBase;
    }
}
