package com.baesystems.ai.lr.service.assets;

import com.baesystems.ai.lr.dao.assets.DraftItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.dao.references.assets.ItemTypeDao;
import com.baesystems.ai.lr.dto.assets.DraftItemDto;
import com.baesystems.ai.lr.dto.assets.DraftItemListDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.exception.BadRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

public class DraftItemServiceImplTest extends PageResourceImplBasedTest
{
    private static final DraftItemListDto INPUT = new DraftItemListDto();
    private static final DraftItemDto ITEM1 = new DraftItemDto();
    private static final DraftItemDto ITEM2 = new DraftItemDto();
    private static final AttributeTypeDto ATTRIBUTE_TYPE1 = new AttributeTypeDto();
    private static final AttributeTypeDto ATTRIBUTE_TYPE2 = new AttributeTypeDto();
    private static final AttributeTypeDto ATTRIBUTE_TYPE3 = new AttributeTypeDto();
    private static final List<AttributeTypeDto> ATTRIBUTE_TYPE_LIST1 = new ArrayList<AttributeTypeDto>();
    private static final List<AttributeTypeDto> ATTRIBUTE_TYPE_LIST2 = new ArrayList<AttributeTypeDto>();
    private static final Long ZERO = 0L;
    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;
    private static final String NAME1 = "One";
    private static final String NAME2 = "Two";
    private static final Integer NEXT_DISPLAY_ORDER = 2;

    private List<Long> expected;
    private LazyItemDto lazyItemDto1;

    /*@Mock
    private DraftItemDao draftItemDao;*/

    @Mock
    private DraftItemPersistDao draftItemPublishDao;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @Mock
    private ItemTypeDao itemTypeDao;

    @InjectMocks
    private final DraftItemService draftItemService = new DraftItemServiceImpl();

    @Before
    public void setUp()
    {
        ITEM1.setId(ONE);
        ITEM1.setItemTypeId(ONE);
        ITEM1.setToParentId(ONE);
        ITEM2.setId(TWO);
        ITEM2.setItemTypeId(TWO);
        ITEM2.setToParentId(ONE);

        ATTRIBUTE_TYPE1.setId(ONE);
        ATTRIBUTE_TYPE1.setMinOccurs(ZERO.intValue());
        ATTRIBUTE_TYPE2.setId(TWO);
        ATTRIBUTE_TYPE2.setMinOccurs(TWO.intValue());
        ATTRIBUTE_TYPE3.setId(THREE);
        ATTRIBUTE_TYPE3.setMinOccurs(ONE.intValue());

        ATTRIBUTE_TYPE_LIST1.add(ATTRIBUTE_TYPE1);
        ATTRIBUTE_TYPE_LIST1.add(ATTRIBUTE_TYPE2);
        ATTRIBUTE_TYPE_LIST2.add(ATTRIBUTE_TYPE3);

        INPUT.setDraftItemDtoList(new ArrayList<DraftItemDto>());
        INPUT.getDraftItemDtoList().add(ITEM1);
        INPUT.getDraftItemDtoList().add(ITEM2);

        expected = new ArrayList<Long>();
        lazyItemDto1 = new LazyItemDto();
        lazyItemDto1.setId(ONE);
    }

    @Test
    public void testSaveDraftItemFromReferenceData() throws MastBusinessException, BadRequestException
    {
        /*expected.add(ONE);
        expected.add(TWO);

        when(this.itemTypeDao.getItemTypeName(ONE)).thenReturn(NAME1);
        when(this.itemTypeDao.getItemTypeName(TWO)).thenReturn(NAME2);
        when(this.draftItemDao.getNextChildDisplayOrder(ONE)).thenReturn(NEXT_DISPLAY_ORDER);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM1, NAME1, NEXT_DISPLAY_ORDER)).thenReturn(ONE);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM2, NAME2, NEXT_DISPLAY_ORDER)).thenReturn(TWO);
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(ONE)).thenReturn(ATTRIBUTE_TYPE_LIST1);
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(TWO)).thenReturn(ATTRIBUTE_TYPE_LIST2);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(lazyItemDto1);
        doNothing().when(this.draftItemPublishDao).createDraftAttribute(ONE, ONE);
        doNothing().when(this.draftItemPublishDao).createDraftAttribute(TWO, ONE);
        doNothing().when(this.draftItemPublishDao).createDraftAttribute(THREE, ONE);

        final Long result = this.draftItemService.appendDraftItems(ONE, INPUT);

        assertTrue(result != null && result.equals(ONE));*/
    }

    @Test
    public void testSaveDraftItemFromReferenceDataNoAttributesToAdd() throws MastBusinessException, BadRequestException
    {
        /*expected.add(ONE);
        expected.add(TWO);

        when(this.itemTypeDao.getItemTypeName(ONE)).thenReturn(NAME1);
        when(this.itemTypeDao.getItemTypeName(TWO)).thenReturn(NAME2);
        when(this.draftItemDao.getNextChildDisplayOrder(ONE)).thenReturn(NEXT_DISPLAY_ORDER);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM1, NAME1, NEXT_DISPLAY_ORDER)).thenReturn(ONE);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM2, NAME2, NEXT_DISPLAY_ORDER)).thenReturn(TWO);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(lazyItemDto1);
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(ONE)).thenReturn(new ArrayList<AttributeTypeDto>());
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(TWO)).thenReturn(new ArrayList<AttributeTypeDto>());

        final Long result = this.draftItemService.appendDraftItems(ONE, INPUT);

        assertTrue(result != null && result.equals(ONE));*/
    }

    @Test
    public void testSaveDraftItemFromReferenceDataOneItem() throws MastBusinessException, BadRequestException
    {
        /*INPUT.getDraftItemDtoList().remove(ITEM2);
        expected.add(ONE);

        when(this.itemTypeDao.getItemTypeName(ONE)).thenReturn(NAME1);
        when(this.draftItemDao.getNextChildDisplayOrder(ONE)).thenReturn(NEXT_DISPLAY_ORDER);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM1, NAME1, NEXT_DISPLAY_ORDER)).thenReturn(ONE);
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(ONE)).thenReturn(new ArrayList<AttributeTypeDto>());
//        when(this.draftItemDao.getItem(ONE)).thenReturn(lazyItemDto1);

        final Long result = this.draftItemService.appendDraftItems(ONE, INPUT);

        assertTrue(result != null && result.equals(ITEM1.getToParentId()));*/
    }

    @Test
    public void testSaveDraftItemFromReferenceDataItemMissingType() throws MastBusinessException, BadRequestException
    {
        /*INPUT.getDraftItemDtoList().get(0).setItemTypeId(null);
        expected.add(TWO);

        when(this.itemTypeDao.getItemTypeName(TWO)).thenReturn(NAME2);
        when(this.draftItemDao.getNextChildDisplayOrder(ONE)).thenReturn(NEXT_DISPLAY_ORDER);
        when(this.draftItemPublishDao.appendDraftItemFromReferenceData(ONE, ITEM2, NAME2, NEXT_DISPLAY_ORDER)).thenReturn(TWO);
        when(this.assetReferenceDataDao.getDefaultAndMandatoryAttributeTypesForItemType(TWO)).thenReturn(ATTRIBUTE_TYPE_LIST2);
//        when(this.draftItemDao.getItem(ONE)).thenReturn(lazyItemDto1);

        doNothing().when(this.draftItemPublishDao).createDraftAttribute(THREE, ONE);

        final Long result = this.draftItemService.appendDraftItems(ONE, INPUT);

        assertTrue(result != null && result.equals(ONE));*/
    }
}
