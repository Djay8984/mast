package com.baesystems.ai.lr.service.employees;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.employees.LrEmployeeDao;
import com.baesystems.ai.lr.dto.employees.EmployeePageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class LrEmployeeServiceImplTest
{
    private static final String SORT = "id";
    private static final String ORDER = "ASC";
    private static final Integer PAGE = 1;
    private static final Integer SIZE = 1;
    private static final Long EMPLOYEE_ID = 1L;
    private static final String FIRST_NAME_A = "FIRST NAME A";
    private static final String LAST_NAME_A = "LAST NAME A";
    private static final String FIRST_NAME_B = "FIRST NAME B";

    private static final EmployeePageResourceDto EMPLOYEE_PAGE_RESOURCE_DTO = new EmployeePageResourceDto();

    private LrEmployeeDto expectedEmployeeDto;
    private Pageable pageSpecification;

    @Mock
    private LrEmployeeDao lrEmployeeDao;

    @InjectMocks
    private LrEmployeeServiceImpl lrEmployeeService;

    @Spy
    private ServiceHelper serviceHelper = new ServiceHelperImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws BadPageRequestException
    {
        expectedEmployeeDto = new LrEmployeeDto();
        expectedEmployeeDto.setId(EMPLOYEE_ID);
        final ArrayList<LrEmployeeDto> content = new ArrayList<>();
        content.add(expectedEmployeeDto);
        EMPLOYEE_PAGE_RESOURCE_DTO.setContent(content);
        pageSpecification = serviceHelper.createPageable(PAGE, SIZE, SORT, ORDER);
    }

    @Test
    public void testSearchEmployeesEmptySearchReturnsAll() throws BadPageRequestException
    {
        when(lrEmployeeDao.searchEmployees(pageSpecification, null, null)).thenReturn(EMPLOYEE_PAGE_RESOURCE_DTO);
        final PageResource<LrEmployeeDto> employeePageResourceDto = lrEmployeeService
                .searchEmployees(PAGE, SIZE, SORT, ORDER, null, null);
        assertTrue(employeePageResourceDto.getContent().contains(expectedEmployeeDto));
    }

    @Test
    public void testSearchEmployeesByName() throws BadPageRequestException
    {
        when(lrEmployeeDao.searchEmployees(pageSpecification, FIRST_NAME_A.concat(LAST_NAME_A), null)).thenReturn(EMPLOYEE_PAGE_RESOURCE_DTO);
        final PageResource<LrEmployeeDto> employeePageResourceDto = lrEmployeeService
                .searchEmployees(PAGE, SIZE, SORT, ORDER, FIRST_NAME_A.concat(LAST_NAME_A), null);
        assertTrue(employeePageResourceDto.getContent().contains(expectedEmployeeDto));
    }

    @Test
    public void testSearchEmployeesByNameNotFound() throws BadPageRequestException
    {
        EMPLOYEE_PAGE_RESOURCE_DTO.setContent(new ArrayList<LrEmployeeDto>(0));
        when(lrEmployeeDao.searchEmployees(pageSpecification, FIRST_NAME_B.concat(LAST_NAME_A), null)).thenReturn(EMPLOYEE_PAGE_RESOURCE_DTO);
        final PageResource<LrEmployeeDto> employeePageResourceDto = lrEmployeeService
                .searchEmployees(PAGE, SIZE, SORT, ORDER, FIRST_NAME_B.concat(LAST_NAME_A), null);
        assertTrue(employeePageResourceDto.getContent().isEmpty());
    }

    @Test
    public void testGetLrEmployeeByFullName() throws RecordNotFoundException
    {
        final String fullName = FIRST_NAME_A.concat(LAST_NAME_A);
        when(lrEmployeeDao.getLrEmployee(fullName)).thenReturn(expectedEmployeeDto);
        assertEquals(expectedEmployeeDto, lrEmployeeService.getLrEmployeeByFullName(fullName));
    }

    @Test
    public void testGetLrEmployeeByFullNameNotFound()
    {
        final String fullName = FIRST_NAME_B.concat(LAST_NAME_A);
        when(lrEmployeeDao.getLrEmployee(fullName)).thenReturn(null);
        try
        {
            lrEmployeeService.getLrEmployeeByFullName(fullName);
            Assert.fail("Expected a record not found exception to be thrown.");
        }
        catch (RecordNotFoundException rnfe)
        {
            assertEquals("Employee with name [" + fullName + "] not found.", rnfe.getMessage());
        }
    }

    @Test
    public void testGetLrEmployee() throws RecordNotFoundException
    {
        when(lrEmployeeDao.getLrEmployee(EMPLOYEE_ID)).thenReturn(expectedEmployeeDto);
        assertEquals(expectedEmployeeDto, lrEmployeeService.getLrEmployee(EMPLOYEE_ID));
    }

    @Test
    public void testGetLrEmployeeNotFound() throws RecordNotFoundException
    {
        final long invalidId = 999L;
        when(lrEmployeeDao.getLrEmployee(invalidId)).thenReturn(null);
        thrown.expect(RecordNotFoundException.class);
        lrEmployeeService.getLrEmployee(invalidId);
    }

}
