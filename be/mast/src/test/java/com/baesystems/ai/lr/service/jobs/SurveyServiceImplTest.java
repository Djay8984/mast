package com.baesystems.ai.lr.service.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.references.codicils.CodicilReferenceDataDao;
import com.baesystems.ai.lr.dao.references.services.ServiceReferenceDataDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueLightDto;
import com.baesystems.ai.lr.dto.references.ServiceCreditStatusDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyListDto;
import com.baesystems.ai.lr.enums.RepairActionType;
import com.baesystems.ai.lr.enums.ServiceCreditStatusType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.defects.RepairService;
import com.baesystems.ai.lr.service.employees.LrEmployeeService;
import com.baesystems.ai.lr.service.references.DefectReferenceDataService;
import com.baesystems.ai.lr.service.references.RepairReferenceDataService;
import com.baesystems.ai.lr.service.tasks.WorkItemService;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;
import com.baesystems.ai.lr.utils.ServiceUtils;

@RunWith(MockitoJUnitRunner.class)
public class SurveyServiceImplTest
{
    private static final Long SURVEY_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long WIP_DEFECT_1_ID = 1L;
    private static final Long WIP_REPAIR_1_ID = 1L;
    private static final Long DEFECT_CATEGORY_1_ID = 1L;
    private static final Long SERVICE_CATALOGUE_1_ID = 1L;
    private static final Long SERVICE_CREDIT_STATUS_1_ID = 1L;
    private static final Long PERMANENT_REPAIR_ACTION_ID = 1L;
    private static final Long EMPLOYEE_1_ID = 1L;
    private static final String CREDITED_SURVEY_STATUS = "Credited";
    private static final String MATCHING_DEFECT_CATEGORY_LETTER = "H";
    private static final String DEFECT_TITLE = "This should end up as the Survey name for damage surveys.";
    private static final String SURVEY_NAME = "This should end up as Survey name for non-damage surveys.";
    private static final String USER_FULL_NAME = "Jing Yu";
    private static final String AMSC_CODE = "AMSC";

    private final List<SurveyDto> expectedSurveyList = new ArrayList<>();
    private final SurveyDto expectedSurveyDto = new SurveyDto();
    private final DefectDto wipDefectDto = new DefectDto();
    private final RepairDto repairDto = new RepairDto();
    private final DefectCategoryDto defectCategoryDto = new DefectCategoryDto();
    private final LinkResource job = new LinkResource(JOB_1_ID);
    private final ServiceCatalogueDto serviceCatalogueDto = new ServiceCatalogueDto();
    private final ServiceCatalogueLightDto serviceCatalogueLightDto = new ServiceCatalogueLightDto();
    private final ServiceCreditStatusDto serviceCreditStatusDto = new ServiceCreditStatusDto();
    private final ReferenceDataDto repairActionDto = new ReferenceDataDto();
    private final LrEmployeeDto lrEmployeeDto = new LrEmployeeDto();
    private final SurveyListDto expectedSurveyListDto = new SurveyListDto();

    @Mock
    private SurveyDao surveyDao;

    @Mock
    private WIPDefectDao wipDefectDao;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private WIPCoCDao wipCocDao;

    @Mock
    private CodicilReferenceDataDao codicilReferenceDataDao;

    @Mock
    private LrEmployeeService lrEmployeeService;

    @Mock
    private ServiceReferenceDataDao serviceReferenceDataDao;

    @Mock
    private DefectReferenceDataService defectDataReferenceService;

    @Mock
    private RepairReferenceDataService repairReferenceDataService;

    @Mock
    private WorkItemService workItemService;

    @Mock
    private RepairService repairService;

    @Mock
    private ServiceUtils serviceUtils;

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    @Captor
    private ArgumentCaptor<SurveyDto> surveyCaptor;

    @InjectMocks
    private final SurveyService surveyService = new SurveyServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws BadPageRequestException
    {
        serviceCatalogueDto.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogueLightDto.setId(SERVICE_CATALOGUE_1_ID);
        serviceCatalogueLightDto.setName(SURVEY_NAME);
        serviceCreditStatusDto.setId(SERVICE_CREDIT_STATUS_1_ID);
        serviceCreditStatusDto.setName(CREDITED_SURVEY_STATUS);

        defectCategoryDto.setId(DEFECT_CATEGORY_1_ID);
        defectCategoryDto.setCategoryLetter(MATCHING_DEFECT_CATEGORY_LETTER);

        wipDefectDto.setId(WIP_DEFECT_1_ID);
        wipDefectDto.setDefectCategory(defectCategoryDto);
        wipDefectDto.setJob(job);
        wipDefectDto.setTitle(DEFECT_TITLE);

        repairDto.setId(WIP_REPAIR_1_ID);
        repairDto.setConfirmed(Boolean.TRUE);
        repairDto.setRepairAction(new LinkResource(PERMANENT_REPAIR_ACTION_ID));
        repairActionDto.setId(PERMANENT_REPAIR_ACTION_ID);
        repairActionDto.setId(RepairActionType.PERMANENT.value());

        lrEmployeeDto.setId(EMPLOYEE_1_ID);
        lrEmployeeDto.setFirstName("Jing");
        lrEmployeeDto.setFirstName("Yu");

        expectedSurveyDto.setId(SURVEY_1_ID);
        expectedSurveyDto.setName(DEFECT_TITLE);
        expectedSurveyDto.setJob(new LinkResource(job.getId()));
        expectedSurveyDto.setServiceCatalogue(new LinkResource(serviceCatalogueDto.getId()));
        expectedSurveyDto.setSurveyStatus(new LinkResource(serviceCreditStatusDto.getId()));
        expectedSurveyDto.setCreditedBy(new LinkResource(lrEmployeeDto.getId()));
        expectedSurveyList.add(expectedSurveyDto);
        expectedSurveyListDto.setSurveys(Collections.singletonList(expectedSurveyDto));
    }

    @Test
    public void updateSurveySuccessful() throws RecordNotFoundException, StaleObjectException
    {
        when(this.surveyDao.updateSurvey(expectedSurveyDto)).thenReturn(expectedSurveyDto);
        final SurveyDto survey = this.surveyService.updateSurvey(SURVEY_1_ID, expectedSurveyDto);
        assertEquals(expectedSurveyDto, survey);
    }

    @Test
    public void updateSurveyInvalidModel() throws RecordNotFoundException, StaleObjectException
    {
        thrown.expect(RecordNotFoundException.class);
        this.surveyService.updateSurvey(SURVEY_1_ID, null);
    }

    @Test
    public void updateSurveys() throws RecordNotFoundException, BadPageRequestException, StaleObjectException
    {
        when(this.surveyDao.updateSurvey(expectedSurveyDto)).thenReturn(expectedSurveyDto);
        final SurveyListDto listOfUpdatedSurveys = this.surveyService.updateSurveys(expectedSurveyListDto);
        assertNotNull(listOfUpdatedSurveys);
        assertTrue(listOfUpdatedSurveys.getSurveys().size() == 1);
        assertEquals(expectedSurveyListDto.getSurveys().get(0), listOfUpdatedSurveys.getSurveys().get(0));
    }

    @Test
    public void updateSurveysIncludingANewOne() throws RecordNotFoundException, BadPageRequestException, StaleObjectException
    {
        final SurveyDto newSurveyDto = new SurveyDto();
        newSurveyDto.setName(DEFECT_TITLE);
        newSurveyDto.setJob(new LinkResource(job.getId()));
        newSurveyDto.setServiceCatalogue(new LinkResource(serviceCatalogueDto.getId()));
        newSurveyDto.setSurveyStatus(new LinkResource(serviceCreditStatusDto.getId()));
        newSurveyDto.setCreditedBy(new LinkResource(lrEmployeeDto.getId()));

        final SurveyListDto expectedSurveyListDtoContainingANewItem = new SurveyListDto();
        expectedSurveyListDtoContainingANewItem.setSurveys(Arrays.asList(expectedSurveyDto, newSurveyDto));

        when(this.surveyDao.updateSurvey(expectedSurveyDto)).thenReturn(expectedSurveyDto);
        when(this.surveyDao.createSurvey(newSurveyDto)).thenReturn(newSurveyDto);

        final SurveyListDto listOfUpdatedSurveys = this.surveyService.updateSurveys(expectedSurveyListDtoContainingANewItem);
        assertNotNull(listOfUpdatedSurveys);
        assertEquals(expectedSurveyListDtoContainingANewItem.getSurveys().size(), listOfUpdatedSurveys.getSurveys().size());
        assertEquals(expectedSurveyDto, listOfUpdatedSurveys.getSurveys().get(0));
        assertEquals(newSurveyDto, listOfUpdatedSurveys.getSurveys().get(1));
    }

    @Test
    public void createWipCocSurveyRepairWorks() throws RecordNotFoundException
    {
        final String userFullName = "John Smith";

        final Long cocId = 50L;
        final LinkResource wipCoc = new LinkResource(cocId);
        repairDto.setCodicil(wipCoc);

        final Long wipCocCategoryId = 99L;
        final LinkResource wipCocCategory = new LinkResource(wipCocCategoryId);

        final CoCDto wipCocDto = new CoCDto();
        wipCocDto.setId(cocId);
        wipCocDto.setCategory(wipCocCategory);
        wipCocDto.setJobScopeConfirmed(true);

        final String categoryName = "flux capacitor";
        final ReferenceDataDto wipCocCategoryRefData = new ReferenceDataDto();
        wipCocCategoryRefData.setName(categoryName);

        final String serviceCatalogueName = "Service Cat Name";
        this.serviceCatalogueDto.setName(serviceCatalogueName);

        final String categoryCode = "FRPS";
        final String surveyType = "REPAIR";

        when(this.repairReferenceDataService.getRepairAction(PERMANENT_REPAIR_ACTION_ID)).thenReturn(repairActionDto);
        when(this.lrEmployeeService.getLrEmployeeByFullName(userFullName)).thenReturn(lrEmployeeDto);
        when(this.serviceUtils.getSecurityContextPrincipal()).thenReturn(userFullName);
        when(this.wipCocDao.getCoC(cocId)).thenReturn(wipCocDto);
        when(this.codicilReferenceDataDao.getCodicilCategory(wipCocCategoryId)).thenReturn(wipCocCategoryRefData);
        when(this.serviceDao.getServiceCatalogueByCodeAndSurveyType(categoryCode, surveyType)).thenReturn(this.serviceCatalogueDto);

        when(this.serviceReferenceDataDao.getServiceCreditStatus(ServiceCreditStatusType.COMPLETE.value())).thenReturn(serviceCreditStatusDto);

        this.surveyService.createWIPCoCSurveyForPermanentRepairs(JOB_1_ID, repairDto);

        verify(this.surveyDao).createSurvey(surveyCaptor.capture());

        final SurveyDto createdSurvey = surveyCaptor.getValue();
        assertNull(createdSurvey.getId());
        assertTrue(createdSurvey.getAutoGenerated());
        assertEquals(new Long(JOB_1_ID), createdSurvey.getJob().getId());
        assertEquals(serviceCatalogueName, createdSurvey.getName());
        assertNull(createdSurvey.getScheduledService());
        assertEquals(SERVICE_CATALOGUE_1_ID, createdSurvey.getServiceCatalogue().getId());
        assertEquals(SERVICE_CREDIT_STATUS_1_ID, createdSurvey.getSurveyStatus().getId());
        assertNotNull(createdSurvey.getDateOfCrediting());
        assertEquals(EMPLOYEE_1_ID, createdSurvey.getCreditedBy().getId());
        assertFalse(createdSurvey.getScheduleDatesUpdated());
        assertTrue(createdSurvey.getApproved());
        assertTrue(createdSurvey.getJobScopeConfirmed());
    }

    @Test
    public void createAnAssetManagementSurvey() throws RecordNotFoundException
    {
        final Long wipCocId = 50L;
        final Long wipCocCategoryId = 99L;
        final String wipCocTitle = "WIP COC TITLE";

        final LinkResource wipCocCategory = new LinkResource(wipCocCategoryId);
        final CoCDto wipCocDto = new CoCDto();
        wipCocDto.setId(wipCocId);
        wipCocDto.setCategory(wipCocCategory);
        wipCocDto.setJobScopeConfirmed(true);
        wipCocDto.setTitle(wipCocTitle);

        when(this.serviceDao.getUniqueServiceCatalogueByCode(AMSC_CODE)).thenReturn(this.serviceCatalogueDto);
        when(this.lrEmployeeService.getLrEmployeeByFullName(USER_FULL_NAME)).thenReturn(lrEmployeeDto);
        when(this.wipCocDao.getCoC(wipCocId)).thenReturn(wipCocDto);
        when(this.serviceReferenceDataDao.getServiceCreditStatus(ServiceCreditStatusType.COMPLETE.value())).thenReturn(serviceCreditStatusDto);
        when(this.serviceUtils.getSecurityContextPrincipal()).thenReturn(USER_FULL_NAME);

        this.surveyService.createAssetManagementSurveyForWipCoC(JOB_1_ID, wipCocId);

        verify(this.surveyDao).createSurvey(surveyCaptor.capture());

        final SurveyDto createdSurvey = surveyCaptor.getValue();
        assertNull(createdSurvey.getId());
        assertTrue(createdSurvey.getAutoGenerated());
        assertEquals(new Long(JOB_1_ID), createdSurvey.getJob().getId());
        assertEquals(wipCocTitle, createdSurvey.getName());
        assertNull(createdSurvey.getScheduledService());
        assertEquals(SERVICE_CATALOGUE_1_ID, createdSurvey.getServiceCatalogue().getId());
        assertEquals(SERVICE_CREDIT_STATUS_1_ID, createdSurvey.getSurveyStatus().getId());
        assertNotNull(createdSurvey.getDateOfCrediting());
        assertEquals(EMPLOYEE_1_ID, createdSurvey.getCreditedBy().getId());
        assertFalse(createdSurvey.getScheduleDatesUpdated());
        assertTrue(createdSurvey.getApproved());
        assertTrue(createdSurvey.getJobScopeConfirmed());
    }
}
