package com.baesystems.ai.lr.service.ihs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.ihs.IhsAssetDao;
import com.baesystems.ai.lr.dao.ihs.IhsGroupFleetDao;
import com.baesystems.ai.lr.dao.ihs.IhsHistDao;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDetailsDto;
import com.baesystems.ai.lr.dto.ihs.IhsAssetDto;
import com.baesystems.ai.lr.dto.ihs.IhsGroupFleetDto;
import com.baesystems.ai.lr.dto.ihs.IhsHistDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.utils.ServiceHelper;

@RunWith(MockitoJUnitRunner.class)
public class IhsAssetServiceImplTest
{

    private static final Long LR_1_ID = 1L;

    @InjectMocks
    private final IhsAssetServiceImpl assetService = new IhsAssetServiceImpl();

    @Mock
    private IhsAssetDao ihsAssetDao;

    @Mock
    private IhsGroupFleetDao ihsGroupFleetDao;

    @Mock
    private IhsHistDao ihsHistDao;

    @Mock
    private ServiceHelper serviceHelper;

    private final IhsAssetDto ihsAssetDto = new IhsAssetDto();

    private final IhsGroupFleetDto ihsGroupFleetDto = new IhsGroupFleetDto();

    private final IhsHistDto ihsHistDto = new IhsHistDto();

    @Before
    public void setUp() throws BadPageRequestException
    {
        ihsAssetDto.setId(LR_1_ID);
        ihsAssetDto.setName("Asset Name");

        ihsGroupFleetDto.setId(LR_1_ID);
        ihsGroupFleetDto.setGroupOwner("Grp Owner");

        ihsHistDto.setId(LR_1_ID);
        ihsHistDto.setKeelLayingDate("20160923");
    }

    @Test
    public final void testGetIhsAsset() throws RecordNotFoundException
    {
        Mockito.when(this.ihsAssetDao.getIhsAsset(Mockito.anyLong())).thenReturn(ihsAssetDto);
        Mockito.when(this.ihsGroupFleetDao.getIhsGroupFleet(Mockito.anyLong())).thenReturn(ihsGroupFleetDto);
        Mockito.when(this.ihsHistDao.getIhsHist(Mockito.anyLong())).thenReturn(ihsHistDto);
        Assert.assertNotNull(assetService.getIhsAsset(LR_1_ID));
    }

    @Test
    public final void testNullValuesGetIhsAsset() throws RecordNotFoundException
    {
        Mockito.when(this.ihsAssetDao.getIhsAsset(Mockito.anyLong())).thenReturn(null);
        Mockito.when(this.ihsGroupFleetDao.getIhsGroupFleet(Mockito.anyLong())).thenReturn(null);
        Mockito.when(this.ihsHistDao.getIhsHist(Mockito.anyLong())).thenReturn(null);
        final IhsAssetDetailsDto dto = assetService.getIhsAsset(LR_1_ID);
        Assert.assertNull(dto.getIhsAsset());
    }
}
