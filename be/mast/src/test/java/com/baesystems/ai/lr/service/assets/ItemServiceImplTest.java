package com.baesystems.ai.lr.service.assets;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.assets.AssetDao;
import com.baesystems.ai.lr.dao.assets.AttributeDao;
import com.baesystems.ai.lr.dao.assets.ItemDao;
import com.baesystems.ai.lr.dao.assets.ItemPersistDao;
import com.baesystems.ai.lr.dao.references.assets.AssetReferenceDataDao;
import com.baesystems.ai.lr.domain.mast.entities.assets.ItemRelationshipDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemDO;
import com.baesystems.ai.lr.domain.mast.entities.assets.VersionedItemPK;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.AttributeDto;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.assets.ItemWithAttributesDto;
import com.baesystems.ai.lr.dto.assets.LazyItemDto;
import com.baesystems.ai.lr.dto.assets.LazyItemPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.ItemQueryDto;
import com.baesystems.ai.lr.dto.references.AttributeTypeDto;
import com.baesystems.ai.lr.dto.references.ItemTypeDto;
import com.baesystems.ai.lr.dto.validation.ItemRelationshipDto;
import com.baesystems.ai.lr.enums.RelationshipType;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelper;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class ItemServiceImplTest extends PageResourceImplBasedTest
{
    @Mock
    private ItemDao itemDao;

    @Mock
    private AssetDao assetDao;

    @Mock
    private ItemPersistDao itemPersistDao;

    @Mock
    private AssetReferenceDataDao assetReferenceDataDao;

    @Mock
    private AttributeDao attributeDao;

    @Mock
    private AssetVersionUpdateService assetVersionUpdateService;

    @InjectMocks
    private final ItemService itemService = new ItemServiceImpl();

    @Spy
    private final ServiceHelper serviceHelper = new ServiceHelperImpl();

    private static final Long ASSET_1_ID = 1L;
    private static final Long ITEM_1_ID = 1L;
    private static final Long ITEM_2_ID = 2L;
    private static final String ITEM_NAME = "Item 1";
    private static final Long ITEM_TYPE_1_ID = 1L;
    private static final Long ATTRIBUTE_1_ID = 1L;
    private static final String ATTRIBUTE_1_VALUE = "Value 1";
    private static final String ATTRIBUTE_1_NAME = "ATTRIBUTE_1_NAME";

    private static final String EXPECTED_ITEM_NAME = "FIRST SECOND THIRD I-NAME";
    private static final String FIRST = "FIRST";
    private static final String SECOND = "SECOND";
    private static final String THIRD = "THIRD";
    private static final String ITEM_TPYE_NAME = "I-NAME";

    private LazyItemPageResourceDto lazyItemPageResourceDto;
    private LazyItemPageResourceDto emptyLazyItemPageResourceDto;
    private Pageable pageSpecification;
    private ItemQueryDto itemQueryDto;
    private AttributeDto firstAttribute;
    private AttributeDto secondAttribute;
    private AttributeDto thirdAttribute;
    private ItemTypeDto itemType;

    @Before
    public void setUp() throws Exception
    {
        final LazyItemDto lazyItemDto = new LazyItemDto();

        this.lazyItemPageResourceDto = new LazyItemPageResourceDto();
        this.lazyItemPageResourceDto.setContent(new ArrayList<LazyItemDto>());
        this.lazyItemPageResourceDto.getContent().add(lazyItemDto);

        this.emptyLazyItemPageResourceDto = new LazyItemPageResourceDto();
        this.emptyLazyItemPageResourceDto.setContent(new ArrayList<LazyItemDto>());

        this.pageSpecification = this.serviceHelper.createPageable(null, null, null, null);

        this.itemQueryDto = new ItemQueryDto();
        this.itemQueryDto.setItemId(Arrays.asList(ITEM_1_ID));
        this.itemQueryDto.setItemName(ITEM_NAME);
        this.itemQueryDto.setItemTypeId(Arrays.asList(ITEM_TYPE_1_ID));

        this.firstAttribute = new AttributeDto();
        this.firstAttribute.setValue(FIRST);
        this.secondAttribute = new AttributeDto();
        this.secondAttribute.setValue(SECOND);
        this.thirdAttribute = new AttributeDto();
        this.thirdAttribute.setValue(THIRD);
        this.itemType = new ItemTypeDto();
        this.itemType.setName(ITEM_TPYE_NAME);

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(this.itemQueryDto, ItemQueryDto.class)).willReturn(this.itemQueryDto);
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithoutAttributesFound() throws Exception
    {
        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.lazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertEquals(this.lazyItemPageResourceDto, resultPageResource);
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithAttributesFound() throws Exception
    {
        this.itemQueryDto.setAttributeId(Arrays.asList(ATTRIBUTE_1_ID));
        this.itemQueryDto.setAttributeValue(ATTRIBUTE_1_VALUE);
        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.lazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertEquals(this.lazyItemPageResourceDto, resultPageResource);
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithoutAttributesNotFound() throws Exception
    {

        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.emptyLazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertTrue(resultPageResource.getContent().isEmpty());
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithAttributesNotFound() throws Exception
    {
        this.itemQueryDto.setAttributeId(Arrays.asList(ATTRIBUTE_1_ID));
        this.itemQueryDto.setAttributeValue(ATTRIBUTE_1_VALUE);
        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.emptyLazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertTrue(resultPageResource.getContent().isEmpty());
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithAttributeNameNotFound() throws Exception
    {
        this.itemQueryDto.setAttributeName(ATTRIBUTE_1_NAME);
        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.emptyLazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertTrue(resultPageResource.getContent().isEmpty());
    }

    @Test
    @Ignore
    public void testGetItemsByAssetWithAttributeName() throws Exception
    {
        this.itemQueryDto.setAttributeName(ATTRIBUTE_1_NAME);
        when(this.itemDao.findItemsByAsset(ASSET_1_ID, 1L, false, this.pageSpecification, this.itemQueryDto))
                .thenReturn(this.lazyItemPageResourceDto);
        final PageResource<LazyItemDto> resultPageResource = this.itemService
                .getItems(null, null, null, null, ASSET_1_ID, 1L, false, this.itemQueryDto);
        Assert.assertEquals(this.lazyItemPageResourceDto, resultPageResource);
    }

    @Test
    @Ignore
    public void testGenerateItemName() throws Exception
    {
        final List<AttributeDto> attributes = new ArrayList<AttributeDto>();
        attributes.add(this.firstAttribute);
        attributes.add(this.secondAttribute);
        attributes.add(this.thirdAttribute);

        final String generatedItemName = invokeGenerateItemName(attributes, this.itemType);
        Assert.assertEquals(EXPECTED_ITEM_NAME, generatedItemName);
    }

    @Test
    @Ignore
    public void testGenerateItemNameEmptyAttributeTypes() throws Exception
    {
        final List<AttributeDto> attributes = new ArrayList<AttributeDto>();
        final String generatedItemName = invokeGenerateItemName(attributes, this.itemType);
        Assert.assertEquals(ITEM_TPYE_NAME, generatedItemName);
    }

    @Test
    @Ignore
    public void testGenerateItemNameNullAttributeTypes() throws Exception
    {
        final String generatedItemName = invokeGenerateItemName(null, this.itemType);
        Assert.assertEquals(ITEM_TPYE_NAME, generatedItemName);
    }

    private String invokeGenerateItemName(final List<AttributeDto> attributes, final ItemTypeDto itemTypeDto) throws Exception
    {
        // Call the private method ItemServiceImpl.generateItemName()
        return Whitebox.<String>invokeMethod(this.itemService, "generateItemName", attributes, itemTypeDto);
    }

    @Test
    @Ignore
    public void setItemReviewedDoesNothingIfTheReviewedFlagOfTrueIsUnchanged()
    {
        unchangedReviewedFlagCheck(Boolean.TRUE);
    }

    @Test
    @Ignore
    public void setItemReviewedDoesNothingIfTheReviewedFlagOfFalseIsUnchanged()
    {
        unchangedReviewedFlagCheck(Boolean.FALSE);
    }

    @Test
    @Ignore
    public void setItemReviewedDoesNothingIfTheReviewedFlagOfNullIsUnchanged()
    {
        unchangedReviewedFlagCheck(null);
    }

    private void unchangedReviewedFlagCheck(final Boolean reviewed)
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(ITEM_1_ID);
        mockedReturnedDto.setReviewed(reviewed);

        final ItemLightDto passedItem = new ItemLightDto();
        passedItem.setId(ITEM_1_ID);
        passedItem.setReviewed(reviewed);

        //// when(itemDao.getItem(ITEM_1_ID))
        // .thenReturn(mockedReturnedDto);
        Mockito.verifyNoMoreInteractions(this.itemDao);

        this.itemService.setItemReviewed(passedItem.getId());
    }

    @Test
    @Ignore
    public void changingItemReviewedToTrueUpdatesDescendants()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(ITEM_1_ID);
        mockedReturnedDto.setReviewed(Boolean.FALSE);

        final ItemLightDto passedItem = new ItemLightDto();
        passedItem.setId(ITEM_1_ID);
        passedItem.setReviewed(Boolean.TRUE);

        //// when(itemDao.getItem(ITEM_1_ID))
        // .thenReturn(mockedReturnedDto);

        this.itemService.setItemReviewed(passedItem.getId());

        // verify(itemDao).getItem(ITEM_1_ID);
        verify(this.itemPersistDao).setItemAndAllDescendantsToSuccessfullyReviewed(ITEM_1_ID);
        Mockito.verifyNoMoreInteractions(this.itemDao);
    }

    @Test
    @Ignore
    public void changingItemReviewedToFalseUpdatesAncestors()
    {
        final LazyItemDto mockedReturnedDto = new LazyItemDto();
        mockedReturnedDto.setId(ITEM_1_ID);
        mockedReturnedDto.setReviewed(Boolean.TRUE);

        final ItemLightDto passedItem = new ItemLightDto();
        passedItem.setId(ITEM_1_ID);
        passedItem.setReviewed(Boolean.FALSE);

        // when(itemDao.getItem(ITEM_1_ID))
        // .thenReturn(mockedReturnedDto);

        this.itemService.setItemReviewed(passedItem.getId());

        // verify(itemDao).getItem(ITEM_1_ID);
        verify(this.itemPersistDao).setItemAndAllAncestorsToRequireAReview(ITEM_1_ID);
        Mockito.verifyNoMoreInteractions(this.itemDao);
    }

    @Test
    @Ignore
    public void noItemNameUpdateIfThereAreNoAttributes()
    {
        final ItemWithAttributesDto itemWithAttributes = new ItemWithAttributesDto();

        Mockito.verifyZeroInteractions(this.itemDao, this.assetReferenceDataDao, this.attributeDao);

        this.itemService.updateItemName(itemWithAttributes);
    }

    @Test
    @Ignore
    public void noItemNameUpdateIfNoAttributesHaveNamingOrders()
    {
        final ItemWithAttributesDto itemWithAttributes = new ItemWithAttributesDto();
        final List<AttributeDto> attributes = new ArrayList<>();

        final AttributeDto attribute = new AttributeDto();
        final Long attributeTypeId = 50L;
        final LinkResource attributeTypeLinkResource = new LinkResource(attributeTypeId);
        attribute.setAttributeType(attributeTypeLinkResource);
        attribute.setId(ATTRIBUTE_1_ID);
        attributes.add(attribute);
        itemWithAttributes.setAttributes(attributes);

        final AttributeTypeDto mockedReturnedDto = new AttributeTypeDto();
        when(this.assetReferenceDataDao.getAttributeType(attributeTypeId)).thenReturn(mockedReturnedDto);

        Mockito.verifyZeroInteractions(this.itemDao, this.attributeDao);

        this.itemService.updateItemName(itemWithAttributes);

        verify(this.assetReferenceDataDao).getAttributeType(attributeTypeId);
        Mockito.verifyNoMoreInteractions(this.assetReferenceDataDao);
    }

    @Test
    public void deleteRelationshipWhereDraftVersionsAllMatch()
    {
        final Long originalRelationshipId = 1L;
        final Long draftVersion = 10L;

        final ItemRelationshipDO originalRelationship = new ItemRelationshipDO();
        originalRelationship.setId(originalRelationshipId);

        final VersionedItemDO fromItem = new VersionedItemDO();
        fromItem.setAssetId(ASSET_1_ID);
        fromItem.setId(ITEM_1_ID);

        final VersionedItemDO toItem = new VersionedItemDO();
        toItem.setAssetId(ASSET_1_ID);
        toItem.setId(ITEM_2_ID);

        originalRelationship.setFromItem(fromItem);
        originalRelationship.setToItem(toItem);

        when(this.itemDao.getItemRelationship(originalRelationshipId)).thenReturn(originalRelationship);

        when(this.assetDao.getDraftVersion(ASSET_1_ID)).thenReturn(draftVersion);

        when(this.itemDao.getItemRelationshipId(ASSET_1_ID, ITEM_1_ID, draftVersion, ITEM_2_ID, draftVersion))
                .thenReturn(Optional.of(originalRelationshipId));

        this.itemService.deleteItemRelationship(originalRelationshipId);

        verify(this.itemPersistDao).deleteItemRelationship(originalRelationshipId);
        Mockito.verifyNoMoreInteractions(this.itemPersistDao);
    }

    @Test
    public void deleteRelationshipWhereDraftVersionsPointToADifferentRelationship()
    {
        final Long originalRelationshipId = 1L;
        final Long deleteableRelationshipId = 1000L;
        final Long draftVersion = 10L;

        final ItemRelationshipDO originalRelationship = new ItemRelationshipDO();
        originalRelationship.setId(originalRelationshipId);

        final VersionedItemDO fromItem = new VersionedItemDO();
        fromItem.setAssetId(ASSET_1_ID);
        fromItem.setId(ITEM_1_ID);

        final VersionedItemDO toItem = new VersionedItemDO();
        toItem.setAssetId(ASSET_1_ID);
        toItem.setId(ITEM_2_ID);

        originalRelationship.setFromItem(fromItem);
        originalRelationship.setToItem(toItem);

        when(this.itemDao.getItemRelationship(originalRelationshipId)).thenReturn(originalRelationship);

        when(this.assetDao.getDraftVersion(ASSET_1_ID)).thenReturn(draftVersion);

        when(this.itemDao.getItemRelationshipId(ASSET_1_ID, ITEM_1_ID, draftVersion, ITEM_2_ID, draftVersion))
                .thenReturn(Optional.of(deleteableRelationshipId));

        this.itemService.deleteItemRelationship(originalRelationshipId);

        verify(this.itemPersistDao).deleteItemRelationship(deleteableRelationshipId);
        Mockito.verifyNoMoreInteractions(this.itemPersistDao);
    }

    @Test
    public void deleteRelationshipThatDoesNotExistBetweenExistingDraftItems()
    {
        final Long originalRelationshipId = 1L;
        final Long deleteableRelationshipId = 1000L;
        final Long draftVersion = 10L;

        final ItemRelationshipDO originalRelationship = new ItemRelationshipDO();
        originalRelationship.setId(originalRelationshipId);

        final VersionedItemDO fromItem = new VersionedItemDO();
        fromItem.setAssetId(ASSET_1_ID);
        fromItem.setId(ITEM_1_ID);

        final VersionedItemDO toItem = new VersionedItemDO();
        toItem.setAssetId(ASSET_1_ID);
        toItem.setId(ITEM_2_ID);

        originalRelationship.setFromItem(fromItem);
        originalRelationship.setToItem(toItem);

        when(this.itemDao.getItemRelationship(originalRelationshipId)).thenReturn(originalRelationship);

        when(this.assetDao.getDraftVersion(ASSET_1_ID)).thenReturn(draftVersion);

        when(this.itemDao.getItemRelationshipId(ASSET_1_ID, ITEM_1_ID, draftVersion, ITEM_2_ID, draftVersion))
                .thenReturn(Optional.empty());

        final VersionedItemPK versionedFromItemPK = new VersionedItemPK(ITEM_1_ID, ASSET_1_ID, draftVersion);
        final VersionedItemPK versionedToItemPK = new VersionedItemPK(ITEM_2_ID, ASSET_1_ID, draftVersion);

        when(this.itemDao.getItemLight(versionedFromItemPK))
                .thenReturn(new ItemLightDto());

        when(this.itemDao.getItemLight(versionedToItemPK))
                .thenReturn(new ItemLightDto());

        final VersionedItemPK versionedToItemPKForRelationshipCreation = new VersionedItemPK(ITEM_2_ID, ASSET_1_ID, draftVersion);

        final ItemLightDto newToItemForRelationship = new ItemLightDto();
        newToItemForRelationship.setId(ITEM_2_ID);

        when(this.itemDao.getItemLight(versionedToItemPKForRelationshipCreation))
                .thenReturn(newToItemForRelationship);

        final ItemRelationshipDto newRelationship = new ItemRelationshipDto();
        newRelationship.setToItem(newToItemForRelationship);
        newRelationship.setType(new LinkResource(RelationshipType.IS_RELATED_TO.getValue()));

        final ItemRelationshipDto newRelationshipToDelete = new ItemRelationshipDto();
        newRelationshipToDelete.setId(deleteableRelationshipId);
        when(this.itemPersistDao.createItemRelationship(ASSET_1_ID, ITEM_1_ID, newRelationship))
                .thenReturn(newRelationshipToDelete);

        this.itemService.deleteItemRelationship(originalRelationshipId);

        verify(this.itemPersistDao).deleteItemRelationship(deleteableRelationshipId);
        verify(this.itemPersistDao).createItemRelationship(ASSET_1_ID, ITEM_1_ID, newRelationship);

        Mockito.verifyNoMoreInteractions(this.itemPersistDao);
    }

    @Test
    public void deleteRelationshipThatDoesNotExistBetweenItemsWithNoDraftState()
    {

        final Long originalRelationshipId = 1L;
        final Long deleteableRelationshipId = 1000L;
        final Long draftVersion = 10L;
        final Long newDraftVersion = 11L;

        final ItemRelationshipDO originalRelationship = new ItemRelationshipDO();
        originalRelationship.setId(originalRelationshipId);

        final VersionedItemDO fromItem = new VersionedItemDO();
        fromItem.setAssetId(ASSET_1_ID);
        fromItem.setId(ITEM_1_ID);

        final VersionedItemDO toItem = new VersionedItemDO();
        toItem.setAssetId(ASSET_1_ID);
        toItem.setId(ITEM_2_ID);

        originalRelationship.setFromItem(fromItem);
        originalRelationship.setToItem(toItem);

        when(this.itemDao.getItemRelationship(originalRelationshipId)).thenReturn(originalRelationship);

        when(this.assetDao.getDraftVersion(ASSET_1_ID)).thenReturn(draftVersion);

        when(this.itemDao.getItemRelationshipId(ASSET_1_ID, ITEM_1_ID, draftVersion, ITEM_2_ID, draftVersion))
                .thenReturn(Optional.empty());

        final VersionedItemPK versionedFromItemPK = new VersionedItemPK(ITEM_1_ID, ASSET_1_ID, draftVersion);
        final VersionedItemPK versionedToItemPK = new VersionedItemPK(ITEM_2_ID, ASSET_1_ID, draftVersion);

        when(this.itemDao.getItemLight(versionedFromItemPK))
                .thenReturn(null);

        when(this.assetVersionUpdateService.upVersionSingleItem(ASSET_1_ID, ITEM_1_ID, false))
                .thenReturn(newDraftVersion);

        when(this.itemDao.getItemLight(versionedToItemPK))
                .thenReturn(null);

        when(this.assetVersionUpdateService.upVersionSingleItem(ASSET_1_ID, ITEM_2_ID, false))
                .thenReturn(newDraftVersion);

        final VersionedItemPK versionedToItemPKForRelationshipCreation = new VersionedItemPK(ITEM_2_ID, ASSET_1_ID, newDraftVersion);

        final ItemLightDto newToItemForRelationship = new ItemLightDto();
        newToItemForRelationship.setId(ITEM_2_ID);

        when(this.itemDao.getItemLight(versionedToItemPKForRelationshipCreation))
                .thenReturn(newToItemForRelationship);

        final ItemRelationshipDto newRelationship = new ItemRelationshipDto();
        newRelationship.setToItem(newToItemForRelationship);
        newRelationship.setType(new LinkResource(RelationshipType.IS_RELATED_TO.getValue()));

        final ItemRelationshipDto newRelationshipToDelete = new ItemRelationshipDto();
        newRelationshipToDelete.setId(deleteableRelationshipId);
        when(this.itemPersistDao.createItemRelationship(ASSET_1_ID, ITEM_1_ID, newRelationship))
                .thenReturn(newRelationshipToDelete);

        this.itemService.deleteItemRelationship(originalRelationshipId);

        verify(this.itemPersistDao).deleteItemRelationship(deleteableRelationshipId);
        verify(this.itemPersistDao).createItemRelationship(ASSET_1_ID, ITEM_1_ID, newRelationship);

        Mockito.verifyNoMoreInteractions(this.itemPersistDao);
    }
}
