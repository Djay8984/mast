package com.baesystems.ai.lr.service.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.hibernate.PropertyValueException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.CodicilDefectQueryDto;
import com.baesystems.ai.lr.enums.CodicilStatusType;
import com.baesystems.ai.lr.enums.StaleCheckType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.exception.StaleObjectException;
import com.baesystems.ai.lr.service.codicils.CoCService;
import com.baesystems.ai.lr.service.codicils.CoCServiceImpl;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.utils.PageResourceFactory;
import com.baesystems.ai.lr.utils.PageResourceFactoryImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;
import com.baesystems.ai.lr.utils.QueryDtoUtils;
import com.baesystems.ai.lr.utils.ServiceHelperImpl;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(QueryDtoUtils.class)
public class CoCServiceImplTest extends PageResourceImplBasedTest
{
    @Mock
    private CoCDao cocDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private SurveyService surveyService;

    @Mock
    private final PageResourceFactory pageFactory = new PageResourceFactoryImpl();

    @InjectMocks
    private final CoCService cocService = new CoCServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    private static final Long COC_1_ID = 1L;
    private static final Long WIP_COC_1_ID = 1L;
    private static final Long DEFECT_1_ID = 1L;
    private static final Long INVALID_COC_ID = 999L;
    private static final Long INVALID_WIP_COC_ID = 999L;

    private static final Long ASSET_1_ID = 1L;
    private static final Long INVALID_ASSET_ID = 999L;

    private static final Long JOB_1_ID = 1L;
    private static final Long INVALID_JOB_ID = 999L;

    private static final Long STATUS_1_ID = 1L;

    private final PropertyValueException propertyValueException = new PropertyValueException("", "", "");
    private final CoCDto expectedCoCDto = new CoCDto();
    private final CoCDto expectedWIPCoCDto = new CoCDto();
    private final CoCDto invalidCoCDto = new CoCDto();
    private Pageable pageSpecification;
    private CodicilDefectQueryDto codicilDefectQueryDto;
    private PageResource<CoCDto> emptyPageResource;

    @Before
    public void setUp() throws BadPageRequestException
    {
        this.pageSpecification = this.serviceHelper.createPageable(null, null, null, null);
        this.codicilDefectQueryDto = new CodicilDefectQueryDto();
        this.codicilDefectQueryDto.setSearchString("Random string");
        this.emptyPageResource = new CoCPageResourceDto();
        this.emptyPageResource.setContent(new ArrayList<CoCDto>());

        this.expectedWIPCoCDto.setStatus(new LinkResource(STATUS_1_ID));
        this.expectedWIPCoCDto.setJob(new LinkResource(JOB_1_ID));
        this.expectedWIPCoCDto.setInheritedFlag(Boolean.TRUE);

        // Let the QueryUtilsDto just return the instance
        PowerMockito.mockStatic(QueryDtoUtils.class);
        BDDMockito.given(QueryDtoUtils.sanitiseQueryDto(this.codicilDefectQueryDto, CodicilDefectQueryDto.class))
                .willReturn(this.codicilDefectQueryDto);
    }

    @Test
    public void testGetCoCWithValidID() throws RecordNotFoundException
    {
        when(this.cocDao.getCoC(COC_1_ID)).thenReturn(this.expectedCoCDto);
        final CoCDto coCDto = this.cocService.getCoC(COC_1_ID, false);
        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testGetWIPCoCWithValidID() throws RecordNotFoundException
    {
        when(this.wipCoCDao.getCoC(WIP_COC_1_ID)).thenReturn(this.expectedCoCDto);
        final CoCDto coCDto = this.cocService.getCoC(WIP_COC_1_ID, true);
        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testGetCoCWithInvalidID() throws RecordNotFoundException
    {
        when(this.cocDao.getCoC(INVALID_COC_ID)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.cocService.getCoC(INVALID_COC_ID, false);
    }

    @Test
    public void testGetWIPCoCWithInvalidID() throws RecordNotFoundException
    {
        when(this.wipCoCDao.getCoC(INVALID_WIP_COC_ID)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.cocService.getCoC(INVALID_WIP_COC_ID, true);
    }

    @Test
    public void testGetCoCByAssetWithValidAssetID() throws BadPageRequestException
    {
        final PageResource<CoCDto> expectedPagedResource = getPageResource(false);
        when(this.cocDao.getCoCsForAsset(this.pageSpecification, ASSET_1_ID, null)).thenReturn(expectedPagedResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService.getCoCsByAsset(null, null, null, null, ASSET_1_ID, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetCoCsByAssetWithInvalidAssetID() throws BadPageRequestException
    {
        when(this.cocDao.getCoCsForAsset(this.pageSpecification, INVALID_ASSET_ID, null)).thenReturn(null);
        assertNull(this.cocService.getCoCsByAsset(null, null, null, null, INVALID_ASSET_ID, null));
    }

    @Test
    public void testGetCoCByAssetWithStatusFound() throws BadPageRequestException
    {
        final PageResource<CoCDto> expectedPagedResource = getPageResource(false);
        when(this.cocDao.getCoCsForAsset(this.pageSpecification, ASSET_1_ID, STATUS_1_ID)).thenReturn(expectedPagedResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService.getCoCsByAsset(null, null, null, null, ASSET_1_ID, STATUS_1_ID);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetCoCsByAssetWithStatusNonFound() throws BadPageRequestException
    {
        when(this.cocDao.getCoCsForAsset(this.pageSpecification, ASSET_1_ID, STATUS_1_ID)).thenReturn(null);
        assertNull(this.cocService.getCoCsByAsset(null, null, null, null, ASSET_1_ID, STATUS_1_ID));
    }

    @Test
    public void testSaveCoCWithValidCoC() throws RecordNotFoundException
    {
        when(this.cocDao.saveCoC(this.expectedCoCDto, ASSET_1_ID)).thenReturn(this.expectedCoCDto);
        final CoCDto coCDto = this.cocService.saveCoC(ASSET_1_ID, this.expectedCoCDto);
        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveWIPCoCWithValidCoC() throws RecordNotFoundException
    {
        when(this.wipCoCDao.saveCoC(JOB_1_ID, this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.saveWIPCoC(JOB_1_ID, this.expectedWIPCoCDto);

        Mockito.verify(this.wipCoCDao).saveCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.surveyService).createAssetManagementSurveyForWipCoC(JOB_1_ID, this.expectedCoCDto.getId());

        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveWipCoCWithoutSurveyCreation() throws RecordNotFoundException
    {
        this.expectedWIPCoCDto.setParent(new LinkResource(COC_1_ID));
        when(this.wipCoCDao.saveCoC(JOB_1_ID, this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.saveWIPCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.wipCoCDao).saveCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.surveyService, Mockito.never()).createAssetManagementSurveyForWipCoC(JOB_1_ID, this.expectedWIPCoCDto.getId());
        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveNonInheritedWIPCoC() throws RecordNotFoundException
    {
        this.expectedWIPCoCDto.setInheritedFlag(Boolean.FALSE);
        when(this.wipCoCDao.saveCoC(JOB_1_ID, this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.saveWIPCoC(JOB_1_ID, this.expectedWIPCoCDto);

        Mockito.verify(this.wipCoCDao).saveCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.surveyService).createAssetManagementSurveyForWipCoC(JOB_1_ID, this.expectedWIPCoCDto.getId());

        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveWIPCoCForDefect() throws RecordNotFoundException
    {
        this.expectedWIPCoCDto.setDefect(new LinkResource(DEFECT_1_ID));
        when(this.wipCoCDao.saveCoC(JOB_1_ID, this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.saveWIPCoC(JOB_1_ID, this.expectedWIPCoCDto);

        Mockito.verify(this.wipCoCDao).saveCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.surveyService).createAssetManagementSurveyForWipCoC(JOB_1_ID, this.expectedWIPCoCDto.getId());

        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveNonInheritedWIPCoCForDefect() throws RecordNotFoundException
    {
        this.expectedWIPCoCDto.setDefect(new LinkResource(DEFECT_1_ID));
        this.expectedWIPCoCDto.setInheritedFlag(Boolean.FALSE);
        when(this.wipCoCDao.saveCoC(JOB_1_ID, this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.saveWIPCoC(JOB_1_ID, this.expectedWIPCoCDto);

        Mockito.verify(this.wipCoCDao).saveCoC(JOB_1_ID, this.expectedWIPCoCDto);
        Mockito.verify(this.surveyService, Mockito.never()).createAssetManagementSurveyForWipCoC(JOB_1_ID, this.expectedWIPCoCDto.getId());

        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testSaveCoCWithInvalidCoC() throws RecordNotFoundException
    {
        doThrow(this.propertyValueException).when(this.cocDao).saveCoC(this.invalidCoCDto, ASSET_1_ID);
        this.thrown.expect(PropertyValueException.class);
        this.cocService.saveCoC(ASSET_1_ID, this.invalidCoCDto);
    }

    @Test
    public void testSaveWIPCoCWithInvalidCoC() throws RecordNotFoundException
    {
        doThrow(this.propertyValueException).when(this.wipCoCDao).saveCoC(JOB_1_ID, this.invalidCoCDto);
        this.thrown.expect(PropertyValueException.class);
        this.cocService.saveWIPCoC(JOB_1_ID, this.invalidCoCDto);
    }

    @Test
    public void testGetWIPCoCsByJob() throws BadPageRequestException
    {
        final PageResource<CoCDto> expectedPagedResource = getPageResource(true);
        when(this.wipCoCDao.getCoCsForJob(this.pageSpecification, JOB_1_ID, null)).thenReturn(expectedPagedResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService.getWIPCoCsForJob(null, null, null, null, JOB_1_ID, null);
        assertEquals(expectedPagedResource, dtoPageResource);
    }

    @Test
    public void testGetWIPCoCsByInvalidJobID() throws RecordNotFoundException, BadPageRequestException
    {
        when(this.wipCoCDao.getCoCsForJob(this.pageSpecification, INVALID_JOB_ID, null)).thenReturn(null);
        assertNull(this.cocService.getWIPCoCsForJob(null, null, null, null, JOB_1_ID, null));
    }

    @Test
    public void testUpdateCoCWithValidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.cocDao.findOneWithLock(this.expectedCoCDto.getId(), CoCDto.class, StaleCheckType.COC)).thenReturn(this.expectedCoCDto);
        // when(wipCoCDao.updateCoC(expectedCoCDto)).thenReturn(expectedCoCDto);
        when(this.cocDao.updateCoC(this.expectedCoCDto)).thenReturn(this.expectedCoCDto);
        final CoCDto coCDto = this.cocService.updateCoC(this.expectedCoCDto);
        assertEquals(this.expectedCoCDto, coCDto);
    }

    @Test
    public void testUpdateWIPCoCWithValidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.wipCoCDao.updateCoC(this.expectedWIPCoCDto)).thenReturn(this.expectedWIPCoCDto);
        final CoCDto coCDto = this.cocService.updateWIPCoC(this.expectedWIPCoCDto);
        assertEquals(this.expectedWIPCoCDto, coCDto);
    }

    @Test
    public void testUpdateWIPCoCWithValidIDAndDeletedState() throws RecordNotFoundException, StaleObjectException
    {
        final CoCDto deletedDto = new CoCDto();

        deletedDto.setStatus(new LinkResource(CodicilStatusType.COC_DELETED.getValue()));
        deletedDto.setJob(new LinkResource(JOB_1_ID));
        deletedDto.setId(WIP_COC_1_ID);

        when(this.wipCoCDao.updateCoC(deletedDto)).thenReturn(deletedDto);
        final CoCDto coCDto = this.cocService.updateWIPCoC(deletedDto);

        assertEquals(deletedDto, coCDto);
    }

    @Test
    public void testUpdateCoCWithInvalidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.cocDao.findOneWithLock(this.expectedCoCDto.getId(), CoCDto.class, StaleCheckType.COC)).thenReturn(null);
        when(this.cocDao.updateCoC(this.invalidCoCDto)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.cocService.updateCoC(this.invalidCoCDto);
    }

    @Test
    public void testUpdateWIPCoCWithInvalidID() throws RecordNotFoundException, StaleObjectException
    {
        when(this.wipCoCDao.updateCoC(this.invalidCoCDto)).thenReturn(null);
        this.thrown.expect(RecordNotFoundException.class);
        this.cocService.updateWIPCoC(this.invalidCoCDto);
    }

    @Test
    public void testQueryCoC() throws BadPageRequestException
    {
        final PageResource<CoCDto> expectedPageResource = getPageResource(false);
        when(this.cocDao.findAllForAsset(this.pageSpecification, ASSET_1_ID, this.codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService
                .getCoCsByQuery(ASSET_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryCoCNonFound() throws BadPageRequestException
    {
        when(this.cocDao.findAllForAsset(this.pageSpecification, ASSET_1_ID, this.codicilDefectQueryDto)).thenReturn(this.emptyPageResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService
                .getCoCsByQuery(ASSET_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(this.emptyPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPCoC() throws BadPageRequestException
    {
        final PageResource<CoCDto> expectedPageResource = getPageResource(true);
        when(this.wipCoCDao.findAllForJob(this.pageSpecification, JOB_1_ID, this.codicilDefectQueryDto)).thenReturn(expectedPageResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService
                .getWIPCoCsByQuery(JOB_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(expectedPageResource, dtoPageResource);
    }

    @Test
    public void testQueryWIPCoCNonFound() throws BadPageRequestException
    {
        when(this.wipCoCDao.findAllForJob(this.pageSpecification, JOB_1_ID, this.codicilDefectQueryDto)).thenReturn(this.emptyPageResource);
        final PageResource<CoCDto> dtoPageResource = this.cocService
                .getWIPCoCsByQuery(JOB_1_ID, null, null, null, null, this.codicilDefectQueryDto);
        assertEquals(this.emptyPageResource, dtoPageResource);
    }

    @Test
    public void testDeleteCoCWithValidID() throws RecordNotFoundException
    {
        when(this.cocDao.exists(COC_1_ID)).thenReturn(false);
        final boolean result = this.cocService.deleteCoC(COC_1_ID);
        assert result;
    }

    @Test
    public void testDeleteWithInvalidID() throws RecordNotFoundException
    {
        when(this.cocDao.exists(INVALID_COC_ID)).thenReturn(true);
        final boolean result = this.cocService.deleteCoC(INVALID_COC_ID);
        assert !result;
    }

    private PageResource<CoCDto> getPageResource(final boolean wip)
    {
        final ArrayList<CoCDto> tempList = new ArrayList<>();
        tempList.add(wip ? this.expectedWIPCoCDto : this.expectedCoCDto);
        final Page<CoCDto> coCDtoPage = new PageImpl<>(tempList);
        return this.pageFactory.createPageResource(coCDtoPage, CoCPageResourceDto.class);
    }
}
