package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.defects.DefectDefectValueDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectItemDto;
import com.baesystems.ai.lr.dto.defects.JobDefectDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

@RunWith(MockitoJUnitRunner.class)
public class ReportSubmissionHelperImplTest
{
    @InjectMocks
    private final ReportSubmissionHelper reportSubmissionHelper = new ReportSubmissionHelperImpl();

    private static final Long ONE = 1L;
    private static final Long TWO = 2L;
    private static final Long THREE = 3L;

    private Long currentId = 0L;

    private Long newId()
    {
        currentId += 1L;
        return currentId;
    }

    private WorkItemDto setUpTask()
    {
        final WorkItemDto wipTask = new WorkItemDto();
        wipTask.setId(ONE);
        wipTask.setCodicil(new LinkResource());
        wipTask.setSurvey(new SurveyDto());
        wipTask.setConditionalParent(new WorkItemDto());
        wipTask.setWorkItem(new WorkItemDto());
        wipTask.setParent(new LinkResource(TWO));
        wipTask.setDueDate(new Date());

        return wipTask;
    }

    private DefectDto setUpDefect()
    {
        final DefectDto wipDefect = new DefectDto();
        wipDefect.setId(ONE);
        wipDefect.setJob(new LinkResource(TWO));
        wipDefect.setAsset(new LinkResource(THREE));

        return wipDefect;
    }

    private Entry<DefectDefectValueDto, DefectDefectValueDto> setUpDefectValueWipAndNonWipPair()
    {
        final DefectDefectValueDto wipDefectValue = new DefectDefectValueDto();
        wipDefectValue.setId(newId());
        wipDefectValue.setDefectValue(new DefectValueDto());
        wipDefectValue.getDefectValue().setId(newId());
        wipDefectValue.setOtherDetails("WIP");
        wipDefectValue.setParent(new LinkResource(newId()));

        final DefectDefectValueDto defectValue = new DefectDefectValueDto();
        defectValue.setId(wipDefectValue.getParent().getId());
        defectValue.setDefectValue(new DefectValueDto());
        defectValue.getDefectValue().setId(newId());
        defectValue.setOtherDetails("REAL");

        return new AbstractMap.SimpleEntry<DefectDefectValueDto, DefectDefectValueDto>(wipDefectValue, defectValue);
    }

    private Entry<DefectItemDto, DefectItemDto> setUpDefectItemWipAndNonWipPair()
    {
        final DefectItemDto wipDefectItem = new DefectItemDto();
        wipDefectItem.setId(newId());
        wipDefectItem.setItem(new ItemLightDto());
        wipDefectItem.getItem().setId(newId());
        wipDefectItem.setParent(new LinkResource(newId()));

        final DefectItemDto defectItem = new DefectItemDto();
        defectItem.setId(wipDefectItem.getParent().getId());
        defectItem.setItem(new ItemLightDto());
        defectItem.getItem().setId(newId());

        return new AbstractMap.SimpleEntry<DefectItemDto, DefectItemDto>(wipDefectItem, defectItem);
    }

    private boolean containsValue(final List<DefectDefectValueDto> list, final DefectDefectValueDto targetElement)
    {
        boolean contains = false;

        for (final DefectDefectValueDto element : list)
        {
            if (element.getDefectValue() != null && targetElement.getDefectValue() != null
                    && element.getDefectValue().getId().equals(targetElement.getDefectValue().getId()))
            {
                contains = true;
                break;
            }
        }

        return contains;
    }

    private boolean containsItem(final List<DefectItemDto> list, final DefectItemDto targetElement)
    {
        boolean contains = false;

        for (final DefectItemDto element : list)
        {
            if (element.getItem() != null && targetElement.getItem() != null && element.getItem().getId().equals(targetElement.getItem().getId()))
            {
                contains = true;
                break;
            }
        }

        return contains;
    }

    @Test
    public void mapWipTaskToTaskNullServiceNullParentCodicil()
    {
        final WorkItemDto wipTask = setUpTask();

        final WorkItemLightDto realTask = this.reportSubmissionHelper.mapWipTaskToTask(wipTask, null);

        assertNull(realTask.getCodicil());
        assertNull(realTask.getSurvey());
        assertNull(realTask.getConditionalParent());
        assertNull(realTask.getWorkItem());
        assertNull(realTask.getScheduledService());
        assertEquals(wipTask.getDueDate(), realTask.getDueDate());
        assertEquals(wipTask.getParent().getId(), realTask.getId());
    }

    @Test
    public void mapWipTaskToTaskMapServiceNullParentCodicil()
    {
        final WorkItemDto wipTask = setUpTask();
        wipTask.getSurvey().setScheduledService(new ScheduledServiceDto());
        wipTask.getSurvey().getScheduledService().setId(THREE);

        final WorkItemLightDto realTask = this.reportSubmissionHelper.mapWipTaskToTask(wipTask, null);

        assertNull(realTask.getCodicil());
        assertNull(realTask.getSurvey());
        assertNull(realTask.getConditionalParent());
        assertNull(realTask.getWorkItem());
        assertEquals(wipTask.getDueDate(), realTask.getDueDate());
        assertEquals(wipTask.getParent().getId(), realTask.getId());
        assertEquals(wipTask.getSurvey().getScheduledService().getId(), realTask.getScheduledService().getId());
    }

    @Test
    public void mapWipTaskToTaskNullSurveyParentCodicil()
    {
        final WorkItemDto wipTask = setUpTask();
        wipTask.setSurvey(null);

        final WorkItemLightDto realTask = this.reportSubmissionHelper.mapWipTaskToTask(wipTask, THREE);

        assertNull(realTask.getSurvey());
        assertNull(realTask.getConditionalParent());
        assertNull(realTask.getWorkItem());
        assertNull(realTask.getScheduledService());
        assertEquals(wipTask.getDueDate(), realTask.getDueDate());
        assertEquals(wipTask.getParent().getId(), realTask.getId());
        assertEquals(THREE, realTask.getCodicil().getId());
    }

    @Test
    public void mergeDefectsNullLists()
    {
        final DefectDto wipDefect = setUpDefect();
        final DefectDto defect = new DefectDto();

        this.reportSubmissionHelper.mergeDefects(wipDefect, defect);

        assertNull(defect.getId());
        assertNull(defect.getAsset());
        assertNotNull(defect.getJobs());
        assertNotNull(defect.getItems());
        assertNotNull(defect.getValues());
        assertTrue(defect.getJobs().stream().map(defectJob -> defectJob.getJob()).collect(Collectors.toList()).contains(wipDefect.getJob()));
    }

    @Test
    public void mergeDefectsExistingJobList()
    {
        final DefectDto wipDefect = setUpDefect();
        final DefectDto defect = new DefectDto();
        defect.setJobs(new ArrayList<>());

        this.reportSubmissionHelper.mergeDefects(wipDefect, defect);

        assertNull(defect.getId());
        assertNull(defect.getAsset());
        assertNotNull(defect.getJobs());
        assertNotNull(defect.getItems());
        assertNotNull(defect.getValues());
        assertTrue(defect.getJobs().stream().map(defectJob -> defectJob.getJob()).collect(Collectors.toList()).contains(wipDefect.getJob()));
    }

    @Test
    public void mergeDefectsExistingJobListContainsJob()
    {
        final DefectDto wipDefect = setUpDefect();
        final DefectDto defect = new DefectDto();
        defect.setJobs(new ArrayList<>());
        defect.getJobs().add(new JobDefectDto());
        defect.getJobs().get(0).setJob(wipDefect.getJob());

        this.reportSubmissionHelper.mergeDefects(wipDefect, defect);

        assertNull(defect.getId());
        assertNull(defect.getAsset());
        assertNotNull(defect.getJobs());
        assertNotNull(defect.getItems());
        assertNotNull(defect.getValues());
        assertTrue(defect.getJobs().stream().map(defectJob -> defectJob.getJob()).collect(Collectors.toList()).contains(wipDefect.getJob()));
    }

    @Test
    public void mergeDefectsExistingLists()
    {
        final DefectDto wipDefect = setUpDefect();
        final DefectDto defect = new DefectDto();
        defect.setJobs(new ArrayList<>());
        defect.setItems(new ArrayList<>());
        defect.setValues(new ArrayList<>());

        this.reportSubmissionHelper.mergeDefects(wipDefect, defect);

        assertNull(defect.getId());
        assertNull(defect.getAsset());
        assertNotNull(defect.getJobs());
        assertNotNull(defect.getItems());
        assertNotNull(defect.getValues());
        assertTrue(defect.getJobs().stream().map(defectJob -> defectJob.getJob()).collect(Collectors.toList()).contains(wipDefect.getJob()));
    }

    @Test
    public void mergeDefectsValueAndItemMerge()
    {
        final DefectDto wipDefect = setUpDefect();
        final DefectDto defect = new DefectDto();
        final Entry<DefectDefectValueDto, DefectDefectValueDto> defectValuePiar1 = setUpDefectValueWipAndNonWipPair();
        final Entry<DefectDefectValueDto, DefectDefectValueDto> defectValuePiar2 = setUpDefectValueWipAndNonWipPair();
        final Entry<DefectDefectValueDto, DefectDefectValueDto> defectValuePiar3 = setUpDefectValueWipAndNonWipPair();
        final Entry<DefectItemDto, DefectItemDto> defectItemPiar1 = setUpDefectItemWipAndNonWipPair();
        final Entry<DefectItemDto, DefectItemDto> defectItemPiar2 = setUpDefectItemWipAndNonWipPair();
        final Entry<DefectItemDto, DefectItemDto> defectItemPiar3 = setUpDefectItemWipAndNonWipPair();

        wipDefect.setItems(new ArrayList<>());
        wipDefect.setValues(new ArrayList<>());
        defect.setItems(new ArrayList<>());
        defect.setValues(new ArrayList<>());

        // add value 2 remove value 3 and update value 1
        defectValuePiar2.getKey().setParent(null);
        wipDefect.getValues().add(defectValuePiar1.getKey());
        wipDefect.getValues().add(defectValuePiar2.getKey());
        defect.getValues().add(defectValuePiar1.getValue());
        defect.getValues().add(defectValuePiar3.getValue());

        // add value 2 remove value 3 and update value 1
        defectItemPiar2.getKey().setParent(null);
        wipDefect.getItems().add(defectItemPiar1.getKey());
        wipDefect.getItems().add(defectItemPiar2.getKey());
        defect.getItems().add(defectItemPiar1.getValue());
        defect.getItems().add(defectItemPiar3.getValue());

        this.reportSubmissionHelper.mergeDefects(wipDefect, defect);

        assertNull(defect.getId());
        assertNull(defect.getAsset());
        assertNotNull(defect.getJobs());
        assertNotNull(defect.getItems());
        assertNotNull(defect.getValues());
        assertTrue(defect.getJobs().stream().map(defectJob -> defectJob.getJob()).collect(Collectors.toList()).contains(wipDefect.getJob()));
        assertEquals(TWO.intValue(), defect.getValues().size());
        assertEquals(TWO.intValue(), defect.getItems().size());
        assertTrue(containsValue(defect.getValues(), defectValuePiar1.getKey()));
        assertTrue(containsValue(defect.getValues(), defectValuePiar2.getKey()));
        assertFalse(containsValue(defect.getValues(), defectValuePiar3.getKey()));
        assertTrue(containsItem(defect.getItems(), defectItemPiar1.getKey()));
        assertTrue(containsItem(defect.getItems(), defectItemPiar2.getKey()));
        assertFalse(containsItem(defect.getItems(), defectItemPiar3.getKey()));
        assertTrue(defect.getValues().stream().map(defectValue -> defectValue.getId()).collect(Collectors.toList())
                .contains(defectValuePiar1.getValue().getId()));
        assertTrue(defect.getValues().stream().map(defectValue -> defectValue.getId()).collect(Collectors.toList())
                .contains(null));
        assertTrue(defect.getItems().stream().map(defectItem -> defectItem.getId()).collect(Collectors.toList())
                .contains(defectItemPiar1.getValue().getId()));
        assertTrue(defect.getItems().stream().map(defectItem -> defectItem.getId()).collect(Collectors.toList())
                .contains(null));
    }

    @Test
    public void generalWipToNonWipMap()
    {
        final SurveyDto wipSurvey = new SurveyDto();
        final ScheduledServiceDto realService = new ScheduledServiceDto();

        wipSurvey.setScheduledService(new ScheduledServiceDto());
        wipSurvey.getScheduledService().setId(ONE);
        wipSurvey.setServiceCatalogue(new LinkResource(TWO));
        wipSurvey.setSurveyStatus(new LinkResource(THREE));

        this.reportSubmissionHelper.generalWipToNonWipMap(wipSurvey, realService);

        assertEquals(wipSurvey.getScheduledService().getId(), realService.getId());
        assertEquals(wipSurvey.getServiceCatalogue().getId(), realService.getServiceCatalogueId());
        assertEquals(wipSurvey.getSurveyStatus().getId(), realService.getServiceCreditStatus().getId());
    }

    @Test
    public void serviceWipToNonWipMap()
    {
        final SurveyDto wipSurvey = new SurveyDto();

        wipSurvey.setScheduledService(new ScheduledServiceDto());
        wipSurvey.getScheduledService().setId(ONE);
        wipSurvey.setServiceCatalogue(new LinkResource(TWO));
        wipSurvey.setSurveyStatus(new LinkResource(THREE));

        final ScheduledServiceDto realService = this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey);

        assertEquals(wipSurvey.getScheduledService().getId(), realService.getId());
        assertEquals(wipSurvey.getServiceCatalogue().getId(), realService.getServiceCatalogueId());
        assertEquals(wipSurvey.getSurveyStatus().getId(), realService.getServiceCreditStatus().getId());
    }

    @Test
    public void mapNewDefect()
    {
        final DefectDto wipDefect = new DefectDto();
        wipDefect.setId(ONE);

        final DefectDto realDefect = this.reportSubmissionHelper.mapNewDefect(wipDefect);

        assertNull(realDefect.getId());
    }
}
