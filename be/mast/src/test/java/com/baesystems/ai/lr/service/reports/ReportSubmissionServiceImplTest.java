package com.baesystems.ai.lr.service.reports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dao.codicils.ActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.AssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.CoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPActionableItemDao;
import com.baesystems.ai.lr.dao.codicils.WIPAssetNoteDao;
import com.baesystems.ai.lr.dao.codicils.WIPCoCDao;
import com.baesystems.ai.lr.dao.codicils.WIPCodicilDao;
import com.baesystems.ai.lr.dao.defects.DefectDao;
import com.baesystems.ai.lr.dao.defects.WIPDefectDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.SurveyDao;
import com.baesystems.ai.lr.dao.services.ProductDao;
import com.baesystems.ai.lr.dao.services.ServiceDao;
import com.baesystems.ai.lr.dao.tasks.WIPWorkItemDao;
import com.baesystems.ai.lr.dao.tasks.WorkItemDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.references.ProductCatalogueDto;
import com.baesystems.ai.lr.dto.references.ServiceCatalogueDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.enums.AttachmentTargetType;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.attachments.AttachmentService;
import com.baesystems.ai.lr.utils.ExceptionMessagesUtils;
import com.baesystems.ai.lr.utils.ReportSubmissionHelper;

@RunWith(MockitoJUnitRunner.class)
public class ReportSubmissionServiceImplTest
{
    @Mock
    private SurveyDao surveyDao;

    @Mock
    private ServiceDao serviceDao;

    @Mock
    private WIPWorkItemDao wipWorkItemDao;

    @Mock
    private WorkItemDao workItemDao;

    @Mock
    private ProductDao productDao;

    @Mock
    private DefectDao defectDao;

    @Mock
    private WIPDefectDao wipDefectDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private WIPCodicilDao wipCodicilDao;

    @Mock
    private CoCDao coCDao;

    @Mock
    private WIPCoCDao wipCoCDao;

    @Mock
    private AssetNoteDao assetNoteDao;

    @Mock
    private WIPAssetNoteDao wipAssetNoteDao;

    @Mock
    private ActionableItemDao actionableItemDao;

    @Mock
    private WIPActionableItemDao wipActionableItemDao;

    @Mock
    private AttachmentService attachmentService;

    @Mock
    private ReportSubmissionHelper reportSubmissionHelper;

    @InjectMocks
    private final ReportSubmissionService reportSubmissionService = new ReportSubmissionServiceImpl();

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private static final Long JOB_1_ID = 1L;
    private static final Long ASSET_2_ID = 2L;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final Boolean PROVISIONAL_DATES = true;

    private Long currentId = 0L;

    private MapperFacade mapper;

    private Long newId()
    {
        currentId += 1L;
        return currentId;
    }

    @SafeVarargs
    private final <T extends BaseDto, S extends BaseDto> Map<T, S> makeMap(@SuppressWarnings("unchecked") final Entry<T, S>... entries)
    {
        final Map<T, S> map = new HashMap<T, S>();

        for (final Entry<T, S> object : entries)
        {
            map.put(object.getKey(), object.getValue());
        }

        return map;
    }

    private <T extends BaseDto> List<T> setToList(final Set<T> set)
    {
        final List<T> list = new ArrayList<T>();
        list.addAll(set);

        return list;
    }

    @SuppressWarnings("unchecked")
    private <T extends BaseDto, S extends BaseDto> Map<T, S> copyMap(final Map<T, S> oldMap)
    {
        final Map<T, S> newMap = new HashMap<T, S>();

        for (final Entry<T, S> entry : oldMap.entrySet())
        {
            newMap.put((T) this.mapper.map(entry.getKey(), entry.getKey().getClass()),
                    (S) this.mapper.map(entry.getValue(), entry.getValue().getClass()));
        }

        return newMap;
    }

    private ActionableItemDto createAi()
    {
        final ActionableItemDto dummyAi = new ActionableItemDto();
        dummyAi.setId(newId());
        dummyAi.setParent(new LinkResource(newId()));
        dummyAi.setDefect(new LinkResource(newId()));
        dummyAi.setInternalId(dummyAi.getId().toString());

        return dummyAi;
    }

    private Entry<ActionableItemDto, ActionableItemDto> createAiAndWipAiExisting()
    {
        final ActionableItemDto wipAi = createAi();

        final ActionableItemDto realAi = new ActionableItemDto();
        realAi.setId(wipAi.getParent().getId());
        realAi.setParent(wipAi.getParent());
        realAi.setDefect(new LinkResource(newId()));
        realAi.setInternalId(realAi.getId().toString());

        return new AbstractMap.SimpleEntry<ActionableItemDto, ActionableItemDto>(wipAi, realAi);
    }

    private Entry<ActionableItemDto, ActionableItemDto> createAiAndWipAiNew()
    {
        final ActionableItemDto wipAi = new ActionableItemDto();
        wipAi.setId(newId());
        wipAi.setDefect(new LinkResource(newId()));
        wipAi.setInternalId("");

        final ActionableItemDto realAi = new ActionableItemDto();
        realAi.setDefect(new LinkResource(newId()));
        realAi.setInternalId("");

        return new AbstractMap.SimpleEntry<ActionableItemDto, ActionableItemDto>(wipAi, realAi);
    }

    private AssetNoteDto createAn()
    {
        final AssetNoteDto dummyAn = new AssetNoteDto();
        dummyAn.setId(newId());
        dummyAn.setParent(new LinkResource(newId()));
        dummyAn.setInternalId(dummyAn.getId().toString());

        return dummyAn;
    }

    private Entry<AssetNoteDto, AssetNoteDto> createAnAndWipAnExisting()
    {
        final AssetNoteDto wipAn = createAn();

        final AssetNoteDto realAn = new AssetNoteDto();
        realAn.setId(wipAn.getParent().getId());
        realAn.setParent(wipAn.getParent());
        realAn.setInternalId(realAn.getId().toString());

        return new AbstractMap.SimpleEntry<AssetNoteDto, AssetNoteDto>(wipAn, realAn);
    }

    private Entry<AssetNoteDto, AssetNoteDto> createAnAndWipAnNew()
    {
        final AssetNoteDto wipAn = new AssetNoteDto();
        wipAn.setId(newId());
        wipAn.setInternalId("");

        final AssetNoteDto realAn = new AssetNoteDto();
        realAn.setInternalId("");

        return new AbstractMap.SimpleEntry<AssetNoteDto, AssetNoteDto>(wipAn, realAn);
    }

    private CoCDto createCoC()
    {
        final CoCDto dummyCoC = new CoCDto();
        dummyCoC.setId(newId());
        dummyCoC.setParent(new LinkResource(newId()));
        dummyCoC.setDefect(new LinkResource(newId()));
        dummyCoC.setInternalId(dummyCoC.getId().toString());

        return dummyCoC;
    }

    private Entry<CoCDto, CoCDto> createCoCAndWipCoCExisting()
    {
        final CoCDto wipCoC = createCoC();

        final CoCDto realCoC = new CoCDto();
        realCoC.setId(wipCoC.getParent().getId());
        realCoC.setParent(wipCoC.getParent());
        realCoC.setDefect(new LinkResource(newId()));
        realCoC.setInternalId(realCoC.getId().toString());

        return new AbstractMap.SimpleEntry<CoCDto, CoCDto>(wipCoC, realCoC);
    }

    private Entry<CoCDto, CoCDto> createCoCAndWipCoCNew()
    {
        final CoCDto wipCoC = new CoCDto();
        wipCoC.setId(newId());
        wipCoC.setDefect(new LinkResource(newId()));
        wipCoC.setInternalId("");

        final CoCDto realCoC = new CoCDto();
        realCoC.setDefect(new LinkResource(newId()));
        realCoC.setInternalId("");

        return new AbstractMap.SimpleEntry<CoCDto, CoCDto>(wipCoC, realCoC);
    }

    private WorkItemDto createTask()
    {
        final WorkItemDto dummyTask = new WorkItemDto();
        dummyTask.setId(newId());
        dummyTask.setParent(new LinkResource(newId()));
        dummyTask.setCodicil(new LinkResource(newId()));
        dummyTask.setInternalId(dummyTask.getId().toString());

        return dummyTask;
    }

    private Entry<WorkItemDto, WorkItemLightDto> createTaskAndWipTaskExisting()
    {
        final WorkItemDto wipTask = createTask();

        final WorkItemLightDto realTask = new WorkItemLightDto();
        realTask.setId(wipTask.getParent().getId());
        realTask.setParent(wipTask.getParent());
        realTask.setCodicil(new LinkResource(newId()));
        realTask.setInternalId(realTask.getId().toString());

        return new AbstractMap.SimpleEntry<WorkItemDto, WorkItemLightDto>(wipTask, realTask);
    }

    private Entry<WorkItemDto, WorkItemLightDto> createTaskAndWipTaskNew()
    {
        final WorkItemDto wipTask = new WorkItemDto();
        wipTask.setId(newId());
        wipTask.setCodicil(new LinkResource(newId()));
        wipTask.setInternalId("");

        final WorkItemLightDto realTask = new WorkItemLightDto();
        realTask.setCodicil(new LinkResource(newId()));
        realTask.setInternalId("");

        return new AbstractMap.SimpleEntry<WorkItemDto, WorkItemLightDto>(wipTask, realTask);
    }

    private SurveyDto createSurvey()
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setId(newId());

        final SurveyDto dummySurvey = new SurveyDto();
        dummySurvey.setId(newId());
        dummySurvey.setServiceCatalogue(new LinkResource(newId()));
        dummySurvey.setScheduledService(service);
        dummySurvey.setInternalId(dummySurvey.getId().toString());

        return dummySurvey;
    }

    private Entry<SurveyDto, ScheduledServiceDto> createServiceAndWipSurveyExisting(final Integer cyclePeriodicity)
    {
        final SurveyDto wipSurvey = createSurvey();

        final ScheduledServiceDto realService = new ScheduledServiceDto();
        realService.setId(wipSurvey.getScheduledService().getId());
        realService.setProvisionalDates(PROVISIONAL_DATES);
        realService.setServiceCatalogueId(wipSurvey.getServiceCatalogue().getId());
        realService.setCyclePeriodicity(cyclePeriodicity);
        realService.setAsset(new LinkResource(ASSET_2_ID));
        realService.setInternalId(realService.getId().toString());

        wipSurvey.setScheduledService(realService);

        return new AbstractMap.SimpleEntry<SurveyDto, ScheduledServiceDto>(wipSurvey, realService);
    }

    private Entry<SurveyDto, ScheduledServiceDto> createServiceAndWipSurveyNew(final Integer cyclePeriodicity)
    {
        final SurveyDto wipSurvey = new SurveyDto();
        wipSurvey.setId(newId());
        wipSurvey.setServiceCatalogue(new LinkResource(newId()));
        wipSurvey.setInternalId("");

        final ScheduledServiceDto realService = new ScheduledServiceDto();
        realService.setProvisionalDates(PROVISIONAL_DATES);
        realService.setServiceCatalogueId(wipSurvey.getServiceCatalogue().getId());
        realService.setCyclePeriodicity(cyclePeriodicity);
        realService.setAsset(new LinkResource(ASSET_2_ID));
        realService.setInternalId("");

        return new AbstractMap.SimpleEntry<SurveyDto, ScheduledServiceDto>(wipSurvey, realService);
    }

    private ServiceCatalogueDto createServiceCatalogue()
    {
        final ServiceCatalogueDto serviceCatalogue = new ServiceCatalogueDto();
        final ProductCatalogueDto productCatalogue = new ProductCatalogueDto();

        productCatalogue.setId(newId());
        serviceCatalogue.setId(newId());
        serviceCatalogue.setCyclePeriodicity(newId().intValue());
        serviceCatalogue.setProductCatalogue(productCatalogue);
        serviceCatalogue.setSchedulingRegime(new LinkResource(newId()));

        return serviceCatalogue;
    }

    private DefectDto createDefect()
    {
        final DefectDto dummyDefect = new DefectDto();
        dummyDefect.setId(newId());
        dummyDefect.setParent(new LinkResource(newId()));
        dummyDefect.setJob(new LinkResource(JOB_1_ID));
        dummyDefect.setInternalId(dummyDefect.getId().toString());

        return dummyDefect;
    }

    private Entry<DefectDto, DefectDto> createDefectAndWipDefectExisting()
    {
        final DefectDto wipDefect = createDefect();

        final DefectDto realDefect = new DefectDto();
        realDefect.setId(wipDefect.getParent().getId());
        realDefect.setParent(wipDefect.getParent());
        realDefect.setInternalId(realDefect.getId().toString());

        return new AbstractMap.SimpleEntry<DefectDto, DefectDto>(wipDefect, realDefect);
    }

    private Entry<DefectDto, DefectDto> createDefectAndWipDefectNew()
    {
        final DefectDto wipDefect = new DefectDto();
        wipDefect.setId(newId());
        wipDefect.setJob(new LinkResource(JOB_1_ID));
        wipDefect.setInternalId("");

        final DefectDto realAn = new DefectDto();
        realAn.setInternalId("");

        return new AbstractMap.SimpleEntry<DefectDto, DefectDto>(wipDefect, realAn);
    }

    @Before
    public void setup()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        this.mapper = factory.getMapperFacade();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateActionableItemsFollowingReportTestExsistingAi() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<ActionableItemDto, ActionableItemDto> aiPair1 = createAiAndWipAiExisting();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair2 = createAiAndWipAiExisting();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair3 = createAiAndWipAiExisting();
        final Map<ActionableItemDto, ActionableItemDto> aiPairMap = makeMap(aiPair1, aiPair2, aiPair3);
        final Map<ActionableItemDto, ActionableItemDto> aiPairMapOriginal = copyMap(aiPairMap);

        when(this.wipActionableItemDao.getActionableItemsForJob(JOB_1_ID)).thenReturn(setToList(aiPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMap.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();
            final ActionableItemDto realAi = aiPair.getValue();

            when(this.wipDefectDao.getParentDefectId(wipAi.getDefect().getId())).thenReturn(realAi.getDefect().getId());
            when(this.actionableItemDao.createActionableItem(ASSET_2_ID, realAi)).thenReturn(realAi);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ACTIONABLE_ITEM), eq(AttachmentTargetType.ACTIONABLE_ITEM));

        this.reportSubmissionService.updateActionableItemsFollowingReport(JOB_1_ID);

        verify(this.wipActionableItemDao).getActionableItemsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMapOriginal.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();
            final ActionableItemDto realAi = aiPair.getValue();

            verify(this.wipDefectDao).getParentDefectId(wipAi.getDefect().getId());
            verify(this.actionableItemDao).createActionableItem(ASSET_2_ID, realAi);
        }

        verify(this.wipActionableItemDao, never()).updateActionableItem(any(ActionableItemDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateActionableItemsFollowingReportTestExsistingAiNullDefect() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<ActionableItemDto, ActionableItemDto> aiPair1 = createAiAndWipAiExisting();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair2 = createAiAndWipAiExisting();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair3 = createAiAndWipAiExisting();

        aiPair2.getKey().setDefect(null);
        aiPair2.getValue().setDefect(null);

        final Map<ActionableItemDto, ActionableItemDto> aiPairMap = makeMap(aiPair1, aiPair2, aiPair3);
        final Map<ActionableItemDto, ActionableItemDto> aiPairMapOriginal = copyMap(aiPairMap);

        when(this.wipActionableItemDao.getActionableItemsForJob(JOB_1_ID)).thenReturn(setToList(aiPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMap.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();
            final ActionableItemDto realAi = aiPair.getValue();

            if (wipAi.getDefect() != null)
            {
                when(this.wipDefectDao.getParentDefectId(wipAi.getDefect().getId())).thenReturn(realAi.getDefect().getId());
            }
            when(this.actionableItemDao.createActionableItem(ASSET_2_ID, realAi)).thenReturn(realAi);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ACTIONABLE_ITEM), eq(AttachmentTargetType.ACTIONABLE_ITEM));

        this.reportSubmissionService.updateActionableItemsFollowingReport(JOB_1_ID);

        verify(this.wipActionableItemDao).getActionableItemsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMapOriginal.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();
            final ActionableItemDto realAi = aiPair.getValue();

            if (wipAi.getDefect() != null)
            {
                verify(this.wipDefectDao).getParentDefectId(wipAi.getDefect().getId());
            }
            verify(this.actionableItemDao).createActionableItem(ASSET_2_ID, realAi);
        }

        verify(this.wipActionableItemDao, never()).updateActionableItem(any(ActionableItemDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateActionableItemsFollowingReportTestNewAi() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<ActionableItemDto, ActionableItemDto> aiPair1 = createAiAndWipAiNew();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair2 = createAiAndWipAiNew();
        final Entry<ActionableItemDto, ActionableItemDto> aiPair3 = createAiAndWipAiNew();
        final Map<ActionableItemDto, ActionableItemDto> aiPairMap = makeMap(aiPair1, aiPair2, aiPair3);
        final Map<ActionableItemDto, ActionableItemDto> aiPairMapOriginal = copyMap(aiPairMap);

        final ActionableItemDto dummyAi = createAi();

        when(this.wipActionableItemDao.getActionableItemsForJob(JOB_1_ID)).thenReturn(setToList(aiPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMap.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();
            final ActionableItemDto realAi = aiPair.getValue();

            when(this.wipDefectDao.getParentDefectId(wipAi.getDefect().getId())).thenReturn(realAi.getDefect().getId());
            when(this.actionableItemDao.createActionableItem(ASSET_2_ID, realAi)).thenReturn(dummyAi);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ACTIONABLE_ITEM), eq(AttachmentTargetType.ACTIONABLE_ITEM));

        this.reportSubmissionService.updateActionableItemsFollowingReport(JOB_1_ID);

        verify(this.wipActionableItemDao).getActionableItemsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMapOriginal.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();

            verify(this.wipDefectDao).getParentDefectId(wipAi.getDefect().getId());
        }

        for (final Entry<ActionableItemDto, ActionableItemDto> aiPair : aiPairMap.entrySet())
        {
            final ActionableItemDto wipAi = aiPair.getKey();

            assertNotNull(wipAi.getParent());
            assertEquals(dummyAi.getId(), wipAi.getParent().getId());
        }

        verify(this.actionableItemDao, times(THREE)).createActionableItem(eq(ASSET_2_ID), any(ActionableItemDto.class));
        verify(this.wipActionableItemDao, times(THREE)).updateActionableItem(any(ActionableItemDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateAssetNotesFollowingReportTestExsistingAn() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<AssetNoteDto, AssetNoteDto> anPair1 = createAnAndWipAnExisting();
        final Entry<AssetNoteDto, AssetNoteDto> anPair2 = createAnAndWipAnExisting();
        final Entry<AssetNoteDto, AssetNoteDto> anPair3 = createAnAndWipAnExisting();
        final Map<AssetNoteDto, AssetNoteDto> anPairMap = makeMap(anPair1, anPair2, anPair3);
        final Map<AssetNoteDto, AssetNoteDto> anPairMapOriginal = copyMap(anPairMap);

        when(this.wipAssetNoteDao.getAssetNotesForJob(JOB_1_ID)).thenReturn(setToList(anPairMap.keySet()));

        for (final Entry<AssetNoteDto, AssetNoteDto> anPair : anPairMap.entrySet())
        {
            final AssetNoteDto realAn = anPair.getValue();

            when(assetNoteDao.saveAssetNote(realAn)).thenReturn(realAn);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ASSET_NOTE), eq(AttachmentTargetType.ASSET_NOTE));

        this.reportSubmissionService.updateAssetNotesFollowingReport(JOB_1_ID);

        verify(this.wipAssetNoteDao).getAssetNotesForJob(JOB_1_ID);

        for (final Entry<AssetNoteDto, AssetNoteDto> anPair : anPairMapOriginal.entrySet())
        {
            final AssetNoteDto realAn = anPair.getValue();

            verify(this.assetNoteDao).saveAssetNote(realAn);
        }

        verify(this.wipAssetNoteDao, never()).saveAssetNote(any(AssetNoteDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateAssetNotesFollowingReportTestNewAn() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<AssetNoteDto, AssetNoteDto> anPair1 = createAnAndWipAnNew();
        final Entry<AssetNoteDto, AssetNoteDto> anPair2 = createAnAndWipAnNew();
        final Entry<AssetNoteDto, AssetNoteDto> anPair3 = createAnAndWipAnNew();
        final Map<AssetNoteDto, AssetNoteDto> anPairMap = makeMap(anPair1, anPair2, anPair3);

        final AssetNoteDto dummyAn = createAn();

        when(this.wipAssetNoteDao.getAssetNotesForJob(JOB_1_ID)).thenReturn(setToList(anPairMap.keySet()));

        for (final Entry<AssetNoteDto, AssetNoteDto> anPair : anPairMap.entrySet())
        {
            final AssetNoteDto realAn = anPair.getValue();

            when(assetNoteDao.saveAssetNote(realAn)).thenReturn(dummyAn);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ASSET_NOTE), eq(AttachmentTargetType.ASSET_NOTE));

        this.reportSubmissionService.updateAssetNotesFollowingReport(JOB_1_ID);

        verify(this.wipAssetNoteDao).getAssetNotesForJob(JOB_1_ID);

        for (final Entry<AssetNoteDto, AssetNoteDto> anPair : anPairMap.entrySet())
        {
            final AssetNoteDto wipAn = anPair.getKey();

            verify(this.assetNoteDao).saveAssetNote(wipAn);

            assertNotNull(wipAn.getParent());
        }

        verify(this.wipAssetNoteDao, times(THREE)).saveAssetNote(any(AssetNoteDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateCoCsFollowingReportTestExsistingCoC() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<CoCDto, CoCDto> cocPair1 = createCoCAndWipCoCExisting();
        final Entry<CoCDto, CoCDto> cocPair2 = createCoCAndWipCoCExisting();
        final Entry<CoCDto, CoCDto> cocPair3 = createCoCAndWipCoCExisting();
        final Map<CoCDto, CoCDto> cocPairMap = makeMap(cocPair1, cocPair2, cocPair3);
        final Map<CoCDto, CoCDto> cocPairMapOriginal = copyMap(cocPairMap);

        when(this.wipCoCDao.getCoCsForJob(JOB_1_ID)).thenReturn(setToList(cocPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMap.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();
            final CoCDto realCoC = cocPair.getValue();

            when(this.wipDefectDao.getParentDefectId(wipCoC.getDefect().getId())).thenReturn(realCoC.getDefect().getId());
            when(this.coCDao.saveCoC(realCoC, ASSET_2_ID)).thenReturn(realCoC);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_COC), eq(AttachmentTargetType.COC));

        this.reportSubmissionService.updateCoCsFollowingReport(JOB_1_ID);

        verify(this.wipCoCDao).getCoCsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMapOriginal.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();
            final CoCDto realCoC = cocPair.getValue();

            verify(this.wipDefectDao).getParentDefectId(wipCoC.getDefect().getId());
            verify(this.coCDao).saveCoC(realCoC, ASSET_2_ID);
        }

        verify(this.wipCoCDao, never()).updateCoC(any(CoCDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateCoCsFollowingReportTestExsistingCoCNullDefect() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<CoCDto, CoCDto> cocPair1 = createCoCAndWipCoCExisting();
        final Entry<CoCDto, CoCDto> cocPair2 = createCoCAndWipCoCExisting();
        final Entry<CoCDto, CoCDto> cocPair3 = createCoCAndWipCoCExisting();

        cocPair2.getKey().setDefect(null);
        cocPair2.getValue().setDefect(null);

        final Map<CoCDto, CoCDto> cocPairMap = makeMap(cocPair1, cocPair2, cocPair3);
        final Map<CoCDto, CoCDto> cocPairMapOriginal = copyMap(cocPairMap);

        when(this.wipCoCDao.getCoCsForJob(JOB_1_ID)).thenReturn(setToList(cocPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMap.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();
            final CoCDto realCoC = cocPair.getValue();

            if (wipCoC.getDefect() != null)
            {
                when(this.wipDefectDao.getParentDefectId(wipCoC.getDefect().getId())).thenReturn(realCoC.getDefect().getId());
            }
            when(this.coCDao.saveCoC(realCoC, ASSET_2_ID)).thenReturn(realCoC);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_COC), eq(AttachmentTargetType.COC));

        this.reportSubmissionService.updateCoCsFollowingReport(JOB_1_ID);

        verify(this.wipCoCDao).getCoCsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMapOriginal.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();
            final CoCDto realCoC = cocPair.getValue();

            if (wipCoC.getDefect() != null)
            {
                verify(this.wipDefectDao).getParentDefectId(wipCoC.getDefect().getId());
            }
            verify(this.coCDao).saveCoC(realCoC, ASSET_2_ID);
        }

        verify(this.wipCoCDao, never()).updateCoC(any(CoCDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateCoCsFollowingReportTestNewCoC() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<CoCDto, CoCDto> cocPair1 = createCoCAndWipCoCNew();
        final Entry<CoCDto, CoCDto> cocPair2 = createCoCAndWipCoCNew();
        final Entry<CoCDto, CoCDto> cocPair3 = createCoCAndWipCoCNew();
        final Map<CoCDto, CoCDto> cocPairMap = makeMap(cocPair1, cocPair2, cocPair3);
        final Map<CoCDto, CoCDto> cocPairMapOriginal = copyMap(cocPairMap);

        final CoCDto dummyCoC = createCoC();

        when(this.wipCoCDao.getCoCsForJob(JOB_1_ID)).thenReturn(setToList(cocPairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMap.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();
            final CoCDto realCoC = cocPair.getValue();

            when(this.wipDefectDao.getParentDefectId(wipCoC.getDefect().getId())).thenReturn(realCoC.getDefect().getId());
            when(this.coCDao.saveCoC(realCoC, ASSET_2_ID)).thenReturn(dummyCoC);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_ACTIONABLE_ITEM), eq(AttachmentTargetType.ACTIONABLE_ITEM));

        this.reportSubmissionService.updateCoCsFollowingReport(JOB_1_ID);

        verify(this.wipCoCDao).getCoCsForJob(JOB_1_ID);
        verify(this.jobDao).getAssetIdForJob(JOB_1_ID);

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMapOriginal.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();

            verify(this.wipDefectDao).getParentDefectId(wipCoC.getDefect().getId());
        }

        for (final Entry<CoCDto, CoCDto> cocPair : cocPairMap.entrySet())
        {
            final CoCDto wipCoC = cocPair.getKey();

            assertNotNull(wipCoC.getParent());
            assertEquals(dummyCoC.getId(), wipCoC.getParent().getId());
        }

        verify(this.coCDao, times(THREE)).saveCoC(any(CoCDto.class), eq(ASSET_2_ID));
        verify(this.wipCoCDao, times(THREE)).updateCoC(any(CoCDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateTasksFollowingReportTestExsistingTaskWithNoRelationships() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<WorkItemDto, WorkItemLightDto> taskPair1 = createTaskAndWipTaskExisting();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair2 = createTaskAndWipTaskExisting();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair3 = createTaskAndWipTaskExisting();
        final Map<WorkItemDto, WorkItemLightDto> taskPairMap = makeMap(taskPair1, taskPair2, taskPair3);
        final Map<WorkItemDto, WorkItemLightDto> taskPairMapOriginal = copyMap(taskPairMap);

        when(this.wipWorkItemDao.getTasksForJob(JOB_1_ID)).thenReturn(setToList(taskPairMap.keySet()));

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMap.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            when(this.wipCodicilDao.getParentId(wipTask.getCodicil().getId())).thenReturn(realTask.getCodicil().getId());
            when(this.reportSubmissionHelper.mapWipTaskToTask(wipTask, realTask.getCodicil().getId())).thenReturn(realTask);
            when(this.workItemDao.createOrUpdateTask(realTask)).thenReturn(realTask);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_TASK), eq(AttachmentTargetType.TASK));

        this.reportSubmissionService.updateTasksFollowingReport(JOB_1_ID);

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMapOriginal.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            verify(this.wipCodicilDao).getParentId(wipTask.getCodicil().getId());
            verify(this.reportSubmissionHelper).mapWipTaskToTask(wipTask, realTask.getCodicil().getId());
            verify(this.workItemDao).createOrUpdateTask(realTask);
        }

        verify(this.wipWorkItemDao, never()).updateTask(any(WorkItemLightDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateTasksFollowingReportTestExsistingTaskWithRelationships() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<WorkItemDto, WorkItemLightDto> taskPair1 = createTaskAndWipTaskExisting();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair2 = createTaskAndWipTaskExisting();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair3 = createTaskAndWipTaskExisting();

        taskPair1.getKey().setConditionalParent(createTask());
        taskPair2.getKey().setWorkItem(createTask());

        final Map<WorkItemDto, WorkItemLightDto> taskPairMap = makeMap(taskPair1, taskPair2, taskPair3);
        final Map<WorkItemDto, WorkItemLightDto> taskPairMapOriginal = copyMap(taskPairMap);

        when(this.wipWorkItemDao.getTasksForJob(JOB_1_ID)).thenReturn(setToList(taskPairMap.keySet()));

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMap.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            when(this.wipCodicilDao.getParentId(wipTask.getCodicil().getId())).thenReturn(realTask.getCodicil().getId());
            when(this.reportSubmissionHelper.mapWipTaskToTask(wipTask, realTask.getCodicil().getId())).thenReturn(realTask);
            when(this.workItemDao.createOrUpdateTask(realTask)).thenReturn(realTask);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_TASK), eq(AttachmentTargetType.TASK));

        this.reportSubmissionService.updateTasksFollowingReport(JOB_1_ID);

        int times;

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMapOriginal.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();
            times = 1;

            if (wipTask.getConditionalParent() != null || wipTask.getWorkItem() != null)
            {
                times = 2;
            }

            verify(this.wipCodicilDao, times(times)).getParentId(wipTask.getCodicil().getId());
            verify(this.reportSubmissionHelper, times(times)).mapWipTaskToTask(wipTask, realTask.getCodicil().getId());
            verify(this.workItemDao, times(times)).createOrUpdateTask(realTask);
        }

        verify(this.wipWorkItemDao, never()).updateTask(any(WorkItemLightDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateTasksFollowingReportTestNewTaskWithNoRelationships() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<WorkItemDto, WorkItemLightDto> taskPair1 = createTaskAndWipTaskNew();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair2 = createTaskAndWipTaskNew();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair3 = createTaskAndWipTaskNew();
        final Map<WorkItemDto, WorkItemLightDto> taskPairMap = makeMap(taskPair1, taskPair2, taskPair3);
        final Map<WorkItemDto, WorkItemLightDto> taskPairMapOriginal = copyMap(taskPairMap);

        when(this.wipWorkItemDao.getTasksForJob(JOB_1_ID)).thenReturn(setToList(taskPairMap.keySet()));

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMap.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            when(this.wipCodicilDao.getParentId(wipTask.getCodicil().getId())).thenReturn(realTask.getCodicil().getId());
            when(this.reportSubmissionHelper.mapWipTaskToTask(wipTask, realTask.getCodicil().getId())).thenReturn(realTask);
            when(this.workItemDao.createOrUpdateTask(realTask)).thenReturn(realTask);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_TASK), eq(AttachmentTargetType.TASK));

        this.reportSubmissionService.updateTasksFollowingReport(JOB_1_ID);

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMapOriginal.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            verify(this.wipCodicilDao).getParentId(wipTask.getCodicil().getId());
            verify(this.reportSubmissionHelper).mapWipTaskToTask(wipTask, realTask.getCodicil().getId());
        }

        verify(this.workItemDao, times(THREE)).createOrUpdateTask(any(WorkItemLightDto.class));
        verify(this.wipWorkItemDao, times(THREE)).updateTask(any(WorkItemLightDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateTasksFollowingReportTestNewTaskWithNullCodicil() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<WorkItemDto, WorkItemLightDto> taskPair1 = createTaskAndWipTaskNew();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair2 = createTaskAndWipTaskNew();
        final Entry<WorkItemDto, WorkItemLightDto> taskPair3 = createTaskAndWipTaskNew();

        taskPair2.getKey().setCodicil(null);
        taskPair2.getValue().setCodicil(null);

        final Map<WorkItemDto, WorkItemLightDto> taskPairMap = makeMap(taskPair1, taskPair2, taskPair3);
        final Map<WorkItemDto, WorkItemLightDto> taskPairMapOriginal = copyMap(taskPairMap);

        when(this.wipWorkItemDao.getTasksForJob(JOB_1_ID)).thenReturn(setToList(taskPairMap.keySet()));

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMap.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            if (wipTask.getCodicil() == null)
            {
                when(this.reportSubmissionHelper.mapWipTaskToTask(wipTask, null)).thenReturn(realTask);
            }
            else
            {
                when(this.wipCodicilDao.getParentId(wipTask.getCodicil().getId())).thenReturn(realTask.getCodicil().getId());
                when(this.reportSubmissionHelper.mapWipTaskToTask(wipTask, realTask.getCodicil().getId())).thenReturn(realTask);
            }
            when(this.workItemDao.createOrUpdateTask(realTask)).thenReturn(realTask);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_TASK), eq(AttachmentTargetType.TASK));

        this.reportSubmissionService.updateTasksFollowingReport(JOB_1_ID);

        for (final Entry<WorkItemDto, WorkItemLightDto> taskPair : taskPairMapOriginal.entrySet())
        {
            final WorkItemDto wipTask = taskPair.getKey();
            final WorkItemLightDto realTask = taskPair.getValue();

            if (wipTask.getCodicil() == null)
            {
                verify(this.reportSubmissionHelper).mapWipTaskToTask(wipTask, null);
            }
            else
            {
                verify(this.wipCodicilDao).getParentId(wipTask.getCodicil().getId());
                verify(this.reportSubmissionHelper).mapWipTaskToTask(wipTask, realTask.getCodicil().getId());
            }
        }

        verify(this.workItemDao, times(THREE)).createOrUpdateTask(any(WorkItemLightDto.class));
        verify(this.wipWorkItemDao, times(THREE)).updateTask(any(WorkItemLightDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateSurveysFollowingReportTestExsistingSurvey() throws BadPageRequestException, RecordNotFoundException
    {
        final ServiceCatalogueDto serviceCatalogue = createServiceCatalogue();

        final Entry<SurveyDto, ScheduledServiceDto> servicePair1 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair2 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair3 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());
        final Map<SurveyDto, ScheduledServiceDto> servicePairMap = makeMap(servicePair1, servicePair2, servicePair3);
        final Map<SurveyDto, ScheduledServiceDto> servicePairMapOriginal = copyMap(servicePairMap);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(setToList(servicePairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMap.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
            when(this.serviceDao.saveService(realService)).thenReturn(realService);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.SURVEY), eq(AttachmentTargetType.SERVICE));

        this.reportSubmissionService.updateSurveysFollowingReport(JOB_1_ID, PROVISIONAL_DATES);

        verify(this.surveyDao).getSurveys(JOB_1_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMapOriginal.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
        }

        verify(this.serviceDao, never()).getServiceCatalogue(any(Long.class));
        verify(this.productDao, never()).isProductCatalogueSelectedForAsset(any(Long.class), any(Long.class), any(Long.class));
        verify(this.productDao, never()).createProduct(any(Long.class), any(Long.class), any(Long.class));
        verify(this.surveyDao, never()).updateSurvey(any(SurveyDto.class));
    }

    @Test
    public void updateSurveysFollowingReportTestExsistingSurveyServiceCatalogueMissmatch() throws BadPageRequestException, RecordNotFoundException
    {
        exception.expect(MastSystemException.class);
        exception.expectMessage(String.format(ExceptionMessagesUtils.ATTEMPTING_TO_UPDATE_IMMUTABLE_FIELD, "service catalogue"));

        final ServiceCatalogueDto serviceCatalogue = createServiceCatalogue();

        final Entry<SurveyDto, ScheduledServiceDto> servicePair1 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair2 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair3 = createServiceAndWipSurveyExisting(serviceCatalogue.getCyclePeriodicity());

        servicePair3.getValue().setServiceCatalogueId(newId());

        final Map<SurveyDto, ScheduledServiceDto> servicePairMap = makeMap(servicePair1, servicePair2, servicePair3);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(setToList(servicePairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMap.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
            when(this.serviceDao.saveService(realService)).thenReturn(realService);
        }

        this.reportSubmissionService.updateSurveysFollowingReport(JOB_1_ID, PROVISIONAL_DATES);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateSurveysFollowingReportTestNewSurvey() throws BadPageRequestException, RecordNotFoundException
    {
        final ServiceCatalogueDto serviceCatalogue = createServiceCatalogue();

        final Entry<SurveyDto, ScheduledServiceDto> servicePair1 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair2 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair3 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Map<SurveyDto, ScheduledServiceDto> servicePairMap = makeMap(servicePair1, servicePair2, servicePair3);
        final Map<SurveyDto, ScheduledServiceDto> servicePairMapOriginal = copyMap(servicePairMap);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(setToList(servicePairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_2_ID, serviceCatalogue.getProductCatalogue().getId(),
                serviceCatalogue.getSchedulingRegime().getId())).thenReturn(true);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMap.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
            when(this.serviceDao.getServiceCatalogue(realService.getServiceCatalogueId())).thenReturn(serviceCatalogue);
            when(this.serviceDao.saveService(realService)).thenReturn(realService);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.SURVEY), eq(AttachmentTargetType.SERVICE));

        this.reportSubmissionService.updateSurveysFollowingReport(JOB_1_ID, PROVISIONAL_DATES);

        verify(this.surveyDao).getSurveys(JOB_1_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMapOriginal.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            verify(this.reportSubmissionHelper).serviceWipToNonWipMap(wipSurvey);
            verify(this.serviceDao).getServiceCatalogue(realService.getServiceCatalogueId());
        }

        final ScheduledServiceDto realService = servicePairMap.values().iterator().next();

        verify(this.serviceDao, times(THREE)).saveService(realService);
        verify(this.productDao, times(THREE)).isProductCatalogueSelectedForAsset(ASSET_2_ID, serviceCatalogue.getProductCatalogue().getId(),
                serviceCatalogue.getSchedulingRegime().getId());
        verify(this.productDao, never()).createProduct(any(Long.class), any(Long.class), any(Long.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateSurveysFollowingReportTestNewSurveyNewProductNeeded() throws BadPageRequestException, RecordNotFoundException
    {
        final ServiceCatalogueDto serviceCatalogue = createServiceCatalogue();
        final ServiceCatalogueDto serviceCatalogueWithExistingProduct = createServiceCatalogue();

        serviceCatalogueWithExistingProduct.setSchedulingRegime(null);
        serviceCatalogueWithExistingProduct.setProductCatalogue(null);

        final Entry<SurveyDto, ScheduledServiceDto> servicePair1 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair2 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair3 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Map<SurveyDto, ScheduledServiceDto> servicePairMap = makeMap(servicePair1, servicePair2, servicePair3);
        final Map<SurveyDto, ScheduledServiceDto> servicePairMapOriginal = copyMap(servicePairMap);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(setToList(servicePairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_2_ID, serviceCatalogue.getProductCatalogue().getId(),
                serviceCatalogue.getSchedulingRegime().getId())).thenReturn(false);
        when(this.productDao.isProductCatalogueSelectedForAsset(ASSET_2_ID, null, null)).thenReturn(true);

        Boolean firstLoop = true;
        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMap.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
            when(this.serviceDao.getServiceCatalogue(realService.getServiceCatalogueId()))
                    .thenReturn(firstLoop ? serviceCatalogue : serviceCatalogueWithExistingProduct);
            when(this.serviceDao.saveService(realService)).thenReturn(realService);
            firstLoop = false;
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.SURVEY), eq(AttachmentTargetType.SERVICE));

        this.reportSubmissionService.updateSurveysFollowingReport(JOB_1_ID, PROVISIONAL_DATES);

        verify(this.surveyDao).getSurveys(JOB_1_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMapOriginal.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            verify(this.reportSubmissionHelper).serviceWipToNonWipMap(wipSurvey);
            verify(this.serviceDao).getServiceCatalogue(realService.getServiceCatalogueId());
        }

        final ScheduledServiceDto realService = servicePairMap.values().iterator().next();

        verify(this.serviceDao, times(THREE)).saveService(realService);
        verify(this.productDao).isProductCatalogueSelectedForAsset(ASSET_2_ID, serviceCatalogue.getProductCatalogue().getId(),
                serviceCatalogue.getSchedulingRegime().getId());
        verify(this.productDao, times(TWO)).isProductCatalogueSelectedForAsset(ASSET_2_ID, null, null);
        verify(this.productDao).createProduct(ASSET_2_ID,
                serviceCatalogue.getProductCatalogue().getId(), serviceCatalogue.getSchedulingRegime().getId());
    }

    @Test
    public void updateSurveysFollowingReportTestNewSurveyNullServiceCatalogue() throws BadPageRequestException, RecordNotFoundException
    {
        exception.expect(MastSystemException.class);
        exception.expectMessage(String.format(ExceptionMessagesUtils.CANNOT_BE_NULL_ERROR_MSG, "Service catalogue"));

        final ServiceCatalogueDto serviceCatalogue = createServiceCatalogue();

        final Entry<SurveyDto, ScheduledServiceDto> servicePair1 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair2 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Entry<SurveyDto, ScheduledServiceDto> servicePair3 = createServiceAndWipSurveyNew(serviceCatalogue.getCyclePeriodicity());
        final Map<SurveyDto, ScheduledServiceDto> servicePairMap = makeMap(servicePair1, servicePair2, servicePair3);

        when(this.surveyDao.getSurveys(JOB_1_ID)).thenReturn(setToList(servicePairMap.keySet()));
        when(this.jobDao.getAssetIdForJob(JOB_1_ID)).thenReturn(ASSET_2_ID);

        for (final Entry<SurveyDto, ScheduledServiceDto> servicePair : servicePairMap.entrySet())
        {
            final SurveyDto wipSurvey = servicePair.getKey();
            final ScheduledServiceDto realService = servicePair.getValue();

            when(this.reportSubmissionHelper.serviceWipToNonWipMap(wipSurvey)).thenReturn(realService);
            when(this.serviceDao.getServiceCatalogue(realService.getServiceCatalogueId())).thenReturn(null);
        }

        this.reportSubmissionService.updateSurveysFollowingReport(JOB_1_ID, PROVISIONAL_DATES);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateDefectsFollowingReportTestExsistingDefect() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<DefectDto, DefectDto> defectPair1 = createDefectAndWipDefectExisting();
        final Entry<DefectDto, DefectDto> defectPair2 = createDefectAndWipDefectExisting();
        final Entry<DefectDto, DefectDto> defectPair3 = createDefectAndWipDefectExisting();
        final Map<DefectDto, DefectDto> defectPairMap = makeMap(defectPair1, defectPair2, defectPair3);
        final Map<DefectDto, DefectDto> defectPairMapOriginal = copyMap(defectPairMap);

        when(this.wipDefectDao.getDefectsForJob(JOB_1_ID)).thenReturn(setToList(defectPairMap.keySet()));

        for (final Entry<DefectDto, DefectDto> defectPair : defectPairMap.entrySet())
        {
            final DefectDto realDefect = defectPair.getValue();

            when(this.defectDao.getDefect(realDefect.getId())).thenReturn(realDefect);
            when(this.defectDao.createDefect(realDefect)).thenReturn(realDefect);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_DEFECT), eq(AttachmentTargetType.DEFECT));

        this.reportSubmissionService.updateDefectsFollowingReport(JOB_1_ID);

        verify(this.wipDefectDao).getDefectsForJob(JOB_1_ID);

        for (final Entry<DefectDto, DefectDto> defectPair : defectPairMapOriginal.entrySet())
        {
            final DefectDto realDefect = defectPair.getValue();

            verify(this.defectDao).getDefect(realDefect.getId());
            verify(this.defectDao).createDefect(realDefect);
        }

        verify(this.wipDefectDao, never()).updateDefect(any(DefectDto.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void updateDefectsFollowingReportTestNewDefect() throws BadPageRequestException, RecordNotFoundException
    {
        final Entry<DefectDto, DefectDto> defectPair1 = createDefectAndWipDefectNew();
        final Entry<DefectDto, DefectDto> defectPair2 = createDefectAndWipDefectNew();
        final Entry<DefectDto, DefectDto> defectPair3 = createDefectAndWipDefectNew();
        final Map<DefectDto, DefectDto> defectPairMap = makeMap(defectPair1, defectPair2, defectPair3);
        final Map<DefectDto, DefectDto> defectPairMapOriginal = copyMap(defectPairMap);

        when(this.wipDefectDao.getDefectsForJob(JOB_1_ID)).thenReturn(setToList(defectPairMap.keySet()));

        for (final Entry<DefectDto, DefectDto> defectPair : defectPairMap.entrySet())
        {
            final DefectDto wipDefect = defectPair.getKey();
            final DefectDto realDefect = defectPair.getValue();

            when(this.reportSubmissionHelper.mapNewDefect(wipDefect)).thenReturn(realDefect);
            when(this.defectDao.createDefect(realDefect)).thenReturn(realDefect);
        }

        doNothing().when(this.attachmentService).migrateAttachmentsFromWipEntities(any(new HashMap<Long, Long>().getClass()),
                eq(AttachmentTargetType.WIP_DEFECT), eq(AttachmentTargetType.DEFECT));

        this.reportSubmissionService.updateDefectsFollowingReport(JOB_1_ID);

        verify(this.wipDefectDao).getDefectsForJob(JOB_1_ID);

        final DefectDto realDefect = defectPairMapOriginal.values().iterator().next();
        verify(this.defectDao, times(THREE)).createDefect(realDefect);
        verify(this.wipDefectDao, times(THREE)).updateDefect(any(DefectDto.class));
        verify(this.defectDao, never()).getDefect(any(Long.class));
    }
}
