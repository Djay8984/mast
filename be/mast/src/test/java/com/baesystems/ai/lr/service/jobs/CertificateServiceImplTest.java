package com.baesystems.ai.lr.service.jobs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.baesystems.ai.lr.dao.jobs.CertificateDao;
import com.baesystems.ai.lr.dao.jobs.JobDao;
import com.baesystems.ai.lr.dao.jobs.WIPCertificateDao;
import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.jobs.CertificateDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.MastBusinessException;
import com.baesystems.ai.lr.utils.DateHelper;
import com.baesystems.ai.lr.utils.DateHelperImpl;
import com.baesystems.ai.lr.utils.PageResourceImplBasedTest;

public class CertificateServiceImplTest extends PageResourceImplBasedTest
{
    private static final Long CERTIFICATE_1_ID = 1L;
    private static final Long JOB_1_ID = 1L;
    private static final Long ONE = 1L;
    private static final Integer CYCLE_PERIODICITY = 60;
    private static final Date ISSUE_DATE = new Date();

    private CertificateDto certificate;
    private JobDto job;

    @Mock
    private WIPCertificateDao wipCertificateDao;

    @Mock
    private CertificateDao certificateDao;

    @Mock
    private JobDao jobDao;

    @Mock
    private final DateHelper dateHelper = new DateHelperImpl();

    @InjectMocks
    private final CertificateService certificateService = new CertificateServiceImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws BadPageRequestException
    {
        job = new JobDto();
        certificate = new CertificateDto();
        certificate.setId(CERTIFICATE_1_ID);
        certificate.setCertificateNumber(JOB_1_ID.toString());
        certificate.setCertificateStatus(new LinkResource(ONE));
        certificate.setCertificateType(new LinkResource(ONE));
        certificate.setCertificateAction(new LinkResource(ONE));
        certificate.setIssueDate(ISSUE_DATE);
        certificate.setOffice(new LinkResource(ONE));
        certificate.setEmployee(new LinkResource(ONE));
        certificate.setJob(new LinkResource(ONE));
    }

    private Date getExpriryDate(final Date issueDate)
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(issueDate);
        calendar.add(Calendar.MONTH, CYCLE_PERIODICITY);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    @Test
    public void testCreateCertificateCalculateExpriyDate() throws MastBusinessException
    {
        certificate.setId(null);

        when(this.certificateDao.getCyclePeriodicityOfCertificateType(ONE)).thenReturn(CYCLE_PERIODICITY);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(job);
        when(this.wipCertificateDao.saveCertificate(certificate)).thenReturn(certificate);

        final CertificateDto output = this.certificateService.createCertificate(certificate, JOB_1_ID);

        assertNotNull(output);
        assertEquals(ISSUE_DATE, output.getIssueDate());
        assertEquals(getExpriryDate(ISSUE_DATE), output.getExpiryDate());
    }

    @Test
    public void testCreateCertificateCalculateCertificateNumber() throws MastBusinessException
    {
        certificate.setId(null);
        certificate.setCertificateNumber(null);

        when(this.certificateDao.getCyclePeriodicityOfCertificateType(ONE)).thenReturn(CYCLE_PERIODICITY);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(job);
        when(this.wipCertificateDao.saveCertificate(certificate)).thenReturn(certificate);

        final CertificateDto output = this.certificateService.createCertificate(certificate, JOB_1_ID);

        assertNotNull(output);
        assertEquals(JOB_1_ID.toString(), output.getCertificateNumber());
    }

    @Test
    public void testCreateCertificateNullDate() throws MastBusinessException
    {
        certificate.setId(null);
        certificate.setIssueDate(null);

        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(job);
        when(this.wipCertificateDao.saveCertificate(certificate)).thenReturn(certificate);

        final CertificateDto output = this.certificateService.createCertificate(certificate, JOB_1_ID);

        assertNotNull(output);
        assertNull(output.getIssueDate());
        assertNull(output.getExpiryDate());
    }

    @Test
    public void testUpdateCertificateCalculateExpriyDate() throws MastBusinessException
    {
        when(this.certificateDao.getCyclePeriodicityOfCertificateType(ONE)).thenReturn(CYCLE_PERIODICITY);
        when(this.jobDao.getJob(JOB_1_ID)).thenReturn(job);
        when(this.wipCertificateDao.saveCertificate(certificate)).thenReturn(certificate);

        final CertificateDto output = this.certificateService.updateCertificate(certificate, JOB_1_ID, CERTIFICATE_1_ID);

        assertNotNull(output);
        assertEquals(ISSUE_DATE, output.getIssueDate());
        assertEquals(getExpriryDate(ISSUE_DATE), output.getExpiryDate());
    }
}
