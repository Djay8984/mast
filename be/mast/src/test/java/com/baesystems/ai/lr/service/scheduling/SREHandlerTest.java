package com.baesystems.ai.lr.service.scheduling;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.baesystems.ai.lr.dto.scheduling.SRECertifiedSuccessResponseDto;
import com.baesystems.ai.lr.dto.scheduling.SREUncertifiedSuccessResponseDto;
import com.baesystems.ai.lr.utils.DateHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class SREHandlerTest
{
    private static final String SCHEDULING_SERVICE_ENDPOINT = "http://localhost:9099/scheduling/";
    private static final String NEXT_SERVICES_PATH = "nextServices?creditedServiceId={creditedServiceId}&assetHarmonisationDate={assetHarmonisationDate}&completionDate={completionDate}";
    private static final String MANUAL_SERVICE_PATH = "manualEntryService?creditedServiceId={creditedServiceId}&assetHarmonisationDate={assetHarmonisationDate}";

    private static final Long CREDITED_SERVICE_ID = 123L;
    private static final Date COMPLETION_DATE = new Date();
    private static final Date ASSET_HARMONISATION_DATE = new Date();

    private static ObjectMapper objectMapper;

    private static URI targetUrlNextServices;
    private static URI targetUrlManualEntryService;

    private static SRECertifiedSuccessResponseDto certifiedSuccessResponse;
    private static SREUncertifiedSuccessResponseDto uncertifiedSuccessResponse;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final SREHandler sreHandler = new SREHandler();

    @BeforeClass
    public static void startUp()
    {
        System.setProperty("sre.service.url", SCHEDULING_SERVICE_ENDPOINT);
        objectMapper = new ObjectMapper();

        targetUrlNextServices = constructNextServicesUri();
        targetUrlManualEntryService = constructManualEntryServiceUri();
    }

    @Before
    public void setUp() throws IOException
    {
        certifiedSuccessResponse = objectMapper.readValue(
                this.getClass().getResourceAsStream("/scheduling/CreditedResponse-001.json"), SRECertifiedSuccessResponseDto.class);
        uncertifiedSuccessResponse = objectMapper.readValue(
                this.getClass().getResourceAsStream("/scheduling/UncreditedResponse-002.json"), SREUncertifiedSuccessResponseDto.class);
    }

    private static URI constructNextServicesUri()
    {
        final SimpleDateFormat dateFormat = DateHelper.mastFormatter();
        final Map<String, String> uriVariables = new HashMap<String, String>();
        uriVariables.put("creditedServiceId", CREDITED_SERVICE_ID.toString());
        uriVariables.put("completionDate", dateFormat.format(COMPLETION_DATE));
        uriVariables.put("assetHarmonisationDate", dateFormat.format(ASSET_HARMONISATION_DATE));

        final URI uri = UriComponentsBuilder.fromHttpUrl(SCHEDULING_SERVICE_ENDPOINT + NEXT_SERVICES_PATH)
                .buildAndExpand(uriVariables).toUri();

        return uri;
    }

    private static URI constructManualEntryServiceUri()
    {
        final SimpleDateFormat dateFormat = DateHelper.mastFormatter();
        final Map<String, String> uriVariables = new HashMap<String, String>();
        uriVariables.put("creditedServiceId", CREDITED_SERVICE_ID.toString());
        uriVariables.put("assetHarmonisationDate", dateFormat.format(ASSET_HARMONISATION_DATE));

        final URI uri = UriComponentsBuilder.fromHttpUrl(SCHEDULING_SERVICE_ENDPOINT + MANUAL_SERVICE_PATH)
                .buildAndExpand(uriVariables).toUri();

        return uri;
    }

    @Test
    public void getNextServices()
    {
        final ResponseEntity<SRECertifiedSuccessResponseDto> baseResponseDto = new ResponseEntity<>(certifiedSuccessResponse, HttpStatus.OK);

        when(this.restTemplate.getForEntity(targetUrlNextServices, SRECertifiedSuccessResponseDto.class)).thenReturn(baseResponseDto);

        final SRECertifiedSuccessResponseDto response = sreHandler.getNextServices(CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE, COMPLETION_DATE);

        assertEquals(certifiedSuccessResponse.getServices(), response.getServices());
        assertEquals(certifiedSuccessResponse.getHullIndicator(), response.getHullIndicator());
    }

    @Test
    public void getManualEntryService()
    {
        final ResponseEntity<SREUncertifiedSuccessResponseDto> baseResponseDto = new ResponseEntity<>(uncertifiedSuccessResponse, HttpStatus.OK);

        when(this.restTemplate.getForEntity(targetUrlManualEntryService, SREUncertifiedSuccessResponseDto.class)).thenReturn(baseResponseDto);

        final SREUncertifiedSuccessResponseDto response = sreHandler.getManualEntryService(CREDITED_SERVICE_ID, ASSET_HARMONISATION_DATE);

        assertEquals(uncertifiedSuccessResponse.getService(), response.getService());
        assertEquals(uncertifiedSuccessResponse.getHullIndicator(), response.getHullIndicator());
    }
}
