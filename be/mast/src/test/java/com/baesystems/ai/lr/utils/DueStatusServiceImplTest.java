package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.cases.CaseMilestoneDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.enums.DueStatusType;

@RunWith(MockitoJUnitRunner.class)
public class DueStatusServiceImplTest
{
    private Date futureDate;
    private Date pastDate;

    @Mock
    private DateHelper dateHelper;

    @InjectMocks
    private final DueStatusService dueStatusService = new DueStatusServiceImpl();

    @Before
    public void setup()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        this.futureDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        this.pastDate = calendar.getTime();
    }

    @Test
    public void calculateDueStatusOverdueTest()
    {
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final CaseMilestoneDto milestone = new CaseMilestoneDto();
        milestone.setDueDate(this.pastDate);

        this.dueStatusService.calculateDueStatus(milestone);

        assertEquals(DueStatusType.OVERDUE.getValue(), milestone.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusNotDueTest()
    {
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);

        final CaseMilestoneDto milestone = new CaseMilestoneDto();
        milestone.setDueDate(this.futureDate);

        this.dueStatusService.calculateDueStatus(milestone);

        assertEquals(DueStatusType.NOT_DUE.getValue(), milestone.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusOverdueTestList()
    {
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final CaseMilestoneDto milestone = new CaseMilestoneDto();
        milestone.setDueDate(this.pastDate);

        this.dueStatusService.calculateDueStatuses(Collections.singletonList(milestone));

        assertEquals(DueStatusType.OVERDUE.getValue(), milestone.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusNotDueTestList()
    {
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);

        final CaseMilestoneDto milestone = new CaseMilestoneDto();
        milestone.setDueDate(this.futureDate);

        this.dueStatusService.calculateDueStatuses(Collections.singletonList(milestone));

        assertEquals(DueStatusType.NOT_DUE.getValue(), milestone.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusOverdueServiceTestList()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.pastDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.pastDate);
        service.setUpperRangeDate(this.pastDate);

        this.dueStatusService.calculateServiceDueStatuses(Collections.singletonList(service));

        assertEquals(DueStatusType.OVERDUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusNotDueServiceTestList()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.futureDate), any(Date.class))).thenReturn(false);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.futureDate);
        service.setUpperRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatuses(Collections.singletonList(service));

        assertEquals(DueStatusType.NOT_DUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusDueServiceTestList()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.futureDate), any(Date.class))).thenReturn(false);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.pastDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.pastDate);
        service.setUpperRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatuses(Collections.singletonList(service));

        assertEquals(DueStatusType.DUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusOverdueServiceTest()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.pastDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.pastDate);
        service.setUpperRangeDate(this.pastDate);

        this.dueStatusService.calculateServiceDueStatus(service);

        assertEquals(DueStatusType.OVERDUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusNotDueServiceTest()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.futureDate), any(Date.class))).thenReturn(false);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.futureDate);
        service.setUpperRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatus(service);

        assertEquals(DueStatusType.NOT_DUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusDueServiceTest()
    {
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.futureDate), any(Date.class))).thenReturn(false);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.futureDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrBeforeStrict(eq(this.pastDate), any(Date.class))).thenReturn(true);
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.pastDate);
        service.setUpperRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatus(service);

        assertEquals(DueStatusType.DUE.getValue(), service.getDueStatus().getId());
    }

    @Test
    public void calculateDueStatusNullTest()
    {
        this.dueStatusService.calculateDueStatus(null);
    }

    @Test
    public void calculateDueStatusNullServiceServiceTest()
    {
        this.dueStatusService.calculateServiceDueStatus(null);
    }

    @Test
    public void calculateDueStatusNullTestList()
    {
        this.dueStatusService.calculateDueStatuses(null);
    }

    @Test
    public void calculateDueStatusNullServiceServiceTestList()
    {
        this.dueStatusService.calculateServiceDueStatuses(null);
    }

    @Test
    public void calculateDueStatusNullDateTest()
    {
        when(this.dateHelper.isTodayOrAfterStrict(eq(this.pastDate), any(Date.class))).thenReturn(false);

        final CaseMilestoneDto milestone = new CaseMilestoneDto();

        this.dueStatusService.calculateDueStatus(milestone);

        assertNull(milestone.getDueStatus());
    }

    @Test
    public void calculateDueStatusNullLowerServiceTest()
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setUpperRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatus(service);

        assertNull(service.getDueStatus());
    }

    @Test
    public void calculateDueStatusNullUpperServiceTest()
    {
        final ScheduledServiceDto service = new ScheduledServiceDto();
        service.setLowerRangeDate(this.futureDate);

        this.dueStatusService.calculateServiceDueStatus(service);

        assertNull(service.getDueStatus());
    }
}
