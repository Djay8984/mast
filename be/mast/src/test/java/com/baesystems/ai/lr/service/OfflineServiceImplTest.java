package com.baesystems.ai.lr.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.assets.ItemDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemPageResourceDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.AssetNotePageResourceDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.codicils.CoCPageResourceDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.DefectPageResourceDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.jobs.JobBundleDto;
import com.baesystems.ai.lr.dto.jobs.JobDto;
import com.baesystems.ai.lr.dto.paging.PageResource;
import com.baesystems.ai.lr.dto.query.AttachmentLinkQueryDto;
import com.baesystems.ai.lr.dto.reports.ReportLightDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.services.SurveyPageResourceDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;
import com.baesystems.ai.lr.exception.BadPageRequestException;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.service.assets.AssetService;
import com.baesystems.ai.lr.service.attachments.AttachmentService;
import com.baesystems.ai.lr.service.codicils.ActionableItemService;
import com.baesystems.ai.lr.service.codicils.AssetNoteService;
import com.baesystems.ai.lr.service.codicils.CoCService;
import com.baesystems.ai.lr.service.defects.DefectService;
import com.baesystems.ai.lr.service.defects.RepairService;
import com.baesystems.ai.lr.service.jobs.JobService;
import com.baesystems.ai.lr.service.jobs.SurveyService;
import com.baesystems.ai.lr.service.reports.ReportService;
import com.baesystems.ai.lr.service.services.ServiceService;
import com.baesystems.ai.lr.service.tasks.WorkItemService;

@RunWith(MockitoJUnitRunner.class)
public class OfflineServiceImplTest
{
    @Mock
    private JobService jobService;

    @Mock
    private ReportService reportService;

    @Mock
    private SurveyService surveyService;

    @Mock
    private ActionableItemService actionableItemService;

    @Mock
    private CoCService coCService;

    @Mock
    private AssetNoteService assetNoteService;

    @Mock
    private DefectService defectService;

    @Mock
    private RepairService repairService;

    @Mock
    private AssetService assetService;

    @Mock
    private ServiceService serviceService;

    @Mock
    private AttachmentService attachmentService;

    @Mock
    private WorkItemService workItemService;

    @InjectMocks
    private final OfflineServiceImpl offlineService = new OfflineServiceImpl();

    private static final Long JOB_1_ID = 1L;
    private static final Long ASSET_1_ID = 1L;
    private static final Long ASSET_VERSION_1 = 1L;

    private final JobBundleDto jobBundleDto = new JobBundleDto();

    private final JobDto jobDto = new JobDto();
    private final List<ReportLightDto> reports = new ArrayList<>();
    private final PageResource<ActionableItemDto> wipActionableItems = new ActionableItemPageResourceDto();
    private final PageResource<CoCDto> wipCoCs = new CoCPageResourceDto();
    private final PageResource<AssetNoteDto> wipAssetNotes = new AssetNotePageResourceDto();
    private final PageResource<DefectDto> wipDefects = new DefectPageResourceDto();
    private final List<RepairDto> wipRepairs = new ArrayList<>();
    private final PageResource<SurveyDto> surveys = new SurveyPageResourceDto();
    private final List<WorkItemLightDto> tasks = new ArrayList<>();

    private final AssetLightDto asset = new AssetLightDto();
    private final AssetModelDto assetModelDto = new AssetModelDto();
    private final PageResource<ActionableItemDto> actionableItems = new ActionableItemPageResourceDto();
    private final PageResource<CoCDto> coCs = new CoCPageResourceDto();
    private final PageResource<AssetNoteDto> assetNotes = new AssetNotePageResourceDto();
    private final PageResource<DefectDto> defects = new DefectPageResourceDto();
    private final List<RepairDto> repairs = new ArrayList<>();
    private final List<ScheduledServiceDto> scheduledServices = new ArrayList<>();

    private final List<SupplementaryInformationDto> attachments = new ArrayList<>();

    @Before
    public void setUp() throws Exception
    {
        this.jobDto.setAsset(new LinkVersionedResource());
        this.jobDto.getAsset().setId(ASSET_1_ID);
        this.jobDto.getAsset().setVersion(ASSET_VERSION_1);
        this.wipActionableItems.setContent(new ArrayList<>());
        this.wipCoCs.setContent(new ArrayList<>());
        this.wipAssetNotes.setContent(new ArrayList<>());
        this.wipDefects.setContent(new ArrayList<>());
        this.actionableItems.setContent(new ArrayList<>());
        this.coCs.setContent(new ArrayList<>());
        this.assetNotes.setContent(new ArrayList<>());
        this.defects.setContent(new ArrayList<>());
        this.assetModelDto.setItems(new ArrayList<>());
        this.surveys.setContent(new ArrayList<>());
        this.assetModelDto.setItems(new ArrayList<>());
        this.assetModelDto.getItems().add(new ItemDto());
        this.jobBundleDto.setAssetModel(this.assetModelDto);
        this.jobBundleDto.setId(JOB_1_ID);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetJobBundle() throws RecordNotFoundException, BadPageRequestException
    {
        when(this.jobService.getJob(JOB_1_ID)).thenReturn(this.jobDto);

        when(this.reportService.getReports(JOB_1_ID)).thenReturn(this.reports);

        when(this.actionableItemService.getWIPActionableItemsForJobId(anyInt(), anyInt(), anyString(), anyString(), eq(JOB_1_ID), anyLong()))
                .thenReturn(this.wipActionableItems);

        when(this.coCService.getWIPCoCsForJob(anyInt(), anyInt(), anyString(), anyString(), eq(JOB_1_ID), anyLong())).thenReturn(this.wipCoCs);

        when(this.assetNoteService.getWIPAssetNotesForJob(anyInt(), anyInt(), anyString(), anyString(), eq(JOB_1_ID))).thenReturn(this.wipAssetNotes);

        when(this.defectService.getWIPDefectsForJob(eq(JOB_1_ID), anyInt(), anyInt(), anyString(), anyString())).thenReturn(this.wipDefects);

        when(this.repairService.getWIPRepairsForWIPDefectsAndWipCoCs(any(List.class), any(List.class))).thenReturn(this.wipRepairs);

        when(this.assetService.getAsset(ASSET_1_ID, ASSET_1_ID, null, null, null)).thenReturn(this.asset);

        when(this.assetService.getAssetModel(ASSET_1_ID, ASSET_VERSION_1)).thenReturn(this.assetModelDto);

        when(this.actionableItemService.getActionableItemsForAssets(anyInt(), anyInt(), anyString(), anyString(), eq(ASSET_1_ID), anyLong()))
                .thenReturn(this.actionableItems);
        when(this.coCService.getCoCsByAsset(anyInt(), anyInt(), anyString(), anyString(), eq(ASSET_1_ID), anyLong())).thenReturn(this.coCs);

        when(this.assetNoteService.getAssetNotesByAsset(anyInt(), anyInt(), anyString(), anyString(), eq(ASSET_1_ID), anyLong()))
                .thenReturn(this.assetNotes);
        when(this.defectService.getDefectsForAsset(anyInt(), anyInt(), anyString(), anyString(), eq(ASSET_1_ID), anyLong())).thenReturn(this.defects);

        when(this.repairService.getRepairsForAsset(ASSET_1_ID)).thenReturn(this.repairs);

        when(this.surveyService.getSurveys(anyInt(), anyInt(), anyString(), anyString(), eq(JOB_1_ID))).thenReturn(this.surveys);

        when(this.serviceService.getServicesForAsset(anyLong(), isNull(List.class), isNull(List.class), isNull(Date.class), isNull(Date.class)))
                .thenReturn(this.scheduledServices);

        when(this.attachmentService.getAttachmentsByQuery(any(AttachmentLinkQueryDto.class))).thenReturn(this.attachments);

        when(this.workItemService.getWIPTasksForJob(anyLong())).thenReturn(this.tasks);

        final JobBundleDto result = this.offlineService.getJobBundle(JOB_1_ID);
        Assert.assertEquals(this.jobBundleDto, result);
    }
}
