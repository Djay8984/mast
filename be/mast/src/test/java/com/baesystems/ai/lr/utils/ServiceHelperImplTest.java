package com.baesystems.ai.lr.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Spy;
import org.springframework.data.domain.Pageable;

import com.baesystems.ai.lr.exception.BadPageRequestException;

public class ServiceHelperImplTest
{
    private static final String ORDER_ASC = "ASC";
    private static final String SORT = "sort";
    private static final String SORT_2 = "sort_2";
    private static final Integer ZERO = 0;
    private static final Integer ONE = 1;
    private static final Integer TEN = 10;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Spy
    private final ServiceHelperImpl serviceHelper = new ServiceHelperImpl();

    private void checkPageSpec(final Pageable pageSpec, final Integer expectedPage, final Integer expectedSize,
            final String extectedOrder, final String... expectedSorts)
    {
        assertEquals(expectedPage.intValue(), pageSpec.getPageNumber());
        assertEquals(expectedSize.intValue(), pageSpec.getPageSize());

        for (final String expectedSort : expectedSorts)
        {
            try
            {
                assertNotNull(pageSpec.getSort().getOrderFor(expectedSort));
                assertEquals(extectedOrder, pageSpec.getSort().getOrderFor(expectedSort).getDirection().name());
            }
            catch (final AssertionError e)
            {
                throw new AssertionError("Sort field does not match the expected value <" + expectedSort + ">.");
            }
        }
    }

    private void checkPageSpecNoOrder(final Pageable pageSpec, final Integer expectedPage, final Integer expectedSize, final String... expectedSorts)
    {
        assertEquals(expectedPage.intValue(), pageSpec.getPageNumber());
        assertEquals(expectedSize.intValue(), pageSpec.getPageSize());
        assertNotNull(pageSpec.getSort());

        for (final String expectedSort : expectedSorts)
        {
            try
            {
                assertNotNull(pageSpec.getSort().getOrderFor(expectedSort));
                assertEquals(ORDER_ASC, pageSpec.getSort().getOrderFor(expectedSort).getDirection().name());
            }
            catch (final AssertionError e)
            {
                throw new AssertionError("Sort field does not match the expected value <" + expectedSort + ">.");
            }
        }
    }

    private void checkPageSpec(final Pageable pageSpec, final Integer expectedPage, final Integer expectedSize)
    {
        assertEquals(expectedPage.intValue(), pageSpec.getPageNumber());
        assertEquals(expectedSize.intValue(), pageSpec.getPageSize());
    }

    @Test
    public void createPageableAllValuesSensiblePage1() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ONE, TEN, SORT, ORDER_ASC);

        checkPageSpec(pageSpec, ONE, TEN, ORDER_ASC, SORT);
    }

    @Test
    public void createPageableAllValuesSensiblePage0() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ZERO, TEN, SORT, ORDER_ASC);

        checkPageSpec(pageSpec, ZERO, TEN, ORDER_ASC, SORT);
    }

    @Test
    public void createPageableNoOrder() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ONE, TEN, SORT, null);

        checkPageSpecNoOrder(pageSpec, ONE, TEN, SORT);
    }

    @Test
    public void createPageableNoSort() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ONE, TEN, null, null);

        checkPageSpec(pageSpec, ONE, TEN);
    }

    @Test
    public void createPageableSizeZero() throws BadPageRequestException
    {
        thrown.expect(BadPageRequestException.class);

        this.serviceHelper.createPageable(ONE, ZERO, null, null);
    }

    @Test
    public void createPageableWithNullPage() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(null, TEN, SORT, ORDER_ASC);

        checkPageSpecNoOrder(pageSpec, ZERO, TEN);
    }

    @Test
    public void createPageableWithNullSize() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ONE, null, SORT, ORDER_ASC);

        checkPageSpec(pageSpec, ONE, Integer.MAX_VALUE);
    }

    @Test
    public void createPageableWithMultipleSort() throws BadPageRequestException
    {
        final Pageable pageSpec = this.serviceHelper.createPageable(ONE, TEN, SORT + "," + SORT_2, ORDER_ASC);

        checkPageSpec(pageSpec, ONE, TEN, ORDER_ASC, SORT, SORT_2);
    }
}
