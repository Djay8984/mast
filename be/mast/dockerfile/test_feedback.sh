#!/bin/bash
result=0 # Set default result to pass this will change if one of the pages does not appear as it should
function url_returns {
  local url=$1
  local expected=$2
  local actual=$(wget http://localhost:8090/$url -e use_proxy=yes -e http_proxy=127.0.0.1:8090 -qO-) # get the text from the pages one by one
  if [ "${expected}" == "${actual}" ]; then
    echo -e "${url}\n>>>PASS"
    return 0
  fi
  echo -e "${url}\nExpected:\n${expected}\nActual:\n${actual}\n>>>FAIL"
  result=1
  return 1
}
Docker_id=$(docker run -d -p 8090:8080 backend-docker-image-$1) # run the docker container
echo "docker id = $Docker_id"
url_returns "lregbe/rest/casesetup/flagstates" \
            "[]"
url_returns "lregbe/rest/casesetup/portsofregistry/1" \
			"[]"
docker stop $Docker_id # stop the docker container running 
if [ "$result" -eq 1 ] # if any of the tests failed exit with status 1. This is impotrant to makes sure the jenkins build is recorded as a failure.
then
        exit 1
fi
