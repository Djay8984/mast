package com.baesystems.ai.lr.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.customProperties.ValidationSchemaFactoryWrapper;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

@Mojo(name = "schema-mojo")
public class SchemaMojo extends AbstractMojo
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaMojo.class);

    /**
     * Directory wherein generated source will be put; main, test, site, ... will be added implictly.
     *
     * @parameter expression="${outputDir}" default-value="${project.build.directory}/generated-sources/js"
     * @required
     */
    @Parameter(property = "outputDir", defaultValue = "${project.build.directory}/generated-sources/js")
    private File outputDir;

    @Parameter(property = "classTypeName")
    private String classTypeName;

    @SuppressWarnings("unchecked")
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        if (classTypeName == null || classTypeName.length() == 0)
        {
            throw new MojoExecutionException("execute - no classTypeName configured");
        }

        Class<Object> classType = null;
        try
        {
            classType = (Class<Object>) Class.forName(classTypeName);
        }
        catch (final ClassNotFoundException e)
        {
            throw new MojoExecutionException("execute - class not found:" + classTypeName);
        }

        LOGGER.info("execute - starting reflection");
        final Reflections reflections = new Reflections(new ConfigurationBuilder()
                // .filterInputsBy(new FilterBuilder().includePackage("my.project.prefix"))
                .setUrls(ClasspathHelper.forClassLoader(classType.getClassLoader())));

        final Set<Class<?>> modules = reflections.getSubTypesOf(classType);

        LOGGER.info("execute - classes found:" + modules.size());

        outputDir.mkdirs();

        final List<String> names = new ArrayList<String>();
        for (final Class<?> dto : modules)
        {

            final ObjectMapper mapper = new ObjectMapper();
            final SchemaFactoryWrapper visitor = new ValidationSchemaFactoryWrapper();
            try
            {
                mapper.acceptJsonFormatVisitor(dto, visitor);
            }
            catch (final JsonMappingException ex)
            {
                LOGGER.error("execute", ex);
                throw new MojoExecutionException("execute", ex);
            }

            final JsonSchema schema = visitor.finalSchema();
            try
            {
                final String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(schema);
                names.add(dto.getSimpleName() + ".json");
                writeFile(outputDir.getAbsolutePath() + "/" + dto.getSimpleName() + ".json", value);
            }
            catch (final JsonProcessingException ex)
            {
                LOGGER.error("execute", ex);
                throw new MojoExecutionException("execute", ex);
            }

        }

        final ObjectMapper mapper = new ObjectMapper();
        String value;
        try
        {
            value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(names);
            writeFile(outputDir.getAbsolutePath() + "/" + "index" + ".json", "jsonFile='" + value + "';");
        }
        catch (final JsonProcessingException ex)
        {
            LOGGER.error("execute", ex);
            throw new MojoExecutionException("execute", ex);
        }
    }

    private void writeFile(final String fileName, final String contents) throws MojoExecutionException
    {
        BufferedWriter writer = null;
        try
        {
            writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(contents);
        }
        catch (final IOException ex)
        {
            throw new MojoExecutionException("writeFile", ex);
        }
        finally
        {
            try
            {
                if (writer != null)
                {
                    writer.close();
                }
            }
            catch (final IOException ex)
            {
                throw new MojoExecutionException("writeFile", ex);
            }
        }
    }
}
