USE `${jdbc.integration.schema}`;

DROP TABLE IF EXISTS `MAST_REF_ActionTaken` ;
CREATE TABLE MAST_REF_ActionTaken
(
	id SMALLINT NOT NULL,
	name VARCHAR(10) NOT NULL,
	deleted BOOL NOT NULL DEFAULT 0,
	last_update_date DATETIME,
	last_update_user_id INTEGER,
	PRIMARY KEY (id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ACTION_TAKEN` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ACTION_TAKEN` (
  `ID` VARCHAR(1000),
  `NAME` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ClassDepartment` ;
CREATE TABLE MAST_REF_ClassDepartment
(
	id SMALLINT NOT NULL,
	code VARCHAR(1),
	name VARCHAR(100),
	deleted BOOL NOT NULL DEFAULT 0,
	last_update_date DATETIME,
	last_update_user_id INTEGER,
	PRIMARY KEY (id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CLASS_DEPARTMENT` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CLASS_DEPARTMENT` (
  `ID` VARCHAR(1000),
  `NAME` VARCHAR(1000),
  `CODE` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AttributeDataType` ;
CREATE TABLE MAST_REF_AttributeDataType
(
    id SMALLINT NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_AllowedAssetAttributeValue` ;
CREATE TABLE MAST_REF_AllowedAssetAttributeValue
(
    id SMALLINT NOT NULL,
    name VARCHAR(200) NOT NULL,
    attribute_type_id SMALLINT NOT NULL,
    item_type_id SMALLINT NOT NULL,
    description_order INTEGER,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_allowed_asset_attribute_value_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ALLOWED_ASSET_ATTRIBUTE_VALUE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ALLOWED_ASSET_ATTRIBUTE_VALUE` (
  `ID` VARCHAR(1000),
  `ATTRIBUTE_TYPE_ID` VARCHAR(1000),
  `VALUE` VARCHAR(1000),
  `DELETED` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetAttributeType` ;
CREATE TABLE MAST_REF_AssetAttributeType
(
     id SMALLINT NOT NULL,
     name VARCHAR(100) NOT NULL,
     description VARCHAR(45),
     item_type_id SMALLINT NOT NULL,
     value_type_id SMALLINT NOT NULL,
     description_order INTEGER,
     start_date DATE,
     end_date DATE,
     naming_order INTEGER,
     naming_decoration VARCHAR(25),
     min_occurance INTEGER NOT NULL,
     max_occurance INTEGER NOT NULL,
     transferable BOOL NOT NULL,
     attribute_copy_rule_id TINYINT NOT NULL,
     is_default BOOL,
     deleted BOOL NOT NULL DEFAULT 0,
     last_update_date DATETIME,
     last_update_user_id VARCHAR(150),
     display_order INTEGER NOT NULL DEFAULT 0,
     PRIMARY KEY (id),
     UNIQUE UQ_mast_ref_attribute_type_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ASSET_ATTRIBUTE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_ATTRIBUTE_TYPE` (
  `ID` VARCHAR(1000),
  `ITEM_TYPE_ID` VARCHAR(1000),
  `NAME` VARCHAR(1000),
  `NAMING_DECORATION` VARCHAR(1000),
  `COPY` VARCHAR(1000),
  `ATTRIBUTE_DATATYPE_STRING` VARCHAR(1000),
  `MAST_DATATYPE_STRING` VARCHAR(1000),
  `MAST_DATATYPE_ID` SMALLINT,
  `MIN_OCCURS` VARCHAR(1000),
  `MAX_OCCURS` VARCHAR(1000),
  `DELETED` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_ASSET_CATEGORY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_CATEGORY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetCategory` ;
CREATE TABLE MAST_REF_AssetCategory
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_MAST_REF_AssetCategory_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ASSET_ITEM_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_ITEM_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `deleted` VARCHAR(100)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetItemType` ;
CREATE TABLE MAST_REF_AssetItemType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(45),
    start_date DATE,
    end_date DATE,
    section VARCHAR(25),
    display_order INTEGER,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_item_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_AssetItemRelationshipType` ;
CREATE TABLE MAST_REF_AssetItemRelationshipType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_relationship_type_id(id)

) ENGINE=InnoDB;


DROP TABLE IF EXISTS `EXCEL_ASSET_ITEM_TYPE_RELATIONSHIP` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_ITEM_TYPE_RELATIONSHIP` (
  `ID` VARCHAR(1000),
  `TO_ID` VARCHAR(1000),
  `FROM_ID` VARCHAR(1000),
  `RELATIONSHIP_TYPE_ID` VARCHAR(1000),
  `MIN_OCCURS` VARCHAR(1000),
  `MAX_OCCURS` VARCHAR(1000),
  `DELETED` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetItemTypeRelationship` ;
CREATE TABLE MAST_REF_AssetItemTypeRelationship
(
    id SMALLINT NOT NULL,
    from_item_type_id SMALLINT,
    to_item_type_id SMALLINT NOT NULL,
    relationship_type_id SMALLINT NOT NULL,
    min_occurance INTEGER NOT NULL,
    max_occurance INTEGER NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    asset_category_id SMALLINT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_item_type_relationship_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ASSET_LIFECYCLE_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_LIFECYCLE_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetLifecycleStatus` ;
CREATE TABLE MAST_REF_AssetLifecycleStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_asset_lifecycle_status_id(id)

) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_ASSET_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSET_TYPE` (
  `id` VARCHAR(100),
  `emblem` VARCHAR(1000),
  `level_1_code` VARCHAR(100),
  `level_1_name` VARCHAR(1000),
  `level_2_code` VARCHAR(100),
  `level_2_name` VARCHAR(1000),
  `level_3_code` VARCHAR(100),
  `level_3_name` VARCHAR(1000),
  `level_4_code` VARCHAR(100),
  `level_4_name` VARCHAR(1000),
  `level_5_code` VARCHAR(100),
  `level_5_name` VARCHAR(1000),
  `description` VARCHAR(1000),
  `asset_category` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssetType` ;
CREATE TABLE MAST_REF_AssetType
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    code VARCHAR(10),
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    category_id SMALLINT,
    parent_id SMALLINT,
    level_indication INTEGER,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_asset_type_id(id)

) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_ASSOCIATED_SCHEDULING_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSOCIATED_SCHEDULING_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssociatedSchedulingType` ;
CREATE TABLE MAST_REF_AssociatedSchedulingType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_AttributeCopyRule` ;
CREATE TABLE MAST_REF_AttributeCopyRule
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_attribute_copy_rule_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_ATTRIBUTE_COPY_RULE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ATTRIBUTE_COPY_RULE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_CERTIFICATE_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CERTIFICATE_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_CertificateStatus` ;
CREATE TABLE MAST_REF_CertificateStatus
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_certificate_status_id(id)

) ENGINE=InnoDB
DEFAULT CHARACTER SET = utf8;


DROP TABLE IF EXISTS `EXCEL_CASE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CASE_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `category` VARCHAR(1000)  
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_CaseType` ;
CREATE TABLE MAST_REF_CaseType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_case_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CLASS_MAINTENANCE_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CLASS_MAINTENANCE_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)  
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ClassMaintenanceStatus` ;
CREATE TABLE MAST_REF_ClassMaintenanceStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_class_maintenance_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_ClassSociety`;
CREATE TABLE MAST_REF_ClassSociety
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_class_society_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CLASS_SOCIETY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CLASS_SOCIETY` (
  `id` VARCHAR(100),
  `display_order` VARCHAR(100),
  `code` VARCHAR(100),
  `name` VARCHAR(1000),
  `iacs_member` VARCHAR(1000),
  `deleted` VARCHAR(100)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_DEFECT_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectStatus` ;
CREATE TABLE MAST_REF_DefectStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(100),
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_defect_status_id(id)

) ENGINE=InnoDB;


DROP TABLE IF EXISTS `EXCEL_PARTY_ROLE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PARTY_ROLE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `type` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_PartyRole` ;
CREATE TABLE MAST_REF_PartyRole
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_party_role_id(id)

) ENGINE=InnoDB;


DROP TABLE IF EXISTS `MAST_REF_CustomerFunctionType` ;
CREATE TABLE MAST_REF_CustomerFunctionType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_customer_function_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_ClassStatus` ;
CREATE TABLE MAST_REF_ClassStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CLASS_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CLASS_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


DROP TABLE IF EXISTS `EXCEL_ASSIGNED_SCHEDULING_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ASSIGNED_SCHEDULING_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_AssignedSchedulingType` ;
CREATE TABLE MAST_REF_AssignedSchedulingType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CONFIDENTIALITY_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CONFIDENTIALITY_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ConfidentialityType` ;
CREATE TABLE MAST_REF_ConfidentialityType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_confidentiality_type_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_COUNTRY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_COUNTRY` (
  `id` VARCHAR(100),
  `country_code` VARCHAR(100),
  `name` VARCHAR(1000),
  `deleted` VARCHAR(100)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_ENDORSEMENT_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ENDORSEMENT_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_EndorsementType` ;
CREATE TABLE MAST_REF_EndorsementType
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_endorsement_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_LR_OFFICE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_LR_OFFICE_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `description` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_PreEICInspectionStatus` ;
CREATE TABLE MAST_REF_PreEICInspectionStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_pre_eic_inspection_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_PRE_EIC_INSPECTION_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PRE_EIC_INSPECTION_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_PRODUCT_FAMILY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PRODUCT_FAMILY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ProductFamily` ;
CREATE TABLE MAST_REF_ProductFamily
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_product_family_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_PRODUCT_GROUP` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PRODUCT_GROUP` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `family` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ProductGroup` ;
CREATE TABLE MAST_REF_ProductGroup
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    display_order INTEGER NOT NULL,
    product_family_id SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_product_group_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_PRODUCT_CATALOGUE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PRODUCT_CATALOGUE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `product_group` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ProductCatalogue` ;
CREATE TABLE MAST_REF_ProductCatalogue
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    product_group_id SMALLINT NOT NULL,
    display_order INTEGER NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_product_catalogue_id(id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_ReportType` ;
CREATE TABLE MAST_REF_ReportType
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_report_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_REPORT_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_REPORT_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_RiskAssessmentStatus` ;
CREATE TABLE MAST_REF_RiskAssessmentStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_risk_assessment_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_RISK_ASSESSMENT_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_RISK_ASSESSMENT_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_RULE_SET_CATEGORY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_RULE_SET_CATEGORY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `lr_applicable` VARCHAR(100)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_SCHEDULING_DUE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SCHEDULING_DUE_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_SchedulingDueType` ;
CREATE TABLE MAST_REF_SchedulingDueType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SCHEDULING_REGIME` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SCHEDULING_REGIME` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_SchedulingRegime` ;
CREATE TABLE MAST_REF_SchedulingRegime
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_SchedulingType` ;
CREATE TABLE MAST_REF_SchedulingType
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    scheduling_due_type_id SMALLINT,
    assigned_scheduling_type_id SMALLINT,
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SCHEDULING_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SCHEDULING_TYPE` (
  `ID` VARCHAR(100),
  `NAME` VARCHAR(1000),
  `SCHEDULING_DUE_TYPE` VARCHAR(1000),
  `ASSIGNED_SCHEDULING_TYPE` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceStatus` ;
CREATE TABLE MAST_REF_ServiceStatus
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_service_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SERVICE_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceType` ;
CREATE TABLE MAST_REF_ServiceType
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_service_type_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SERVICE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_SERVICE_CREDIT_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_CREDIT_STATUS` (
  `id` VARCHAR(100),
  `code` VARCHAR(100),
  `name` VARCHAR(100),
  `description` VARCHAR(100)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceCreditStatus` ;
CREATE TABLE MAST_REF_ServiceCreditStatus
(
    id SMALLINT NOT NULL,
    code VARCHAR(2) NOT NULL DEFAULT 'NS',
    name VARCHAR(45) NOT NULL,
    description VARCHAR(100),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_service_credit_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_TECHNICAL_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_TECHNICAL_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `description` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_WorkItemAction` ;
CREATE TABLE MAST_REF_WorkItemAction
(
    id SMALLINT NOT NULL,
    reference_code VARCHAR(5) NOT NULL,
    name VARCHAR(200) NOT NULL,
    task_category VARCHAR(50),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;


DROP TABLE IF EXISTS `EXCEL_WORK_ITEM_ACTION` ;
CREATE TABLE IF NOT EXISTS `EXCEL_WORK_ITEM_ACTION` (
  `id` VARCHAR(100),
  `reference_code` VARCHAR(5),
  `name` VARCHAR(1000),
  `task_category` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_HarmonisationType` ;
CREATE TABLE MAST_REF_HarmonisationType
(
    id TINYINT NOT NULL,
    name VARCHAR(10) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_HARMONISATION_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_HARMONISATION_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceRuleset` ;
CREATE TABLE MAST_REF_ServiceRuleset
(
    id SMALLINT NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SERVICE_RULESET` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_RULESET` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceGroup` ;
CREATE TABLE MAST_REF_ServiceGroup
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    description VARCHAR(45),
    associated_scheduling_type SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_SERVICE_GROUP` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_GROUP` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `ASSOCIATED_SCHEDULING_TYPE` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `EXCEL_SERVICE_CATALOGUE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_SERVICE_CATALOGUE` (
  `ID` VARCHAR(100),
  `DISPLAY_ORDER` VARCHAR(1000),
  `SERVICE_RULESET` VARCHAR(1000),
  `SERVICE_CODE` VARCHAR(1000),
  `CODE_CONTINUOUS` VARCHAR(1000),
  `NAME` VARCHAR(1000),
  `NAME_CONTINUOUS` VARCHAR(1000),
  `SERVICE_CATEGORY` VARCHAR(1000),
  `SERVICE_TYPE` VARCHAR(1000),
  `SCHEDULING_REGIME` VARCHAR(1000),
  `CYCLE_PERIODICITY` VARCHAR(1000),
  `HARMONISATION_TYPE` VARCHAR(1000),
  `UPPER_RANGE_PERIOD` VARCHAR(1000),
  `LOWER_RANGE_PERIOD` VARCHAR(1000),
  `MULTIPLE_INDICATOR` VARCHAR(1000),
  `CYCLE_PERIODICITY_EDITABLE` VARCHAR(1000),
  `PRODUCT_NAME` VARCHAR(1000),
  `DEFECT_CATEGORY` VARCHAR(1000),
  `SCHEDULING_TYPE` VARCHAR(1000),
  `SERVICE_GROUP` VARCHAR(1000)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ServiceCatalogue` ;
CREATE TABLE MAST_REF_ServiceCatalogue
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    code VARCHAR(10) NOT NULL,
    name VARCHAR(45) NOT NULL,
    service_type_id SMALLINT,
    survey_type_id SMALLINT,
    product_catalogue_id SMALLINT NOT NULL,
    defect_category_id SMALLINT,
    service_ruleset_id SMALLINT,
    service_group_id SMALLINT NOT NULL,
    scheduling_type_id SMALLINT NOT NULL,
    continuous_indicator BOOL,
    harmonisation_type_id TINYINT,
    display_order INTEGER NOT NULL,
    cycle_periodicity INTEGER NOT NULL,
    cycle_periodicity_editable BOOL,
    lower_range_date_offset_months INTEGER NOT NULL,
    upper_range_date_offset_months INTEGER NOT NULL,
    description VARCHAR(45),
    category_letter VARCHAR(1),
    deleted BOOL NOT NULL DEFAULT 0,
    multiple_indicator BOOL,
    scheduling_regime_id SMALLINT,
    scheduling_subregime_id TINYINT,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_ServiceCatalogueRelationship` ;
CREATE TABLE MAST_REF_ServiceCatalogueRelationship
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    from_service_catalogue_id SMALLINT NOT NULL,
    to_service_catalogue_id SMALLINT NOT NULL,
    service_catalogue_relationship_type_id SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_DefectCategory` ;
CREATE TABLE MAST_REF_DefectCategory
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    category_letter VARCHAR(1),
    parent_id SMALLINT,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `MAST_REF_SurveyType` ;
CREATE TABLE MAST_REF_SurveyType
(
    id SMALLINT NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(45),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO MAST_REF_SurveyType (id, name)
VALUES(1, 'DAMAGE'),
       (2, 'REPAIR');

DROP TABLE IF EXISTS `MAST_REF_ServiceCatalogueRelationshipType` ;
CREATE TABLE MAST_REF_ServiceCatalogueRelationshipType
(
    id SMALLINT NOT NULL,
    name VARCHAR(45) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;
       

INSERT INTO MAST_REF_ServiceCatalogueRelationshipType (id, name)
VALUES(1, 'Is dependent on'),
       (2, 'Is counterpart of');
       
-- Attribute Data Type

DROP TABLE IF EXISTS `EXCEL_ATTRIBUTE_DATA_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_ATTRIBUTE_DATA_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_ClassNotation`;
CREATE TABLE MAST_REF_ClassNotation
(
    id SMALLINT NOT NULL,
    name VARCHAR(1000) NOT NULL,
    description VARCHAR(1000),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_class_notation_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_CLASS_NOTATION` ;
CREATE TABLE IF NOT EXISTS `EXCEL_CLASS_NOTATION` (
  `id` VARCHAR(100),
  `display_order` VARCHAR(100),
  `name` VARCHAR(1000),
  `class_notation_category` VARCHAR(1000)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_MigrationStatus` ;
CREATE TABLE MAST_REF_MigrationStatus
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE UQ_mast_ref_migration_status_id(id)

) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_MIGRATION_STATUS` ;
CREATE TABLE IF NOT EXISTS `EXCEL_MIGRATION_STATUS` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectValueSeverity` ;
CREATE TABLE MAST_REF_DefectValueSeverity
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_DEFECT_VALUE_SEVERITY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_VALUE_SEVERITY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectCategory` ;
CREATE TABLE MAST_REF_DefectCategory
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    category_letter VARCHAR(1),
    parent_id SMALLINT,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_DEFECT_CATEGORY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_CATEGORY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `CATEGORY_LETTER` VARCHAR(1),
  `PARENT_ID` VARCHAR(100)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectDescriptor` ;
CREATE TABLE MAST_REF_DefectDescriptor
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_DEFECT_VALUE_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_VALUE_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectValue` ;
CREATE TABLE MAST_REF_DefectValue
(
    id SMALLINT NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    severity_id SMALLINT,
    defect_descriptor_id SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_DEFECT_VALUE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_VALUE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `TYPE` VARCHAR(1000),
  `SEVERITY` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_DefectCategory_DefectValue` ;
CREATE TABLE MAST_REF_DefectCategory_DefectValue
(
    id SMALLINT NOT NULL,
    defect_value_id SMALLINT NOT NULL,
    defect_category_id SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_DEFECT_VALUE_CATEGORY_MAPPING` ;
CREATE TABLE IF NOT EXISTS `EXCEL_DEFECT_VALUE_CATEGORY_MAPPING` (
  `id` VARCHAR(100),
  `VALUE_ID` VARCHAR(100),
  `CATEGORY_ID` VARCHAR(100)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_RepairType` ;
CREATE TABLE MAST_REF_RepairType
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_REPAIR_TYPE` ;
CREATE TABLE IF NOT EXISTS `EXCEL_REPAIR_TYPE` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `CATEGORY` VARCHAR(100)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_Material` ;
CREATE TABLE MAST_REF_Material
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    description VARCHAR(500),
    material_type_id SMALLINT NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_MATERIAL` ;
CREATE TABLE IF NOT EXISTS `EXCEL_MATERIAL` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000),
  `MATERIAL_CATEGORY` VARCHAR(1000),
  `DEFECT_CATEGORY` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_MaterialCategory` ;
CREATE TABLE MAST_REF_MaterialCategory
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    description VARCHAR(500),
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_MATERIAL_CATEGORY` ;
CREATE TABLE IF NOT EXISTS `EXCEL_MATERIAL_CATEGORY` (
  `id` VARCHAR(100),
  `name` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `MAST_REF_Port` ;
CREATE TABLE MAST_REF_Port
(
    id SMALLINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    deleted BOOL NOT NULL DEFAULT 0,
    last_update_date DATETIME,
    last_update_user_id VARCHAR(150),
    PRIMARY KEY (id)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `EXCEL_PORT` ;
CREATE TABLE IF NOT EXISTS `EXCEL_PORT` (
  `ID` VARCHAR(100),
  `NAME` VARCHAR(1000),
  `DELETED` VARCHAR(1000)
  ) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
