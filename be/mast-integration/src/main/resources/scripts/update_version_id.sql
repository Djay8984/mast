-- If all is successful, update the reference data version.
UPDATE`${jdbc.mast.schema}`.MAST_REF_ReferenceDataVersion
SET version = version + 1;
