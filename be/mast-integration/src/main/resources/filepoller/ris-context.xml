<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:batch="http://www.springframework.org/schema/batch"
       xmlns:util="http://www.springframework.org/schema/util"
       xsi:schemaLocation="http://www.springframework.org/schema/batch
       http://www.springframework.org/schema/batch/spring-batch-2.2.xsd
       http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
       http://www.springframework.org/schema/util
       http://www.springframework.org/schema/util/spring-util.xsd
       http://camel.apache.org/schema/spring
       http://camel.apache.org/schema/spring/camel-spring.xsd">

    <batch:job id="risJob" job-repository="batchJobRepository">
        <batch:step id="phase1-import" next="phase2-update">
            <batch:tasklet transaction-manager="transactionManager">
                <batch:chunk reader="risAuthReader" processor="risAuthProcessor" writer="risIntegrationJpaWriter" commit-interval="10000"/>
            </batch:tasklet>
        </batch:step>
        <batch:step id="phase2-update" next="phase3-delete">
            <batch:tasklet transaction-manager="transactionManager">
                <batch:chunk reader="risStagingAuthUpdateJDBCReader" processor="risAuthUpdateProcessor" writer="risAuthUpdateWriter" commit-interval="1000"/>
            </batch:tasklet>
        </batch:step>
        <batch:step id="phase3-delete" next="phase4-generate-success-report">
            <batch:tasklet transaction-manager="transactionManager">
                <batch:chunk reader="risStagingAuthDeleteJDBCReader" processor="risAuthDeleteProcessor" writer="risAuthUpdateWriter" commit-interval="1000"/>
            </batch:tasklet>
        </batch:step>
        <batch:step id="phase4-generate-success-report" next="phase5-generate-failure-report">
            <batch:tasklet transaction-manager="transactionManager">
                <batch:chunk reader="risReportStagingReaderSuccesses" writer="risReportFileItemWriterSuccesses" commit-interval="50"/>
            </batch:tasklet>
        </batch:step>
        <batch:step id="phase5-generate-failure-report" next="phase6-increment-version">
            <batch:tasklet transaction-manager="transactionManager">
                <batch:chunk reader="risReportStagingReaderFailures" writer="risReportFileItemWriterFailures" commit-interval="50"/>
            </batch:tasklet>
        </batch:step>
        <batch:step id="phase6-increment-version" next="phase7-purge-staging">
            <batch:tasklet ref="risRefVersionUpdate" transaction-manager="transactionManager"/>
        </batch:step>
        <batch:step id="phase7-purge-staging">
            <batch:tasklet ref="risStagingPurge" transaction-manager="transactionManager"/>
        </batch:step>
    </batch:job>

    <!-- Step 1 - import -->
    <bean id="risAuthReader" class="org.springframework.batch.item.xml.StaxEventItemReader" scope="step">
        <property name="fragmentRootElementName" value="T_AUTHORISATIONS"/>
        <property name="resource" value="#{jobParameters['CamelFileNameProduced']}"/>
        <property name="unmarshaller" ref="risAuthMarshaller"/>
    </bean>
    <bean id="risAuthMarshaller" class="com.baesystems.ai.lr.integration.ris.SkippableXStreamMarshaller">
        <property name="autodetectAnnotations" value="true"/>
        <property name="aliases">
            <util:map id="aliases">
                <entry key="T_AUTHORISATIONS" value="com.baesystems.ai.lr.integration.ris.dto.RISAuthorisationDto"/>
            </util:map>
        </property>
    </bean>
    <bean id="risAuthProcessor" class="com.baesystems.ai.lr.integration.ris.RISAuthProcessor"/>

    <!-- Step 2 - update -->
    <!-- forced to use JDBC implementation of this due to known issue of JPA Paging Reader EM holding reference to entities
    and not flushing until the the beginning of a re-read, if count of current page is < than commit-interval (and therefore
    no re-read is required, modified entities are not persisted to the db. This is despite using JPAStaging Task to manually
    flushing the integration EM!, I am not sure how the other migration/integration jobs are not suffering from this. They very well could
    be. -->
    <bean id="risStagingAuthUpdateJDBCReader" class="org.springframework.batch.item.database.JdbcPagingItemReader" scope="step">
        <property name="dataSource" ref="integrationDataSource"/>
        <property name="queryProvider">
            <bean class="org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean">
                <property name="dataSource" ref="integrationDataSource" />
                <property name="databaseType" value="MySQL" />
                <property name="selectClause" value="select ID, OPERATION, CERTIFICATE_CODE as certificateCode, FLAG_NAME as flagName, SURVEY_CODE as surveyCode, AUTHORISATION_INDICATOR as authorisationIndicator, FAILURE_REASON as failureReason, JOB_EXECUTION_ID as jobExecutionId, WARNING_FLAG as warningFlag, EXCEPTION_CHECKSUM_FLAG as exceptionChecksumFlag, FAILURE_REASON as failureReason"/>
                <property name="fromClause" value="from STAGING_RIS_AUTH_UPDATE" />
                <property name="whereClause">
                    <value>
                    <![CDATA[
                            where JOB_EXECUTION_ID = #{stepExecution.jobExecutionId}
                            and (OPERATION = 'INSERT' or OPERATION = 'UPDATE')
                       ]]>
                    </value>
                </property>
                <property name="sortKey" value="ID" />
            </bean>
        </property>
        <property name="pageSize" value="1000" />
        <property name="rowMapper" ref="risAuthRowMapper"/>
    </bean>

    <!--<bean id="risStagingAuthUpdateReader" class="org.springframework.batch.item.database.JpaPagingItemReader" scope="step">-->
    <!--<property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>-->
    <!--<property name="queryString" value="select stagingRISAuthUpdate-->
    <!--from RISAuthorisationUpdateDO stagingRISAuthUpdate-->
    <!--where stagingRISAuthUpdate.jobExecutionId = #{stepExecution.jobExecutionId}-->
    <!--and (stagingRISAuthUpdate.operation = 'INSERT'-->
    <!--or stagingRISAuthUpdate.operation = 'UPDATE')"/>-->
    <!--<property name="pageSize" value="10000"/>-->
    <!--</bean>-->

    <bean id="risAuthUpdateProcessor" class="com.baesystems.ai.lr.integration.ris.RISAuthUpdateProcessor"/>

    <!-- use custom task to write to multiple writers -->
    <bean id="risAuthUpdateWriter" class="com.baesystems.ai.lr.integration.util.JPAStagingTask">
        <constructor-arg ref="risListMastJpaWriter"/>
        <constructor-arg ref="risIntegrationJpaWriter"/>
        <constructor-arg ref="risSimpleStagingConverter"/>
    </bean>
    <bean id = "risSimpleStagingConverter" class="com.baesystems.ai.lr.integration.ris.RISSimpleStagingProcessor"/>

    <!-- Step 3 - delete -->
    <bean id="risStagingAuthDeleteJDBCReader" class="org.springframework.batch.item.database.JdbcPagingItemReader" scope="step">
        <property name="dataSource" ref="integrationDataSource"/>
        <property name="queryProvider">
            <bean class="org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean">
                <property name="dataSource" ref="integrationDataSource" />
                <property name="databaseType" value="MySQL" />
                <property name="selectClause" value="select ID, OPERATION, CERTIFICATE_CODE as certificateCode, FLAG_NAME as flagName, SURVEY_CODE as surveyCode, AUTHORISATION_INDICATOR as authorisationIndicator, FAILURE_REASON as failureReason, JOB_EXECUTION_ID as jobExecutionId, WARNING_FLAG as warningFlag, EXCEPTION_CHECKSUM_FLAG as exceptionChecksumFlag, FAILURE_REASON as failureReason"/>
                <property name="fromClause" value="from STAGING_RIS_AUTH_UPDATE" />
                <property name="whereClause">
                    <value>
                        <![CDATA[
                            where JOB_EXECUTION_ID = #{stepExecution.jobExecutionId}
                            and OPERATION = 'DELETE'
                        ]]>
                    </value>
                </property>
                <property name="sortKey" value="ID" />
            </bean>
        </property>
        <property name="pageSize" value="1000" />
        <property name="rowMapper" ref="risAuthRowMapper"/>
    </bean>

    <!--<bean id="risStagingAuthDeleteReader" class="org.springframework.batch.item.database.JpaPagingItemReader" scope="step">-->
    <!--<property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>-->
    <!--<property name="queryString" value="select stagingRISAuthUpdate-->
    <!--from RISAuthorisationUpdateDO stagingRISAuthUpdate-->
    <!--where stagingRISAuthUpdate.jobExecutionId = #{stepExecution.jobExecutionId}-->
    <!--and stagingRISAuthUpdate.operation = 'DELETE'"/>-->
    <!--<property name="pageSize" value="1000"/>-->
    <!--</bean>-->

    <bean id="risAuthDeleteProcessor" class="com.baesystems.ai.lr.integration.ris.RISAuthDeleteProcessor"/>

    <!-- Step 4 - success report -->
    <bean id="risReportStagingReaderSuccesses" class="org.springframework.batch.item.database.JpaPagingItemReader" scope="step">
        <property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>
        <property name="queryString" value="select stagingRISAuthUpdate
                                            from RISAuthorisationUpdateDO stagingRISAuthUpdate
                                            where stagingRISAuthUpdate.jobExecutionId = #{stepExecution.jobExecutionId}
                                            and (stagingRISAuthUpdate.exceptionFlag is null or
                                            stagingRISAuthUpdate.exceptionFlag = false)"/>
        <property name="pageSize" value="50"/>
    </bean>
    <bean id="risReportFileItemWriterSuccesses" class="org.springframework.batch.item.file.FlatFileItemWriter" scope="step">
        <property name="resource"
                  value="file://#{jobParameters['MastRISFolder']}/done/success/job#{stepExecution.jobExecutionId}_#{jobParameters['CamelFileName']}_succeeded.txt"/>
        <property name="shouldDeleteIfExists" value="true"/>
        <property name="lineAggregator">
            <bean class="org.springframework.batch.item.file.transform.DelimitedLineAggregator">
                <property name="delimiter">
                    <util:constant static-field="org.springframework.batch.item.file.transform.DelimitedLineTokenizer.DELIMITER_TAB"/>
                </property>
                <property name="fieldExtractor">
                    <bean class="org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor">
                        <property name="names" value="id, operation, flagName, surveyCode"/>
                    </bean>
                </property>
            </bean>
        </property>
        <property name="headerCallback">
            <bean class="com.baesystems.ai.lr.integration.ris.RISReportHeader">
                <property name="reportType" value="SUCCESS"/>
                <property name="delimiter">
                    <util:constant static-field="org.springframework.batch.item.file.transform.DelimitedLineTokenizer.DELIMITER_TAB"/>
                </property>
            </bean>
        </property>
    </bean>

    <!-- Step 5 - failure report -->
    <bean id="risReportStagingReaderFailures" class="org.springframework.batch.item.database.JpaPagingItemReader" scope="step">
        <property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>
        <property name="queryString" value="select stagingRISAuthUpdate
                                            from RISAuthorisationUpdateDO stagingRISAuthUpdate
                                            where stagingRISAuthUpdate.jobExecutionId = #{stepExecution.jobExecutionId}
                                            and stagingRISAuthUpdate.exceptionFlag = true"/>
        <property name="pageSize" value="50"/>
    </bean>
    <bean id="risReportFileItemWriterFailures" class="org.springframework.batch.item.file.FlatFileItemWriter" scope="step">
        <property name="resource"
                  value="file://#{jobParameters['MastRISFolder']}/done/failure/job#{stepExecution.jobExecutionId}_#{jobParameters['CamelFileName']}_failed.txt"/>
        <property name="shouldDeleteIfExists" value="true"/>
        <property name="lineAggregator">
            <bean class="org.springframework.batch.item.file.transform.DelimitedLineAggregator">
                <property name="delimiter">
                    <util:constant static-field="org.springframework.batch.item.file.transform.DelimitedLineTokenizer.DELIMITER_TAB"/>
                </property>
                <property name="fieldExtractor">
                    <bean class="org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor">
                        <property name="names" value="id, operation, flagName, surveyCode, failureReason"/>
                    </bean>
                </property>
            </bean>
        </property>
        <property name="headerCallback">
            <bean class="com.baesystems.ai.lr.integration.ris.RISReportHeader">
                <property name="reportType" value="FAILURE"/>
                <property name="delimiter">
                    <util:constant static-field="org.springframework.batch.item.file.transform.DelimitedLineTokenizer.DELIMITER_TAB"/>
                </property>
            </bean>
        </property>
    </bean>

    <!-- Step 6 - version -->
    <bean id="risRefVersionUpdate" class="com.baesystems.ai.lr.integration.ris.RISRefVersionUpdateTask" scope="step"/>

    <!-- Step 7 - cleaning -->
    <bean id="risStagingPurge" class="com.baesystems.ai.lr.integration.util.JPAUpdateTask" scope="step">
        <property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>
        <property name="transactionManager" ref = "integrationTransactionManager"/>
        <property name="queryString" value="delete
          from RISAuthorisationUpdateDO staging
          where staging.jobExecutionId = #{stepExecution.jobExecutionId}
          and (staging.exceptionFlag is null
            or staging.exceptionFlag = false)"/>
    </bean>

    <!-- Common -->
    <bean id="risListIntegrationJpaWriter" class="com.baesystems.ai.lr.integration.util.ListJPAItemWriter">
        <property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>
    </bean>
    <bean id="risIntegrationJpaWriter" class="com.baesystems.ai.lr.integration.util.JPAItemWriter">
        <property name="entityManagerFactory" ref="integrationEntityManagerFactory"/>
    </bean>
    <bean id="risListMastJpaWriter" class="com.baesystems.ai.lr.integration.util.ListJPAItemWriter">
        <property name="entityManagerFactory" ref="mastEntityManagerFactory"/>
    </bean>
    <bean id="risAuthRowMapper" class="org.springframework.jdbc.core.BeanPropertyRowMapper">
        <property name="mappedClass" value="com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO" />
    </bean>
    <routeContext id="risContext" xmlns="http://camel.apache.org/schema/spring">
        <route id="risRoute">
            <from uri="file://{{mast.ris.folder}}/?delay=5000&amp;idempotent=true&amp;idempotentRepository=#fileStore&amp;move=done/${file:name}"/>
            <unmarshal ref="gzDataFormat"/>
            <unmarshal ref="tarFileDataFormat"/>
            <to uri="md5TarProcessor"/>
            <setHeader headerName="MastRISFolder">
                <simple>{{mast.ris.folder}}</simple>
            </setHeader>
            <to uri="spring-batch:risJob"/>
        </route>
    </routeContext>
</beans>
