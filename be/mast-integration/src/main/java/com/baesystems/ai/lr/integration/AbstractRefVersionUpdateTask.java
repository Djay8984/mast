package com.baesystems.ai.lr.integration;

import com.baesystems.ai.lr.domain.mast.repositories.ReferenceDataVersionRepository;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Use this to increment the reference data version following a change from one of the
 * integration components
 */
public abstract class AbstractRefVersionUpdateTask implements Tasklet
{
    @Autowired
    private ReferenceDataVersionRepository referenceDataVersionRepository;

    @Override
    public RepeatStatus execute(final StepContribution stepContribution, final ChunkContext chunkContext) throws Exception //NOPMD
    {
        final Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
        if (referenceDataChanged(jobExecutionId))
        {
            this.referenceDataVersionRepository.incrementVersion(ReferenceDataVersionRepository.TOP_LEVEL_ID);
        }
        return RepeatStatus.FINISHED;
    }

    /**
     * Implement the process of confirming reference data has changed.
     */
    protected abstract boolean referenceDataChanged(Long jobExecutionId);
}
