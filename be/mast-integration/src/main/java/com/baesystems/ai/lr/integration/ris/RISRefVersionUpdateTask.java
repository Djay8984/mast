package com.baesystems.ai.lr.integration.ris;

import com.baesystems.ai.lr.domain.integration.repositories.RISAuthorisationUpdateRepository;
import com.baesystems.ai.lr.integration.AbstractRefVersionUpdateTask;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Ref data version update implementation for RIS
 */
public class RISRefVersionUpdateTask extends AbstractRefVersionUpdateTask
{
    @Autowired
    private RISAuthorisationUpdateRepository risAuthorisationUpdateRepository;

    @Override
    protected boolean referenceDataChanged(final Long jobExecutionId)
    {
        return this.risAuthorisationUpdateRepository.numberOfSuccessfulRecords(jobExecutionId) > 0;
    }
}
