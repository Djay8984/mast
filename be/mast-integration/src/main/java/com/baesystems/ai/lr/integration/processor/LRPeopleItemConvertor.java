package com.baesystems.ai.lr.integration.processor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.domain.integration.entities.StagingLRPeopleDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeDO;
import com.baesystems.ai.lr.domain.mast.entities.employees.LrEmployeeOfficeDO;
import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;
import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRepository;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRoleRepository;
import com.baesystems.ai.lr.domain.mast.repositories.OfficeRepository;

public class LRPeopleItemConvertor implements ItemProcessor<StagingLRPeopleDO, LrEmployeeDO>
{
    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private LrEmployeeRepository employeeRepository;

    @Autowired
    private LrEmployeeRoleRepository employeeRoleRepository;

    @Override
    public LrEmployeeDO process(final StagingLRPeopleDO item)
    {
        LrEmployeeDO employee = this.employeeRepository.findByOneWorldNumber(item.getOneWorldNumber());
        if (employee == null)
        {
            employee = new LrEmployeeDO();
            employee.setOffices(new ArrayList<LrEmployeeOfficeDO>());
            employee.setRoles(new ArrayList<LrEmployeeRoleDO>());
        }

        employee.setNetworkUserId(item.getNetworkUserId());
        employee.setOneWorldNumber(item.getOneWorldNumber());

        employee.setFirstName(item.getFirstName());
        employee.setMiddleNames(item.getMiddleNames());
        employee.setLastName(item.getLastName());

        employee.setEmailAddress(item.getEmailAddress());

        employee.setWorkPhone(item.getWorkPhone());
        employee.setWorkMobile(item.getWorkMobile());

        employee.setDepartment(item.getDepartment());

        final List<LrEmployeeOfficeDO> offices = this.processOffices(item, employee);
        employee.getOffices().clear();
        employee.getOffices().addAll(offices);

        final List<LrEmployeeRoleDO> roles = employee.getRoles();
        roles.clear();

        final LrEmployeeRoleDO role = this.getEmployeeRole(item.getJobGroup());
        if (role != null)
        {
            roles.add(role);
        }

        return employee;
    }

    private List<LrEmployeeOfficeDO> processOffices(final StagingLRPeopleDO item, final LrEmployeeDO employee)
    {
        // Work Location Name is being used for OFFICE which is incorrect for next-release
        final List<LrEmployeeOfficeDO> offices = new ArrayList<LrEmployeeOfficeDO>();
        final OfficeDO office = this.getOffice(item.getWorkLocationName());
        if (office != null)
        {
            final LrEmployeeOfficeDO employeeOffice = new LrEmployeeOfficeDO();
            employeeOffice.setOffice(office);
            employeeOffice.setEmployee(employee);
            employeeOffice.setPrimary(true);

            offices.add(employeeOffice);
        }
        return offices;
    }

    private LrEmployeeRoleDO getEmployeeRole(final String name)
    {
        return this.employeeRoleRepository.findByName(name);
    }

    private OfficeDO getOffice(final String name)
    {
        return officeRepository.findByCode(name);
    }
}
