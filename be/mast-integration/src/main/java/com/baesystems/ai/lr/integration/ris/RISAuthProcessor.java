package com.baesystems.ai.lr.integration.ris;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.integration.ris.dto.RISAuthorisationDto;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

public class RISAuthProcessor implements ItemProcessor<RISAuthorisationDto, RISAuthorisationUpdateDO>
{
    private Long jobExecutionId;
    private Boolean checkSumValid = false;

    private final MapperFacade mapper;

    public RISAuthProcessor()
    {
        final DefaultMapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(RISAuthorisationDto.class, RISAuthorisationUpdateDO.class)
                .field("flagCode", "flagName") //Its actually the name, saves confusion later.
                .byDefault()
                .customize(new CustomMapper<RISAuthorisationDto, RISAuthorisationUpdateDO>()
                {
                    @Override
                    public void mapAtoB(final RISAuthorisationDto risAuthorisationDto,
                            final RISAuthorisationUpdateDO risAuthorisationUpdateDO, final MappingContext context)
                    {
                        risAuthorisationUpdateDO.setJobExecutionId(jobExecutionId);
                        risAuthorisationUpdateDO.setExceptionChecksumFlag(!checkSumValid);
                    }
                })
                .register();

        mapper = factory.getMapperFacade();
    }

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution)
    {
        jobExecutionId = stepExecution.getJobExecutionId();
        this.checkSumValid = Boolean.valueOf(stepExecution.getJobParameters().getString("CHECKSUM_VALID"));
    }

    @Override
    public RISAuthorisationUpdateDO process(final RISAuthorisationDto risAuthorisationDto)
    {
        return mapper.map(risAuthorisationDto, RISAuthorisationUpdateDO.class);
    }
}
