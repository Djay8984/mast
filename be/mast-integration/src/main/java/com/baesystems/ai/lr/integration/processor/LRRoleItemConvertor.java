package com.baesystems.ai.lr.integration.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.domain.mast.entities.references.LrEmployeeRoleDO;
import com.baesystems.ai.lr.domain.mast.repositories.LrEmployeeRoleRepository;

public class LRRoleItemConvertor implements ItemProcessor<Object[], LrEmployeeRoleDO>
{
    @Autowired
    private LrEmployeeRoleRepository roleRepository;

    @Override
    public LrEmployeeRoleDO process(final Object[] roleArray)
    {
        final String roleName = (String) roleArray[0];
        final String jobGroup = (String) roleArray[1];

        LrEmployeeRoleDO role = this.getRole(roleName);
        if (role == null)
        {
            role = new LrEmployeeRoleDO();
            role.setName(roleName);
            role.setJobGroup(jobGroup);
        }
        return role;
    }

    private LrEmployeeRoleDO getRole(final String name)
    {
        return roleRepository.findByName(name);
    }
}
