package com.baesystems.ai.lr.integration.ris.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * This is currently not used, but keeping in case scope of RIS integration increases later
 */
@XStreamAlias("T_COUNTRIES")
public class RISCountriesDto implements RISDto
{
    private static final long serialVersionUID = 3115982781146505654L;

    @XStreamAlias("OPERATION")
    private String operation;

    @XStreamAlias("code")
    private String code;

    @XStreamAlias("name")
    private String name;

    @XStreamAlias("FLAG_INDICATOR")
    private String flagIndicator;

    @XStreamAlias("harmonisation_indicator")
    private String harmonisationIndicator;

    @XStreamAlias("authorisation_indicator")
    private String authorisationIndicator;

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(final String operation)
    {
        this.operation = operation;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getFlagIndicator()
    {
        return flagIndicator;
    }

    public void setFlagIndicator(final String flagIndicator)
    {
        this.flagIndicator = flagIndicator;
    }

    public String getHarmonisationIndicator()
    {
        return harmonisationIndicator;
    }

    public void setHarmonisationIndicator(final String harmonisationIndicator)
    {
        this.harmonisationIndicator = harmonisationIndicator;
    }

    public String getAuthorisationIndicator()
    {
        return authorisationIndicator;
    }

    public void setAuthorisationIndicator(final String authorisationIndicator)
    {
        this.authorisationIndicator = authorisationIndicator;
    }
}
