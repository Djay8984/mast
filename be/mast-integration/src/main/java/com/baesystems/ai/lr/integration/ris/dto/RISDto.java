package com.baesystems.ai.lr.integration.ris.dto;

import com.baesystems.ai.lr.dto.base.Dto;

import java.io.Serializable;

/**
 * Marker for RIS specific data objects
 */
public interface RISDto extends Dto, Serializable
{
}
