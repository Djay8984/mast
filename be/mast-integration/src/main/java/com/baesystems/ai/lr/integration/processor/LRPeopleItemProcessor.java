package com.baesystems.ai.lr.integration.processor;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

import com.baesystems.ai.lr.domain.integration.entities.StagingLRPeopleDO;

public class LRPeopleItemProcessor implements ItemProcessor<StagingLRPeopleDO, StagingLRPeopleDO>
{
    private Long jobExecutionId;
    private Boolean checkSumValid = false;

    @BeforeStep
    public void beforeStep(final StepExecution stepExecution)
    {
        jobExecutionId = stepExecution.getJobExecutionId();
        this.checkSumValid = Boolean.valueOf(stepExecution.getJobParameters().getString("CHECKSUM_VALID"));
    }

    @Override
    public StagingLRPeopleDO process(final StagingLRPeopleDO item)
    {
        // set job execution id
        item.setJobExecutionId(jobExecutionId);
        item.setExceptionChecksum(!checkSumValid);

        boolean error = false;
        error = error || item.getExceptionChecksum();
        item.setException(error);
        return item;
    }
}
