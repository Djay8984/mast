package com.baesystems.ai.lr.integration.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.domain.mast.entities.references.OfficeDO;
import com.baesystems.ai.lr.domain.mast.repositories.OfficeRepository;

public class LROfficeItemConvertor implements ItemProcessor<String, OfficeDO>
{
    @Autowired
    private OfficeRepository officeRepository;

    @Override
    public OfficeDO process(final String code)
    {
        return officeRepository.findByCode(code);
    }

}
