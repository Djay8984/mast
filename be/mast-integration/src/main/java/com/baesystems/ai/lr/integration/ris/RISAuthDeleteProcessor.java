package com.baesystems.ai.lr.integration.ris;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueFlagAdministrationDO;

import java.util.Collections;
import java.util.List;

/**
 * Delete, processes DELETE operations, removes the appropriate link records for country and survey
 * if one exists.
 */
public class RISAuthDeleteProcessor extends AbstractRISAuthProcessor
{
    private static final String LINK_DOES_NOT_EXIST = "link does not exist";
    private static final List<String> VALID_OPERATIONS = Collections.singletonList("DELETE");

    @Override
    protected void innerProcess(final RISAuthorisationUpdateDO risAuthorisationUpdateDO,
            final List<ServiceCatalogueFlagAdministrationDO> newUpdatedEntities, final Long flagAdministrationID, final Long surveyID,
            final boolean linkExists)
    {

        if (linkExists)
        {
            final List<ServiceCatalogueFlagAdministrationDO> links = getServiceCatalogueFlagAdministrationRepository()
                    .findByServiceCatalogAndFlag(surveyID, flagAdministrationID);
            for (final ServiceCatalogueFlagAdministrationDO scfaDO : links)
            {
                scfaDO.setDeleted(true);
                newUpdatedEntities.add(scfaDO);
            }
        }
        else
        {
            risAuthorisationUpdateDO.setExceptionFlag(true);
            risAuthorisationUpdateDO.setFailureReason(LINK_DOES_NOT_EXIST);
            LOG.warn(String.format(LINK_DOES_NOT_EXIST + "for %s. ignoring", risAuthorisationUpdateDO.getFlagName()));
        }
    }

    @Override
    protected List<String> validOperations()
    {
        return VALID_OPERATIONS;
    }
}
