package com.baesystems.ai.lr.integration.ris;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueFlagAdministrationDO;
import com.baesystems.ai.lr.domain.mast.repositories.FlagStateRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceCatalogueFlagAdministrationRepository;
import com.baesystems.ai.lr.domain.mast.repositories.ServiceCatalogueRepository;
import com.baesystems.ai.lr.exception.MastSystemException;
import com.baesystems.ai.lr.integration.util.StagingPairDto;

/**
 * Abstract Processor of RIS imported changes to authority relationships between countries and service catalogs
 * <p>
 * Implementations to provide a restriction on suitable operations to be read.
 */
public abstract class AbstractRISAuthProcessor implements
        ItemProcessor<RISAuthorisationUpdateDO, StagingPairDto<List<ServiceCatalogueFlagAdministrationDO>, RISAuthorisationUpdateDO>>
{
    public static final Logger LOG = LoggerFactory.getLogger(RISAuthUpdateProcessor.class);

    private static final String UNABLE_TO_MAP_COUNTRY = "Unable to map flag name %s, update Flag reference data";
    private static final String UNABLE_TO_MAP_SERVICE = "Unable to map service code %s, update Service reference data";

    @Autowired
    private FlagStateRepository flagStateRepository;

    @Autowired
    private ServiceCatalogueRepository serviceCatalogueRepository;

    @Autowired
    private ServiceCatalogueFlagAdministrationRepository serviceCatalogueFlagAdministrationRepository;

    protected abstract void innerProcess(final RISAuthorisationUpdateDO risAuthorisationUpdateDO,
            final List<ServiceCatalogueFlagAdministrationDO> newUpdatedEntities,
            final Long flagAdministrationID, final Long surveyID, final boolean linkExists);

    protected abstract List<String> validOperations();

    @Override
    public StagingPairDto<List<ServiceCatalogueFlagAdministrationDO>, RISAuthorisationUpdateDO> process(
            final RISAuthorisationUpdateDO risAuthorisationUpdateDO)
    {
        if (!validOperations().contains(risAuthorisationUpdateDO.getOperation()))
        {
            // Just to be extra safe,
            throw new MastSystemException("Invalid Operation type passed to RIS Processor");
        }

        final String flagName = risAuthorisationUpdateDO.getFlagName();

        // 99% of time this will be a single record but not necessarily
        final List<Long> flagAdministrationIDs = this.flagStateRepository.findIDsByName(flagName);
        final List<ServiceCatalogueFlagAdministrationDO> newUpdatedEntities = new ArrayList<>();

        if (flagAdministrationIDs.isEmpty())
        {
            LOG.warn(String.format(UNABLE_TO_MAP_COUNTRY, flagName));
            risAuthorisationUpdateDO.setExceptionFlag(true);
            risAuthorisationUpdateDO.setFailureReason(String.format(UNABLE_TO_MAP_COUNTRY, flagName));
        }

        else
        {
            for (final Long flagAdministrationID : flagAdministrationIDs)
            {
                final String surveyCode = risAuthorisationUpdateDO.getSurveyCode();
                final List<Long> surveyIDs = this.serviceCatalogueRepository.findIDsBySurveyCode(surveyCode);

                if (surveyIDs.isEmpty())
                {
                    LOG.warn(String.format(UNABLE_TO_MAP_SERVICE, surveyCode));
                    risAuthorisationUpdateDO.setExceptionFlag(true);
                    risAuthorisationUpdateDO.setFailureReason(String.format(UNABLE_TO_MAP_SERVICE, surveyCode));
                }
                else
                {
                    for (final Long surveyID : surveyIDs)
                    {
                        final boolean linkAlreadyExists =
                                this.serviceCatalogueFlagAdministrationRepository.countByServiceCatalogAndFlag(surveyID, flagAdministrationID) > 0;
                        innerProcess(risAuthorisationUpdateDO, newUpdatedEntities, flagAdministrationID, surveyID, linkAlreadyExists);
                    }
                }
            }
        }
        final StagingPairDto<List<ServiceCatalogueFlagAdministrationDO>, RISAuthorisationUpdateDO> pair = new StagingPairDto<>();
        pair.setSourceObject(newUpdatedEntities);
        pair.setTargetObject(risAuthorisationUpdateDO);
        return pair;
    }

    protected ServiceCatalogueFlagAdministrationRepository getServiceCatalogueFlagAdministrationRepository()
    {
        return this.serviceCatalogueFlagAdministrationRepository;
    }
}
