package com.baesystems.ai.lr.integration.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.lr.ids.schema.AccountDetailsType;
import org.lr.ids.schema.AddressDetailsType;
import org.lr.ids.schema.CdhDetails;
import org.lr.ids.schema.CdhDetails.Accounts;
import org.lr.ids.schema.CdhDetails.Organization;
import org.lr.ids.schema.CdhDetails.Organization.AddressDetails;
import org.lr.ids.schema.CdhDetails.Organization.ContactDetails;
import org.lr.ids.schema.ContactDetailsType;
import org.lr.ids.schema.OrgDetailsType;
import org.lr.ids.schema.OrgDetailsType.ClientFacingOffices;
import org.lr.ids.schema.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baesystems.ai.lr.dto.customers.AddressDto;
import com.baesystems.ai.lr.dto.customers.OrganisationDto;
import com.baesystems.ai.lr.dto.customers.PartyContactDto;
import com.baesystems.ai.lr.dto.references.OfficeDto;
import com.baesystems.ai.lr.exception.RecordNotFoundException;
import com.baesystems.ai.lr.integration.exception.MandatoryFieldException;
import com.baesystems.ai.lr.service.customers.CustomerService;
import com.baesystems.ai.lr.service.employees.OfficeService;

@Component(value = "CreateUpdateOrgPtProcessor")
public class CreateUpdateOrgPtProcessor implements Processor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUpdateOrgPtProcessor.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OfficeService officeService;

    @Override
    public void process(final Exchange exchange) throws MandatoryFieldException
    {
        final CdhDetails details = exchange.getIn().getBody(CdhDetails.class);

        final OrganisationDto customer = new OrganisationDto();

        final Organization organisation = details.getOrganization();

        if (organisation != null)
        {
            final OrgDetailsType orgDetails = organisation.getOrgDetails();

            if (orgDetails != null)
            {
                this.processDetails(customer, orgDetails);
            }

            final Accounts accounts = details.getAccounts();
            if (accounts != null)
            {
                customer.setJdeReferenceNumber(this.getJdeReference(accounts));
            }

            final AddressDetails addressDetails = organisation.getAddressDetails();
            if (addressDetails != null)
            {
                customer.setAddresses(this.processAddresses(addressDetails.getAddress()));
            }

            final ContactDetails contactDetails = organisation.getContactDetails();
            if (contactDetails != null)
            {
                customer.setContacts(this.processContactDetails(contactDetails));
            }

            this.customerService.saveCustomer(customer);
        }

    }

    private void processDetails(final OrganisationDto customer, final OrgDetailsType orgDetails) throws MandatoryFieldException
    {
        customer.setId(this.parseLong(orgDetails.getPartyId(), "partyId", true));
        customer.setName(this.parseString(orgDetails.getCustomerName(), "name", true));
        customer.setApprovalStatus(this.parseBoolean(orgDetails.getLrApprovedFlag(), "approvalFlag", false));
        customer.setCdhCustomerId(this.parseLong(orgDetails.getCustomerId(), "customerId", true));

        final ClientFacingOffices offices = orgDetails.getClientFacingOffices();
        if (offices != null)
        {
            customer.setOffice(this.getOffice(offices));
        }
    }

    private String getJdeReference(final Accounts accounts)
    {
        String jdeReference = null;
        if (accounts.getAccount() != null && !accounts.getAccount().isEmpty())
        {
            final AccountDetailsType account = accounts.getAccount().get(0);
            jdeReference = account.getJdeReferenceNumber();
        }
        return jdeReference == null ? "" : jdeReference;
    }

    private OfficeDto getOffice(final ClientFacingOffices offices)
    {

        OfficeDto office = null;
        if (offices != null)
        {
            final List<String> officeList = offices.getOffice();
            if (officeList != null && !officeList.isEmpty())
            {
                final String code = officeList.get(0);
                try
                {
                    office = officeService.getOfficeByCode(code);
                }
                catch (final RecordNotFoundException exception)
                {
                    LOGGER.warn("getOffice - unable to find office from :" + code + " so ignoring");
                }
            }
        }
        return office;
    }

    private List<AddressDto> processAddresses(final AddressDetailsType addressDetails) throws MandatoryFieldException
    {
        final List<AddressDto> addresses = new ArrayList<AddressDto>();

        if (addressDetails != null)
        {
            final AddressDto address = new AddressDto();
            address.setAddressLine1(this.parseString(addressDetails.getAddressLine1(), "addressLine1", false));
            address.setAddressLine2(this.parseString(addressDetails.getAddressLine2(), "addressLine2", false));
            address.setAddressLine3(this.parseString(addressDetails.getAddressLine3(), "addressLine3", false));

            address.setTown(this.parseString(addressDetails.getAddressLine4(), "town", false));
            address.setCity(this.parseString(addressDetails.getCity(), "city", false));
            // address.setCountry(processCountry(addressDetails.getCountry()));
            address.setPostcode(this.parseString(addressDetails.getPostalCode(), "postcode", false));

            addresses.add(address);
        }
        return addresses;
    }

    private List<PartyContactDto> processContactDetails(final ContactDetails contactDetails) throws MandatoryFieldException
    {
        final List<PartyContactDto> contacts = new ArrayList<PartyContactDto>();
        for (final ContactDetailsType contact : contactDetails.getContact())
        {
            if (contact != null && contact.getPhones() != null && contact.getPhones().getPhone() != null)
            {
                for (final PhoneType phone : contact.getPhones().getPhone())
                {
                    final PartyContactDto contactDetail = new PartyContactDto();
                    contactDetail.setExtension(this.parseInteger(phone.getExtension(), "extension", false));
                    contactDetail.setPhoneNumber(this.parseString(phone.getNumber(), "phoneNumber", false));

                    // Mandatory fields not available in WSDL
                    contactDetail.setFirstName("");
                    contactDetail.setLastName("");
                    contactDetail.setMiddleName("");
                    contactDetail.setTitle("");

                    contacts.add(contactDetail);
                }
            }
        }
        return contacts;
    }

    private Boolean parseBoolean(final String flag, final String field, final boolean isMandatory) throws MandatoryFieldException
    {
        if (isMandatory)
        {
            verifyMandatoryField(flag, field);
        }

        if (flag == null)
        {
            LOGGER.warn("parseBoolean - missing value for" + field);
        }
        return flag == null ? Boolean.FALSE : Boolean.valueOf(flag);
    }

    public String parseString(final String value, final String field, final boolean isMandatory) throws MandatoryFieldException
    {
        if (isMandatory)
        {
            verifyMandatoryField(value, field);
        }

        if (value == null)
        {
            LOGGER.warn("parseString - missing value for" + field);
        }

        return value == null ? "" : value.trim();
    }

    private Integer parseInteger(final String intValue, final String field, final boolean isMandatory) throws MandatoryFieldException
    {
        Integer result = null;
        if (isMandatory)
        {
            verifyMandatoryField(intValue, field);
        }
        try
        {
            result = Integer.valueOf(intValue);
        }
        catch (final NumberFormatException formatException)
        {
            LOGGER.warn("parseInteger - unable to parse int value from:" + intValue + "," + field);
        }

        return result;
    }

    private Long parseLong(final String longValue, final String field, final boolean isMandatory) throws MandatoryFieldException
    {
        Long result = null;
        if (isMandatory)
        {
            verifyMandatoryField(longValue, field);
        }

        try
        {
            result = Long.valueOf(longValue);
        }
        catch (final NumberFormatException formatException)
        {
            LOGGER.warn("parseLong - unable to parse long value from:" + longValue + "," + field);
        }

        return result;
    }

    private void verifyMandatoryField(final String value, final String field) throws MandatoryFieldException
    {
        if (value == null || value.isEmpty())
        {
            final String message = String.format("Mandatory field not provided: %s", field);
            LOGGER.warn(message);
            throw new MandatoryFieldException(message);
        }
    }
}
