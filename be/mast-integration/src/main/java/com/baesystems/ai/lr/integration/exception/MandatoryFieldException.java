package com.baesystems.ai.lr.integration.exception;

public class MandatoryFieldException extends Exception
{
    private static final long serialVersionUID = -1086045713402335218L;

    public MandatoryFieldException(final String message)
    {
        super(message);
    }
}
