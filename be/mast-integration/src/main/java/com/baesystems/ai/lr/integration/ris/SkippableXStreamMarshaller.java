package com.baesystems.ai.lr.integration.ris;

import com.thoughtworks.xstream.XStream;
import org.springframework.oxm.xstream.XStreamMarshaller;

public class SkippableXStreamMarshaller extends XStreamMarshaller
{
    @Override
    protected void configureXStream(final XStream xstream)
    {
        super.configureXStream(xstream);
        xstream.ignoreUnknownElements();
    }
}
