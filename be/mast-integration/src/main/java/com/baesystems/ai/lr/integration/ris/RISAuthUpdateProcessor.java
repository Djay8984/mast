package com.baesystems.ai.lr.integration.ris;

import java.util.Arrays;
import java.util.List;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.domain.mast.entities.LinkDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueFlagAdministrationDO;

/**
 * Update processor, processes INSERT and UPDATE operations, creates the appropriate link records for country and survey
 * if the one does not already exist.
 */
public class RISAuthUpdateProcessor extends AbstractRISAuthProcessor
{
    private static final String LINK_ALREADY_EXISTS = "link already exists";
    private static final List<String> VALID_OPERATIONS = Arrays.asList("INSERT", "UPDATE");

    @Override
    protected void innerProcess(final RISAuthorisationUpdateDO risAuthorisationUpdateDO,
            final List<ServiceCatalogueFlagAdministrationDO> newUpdatedEntities, final Long flagAdministrationID, final Long surveyID,
            final boolean linkExists)
    {
        if (linkExists)
        {
            risAuthorisationUpdateDO.setExceptionFlag(true);
            risAuthorisationUpdateDO.setFailureReason(LINK_ALREADY_EXISTS);
            LOG.warn(String.format(LINK_ALREADY_EXISTS + "for %s. ignoring", risAuthorisationUpdateDO.getFlagName()));
        }
        else
        {
            final ServiceCatalogueFlagAdministrationDO scfaDO = new ServiceCatalogueFlagAdministrationDO();
            scfaDO.setFlagStateDO(new LinkDO());
            scfaDO.getFlagStateDO().setId(flagAdministrationID);
            scfaDO.setServiceCatalogueDO(new LinkDO());
            scfaDO.getServiceCatalogueDO().setId(surveyID);
            newUpdatedEntities.add(scfaDO);
        }
    }

    @Override
    protected List<String> validOperations()
    {
        return VALID_OPERATIONS;
    }
}
