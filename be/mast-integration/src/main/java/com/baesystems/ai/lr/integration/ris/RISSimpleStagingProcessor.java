package com.baesystems.ai.lr.integration.ris;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.domain.mast.entities.references.ServiceCatalogueFlagAdministrationDO;
import com.baesystems.ai.lr.integration.util.StagingPairDto;
import org.springframework.batch.item.ItemProcessor;

import java.util.List;

public class RISSimpleStagingProcessor
        implements ItemProcessor<StagingPairDto<List<ServiceCatalogueFlagAdministrationDO>, RISAuthorisationUpdateDO>, RISAuthorisationUpdateDO>
{
    /**
     * This is used in conjunction with JPAStagingTask, we don't need to actually change any details on the stagingDO here as
     * we have already done that in a previous process step we just need to return it.
     */
    @Override
    public RISAuthorisationUpdateDO process(
            final StagingPairDto<List<ServiceCatalogueFlagAdministrationDO>, RISAuthorisationUpdateDO> listRISAuthorisationUpdateDOStagingPairDto)
    {
        return listRISAuthorisationUpdateDOStagingPairDto.getTargetObject();
    }
}
