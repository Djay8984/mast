package com.baesystems.ai.lr.integration.ris;

import com.baesystems.ai.lr.integration.util.AbstractReportHeader;

public class RISReportHeader extends AbstractReportHeader
{
    private static final String STAGING_ID = "Staging ID";
    private static final String OPERATION = "Operation";
    private static final String FLAG_NAME = "Country name";
    private static final String SURVEY_CODE = "Survey Code";
    private static final String REASON = "Failure reason";

    @Override
    protected String getFailureHeader()
    {
        return STAGING_ID + getDelimiter()
                + OPERATION + getDelimiter()
                + FLAG_NAME + getDelimiter()
                + SURVEY_CODE + getDelimiter()
                + REASON + getDelimiter();
    }

    @Override
    protected String getSuccessHeader()
    {
        return STAGING_ID + getDelimiter()
                + OPERATION + getDelimiter()
                + FLAG_NAME + getDelimiter()
                + SURVEY_CODE + getDelimiter();
    }
}
