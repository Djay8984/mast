package com.baesystems.ai.lr.integration.ris.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.Objects;

/**
 * Dto representation of the Authority update data that is imported from RIS.
 */
@XStreamAlias("T_AUTHORISATIONS")
public class RISAuthorisationDto implements RISDto
{
    private static final long serialVersionUID = 4696246777871241917L;
    private static final int HASHCODE_BASE = 31;

    @XStreamAlias("OPERATION")
    private String operation;

    @XStreamAlias("certificate_code")
    private String certificateCode;

    @XStreamAlias("flag_code")
    private String flagCode;

    @XStreamAlias("survey_code")
    private String surveyCode;

    @XStreamAlias("authorisation_indicator")
    private String authorisationIndicator;

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(final String operation)
    {
        this.operation = operation;
    }

    public String getCertificateCode()
    {
        return certificateCode;
    }

    public void setCertificateCode(final String certificateCode)
    {
        this.certificateCode = certificateCode;
    }

    public String getFlagCode()
    {
        return flagCode;
    }

    public void setFlagCode(final String flagCode)
    {
        this.flagCode = flagCode;
    }

    public String getSurveyCode()
    {
        return surveyCode;
    }

    public void setSurveyCode(final String surveyCode)
    {
        this.surveyCode = surveyCode;
    }

    public String getAuthorisationIndicator()
    {
        return authorisationIndicator;
    }

    public void setAuthorisationIndicator(final String authorisationIndicator)
    {
        this.authorisationIndicator = authorisationIndicator;
    }

    @Override
    public boolean equals(final Object other)
    {
        boolean result;
        if (this == other)
        {
            result = true;
        }
        else if (other == null || getClass() != other.getClass())
        {
            result = false;
        }
        else
        {
            final RISAuthorisationDto that = (RISAuthorisationDto) other;
            result = Objects.equals(operation, that.operation)
                    && Objects.equals(flagCode, that.flagCode)
                    && Objects.equals(surveyCode, that.surveyCode)
                    && Objects.equals(authorisationIndicator, that.authorisationIndicator);
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        int result = operation.hashCode();
        result = HASHCODE_BASE * result + flagCode.hashCode();
        result = HASHCODE_BASE * result + surveyCode.hashCode();
        result = HASHCODE_BASE * result + authorisationIndicator.hashCode();
        return result;
    }
}
