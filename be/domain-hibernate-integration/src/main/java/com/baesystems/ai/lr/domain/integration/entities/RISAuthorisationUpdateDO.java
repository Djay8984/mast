package com.baesystems.ai.lr.domain.integration.entities;

import com.baesystems.ai.lr.domain.entities.BaseDO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STAGING_RIS_AUTH_UPDATE")
public class RISAuthorisationUpdateDO extends BaseDO
{
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "OPERATION")
    private String operation;

    @Column(name = "FLAG_NAME")
    private String flagName;

    @Column(name = "SURVEY_CODE")
    private String surveyCode;

    @Column(name = "AUTHORISATION_INDICATOR")
    private String authorisationIndicator;

    @Column(name = "JOB_EXECUTION_ID")
    private Long jobExecutionId;

    @Column(name = "WARNING_FLAG")
    private Boolean warningFlag;

    @Column(name = "EXCEPTION_CHECKSUM_FLAG")
    private Boolean exceptionChecksumFlag;

    @Column(name = "EXCEPTION_FLAG")
    private Boolean exceptionFlag;

    @Column(name = "FAILURE_REASON")
    private String failureReason;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(final String operation)
    {
        this.operation = operation;
    }

    public String getFlagName()
    {
        return flagName;
    }

    public void setFlagName(final String flagName)
    {
        this.flagName = flagName;
    }

    public String getSurveyCode()
    {
        return surveyCode;
    }

    public void setSurveyCode(final String surveyCode)
    {
        this.surveyCode = surveyCode;
    }

    public String getAuthorisationIndicator()
    {
        return authorisationIndicator;
    }

    public void setAuthorisationIndicator(final String authorisationIndicator)
    {
        this.authorisationIndicator = authorisationIndicator;
    }

    public Long getJobExecutionId()
    {
        return jobExecutionId;
    }

    public void setJobExecutionId(final Long jobExecutionId)
    {
        this.jobExecutionId = jobExecutionId;
    }

    public Boolean getWarningFlag()
    {
        return warningFlag;
    }

    public void setWarningFlag(final Boolean warningFlag)
    {
        this.warningFlag = warningFlag;
    }

    public Boolean getExceptionChecksumFlag()
    {
        return exceptionChecksumFlag;
    }

    public void setExceptionChecksumFlag(final Boolean exceptionChecksumFlag)
    {
        this.exceptionChecksumFlag = exceptionChecksumFlag;
    }

    public Boolean getExceptionFlag()
    {
        return exceptionFlag;
    }

    public void setExceptionFlag(final Boolean exceptionFlag)
    {
        this.exceptionFlag = exceptionFlag;
    }

    public String getFailureReason()
    {
        return failureReason;
    }

    public void setFailureReason(final String failureReason)
    {
        this.failureReason = failureReason;
    }
}
