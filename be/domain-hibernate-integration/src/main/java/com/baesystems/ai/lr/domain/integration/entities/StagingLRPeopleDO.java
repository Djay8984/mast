package com.baesystems.ai.lr.domain.integration.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.entities.BaseDO;

@Entity
@Table(name = "STAGING_LRPEOPLE")
public class StagingLRPeopleDO extends BaseDO
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "job_execution_id")
    private Long jobExecutionId;

    @Column(name = "person_id")
    private String personId;

    @Column(name = "oneworld_number")
    private String oneWorldNumber;

    @Column(name = "title")
    private String title;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleNames;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "job_group")
    private String jobGroup;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "stream")
    private String stream;

    @Column(name = "department")
    private String department;

    @Column(name = "work_phone")
    private String workPhone;

    @Column(name = "work_mobile")
    private String workMobile;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "legal_entity")
    private String legalEntity;

    @Column(name = "network_user_id")
    private String networkUserId;

    @Column(name = "work_location_name")
    private String workLocationName;

    @Column(name = "work_address_style")
    private String workAddressStyle;

    @Column(name = "work_address_line1")
    private String workAddressLine1;

    @Column(name = "work_address_line2")
    private String workAddressLine2;

    @Column(name = "work_address_line3")
    private String workAddressLine3;

    @Column(name = "work_town_or_city")
    private String workTownOrCity;

    @Column(name = "work_postcode")
    private String workPostcode;

    @Column(name = "work_country")
    private String workCountry;

    @Column(name = "work_region1")
    private String workRegion1;

    @Column(name = "work_region2")
    private String workRegion2;

    @Column(name = "work_region3")
    private String workRegion3;    
    
    @Column(name = "exception_length_flag")
    private Boolean exceptionLength;
    
    @Column(name = "exception_duplicate_flag")
    private Boolean exceptionDuplicate;
    
    @Column(name = "exception_checksum_flag")
    private Boolean exceptionChecksum;

    @Column(name = "exception_flag")
    private Boolean exception;

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

	public Long getJobExecutionId()
    {
        return jobExecutionId;
    }

    public void setJobExecutionId(final Long jobExecutionId)
    {
        this.jobExecutionId = jobExecutionId;
    }

    public String getPersonId()
    {
        return personId;
    }

    public void setPersonId(final String personId)
    {
        this.personId = personId;
    }

    public String getOneWorldNumber()
    {
        return oneWorldNumber;
    }

    public void setOneWorldNumber(final String oneWorldNumber)
    {
        this.oneWorldNumber = oneWorldNumber;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    public String getMiddleNames()
    {
        return middleNames;
    }

    public void setMiddleNames(final String middleNames)
    {
        this.middleNames = middleNames;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    public String getJobGroup()
    {
        return jobGroup;
    }

    public void setJobGroup(final String jobGroup)
    {
        this.jobGroup = jobGroup;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(final String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    public String getStream()
    {
        return stream;
    }

    public void setStream(final String stream)
    {
        this.stream = stream;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getLegalEntity()
    {
        return legalEntity;
    }

    public void setLegalEntity(final String legalEntity)
    {
        this.legalEntity = legalEntity;
    }

    public String getNetworkUserId()
    {
        return networkUserId;
    }

    public void setNetworkUserId(final String networkUserId)
    {
        this.networkUserId = networkUserId;
    }

    public String getWorkAddressStyle()
    {
        return workAddressStyle;
    }

    public void setWorkAddressStyle(final String workAddressStyle)
    {
        this.workAddressStyle = workAddressStyle;
    }

    public String getWorkAddressLine1()
    {
        return workAddressLine1;
    }

    public void setWorkAddressLine1(final String workAddressLine1)
    {
        this.workAddressLine1 = workAddressLine1;
    }

    public String getWorkAddressLine2()
    {
        return workAddressLine2;
    }

    public void setWorkAddressLine2(final String workAddressLine2)
    {
        this.workAddressLine2 = workAddressLine2;
    }

    public String getWorkAddressLine3()
    {
        return workAddressLine3;
    }

    public void setWorkAddressLine3(final String workAddressLine3)
    {
        this.workAddressLine3 = workAddressLine3;
    }

    public String getWorkTownOrCity()
    {
        return workTownOrCity;
    }

    public void setWorkTownOrCity(final String workTownOrCity)
    {
        this.workTownOrCity = workTownOrCity;
    }

    public String getWorkPostcode()
    {
        return workPostcode;
    }

    public void setWorkPostcode(final String workPostcode)
    {
        this.workPostcode = workPostcode;
    }

    public String getWorkCountry()
    {
        return workCountry;
    }

    public void setWorkCountry(final String workCountry)
    {
        this.workCountry = workCountry;
    }

    public String getDepartment()
    {
        return department;
    }

    public void setDepartment(final String department)
    {
        this.department = department;
    }

    public String getWorkPhone()
    {
        return workPhone;
    }

    public void setWorkPhone(final String workPhone)
    {
        this.workPhone = workPhone;
    }

    public Boolean getExceptionLength() {
		return exceptionLength;
	}

	public void setExceptionLength(Boolean exceptionLength) {
		this.exceptionLength = exceptionLength;
	}

	public Boolean getExceptionDuplicate() {
		return exceptionDuplicate;
	}
	
	public void setExceptionDuplicate(Boolean exceptionDuplicate) {
		this.exceptionDuplicate = exceptionDuplicate;
	}

	public String getWorkMobile()
    {
        return workMobile;
    }

    public void setWorkMobile(final String workMobile)
    {
        this.workMobile = workMobile;
    }

    public String getWorkRegion1()
    {
        return workRegion1;
    }

    public void setWorkRegion1(final String workRegion1)
    {
        this.workRegion1 = workRegion1;
    }

    public String getWorkRegion2()
    {
        return workRegion2;
    }

    public void setWorkRegion2(final String workRegion2)
    {
        this.workRegion2 = workRegion2;
    }

    public String getWorkRegion3()
    {
        return workRegion3;
    }

    public void setWorkRegion3(final String workRegion3)
    {
        this.workRegion3 = workRegion3;
    }

    public String getWorkLocationName()
    {
        return workLocationName;
    }

    public void setWorkLocationName(final String workLocationName)
    {
        this.workLocationName = workLocationName;
    }

    public Boolean getExceptionChecksum()
    {
        return exceptionChecksum;
    }

    public void setExceptionChecksum(final Boolean exceptionChecksum)
    {
        this.exceptionChecksum = exceptionChecksum;
    }

    public Boolean getException()
    {
        return exception;
    }

    public void setException(final Boolean exception)
    {
        this.exception = exception;
    }
}
