package com.baesystems.ai.lr.domain.integration.repositories;

import com.baesystems.ai.lr.domain.integration.entities.RISAuthorisationUpdateDO;
import com.baesystems.ai.lr.domain.repositories.SpringDataRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RISAuthorisationUpdateRepository extends SpringDataRepository<RISAuthorisationUpdateDO, Long>
{
    @Query("select count(id) from RISAuthorisationUpdateDO"
            + " where jobExecutionId = :jobExecutionId"
            + " and (failureReason is null or failureReason = '')"
            + " and (exceptionFlag is null or exceptionFlag = false)"
            + " and (warningFlag is null or warningFlag = false)")
    Integer numberOfSuccessfulRecords(@Param("jobExecutionId") Long jobExecutionId);
}
