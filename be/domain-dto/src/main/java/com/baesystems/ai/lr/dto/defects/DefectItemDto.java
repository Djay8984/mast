package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class DefectItemDto extends BaseDto
{
    private static final long serialVersionUID = 2207746146624404895L;

    @NotNull
    @SubIdNotNull
    @Valid
    private ItemLightDto item;

    @Valid
    private LinkResource defect;

    @Valid
    private LinkResource parent;

    public ItemLightDto getItem()
    {
        return item;
    }

    public void setItem(final ItemLightDto item)
    {
        this.item = item;
    }

    public LinkResource getDefect()
    {
        return defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }
}
