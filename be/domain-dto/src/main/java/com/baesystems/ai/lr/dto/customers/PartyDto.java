package com.baesystems.ai.lr.dto.customers;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.references.OfficeDto;

public class PartyDto extends PartyBaseDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    private Long cdhCustomerId;

    @Size(message = "invalid length", max = 240)
    private String jdeReferenceNumber;

    @Min(value = 1)
    @Max(value = 9999999)
    private Long imoNumber;

    @Size(message = "invalid length", max = 20)
    private String phoneNumber;

    @Size(message = "invalid length", max = 20)
    private String faxNumber;

    @SubIdNotNull
    @Valid
    private OfficeDto office;

    @Valid
    private List<PartyContactDto> contacts;

    @Valid
    private List<AddressDto> addresses;

    public List<PartyContactDto> getContacts()
    {
        return contacts;
    }

    public void setContacts(final List<PartyContactDto> contacts)
    {
        this.contacts = contacts;
    }

    public Long getCdhCustomerId()
    {
        return cdhCustomerId;
    }

    public void setCdhCustomerId(final Long cdhCustomerId)
    {
        this.cdhCustomerId = cdhCustomerId;
    }

    public String getJdeReferenceNumber()
    {
        return jdeReferenceNumber;
    }

    public void setJdeReferenceNumber(final String jdeReferenceNumber)
    {
        this.jdeReferenceNumber = jdeReferenceNumber;
    }

    public Long getImoNumber()
    {
        return imoNumber;
    }

    public void setImoNumber(final Long imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber()
    {
        return faxNumber;
    }

    public void setFaxNumber(final String faxNumber)
    {
        this.faxNumber = faxNumber;
    }

    public OfficeDto getOffice()
    {
        return office;
    }

    public void setOffice(final OfficeDto office)
    {
        this.office = office;
    }

    public List<AddressDto> getAddresses()
    {
        return addresses;
    }

    public void setAddresses(final List<AddressDto> addresses)
    {
        this.addresses = addresses;
    }
}
