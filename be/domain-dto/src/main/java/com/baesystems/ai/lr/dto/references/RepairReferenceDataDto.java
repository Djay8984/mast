package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class RepairReferenceDataDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 4532544517329535901L;

    private List<MaterialDto> materials;

    private List<ReferenceDataDto> repairTypes;

    private List<ReferenceDataDto> repairActions;

    private List<ReferenceDataDto> materialTypes;

    public List<MaterialDto> getMaterials()
    {
        return materials;
    }

    public void setMaterials(final List<MaterialDto> materials)
    {
        this.materials = materials;
    }

    public List<ReferenceDataDto> getRepairTypes()
    {
        return repairTypes;
    }

    public void setRepairTypes(final List<ReferenceDataDto> repairTypes)
    {
        this.repairTypes = repairTypes;
    }

    public List<ReferenceDataDto> getRepairActions()
    {
        return repairActions;
    }

    public void setRepairActions(final List<ReferenceDataDto> repairActions)
    {
        this.repairActions = repairActions;
    }

    public List<ReferenceDataDto> getMaterialTypes()
    {
        return materialTypes;
    }

    public void setMaterialTypes(final List<ReferenceDataDto> materialTypes)
    {
        this.materialTypes = materialTypes;
    }

}
