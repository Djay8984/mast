package com.baesystems.ai.lr.dto.reports;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ReportLightDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -809426423717541874L;

    private String stalenessHash;

    @Size(message = "invalid length", max = 25)
    private String referenceCode;

    private Long reportVersion;

    @Valid
    @NotNull
    private LinkResource reportType;

    @Valid
    @NotNull
    private LinkResource job;

    @Size(message = "invalid length", max = 2000)
    private String classRecommendation;

    @Size(message = "invalid length", max = 2000)
    private String secondClassRecommendation;

    private Date issueDate;

    private Date authorisationDate;

    @Size(message = "invalid length", max = 2000)
    private String notes;

    @Size(message = "invalid length", max = 2000)
    private String confidentialNotes;

    @Valid
    private LinkResource issuedBy;

    @Valid
    private LinkResource authorisedBy;

    @Valid
    private LinkResource endorsedBy;

    private Boolean approved;

    @Size(message = "invalid length", max = 1000)
    private String rejectionComments;

    // This is used to update the associated harmonisation date of an asset when a report is submitted.
    private Date harmonisationDate;

    public String getReferenceCode()
    {
        return this.referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public Long getReportVersion()
    {
        return this.reportVersion;
    }

    public void setReportVersion(final Long reportVersion)
    {
        this.reportVersion = reportVersion;
    }

    public LinkResource getReportType()
    {
        return this.reportType;
    }

    public void setReportType(final LinkResource reportType)
    {
        this.reportType = reportType;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public String getClassRecommendation()
    {
        return this.classRecommendation;
    }

    public void setClassRecommendation(final String classRecommendation)
    {
        this.classRecommendation = classRecommendation;
    }

    public String getSecondClassRecommendation()
    {
        return this.secondClassRecommendation;
    }

    public void setSecondClassRecommendation(final String secondClassRecommendation)
    {
        this.secondClassRecommendation = secondClassRecommendation;
    }

    public Date getIssueDate()
    {
        return this.issueDate;
    }

    public void setIssueDate(final Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public Date getAuthorisationDate()
    {
        return this.authorisationDate;
    }

    public void setAuthorisationDate(final Date authorisationDate)
    {
        this.authorisationDate = authorisationDate;
    }

    public String getNotes()
    {
        return this.notes;
    }

    public void setNotes(final String notes)
    {
        this.notes = notes;
    }

    public String getConfidentialNotes()
    {
        return this.confidentialNotes;
    }

    public void setConfidentialNotes(final String confidentialNotes)
    {
        this.confidentialNotes = confidentialNotes;
    }

    public LinkResource getIssuedBy()
    {
        return this.issuedBy;
    }

    public void setIssuedBy(final LinkResource issuedBy)
    {
        this.issuedBy = issuedBy;
    }

    public LinkResource getAuthorisedBy()
    {
        return this.authorisedBy;
    }

    public void setAuthorisedBy(final LinkResource authorisedBy)
    {
        this.authorisedBy = authorisedBy;
    }

    public LinkResource getEndorsedBy()
    {
        return this.endorsedBy;
    }

    public void setEndorsedBy(final LinkResource endorsedBy)
    {
        this.endorsedBy = endorsedBy;
    }

    public Boolean getApproved()
    {
        return this.approved;
    }

    public void setApproved(final Boolean approved)
    {
        this.approved = approved;
    }

    public String getRejectionComments()
    {
        return this.rejectionComments;
    }

    public void setRejectionComments(final String rejectionComments)
    {
        this.rejectionComments = rejectionComments;
    }

    public Date getHarmonisationDate()
    {
        return this.harmonisationDate;
    }

    public void setHarmonisationDate(final Date harmonisationDate)
    {
        this.harmonisationDate = harmonisationDate;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "ReportLightDto [referenceCode=" + this.referenceCode
                + ", reportType=" + getIdOrNull(this.reportType)
                + ", job=" + getIdOrNull(this.job)
                + ", classRecommendation=" + this.classRecommendation
                + ", secondClassRecommendation=" + this.secondClassRecommendation
                + ", issueDate=" + getStringRepresentationOfDate(this.issueDate)
                + ", authorisationDate=" + getStringRepresentationOfDate(this.authorisationDate)
                + ", notes=" + this.notes
                + ", confidentialNotes=" + this.confidentialNotes
                + ", issuedBy=" + getIdOrNull(this.issuedBy)
                + ", authorisedBy=" + getIdOrNull(this.authorisedBy)
                + ", endorsedBy=" + getIdOrNull(this.endorsedBy)
                + ", approved=" + this.approved
                + ", rejectionComments=" + this.rejectionComments
                + ", harmonisationDate=" + getStringRepresentationOfDate(this.harmonisationDate)
                + ", getId()=" + getId() + "]";
    }
}
