package com.baesystems.ai.lr.dto.ihs;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsAssetDetailsDto extends BaseDto
{

    private static final long serialVersionUID = 7292783580182959304L;

    @Valid
    private IhsAssetDto ihsAsset;

    @Valid
    private IhsGroupFleetDto ihsGroupFleet;

    @Valid
    private IhsHistDto ihsHist;

    public IhsAssetDto getIhsAsset()
    {
        return ihsAsset;
    }

    public void setIhsAsset(final IhsAssetDto ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public IhsGroupFleetDto getIhsGroupFleet()
    {
        return ihsGroupFleet;
    }

    public void setIhsGroupFleet(final IhsGroupFleetDto ihsGroupFleet)
    {
        this.ihsGroupFleet = ihsGroupFleet;
    }

    public IhsHistDto getIhsHist()
    {
        return ihsHist;
    }

    public void setIhsHist(final IhsHistDto ihsHist)
    {
        this.ihsHist = ihsHist;
    }
}
