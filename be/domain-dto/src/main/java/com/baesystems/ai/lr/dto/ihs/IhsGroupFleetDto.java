package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsGroupFleetDto extends BaseDto
{

    private static final long serialVersionUID = -7129815958921500826L;

    @Size(message = "invalid length", max = 7)
    private String regdOwner;

    @Size(message = "invalid length", max = 7)
    private String groupOwner;

    public String getRegdOwner()
    {
        return regdOwner;
    }

    public void setRegdOwner(final String regdOwner)
    {
        this.regdOwner = regdOwner;
    }

    public String getGroupOwner()
    {
        return groupOwner;
    }

    public void setGroupOwner(final String groupOwner)
    {
        this.groupOwner = groupOwner;
    }
}
