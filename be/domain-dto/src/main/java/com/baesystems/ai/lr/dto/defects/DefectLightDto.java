package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.references.DefectCategoryDto;

public class DefectLightDto extends FaultDto
{
    private static final long serialVersionUID = 4241397782209250604L;

    @NotNull
    @Valid
    private DefectCategoryDto defectCategory;

    @Valid
    private LinkResource defectStatus;

    @Valid
    private List<DefectDefectValueDto> values;

    @Valid
    private List<DefectItemDto> items;

    private Integer repairCount;
    private Integer courseOfActionCount;
    private LinkResource parent;

    public DefectCategoryDto getDefectCategory()
    {
        return defectCategory;
    }

    public void setDefectCategory(final DefectCategoryDto defectCategory)
    {
        this.defectCategory = defectCategory;
    }

    public LinkResource getDefectStatus()
    {
        return defectStatus;
    }

    public void setDefectStatus(final LinkResource defectStatus)
    {
        this.defectStatus = defectStatus;
    }

    public List<DefectDefectValueDto> getValues()
    {
        return values;
    }

    public void setValues(final List<DefectDefectValueDto> values)
    {
        this.values = values;
    }

    public List<DefectItemDto> getItems()
    {
        return items;
    }

    public void setItems(final List<DefectItemDto> items)
    {
        this.items = items;
    }

    @Override
    public Integer getRepairCount()
    {
        return repairCount;
    }

    @Override
    public void setRepairCount(final Integer repairCount)
    {
        this.repairCount = repairCount;
    }

    @Override
    public Integer getCourseOfActionCount()
    {
        return courseOfActionCount;
    }

    @Override
    public void setCourseOfActionCount(final Integer courseOfActionCount)
    {
        this.courseOfActionCount = courseOfActionCount;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }


}
