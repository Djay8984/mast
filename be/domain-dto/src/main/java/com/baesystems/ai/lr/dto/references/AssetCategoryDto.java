package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;

public class AssetCategoryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 7710879100036109253L;

    @Valid
    private LinkResource businessProcess;

    public final LinkResource getBusinessProcess()
    {
        return businessProcess;
    }

    public void setBusinessProcess(final LinkResource businessProcess)
    {
        this.businessProcess = businessProcess;
    }

}
