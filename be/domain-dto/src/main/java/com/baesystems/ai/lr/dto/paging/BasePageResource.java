package com.baesystems.ai.lr.dto.paging;

public abstract class BasePageResource<T> implements PageResource<T>
{

    private PaginationDto pagination;

    @Override
    public PaginationDto getPagination()
    {
        return pagination;
    }

    @Override
    public void setPagination(final PaginationDto pagination)
    {
        this.pagination = pagination;
    }
}
