package com.baesystems.ai.lr.dto.jobs;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CertificateDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -447851452971444204L;

    private String stalenessHash;

    @JsonIgnore
    private Boolean approved;

    @Size(message = "invalid length", max = 25)
    private String certificateNumber;

    @Valid
    private LinkResource certificateStatus;

    @NotNull
    @Valid
    private LinkResource certificateType;

    @Valid
    private LinkResource certificateAction;

    private Date expiryDate;

    private Date extendedDate;

    private Date issueDate;

    @SubIdNotNull
    private LinkResource office;

    @SubIdNotNull
    private LinkResource employee;

    @Valid
    private LinkResource job;

    @JsonIgnore
    @Valid
    private LinkResource parent;

    public String getCertificateNumber()
    {
        return this.certificateNumber;
    }

    public void setCertificateNumber(final String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public LinkResource getCertificateStatus()
    {
        return this.certificateStatus;
    }

    public void setCertificateStatus(final LinkResource certificateStatus)
    {
        this.certificateStatus = certificateStatus;
    }

    public LinkResource getCertificateType()
    {
        return this.certificateType;
    }

    public void setCertificateType(final LinkResource certificateType)
    {
        this.certificateType = certificateType;
    }

    public LinkResource getCertificateAction()
    {
        return this.certificateAction;
    }

    public void setCertificateAction(final LinkResource certificateAction)
    {
        this.certificateAction = certificateAction;
    }

    public Date getExpiryDate()
    {
        return this.expiryDate;
    }

    public void setExpiryDate(final Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Date getExtendedDate()
    {
        return this.extendedDate;
    }

    public void setExtendedDate(final Date extendedDate)
    {
        this.extendedDate = extendedDate;
    }

    public Date getIssueDate()
    {
        return this.issueDate;
    }

    public void setIssueDate(final Date issueDate)
    {
        this.issueDate = issueDate;
    }

    public LinkResource getOffice()
    {
        return this.office;
    }

    public void setOffice(final LinkResource office)
    {
        this.office = office;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    @JsonProperty
    public Boolean getApproved()
    {
        return this.approved;
    }

    @JsonIgnore
    public void setApproved(final Boolean approved)
    {
        this.approved = approved;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CertificateDto [certificateNumber=" + this.certificateNumber
                + ", certificateStatus=" + getIdOrNull(this.certificateStatus)
                + ", certificateType=" + getIdOrNull(this.certificateType)
                + ", certificateAction=" + getIdOrNull(this.certificateAction)
                + ", expiryDate=" + getStringRepresentationOfDate(this.expiryDate)
                + ", extendedDate=" + getStringRepresentationOfDate(this.extendedDate)
                + ", issueDate=" + getStringRepresentationOfDate(this.issueDate)
                + ", office=" + getIdOrNull(this.office)
                + ", employee=" + getIdOrNull(this.employee)
                + ", job=" + getIdOrNull(this.job)
                + ", getId()=" + getId() + "]";
    }
}
