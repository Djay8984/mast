package com.baesystems.ai.lr.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NamedLinkResource extends LinkResource
{
    private static final long serialVersionUID = 1914884139860649971L;

    @JsonIgnore
    private String name;

    @JsonProperty
    public String getName()
    {
        return name;
    }

    @JsonIgnore
    public void setName(final String name)
    {
        this.name = name;
    }
}
