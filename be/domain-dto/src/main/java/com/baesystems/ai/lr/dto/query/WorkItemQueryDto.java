package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class WorkItemQueryDto implements QueryDto
{
    private static final long serialVersionUID = 7993937122049302942L;

    private List<Long> surveyId;

    private List<Long> scheduledServiceId;

    private List<Long> itemId;

    public List<Long> getItemId()
    {
        return itemId;
    }

    public void setItemId(final List<Long> itemId)
    {
        this.itemId = itemId;
    }

    public List<Long> getSurveyId()
    {
        return surveyId;
    }

    public void setSurveyId(final List<Long> surveyId)
    {
        this.surveyId = surveyId;
    }

    public List<Long> getScheduledServiceId()
    {
        return scheduledServiceId;
    }

    public void setScheduledServiceId(final List<Long> scheduledServiceId)
    {
        this.scheduledServiceId = scheduledServiceId;
    }
}
