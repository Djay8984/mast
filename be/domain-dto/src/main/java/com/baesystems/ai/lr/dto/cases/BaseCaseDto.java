package com.baesystems.ai.lr.dto.cases;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.CustomerLinkDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.employees.CaseSurveyorWithLinksDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;

public class BaseCaseDto extends BaseDto
{
    private static final long serialVersionUID = 9166544449172271824L;

    @NotNull
    @Valid
    private LinkResource caseType;

    @NotNull
    @Valid
    private LinkResource caseStatus;

    @Valid
    private LinkResource previousCaseStatus;

    @Valid
    private LinkResource preEicInspectionStatus;

    @Valid
    private LinkResource riskAssessmentStatus;

    private Date tocAcceptanceDate;

    @Size(message = "invalid length", max = 50)
    private String contractReferenceNumber;

    @Valid
    private List<CaseSurveyorWithLinksDto> surveyors;

    @Valid
    private List<OfficeLinkDto> offices;

    @Valid
    private List<CustomerLinkDto> customers;

    private Date createdOn;

    private Date caseAcceptanceDate;

    private Date caseClosedDate;

    @Valid
    private List<CaseMilestoneDto> milestones;

    @Size(message = "invalid length", max = 500)
    private String statusReason;

    public LinkResource getCaseType()
    {
        return caseType;
    }

    public void setCaseType(final LinkResource caseType)
    {
        this.caseType = caseType;
    }

    public LinkResource getCaseStatus()
    {
        return caseStatus;
    }

    public void setCaseStatus(final LinkResource caseStatus)
    {
        this.caseStatus = caseStatus;
    }

    public LinkResource getPreviousCaseStatus()
    {
        return previousCaseStatus;
    }

    public void setPreviousCaseStatus(final LinkResource previousCaseStatus)
    {
        this.previousCaseStatus = previousCaseStatus;
    }

    public LinkResource getPreEicInspectionStatus()
    {
        return preEicInspectionStatus;
    }

    public void setPreEicInspectionStatus(final LinkResource preEicInspectionStatus)
    {
        this.preEicInspectionStatus = preEicInspectionStatus;
    }

    public LinkResource getRiskAssessmentStatus()
    {
        return riskAssessmentStatus;
    }

    public void setRiskAssessmentStatus(final LinkResource riskAssessmentStatus)
    {
        this.riskAssessmentStatus = riskAssessmentStatus;
    }

    public Date getTocAcceptanceDate()
    {
        return tocAcceptanceDate;
    }

    public void setTocAcceptanceDate(final Date tocAcceptanceDate)
    {
        this.tocAcceptanceDate = tocAcceptanceDate;
    }

    public String getContractReferenceNumber()
    {
        return contractReferenceNumber;
    }

    public void setContractReferenceNumber(final String contractReferenceNumber)
    {
        this.contractReferenceNumber = contractReferenceNumber;
    }

    public List<CaseSurveyorWithLinksDto> getSurveyors()
    {
        return surveyors;
    }

    public void setSurveyors(final List<CaseSurveyorWithLinksDto> surveyors)
    {
        this.surveyors = surveyors;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public List<CustomerLinkDto> getCustomers()
    {
        return customers;
    }

    public void setCustomers(final List<CustomerLinkDto> customers)
    {
        this.customers = customers;
    }

    public Date getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getCaseAcceptanceDate()
    {
        return caseAcceptanceDate;
    }

    public void setCaseAcceptanceDate(final Date caseAcceptanceDate)
    {
        this.caseAcceptanceDate = caseAcceptanceDate;
    }

    public List<CaseMilestoneDto> getMilestones()
    {
        return milestones;
    }

    public void setMilestones(final List<CaseMilestoneDto> milestones)
    {
        this.milestones = milestones;
    }

    public String getStatusReason()
    {
        return statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Date getCaseClosedDate()
    {
        return caseClosedDate;
    }

    public void setCaseClosedDate(final Date caseClosedDate)
    {
        this.caseClosedDate = caseClosedDate;
    }

}
