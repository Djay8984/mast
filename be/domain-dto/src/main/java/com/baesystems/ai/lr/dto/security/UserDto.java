package com.baesystems.ai.lr.dto.security;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class UserDto extends BaseDto
{
    private static final long serialVersionUID = 8957544789826737609L;

    @NotNull
    private String userName;

    @NotNull
    private List<String> groupList;

    private String emailAddress;

    private boolean deleted;

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(final String userName)
    {
        this.userName = userName;
    }

    public List<String> getGroupList()
    {
        return groupList;
    }

    public void setGroupList(final List<String> groupList)
    {
        this.groupList = groupList;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }
}
