package com.baesystems.ai.lr.dto.tasks;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.ReportContentElementDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.references.WorkItemActionDto;
import com.baesystems.ai.lr.dto.services.PostponementDto;

public abstract class BaseWorkItemDto extends ReportContentElementDto implements DueStatusUpdateableDto
{
    private static final long serialVersionUID = 3255354513654790758L;

    @NotNull
    private String referenceCode;

    private String name;

    private String description;

    private String longDescription;

    private String serviceCode;

    @Valid
    private LinkResource workItemType;

    @Valid
    private WorkItemActionDto workItemAction;

    @Valid
    private List<WorkItemAttributeDto> attributes;

    private Boolean attributeMandatory;

    private Date dueDate;

    @Size(message = "invalid length", max = 2000)
    private String notes;

    @Valid
    private LinkResource codicil;

    private Boolean attachmentRequired;

    private Integer itemOrder;

    private Date resolutionDate;

    @Valid
    private LinkResource resolutionStatus;

    @Valid
    private LinkResource classSociety;

    @SubIdNotNull
    private LrEmployeeDto assignedTo;

    private Date assignedDate;

    @SubIdNotNull
    private LrEmployeeDto resolvedBy;

    @Valid
    @NotNull
    private LinkResource creditStatus;

    @NotNull
    @Size(max = 20)
    private String taskNumber;

    @Valid
    private LinkResource parent;

    @Valid
    private LinkResource dueStatus;

    @Valid
    private PostponementDto postponement;

    @Valid
    private LinkResource creditedByJob;

    public String getReferenceCode()
    {
        return this.referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getLongDescription()
    {
        return this.longDescription;
    }

    public void setLongDescription(final String longDescription)
    {
        this.longDescription = longDescription;
    }

    public String getServiceCode()
    {
        return this.serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public LinkResource getWorkItemType()
    {
        return this.workItemType;
    }

    public void setWorkItemType(final LinkResource workItemType)
    {
        this.workItemType = workItemType;
    }

    public WorkItemActionDto getWorkItemAction()
    {
        return this.workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDto workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public Boolean getAttributeMandatory()
    {
        return this.attributeMandatory;
    }

    public void setAttributeMandatory(final Boolean attributeMandatory)
    {
        this.attributeMandatory = attributeMandatory;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getNotes()
    {
        return this.notes;
    }

    public void setNotes(final String notes)
    {
        this.notes = notes;
    }

    public LinkResource getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final LinkResource codicil)
    {
        this.codicil = codicil;
    }

    public Boolean getAttachmentRequired()
    {
        return this.attachmentRequired;
    }

    public void setAttachmentRequired(final Boolean attachmentRequired)
    {
        this.attachmentRequired = attachmentRequired;
    }

    public Integer getItemOrder()
    {
        return this.itemOrder;
    }

    public void setItemOrder(final Integer itemOrder)
    {
        this.itemOrder = itemOrder;
    }

    public Date getResolutionDate()
    {
        return this.resolutionDate;
    }

    public void setResolutionDate(final Date resolutionDate)
    {
        this.resolutionDate = resolutionDate;
    }

    public LinkResource getResolutionStatus()
    {
        return this.resolutionStatus;
    }

    public void setResolutionStatus(final LinkResource resolutionStatus)
    {
        this.resolutionStatus = resolutionStatus;
    }

    public LinkResource getClassSociety()
    {
        return this.classSociety;
    }

    public void setClassSociety(final LinkResource classSociety)
    {
        this.classSociety = classSociety;
    }

    public LrEmployeeDto getAssignedTo()
    {
        return this.assignedTo;
    }

    public void setAssignedTo(final LrEmployeeDto assignedTo)
    {
        this.assignedTo = assignedTo;
    }

    public LrEmployeeDto getResolvedBy()
    {
        return this.resolvedBy;
    }

    public void setResolvedBy(final LrEmployeeDto resolvedBy)
    {
        this.resolvedBy = resolvedBy;
    }

    public LinkResource getCreditStatus()
    {
        return this.creditStatus;
    }

    public void setCreditStatus(final LinkResource creditStatus)
    {
        this.creditStatus = creditStatus;
    }

    public String getTaskNumber()
    {
        return this.taskNumber;
    }

    public void setTaskNumber(final String taskNumber)
    {
        this.taskNumber = taskNumber;
    }

    public List<WorkItemAttributeDto> getAttributes()
    {
        return this.attributes;
    }

    public void setAttributes(final List<WorkItemAttributeDto> attributes)
    {
        this.attributes = attributes;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public LinkResource getCreditedByJob()
    {
        return this.creditedByJob;
    }

    public void setCreditedByJob(final LinkResource creditedByJob)
    {
        this.creditedByJob = creditedByJob;
    }

    public PostponementDto getPostponement()
    {
        return this.postponement;
    }

    public void setPostponement(final PostponementDto postponement)
    {
        this.postponement = postponement;
    }
}
