package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsHiprDto extends BaseDto
{
    private static final long serialVersionUID = -4977275544982579229L;

    @Size(message = "invalid length", max = 4)
    private String propulsion;

    public String getPropulsion()
    {
        return propulsion;
    }

    public void setPropulsion(final String propulsion)
    {
        this.propulsion = propulsion;
    }

}
