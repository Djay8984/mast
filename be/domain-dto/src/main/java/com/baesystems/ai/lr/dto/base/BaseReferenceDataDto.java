package com.baesystems.ai.lr.dto.base;

import com.baesystems.ai.lr.dto.annotations.ValidId;

@ValidId
public class BaseReferenceDataDto extends BaseDto
{
    private static final long serialVersionUID = -7700260036236546617L;

    private Boolean deleted;

    public Boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(final Boolean deleted)
    {
        this.deleted = deleted;
    }
}
