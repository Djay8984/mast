package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.assets.AssetLightDto;
import com.baesystems.ai.lr.dto.assets.AssetModelDto;
import com.baesystems.ai.lr.dto.attachments.SupplementaryInformationDto;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.defects.DefectDto;
import com.baesystems.ai.lr.dto.defects.RepairDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemLightDto;

/**
 * This DTO is to be used to send all the required data for Job, such that a client may work "offline".
 */
public class JobBundleDto extends BaseDto
{
    private static final long serialVersionUID = -5132954694615676811L;

    // Job Specific info
    private JobDto job;
    private List<ReportDto> report;
    private List<SurveyDto> survey;
    private List<WorkItemLightDto> wipTask;
    private List<ActionableItemDto> wipActionableItem;
    private List<CoCDto> wipCoC;
    private List<AssetNoteDto> wipAssetNote;
    private List<DefectDto> wipDefect;
    private List<RepairDto> wipRepair;

    // Asset specific info
    private AssetLightDto asset;
    private AssetModelDto assetModel;
    private List<ActionableItemDto> actionableItem;
    private List<CoCDto> coC;
    private List<AssetNoteDto> assetNote;
    private List<DefectDto> defect;
    private List<RepairDto> repair;
    private List<ScheduledServiceDto> service;
    private List<WorkItemLightDto> task;

    // Other
    private List<SupplementaryInformationDto> attachment;

    public JobDto getJob()
    {
        return job;
    }

    public void setJob(final JobDto job)
    {
        this.job = job;
    }

    public List<ReportDto> getReport()
    {
        return report;
    }

    public void setReport(final List<ReportDto> report)
    {
        this.report = report;
    }

    public List<ActionableItemDto> getWipActionableItem()
    {
        return wipActionableItem;
    }

    public void setWipActionableItem(final List<ActionableItemDto> wipActionableItem)
    {
        this.wipActionableItem = wipActionableItem;
    }

    public List<CoCDto> getWipCoC()
    {
        return wipCoC;
    }

    public void setWipCoC(final List<CoCDto> wipCoC)
    {
        this.wipCoC = wipCoC;
    }

    public List<AssetNoteDto> getWipAssetNote()
    {
        return wipAssetNote;
    }

    public void setWipAssetNote(final List<AssetNoteDto> wipAssetNote)
    {
        this.wipAssetNote = wipAssetNote;
    }

    public List<DefectDto> getWipDefect()
    {
        return wipDefect;
    }

    public void setWipDefect(final List<DefectDto> wipDefect)
    {
        this.wipDefect = wipDefect;
    }

    public List<RepairDto> getWipRepair()
    {
        return wipRepair;
    }

    public void setWipRepair(final List<RepairDto> wipRepair)
    {
        this.wipRepair = wipRepair;
    }

    public AssetLightDto getAsset()
    {
        return asset;
    }

    public void setAsset(final AssetLightDto asset)
    {
        this.asset = asset;
    }

    public AssetModelDto getAssetModel()
    {
        return assetModel;
    }

    public void setAssetModel(final AssetModelDto assetModel)
    {
        this.assetModel = assetModel;
    }

    public List<ActionableItemDto> getActionableItem()
    {
        return actionableItem;
    }

    public void setActionableItem(final List<ActionableItemDto> actionableItem)
    {
        this.actionableItem = actionableItem;
    }

    public List<CoCDto> getCoC()
    {
        return coC;
    }

    public void setCoC(final List<CoCDto> coC)
    {
        this.coC = coC;
    }

    public List<AssetNoteDto> getAssetNote()
    {
        return assetNote;
    }

    public void setAssetNote(final List<AssetNoteDto> assetNote)
    {
        this.assetNote = assetNote;
    }

    public List<DefectDto> getDefect()
    {
        return defect;
    }

    public void setDefect(final List<DefectDto> defect)
    {
        this.defect = defect;
    }

    public List<SurveyDto> getSurvey()
    {
        return survey;
    }

    public void setSurvey(final List<SurveyDto> survey)
    {
        this.survey = survey;
    }

    public List<SupplementaryInformationDto> getAttachment()
    {
        return attachment;
    }

    public void setAttachment(final List<SupplementaryInformationDto> attachment)
    {
        this.attachment = attachment;
    }

    public List<RepairDto> getRepair()
    {
        return repair;
    }

    public void setRepair(final List<RepairDto> repair)
    {
        this.repair = repair;
    }

    public List<ScheduledServiceDto> getService()
    {
        return service;
    }

    public void setService(final List<ScheduledServiceDto> service)
    {
        this.service = service;
    }

    public List<WorkItemLightDto> getWipTask()
    {
        return wipTask;
    }

    public void setWipTask(final List<WorkItemLightDto> wipTask)
    {
        this.wipTask = wipTask;
    }

    public List<WorkItemLightDto> getTask()
    {
        return task;
    }

    public void setTask(final List<WorkItemLightDto> task)
    {
        this.task = task;
    }
}
