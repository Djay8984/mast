package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.customers.PartyDto;

public class CustomerLinkDto extends BaseDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    @NotNull
    @Valid
    @SubIdNotNull
    private PartyDto customer;

    @NotNull
    @Valid
    private LinkResource relationship;

    @NotNull
    @Valid
    private List<CustomerHasFunctionDto> functions;

    public PartyDto getCustomer()
    {
        return customer;
    }

    public void setCustomer(final PartyDto customer)
    {
        this.customer = customer;
    }

    public LinkResource getRelationship()
    {
        return relationship;
    }

    public void setRelationship(final LinkResource relationship)
    {
        this.relationship = relationship;
    }

    public List<CustomerHasFunctionDto> getFunctions()
    {
        return functions;
    }

    public void setFunctions(final List<CustomerHasFunctionDto> functions)
    {
        this.functions = functions;
    }
}
