package com.baesystems.ai.lr.dto;

import java.util.Date;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class DateDto extends BaseDto
{
    private static final long serialVersionUID = 8532023186920656572L;

    private Date date;

    public Date getDate()
    {
        return date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }
}
