package com.baesystems.ai.lr.dto.references;

public class WorkItemActionDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 9086962175800512168L;

    private String referenceCode;

    private String taskCategory;

    public String getTaskCategory()
    {
        return taskCategory;
    }

    public void setTaskCategory(final String taskCategory)
    {
        this.taskCategory = taskCategory;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(final String referenceCode)
    {
        this.referenceCode = referenceCode;
    }
}
