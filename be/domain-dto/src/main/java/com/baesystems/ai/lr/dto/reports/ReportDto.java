package com.baesystems.ai.lr.dto.reports;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

public class ReportDto extends ReportLightDto
{
    private static final long serialVersionUID = -809426423717541874L;

    @JsonRawValue
    private String content;

    @JsonSetter
    public void setContent(final JsonNode content)
    {
        this.content = content.toString();
    }

    public void setContent(final String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
}
