package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class ScheduledServiceListDto implements Serializable, Dto
{
    private static final long serialVersionUID = 2947839802965876099L;

    @NotNull
    @Valid
    private List<ScheduledServiceDto> scheduledServices;

    public List<ScheduledServiceDto> getScheduledServices()
    {
        return scheduledServices;
    }

    public void setScheduledServices(final List<ScheduledServiceDto> scheduledServices)
    {
        this.scheduledServices = scheduledServices;
    }
}
