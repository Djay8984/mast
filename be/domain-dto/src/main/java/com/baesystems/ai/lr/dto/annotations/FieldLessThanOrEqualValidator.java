package com.baesystems.ai.lr.dto.annotations;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.time.DateUtils;

public class FieldLessThanOrEqualValidator implements ConstraintValidator<FieldLessThanOrEqual, Object>
{
    private String firstFieldName;
    private String secondFieldName;
    private boolean useTime;

    /**
     * Get the field names from the annotation.
     */
    @Override
    public void initialize(final FieldLessThanOrEqual constraintAnnotation)
    {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
        useTime = constraintAnnotation.useTime();
    }

    /**
     * Method for testing if the first named field is less than or equal to the second named field. Validation must be
     * added at the class level and can take a list of field pairs, which must be of the same type and must be
     * comparable (Double, Float, Long, Integer and Date are accepted).
     * 
     * @return true if firstObj <= secondObj false if not
     */
    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context)
    {
        Object firstObj = null;
        Object secondObj = null;
        boolean valid = false;

        try
        {
            firstObj = PropertyUtils.getProperty(value, firstFieldName);
            secondObj = PropertyUtils.getProperty(value, secondFieldName);

            valid = firstObj == null || secondObj == null;

            if (!valid)
            {
                valid = Compare(firstObj, secondObj);
            }
        }
        catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception)
        {
            // The method that this overrides doesn't throw these exceptions so it needs to throw a built in java
            // exception if there is a problem.
            final UnsupportedOperationException newException = new UnsupportedOperationException();
            newException.initCause(exception);
            throw newException;
        }

        return valid;
    }

    /**
     * Do the comparison.
     * 
     * @param firstObj
     * @param secondObj
     * @return true if firstObj <= secondObj false if not
     */
    private boolean Compare(final Object firstObj, final Object secondObj)
    {
        boolean valid = false;
        if (firstObj instanceof Date && secondObj instanceof Date)
        {
            if (useTime)
            {
                valid = ((Date) firstObj).before((Date) secondObj) || ((Date) firstObj).equals(secondObj);
            }
            else
            {
                valid = ((Date) firstObj).before((Date) secondObj) || DateUtils.isSameDay((Date) firstObj, (Date) secondObj);
            }
        }
        else if (firstObj instanceof Double && secondObj instanceof Double)
        {
            valid = ((Double) firstObj) <= ((Double) secondObj);
        }
        else if (firstObj instanceof Float && secondObj instanceof Float)
        {
            valid = ((Float) firstObj) <= ((Float) secondObj);
        }
        else if (firstObj instanceof Long && secondObj instanceof Long)
        {
            valid = ((Long) firstObj) <= ((Long) secondObj);
        }
        else if (firstObj instanceof Integer && secondObj instanceof Integer)
        {
            valid = ((Integer) firstObj) <= ((Integer) secondObj);
        }
        return valid;
    }
}
