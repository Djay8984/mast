package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsSsch2Dto extends BaseDto
{
    private static final long serialVersionUID = -2212835568628441098L;

    private Integer grossTonnage;

    public Integer getGrossTonnage()
    {
        return grossTonnage;
    }

    public void setGrossTonnage(final Integer grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }
}
