package com.baesystems.ai.lr.dto.base;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.time.DateFormatUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseDto implements IdDto, Serializable
{
    private static final long serialVersionUID = -7700260036236546617L;

    @JsonProperty("_id")
    private String internalId;

    private Long id;

    public BaseDto()
    {
        this.internalId = UUID.randomUUID().toString();
    }

    @Override
    public final Long getId()
    {
        return this.id;
    }

    @Override
    public final void setId(final Long id)
    {
        this.id = id;
    }

    @Override
    public String getInternalId()
    {
        return this.id == null ? this.internalId : this.id.toString();
    }

    @Override
    public void setInternalId(final String internalId)
    {
        if (internalId != null)
        {
            this.internalId = this.id == null ? internalId : this.id.toString();
        }
    }

    @Override
    public final boolean equals(final Object other)
    {
        boolean result = true;

        if (!(other instanceof BaseDto))
        {
            result = false;
        }
        else
        {
            final BaseDto baseDto = (BaseDto) other;

            if (baseDto.getId() != null && !baseDto.getId().equals(getId()))
            {
                result = false;
            }
        }

        return result;
    }

    @Override
    public final int hashCode()
    {
        int result;

        if (getId() != null)
        {
            result = getId().hashCode();
        }
        else
        {
            result = super.hashCode();
        }

        return result;
    }

    protected String getStringRepresentationOfList(final List<? extends BaseDto> listOfDtos)
    {
        final StringBuilder builder = new StringBuilder();

        if (listOfDtos == null)
        {
            builder.append("NULL");
        }
        else if (listOfDtos.isEmpty())
        {
            builder.append("[]");
        }
        else
        {
            listOfDtos.stream().forEach(dto -> builder.append(dto.getId()));
        }

        return builder.toString();
    }

    protected String getStringRepresentationOfDate(final Date dateToFormat)
    {
        String result = null;

        if (dateToFormat != null)
        {
            result = DateFormatUtils.ISO_DATE_FORMAT.format(dateToFormat);
        }

        return result;
    }

    protected <T extends BaseDto> Long getIdOrNull(final T dto)
    {
        return dto == null ? null : dto.getId();
    }
}
