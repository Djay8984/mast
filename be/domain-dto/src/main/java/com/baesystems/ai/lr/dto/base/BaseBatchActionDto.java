package com.baesystems.ai.lr.dto.base;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Base class for batch actions.
 * FE should populate assetIds and optional itemTypeId.
 * BE populates rejectedAssetIds only in the case where FE have specified an itemTypeId.
 */
public class BaseBatchActionDto extends BaseDto
{
    private static final long serialVersionUID = 6340260036236324617L;

    @NotNull
    private List<Long> assetIds;

    private Long itemTypeId;

    private List<Long> rejectedAssetIds;

    public final List<Long> getAssetIds()
    {
        return assetIds;
    }

    public final void setAssetIds(final List<Long> assetIds)
    {
        this.assetIds = assetIds;
    }

    public final Long getItemTypeId()
    {
        return this.itemTypeId;
    }

    public final void setItemTypeId(Long itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public final List<Long> getRejectedAssetIds()
    {
        return this.rejectedAssetIds;
    }

    public final void setRejectedAssetIds(List<Long> rejectedAssetIds)
    {
        this.rejectedAssetIds = rejectedAssetIds;
    }
}
