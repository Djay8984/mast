package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class EmployeeOfficeDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -9420343487388024L;

    private LinkResource office;

    private LinkResource employee;

    public LinkResource getOffice()
    {
        return this.office;
    }

    public void setOffice(final LinkResource office)
    {
        this.office = office;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }
}
