package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class CaseReferenceDto extends BaseReferenceDataDto
{

    private static final long serialVersionUID = 8189539576205999292L;

    private List<CaseStatusDto> caseStatuses;

    public List<CaseStatusDto> getCaseStatuses()
    {
        return caseStatuses;
    }

    public void setCaseStatuses(final List<CaseStatusDto> caseStatuses)
    {
        this.caseStatuses = caseStatuses;
    }
}
