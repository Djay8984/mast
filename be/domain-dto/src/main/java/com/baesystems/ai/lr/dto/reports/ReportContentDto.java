package com.baesystems.ai.lr.dto.reports;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.dto.codicils.ActionableItemDto;
import com.baesystems.ai.lr.dto.codicils.AssetNoteDto;
import com.baesystems.ai.lr.dto.codicils.CoCDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.baesystems.ai.lr.dto.tasks.WorkItemDto;

public class ReportContentDto implements Serializable
{
    private static final long serialVersionUID = -4982569443309854864L;

    private List<AssetNoteDto> wipAssetNotes;

    private List<CoCDto> wipCocs;

    private List<ActionableItemDto> wipActionableItems;

    private List<SurveyDto> surveys;

    private List<WorkItemDto> tasks;

    public List<AssetNoteDto> getWipAssetNotes()
    {
        return wipAssetNotes;
    }

    public void setWipAssetNotes(final List<AssetNoteDto> wipAssetNotes)
    {
        this.wipAssetNotes = wipAssetNotes;
    }

    public List<CoCDto> getWipCocs()
    {
        return wipCocs;
    }

    public void setWipCocs(final List<CoCDto> wipCocs)
    {
        this.wipCocs = wipCocs;
    }

    public List<ActionableItemDto> getWipActionableItems()
    {
        return wipActionableItems;
    }

    public void setWipActionableItems(final List<ActionableItemDto> wipActionableItems)
    {
        this.wipActionableItems = wipActionableItems;
    }

    public List<SurveyDto> getSurveys()
    {
        return surveys;
    }

    public void setSurveys(final List<SurveyDto> surveys)
    {
        this.surveys = surveys;
    }

    public List<WorkItemDto> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<WorkItemDto> tasks)
    {
        this.tasks = tasks;
    }
}
