package com.baesystems.ai.lr.dto.defects;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.DeletableDto;

public class FaultDto extends DeletableDto
{
    private Integer sequenceNumber;

    private Integer firstFrameNumber;

    private Integer lastFrameNumber;

    private Date incidentDate;

    @Size(message = "invalid length", max = 2000)
    private String incidentDescription;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String title;

    private Boolean promptThoroughRepair;

    @NotNull
    @Valid
    private LinkResource confidentialityType;
    
    @Valid
    private List<JobDefectDto> jobs;

    private LinkResource job;
    private LinkResource asset;
    private Integer repairCount;
    private Integer courseOfActionCount;

    public Integer getSequenceNumber()
    {
        return sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getFirstFrameNumber()
    {
        return firstFrameNumber;
    }

    public void setFirstFrameNumber(final Integer firstFrameNumber)
    {
        this.firstFrameNumber = firstFrameNumber;
    }

    public Integer getLastFrameNumber()
    {
        return lastFrameNumber;
    }

    public void setLastFrameNumber(final Integer lastFrameNumber)
    {
        this.lastFrameNumber = lastFrameNumber;
    }

    public Date getIncidentDate()
    {
        return incidentDate;
    }

    public void setIncidentDate(final Date incidentDate)
    {
        this.incidentDate = incidentDate;
    }

    public String getIncidentDescription()
    {
        return incidentDescription;
    }

    public void setIncidentDescription(final String incidentDescription)
    {
        this.incidentDescription = incidentDescription;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public Integer getRepairCount()
    {
        return repairCount;
    }

    public void setRepairCount(final Integer repairCount)
    {
        this.repairCount = repairCount;
    }

    public Integer getCourseOfActionCount()
    {
        return courseOfActionCount;
    }

    public void setCourseOfActionCount(final Integer courseOfActionCount)
    {
        this.courseOfActionCount = courseOfActionCount;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public Boolean getPromptThoroughRepair()
    {
        return promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }
    
    public List<JobDefectDto> getJobs()
    {
        return this.jobs;
    }

    public void setJobs(final List<JobDefectDto> jobs)
    {
        this.jobs = jobs;
    }

}
