package com.baesystems.ai.lr.dto.jobs;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.reports.ReportDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class FollowUpActionDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = 4158767297567245418L;

    private String stalenessHash;

    @Size(message = "invalid length", max = 50)
    private String title;

    @Size(message = "invalid length", max = 2000)
    private String description;

    @NotNull
    @Valid
    private LinkResource raisedBy;

    @Valid
    private LinkResource completedBy;

    private Boolean addedManually;

    @NotNull
    @Valid
    private LinkResource followUpActionStatus;

    private Date completionDate;

    private Date targetDate;

    @NotNull
    @Valid
    private LinkResource asset;

    @NotNull
    @Valid
    private ReportDto report;

    @Valid
    private LinkResource survey;

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkResource getRaisedBy()
    {
        return this.raisedBy;
    }

    public void setRaisedBy(final LinkResource raisedBy)
    {
        this.raisedBy = raisedBy;
    }

    public LinkResource getCompletedBy()
    {
        return this.completedBy;
    }

    public void setCompletedBy(final LinkResource completedBy)
    {
        this.completedBy = completedBy;
    }

    public Boolean getAddedManually()
    {
        return this.addedManually;
    }

    public void setAddedManually(final Boolean addedManually)
    {
        this.addedManually = addedManually;
    }

    public LinkResource getFollowUpActionStatus()
    {
        return this.followUpActionStatus;
    }

    public void setFollowUpActionStatus(final LinkResource followUpActionStatus)
    {
        this.followUpActionStatus = followUpActionStatus;
    }

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Date getTargetDate()
    {
        return this.targetDate;
    }

    public void setTargetDate(final Date targetDate)
    {
        this.targetDate = targetDate;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public ReportDto getReport()
    {
        return this.report;
    }

    public void setReport(final ReportDto report)
    {
        this.report = report;
    }

    public LinkResource getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final LinkResource survey)
    {
        this.survey = survey;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "FollowUpActionDto [followUpActionStatus=" + getIdOrNull(this.followUpActionStatus)
                + ", getId()=" + getId() + "]";

    }

}
