package com.baesystems.ai.lr.dto.query;

import java.io.Serializable;

import com.baesystems.ai.lr.dto.base.Dto;

/**
 * Simple marker for Query type Dtos
 */
public interface QueryDto extends Dto, Serializable
{
}
