package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class RepairItemDto extends BaseDto
{
    private static final long serialVersionUID = 2207746146624404895L;

    @NotNull
    @Valid
    private DefectItemLinkDto item; // This is the id in the defect item table not the id of the item.

    public DefectItemLinkDto getItem()
    {
        return item;
    }

    public void setItem(final DefectItemLinkDto item)
    {
        this.item = item;
    }
}
