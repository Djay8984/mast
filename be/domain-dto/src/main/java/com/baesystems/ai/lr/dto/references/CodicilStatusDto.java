package com.baesystems.ai.lr.dto.references;

public class CodicilStatusDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -233376735887388024L;

    private Long typeId;

    public Long getTypeId()
    {
        return typeId;
    }

    public void setTypeId(final Long typeId)
    {
        this.typeId = typeId;
    }
}
