package com.baesystems.ai.lr.dto.references;

import java.util.List;

import com.baesystems.ai.lr.dto.LinkResource;

public class WorkItemConditionalAttributeDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -448940813171868929L;

    private Boolean generatesFollowOnAttribute;

    private String serviceCode;

    private ItemTypeDto itemType;

    private WorkItemActionDto workItemAction;

    private LinkResource attributeDataType;

    private List<AllowedWorkItemAttributeValueDto> allowedAttributeValues;

    private WorkItemConditionalAttributeDto followOnAttribute;

    public Boolean getGeneratesFollowOnAttribute()
    {
        return generatesFollowOnAttribute;
    }

    public void setGeneratesFollowOnAttribute(final Boolean generatesFollowOnAttribute)
    {
        this.generatesFollowOnAttribute = generatesFollowOnAttribute;
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    public void setServiceCode(final String serviceCode)
    {
        this.serviceCode = serviceCode;
    }

    public ItemTypeDto getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDto itemType)
    {
        this.itemType = itemType;
    }

    public WorkItemActionDto getWorkItemAction()
    {
        return workItemAction;
    }

    public void setWorkItemAction(final WorkItemActionDto workItemAction)
    {
        this.workItemAction = workItemAction;
    }

    public LinkResource getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final LinkResource attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public List<AllowedWorkItemAttributeValueDto> getAllowedAttributeValues()
    {
        return allowedAttributeValues;
    }

    public void setAllowedAttributeValues(final List<AllowedWorkItemAttributeValueDto> allowedAttributeValues)
    {
        this.allowedAttributeValues = allowedAttributeValues;
    }

    public WorkItemConditionalAttributeDto getFollowOnAttribute()
    {
        return followOnAttribute;
    }

    public void setFollowOnAttribute(final WorkItemConditionalAttributeDto followOnAttribute)
    {
        this.followOnAttribute = followOnAttribute;
    }
}
