package com.baesystems.ai.lr.dto.defects;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class DefectItemLinkDto extends BaseDto
{
    private static final long serialVersionUID = 1914884139860649971L;

    private LinkResource item;

    public LinkResource getItem()
    {
        return item;
    }

    public void setItem(final LinkResource item)
    {
        this.item = item;
    }

}
