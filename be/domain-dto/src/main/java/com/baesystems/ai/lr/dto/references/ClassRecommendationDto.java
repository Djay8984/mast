package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ClassRecommendationDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -5628040548482762751L;

    private LinkResource reportType;

    private String narrativeTitle;

    private String narrativeText;

    public LinkResource getReportType()
    {
        return reportType;
    }

    public void setReportType(final LinkResource reportType)
    {
        this.reportType = reportType;
    }

    public String getNarrativeTitle()
    {
        return narrativeTitle;
    }

    public void setNarrativeTitle(final String narrativeTitle)
    {
        this.narrativeTitle = narrativeTitle;
    }

    public String getNarrativeText()
    {
        return narrativeText;
    }

    public void setNarrativeText(final String narrativeText)
    {
        this.narrativeText = narrativeText;
    }
}
