package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.WorkItemConditionalAttributeDto;

public class WorkItemAttributeDto extends BaseDto
{
    private static final long serialVersionUID = -5290824531522351772L;

    private String description;

    private String attributeValueString;

    @Valid
    private LinkResource workItem;

    @Valid
    private LinkResource attributeDataType;

    @Valid
    private WorkItemConditionalAttributeDto workItemConditionalAttribute;

    @Valid
    private List<LinkResource> allowedAttributeValues;

    @Valid
    private LinkResource followedOnFrom;

    @Valid
    private LinkResource parent;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public LinkResource getAttributeDataType()
    {
        return attributeDataType;
    }

    public void setAttributeDataType(final LinkResource attributeDataType)
    {
        this.attributeDataType = attributeDataType;
    }

    public List<LinkResource> getAllowedAttributeValues()
    {
        return allowedAttributeValues;
    }

    public void setAllowedAttributeValues(final List<LinkResource> allowedAttributeValues)
    {
        this.allowedAttributeValues = allowedAttributeValues;
    }

    public String getAttributeValueString()
    {
        return attributeValueString;
    }

    public void setAttributeValueString(final String attributeValueString)
    {
        this.attributeValueString = attributeValueString;
    }

    public WorkItemConditionalAttributeDto getWorkItemConditionalAttribute()
    {
        return workItemConditionalAttribute;
    }

    public void setWorkItemConditionalAttribute(final WorkItemConditionalAttributeDto workItemConditionalAttribute)
    {
        this.workItemConditionalAttribute = workItemConditionalAttribute;
    }

    public LinkResource getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final LinkResource workItem)
    {
        this.workItem = workItem;
    }

    public LinkResource getFollowedOnFrom()
    {
        return followedOnFrom;
    }

    public void setFollowedOnFrom(final LinkResource followedOnFrom)
    {
        this.followedOnFrom = followedOnFrom;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }
}
