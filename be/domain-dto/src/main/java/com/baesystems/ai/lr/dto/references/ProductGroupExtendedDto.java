package com.baesystems.ai.lr.dto.references;

import java.util.List;

public class ProductGroupExtendedDto extends ProductGroupDto
{
    private static final long serialVersionUID = -7309870484259113085L;

    private List<ProductCatalogueExtendedDto> productCatalogues;

    public List<ProductCatalogueExtendedDto> getProductCatalogues()
    {
        return productCatalogues;
    }

    public void setProductCatalogues(final List<ProductCatalogueExtendedDto> productCatalogues)
    {
        this.productCatalogues = productCatalogues;
    }
}
