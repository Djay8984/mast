package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class DefectValueDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 3519586990228540293L;

    @Size(message = "invalid length", max = 100)
    private String name;

    @Valid
    private ReferenceDataDto severity;

    @Valid
    private List<DefectCategoryDto> defectCategories;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public ReferenceDataDto getSeverity()
    {
        return severity;
    }

    public void setSeverity(final ReferenceDataDto severity)
    {
        this.severity = severity;
    }

    public List<DefectCategoryDto> getDefectCategories()
    {
        return defectCategories;
    }

    public void setDefectCategories(final List<DefectCategoryDto> defectCategories)
    {
        this.defectCategories = defectCategories;
    }
}
