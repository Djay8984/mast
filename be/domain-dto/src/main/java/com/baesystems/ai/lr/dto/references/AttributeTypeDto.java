package com.baesystems.ai.lr.dto.references;

import java.util.Date;
import java.util.List;

public class AttributeTypeDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 1530293187484995813L;

    private ItemTypeDto itemType;

    private ReferenceDataDto valueType;

    private Long descriptionOrder;

    private Date startDate;

    private Date endDate;

    private Long namingOrder;

    private String namingDecoration;

    private Integer minOccurs;

    private Integer maxOccurs;

    private Boolean transferable;

    private List<AllowedAssetAttributeValueDto> allowedValues;
    
    private Long displayOrder;

    public ReferenceDataDto getValueType()
    {
        return valueType;
    }

    public void setValueType(final ReferenceDataDto valueType)
    {
        this.valueType = valueType;
    }

    public ItemTypeDto getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDto itemType)
    {
        this.itemType = itemType;
    }

    public Long getDescriptionOrder()
    {
        return descriptionOrder;
    }

    public void setDescriptionOrder(final Long descriptionOrder)
    {
        this.descriptionOrder = descriptionOrder;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(final Date endDate)
    {
        this.endDate = endDate;
    }

    public Long getNamingOrder()
    {
        return namingOrder;
    }

    public void setNamingOrder(final Long namingOrder)
    {
        this.namingOrder = namingOrder;
    }

    public String getNamingDecoration()
    {
        return namingDecoration;
    }

    public void setNamingDecoration(final String namingDecoration)
    {
        this.namingDecoration = namingDecoration;
    }

    public Integer getMinOccurs()
    {
        return minOccurs;
    }

    public void setMinOccurs(final Integer minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public Integer getMaxOccurs()
    {
        return maxOccurs;
    }

    public void setMaxOccurs(final Integer maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public Boolean getTransferable()
    {
        return transferable;
    }

    public void setTransferable(final Boolean transferable)
    {
        this.transferable = transferable;
    }

    public List<AllowedAssetAttributeValueDto> getAllowedValues()
    {
        return allowedValues;
    }

    public void setAllowedValues(final List<AllowedAssetAttributeValueDto> allowedValues)
    {
        this.allowedValues = allowedValues;
    }

    public Long getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Long displayOrder)
    {
        this.displayOrder = displayOrder;
    }
}
