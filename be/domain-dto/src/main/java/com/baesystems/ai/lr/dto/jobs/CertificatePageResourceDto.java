package com.baesystems.ai.lr.dto.jobs;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class CertificatePageResourceDto extends BasePageResource<CertificateDto>
{
    private List<CertificateDto> content;

    @Override
    public List<CertificateDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<CertificateDto> content)
    {
        this.content = content;
    }

}
