package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.ReferenceDataDto;

// NB: this class contains no validation on purpose.
public class ItemLightDto extends BaseDto
{
    private static final long serialVersionUID = -4267363168907923576L;

    private ReferenceDataDto itemClass;

    private Long assetId;

    private Long assetVersionId;

    private Boolean reviewed;

    private String name;

    private LinkResource itemType;

    private Integer displayOrder;

    private Boolean decommissioned;

    public Long getAssetId()
    {
        return this.assetId;
    }

    public void setAssetId(final Long assetId)
    {
        this.assetId = assetId;
    }

    public Long getAssetVersionId()
    {
        return this.assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    public LinkResource getItemType()
    {
        return this.itemType;
    }

    public void setItemType(final LinkResource itemType)
    {
        this.itemType = itemType;
    }

    public ReferenceDataDto getItemClass()
    {
        return this.itemClass;
    }

    public void setItemClass(final ReferenceDataDto itemClass)
    {
        this.itemClass = itemClass;
    }

    public Boolean getReviewed()
    {
        return this.reviewed;
    }

    public void setReviewed(final Boolean reviewed)
    {
        this.reviewed = reviewed;
    }

    public final String getName()
    {
        return this.name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Boolean getDecommissioned()
    {
        return this.decommissioned;
    }

    public void setDecommissioned(final Boolean decommissioned)
    {
        this.decommissioned = decommissioned;
    }
}
