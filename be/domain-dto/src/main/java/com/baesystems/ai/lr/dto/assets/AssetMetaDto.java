package com.baesystems.ai.lr.dto.assets;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class AssetMetaDto extends BaseDto
{
    private static final long serialVersionUID = -43386412187057888L;

    private Long publishedVersionId;
    private Long draftVersionId;
    private String checkedOutBy;
    private String name;

    public Long getPublishedVersionId()
    {
        return publishedVersionId;
    }

    public void setPublishedVersionId(final Long publishedVersionId)
    {
        this.publishedVersionId = publishedVersionId;
    }

    public Long getDraftVersionId()
    {
        return draftVersionId;
    }

    public void setDraftVersionId(final Long draftVersionId)
    {
        this.draftVersionId = draftVersionId;
    }

    public String getCheckedOutBy()
    {
        return checkedOutBy;
    }

    public void setCheckedOutBy(final String checkedOutBy)
    {
        this.checkedOutBy = checkedOutBy;
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
