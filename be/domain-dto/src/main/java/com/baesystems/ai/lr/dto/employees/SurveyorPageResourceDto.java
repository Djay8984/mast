package com.baesystems.ai.lr.dto.employees;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class SurveyorPageResourceDto extends BasePageResource<SurveyorDto>
{
    private List<SurveyorDto> content;

    @Override
    public List<SurveyorDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<SurveyorDto> content)
    {
        this.content = content;
    }

}
