package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class FollowUpActionTemplateDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -7754372384198264857L;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String title;

    @NotNull
    @Size(message = "invalid length", max = 2000)
    private String description;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }
}
