package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ServiceCreditStatusDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -323376733957388024L;

    @NotNull
    @Size(max = 100)
    private String code;

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
