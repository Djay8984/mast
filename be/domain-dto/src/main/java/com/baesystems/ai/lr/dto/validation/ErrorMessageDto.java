package com.baesystems.ai.lr.dto.validation;

import java.io.Serializable;

public class ErrorMessageDto implements Serializable
{
    private static final long serialVersionUID = -4622984003119535779L;

    private String message;

    private String detailedMessage;

    private int exceptionCode;

    public ErrorMessageDto()
    {
    }

    /**
     * Set the error message, but leave the detailed message blank.
     *
     * @param message, The message
     */
    public ErrorMessageDto(final String message)
    {
        this.message = message;
    }

    public ErrorMessageDto(final String message, final int exceptionCode)
    {
        this.message = message;
        this.exceptionCode = exceptionCode;
    }

    /**
     * Set the error message and the detailed message directly. This accepts the stack trace/detailed message formatted
     * as a string and is unlikely to be used in most cases.
     *
     * @param message, The message
     * @param detailedMessage, The stack trace or detailed message as a list of strings.
     */
    public ErrorMessageDto(final String message, final String detailedMessage)
    {
        this.message = message;
        this.detailedMessage = detailedMessage;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    public String getDetailedMessage()
    {
        return detailedMessage;
    }

    public void setDetailedMessage(final String detailedMessage)
    {
        this.detailedMessage = detailedMessage;
    }

    public int getExceptionCode()
    {
        return this.exceptionCode;
    }

    public void setExceptionCode(final int exceptionCode)
    {
        this.exceptionCode = exceptionCode;
    }
}
