package com.baesystems.ai.lr.dto.references;

import java.util.Map;

public class ReferenceDataMapDto
{
    private Map<String, Object> referenceData;

    public Map<String, Object> getReferenceData()
    {
        return referenceData;
    }

    public void setReferenceData(final Map<String, Object> referenceData)
    {
        this.referenceData = referenceData;
    }
}
