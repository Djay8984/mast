package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;

public class MaterialDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -809505328561952980L;

    @Valid
    private LinkResource type;

    public LinkResource getType()
    {
        return type;
    }

    public void setType(final LinkResource type)
    {
        this.type = type;
    }

}
