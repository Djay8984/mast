package com.baesystems.ai.lr.dto.validation;

import java.util.Map;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class AssetModelTemplateDto extends BaseDto
{
    private static final long serialVersionUID = 2742817088524457344L;

    private Map<Long, ItemTemplateDto> assetModelTemplate;

    public Map<Long, ItemTemplateDto> getAssetModelTemplate()
    {
        return assetModelTemplate;
    }

    public void setAssetModelTemplate(final Map<Long, ItemTemplateDto> assetModelTemplate)
    {
        this.assetModelTemplate = assetModelTemplate;
    }
}
