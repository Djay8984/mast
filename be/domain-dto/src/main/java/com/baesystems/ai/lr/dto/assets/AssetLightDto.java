package com.baesystems.ai.lr.dto.assets;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetLightDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -4552907118794216757L;

    private String stalenessHash;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String name;

    @NotNull
    private Boolean isLead;

    @Min(value = 0)
    @Max(value = 9999999)
    private Long leadImo;

    @Valid
    private LinkResource classMaintenanceStatus;

    @Size(message = "invalid length", max = 500)
    private String classNotation;

    @Size(message = "invalid length", max = 500)
    private String machineryClassNotation;

    @NotNull
    private Date buildDate;

    private Date estimatedBuildDate;

    private Date keelLayingDate;

    @Min(value = 0)
    private Double grossTonnage;

    @NotNull
    @Valid
    private LinkResource assetType;

    @NotNull
    @Valid
    private LinkResource assetCategory;

    @Valid
    private LinkResource classStatus;

    @Size(message = "invalid length", max = 100)
    private String builder;

    @Size(message = "invalid length", max = 15)
    private String yardNumber;

    @Valid
    private LinkResource ihsAsset;

    @Valid
    private LinkResource ruleSet;

    @Valid
    private LinkResource previousRuleSet;

    @Valid
    private LinkResource productRuleSet;

    private Date harmonisationDate;

    @Valid
    private LinkResource flagState;

    @Valid
    private LinkResource registeredPort;

    private Date dateOfRegistry;

    @Valid
    private LinkResource assetLifecycleStatus;

    @Valid
    private LinkResource coClassificationSociety;

    @Valid
    private List<CustomerLinkDto> customers;

    @Valid
    private List<OfficeLinkDto> offices;

    @Valid
    private LinkResource countryOfBuild;

    @Size(message = "invalid length", max = 500)
    private String descriptiveNote;

    @Size(message = "invalid length", max = 100)
    private String callSign;

    @NotNull
    private Integer hullIndicator;

    @Valid
    private LinkResource classDepartment;

    @Valid
    private LinkResource linkedAsset;

    private Date effectiveDate;

    @Min(value = 0)
    @Max(value = 9999999)
    private Double deadWeight;

    private Long assetVersionId;

    private Long parentPublishVersionId;

    public Long getAssetVersionId()
    {
        return assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Boolean getIsLead()
    {
        return this.isLead;
    }

    public void setIsLead(final Boolean isLead)
    {
        this.isLead = isLead;
    }

    public Long getLeadImo()
    {
        return this.leadImo;
    }

    public void setLeadImo(final Long leadImo)
    {
        this.leadImo = leadImo;
    }

    public String getClassNotation()
    {
        return this.classNotation;
    }

    public void setClassNotation(final String classNotation)
    {
        this.classNotation = classNotation;
    }

    public final Date getBuildDate()
    {
        return this.buildDate;
    }

    public final void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public Date getEstimatedBuildDate()
    {
        return this.estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public Date getKeelLayingDate()
    {
        return this.keelLayingDate;
    }

    public void setKeelLayingDate(final Date keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public final LinkResource getAssetType()
    {
        return this.assetType;
    }

    public final void setAssetType(final LinkResource assetType)
    {
        this.assetType = assetType;
    }

    public final LinkResource getClassStatus()
    {
        return this.classStatus;
    }

    public final void setClassStatus(final LinkResource classStatus)
    {
        this.classStatus = classStatus;
    }

    public final String getBuilder()
    {
        return this.builder;
    }

    public final void setBuilder(final String builder)
    {
        this.builder = builder;
    }

    public LinkResource getAssetCategory()
    {
        return this.assetCategory;
    }

    public void setAssetCategory(final LinkResource assetCategory)
    {
        this.assetCategory = assetCategory;
    }

    public LinkResource getIhsAsset()
    {
        return this.ihsAsset;
    }

    public void setIhsAsset(final LinkResource ihsAsset)
    {
        this.ihsAsset = ihsAsset;
    }

    public String getYardNumber()
    {
        return this.yardNumber;
    }

    public void setYardNumber(final String yardNumber)
    {
        this.yardNumber = yardNumber;
    }

    public LinkResource getRuleSet()
    {
        return this.ruleSet;
    }

    public void setRuleSet(final LinkResource ruleSet)
    {
        this.ruleSet = ruleSet;
    }

    public LinkResource getPreviousRuleSet()
    {
        return this.previousRuleSet;
    }

    public void setPreviousRuleSet(final LinkResource previousRuleSet)
    {
        this.previousRuleSet = previousRuleSet;
    }

    public LinkResource getProductRuleSet()
    {
        return this.productRuleSet;
    }

    public void setProductRuleSet(final LinkResource productRuleSet)
    {
        this.productRuleSet = productRuleSet;
    }

    public Date getHarmonisationDate()
    {
        return this.harmonisationDate;
    }

    public void setHarmonisationDate(final Date harmonisationDate)
    {
        this.harmonisationDate = harmonisationDate;
    }

    public LinkResource getRegisteredPort()
    {
        return this.registeredPort;
    }

    public void setRegisteredPort(final LinkResource registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public Date getDateOfRegistry()
    {
        return this.dateOfRegistry;
    }

    public void setDateOfRegistry(final Date dateOfRegistry)
    {
        this.dateOfRegistry = dateOfRegistry;
    }

    public LinkResource getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final LinkResource assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public LinkResource getCoClassificationSociety()
    {
        return this.coClassificationSociety;
    }

    public void setCoClassificationSociety(final LinkResource coClassificationSociety)
    {
        this.coClassificationSociety = coClassificationSociety;
    }

    public LinkResource getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final LinkResource classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public LinkResource getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final LinkResource flagState)
    {
        this.flagState = flagState;
    }

    public List<CustomerLinkDto> getCustomers()
    {
        return this.customers;
    }

    public void setCustomers(final List<CustomerLinkDto> customers)
    {
        this.customers = customers;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public LinkResource getCountryOfBuild()
    {
        return this.countryOfBuild;
    }

    public void setCountryOfBuild(final LinkResource countryOfBuild)
    {
        this.countryOfBuild = countryOfBuild;
    }

    public String getDescriptiveNote()
    {
        return this.descriptiveNote;
    }

    public void setDescriptiveNote(final String description)
    {
        this.descriptiveNote = description;
    }

    public String getCallSign()
    {
        return this.callSign;
    }

    public void setCallSign(final String callSign)
    {
        this.callSign = callSign;
    }

    public String getMachineryClassNotation()
    {
        return this.machineryClassNotation;
    }

    public void setMachineryClassNotation(final String machineryClassNotation)
    {
        this.machineryClassNotation = machineryClassNotation;
    }

    public Integer getHullIndicator()
    {
        return this.hullIndicator;
    }

    public void setHullIndicator(final Integer hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    public Long getParentPublishVersionId()
    {
        return parentPublishVersionId;
    }

    public void setParentPublishVersionId(final Long parentPublishVersionId)
    {
        this.parentPublishVersionId = parentPublishVersionId;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public LinkResource getClassDepartment()
    {
        return classDepartment;
    }

    public void setClassDepartment(final LinkResource classDepartment)
    {
        this.classDepartment = classDepartment;
    }

    public LinkResource getLinkedAsset()
    {
        return linkedAsset;
    }

    public void setLinkedAsset(final LinkResource linkedAsset)
    {
        this.linkedAsset = linkedAsset;
    }

    public Date getEffectiveDate()
    {
        return effectiveDate;
    }

    public void setEffectiveDate(final Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public Double getDeadWeight()
    {
        return deadWeight;
    }

    public void setDeadWeight(final Double deadWeight)
    {
        this.deadWeight = deadWeight;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetLightDto [name=" + this.name
                + ", isLead=" + this.isLead
                + ", leadImo=" + this.leadImo
                + ", classMaintenanceStatus=" + getIdOrNull(this.classMaintenanceStatus)
                + ", classNotation=" + this.classNotation
                + ", machineryClassNotation=" + this.machineryClassNotation
                + ", buildDate=" + getStringRepresentationOfDate(this.buildDate)
                + ", estimatedBuildDate=" + getStringRepresentationOfDate(this.estimatedBuildDate)
                + ", keelLayingDate=" + getStringRepresentationOfDate(this.keelLayingDate)
                + ", grossTonnage=" + this.grossTonnage
                + ", assetType=" + getIdOrNull(this.assetType)
                + ", assetCategory=" + getIdOrNull(this.assetCategory)
                + ", classStatus=" + getIdOrNull(this.classStatus)
                + ", builder=" + this.builder
                + ", yardNumber=" + this.yardNumber
                + ", ihsAsset=" + getIdOrNull(this.ihsAsset)
                + ", ruleSet=" + getIdOrNull(this.ruleSet)
                + ", previousRuleSet=" + getIdOrNull(this.previousRuleSet)
                + ", productRuleSet=" + getIdOrNull(this.productRuleSet)
                + ", harmonisationDate=" + getStringRepresentationOfDate(this.harmonisationDate)
                + ", flagState=" + getIdOrNull(this.flagState)
                + ", registeredPort=" + getIdOrNull(this.registeredPort)
                + ", dateOfRegistry=" + getStringRepresentationOfDate(this.dateOfRegistry)
                + ", assetLifecycleStatus=" + getIdOrNull(this.assetLifecycleStatus)
                + ", coClassificationSociety=" + getIdOrNull(this.coClassificationSociety)
                + ", customers=" + getStringRepresentationOfList(this.customers)
                + ", offices=" + getStringRepresentationOfList(this.offices)
                + ", countryOfBuild=" + getIdOrNull(this.countryOfBuild)
                + ", description=" + this.descriptiveNote
                + ", callSign=" + this.callSign
                + ", hullIndicator=" + this.hullIndicator
                + ", parentPublishVersionId=" + this.parentPublishVersionId
                + ", linkedAsset=" + getIdOrNull(this.linkedAsset)
                + ", effectiveDate=" + getStringRepresentationOfDate(this.effectiveDate)
                + ", deadWeight=" + this.deadWeight
                + ", classDepartment=" + getIdOrNull(this.classDepartment) + "]";
    }
}
