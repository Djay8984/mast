package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class CodicilTemplateDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -4431356078499112631L;

    @Valid
    private ReferenceDataDto category;

    @Size(message = "invalid length", max = 50)
    private String title;

    @Valid
    private ReferenceDataDto confidentialityType;

    private Boolean editableBySurveyor;

    @Size(message = "invalid length", max = 2000)
    private String description;

    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    @Valid
    private ReferenceDataDto templateStatus;

    @Size(message = "invalid length", max = 10)
    private String version;

    @NotNull
    @Valid
    private CodicilTypeDto codicilType;

    private ItemTypeDto itemType;

    public ReferenceDataDto getCategory()
    {
        return category;
    }

    public void setCategory(final ReferenceDataDto category)
    {
        this.category = category;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public ReferenceDataDto getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final ReferenceDataDto confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public Boolean getEditableBySurveyor()
    {
        return editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getSurveyorGuidance()
    {
        return surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public ReferenceDataDto getTemplateStatus()
    {
        return templateStatus;
    }

    public void setTemplateStatus(final ReferenceDataDto templateStatus)
    {
        this.templateStatus = templateStatus;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(final String version)
    {
        this.version = version;
    }

    public CodicilTypeDto getCodicilType()
    {
        return codicilType;
    }

    public void setCodicilType(final CodicilTypeDto codicilType)
    {
        this.codicilType = codicilType;
    }

    public ItemTypeDto getItemType()
    {
        return itemType;
    }

    public void setItemType(final ItemTypeDto itemType)
    {
        this.itemType = itemType;
    }
}
