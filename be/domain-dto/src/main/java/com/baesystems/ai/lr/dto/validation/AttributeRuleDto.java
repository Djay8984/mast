package com.baesystems.ai.lr.dto.validation;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class AttributeRuleDto extends BaseDto
{
    private static final long serialVersionUID = 164540031130318790L;

    @JsonIgnore
    private Long itemTypeId;

    private Long attributeTypeId;

    private String valueType;

    private Integer minOccurs;

    private Integer maxOccurs;

    private Boolean transferable;

    public Long getItemTypeId()
    {
        return itemTypeId;
    }

    public void setItemTypeId(final Long itemTypeId)
    {
        this.itemTypeId = itemTypeId;
    }

    public Long getAttributeTypeId()
    {
        return attributeTypeId;
    }

    public void setAttributeTypeId(final Long attributeTypeId)
    {
        this.attributeTypeId = attributeTypeId;
    }

    public String getValueType()
    {
        return valueType;
    }

    public void setValueType(final String valueType)
    {
        this.valueType = valueType;
    }

    public Integer getMinOccurs()
    {
        return minOccurs;
    }

    public void setMinOccurs(final Integer minOccurs)
    {
        this.minOccurs = minOccurs;
    }

    public Integer getMaxOccurs()
    {
        return maxOccurs;
    }

    public void setMaxOccurs(final Integer maxOccurs)
    {
        this.maxOccurs = maxOccurs;
    }

    public Boolean getTransferable()
    {
        return transferable;
    }

    public void setTransferable(final Boolean transferable)
    {
        this.transferable = transferable;
    }
}
