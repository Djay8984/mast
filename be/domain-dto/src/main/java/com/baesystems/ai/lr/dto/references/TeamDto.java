package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class TeamDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -5439202041123488024L;

    private LinkResource officeRole;

    public LinkResource getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkResource officeRole)
    {
        this.officeRole = officeRole;
    }
}
