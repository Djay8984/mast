package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsHistDto extends BaseDto
{

    private static final long serialVersionUID = 4722244586576426851L;

    @Size(message = "invalid length", max = 8)
    private String keelLayingDate;

    public String getKeelLayingDate()
    {
        return keelLayingDate;
    }

    public void setKeelLayingDate(final String keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

}
