package com.baesystems.ai.lr.dto.cases;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class CasePageResourceDto extends BasePageResource<CaseDto>
{
    private List<CaseDto> content;

    @Override
    public List<CaseDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<CaseDto> content)
    {
        this.content = content;
    }

}
