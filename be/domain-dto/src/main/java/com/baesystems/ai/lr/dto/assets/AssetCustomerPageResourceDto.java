package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class AssetCustomerPageResourceDto extends BasePageResource<CustomerLinkDto>
{
    @Valid
    private List<CustomerLinkDto> content;

    @Override
    public List<CustomerLinkDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<CustomerLinkDto> content)
    {
        this.content = content;
    }

}
