package com.baesystems.ai.lr.dto.references;

import java.util.List;

public class ProductTypeExtendedDto extends ProductTypeDto
{
    private static final long serialVersionUID = -4709845654020795439L;

    private List<ProductGroupExtendedDto> productGroups;

    public List<ProductGroupExtendedDto> getProductGroups()
    {
        return productGroups;
    }

    public void setProductGroups(final List<ProductGroupExtendedDto> productGroups)
    {
        this.productGroups = productGroups;
    }
}
