package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsHitlDto extends BaseDto
{
    private static final long serialVersionUID = -1443333557922813103L;

    private Integer netTonnage;

    public Integer getNetTonnage()
    {
        return netTonnage;
    }

    public void setNetTonnage(final Integer netTonnage)
    {
        this.netTonnage = netTonnage;
    }
}
