package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class LrEmployeeDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -4082797965073107197L;

    @Size(message = "invalid length", max = 30)
    private String oneWorldNumber;

    @Size(message = "invalid length", max = 150)
    private String firstName;

    @NotNull
    @Size(message = "invalid length", max = 150)
    private String lastName;

    private String name;

    @Size(message = "invalid length", max = 240)
    private String emailAddress;

    public final String getOneWorldNumber()
    {
        return this.oneWorldNumber;
    }

    public final void setOneWorldNumber(final String oneWorldNumber)
    {
        this.oneWorldNumber = oneWorldNumber;
    }

    public final String getFirstName()
    {
        return this.firstName;
    }

    public final void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    public final String getLastName()
    {
        return this.lastName;
    }

    public final void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    public final String getEmailAddress()
    {
        return this.emailAddress;
    }

    public final void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
