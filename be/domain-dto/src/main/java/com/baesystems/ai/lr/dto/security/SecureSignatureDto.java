package com.baesystems.ai.lr.dto.security;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class SecureSignatureDto extends BaseDto
{
    private static final long serialVersionUID = -6634458596239166226L;

    @NotNull
    private Long id;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String name;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String algorithm;

    @Size(message = "invalid length", max = 255)
    private String description;

    @NotNull
    @Size(message = "invalid length", max = 20000)
    private String publicKey;

    @NotNull
    private Boolean enabled;

    public final String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public final String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public final String getPublicKey()
    {
        return publicKey;
    }

    public void setPublicKey(final String publicKey)
    {
        this.publicKey = publicKey;
    }

    public final Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(final Boolean enabled)
    {
        this.enabled = enabled;
    }

    public final String getAlgorithm()
    {
        return algorithm;
    }

    public void setAlgorithm(final String algorithm)
    {
        this.algorithm = algorithm;
    }
}
