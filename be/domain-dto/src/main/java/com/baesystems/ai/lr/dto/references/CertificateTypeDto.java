package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;

public class CertificateTypeDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 5202643030091407981L;

    @Size(message = "invalid length", max = 30)
    private String formNumber;

    private Double version;

    private LinkResource serviceCatalogue;

    private Integer displayOrder;

    public String getFormNumber()
    {
        return formNumber;
    }

    public void setFormNumber(final String formNumber)
    {
        this.formNumber = formNumber;
    }

    public Double getVersion()
    {
        return version;
    }

    public void setVersion(final Double version)
    {
        this.version = version;
    }

    public LinkResource getServiceCatalogue()
    {
        return serviceCatalogue;
    }

    public void setServiceCatalogue(final LinkResource serviceCatalogue)
    {
        this.serviceCatalogue = serviceCatalogue;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }
}
