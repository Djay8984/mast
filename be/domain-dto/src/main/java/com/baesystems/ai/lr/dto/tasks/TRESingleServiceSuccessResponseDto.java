package com.baesystems.ai.lr.dto.tasks;

public class TRESingleServiceSuccessResponseDto extends TREBaseResponseDto
{
    private static final long serialVersionUID = -1173906584854438781L;

    private TREServiceDto service;

    public TREServiceDto getService()
    {
        return service;
    }

    public void setService(final TREServiceDto service)
    {
        this.service = service;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TRESingleServiceSuccessResponseDto [getService()=");
        builder.append(getService());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append("]");
        return builder.toString();
    }
}
