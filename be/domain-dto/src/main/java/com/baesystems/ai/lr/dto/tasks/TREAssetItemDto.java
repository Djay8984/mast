package com.baesystems.ai.lr.dto.tasks;

import java.util.List;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class TREAssetItemDto implements RuleEngineDto
{
    private static final long serialVersionUID = -4771053375431682808L;

    private Integer assetItemIdentifier;

    private List<String> tasks;

    public Integer getAssetItemIdentifier()
    {
        return assetItemIdentifier;
    }

    public void setAssetItemIdentifier(final Integer assetItemIdentifier)
    {
        this.assetItemIdentifier = assetItemIdentifier;
    }

    public List<String> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<String> tasks)
    {
        this.tasks = tasks;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("TREAssetItemDto [getAssetItemIdentifier()=");
        builder.append(getAssetItemIdentifier());
        builder.append(", getTasks()=");
        builder.append(getTasks());
        builder.append("]");
        return builder.toString();
    }
}
