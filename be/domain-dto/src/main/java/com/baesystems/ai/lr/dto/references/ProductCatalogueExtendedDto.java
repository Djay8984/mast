package com.baesystems.ai.lr.dto.references;

import java.util.List;

public class ProductCatalogueExtendedDto extends ProductCatalogueLightDto
{
    private static final long serialVersionUID = 8844184679970379090L;

    private List<ReferenceDataDto> ruleSets;

    private List<ServiceCatalogueLightDto> serviceCatalogues;

    public List<ServiceCatalogueLightDto> getServiceCatalogues()
    {
        return serviceCatalogues;
    }

    public void setServiceCatalogues(final List<ServiceCatalogueLightDto> serviceCatalogues)
    {
        this.serviceCatalogues = serviceCatalogues;
    }

    public List<ReferenceDataDto> getRuleSets()
    {
        return ruleSets;
    }

    public void setRuleSets(final List<ReferenceDataDto> ruleSets)
    {
        this.ruleSets = ruleSets;
    }
}
