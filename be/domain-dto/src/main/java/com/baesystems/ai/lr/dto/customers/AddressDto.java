package com.baesystems.ai.lr.dto.customers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class AddressDto extends BaseDto
{
    private static final long serialVersionUID = 7751114687184441968L;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String addressLine1;

    @Size(message = "invalid length", max = 100)
    private String addressLine2;

    @Size(message = "invalid length", max = 100)
    private String addressLine3;

    @NotNull
    @Size(message = "invalid length", max = 30)
    private String town;

    @Size(message = "invalid length", max = 100)
    private String city;

    @NotNull
    @Size(message = "invalid length", max = 10)
    private String postcode;

    @Valid
    private LinkResource country;

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(final String town)
    {
        this.town = town;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(final String city)
    {
        this.city = city;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(final String postcode)
    {
        this.postcode = postcode;
    }

    public LinkResource getCountry()
    {
        return country;
    }

    public void setCountry(final LinkResource country)
    {
        this.country = country;
    }
}
