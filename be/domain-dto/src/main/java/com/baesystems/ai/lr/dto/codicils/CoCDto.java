package com.baesystems.ai.lr.dto.codicils;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class CoCDto extends CodicilDto implements DueStatusUpdateableDto, StaleObject
{
    private static final long serialVersionUID = -4350347374072610940L;

    private String stalenessHash;

    private LinkResource template;

    @NotNull
    @Size(message = "invalid length", min = 1, max = 50)
    private String title;

    @NotNull
    @Size(message = "invalid length", min = 1, max = 2000)
    private String description;

    @Size(message = "invalid length", max = 45)
    private String affectedItems;

    @Valid
    private LinkResource assetItem;

    @Valid
    private LinkResource job;

    @Valid
    private LinkResource asset;

    @Valid
    private LinkResource defect;

    private List<LinkResource> childWIPs;

    @NotNull
    private Boolean inheritedFlag;

    @Size(message = "invalid length", max = 100)
    private String actionTaken;

    private LrEmployeeDto employee;

    private LinkResource parent;

    private Boolean editableBySurveyor;

    @NotNull
    @Valid
    private LinkResource confidentialityType;

    @Valid
    private LinkResource category;

    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    private Boolean requireApproval;

    private Boolean jobScopeConfirmed;

    private Integer sequenceNumber;

    private Date dateOfCrediting;

    @Valid
    private LinkResource creditedBy;

    @Valid
    private SurveyDto survey;

    public Boolean getEditableBySurveyor()
    {
        return this.editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getAffectedItems()
    {
        return this.affectedItems;
    }

    public void setAffectedItems(final String affectedItems)
    {
        this.affectedItems = affectedItems;
    }

    public LinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public Boolean getInheritedFlag()
    {
        return this.inheritedFlag;
    }

    public void setInheritedFlag(final Boolean inheritedFlag)
    {
        this.inheritedFlag = inheritedFlag;
    }

    public String getActionTaken()
    {
        return this.actionTaken;
    }

    public void setActionTaken(final String actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    public LrEmployeeDto getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LrEmployeeDto employee)
    {
        this.employee = employee;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public Boolean getRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final Boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public Boolean getJobScopeConfirmed()
    {
        return this.jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public List<LinkResource> getChildWIPs()
    {
        return this.childWIPs;
    }

    public void setChildWIPs(final List<LinkResource> childWIPs)
    {
        this.childWIPs = childWIPs;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getDateOfCrediting()
    {
        return this.dateOfCrediting;
    }

    public void setDateOfCrediting(final Date dateOfCrediting)
    {
        this.dateOfCrediting = dateOfCrediting;
    }

    public LinkResource getCreditedBy()
    {
        return this.creditedBy;
    }

    public void setCreditedBy(final LinkResource creditedBy)
    {
        this.creditedBy = creditedBy;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;

    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CoCDto [template=" + getIdOrNull(template)
                + ", title=" + title
                + ", imposedDate=" + getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + getStringRepresentationOfDate(this.getDueDate())
                + ", description=" + description
                + ", affectedItems=" + affectedItems
                + ", status=" + getIdOrNull(this.getStatus())
                + ", assetItem=" + getIdOrNull(assetItem)
                + ", job=" + getIdOrNull(job)
                + ", asset=" + getIdOrNull(asset)
                + ", defect=" + getIdOrNull(defect)
                + ", childWIPs=" + getStringRepresentationOfList(childWIPs)
                + ", inheritedFlag=" + inheritedFlag
                + ", actionTaken=" + actionTaken
                + ", employee=" + getIdOrNull(employee)
                + ", parent=" + getIdOrNull(parent)
                + ", editableBySurveyor=" + editableBySurveyor
                + ", confidentialityType=" + getIdOrNull(confidentialityType)
                + ", category=" + getIdOrNull(category)
                + ", surveyorGuidance=" + surveyorGuidance
                + ", requireApproval=" + requireApproval
                + ", jobScopeConfirmed=" + jobScopeConfirmed
                + ", sequenceNumber=" + sequenceNumber
                + ", dateOfCrediting=" + getStringRepresentationOfDate(dateOfCrediting)
                + ", creditedBy=" + getIdOrNull(creditedBy)
                + ", getId()=" + getId() + "]";
    }
}
