package com.baesystems.ai.lr.dto.services;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ScheduledServiceDto extends BaseDto implements DueStatusUpdateableDto, StaleObject
{
    private static final long serialVersionUID = 248381680076140946L;

    private String stalenessHash;

    @NotNull
    private Long serviceCatalogueId;

    @Valid
    private LinkResource parentService;

    @Valid
    @NotNull
    private LinkResource asset;

    @Valid
    private LinkResource assetItem;

    private Integer cyclePeriodicity;

    @Valid
    private PostponementDto postponement;

    private Date assignedDate;

    private Date assignedDateManual;

    private Date dueDate;

    private LinkResource dueStatus;

    private Date dueDateManual;

    private Date lowerRangeDate;

    private Date lowerRangeDateManual;

    private Date upperRangeDate;

    private Date upperRangeDateManual;

    @Valid
    private LinkResource serviceStatus;

    @Valid
    private LinkResource serviceCreditStatus;

    private Date completionDate;

    @NotNull
    private Boolean provisionalDates;

    private Date creditingDate;

    private Integer serviceCycleNumber;

    private Integer serviceOccurenceNumber;

    @Size(message = "invalid length", max = 9)
    private String lastCreditedJob;

    @Size(message = "invalid length", max = 9)
    private String lastPartheldJob;

    private Boolean provisional;

    private Boolean continuous;

    private Boolean active;

    public Long getServiceCatalogueId()
    {
        return this.serviceCatalogueId;
    }

    public void setServiceCatalogueId(final Long serviceCatalogueId)
    {
        this.serviceCatalogueId = serviceCatalogueId;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    public Integer getCyclePeriodicity()
    {
        return this.cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Date getAssignedDate()
    {
        return this.assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getAssignedDateManual()
    {
        return this.assignedDateManual;
    }

    public void setAssignedDateManual(final Date assignedDateManual)
    {
        this.assignedDateManual = assignedDateManual;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public Date getDueDateManual()
    {
        return this.dueDateManual;
    }

    public void setDueDateManual(final Date dueDateManual)
    {
        this.dueDateManual = dueDateManual;
    }

    public Date getLowerRangeDate()
    {
        return this.lowerRangeDate;
    }

    public void setLowerRangeDate(final Date lowerRangeDate)
    {
        this.lowerRangeDate = lowerRangeDate;
    }

    public Date getLowerRangeDateManual()
    {
        return this.lowerRangeDateManual;
    }

    public void setLowerRangeDateManual(final Date lowerRangeDateManual)
    {
        this.lowerRangeDateManual = lowerRangeDateManual;
    }

    public Date getUpperRangeDate()
    {
        return this.upperRangeDate;
    }

    public void setUpperRangeDate(final Date upperRangeDate)
    {
        this.upperRangeDate = upperRangeDate;
    }

    public Date getUpperRangeDateManual()
    {
        return this.upperRangeDateManual;
    }

    public void setUpperRangeDateManual(final Date upperRangeDateManual)
    {
        this.upperRangeDateManual = upperRangeDateManual;
    }

    public LinkResource getServiceStatus()
    {
        return this.serviceStatus;
    }

    public void setServiceStatus(final LinkResource serviceStatus)
    {
        this.serviceStatus = serviceStatus;
    }

    public LinkResource getServiceCreditStatus()
    {
        return this.serviceCreditStatus;
    }

    public void setServiceCreditStatus(final LinkResource serviceCreditStatus)
    {
        this.serviceCreditStatus = serviceCreditStatus;
    }

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    public Boolean getProvisionalDates()
    {
        return this.provisionalDates;
    }

    public void setProvisionalDates(final Boolean provisionalDates)
    {
        this.provisionalDates = provisionalDates;
    }

    public Date getCreditingDate()
    {
        return this.creditingDate;
    }

    public void setCreditingDate(final Date creditingDate)
    {
        this.creditingDate = creditingDate;
    }

    public Integer getServiceCycleNumber()
    {
        return this.serviceCycleNumber;
    }

    public void setServiceCycleNumber(final Integer serviceCycleNumber)
    {
        this.serviceCycleNumber = serviceCycleNumber;
    }

    public Integer getServiceOccurenceNumber()
    {
        return this.serviceOccurenceNumber;
    }

    public void setServiceOccurenceNumber(final Integer serviceOccurenceNumber)
    {
        this.serviceOccurenceNumber = serviceOccurenceNumber;
    }

    public String getLastCreditedJob()
    {
        return this.lastCreditedJob;
    }

    public void setLastCreditedJob(final String lastCreditedJob)
    {
        this.lastCreditedJob = lastCreditedJob;
    }

    public String getLastPartheldJob()
    {
        return this.lastPartheldJob;
    }

    public void setLastPartheldJob(final String lastPartheldJob)
    {
        this.lastPartheldJob = lastPartheldJob;
    }

    public Boolean getProvisional()
    {
        return this.provisional;
    }

    public void setProvisional(final Boolean provisional)
    {
        this.provisional = provisional;
    }

    public Boolean getContinuous()
    {
        return this.continuous;
    }

    public void setContinuous(final Boolean continuous)
    {
        this.continuous = continuous;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(final Boolean active)
    {
        this.active = active;
    }

    public LinkResource getParentService()
    {
        return this.parentService;
    }

    public void setParentService(final LinkResource parentService)
    {
        this.parentService = parentService;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    public PostponementDto getPostponement()
    {
        return this.postponement;
    }

    public void setPostponement(final PostponementDto postponement)
    {
        this.postponement = postponement;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        // Note: Deliberately don't include dueStatus in the hash as this data is derived from other fields.

        String stringRepresentationOfPostponedDate = null;
        Long postponedTypeForHash = null;

        if (this.postponement != null)
        {
            stringRepresentationOfPostponedDate = this.getStringRepresentationOfDate(this.postponement.getDate());
            postponedTypeForHash = this.getIdOrNull(this.postponement.getPostponementType());
        }

        return "ScheduledServiceDto [serviceCatalogueId=" + this.serviceCatalogueId
                + ", parentService=" + this.getIdOrNull(this.parentService)
                + ", asset=" + this.getIdOrNull(this.asset)
                + ", assetItem=" + this.getIdOrNull(this.assetItem)
                + ", cyclePeriodicity=" + this.cyclePeriodicity
                + ", postponedDate=" + stringRepresentationOfPostponedDate
                + ", postponedType=" + postponedTypeForHash
                + ", assignedDate=" + this.getStringRepresentationOfDate(this.assignedDate)
                + ", assignedDateManual=" + this.getStringRepresentationOfDate(this.assignedDateManual)
                + ", dueDate=" + this.getStringRepresentationOfDate(this.dueDate)
                + ", dueDateManual=" + this.getStringRepresentationOfDate(this.dueDateManual)
                + ", lowerRangeDate=" + this.getStringRepresentationOfDate(this.lowerRangeDate)
                + ", lowerRangeDateManual=" + this.getStringRepresentationOfDate(this.lowerRangeDateManual)
                + ", upperRangeDate=" + this.getStringRepresentationOfDate(this.upperRangeDate)
                + ", upperRangeDateManual=" + this.getStringRepresentationOfDate(this.upperRangeDateManual)
                + ", serviceStatus=" + this.getIdOrNull(this.serviceStatus)
                + ", serviceCreditStatus=" + this.getIdOrNull(this.serviceCreditStatus)
                + ", completionDate=" + this.getStringRepresentationOfDate(this.completionDate)
                + ", provisionalDates=" + this.provisionalDates
                + ", creditingDate=" + this.getStringRepresentationOfDate(this.creditingDate)
                + ", serviceCycleNumber=" + this.serviceCycleNumber
                + ", serviceOccurenceNumber=" + this.serviceOccurenceNumber
                + ", lastCreditedJob=" + this.lastCreditedJob
                + ", lastPartheldJob=" + this.lastPartheldJob
                + ", provisional=" + this.provisional
                + ", continuous=" + this.continuous
                + ", active=" + (this.active == null ? Boolean.TRUE : this.active)
                + ", getId()=" + this.getId()
                + "]";
    }
}
