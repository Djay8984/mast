package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ProductTypeDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 1337993814944449072L;

    @Size(message = "invalid length", max = 45)
    private String name;

    @Size(message = "invalid length", max = 45)
    private String description;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }
}
