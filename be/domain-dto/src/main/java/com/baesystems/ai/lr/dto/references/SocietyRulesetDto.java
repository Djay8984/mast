package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class SocietyRulesetDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -233376735887388024L;

    @Size(message = "invalid length", max = 500)
    private String name;

    @Size(message = "invalid length", max = 500)
    private String description;

    @Size(message = "invalid length", max = 500)
    private String category;

    private Boolean isLrRuleset;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(final String category)
    {
        this.category = category;
    }

    public Boolean getIsLrRuleset()
    {
        return isLrRuleset;
    }

    public void setIsLrRuleset(final Boolean isLrRuleset)
    {
        this.isLrRuleset = isLrRuleset;
    }
}
