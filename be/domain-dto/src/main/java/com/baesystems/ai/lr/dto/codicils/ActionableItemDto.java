package com.baesystems.ai.lr.dto.codicils;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.FieldLessThanOrEqual;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@FieldLessThanOrEqual.List({
                            @FieldLessThanOrEqual(first = "imposedDate", second = "dueDate", useTime = false, message = "The due date cannot be before the imposed date")})
public class ActionableItemDto extends CodicilDto implements DueStatusUpdateableDto, StaleObject
{
    private static final long serialVersionUID = -7222445739741683607L;

    private String stalenessHash;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String title;

    @NotNull
    @Size(message = "invalid length", max = 2000)
    private String description;

    @NotNull
    private Boolean requireApproval;

    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    private Boolean editableBySurveyor;

    private List<LinkResource> childWIPs;

    @NotNull
    @Valid
    private LinkResource category;

    @NotNull
    @Valid
    private LinkResource confidentialityType;

    @Valid
    private LinkResource job;

    @Valid
    private LinkResource defect;

    @Valid
    private LinkResource asset;

    @Valid
    private ItemLightDto assetItem;

    @Valid
    private LinkResource employee;

    @Valid
    private LinkResource template;

    private LinkResource parent;

    private Boolean jobScopeConfirmed;

    private Integer sequenceNumber;

    private Date dateOfCrediting;

    private LinkResource scheduledService;

    @Valid
    private LinkResource creditedBy;

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Boolean getRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final Boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public Boolean getEditableBySurveyor()
    {
        return this.editableBySurveyor;
    }

    public void setEditableBySurveyor(final Boolean editableBySurveyor)
    {
        this.editableBySurveyor = editableBySurveyor;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public ItemLightDto getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final ItemLightDto assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public Boolean getJobScopeConfirmed()
    {
        return this.jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public List<LinkResource> getChildWIPs()
    {
        return this.childWIPs;
    }

    public void setChildWIPs(final List<LinkResource> childWIPs)
    {
        this.childWIPs = childWIPs;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getDateOfCrediting()
    {
        return this.dateOfCrediting;
    }

    public void setDateOfCrediting(final Date dateOfCrediting)
    {
        this.dateOfCrediting = dateOfCrediting;
    }

    public LinkResource getCreditedBy()
    {
        return this.creditedBy;
    }

    public void setCreditedBy(final LinkResource creditedBy)
    {
        this.creditedBy = creditedBy;
    }

    public LinkResource getScheduledService()
    {
        return this.scheduledService;
    }

    public void setScheduledService(final LinkResource scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "ActionableItemDto [title=" + this.title
                + ", description=" + this.description
                + ", imposedDate=" + getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + getStringRepresentationOfDate(this.getDueDate())
                + ", requireApproval=" + this.requireApproval
                + ", surveyorGuidance=" + this.surveyorGuidance
                + ", editableBySurveyor=" + this.editableBySurveyor
                + ", childWIPs=" + getStringRepresentationOfList(this.childWIPs)
                + ", category=" + getIdOrNull(this.category)
                + ", confidentialityType=" + getIdOrNull(this.confidentialityType)
                + ", job=" + getIdOrNull(this.job)
                + ", status=" + getIdOrNull(this.getStatus())
                + ", defect=" + getIdOrNull(this.defect)
                + ", asset=" + getIdOrNull(this.asset)
                + ", assetItem=" + (this.assetItem != null ? this.assetItem.getId() : null)
                + ", employee=" + getIdOrNull(this.employee)
                + ", template=" + getIdOrNull(this.template)
                + ", parent=" + getIdOrNull(this.parent)
                + ", jobScopeConfirmed=" + this.jobScopeConfirmed
                + ", sequenceNumber=" + this.sequenceNumber
                + ", dateOfCrediting=" + getStringRepresentationOfDate(this.dateOfCrediting)
                + ", creditedBy=" + getIdOrNull(this.creditedBy)
                + ", scheduledService=" + getIdOrNull(this.scheduledService)
                + ", getId()=" + getId() + "]";
    }
}
