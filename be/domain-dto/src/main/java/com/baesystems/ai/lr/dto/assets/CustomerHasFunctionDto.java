package com.baesystems.ai.lr.dto.assets;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class CustomerHasFunctionDto extends BaseDto
{
    private static final long serialVersionUID = -3731421555553712827L;

    @Valid
    private LinkResource customerFunction;

    public LinkResource getCustomerFunction()
    {
        return customerFunction;
    }

    public void setCustomerFunction(final LinkResource customerFunction)
    {
        this.customerFunction = customerFunction;
    }
}
