package com.baesystems.ai.lr.dto.cases;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.FlexibleMandatoryFields;
import com.fasterxml.jackson.annotation.JsonIgnore;

@FlexibleMandatoryFields.List({
                               @FlexibleMandatoryFields(profiles = {1L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                         2,
                                                                                                                                                         3,
                                                                                                                                                         4,
                                                                                                                                                         5,
                                                                                                                                                         7,
                                                                                                                                                         8,
                                                                                                                                                         9}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "classStatus",
                                                                                                                                                                                "proposedFlagState",
                                                                                                                                                                                "ruleSet",
                                                                                                                                                                                "classMaintenanceStatus",
                                                                                                                                                                                "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {3L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                         2,
                                                                                                                                                         3,
                                                                                                                                                         4,
                                                                                                                                                         5,
                                                                                                                                                         7,
                                                                                                                                                         8,
                                                                                                                                                         9}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "proposedFlagState"}),
                               @FlexibleMandatoryFields(profiles = {6L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                         2,
                                                                                                                                                         3,
                                                                                                                                                         4,
                                                                                                                                                         5,
                                                                                                                                                         7,
                                                                                                                                                         8,
                                                                                                                                                         9}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "estimatedBuildDate",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "classStatus",
                                                                                                                                                                                "proposedFlagState",
                                                                                                                                                                                "ruleSet",
                                                                                                                                                                                "classMaintenanceStatus",
                                                                                                                                                                                "coClassificationSociety",
                                                                                                                                                                                "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {10L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                          2,
                                                                                                                                                          3,
                                                                                                                                                          4,
                                                                                                                                                          5,
                                                                                                                                                          7,
                                                                                                                                                          8,
                                                                                                                                                          9}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "preEicInspectionStatus",
                                                                                                                                                                                 "riskAssessmentStatus",
                                                                                                                                                                                 "losingSociety",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "classStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "ruleSet",
                                                                                                                                                                                 "classMaintenanceStatus",
                                                                                                                                                                                 "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {13L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                          2,
                                                                                                                                                          3,
                                                                                                                                                          4,
                                                                                                                                                          5,
                                                                                                                                                          7,
                                                                                                                                                          8,
                                                                                                                                                          9}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "losingSociety",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "offices",
                                                                                                                                                                                 "surveyors"}),
                               @FlexibleMandatoryFields(profiles = {15L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {
                                                                                                                                                          2,
                                                                                                                                                          3,
                                                                                                                                                          4,
                                                                                                                                                          5,
                                                                                                                                                          7,
                                                                                                                                                          8,
                                                                                                                                                          9}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "ruleSet",
                                                                                                                                                                                 "buildDate"}),
                               // Closing
                               @FlexibleMandatoryFields(profiles = {1L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "classStatus",
                                                                                                                                                                                "proposedFlagState",
                                                                                                                                                                                "registeredPort",
                                                                                                                                                                                "ruleSet",
                                                                                                                                                                                "classMaintenanceStatus",
                                                                                                                                                                                "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {3L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "proposedFlagState",
                                                                                                                                                                                "registeredPort"}),
                               @FlexibleMandatoryFields(profiles = {6L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                "contractReferenceNumber",
                                                                                                                                                                                "estimatedBuildDate",
                                                                                                                                                                                "caseAcceptanceDate",
                                                                                                                                                                                "assetName",
                                                                                                                                                                                "imoNumber",
                                                                                                                                                                                "grossTonnage",
                                                                                                                                                                                "assetType",
                                                                                                                                                                                "assetLifecycleStatus",
                                                                                                                                                                                "classStatus",
                                                                                                                                                                                "proposedFlagState",
                                                                                                                                                                                "registeredPort",
                                                                                                                                                                                "ruleSet",
                                                                                                                                                                                "classMaintenanceStatus",
                                                                                                                                                                                "coClassificationSociety",
                                                                                                                                                                                "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {10L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "preEicInspectionStatus",
                                                                                                                                                                                 "riskAssessmentStatus",
                                                                                                                                                                                 "losingSociety",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "classStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "registeredPort",
                                                                                                                                                                                 "ruleSet",
                                                                                                                                                                                 "classMaintenanceStatus",
                                                                                                                                                                                 "buildDate"}),
                               @FlexibleMandatoryFields(profiles = {13L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "losingSociety",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "registeredPort"}),
                               @FlexibleMandatoryFields(profiles = {15L}, modeField = "caseType", secondaryModeField = "caseStatus", secondaryProfiles = {6}, mandatoryFields = {
                                                                                                                                                                                 "contractReferenceNumber",
                                                                                                                                                                                 "caseAcceptanceDate",
                                                                                                                                                                                 "assetName",
                                                                                                                                                                                 "imoNumber",
                                                                                                                                                                                 "grossTonnage",
                                                                                                                                                                                 "assetType",
                                                                                                                                                                                 "assetLifecycleStatus",
                                                                                                                                                                                 "proposedFlagState",
                                                                                                                                                                                 "registeredPort",
                                                                                                                                                                                 "ruleSet",
                                                                                                                                                                                 "buildDate"})})
public class CaseWithAssetDetailsDto extends CaseDto implements StaleObject
{
    private static final long serialVersionUID = -1708268865462317425L;

    private String stalenessHash;

    @Size(message = "invalid length", max = 50)
    private String assetName;

    @Min(value = 0)
    @Max(value = 9999999)
    private Long imoNumber;

    @Min(value = 0)
    @Max(value = 9999999)
    private Double grossTonnage;

    @Valid
    private LinkResource assetLifecycleStatus;

    @Valid
    private LinkResource assetType;

    @Valid
    private LinkResource classStatus;

    @Valid
    private LinkResource flagState;

    @Valid
    private LinkResource proposedFlagState;

    @Valid
    private LinkResource registeredPort;

    private Date dateOfRegistry;

    @Valid
    private LinkResource ruleSet;

    @Valid
    private LinkResource classMaintenanceStatus;

    @Size(message = "invalid length", max = 500)
    private String classNotation;

    @Valid
    private LinkResource coClassificationSociety;

    private Date buildDate;

    private Date estimatedBuildDate;

    private Date keelLayingDate;

    @Valid
    private LinkResource losingSociety;

    public String getAssetName()
    {
        return this.assetName;
    }

    public void setAssetName(final String assetName)
    {
        this.assetName = assetName;
    }

    public Long getImoNumber()
    {
        return this.imoNumber;
    }

    public void setImoNumber(final Long imoNumber)
    {
        this.imoNumber = imoNumber;
    }

    public Double getGrossTonnage()
    {
        return this.grossTonnage;
    }

    public void setGrossTonnage(final Double grossTonnage)
    {
        this.grossTonnage = grossTonnage;
    }

    public LinkResource getAssetLifecycleStatus()
    {
        return this.assetLifecycleStatus;
    }

    public void setAssetLifecycleStatus(final LinkResource assetLifecycleStatus)
    {
        this.assetLifecycleStatus = assetLifecycleStatus;
    }

    public LinkResource getAssetType()
    {
        return this.assetType;
    }

    public void setAssetType(final LinkResource assetType)
    {
        this.assetType = assetType;
    }

    public LinkResource getClassStatus()
    {
        return this.classStatus;
    }

    public void setClassStatus(final LinkResource classStatus)
    {
        this.classStatus = classStatus;
    }

    public LinkResource getFlagState()
    {
        return this.flagState;
    }

    public void setFlagState(final LinkResource flagState)
    {
        this.flagState = flagState;
    }

    public LinkResource getProposedFlagState()
    {
        return this.proposedFlagState;
    }

    public void setProposedFlagState(final LinkResource proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public LinkResource getRegisteredPort()
    {
        return this.registeredPort;
    }

    public void setRegisteredPort(final LinkResource registeredPort)
    {
        this.registeredPort = registeredPort;
    }

    public Date getDateOfRegistry()
    {
        return this.dateOfRegistry;
    }

    public void setDateOfRegistry(final Date dateOfRegistry)
    {
        this.dateOfRegistry = dateOfRegistry;
    }

    public LinkResource getRuleSet()
    {
        return this.ruleSet;
    }

    public void setRuleSet(final LinkResource ruleSet)
    {
        this.ruleSet = ruleSet;
    }

    public LinkResource getClassMaintenanceStatus()
    {
        return this.classMaintenanceStatus;
    }

    public void setClassMaintenanceStatus(final LinkResource classMaintenanceStatus)
    {
        this.classMaintenanceStatus = classMaintenanceStatus;
    }

    public String getClassNotation()
    {
        return this.classNotation;
    }

    public void setClassNotation(final String classNotation)
    {
        this.classNotation = classNotation;
    }

    public LinkResource getCoClassificationSociety()
    {
        return this.coClassificationSociety;
    }

    public void setCoClassificationSociety(final LinkResource coClassificationSociety)
    {
        this.coClassificationSociety = coClassificationSociety;
    }

    public Date getBuildDate()
    {
        return this.buildDate;
    }

    public void setBuildDate(final Date buildDate)
    {
        this.buildDate = buildDate;
    }

    public Date getEstimatedBuildDate()
    {
        return this.estimatedBuildDate;
    }

    public void setEstimatedBuildDate(final Date estimatedBuildDate)
    {
        this.estimatedBuildDate = estimatedBuildDate;
    }

    public Date getKeelLayingDate()
    {
        return this.keelLayingDate;
    }

    public void setKeelLayingDate(final Date keelLayingDate)
    {
        this.keelLayingDate = keelLayingDate;
    }

    public LinkResource getLosingSociety()
    {
        return this.losingSociety;
    }

    public void setLosingSociety(final LinkResource losing)
    {
        this.losingSociety = losing;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CaseWithAssetDetailsDto [assetName=" + this.assetName
                + ", imoNumber=" + this.imoNumber
                + ", grossTonnage=" + this.grossTonnage
                + ", assetLifecycleStatus=" + getIdOrNull(this.assetLifecycleStatus)
                + ", assetType=" + getIdOrNull(this.assetType)
                + ", classStatus=" + getIdOrNull(this.classStatus)
                + ", flagState=" + getIdOrNull(this.flagState)
                + ", proposedFlagState=" + getIdOrNull(this.proposedFlagState)
                + ", registeredPort=" + getIdOrNull(this.registeredPort)
                + ", dateOfRegistry=" + getStringRepresentationOfDate(this.dateOfRegistry)
                + ", ruleSet=" + getIdOrNull(this.ruleSet)
                + ", classMaintenanceStatus=" + getIdOrNull(this.classMaintenanceStatus)
                + ", classNotation=" + this.classNotation
                + ", coClassificationSociety=" + getIdOrNull(this.coClassificationSociety)
                + ", buildDate=" + getStringRepresentationOfDate(this.buildDate)
                + ", estimatedBuildDate=" + getStringRepresentationOfDate(this.estimatedBuildDate)
                + ", keelLayingDate=" + getStringRepresentationOfDate(this.keelLayingDate)
                + ", losingSociety=" + getIdOrNull(this.losingSociety)
                + ", getCaseType()=" + getIdOrNull(getCaseType())
                + ", getCaseStatus()=" + getIdOrNull(getCaseStatus())
                + ", getPreviousCaseStatus()=" + getIdOrNull(getPreviousCaseStatus())
                + ", getPreEicInspectionStatus()=" + getIdOrNull(getPreEicInspectionStatus())
                + ", getRiskAssessmentStatus()=" + getIdOrNull(getRiskAssessmentStatus())
                + ", getTocAcceptanceDate()=" + getStringRepresentationOfDate(getTocAcceptanceDate())
                + ", getContractReferenceNumber()=" + getContractReferenceNumber()
                + ", getSurveyors()=" + getStringRepresentationOfList(getSurveyors())
                + ", getOffices()=" + getStringRepresentationOfList(getOffices())
                + ", getCustomers()=" + getStringRepresentationOfList(getCustomers())
                + ", getCreatedOn()=" + getStringRepresentationOfDate(getCreatedOn())
                + ", getCaseAcceptanceDate()=" + getStringRepresentationOfDate(getCaseAcceptanceDate())
                + ", getMilestones()=" + getStringRepresentationOfList(getMilestones())
                + ", getStatusReason()=" + getStatusReason()
                + ", getCaseClosedDate()=" + getStringRepresentationOfDate(getCaseClosedDate())
                + ", getId()=" + getId() + "]";
    }
}
