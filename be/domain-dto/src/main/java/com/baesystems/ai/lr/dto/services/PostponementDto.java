package com.baesystems.ai.lr.dto.services;

import java.util.Date;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class PostponementDto extends BaseDto
{
    private static final long serialVersionUID = -4062812401882695995L;

    private Date date;

    @Valid
    private LinkResource postponementType;

    public Date getDate()
    {
        return this.date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    public LinkResource getPostponementType()
    {
        return this.postponementType;
    }

    public void setPostponementType(final LinkResource postponementType)
    {
        this.postponementType = postponementType;
    }

}
