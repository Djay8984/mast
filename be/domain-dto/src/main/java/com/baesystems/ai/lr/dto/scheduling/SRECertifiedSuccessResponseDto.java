package com.baesystems.ai.lr.dto.scheduling;

import java.util.List;

public class SRECertifiedSuccessResponseDto extends SREBaseResponseDto
{
    private static final long serialVersionUID = -2901611084070702765L;

    private List<SREServiceDto> services;

    private Integer hullIndicator;

    public List<SREServiceDto> getServices()
    {
        return services;
    }

    public void setServices(final List<SREServiceDto> services)
    {
        this.services = services;
    }

    public Integer getHullIndicator()
    {
        return hullIndicator;
    }

    public void setHullIndicator(final Integer hullIndicator)
    {
        this.hullIndicator = hullIndicator;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("SRECertifiedSuccessResponseDto [getServices()=");
        builder.append(getServices());
        builder.append(", getHullIndicator()=");
        builder.append(getHullIndicator());
        builder.append(", getMessage()=");
        builder.append(getMessage());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append("]");
        return builder.toString();
    }
}
