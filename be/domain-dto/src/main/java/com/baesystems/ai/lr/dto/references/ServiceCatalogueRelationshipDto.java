package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ServiceCatalogueRelationshipDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 4498887728886035960L;

    @NotNull
    @Valid
    private LinkResource toServiceCatalogue;

    @NotNull
    @Valid
    private LinkResource fromServiceCatalogue;

    @NotNull
    @Valid
    private LinkResource relationshipType;

    public LinkResource getToServiceCatalogue()
    {
        return toServiceCatalogue;
    }

    public void setToServiceCatalogue(final LinkResource toServiceCatalogue)
    {
        this.toServiceCatalogue = toServiceCatalogue;
    }

    public LinkResource getFromServiceCatalogue()
    {
        return fromServiceCatalogue;
    }

    public void setFromServiceCatalogue(final LinkResource fromServiceCatalogue)
    {
        this.fromServiceCatalogue = fromServiceCatalogue;
    }

    public LinkResource getRelationshipType()
    {
        return relationshipType;
    }

    public void setRelationshipType(final LinkResource relationshipType)
    {
        this.relationshipType = relationshipType;
    }
}
