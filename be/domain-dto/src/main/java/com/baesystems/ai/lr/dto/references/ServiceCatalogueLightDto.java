package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ServiceCatalogueLightDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -3224181422510117184L;

    private String name;

    private String code;

    private LinkResource serviceType;

    private LinkResource surveyType;

    private LinkResource defectCategory;

    private LinkResource serviceRuleset;

    private LinkResource serviceGroup;

    private LinkResource schedulingType;

    private Boolean continuousIndicator;

    private LinkResource harmonisationType;

    private Integer displayOrder;

    private Integer cyclePeriodicity;

    private Boolean cyclePeriodicityEditable;

    private Integer lowerRangeDateOffsetMonths;

    private Integer upperRangeDateOffsetMonths;

    private String description;

    private String categoryLetter;

    private Boolean multipleIndicator;

    private LinkResource schedulingRegime;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public LinkResource getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(final LinkResource serviceType)
    {
        this.serviceType = serviceType;
    }

    public LinkResource getSurveyType()
    {
        return surveyType;
    }

    public void setSurveyType(final LinkResource surveyType)
    {
        this.surveyType = surveyType;
    }

    public LinkResource getDefectCategory()
    {
        return defectCategory;
    }

    public void setDefectCategory(final LinkResource defectCategory)
    {
        this.defectCategory = defectCategory;
    }

    public LinkResource getServiceRuleset()
    {
        return serviceRuleset;
    }

    public void setServiceRuleset(final LinkResource serviceRuleset)
    {
        this.serviceRuleset = serviceRuleset;
    }

    public LinkResource getServiceGroup()
    {
        return serviceGroup;
    }

    public void setServiceGroup(final LinkResource serviceGroup)
    {
        this.serviceGroup = serviceGroup;
    }

    public LinkResource getSchedulingType()
    {
        return schedulingType;
    }

    public void setSchedulingType(final LinkResource schedulingType)
    {
        this.schedulingType = schedulingType;
    }

    public Boolean getContinuousIndicator()
    {
        return continuousIndicator;
    }

    public void setContinuousIndicator(final Boolean continuousIndicator)
    {
        this.continuousIndicator = continuousIndicator;
    }

    public LinkResource getHarmonisationType()
    {
        return harmonisationType;
    }

    public void setHarmonisationType(final LinkResource harmonisationType)
    {
        this.harmonisationType = harmonisationType;
    }

    public Integer getDisplayOrder()
    {
        return displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Integer getCyclePeriodicity()
    {
        return cyclePeriodicity;
    }

    public void setCyclePeriodicity(final Integer cyclePeriodicity)
    {
        this.cyclePeriodicity = cyclePeriodicity;
    }

    public Boolean getCyclePeriodicityEditable()
    {
        return cyclePeriodicityEditable;
    }

    public void setCyclePeriodicityEditable(final Boolean cyclePeriodicityEditable)
    {
        this.cyclePeriodicityEditable = cyclePeriodicityEditable;
    }

    public Integer getLowerRangeDateOffsetMonths()
    {
        return lowerRangeDateOffsetMonths;
    }

    public void setLowerRangeDateOffsetMonths(final Integer lowerRangeDateOffsetMonths)
    {
        this.lowerRangeDateOffsetMonths = lowerRangeDateOffsetMonths;
    }

    public Integer getUpperRangeDateOffsetMonths()
    {
        return upperRangeDateOffsetMonths;
    }

    public void setUpperRangeDateOffsetMonths(final Integer upperRangeDateOffsetMonths)
    {
        this.upperRangeDateOffsetMonths = upperRangeDateOffsetMonths;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getCategoryLetter()
    {
        return categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public Boolean getMultipleIndicator()
    {
        return multipleIndicator;
    }

    public void setMultipleIndicator(final Boolean multipleIndicator)
    {
        this.multipleIndicator = multipleIndicator;
    }

    public LinkResource getSchedulingRegime()
    {
        return schedulingRegime;
    }

    public void setSchedulingRegime(final LinkResource schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }
}
