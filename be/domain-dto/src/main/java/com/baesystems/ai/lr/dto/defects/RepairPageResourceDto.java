package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class RepairPageResourceDto extends BasePageResource<RepairDto>
{
    private List<RepairDto> content;

    @Override
    public List<RepairDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<RepairDto> content)
    {
        this.content = content;
    }

}
