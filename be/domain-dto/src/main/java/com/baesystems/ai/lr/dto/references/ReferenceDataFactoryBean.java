package com.baesystems.ai.lr.dto.references;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ReferenceDataFactoryBean
{
    private Map<String, Map<String, Object>> referenceData;

    public Map<String, Map<String, Object>> getReferenceData()
    {
        return getFormattedReferenceData(this.referenceData);
    }

    public void setReferenceData(final Map<String, Map<String, Object>> referenceData)
    {
        this.referenceData = referenceData;
    }

    public ReferenceDataMapDto createInstance(final String requiredSubset, final String requiredRefData) throws Exception
    {
        final Map<String, Object> newRefData = new HashMap<String, Object>();

        if (requiredRefData == null && requiredSubset != null && referenceData.containsKey(requiredSubset))
        {
            final Map<String, Object> refDataMap = referenceData.get(requiredSubset);
            newRefData.putAll(refDataMap);
        }

        else if (requiredSubset != null && requiredRefData != null && referenceData.containsKey(requiredSubset)
                && referenceData.get(requiredSubset).containsKey(requiredRefData))
        {
            final Object refData = referenceData.get(requiredSubset).get(requiredRefData);
            newRefData.put(requiredRefData, refData);
        }

        final ReferenceDataMapDto refData = new ReferenceDataMapDto();
        refData.setReferenceData(getFormattedReferenceDataSubset(newRefData));

        return refData;
    }

    /**
     * Takes a map with nested maps whose keys are kebab case strings and returns the same map containing nested maps
     * but with the keys as camel case strings.
     *
     * @param refData
     * @return formattedReferenceData
     */
    private Map<String, Map<String, Object>> getFormattedReferenceData(final Map<String, Map<String, Object>> refData)
    {
        final Map<String, Map<String, Object>> formattedReferenceData = new HashMap<String, Map<String, Object>>();

        for (Entry<String, Map<String, Object>> ref : refData.entrySet())
        {
            formattedReferenceData.put(ref.getKey(), getFormattedReferenceDataSubset(ref.getValue()));
        }

        return formattedReferenceData;
    }

    /**
     * Takes a map with keys that are kebab case strings and returns the same map but with the keys as camel case
     * strings.
     *
     * @param refDataSubset
     * @return formattedReferenceDataSubset
     */
    private Map<String, Object> getFormattedReferenceDataSubset(final Map<String, Object> refDataSubset)
    {
        final Map<String, Object> formattedReferenceDataSubset = new HashMap<String, Object>();

        for (Entry<String, Object> ref : refDataSubset.entrySet())
        {
            final String camelKey = kebabToCamel(ref.getKey());
            formattedReferenceDataSubset.put(camelKey, ref.getValue());
        }

        return formattedReferenceDataSubset;
    }

    /**
     * Converts a string from kebab case format to camel case format.<br>
     * e.g. some-string-or-other --> someStringOrOther
     *
     * @param kebabString
     * @return camelString
     */
    private String kebabToCamel(final String kebabString)
    {
        String camelString = null;

        if (kebabString != null)
        {
            final String[] words = kebabString.split("-");

            final StringBuilder camelBuilder = new StringBuilder();
            camelBuilder.append(words[0]);

            for (int i = 1; i < words.length; i++)
            {
                camelBuilder.append(words[i].substring(0, 1).toUpperCase());
                camelBuilder.append(words[i].substring(1));
            }

            camelString = camelBuilder.toString();
        }

        return camelString;
    }
}
