package com.baesystems.ai.lr.dto.defects;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class JobDefectDto extends BaseDto
{
    private static final long serialVersionUID = -6015916015076970462L;

    @NotNull
    @SubIdNotNull
    private LinkResource job;

    public LinkResource getJob()
    {
        return job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }
}
