package com.baesystems.ai.lr.dto.query;

import java.util.List;

import javax.validation.constraints.Size;

public class CaseQueryDto implements QueryDto
{
    private static final long serialVersionUID = 4460513661829394805L;

    @Size(min = 1)
    private List<Long> caseStatusId;

    @Size(min = 1)
    private List<Long> employeeId;

    @Size(min = 1)
    private List<Long> assetId;

    @Size(min = 1)
    private List<Long> caseTypeId;

    public List<Long> getCaseStatusId()
    {
        return caseStatusId;
    }

    public void setCaseStatusId(final List<Long> caseStatusId)
    {
        this.caseStatusId = caseStatusId;
    }

    public List<Long> getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(final List<Long> employeeId)
    {
        this.employeeId = employeeId;
    }

    public List<Long> getAssetId()
    {
        return assetId;
    }

    public void setAssetId(final List<Long> assetId)
    {
        this.assetId = assetId;
    }

    public List<Long> getCaseTypeId()
    {
        return caseTypeId;
    }

    public void setCaseTypeId(final List<Long> caseTypeId)
    {
        this.caseTypeId = caseTypeId;
    }
}
