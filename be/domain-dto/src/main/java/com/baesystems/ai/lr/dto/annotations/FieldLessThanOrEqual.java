package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = FieldLessThanOrEqualValidator.class)
@Documented
public @interface FieldLessThanOrEqual
{
    String message()

    default "invalid field relationship";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The first field
     */
    String first();

    /**
     * @return The second field
     */
    String second();

    /**
     * @return Whether or not to use the time in date comparisons (true or null the time is used, false only the date is
     *         used)
     */
    boolean useTime();

    /**
     * Annotation for testing if the first named field is less than or equal to the second named field. Validation must
     * be added at the class level and can take a list of field pairs, which must be of the same type and must be
     * comparable (Double, Float, Long, Integer and Date are accepted).
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        FieldLessThanOrEqual[] value();
    }
}
