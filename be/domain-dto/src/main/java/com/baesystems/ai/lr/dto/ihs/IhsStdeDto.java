package com.baesystems.ai.lr.dto.ihs;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsStdeDto extends BaseDto
{
    private static final long serialVersionUID = 1099304587715857288L;

    @Size(message = "invalid length", max = 2)
    private String decks;

    public String getDecks()
    {
        return decks;
    }

    public void setDecks(final String decks)
    {
        this.decks = decks;
    }
}
