package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class ReferenceDataDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -233376735887388024L;

    private String name;

    private String description;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }
}
