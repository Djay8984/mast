package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class PortOfRegistryDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = -6052030491095514197L;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String name;

    @Valid
    private OfficeDto sdo;

    public final String getName()
    {
        return name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public OfficeDto getSdo()
    {
        return sdo;
    }

    public void setSdo(final OfficeDto sdo)
    {
        this.sdo = sdo;
    }
}
