package com.baesystems.ai.lr.dto.cases;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;

public class CaseDto extends BaseCaseDto
{
    private static final long serialVersionUID = -6648664306570326177L;

    @SubIdNotNull
    @Valid
    private LinkResource asset;

    public LinkResource getAsset()
    {
        return asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }
}
