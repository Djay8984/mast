package com.baesystems.ai.lr.dto.references;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;

public class DefectCategoryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = 6193987923324562488L;

    @Size(message = "invalid length", max = 1)
    private String categoryLetter;

    @Valid
    private LinkResource parent;

    public String getCategoryLetter()
    {
        return categoryLetter;
    }

    public void setCategoryLetter(final String categoryLetter)
    {
        this.categoryLetter = categoryLetter;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }
}
