package com.baesystems.ai.lr.dto.query;

import java.util.List;

public class AttachmentLinkQueryDto implements QueryDto
{
    private static final long serialVersionUID = -1891604257764430673L;
    private List<Long> defect;
    private List<Long> wipDefect;
    private List<Long> job;
    private List<Long> coC;
    private List<Long> wipCoC;
    private List<Long> assetNote;
    private List<Long> wipAssetNote;
    private List<Long> asset;
    private List<Long> item;
    private List<Long> milestone;
    private List<Long> repair;
    private List<Long> certificate;
    private List<Long> report;
    private List<Long> aCase;
    private List<Long> service;
    private List<Long> actionableItem;
    private List<Long> wipActionableItem;
    private List<Long> workItem;
    private List<Long> wipWorkItem;
    private List<Long> survey;

    public void setDefect(final List<Long> defect)
    {
        this.defect = defect;
    }

    public void setWipDefect(final List<Long> wipDefect)
    {
        this.wipDefect = wipDefect;
    }

    public void setJob(final List<Long> job)
    {
        this.job = job;
    }

    public void setCoC(final List<Long> coC)
    {
        this.coC = coC;
    }

    public void setWipCoC(final List<Long> wipCoC)
    {
        this.wipCoC = wipCoC;
    }

    public void setAssetNote(final List<Long> assetNote)
    {
        this.assetNote = assetNote;
    }

    public void setWipAssetNote(final List<Long> wipAssetNote)
    {
        this.wipAssetNote = wipAssetNote;
    }

    public void setAsset(final List<Long> asset)
    {
        this.asset = asset;
    }

    public void setItem(final List<Long> item)
    {
        this.item = item;
    }

    public void setMilestone(final List<Long> milestone)
    {
        this.milestone = milestone;
    }

    public void setRepair(final List<Long> repair)
    {
        this.repair = repair;
    }

    public void setCertificate(final List<Long> certificate)
    {
        this.certificate = certificate;
    }

    public void setReport(final List<Long> report)
    {
        this.report = report;
    }

    public void setaCase(final List<Long> aCase)
    {
        this.aCase = aCase;
    }

    public void setService(final List<Long> service)
    {
        this.service = service;
    }

    public void setActionableItem(final List<Long> actionableItem)
    {
        this.actionableItem = actionableItem;
    }

    public void setWipActionableItem(final List<Long> wipActionableItem)
    {
        this.wipActionableItem = wipActionableItem;
    }

    public List<Long> getDefect()
    {
        return defect;
    }

    public List<Long> getWipDefect()
    {
        return wipDefect;
    }

    public List<Long> getJob()
    {
        return job;
    }

    public List<Long> getCoC()
    {
        return coC;
    }

    public List<Long> getWipCoC()
    {
        return wipCoC;
    }

    public List<Long> getAssetNote()
    {
        return assetNote;
    }

    public List<Long> getWipAssetNote()
    {
        return wipAssetNote;
    }

    public List<Long> getAsset()
    {
        return asset;
    }

    public List<Long> getItem()
    {
        return item;
    }

    public List<Long> getMilestone()
    {
        return milestone;
    }

    public List<Long> getRepair()
    {
        return repair;
    }

    public List<Long> getCertificate()
    {
        return certificate;
    }

    public List<Long> getReport()
    {
        return report;
    }

    public List<Long> getaCase()
    {
        return aCase;
    }

    public List<Long> getService()
    {
        return service;
    }

    public List<Long> getActionableItem()
    {
        return actionableItem;
    }

    public List<Long> getWipActionableItem()
    {
        return wipActionableItem;
    }

    public List<Long> getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final List<Long> workItem)
    {
        this.workItem = workItem;
    }

    public List<Long> getWipWorkItem()
    {
        return wipWorkItem;
    }

    public void setWipWorkItem(final List<Long> wipWorkItem)
    {
        this.wipWorkItem = wipWorkItem;
    }

    public List<Long> getSurvey()
    {
        return survey;
    }

    public void setSurvey(final List<Long> survey)
    {
        this.survey = survey;
    }
}
