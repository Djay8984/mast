package com.baesystems.ai.lr.dto.defects;

import com.baesystems.ai.lr.dto.StaleObject;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DeficiencyDto extends DeficiencyLightDto implements StaleObject
{
    private static final long serialVersionUID = 999757624122048500L;

    private String stalenessHash;

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DeficiencyDto [stalenessHash=" + this.stalenessHash
                + ", jobs=" + getStringRepresentationOfList(getJobs())
                + ", getSequenceNumber()=" + getSequenceNumber()
                + ", getFirstFrameNumber()=" + getFirstFrameNumber()
                + ", getLastFrameNumber()=" + getLastFrameNumber()
                + ", getIncidentDate()=" + getStringRepresentationOfDate(getIncidentDate())
                + ", getIncidentDescription()=" + getIncidentDescription()
                + ", getTitle()=" + getTitle()
                + ", getAsset()=" + getIdOrNull(getAsset())
                + ", getRepairCount()=" + getRepairCount()
                + ", getCourseOfActionCount()=" + getCourseOfActionCount()
                + ", getJob()=" + getIdOrNull(getJob())
                //+ ", getParent()=" + getIdOrNull(getParent())
                + ", getPromptThoroughRepair()=" + getPromptThoroughRepair()
                + ", getConfidentialityType()=" + getIdOrNull(getConfidentialityType())
                + ", getId()=" + getId() + "]";
    }
}
