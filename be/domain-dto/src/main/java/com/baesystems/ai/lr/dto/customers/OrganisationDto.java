package com.baesystems.ai.lr.dto.customers;

import javax.validation.constraints.NotNull;

public class OrganisationDto extends PartyDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    @NotNull
    private Boolean approvalStatus;

    public Boolean getApprovalStatus()
    {
        return approvalStatus;
    }

    public void setApprovalStatus(final Boolean approvalStatus)
    {
        this.approvalStatus = approvalStatus;
    }
}
