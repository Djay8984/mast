package com.baesystems.ai.lr.dto;

public interface StaleObject
{
    public String getStalenessHash();

    public void setStalenessHash(String newStalenessHash);

    public String getStringRepresentationForHash();
}
