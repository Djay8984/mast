package com.baesystems.ai.lr.dto.scheduling;

import java.util.Date;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class SREServiceDto implements RuleEngineDto
{
    private static final long serialVersionUID = 2112068610703020797L;

    private Integer serviceCatalogueIdentifier;

    private Integer schedulingRegime;

    private Integer occurenceNumber;

    private Date assignedDate;

    private Date dueDate;

    private Date lowerRangeDate;

    private Date upperRangeDate;

    public Integer getServiceCatalogueIdentifier()
    {
        return serviceCatalogueIdentifier;
    }

    public void setServiceCatalogueIdentifier(final Integer serviceCatalogueIdentifier)
    {
        this.serviceCatalogueIdentifier = serviceCatalogueIdentifier;
    }

    public Integer getSchedulingRegime()
    {
        return schedulingRegime;
    }

    public void setSchedulingRegime(final Integer schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    public Integer getOccurenceNumber()
    {
        return occurenceNumber;
    }

    public void setOccurenceNumber(final Integer occurenceNumber)
    {
        this.occurenceNumber = occurenceNumber;
    }

    public Date getAssignedDate()
    {
        return assignedDate;
    }

    public void setAssignedDate(final Date assignedDate)
    {
        this.assignedDate = assignedDate;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getLowerRangeDate()
    {
        return lowerRangeDate;
    }

    public void setLowerRangeDate(final Date lowerRangeDate)
    {
        this.lowerRangeDate = lowerRangeDate;
    }

    public Date getUpperRangeDate()
    {
        return upperRangeDate;
    }

    public void setUpperRangeDate(final Date upperRangeDate)
    {
        this.upperRangeDate = upperRangeDate;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("SREServiceDto [getServiceCatalogueIdentifier()=");
        builder.append(getServiceCatalogueIdentifier());
        builder.append(", getSchedulingRegime()=");
        builder.append(getSchedulingRegime());
        builder.append(", getOccurenceNumber()=");
        builder.append(getOccurenceNumber());
        builder.append(", getAssignedDate()=");
        builder.append(getAssignedDate());
        builder.append(", getDueDate()=");
        builder.append(getDueDate());
        builder.append(", getLowerRangeDate()=");
        builder.append(getLowerRangeDate());
        builder.append(", getUpperRangeDate()=");
        builder.append(getUpperRangeDate());
        builder.append("]");
        return builder.toString();
    }
}
