package com.baesystems.ai.lr.dto.validation;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.BaseDto;

public class ItemRelationshipDto extends BaseDto
{
    private static final long serialVersionUID = -5561243473451566709L;

    @NotNull
    private ItemLightDto toItem;
    private LinkResource type;

    public ItemLightDto getToItem()
    {
        return toItem;
    }

    public void setToItem(final ItemLightDto toItem)
    {
        this.toItem = toItem;
    }

    public LinkResource getType()
    {
        return type;
    }

    public void setType(final LinkResource type)
    {
        this.type = type;
    }
}
