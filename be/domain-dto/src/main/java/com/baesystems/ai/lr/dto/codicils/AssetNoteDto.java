package com.baesystems.ai.lr.dto.codicils;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class AssetNoteDto extends CodicilDto implements DueStatusUpdateableDto, StaleObject
{
    private static final long serialVersionUID = 6721935950386166905L;

    private String stalenessHash;

    private String templateName;

    @NotNull
    @Size(message = "invalid length", min = 1, max = 2000)
    private String description;

    @Size(message = "invalid length", max = 45)
    private String affectedItems;

    @Valid
    private LinkResource asset;

    @Valid
    private ItemLightDto assetItem;

    private LinkResource parent;

    private List<LinkResource> childWIPs;

    @Valid
    private LinkResource job;

    @Size(message = "invalid length", max = 100)
    private String actionTaken;

    private LinkResource employee;

    @Valid
    private LinkResource template;

    @Valid
    private Boolean inheritedFlag;

    @NotNull
    @Size(message = "invalid length", min = 1, max = 50)
    private String title;

    @NotNull
    @Valid
    private LinkResource category;

    @Size(message = "invalid length", max = 2000)
    private String surveyorGuidance;

    @NotNull
    @Valid
    private LinkResource confidentialityType;

    @NotNull
    private boolean requireApproval;

    private Boolean jobScopeConfirmed;

    private Integer sequenceNumber;

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public String getTemplateName()
    {
        return this.templateName;
    }

    public void setTemplateName(final String templateName)
    {
        this.templateName = templateName;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getAffectedItems()
    {
        return this.affectedItems;
    }

    public void setAffectedItems(final String affectedItems)
    {
        this.affectedItems = affectedItems;
    }

    public LinkResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkResource asset)
    {
        this.asset = asset;
    }

    public LinkResource getJob()
    {
        return this.job;
    }

    public void setJob(final LinkResource job)
    {
        this.job = job;
    }

    public String getActionTaken()
    {
        return this.actionTaken;
    }

    public void setActionTaken(final String actionTaken)
    {
        this.actionTaken = actionTaken;
    }

    public LinkResource getEmployee()
    {
        return this.employee;
    }

    public void setEmployee(final LinkResource employee)
    {
        this.employee = employee;
    }

    public Boolean getInheritedFlag()
    {
        return this.inheritedFlag;
    }

    public void setInheritedFlag(final Boolean inheritedFlag)
    {
        this.inheritedFlag = inheritedFlag;
    }

    public ItemLightDto getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final ItemLightDto assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkResource getTemplate()
    {
        return this.template;
    }

    public void setTemplate(final LinkResource template)
    {
        this.template = template;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public LinkResource getCategory()
    {
        return this.category;
    }

    public void setCategory(final LinkResource category)
    {
        this.category = category;
    }

    public String getSurveyorGuidance()
    {
        return this.surveyorGuidance;
    }

    public void setSurveyorGuidance(final String surveyorGuidance)
    {
        this.surveyorGuidance = surveyorGuidance;
    }

    public LinkResource getConfidentialityType()
    {
        return this.confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public boolean isRequireApproval()
    {
        return this.requireApproval;
    }

    public void setRequireApproval(final boolean requireApproval)
    {
        this.requireApproval = requireApproval;
    }

    public Boolean getJobScopeConfirmed()
    {
        return this.jobScopeConfirmed;
    }

    public void setJobScopeConfirmed(final Boolean jobScopeConfirmed)
    {
        this.jobScopeConfirmed = jobScopeConfirmed;
    }

    public List<LinkResource> getChildWIPs()
    {
        return this.childWIPs;
    }

    public void setChildWIPs(final List<LinkResource> childWIPs)
    {
        this.childWIPs = childWIPs;
    }

    public Integer getSequenceNumber()
    {
        return this.sequenceNumber;
    }

    public void setSequenceNumber(final Integer sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "AssetNoteDto [templateName=" + this.templateName
                + ", imposedDate=" + getStringRepresentationOfDate(this.getImposedDate())
                + ", dueDate=" + getStringRepresentationOfDate(this.getDueDate())
                + ", description=" + this.description
                + ", affectedItems=" + this.affectedItems
                + ", status=" + getIdOrNull(this.getStatus())
                + ", asset=" + getIdOrNull(this.asset)
                + ", assetItem=" + (this.assetItem != null ? this.assetItem.getId() : null)
                + ", parent=" + getIdOrNull(this.parent)
                + ", childWIPs=" + getStringRepresentationOfList(this.childWIPs)
                + ", job=" + getIdOrNull(this.job)
                + ", actionTaken=" + this.actionTaken
                + ", employee=" + getIdOrNull(this.employee)
                + ", template=" + getIdOrNull(this.template)
                + ", inheritedFlag=" + this.inheritedFlag
                + ", title=" + this.title
                + ", category=" + getIdOrNull(this.category)
                + ", surveyorGuidance=" + this.surveyorGuidance
                + ", confidentialityType=" + getIdOrNull(this.confidentialityType)
                + ", requireApproval=" + this.requireApproval
                + ", jobScopeConfirmed=" + this.jobScopeConfirmed
                + ", sequenceNumber=" + this.sequenceNumber
                + ", getId()=" + getId() + "]";
    }
}
