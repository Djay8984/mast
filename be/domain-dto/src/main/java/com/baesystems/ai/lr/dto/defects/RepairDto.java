package com.baesystems.ai.lr.dto.defects;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.DeletableDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class RepairDto extends DeletableDto implements StaleObject
{
    private static final long serialVersionUID = 2207746146624404895L;

    private String stalenessHash;

    @NotNull
    private Boolean promptThoroughRepair;

    @NotNull
    @Size(message = "invalid length", max = 2000)
    private String description;

    @Size(message = "invalid length", max = 5000)
    private String repairTypeDescription;

    @Size(message = "invalid length", max = 1000)
    private String narrative;

    @Valid
    private LinkResource defect;

    @NotNull
    @Valid
    private List<LinkResource> repairTypes;

    @NotNull
    @Valid
    private LinkResource repairAction;

    @Valid
    private List<LinkResource> materialsUsed;

    @NotNull
    @Valid
    private List<RepairItemDto> repairs;

    @Valid
    private LinkResource parent;

    @Valid
    private LinkResource codicil;

    @NotNull
    private Boolean confirmed;

    private Date updatedDate;

    @Valid
    private SurveyDto survey;

    public LinkResource getDefect()
    {
        return this.defect;
    }

    public void setDefect(final LinkResource defect)
    {
        this.defect = defect;
    }

    public Boolean getPromptThoroughRepair()
    {
        return this.promptThoroughRepair;
    }

    public void setPromptThoroughRepair(final Boolean promptThoroughRepair)
    {
        this.promptThoroughRepair = promptThoroughRepair;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public String getRepairTypeDescription()
    {
        return this.repairTypeDescription;
    }

    public void setRepairTypeDescription(final String repairTypeDescription)
    {
        this.repairTypeDescription = repairTypeDescription;
    }

    public String getNarrative()
    {
        return this.narrative;
    }

    public void setNarrative(final String narrative)
    {
        this.narrative = narrative;
    }

    public List<LinkResource> getRepairTypes()
    {
        return this.repairTypes;
    }

    public void setRepairTypes(final List<LinkResource> repairTypes)
    {
        this.repairTypes = repairTypes;
    }

    public LinkResource getRepairAction()
    {
        return this.repairAction;
    }

    public void setRepairAction(final LinkResource repairAction)
    {
        this.repairAction = repairAction;
    }

    public List<LinkResource> getMaterialsUsed()
    {
        return this.materialsUsed;
    }

    public void setMaterialsUsed(final List<LinkResource> materialsUsed)
    {
        this.materialsUsed = materialsUsed;
    }

    public List<RepairItemDto> getRepairs()
    {
        return this.repairs;
    }

    public void setRepairs(final List<RepairItemDto> repairs)
    {
        this.repairs = repairs;
    }

    public Boolean getConfirmed()
    {
        return this.confirmed;
    }

    public void setConfirmed(final Boolean confirmed)
    {
        this.confirmed = confirmed;
    }

    public Date getUpdatedDate()
    {
        return this.updatedDate;
    }

    public void setUpdatedDate(final Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public LinkResource getParent()
    {
        return this.parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }

    public LinkResource getCodicil()
    {
        return this.codicil;
    }

    public void setCodicil(final LinkResource codicil)
    {
        this.codicil = codicil;
    }

    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "RepairDto [promptThoroughRepair=" + this.promptThoroughRepair
                + ", description=" + this.description
                + ", repairTypeDescription=" + this.repairTypeDescription
                + ", narrative=" + this.narrative
                + ", defect=" + getIdOrNull(this.defect)
                + ", repairTypes=" + getStringRepresentationOfList(this.repairTypes)
                + ", repairAction=" + getIdOrNull(this.repairAction)
                + ", materialsUsed=" + getStringRepresentationOfList(this.materialsUsed)
                + ", repairs=" + getStringRepresentationOfList(this.repairs)
                + ", parent=" + getIdOrNull(this.parent)
                + ", codicil=" + getIdOrNull(this.codicil)
                + ", confirmed=" + this.confirmed
                + ", getId()=" + getId() + "]";
    }
}
