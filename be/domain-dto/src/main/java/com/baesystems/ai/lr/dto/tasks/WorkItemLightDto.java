package com.baesystems.ai.lr.dto.tasks;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class WorkItemLightDto extends BaseWorkItemDto implements StaleObject
{
    private static final long serialVersionUID = 8252436811258196440L;

    private String stalenessHash;

    @Valid
    @NotNull
    private LinkResource assetItem;

    @Valid
    private LinkResource survey;

    @Valid
    private LinkResource scheduledService;

    @Valid
    private WorkItemLightDto conditionalParent;

    @Valid
    private WorkItemLightDto workItem;

    public LinkResource getAssetItem()
    {
        return this.assetItem;
    }

    public void setAssetItem(final LinkResource assetItem)
    {
        this.assetItem = assetItem;
    }

    public LinkResource getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final LinkResource survey)
    {
        this.survey = survey;
    }

    public LinkResource getScheduledService()
    {
        return this.scheduledService;
    }

    public void setScheduledService(final LinkResource scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    public WorkItemLightDto getConditionalParent()
    {
        return this.conditionalParent;
    }

    public void setConditionalParent(final WorkItemLightDto conditionalParent)
    {
        this.conditionalParent = conditionalParent;
    }

    public WorkItemLightDto getWorkItem()
    {
        return this.workItem;
    }

    public void setWorkItem(final WorkItemLightDto workItem)
    {
        this.workItem = workItem;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "WorkItemLightDto [assetItem=" + getIdOrNull(this.assetItem)
                + ", survey=" + getIdOrNull(this.survey)
                + ", scheduledService=" + getIdOrNull(this.scheduledService)
                + ", conditionalParent=" + getIdOrNull(this.conditionalParent)
                + ", workItem=" + getIdOrNull(this.workItem)
                + ", getReferenceCode()=" + getReferenceCode()
                + ", getName()=" + getName()
                + ", getDescription()=" + getDescription()
                + ", getLongDescription()=" + getLongDescription()
                + ", getServiceCode()=" + getServiceCode()
                + ", getWorkItemType()=" + getIdOrNull(getWorkItemType())
                + ", getWorkItemAction()=" + getIdOrNull(getWorkItemAction())
                + ", getAttributeMandatory()=" + getAttributeMandatory()
                + ", getDueDate()=" + getStringRepresentationOfDate(getDueDate())
                + ", getNotes()=" + getNotes()
                + ", getCodicil()=" + getIdOrNull(getCodicil())
                + ", getAttachmentRequired()=" + getAttachmentRequired()
                + ", getItemOrder()=" + getItemOrder()
                + ", getResolutionDate()=" + getStringRepresentationOfDate(getResolutionDate())
                + ", getResolutionStatus()=" + getIdOrNull(getResolutionStatus())
                + ", getClassSociety()=" + getIdOrNull(getClassSociety())
                + ", getAssignedTo()=" + getIdOrNull(getAssignedTo())
                + ", getResolvedBy()=" + getIdOrNull(getResolvedBy())
                + ", getCreditStatus()=" + getIdOrNull(getCreditStatus())
                + ", getTaskNumber()=" + getTaskNumber()
                + ", getAttributes()=" + getStringRepresentationOfList(getAttributes())
                + ", getParent()=" + getIdOrNull(getParent())
                + ", getAssignedDate()=" + getStringRepresentationOfDate(getAssignedDate())
                + ", getDueStatus()=" + getIdOrNull(getDueStatus())
                + ", getActionTakenDate()=" + getStringRepresentationOfDate(getActionTakenDate())
                + ", getId()=" + getId() + "]";
    }
}
