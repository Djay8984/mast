package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.services.SurveyDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DefectDto extends DefectLightDto implements StaleObject
{
    private static final long serialVersionUID = 2207746146624404895L;

    private String stalenessHash;



    @Valid
    private SurveyDto survey;



    public SurveyDto getSurvey()
    {
        return this.survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "DefectDto [stalenessHash=" + this.stalenessHash
                + ", jobs=" + getStringRepresentationOfList(getJobs())
                + ", survey=" + getIdOrNull(this.survey)
                + ", getSequenceNumber()=" + getSequenceNumber()
                + ", getFirstFrameNumber()=" + getFirstFrameNumber()
                + ", getLastFrameNumber()=" + getLastFrameNumber()
                + ", getIncidentDate()=" + getStringRepresentationOfDate(getIncidentDate())
                + ", getIncidentDescription()=" + getIncidentDescription()
                + ", getTitle()=" + getTitle()
                + ", getDefectCategory()=" + getIdOrNull(getDefectCategory())
                + ", getDefectStatus()=" + getIdOrNull(getDefectStatus())
                + ", getAsset()=" + getIdOrNull(getAsset())
                + ", getValues()=" + getStringRepresentationOfList(getValues())
                + ", getItems()=" + getStringRepresentationOfList(getItems())
                + ", getRepairCount()=" + getRepairCount()
                + ", getCourseOfActionCount()=" + getCourseOfActionCount()
                + ", getJob()=" + getIdOrNull(getJob())
                + ", getParent()=" + getIdOrNull(getParent())
                + ", getPromptThoroughRepair()=" + getPromptThoroughRepair()
                + ", getConfidentialityType()=" + getIdOrNull(getConfidentialityType())
                + ", getId()=" + getId() + "]";
    }
}
