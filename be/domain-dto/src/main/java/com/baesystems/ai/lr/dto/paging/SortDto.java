package com.baesystems.ai.lr.dto.paging;

import java.util.List;

public class SortDto
{
    private List<OrderDto> orders;

    public void setOrders(final List<OrderDto> orders)
    {
        this.orders = orders;
    }

    public List<OrderDto> getOrders()
    {
        return orders;
    }
}
