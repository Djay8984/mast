package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.LinkResource;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CodicilCategoryDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -7700260036236544321L;

    @NotNull
    @Valid
    private LinkResource codicilTypeId;

    public final LinkResource getCodicilTypeId()
    {
        return codicilTypeId;
    }

    public final void setCodicilTypeId(LinkResource codicilTypeId)
    {
        this.codicilTypeId = codicilTypeId;
    }
}
