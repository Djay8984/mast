package com.baesystems.ai.lr.dto.references;

public class CreditingStatusDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -4407073638923834162L;

    private String code;

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }
}
