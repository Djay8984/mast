package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = MatchingNullPatternValidator.class)
@Documented
public @interface MatchingNullPattern
{
    String message()

    default "Null patterns of list members do not match for specified field(s)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The list of field names
     */
    String[] fields();

    /**
     * Annotation for enforcing that the given fields are mutually exclusive.
     */
    @Target({FIELD, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        MatchingNullPattern[] value();
    }
}
