package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.base.BaseDto;

public abstract class ItemBaseDto<T extends BaseDto> extends BaseDto
{
    private static final long serialVersionUID = 589442249749757715L;

    private Long assetVersionId;

    @Valid
    private List<LinkVersionedResource> related; // This will need changing at some stage.

    @Valid
    private List<AttributeDto> attributes;

    @NotNull
    private Boolean reviewed;

    @NotNull
    @Size(message = "invalid length", max = 200)
    private String name;

    @NotNull
    @Valid
    private LinkResource itemType;

    private LinkVersionedResource parentItem;

    private Integer displayOrder;

    private Long reviewedItems;

    private Boolean decommissioned;

    private List<T> items;

    public LinkResource getItemType()
    {
        return this.itemType;
    }

    public void setItemType(final LinkResource itemType)
    {
        this.itemType = itemType;
    }

    public List<LinkVersionedResource> getRelated()
    {
        return this.related;
    }

    public void setRelated(final List<LinkVersionedResource> related)
    {
        this.related = related;
    }

    public final List<AttributeDto> getAttributes()
    {
        return this.attributes;
    }

    public final void setAttributes(final List<AttributeDto> attributes)
    {
        this.attributes = attributes;
    }

    public Boolean getReviewed()
    {
        return this.reviewed;
    }

    public void setReviewed(final Boolean reviewed)
    {
        this.reviewed = reviewed;
    }

    public final String getName()
    {
        return this.name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public LinkVersionedResource getParentItem()
    {
        return this.parentItem;
    }

    public void setParentItem(final LinkVersionedResource parentItem)
    {
        this.parentItem = parentItem;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public Long getReviewedItems()
    {
        return this.reviewedItems;
    }

    public void setReviewedItems(final Long reviewedItems)
    {
        this.reviewedItems = reviewedItems;
    }

    public Boolean getDecommissioned()
    {
        return this.decommissioned;
    }

    public void setDecommissioned(final Boolean decommissioned)
    {
        this.decommissioned = decommissioned;
    }

    public Long getAssetVersionId()
    {
        return this.assetVersionId;
    }

    public void setAssetVersionId(final Long assetVersionId)
    {
        this.assetVersionId = assetVersionId;
    }

    public List<T> getItems()
    {
        return items;
    }

    public void setItems(final List<T> items)
    {
        this.items = items;
    }
}
