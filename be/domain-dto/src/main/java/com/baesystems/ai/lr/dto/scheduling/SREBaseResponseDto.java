package com.baesystems.ai.lr.dto.scheduling;

import com.baesystems.ai.lr.dto.base.RuleEngineDto;

public class SREBaseResponseDto implements RuleEngineDto
{
    private static final long serialVersionUID = 1900127264407216946L;

    private String status;
    private String message;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        builder.append("SREBaseResponseDto [getMessage()=");
        builder.append(getMessage());
        builder.append(", getStatus()=");
        builder.append(getStatus());
        builder.append("]");
        return builder.toString();
    }
}
