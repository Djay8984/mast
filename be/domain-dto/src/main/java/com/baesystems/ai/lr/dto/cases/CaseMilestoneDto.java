package com.baesystems.ai.lr.dto.cases;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class CaseMilestoneDto extends BaseDto implements DueStatusUpdateableDto, StaleObject
{
    private static final long serialVersionUID = -7313094930011439097L;

    private String stalenessHash;

    private Date completionDate;

    private Date dueDate;

    private LinkResource dueStatus;

    @NotNull
    private Boolean inScope;

    @NotNull
    @Valid
    private LinkResource milestoneStatus;

    @NotNull
    @Valid
    private LinkResource milestone;

    @NotNull
    @Valid
    private LinkResource aCase;

    public Date getCompletionDate()
    {
        return this.completionDate;
    }

    public void setCompletionDate(final Date completionDate)
    {
        this.completionDate = completionDate;
    }

    @Override
    public Date getDueDate()
    {
        return this.dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public LinkResource getDueStatus()
    {
        return this.dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public Boolean getInScope()
    {
        return this.inScope;
    }

    public void setInScope(final Boolean inScope)
    {
        this.inScope = inScope;
    }

    public LinkResource getMilestoneStatus()
    {
        return this.milestoneStatus;
    }

    public void setMilestoneStatus(final LinkResource milestoneStatus)
    {
        this.milestoneStatus = milestoneStatus;
    }

    public LinkResource getMilestone()
    {
        return this.milestone;
    }

    public void setMilestone(final LinkResource milestone)
    {
        this.milestone = milestone;
    }

    public LinkResource getaCase()
    {
        return this.aCase;
    }

    public void setaCase(final LinkResource caseId)
    {
        this.aCase = caseId;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "CaseMilestoneDto [completionDate=" + getStringRepresentationOfDate(this.completionDate)
                + ", dueDate=" + getStringRepresentationOfDate(this.dueDate)
                + ", inScope=" + this.inScope
                + ", milestoneStatus=" + getIdOrNull(this.milestoneStatus)
                + ", milestone=" + getIdOrNull(this.milestone)
                + ", aCase=" + getIdOrNull(this.aCase)
                + ", getId()=" + getId() + "]";
    }
}
