package com.baesystems.ai.lr.dto.defects;

import java.util.List;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class DefectPageResourceDto extends BasePageResource<DefectDto>
{
    private List<DefectDto> content;

    @Override
    public List<DefectDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<DefectDto> content)
    {
        this.content = content;
    }

}
