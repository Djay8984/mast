package com.baesystems.ai.lr.dto;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class LinkVersionedResource extends BaseDto
{
    private static final long serialVersionUID = 5169317290172980295L;

    private Long version;

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(final Long version)
    {
        this.version = version;
    }

    public LinkVersionedResource(final Long id, final Long version)
    {
        super();
        this.version = version;
        this.setId(id);
    }

    public LinkVersionedResource()
    {
        super();
    }
}
