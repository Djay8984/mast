package com.baesystems.ai.lr.dto.employees;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OfficeLinkDto extends BaseDto
{
    private static final long serialVersionUID = 4681535730575213262L;

    @NotNull
    private LinkResource office;

    @NotNull
    @Valid
    private LinkResource officeRole;

    public LinkResource getOffice()
    {
        return office;
    }

    public void setOffice(final LinkResource office)
    {
        this.office = office;
    }

    public LinkResource getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkResource officeRole)
    {
        this.officeRole = officeRole;
    }

}
