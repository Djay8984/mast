package com.baesystems.ai.lr.dto.employees;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyorDto extends BaseDto
{
    private static final long serialVersionUID = 345061382252024287L;

    @Size(message = "invalid length", max = 362)
    // This size is the sum of all names in lr_employee plus 2 spaces.
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

}
