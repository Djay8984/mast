package com.baesystems.ai.lr.dto.references;

public class CaseStatusDto extends ReferenceDataDto
{
    private static final long serialVersionUID = -233376735887388024L;

    private Boolean canSaveWithErrors;

    private Boolean synchroniseAsset;

    private Boolean open;

    public Boolean getCanSaveWithErrors()
    {
        return canSaveWithErrors;
    }

    public void setCanSaveWithErrors(final Boolean canSaveWithErrors)
    {
        this.canSaveWithErrors = canSaveWithErrors;
    }

    public Boolean getSynchroniseAsset()
    {
        return synchroniseAsset;
    }

    public void setSynchroniseAsset(final Boolean synchroniseAsset)
    {
        this.synchroniseAsset = synchroniseAsset;
    }

    public Boolean getOpen()
    {
        return open;
    }

    public void setOpen(Boolean open)
    {
        this.open = open;
    }
}
