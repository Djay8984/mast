package com.baesystems.ai.lr.dto.validation;

import java.util.ArrayList;
import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class ItemTemplateDto extends BaseDto
{
    private static final long serialVersionUID = -4232216876211393837L;

    private List<ItemRuleDto> validItems;

    private List<AttributeRuleDto> validAttributes;

    public List<ItemRuleDto> getValidItems()
    {
        if (this.validItems == null)
        {
            this.validItems = new ArrayList<ItemRuleDto>();
        }

        return this.validItems;
    }

    public void setValidItems(final List<ItemRuleDto> validItems)
    {
        this.validItems = validItems;
    }

    public List<AttributeRuleDto> getValidAttributes()
    {
        if (this.validAttributes == null)
        {
            this.validAttributes = new ArrayList<AttributeRuleDto>();
        }
        return this.validAttributes;
    }

    public void setValidAttributes(final List<AttributeRuleDto> validAttributes)
    {
        this.validAttributes = validAttributes;
    }

}
