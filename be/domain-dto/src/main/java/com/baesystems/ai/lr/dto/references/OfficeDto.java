package com.baesystems.ai.lr.dto.references;

import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class OfficeDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 5884419650465860029L;

    @Size(message = "invalid length", max = 60)
    private String name;

    @Size(message = "invalid length", max = 3)
    private String code;

    private LinkResource officeRole;

    @Size(message = "invalid length", max = 200)
    private String emailAddress;

    @Size(message = "invalid length", max = 50)
    private String phoneNumber;

    @Size(message = "invalid length", max = 200)
    private String addressLine1;

    @Size(message = "invalid length", max = 200)
    private String addressLine2;

    @Size(message = "invalid length", max = 200)
    private String addressLine3;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(final String code)
    {
        this.code = code;
    }

    public LinkResource getOfficeRole()
    {
        return officeRole;
    }

    public void setOfficeRole(final LinkResource officeRole)
    {
        this.officeRole = officeRole;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(final String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAddressLine1()
    {
        return addressLine1;
    }

    public void setAddressLine1(final String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2()
    {
        return addressLine2;
    }

    public void setAddressLine2(final String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3()
    {
        return addressLine3;
    }

    public void setAddressLine3(final String addressLine3)
    {
        this.addressLine3 = addressLine3;
    }

}
