package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class ReferenceDataVersionDto extends BaseDto
{
    private static final long serialVersionUID = -7977194839937596657L;

    private Integer version;

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(final Integer version)
    {
        this.version = version;
    }
}
