package com.baesystems.ai.lr.dto.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = MutuallyExclusiveValidator.class)
@Documented
public @interface MutuallyExclusive
{
    String message()

    default "fields are mutually exclusive";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The list of field names
     */
    String[] fields();

    /**
     * @return Whether or not to allow all of the fields to be null
     */
    boolean allowAllNull();

    /**
     * Annotation for enforcing that the given fields are mutually exclusive.
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List
    {
        MutuallyExclusive[] value();
    }
}
