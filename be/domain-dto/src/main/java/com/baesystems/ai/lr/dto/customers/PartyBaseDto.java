package com.baesystems.ai.lr.dto.customers;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class PartyBaseDto extends BaseDto
{
    private static final long serialVersionUID = 5772816887977390746L;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String name;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }
}
