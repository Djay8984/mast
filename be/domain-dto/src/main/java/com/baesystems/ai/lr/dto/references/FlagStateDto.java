package com.baesystems.ai.lr.dto.references;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class FlagStateDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 5041672297073754163L;

    @NotNull
    @Size(message = "invalid length", max = 50)
    private String name;

    private byte[] flag;

    private String flagCode;

    private List<LinkResource> ports;

    public final String getName()
    {
        return name;
    }

    public final void setName(final String name)
    {
        this.name = name;
    }

    public List<LinkResource> getPorts()
    {
        return ports;
    }

    public void setPorts(final List<LinkResource> ports)
    {
        this.ports = ports;
    }

    public byte[] getFlag()
    {
        return flag;
    }

    public void setFlag(final byte[] flag)
    {
        this.flag = flag;
    }

    public String getFlagCode()
    {
        return flagCode;
    }

    public void setFlagCode(final String flagCode)
    {
        this.flagCode = flagCode;
    }
}
