package com.baesystems.ai.lr.dto.jobs;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.LinkVersionedResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.annotations.FieldLessThanOrEqual;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.employees.EmployeeLinkDto;
import com.baesystems.ai.lr.dto.employees.OfficeLinkDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@FieldLessThanOrEqual.List({
                            @FieldLessThanOrEqual(first = "firstVisitDate", second = "lastVisitDate", useTime = false, message = "The last visit date cannot be before the first visit date")})
public class JobDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = 8289884782043777809L;

    private String stalenessHash;

    @Size(message = "invalid length", max = 2500)
    private String note;

    private Boolean requestByTelephoneIndicator;

    @NotNull
    private Boolean scopeConfirmed;

    private Date scopeConfirmedDate;

    @Valid
    private LinkResource scopeConfirmedBy;

    private Date requestedAttendanceDate;

    @Size(message = "invalid length", max = 500)
    private String statusReason;

    private Long workOrderNumber;

    private Date writtenServiceRequestReceivedDate;

    private Date writtenServiceResponseSentDate;

    @NotNull
    @Valid
    private LinkVersionedResource asset;

    @Valid
    private LinkResource aCase;

    @NotNull
    @Valid
    private List<OfficeLinkDto> offices;

    @Valid
    private List<EmployeeLinkDto> employees;

    @NotNull
    @Valid
    private LinkResource jobStatus;

    private Date createdOn;

    private Date etaDate;

    private Date etdDate;

    @Size(message = "invalid length", max = 50)
    private String location;

    @Size(message = "invalid length", max = 50)
    private String description;

    private Date firstVisitDate;

    private Date lastVisitDate;

    private Date completedOn;

    private String completedBy;

    private Boolean zeroVisitJob;

    @Valid
    private LinkResource proposedFlagState;

    private Date updatedOn;

    @Valid
    private LinkResource updatedOnBehalfOf;

    private Boolean classGroupJob;

    @NotNull
    @Valid
    private LinkResource jobCategory;

    @Valid
    private LinkResource jobTeam;

    public String getNote()
    {
        return this.note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public Boolean getRequestByTelephoneIndicator()
    {
        return this.requestByTelephoneIndicator;
    }

    public void setRequestByTelephoneIndicator(final Boolean requestByTelephoneIndicator)
    {
        this.requestByTelephoneIndicator = requestByTelephoneIndicator;
    }

    public Date getRequestedAttendanceDate()
    {
        return this.requestedAttendanceDate;
    }

    public void setRequestedAttendanceDate(final Date requestedAttendanceDate)
    {
        this.requestedAttendanceDate = requestedAttendanceDate;
    }

    public String getStatusReason()
    {
        return this.statusReason;
    }

    public void setStatusReason(final String statusReason)
    {
        this.statusReason = statusReason;
    }

    public Long getWorkOrderNumber()
    {
        return this.workOrderNumber;
    }

    public void setWorkOrderNumber(final Long workOrderNumber)
    {
        this.workOrderNumber = workOrderNumber;
    }

    public Date getWrittenServiceRequestReceivedDate()
    {
        return this.writtenServiceRequestReceivedDate;
    }

    public void setWrittenServiceRequestReceivedDate(final Date writtenServiceRequestReceivedDate)
    {
        this.writtenServiceRequestReceivedDate = writtenServiceRequestReceivedDate;
    }

    public Date getWrittenServiceResponseSentDate()
    {
        return this.writtenServiceResponseSentDate;
    }

    public void setWrittenServiceResponseSentDate(final Date writtenServiceResponseSentDate)
    {
        this.writtenServiceResponseSentDate = writtenServiceResponseSentDate;
    }

    public LinkVersionedResource getAsset()
    {
        return this.asset;
    }

    public void setAsset(final LinkVersionedResource asset)
    {
        this.asset = asset;
    }

    public List<OfficeLinkDto> getOffices()
    {
        return this.offices;
    }

    public void setOffices(final List<OfficeLinkDto> offices)
    {
        this.offices = offices;
    }

    public List<EmployeeLinkDto> getEmployees()
    {
        return this.employees;
    }

    public void setEmployees(final List<EmployeeLinkDto> employees)
    {
        this.employees = employees;
    }

    public LinkResource getJobStatus()
    {
        return this.jobStatus;
    }

    public void setJobStatus(final LinkResource jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public Date getCreatedOn()
    {
        return this.createdOn;
    }

    public void setCreatedOn(final Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public Date getEtaDate()
    {
        return this.etaDate;
    }

    public void setEtaDate(final Date etaDate)
    {
        this.etaDate = etaDate;
    }

    public Date getEtdDate()
    {
        return this.etdDate;
    }

    public void setEtdDate(final Date etdDate)
    {
        this.etdDate = etdDate;
    }

    public LinkResource getaCase()
    {
        return this.aCase;
    }

    public void setaCase(final LinkResource aCase)
    {
        this.aCase = aCase;
    }

    public String getLocation()
    {
        return this.location;
    }

    public void setLocation(final String location)
    {
        this.location = location;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Boolean getScopeConfirmed()
    {
        return this.scopeConfirmed;
    }

    public void setScopeConfirmed(final Boolean scopeConfirmed)
    {
        this.scopeConfirmed = scopeConfirmed;
    }

    public Date getScopeConfirmedDate()
    {
        return this.scopeConfirmedDate;
    }

    public void setScopeConfirmedDate(final Date scopeConfirmedDate)
    {
        this.scopeConfirmedDate = scopeConfirmedDate;
    }

    public LinkResource getScopeConfirmedBy()
    {
        return this.scopeConfirmedBy;
    }

    public void setScopeConfirmedBy(final LinkResource scopeConfirmedBy)
    {
        this.scopeConfirmedBy = scopeConfirmedBy;
    }

    public Date getFirstVisitDate()
    {
        return this.firstVisitDate;
    }

    public void setFirstVisitDate(final Date firstVisitDate)
    {
        this.firstVisitDate = firstVisitDate;
    }

    public Date getLastVisitDate()
    {
        return this.lastVisitDate;
    }

    public void setLastVisitDate(final Date lastVisitDate)
    {
        this.lastVisitDate = lastVisitDate;
    }

    public Date getCompletedOn()
    {
        return this.completedOn;
    }

    public void setCompletedOn(final Date completedOn)
    {
        this.completedOn = completedOn;
    }

    public String getCompletedBy()
    {
        return this.completedBy;
    }

    public void setCompletedBy(final String completedBy)
    {
        this.completedBy = completedBy;
    }

    public Boolean getZeroVisitJob()
    {
        return zeroVisitJob;
    }

    public void setZeroVisitJob(final Boolean zeroVisitJob)
    {
        this.zeroVisitJob = zeroVisitJob;
    }

    public LinkResource getProposedFlagState()
    {
        return proposedFlagState;
    }

    public void setProposedFlagState(final LinkResource proposedFlagState)
    {
        this.proposedFlagState = proposedFlagState;
    }

    public Date getUpdatedOn()
    {
        return this.updatedOn;
    }

    public void setUpdatedOn(final Date updatedOn)
    {
        this.updatedOn = updatedOn;
    }

    public LinkResource getUpdatedOnBehalfOf()
    {
        return this.updatedOnBehalfOf;
    }

    public void setUpdatedOnBehalfOf(final LinkResource updatedOnBehalfOf)
    {
        this.updatedOnBehalfOf = updatedOnBehalfOf;
    }

    public Boolean getClassGroupJob()
    {
        return classGroupJob;
    }

    public void setClassGroupJob(final Boolean classGroupJob)
    {
        this.classGroupJob = classGroupJob;
    }

    public LinkResource getJobCategory()
    {
        return this.jobCategory;
    }

    public void setJobCategory(final LinkResource jobCategory)
    {
        this.jobCategory = jobCategory;
    }

    public LinkResource getJobTeam()
    {
        return this.jobTeam;
    }

    public void setJobTeam(final LinkResource jobTeam)
    {
        this.jobTeam = jobTeam;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "JobDto [note=" + this.note
                + ", requestByTelephoneIndicator=" + this.requestByTelephoneIndicator
                + ", scopeConfirmed=" + this.scopeConfirmed
                + ", scopeConfirmedDate=" + getStringRepresentationOfDate(this.scopeConfirmedDate)
                + ", scopeConfirmedBy=" + getIdOrNull(this.scopeConfirmedBy)
                + ", requestedAttendanceDate=" + getStringRepresentationOfDate(this.requestedAttendanceDate)
                + ", statusReason=" + this.statusReason
                + ", workOrderNumber=" + this.workOrderNumber
                + ", writtenServiceRequestReceivedDate=" + getStringRepresentationOfDate(this.writtenServiceRequestReceivedDate)
                + ", writtenServiceResponseSentDate=" + getStringRepresentationOfDate(this.writtenServiceResponseSentDate)
                + ", asset=" + getIdOrNull(this.asset)
                + ", aCase=" + getIdOrNull(this.aCase)
                + ", offices=" + getStringRepresentationOfList(this.offices)
                + ", employees=" + getStringRepresentationOfList(this.employees)
                + ", jobStatus=" + getIdOrNull(this.jobStatus)
                + ", createdOn=" + getStringRepresentationOfDate(this.createdOn)
                + ", etaDate=" + getStringRepresentationOfDate(this.etaDate)
                + ", etdDate=" + getStringRepresentationOfDate(this.etdDate)
                + ", location=" + this.location
                + ", description=" + this.description
                + ", firstVisitDate=" + getStringRepresentationOfDate(this.firstVisitDate)
                + ", lastVisitDate=" + getStringRepresentationOfDate(this.lastVisitDate)
                + ", completedOn=" + getStringRepresentationOfDate(this.completedOn)
                + ", completedBy=" + this.completedBy
                + ", zeroVisit=" + this.zeroVisitJob
                + ", proposedFlag=" + getIdOrNull(this.proposedFlagState)
                + ", updatedOn=" + getStringRepresentationOfDate(this.updatedOn)
                + ", updatedOnBehalfOf=" + getIdOrNull(this.updatedOnBehalfOf)
                + ", classGroupJob=" + this.classGroupJob
                + ", jobCategory=" + getIdOrNull(this.jobCategory)
                + ", jobTeam=" + getIdOrNull(this.jobTeam)
                + ", getId()=" + getId() + "]";
    }
}
