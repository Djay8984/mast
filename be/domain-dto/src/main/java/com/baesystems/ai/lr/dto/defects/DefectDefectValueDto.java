package com.baesystems.ai.lr.dto.defects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.annotations.SubIdNotNull;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.DefectValueDto;

public class DefectDefectValueDto extends BaseDto
{
    private static final long serialVersionUID = 5757240290554132918L;

    @NotNull
    @SubIdNotNull
    @Valid
    private DefectValueDto defectValue;

    @Size(message = "invalid length", max = 50)
    private String otherDetails;

    @Valid
    private LinkResource parent;

    public DefectValueDto getDefectValue()
    {
        return defectValue;
    }

    public void setDefectValue(final DefectValueDto defectValue)
    {
        this.defectValue = defectValue;
    }

    public String getOtherDetails()
    {
        return otherDetails;
    }

    public void setOtherDetails(final String otherDetails)
    {
        this.otherDetails = otherDetails;
    }

    public LinkResource getParent()
    {
        return parent;
    }

    public void setParent(final LinkResource parent)
    {
        this.parent = parent;
    }
}
