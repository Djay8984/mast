package com.baesystems.ai.lr.dto.references;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.services.ProductDto;

public class ProductListDto implements Serializable
{
    private static final long serialVersionUID = 6583972251905124705L;

    @NotNull
    private List<ProductDto> productList;

    public List<ProductDto> getProductList()
    {
        return this.productList;
    }

    public void setProductList(final List<ProductDto> productList)
    {
        this.productList = productList;
    }


}
