package com.baesystems.ai.lr.dto.attachments;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.baesystems.ai.lr.dto.references.LrEmployeeDto;

public class SupplementaryInformationDto extends BaseDto
{
    private static final long serialVersionUID = -3219159044886874926L;

    @Size(message = "invalid length", max = 50)
    private String title;

    @Size(message = "invalid length", max = 2500)
    private String note;

    @Size(message = "invalid length", max = 500)
    private String attachmentUrl;

    @Size(message = "invalid length", max = 100)
    private String assetLocationViewpoint;

    @Valid
    private LinkResource attachmentType;

    @Valid
    private LinkResource confidentialityType;

    @Valid
    private LinkResource location;

    @Valid
    private LrEmployeeDto author;

    private Date creationDate;

    private String updatedBy;

    private Date updatedDate;

    private AttachmentEntityLink entityLink;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getNote()
    {
        return note;
    }

    public void setNote(final String note)
    {
        this.note = note;
    }

    public String getAttachmentUrl()
    {
        return attachmentUrl;
    }

    public void setAttachmentUrl(final String attachmentUrl)
    {
        this.attachmentUrl = attachmentUrl;
    }

    public String getAssetLocationViewpoint()
    {
        return assetLocationViewpoint;
    }

    public void setAssetLocationViewpoint(final String assetLocationViewpoint)
    {
        this.assetLocationViewpoint = assetLocationViewpoint;
    }

    public LinkResource getAttachmentType()
    {
        return attachmentType;
    }

    public void setAttachmentType(final LinkResource attachmentType)
    {
        this.attachmentType = attachmentType;
    }

    public LinkResource getConfidentialityType()
    {
        return confidentialityType;
    }

    public void setConfidentialityType(final LinkResource confidentialityType)
    {
        this.confidentialityType = confidentialityType;
    }

    public LinkResource getLocation()
    {
        return location;
    }

    public void setLocation(final LinkResource location)
    {
        this.location = location;
    }

    public LrEmployeeDto getAuthor()
    {
        return author;
    }

    public void setAuthor(final LrEmployeeDto author)
    {
        this.author = author;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(final Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(final String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public AttachmentEntityLink getEntityLink()
    {
        return entityLink;
    }

    public void setEntityLink(final AttachmentEntityLink entityLink)
    {
        this.entityLink = entityLink;
    }

    public static class AttachmentEntityLink
    {
        private LinkResource link;
        private String type;

        public LinkResource getLink()
        {
            return link;
        }

        public void setLink(final LinkResource link)
        {
            this.link = link;
        }

        public String getType()
        {
            return type;
        }

        public void setType(final String type)
        {
            this.type = type;
        }
    }
}
