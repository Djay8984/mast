package com.baesystems.ai.lr.dto.references;

import com.baesystems.ai.lr.dto.base.BaseReferenceDataDto;

public class AllowedWorkItemAttributeValueDto extends BaseReferenceDataDto
{
    private static final long serialVersionUID = 4951251818871426915L;

    private String value;

    private Integer displayOrder;

    private Boolean isFollowOnTrigger;

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(final String value)
    {
        this.value = value;
    }

    public Boolean getIsFollowOnTrigger()
    {
        return isFollowOnTrigger;
    }

    public void setIsFollowOnTrigger(final Boolean isFollowOnTrigger)
    {
        this.isFollowOnTrigger = isFollowOnTrigger;
    }
}
