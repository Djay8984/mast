package com.baesystems.ai.lr.dto.customers;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class PartyContactDto extends BaseDto
{
    private static final long serialVersionUID = -2317933704836084489L;

    @NotNull
    @Size(message = "invalid length", max = 5)
    private String title;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String firstName;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String lastName;

    private String middleName;

    private String phoneNumber;

    private Integer extension;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

    public String getMiddleName()
    {
        return middleName;
    }

    public void setMiddleName(final String middleName)
    {
        this.middleName = middleName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public Integer getExtension()
    {
        return extension;
    }

    public void setExtension(final Integer extension)
    {
        this.extension = extension;
    }
}
