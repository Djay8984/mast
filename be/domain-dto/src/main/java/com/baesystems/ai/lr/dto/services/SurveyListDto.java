package com.baesystems.ai.lr.dto.services;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class SurveyListDto implements Dto, Serializable
{
    private static final long serialVersionUID = 3108037396746737893L;

    @NotNull
    @Valid
    private List<SurveyDto> surveys;

    public List<SurveyDto> getSurveys()
    {
        return surveys;
    }

    public void setSurveys(final List<SurveyDto> surveys)
    {
        this.surveys = surveys;
    }
}
