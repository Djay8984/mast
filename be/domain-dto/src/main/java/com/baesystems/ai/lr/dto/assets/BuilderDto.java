package com.baesystems.ai.lr.dto.assets;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class BuilderDto extends BaseDto
{
    private static final long serialVersionUID = -3918900973540770393L;

    @NotNull
    @Size(message = "invalid length", max = 100)
    private String name;

    @NotNull
    private Boolean approvalStatus;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Boolean getApprovalStatus()
    {
        return approvalStatus;
    }

    public void setApprovalStatus(final Boolean approvalStatus)
    {
        this.approvalStatus = approvalStatus;
    }
}
