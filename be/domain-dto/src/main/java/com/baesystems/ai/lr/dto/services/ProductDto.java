package com.baesystems.ai.lr.dto.services;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.StaleObject;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ProductDto extends BaseDto implements StaleObject
{
    private static final long serialVersionUID = -3562734472471817574L;

    private String stalenessHash;

    @NotNull
    private Long productCatalogueId;

    @NotNull
    @Size(message = "invalid length", max = 45)
    private String name;

    @Size(message = "invalid length", max = 45)
    private String description;

    @NotNull
    private Integer displayOrder;

    @NotNull
    @Valid
    private LinkResource productGroup;

    @NotNull
    @Valid
    private LinkResource productType;

    @Valid
    private LinkResource schedulingRegime;

    public Long getProductCatalogueId()
    {
        return this.productCatalogueId;
    }

    public void setProductCatalogueId(final Long productCatalogueId)
    {
        this.productCatalogueId = productCatalogueId;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Integer getDisplayOrder()
    {
        return this.displayOrder;
    }

    public void setDisplayOrder(final Integer displayOrder)
    {
        this.displayOrder = displayOrder;
    }

    public LinkResource getProductGroup()
    {
        return this.productGroup;
    }

    public void setProductGroup(final LinkResource productGroup)
    {
        this.productGroup = productGroup;
    }

    public LinkResource getProductType()
    {
        return this.productType;
    }

    public void setProductType(final LinkResource productType)
    {
        this.productType = productType;
    }

    public LinkResource getSchedulingRegime()
    {
        return this.schedulingRegime;
    }

    public void setSchedulingRegime(final LinkResource schedulingRegime)
    {
        this.schedulingRegime = schedulingRegime;
    }

    @Override
    public String getStalenessHash()
    {
        return this.stalenessHash;
    }

    @Override
    public void setStalenessHash(final String newStalenessHash)
    {
        this.stalenessHash = newStalenessHash;
    }

    @Override
    @JsonIgnore
    public String getStringRepresentationForHash()
    {
        return "ProductDto ["
                + "schedulingRegime=" + getIdOrNull(this.schedulingRegime)
                + ", getId()=" + getId() + "]";
    }
}
