package com.baesystems.ai.lr.dto.query;

import java.util.Date;
import java.util.List;

import com.baesystems.ai.lr.dto.annotations.UsesLikeComparator;

/**
 * Common dto class used for querying Codicil type entities (Asset Notes, Actionable Items, CoCs) and also Defects.
 */
public class CodicilDefectQueryDto implements QueryDto
{
    private static final long serialVersionUID = 7457519316161753246L;

    @UsesLikeComparator
    private String searchString;

    private List<Long> categoryList;

    private List<Long> statusList;

    private List<Long> itemTypeList;

    private List<Long> confidentialityList;

    private Date dueDateMin;

    private Date dueDateMax;

    public String getSearchString()
    {
        return searchString;
    }

    public void setSearchString(final String searchString)
    {
        this.searchString = searchString;
    }

    public List<Long> getCategoryList()
    {
        return categoryList;
    }

    public void setCategoryList(final List<Long> categoryList)
    {
        this.categoryList = categoryList;
    }

    public List<Long> getStatusList()
    {
        return statusList;
    }

    public void setStatusList(final List<Long> statusList)
    {
        this.statusList = statusList;
    }

    public List<Long> getItemTypeList()
    {
        return itemTypeList;
    }

    public void setItemTypeList(final List<Long> itemTypeList)
    {
        this.itemTypeList = itemTypeList;
    }

    public List<Long> getConfidentialityList()
    {
        return confidentialityList;
    }

    public void setConfidentialityList(final List<Long> confidentialityList)
    {
        this.confidentialityList = confidentialityList;
    }

    public Date getDueDateMin()
    {
        return dueDateMin;
    }

    public void setDueDateMin(final Date dueDateMin)
    {
        this.dueDateMin = dueDateMin;
    }

    public Date getDueDateMax()
    {
        return dueDateMax;
    }

    public void setDueDateMax(final Date dueDateMax)
    {
        this.dueDateMax = dueDateMax;
    }
}
