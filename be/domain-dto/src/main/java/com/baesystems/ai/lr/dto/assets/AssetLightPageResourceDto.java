package com.baesystems.ai.lr.dto.assets;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class AssetLightPageResourceDto extends BasePageResource<AssetLightDto>
{
    @Valid
    private List<AssetLightDto> content;

    @Override
    public List<AssetLightDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<AssetLightDto> content)
    {
        this.content = content;
    }

}
