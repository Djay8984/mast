package com.baesystems.ai.lr.dto.tasks;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.assets.ItemLightDto;
import com.baesystems.ai.lr.dto.services.ScheduledServiceDto;
import com.baesystems.ai.lr.dto.services.SurveyDto;

public class WorkItemDto extends BaseWorkItemDto
{
    private static final long serialVersionUID = 7031502991694251802L;

    @Valid
    @NotNull
    private ItemLightDto assetItem;

    @Valid
    private SurveyDto survey;

    @Valid
    private ScheduledServiceDto scheduledService;

    @Valid
    private WorkItemDto conditionalParent;

    @Valid
    private WorkItemDto workItem;

    public ItemLightDto getAssetItem()
    {
        return assetItem;
    }

    public void setAssetItem(final ItemLightDto assetItem)
    {
        this.assetItem = assetItem;
    }

    public SurveyDto getSurvey()
    {
        return survey;
    }

    public void setSurvey(final SurveyDto survey)
    {
        this.survey = survey;
    }

    public ScheduledServiceDto getScheduledService()
    {
        return scheduledService;
    }

    public void setScheduledService(final ScheduledServiceDto scheduledService)
    {
        this.scheduledService = scheduledService;
    }

    public WorkItemDto getConditionalParent()
    {
        return conditionalParent;
    }

    public void setConditionalParent(final WorkItemDto conditionalParent)
    {
        this.conditionalParent = conditionalParent;
    }

    public WorkItemDto getWorkItem()
    {
        return workItem;
    }

    public void setWorkItem(final WorkItemDto workItem)
    {
        this.workItem = workItem;
    }
}
