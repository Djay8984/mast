package com.baesystems.ai.lr.dto.codicils;

import java.util.List;

import javax.validation.Valid;

import com.baesystems.ai.lr.dto.paging.BasePageResource;

public class CoCPageResourceDto extends BasePageResource<CoCDto>
{
    @Valid
    private List<CoCDto> content;

    @Override
    public List<CoCDto> getContent()
    {
        return content;
    }

    @Override
    public void setContent(final List<CoCDto> content)
    {
        this.content = content;
    }

}
