package com.baesystems.ai.lr.dto.paging;

public class OrderDto
{
    private String direction;

    private String property;

    private boolean ignoreCase;

    public String getDirection()
    {
        return direction;
    }

    public void setDirection(final String direction)
    {
        this.direction = direction;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(final String property)
    {
        this.property = property;
    }

    public Boolean getIgnoreCase()
    {
        return ignoreCase;
    }

    public void setIgnoreCase(final Boolean ignoreCase)
    {
        this.ignoreCase = ignoreCase;
    }
}
