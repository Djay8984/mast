package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsHideDto extends BaseDto
{

    private static final long serialVersionUID = 8147130127828545630L;
    
    private Double depthMoulded;

    public Double getDepthMoulded()
    {
        return depthMoulded;
    }

    public void setDepthMoulded(final Double depthMoulded)
    {
        this.depthMoulded = depthMoulded;
    }
}
