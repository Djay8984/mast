package com.baesystems.ai.lr.dto.tasks;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.base.Dto;

public class WorkItemLightListDto implements Dto, Serializable
{
    private static final long serialVersionUID = 154914170398655947L;

    @NotNull
    @Valid
    private List<WorkItemLightDto> tasks;

    public List<WorkItemLightDto> getTasks()
    {
        return tasks;
    }

    public void setTasks(final List<WorkItemLightDto> tasks)
    {
        this.tasks = tasks;
    }
}
