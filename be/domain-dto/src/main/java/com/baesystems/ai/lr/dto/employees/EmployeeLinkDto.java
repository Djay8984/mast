package com.baesystems.ai.lr.dto.employees;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeLinkDto extends BaseDto
{
    private static final long serialVersionUID = -1222659624509355114L;

    @NotNull
    @Valid
    private LinkResource lrEmployee;

    @NotNull
    @Valid
    private LinkResource employeeRole;

    public LinkResource getLrEmployee()
    {
        return lrEmployee;
    }

    public void setLrEmployee(final LinkResource lrEmployee)
    {
        this.lrEmployee = lrEmployee;
    }

    public LinkResource getEmployeeRole()
    {
        return employeeRole;
    }

    public void setEmployeeRole(final LinkResource employeeRole)
    {
        this.employeeRole = employeeRole;
    }
}
