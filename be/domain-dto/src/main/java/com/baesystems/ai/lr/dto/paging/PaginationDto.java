package com.baesystems.ai.lr.dto.paging;

import java.io.Serializable;
import java.util.List;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class PaginationDto extends BaseDto implements Serializable
{
    private static final long serialVersionUID = 3599923686383646269L;

    private Integer size;

    private Integer number;

    private Boolean first;

    private Boolean last;

    private Integer totalPages;

    private Long totalElements;

    private Integer numberOfElements;

    private SortDto sort;

    private List<PageLinkDto> link;

    public List<PageLinkDto> getLink()
    {
        return link;
    }

    public void setLink(final List<PageLinkDto> link)
    {
        this.link = link;
    }

    public Integer getSize()
    {
        return size;
    }

    public void setSize(final Integer size)
    {
        this.size = size;
    }

    public Integer getNumber()
    {
        return number;
    }

    public void setNumber(final Integer number)
    {
        this.number = number;
    }

    public Boolean getFirst()
    {
        return first;
    }

    public void setFirst(final Boolean first)
    {
        this.first = first;
    }

    public Boolean getLast()
    {
        return last;
    }

    public void setLast(final Boolean last)
    {
        this.last = last;
    }

    public Integer getTotalPages()
    {
        return totalPages;
    }

    public void setTotalPages(final Integer totalPages)
    {
        this.totalPages = totalPages;
    }

    public Long getTotalElements()
    {
        return totalElements;
    }

    public void setTotalElements(final Long totalElements)
    {
        this.totalElements = totalElements;
    }

    public Integer getNumberOfElements()
    {
        return numberOfElements;
    }

    public void setNumberOfElements(final Integer numberOfElements)
    {
        this.numberOfElements = numberOfElements;
    }

    public SortDto getSort()
    {
        return sort;
    }

    public void setSort(final SortDto sort)
    {
        this.sort = sort;
    }
}
