package com.baesystems.ai.lr.dto.base;


public class DeletableDto extends BaseDto
{
    private static final long serialVersionUID = -7382431817051371773L;

    private boolean deleted;

    public boolean isDeleted()
    {
        return deleted;
    }

    public void setDeleted(final boolean deleted)
    {
        this.deleted = deleted;
    }
}
