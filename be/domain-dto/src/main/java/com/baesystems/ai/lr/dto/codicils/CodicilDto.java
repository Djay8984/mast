package com.baesystems.ai.lr.dto.codicils;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.baesystems.ai.lr.dto.LinkResource;
import com.baesystems.ai.lr.dto.base.DueStatusUpdateableDto;
import com.baesystems.ai.lr.dto.base.ReportContentElementDto;

public class CodicilDto extends ReportContentElementDto implements DueStatusUpdateableDto
{
    private static final long serialVersionUID = 34258180855215370L;

    @NotNull
    private Date imposedDate;

    private Date dueDate;

    @Valid
    private LinkResource dueStatus;

    @NotNull
    @Valid
    private LinkResource status;

    public Date getImposedDate()
    {
        return imposedDate;
    }

    public void setImposedDate(final Date imposedDate)
    {
        this.imposedDate = imposedDate;
    }

    @Override
    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(final Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public LinkResource getDueStatus()
    {
        return dueStatus;
    }

    @Override
    public void setDueStatus(final LinkResource dueStatus)
    {
        this.dueStatus = dueStatus;
    }

    public LinkResource getStatus()
    {
        return status;
    }

    public void setStatus(final LinkResource status)
    {
        this.status = status;
    }
}
