package com.baesystems.ai.lr.dto.ihs;

import com.baesystems.ai.lr.dto.base.BaseDto;

public class IhsEquipmentDetailsDto  extends BaseDto
{

    private static final long serialVersionUID = 4957854355803557867L;

    private IhsEquipment2Dto equipment2Dto;
    
    private IhsEquipment4Dto equipment4Dto;
    
    public IhsEquipment2Dto getEquipment2Dto()
    {
        return equipment2Dto;
    }
    public void setEquipment2Dto(final IhsEquipment2Dto equipment2Dto)
    {
        this.equipment2Dto = equipment2Dto;
    }
    public IhsEquipment4Dto getEquipment4Dto()
    {
        return equipment4Dto;
    }
    public void setEquipment4Dto(final IhsEquipment4Dto equipment4Dto)
    {
        this.equipment4Dto = equipment4Dto;
    }
}
