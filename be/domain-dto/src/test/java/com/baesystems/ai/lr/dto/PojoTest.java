package com.baesystems.ai.lr.dto;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.PojoField;
import com.openpojo.reflection.PojoMethod;
import com.openpojo.reflection.filters.FilterChain;
import com.openpojo.reflection.filters.FilterClassName;
import com.openpojo.reflection.filters.FilterSyntheticClasses;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.reflection.impl.PojoClassImpl;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsRule;
import com.openpojo.validation.rule.impl.NoStaticExceptFinalRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.DefaultValuesNullTester;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

public class PojoTest
{

    // The package to test
    private static final String POJO_PACKAGE = "com.baesystems.ai.lr";

    @Test
    public void testPojoStructureAndBehavior()
    {
        final Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                // See com.openpojo.validation.rule.impl for more ...
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                // Add Testers to validate behaviour for POJO_PACKAGE
                // See com.openpojo.validation.test.impl for more ...
                .with(new NoPublicFieldsRule())
                .with(new NoStaticExceptFinalRule())
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())

                // Create Testers to validate behaviour for POJO_PACKAGE
                .with(new DefaultValuesNullTester())
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        final PojoClassFilter pojoClassFilter = new FilterChain(new FilterSyntheticClasses(), new FilterClassName(".*Test\\.java$"));
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClassesRecursively(POJO_PACKAGE, pojoClassFilter);
        pojoClasses = filterClasses(pojoClasses);
        validator.validate(pojoClasses);
    }

    private List<PojoClass> filterClasses(List<PojoClass> pojoClasses)
    {
        final List<PojoClass> results = new ArrayList<PojoClass>();
        for (final PojoClass clazz : pojoClasses)
        {
            if (clazz.isEnum())
            {
                continue;
            }
            final List<PojoField> fields = new ArrayList<PojoField>(0);
            final List<PojoMethod> methods = new ArrayList<PojoMethod>(0);

            for (final PojoField field : clazz.getPojoFields())
            {
                if (!field.getName().contains("internalId"))
                {
                    fields.add(field);
                }
            }

            for (final PojoMethod method : clazz.getPojoMethods())
            {
                if (!method.getName().contains("internalId"))
                {
                    methods.add(method);
                }
            }

            results.add(new PojoClassImpl(clazz.getClazz(), fields, methods));
        }
        return results;
    }
}
