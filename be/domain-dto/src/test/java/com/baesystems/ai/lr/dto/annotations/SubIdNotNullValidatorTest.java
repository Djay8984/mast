package com.baesystems.ai.lr.dto.annotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.validation.ConstraintValidatorContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.baesystems.ai.lr.dto.annotations.SubIdNotNullValidator;
import com.baesystems.ai.lr.dto.base.BaseDto;

@RunWith(MockitoJUnitRunner.class)
public class SubIdNotNullValidatorTest
{

    private BaseDto baseDto;

    private static final Long VALID_ID = 1L;
    private static final Long ZERO_ID = 0L;
    private static final Long NEGATIVE_ID = -1L;

    @Mock
    private ConstraintValidatorContext constraintContext;

    @InjectMocks
    private final SubIdNotNullValidator subIdNotNullValidator = new SubIdNotNullValidator();

    @Before
    public void setUp()
    {
        baseDto = new BaseDto();
    }

    /**
     * Test with valid id. Should pass validation.
     */
    @Test
    public void isValidValidId()
    {
        baseDto.setId(VALID_ID);

        assertTrue(subIdNotNullValidator.isValid(baseDto, constraintContext));
    }

    /**
     * Test with invalid id. Should fail validation.
     */
    @Test
    public void isValidNegativeId()
    {
        baseDto.setId(NEGATIVE_ID);

        assertFalse(subIdNotNullValidator.isValid(baseDto, constraintContext));
    }

    /**
     * Test with invalid id. Should fail validation.
     */
    @Test
    public void isValidZeroId()
    {
        baseDto.setId(ZERO_ID);

        assertFalse(subIdNotNullValidator.isValid(baseDto, constraintContext));
    }

    /**
     * Test with null id. Should fail validation.
     */
    @Test
    public void isValidNullId()
    {
        baseDto.setId(null);

        assertFalse(subIdNotNullValidator.isValid(baseDto, constraintContext));
    }

    /**
     * Test with null object. Should pass validation here because the error should be generated elsewhere and there is
     * no need to repeat it.
     */
    @Test
    public void isValidNullObject()
    {
        assertTrue(subIdNotNullValidator.isValid(null, constraintContext));
    }
}
