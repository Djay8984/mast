package com.baesystems.ai.lr.mock.drools;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The most primitive mock ever.
 */
public class MockDroolsServlet extends HttpServlet
{
    private static final long serialVersionUID = 2214615644593154036L;

    private static final String SYSTEM_PROPERTY_MOCK_DROOLS_RETURNS_EMPTY = "drools.returns.empty";
    private static final String SYSTEM_PROPERTY_MOCK_DROOLS_RETURNS_EMPTY_SWITCHED_ON = "true";
    private static final String MOCK_DROOLS_SUB_DIR = SYSTEM_PROPERTY_MOCK_DROOLS_RETURNS_EMPTY_SWITCHED_ON.equals(System
            .getProperty(SYSTEM_PROPERTY_MOCK_DROOLS_RETURNS_EMPTY)) ? "empty/" : "";
    private static final String MOCK_DROOLS_DIR = "/mock-drools/";
    private static final String MOCK_DROOLS_REDIRECT_CREDITED_RESPONSE = MOCK_DROOLS_DIR + "CreditedResponse-001.json";
    private static final String MOCK_DROOLS_REDIRECT_UNCREDITED_RESPONSE = MOCK_DROOLS_DIR + "UncreditedResponse-002.json";
    private static final String MOCK_DROOLS_REDIRECT_TASKS_FOR_SERVICE_SUCCESS_RESPONSE = MOCK_DROOLS_DIR + MOCK_DROOLS_SUB_DIR
            + "TasksForServiceSuccessResponse-001.json";
    private static final String MOCK_DROOLS_REDIRECT_TASKS_FOR_ASSET_SUCCESS_RESPONSE = MOCK_DROOLS_DIR + MOCK_DROOLS_SUB_DIR
            + "TasksForAssetSuccessResponse-001.json";

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException
    {
        if (req.getRequestURI().contains("nextServices"))
        {
            resp.sendRedirect(MOCK_DROOLS_REDIRECT_CREDITED_RESPONSE);
        }
        else if (req.getRequestURI().contains("manualEntryService"))
        {
            resp.sendRedirect(MOCK_DROOLS_REDIRECT_UNCREDITED_RESPONSE);
        }
        else if (req.getRequestURI().contains("tasksForAssetService"))
        {
            resp.sendRedirect(MOCK_DROOLS_REDIRECT_TASKS_FOR_SERVICE_SUCCESS_RESPONSE);
        }
        else if (req.getRequestURI().contains("tasksForAsset"))
        {
            resp.sendRedirect(MOCK_DROOLS_REDIRECT_TASKS_FOR_ASSET_SUCCESS_RESPONSE);
        }
    }
}
