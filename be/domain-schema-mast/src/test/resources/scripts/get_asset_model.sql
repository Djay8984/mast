-- select i1.id, i2.id
select v.description as value_type,
a.value as attribute_value,
a.name as attribute_name,
i9.name as item,
i8.name as parent,
i7.name as parent_2,
i6.name as parent_3,
i5.name as parent_4,
i4.name as parent_5,
i3.name as parent_6,
i2.name as parent_7,
i1.name as parent_8
from mast_asset_item i1
right join mast_asset_item i2
on i1.id = i2.parent_item_id
right join mast_asset_item i3
on i2.id = i3.parent_item_id
right join mast_asset_item i4
on i3.id = i4.parent_item_id
right join mast_asset_item i5
on i4.id = i5.parent_item_id
right join mast_asset_item i6
on i5.id = i6.parent_item_id
right join mast_asset_item i7
on i6.id = i7.parent_item_id
right join mast_asset_item i8
on i7.id = i8.parent_item_id
right join mast_asset_item i9
on i8.id = i9.parent_item_id
right join mast_asset_attribute a
on i9.id = a.item_id
join mast_ref_attribute_type t
on a.attribute_type_id = t.id
join mast_ref_allowed_attribute_value v
on v.id = t.value_type_id
where i1.asset_id = 5
or i2.asset_id = 5
or i3.asset_id = 5
or i4.asset_id = 5
or i5.asset_id = 5
or i6.asset_id = 5
or i7.asset_id = 5
or i8.asset_id = 5
or i9.asset_id = 5
order by parent_8, parent_7, parent_6, parent_5, parent_4, parent_3, parent_2, parent, item, attribute_name;