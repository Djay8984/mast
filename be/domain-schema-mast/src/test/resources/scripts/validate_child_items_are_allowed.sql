-- select i1.id, i2.id
select distinct i9.id
from mast_asset_item i1
right join mast_asset_item i2
on i1.id = i2.parent_item_id
right join mast_asset_item i3
on i2.id = i3.parent_item_id
right join mast_asset_item i4
on i3.id = i4.parent_item_id
right join mast_asset_item i5
on i4.id = i5.parent_item_id
right join mast_asset_item i6
on i5.id = i6.parent_item_id
right join mast_asset_item i7
on i6.id = i7.parent_item_id
right join mast_asset_item i8
on i7.id = i8.parent_item_id
right join mast_asset_item i9
on i8.id = i9.parent_item_id
right join mast_asset_attribute a
on i9.id = a.item_id
join mast_ref_attribute_type t
on a.attribute_type_id = t.id
join mast_ref_allowed_attribute_value v
on v.id = t.value_type_id
where i1.asset_id = 5
or i2.asset_id = 5
or i3.asset_id = 5
or i4.asset_id = 5
or i5.asset_id = 5
or i6.asset_id = 5
or i7.asset_id = 5
or i8.asset_id = 5
or i9.asset_id = 5;

-- child nodes abide by relationships
select ci.id ci_i,
ci.name as ci_n,
itr.id as itr_i,
ci.parent_item_id as ci_p,
pi.id as pi_i,
pi.name as pi_n
from mast_asset_item pi
right join mast_asset_item ci
on ci.parent_item_id = pi.id
join mast_ref_item_type_relationship itr
on ci.item_type_id = itr.to_item_type_id
and pi.item_type_id = itr.from_item_type_id
where itr.relationship_type_id = 1;
