package com.baesystems.ai.lr.domain.repositories;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryMetadata;

public class BaseRepositoryFactory extends JpaRepositoryFactory
{
    public BaseRepositoryFactory(final EntityManager entityManager)
    {
        super(entityManager);
    }

    @Override
    protected Class<?> getRepositoryBaseClass(final RepositoryMetadata metadata)
    {
        return BaseRepositoryImpl.class;
    }
}
