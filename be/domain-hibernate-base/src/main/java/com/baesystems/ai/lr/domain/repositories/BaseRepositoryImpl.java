package com.baesystems.ai.lr.domain.repositories;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;

import com.baesystems.ai.lr.dto.query.AbstractQueryDto;

public class BaseRepositoryImpl<T, I extends Serializable>
        extends BaseRepositoryInternal<T, I> implements BaseRepository<T, I>
{
    public BaseRepositoryImpl(final JpaEntityInformation<T, I> entityInformation, final EntityManager entityManager)
    {
        super(entityInformation, entityManager);
    }

    public BaseRepositoryImpl(final Class<T> domainClass, final EntityManager entityManager)
    {
        super(domainClass, entityManager);
    }

    /**
     * Setter method for strictMissingFields. By default all methods are strict on missing fields. This should be used
     * in almost all case. Only where the same query needs to be used on different repositories should set
     * <code>strictMissingFields=false</code> and the query should only have a value defined on fields that really need
     * it because this can hide errors and produce unexpected results.
     *
     * The override is missing deliberately so that this method has to be explicitly added to the interfaces for
     * repositories that need it.
     *
     * @param strictMissingFields whether or not to throw errors when the requested fields are not found or let the
     *        query define the behaviour. true=always throw an error, false=substitute with boolean if defined on query.
     **/
    public void setStrictMissingFields(final boolean strictMissingFields)
    {
        this.setStrictMissingFieldsInternal(strictMissingFields);
    }

    @Override
    public <S extends T> S merge(final S entity)
    {
        return this.entityManager.merge(entity);
    }

    /**
     * Wrapper method for find all that uses a null check to decide which method to use.
     */
    @Override
    public List<T> findAll(final AbstractQueryDto query)
    {
        List<T> result = null;

        if (query == null)
        {
            result = this.findAll();
        }
        else
        {
            result = this.findAllInternal(query);
        }

        return result;
    }

    /**
     * Wrapper method for find all that uses a null check to decide which method to use.
     */
    @Override
    public List<T> findAll(final Sort sort, final AbstractQueryDto query)
    {
        List<T> result = null;

        if (query == null)
        {
            result = this.findAllInternal(sort);
        }
        else
        {
            result = this.findAllInternal(sort, query);
        }

        return result;
    }

    /**
     * Wrapper method for find all that uses a null check to decide which method to use.
     */
    @Override
    public Page<T> findAll(final Pageable pageable, final AbstractQueryDto query)
    {
        Page<T> result = null;

        if (query == null)
        {
            result = this.findAll(pageable);
        }
        else
        {
            result = this.findAllInternal(pageable, query);
        }

        return result;
    }

    /**
     * Wrapper method for count that uses a null check to decide which method to use.
     */
    @Override
    public Long count(final AbstractQueryDto query)
    {
        Long result = null;

        if (query == null)
        {
            result = this.count();
        }
        else
        {
            result = this.countInternal(query);
        }

        return result;
    }
}
