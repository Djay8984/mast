package com.baesystems.ai.lr.domain.repositories;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.baesystems.ai.lr.domain.utils.QueryProcessor;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;

public class BaseRepositoryInternal<T, I extends Serializable> extends SimpleJpaRepository<T, I>
{
    protected final EntityManager entityManager;
    private final Class<T> domainClass;
    private boolean strictMissingFields = true;

    /**
     * Setter method for strictMissingFields. By default all methods are strict on missing fields. This should be used
     * in almost all case. Only where the same query needs to be used on different repositories should set
     * <code>strictMissingFields=false</code> and the query should only have a value defined on fields that really need
     * it because this can hide errors and produce unexpected results.
     *
     * The override is missing deliberately so that this method has to be explicitly added to the interfaces for
     * repositories that need it.
     *
     * @param strictMissingFields whether or not to throw errors when the requested fields are not found or let the
     *        query define the behaviour. true=always throw an error, false=substitute with boolean if defined on query.
     **/
    protected void setStrictMissingFieldsInternal(final boolean strictMissingFields)
    {
        this.strictMissingFields = strictMissingFields;
    }

    public BaseRepositoryInternal(final JpaEntityInformation<T, I> entityInformation, final EntityManager entityManager)
    {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.domainClass = entityInformation.getJavaType();
    }

    public BaseRepositoryInternal(final Class<T> domainClass, final EntityManager entityManager)
    {
        super(domainClass, entityManager);
        this.entityManager = entityManager;
        this.domainClass = domainClass;
    }

    /**
     * Generic method to query the data base table of the class cls with the query contained in query.
     *
     * @param query The query.
     * @return List<cls> The list of results.
     */
    protected List<T> findAllInternal(final AbstractQueryDto query)
    {
        final QueryProcessor queryProcessor = new QueryProcessor(this.strictMissingFields);
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = builder.createQuery(this.domainClass);
        final From<T, T> root = criteriaQuery.from(this.domainClass);

        Predicate[] predicateList = new Predicate[0];

        predicateList = queryProcessor.processQuery(predicateList, builder, root, query);

        criteriaQuery.where(predicateList).distinct(true);
        return this.entityManager.createQuery(criteriaQuery).getResultList();
    }

    /**
     * Generic method to query the data base table of the class cls with the query contained in query.
     *
     * @param sort Description of how to sort the results
     * @param query The query
     * @return List<cls> An ordered list of results
     */
    protected List<T> findAllInternal(final Sort sort, final AbstractQueryDto query)
    {
        final QueryProcessor queryProcessor = new QueryProcessor(this.strictMissingFields);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = builder.createQuery(this.domainClass);
        final From<T, T> root = criteriaQuery.from(this.domainClass);

        Predicate[] predicateList = new Predicate[0];
        predicateList = queryProcessor.processQuery(predicateList, builder, root, query);

        criteriaQuery.orderBy(queryProcessor.processOrdering(sort, root, builder));
        criteriaQuery.where(predicateList).distinct(true);

        final TypedQuery<T> typedQuery = this.entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    /**
     * Generic method to use in place of the default find all when an ordered list is needed.
     *
     * @param sort Description of how to sort the results
     * @return List<cls> An ordered list of results
     */
    protected List<T> findAllInternal(final Sort sort)
    {
        final QueryProcessor queryProcessor = new QueryProcessor(this.strictMissingFields);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = builder.createQuery(this.domainClass);
        final From<T, T> root = criteriaQuery.from(this.domainClass);

        criteriaQuery.orderBy(queryProcessor.processOrdering(sort, root, builder));
        criteriaQuery.select(root);

        final TypedQuery<T> typedQuery = this.entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    /**
     * Generic method to query the data base table of the class cls with the query contained in query.
     *
     * @param pageable Description of the required page.
     * @param query The query.
     * @return Page<cls> The page of results.
     */
    protected Page<T> findAllInternal(final Pageable pageable, final AbstractQueryDto query)
    {
        QueryProcessor queryProcessor = new QueryProcessor(this.strictMissingFields);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<Long> criteriaQueryCount = builder.createQuery(Long.class);
        final From<T, T> rootCount = criteriaQueryCount.from(this.domainClass);

        Predicate[] predicateListCount = new Predicate[0];
        predicateListCount = queryProcessor.processQuery(predicateListCount, builder, rootCount, query);

        criteriaQueryCount.select(builder.count(rootCount));
        criteriaQueryCount.where(predicateListCount).distinct(true);
        final Long resultCountLong = this.entityManager.createQuery(criteriaQueryCount).getSingleResult();
        final int resultCount = resultCountLong.intValue();
        final int resultsFrom = pageable.getPageNumber() * pageable.getPageSize();

        List<T> result = new ArrayList<T>();

        if (resultCount >= resultsFrom)
        {
            queryProcessor = new QueryProcessor(this.strictMissingFields);

            final CriteriaQuery<T> criteriaQuery = builder.createQuery(this.domainClass);
            final From<T, T> root = criteriaQuery.from(this.domainClass);

            Predicate[] predicateList = new Predicate[0];
            predicateList = queryProcessor.processQuery(predicateList, builder, root, query);

            criteriaQuery.orderBy(queryProcessor.processOrdering(pageable.getSort(), root, builder));
            criteriaQuery.where(predicateList).distinct(true);

            TypedQuery<T> typedQuery = this.entityManager.createQuery(criteriaQuery);
            typedQuery = typedQuery.setFirstResult(resultsFrom);
            typedQuery = typedQuery.setMaxResults(pageable.getPageSize());
            result = typedQuery.getResultList();
        }

        return new PageImpl<T>(result, pageable, resultCount);
    }

    /**
     * Generic method to query the data base table of the class cls with the query contained in query and return an
     * count of the results.
     *
     * @param pageable Description of the required page.
     * @param query The query.
     * @param cls The DO object to be queried.
     * @return The number of results.
     */
    protected Long countInternal(final AbstractQueryDto query)
    {
        final QueryProcessor queryProcessor = new QueryProcessor(this.strictMissingFields);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<Long> criteriaQueryCount = builder.createQuery(Long.class);
        final From<T, T> rootCount = criteriaQueryCount.from(this.domainClass);

        Predicate[] predicateListCount = new Predicate[0];
        predicateListCount = queryProcessor.processQuery(predicateListCount, builder, rootCount, query);

        criteriaQueryCount.select(builder.count(rootCount));
        criteriaQueryCount.where(predicateListCount).distinct(true);
        return this.entityManager.createQuery(criteriaQueryCount).getSingleResult();
    }
}
