package com.baesystems.ai.lr.domain.entities;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseDO
{
    public abstract Long getId();

    public boolean equals(Object other)
    {
        boolean result = true;

        if (this != other)
        {
            result = false;
        }

        if (!(other instanceof BaseDO))
        {
            result = false;
        }
        else
        {
            final BaseDO baseDo = (BaseDO) other;

            if (baseDo.getId() != null && !baseDo.getId().equals(getId()))
            {
                result = false;
            }
        }

        return result;
    }

    public int hashCode()
    {
        int result;

        if (getId() != null)
        {
            result = getId().hashCode();
        }
        else
        {
            result = super.hashCode();
        }

        return result;
    }
}
