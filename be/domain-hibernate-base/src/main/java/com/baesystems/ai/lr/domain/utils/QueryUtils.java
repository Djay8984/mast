package com.baesystems.ai.lr.domain.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import com.baesystems.ai.lr.domain.exception.ExceptionMessageUtils;
import com.baesystems.ai.lr.domain.exception.MastQueryException;

public class QueryUtils
{
    private static final int ONE = 1;
    private static final String DOT_REGEX = "\\.";
    private static final String EMBEDDABLE = "javax.persistence.Embeddable";
    private final Map<String, Object> rootMap = new HashMap<String, Object>();
    protected static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    /**
     * Method to add a new predicate to an existing array.
     *
     * @param original Original array.
     * @param added New element.
     * @return New array with original elements plus the new one at the end.
     */
    protected Predicate[] addElement(final Predicate[] original, final Predicate added)
    {
        final int length = original.length;
        final Predicate[] result = Arrays.copyOf(original, length + 1);
        result[length] = added;
        return result;
    }

    /**
     * Method to do repeated gets on nested objects. To do this the query must use the dot notation (as in
     * parent.nestedObject)
     *
     * @param path using not notation
     * @param root the root from the query builder (either root or join)
     * @return the path for the query
     */
    protected <S> Path<Object> get(final String path, final From<S, S> root)
    {
        final String[] pathSegments = path.split(DOT_REGEX);
        Path<Object> returnValue = this.safeGet(pathSegments[0], root);

        for (final String field : Arrays.copyOfRange(pathSegments, 1, pathSegments.length))
        {
            returnValue = this.safeGet(field, returnValue);
        }

        return returnValue;
    }

    /**
     * Method to do repeated gets on nested objects. To do this the query must use the dot notation (as in
     * parent.nestedObject). Unlike the method above this method joins on the fields as it goes to guarantee the mapping
     * will work even for one to many and entities with more than one primary key (like asset). This should only be used
     * for sorting where manual joining is not an option, because joining when it is not needed will make the api slow.
     *
     * @param path using not notation
     * @param root the root from the query builder (either root or join)
     * @return the path for the query
     */
    protected <S> Path<Object> getAutoJoin(final String path, final From<S, S> inputRoot)
    {
        final String[] pathSegments = path.split(DOT_REGEX);
        Path<Object> returnValue = null;

        if (pathSegments.length > ONE)
        { // skip if there is only one field
            From<S, S> root = inputRoot;
            int joins = 0;
            From<S, S> newRoot = null;
            for (final String field : Arrays.copyOfRange(pathSegments, 0, pathSegments.length - 1))
            {
                newRoot = root;
                newRoot = this.safeJoin(field, newRoot, JoinType.LEFT);

                if (this.isEmbeddable(newRoot.getJavaType()))
                { // stop if an entity that cannot not be joined is reached and do the rest of the get from the previous
                  // root. This happens with embeddable objects
                    break;
                }

                ++joins;
                root = newRoot;
            }

            final String[] remaining = Arrays.copyOfRange(pathSegments, joins, pathSegments.length);
            returnValue = this.get(String.join(".", remaining), root);
        }
        else
        {
            returnValue = this.safeGet(pathSegments[0], inputRoot);
        }

        return returnValue;
    }

    /**
     * Method to do repeated joins on nested objects. To do this the query must use the dot notation (as in
     * parent.nestedObject)
     *
     * @param path using not notation
     * @param inputRoot the root from the query builder (either root or join)
     * @return the path for the query
     */
    protected <S> From<S, S> join(final String path, final From<S, S> inputRoot, final JoinType joinType)
    {
        From<S, S> root = inputRoot; // use the input root unless a join is defined

        if (path != null && !path.isEmpty())
        {
            final String[] pathSegments = path.split(DOT_REGEX);
            root = this.safeJoin(pathSegments[0], inputRoot, joinType);

            for (final String field : Arrays.copyOfRange(pathSegments, 1, pathSegments.length))
            {
                root = this.safeJoin(field, root, joinType);
            }
        }

        return root;
    }

    /**
     * Do get inside try/catch to give a useful error if it fails.
     */
    protected <S> Path<Object> safeGet(final String path, final From<S, S> rootPath)
    {
        try
        {
            return rootPath.get(path);
        }
        catch (final IllegalArgumentException | ClassCastException exception)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, path), exception);
        }
    }

    /**
     * Do get inside try/catch to give a useful error if it fails.
     */
    protected <S> Path<Object> safeGet(final String path, final Path<Object> rootPath)
    {
        try
        {
            return rootPath.get(path);
        }
        catch (final IllegalArgumentException | ClassCastException exception)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, path), exception);
        }
    }

    /**
     * Do join inside try/catch to give a useful error if it fails. Default to left if no type is given
     */
    @SuppressWarnings("unchecked")
    protected <S> From<S, S> safeJoin(final String path, final From<S, S> inputRoot, final JoinType joinType)
    {
        From<S, S> root = null;

        // The key needs to uniquely identify the target (even if two DO have a field with the same name) so the
        // full name of the current entity class and the join field are used.
        final String key = inputRoot.getJavaType().toString() + "." + path;

        if (this.rootMap.get(key) == null)
        { // if this entity had not been joined before join it now
            try
            {
                root = inputRoot.join(path, joinType == null ? JoinType.LEFT : joinType);
            }
            catch (final IllegalArgumentException exception)
            {
                throw new MastQueryException(String.format(ExceptionMessageUtils.JOIN_FIELD_NOT_FOUND, path), exception);
            }
            catch (final UnsupportedOperationException exception)
            {
                throw new MastQueryException(exception.getMessage(), exception);
            }
            this.rootMap.put(key, root);
        }
        else
        { // if it has been joined before the existing alias from the map to save joining again.
            root = (From<S, S>) this.rootMap.get(key);
        }

        return root;
    }

    /**
     * Method to retrieve the final field, vie reflection, from a string of objects joined using the dot notation. This
     * is used to test the fields type.
     *
     * @param root The current object being queried (this will be the object type set in the repository definition
     *        unless there is a join)
     * @param path path to the required field using dot notation
     * @return the field at the end of the chain.
     */
    protected <S> Field getField(final From<S, S> root, final String path)
    {
        final String[] pathSegments = path.split(DOT_REGEX);
        Field field = null;
        try
        {
            field = this.getDeclaredField(root.type().getJavaType(), pathSegments[0]);

            for (final String fieldName : Arrays.copyOfRange(pathSegments, 1, pathSegments.length))
            {
                field = this.getDeclaredField(field.getType(), fieldName);
            }
        }
        catch (final NoSuchFieldException exception)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, path), exception);
        }

        return field;
    }

    protected <S> boolean fieldExists(final From<S, S> root, final String path)
    {
        boolean returnValue = true;

        try
        {
            this.getField(root, path);
        }
        catch (final MastQueryException exception)
        {
            returnValue = false;
        }

        return returnValue;
    }

    /**
     * Method to get a field with a given name for the class cls or any of its super classes.
     *
     * @param cls the current class
     * @param fieldName the name of the desired field
     * @return the field
     * @throws NoSuchFieldException
     */
    private <S> Field getDeclaredField(final Class<S> cls, final String fieldName) throws NoSuchFieldException
    {
        Field returnValue = null;

        try
        {
            returnValue = cls.getDeclaredField(fieldName);
        }
        catch (final NoSuchFieldException exception)
        { // if the field is not found on this class try the super class
            final Class<?> superclass = cls.getSuperclass();

            if (superclass == null)
            { // if there is no super class then the field genuinely doesn't exist
                throw exception;
            }

            returnValue = this.getDeclaredField(superclass, fieldName);
        }

        return returnValue;
    }

    private boolean isEmbeddable(final Class<?> cls)
    {
        boolean isEmbeddable = false;

        for (final Annotation annotation : cls.getAnnotations())
        {
            if (EMBEDDABLE.equals(annotation.annotationType().getName()))
            {
                isEmbeddable = true;
                break;
            }
        }

        return isEmbeddable;
    }
}
