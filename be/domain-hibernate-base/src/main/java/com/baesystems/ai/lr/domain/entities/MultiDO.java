package com.baesystems.ai.lr.domain.entities;

public interface MultiDO
{
    Object getContent();

    void setContent(Object content);
}
