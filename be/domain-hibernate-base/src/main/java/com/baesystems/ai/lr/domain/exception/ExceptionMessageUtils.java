package com.baesystems.ai.lr.domain.exception;

//todo can we rename this to something less similar to com.baesystems.ai.lr.utils. ExceptionMessagesUtils;
public class ExceptionMessageUtils
{
    public static final String FIELD_NOT_FOUND = "The field '%s' does not exist on the requested entity.";
    public static final String JOIN_FIELD_NOT_FOUND = "Cannot join on '%s' because it does not exist on the requested entity.";
    public static final String MUST_BE_A_NUMBER_OR_DATE = "The value in field '%s' must be a number or a date if the %s operator is being used.";
    public static final String MUST_BE_A_LIST = "The value in field '%s' must be a list if the IN operator is being used.";
    public static final String FIELD_INCONSISTENT = "The value in field '%s' is not consitent with the field's type.";
    public static final String INCORRECT_FORMAT = "The query is not formatted correctly.";
}
