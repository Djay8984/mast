package com.baesystems.ai.lr.domain.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Sort;

import com.baesystems.ai.lr.domain.exception.ExceptionMessageUtils;
import com.baesystems.ai.lr.domain.exception.MastQueryException;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;

public class QueryProcessor extends QueryRelationshipProcessor
{
    private static final int ONE = 1;
    private static final String ID_STRING = "id";
    private final boolean strictMissingFields;

    public QueryProcessor()
    {
        this.strictMissingFields = true;
    }

    /**
     * Constructor that allows the setting of strictMissingFields. By default all methods are strict on missing fields.
     * This should be used in almost all case. Only where the same query needs to be used on different repositories
     * should set <code>strictMissingFields=false</code> and the query should only have a value defined on fields that
     * really need it because this can hide errors and produce unexpected results.
     *
     * @param strictMissingFields whether or not to throw errors when the requested fields are not found or let the
     *        query define the behaviour. true=always throw an error, false=substitute with boolean if defined on query.
     **/
    public QueryProcessor(final boolean strictMissingFields)
    {
        this.strictMissingFields = strictMissingFields;
    }

    /**
     * Method to process a query dealing with the and or and not fields if predicate or creating a predicate if not. The
     * query object should contain only one of "AND" "OR" and "NOT" or all off "field", "value" and "relationship" and
     * in the second instance can contain "joinField" and optionally "joinType" (defaults to left). This is not verified
     * here but the method does run in the order of priority "AND" then "OR" then "NOT" then builds the query and will
     * only deal with the first of these.
     *
     * The method also looks to see if a join field has been specified and if so it changes the root that the query
     * operates on to that of the join table. An sub-queries, or sub-joins, will operate relative to this.
     *
     * The method calls itself (or itself through processQueryList) as it descends through the nested queries. When
     * queries are found in the and list after processing all of the elements the method creates a composite predicate
     * from the list of predicates with the and clause. The same is true of the or list and the or clause. The not field
     * only allows for one query (though this may have many elements) so by the time the recursion reached the level of
     * the not there should be only one predicate; this is then negated. Finally if all of the other field are null or
     * empty the method calls buildQueryElement to create a predicate and adds this to the list of predicates.
     *
     * @param predicateList The existing list of predicated to be added to.
     * @param builder
     * @param inputRoot
     * @param query The current query to process.
     * @return A list of predicates corresponding to those queries already processed at the current level or recursion.
     */
    @SuppressWarnings({"PMD.AvoidReassigningParameters", "PMD.CyclomaticComplexity"})
    public <S> Predicate[] processQuery(Predicate[] predicateList, final CriteriaBuilder builder, final From<S, S> inputRoot,
            final AbstractQueryDto query)
    {
        if (!this.strictMissingFields && query.getSelectWhenFieldIsMissing() != null
                && (query.getField() != null && !this.fieldExists(inputRoot, query.getField())
                        || query.getJoinField() != null && !this.fieldExists(inputRoot, query.getJoinField())))
        {
            predicateList = this.addElement(predicateList,
                    query.getSelectWhenFieldIsMissing() ? builder.and(new Predicate[0]) : builder.or(new Predicate[0]));
        }
        else
        {
            final From<S, S> root = this.join(query.getJoinField(), inputRoot, query.getJoinType());

            if (query.getNot() != null)
            {
                Predicate[] notList = new Predicate[0];
                notList = this.processQuery(notList, builder, root, query.getNot());

                if (notList.length == ONE)
                {
                    predicateList = this.addElement(predicateList, notList[0].not());
                }
                else
                {
                    throw new MastQueryException(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));
                }
            }
            else if (query.getAnd() != null && !query.getAnd().isEmpty())
            {
                final Predicate[] andList = new Predicate[0];
                predicateList = this.addElement(predicateList,
                        builder.and(this.processQueryList(andList, builder, root, query.getAnd())));
            }
            else if (query.getOr() != null && !query.getOr().isEmpty())
            {
                final Predicate[] orList = new Predicate[0];
                predicateList = this.addElement(predicateList,
                        builder.or(this.processQueryList(orList, builder, root, query.getOr())));
            }
            else if (query.getField() != null && query.getValue() != null && query.getRelationship() != null)
            {
                predicateList = this.addElement(predicateList,
                        this.buildQueryElement(builder, root, query.getField(), query.getValue(), query.getRelationship()));
            }
            else if (query.getAnd() != null
                    || query.getOr() != null
                    || query.getNot() != null
                    || query.getField() != null
                    || query.getValue() != null
                    || query.getRelationship() != null
                    || query.getJoinField() != null
                    || query.getJoinType() != null)
            {
                throw new MastQueryException(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));
            }
        }

        return predicateList;
    }

    /**
     * Method to process a list of queries. All this method does is call processQuery for every element in the list
     * which concatenates the list of predicates.
     *
     * @param predicateList Existing list of predicates, to add to.
     * @param builder
     * @param root
     * @param queryList List of queries.
     * @return A list of predicates corresponding to those queries already processed at the current level or recursion.
     */
    @SuppressWarnings("PMD.AvoidReassigningParameters")
    private <S> Predicate[] processQueryList(Predicate[] predicateList, final CriteriaBuilder builder, final From<S, S> root,
            final List<AbstractQueryDto> queryList)
    {
        for (final AbstractQueryDto query : queryList)
        {
            predicateList = this.processQuery(predicateList, builder, root, query);
        }

        return predicateList;
    }

    /**
     * Method to convert the list of ordering fields in the pagination into the correct format for the criteria builder.
     * If no are given the id is used by default. If some are given, but the id is not include then it is added at the
     * end to make sure the order is unique.
     *
     * @param pageable containing the ordering description.
     * @param root
     * @param builder
     * @return List of orders for the query builder.
     */
    public <S> List<Order> processOrdering(final Sort sort, final From<S, S> root, final CriteriaBuilder builder)
    {
        final List<Order> orders = new ArrayList<Order>();
        boolean idFound = false;

        if (sort != null)
        {
            final Iterator<Sort.Order> iter = sort.iterator();

            while (iter.hasNext())
            { // Iterate over all of the sort fields in the page information
                final Sort.Order value = iter.next();

                if (ID_STRING.equals(value.getProperty()))
                {
                    idFound = true;
                }

                if (value.isAscending())
                {
                    orders.add(builder.asc(this.getAutoJoin(value.getProperty(), root)));
                }
                else
                {
                    orders.add(builder.desc(this.getAutoJoin(value.getProperty(), root)));
                }
            }
        }

        if (!idFound && this.fieldExists(root, ID_STRING))
        { // add id as the last parameter if it is not already present.
            orders.add(builder.asc(root.get(ID_STRING)));
        }

        return orders;
    }
}
