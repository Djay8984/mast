package com.baesystems.ai.lr.domain.interceptors;

import java.util.Arrays;

import org.hibernate.EmptyInterceptor;

public class NullsLastInterceptor extends EmptyInterceptor
{
    private static final long serialVersionUID = -2642928343678044872L;

    private static final String ORDER_BY_TOKEN = "order by ";

    @Override
    public String onPrepareStatement(final String sql)
    {
        String retVal = sql;

        int orderByStart = sql.toLowerCase().indexOf(ORDER_BY_TOKEN);
        if (orderByStart != -1)
        {
            orderByStart += ORDER_BY_TOKEN.length();
            final String orderByContent = sql.substring(orderByStart);
            final StringBuilder sb = new StringBuilder(sql.substring(0, orderByStart));

            // get sections of the string that are separated by spaces, but do not have a preceding comma. In case there
            // are other statements (like limit) after the order by statement. Temporarily remove the spaces between the
            // ordering field and asc and desc.
            final String[] orderByContentParts = orderByContent
                    .replaceAll(" asc,", "|asc,")
                    .replaceAll(" asc ", "|asc ")
                    .replaceAll(" asc$", "|asc")
                    .replaceAll(" desc,", "|desc,")
                    .replaceAll(" desc ", "|desc ")
                    .replaceAll(" desc$", "|desc")
                    .split("(?<=[0-9a-zA-Z\\?] )");

            // The first of these is the list of thing to order by which is to be processed.
            final String[] orders = orderByContentParts[0].split(", ");
            for (int i = 0; i < orders.length; i++)
            {
                final String[] parts = orders[i].trim().split(" ");
                sb.append("isNull(").append(parts[0].split("\\|")[0]).append("), ").append(orders[i]);

                if (i != orders.length - 1)
                {
                    sb.append(", ");
                }
            }

            // copy anything that comes after the order by part onto the end.
            for (final String element : Arrays.copyOfRange(orderByContentParts, 1, orderByContentParts.length))
            {
                sb.append(element);
            }
            retVal = sb.toString();
        }

        retVal = retVal.replaceAll("\\|asc", " asc").replaceAll("\\|desc", " desc");

        return super.onPrepareStatement(retVal);
    }

}
