package com.baesystems.ai.lr.domain.exception;

import org.springframework.http.HttpStatus;

public class MastQueryException extends RuntimeException
{
    private static final long serialVersionUID = 5335749937127097954L;

    public MastQueryException()
    {
        super();
    }

    public MastQueryException(final String message)
    {
        super(message);
    }

    public MastQueryException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public MastQueryException(final Throwable cause)
    {
        super(cause);
    }

    public int getErrorCode()
    {
        return HttpStatus.BAD_REQUEST.value();
    }
}
