package com.baesystems.ai.lr.domain.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import com.baesystems.ai.lr.domain.exception.ExceptionMessageUtils;
import com.baesystems.ai.lr.domain.exception.MastQueryException;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

public class QueryRelationshipProcessor extends QueryRelationshipProcessorUtils
{
    /**
     * Method to create a predicate, for use in the overall query, that is equivalent to the input query. The result
     * will be an SQL query of the form field relationship value (e.g. {"field":"id", "value":10, "relationship": "LE"}
     * produces the equivalent of id <= 10.
     *
     * @param builder
     * @param root
     * @param field The name of the DO field being queried (e.g. id).
     * @param value The value the field is to be compared to.
     * @param relationship The type of comparison chosen from {@link QueryRelationshipType}. (e.g. <,>,<=,>=,!=,==,like
     *        and in).
     * @return A predicate corresponding to the query.
     */
    protected <S> Predicate buildQueryElement(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final QueryRelationshipType relationship)
    {
        Predicate predicate = null;

        switch (relationship)
        {
            case EQ:
            case NE:
            case LT:
            case LE:
            case GT:
            case GE:
                predicate = this.processComparable(builder, root, field, value, relationship, false);
                break;
            case IN:
                predicate = this.processIn(root, field, value);
                break;
            case LIKE:
                predicate = this.processLike(builder, root, field, value);
                break;
            default:
                break;
        }

        return predicate;
    }

    /**
     * This method will call the appropriate sub method for a comparison. Based on the field type passed the sub methods
     * will then decide whether to do a number or date comparison. In the event that the field is a string then they
     * will try to cans to a number first. If this fails (because the value maybe a date) then the this method will call
     * them again telling them to cast as a date. If that fails then an exception is thrown.
     *
     * @param builder
     * @param root
     * @param field The name of the DO field being queried (e.g. id).
     * @param value The value the field is to be compared to.
     * @param relationship The type of comparison chosen from {@link QueryRelationshipType}. (e.g. <,>,<=,>=,!=,==,like
     *        and in).
     * @param inputRetryWithDates true to try again casting the filed as a date
     * @return A predicate corresponding to the query.
     */
    protected <S> Predicate processComparable(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final QueryRelationshipType relationship, final boolean inputRetryWithDates)
    {
        if (value instanceof ArrayList)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, field));
        }

        Predicate predicate = null;
        boolean retryWithDates = inputRetryWithDates;

        try
        {
            Class<?> type = null;

            if (!retryWithDates)
            {
                type = this.getField(root, field).getType();
                retryWithDates = Date.class.isInstance(type);
            }
            else
            {
                type = Date.class;
            }
            predicate = this.comparableSwitch(builder, root, field, value, relationship, type);
        }
        catch (final ClassCastException | ParseException exception)
        {
            if (!retryWithDates)
            {
                predicate = this.processComparable(builder, root, field, value, relationship, true);
            }
            else
            {
                throw new MastQueryException(String.format(ExceptionMessageUtils.MUST_BE_A_NUMBER_OR_DATE, field, relationship), exception);
            }
        }
        catch (final NumberFormatException exception)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, field), exception);
        }

        return predicate;
    }

    protected <S> Predicate comparableSwitch(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final QueryRelationshipType relationship, final Class<?> type) throws ParseException
    {
        Predicate predicate = null;

        switch (relationship)
        {
            case EQ:
                predicate = this.processEq(builder, root, field, value, type);
                break;
            case NE:
                predicate = this.processNe(builder, root, field, value, type);
                break;
            case LT:
                predicate = this.processLt(builder, root, field, value, type);
                break;
            case LE:
                predicate = this.processLe(builder, root, field, value, type);
                break;
            case GT:
                predicate = this.processGt(builder, root, field, value, type);
                break;
            case GE:
                predicate = this.processGe(builder, root, field, value, type);
                break;
            default:
                break;
        }

        return predicate;
    }
}
