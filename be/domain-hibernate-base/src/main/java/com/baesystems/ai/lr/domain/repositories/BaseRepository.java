package com.baesystems.ai.lr.domain.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import com.baesystems.ai.lr.dto.query.AbstractQueryDto;

@NoRepositoryBean
public interface BaseRepository<T, I extends Serializable> extends Repository<T, I>
{
    <S extends T> S merge(S entity);

    List<T> findAll(final AbstractQueryDto query);

    List<T> findAll(Sort sort, AbstractQueryDto query);

    Page<T> findAll(Pageable pageable, AbstractQueryDto query);

    Long count(AbstractQueryDto query);
}
