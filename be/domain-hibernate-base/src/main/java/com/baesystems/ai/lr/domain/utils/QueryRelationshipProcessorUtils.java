package com.baesystems.ai.lr.domain.utils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import com.baesystems.ai.lr.domain.exception.ExceptionMessageUtils;
import com.baesystems.ai.lr.domain.exception.MastQueryException;

public class QueryRelationshipProcessorUtils extends QueryUtils
{
    protected <S> Predicate processEq(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        final Field fieldFromObject = this.getField(root, field);

        Predicate returnValue = null;

        if (Date.class.equals(fieldFromObject.getType()))
        { // see if the field on the object is a date if so try and parse the value
            returnValue = builder.equal(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.equal(this.get(field, root), value);
        }

        return returnValue;
    }

    protected <S> Predicate processNe(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        Predicate returnValue = null;

        if (Date.class.equals(type))
        { // see if the field on the object is a date if so try and parse the value
            returnValue = builder.notEqual(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.notEqual(this.get(field, root), value);
        }

        return returnValue;
    }

    protected <S> Predicate processLt(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        Predicate returnValue = null;

        if (Date.class.equals(type))
        {
            returnValue = builder.lessThan(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.lt(this.get(field, root).as(Number.class), (Number) value);
        }

        return returnValue;
    }

    protected <S> Predicate processLe(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        Predicate returnValue = null;

        if (Date.class.equals(type))
        {
            returnValue = builder.lessThanOrEqualTo(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.le(this.get(field, root).as(Number.class), (Number) value);
        }

        return returnValue;
    }

    protected <S> Predicate processGt(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        Predicate returnValue = null;

        if (Date.class.equals(type))
        {
            returnValue = builder.greaterThan(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.gt(this.get(field, root).as(Number.class), (Number) value);
        }

        return returnValue;
    }

    protected <S> Predicate processGe(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value, final Class<?> type) throws ParseException
    {
        Predicate returnValue = null;

        if (Date.class.equals(type))
        {
            returnValue = builder.greaterThanOrEqualTo(this.get(field, root).as(Date.class), DATE_FORMATTER.parse((String) value));
        }
        else
        {
            returnValue = builder.ge(this.get(field, root).as(Number.class), (Number) value);
        }

        return returnValue;
    }

    protected <S> Predicate processIn(final From<S, S> root, final String field,
            final Object value)
    {
        if (value instanceof ArrayList)
        {
            @SuppressWarnings("unchecked")
            final ArrayList<Object> list = (ArrayList<Object>) value;
            final Object[] array = list.toArray();
            try
            {
                return this.get(field, root).in(array);
            }
            catch (final NumberFormatException | ClassCastException exception)
            {
                throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, field), exception);
            }
        }
        else
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.MUST_BE_A_LIST, field));
        }
    }

    protected <S> Predicate processLike(final CriteriaBuilder builder, final From<S, S> root, final String field,
            final Object value)
    {
        if (value instanceof ArrayList)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, field));
        }
        try
        {
            final String search = (String) value;
            return builder.like(this.get(field, root).as(String.class), search.replaceAll("%", "\\%").replaceAll("\\*", "%"));
        }
        catch (final ClassCastException exception)
        {
            throw new MastQueryException(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, field), exception);
        }
    }
}
