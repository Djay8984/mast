package com.baesystems.ai.lr.domain.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_TEST_NO_ID")
public class TestNoIdDO extends BaseDO
{
    @Id
    @Column(name = "field")
    private Date field;

    public Date getField()
    {
        return this.field;
    }

    public void setField(final Date field)
    {
        this.field = field;
    }

    @Override
    public Long getId()
    {
        return null;
    }
}
