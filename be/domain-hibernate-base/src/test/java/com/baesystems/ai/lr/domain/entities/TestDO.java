package com.baesystems.ai.lr.domain.entities;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.baesystems.ai.lr.domain.mast.entities.LinkDO;

@Entity
@Table(name = "MAST_TEST")
public class TestDO extends SuperTestDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "date")
    private Date date;

    @JoinColumn(name = "test_id")
    @OneToOne(cascade = {CascadeType.ALL})
    private TestDO test;

    @JoinColumn(name = "list_id")
    @OneToOne(cascade = {CascadeType.ALL})
    private TestListDO list;

    @JoinColumn(name = "list2_id")
    @OneToOne(cascade = {CascadeType.ALL})
    private TestList2DO list2;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "link_id"))})
    private LinkDO link;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public Date getDate()
    {
        return this.date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    public TestDO getTest()
    {
        return this.test;
    }

    public void setTest(final TestDO test)
    {
        this.test = test;
    }

    public TestListDO getList()
    {
        return this.list;
    }

    public void setList(final TestListDO list)
    {
        this.list = list;
    }

    public TestList2DO getList2()
    {
        return this.list2;
    }

    public void setList2(final TestList2DO list2)
    {
        this.list2 = list2;
    }

    public LinkDO getLink()
    {
        return this.link;
    }

    public void setLink(final LinkDO link)
    {
        this.link = link;
    }
}
