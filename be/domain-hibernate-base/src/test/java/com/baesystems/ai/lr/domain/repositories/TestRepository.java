package com.baesystems.ai.lr.domain.repositories;

import com.baesystems.ai.lr.domain.entities.TestDO;

public interface TestRepository extends SpringDataRepository<TestDO, Long>
{

}
