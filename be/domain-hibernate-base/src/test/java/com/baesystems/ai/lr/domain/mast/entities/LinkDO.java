package com.baesystems.ai.lr.domain.mast.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LinkDO implements Serializable
{
    private static final long serialVersionUID = 2339652961543738469L;

    @Column
    private Long id;

    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }
}
