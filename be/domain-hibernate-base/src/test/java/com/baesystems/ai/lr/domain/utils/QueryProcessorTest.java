package com.baesystems.ai.lr.domain.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.SingularAttribute;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.baesystems.ai.lr.domain.entities.TestDO;
import com.baesystems.ai.lr.domain.entities.TestNoIdDO;
import com.baesystems.ai.lr.domain.exception.ExceptionMessageUtils;
import com.baesystems.ai.lr.domain.exception.MastQueryException;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

@SuppressWarnings({"unchecked", "PMD.TooManyMethods", "PMD.ShortVariable"})
@RunWith(Theories.class)
public class QueryProcessorTest<S>
{
    private static final String ID = "id";
    private static final String TEST = "test";
    private static final String NAME = "name";
    private static final String DATE = "date";
    private static final String SUPER_TEST_FILED = "superTestField";
    private static final String A_DATE = "2016-09-30";
    private static final String TEST_DOT_ID = "test.id";
    private static final String TEST_DOT_DATE = "test.date";
    private static final String LINK_DOT_ID = "link.id";
    private static final String INVALID_PATH = "invalid";
    private static final String TEST_DOT_INVALID_PATH = "test.invalid";
    private static final String LIST = "list";
    private static final String LIST2 = "list2";
    private static final String AND = "AND";
    private static final String OR = "OR";

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int SIX = 6;

    private static EntityManager entityManager;

    private QueryProcessor queryProcessor;
    private CriteriaBuilder builder;
    private CriteriaQuery<S> criteriaQuery;
    private From<S, S> root;

    private AbstractQueryDto query;
    private Predicate[] predicateList;

    @DataPoints
    public static final QueryRelationshipType[] RELATIONSHIPS = {QueryRelationshipType.EQ, QueryRelationshipType.NE, QueryRelationshipType.LT,
                                                                 QueryRelationshipType.LE, QueryRelationshipType.GT, QueryRelationshipType.GE,
                                                                 QueryRelationshipType.LIKE};

    @DataPoints
    public static final String[] RELATIONSHIPS2 = {"EQ", "NE", "LT", "LE", "GT", "GE"};

    @DataPoints
    public static final boolean[] TRUE_AND_FLASE = {true, false};

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        final EntityManagerFactory factory = Persistence.createEntityManagerFactory("testMastPersistence");
        entityManager = factory.createEntityManager();
    }

    @Before
    public void setup()
    {
        final Class<S> cls = (Class<S>) TestDO.class;
        this.builder = QueryProcessorTest.entityManager.getCriteriaBuilder();
        this.criteriaQuery = this.builder.createQuery(cls);
        this.root = this.criteriaQuery.from(cls);

        this.query = new AbstractQueryDto();
        this.predicateList = new Predicate[0];

        this.queryProcessor = new QueryProcessor();
    }

    @Test
    public void processQueryEmptyTest()
    {
        final Pattern expectedSql = Pattern.compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where 1=1$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ZERO, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQuerySimpleEQandNETest()
    {
        this.query.setField(ID);
        this.query.setValue(ONE);

        final String[] symbols = {"=", "<>"};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.EQ, QueryRelationshipType.NE};

        for (int i = 0; i < TWO; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.id" + symbols[i] + "1L$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySimpleEQandNEDateTest()
    {
        this.query.setField(DATE);
        this.query.setValue(A_DATE);

        final String[] symbols = {"=", "<>"};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.EQ, QueryRelationshipType.NE};

        for (int i = 0; i < TWO; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.date" + symbols[i] + "\\:[a-zA-Z0-9]*$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQueryDateInStringFieldTest()
    {
        this.query.setField(NAME);
        this.query.setValue(A_DATE);

        final String[] symbols = {"<", "<=", ">", ">="};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.LT, QueryRelationshipType.LE, QueryRelationshipType.GT,
                                                       QueryRelationshipType.GE};
        for (int i = 0; i < FOUR; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where cast\\([a-zA-Z0-9]*\\.name as timestamp\\)" + symbols[i]
                            + "\\:[a-zA-Z0-9]*$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySimpleLTandGTTest()
    {
        this.query.setField(ID);
        this.query.setValue(ONE);

        final String[] symbols = {"<", "<=", ">", ">="};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.LT, QueryRelationshipType.LE, QueryRelationshipType.GT,
                                                       QueryRelationshipType.GE};

        for (int i = 0; i < FOUR; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where cast\\([a-zA-Z0-9]*\\.id as java\\.lang\\.Number\\)" + symbols[i]
                            + "1$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySimpleLTandGTDatesTest()
    {
        this.query.setField(DATE);
        this.query.setValue(A_DATE);

        final String[] symbols = {"<", "<=", ">", ">="};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.LT, QueryRelationshipType.LE, QueryRelationshipType.GT,
                                                       QueryRelationshipType.GE};

        for (int i = 0; i < FOUR; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.date" + symbols[i]
                            + "\\:[a-zA-Z0-9]*$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySimpleINTest()
    {
        final List<Integer> list = new ArrayList<Integer>();

        list.add(ONE);
        list.add(TWO);
        list.add(THREE);

        this.query.setField(ID);
        this.query.setValue(list);
        this.query.setRelationship(QueryRelationshipType.IN);

        final Pattern expectedSql = Pattern
                .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.id in \\(1L, 2L, 3L\\)$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQuerySimpleLIKETest()
    {
        this.query.setField(NAME);
        this.query.setValue("*name*");
        this.query.setRelationship(QueryRelationshipType.LIKE);

        final Pattern expectedSql = Pattern
                .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.name like \\:[a-zA-Z0-9]*$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Theory
    public void processQuerySimpleListsInTheWrongPlaceTest(final QueryRelationshipType relationship)
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.FIELD_INCONSISTENT, ID));

        final List<Integer> list = new ArrayList<Integer>();

        list.add(ONE);
        list.add(TWO);
        list.add(THREE);

        this.query.setField(ID);
        this.query.setValue(list);
        this.query.setRelationship(relationship);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleINNotListTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.MUST_BE_A_LIST, ID));

        this.query.setField(ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.IN);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleAndTest()
    {
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();
        final List<AbstractQueryDto> queryList = new ArrayList<AbstractQueryDto>();

        query1.setField(ID);
        query1.setValue(ONE);
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setField(NAME);
        query2.setValue(NAME);
        query2.setRelationship(QueryRelationshipType.EQ);

        queryList.add(query1);
        queryList.add(query2);

        this.query.setAnd(queryList);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where \\( [a-zA-Z0-9]*\\.id=1L \\) and \\( [a-zA-Z0-9]*\\.name=\\:[a-zA-Z0-9]* \\)$");

        this.predicateList = new Predicate[0];
        this.queryProcessor = new QueryProcessor();

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertEquals(TWO, this.predicateList[0].getExpressions().size());
        assertEquals(AND, this.predicateList[0].getOperator().name());
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQuerySimpleOrTest()
    {
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();
        final List<AbstractQueryDto> queryList = new ArrayList<AbstractQueryDto>();

        query1.setField(ID);
        query1.setValue(ONE);
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setField(NAME);
        query2.setValue(NAME);
        query2.setRelationship(QueryRelationshipType.EQ);

        queryList.add(query1);
        queryList.add(query2);

        this.query.setOr(queryList);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where \\( [a-zA-Z0-9]*\\.id=1L \\) or \\( [a-zA-Z0-9]*\\.name=\\:[a-zA-Z0-9]* \\)$");

        this.predicateList = new Predicate[0];
        this.queryProcessor = new QueryProcessor();

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertEquals(TWO, this.predicateList[0].getExpressions().size());
        assertEquals(OR, this.predicateList[0].getOperator().name());
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQuerySimpleNotTest()
    {
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();
        final AbstractQueryDto query3 = new AbstractQueryDto();
        final List<AbstractQueryDto> queryList = new ArrayList<AbstractQueryDto>();

        query1.setField(ID);
        query1.setValue(ONE);
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setField(NAME);
        query2.setValue(NAME);
        query2.setRelationship(QueryRelationshipType.EQ);

        queryList.add(query1);
        queryList.add(query2);

        query3.setOr(queryList);

        this.query.setNot(query3);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where \\( [a-zA-Z0-9]*\\.id<>1L \\) and \\( [a-zA-Z0-9]*\\.name<>\\:[a-zA-Z0-9]* \\)$");

        this.predicateList = new Predicate[0];
        this.queryProcessor = new QueryProcessor();

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertEquals(TWO, this.predicateList[0].getExpressions().size());
        assertEquals(AND, this.predicateList[0].getOperator().name()); // because the expression has been negated
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQuerySingleJoinTest()
    {
        this.query.setJoinField(LIST);
        this.query.setJoinType(JoinType.LEFT);
        this.query.setField(ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.list as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.id=1L$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQueryNestedJoinTest()
    {
        this.query.setJoinField("test.list");
        this.query.setJoinType(JoinType.LEFT);
        this.query.setField(ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.test as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.list as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.id=1L$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQueryMultipleJoinsTest()
    {
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();
        final List<AbstractQueryDto> queryList = new ArrayList<AbstractQueryDto>();

        query1.setJoinField(LIST);
        query1.setField(ID);
        query1.setValue(ONE);
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setJoinField(LIST);
        query2.setField(ID);
        query2.setValue(TWO);
        query2.setRelationship(QueryRelationshipType.EQ);

        queryList.add(query1);
        queryList.add(query2);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.list as [a-zA-Z0-9]* where \\( [a-zA-Z0-9]*\\.id=1L \\) or \\( [a-zA-Z0-9]*\\.id=2L \\)$");

        this.query.setOr(queryList);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertEquals(TWO, this.predicateList[0].getExpressions().size());
        assertEquals(OR, this.predicateList[0].getOperator().name());
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQueryMultipleJoinsToMultpleFieldTest()
    {
        final AbstractQueryDto query1 = new AbstractQueryDto();
        final AbstractQueryDto query2 = new AbstractQueryDto();
        final AbstractQueryDto query3 = new AbstractQueryDto();
        final List<AbstractQueryDto> queryList = new ArrayList<AbstractQueryDto>();

        query1.setJoinField(LIST);
        query1.setField(ID);
        query1.setValue(ONE);
        query1.setRelationship(QueryRelationshipType.EQ);

        query2.setJoinField(LIST2);
        query2.setField(ID);
        query2.setValue(TWO);
        query2.setRelationship(QueryRelationshipType.EQ);

        query3.setJoinField(LIST);
        query3.setField(ID);
        query3.setValue(THREE);
        query3.setRelationship(QueryRelationshipType.EQ);

        queryList.add(query1);
        queryList.add(query2);
        queryList.add(query3);

        final Pattern expectedSql = Pattern
                .compile(
                        "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.list as [a-zA-Z0-9]* left join [a-zA-Z0-9]*\\.list2 as [a-zA-Z0-9]*"
                                + " where \\( [a-zA-Z0-9]*\\.id=1L \\) or \\( [a-zA-Z0-9]*\\.id=2L \\) or \\( [a-zA-Z0-9]*\\.id=3L \\)$");

        this.query.setOr(queryList);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertEquals(THREE, this.predicateList[0].getExpressions().size());
        assertEquals(OR, this.predicateList[0].getOperator().name());
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQueryNestedTest()
    {
        this.query.setField(TEST_DOT_ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        final Pattern expectedSql = Pattern
                .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.test\\.id=1L$");

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Test
    public void processQueryNestedDateTest()
    {
        this.query.setField(TEST_DOT_DATE);
        this.query.setValue(A_DATE);

        final String[] symbols = {"=", "<>", "<", "<=", ">", ">="};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.EQ, QueryRelationshipType.NE, QueryRelationshipType.LT,
                                                       QueryRelationshipType.LE, QueryRelationshipType.GT, QueryRelationshipType.GE};

        for (int i = 0; i < SIX; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.test\\.date" + symbols[i] + "\\:[a-zA-Z0-9]*$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySuperClassDateTest()
    {
        this.query.setField(SUPER_TEST_FILED);
        this.query.setValue(A_DATE);

        final String[] symbols = {"=", "<>", "<", "<=", ">", ">="};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.EQ, QueryRelationshipType.NE, QueryRelationshipType.LT,
                                                       QueryRelationshipType.LE, QueryRelationshipType.GT, QueryRelationshipType.GE};

        for (int i = 0; i < SIX; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile(
                            "^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.superTestField" + symbols[i] + "\\:[a-zA-Z0-9]*$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processOrderingEmptyTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Pageable pageable = new PageRequest(ZERO, ONE);

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        // check that the id is added as the default is nothing is given.
        assertEquals(ONE, orders.size());
        assertEquals(ID, this.getFieldName(orders.get(0)));
        assertTrue(orders.get(0).isAscending()); // ASC is the default
    }

    @Test
    public void processOrderingEmptyNoIdTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Class<S> cls = (Class<S>) TestNoIdDO.class;
        this.criteriaQuery = this.builder.createQuery(cls);
        this.root = this.criteriaQuery.from(cls);

        final Pageable pageable = new PageRequest(ZERO, ONE);

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        // check that the id is not added.
        assertTrue(orders.isEmpty());
    }

    @Test
    public void processOrderingNameTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Pageable pageable = new PageRequest(ZERO, ONE, new Sort(NAME));

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        // check that the id is added as the default after the other parameter.
        assertEquals(TWO, orders.size());
        assertEquals(NAME, this.getFieldName(orders.get(0)));
        assertEquals(ID, this.getFieldName(orders.get(1)));
        assertTrue(orders.get(0).isAscending());
        assertTrue(orders.get(1).isAscending()); // ASC is the default
    }

    @Test
    public void processOrderingIdThenNameTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Pageable pageable = new PageRequest(ZERO, ONE, new Sort(new String[]{ID, NAME}));

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        // check that both parameters are there, but id is not there twice.
        assertEquals(TWO, orders.size());
        assertEquals(ID, this.getFieldName(orders.get(0)));
        assertEquals(NAME, this.getFieldName(orders.get(1)));
        assertTrue(orders.get(0).isAscending());
        assertTrue(orders.get(1).isAscending()); // ASC is the default
    }

    @Test
    public void processOrderingIdThenNameDescTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Pageable pageable = new PageRequest(ZERO, ONE, new Sort(Direction.DESC, new String[]{ID, NAME}));

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        // check that both parameters are there, but id is not there twice.
        assertEquals(TWO, orders.size());
        assertEquals(ID, this.getFieldName(orders.get(0)));
        assertEquals(NAME, this.getFieldName(orders.get(1)));
        assertFalse(orders.get(0).isAscending());
        assertFalse(orders.get(1).isAscending()); // check DESC
    }

    @Test
    public void processOrderingNestedTest() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    {
        final Pageable pageable = new PageRequest(ZERO, ONE, new Sort(Direction.ASC, new String[]{TEST_DOT_DATE}));

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        assertEquals(TWO, orders.size());
        assertEquals(DATE, this.getFieldName(orders.get(0))); // should be the field name of the inner property
        assertEquals(ID, this.getFieldName(orders.get(1)));
    }

    @Test
    public void processOrderingNestedTestStopOnLink() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
    { // Test can't test if the break in the loop is hit, but it would fail if it wasn't because the get will fail.
        final Pageable pageable = new PageRequest(ZERO, ONE, new Sort(Direction.ASC, new String[]{LINK_DOT_ID}));

        final List<Order> orders = this.queryProcessor.processOrdering(pageable.getSort(), this.root, this.builder);

        assertEquals(TWO, orders.size());
        assertEquals(ID, this.getFieldName(orders.get(0))); // should be the field name of the inner property
        assertEquals(ID, this.getFieldName(orders.get(1)));
    }

    @Theory
    public void processQueryWrongFieldName1Test(final String relationship)
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, INVALID_PATH));

        this.query.setField(INVALID_PATH);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.valueOf(relationship));

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryWrongFieldName2Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, INVALID_PATH));

        this.query.setField(INVALID_PATH);
        this.query.setValue(INVALID_PATH);
        this.query.setRelationship(QueryRelationshipType.LIKE);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Theory
    public void processQueryWrongFieldSubstituteTest(final boolean substitute)
    {
        final Pattern expectedSql = Pattern
                .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where " + (substitute ? "1" : "0") + "=1$");

        this.queryProcessor = new QueryProcessor(false);
        this.query.setField(INVALID_PATH);
        this.query.setValue(INVALID_PATH);
        this.query.setRelationship(QueryRelationshipType.EQ);
        this.query.setSelectWhenFieldIsMissing(substitute);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Theory
    public void processQueryWrongJoinFieldSubstituteTest(final boolean substitute)
    {
        final Pattern expectedSql = Pattern
                .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where " + (substitute ? "1" : "0") + "=1$");

        this.queryProcessor = new QueryProcessor(false);
        this.query.setJoinField(INVALID_PATH);
        this.query.setField(NAME);
        this.query.setValue(NAME);
        this.query.setRelationship(QueryRelationshipType.EQ);
        this.query.setSelectWhenFieldIsMissing(substitute);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
        this.criteriaQuery.where(this.predicateList);

        final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

        assertEquals(ONE, this.predicateList.length);
        assertTrue(expectedSql.matcher(sql).matches());
    }

    @Theory
    public void processQueryValidFieldSubstituteTest(final boolean substitute)
    {
        this.queryProcessor = new QueryProcessor(false);

        this.processQuerySimpleEQandNEDateTest();

        this.predicateList = new Predicate[0];
        this.query.setSelectWhenFieldIsMissing(substitute);

        this.processQuerySimpleEQandNEDateTest();
    }

    @Theory
    public void processQueryValidJoinFieldSubstituteTest(final boolean substitute)
    {
        this.queryProcessor = new QueryProcessor(false);

        this.processQuerySingleJoinTest();

        this.predicateList = new Predicate[0];
        this.query.setSelectWhenFieldIsMissing(substitute);

        this.processQuerySingleJoinTest();
    }

    @Theory
    public void processQueryNullFieldSubstituteTest(final boolean substitute)
    {
        this.query.setSelectWhenFieldIsMissing(substitute);
        this.queryProcessor = new QueryProcessor(false);
        this.processQuerySimpleNullFields3Test();
    }

    @Theory
    public void processQueryNullJoinFieldSubstituteTest()
    {
        this.queryProcessor = new QueryProcessor(false);
        this.processQuerySimpleEQandNETest();
    }

    @Test
    public void processQueryWrongFieldNameNested1Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, TEST_DOT_INVALID_PATH));

        this.query.setField(TEST_DOT_INVALID_PATH);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryWrongFieldNameNested2Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.FIELD_NOT_FOUND, INVALID_PATH));

        this.query.setField(TEST_DOT_INVALID_PATH);
        this.query.setValue(INVALID_PATH);
        this.query.setRelationship(QueryRelationshipType.LIKE);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryWrongFieldNameJoinTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.JOIN_FIELD_NOT_FOUND, INVALID_PATH));

        this.query.setJoinField(INVALID_PATH);
        this.query.setField(ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryUnsupportedJoinTypeTest()
    {
        this.thrown.expect(MastQueryException.class);

        this.query.setJoinField(LIST);
        this.query.setJoinType(JoinType.RIGHT);
        this.query.setField(ID);
        this.query.setValue(ONE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Theory
    public void processQueryFieldAndValueDontMatchTest(final String relationship)
    {
        this.thrown.expect(MastQueryException.class);

        this.query.setField(ID);
        this.query.setValue(NAME);
        this.query.setRelationship(QueryRelationshipType.valueOf(relationship));

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryFieldAndValueDontMatchINTest()
    {
        this.thrown.expect(MastQueryException.class);

        final List<String> list = new ArrayList<String>();

        list.add(NAME);

        this.query.setField(ID);
        this.query.setValue(list);
        this.query.setRelationship(QueryRelationshipType.IN);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQueryFieldAndValueDontMatchLikeTest()
    {
        this.thrown.expect(MastQueryException.class);

        this.query.setField(ID);
        this.query.setValue(NAME.getClass());
        this.query.setRelationship(QueryRelationshipType.LIKE);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleEQInvalidDateTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.MUST_BE_A_NUMBER_OR_DATE, DATE, QueryRelationshipType.EQ));

        this.query.setField(DATE);
        this.query.setValue(DATE);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleNEInvalidDateTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.MUST_BE_A_NUMBER_OR_DATE, DATE, QueryRelationshipType.NE));

        this.query.setField(DATE);
        this.query.setValue(DATE);
        this.query.setRelationship(QueryRelationshipType.NE);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleEQandNEEmptyJoinTest()
    {
        this.query.setJoinField("");
        this.query.setField(ID);
        this.query.setValue(ONE);

        final String[] symbols = {"=", "<>"};
        final QueryRelationshipType[] relationships = {QueryRelationshipType.EQ, QueryRelationshipType.NE};

        for (int i = 0; i < TWO; ++i)
        {
            this.query.setRelationship(relationships[i]);

            final Pattern expectedSql = Pattern
                    .compile("^select [a-zA-Z0-9]* from TestDO as [a-zA-Z0-9]* where [a-zA-Z0-9]*\\.id" + symbols[i] + "1L$");

            this.predicateList = new Predicate[0];
            this.queryProcessor = new QueryProcessor();

            this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
            this.criteriaQuery.where(this.predicateList);

            final String sql = entityManager.createQuery(this.criteriaQuery).unwrap(org.hibernate.Query.class).getQueryString();

            assertEquals(ONE, this.predicateList.length);
            assertTrue(expectedSql.matcher(sql).matches());
        }
    }

    @Test
    public void processQuerySimpleNullFields1Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setField(ID);
        this.query.setValue(NAME);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleNullFields2Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setField(ID);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleNullFields3Test()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setValue(NAME);
        this.query.setRelationship(QueryRelationshipType.EQ);

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleEmptyAndTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setAnd(new ArrayList<AbstractQueryDto>());

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleEmptyOrTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setOr(new ArrayList<AbstractQueryDto>());

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    @Test
    public void processQuerySimpleEmptyNotTest()
    {
        this.thrown.expect(MastQueryException.class);
        this.thrown.expectMessage(String.format(ExceptionMessageUtils.INCORRECT_FORMAT));

        this.query.setNot(new AbstractQueryDto());

        this.predicateList = this.queryProcessor.processQuery(this.predicateList, this.builder, this.root, this.query);
    }

    private String getFieldName(final Order order)
    {
        String returnValue = null;

        try
        {
            final Field attributeField = order.getExpression().getClass().getDeclaredField("attribute");
            attributeField.setAccessible(true);
            final SingularAttribute<Object, Object> attribute = (SingularAttribute<Object, Object>) attributeField.get(order.getExpression());
            returnValue = attribute.getName();
        }
        catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException exception)
        {
            throw new AssertionError(exception);
        }

        return returnValue;
    }
}
