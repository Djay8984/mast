package com.baesystems.ai.lr.domain.interceptors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.baesystems.ai.lr.domain.interceptors.NullsLastInterceptor;

public class NullsLastInterceptorTest
{
    private static final String END = " limit ?,?";

    private static final String NO_FIELDS = "query";

    private static final String SINGLE_FIELD = "query order by object.field_1 asc";
    private static final String SINGLE_FIELD_RESULT = "query order by isNull(object.field_1), object.field_1 asc";

    private static final String MULTIPLE_FIELD = "query order by object.field_1 asc, object.field_2 asc";
    private static final String MULTIPLE_FIELD_RESULT = "query order by isNull(object.field_1), object.field_1 asc, isNull(object.field_2), object.field_2 asc";

    private final NullsLastInterceptor nullsLastInterceptor = new NullsLastInterceptor();

    public void checkFields(final String fields, final String expectedResult)
    {
        final String result = this.nullsLastInterceptor.onPrepareStatement(fields);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void processSingleField()
    {
        this.checkFields(SINGLE_FIELD, SINGLE_FIELD_RESULT);
    }

    @Test
    public void processMultipleFields()
    {
        this.checkFields(MULTIPLE_FIELD, MULTIPLE_FIELD_RESULT);
    }

    @Test
    public void processNoFields()
    {
        this.checkFields(NO_FIELDS, NO_FIELDS);
    }

    @Test
    public void processMultipleFieldsNotAtEnd()
    {
        this.checkFields(MULTIPLE_FIELD + END, MULTIPLE_FIELD_RESULT + END);
    }
}
