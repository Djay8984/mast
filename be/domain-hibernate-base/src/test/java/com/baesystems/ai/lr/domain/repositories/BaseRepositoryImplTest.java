package com.baesystems.ai.lr.domain.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.baesystems.ai.lr.domain.entities.TestDO;
import com.baesystems.ai.lr.domain.exception.MastQueryException;
import com.baesystems.ai.lr.dto.query.AbstractQueryDto;
import com.baesystems.ai.lr.enums.QueryRelationshipType;

@RunWith(MockitoJUnitRunner.class)
public class BaseRepositoryImplTest<S>
{
    private static final String INVALID = "invalid";

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FOUR = 4;

    private static final Long ONE_LONG = 1L;
    private static final Long TWO_LONG = 2L;
    private static final Long THREE_LONG = 3L;

    private static EntityManager entityManager;

    private AbstractQueryDto query;

    private static TestRepository testRepository;
    private static TestWithMissingFieldsAllowedRepository testWithMissingFieldsAllowedRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void startUp()
    {
        final EntityManagerFactory factory = Persistence.createEntityManagerFactory("testMastPersistence");
        entityManager = factory.createEntityManager();
        testRepository = new BaseRepositoryFactory(entityManager).getRepository(TestRepository.class);
        testWithMissingFieldsAllowedRepository = new BaseRepositoryFactory(entityManager).getRepository(TestWithMissingFieldsAllowedRepository.class);
        testWithMissingFieldsAllowedRepository.setStrictMissingFields(false);
    }

    @Before
    public void setup()
    {
        this.query = new AbstractQueryDto();
    }

    @Test
    public void findAllList()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);

            final List<TestDO> result = testRepository.findAll(this.query);

            assertEquals(ONE, result.size());
            assertEquals(ONE_LONG, result.get(0).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllListWithSort()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);

            testRepository.save(test1);
            testRepository.save(test2);

            final List<TestDO> result = testRepository.findAll(new Sort("id"), this.query);

            assertEquals(TWO, result.size());
            assertEquals(ONE_LONG, result.get(0).getId());
            assertEquals(TWO_LONG, result.get(1).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllCount()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);

            final Long result = testRepository.count(this.query);

            assertEquals(ONE_LONG, result);
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllPage()
    {
        entityManager.getTransaction().begin();
        try
        {
            final Pageable pageable = new PageRequest(ZERO, ONE);

            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);

            final Page<TestDO> result = testRepository.findAll(pageable, this.query);

            this.testPagination(result, ZERO, ONE, ONE, ONE);
            assertEquals(ONE_LONG, result.getContent().get(0).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllSecondPage()
    {
        entityManager.getTransaction().begin();
        try
        {
            final Pageable pageable = new PageRequest(ONE, ONE);

            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);
            testRepository.save(test1);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);
            testRepository.save(test2);

            final TestDO test3 = new TestDO();
            test3.setId(THREE_LONG);
            testRepository.save(test3);

            final Page<TestDO> result = testRepository.findAll(pageable, this.query);

            this.testPagination(result, ONE, ONE, THREE, THREE);
            assertEquals(TWO_LONG, result.getContent().get(0).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllLargerPage()
    {
        entityManager.getTransaction().begin();
        try
        {
            final Pageable pageable = new PageRequest(ZERO, TWO);

            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);
            testRepository.save(test1);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);
            testRepository.save(test2);

            final TestDO test3 = new TestDO();
            test3.setId(THREE_LONG);
            testRepository.save(test3);

            final Page<TestDO> result = testRepository.findAll(pageable, this.query);

            this.testPagination(result, ZERO, TWO, THREE, TWO);
            assertEquals(ONE_LONG, result.getContent().get(0).getId());
            assertEquals(TWO_LONG, result.getContent().get(1).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllSecondPageLargerPage()
    {
        entityManager.getTransaction().begin();
        try
        {
            final Pageable pageable = new PageRequest(ONE, TWO);

            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);
            testRepository.save(test1);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);
            testRepository.save(test2);

            final TestDO test3 = new TestDO();
            test3.setId(THREE_LONG);
            testRepository.save(test3);

            final Page<TestDO> result = testRepository.findAll(pageable, this.query);

            this.testPagination(result, ONE, ONE, THREE, TWO);
            assertEquals(THREE_LONG, result.getContent().get(0).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllPageOutOfRange()
    { // this should Produce an empty page without calling the second query (only the count).
        entityManager.getTransaction().begin();
        try
        {
            final Pageable pageable = new PageRequest(FOUR, TWO);

            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);
            testRepository.merge(test1);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);
            testRepository.merge(test2);

            final TestDO test3 = new TestDO();
            test3.setId(THREE_LONG);
            testRepository.merge(test3);

            final Page<TestDO> result = testRepository.findAll(pageable, this.query);

            this.testPagination(result, FOUR, ZERO, THREE, TWO);
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void testOtherConstuctor() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
    {
        final BaseRepository baseRepository = new BaseRepositoryImpl(TestDO.class, entityManager);

        final Field entityManagerField = baseRepository.getClass().getSuperclass().getDeclaredField("entityManager");
        entityManagerField.setAccessible(true);
        final Object repoEntityManager = entityManagerField.get(baseRepository);

        final Field domainClassField = baseRepository.getClass().getSuperclass().getDeclaredField("domainClass");
        domainClassField.setAccessible(true);
        final Object domainClass = domainClassField.get(baseRepository);

        assertNotNull(repoEntityManager);
        assertNotNull(domainClass);
        assertEquals(TestDO.class, domainClass);
    }

    @Test
    public void findAllNoQuery()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);

            final List<TestDO> result = testRepository.findAll((AbstractQueryDto) null);

            assertEquals(ONE, result.size());
            assertEquals(ONE_LONG, result.get(0).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllListWithSortNoQuery()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test1 = new TestDO();
            test1.setId(ONE_LONG);

            final TestDO test2 = new TestDO();
            test2.setId(TWO_LONG);

            testRepository.save(test1);
            testRepository.save(test2);

            final List<TestDO> result = testRepository.findAll(new Sort("id"), null);

            assertEquals(TWO, result.size());
            assertEquals(ONE_LONG, result.get(0).getId());
            assertEquals(TWO_LONG, result.get(1).getId());
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllPaginatedNoQuery()
    {
        final Pageable pageable = new PageRequest(ZERO, ONE);

        final TestDO test = new TestDO();
        test.setId(ONE_LONG);

        testRepository.save(test);

        final Page<TestDO> result = testRepository.findAll(pageable, null);

        this.testPagination(result, ZERO, ZERO, ZERO, ZERO);
    }

    @Test
    public void countNoQuery()
    {
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);

            final Long result = testRepository.count((AbstractQueryDto) null);

            assertEquals(ONE_LONG, result);
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    public void findAllListMissingField()
    {
        this.query.setField(INVALID);
        this.query.setValue(ONE_LONG);
        this.query.setRelationship(QueryRelationshipType.EQ);
        this.query.setSelectWhenFieldIsMissing(true);
        entityManager.getTransaction().begin();
        try
        {
            final TestDO test = new TestDO();
            test.setId(ONE_LONG);

            testRepository.save(test);
            testWithMissingFieldsAllowedRepository.save(test);

            try
            {
                testRepository.findAll(this.query);
            }
            catch (final MastQueryException exception)
            {
                final List<TestDO> result = testWithMissingFieldsAllowedRepository.findAll(this.query);

                assertEquals(ONE, result.size());
                assertEquals(ONE_LONG, result.get(0).getId());
            }
        }
        finally
        {
            entityManager.getTransaction().rollback();
        }
    }

    private void testPagination(final Page<?> result, final int pageNumber, final int numberOfElements, final int total, final int totalPages)
    {
        assertEquals(pageNumber, result.getNumber());
        assertEquals(numberOfElements, result.getNumberOfElements());
        assertEquals(total, result.getTotalElements());
        assertEquals(totalPages, result.getTotalPages());
        assertEquals(numberOfElements, result.getContent().size());
    }
}
