package com.baesystems.ai.lr.domain.repositories;

import com.baesystems.ai.lr.domain.entities.TestDO;

public interface TestWithMissingFieldsAllowedRepository extends SpringDataRepository<TestDO, Long>
{
    void setStrictMissingFields(boolean strictMissingFields);
}
