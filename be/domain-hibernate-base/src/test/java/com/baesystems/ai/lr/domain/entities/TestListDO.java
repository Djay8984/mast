package com.baesystems.ai.lr.domain.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MAST_TEST_LIST")
public class TestListDO extends BaseDO
{
    @Id
    @Column(name = "id")
    private Long id;

    @OneToMany(mappedBy = "list", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<TestDO> testDos;

    @Override
    public Long getId()
    {
        return this.id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

    public List<TestDO> getTestDos()
    {
        return testDos;
    }

    public void setTestDos(final List<TestDO> testDos)
    {
        this.testDos = testDos;
    }
}
