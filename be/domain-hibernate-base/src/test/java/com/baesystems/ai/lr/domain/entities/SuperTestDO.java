package com.baesystems.ai.lr.domain.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class SuperTestDO extends BaseDO
{
    @Column(name = "superTestField")
    private Date superTestField;

    public Date getSuperTestField()
    {
        return this.superTestField;
    }

    public void setSuperTestField(final Date superTestField)
    {
        this.superTestField = superTestField;
    }

    @Override
    public Long getId()
    {
        return null;
    }
}
